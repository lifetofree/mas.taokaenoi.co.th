﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="employee_profile.aspx.cs" Inherits="websystem_hr_employee_profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImage").MultiFile();
            }
        })
    </script>

    <script type="text/javascript">
        function openModal() {
            $('#ordine').modal('show');
        }
    </script>


    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewEdit" runat="server">
            <asp:Label ID="Label2" runat="server"></asp:Label>
            <asp:HyperLink runat="server" ID="txtfocus_edit" />
            <%----------- Menu Tab Start---------------%>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-wrench"></i><strong>&nbsp; Process การดำเนินการ</strong></h3>
                </div>
                <div class="form-horizontal" role="form">
                    <div class="panel-body">
                        <div class="form-group col-sm-12">

                            <div class="col-sm-1"></div>
                            <div class="col-sm-2">
                                <asp:ImageButton ID="btngen_edit" CssClass="img-responsive img-fluid" Width="60%" runat="server" CommandName="btngeneral_edit" CommandArgument="1" OnCommand="btnCommand" />
                            </div>
                            <div class="col-sm-2">
                                <asp:ImageButton ID="btnpos_edit" CssClass="img-responsive img-fluid" Width="60%" runat="server" CommandName="btnposition_edit" CommandArgument="2" OnCommand="btnCommand" />
                            </div>
                            <div class="col-sm-2">
                                <asp:ImageButton ID="btnaddress_edit" CssClass="img-responsive img-fluid" Width="60%" runat="server" CommandName="btnaddress_edit" CommandArgument="3" OnCommand="btnCommand" />
                            </div>
                            <div class="col-sm-2">
                                <asp:ImageButton ID="btnheal_edit" CssClass="img-responsive img-fluid" Width="60%" runat="server" CommandName="btnhealth_edit" CommandArgument="4" OnCommand="btnCommand" />
                            </div>
                            <div class="col-sm-2">
                                <asp:ImageButton ID="btmapprove_edit" CssClass="img-responsive img-fluid" Width="60%" runat="server" CommandName="btnapprove_edit" CommandArgument="5" OnCommand="btnCommand" />
                            </div>
                            <div class="col-sm-1"></div>
                        </div>
                    </div>

                </div>
            </div>
            <%-------------- Menu Tab End--------------%>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลพนักงาน</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">

                        <asp:FormView ID="FvEdit_Detail" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <EditItemTemplate>



                                <%-------------------------------- ข้อมูลทั่วไป --------------------------------%>
                                <asp:Panel ID="BoxGen" runat="server">

                                    <div class="form-group">
                                        <label class="col-sm-10">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label109" runat="server" CssClass="h01" Text="Personal Information" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label110" runat="server" Text="ข้อมูลส่วนตัว" /></b></small>
                                                <asp:Label ID="lbl_emp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>' />
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:LinkButton ID="btnchangepass" CssClass="btn btn-info" runat="server" OnCommand="btnCommand" CommandName="BtnChangePassword" data-toggle="tooltip" title="เปลี่ยนรหัสผ่าน"><i class="fa fa-refresh"> เปลี่ยนรหัสผ่าน</i></asp:LinkButton>
                                        </div>



                                        <div id="ordine" class="modal open" role="dialog">
                                            <div class="modal-dialog" style="width: 40%;">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title">เปลี่ยนรหัสผ่าน</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="panel-body">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label9" runat="server" Text="New Password" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label12" runat="server" Text="รหัสผ่านใหม่" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txtpass_1" CssClass="form-control" runat="server" ValidationGroup="ChangePass"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RPass_1" ValidationGroup="ChangePass" runat="server" Display="None"
                                                                        ControlToValidate="txtpass_1" Font-Size="11"
                                                                        ErrorMessage="รหัสผ่าน ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RPass_1" Width="160" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-5 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label13" runat="server" Text="New Password" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label65" runat="server" Text="รหัสผ่านใหม่ อีกครั้ง" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txtpass_2" CssClass="form-control" runat="server" ValidationGroup="ChangePass"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RPass_2" ValidationGroup="ChangePass" runat="server" Display="None"
                                                                        ControlToValidate="txtpass_2" Font-Size="11"
                                                                        ErrorMessage="รหัสผ่าน ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Vsalidlwoer6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RPass_2" Width="160" />

                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-sm-2  control-label col-sm-offset-5">
                                                                    <asp:LinkButton ID="btnupdate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="btnChange_Password" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" ValidationGroup="ChangePass" UseSubmitBehavior="false">Save &nbsp;</asp:LinkButton>
                                                                </div>
                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lbl_massage_alert" runat="server"></asp:Label>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="btnClose_Change_Password">Close &nbsp;</asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label ">
                                        </label>

                                        <label class="col-sm-1 control-label ">
                                            <asp:Image ID="image" runat="server"></asp:Image>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label ">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label443" runat="server" Text="Picture Profile" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label444" runat="server" Text="รูปภาพส่วนตัว" /></b></small>
                                            </h2>
                                        </label>

                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <div class="col-sm-2">
                                                    <asp:FileUpload ID="UploadFile" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass=" btn-sm multi max-1" accept="jpg" />
                                                    <small>
                                                        <p class="help-block"><font color="red">**Attach File name jpg</font></p>
                                                    </small>
                                                    <p />
                                                    <asp:LinkButton ID="btnupload" OnCommand="btnCommand" CommandName="CmdUpload" runat="server" CssClass="btn-sm btn-primary" Text="Upload File"></asp:LinkButton>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>



                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label14" runat="server" Text="EmpCode" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label18" runat="server" Text="รหัสพนักงาน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txt_emp_code" CssClass="form-control" runat="server" Enabled="false" Text='<%# Eval("emp_code") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label17" runat="server" Text="Employee In" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label19" runat="server" Text="วันที่เริ่มงาน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txt_startdate" CssClass="form-control from-date-datepicker" Enabled="false" MaxLengh="100%" runat="server" Text='<%# Eval("emp_start_date") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="RqDocCwreate" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="txt_startdate" Font-Size="11"
                                                    ErrorMessage="Plese startdate" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqDocCwreate" Width="160" />
                                            </div>
                                        </div>


                                    </div>



                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label74" runat="server" Text="EmployeeGroupType" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label77" runat="server" Text="ประเภทกลุ่มพนักงาน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_target" runat="server" Visible="false" Text='<%# Eval("TIDX") %>'></asp:Label>
                                            <asp:DropDownList ID="ddl_target_edit" Enabled="false" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">ประเภทกลุ่มพนักงาน....</asp:ListItem>
                                                <asp:ListItem Value="1">Daily</asp:ListItem>
                                                <asp:ListItem Value="2">Mth</asp:ListItem>
                                                <asp:ListItem Value="3">Office</asp:ListItem>
                                                <asp:ListItem Value="4">Sup</asp:ListItem>
                                                <asp:ListItem Value="5">Ast.</asp:ListItem>
                                                <asp:ListItem Value="6">Mgr</asp:ListItem>
                                                <asp:ListItem Value="7">Dir</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label20" runat="server" Text="EmployeeType" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label21" runat="server" Text="ประเภทพนักงาน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_emptype" runat="server" Visible="false" Text='<%# Eval("emp_type_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlemptype" Enabled="false" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">ประเภทพนักงาน....</asp:ListItem>
                                                <asp:ListItem Value="1">รายวัน</asp:ListItem>
                                                <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                                                <asp:ListItem Value="3">รายเดือน จ่าย 2 ครั้ง</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Requir0" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlemptype" Font-Size="11"
                                                ErrorMessage="ประเภทพนักงาน ...."
                                                ValidationExpression="ประเภทพนักงาน ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCa83l09loutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requir0" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Labe4l228" runat="server" Text="Location" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label3230" runat="server" Text="ประจำสำนักงาน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_location_idx" runat="server" Visible="false" Text='<%# Eval("LocIDX") %>'></asp:Label>
                                            <asp:DropDownList ID="ddllocation" Enabled="false" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">ประจำสำนักงาน....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label267" runat="server" Text="Calculate Tax" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label268" runat="server" Text="จำนวนเดือนที่ใช้คำนวนภาษี" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txttax_mounth" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label ">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label23" runat="server" Text="Employee Probation" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label44" runat="server" Text="วันผ่านทดลองงาน" /></b></small>
                                                <asp:Label ID="lbl_probation_status" Visible="false" Text='<%# Eval("emp_probation_status") %>' runat="server" />
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txt_probationdate" CssClass="form-control from-date-datepicker" Enabled="false" MaxLengh="100%" runat="server" Text='<%# Eval("emp_probation_date") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label48" runat="server" Text="Employee Out" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label57" runat="server" Text="วันลาออก" /></b></small>
                                                <asp:Label ID="lbl_emp_status" Visible="false" Text='<%# Eval("emp_status") %>' runat="server" />
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txt_empoutdate" CssClass="form-control from-date-datepicker" Enabled="false" MaxLengh="100%" runat="server" Text='<%# Eval("emp_resign_date") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label222" runat="server" Text="Name(Thai)" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label224" runat="server" Text="ชื่อ(ไทย)" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-1">
                                            <asp:Label ID="lbl_prefix_idx" runat="server" Visible="false" Text='<%# Eval("prefix_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddl_prefix_th_edit" runat="server" CssClass="form-control" Width="105" Enabled="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="1">นาย</asp:ListItem>
                                                <asp:ListItem Value="2">นางสาว</asp:ListItem>
                                                <asp:ListItem Value="3">นาง</asp:ListItem>
                                                <asp:ListItem Value="4">อื่นๆ</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txt_name_th" CssClass="form-control" runat="server" Enabled="true" Text='<%# Eval("emp_firstname_th") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="C_NameTH" runat="server" ControlToValidate="txt_name_th" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameTH" Width="160" />

                                            <asp:RegularExpressionValidator ID="R_NamTH" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_name_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NamTH" Width="160" />
                                        </div>


                                        <label class="col-sm-1 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label1" runat="server" Text="Lastname" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label4" runat="server" Text="นามสกุล" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txt_surname_th" CssClass="form-control" runat="server" Enabled="true" Text='<%# Eval("emp_lastname_th") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="C_LastnameTH" runat="server" ControlToValidate="txt_surname_th" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameTH" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameTH" Width="160" />

                                            <asp:RegularExpressionValidator ID="R_LastNameTH" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_surname_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastNameTH2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameTH" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label5" runat="server" Text="Name(English)" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label22" runat="server" Text="ชื่อ(อังกฤษ)" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-1">
                                            <asp:DropDownList ID="ddl_prefix_en_edit" runat="server" CssClass="form-control" Enabled="true" Width="105" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="1">Mr.</asp:ListItem>
                                                <asp:ListItem Value="2">Miss</asp:ListItem>
                                                <asp:ListItem Value="3">Mrs.</asp:ListItem>
                                                <asp:ListItem Value="4">Orther.</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txt_name_en" CssClass="form-control" runat="server" Enabled="true" Text='<%# Eval("emp_firstname_en") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="C_NameEN" runat="server" ControlToValidate="txt_name_en" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameEN" Width="160" />

                                            <asp:RegularExpressionValidator ID="R_NameEN" runat="server" ErrorMessage="*เเฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_name_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NameEN" Width="160" />
                                        </div>

                                        <label class="col-sm-1 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label8" runat="server" Text="Lastname" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label24" runat="server" Text="นามสกุล" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txt_surname_en" CssClass="form-control" runat="server" Enabled="true" Text='<%# Eval("emp_lastname_en") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="C_LastnameEN" runat="server" ControlToValidate="txt_surname_en" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameEN" Width="160" />

                                            <asp:RegularExpressionValidator ID="R_LastNameEN" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_surname_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameEN" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label10" runat="server" Text="NickName(Thai)" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label25" runat="server" Text="ชื่อเล่น(ไทย)" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txt_nickname_th" CssClass="form-control" runat="server" Enabled="false" Text='<%# Eval("emp_nickname_th") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                            <asp:RegularExpressionValidator ID="R_NickNameTH" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_nickname_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="SaveE" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NickNameTH" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NickNameTH" Width="160" />
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label11" runat="server" Text="NickName(English)" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label26" runat="server" Text="ชื่อเล่น(อังกฤษ)" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txt_nickname_en" CssClass="form-control" runat="server" Enabled="false" Text='<%# Eval("emp_nickname_en") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                            <asp:RegularExpressionValidator ID="R_NickNameEN" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_nickname_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="SaveE" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NickNameEN" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NickNameEN" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label229" runat="server" Text="CostCenter" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label30" runat="server" Text="ศูนย์ค่าใช้จ่าย" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_cost_idx" runat="server" Visible="false" Text='<%# Eval("costcenter_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlcostcenter" runat="server" Enabled="false" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือก Cost Center....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label15" runat="server" Text="Birthday" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label16" runat="server" Text="วันเกิด" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtbirth" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" Text='<%# Eval("emp_birthday") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label ">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label27" runat="server" Text="Sex" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label28" runat="server" Text="เพศ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_sex_idx" runat="server" Visible="false" Text='<%# Eval("sex_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddl_sex" runat="server" Enabled="false" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกสถานะเพศ....</asp:ListItem>
                                                <asp:ListItem Value="1">ชาย</asp:ListItem>
                                                <asp:ListItem Value="2">หญิง</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Labe3l15" runat="server" Text="Status" /><br />
                                                <small><b>
                                                    <asp:Label ID="Lab2el16" runat="server" Text="สถานภาพ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_married_status_idx" runat="server" Visible="false" Text='<%# Eval("married_status") %>'></asp:Label>
                                            <asp:DropDownList ID="ddl_status" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกสถานภาพ....</asp:ListItem>
                                                <asp:ListItem Value="1">โสด</asp:ListItem>
                                                <asp:ListItem Value="2">สมรส</asp:ListItem>
                                                <asp:ListItem Value="3">หย่าร้าง</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Labe4l231" runat="server" Text="Nationality" /><br />
                                                <small><b>
                                                    <asp:Label ID="Lab3el232" runat="server" Text="สัญชาติ" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_nation_idx" runat="server" Visible="false" Text='<%# Eval("nat_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlnation_edit" runat="server" CssClass="form-control" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกสัญชาติ....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="La2bel42" runat="server" Text="Race" /><br />
                                                <small><b>
                                                    <asp:Label ID="La4bel43" runat="server" Text="เชื้อชาติ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_race_idx" runat="server" Visible="false" Text='<%# Eval("race_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlrace" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกเชื้อชาติ....</asp:ListItem>
                                                <asp:ListItem Value="1">ไทย</asp:ListItem>
                                                <asp:ListItem Value="2">ลาว</asp:ListItem>
                                                <asp:ListItem Value="3">พม่า</asp:ListItem>
                                                <asp:ListItem Value="4">กัมพูชา</asp:ListItem>
                                                <asp:ListItem Value="5">ไทยใหญ่</asp:ListItem>
                                                <asp:ListItem Value="6">ไทยลื้อ</asp:ListItem>
                                                <asp:ListItem Value="7">อื่นๆ</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Lab4el47" runat="server" Text="Religion" /><br />
                                                <small><b>
                                                    <asp:Label ID="Lab3el49" runat="server" Text="ศาสนา" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_rel_idx" runat="server" Visible="false" Text='<%# Eval("rel_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlreligion" runat="server" Enabled="false" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกศาสนา....</asp:ListItem>
                                                <asp:ListItem Value="1">ศาสนาพุทธ</asp:ListItem>
                                                <asp:ListItem Value="2">ศาสนาคริสต์</asp:ListItem>
                                                <asp:ListItem Value="3">ศาสนาอิศลาม</asp:ListItem>
                                                <asp:ListItem Value="4">อื่นๆ</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label29" runat="server" Text="MilitaryStatus" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label31" runat="server" Text="สถานะทางทหาร" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_mil_idx" runat="server" Visible="false" Text='<%# Eval("mil_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlmilitary_edit" runat="server" Enabled="false" CssClass="form-control">
                                                <asp:ListItem Value="1">ผ่านการเกณทหาร</asp:ListItem>
                                                <asp:ListItem Value="2">เรียนรักษาดินแดน</asp:ListItem>
                                                <asp:ListItem Value="3">ได้รับข้อยกเว้น</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label32" runat="server" Text="ID Card" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label33" runat="server" Text="เลขบัตรประชาชน / Tax ID (ต่างชาติ)" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtidcard" CssClass="form-control" runat="server" Enabled="true" Text='<%# Eval("identity_card") %>'></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label36" runat="server" Text="Issued At" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label37" runat="server" Text="อกกให้ ณ" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtissued_at" CssClass="form-control" runat="server" Enabled="true" Text='<%# Eval("issued_at") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label34" runat="server" Text="Expiration Date" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label35" runat="server" Text="วันหมดอายุ" /></b></small>
                                            </h2>
                                        </label>
                                        <asp:UpdatePanel ID="updateRoomInsert" runat="server">
                                            <ContentTemplate>
                                                <div class="col-sm-2">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="txtexpcard" CssClass="form-control from-date-datepicker" MaxLengh="100%" Enabled="true" runat="server" Text='<%# Eval("idate_expired") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <%-----------------------------------------------------------------------------------------------%>
                                    <hr />
                                    <%--Driving Status--%>
                                    <%-----------------------------------------------------------------------------------------------%>

                                    <div class="form-group">
                                        <label class="col-sm-12">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label111" runat="server" CssClass="h01" Text="Driving Status" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label112" runat="server" Text="สถานะการขับขี่ยานพาหนะ" /></b></small>
                                            </h2>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label ">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label113" runat="server" Text="Driving Car" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label114" runat="server" Text="ขับขี่รถยนต์" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_dvcar_idx" runat="server" Visible="false" Text='<%# Eval("dvcar_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddldvcar" runat="server" Enabled="false" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label115" runat="server" Text="Licens Driving Car" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label116" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtlicen_car" CssClass="form-control" runat="server" Enabled="false" Text='<%# Eval("dvlicen_car") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label ">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label275" runat="server" Text="Driving Car Status" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label276" runat="server" Text="รถยนต์ส่วนตัว" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_dvcarstatus_idx" runat="server" Visible="false" Text='<%# Eval("dvcarstatus_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddldvcarstatus" runat="server" Enabled="false" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกสถานะรถ....</asp:ListItem>
                                                <asp:ListItem Value="1">มีรถยนต์ส่วนตัว</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่มีรถยนต์ส่วนตัว</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label241" runat="server" Text="Driving Motocyle" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label242" runat="server" Text="ขับขี่รถจักรยานยนต์" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_dvmt_idx" runat="server" Visible="false" Text='<%# Eval("dvmt_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddldvmt" runat="server" Enabled="false" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label228" runat="server" Text="Licens Driving Motocyle" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label230" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtlicens_moto" CssClass="form-control" runat="server" Enabled="false" Text='<%# Eval("dvlicen_moto") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label ">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label277" runat="server" Text="Driving Motocyle Status" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label278" runat="server" Text="รถจักรยานยนต์ส่วนตัว" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_dvmtstatus_idx" runat="server" Visible="false" Text='<%# Eval("dvmtstatus_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddldvmtstatus" runat="server" Enabled="false" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกสถานะรถ....</asp:ListItem>
                                                <asp:ListItem Value="1">มีรถจักรยานยนต์ส่วนตัว</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่มีรถจักรยานยนต์ส่วนตัว</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label233" runat="server" Text="Driving Forklift" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label234" runat="server" Text="ขับขี่รถโฟล์คลิฟท์" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_dvfork_idx" runat="server" Visible="false" Text='<%# Eval("dvfork_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddldvfork" runat="server" Enabled="false" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label235" runat="server" Text="Licens Driving Forklift" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label236" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtlicens_fork" CssClass="form-control" runat="server" Enabled="false" Text='<%# Eval("dvlicen_fork") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label237" runat="server" Text="Driving Truck" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label238" runat="server" Text="ขับขี่รถบรรทุก" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_dvtruck_idx" runat="server" Visible="false" Text='<%# Eval("dvtruck_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddldvtruck" runat="server" Enabled="false" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label239" runat="server" Text="Licens Driving Truck" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label240" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtlicens_truck" CssClass="form-control" runat="server" Enabled="false" Text='<%# Eval("dvlicen_truck") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <%-----------------------------------------------------------------------------------------------%>
                                    <hr />
                                    <%--Bank Datail--%>
                                    <%-----------------------------------------------------------------------------------------------%>

                                    <div class="form-group">
                                        <label class="col-sm-12">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label123" runat="server" CssClass="h01" Text="Bank" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label124" runat="server" Text="ข้อมูลทางธนาคาร" /></b></small>
                                            </h2>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label ">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label125" runat="server" Text="Bank" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label126" runat="server" Text="ธนาคาร" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_bank_idx" runat="server" Visible="false" Text='<%# Eval("bank_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="dllbank" runat="server" Enabled="true" CssClass="form-control">
                                                <asp:ListItem Value="1">ไทยพาณิชย์ จำกัด (มหาชน)</asp:ListItem>
                                                <asp:ListItem Value="2">กรุงเทพ จำกัด (มหาชน)</asp:ListItem>
                                                <asp:ListItem Value="3">กรุงไทย จำกัด (มหาชน)</asp:ListItem>
                                                <asp:ListItem Value="4">กสิกรไทย จำกัด (มหาชน)</asp:ListItem>
                                                <asp:ListItem Value="5">กรุงศรีอยุธยา จำกัด (มหาชน)</asp:ListItem>
                                                <asp:ListItem Value="6">ทหารไทย จำกัด (มหาชน)</asp:ListItem>
                                                <asp:ListItem Value="7">นครหลวงไทย จำกัด (มหาชน)</asp:ListItem>
                                                <asp:ListItem Value="8">ยูโอบี จำกัด (มหาชน)</asp:ListItem>
                                                <asp:ListItem Value="9">ธนชาต จำกัด (มหาชน)</asp:ListItem>
                                                <asp:ListItem Value="10">เกียรตินาคิน จำกัด (มหาชน).</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label127" runat="server" Text="Account Name" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label128" runat="server" Text="ชื่อบัญชี" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtaccountname" CssClass="form-control" Enabled="true" runat="server" Text='<%# Eval("bank_name") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label131" runat="server" Text="Account Number" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label132" runat="server" Text="เลขที่บัญชี" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtaccountnumber" CssClass="form-control" Enabled="true" runat="server" Text='<%# Eval("bank_no") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>


                                    <%-----------------------------------------------------------------------------------------------%>
                                    <hr />
                                    <%--Emergency--%>
                                    <%-----------------------------------------------------------------------------------------------%>

                                    <div class="form-group">
                                        <label class="col-sm-12">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label129" runat="server" CssClass="h01" Text="Emergency" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label130" runat="server" Text="ข้อมูลติดต่อกรณีฉุกเฉิน" /></b></small>
                                            </h2>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label135" runat="server" Text="Emergency Contact" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label136" runat="server" Text="กรณีฉุกเฉินติดต่อ" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtemercon" CssClass="form-control" runat="server" Text='<%# Eval("emer_name") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label133" runat="server" Text="Relationship" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label134" runat="server" Text="ความสัมพันธ์" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtrelation" CssClass="form-control" runat="server" Text='<%# Eval("emer_relation") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label137" runat="server" Text="Telephone" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label138" runat="server" Text="เบอร์โทร" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txttelemer" CssClass="form-control" runat="server" Text='<%# Eval("emer_tel") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>


                                    <%-----------------------------------------------------------------------------------------------%>
                                    <hr />
                                    <%--Family--%>
                                    <%-----------------------------------------------------------------------------------------------%>

                                    <div class="form-group">
                                        <label class="col-sm-12">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label139" runat="server" CssClass="h01" Text="Family" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label140" runat="server" Text="ข้อมูลครอบครัว" /></b></small>
                                            </h2>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label141" runat="server" Text="Father Name" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label142" runat="server" Text="ชื่อ-สกุล บิดา" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtfathername_edit" CssClass="form-control" runat="server" Text='<%# Eval("fathername") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label243" runat="server" Text="Licen no." /><br />
                                                <small><b>
                                                    <asp:Label ID="Label244" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtlicenfather_edit" CssClass="form-control" runat="server" Text='<%# Eval("licen_father") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label251" runat="server" Text="Telephone" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label252" runat="server" Text="เบอร์ติดต่อ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txttelfather_edit" CssClass="form-control" runat="server" Text='<%# Eval("telfather") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label145" runat="server" Text="Mother Name" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label146" runat="server" Text="ชื่อ-สกุล มารดา" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtmothername_edit" CssClass="form-control" runat="server" Text='<%# Eval("mothername") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label247" runat="server" Text="Licen no." /><br />
                                                <small><b>
                                                    <asp:Label ID="Label248" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtlicenmother_edit" CssClass="form-control" runat="server" Text='<%# Eval("licen_mother") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label147" runat="server" Text="Telephone" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label148" runat="server" Text="เบอร์ติดต่อ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txttelmother_edit" CssClass="form-control" runat="server" Text='<%# Eval("telmother") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label149" runat="server" Text="Wife Name" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label150" runat="server" Text="ชื่อ-สกุล คู่สมรส" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtwifename_edit" CssClass="form-control" runat="server" Text='<%# Eval("wifename") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label249" runat="server" Text="Licen no." /><br />
                                                <small><b>
                                                    <asp:Label ID="Label250" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtlicenwife_edit" CssClass="form-control" runat="server" Text='<%# Eval("licen_wife") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label151" runat="server" Text="Telephone" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label152" runat="server" Text="เบอร์ติดต่อ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txttelwife_edit" CssClass="form-control" runat="server" Text='<%# Eval("telwife") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-12">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label3" runat="server" CssClass="h01" Text="Family" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label7" runat="server" Text="ข้อมูลบุตร" /></b></small>
                                            </h2>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-12">

                                            <asp:GridView ID="GvChildAdd_View"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover"
                                                HeaderStyle-CssClass="table_headCenter"
                                                HeaderStyle-Height="40px"
                                                ShowFooter="False"
                                                ShowHeaderWhenEmpty="True"
                                                AllowPaging="True"
                                                PageSize="10"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                DataKeyNames="CHIDX"
                                                OnPageIndexChanging="Master_PageIndexChanging">

                                                <PagerStyle CssClass="PageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>

                                                    <asp:TemplateField HeaderText="#">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <%# (Container.DataItemIndex +1) %>
                                                                <asp:Label ID="Lab2el5w82" runat="server" Visible="false" Text='<%# Eval("CHIDX")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="Labe2l5w38" runat="server" Text='<%# Eval("Child_name")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="บุตรคนที่">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lbP2a4g2e1" runat="server" Text='<%# Eval("Child_num")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                    </div>


                                    <%-----------------------------------------------------------------------------------------------%>
                                    <hr />
                                    <%--Reference--%>
                                    <%-----------------------------------------------------------------------------------------------%>

                                    <div class="form-group">
                                        <label class="col-sm-12">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label163" runat="server" CssClass="h01" Text="Reference" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label257" runat="server" Text="บุคคลอ้างอิง" /></b></small>
                                            </h2>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-12">

                                            <asp:GridView ID="GvReference_View"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover"
                                                HeaderStyle-CssClass="table_headCenter"
                                                HeaderStyle-Height="40px"
                                                ShowFooter="False"
                                                ShowHeaderWhenEmpty="True"
                                                AllowPaging="True"
                                                PageSize="5"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                DataKeyNames="ReIDX"
                                                OnPageIndexChanging="Master_PageIndexChanging">

                                                <PagerStyle CssClass="PageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>

                                                    <asp:TemplateField HeaderText="#">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <%# (Container.DataItemIndex +1) %>
                                                                <asp:Label ID="L2abs2del5w8" runat="server" Visible="false" Text='<%# Eval("ReIDX")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="Labe1" runat="server" Text='<%# Eval("ref_fullname")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ตำแหน่ง">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lb3P23ag2e1" runat="server" Text='<%# Eval("ref_position")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ความสัมพันธ์">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lb3P2a1g2e1" runat="server" Text='<%# Eval("ref_relation")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="เบอร์โทรศัพท์">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lb3P2ag2e1" runat="server" Text='<%# Eval("ref_tel")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>

                                        </div>
                                    </div>


                                    <%-----------------------------------------------------------------------------------------------%>
                                    <hr />
                                    <%--Prior Experiences--%>
                                    <%-----------------------------------------------------------------------------------------------%>

                                    <div class="form-group">
                                        <label class="col-sm-12">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label153" runat="server" CssClass="h01" Text="Prior Experiences" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label154" runat="server" Text="ประวัติการทำงาน" /></b></small>
                                            </h2>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-12">

                                            <asp:GridView ID="GvPri_View"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover"
                                                HeaderStyle-CssClass="table_headCenter"
                                                HeaderStyle-Height="40px"
                                                ShowFooter="False"
                                                ShowHeaderWhenEmpty="True"
                                                AllowPaging="True"
                                                PageSize="10"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                DataKeyNames="ExperIDX"
                                                OnPageIndexChanging="Master_PageIndexChanging">

                                                <PagerStyle CssClass="PageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>

                                                    <asp:TemplateField HeaderText="#">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <%# (Container.DataItemIndex +1) %>
                                                                <asp:Label ID="L2ab2del5w8" runat="server" Visible="false" Text='<%# Eval("ExperIDX")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>


                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ชื่อบริษัท">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="L2ab2el5w8" runat="server" Text='<%# Eval("Pri_Nameorg")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ตำแหน่งล่าสุด">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="l2bP42ag2e1" runat="server" Text='<%# Eval("Pri_pos")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="วันที่เริ่มงาน">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lb3wP2a1g2e1" runat="server" Text='<%# Eval("Pri_startdate")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="วันที่ออกจากงาน">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lebP32ag2e1" runat="server" Text='<%# Eval("Pri_resigndate")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>

                                        </div>
                                    </div>


                                    <%-----------------------------------------------------------------------------------------------%>
                                    <hr />
                                    <%--Education--%>
                                    <%-----------------------------------------------------------------------------------------------%>

                                    <div class="form-group">
                                        <label class="col-sm-12">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label164" runat="server" CssClass="h01" Text="Education" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label165" runat="server" Text="ประวัติการศึกษา" /></b></small>
                                            </h2>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-12">

                                            <asp:GridView ID="GvEducation_View"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover"
                                                HeaderStyle-CssClass="table_headCenter"
                                                HeaderStyle-Height="40px"
                                                ShowFooter="False"
                                                ShowHeaderWhenEmpty="True"
                                                AllowPaging="True"
                                                PageSize="10"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                DataKeyNames="EDUIDX"
                                                OnPageIndexChanging="Master_PageIndexChanging">

                                                <PagerStyle CssClass="PageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>

                                                    <asp:TemplateField HeaderText="#">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <%# (Container.DataItemIndex +1) %>
                                                            </div>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="วุฒิการศึกษา">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="La4bel5w8" runat="server" Text='<%# Eval("qualification_name")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ชื่อสถานศึกษา">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="l4bP24ag2e1" runat="server" Text='<%# Eval("Edu_name")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="สาขาวิชาที่ศึกษา">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="l4b4Pag2e1" runat="server" Text='<%# Eval("Edu_branch")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ปีที่จบศึกษา">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="l4bP3ag2e1" runat="server" Text='<%# Eval("Edu_end")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                    </div>


                                    <%-----------------------------------------------------------------------------------------------%>
                                    <hr />
                                    <%--Training History--%>
                                    <%-----------------------------------------------------------------------------------------------%>

                                    <div class="form-group">
                                        <label class="col-sm-12">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label166" runat="server" CssClass="h01" Text="Training History" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label167" runat="server" Text="ประวัติการฝึกอบรม" /></b></small>
                                            </h2>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-12">

                                            <asp:GridView ID="GvTrain_View"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover"
                                                HeaderStyle-CssClass="table_headCenter"
                                                HeaderStyle-Height="40px"
                                                ShowFooter="False"
                                                ShowHeaderWhenEmpty="True"
                                                AllowPaging="True"
                                                PageSize="10"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                DataKeyNames="TNIDX"
                                                OnPageIndexChanging="Master_PageIndexChanging">

                                                <PagerStyle CssClass="PageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>

                                                    <asp:TemplateField HeaderText="#">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <%# (Container.DataItemIndex +1) %>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หลักสูตรอาบรม">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="Labewl5w8" runat="server" Text='<%# Eval("Train_courses")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="วันที่อบรม">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lbP2aw2g2e1" runat="server" Text='<%# Eval("Train_date")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ผลการประเมิน">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lbPagw2e1" runat="server" Text='<%# Eval("Train_assessment")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                    </div>

                                </asp:Panel>

                                <%-------------------------------- ข้อมูลตำแหน่ง --------------------------------%>
                                <asp:Panel ID="BoxPos" runat="server">

                                    <div class="form-group">
                                        <label class="col-sm-12">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label196" runat="server" CssClass="h01" Text="Main Position" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label197" runat="server" Text="ตำแหน่งงานหลัก" /></b></small>
                                                <asp:Label ID="lbl_org_edit" runat="server" Visible="false" Text='<%# Eval("org_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_rdep_edit" runat="server" Visible="false" Text='<%# Eval("rdept_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_rsec_edit" runat="server" Visible="false" Text='<%# Eval("rsec_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_rpos_edit" runat="server" Visible="false" Text='<%# Eval("rpos_idx") %>'></asp:Label>
                                            </h2>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-12">

                                            <asp:GridView ID="GvPosAdd_View"
                                                DataKeyNames="EODSPIDX"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover"
                                                HeaderStyle-CssClass="table_headCenter"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="True"
                                                PageSize="10"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                OnPageIndexChanging="Master_PageIndexChanging">

                                                <PagerStyle CssClass="PageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>

                                                    <asp:TemplateField HeaderText="#">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <%# (Container.DataItemIndex +1) %>
                                                                <asp:Label ID="Label61" runat="server" Text='<%# Eval("OrgIDX_S")%>' Visible="false"></asp:Label>
                                                                <asp:Label ID="Label62" runat="server" Text='<%# Eval("RDeptIDX_S")%>' Visible="false"></asp:Label>
                                                                <asp:Label ID="Label63" runat="server" Text='<%# Eval("RSecIDX_S")%>' Visible="false"></asp:Label>
                                                                <asp:Label ID="Label64" runat="server" Text='<%# Eval("RPosIDX_S")%>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lbEstaout" runat="server" Text='<%# Eval("EStaOut")%>' Visible="false"></asp:Label>
                                                            </div>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="สำนักงาน">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="Label69" runat="server" Text='<%# Eval("OrgNameTH_S")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ฝ่าย">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="Label70" runat="server" Text='<%# Eval("DeptNameTH_S")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="แผนก">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="Label71" runat="server" Text='<%# Eval("SecNameTH_S")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ตำแหน่ง">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="Label72" runat="server" Text='<%# Eval("PosNameTH_S")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Job Grade">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="La2bel72" runat="server" Text='<%# Eval("JobLevel")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="สถานะ">
                                                        <ItemTemplate>
                                                            <div style="text-align: center; padding-top: 10px;">
                                                                <span class='<%# Eval("EStaOut").ToString() == "1" ? "fa fa-check" : "fa fa-minus" %>'></span>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>

                                            </asp:GridView>

                                        </div>
                                    </div>

                                </asp:Panel>

                                <%-------------------------------- ข้อมูลที่อยู่ --------------------------------%>
                                <asp:Panel ID="BoxAddress" runat="server">

                                    <div class="form-group">
                                        <label class="col-sm-12">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label70" runat="server" CssClass="h01" Text="Address & Contact (Present)" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label6" runat="server" Text="ที่อยู่และการติดต่อ(ปัจจุบัน)" /></b></small>
                                            </h2>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label63" runat="server" Text="PresentAddress" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label66" runat="server" Text="ที่อยู่ปัจจุบัน" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtaddress_present" autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" CssClass="form-control" Enabled="true" runat="server" Text='<%# Eval("emp_address") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label68" runat="server" Text="Country" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label69" runat="server" Text="ประเทศ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_country_edit" runat="server" Visible="false" Text='<%# Eval("country_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlcountry_present_edit" runat="server" Enabled="true" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกประเทศ....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Lab3el15" runat="server" Text="Province" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label416" runat="server" Text="จังหวัด" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_prov_edit" runat="server" Visible="false" Text='<%# Eval("prov_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlprovince_present_edit" runat="server" CssClass="form-control" Enabled="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกจังหวัด....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label71" runat="server" Text="Amphoe" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label72" runat="server" Text="อำเภอ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_amp_edit" runat="server" Visible="false" Text='<%# Eval("amp_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlamphoe_present_edit" runat="server" CssClass="form-control" Enabled="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกอำเภอ....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label73" runat="server" Text="District" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label75" runat="server" Text="ตำบล" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_dist_edit" runat="server" Visible="false" Text='<%# Eval("dist_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddldistrict_present_edit" runat="server" CssClass="form-control" Enabled="true" AutoPostBack="true">
                                                <asp:ListItem Value="0">เลือกตำบล....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label79" runat="server" Text="ZipCode" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label80" runat="server" Text="รหัสไปรษณีย์" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtzipcode_present" CssClass="form-control" runat="server" Enabled="true" Text='<%# Eval("post_code") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label83" runat="server" Text="Email" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label84" runat="server" Text="อีเมล" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtemail_present" CssClass="form-control" runat="server" Enabled="true" Text='<%# Eval("emp_email") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                            <asp:RegularExpressionValidator ID="R_Email" runat="server" SetFocusOnError="true" Display="None" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtemail_present" ErrorMessage="*กรอกในรูปแบบEmail" ValidationGroup="fromgroup"></asp:RegularExpressionValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_Email2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Email" Width="160" />
                                        </div>
                                    </div>

                                    <asp:Panel ID="box_email_personal" runat="server" Visible="true">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label440" runat="server" Text="Personal Email" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label441" runat="server" Text="อีเมลส่วนตัว" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="txtpersonal_email" CssClass="form-control" runat="server" Text='<%# Eval("personal_email") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                                <asp:RegularExpressionValidator ID="Reguar1" runat="server" SetFocusOnError="true" Display="None" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtpersonal_email" ErrorMessage="*กรอกในรูปแบบEmail" ValidationGroup="fromgroup"></asp:RegularExpressionValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reguar1" Width="160" />
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label76" runat="server" Text="Mobile Phone" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label78" runat="server" Text="โทรศัพท์มือถือ" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txttelmobile_present" CssClass="form-control" runat="server" Enabled="true" Text='<%# Eval("emp_mobile_no") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label81" runat="server" Text="Home Phone" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label82" runat="server" Text="โทรศัพท์บ้าน" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txttelhome_present" CssClass="form-control" runat="server" Enabled="true" Text='<%# Eval("emp_phone_no") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <%-----------------------------------------------------------------------------------------------%>
                                    <hr />
                                    <%-----------------------------------------------------------------------------------------------%>

                                    <div class="form-group">
                                        <label class="col-sm-12">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label85" runat="server" CssClass="h01" Text="Address & Contact (Permanent)" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label86" runat="server" Text="ที่อยู่และการติดต่อ(ทะเบียนบ้าน)" /></b></small>
                                            </h2>
                                        </label>
                                    </div>

                                    <asp:Panel ID="BoxAddress_edit" runat="server">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label87" runat="server" Text="Permanentaddress" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label88" runat="server" Text="ที่อยู่ตามทะเบียนบ้าน
" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="txtaddress_permanentaddress" autocomplete="off" MaxLength="250" TextMode="MultiLine" Enabled="true" Rows="3" CssClass="form-control" runat="server" Text='<%# Eval("emp_address_permanent") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label89" runat="server" Text="Country" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label90" runat="server" Text="ประเทศ" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lbl_ddlcountry_permanent_edit" runat="server" Visible="false" Text='<%# Eval("country_idx_permanent") %>'></asp:Label>
                                                <asp:DropDownList ID="ddlcountry_permanentaddress_edit" runat="server" Enabled="true" CssClass="form-control">
                                                    <asp:ListItem Value="0">เลือกประเทศ....</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label91" runat="server" Text="Province" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label92" runat="server" Text="จังหวัด" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lbl_ddlprovince_permanent_edit" runat="server" Visible="false" Text='<%# Eval("prov_idx_permanent") %>'></asp:Label>
                                                <asp:DropDownList ID="ddlprovince_permanentaddress_edit" runat="server" CssClass="form-control" Enabled="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="0">เลือกจังหวัด....</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label93" runat="server" Text="Amphoe" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label94" runat="server" Text="อำเภอ" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lbl_ddlamphoe_permanent_edit" runat="server" Visible="false" Text='<%# Eval("amp_idx_permanent") %>'></asp:Label>
                                                <asp:DropDownList ID="ddlamphoe_permanentaddress_edit" runat="server" Enabled="true" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="0">เลือกอำเภอ....</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label95" runat="server" Text="District" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label96" runat="server" Text="ตำบล" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:Label ID="lbl_ddldistrict_permanent_edit" runat="server" Visible="false" Text='<%# Eval("dist_idx_permanent") %>'></asp:Label>
                                                <asp:DropDownList ID="ddldistrict_permanentaddress_edit" runat="server" Enabled="true" CssClass="form-control" AutoPostBack="true">
                                                    <asp:ListItem Value="0">เลือกตำบล....</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label97" runat="server" Text="Home Phone" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label98" runat="server" Text="โทรศัพท์บ้าน" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="txttelmobile_permanentaddress" CssClass="form-control" Enabled="true" runat="server" Text='<%# Eval("PhoneNumber_permanent") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                            </div>

                                        </div>

                                    </asp:Panel>

                                </asp:Panel>

                                <%-------------------------------- ข้อมูลสุขภาพ --------------------------------%>
                                <asp:Panel ID="BoxHeal" runat="server">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label38" runat="server" Text="Currently used hospital" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label39" runat="server" Text="ปัจจุบันใช้สถานพยาบาล" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_hos_edit" runat="server" Visible="false" Text='<%# Eval("soc_hos_idx") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlhospital" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกโรงพยาบาล</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label121" runat="server" Text="Date of examination" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label122" runat="server" Text="วันที่ตรวจร่างกาย" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <div class='input-group date'>
                                                <%--Text='<%# formatDate((String)Eval("Examinationdate")) %>'--%>
                                                <asp:TextBox ID="txtexaminationdate" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" Text='<%# Eval("Examinationdate") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label40" runat="server" Text="Social Security No" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label41" runat="server" Text="เลขที่ประกันสังคม" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtsecurityid" CssClass="form-control" runat="server" Text='<%# Eval("soc_no") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label45" runat="server" Text="Expiration Date" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label46" runat="server" Text="วันหมดอายุ" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <div class='input-group date'>
                                                <%--Text='<%# formatDate((String)Eval("soc_expired_date")) %>' --%>
                                                <asp:TextBox ID="txtexpsecurity" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" Text='<%# Eval("soc_expired_date") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label50" runat="server" Text="Height" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label51" runat="server" Text="ส่วนสูง" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtheight" CssClass="form-control" runat="server" Text='<%# Eval("height_s") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                            <asp:RegularExpressionValidator ID="R_Height" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtheight" ValidationExpression="^[0-9'.\s]{1,3}$" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="VS_Height2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Height" Width="160" />
                                        </div>
                                        <label class="col-sm-1 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label54" runat="server" Text="Cm" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label55" runat="server" Text="ซม." /></b></small>
                                            </h2>
                                        </label>

                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label52" runat="server" Text="Weight" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label53" runat="server" Text="น้ำหนัก" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtweight" CssClass="form-control" runat="server" Text='<%# Eval("weight_s") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                            <asp:RegularExpressionValidator ID="R_Weigth" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtweight" ValidationExpression="^[0-9'.\s]{1,3}$" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_Weigth2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Weigth" Width="160" />
                                        </div>
                                        <label class="col-sm-1 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label56" runat="server" Text="Kg" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label58" runat="server" Text="กก." /></b></small>
                                            </h2>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label59" runat="server" Text="BloodGroup" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label60" runat="server" Text="กรุ๊ปเลือด" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_BRHIDX" runat="server" Visible="false" Text='<%# Eval("BTypeIDX") %>' />
                                            <asp:DropDownList ID="ddlblood" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกกรุ๊ปเลือด....</asp:ListItem>
                                                <asp:ListItem Value="1">เอ (A)</asp:ListItem>
                                                <asp:ListItem Value="2">บี (B)</asp:ListItem>
                                                <asp:ListItem Value="3">โอ (O)</asp:ListItem>
                                                <asp:ListItem Value="4">เอบี (AB)</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label61" runat="server" Text="Scar" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label62" runat="server" Text="ตำหนิ" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtscar" CssClass="form-control" runat="server" Text='<%# Eval("scar_s") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label117" runat="server" Text="Medical certificate" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label118" runat="server" Text="ใบรับรองแพทย์" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_medical_id" runat="server" Visible="false" Text='<%# Eval("medicalcertificate_id") %>' />
                                            <asp:DropDownList ID="ddlmedicalcertificate" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกสถานะใบรับรอง....</asp:ListItem>
                                                <asp:ListItem Value="1">มี</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่มี</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label119" runat="server" Text="Result Lab" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label120" runat="server" Text="ผลตรวจ LAB" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_resultlab_id" runat="server" Visible="false" Text='<%# Eval("resultlab_id") %>' />
                                            <asp:DropDownList ID="ddlresultlab" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกสถานะผลตรวจ....</asp:ListItem>
                                                <asp:ListItem Value="1">มี</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่มี</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                </asp:Panel>

                                <%-------------------------------- ข้อมูลผู้อนุมัติ --------------------------------%>
                                <asp:Panel ID="BoxApprove" runat="server">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label42" runat="server" Text="Approve HRIS 1" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label43" runat="server" Text="ผู้อนุมัติใบลา 1" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_approve_1_edit" runat="server" Visible="false" Text='<%# Eval("emp_idx_approve1") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlapprove1" runat="server" Enabled="false" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกผู้อนุมัติ 1....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label47" runat="server" Text="Approve HRIS 2" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label49" runat="server" Text="ผู้อนุมัติใบลา 2" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:Label ID="lbl_approve_2_edit" runat="server" Visible="false" Text='<%# Eval("emp_idx_approve2") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlapprove2" runat="server" Enabled="false" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกผู้อนุมัติ 2....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                </asp:Panel>

                            </EditItemTemplate>

                        </asp:FormView>

                        <div class="col-sm-1">
                            <hr />
                            <asp:LinkButton ID="LinkButton10" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="btnUpdate" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" ValidationGroup="fromgroup">Save &nbsp;</asp:LinkButton>
                        </div>

                    </div>
                </div>
            </div>
        </asp:View>

    </asp:MultiView>

</asp:Content>

