﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_TrnPlan.aspx.cs" Inherits="websystem_el_TrnPlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">


    <script type="text/javascript"> 

        function openModalTraning() {

            $('#DvUpdpnl_modal_training').modal('show');
        }
        function closeModalTraning() {
            $('#DvUpdpnl_modal_training').modal('hide');
        }

    </script>


    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:Panel ID="Panel3" runat="server" CssClass="m-t-10">
        <asp:Panel ID="panelMenu" runat="server">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <%-- <a class="navbar-brand">Menu</a>--%>
                </div>
                <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left" runat="server">
                        <li id="liListData" runat="server">
                            <asp:LinkButton ID="btnListData" runat="server"
                                OnCommand="btnCommand" CommandName="btnListData" Text="ข้อมูลทั่วไป" />
                        </li>
                        <li id="liInsert" runat="server">
                            <asp:LinkButton ID="btnInsert" runat="server"
                                OnCommand="btnCommand" CommandName="btnInsert" Text="สร้างแผนการฝึกอบรม" />
                        </li>
                        <li id="liplanReport" runat="server" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                                aria-expanded="false">แผนการอบรม <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li id="lischeduler" runat="server" visible="true">
                                    <asp:LinkButton ID="btnscheduler" runat="server"
                                        OnCommand="btnCommand" CommandName="btnscheduler" Text="ตารางแผนการอบรม" />
                                </li>
                                <li id="lirptscheduler" runat="server" visible="true">
                                    <asp:LinkButton ID="btnrptscheduler" runat="server"
                                        OnCommand="btnCommand" CommandName="btnrptscheduler" Text="รายงานแผนการอบรม" />
                                </li>
                            </ul>
                        </li>

                        <li id="liHR_Wait" runat="server" visible="true">
                            <asp:Label ID="lbHR_Wait" runat="server" Text="รายการที่รออนุมัติ" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnHR_Wait" runat="server"
                                OnCommand="btnCommand" CommandName="btnHR_Wait" Text="รายการที่รออนุมัติ" />
                        </li>
                        <li id="liHR_App" runat="server" visible="true">
                            <asp:Label ID="lbHR_App" runat="server" Text="รายการที่อนุมัติเสร็จแล้ว" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnHR_App" runat="server"
                                OnCommand="btnCommand" CommandName="btnHR_App" Text="รายการที่อนุมัติเสร็จแล้ว" />
                        </li>
                        <li id="liMD_Wait" runat="server" visible="true">
                            <asp:Label ID="lbMD_Wait" runat="server" Text="รายการที่รออนุมัติ" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnMD_Wait" runat="server"
                                OnCommand="btnCommand" CommandName="btnMD_Wait" Text="รายการที่รออนุมัติ" />
                        </li>
                        <li id="liMD_App" runat="server" visible="true">
                            <asp:Label ID="lbMD_App" runat="server" Text="รายการที่อนุมัติเสร็จแล้ว" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnMD_App" runat="server"
                                OnCommand="btnCommand" CommandName="btnMD_App" Text="รายการที่อนุมัติเสร็จแล้ว" />
                        </li>

                    </ul>
                </div>
            </nav>

            <div class="clearfix"></div>

        </asp:Panel>

    </asp:Panel>

    <asp:MultiView ID="MultiViewBody" runat="server" ActiveViewIndex="0">

        <asp:View ID="View_ListDataPag" runat="server">

            <asp:Panel ID="pnlListData" runat="server">

                <asp:Panel ID="pnlsearch" runat="server">
                    <div class="panel panel-primary m-t-10">
                        <div class="panel-heading f-bold">ค้นหา</div>
                        <div class="panel-body">
                            <asp:Panel ID="Panel6" runat="server" Visible="false">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>เดือน</label>
                                        <asp:DropDownList ID="ddlMonthSearch_L" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </asp:Panel>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>ปี</label>
                                    <asp:DropDownList ID="ddlYearSearch_L" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>เลขที่เอกสาร</label>
                                    <asp:TextBox ID="txtFilterKeyword_L" runat="server"
                                        CssClass="form-control"
                                        placeholder="เลขที่เอกสาร..." />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>สถานะของเอกสาร</label>
                                    <asp:DropDownList ID="ddlStatusapprove_L" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="999" Text="-" Selected="True" />
                                        <asp:ListItem Value="0" Text="ดำเนินการ" />
                                        <asp:ListItem Value="4" Text="อนุมัติ" />
                                        <asp:ListItem Value="5" Text="กลับไปแก้ไข" />
                                        <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label>
                                    &nbsp;
                                </label>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnFilter" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                        OnCommand="btnCommand" CommandName="btnFilter" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>


                <div class="row">

                    <asp:GridView ID="GvListData"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="10"
                        OnRowCommand="onRowCommand"
                        OnRowDataBound="onRowDataBound"
                        OnPageIndexChanging="gvPageIndexChanging"
                        DataKeyNames="u0_training_plan_idx"
                        ShowFooter="false">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size:13px;">
                                        <%# (Container.DataItemIndex + 1) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size:13px;">
                                        <%# Eval("training_plan_no") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size:13px;">
                                        <%# Eval("training_plan_date") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ปี" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size:13px;">
                                        <%# Eval("training_plan_year") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size:13px;">
                                        <%# Eval("course_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size:13px;">
                                        <%# Eval("training_group_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size:13px;">
                                        <%# Eval("training_branch_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                <ItemTemplate>

                                    <span>
                                       <%-- <%# getTextDoc((int)Eval("approve_status") , 
                                                       (int)Eval("md_approve_status") , 
                                                       (string)Eval("decision_name"),
                                                       (string)Eval("node_name"),
                                                       (string)Eval("actor_name"),
                                                       (string)Eval("md_decision_name"),
                                                       (string)Eval("md_node_name"),
                                                       (string)Eval("md_actor_name")
                                                       ) %>--%>
                                         <%# getTextDoc((string)Eval("zprocess_name") , 
                                                       (int)Eval("el_approve_status") 
                                                       ) %>
                                    </span>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size:13px;">
                                        <%# getStatus( (int)Eval("training_plan_status")) %>
                                        <br />
                                        <%# getStatusPlanCourse( (int)Eval("trn_course_flag")) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>

                                    <asp:TextBox ID="txtapprove_status_GvListData" runat="server"
                                        Visible="false" Text='<%# Eval("approve_status") %>' />
                                    <asp:TextBox ID="txtmd_approve_status_GvListData" runat="server"
                                        Visible="false" Text='<%# Eval("md_approve_status") %>' />

                                    <asp:LinkButton ID="btnDetail"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail"
                                        CommandArgument='<%# Eval("u0_training_plan_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>

                                    <asp:LinkButton CssClass="btn btn-warning btn-sm" runat="server"
                                        ID="btnUpdate_GvListData"
                                        CommandName="btnUpdate_GvListData" OnCommand="btnCommand"
                                        data-original-title="แก้ไข" data-toggle="tooltip" Text="แก้ไข"
                                        CommandArgument='<%# Eval("u0_training_plan_idx") %>'>
                                    <i class="fa fa-pencil-alt"></i>
                                    </asp:LinkButton>

                                    <asp:LinkButton CssClass="btn btn-danger btn-sm" runat="server"
                                        data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                        ID="btnDelete_GvListData"
                                        CommandName="btnDelete" OnCommand="btnCommand"
                                        CommandArgument='<%# Eval("u0_training_plan_idx") %>'
                                        OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                    <i class="fa fa-trash"></i>
                                    </asp:LinkButton>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>


            </asp:Panel>
            <%-- Start Select --%>
        </asp:View>

        <asp:View ID="View_trainingPage" runat="server">

            <asp:FormView ID="fvCRUD" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <InsertItemTemplate>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; ข้อมูลแผนการฝึกอบรม</strong></h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal" role="form">
                                <asp:Panel ID="Panel5" runat="server" Visible="false">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสหลักสูตร :" />
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txttraining_plan_no" runat="server" CssClass="form-control" Enabled="false"
                                                        Text='<%# Eval("training_plan_no") %>' />
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label3" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่สร้างPlan :" />
                                            <div class="col-md-4">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txttraining_plan_date" runat="server"
                                                        CssClass="form-control filter-order-from"
                                                        Enabled="false"
                                                        Text='<%# Eval("training_plan_date") %>' />
                                                    <span class="input-group-addon show-order-sale-log-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <asp:Label ID="Label5" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="ปี : " />
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txttraining_plan_year" runat="server"
                                                        CssClass="form-control"
                                                        TextMode="Number"
                                                        MaxLength="4"
                                                        AutoPostBack="true"
                                                        OnTextChanged="onTextChanged" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                        ValidationGroup="btnSaveInsert" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txttraining_plan_year"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกปี" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อหลักสูตร :" />
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlu0_course_idx_ref" runat="server"
                                                    AutoPostBack="true"
                                                    OnSelectedIndexChanged="FvDetail_DataBound"
                                                    CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                    ValidationGroup="btnSaveInsert" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="ddlu0_course_idx_ref"
                                                    Font-Size="1em" ForeColor="Red"
                                                    CssClass="pull-left"
                                                    InitialValue="0"
                                                    ErrorMessage="กรุณากรอกชื่อหลักสูตร" />
                                            </div>

                                            <div class="col-md-6">
                                                <asp:Label ID="Label1" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="กลุ่มวิชา : " />
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txttraining_group_name" runat="server"
                                                        Enabled="false"
                                                        CssClass="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label10" class="col-md-2 control-labelnotop text_right" runat="server" Text="วิทยากร :" />
                                            <div class="col-md-4">
                                                <asp:RadioButtonList ID="rdllecturer_type" runat="server"
                                                    CssClass="radio-list-inline-emps" RepeatDirection="Horizontal"
                                                    AutoPostBack="true"
                                                    OnSelectedIndexChanged="FvDetail_DataBound">
                                                    <asp:ListItem Value="0" Text="ภายใน" Selected="True" />
                                                    <asp:ListItem Value="1" Text="ภายนอก" />
                                                </asp:RadioButtonList>
                                                <asp:DropDownList ID="ddlm0_institution_idx_ref" runat="server"
                                                    CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7"
                                                    ValidationGroup="btnSaveInsert" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="ddlm0_institution_idx_ref"
                                                    Font-Size="1em" ForeColor="Red"
                                                    CssClass="pull-left"
                                                    InitialValue="0"
                                                    ErrorMessage="กรุณากรอกวิทยากร" />
                                            </div>

                                            <div class="col-md-6">
                                                <asp:Label ID="Label6" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="สาขาวิชา :" />
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txttraining_branch_name" runat="server"
                                                        Enabled="false"
                                                        CssClass="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label16" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานที่ :" />
                                            <div class="col-md-4">
                                                <asp:CheckBox ID="cb_MTT" runat="server" Text="" Enabled="true" />
                                                <asp:Label ID="Label18" runat="server" Text="MTT"></asp:Label>
                                                &nbsp;
                                            <asp:CheckBox ID="cb_NPW" runat="server" Text="" Enabled="true" />
                                                <asp:Label ID="Label19" runat="server" Text="NPW"></asp:Label>
                                                &nbsp;
                                                    <asp:CheckBox ID="cb_RJN" runat="server" Text="" Enabled="true" />
                                                <asp:Label ID="Label7" runat="server" Text="RJN"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Label2" class="col-md-2 control-labelnotop text_right" runat="server" Text="กลุ่มเป้าหมาย :" />
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlm0_target_group_idx_ref" runat="server" CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                                    ValidationGroup="btnSaveInsert" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="ddlm0_target_group_idx_ref"
                                                    Font-Size="1em" ForeColor="Red"
                                                    CssClass="pull-left"
                                                    InitialValue="0"
                                                    ErrorMessage="กรุณากรอกกลุ่มเป้าหมาย" />
                                            </div>

                                        </div>
                                    </div>


                                    <div class="row">

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label13" class="col-md-2 control-labelnotop text_right" runat="server" Text="จำนวนคนต่อรุ่น :" />
                                                <div class="col-md-2">
                                                    <asp:TextBox ID="txttraining_plan_qty" runat="server"
                                                        CssClass="form-control" TextMode="Number"
                                                        Text='<%# Eval("training_plan_qty") %>'
                                                        AutoPostBack="true"
                                                        OnTextChanged="onTextChanged" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                        ValidationGroup="btnSaveInsert" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txttraining_plan_qty"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกจำนวนคนต่อรุ่น" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label22" class="col-md-2 control-labelnotop text_right" runat="server" Text="งบประมาณ :" />
                                                <div class="col-md-2">

                                                    <asp:TextBox ID="txttraining_plan_budget" runat="server"
                                                        CssClass="form-control"
                                                        TextMode="Number"
                                                        Text='<%# Eval("training_plan_budget") %>'
                                                        AutoPostBack="true"
                                                        OnTextChanged="onTextChanged" />

                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                                        ValidationGroup="btnSaveInsert" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txttraining_plan_amount"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกค่าใช้จ่ายต่อคน" />--%>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label21" class="col-md-2 control-labelnotop text_right" runat="server" Text="จำนวนรุ่น :" />
                                                <div class="col-md-2">
                                                    <asp:TextBox ID="txttraining_plan_model" runat="server" CssClass="form-control" TextMode="Number"
                                                        Text='<%# Eval("training_plan_model") %>'
                                                        AutoPostBack="true"
                                                        OnTextChanged="onTextChanged" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                        ValidationGroup="btnSaveInsert" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txttraining_plan_model"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกจำนวนรุ่น" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label11" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="แผนการอบรม :" />
                                                <div class="col-md-10">
                                                    <asp:GridView ID="GvMonthList" runat="server"
                                                        CssClass="table table-striped table-responsive table-bordered"
                                                        GridLines="None" OnRowCommand="onRowCommand"
                                                        AutoGenerateColumns="false">
                                                        <Columns>

                                                            <asp:TemplateField
                                                                HeaderText="ปี"
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtyear_edit" runat="server" Text='<%# Bind("training_plan_year") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtyear_item" runat="server" Text='<%# Bind("training_plan_year") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ม.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm1_edit" runat="server" Text='<%# Bind("training_plan_m1") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm1_item" runat="server" Text='<%# Bind("training_plan_m1") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ก.พ."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm2_edit" runat="server" Text='<%# Bind("training_plan_m2") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm2_item" runat="server" Text='<%# Bind("training_plan_m2") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="มี.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm3_edit" runat="server" Text='<%# Bind("training_plan_m3") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm3_item" runat="server" Text='<%# Bind("training_plan_m3") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="เม.ษ."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm4_edit" runat="server" Text='<%# Bind("training_plan_m4") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm4_item" runat="server" Text='<%# Bind("training_plan_m4") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="พ.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm5_edit" runat="server" Text='<%# Bind("training_plan_m5") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm5_item" runat="server" Text='<%# Bind("training_plan_m5") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="มิ.ย."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm6_edit" runat="server" Text='<%# Bind("training_plan_m6") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm6_item" runat="server" Text='<%# Bind("training_plan_m6") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ก.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm7_edit" runat="server" Text='<%# Bind("training_plan_m7") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm7_item" runat="server" Text='<%# Bind("training_plan_m7") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ส.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm8_edit" runat="server" Text='<%# Bind("training_plan_m8") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm8_item" runat="server" Text='<%# Bind("training_plan_m8") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ก.ย."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm9_edit" runat="server" Text='<%# Bind("training_plan_m9") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm9_item" runat="server" Text='<%# Bind("training_plan_m9") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ต.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm10_edit" runat="server" Text='<%# Bind("training_plan_m10") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm10_item" runat="server" Text='<%# Bind("training_plan_m10") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%--    <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="พ.ย."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm11_edit" runat="server" Text='<%# Bind("training_plan_m11") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm11_item" runat="server" Text='<%# Bind("training_plan_m11") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ธ.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm12_edit" runat="server" Text='<%# Bind("training_plan_m12") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm12_item" runat="server" Text='<%# Bind("training_plan_m12") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <div class="word-wrap">
                                                                        <asp:LinkButton ID="btnAdd_GvMonthList"
                                                                            CssClass="btn-primary btn-sm" runat="server"
                                                                            data-original-title="เพิ่ม" data-toggle="tooltip" Text="เพิ่ม"
                                                                            OnCommand="btnCommand" CommandName="btnAdd_GvMonthList">
                                                                           <i class="fa fa-plus"></i></asp:LinkButton>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <%-- list plan year --%>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-10">

                                                    <div class="panel-info">
                                                        <div class="panel-heading f-bold">รายละเอียดแผนการอบรม</div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-10">
                                                    <asp:GridView ID="GvMonthList_L" runat="server"
                                                        CssClass="table table-striped table-responsive table-bordered"
                                                        GridLines="None" OnRowCommand="onRowCommand"
                                                        AutoGenerateColumns="false">
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField
                                                                HeaderText="ปี"
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtyear_edit_L" runat="server" Text='<%# Bind("training_plan_year") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtyear_item_L" runat="server" Text='<%# Bind("training_plan_year") %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ม.ค."
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm1_edit_L" runat="server" Text='<%# Bind("training_plan_m1") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm1_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m1")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ก.พ."
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm2_edit_L" runat="server" Text='<%# Bind("training_plan_m2") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm2_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m2")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="มี.ค."
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm3_edit_L" runat="server" Text='<%# Bind("training_plan_m3") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm3_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m3")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="เม.ษ."
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm4_edit_L" runat="server" Text='<%# Bind("training_plan_m4") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm4_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m4")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="พ.ค."
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm5_edit_L" runat="server" Text='<%# Bind("training_plan_m5") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm5_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m5")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="มิ.ย."
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm6_edit_L" runat="server" Text='<%# Bind("training_plan_m6") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm6_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m6")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ก.ค."
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm7_edit_L" runat="server" Text='<%# Bind("training_plan_m7") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm7_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m7")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ส.ค."
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm8_edit_L" runat="server" Text='<%# Bind("training_plan_m8") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm8_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m8")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ก.ย."
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm9_edit_L" runat="server" Text='<%# Bind("training_plan_m9") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm9_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m9")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ต.ค."
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm10_edit_L" runat="server" Text='<%# Bind("training_plan_m10") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm10_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m10")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%--    <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="พ.ย."
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm11_edit_L" runat="server" Text='<%# Bind("training_plan_m11") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm11_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m11")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ธ.ค."
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm12_edit_L" runat="server" Text='<%# Bind("training_plan_m12") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm12_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m12")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <div class="word-wrap">
                                                                        <asp:LinkButton ID="btnDel_GvMonthList_L"
                                                                            CssClass="btn-danger btn-sm" runat="server"
                                                                            data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                                                            OnCommand="btnCommand" CommandName="btnDel_GvMonthList_L">
                                                                           <i class="fa fa-close"></i></asp:LinkButton>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label23" class="col-md-2 control-labelnotop text_right" runat="server" Text="ค่าใช้จ่ายต่อคน :" />
                                                <div class="col-md-2">

                                                    <asp:TextBox ID="txttraining_plan_amount" runat="server" CssClass="form-control"
                                                        Enabled="false"
                                                        Text='<%# Eval("training_plan_amount") %>' />

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <asp:Panel ID="Panel2" runat="server" Visible="false">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">

                                                    <asp:Label ID="Label24" class="col-md-2 control-labelnotop text_right" runat="server" Text="CostPer Head :" />
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txttraining_plan_costperhead" runat="server"
                                                            Enabled="false"
                                                            CssClass="form-control" TextMode="Number"
                                                            Text='<%# Eval("training_plan_costperhead") %>' />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="Panel4" runat="server" Visible="true">

                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label17" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานะการใช้งาน :" />
                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="ddltraining_plan_status" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="1" Text="ใช้งาน" />
                                                            <asp:ListItem Value="0" Text="ไม่ใช้งาน" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>



                                    <%-- End  --%>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>


            </asp:FormView>

            <%-- Edit  --%>
            <asp:FormView ID="fv_Update" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <EditItemTemplate>


                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; ข้อมูลแผนการฝึกอบรม</strong></h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal" role="form">
                                <asp:Panel ID="Panel5" runat="server" Visible="true">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสหลักสูตร :" />
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txttraining_plan_no" runat="server" CssClass="form-control" Enabled="false"
                                                        Text='<%# Eval("training_plan_no") %>' />
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label3" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่สร้างPlan :" />
                                            <div class="col-md-4">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txttraining_plan_date" runat="server"
                                                        CssClass="form-control filter-order-from"
                                                        Enabled="false"
                                                        Text='<%# Eval("training_plan_date") %>' />
                                                    <span class="input-group-addon show-order-sale-log-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <asp:Label ID="Label5" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="ปี : " />
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txttraining_plan_year" runat="server"
                                                        CssClass="form-control"
                                                        Text='<%# Eval("training_plan_year") %>'
                                                        TextMode="Number"
                                                        MaxLength="4"
                                                        Enabled="false" />
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                        ValidationGroup="btnSaveUpdate" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txttraining_plan_year"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกปี" />--%>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อหลักสูตร :" />
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlu0_course_idx_ref" runat="server"
                                                    AutoPostBack="true" Enabled="false"
                                                    OnSelectedIndexChanged="FvDetail_DataBound"
                                                    CssClass="form-control" />
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                    ValidationGroup="btnSaveUpdate" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="ddlu0_course_idx_ref"
                                                    Font-Size="1em" ForeColor="Red"
                                                    CssClass="pull-left"
                                                    InitialValue="0"
                                                    ErrorMessage="กรุณากรอกชื่อหลักสูตร" />--%>
                                            </div>

                                            <div class="col-md-6">
                                                <asp:Label ID="Label1" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="กลุ่มวิชา : " />
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txttraining_group_name" runat="server"
                                                        Enabled="false"
                                                        Text='<%# Eval("training_group_name") %>'
                                                        CssClass="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label10" class="col-md-2 control-labelnotop text_right" runat="server" Text="วิทยากร :" />
                                            <div class="col-md-4">
                                                <asp:RadioButtonList ID="rdllecturer_type" runat="server"
                                                    CssClass="radio-list-inline-emps" RepeatDirection="Horizontal"
                                                    AutoPostBack="true"
                                                    SelectedValue='<%# Eval("lecturer_type") %>'
                                                    OnSelectedIndexChanged="FvDetail_DataBound">
                                                    <asp:ListItem Value="0" Text="ภายใน" Selected="True" />
                                                    <asp:ListItem Value="1" Text="ภายนอก" />
                                                </asp:RadioButtonList>
                                                <asp:DropDownList ID="ddlm0_institution_idx_ref" runat="server"
                                                    CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7"
                                                    ValidationGroup="btnSaveUpdate" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="ddlm0_institution_idx_ref"
                                                    Font-Size="1em" ForeColor="Red"
                                                    CssClass="pull-left"
                                                    InitialValue="0"
                                                    ErrorMessage="กรุณากรอกวิทยากร" />
                                            </div>

                                            <div class="col-md-6">
                                                <asp:Label ID="Label6" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="สาขาวิชา :" />
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txttraining_branch_name" runat="server"
                                                        Enabled="false"
                                                        Text='<%# Eval("training_branch_name") %>'
                                                        CssClass="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label16" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานที่ :" />
                                            <div class="col-md-4">
                                                <asp:CheckBox ID="cb_MTT" runat="server" Text="" Enabled="false" />
                                                <asp:Label ID="Label18" runat="server" Text="MTT"></asp:Label>
                                                &nbsp;
                                            <asp:CheckBox ID="cb_NPW" runat="server" Text="" Enabled="false" />
                                                <asp:Label ID="Label19" runat="server" Text="NPW"></asp:Label>
                                                &nbsp;
                                                    <asp:CheckBox ID="cb_RJN" runat="server" Text="" Enabled="false" />
                                                <asp:Label ID="Label7" runat="server" Text="RJN"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Label2" class="col-md-2 control-labelnotop text_right" runat="server" Text="กลุ่มเป้าหมาย :" />
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlm0_target_group_idx_ref" runat="server" CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                                    ValidationGroup="btnSaveUpdate" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="ddlm0_target_group_idx_ref"
                                                    Font-Size="1em" ForeColor="Red"
                                                    CssClass="pull-left"
                                                    InitialValue="0"
                                                    ErrorMessage="กรุณากรอกกลุ่มเป้าหมาย" />
                                            </div>

                                        </div>
                                    </div>


                                    <div class="row">

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label13" class="col-md-2 control-labelnotop text_right" runat="server" Text="จำนวนคนต่อรุ่น :" />
                                                <div class="col-md-2">
                                                    <asp:TextBox ID="txttraining_plan_qty" runat="server"
                                                        CssClass="form-control" TextMode="Number"
                                                        Text='<%# Eval("training_plan_qty") %>'
                                                        AutoPostBack="true"
                                                        OnTextChanged="onTextChanged" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                        ValidationGroup="btnSaveUpdate" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txttraining_plan_qty"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกจำนวนคนต่อรุ่น" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label22" class="col-md-2 control-labelnotop text_right" runat="server" Text="งบประมาณ :" />
                                                <div class="col-md-2">

                                                    <asp:TextBox ID="txttraining_plan_budget" runat="server"
                                                        CssClass="form-control"
                                                        TextMode="Number"
                                                        AutoPostBack="true"
                                                        OnTextChanged="onTextChanged"
                                                        Text='<%# Eval("training_plan_budget") %>' />

                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                                        ValidationGroup="btnSaveUpdate" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txttraining_plan_amount"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกค่าใช้จ่ายต่อคน" />--%>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label21" class="col-md-2 control-labelnotop text_right" runat="server" Text="จำนวนรุ่น :" />
                                                <div class="col-md-2">
                                                    <asp:TextBox ID="txttraining_plan_model" runat="server" CssClass="form-control" TextMode="Number"
                                                        Text='<%# Eval("training_plan_model") %>'
                                                        AutoPostBack="true"
                                                        OnTextChanged="onTextChanged" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                        ValidationGroup="btnSaveUpdate" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txttraining_plan_model"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกจำนวนรุ่น" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <%-- start --%>


                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label11" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="แผนการอบรม :" />
                                                <div class="col-md-10">
                                                    <asp:GridView ID="GvMonthList" runat="server"
                                                        CssClass="table table-striped table-responsive table-bordered"
                                                        GridLines="None" OnRowCommand="onRowCommand"
                                                        AutoGenerateColumns="false">
                                                        <Columns>

                                                            <asp:TemplateField
                                                                HeaderText="ปี"
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtyear_edit" runat="server" Text='<%# Bind("training_plan_year") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtyear_item" runat="server" Text='<%# Bind("training_plan_year") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ม.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm1_edit" runat="server" Text='<%# Bind("training_plan_m1") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm1_item" runat="server" Text='<%# Bind("training_plan_m1") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ก.พ."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm2_edit" runat="server" Text='<%# Bind("training_plan_m2") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm2_item" runat="server" Text='<%# Bind("training_plan_m2") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="มี.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm3_edit" runat="server" Text='<%# Bind("training_plan_m3") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm3_item" runat="server" Text='<%# Bind("training_plan_m3") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="เม.ษ."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm4_edit" runat="server" Text='<%# Bind("training_plan_m4") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm4_item" runat="server" Text='<%# Bind("training_plan_m4") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="พ.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm5_edit" runat="server" Text='<%# Bind("training_plan_m5") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm5_item" runat="server" Text='<%# Bind("training_plan_m5") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="มิ.ย."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm6_edit" runat="server" Text='<%# Bind("training_plan_m6") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm6_item" runat="server" Text='<%# Bind("training_plan_m6") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ก.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm7_edit" runat="server" Text='<%# Bind("training_plan_m7") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm7_item" runat="server" Text='<%# Bind("training_plan_m7") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ส.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm8_edit" runat="server" Text='<%# Bind("training_plan_m8") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm8_item" runat="server" Text='<%# Bind("training_plan_m8") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ก.ย."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm9_edit" runat="server" Text='<%# Bind("training_plan_m9") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm9_item" runat="server" Text='<%# Bind("training_plan_m9") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ต.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm10_edit" runat="server" Text='<%# Bind("training_plan_m10") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm10_item" runat="server" Text='<%# Bind("training_plan_m10") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%--    <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="พ.ย."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm11_edit" runat="server" Text='<%# Bind("training_plan_m11") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm11_item" runat="server" Text='<%# Bind("training_plan_m11") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ธ.ค."
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm12_edit" runat="server" Text='<%# Bind("training_plan_m12") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtm12_item" runat="server" Text='<%# Bind("training_plan_m12") %>'
                                                                        TextMode="Number"
                                                                        Width="100%"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <div class="word-wrap">
                                                                        <asp:LinkButton ID="btnAdd_GvMonthList"
                                                                            CssClass="btn-primary btn-sm" runat="server"
                                                                            data-original-title="เพิ่ม" data-toggle="tooltip" Text="เพิ่ม"
                                                                            OnCommand="btnCommand" CommandName="btnAdd_GvMonthList">
                                                                           <i class="fa fa-plus"></i></asp:LinkButton>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>
                                                    </asp:GridView>

                                                    <div class="row">

                                                        <div class="panel-info">
                                                            <div class="panel-heading f-bold">รายละเอียดแผนการอบรม</div>
                                                        </div>

                                                    </div>

                                                    <%-- list plan year --%>

                                                    <div class="row">

                                                        <asp:GridView ID="GvMonthList_L" runat="server"
                                                            CssClass="table table-striped table-responsive table-bordered"
                                                            GridLines="None" OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">ไม่พบข้อมูล</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField
                                                                    HeaderText="ปี"
                                                                    ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtyear_edit_L" runat="server" Text='<%# Bind("training_plan_year") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtyear_item_L" runat="server" Text='<%# Bind("training_plan_year") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%-- <ItemStyle Width="10%" />--%>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ม.ค."
                                                                    ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtm1_edit_L" runat="server" Text='<%# Bind("training_plan_m1") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtm1_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m1")) %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%-- <ItemStyle Width="10%" />--%>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ก.พ."
                                                                    ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtm2_edit_L" runat="server" Text='<%# Bind("training_plan_m2") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtm2_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m2")) %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%--<ItemStyle Width="10%" />--%>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="มี.ค."
                                                                    ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtm3_edit_L" runat="server" Text='<%# Bind("training_plan_m3") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtm3_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m3")) %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%--<ItemStyle Width="10%" />--%>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="เม.ษ."
                                                                    ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtm4_edit_L" runat="server" Text='<%# Bind("training_plan_m4") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtm4_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m4")) %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%--<ItemStyle Width="10%" />--%>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="พ.ค."
                                                                    ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtm5_edit_L" runat="server" Text='<%# Bind("training_plan_m5") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtm5_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m5")) %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%-- <ItemStyle Width="10%" />--%>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="มิ.ย."
                                                                    ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtm6_edit_L" runat="server" Text='<%# Bind("training_plan_m6") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtm6_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m6")) %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%-- <ItemStyle Width="10%" />--%>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ก.ค."
                                                                    ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtm7_edit_L" runat="server" Text='<%# Bind("training_plan_m7") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtm7_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m7")) %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%-- <ItemStyle Width="10%" />--%>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ส.ค."
                                                                    ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtm8_edit_L" runat="server" Text='<%# Bind("training_plan_m8") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtm8_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m8")) %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%-- <ItemStyle Width="10%" />--%>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ก.ย."
                                                                    ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtm9_edit_L" runat="server" Text='<%# Bind("training_plan_m9") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtm9_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m9")) %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%-- <ItemStyle Width="10%" />--%>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ต.ค."
                                                                    ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtm10_edit_L" runat="server" Text='<%# Bind("training_plan_m10") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtm10_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m10")) %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%--    <ItemStyle Width="10%" />--%>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="พ.ย."
                                                                    ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtm11_edit_L" runat="server" Text='<%# Bind("training_plan_m11") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtm11_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m11")) %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%--<ItemStyle Width="10%" />--%>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ธ.ค."
                                                                    ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtm12_edit_L" runat="server" Text='<%# Bind("training_plan_m12") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtm12_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m12")) %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%--<ItemStyle Width="10%" />--%>
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:LinkButton ID="btnDel_GvMonthList_L"
                                                                                CssClass="btn-danger btn-sm" runat="server"
                                                                                data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                                                                OnCommand="btnCommand" CommandName="btnDel_GvMonthList_L">
                                                                           <i class="fa fa-close"></i></asp:LinkButton>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                            </Columns>
                                                        </asp:GridView>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <%-- end --%>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label23" class="col-md-2 control-labelnotop text_right" runat="server" Text="ค่าใช้จ่ายต่อคน :" />
                                                <div class="col-md-2">

                                                    <asp:TextBox ID="txttraining_plan_amount" runat="server" CssClass="form-control"
                                                        Text='<%# Eval("training_plan_amount") %>'
                                                        Enabled="false" />

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <asp:Panel ID="Panel2" runat="server" Visible="false">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">

                                                    <asp:Label ID="Label24" class="col-md-2 control-labelnotop text_right" runat="server" Text="CostPer Head :" />
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txttraining_plan_costperhead" runat="server"
                                                            Enabled="false"
                                                            CssClass="form-control" TextMode="Number"
                                                            Text='<%# Eval("training_plan_costperhead") %>' />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="Panel4" runat="server" Visible="true">

                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label17" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานะการใช้งาน :" />
                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="ddltraining_plan_status"
                                                            SelectedValue='<%# Eval("training_plan_status") %>'
                                                            runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="1" Text="ใช้งาน" />
                                                            <asp:ListItem Value="0" Text="ไม่ใช้งาน" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="pnlhistory" runat="server" Visible="false">

                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label97" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                                    <div class="col-md-9">


                                                        <div class="panel-info">
                                                            <div class="panel-heading f-bold">รายละเอียดการบันทึกข้อมูล</div>
                                                        </div>
                                                        <asp:GridView ID="GvHistory" runat="server"
                                                            CssClass="table table-bordered word-wrap"
                                                            GridLines="None" OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">ไม่พบข้อมูล</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField
                                                                    HeaderText="วันที่ / เวลา"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtzdate_edit_L" runat="server" Text='<%# Bind("zdate") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtzdate_item_L" runat="server" Text='<%# Eval("zdate") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ผู้ดำเนินการ"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtFullNameTH_edit_L" runat="server" Text='<%# Bind("FullNameTH") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtFullNameTH_item_L" runat="server" Text='<%# Eval("FullNameTH") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ดำเนินการ"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtnode_name_edit_L" runat="server" Text='<%# Bind("node_name") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtnode_name_item_L" runat="server" Text='<%# Eval("node_name") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ผลการดำเนินการ"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtdecision_name_edit_L" runat="server" Text='<%# Bind("decision_name") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtdecision_name_item_L" runat="server" Text='<%# Eval("decision_name") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="สาเหตุ / หมายเหตุ"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small"
                                                                    HeaderStyle-Width="25%">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtapprove_remark_edit_L" runat="server" Text='<%# Bind("approve_remark") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtapprove_remark_item_L" runat="server" Text='<%# Eval("approve_remark") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </asp:Panel>

                                    <%-- End  --%>
                                </div>
                            </div>
                        </div>
                </EditItemTemplate>


            </asp:FormView>



            <%-- End Select --%>

            <asp:Panel ID="pnlSave" runat="server">

                <div class="row">

                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <asp:LinkButton ID="btnSaveUpdate" CssClass="btn btn-success" runat="server"
                                    CommandName="btnSaveUpdate" OnCommand="btnCommand"
                                    Text="<i class='fa fa-save fa-lg'></i> บันทึกการเปลี่ยนแปลง"
                                    data-toggle="tooltip" title="บันทึกการเปลี่ยนแปลง"
                                    ValidationGroup="btnSaveUpdate" />
                                <asp:LinkButton CssClass="btn btn-success" runat="server"
                                    CommandName="btnSaveInsert" OnCommand="btnCommand"
                                    Text="<i class='fa fa-save fa-lg'></i> บันทึก"
                                    data-toggle="tooltip" title="บันทึก"
                                    ID="btnSaveInsert"
                                    ValidationGroup="btnSaveInsert" />
                                <asp:LinkButton CssClass="btn btn-warning"
                                    ID="btnClear" Visible="false"
                                    OnCommand="btnCommand" CommandName="btnInsert"
                                    data-toggle="tooltip" title="ยกเลิก" runat="server"
                                    Text="<i class='fa fa-close fa-lg'></i> ยกเลิก" />
                                <asp:LinkButton CssClass="btn btn-danger"
                                    data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                    Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                    ID="btnCancel"
                                    CommandName="btnCancel" OnCommand="btnCommand" />
                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>
            <br />


        </asp:View>


        <asp:View ID="View_HR_WList" runat="server">

            <asp:Panel ID="Panel1" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_HR_W" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_HR_W" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_HR_W" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_HR_W" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvHR_WList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_training_plan_idx"
                    ShowFooter="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# (Container.DataItemIndex + 1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_plan_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_plan_date") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ปี" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_plan_year") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("course_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_group_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_branch_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <span>
                                   <%-- <%# getTextDoc((int)Eval("approve_status") , 
                                                       (int)Eval("md_approve_status") , 
                                                       (string)Eval("decision_name"),
                                                       (string)Eval("node_name"),
                                                       (string)Eval("actor_name"),
                                                       (string)Eval("md_decision_name"),
                                                       (string)Eval("md_node_name"),
                                                       (string)Eval("md_actor_name")
                                                       ) %>--%>
                                    <%# getTextDoc((string)Eval("zprocess_name") , 
                                                       (int)Eval("el_approve_status") 
                                                       ) %>
                                    
                                </span>
                               
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <asp:LinkButton ID="btnDetail_GvHR_WList"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvHR_WList"
                                        CommandArgument='<%# Eval("u0_training_plan_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>



        <asp:View ID="View_HR_AList" runat="server">

            <asp:Panel ID="Panel8" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_HR_A" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_HR_A" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_HR_A" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_HR_A" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvHR_AList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_training_plan_idx"
                    ShowFooter="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# (Container.DataItemIndex + 1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_plan_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_plan_date") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ปี" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_plan_year") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("course_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_group_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_branch_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>

                                <span>
                                   <%-- <%# getTextDoc((int)Eval("approve_status") , 
                                                       (int)Eval("md_approve_status") , 
                                                       (string)Eval("decision_name"),
                                                       (string)Eval("node_name"),
                                                       (string)Eval("actor_name"),
                                                       (string)Eval("md_decision_name"),
                                                       (string)Eval("md_node_name"),
                                                       (string)Eval("md_actor_name")
                                                       ) %>--%>
                                    <%# getTextDoc((string)Eval("zprocess_name") , 
                                                       (int)Eval("el_approve_status") 
                                                       ) %>
                                </span>

                               
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <asp:LinkButton ID="btnDetail_GvHR_AList"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvHR_AList"
                                        CommandArgument='<%# Eval("u0_training_plan_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>


        <%-- Start MD --%>


        <asp:View ID="View_MD_WList" runat="server">

            <asp:Panel ID="Panel9" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_MD_W" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_MD_W" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_MD_W" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_MD_W" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvMD_WList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_training_plan_idx"
                    ShowFooter="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# (Container.DataItemIndex + 1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_plan_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_plan_date") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ปี" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_plan_year") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("course_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_group_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_branch_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>

                                <span>
                                   <%-- <%# getTextDoc((int)Eval("approve_status") , 
                                                       (int)Eval("md_approve_status") , 
                                                       (string)Eval("decision_name"),
                                                       (string)Eval("node_name"),
                                                       (string)Eval("actor_name"),
                                                       (string)Eval("md_decision_name"),
                                                       (string)Eval("md_node_name"),
                                                       (string)Eval("md_actor_name")
                                                       ) %>--%>
                                    <%# getTextDoc((string)Eval("zprocess_name") , 
                                                       (int)Eval("el_approve_status") 
                                                       ) %>
                                </span>

                                
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <asp:LinkButton ID="btnDetail_GvMD_WList"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvMD_WList"
                                        CommandArgument='<%# Eval("u0_training_plan_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>


        <asp:View ID="View_MD_AList" runat="server">

            <asp:Panel ID="Panel10" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_MD_A" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_MD_A" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_MD_A" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_MD_A" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvMD_AList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_training_plan_idx"
                    ShowFooter="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# (Container.DataItemIndex + 1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_plan_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_plan_date") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ปี" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_plan_year") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("course_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_group_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_branch_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>

                                <span>
                                    <%--<%# getTextDoc((int)Eval("approve_status") , 
                                                       (int)Eval("md_approve_status") , 
                                                       (string)Eval("decision_name"),
                                                       (string)Eval("node_name"),
                                                       (string)Eval("actor_name"),
                                                       (string)Eval("md_decision_name"),
                                                       (string)Eval("md_node_name"),
                                                       (string)Eval("md_actor_name")
                                                       ) %>--%>
                                    <%# getTextDoc((string)Eval("zprocess_name") , 
                                                       (int)Eval("el_approve_status") 
                                                       ) %>
                                </span>


                               
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <asp:LinkButton ID="btnDetail_GvMD_AList"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvMD_AList"
                                        CommandArgument='<%# Eval("u0_training_plan_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>

        <%-- End MD --%>


        <%--  OnDataBound="FvDetail_DataBound" --%>
        <asp:View ID="View_HR_WDetail" runat="server">
            <asp:FormView ID="fv_preview" runat="server" Width="100%">
                <EditItemTemplate>


                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; 
                                        <asp:Label ID="lbtitle_preview" runat="server" Text="ข้อมูลแผนการฝึกอบรม"></asp:Label>
                            </strong></h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal" role="form">
                                <asp:Panel ID="Panel5" runat="server" Visible="true">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label20" class="col-md-2 f-s-13 f-bold control-labelnotop text_right" runat="server" Text="รหัสหลักสูตร :" />
                                                <div class="col-md-10">
                                                    <asp:Label ID="txttraining_plan_no" runat="server" CssClass="f-s-13 control-label"
                                                        Text='<%# Eval("training_plan_no") %>' />
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>


                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label3" CssClass="col-md-2 f-s-13 f-bold control-labelnotop text_right" runat="server" Text="วันที่สร้างเอกสาร :" />
                                            <div class="col-md-10">
                                                <asp:Label ID="txttraining_plan_date" runat="server"
                                                    CssClass="f-s-13 control-label"
                                                    Text='<%# Eval("training_plan_date") %>' />
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label4" CssClass="col-md-2 f-s-13 f-bold control-labelnotop text_right"
                                                runat="server" Text="ปี :" />
                                            <div class="col-md-10">
                                                <asp:Label ID="txttraining_plan_year" runat="server"
                                                    CssClass="f-s-13 control-label"
                                                    Text='<%# Eval("training_plan_year") %>' />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label12" class="col-md-2 f-s-13 f-bold control-labelnotop text_right" runat="server" Text="ชื่อหลักสูตร :" />
                                            <div class="col-md-10">
                                                <asp:Label ID="ddlu0_course_idx_ref" runat="server"
                                                    Text='<%# Eval("course_name") %>'
                                                    CssClass="f-s-13 control-label" />
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label8" CssClass="col-md-2 f-s-13 f-bold control-labelnotop text_right"
                                                runat="server" Text="กลุ่มวิชา :" />
                                            <div class="col-md-10">
                                                <asp:Label ID="txttraining_group_name" runat="server"
                                                    CssClass="f-s-13 control-label"
                                                    Text='<%# Eval("training_group_name") %>' />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label9" CssClass="col-md-2 f-s-13 f-bold control-labelnotop text_right"
                                                runat="server" Text="สาขาวิชา :" />
                                            <div class="col-md-10">
                                                <asp:Label ID="txttraining_branch_name" runat="server"
                                                    CssClass="f-s-13 control-label"
                                                    Text='<%# Eval("training_branch_name") %>' />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label10" class="col-md-2 f-s-13 f-bold control-labelnotop text_right" runat="server" Text="วิทยากร :" />
                                            <div class="col-md-10">

                                                <asp:Label ID="rdllecturer_type" runat="server"
                                                    CssClass="f-s-13 control-label"
                                                    Text='<%# getlecturer_type((int)Eval("lecturer_type") ,
                                                        "โดย",
                                                        (string)Eval("institution_name")) %>' />

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <asp:Panel ID="Panel7" runat="server" Visible="false">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label16" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานที่ :" />
                                                <div class="col-md-10">
                                                    <asp:CheckBox ID="cb_MTT" runat="server" Text="" Enabled="false" />
                                                    <asp:Label ID="Label18" runat="server" Text="MTT"></asp:Label>
                                                    &nbsp;
                                            <asp:CheckBox ID="cb_NPW" runat="server" Text="" Enabled="false" />
                                                    <asp:Label ID="Label19" runat="server" Text="NPW"></asp:Label>
                                                    &nbsp;
                                                    <asp:CheckBox ID="cb_RJN" runat="server" Text="" Enabled="false" />
                                                    <asp:Label ID="Label7" runat="server" Text="RJN"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Label2" class="col-md-2 f-s-13 f-bold control-labelnotop text_right" runat="server" Text="กลุ่มเป้าหมาย :" />
                                            <div class="col-md-10">
                                                <asp:Label ID="ddlm0_target_group_idx_ref"
                                                    runat="server" CssClass="f-s-13 control-label"
                                                    Text='<%# Eval("target_group_name") %>' />

                                            </div>

                                        </div>
                                    </div>


                                    <div class="row">

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label13" class="col-md-2 f-s-13 f-bold control-labelnotop text_right" runat="server" Text="จำนวนคนต่อรุ่น :" />
                                                <div class="col-md-10">
                                                    <asp:Label ID="txttraining_plan_qty" runat="server"
                                                        CssClass="f-s-13 control-label"
                                                        Text='<%#  string.Format("{0:n0}", Eval("training_plan_qty")) %>' />

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label23" class="col-md-2 f-s-13 f-bold control-labelnotop text_right" runat="server" Text="งบประมาณ :" />
                                                <div class="col-md-10">
                                                    <asp:Label ID="txttraining_plan_budget" runat="server"
                                                        CssClass="f-s-13 control-label"
                                                        Text='<%# string.Format("{0:n2}",  Eval("training_plan_budget")) %>' />
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label21" class="col-md-2 f-s-13 f-bold control-labelnotop text_right" runat="server" Text="จำนวนรุ่น :" />
                                                <div class="col-md-10">
                                                    <asp:Label ID="txttraining_plan_model"
                                                        runat="server"
                                                        CssClass="f-s-13 control-label"
                                                        Text='<%# string.Format("{0:n0}", Eval("training_plan_model")) %>' />
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label11" CssClass="col-md-2 f-s-13 f-bold control-labelnotop text_right" runat="server" Text="แผนการอบรม :" />
                                                <div class="col-md-10">

                                                    <asp:GridView ID="GvMonthList_A" runat="server"
                                                        CssClass="table table-striped f-s-12 table-empshift-responsive"
                                                        GridLines="None" OnRowCommand="onRowCommand"
                                                        AutoGenerateColumns="false">
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField
                                                                HeaderText="ปี"
                                                                ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtyear_edit_L" runat="server" Text='<%# Bind("training_plan_year") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtyear_item_L" runat="server" Text='<%# Bind("training_plan_year") %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ม.ค."
                                                                ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm1_edit_L" runat="server" Text='<%# Bind("training_plan_m1") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm1_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m1")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ก.พ."
                                                                ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm2_edit_L" runat="server" Text='<%# Bind("training_plan_m2") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm2_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m2")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="มี.ค."
                                                                ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm3_edit_L" runat="server" Text='<%# Bind("training_plan_m3") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm3_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m3")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="เม.ษ."
                                                                ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm4_edit_L" runat="server" Text='<%# Bind("training_plan_m4") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm4_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m4")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="พ.ค."
                                                                ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm5_edit_L" runat="server" Text='<%# Bind("training_plan_m5") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm5_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m5")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="มิ.ย."
                                                                ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm6_edit_L" runat="server" Text='<%# Bind("training_plan_m6") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm6_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m6")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ก.ค."
                                                                ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm7_edit_L" runat="server" Text='<%# Bind("training_plan_m7") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm7_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m7")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ส.ค."
                                                                ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm8_edit_L" runat="server" Text='<%# Bind("training_plan_m8") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm8_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m8")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ก.ย."
                                                                ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm9_edit_L" runat="server" Text='<%# Bind("training_plan_m9") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm9_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m9")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%-- <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ต.ค."
                                                                ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm10_edit_L" runat="server" Text='<%# Bind("training_plan_m10") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm10_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m10")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%--    <ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="พ.ย."
                                                                ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm11_edit_L" runat="server" Text='<%# Bind("training_plan_m11") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm11_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m11")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ธ.ค."
                                                                ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtm12_edit_L" runat="server" Text='<%# Bind("training_plan_m12") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtm12_item_L" runat="server" Text='<%# getStrformate((string)Eval("training_plan_m12")) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                                <%--<ItemStyle Width="10%" />--%>
                                                            </asp:TemplateField>


                                                        </Columns>
                                                    </asp:GridView>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label22" class="col-md-2 f-s-13 f-bold control-labelnotop text_right" runat="server" Text="ค่าใช้จ่ายต่อคน :" />
                                                <div class="col-md-2">
                                                    <asp:Label ID="txttraining_plan_amount" runat="server"
                                                        CssClass="f-s-13 control-label"
                                                        Text='<%# string.Format("{0:n2}", Eval("training_plan_amount")) %>' />
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label36" class="col-md-2 f-s-13 f-bold control-labelnotop text_right" runat="server" Text="สถานะของเอกสาร :" />
                                                <div class="col-md-10">


                                                    <asp:Label ID="Label37"
                                                        runat="server"
                                                        CssClass="f-s-13 control-label"
                                                        Text='<%# getTextDoc((string)Eval("zprocess_name") , 
                                                       (int)Eval("el_approve_status") 
                                                       ) %>' />
<%--<%# getTextDoc((int)Eval("approve_status") , 
                                                       (int)Eval("md_approve_status") , 
                                                       (string)Eval("decision_name"),
                                                       (string)Eval("node_name"),
                                                       (string)Eval("actor_name"),
                                                       (string)Eval("md_decision_name"),
                                                       (string)Eval("md_node_name"),
                                                       (string)Eval("md_actor_name")
                                                       )
                                                        %>--%>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <%-- End  --%>
                                </div>
                            </div>
                        </div>
                    </div>

                    <asp:Panel ID="pnlDetailApp" runat="server">

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h2 class="panel-title">รายละเอียดการอนุมัติ
                                </h2>
                            </div>
                            <div class="panel-body">

                                <asp:Panel ID="pnlapprove_hr" runat="server">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ผลอนุมัติ" />
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatusapprove" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="ผลการอนุมัติ....." />
                                                        <asp:ListItem Value="4" Text="อนุมัติ" />
                                                        <asp:ListItem Value="5" Text="กลับไปแก้ไข" />
                                                        <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredddlStatusapprove"
                                                        ValidationGroup="btnupdateApprove_HR" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlStatusapprove"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกผลอนุมัติ" />
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label35" CssClass="col-sm-3 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม" />
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtdetailApprove" CssClass="form-control" runat="server"
                                                        placeholder="ความคิดเห็นเพิ่มเติม ....." MaxLength="150" TextMode="multiline" Rows="4"></asp:TextBox>
                                                </div>
                                            </div>

                                            <asp:Panel ID="pnlApprove_HR_W" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_HR" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_HR"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_HR"
                                                            CommandArgument='<%# Eval("u0_training_plan_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_HR"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnHR_Wait"
                                                            data-toggle="tooltip"
                                                            Visible="true"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="pnlApprove_HR_A" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_HR_A" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_HR_A"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_HR_A"
                                                            CommandArgument='<%# Eval("u0_training_plan_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_HR_A"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnHR_App"
                                                            data-toggle="tooltip"
                                                            Visible="true"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>


                                        </div>
                                    </div>

                                </asp:Panel>

                                <asp:Panel ID="pnlapprove_md" runat="server">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label38" CssClass="col-sm-3 control-label" runat="server" Text="ผลอนุมัติ" />
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatusapprove_md" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="ผลการอนุมัติ....." />
                                                        <asp:ListItem Value="4" Text="อนุมัติ" />
                                                        <asp:ListItem Value="5" Text="กลับไปแก้ไข" />
                                                        <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredddlStatusapprove_md"
                                                        ValidationGroup="btnupdateApprove_md" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlStatusapprove_md"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกผลอนุมัติ" />
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label39" CssClass="col-sm-3 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม" />
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtdetailApprove_md" CssClass="form-control" runat="server"
                                                        placeholder="ความคิดเห็นเพิ่มเติม ....." MaxLength="150" TextMode="multiline" Rows="4"></asp:TextBox>
                                                </div>
                                            </div>

                                            <asp:Panel ID="pnlApprove_MD_W" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_MD" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_md"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_MD"
                                                            CommandArgument='<%# Eval("u0_training_plan_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_MD"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnMD_Wait"
                                                            data-toggle="tooltip"
                                                            Visible="true"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="pnlApprove_MD_A" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_MD_A" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_md"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_MD_A"
                                                            CommandArgument='<%# Eval("u0_training_plan_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_MD_A"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnMD_App"
                                                            data-toggle="tooltip"
                                                            Visible="true"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </asp:Panel>

                            </div>
                        </div>
                    </asp:Panel>
                </EditItemTemplate>


            </asp:FormView>

            <div class="row">
                <div class="form-group">
                    <div class="col-md-12">

                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10">

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_HR_W"
                                CommandName="btnHR_Wait" OnCommand="btnCommand" />

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_HR"
                                CommandName="btnHR_App" OnCommand="btnCommand" />

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_MD_W"
                                CommandName="btnMD_Wait" OnCommand="btnCommand" />

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_MD"
                                CommandName="btnMD_App" OnCommand="btnCommand" />

                        </div>

                    </div>
                </div>
            </div>
            <br />

        </asp:View>

        <asp:View ID="View_SCHEDList" runat="server">

            <asp:Panel ID="Panel11" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_SCHED" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_SCHED" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>กลุ่มวิชา</label>
                                <asp:DropDownList ID="ddltrn_groupSearch_SCHED" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_SCHED" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_SCHED" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="col-md-11 col-md-offset-1">
                <asp:Panel ID="panel13" runat="server" CssClass="text-right m-b-10 hidden-print">
                    <%--<button
                        onclick="tableToExcel('Tableprint_sched', 'ตารางแผนการอบรม')"
                        class="btn btn-primary">
                        <i class='fa fa-file-excel'></i>
                        Export Excel
                    </button>--%>

                    <%--<button id="btnExport" onclick="fnExcelReport();"> EXPORT </button>--%>

                    <asp:LinkButton ID="btnprint_sched" CssClass="btn btn-primary" runat="server"
                        Text="<i class='fa fa-print'></i> พิมพ์" OnClientClick="return printDiv('print_sched');" />
                </asp:Panel>
            </div>
            <div id="print_sched" class="row">

                <table id="Tableprint_sched"
                    class="table table-bordered">
                    <thead>
                        <tr style="background-color: #e7e7e7; padding: 5px 0px;">

                            <% for (int iWeek = 1; iWeek <= 12; iWeek++)
                                {
                            %>
                            <th style="width: 80px;">
                                <center>
                                <%= MonthTH(iWeek) %>
                                    </center>
                            </th>
                            <% 
                                } %>
                        </tr>
                    </thead>
                    <tbody>

                        <%= getHtmlSched() %>
                    </tbody>

                </table>


            </div>

            <br />

        </asp:View>


        <asp:View ID="View_rpt_SCHEDList" runat="server">

            <asp:Panel ID="Panel12" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_rpt_SCHED" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_rpt_SCHED" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>กลุ่มวิชา</label>
                                <asp:DropDownList ID="ddltrn_groupSearch_rpt_SCHED" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_rpt_SCHED" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_rpt_SCHED" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="col-md-11 col-md-offset-1">
                <asp:Panel ID="panel2" runat="server" CssClass="text-right m-b-10 hidden-print">

                    <button
                        onclick="tableToExcel('Tableprint_plan', 'รายงานแผนการอบรม')"
                        class="btn btn-primary">
                        <i class='fa fa-file-excel'></i>
                        Export Excel
                    </button>
                    <asp:LinkButton ID="btnprint_plan" CssClass="btn btn-primary" runat="server"
                        Text="<i class='fa fa-print'></i> พิมพ์" OnClientClick="return printDiv('print_plan');" />
                </asp:Panel>
            </div>

            <div class="panel-body" style="height: 600px; overflow-y: scroll; overflow-x: scroll;">
                <div id="print_plan" class="row">

                    <%-- table table-bordered  --%>
                    <table id="Tableprint_plan"
                        class="table table-bordered word-wrap">
                        <thead style="background-color: #ccffcc; padding: 2px 3px;">
                            <tr>
                                <td rowspan="2" style="background-color: #ccffcc;">
                                    <center> หมวด </center>
                                </td>
                                <td rowspan="2" style="background-color: #ccffcc;">
                                    <center>หัวข้อหลักสูตร</center>
                                </td>
                                <td colspan="4" style="background-color: #ccffcc;">
                                    <center>กลุ่มเป้ามาย</center>
                                </td>
                                <td rowspan="2" style="background-color: #ccffcc;">
                                    <center>จำนวนคน</center>
                                </td>
                                <td rowspan="2" style="background-color: #ccffcc;">
                                    <center>จำนวน</center>
                                </td>
                                <td colspan="12" style="background-color: #ccffcc;">
                                    <center>แผนการอบรม</center>
                                </td>
                                <td rowspan="2" style="background-color: #ccffcc;">
                                    <center>Budget</center>
                                </td>
                                <td rowspan="2" style="background-color: #ccffcc;">
                                    <center>Cost Per Head</center>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>MTT</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>NPW</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>RJN</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>รายละเอียด</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>ม.ค.</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>ก.พ.</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>มี.ค.</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>เม.ย.</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>พ.ค.</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>มิ.ย.</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>ก.ค.</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>ส.ค.</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>ก.ย.</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>ต.ค.</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>พ.ย.</center>
                                </td>
                                <td style="background-color: #ccffcc; padding: 0px 0px;">
                                    <center>ธ.ค.</center>
                                </td>
                            </tr>
                        </thead>
                        <tbody>

                            <%= getHtml_rpt_Sched_H() %>
                        </tbody>

                    </table>

                </div>
            </div>

            <br />

        </asp:View>



    </asp:MultiView>


    <%-- end search needs --%>

    <%-- start modal --%>

    <div class="col-lg-12" runat="server" id="Div2">
        <div id="DvRemarkRePrint" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 50%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">ช่องแสดงเหตุผลในการ Reprint</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="row">
                                    <div class="form-group">
                                        <asp:Label ID="Label22" runat="server" Text="เหตุผล" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtRemarkReprint" TextMode="multiline" Rows="5" runat="server" CssClass="form-control embed-responsive-item" PlaceHoldaer="........" />

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10">
                                            <asp:Label ID="Label23" runat="server" Text="*** ถ้าไม่ใส่เหตุผลจะไม่สามารถ Reprint ได้ !"
                                                ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-10">

                                            <asp:LinkButton ID="Cancel" class="btn btn-default btn-sm" data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal" CommandName="Cancel">&nbsp;Close</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%-- end modal --%>

    <asp:HiddenField ID="Hddfld_status" runat="server" />
    <asp:HiddenField ID="Hddfld_training_plan_no" runat="server" />
    <asp:HiddenField ID="Hddfld_u0_training_plan_idx" runat="server" />

    <script type="text/javascript">
        //http://localhost/mas.taokaenoi.co.th/websystem/elearning/transection_el/el_TrnPlan.aspx
        function ShowCurrentTime() {
            $.ajax({
                type: "POST",
                url: '<%=this.Request.Url.OriginalString %>/GetCurrentTime',
                <%--data: '{name: "' + $("#<%=txtUserName.ClientID%>")[0].value + '" }',--%>
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        function OnSuccess(response) {
            alert(response.d);
        }
        <%--function ShowCurrentTime() {
            alert('<%=this.Request.Url.OriginalString %>');
        }--%>
</script>


    <script type="text/javascript">

        function CallJS() {
            PageMethods.CodebehindMethodName(onSucceed, onError);
            return false;
        }
        function onSucceed(value) {
            alert(value)
        }
        function onError(value) {
            alert(value)
        }
    </script>

    <script type="text/javascript">
        function ShowCurrentTime1(value) {

            //var connection = new ActiveXObject("ADODB.Connection");

            //var connectionstring = "Data Source=<server>;Initial Catalog=<catalog>;User ID=<user>;Password=<password>;Provider=SQLOLEDB";

            //connection.Open(connectionstring);
            //var rs = new ActiveXObject("ADODB.Recordset");

            //rs.Open("SELECT * FROM table", connection);
            //rs.MoveFirst
            //while (!rs.eof) {
            //    document.write(rs.fields(1));
            //    rs.movenext;
            //}

            //rs.close;
            //connection.close;   url: '<%=this.Request.Url.OriginalString %>/GetCurrentTime',

           // alert('<%=ResolveUrl("el_TrnPlan.aspx") %>');
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("el_TrnPlan.aspx") %>/GetCurrentTime',
                data: '{name: "' + value + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                    //$("#success_alert").hide();

                }
            });
        }
        function OnSuccess(response) {
            alert(response.d);
            //$('#DvRemarkRePrint').modal('show');
            //  $("#success_alert").show();
            //        $("#success_alert").fadeTo(500, 500).slideUp(500, function () {
            //        $("#success_alert").slideUp(500);
            //});
        }
    </script>

    <script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
    </script>

    <script type="text/javascript">
        function fnExcelReport() {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById('Tableprint_sched'); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            //This function will raise when click on html button control
            $("#Button1_sched").click(function () {
                showalert('HTML Button Clicked');
            });
            //This function will raise when click on asp button control

        });
        function showalert(btnText) {
            alert(btnText)
        }
    </script>

    <style type="text/css">
        .wrapword_p1 {
            word-wrap: break-word;
        }
    </style>
    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
                //printWindow.resizeTo(100,100);
                //printWindow.focus();
                //printwindow.window.close();
            }, 500);
            return false;

        }

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function dtPickerInput(inputClassFrom, inputClassTo) {
            $(inputClassFrom).datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(inputClassTo).datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function dtPickerBtn(inputClassFrom, inputClassTo, btnClassFrom, btnClassTo) {
            $(btnClassFrom).click(function () {
                $(inputClassFrom).data("DateTimePicker").show();
            });
            $(btnClassTo).click(function () {
                $(inputClassTo).data("DateTimePicker").show();
            });
        }

        $(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>
    <div>
    </div>

</asp:Content>

