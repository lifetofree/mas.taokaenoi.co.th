﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_en_u0_form_result : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    data_employee _dtEmployee = new data_employee();
    function_tool _funcTool = new function_tool();
    data_en_planning _dtenplan = new data_en_planning();
    DataMachine _dtmachine = new DataMachine();

    string _localJson = String.Empty;

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlSelect_Group = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Group_Machine"];
    static string _urlSelect_Time = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Time_Machine"];
    static string _urlSelect_Standard = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Standard_Machine"];
    static string _urlSelect_Method = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Method_Machine"];
    static string _urlSelect_Tooling = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Tooling_Machine"];
    static string _urlSelect_TypeItem = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeItem_Machine"];
    static string _urlSelect_TypeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeMachineItem"];
    static string _urlSelect_GroupCodeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_GroupCodeMachine"];
    static string _urlSelect_ContentMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ContentMachine"];
    static string _urlInsert_FormResult = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_FormResult"];
    static string _urlSelect_FormResult = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_FormResult"];
    static string _urlSelect_GroupCodeFormResult = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_GroupCodeFormResult"];
    static string _urlSelect_DetailFormResult = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_DetailFormResult"];
    static string _urlDelete_DetailFormResult = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_DetailFormResult"];
    static string _urlSelectEdit_DetailFormResult = _serviceUrl + ConfigurationManager.AppSettings["urlSelectEdit_DetailFormResult"];
    static string _urlUpdate_FormResult = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_FormResult"];
    static string urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Location"];



    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();
            SetDefaultIndex();
        }
    }

    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void select_typecodemachine(DropDownList ddlName, int tmcidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeCodeList = new TypeCode_Detail[1];
        TypeCode_Detail groupmachine = new TypeCode_Detail();
        groupmachine.TmcIDX = tmcidx;

        _dtenplan.BoxEN_TypeCodeList[0] = groupmachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_Group, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeCodeList, "NameTypecode", "TCIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรหัสกลุ่มเครื่องจักร...", "0"));

    }

    protected void select_groupmachine(DropDownList ddlName, int tcidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_GroupCodeList = new GroupCode_Detail[1];
        GroupCode_Detail groupmachine = new GroupCode_Detail();
        groupmachine.TCIDX = tcidx;

        _dtenplan.BoxEN_GroupCodeList[0] = groupmachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_GroupCodeMachine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_GroupCodeList, "NameTypecode", "GCIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรหัสกลุ่ม...", "0"));

    }

    protected void select_qtymachine(DropDownList ddlName)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TimeList = new Timing_Detail[1];
        Timing_Detail qtymachine = new Timing_Detail();
        _dtenplan.BoxEN_TimeList[0] = qtymachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_Time, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TimeList, "time_desc", "m0tidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาจำนวนครั้งเข้าตรวจสอบ...", "0"));

    }

    protected void select_typemachine(DropDownList ddlName)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeItemList = new TypeItem_Detail[1];
        TypeItem_Detail typemachine = new TypeItem_Detail();
        _dtenplan.BoxEN_TypeItemList[0] = typemachine;
        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtenplan));
        _dtenplan = callServicePostENPlanning(_urlSelect_TypeMachine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeItemList, "NameEN", "TmcIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทเครื่องจักร...", "0"));

    }

    protected void select_standardmachine(DropDownList ddlName, int m0coidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_StandardList = new Standard_Detail[1];
        Standard_Detail qtymachine = new Standard_Detail();
        qtymachine.m0coidx = m0coidx;
        _dtenplan.BoxEN_StandardList[0] = qtymachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_Standard, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_StandardList, "standard_name_th", "m0stidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสเปคเครื่องจักร...", "0"));

    }

    protected void select_typeitemmachine(DropDownList ddlName, int GCIDX)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeItemList = new TypeItem_Detail[1];
        TypeItem_Detail qtymachine = new TypeItem_Detail();
        qtymachine.GCIDX = GCIDX;
        _dtenplan.BoxEN_TypeItemList[0] = qtymachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_TypeItem, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeItemList, "type_name_th", "m0tyidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือก POINT จุดที่ตรวจ...", "0"));

    }

    protected void select_methodmachine(DropDownList ddlName, int m0stidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_MethodList = new Method_Detail[1];
        Method_Detail qtymachine = new Method_Detail();
        qtymachine.m0stidx = m0stidx;
        _dtenplan.BoxEN_MethodList[0] = qtymachine;
        //   txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtenplan));
        _dtenplan = callServicePostENPlanning(_urlSelect_Method, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_MethodList, "method_name_th", "m0meidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกวิธีตรวจสอบเครื่องจักร...", "0"));

    }

    protected void select_toolingmachine(CheckBoxList chkName, int GCIDX)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_ToolingList = new Tooling_Detail[1];
        Tooling_Detail qtymachine = new Tooling_Detail();
        qtymachine.GCIDX = GCIDX;
        _dtenplan.BoxEN_ToolingList[0] = qtymachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_Tooling, _dtenplan);
        setChkData(chkName, _dtenplan.BoxEN_ToolingList, "tooling_name_th", "m0toidx");

    }

    protected void select_contentmachine(DropDownList ddlName, int m0tyidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_ContentList = new Content_Detail[1];
        Content_Detail qtymachine = new Content_Detail();
        qtymachine.m0tyidx = m0tyidx;
        _dtenplan.BoxEN_ContentList[0] = qtymachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_ContentMachine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_ContentList, "contentth", "m0coidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรายการ...", "0"));

    }

    protected void Select_Location(DropDownList ddlName)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];

        _dtmachine = callServicePostENRepair(urlSelect_Location, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "LocName", "LocIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานที่...", "0"));
    }

    protected void SelectMasterList()
    {
        _dtenplan.BoxEN_u0formList = new u0form_Detail[1];
        u0form_Detail dtcontent = new u0form_Detail();
        dtcontent.LocIDX = int.Parse(ViewState["LocIDX"].ToString());
        _dtenplan.BoxEN_u0formList[0] = dtcontent;
        _dtenplan = callServicePostENPlanning(_urlSelect_FormResult, _dtenplan);
        setGridData(GvMaster, _dtenplan.BoxEN_u0formList);
    }

    protected void Select_GroupCodeResult()
    {
        _dtenplan.BoxEN_u0formList = new u0form_Detail[1];
        u0form_Detail dtcontent = new u0form_Detail();
        dtcontent.TCIDX = int.Parse(ViewState["TCIDX"].ToString());
        dtcontent.LocIDX = int.Parse(ViewState["LocIDX_"].ToString());
        _dtenplan.BoxEN_u0formList[0] = dtcontent;
        //  txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));
        _dtenplan = callServicePostENPlanning(_urlSelect_GroupCodeFormResult, _dtenplan);
        setGridData(GvGroupCode, _dtenplan.BoxEN_u0formList);
    }

    protected void SelectEdit_FormResult()
    {
        _dtenplan.BoxEN_u0formList = new u0form_Detail[1];
        u0form_Detail dtcontent = new u0form_Detail();
        dtcontent.m2idx = int.Parse(ViewState["m2idx_edit"].ToString());
        _dtenplan.BoxEN_u0formList[0] = dtcontent;

        _dtenplan = callServicePostENPlanning(_urlSelectEdit_DetailFormResult, _dtenplan);
        setFormViewData(FvEdit, _dtenplan.BoxEN_u0formList);

    }


    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        chkName.Items.Clear();
        // bind items
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void linkBtnAsynTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger asynctrigger1 = new AsyncPostBackTrigger();
        asynctrigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(asynctrigger1);
    }


    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_en_planning callServicePostENPlanning(string _cmdUrl, data_en_planning _dtenplan)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtenplan);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtenplan = (data_en_planning)_funcTool.convertJsonToObject(typeof(data_en_planning), _localJson);

        return _dtenplan;
    }

    protected DataMachine callServicePostENRepair(string _cmdUrl, DataMachine _dtmachine)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmachine);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtmachine = (DataMachine)_funcTool.convertJsonToObject(typeof(DataMachine), _localJson);

        return _dtmachine;
    }

    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }



                break;

            case "GvGroupCode":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvItem = (GridView)e.Row.Cells[0].FindControl("GvItem");
                    Label lblGCIDX = (Label)e.Row.Cells[0].FindControl("lblGCIDX");
                    Label lblm1idx = (Label)e.Row.Cells[0].FindControl("lblm1idx");

                    _dtenplan.BoxEN_u0formList = new u0form_Detail[1];
                    u0form_Detail dtcontent = new u0form_Detail();
                    dtcontent.m1idx = int.Parse(lblm1idx.Text);
                    _dtenplan.BoxEN_u0formList[0] = dtcontent;

                    _dtenplan = callServicePostENPlanning(_urlSelect_DetailFormResult, _dtenplan);
                    if (_dtenplan.BoxEN_u0formList != null)
                    {
                        setGridData(GvItem, _dtenplan.BoxEN_u0formList);
                        lbltopic.Text = _dtenplan.BoxEN_u0formList[0].NameTypecode;

                    }
                    else
                    {
                        setGridData(GvItem, null);
                    }

                }
                break;

            case "GvItem":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                break;
        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();
                mergeCell(GvMaster);
                break;
        }
    }

    #endregion

    #region MergeCell
    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvMaster":
                for (int rowIndex = GvMaster.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvMaster.Rows[rowIndex];
                    GridViewRow previousRow = GvMaster.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[1].FindControl("lblocname")).Text == ((Label)previousRow.Cells[1].FindControl("lblocname")).Text &&
                        ((Label)currentRow.Cells[2].FindControl("lbcode")).Text == ((Label)previousRow.Cells[2].FindControl("lbcode")).Text &&
                         ((Label)currentRow.Cells[3].FindControl("lbname")).Text == ((Label)previousRow.Cells[3].FindControl("lbname")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;
                            previousRow.Cells[1].RowSpan = 2;
                            previousRow.Cells[2].RowSpan = 2;
                            previousRow.Cells[3].RowSpan = 2;
                            previousRow.Cells[4].RowSpan = 2;
                            previousRow.Cells[5].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;
                            currentRow.Cells[2].RowSpan = previousRow.Cells[2].RowSpan + 1;
                            currentRow.Cells[3].RowSpan = previousRow.Cells[3].RowSpan + 1;
                            currentRow.Cells[4].RowSpan = previousRow.Cells[4].RowSpan + 1;
                            currentRow.Cells[5].RowSpan = previousRow.Cells[5].RowSpan + 1;
                        }
                        previousRow.Cells[0].Visible = false;
                        previousRow.Cells[1].Visible = false;
                        previousRow.Cells[2].Visible = false;
                        previousRow.Cells[3].Visible = false;
                        previousRow.Cells[4].Visible = false;
                        previousRow.Cells[5].Visible = false;
                    }
                }
                break;


        }
    }

    #endregion


    #region Set Default View
    protected void SetDefaultIndex()
    {
        _divMenuLiToViewIndex.Attributes.Add("class", "active");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        MvMaster.SetActiveView(ViewIndex);
        select_typemachine(ddltype_search);
        Select_Location(ddllocate_search);

    }

    protected void SetDefaultAdd()
    {
        _divMenuLiToViewAdd.Attributes.Add("class", "active");
        _divMenuLiToViewIndex.Attributes.Remove("class");

        MvMaster.SetActiveView(ViewAdd);
        FvDetailUser.ChangeMode(FormViewMode.Insert);
        FvDetailUser.DataBind();

        FvInsert.ChangeMode(FormViewMode.Insert);
        FvInsert.DataBind();

        var ddltypemachine = (DropDownList)FvInsert.FindControl("ddltypemachine");
        var GvReportAdd = (GridView)FvInsert.FindControl("GvReportAdd");
        var ddltypeitem = (DropDownList)FvInsert.FindControl("ddltypeitem");
        var ddllocate = (DropDownList)FvInsert.FindControl("ddllocate");
        var idchecklist = (Control)FvInsert.FindControl("idchecklist");

        select_typemachine(ddltypemachine);
        Select_Location(ddllocate);
        CleardataSetFormList();
        GvReportAdd.Visible = false;
        idchecklist.Visible = false;
    }

    #endregion

    #region SetDefault Form

    protected void SetViewState_Form()
    {

        DataSet dsFoodMaterialList = new DataSet();
        dsFoodMaterialList.Tables.Add("dsAddListTable");

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("LocName", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("LocIDX", typeof(int));


        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("NameEN", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("TmcIDX", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("NameTypecode", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("TCIDX", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("NameGroupcode", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("GCIDX", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("type_name_th", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("m0tyidx", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("contentth", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("m0coidx", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("standard_name_th", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("m0stidx", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("method_name_th", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("m0meidx", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("time", typeof(String));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("tooling_name_th", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("m0toidx", typeof(String));


        ViewState["vsBuyequipment"] = dsFoodMaterialList;

    }

    protected void setAddList_Form()
    {
        if (ViewState["vsBuyequipment"] != null)
        {
            DropDownList ddltypemachine = (DropDownList)FvInsert.FindControl("ddltypemachine");
            DropDownList ddltypecode = (DropDownList)FvInsert.FindControl("ddltypecode");
            DropDownList ddlgroupmachine = (DropDownList)FvInsert.FindControl("ddlgroupmachine");
            DropDownList ddltypeitem = (DropDownList)FvInsert.FindControl("ddltypeitem");
            DropDownList ddlcontent = (DropDownList)FvInsert.FindControl("ddlcontent");
            DropDownList ddlmethod = (DropDownList)FvInsert.FindControl("ddlmethod");
            DropDownList ddlspec = (DropDownList)FvInsert.FindControl("ddlspec");
            DropDownList ddllocate = (DropDownList)FvInsert.FindControl("ddllocate");

            TextBox txttime = (TextBox)FvInsert.FindControl("txttime");
            CheckBoxList chktooling = (CheckBoxList)FvInsert.FindControl("chktooling");

            GridView GvReportAdd = (GridView)FvInsert.FindControl("GvReportAdd");
            int sumcheck_create = 0;
            int i_create = 0;
            string test_detail = "";
            string test_detail_idx = "";

            List<String> AddoingList_checklist = new List<string>();
            foreach (ListItem chkLis_create in chktooling.Items)
            {
                if (chkLis_create.Selected)
                {

                    sumcheck_create = sumcheck_create + 1;


                    test_detail += chkLis_create.Text + ',';
                    test_detail_idx += chkLis_create.Value + ',';
                    sumcheck_create++;
                }
            }

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable"].Rows)
            {

                if (dr["time"].ToString() == txttime.Text &&
                          int.Parse(dr["m0coidx"].ToString()) == int.Parse(ddlcontent.SelectedValue) &&
                          int.Parse(dr["m0stidx"].ToString()) == int.Parse(ddlspec.SelectedValue) &&
                     int.Parse(dr["m0tyidx"].ToString()) == int.Parse(ddltypeitem.SelectedValue) &&
                     int.Parse(dr["TCIDX"].ToString()) == int.Parse(ddltypecode.SelectedValue) &&
                     int.Parse(dr["GCIDX"].ToString()) == int.Parse(ddlgroupmachine.SelectedValue) &&
                    int.Parse(dr["m0meidx"].ToString()) == int.Parse(ddlmethod.SelectedValue) &&
                      int.Parse(dr["LocIDX"].ToString()) == int.Parse(ddllocate.SelectedValue))
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }
            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable"].NewRow();
            drContacts["LocName"] = ddllocate.SelectedItem.Text;
            drContacts["LocIDX"] = ddllocate.SelectedValue;
            drContacts["NameEN"] = ddltypemachine.SelectedItem.Text;
            drContacts["TmcIDX"] = ddltypemachine.SelectedValue;
            drContacts["NameTypecode"] = ddltypecode.SelectedItem.Text;
            drContacts["TCIDX"] = ddltypecode.SelectedValue;

            if (ddlgroupmachine.SelectedValue != "0")
            {
                drContacts["NameGroupcode"] = ddlgroupmachine.SelectedItem.Text;
                drContacts["GCIDX"] = ddlgroupmachine.SelectedValue;
            }
            else
            {
                drContacts["NameGroupcode"] = "-";
                drContacts["GCIDX"] = "0";
            }

            drContacts["type_name_th"] = ddltypeitem.SelectedItem.Text;
            drContacts["m0tyidx"] = ddltypeitem.SelectedValue;
            drContacts["contentth"] = ddlcontent.SelectedItem.Text;
            drContacts["m0coidx"] = ddlcontent.SelectedValue;
            drContacts["standard_name_th"] = ddlspec.SelectedItem.Text;
            drContacts["m0stidx"] = ddlspec.SelectedValue;
            drContacts["method_name_th"] = ddlmethod.SelectedItem.Text;
            drContacts["m0meidx"] = ddlmethod.SelectedValue;

            if (txttime.Text != "")
            {
                drContacts["time"] = txttime.Text;
            }
            else
            {
                drContacts["time"] = "-";
            }

            if (test_detail != "")
            {
                drContacts["tooling_name_th"] = test_detail;
                drContacts["m0toidx"] = test_detail_idx;
            }
            else
            {
                drContacts["tooling_name_th"] = "-";
                drContacts["m0toidx"] = "0";
            }



            dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts);
            ViewState["vsBuyequipment"] = dsContacts;
            GvReportAdd.Visible = true;
            setGridData(GvReportAdd, dsContacts.Tables["dsAddListTable"]);
            GvReportAdd.Visible = true;

        }
    }

    protected void CleardataSetFormList()
    {
        var GvReportAdd = (GridView)FvInsert.FindControl("GvReportAdd");
        ViewState["vsBuyequipment"] = null;
        GvReportAdd.DataSource = ViewState["vsBuyequipment"];
        GvReportAdd.DataBind();
        SetViewState_Form();
    }

    #endregion

    #region onRowCommand

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "bnDeleteList":
                    GridView GvReportAdd = (GridView)FvInsert.FindControl("GvReportAdd");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];
                    dsContacts.Tables["dsAddListTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(GvReportAdd, dsContacts.Tables["dsAddListTable"]);
                    if (dsContacts.Tables["dsAddListTable"].Rows.Count < 1)
                    {
                        GvReportAdd.Visible = false;
                    }
                    break;

            }
        }
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        var ddltypecode = (DropDownList)FvInsert.FindControl("ddltypecode");
        var ddltypemachine = (DropDownList)FvInsert.FindControl("ddltypemachine");
        var ddlgroupmachine = (DropDownList)FvInsert.FindControl("ddlgroupmachine");
        var ddltypeitem = (DropDownList)FvInsert.FindControl("ddltypeitem");
        var ddlcontent = (DropDownList)FvInsert.FindControl("ddlcontent");
        var ddlspec = (DropDownList)FvInsert.FindControl("ddlspec");
        var ddlmethod = (DropDownList)FvInsert.FindControl("ddlmethod");
        var chktooling = (CheckBoxList)FvInsert.FindControl("chktooling");
        var idchecklist = (Control)FvInsert.FindControl("idchecklist");
        var ddlcontent_edit = (DropDownList)FvEdit.FindControl("ddlcontent_edit");
        var ddltype_edit = (DropDownList)FvEdit.FindControl("ddltype_edit");
        var ddlstandard_edit = (DropDownList)FvEdit.FindControl("ddlstandard_edit");
        var ddlmethod_edit = (DropDownList)FvEdit.FindControl("ddlmethod_edit");

        switch (ddName.ID)
        {
            case "ddltypemachine":

                select_typecodemachine(ddltypecode, int.Parse(ddltypemachine.SelectedValue));

                break;

            case "ddltypecode":
                select_groupmachine(ddlgroupmachine, int.Parse(ddltypecode.SelectedValue));
                break;


            case "ddltype_search":
                select_typecodemachine(ddltypecode_search, int.Parse(ddltype_search.SelectedValue));

                break;

            case "ddlgroupmachine":
                select_typeitemmachine(ddltypeitem, int.Parse(ddlgroupmachine.SelectedValue));
                select_toolingmachine(chktooling, int.Parse(ddlgroupmachine.SelectedValue));
                idchecklist.Visible = true;

                break;

            case "ddltypeitem":
                select_contentmachine(ddlcontent, int.Parse(ddltypeitem.SelectedValue));

                break;

            case "ddlcontent":
                select_standardmachine(ddlspec, int.Parse(ddlcontent.SelectedValue));

                break;

            case "ddlspec":
                select_methodmachine(ddlmethod, int.Parse(ddlspec.SelectedValue));
                break;

            case "ddltype_edit":
                select_contentmachine(ddlcontent_edit, int.Parse(ddltype_edit.SelectedValue));
                break;

            case "ddlcontent_edit":
                select_standardmachine(ddlstandard_edit, int.Parse(ddlcontent_edit.SelectedValue));

                break;

            case "ddlstandard_edit":
                select_methodmachine(ddlmethod_edit, int.Parse(ddlstandard_edit.SelectedValue));

                break;


        }
    }

    #endregion

    #region FormView

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewAdd.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }
                break;

        }
    }


    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        Control divsave = (Control)FvInsert.FindControl("divsave");
        DropDownList ddltypemachine = (DropDownList)FvInsert.FindControl("ddltypemachine");
        DropDownList ddltypecode = (DropDownList)FvInsert.FindControl("ddltypecode");
        DropDownList ddlgroupmachine = (DropDownList)FvInsert.FindControl("ddlgroupmachine");
        DropDownList ddltypeitem = (DropDownList)FvInsert.FindControl("ddltypeitem");
        DropDownList ddlcontent = (DropDownList)FvInsert.FindControl("ddlcontent");
        DropDownList ddlmethod = (DropDownList)FvInsert.FindControl("ddlmethod");
        DropDownList ddlspec = (DropDownList)FvInsert.FindControl("ddlspec");
        DropDownList ddllocate = (DropDownList)FvInsert.FindControl("ddllocate");
        TextBox txttime1 = (TextBox)FvInsert.FindControl("txttime");
        CheckBoxList chktooling = (CheckBoxList)FvInsert.FindControl("chktooling");

        _dtenplan.BoxEN_m1formList = new m1form_Detail[1];
        _dtenplan.BoxEN_m2formList = new m2form_Detail[1];
        _dtenplan.BoxEN_m3formList = new m3form_Detail[1];

        m1form_Detail _master1 = new m1form_Detail();
        m2form_Detail _master2 = new m2form_Detail();
        m3form_Detail _master3 = new m3form_Detail();


        switch (cmdName)
        {

            case "_divMenuBtnToDivIndex":
                SetDefaultIndex();

                break;

            case "_divMenuBtnToDivAdd":
                SetDefaultAdd();

                break;
            case "CmdAddForm":
                setAddList_Form();
                divsave.Visible = true;
                ddltypemachine.SelectedValue = "0";
                ddltypecode.SelectedValue = "0";
                ddlgroupmachine.SelectedValue = "0";
                ddltypeitem.SelectedValue = "0";
                ddlcontent.SelectedValue = "0";
                ddlmethod.SelectedValue = "0";
                ddlspec.SelectedValue = "0";
                ddllocate.SelectedValue = "0";
                txttime1.Text = String.Empty;

                break;

            case "CmdInsertForm":
                int i = 0;
                var ds_udoc1_insert = (DataSet)ViewState["vsBuyequipment"];
                var _document1 = new m1form_Detail[ds_udoc1_insert.Tables[0].Rows.Count];
                var _document2 = new m2form_Detail[ds_udoc1_insert.Tables[0].Rows.Count];

                foreach (DataRow dtrow in ds_udoc1_insert.Tables[0].Rows)
                {
                    _document1[i] = new m1form_Detail();
                    _document2[i] = new m2form_Detail();

                    _dtenplan.BoxEN_m1formList = new m1form_Detail[1];
                    _dtenplan.BoxEN_m2formList = new m2form_Detail[1];

                    _document1[i].TCIDX = int.Parse(dtrow["TCIDX"].ToString());
                    _document1[i].CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                    _document1[i].LocIDX = int.Parse(dtrow["LocIDX"].ToString());


                    if (dtrow["GCIDX"].ToString() == "")
                    {
                        _document1[i].GCIDX = 0;
                        _document2[i].GCIDX2 = 0;
                    }
                    else
                    {
                        _document1[i].GCIDX = int.Parse(dtrow["GCIDX"].ToString());
                        _document2[i].GCIDX2 = int.Parse(dtrow["GCIDX"].ToString());
                    }

                    _document2[i].m0tyidx = int.Parse(dtrow["m0tyidx"].ToString());
                    _document2[i].m0coidx = int.Parse(dtrow["m0coidx"].ToString());
                    _document2[i].m0stidx = int.Parse(dtrow["m0stidx"].ToString());
                    _document2[i].m0meidx = int.Parse(dtrow["m0meidx"].ToString());
                    _document2[i].time = dtrow["time"].ToString();
                    _document2[i].CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                    _document2[i].TCIDX2 = int.Parse(dtrow["TCIDX"].ToString());

                    if (dtrow["m0toidx"].ToString() == "")
                    {
                        _document2[i].m0toidx_comma = "0";
                    }
                    else
                    {
                        _document2[i].m0toidx_comma = dtrow["m0toidx"].ToString();
                    }


                    i++;

                }
                _dtenplan.BoxEN_m1formList = _document1;
                _dtenplan.BoxEN_m2formList = _document2;
                // txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));
                _dtenplan = callServicePostENPlanning(_urlInsert_FormResult, _dtenplan);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "BtnBack":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "cmdlocate":
                string[] arg1_locate = new string[0];
                arg1_locate = e.CommandArgument.ToString().Split(';');
                int LocIDX = int.Parse(arg1_locate[0]);
                ViewState["LocIDX"] = LocIDX;
                SelectMasterList();
                mergeCell(GvMaster);



                break;
            case "CmdViewDetail":
                string[] arg1 = new string[1];
                arg1 = e.CommandArgument.ToString().Split(';');
                int TCIDX = int.Parse(arg1[0]);
                int LocIDX_ = int.Parse(arg1[1]);
                ViewState["TCIDX"] = TCIDX;
                ViewState["LocIDX_"] = LocIDX_;

                Select_GroupCodeResult();
                MvMaster.SetActiveView(ViewDetail);
                break;

            case "btnsearch":
                ViewState["LocIDX"] = ddllocate_search.SelectedValue;

                _dtenplan.BoxEN_u0formList = new u0form_Detail[1];
                u0form_Detail dtform = new u0form_Detail();
                dtform.TCIDX = int.Parse(ddltypecode_search.SelectedValue);
                dtform.TmcIDX = int.Parse(ddltype_search.SelectedValue);
                dtform.LocIDX = int.Parse(ddllocate_search.SelectedValue);

                _dtenplan.BoxEN_u0formList[0] = dtform;
                _dtenplan = callServicePostENPlanning(_urlSelect_FormResult, _dtenplan);
                setGridData(GvMaster, _dtenplan.BoxEN_u0formList);

                mergeCell(GvMaster);

                break;

            case "CmdDel":
                string[] arg1_delete = new string[1];
                arg1_delete = e.CommandArgument.ToString().Split(';');
                int m2idx = int.Parse(arg1_delete[0]);


                _dtenplan.BoxEN_u0formList = new u0form_Detail[1];
                u0form_Detail dtdelete = new u0form_Detail();
                dtdelete.m2idx = m2idx;
                dtdelete.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                _dtenplan.BoxEN_u0formList[0] = dtdelete;

                // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtenplan));

                _dtenplan = callServicePostENPlanning(_urlDelete_DetailFormResult, _dtenplan);
                Select_GroupCodeResult();

                break;

            case "CmdEditIT":
                string[] arg1_edit = new string[1];
                arg1_edit = e.CommandArgument.ToString().Split(';');
                int m2idx_edit = int.Parse(arg1_edit[0]);
                ViewState["m2idx_edit"] = m2idx_edit;
                GvGroupCode.Visible = false;
                divbtneditsave.Visible = false;
                panel_edit.Visible = true;
                FvEdit.ChangeMode(FormViewMode.Edit);
                FvEdit.DataBind();

                SelectEdit_FormResult();

                DropDownList ddltype_edit = (DropDownList)FvEdit.FindControl("ddltype_edit");
                TextBox txtm0tyidx = (TextBox)FvEdit.FindControl("txtm0tyidx");
                TextBox txtGCIDX = (TextBox)FvEdit.FindControl("txtGCIDX");


                DropDownList ddlcontent_edit = (DropDownList)FvEdit.FindControl("ddlcontent_edit");
                TextBox txtm0coidx = (TextBox)FvEdit.FindControl("txtm0coidx");
                DropDownList ddlstandard_edit = (DropDownList)FvEdit.FindControl("ddlstandard_edit");
                TextBox txtm0stidx = (TextBox)FvEdit.FindControl("txtm0stidx");
                DropDownList ddlmethod_edit = (DropDownList)FvEdit.FindControl("ddlmethod_edit");
                TextBox txm0meidx = (TextBox)FvEdit.FindControl("txm0meidx");
                CheckBoxList chktooling_edit = (CheckBoxList)FvEdit.FindControl("chktooling_edit");
                TextBox txtchk_tool = (TextBox)FvEdit.FindControl("txtchk_tool");


                select_typeitemmachine(ddltype_edit, int.Parse(txtGCIDX.Text));
                ddltype_edit.SelectedValue = txtm0tyidx.Text;
                select_contentmachine(ddlcontent_edit, int.Parse(txtm0tyidx.Text));
                ddlcontent_edit.SelectedValue = txtm0coidx.Text;
                select_standardmachine(ddlstandard_edit, int.Parse(txtm0coidx.Text));
                ddlstandard_edit.SelectedValue = txtm0stidx.Text;
                select_methodmachine(ddlmethod_edit, int.Parse(txtm0stidx.Text));
                ddlmethod_edit.SelectedValue = txm0meidx.Text;
                select_toolingmachine(chktooling_edit, int.Parse(txtGCIDX.Text));

                string[] ToId = txtchk_tool.Text.Split(',');

                foreach (ListItem li in chktooling_edit.Items)
                {
                    foreach (string To_check in ToId)
                    {

                        if (li.Value == To_check)
                        {
                            li.Selected = true;

                            break;
                        }
                    }
                }

                //  ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                break;

            case "CmdUpdate":
                TextBox txtm2idx = (TextBox)FvEdit.FindControl("txtm2idx");
                DropDownList ddltype_edit1 = (DropDownList)FvEdit.FindControl("ddltype_edit");
                DropDownList ddlcontent_edit1 = (DropDownList)FvEdit.FindControl("ddlcontent_edit");
                DropDownList ddlstandard_edit1 = (DropDownList)FvEdit.FindControl("ddlstandard_edit");
                DropDownList ddlmethod_edit1 = (DropDownList)FvEdit.FindControl("ddlmethod_edit");
                CheckBoxList chktooling_edit1 = (CheckBoxList)FvEdit.FindControl("chktooling_edit");
                TextBox txttime = (TextBox)FvEdit.FindControl("txttime");


                _dtenplan.BoxEN_u0formList = new u0form_Detail[1];
                u0form_Detail dtedit = new u0form_Detail();

                dtedit.m2idx = int.Parse(txtm2idx.Text);
                dtedit.m0tyidx = int.Parse(ddltype_edit1.SelectedValue);
                dtedit.m0coidx = int.Parse(ddlcontent_edit1.SelectedValue);
                dtedit.m0stidx = int.Parse(ddlstandard_edit1.SelectedValue);
                dtedit.m0meidx = int.Parse(ddlmethod_edit1.SelectedValue);
                dtedit.time = txttime.Text;
                foreach (ListItem li in chktooling_edit1.Items)
                {

                    if (li.Selected == true)
                    {

                        dtedit.m0toidx_comma += li.Value + ",";

                    }

                }


                dtedit.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                _dtenplan.BoxEN_u0formList[0] = dtedit;

                _dtenplan = callServicePostENPlanning(_urlUpdate_FormResult, _dtenplan);
                Select_GroupCodeResult();
                GvGroupCode.Visible = true;
                divbtneditsave.Visible = true;
                panel_edit.Visible = false;
                break;

            case "CmdCancel":
                Select_GroupCodeResult();
                GvGroupCode.Visible = true;
                divbtneditsave.Visible = true;
                panel_edit.Visible = false;

                break;

            case "CmdbackForm":
                SelectMasterList();
                MvMaster.SetActiveView(ViewIndex);
                SetDefaultIndex();
                mergeCell(GvMaster);

                break;
        }
    }
    #endregion
}
