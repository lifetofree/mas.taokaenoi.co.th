﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;
using System.Data.SqlClient;

public partial class websystem_MasterData_it_news_master : System.Web.UI.Page
{

    #region initial function/data

    function_tool _funcTool = new function_tool();
    data_it_news _data_it_news = new data_it_news();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlSettypeNews = _serviceUrl + ConfigurationManager.AppSettings["urlSettypeNews"];
    static string _urlGettypeNews = _serviceUrl + ConfigurationManager.AppSettings["urlGettypeNews"];
    static string _urlSetdelete_typeNews = _serviceUrl + ConfigurationManager.AppSettings["urlSetdelete_typeNews"];
    static string _urlGettypeNews_Ondropdownlist = _serviceUrl + ConfigurationManager.AppSettings["urlGettypeNews_Ondropdownlist"];
    static string _urlSetITNews = _serviceUrl + ConfigurationManager.AppSettings["urlSetITNews"];
    static string _urlGetITNews = _serviceUrl + ConfigurationManager.AppSettings["urlGetITNews"];
    static string _urlSetdeleteNews = _serviceUrl + ConfigurationManager.AppSettings["urlSetdeleteNews"];



    string _localJson = "";
    int _tempInt = 0;


    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            setdefault();
            
        
        }

        linkBtnTrigger(listMenu3);
        linkBtnTrigger(listMenu2);
        linkBtnTrigger(btnsavenews);
        linkBtnTrigger(btnsave);
        linkBtnTrigger(btnShowBoxNews);
    }

    #region setdefault
    protected void setdefault()
    {
        mvMultiview.SetActiveView(viewInsert_NewsIT);
        listMenu2.BackColor = System.Drawing.Color.Transparent;
        listMenu0.BackColor = System.Drawing.Color.Transparent;
        listMenu3.BackColor = System.Drawing.Color.LightGray;
        SELECT_IT_NEWS();

    }

    #endregion

    #region gridview

    #region gvPageIndexChanging

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {


            case "gvtypeNews":

                gvtypeNews.PageIndex = e.NewPageIndex;
                gvtypeNews.DataBind();
                SELECT_MASTER_TYPENEWS();

                break;

            case "gvNewsIT":

                gvNewsIT.PageIndex = e.NewPageIndex;
                gvNewsIT.DataBind();
                SELECT_IT_NEWS();

                break;

            case "gvNewsITlist":

                gvNewsITlist.PageIndex = e.NewPageIndex;
                gvNewsITlist.DataBind();
                SELECT_IT_NEWSList();

                break;
        }

    }

    #endregion

    #region gvRowUpdating

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvtypeNews":

                var _update_m0_news_idx = (TextBox)gvtypeNews.Rows[e.RowIndex].FindControl("_update_m0_news_idx");
                var _update_nametypenews = (TextBox)gvtypeNews.Rows[e.RowIndex].FindControl("txtupdate_typeNews");
                var _update_statustypenews = (DropDownList)gvtypeNews.Rows[e.RowIndex].FindControl("ddlstatus_type_news");
                var lbltxtShowtypeNewsUpdate = (Label)gvtypeNews.Rows[e.RowIndex].FindControl("lbltxtShowtypeNewsUpdate");

                gvtypeNews.EditIndex = -1;

                data_it_news _dataitNews = new data_it_news();
                _dataitNews.m0_type_it_news_list = new m0_type_it_news_details[1];
                m0_type_it_news_details _updatetypenews = new m0_type_it_news_details();

                _updatetypenews.m0_news_idx = int.Parse(_update_m0_news_idx.Text);
                _updatetypenews.status_type_news = int.Parse(_update_statustypenews.SelectedValue);
                _updatetypenews.type_it_news = _update_nametypenews.Text;

                ViewState["_TYPENAMENEWS"] = _update_nametypenews.Text;

                _dataitNews.m0_type_it_news_list[0] = _updatetypenews;

                _dataitNews = callServiceITNews(_urlSettypeNews, _dataitNews);

                if (_dataitNews.return_code == "0")
                {
                      
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);
                    SELECT_MASTER_TYPENEWS();
                }
                else

                {


                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);


                }


                break;

            case "gvNewsIT":

                var _update_u0_news_idx = (TextBox)gvNewsIT.Rows[e.RowIndex].FindControl("_update_u0_news_idx");
                var txtupdate_titlenews = (TextBox)gvNewsIT.Rows[e.RowIndex].FindControl("txtupdate_titlenews");
                var txtupdate_details_news = (TextBox)gvNewsIT.Rows[e.RowIndex].FindControl("txtupdate_details_news");
                var ddlupdate_m0_news = (DropDownList)gvNewsIT.Rows[e.RowIndex].FindControl("ddlupdate_m0_news");
                var ddlupdate_status_news = (DropDownList)gvNewsIT.Rows[e.RowIndex].FindControl("ddlupdate_status_news");

                gvNewsIT.EditIndex = -1;

                data_it_news _data_updateitNews = new data_it_news();
                _data_updateitNews.u0_it_news_list = new u0_it_news_details[1];
                u0_it_news_details _updatnews = new u0_it_news_details();


                _updatnews.u0_news_idx = int.Parse(_update_u0_news_idx.Text);
                _updatnews.status_news = int.Parse(ddlupdate_status_news.SelectedValue);
                _updatnews.m0_news_idx = int.Parse(ddlupdate_m0_news.SelectedValue);
                _updatnews.details_news = txtupdate_details_news.Text;
                _updatnews.title_it_news = txtupdate_titlenews.Text;

                _data_updateitNews.u0_it_news_list[0] = _updatnews;

                _data_updateitNews = callServiceITNews(_urlSetITNews, _data_updateitNews);


                if (_data_updateitNews.return_code == "0")
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);

                }

                else

                {


                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);


                }

                SELECT_IT_NEWS();



                break;
        }

    }

    #endregion

    #region gvRowDataBound

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvtypeNews":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbstatus_type_news = (Label)e.Row.Cells[2].FindControl("lbstatus_type_news");
                    Label status_online = (Label)e.Row.Cells[2].FindControl("status_online");
                    Label status_offline = (Label)e.Row.Cells[2].FindControl("status_offline");

                    ViewState["status_news"] = lbstatus_type_news.Text;

                    if (ViewState["status_news"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_news"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    btnShowinsertType.Visible = true;
                    panel_Insert_TYpeNewsIT.Visible = false;
                    btnBack.Visible = false;


                }

                break;

            case "gvNewsIT":


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbstatus_news = (Label)e.Row.Cells[3].FindControl("lbstatus_news");
                    Label status_news_online = (Label)e.Row.Cells[3].FindControl("status_news_online");
                    Label status_news_offline = (Label)e.Row.Cells[3].FindControl("status_news_offline");

                    ViewState["_status_news"] = lbstatus_news.Text;

                    if (ViewState["_status_news"].ToString() == "0")
                    {
                        status_news_offline.Visible = true;
                    }
                    else if (ViewState["_status_news"].ToString() == "1")
                    {
                        status_news_online.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";


                    TextBox txtupdate_m0_news_idx = (TextBox)e.Row.Cells[0].FindControl("txtupdate_m0_news_idx");
                    DropDownList ddlupdate_m0_news = (DropDownList)e.Row.Cells[0].FindControl("ddlupdate_m0_news");

                    data_it_news _dataupdateNews = new data_it_news();
                    _dataupdateNews.m0_type_it_news_list = new m0_type_it_news_details[1];
                    m0_type_it_news_details select_News_ondropdownlist = new m0_type_it_news_details();

                    _dataupdateNews.m0_type_it_news_list[0] = select_News_ondropdownlist;

                    _dataupdateNews = callServiceITNews(_urlGettypeNews_Ondropdownlist, _dataupdateNews);

                    ddlupdate_m0_news.AppendDataBoundItems = true;
                    ddlupdate_m0_news.Items.Add(new ListItem("กรุณาเลือกประเภทข่าวไอที", "0"));
                    ddlupdate_m0_news.DataSource = _dataupdateNews.m0_type_it_news_list;
                    ddlupdate_m0_news.DataTextField = "type_it_news";
                    ddlupdate_m0_news.DataValueField = "m0_news_idx";
                    ddlupdate_m0_news.DataBind();
                    ddlupdate_m0_news.SelectedValue = txtupdate_m0_news_idx.Text;

                    panelInsertNews.Visible = false;
                    btnShowBoxNews.Visible = true;
                    btncancelNews.Visible = false;
                    SETFOCUS_ONTOP.Focus();
                }

                break;


            case "gvNewsITlist":


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbstatusnews = (Label)e.Row.Cells[3].FindControl("lbstatusnews");
                    Label status_newsonline = (Label)e.Row.Cells[3].FindControl("statusnewsonline");
                    Label status_newsoffline = (Label)e.Row.Cells[3].FindControl("statusnewsoffline");

                    ViewState["_statusnews"] = lbstatusnews.Text;

                    if (ViewState["_statusnews"].ToString() == "0")
                    {
                        status_newsoffline.Visible = true;
                    }
                    else if (ViewState["_statusnews"].ToString() == "1")
                    {
                        status_newsonline.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                   // btnShowBoxNews.Visible = true;
                }

                break;
        }
    }

    #endregion

    #region gvRowCancelingEdit

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvtypeNews":

                gvtypeNews.EditIndex = -1;
                SELECT_MASTER_TYPENEWS();
                SETFOCUS_ONTOP.Focus();
                break;

            case "gvNewsIT":

                gvNewsIT.EditIndex = -1;
                SELECT_IT_NEWS();
                SETFOCUS_ONTOP.Focus();
                break;
        }
    }

    #endregion

    #region GvRowEditing

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvtypeNews":

                gvtypeNews.EditIndex = e.NewEditIndex;
                SELECT_MASTER_TYPENEWS();

                break;

            case "gvNewsIT":

                gvNewsIT.EditIndex = e.NewEditIndex;
                SELECT_IT_NEWS();

                break;
        }
    }

    #endregion

    #endregion


    #region SELECT DATA 

    # region SELECT_MASTER_TYPENEWS

    protected void SELECT_MASTER_TYPENEWS()
    {

        _data_it_news.m0_type_it_news_list = new m0_type_it_news_details[1];
        m0_type_it_news_details select_typeNews = new m0_type_it_news_details();

        _data_it_news.m0_type_it_news_list[0] = select_typeNews;

        _data_it_news = callServiceITNews(_urlGettypeNews, _data_it_news);

        if (_data_it_news.m0_type_it_news_list != null)
        {

            ViewState["vsMasterDataTypeNews"] = _data_it_news.m0_type_it_news_list;

            gvtypeNews.DataSource = ViewState["vsMasterDataTypeNews"];
            gvtypeNews.DataBind();


        }
        else
        {
            gvtypeNews.DataBind();
        }

    }

    #endregion

    #region SELECT_IT_NEWS

    protected void SELECT_IT_NEWS()
    {

        data_it_news _dataNews = new data_it_news();
        _dataNews.u0_it_news_list = new u0_it_news_details[1];
        u0_it_news_details select_News = new u0_it_news_details();

        _dataNews.u0_it_news_list[0] = select_News;

        _dataNews = callServiceITNews(_urlGetITNews, _dataNews);

        if(_dataNews.u0_it_news_list != null)
        {
            ViewState["vsDataNews"] = _dataNews.u0_it_news_list;
            gvNewsIT.DataSource = ViewState["vsDataNews"];
            gvNewsIT.DataBind();

        }
        else
        {
            gvNewsIT.DataBind();
        }

      



    }

    #endregion

    protected void Check_NEWS_Duplicate(string _check_input, Label lbname)
    {

        u0_it_news_details[] _listNews = (u0_it_news_details[])ViewState["vsDataNews"];
        lbname.Text = "";

        if (_listNews == null)
        {

            if (_check_input != null)
            {
                lbname.Text = "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้ชื่อประเภทข่าวนี้ได้";
                lbname.ForeColor = System.Drawing.Color.Green;
                ViewState["checkType"] = 0;
            }
            else if (_check_input == null)
            {
                lbname.Text = "";
            }
        }
        else
        {
            var _linqCheckType = (from datanews in _listNews
                                  where datanews.title_it_news.Contains(_check_input)
                                  select new
                                  {
                                      datanews.title_it_news
                                  }).ToList();

            string _check_type = "";

            for (int type = 0; type < _linqCheckType.Count(); type++)
            {
                _check_type = _linqCheckType[type].title_it_news.ToString();

            }

            if (_check_type == _check_input.ToString())
            {

                lbname.Text = "<i class='fa fa-times' aria-hidden='true'></i>  " + "มีประเภทข่าว  " + _check_type + " นี้แล้ว กรุณาทำการกรอกประเภทข่าวใหม่";
                lbname.ForeColor = System.Drawing.Color.Red;
                ViewState["checkType"] = 1;
            }
            else
            {
                if (_check_input != null && _check_input != "")
                {
                    lbname.Text = "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้ชื่อประเภทข่าวนี้ได้";
                    lbname.ForeColor = System.Drawing.Color.Green;
                    ViewState["checkType"] = 0;
                }
                else if (_check_input == null && _check_input == "")
                {
                    lbname.Text = "";
                }

            }

        }

    }

    protected void Check_TYPENEWS_Duplicate(string _checktype_input , Label lbname)
    {

        m0_type_it_news_details[] _listTypeNews = (m0_type_it_news_details[])ViewState["vsMasterDataTypeNews"];
        lbname.Text = "";

        if (_listTypeNews == null)
        {

            if (_checktype_input != null)
            {
                lbname.Text = "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้ชื่อประเภทข่าวนี้ได้";
                lbname.ForeColor = System.Drawing.Color.Green;
                ViewState["checkType"] = 0;
            }
            else if (_checktype_input == null)
            {
                lbname.Text = "";
            }
        }
        else
        {
            var _linqCheckType = (from datatype in _listTypeNews 
                                  where datatype.type_it_news.Contains(_checktype_input) 
                                   select new
                                      {
                                          datatype.type_it_news
                                      }).ToList();

            string _check_type = "";

            for (int type = 0; type < _linqCheckType.Count(); type++)
            {
                _check_type = _linqCheckType[type].type_it_news.ToString();

            }

            if (_check_type == _checktype_input.ToString())
            {

                lbname.Text = "<i class='fa fa-times' aria-hidden='true'></i>  " + "มีประเภทข่าว  " + _check_type + " นี้แล้ว กรุณาทำการกรอกประเภทข่าวใหม่";
                lbname.ForeColor = System.Drawing.Color.Red;
                ViewState["checkType"] = 1;
            }
            else
            {
                if (_checktype_input != null && txtNameTYpeNews.Text != "")
                {
                    lbname.Text = "<i class='fa fa-check' aria-hidden='true'></i>  " + "สามารถใช้ชื่อประเภทข่าวนี้ได้";
                    lbname.ForeColor = System.Drawing.Color.Green;
                    ViewState["checkType"] = 0;
                }
                else if (_checktype_input == null && txtNameTYpeNews.Text == "")
                {
                    lbname.Text = "";
                }
                   
            }

        }

    }

    #region SELECT_IT_NEWSlist

    protected void SELECT_IT_NEWSList()
    {

        data_it_news _dataNews = new data_it_news();
        _dataNews.u0_it_news_list = new u0_it_news_details[1];
        u0_it_news_details select_News = new u0_it_news_details();

        _dataNews.u0_it_news_list[0] = select_News;

        _dataNews = callServiceITNews(_urlGetITNews, _dataNews);

        gvNewsITlist.DataSource = _dataNews.u0_it_news_list;
        gvNewsITlist.DataBind();



    }

    #endregion

    #region SELECT_MASTER_TYPENEWS_ON_DROPDOWNLIST

    protected void SELECT_MASTER_TYPENEWS_ON_DROPDOWNLIST()
    {

        data_it_news _datatypeNews = new data_it_news();
        _datatypeNews.m0_type_it_news_list = new m0_type_it_news_details[1];
        m0_type_it_news_details select_typeNews_ondropdownlist = new m0_type_it_news_details();

        _datatypeNews.m0_type_it_news_list[0] = select_typeNews_ondropdownlist;

        _datatypeNews = callServiceITNews(_urlGettypeNews_Ondropdownlist, _datatypeNews);

        ddlNametypeNews.AppendDataBoundItems = true;
        ddlNametypeNews.Items.Add(new ListItem("กรุณาเลือกประเภทข่าวไอที", "0"));
        ddlNametypeNews.DataSource = _datatypeNews.m0_type_it_news_list;
        ddlNametypeNews.DataTextField = "type_it_news";
        ddlNametypeNews.DataValueField = "m0_news_idx";
        ddlNametypeNews.DataBind();

    }
    #endregion




    #endregion

    #region btnCommand

    protected void btncommand(object sender, CommandEventArgs e)
    {
        string cmd_name = e.CommandName.ToString();
        string cmd_arg = e.CommandArgument.ToString();

        gvtypeNews.EditIndex = -1;
        gvNewsITlist.EditIndex = -1;

        switch (cmd_name)
        {
            case "viewmanu1":

                listMenu0.BackColor = System.Drawing.Color.LightGray;
                listMenu2.BackColor = System.Drawing.Color.Transparent;
                listMenu3.BackColor = System.Drawing.Color.Transparent;

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                SETFOCUS_ONTOP.Focus();

                break;

            case "viewmanu2":

                linkBtnTrigger(btnsavenews);
                linkBtnTrigger(btnsave);

                listMenu0.BackColor = System.Drawing.Color.Transparent;
                listMenu2.BackColor = System.Drawing.Color.LightGray;
                listMenu3.BackColor = System.Drawing.Color.Transparent;
                btnShowinsertType.Visible = true;
                btnBack.Visible = false;
                panel_Insert_TYpeNewsIT.Visible = false;
                SELECT_MASTER_TYPENEWS();

                mvMultiview.SetActiveView(viewInsert_TYpeNewsIT);

                SETFOCUS_ONTOP.Focus();

                break;

            case "viewmanu3":

                // linkBtnTrigger(btnsavenews);

                linkBtnTrigger(btnsavenews);
                linkBtnTrigger(btnsave);

                listMenu0.BackColor = System.Drawing.Color.Transparent;
                listMenu2.BackColor = System.Drawing.Color.Transparent;
                listMenu3.BackColor = System.Drawing.Color.LightGray;

                SELECT_MASTER_TYPENEWS_ON_DROPDOWNLIST();
                SELECT_IT_NEWSList();
                mvMultiview.SetActiveView(viewInsert_NewsIT);

                btnShowBoxNews.Visible = true;
                panelInsertNews.Visible = false;
                btncancelNews.Visible = false;
                SETFOCUS_ONTOP.Focus();

                break;


            case "save_type_news":


                Check_TYPENEWS_Duplicate(txtNameTYpeNews.Text, lbltxtShowTypeNews);

                if (ViewState["checkType"].ToString() == "1")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีประเภทข่าว  " + txtNameTYpeNews.Text + " นี้แล้ว กรุณาทำการกรอกประเภทข่าวใหม่');", true);
                }
                else
                {
                    _data_it_news.m0_type_it_news_list = new m0_type_it_news_details[1];
                    m0_type_it_news_details insert_typeNews = new m0_type_it_news_details();
                    insert_typeNews.type_it_news = txtNameTYpeNews.Text;
                    insert_typeNews.status_type_news = int.Parse(ddlStatusTypeNews.SelectedValue);
                    _data_it_news.m0_type_it_news_list[0] = insert_typeNews;

                    _data_it_news = callServiceITNews(_urlSettypeNews, _data_it_news);


                    if (_data_it_news.return_code == "0")
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);
                        txtNameTYpeNews.Text = "";
                        lbltxtShowTypeNews.Text = "";
                    }

                    else

                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);
                      
                    }

                }

                SELECT_MASTER_TYPENEWS();
                SETFOCUS_ONTOP.Focus();


                break;


            case "btncancel":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "deletetypeNews":

                ViewState["_DELETE_M0NEWSIDX"] = int.Parse(cmd_arg);

                _data_it_news.m0_type_it_news_list = new m0_type_it_news_details[1];
                m0_type_it_news_details del_typeNews = new m0_type_it_news_details();
                del_typeNews.m0_news_idx = int.Parse(ViewState["_DELETE_M0NEWSIDX"].ToString());
                _data_it_news.m0_type_it_news_list[0] = del_typeNews;
                _data_it_news = callServiceITNews(_urlSetdelete_typeNews, _data_it_news);

                if (_data_it_news.return_code == "0")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);
                }

                else

                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);
                }

                SELECT_MASTER_TYPENEWS();

                SETFOCUS_ONTOP.Focus();

                break;

            case "deleteNews":


                ViewState["_DELETE_U0NEWSIDX"] = int.Parse(cmd_arg);

                _data_it_news.u0_it_news_list = new u0_it_news_details[1];
                u0_it_news_details delNews = new u0_it_news_details();

                delNews.u0_news_idx = int.Parse(ViewState["_DELETE_U0NEWSIDX"].ToString());

                _data_it_news.u0_it_news_list[0] = delNews;


                _data_it_news = callServiceITNews(_urlSetdeleteNews, _data_it_news);

                if (_data_it_news.return_code == "0")
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);

                }

                else

                {


                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);


                }

                SELECT_IT_NEWS();
                SETFOCUS_ONTOP.Focus();

                break;

            case "save_news":

                string pathfilepic = ConfigurationManager.AppSettings["pathfile_itnews"];
                string pathfile_sentmail = Server.MapPath(pathfilepic);


                _data_it_news.u0_it_news_list = new u0_it_news_details[1];
                u0_it_news_details insert_News = new u0_it_news_details();

                insert_News.title_it_news = txttitleNews.Text;
                insert_News.m0_news_idx = int.Parse(ddlNametypeNews.SelectedValue);
                insert_News.details_news = txtdetailsNews.Text;
                insert_News.status_news = int.Parse(ddlStatusNews.SelectedValue);
                //  insert_News.name_type_news = ddlStatusNews.SelectedItem.Text;

                if (chk_sameAddress_present.Checked)
                {
                    insert_News.sentmail = "1";
                }
                else
                {
                    insert_News.sentmail = "0";
                }

                _data_it_news.u0_it_news_list[0] = insert_News;

                if (uploadpicture.HasFile)
                {
                    insert_News.hasfile = "1";
                    insert_News.pathfilepic = pathfile_sentmail;
                }

                else
                {
                    insert_News.hasfile = "0";
                    string filePath_default = Server.MapPath(pathfilepic + "default" + "/" + "default" + ".jpg");
                    insert_News.pathfilepic = filePath_default;
                }

               
                _data_it_news = callServiceITNews(_urlSetITNews, _data_it_news);

                if (_data_it_news.u0_it_news_list[0].u0_news_idx.ToString() != null)
                {
                    ViewState["_idpicnews"] = _data_it_news.u0_it_news_list[0].u0_news_idx.ToString();

                    if (uploadpicture.HasFile)
                    {

                        string getPathfile = ConfigurationManager.AppSettings["pathfile_itnews"];
                        string idpicture = ViewState["_idpicnews"].ToString();//_returndocumentcode;
                        string fileName_upload = idpicture;
                        string filePath_upload = Server.MapPath(getPathfile + idpicture);

                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }
                        string extension = Path.GetExtension(uploadpicture.FileName);

                        uploadpicture.SaveAs(Server.MapPath(getPathfile + idpicture) + "\\" + fileName_upload + extension);


                    }

                    if (_data_it_news.return_code == "0")
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);
                        ClearInputData(panelInsertNews);
                        SELECT_IT_NEWS();
                        ddlStatusNews.SelectedValue = "1";
                        chk_sameAddress_present.Checked = false;
                    }

                    else

                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);

                    }

                }

                SELECT_IT_NEWSList();

                break;

            case "cmdShowBoxTypeNews":

                int view = int.Parse(cmd_arg);
                ddlNametypeNews.Items.Clear();
                linkBtnTrigger(btnsavenews);
                panel_Insert_TYpeNewsIT.Visible = false;
                btnShowinsertType.Visible = false;
                panelInsertNews.Visible = false;
                btnShowBoxNews.Visible = false;
                btnBack.Visible = false;
                btncancelNews.Visible = false;
                gvtypeNews.EditIndex = -1;
                gvNewsITlist.EditIndex = -1;

                switch (view)
                {
                    case 0: //Type News
                        panel_Insert_TYpeNewsIT.Visible = true;
                        btnBack.Visible = true;
                        ddlStatusTypeNews.SelectedValue = "1";
                        SELECT_MASTER_TYPENEWS();
                        break;

                    case 1:// News
                        SELECT_MASTER_TYPENEWS_ON_DROPDOWNLIST();
                        panelInsertNews.Visible = true;
                        btncancelNews.Visible = true;
                        ddlStatusNews.SelectedValue = "1";
                        SELECT_IT_NEWS();
                        break;
                }


                break;

            case "btnback":
                int viewback = int.Parse(cmd_arg);

                panel_Insert_TYpeNewsIT.Visible = false;
                btnShowinsertType.Visible = false;
                panelInsertNews.Visible = false;
                btnShowBoxNews.Visible = false;
                btnBack.Visible = false;
                btncancelNews.Visible = false;

                switch (viewback)
                {
                    case 0 :
                        btnShowinsertType.Visible = true;
                        ClearInputData(panel_Insert_TYpeNewsIT);

                        break;

                    case 1:
                        btnShowBoxNews.Visible = true;
                        break;
                }

                SETFOCUS_ONTOP.Focus();

                break;
        }

    }

    #endregion


    #region call service
    protected data_it_news callServiceITNews(string _cmdUrl, data_it_news _data_it_news)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_it_news);
        // litDebug.Text = _localJson;

        // call services
        // _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_it_news = (data_it_news)_funcTool.convertJsonToObject(typeof(data_it_news), _localJson);

        return _data_it_news;
    }

    #endregion

    protected void txtChange(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;

        switch (txtName.ID)
        {
            case "txtNameTYpeNews":

                Check_TYPENEWS_Duplicate(txtNameTYpeNews.Text, lbltxtShowTypeNews);

                break;
        }

    }


    private void ClearInputData(Panel namepanel)
    {

        foreach (Control cntrl in namepanel.Controls)
        {
            if (cntrl is TextBox)
            {
                ((TextBox)cntrl).Text = "";

            }
            else if (cntrl is DropDownList)
            {
                ((DropDownList)cntrl).SelectedValue = "0";

            }
            else if (cntrl is Label)
            {
                ((Label)cntrl).Text = "";

            }
        }

    }
    #region linkBtnTrigger

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger = new PostBackTrigger();
        trigger.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger);
    }

    #endregion reuse

}