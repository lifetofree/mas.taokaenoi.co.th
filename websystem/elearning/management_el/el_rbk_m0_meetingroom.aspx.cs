﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_rbk_m0_meetingroom : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
    string _Folder_meetingroom_image = "meetingroom_image";
    string _PathFileimage = ConfigurationSettings.AppSettings["path_flieimage_elearning"];

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_m0_meeting = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_meeting"];
    static string _urlSetInsel_m0_meeting = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_m0_meeting"];
    static string _urlSetUpdel_m0_meeting = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_m0_meeting"];
    static string _urlDelel_m0_meeting = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_m0_meeting"];
    static string _urlGetErrorel_m0_meeting = _serviceUrl + ConfigurationManager.AppSettings["urlGetErrorel_m0_meeting"];
    static string _urlGetRbkm0Place = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Place"];

    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
        linkBtnTrigger(btnToInsert);
        linkBtnTrigger(btnFilter);

        //linkBtnTrigger(lbCmdUpdate);
       // linkBtnTrigger(lbCmdCancel);

        linkBtnTrigger(btnSaveInsert);
        linkBtnTrigger(btnCancel);
        gridViewTrigger(GvMaster);

    }

    #region selected   
    protected void actionIndex()
    {
        pnlsearch.Visible = true;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_m0_meeting_action = new m0_meeting[1];

        m0_meeting obj = new m0_meeting();

        obj.m0_meeting_idx = 0;
        obj.filter_keyword = txtFilterKeyword.Text;

        data_elearning.el_m0_meeting_action[0] = obj;
        //litDebug.Text = _funcTool.convertObjectToXml(data_elearning);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_elearning));
        //litDebug.Text = _func_dmu.zJson(_urlGetel_m0_meeting, data_elearning);

        data_elearning = _func_dmu.zCallServicePostNetwork(_urlGetel_m0_meeting, data_elearning);

        setGridData(GvMaster, data_elearning.el_m0_meeting_action);

    }
    #endregion selected 
    

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _m0_meeting_idx;
        string _txtmeeting_name, _txtmeeting_code, _txtmeeting_remark;
        int _cemp_idx;

        m0_meeting objM0_ProductType = new m0_meeting();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                DropDownList ddlplace_idx_ref = (DropDownList)ViewInsert.FindControl("ddlplace_idx_ref");
                getPlace(ddlplace_idx_ref, 0);
                break;
            case "btnEdit":
                MvMaster.SetActiveView(ViewEdit);
                _m0_meeting_idx = int.Parse(cmdArg);
                fv_Edit.ChangeMode(FormViewMode.Edit);
                zShowdataUpdate(_m0_meeting_idx);

                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":
                _txtmeeting_remark = ((TextBox)ViewInsert.FindControl("txtmeeting_remark")).Text.Trim();
                DropDownList _ddlmeeting_status = (DropDownList)ViewInsert.FindControl("ddlmeeting_status");
                DropDownList ddlplace_idx_ref_ins = (DropDownList)ViewInsert.FindControl("ddlplace_idx_ref");
                DropDownList ddlpositon_status_ins = (DropDownList)ViewInsert.FindControl("ddlpositon_status");
                FileUpload fldmeeting_file_name_ins = (FileUpload)ViewInsert.FindControl("fldmeeting_file_name");
                TextBox txtmeeting_date_from = (TextBox)ViewInsert.FindControl("txtmeeting_date_from");
                TextBox txtmeeting_date_to = (TextBox)ViewInsert.FindControl("txtmeeting_date_to");
                TextBox txtfile_duration = (TextBox)ViewInsert.FindControl("txtfile_duration");
                _cemp_idx = emp_idx;
                int icode = 0;
                
                if(checkCodeError(0
                                  , _func_dmu.zStringToInt(ddlplace_idx_ref_ins.SelectedValue)
                                  , _func_dmu.zStringToInt(ddlpositon_status_ins.SelectedValue)
                                  ) == true)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('สถานที่,ตำแหน่งที่ต้องการแสดง นี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                    return;
                }
                m0_meeting obj = new m0_meeting();
                _data_elearning.el_m0_meeting_action = new m0_meeting[1];
                obj.m0_meeting_idx = 0;//_type_idx; 
                obj.meeting_remark = _txtmeeting_remark;
                obj.place_idx_ref = _func_dmu.zStringToInt(ddlplace_idx_ref_ins.SelectedValue);
                obj.positon_status = _func_dmu.zStringToInt(ddlpositon_status_ins.SelectedValue);
                obj.meeting_status = _func_dmu.zStringToInt(_ddlmeeting_status.SelectedValue);
                obj.meeting_file_name = zUploadfileImage(fldmeeting_file_name_ins, ddlplace_idx_ref_ins.SelectedValue+"_"+ ddlpositon_status_ins.SelectedValue);
                obj.file_flag = _func_dmu.zStringToInt(hddf_file_flag.Value);
                obj.file_duration = _func_dmu.zStringToDecimal(txtfile_duration.Text);
                obj.meeting_created_by = _cemp_idx;
                obj.meeting_date_from = _func_dmu.zDateToDB(txtmeeting_date_from.Text);
                obj.meeting_date_to = _func_dmu.zDateToDB(txtmeeting_date_to.Text);
                _data_elearning.el_m0_meeting_action[0] = obj;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                
                _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_m0_meeting, _data_elearning);

                if (_data_elearning.return_code == 0)
                {
                    actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }

                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnDelete":

                _m0_meeting_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_elearning.el_m0_meeting_action = new m0_meeting[1];
                objM0_ProductType.m0_meeting_idx = _m0_meeting_idx;
               // objM0_ProductType.CEmpIDX = _cemp_idx;

                _data_elearning.el_m0_meeting_action[0] = objM0_ProductType;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

                _data_elearning = _func_dmu.zCallServicePostNetwork(_urlDelel_m0_meeting, _data_elearning);


                if (_data_elearning.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }

                break; 
            case "btnFilter":
                actionIndex();
                break;
            case "btnupdate":
                zsaveupdate();
                break;


        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    pnlsearch.Visible = false;
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                      

                    }
                    /*
                    Label lbfile_flag = (Label)e.Row.Cells[0].FindControl("lbfile_flag");
                    Panel pnl_img = (Panel)e.Row.Cells[4].FindControl("pnl_img");
                    Panel pnl_video = (Panel)e.Row.Cells[4].FindControl("pnl_video");
                    pnl_img.Visible = false;
                    pnl_video.Visible = false;
                    if (_func_dmu.zStringToInt(lbfile_flag.Text) == 0)
                    {
                        pnl_img.Visible = true;
                    }
                    else
                    {
                        pnl_video.Visible = true;
                    }
                    */
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                
                break;
        }
    }
    protected void zsaveupdate()
    {
        int m0_meeting_idx_update = Convert.ToInt32(((TextBox)fv_Edit.FindControl("txtm0_meeting_idxUpdate")).Text);
        var txtmeeting_remark = (TextBox)fv_Edit.FindControl("txtmeeting_remarkUpdate");
        var ddlmeeting_status = (DropDownList)fv_Edit.FindControl("ddlmeeting_statusUpdate");
        DropDownList ddlplace_idx_ref = (DropDownList)fv_Edit.FindControl("ddlplace_idx_refUpdate");
        DropDownList ddlpositon_status = (DropDownList)fv_Edit.FindControl("ddlpositon_statusUpdate");
        FileUpload fldmeeting_file_nameUpdate = (FileUpload)fv_Edit.FindControl("fldmeeting_file_nameUpdate");
        TextBox txtmeeting_file_nameUpdate = (TextBox)fv_Edit.FindControl("txtmeeting_file_nameUpdate");
        CheckBox cb_delimage = (CheckBox)fv_Edit.FindControl("cb_delimage");
        TextBox txtmeeting_date_from = (TextBox)fv_Edit.FindControl("txtmeeting_date_fromUpdate");
        TextBox txtmeeting_date_to = (TextBox)fv_Edit.FindControl("txtmeeting_date_toUpdate");
        TextBox txtfile_flagUpdate = (TextBox)fv_Edit.FindControl("txtfile_flagUpdate");
        TextBox txtfile_durationUpdate = (TextBox)fv_Edit.FindControl("txtfile_durationUpdate");

        int icode = 0;

        if (checkCodeError(m0_meeting_idx_update
                          , _func_dmu.zStringToInt(ddlplace_idx_ref.SelectedValue)
                          , _func_dmu.zStringToInt(ddlpositon_status.SelectedValue)
                          ) == true)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('สถานที่,ตำแหน่งที่ต้องการแสดง นี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
            return;
        }
        
        _data_elearning.el_m0_meeting_action = new m0_meeting[1];
        m0_meeting obj = new m0_meeting();

        obj.m0_meeting_idx = m0_meeting_idx_update;
        obj.meeting_remark = txtmeeting_remark.Text;
        obj.place_idx_ref = _func_dmu.zStringToInt(ddlplace_idx_ref.SelectedValue);
        obj.positon_status = _func_dmu.zStringToInt(ddlpositon_status.SelectedValue);
        obj.meeting_status = int.Parse(ddlmeeting_status.SelectedValue);
        obj.meeting_updated_by = emp_idx;
        obj.meeting_date_from = _func_dmu.zDateToDB(txtmeeting_date_from.Text);
        obj.meeting_date_to = _func_dmu.zDateToDB(txtmeeting_date_to.Text);
        obj.file_duration = _func_dmu.zStringToDecimal(txtfile_durationUpdate.Text);
        if (cb_delimage.Checked == true)
        {
            zDelImages(txtmeeting_file_nameUpdate.Text);
        }
        else
        {
            if (fldmeeting_file_nameUpdate.HasFile == false)
            {
                obj.meeting_file_name = txtmeeting_file_nameUpdate.Text;
                obj.file_flag = _func_dmu.zStringToInt(txtfile_flagUpdate.Text);
            }
            else
            {
                zDelImages(txtmeeting_file_nameUpdate.Text);
                obj.meeting_file_name = zUploadfileImage(fldmeeting_file_nameUpdate, ddlplace_idx_ref.SelectedValue + "_" + ddlpositon_status.SelectedValue);
                obj.file_flag = _func_dmu.zStringToInt(hddf_file_flag.Value);
            }
        }

        _data_elearning.el_m0_meeting_action[0] = obj;

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetUpdel_m0_meeting, _data_elearning);

        if (_data_elearning.return_code == 0)
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
        else
        {
            setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
        }

    }
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        //switch (fvName.ID)
        //{



        //}
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }
    
    public Boolean checkCodeError(int _m0_meeting_idx, int _place_idx_ref, int _positon_status)
    {
        Boolean error = false;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_m0_meeting_action = new m0_meeting[1];

        m0_meeting obj = new m0_meeting();

        obj.m0_meeting_idx = _m0_meeting_idx;
        obj.place_idx_ref = _place_idx_ref;
        obj.positon_status = _positon_status;
        data_elearning.el_m0_meeting_action[0] = obj;
        data_elearning = _func_dmu.zCallServicePostNetwork(_urlGetErrorel_m0_meeting, data_elearning);
        if(data_elearning.el_m0_meeting_action != null)
        {
            
            error = true;

        }
        return error;
    }

    #endregion reuse

    private string zUploadfileImage(FileUpload afileUpload, string docno)
    {
        string filenamedata = "";
        if (afileUpload.HasFile == false)
        {

        }
        else
        {
            string _Path = _PathFile + _Folder_meetingroom_image ;// + "\\" + item.ToString();
            string mapPath = HttpContext.Current.Server.MapPath(_Path);
            string phyicalFilePath = string.Empty;
            string _itemExtension = "";
            string _itemNameNew = "";
            string datetimeNow = "";
            String filename = string.Empty;
            if (Directory.Exists(mapPath) == false)
            {
                Directory.CreateDirectory(mapPath);
            }
            hddf_file_flag.Value = "0";
            try
            {

                filename = afileUpload.FileName;
                _itemExtension = Path.GetExtension(afileUpload.PostedFile.FileName);
                //datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                hddf_file_flag.Value = (setTypeFile(_itemExtension.ToLower())).ToString();
                _itemNameNew = docno + _itemExtension.ToLower();
                phyicalFilePath = mapPath + "\\" + _itemNameNew;
                afileUpload.SaveAs(phyicalFilePath);
                filenamedata = _itemNameNew;

            }
            catch (Exception)
            {
                //  Response.Write("{success:false}");

            }
        }

        return filenamedata;

    }
    private int setTypeFile(string _TypeFile)
    {
        int i = 0;
        if (_TypeFile.ToLower() == ".mp4")
        {
            i = 1;
        }
        else if (_TypeFile.ToLower() == ".mp3")
        {
            i = 1;
        }
        else if (_TypeFile.ToLower() == ".ogg")
        {
            i = 1;
        }
        else if (_TypeFile.ToLower() == ".wmv")
        {
            i = 1;
        }
        return i;
    }
    private void zDelImages( string Images)
    {
        if ((Images != ""))
        {
            string _Path = _PathFile + _Folder_meetingroom_image + "\\" + Images;
            try
            {
                File.Delete(Server.MapPath(_Path));
            }
            catch { }
        }

    }
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    protected void gridViewTrigger(GridView linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    public string getImgUrl(string AImage = "")
    {
        string sPathImage = "";
        // string host = HttpContext.Current.Request.Url.Host;
        // host = _func_dmu.zHostname(host);
        // host = "http://" + host;
        if (AImage == "")
        {
            sPathImage = _PathFileimage + _func_dmu._IconnoImage;
        }
        else
        {
            sPathImage = _PathFile + _Folder_meetingroom_image + "/" + AImage;
        }
        //sPathImage = Server.MapPath(sPathImage);
      sPathImage = ResolveUrl(sPathImage);

        return sPathImage;
    }

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        trainingLoolup obj_trainingLoolup = new trainingLoolup();
        if (sender is FormView)
        {
            var FvName = (FormView)sender;
            if (FvName.ID == "fv_Edit")
            {
                LinkButton lbCmdUpdate = (LinkButton)FvName.FindControl("lbCmdUpdate");
                LinkButton lbCmdCancel = (LinkButton)FvName.FindControl("lbCmdCancel");
                linkBtnTrigger(lbCmdUpdate);
                linkBtnTrigger(lbCmdCancel);
            }

        }
    }
    private void zShowdataUpdate(int id)
    {
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_m0_meeting_action = new m0_meeting[1];

        m0_meeting obj = new m0_meeting();
        obj.m0_meeting_idx = id;
        data_elearning.el_m0_meeting_action[0] = obj;
        data_elearning = _func_dmu.zCallServicePostNetwork(_urlGetel_m0_meeting, data_elearning);
        _func_dmu.zSetFormViewData(fv_Edit, data_elearning.el_m0_meeting_action);
        if (data_elearning.el_m0_meeting_action != null)
        {
            DropDownList ddlplace_idx_ref = (DropDownList)fv_Edit.FindControl("ddlplace_idx_refUpdate");
            getPlace(ddlplace_idx_ref, data_elearning.el_m0_meeting_action[0].place_idx_ref);
        }
        else
        {
            getPlace(ddlplace_idx_ref, 0);
        }

    }
    protected void getPlace(DropDownList ddlName, int _place_idx)
    {

        data_roombooking data_m0_place_detail = new data_roombooking();
        rbk_m0_place_detail m0_place_detail = new rbk_m0_place_detail();
        data_m0_place_detail.rbk_m0_place_list = new rbk_m0_place_detail[1];

        m0_place_detail.condition = 1;

        data_m0_place_detail.rbk_m0_place_list[0] = m0_place_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_place_detail = callServicePostRoomBooking(_urlGetRbkm0Place, data_m0_place_detail);

        setDdlData(ddlName, data_m0_place_detail.rbk_m0_place_list, "place_name", "place_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานที่ ---", "0"));
        ddlName.SelectedValue = _place_idx.ToString();

    }
    protected data_roombooking callServicePostRoomBooking(string _cmdUrl, data_roombooking _data_roombooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_roombooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_roombooking = (data_roombooking)_funcTool.convertJsonToObject(typeof(data_roombooking), _localJson);


        return _data_roombooking;
    }


    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    private void Page_Init(object sender, EventArgs e)
    {
        ClearApplicationCache();
    }
    #region reuse
    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }
    #endregion

}