﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{
   public void ProcessRequest(HttpContext context)
   {
      string callback = context.Request.QueryString["callback"];
      string keyword = String.Empty;
      keyword = context.Request.QueryString["keyword"];
      string json = this.getEmployee(keyword);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string getEmployee(string keyword)
   {
      List<object> emp = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_centralized"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            int statusOnline = 1;
            int typeEmp = 2; //monthly
            if (keyword != String.Empty)
            {
               cmd.CommandText = "SELECT TOP 1000 EmpIDX, EmpCode, FullNameTH FROM [Centralized].[dbo].[ViewEmpProfile] WHERE (EmpCode LIKE @keyword + '%' OR FullNameTH LIKE '%' +  @keyword + '%') AND EmpType = " + typeEmp + " AND EmpStatus = " + statusOnline + " ORDER BY EmpCode ASC";
               cmd.Parameters.AddWithValue("@keyword", keyword);
            }
            cmd.Connection = conn;
            conn.Open();
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
               while (sdr.Read())
               {
                  emp.Add(new
                  {
                     empIdx = sdr["EmpIDX"],
                     empCode = sdr["EmpCode"],
                     fullNameTH = sdr["FullNameTH"]
                  });
               }
            }
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emp));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}
