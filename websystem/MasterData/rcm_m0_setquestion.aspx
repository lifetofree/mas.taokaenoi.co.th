﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="rcm_m0_setquestion.aspx.cs" Inherits="websystem_MasterData_rcm_m0_setquestion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#imgupload_q").MultiFile();
            }
        })
    </script>
    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#imgupload_1").MultiFile();
            }
        })
    </script>
    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#imgupload_2").MultiFile();
            }
        })
    </script>
    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#imgupload_3").MultiFile();
            }
        })
    </script>
    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#imgupload_4").MultiFile();
            }
        })
    </script>

    <style>
        .img-banner {
            width: auto;
            max-height: 300px;
            margin: 0 auto 7px;
        }

        .h-title {
            font-size: 24px;
            font-weight: 500;
            font-family: inherit;
            font-weight: 500;
            color: inherit;
        }

        .row_v2 {
            margin-right: -15px;
            margin-left: -15px;
        }

            .row_v2:after {
                clear: both;
            }

            .row_v2:after, .row_v2:before {
                display: table;
                content: " ";
            }

        .control-label {
            line-height: 30px;
        }

        .mt-7 {
            margin-top: 7px;
        }

        .p-0 {
            padding: 0px;
        }
    </style>
    <style type="text/css">
        .bg-logo {
            background-image: url('../../masterpage/images/hr/background_register_2.png');
            background-size: 100% 100%;
            width: 100%;
            height: 100%;
            text-align: center;
        }

        .imghead {
            margin: 0 auto;
            max-height: 500px;
            width: auto;
        }

        .btn-backtomain {
            padding: 5px 40px;
            display: table;
            background: #FCB326;
            margin: 0 auto;
            color: #fff;
            border-radius: 8px;
            font-size: 20px;
            cursor: pointer;
            box-shadow: 0px 4px #F99B27;
        }

        .img-h500 {
            max-height: 500px;
            width: auto;
            max-width: 100%;
        }

        .p-0 {
            padding: 0;
        }

        .rule-exam {
            text-indent: 1.5em;
            margin-top: 0;
            color: #808080;
        }

        .box-type-topic {
            background-color: #5CA2D0;
            border-radius: 3px;
            padding: 7px;
            border: 1px solid #DDDDDD;
            color: #fff;
        }

        .answer_choice input[type="radio"] {
            display: none;
        }

        .answer_choice label {
            cursor: pointer;
            width: 100%;
            border: 1px solid #ddd;
            padding: 3px;
            line-height: 1rem;
            border-radius: 4px;
            height: 40px;
            overflow: hidden;
            font-weight: 100;
            font-size: 14px;
        }

        /*      .answer_choice label:hover {
                border: 1px solid #ffa06a;
                background-color: #ffa06a0d;
            }*/

        .answer_choice input[type="radio"]:checked + label {
            /*  border: 1px solid #35c695;
            background-color: #35c6950d;*/
            border: 1px solid #e0113b;
            background-color: #e0113b0d;
        }

        .box-choice:nth-child(odd) {
            padding-right: 7px;
        }

        .box-choice:nth-child(even) {
            padding-left: 7px;
        }

        .panel-exam-main {
            border-color: #EC2839;
        }

            .panel-exam-main > .panel-heading {
                background-color: #EC2839;
                color: #fff;
            }

        textarea {
            width: 100%;
            margin-top: 7px;
            resize: none;
        }

        :focus {
            outline: unset;
        }

        .check_true input[type="radio"]:checked + label {
            border: 1px solid #35c695 !important;
            background-color: #35c6950d !important;
        }

        .check_true label:before {
            /*     content: "✓";
            position: absolute;
            bottom: 0;
            right: 15px;
            font-size: 30px;
            font-weight: bold;
            color: #1fab1d;
            line-height: initial;*/
        }

        /*    html {
            scroll-behavior: smooth;
        }*/
        .box-qeustion {
            border: 1px solid #ddd;
            border-radius: 3px;
            margin-top: 7px;
            padding-top: 15px;
            padding-bottom: 15px;
        }

        #mtoh {
            display: none;
            position: fixed;
            bottom: 20px;
            right: 10px;
            z-index: 99;
        }

        .txt_score_comment {
            display: inline-block;
            width: 100px;
        }

        .auto-scroll {
            overflow: auto;
        }

        .pageCustom_chr table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .pageCustom_chr table > thead > tr > td,
        .table > tbody > tr > td,
        .pageCustom_chr table > tfoot > tr > td {
            font-size: small;
        }

        @media (max-width: 772px) {
            .box-choice {
                padding: 0 15px !important;
            }

            .auto-scroll {
                padding: 0;
                margin: 0 15px;
            }

            .sm-row {
                margin: 0 -15px;
            }

            .xs-mt-7 {
                margin-top: 7px;
            }
        }
    </style>
    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                <%--lemonchiffon--%>
                <ul class="nav navbar-nav navbar-left" runat="server">
                    <li id="_divMenuLiToViewIndex" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivIndex" runat="server"
                            CommandName="_divMenuBtnToDivIndex"
                            OnCommand="btnCommand" Text="ข้อมูลชุดคำถาม" />
                    </li>


                    <li id="_divMenuLiToViewAdd" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAdd" runat="server"
                            CommandName="_divMenuBtnToDivAdd"
                            OnCommand="btnCommand" Text="เพิ่มชุดคำถาม" />
                    </li>
                    <li id="_divMenuLiToViewAddScore" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAddScore" runat="server"
                            CommandName="_divMenuBtnToDivAddScore"
                            OnCommand="btnCommand" Text="ตรวจคะแนน" />
                    </li>
                    <li id="_divMenuLiToViewManual" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivManual" runat="server"
                            CommandName="_divMenuBtnToDivManual"
                            OnCommand="btnCommand" Text="คู่มือการใช้งาน" />
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <asp:UpdatePanel ID="update_viewindex" runat="server" style="margin: 0 -10px;">
                <ContentTemplate>

                    <div class="col-md-12">
                        <asp:Image ID="imghead" runat="server" class="img-responsive img-fluid img-banner" ImageUrl="~/images/recruit/banner_register_6.png" />
                    </div>

                    <div class="col-md-12">

                        <div class="alert-message alert-message-success">
                            <blockquote class="danger" style="font-size: small; background-color: #ff9999;">
                                <div class="h-title">ประเภทชุดคำถาม</div>
                            </blockquote>

                        </div>

                        <div id="SETBoxAllSearch" runat="server" visible="true">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาประเภทชุดคำถาม</strong></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <asp:Label ID="Label2d0" runat="server" Text="ระบุประเภทคำถาม" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddltypequest_search" ValidationGroup="btnsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกประเภทคำถาม"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ValidationGroup="btnsearch" runat="server" Display="None"
                                                    ControlToValidate="ddltypequest_search" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกประเภทคำถาม"
                                                    ValidationExpression="กรุณาเลือกประเภทคำถาม" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator17" Width="160" />

                                            </div>
                                        </div>

                                        <div id="divchoosetype_search" runat="server" visible="false">
                                            <div class="form-group">
                                                <asp:Label ID="Label59" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlOrg_search" ValidationGroup="btnsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="0">กรุณาเลือกองค์กร...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%--        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="btnsearch" runat="server" Display="None"
                                                        ControlToValidate="ddlOrg_search" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกองค์กร"
                                                        ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />--%>
                                                </div>
                                                <asp:Label ID="Label4" runat="server" Text="ฝ่าย" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlDep_search" ValidationGroup="btnsearch" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="btnsearch" runat="server" Display="None"
                                                        ControlToValidate="ddlDep_search" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกฝ่าย"
                                                        ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />--%>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label19" runat="server" Text="แผนก" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlSec_search" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกแผนก...</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                                <asp:Label ID="Label1" runat="server" Text="ตำแหน่ง" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlPos_search" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="0">กรุณาเลือกตำแหน่ง...</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-5 col-sm-offset-2">
                                                <asp:LinkButton ID="btnsearch" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                                <asp:LinkButton ID="btnRefresh" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="btnRefresh" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <asp:GridView ID="GvTypeTopic" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-md-12"
                            HeaderStyle-Height="40px"
                            AllowPaging="false"
                            DataKeyNames="m0_tqidx"
                            OnRowDataBound="Master_RowDataBound">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>


                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" ItemStyle-CssClass="text-left">
                                    <ItemTemplate>


                                        <div class="alert-message alert-message-success">
                                            <blockquote class="danger" style="background-color: #9fdfdf;">
                                                <h4><b>
                                                    <i class="fa fa-book"></i>&nbsp;
                                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Text='<%# Eval("type_quest") %>'></asp:Label>

                                                </b></h4>


                                            </blockquote>
                                        </div>

                                        <asp:Label ID="lbltypidx" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("m0_tqidx") %>'></asp:Label>

                                        <asp:GridView ID="GvTopic" runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-md-12"
                                            HeaderStyle-CssClass="default small"
                                            HeaderStyle-Height="40px"
                                            AllowPaging="false"
                                            DataKeyNames="m0_toidx"
                                            OnRowDataBound="Master_RowDataBound">

                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <asp:TemplateField HeaderText="ข้อมูลแบบทดสอบ" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" Visible="true" ItemStyle-Font-Size="Small">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblm0_toidx" runat="server" Visible="false" Text='<%# Eval("m0_toidx") %>' />
                                                        <asp:Label ID="Label17" Visible="false" runat="server" Text='<%# Eval("orgidx") %>' />
                                                        <asp:Label ID="Label21" Visible="false" runat="server" Text='<%# Eval("rdeptidx") %>' />
                                                        <asp:Label ID="Label24" Visible="false" runat="server" Text='<%# Eval("rsecidx") %>' />
                                                        <asp:Label ID="Label25" Visible="false" runat="server" Text='<%# Eval("rposidx") %>' />
                                                        <%--   <asp:Label ID="Label15" runat="server" Text='<%# Eval("OrgNameTH") %>' />--%>


                                                        <strong>
                                                            <asp:Label ID="Label60" runat="server"> องค์กร: </asp:Label></strong>
                                                        <asp:Literal ID="lbPosName" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                        <br />
                                                        <strong>
                                                            <asp:Label ID="Label61" runat="server"> ฝ่าย: </asp:Label></strong>
                                                        <asp:Label ID="lbdep" runat="server" Text='<%# Eval("DeptNameTH") %>'></asp:Label>
                                                        <br />
                                                        <strong>
                                                            <asp:Label ID="lbdat" runat="server"> แผนก: </asp:Label></strong>
                                                        <asp:Label ID="lblocate" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:Label>
                                                        <br />
                                                        <strong>
                                                            <asp:Label ID="Label31" runat="server"> ตำแหน่ง: </asp:Label></strong>
                                                        <asp:Label ID="Label32" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:Label>

                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ตำแหน่ง" Visible="false" ItemStyle-CssClass="text-center" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpname" runat="server" Text='<%# Eval("P_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ชุดคำถาม" ItemStyle-CssClass="text-center" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblm0_toidxper" Visible="false" runat="server" Text='<%# Eval("m0_toidx") %>'></asp:Label>
                                                        <asp:Label ID="lbpricenotshow" runat="server" Text='<%# Eval("topic_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--   <asp:TemplateField HeaderText="ข้อมูลผู้รับผิดชอบ" ItemStyle-CssClass="text-center" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>

                                                      <asp:GridView ID="GvPerson" runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover table-responsive col-md-12"
                                                            HeaderStyle-CssClass="info small"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="false"
                                                            DataKeyNames="P_EmpIDX"
                                                            OnRowDataBound="Master_RowDataBound">

                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="ผู้รับผิดชอบ" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" Visible="true" ItemStyle-Font-Size="Small">

                                                                    <ItemTemplate>

                                                                        <strong>
                                                                            <asp:Label ID="Label60" runat="server"> องค์กร: </asp:Label></strong>
                                                                        <asp:Literal ID="lbPosName" runat="server" Text='<%# Eval("org_name_th") %>' />
                                                                        <br />
                                                                        <strong>
                                                                            <asp:Label ID="Label61" runat="server"> ฝ่าย: </asp:Label></strong>
                                                                        <asp:Label ID="lbdes" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                                                        <br />
                                                                        <strong>
                                                                            <asp:Label ID="lbdat" runat="server"> แผนก: </asp:Label></strong>
                                                                        <asp:Label ID="lblocate" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                                        <br />
                                                                        <strong>
                                                                            <asp:Label ID="Label31" runat="server"> ตำแหน่ง: </asp:Label></strong>
                                                                        <asp:Label ID="Label32" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                                        <br />
                                                                        <strong>
                                                                            <asp:Label ID="Label15" runat="server"> ชื่อ-นามสกุล: </asp:Label></strong>
                                                                        <asp:Label ID="Label33" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>


                                                            </Columns>
                                                        </asp:GridView>

                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>

                                                <asp:TemplateField HeaderText="ดูรายละเอียด" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnview" CssClass="btn btn-primary btn-sm" runat="server" CommandName="CmdViewDetail" OnCommand="btnCommand" data-toggle="tooltip" title="View" CommandArgument='<%# Eval("m0_toidx")+ ";" + Eval("PIDX") %>'><i class="fa fa-share-square"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnAddSub" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdAddDetail" OnCommand="btnCommand" data-toggle="tooltip" title="Add" CommandArgument='<%#  Eval("m0_tqidx") + ";" + Eval("m0_toidx") + ";" + Eval("orgidx") + ";" + Eval("rdeptidx")+ ";" + Eval("rsecidx")+ ";" + Eval("rposidx")%>'><i class="fa fa-plus-square"></i></asp:LinkButton>

                                                    </ItemTemplate>

                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>


                                            </Columns>
                                        </asp:GridView>


                                    </ItemTemplate>
                                </asp:TemplateField>


                            </Columns>
                        </asp:GridView>


                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewAdd" runat="server">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-md-12">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Topic Question</strong></h3>
                    </div>
                    <asp:FormView ID="fv_insert" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:UpdatePanel ID="updatepic" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="รายการที่เพิ่ม" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlchooseqs" ValidationGroup="SaveTopic" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกรายการที่เพิ่ม"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="ชุดคำถาม"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="คำถามย่อย"></asp:ListItem>

                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveTopic" runat="server" Display="None"
                                                        ControlToValidate="ddlchooseqs" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกรายการที่เพิ่ม"
                                                        ValidationExpression="กรุณาเลือกรายการที่เพิ่ม" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                                </div>
                                                <div class="col-sm-4" style="color: transparent;">
                                                    <asp:FileUpload ID="FileUpload4" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="XX-Small"
                                                        ClientIDMode="Static" runat="server"
                                                        CssClass="btn btn-warning btn-xs hidden"
                                                        accept="gif|jpg|png" />
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddataset" />
                                            <asp:AsyncPostBackTrigger ControlID="ddlchooseqs" EventName="SelectedIndexChanged" />

                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="form-group" id="div_typequest" runat="server" visible="false">
                                                <asp:Label ID="Label20" runat="server" Text="ระบุประเภทคำถาม" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddltypequest" ValidationGroup="SaveTopic" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกประเภทคำถาม"></asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                        ControlToValidate="ddltypequest" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกประเภทคำถาม"
                                                        ValidationExpression="กรุณาเลือกประเภทคำถาม" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>

                                            <asp:AsyncPostBackTrigger ControlID="ddltypequest" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>


                                    <div id="divchoosetype" runat="server" visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label59" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlOrg" ValidationGroup="SaveTopic" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                                <%--       <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlOrg" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกองค์กร"
                                                    ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />--%>
                                            </div>
                                            <asp:Label ID="Label4" runat="server" Text="ฝ่าย" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlDep" ValidationGroup="SaveTopic" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                                </asp:DropDownList>
                                                <%--                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlDep" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกฝ่าย"
                                                    ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label19" runat="server" Text="แผนก" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSec" ValidationGroup="SaveTopic" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกแผนก...</asp:ListItem>
                                                </asp:DropDownList>
                                                <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlSec" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกแผนก"
                                                    ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />--%>
                                            </div>
                                            <asp:Label ID="Label1" runat="server" Text="ตำแหน่ง" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlPos" ValidationGroup="SaveTopic" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกตำแหน่ง...</asp:ListItem>
                                                </asp:DropDownList>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlPos" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกตำแหน่ง"
                                                    ValidationExpression="กรุณาเลือกตำแหน่ง" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />--%>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-group" id="div_txttopic" runat="server" visible="false">
                                        <asp:Label ID="Label2" runat="server" Text="ชื่อชุดคำถาม" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txttopic" ValidationGroup="SaveTopic" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveTopic" runat="server" Display="None" ControlToValidate="txttopic" Font-Size="11"
                                                ErrorMessage="กรุณากรอกชื่อชุดคำถาม"
                                                ValidationExpression="กรุณากรอกชื่อชุดคำถาม"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveTopic" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txttopic"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                        </div>
                                    </div>


                                    <asp:Panel ID="panel_quest" runat="server" Visible="false">
                                        <asp:UpdatePanel ID="update_panelquest" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:Label ID="Label10" runat="server" Text="ชุดคำถาม" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddltopic" ValidationGroup="SaveDataSet" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0" Text="กรุณาเลือกชุดคำถาม"></asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                            ControlToValidate="ddltopic" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกชุดคำถาม"
                                                            ValidationExpression="กรุณาเลือกชุดคำถาม" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddltopic" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:Label ID="Label13" runat="server" Text="ประเภทคำตอบ" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddltypeans" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="SaveDataSet" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0" Text="กรุณาเลือกประเภทคำตอบ"></asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                            ControlToValidate="ddltypeans" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกประเภทคำตอบ"
                                                            ValidationExpression="กรุณาเลือกประเภทคำตอบ" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator16" Width="160" />

                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddltypeans" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                        <div class="form-group">
                                            <asp:Label ID="Label23" runat="server" Text="ลำดับ" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtcourse_item" OnTextChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control"
                                                    TextMode="Number" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15"
                                                    ValidationGroup="SaveDataSet" runat="server"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="txtcourse_item"
                                                    Font-Size="1em" ForeColor="Red"
                                                    Display="None"
                                                    CssClass="pull-left"
                                                    ErrorMessage="กรุณากรอกลำดับ"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label11" runat="server" Text="คำถาม" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtquest" ValidationGroup="SaveDataSet" row="3" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SaveDataSet" runat="server" Display="None" ControlToValidate="txtquest" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกคำถาม"
                                                    ValidationExpression="กรุณากรอกคำถาม"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                    ValidationGroup="SaveDataSet" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtquest"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                            </div>
                                            <div class="col-sm-3">


                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div class="form-group">

                                                            <div class="col-md-4">

                                                                <asp:FileUpload ID="imgupload_q" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                    CssClass="btn btn-sm btn-warning " accept="jpg" />
                                                                <p class="help-block"><font size="1pt" color="red">**เฉพาะ jpg</font></p>
                                                            </div>


                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnAdddataset" />
                                                    </Triggers>
                                                </asp:UpdatePanel>


                                            </div>
                                        </div>



                                        <div id="div_choice" runat="server" visible="false">

                                            <div class="form-group">
                                                <asp:Label ID="Label12" runat="server" Text="คำตอบ ก." CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtchoice1" ValidationGroup="SaveDataSet" row="3" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveDataSet" runat="server" Display="None" ControlToValidate="txtchoice1" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกคำตอบ"
                                                        ValidationExpression="กรุณากรอกคำตอบ"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                        ValidationGroup="SaveDataSet" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtchoice1"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />--%>
                                                </div>
                                                <div class="col-sm-3">

                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="form-group">
                                                                <div class="col-md-4">
                                                                    <asp:FileUpload ID="imgupload_1" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                        CssClass="btn btn-sm btn-warning" accept="jpg" />
                                                                    <p class="help-block"><font size="1pt" color="red">**เฉพาะ jpg</font></p>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnAdddataset" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label14" runat="server" Text="คำตอบ ข." CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtchoice2" ValidationGroup="SaveDataSet" row="3" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="SaveDataSet" runat="server" Display="None" ControlToValidate="txtchoice2" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกคำตอบ"
                                                        ValidationExpression="กรุณากรอกคำตอบ"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                        ValidationGroup="SaveDataSet" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtchoice2"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator4" Width="160" />--%>
                                                </div>
                                                <div class="col-sm-3">

                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="form-group">
                                                                <div class="col-md-4">
                                                                    <asp:FileUpload ID="imgupload_2" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                        CssClass="btn btn-sm btn-warning" accept="jpg" />
                                                                    <p class="help-block"><font size="1pt" color="red">**เฉพาะ jpg</font></p>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnAdddataset" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label16" runat="server" Text="คำตอบ ค." CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtchoice3" ValidationGroup="SaveDataSet" row="3" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="SaveDataSet" runat="server" Display="None" ControlToValidate="txtchoice3" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกคำตอบ"
                                                        ValidationExpression="กรุณากรอกคำตอบ"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                        ValidationGroup="SaveDataSet" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtchoice3"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator5" Width="160" />--%>
                                                </div>
                                                <div class="col-sm-3">

                                                    <asp:UpdatePanel ID="updatepanel3" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="form-group">
                                                                <div class="col-md-4">
                                                                    <asp:FileUpload ID="imgupload_3" Font-Size="small" ViewStateMode="enabled" runat="server"
                                                                        CssClass="btn btn-sm btn-warning" accept="jpg" />
                                                                    <p class="help-block"><font size="1pt" color="red">**เฉพาะ jpg</font></p>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnAdddataset" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label18" runat="server" Text="คำตอบ ง." CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtchoice4" ValidationGroup="SaveDataSet" row="3" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="SaveDataSet" runat="server" Display="None" ControlToValidate="txtchoice4" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกคำตอบ"
                                                        ValidationExpression="กรุณากรอกคำตอบ"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                                                        ValidationGroup="SaveDataSet" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtchoice4"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator6" Width="160" />--%>
                                                </div>
                                                <div class="col-sm-3">

                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="form-group">
                                                                <div class="col-md-4">
                                                                    <asp:FileUpload ID="imgupload_4" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                        CssClass="btn btn-sm btn-warning" accept="jpg" />
                                                                    <p class="help-block"><font size="1pt" color="red">**เฉพาะ jpg</font></p>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnAdddataset" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>


                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label22" runat="server" Text="เฉลย." CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddl_answer" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกคำตอบที่ถูกต้อง" />
                                                        <asp:ListItem Value="1" Text="ก" />
                                                        <asp:ListItem Value="2" Text="ข" />
                                                        <asp:ListItem Value="3" Text="ค" />
                                                        <asp:ListItem Value="4" Text="ง" />
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                        ControlToValidate="ddl_answer" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกคำตอบที่ถูกต้อง"
                                                        ValidationExpression="กรุณาเลือกคำตอบที่ถูกต้อง" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="160" />

                                                </div>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-2">
                                                <asp:LinkButton ID="btnAdddataset" CssClass="btn btn-primary btn-sm" runat="server" Text="ADD +" ValidationGroup="SaveDataSet" OnCommand="btnCommand" CommandName="CmdAdddataset"></asp:LinkButton>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-1">
                                            </div>
                                            <div class="col-sm-10">
                                                <asp:UpdatePanel ID="update1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="GvWrite"
                                                            runat="server"
                                                            CssClass="table table-striped table-responsive info"
                                                            GridLines="None"
                                                            OnRowDataBound="Master_RowDataBound"
                                                            Visible="false"
                                                            OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">


                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>

                                                            <Columns>


                                                                <asp:TemplateField HeaderText="ลำดับที่" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lbcourse_item" runat="server" Text='<%# Eval("No_Quest") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="คำถาม" ItemStyle-CssClass="text-center" ItemStyle-Width="20%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lbquest" runat="server" Text='<%# Eval("Question") %>' />

                                                                            <br />
                                                                            <asp:HyperLink runat="server" ID="images_q" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>

                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteListWrite"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                                    </ItemTemplate>

                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                            </Columns>

                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-sm-1">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-1">
                                            </div>
                                            <div class="col-sm-10">
                                                <asp:UpdatePanel ID="update2" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="GvReportAdd"
                                                            runat="server"
                                                            CssClass="table table-striped table-responsive info"
                                                            GridLines="None"
                                                            OnRowDataBound="Master_RowDataBound"
                                                            Visible="false"
                                                            OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">


                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>

                                                            <Columns>

                                                                <asp:TemplateField HeaderText="ลำดับที่" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lbcourse_item" runat="server" Text='<%# Eval("No_Quest") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="คำถาม" ItemStyle-CssClass="text-center" ItemStyle-Width="20%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lbquest" runat="server" Text='<%# Eval("Question") %>' />

                                                                            <br />
                                                                            <asp:HyperLink runat="server" ID="images_q" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>

                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="คำตอบ ก." ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lbchoice1" runat="server" Text='<%# Eval("Choice_a") %>' />

                                                                            <br />
                                                                            <asp:HyperLink runat="server" ID="images_1" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>


                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="คำตอบ ข." ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lbchoice2" runat="server" Text='<%# Eval("Choice_b") %>' />

                                                                            <br />
                                                                            <asp:HyperLink runat="server" ID="images_2" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>


                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="คำตอบ ค." ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lbchoice3" runat="server" Text='<%# Eval("Choice_c") %>' />

                                                                            <br />
                                                                            <asp:HyperLink runat="server" ID="images_3" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>


                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="คำตอบ ง." ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lbchoice4" runat="server" Text='<%# Eval("Choice_d") %>' />

                                                                            <br />
                                                                            <asp:HyperLink runat="server" ID="images_4" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>


                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="เฉลย" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lbchoice_ans" runat="server" Text='<%# Eval("Choice_ans") %>' />
                                                                            <asp:Label ID="lbchoice_ansidx" Visible="false" runat="server" Text='<%# Eval("Choice_ansIDX") %>' />

                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                                    </ItemTemplate>

                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>
                                                            </Columns>

                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-sm-1">
                                            </div>
                                        </div>


                                    </asp:Panel>

                                    <asp:Panel ID="panel_chooseperson" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label26" runat="server" Text="ตำแหน่งเปิดรับสมัคร" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlposition" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlposition" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกตำแหน่งเปิดรับสมัคร"
                                                    ValidationExpression="กรุณาเลือกตำแหน่งเปิดรับสมัคร" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="form-group">
                                            <div class="col-sm-1">
                                            </div>
                                            <div class="col-sm-10">

                                                <b>
                                                    <%--  <h4 style="color: #4d4dff"><i class="glyphicon glyphicon-user"></i>ผู้รับผิดชอบชุดแบบทดสอบ</h4>--%>
                                                    <h3 class="panel-title" style="color: #4d4dff"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ผู้รับผิดชอบแบบทดสอบ</strong></h3>
                                                </b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label27" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlorg_person" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlorg_person" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกองค์กร"
                                                    ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />


                                            </div>
                                            <asp:Label ID="Label28" runat="server" Text="ฝ่าย" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlrdept_person" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlrdept_person" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกฝ่าย"
                                                    ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label29" runat="server" Text="แผนก" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlrsec_person" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกแผนก...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlrsec_person" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกแผนก"
                                                    ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                            </div>
                                            <asp:Label ID="Label30" runat="server" Text="ชื่อผู้รับผิดชอบ" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlname_person" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกชื่อผู้รับผิดชอบ...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlname_person" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกชื่อผู้รับผิดชอบ"
                                                    ValidationExpression="กรุณาเลือกชื่อผู้รับผิดชอบ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                            </div>

                                            <asp:LinkButton ID="lbldataset_person" ValidationGroup="SaveDataSet" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdDataSet_Person" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')">ADD + </asp:LinkButton>

                                        </div>
                                        <br />

                                        <div class="form-group">
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-8">
                                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="GvPerson"
                                                            runat="server"
                                                            CssClass="table table-striped table-responsive"
                                                            GridLines="None"
                                                            HeaderStyle-CssClass="info"
                                                            OnRowDataBound="Master_RowDataBound"
                                                            Visible="false"
                                                            OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">


                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>

                                                            <Columns>


                                                                <asp:TemplateField HeaderText="ชื่อชุดคำถาม" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lbltopic_person" runat="server" Text='<%# Eval("Topic_Name") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ตำแหน่งเปิดรับสมัคร" ItemStyle-CssClass="text-center" ItemStyle-Width="20%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblpname_person" runat="server" Text='<%# Eval("P_Name") %>' />
                                                                        <asp:Label ID="lblpidx_person" Visible="false" runat="server" Text='<%# Eval("PIDX") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ผู้รับผิดชอบ" ItemStyle-CssClass="text-left" ItemStyle-Width="20%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <b>
                                                                            <asp:Label ID="Label185" runat="server" Text="องค์กร : " /></b><asp:Label ID="Label186" runat="server" Text='<%# Eval("Org_Name") %>'></asp:Label>

                                                                        </p>
                                    <b>
                                        <asp:Label ID="lblemp_mobile2_no_" runat="server" Text="ฝ่าย : " /></b><asp:Label ID="lblemp_mobile_no" runat="server" Text='<%# Eval("Dep_Name") %>'></asp:Label>
                                                                        </p>                                    
                                    <b>
                                        <asp:Label ID="lblemp_e23mail_" runat="server" Text="แผนก : " /></b><asp:Label ID="lblem32p_email" runat="server" Text='<%# Eval("Sec_Name") %>'></asp:Label>
                                                                        </p>
                                    <b>
                                        <asp:Label ID="Label64" runat="server" Text="ชื่อผู้รับผิดชอบ : " /></b><asp:Label ID="Label184" runat="server" Text='<%# Eval("Person_Name") %>'></asp:Label>
                                                                        <asp:Label ID="lblEmpidx" Visible="false" runat="server" Text='<%# Eval("EmpIDX") %>'></asp:Label>
                                                                        </p>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteListPerson"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                                    </ItemTemplate>

                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                            </Columns>

                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-sm-2">
                                            </div>
                                        </div>


                                    </asp:Panel>

                                    <div class="form-group" id="div_save" runat="server" visible="false">
                                        <div class="col-sm-2 col-sm-offset-2">
                                            <asp:LinkButton ID="lbladd" ValidationGroup="SaveTopic" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewDetail" runat="server">
            <div class="col-md-12">
                <asp:Image ID="Image1" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/recruit/banner_register_6.png" Style="height: 500px; width: 100%;" />
            </div>
            <div class="col-md-12">

                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: #ffffb3;">
                        <h3><b>ประเภทคำตอบ</b></h3>
                    </blockquote>

                </div>
                <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="GvTypeAns" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-md-12"
                            HeaderStyle-Height="40px"
                            AllowPaging="false"
                            DataKeyNames="m0_taidx"
                            OnRowDataBound="Master_RowDataBound">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>


                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" ItemStyle-CssClass="text-left">
                                    <ItemTemplate>

                                        <div class="alert-message alert-message-success">
                                            <blockquote class="danger" style="background-color: #cce6ff;">
                                                <h4><b>
                                                    <i class="fa fa-edit"></i>&nbsp;
                                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Text='<%# Eval("type_asn") %>'></asp:Label>
                                                </b></h4>
                                            </blockquote>
                                        </div>

                                        <asp:Label ID="lblm0_taidx" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("m0_taidx") %>'></asp:Label>

                                        <asp:GridView ID="GvQuestion" runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-md-12"
                                            HeaderStyle-CssClass="danger small"
                                            HeaderStyle-Height="40px"
                                            AllowPaging="false"
                                            DataKeyNames="m0_quidx"
                                            OnRowCommand="onRowCommand"
                                            OnRowDataBound="Master_RowDataBound">

                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <asp:TemplateField HeaderText="ข้อที่" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" Visible="true" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblm0_toidx" Visible="false" runat="server" Text='<%# Eval("m0_toidx") %>' />
                                                        <asp:Label ID="lblm0_taidx" Visible="false" runat="server" Text='<%# Eval("m0_taidx") %>' />
                                                        <asp:Label ID="lblno_choice" runat="server" Text='<%# Eval("no_choice") %>' />
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="คำถาม" ItemStyle-CssClass="text-center" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblchoiceq" runat="server" Text='<%# Eval("quest_name") %>'></asp:Label>
                                                        <br />
                                                        <asp:HyperLink runat="server" ID="images_q" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="คำตอบ ก." ItemStyle-CssClass="text-center" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblchoice1" runat="server" Text='<%# Eval("choice_a") %>'></asp:Label>
                                                        <br />
                                                        <asp:HyperLink runat="server" ID="images_1" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="คำตอบ ข." ItemStyle-CssClass="text-center" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblchoice2" runat="server" Text='<%# Eval("choice_b") %>'></asp:Label>
                                                        <br />
                                                        <asp:HyperLink runat="server" ID="images_2" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="คำตอบ ค." ItemStyle-CssClass="text-center" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblchoice3" runat="server" Text='<%# Eval("choice_c") %>'></asp:Label>
                                                        <br />
                                                        <asp:HyperLink runat="server" ID="images_3" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="คำตอบ ง." ItemStyle-CssClass="text-center" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblchoice4" runat="server" Text='<%# Eval("choice_d") %>'></asp:Label>
                                                        <br />
                                                        <asp:HyperLink runat="server" ID="images_4" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="เฉลย" ItemStyle-CssClass="text-center" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblchoice_ans" runat="server" Text='<%# Eval("choice_ans") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Manage" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btndeletequest" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelQuest" OnClientClick="return confirm('คุณต้องการลบคำถามนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" data-toggle="tooltip" title="View" CommandArgument='<%# Eval("m0_quidx") + ";" + Eval("m0_toidx") + ";" + Eval("m0_taidx") + ";" + Eval("no_choice") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                                    </ItemTemplate>

                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>


                                            </Columns>
                                        </asp:GridView>
                                    </ItemTemplate>
                                </asp:TemplateField>


                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>


            <br />
            <asp:UpdatePanel ID="updateperson" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="col-md-12">
                        <div class="alert-message alert-message-success">
                            <blockquote class="danger" style="font-size: small; background-color: #ffdd99;">
                                <h3><b>ตำแหน่งเปิดรับสมัคร & ผู้รับผิดชอบแบบทดสอบ</b></h3>
                            </blockquote>

                        </div>

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Position & Personal Responsibility</strong></h3>
                            </div>


                            <div class="panel-body">

                                <div class="form-group">

                                    <asp:LinkButton ID="btnaddperson" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddStatus" runat="server" CommandName="CmdAddPerson" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                                </div>
                                <div class="form-horizontal" role="form">
                                    <asp:Panel ID="panel_chooseperson_addon" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label35" runat="server" Text="ตำแหน่งเปิดรับสมัคร" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddlposition_addon" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="SaveAddon" runat="server" Display="None"
                                                    ControlToValidate="ddlposition_addon" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกตำแหน่งเปิดรับสมัคร"
                                                    ValidationExpression="กรุณาเลือกตำแหน่งเปิดรับสมัคร" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />


                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label27" runat="server" Text="องค์กร" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddlorg_person_addon" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveAddon" runat="server" Display="None"
                                                    ControlToValidate="ddlorg_person_addon" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกองค์กร"
                                                    ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />


                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label28" runat="server" Text="ฝ่าย" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddlrdept_person_addon" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveAddon" runat="server" Display="None"
                                                    ControlToValidate="ddlrdept_person_addon" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกฝ่าย"
                                                    ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label29" runat="server" Text="แผนก" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddlrsec_person_addon" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกแผนก...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="SaveAddon" runat="server" Display="None"
                                                    ControlToValidate="ddlrsec_person_addon" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกแผนก"
                                                    ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label30" runat="server" Text="ชื่อผู้รับผิดชอบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddlname_person_addon" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกชื่อผู้รับผิดชอบ...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="SaveAddon" runat="server" Display="None"
                                                    ControlToValidate="ddlname_person_addon" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกชื่อผู้รับผิดชอบ"
                                                    ValidationExpression="กรุณาเลือกชื่อผู้รับผิดชอบ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="lbldataset_person" ValidationGroup="SaveAddon" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="Cmdaddon_person" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel_addon" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>

                                            </div>
                                        </div>
                                    </asp:Panel>


                                    <asp:GridView ID="GvPerson_Detail" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-md-12"
                                        HeaderStyle-CssClass="info small"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="false"
                                        DataKeyNames="TPIDX"
                                        OnRowDataBound="Master_RowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="ข้อมูลตำแหน่ง" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" Visible="true" ItemStyle-Font-Size="Small">

                                                <ItemTemplate>

                                                    <strong>
                                                        <asp:Label ID="Label6w0" runat="server"> ตำแหน่ง: </asp:Label></strong>
                                                    <asp:Literal ID="lbP_Name" runat="server" Text='<%# Eval("P_Name") %>' />
                                                    <asp:Literal ID="lblPIDX" Visible="false" runat="server" Text='<%# Eval("PIDX") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Labeel61" runat="server"> รายละเอียดงาน: </asp:Label></strong>
                                                    <asp:Label ID="lbdes" runat="server" Text='<%# Eval("P_Description") %>'></asp:Label>
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="lbsdat" runat="server"> สถานที่ปฎิบัติการ: </asp:Label></strong>
                                                    <asp:Label ID="lblocate" runat="server" Text='<%# Eval("P_Location") %>'></asp:Label>

                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ผู้รับผิดชอบ" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" Visible="true" ItemStyle-Font-Size="Small">

                                                <ItemTemplate>

                                                    <strong>
                                                        <asp:Label ID="Label60" runat="server"> องค์กร: </asp:Label></strong>
                                                    <asp:Literal ID="lbPosName" runat="server" Text='<%# Eval("org_name_th") %>' />
                                                    <asp:Literal ID="Literal1" Visible="false" runat="server" Text='<%# Eval("P_EmpIDX") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label61" runat="server"> ฝ่าย: </asp:Label></strong>
                                                    <asp:Label ID="lbdepth" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="lbdat" runat="server"> แผนก: </asp:Label></strong>
                                                    <asp:Label ID="lbsec" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label31" runat="server"> ตำแหน่ง: </asp:Label></strong>
                                                    <asp:Label ID="Label32" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label15" runat="server"> ชื่อ-นามสกุล: </asp:Label></strong>
                                                    <asp:Label ID="Label33" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Manage" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btndeleteperson" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel_person" OnClientClick="return confirm('คุณต้องการลบคำถามนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" data-toggle="tooltip" title="View" CommandArgument='<%# Eval("TPIDX") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-1 col-sm-offset-11">
                                <asp:LinkButton ID="lblcancel" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Back"><i class="fa fa-reply"></i></asp:LinkButton>
                            </div>
                        </div>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>
        <asp:View runat="server" ID="Viewscore">
         


            <div class="col-sm-12" runat="server" id="score_list">
                <div class="row_v2">

                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-search"></i><strong>&nbsp; ค้นหา</strong></h3>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-offset-1">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <label class="col-sm-3 col-md-3  col-lg-3 control-label text-right">ค้นหา เลขบัตรประชาชน :</label>
                                                    <div class="col-sm-6 col-md-4 col-lg-3" style="">
                                                        <asp:TextBox ID="txt_src_score" runat="server" CssClass="form-control" autocomplete="off" placeholder="เลขบัตรประชาชน ...">
                                                        </asp:TextBox>
                                                        <asp:LinkButton runat="server" ID="btn_src_score" CssClass="btn btn-primary mt-7" OnCommand="btnCommand" CommandName="btn_src_score"><i class="fa fa-search"></i>&nbsp;ค้นหา</asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="btn_src_score_refresh" CssClass="btn btn-default mt-7" OnCommand="btnCommand" CommandName="btn_src_score_refresh"><i class="fa fa-refresh"></i></asp:LinkButton>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-12 auto-scroll">
                        <%--<div id="GvApprove1_scroll" style="overflow-x: auto; width: 100%" runat="server">--%>

                        <asp:GridView ID="Gvscore" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover "
                            OnRowDataBound="Master_RowDataBound"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            AllowPaging="true"
                            PageSize="10">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <div style="text-align: center; padding-top: 5px;">
                                            <asp:Label ID="bom_m1_idx" runat="server" Visible="false"  />
                                            <%# (Container.DataItemIndex +1) %>
                                        </div>
                                

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <div style="padding-top: 5px; text-align: left; white-space: nowrap;">
                                            <asp:Label runat="server"
                                                Text='<%# Eval("FirstNameTH") +" "+ Eval("LastNameTH") +" "+ (((int)Eval("m0_tqidx") == 1) ? "(พื้นฐาน)" : "(เฉพาะทาง)" ) %>'></asp:Label>
                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="หมายเลขบัตรประชาชน" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <div style="padding-top: 5px; text-align: left; white-space: nowrap;">
                                            <asp:Label runat="server"
                                                Text='<%# Eval("identity_card")  %>'></asp:Label>
                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="สถานะทำข้อสอบ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                    <ItemTemplate>

                                        <div style="padding: 5px 10px 0px; text-align: center;">
                                            <asp:Label ID="lb_exam_status" Visible="false" runat="server"
                                                CssClass="col-sm-12" Text='<%# Eval("exam_status") %>'></asp:Label>
                                            <asp:Label ID="lb_exam_status_true" runat="server" Visible="false"
                                                data-toggle="tooltip" data-placement="top" title="Success"
                                                CssClass="col-sm-12 p-0">
                                                <div style=" color:green;">
                                                    <span><b style="white-space: nowrap;">สำเร็จ</b></span>
                                                </div>
                                            </asp:Label>
                                            <asp:Label ID="lb_exam_status_false" runat="server" Visible="false"
                                                data-toggle="tooltip" data-placement="top" title="Unsuccess" CssClass="col-sm-12 p-0">
                                                <div style=" color:red;">
                                                <span><b style="white-space: nowrap;">ไม่สำเร็จ</b></span>
                                                     </div>
                                            </asp:Label>

                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <div style="justify-content: center; display: flex;">

                                            <asp:LinkButton ID="Edit_m1" CssClass="btn btn-primary btn-sm" runat="server" CommandName="read_score_detail"
                                                data-toggle="tooltip" OnCommand="btnCommand" title="Edit" CommandArgument='<%# Eval("EmpIDX") %>'>
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                            <%--    <asp:LinkButton ID="btnTodelete_m1" CssClass="btn btn-danger btn-sm" runat="server" CommandName="btnTodelete_m1"
                                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่ ?')"
                                                CommandArgument='<%#Eval("rq_idx") %>' title="Delete" Style="margin-left: 4px;">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>--%>
                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </div>
                </div>

            </div>
            <div class="col-sm-12" runat="server" id="score_detail" visible="false">
                <asp:LinkButton ID="backtolistscore" runat="server"
                    CommandName="_backtolistscore"
                    OnCommand="btnCommand" Text="Back" CssClass="btn btn-primary" CommandArgument="0" />
                <div class="row_v2" style="margin-top: 7px;">
                    <div class="col-sm-12">

                        <asp:GridView ID="Gvscore_detail_u0" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover  col-md-12"
                            OnRowDataBound="Master_RowDataBound">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <div style="text-align: center; padding-top: 5px;">
                                            <asp:Label ID="bom_m1_idx" runat="server" Visible="false" Text='<%# Eval("u0_anidx") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </div>
                                        <asp:TextBox runat="server" ID="CEmpIDX" Text='<%# Eval("CEmpIDX") %>' Visible="false"></asp:TextBox>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ชื่อข้อสอบ" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <div style="padding-top: 5px; text-align: left; white-space: nowrap;">
                                            <asp:Label runat="server" ID="topic_name"
                                                Text='<%# Eval("topic_name")  %>'></asp:Label>
                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="คะแนน ปรนัย" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <div style="padding-top: 5px; text-align: left; white-space: nowrap;">
                                            <asp:Label runat="server"
                                                Text='<%# Eval("score_choice")  %>'></asp:Label>
                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="คะแนน อัตนัย" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <div style="padding-top: 5px; text-align: left; white-space: nowrap;">
                                            <asp:Label runat="server"
                                                Text='<%# Eval("score_comment")  %>'></asp:Label>
                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="คะแนน รวม" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <div style="padding-top: 5px; text-align: left; white-space: nowrap;">
                                            <asp:Label runat="server"
                                                Text='<%# Eval("total_score")  %>'></asp:Label>
                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ตรวจคะแนน" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <div style="padding: 5px 10px 0px; text-align: center;">
                                            <asp:Label ID="lb_exam_check_exam" Visible="false" runat="server"
                                                CssClass="col-sm-12" Text='<%# Eval("check_exam") %>'></asp:Label>
                                            <asp:Label ID="lb_exam__check_exam_true" runat="server" Visible="false"
                                                data-toggle="tooltip" data-placement="top" title="Success"
                                                CssClass="col-sm-12 p-0">
                                                <div style=" color:green;">
                                                    <span><b style="white-space: nowrap;">ตรวจแล้ว</b></span>
                                                </div>
                                            </asp:Label>
                                            <asp:Label ID="lb_exam__check_exam_false" runat="server" Visible="false"
                                                data-toggle="tooltip" data-placement="top" title="Unsuccess" CssClass="col-sm-12 p-0">
                                                <div style=" color:red;">
                                                <span><b style="white-space: nowrap;">ยังไม่ตรวจ</b></span>
                                                     </div>
                                            </asp:Label>

                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <div style="justify-content: center; display: flex;">

                                            <asp:LinkButton ID="read_answer_u0" CssClass="btn btn-primary btn-sm" runat="server" CommandName="read_answer_u0"
                                                data-toggle="tooltip" OnCommand="btnCommand" title="ตรวจและใช้คะแนน" CommandArgument='<%# Eval("m0_toidx") %>'>
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                            <%--    <asp:LinkButton ID="btnTodelete_m1" CssClass="btn btn-danger btn-sm" runat="server" CommandName="btnTodelete_m1"
                                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่ ?')"
                                                CommandArgument='<%#Eval("rq_idx") %>' title="Delete" Style="margin-left: 4px;">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>--%>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </div>
                </div>
            </div>
            <div class="col-sm-12" id="score_detail_sub" runat="server" visible="false">
                <asp:LinkButton ID="LinkButton2" runat="server"
                    CommandName="_backtolistscore"
                    OnCommand="btnCommand" Text="Back" CssClass="btn btn-primary" CommandArgument="1" />


                <div class="row">
                    <div class="col-sm-12 box-qeustion">
                        <div class="" style="margin-bottom: 7px;">
                            <span class="h4">ชุดข้อสอบ :</span><span class="h4" id="s_topic_name" runat="server"></span>

                        </div>
                        <asp:Repeater ID="ChildRepeater" runat="server" OnItemDataBound="Boundtopictype">

                            <ItemTemplate>
                                <div class="row m-b-10">
                                    <asp:TextBox runat="server" ID="m0_toidx" Visible="false" Text='<%# Eval("m0_toidx") %>'></asp:TextBox>
                                    <asp:TextBox runat="server" ID="m0_taidx" Visible="false" Text='<%# Eval("m0_taidx") %>'></asp:TextBox>

                                    <div class="col-sm-12 box-type-topic">
                                        <div class=""><%#Eval("type_asn") %></div>

                                    </div>
                                    <div class="col-sm-12 m-t-10">
                                        <asp:Repeater ID="Repquestion" runat="server" OnItemDataBound="BoundRepquestion">
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="m0_toidx" Visible="false" Text='<%# Eval("m0_toidx") %>'></asp:TextBox>
                                                <asp:TextBox runat="server" ID="m0_taidx" Visible="false" Text='<%# Eval("m0_taidx") %>'></asp:TextBox>
                                                <asp:TextBox runat="server" ID="m0_quidx" Visible="false" Text='<%# Eval("m0_quidx") %>'></asp:TextBox>
                                                <asp:TextBox runat="server" ID="choice_ans" Visible="false" Text='<%# Eval("choice_ans") %>'></asp:TextBox>

                                                <asp:TextBox runat="server" ID="TextBox1" Visible="false" Text='<%# Eval("choice_ans") %>'></asp:TextBox>
                                                <div class="row">
                                                    <div class="col-sm-12"><%# Container.ItemIndex+1 +". "+  Eval("quest_name") %></div>

                                                    <div class="col-sm-9" runat="server" id="Box_score_comment" visible='<%# (Eval("m0_taidx").ToString() == "2" )?true:false %>'>
                                                        <div style="display: inline-block;">คะแนน :</div>
                                                        <asp:TextBox runat="server" ID="txt_score_comment" Text="" CssClass="form-control txt_score_comment" placeholder="ให้คะแนน" onkeydown="return (!(event.keyCode>=65 && event.keyCode<=90 ) && event.keyCode!=32);"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-sm-12" runat="server" id="box_quest_radio" visible='<%# (Eval("m0_taidx").ToString() == "1" )?true:false %>'>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-6 box-choice">
                                                                <asp:RadioButton ID="ra_choice_a" runat="server" Text='<%# "a. "+ Eval("choice_a") %>' CssClass="answer_choice" GroupName="choice" Enabled="false" />

                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 box-choice">
                                                                <asp:RadioButton ID="ra_choice_b" runat="server" Text='<%# "b. "+  Eval("choice_b") %>' CssClass="answer_choice" GroupName="choice" Enabled="false" />

                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 box-choice">
                                                                <asp:RadioButton ID="ra_choice_c" runat="server" Text='<%# "c. "+ Eval("choice_c") %>' CssClass="answer_choice" GroupName="choice" Enabled="false" />

                                                            </div>
                                                            <div class="col-xs-12 col-sm-6 box-choice">
                                                                <asp:RadioButton ID="ra_choice_d" runat="server" Text='<%# "d. "+ Eval("choice_d") %>' CssClass="answer_choice" GroupName="choice" Enabled="false" />

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12" runat="server" id="box_quest_text" visible='<%# (Eval("m0_taidx").ToString() == "2" )?true:false %>'>
                                                        <asp:TextBox ID="answer_txt" TextMode="multiline" CssClass="form-control" Columns="50" Rows="5" runat="server" Enabled="false" />
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>

                                </div>

                            </ItemTemplate>
                        </asp:Repeater>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="col-sm-12">
                                    <asp:LinkButton ID="btn_confirm_score" runat="server"
                                        CommandName="btn_confirm_score"
                                        OnCommand="btnCommand" Text="ตกลง" CssClass="btn btn-success mt-7 pull-right" CommandArgument="0" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>

</asp:Content>

