﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;



public partial class websystem_MasterData_qa_lab_create_form : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_qa _data_qa = new data_qa();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- master data setname & form detail --//
    static string _urlQaGetTestDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetTestDetail"];
    static string _urlQaGetNameList = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetNameList"];
    static string _urlQaSetFormDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetFormDetail"];
    static string _urlQaGetFormDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetFormDetail"];
    static string _urlQaDelFormDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaDelFormDetail"];
    static string _urlQaSetR0FormCreate = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetR0FormCreate"];
    static string _urlQaGetR0FormCreate = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetR0FormCreate"];
    static string _urlQaGetR1FormCreate = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetR1FormCreate"];
    static string _urlQaDelR0FormCreate = _serviceUrl + ConfigurationManager.AppSettings["urlQaDelR0FormCreate"];
    static string _urlQaSetR1FormCreate = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetR1FormCreate"];
    
    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;

    // rdept permission
    // int[] rdept_qmr = { 20 }; //QMR:26
    #endregion initial function/data


    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        ////foreach (int item in rdept_qmr)
        ////{
        ////    if (_dataEmployee.employee_list[0].rdept_idx == item)
        ////    {
        ////        _flag_qmr = true;
        ////        break;
        ////    }
        ////}
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            selectR0FormCreate();
        }
        linkBtnTrigger(btnCreateForm);
        createForm();
    }

  

    #region event command


    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();


        switch (cmdName)
        {

            case "cmdCreateForm":
                switch (cmdArg)
                {
                    case "0":
                        gvMaster.EditIndex = -1;
                        selectR0FormCreate();
                        setFormData(fvformInsert, FormViewMode.Insert, null);
                        getM0TestDetailList((DropDownList)fvformInsert.FindControl("ddlTestType"), "0");
                        createForm();
                       
                        break;

                    case "1":
                        gvR1FormCreate.EditIndex = -1;
                        getR1FormList();
                        setFormData(fvformInsertRoot, FormViewMode.Insert, null);
                        getR0FormList();
                        getM0FormDetailList((DropDownList)fvformInsertRoot.FindControl("ddlTopicForm"), "0");
                        break;


                    case "2":
                        gvFormListLv3.EditIndex = -1;
                        setFormData(fvInsertRootSubSet, FormViewMode.Insert, null);
                        getFormListLv3();

                        break;

                    case "3":
                        gvDataFormListSubset.EditIndex = -1;
                        setFormData(fvInsertDataRootSubSet, FormViewMode.Insert, null);
                        getFormListLv4();

                        break;


                }

                break;

            case "cmdCancel":
                switch (cmdArg)
                {
                    case "0":
                        setActiveView("pageGenaral", 0,0,0);
                        setFormData(fvformInsert, FormViewMode.ReadOnly, null);
                        SETFOCUS.Focus();
                        break;

                    case "1":
                        setActiveView("pageCreateRoot", 0,0,0);
                        setFormData(fvformInsertRoot, FormViewMode.ReadOnly, null);
                        gvR1FormCreate.EditIndex = -1;
                        getR1FormList();
                        SETFOCUS.Focus();
                        break;

                    case "2":
                        setActiveView("pageCreateRootSub", 0, 0,0);
                        setFormData(fvInsertRootSubSet, FormViewMode.ReadOnly, null);
                        gvFormListLv3.EditIndex = -1;
                        getSelectFormLv3List();
                        SETFOCUS.Focus();
                        break;

                    case "3":
                        setActiveView("pageCreateRootSubSet", 0, 0,0);
                        setFormData(fvInsertDataRootSubSet, FormViewMode.ReadOnly, null);
                        gvDataFormListSubset.EditIndex = -1;
                        getSelectFormLv4List();
                        SETFOCUS.Focus();
                        break;

                    case "4":
                        setActiveView("pageCreateRootSub",0, int.Parse(ViewState["form_idx"].ToString()), int.Parse(ViewState["T1"].ToString()));
                       // litDebug.Text = (ViewState["T1"].ToString());
                        setFormData(fvInsertRootSubSet, FormViewMode.ReadOnly, null);
                        ViewState["r1_form_idx"] = lbCheckBack.Text;

                        // lbCheckBack.Text 
                        gvFormListLv3.EditIndex = -1;
                        getSelectFormLv3List();
                        SETFOCUS.Focus();
                        break;


                }

                break;

            case "cmdView":
                string[] cmdArgView = cmdArg.Split(',');
                ViewState["r0_form_idx"] = int.TryParse(cmdArgView[0].ToString(), out _default_int) ? int.Parse(cmdArgView[0].ToString()) : _default_int;
                int conditionView = int.TryParse(cmdArgView[1].ToString(), out _default_int) ? int.Parse(cmdArgView[1].ToString()) : _default_int;

                switch (conditionView)
                {
                    case 0:
                        setActiveView("pageCreateRoot", int.Parse(ViewState["r0_form_idx"].ToString()),0,0);
                        setFormData(fvformInsertRoot, FormViewMode.ReadOnly, null);
                        gvR1FormCreate.EditIndex = -1;
                        getR1FormList();
                        break;

                    case 1:
                        setActiveView("pageCreateRootSub", int.Parse(ViewState["r0_form_idx"].ToString()),0,0);
                        setFormData(fvInsertRootSubSet, FormViewMode.ReadOnly, null);
                        // gvR1FormCreate.EditIndex = -1;
                        //getFormListLv3();
                        //selectRootSubFormDetail();
                        break;

                }


                break;

            case "cmdViewRoot":
                string[] cmdArgViewRoot = cmdArg.Split(',');
                ViewState["r1_form_idx"] = int.TryParse(cmdArgViewRoot[0].ToString(), out _default_int) ? int.Parse(cmdArgViewRoot[0].ToString()) : _default_int;
                int conditionViewRoot = int.TryParse(cmdArgViewRoot[1].ToString(), out _default_int) ? int.Parse(cmdArgViewRoot[1].ToString()) : _default_int;
                ViewState["form_idx"] = int.TryParse(cmdArgViewRoot[2].ToString(), out _default_int) ? int.Parse(cmdArgViewRoot[2].ToString()) : _default_int;
                ViewState["root_form_idx"] = int.TryParse(cmdArgViewRoot[3].ToString(), out _default_int) ? int.Parse(cmdArgViewRoot[3].ToString()) : _default_int;

                switch (conditionViewRoot)
                {
                    case 0:
                        gvFormListLv3.EditIndex = -1;
                        lbCheckBack.Text = ViewState["r1_form_idx"].ToString();
                        setActiveView("pageCreateRootSub", int.Parse(ViewState["r1_form_idx"].ToString()) , int.Parse(ViewState["form_idx"].ToString()), int.Parse(lbCheckBack.Text));
                        setFormData(fvInsertRootSubSet, FormViewMode.ReadOnly, null);
                        getSelectFormLv3List();
                      //  litDebug.Text = ViewState["root_form_idx"].ToString();


                        break;

                    case 1:
                        gvDataFormListSubset.EditIndex = -1;
                        int T1 = int.Parse(ViewState["r1_form_idx"].ToString());
                        ViewState["T1"] = T1;
                        setActiveView("pageCreateRootSubSet", int.Parse(ViewState["r1_form_idx"].ToString()), int.Parse(ViewState["form_idx"].ToString()), int.Parse(ViewState["T1"].ToString()));
                        setFormData(fvInsertDataRootSubSet, FormViewMode.ReadOnly, null);
                        getSelectFormLv4List();
                     //   litDebug.Text = ViewState["root_form_idx"].ToString();
                        break;

                }


                break;

            case "cmdSave":
                switch (cmdArg)
                {
                    case "0":
                        TextBox tbFormName = (TextBox)fvformInsert.FindControl("tbFormName");
                        DropDownList ddlFormStatus = (DropDownList)fvformInsert.FindControl("ddlFormStatus");
                        DropDownList ddlTestType = (DropDownList)fvformInsert.FindControl("ddlTestType");
                       // DropDownList ddlTopicForm = (DropDownList)fvformInsert.FindControl("ddlTopicForm");

                        _data_qa.qa_r0_form_create_list = new qa_r0_form_create_detail[1];
                        qa_r0_form_create_detail _r0form = new qa_r0_form_create_detail();
                        _r0form.cemp_idx = _emp_idx;
                        _r0form.r0_form_create_name = tbFormName.Text;
                        _r0form.test_detail_idx = int.Parse(ddlTestType.SelectedValue);
                        _r0form.r0_form_create_status = int.Parse(ddlFormStatus.SelectedValue);

                        _data_qa.qa_r0_form_create_list[0] = _r0form;
                        _data_qa = callServicePostMasterQA(_urlQaSetR0FormCreate, _data_qa);
                     
                        selectR0FormCreate();
                        setFormData(fvformInsert, FormViewMode.ReadOnly, null);
                        setFormData(fvformInsert, FormViewMode.Insert, null);
                        getM0TestDetailList((DropDownList)fvformInsert.FindControl("ddlTestType"), "0");
                        //getM0FormDetailList((DropDownList)fvformInsert.FindControl("ddlTopicForm"), "0");
                        break;

                    case "1":
                        DropDownList ddlFormStatusLevel2 = (DropDownList)fvformInsertRoot.FindControl("ddlFormStatusLevel2");
                        DropDownList ddlTopicForm = (DropDownList)fvformInsertRoot.FindControl("ddlTopicForm");

                        _data_qa.qa_r1_form_create_list = new qa_r1_form_create_detail[1];
                        qa_r1_form_create_detail _r1form = new qa_r1_form_create_detail();
                        _r1form.cemp_idx = _emp_idx;
                        _r1form.r0_form_create_idx = int.Parse(ViewState["r0_form_idx"].ToString());
                        _r1form.form_detail_idx = int.Parse(ddlTopicForm.SelectedValue);
                        _r1form.r1_form_create_status = int.Parse(ddlFormStatusLevel2.SelectedValue);

                        _data_qa.qa_r1_form_create_list[0] = _r1form;
                        _data_qa = callServicePostMasterQA(_urlQaSetR1FormCreate, _data_qa);

                        getR1FormList();
                        break;

                    case "2":

                        DropDownList ddlFormStatusLevel3 = (DropDownList)fvInsertRootSubSet.FindControl("ddlFormStatusLevel3");
                        TextBox tbTopicFormLv3 = (TextBox)fvInsertRootSubSet.FindControl("tbTopicFormLv3");

                        data_qa _dataInsertLv3 = new data_qa();
                        _dataInsertLv3.qa_r1_form_create_list = new qa_r1_form_create_detail[1];
                        qa_r1_form_create_detail _r1formLv3 = new qa_r1_form_create_detail();
                        _r1formLv3.cemp_idx = _emp_idx;
                        _r1formLv3.text_name_setform = tbTopicFormLv3.Text;
                        _r1formLv3.form_detail_idx = int.Parse(ViewState["form_idx"].ToString());
                        _r1formLv3.r1_form_detail_root_idx = int.Parse(ViewState["r1_form_idx"].ToString());
                        _r1formLv3.r1_form_create_status = int.Parse(ddlFormStatusLevel3.SelectedValue);
                        _dataInsertLv3.qa_r1_form_create_list[0] = _r1formLv3;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataInsertLv3));
                        _dataInsertLv3 = callServicePostMasterQA(_urlQaSetR1FormCreate, _dataInsertLv3);
                        getSelectFormLv3List();

                        break;

                    case "3":

                        DropDownList ddlFormStatusLevel4 = (DropDownList)fvInsertDataRootSubSet.FindControl("ddlFormStatusLevel4");
                        TextBox tbTopicFormLv4 = (TextBox)fvInsertDataRootSubSet.FindControl("tbTopicFormLv4");

                        data_qa _dataInsertLv4 = new data_qa();
                        _dataInsertLv4.qa_r1_form_create_list = new qa_r1_form_create_detail[1];
                        qa_r1_form_create_detail _r1formLv4 = new qa_r1_form_create_detail();
                        _r1formLv4.cemp_idx = _emp_idx;
                        _r1formLv4.text_name_setform = tbTopicFormLv4.Text;
                        _r1formLv4.form_detail_idx = int.Parse(ViewState["form_idx"].ToString());
                        _r1formLv4.r1_form_detail_root_idx = int.Parse(ViewState["r1_form_idx"].ToString());
                        _r1formLv4.r1_form_create_status = int.Parse(ddlFormStatusLevel4.SelectedValue);
                        _dataInsertLv4.qa_r1_form_create_list[0] = _r1formLv4;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataInsertLv3));
                        _dataInsertLv4 = callServicePostMasterQA(_urlQaSetR1FormCreate, _dataInsertLv4);
                        getSelectFormLv4List();

                        break;

                }

                break;

            case "cmdDelete":
                int _formIDX = int.Parse(cmdArg);
                _data_qa.qa_r0_form_create_list = new qa_r0_form_create_detail[1];
                qa_r0_form_create_detail _delform = new qa_r0_form_create_detail();
                _delform.condition = 0;
                _delform.r0_form_create_idx = _formIDX;
                _data_qa.qa_r0_form_create_list[0] = _delform;
                _data_qa = callServicePostMasterQA(_urlQaDelR0FormCreate, _data_qa);

                if (_data_qa.return_code == 0)
                {
                    // litDebug.Text = "success";
                }
                else
                {
                    // litDebug.Text = "not success";
                }

                selectR0FormCreate();

                break;

            case "cmdDeleteRoot":

                string[] cmdArgDeleteForm = cmdArg.Split(',');
                int form_idx_del = int.TryParse(cmdArgDeleteForm[0].ToString(), out _default_int) ? int.Parse(cmdArgDeleteForm[0].ToString()) : _default_int;
                int condition_del = int.TryParse(cmdArgDeleteForm[1].ToString(), out _default_int) ? int.Parse(cmdArgDeleteForm[1].ToString()) : _default_int;

                _data_qa.qa_r0_form_create_list = new qa_r0_form_create_detail[1];
                qa_r0_form_create_detail _delformR1 = new qa_r0_form_create_detail();

                _delformR1.condition = 1;
                _delformR1.r0_form_create_idx = form_idx_del;
                _data_qa.qa_r0_form_create_list[0] = _delformR1;
                _data_qa = callServicePostMasterQA(_urlQaDelR0FormCreate, _data_qa);

                switch (condition_del.ToString())
                {
                    case "0":
                        getR1FormList();
                        break;
                    case "1":
                        getSelectFormLv3List();
                        break;
                    case "2":
                        getSelectFormLv4List();
                        break;
                }



               

                break;

            case "cmdAddForm":
               
                DropDownList _testType = (DropDownList)fvformInsert.FindControl("ddlTestType");
                DropDownList _topicForm = (DropDownList)fvformInsert.FindControl("ddlTopicForm");
                GridView _gvFormList = (GridView)fvformInsert.FindControl("gvCreateFormList");
                Panel panelFormButton = (Panel)fvformInsert.FindControl("panelFormButton");

                var _dataset_createForm = (DataSet)ViewState["createFormList"];
                var _datarow_createForm = _dataset_createForm.Tables[0].NewRow();

                //ค่าที่เก็บที่จะเอามาเช็ค
                ViewState["m0Topic_check"] = (_topicForm.SelectedValue);
                int _numrow = _dataset_createForm.Tables[0].Rows.Count; //จำนวนแถว
                if (_numrow > 0)
                {
                    foreach (DataRow check in _dataset_createForm.Tables[0].Rows)
                    {
                        ViewState["_check_topicDetail"] = check["m0FormDetailIDX"];
                        //กรณิประเภทการตรวจตรงกัน
                        if (ViewState["m0Topic_check"].ToString() == ViewState["_check_topicDetail"].ToString())
                        {

                            ViewState["CheckDataset"] = "2";
                            break;

                        }
                        else
                        {
                            //กรณิประเภทการตรวจไม่ตรงกัน
                            ViewState["CheckDataset"] = "1";
                        }

                    }
                    //ถ้าข้อมูลไม่ซ้ำ จะเข้าการทำงานนี้
                    if (ViewState["CheckDataset"].ToString() == "1")
                    {
                        _datarow_createForm["m0TestDetailIDX"] = (_testType.SelectedValue);
                        _datarow_createForm["nameTestDetail"] = (_testType.SelectedItem);
                        _datarow_createForm["m0FormDetailIDX"] = (_topicForm.SelectedValue);
                        _datarow_createForm["nameTopic"] = (_topicForm.SelectedItem);

                        _dataset_createForm.Tables[0].Rows.Add(_datarow_createForm);
                        ViewState["createFormList"] = _dataset_createForm;

                        _gvFormList.DataSource = _dataset_createForm.Tables[0];
                        _gvFormList.DataBind();

                        panelFormButton.Visible = true;
                    }
                    else if (ViewState["CheckDataset"].ToString() == "2")
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขออภัยค่ะ ท่านมีการเพิ่มข้อมูลหัวข้อนี้แล้วค่ะ');", true);

                    }
                }
                else
                {
                    _datarow_createForm["m0TestDetailIDX"] = (_testType.SelectedValue);
                    _datarow_createForm["nameTestDetail"] = (_testType.SelectedItem);
                    _datarow_createForm["m0FormDetailIDX"] = (_topicForm.SelectedValue);
                    _datarow_createForm["nameTopic"] = (_topicForm.SelectedItem);

                    _dataset_createForm.Tables[0].Rows.Add(_datarow_createForm);
                    ViewState["createFormList"] = _dataset_createForm;

                    _gvFormList.DataSource = _dataset_createForm.Tables[0];
                    _gvFormList.DataBind();
                    panelFormButton.Visible = true;
                }


                break;

        }
    }
    #endregion event command

    #region data set
    protected void createForm()
    {
        var _createFormList = new DataSet();
        _createFormList.Tables.Add("datacreateFromList");
        //_createFormList.Tables[0].Columns.Add("r0FormIDX", typeof(int));
        _createFormList.Tables[0].Columns.Add("m0FormDetailIDX", typeof(int));
        _createFormList.Tables[0].Columns.Add("m0TestDetailIDX", typeof(int));
        _createFormList.Tables[0].Columns.Add("nameFormCreate", typeof(String));
        _createFormList.Tables[0].Columns.Add("nameTestDetail", typeof(String));
        _createFormList.Tables[0].Columns.Add("nameTopic", typeof(String));

        if (ViewState["createFormList"] == null)
        {
            ViewState["createFormList"] = _createFormList;
        }
    }

    #endregion

    #region dropdown list
    protected void getM0TestDetailList(DropDownList ddlName, string _test_detail_idx)
    {
        data_qa _dataqa = new data_qa();
        _dataqa.qa_m0_test_detail_list = new qa_m0_test_detail[1];
        qa_m0_test_detail _m0test = new qa_m0_test_detail();
        _m0test.condition = 1;
        _dataqa.qa_m0_test_detail_list[0] = _m0test;

        _dataqa = callServicePostMasterQA(_urlQaGetTestDetail, _dataqa);
        setDdlData(ddlName, _dataqa.qa_m0_test_detail_list, "test_detail_name", "test_detail_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทรายการที่ตรวจ ---", "0"));
        ddlName.SelectedValue = _test_detail_idx;
    }

    protected void getM0FormDetailList(DropDownList ddlName, string _form_detail_idx)
    {
        data_qa _dataqa = new data_qa();
        _dataqa.qa_m0_form_detail_list = new qa_m0_form_detail[1];
        qa_m0_form_detail _m0form = new qa_m0_form_detail();
        _m0form.condition = 1;
        _dataqa.qa_m0_form_detail_list[0] = _m0form;

        _dataqa = callServicePostMasterQA(_urlQaGetFormDetail, _dataqa);
        setDdlData(ddlName, _dataqa.qa_m0_form_detail_list, "form_detail_name", "form_detail_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฟอร์มรายการที่ตรวจ ---", "0"));
        ddlName.SelectedValue = _form_detail_idx;
    }

    #endregion

    #region grid view
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":
                gvMaster.PageIndex = e.NewPageIndex;
                gvMaster.DataBind();
                selectR0FormCreate();

                break;

            case "gvR1FormCreate":
                gvR1FormCreate.PageIndex = e.NewPageIndex;
                gvR1FormCreate.DataBind();
                getR1FormList();
                break;

            case "gvFormListLv3":
                gvFormListLv3.PageIndex = e.NewPageIndex;
                gvFormListLv3.DataBind();
                getSelectFormLv3List();
                break;

            case "gvDataFormListSubset":
                gvDataFormListSubset.PageIndex = e.NewPageIndex;
                gvDataFormListSubset.DataBind();
                getSelectFormLv4List();
                break;

        }

    }
    protected void gvRowDeleted(object sender, GridViewDeleteEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvCreateFormList":

                GridView gvCreateFormList = (GridView)fvformInsert.FindControl("gvCreateFormList");
                var DeleteFormList = (DataSet)ViewState["createFormList"];
                var drDriving = DeleteFormList.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["createFormList"] = DeleteFormList;
                gvCreateFormList.EditIndex = -1;

                setGridData(gvCreateFormList, ViewState["createFormList"]);



                break;
        }

    }
    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var gvName = (GridView)sender;
        switch (gvName.ID)
        {
            case "gvMaster":
                gvMaster.EditIndex = e.NewEditIndex;
                selectR0FormCreate();
                break;

            case "gvR1FormCreate":
                gvR1FormCreate.EditIndex = e.NewEditIndex;
                getR1FormList();
                break;

            case "gvFormListLv3":
                gvFormListLv3.EditIndex = e.NewEditIndex;
                getSelectFormLv3List();
                break;

            case "gvDataFormListSubset":
                gvDataFormListSubset.EditIndex = e.NewEditIndex;
                getSelectFormLv4List();
                break;
        }
         
    }
    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gvName = (GridView)sender;
        switch (gvName.ID)
        {
            case "gvMaster":

                var _update_r0Formidx= (TextBox)gvMaster.Rows[e.RowIndex].FindControl("_Update_R0FormIDX");
                var _update_r0FormName = (TextBox)gvMaster.Rows[e.RowIndex].FindControl("tbupdate_formname");
                var _update_testtypeidx = (DropDownList)gvMaster.Rows[e.RowIndex].FindControl("ddlTestTypeUpdate");
                var _update_r0status = (DropDownList)gvMaster.Rows[e.RowIndex].FindControl("ddlupdater0status");

                gvMaster.EditIndex = -1;

                _data_qa.qa_r0_form_create_list = new qa_r0_form_create_detail[1];
                qa_r0_form_create_detail _upFormR0 = new qa_r0_form_create_detail();
                _upFormR0.r0_form_create_idx = int.Parse(_update_r0Formidx.Text);
                _upFormR0.r0_form_create_name = _update_r0FormName.Text;
                _upFormR0.test_detail_idx = int.Parse(_update_testtypeidx.SelectedValue);
                _upFormR0.r0_form_create_status = int.Parse(_update_r0status.SelectedValue);
                _data_qa.qa_r0_form_create_list[0] = _upFormR0;

                _data_qa = callServicePostMasterQA(_urlQaSetR0FormCreate, _data_qa);
                if (_data_qa.return_code == 102)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                else
                {

                }

                selectR0FormCreate();
                SETFOCUS.Focus();
                break;

            case "gvR1FormCreate":


                var _update_r1Formidx = (TextBox)gvR1FormCreate.Rows[e.RowIndex].FindControl("_Update_R1FormName");
                var _update_formdetail = (DropDownList)gvR1FormCreate.Rows[e.RowIndex].FindControl("ddlFormDetailUpdate");
                var _update_r1status = (DropDownList)gvR1FormCreate.Rows[e.RowIndex].FindControl("ddlupdater1status");

                gvR1FormCreate.EditIndex = -1;

                _data_qa.qa_r1_form_create_list = new qa_r1_form_create_detail[1];
                qa_r1_form_create_detail _upFormR1 = new qa_r1_form_create_detail();
                _upFormR1.r1_form_create_idx = int.Parse(_update_r1Formidx.Text);
                _upFormR1.form_detail_idx = int.Parse(_update_formdetail.SelectedValue);
                _upFormR1.r1_form_create_status = int.Parse(_update_r1status.SelectedValue);
                _data_qa.qa_r1_form_create_list[0] = _upFormR1;

                _data_qa = callServicePostMasterQA(_urlQaSetR1FormCreate, _data_qa);
                if (_data_qa.return_code == 102)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                else
                {

                }

                getR1FormList();
                SETFOCUS.Focus();

                

                break;

            case "gvFormListLv3":

                var _update_r1Formidxlv3 = (TextBox)gvFormListLv3.Rows[e.RowIndex].FindControl("_Update_R1FormNamelv3");
                var _update_formdetailFroml2 = (TextBox)gvFormListLv3.Rows[e.RowIndex].FindControl("tbFormDetailIDXUpdate");
                var _update_r1statuslv3 = (DropDownList)gvFormListLv3.Rows[e.RowIndex].FindControl("ddlupdater2statuslv3");
                var _update_r1FormNamelv3 = (TextBox)gvFormListLv3.Rows[e.RowIndex].FindControl("tbupdate_R1formnameLv3");

                gvFormListLv3.EditIndex = -1;

                _data_qa.qa_r1_form_create_list = new qa_r1_form_create_detail[1];
                qa_r1_form_create_detail _upFormR1lv3 = new qa_r1_form_create_detail();
                _upFormR1lv3.r1_form_create_idx = int.Parse(_update_r1Formidxlv3.Text);
                _upFormR1lv3.form_detail_idx = int.Parse(ViewState["form_idx"].ToString());
                _upFormR1lv3.text_name_setform = _update_r1FormNamelv3.Text;
                _upFormR1lv3.r1_form_create_status = int.Parse(_update_r1statuslv3.SelectedValue);
                _data_qa.qa_r1_form_create_list[0] = _upFormR1lv3;

                _data_qa = callServicePostMasterQA(_urlQaSetR1FormCreate, _data_qa);
                if (_data_qa.return_code == 102)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                else
                {

                }

                getSelectFormLv3List();
                SETFOCUS.Focus();
                break;

            case "gvDataFormListSubset":

                var _update_r1Formidxlv4 = (TextBox)gvDataFormListSubset.Rows[e.RowIndex].FindControl("_Update_R1FormNamelv4");
                var _update_formdetailFroml3 = (TextBox)gvDataFormListSubset.Rows[e.RowIndex].FindControl("tbFormDetailIDXUpdate");
                var _update_r1statuslv4 = (DropDownList)gvDataFormListSubset.Rows[e.RowIndex].FindControl("ddlupdater2statuslv4");
                var _update_r1FormNamelv4 = (TextBox)gvDataFormListSubset.Rows[e.RowIndex].FindControl("tbupdate_R1formnameLv4");

                gvDataFormListSubset.EditIndex = -1;

                _data_qa.qa_r1_form_create_list = new qa_r1_form_create_detail[1];
                qa_r1_form_create_detail _upFormR1lv4 = new qa_r1_form_create_detail();
                _upFormR1lv4.r1_form_create_idx = int.Parse(_update_r1Formidxlv4.Text);
                _upFormR1lv4.form_detail_idx = int.Parse(ViewState["form_idx"].ToString());
                _upFormR1lv4.text_name_setform = _update_r1FormNamelv4.Text;
                _upFormR1lv4.r1_form_create_status = int.Parse(_update_r1statuslv4.SelectedValue);
                _data_qa.qa_r1_form_create_list[0] = _upFormR1lv4;

                _data_qa = callServicePostMasterQA(_urlQaSetR1FormCreate, _data_qa);
                if (_data_qa.return_code == 102)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                else
                {

                }

                getSelectFormLv4List();
                SETFOCUS.Focus();
                break;
        }
    }
    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var gvName = (GridView)sender;
        switch (gvName.ID)
        {
            case "gvMaster":
                gvMaster.EditIndex = -1;
                selectR0FormCreate();
                SETFOCUS.Focus();
                break;

            case "gvR1FormCreate":
                gvR1FormCreate.EditIndex = -1;
                getR1FormList();
                SETFOCUS.Focus();
                break;

            case "gvFormListLv3":
                gvFormListLv3.EditIndex = -1;
                getSelectFormLv3List();
                SETFOCUS.Focus();
                break;

            case "gvDataFormListSubset":
                gvDataFormListSubset.EditIndex = -1;
                getSelectFormLv4List();
                SETFOCUS.Focus();
                break;
        }

    }
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;
        switch (gvName.ID)
        {
            case "gvCreateFormList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                  
                }

                break;

            case "gvMaster":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lbstatus = (Label)e.Row.FindControl("lbstatus");
                    Label Formidx = (Label)e.Row.FindControl("lbFormidx");
                    Label status_offline = (Label)e.Row.FindControl("status_offline");
                    Label status_online = (Label)e.Row.FindControl("status_online");

                    ViewState["status_subForm"] = lbstatus.Text;

                    if (ViewState["status_subForm"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_subForm"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }

              
                }
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox tbtestIDXUpdate = (TextBox)e.Row.FindControl("tbtestIDXUpdate");
                    getM0TestDetailList((DropDownList)e.Row.FindControl("ddlTestTypeUpdate"), tbtestIDXUpdate.Text);
                    setFormData(fvformInsert, FormViewMode.ReadOnly, null);
                }


                break;

            case "gvR1FormCreate":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbR1status = (Label)e.Row.FindControl("lbR1status");
                    // Label Formidx = (Label)e.Row.FindControl("lbFormidx");
                    Label R1status_offline = (Label)e.Row.FindControl("lbstatus_offline");
                    Label R1status_online = (Label)e.Row.FindControl("lbstatus_online");

                    ViewState["status_R1subForm"] = lbR1status.Text;

                    if (ViewState["status_R1subForm"].ToString() == "0")
                    {
                        R1status_offline.Visible = true;
                    }
                    else if (ViewState["status_R1subForm"].ToString() == "1")
                    {
                        R1status_online.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox tbFormDetailIDXUpdate = (TextBox)e.Row.FindControl("tbFormDetailIDXUpdate");
                    getM0FormDetailList((DropDownList)e.Row.FindControl("ddlFormDetailUpdate"), tbFormDetailIDXUpdate.Text);
                    setFormData(fvformInsertRoot, FormViewMode.ReadOnly, null);
                }




                break;

            case "gvFormListLv3":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbR1status = (Label)e.Row.FindControl("lbR1status");
                    // Label Formidx = (Label)e.Row.FindControl("lbFormidx");
                    Label R1status_offline = (Label)e.Row.FindControl("lbstatus_offline");
                    Label R1status_online = (Label)e.Row.FindControl("lbstatus_online");

                    ViewState["status_R1subForm"] = lbR1status.Text;

                    if (ViewState["status_R1subForm"].ToString() == "0")
                    {
                        R1status_offline.Visible = true;
                    }
                    else if (ViewState["status_R1subForm"].ToString() == "1")
                    {
                        R1status_online.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    setFormData(fvInsertRootSubSet, FormViewMode.ReadOnly, null);
                }


                break;

            case "gvDataFormListSubset":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbR1status = (Label)e.Row.FindControl("lbR1status");
                    // Label Formidx = (Label)e.Row.FindControl("lbFormidx");
                    Label R1status_offline = (Label)e.Row.FindControl("lbstatus_offline");
                    Label R1status_online = (Label)e.Row.FindControl("lbstatus_online");

                    ViewState["status_R1subForm"] = lbR1status.Text;

                    if (ViewState["status_R1subForm"].ToString() == "0")
                    {
                        R1status_offline.Visible = true;
                    }
                    else if (ViewState["status_R1subForm"].ToString() == "1")
                    {
                        R1status_online.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    setFormData(fvInsertDataRootSubSet, FormViewMode.ReadOnly, null);
                }


                break;
        }

    }
    #endregion

    #region select 
    protected void selectR0FormCreate()
    {
        _data_qa.qa_r0_form_create_list = new qa_r0_form_create_detail[1];
        qa_r0_form_create_detail _selectForm = new qa_r0_form_create_detail();
        _selectForm.condition = 1;
        _data_qa.qa_r0_form_create_list[0] = _selectForm;
        _data_qa = callServicePostMasterQA(_urlQaGetR0FormCreate, _data_qa);
     
        setGridData(gvMaster, _data_qa.qa_r0_form_create_list);

    }

    protected void getR0FormList()
    {
        setFormData(fvformInsertRoot, FormViewMode.Insert, null);
        TextBox tbFormNameLevel1 = (TextBox)fvformInsertRoot.FindControl("tbFormNameLevel1");

        data_qa _dataQaR0Form = new data_qa();
        _dataQaR0Form.qa_r0_form_create_list = new qa_r0_form_create_detail[1];
        qa_r0_form_create_detail _r0Formlist = new qa_r0_form_create_detail();
        _r0Formlist.condition = 1;
        _dataQaR0Form.qa_r0_form_create_list[0] = _r0Formlist;
        _dataQaR0Form = callServicePostMasterQA(_urlQaGetR0FormCreate, _dataQaR0Form);
        var _linqSetFrom = (from dataqa in _dataQaR0Form.qa_r0_form_create_list
                            where dataqa.r0_form_create_idx == int.Parse(ViewState["r0_form_idx"].ToString())
                            select new
                            {
                                dataqa.r0_form_create_name
                            }).ToList();
        foreach (var item1 in _linqSetFrom)
        {
            tbFormNameLevel1.Text = item1.r0_form_create_name;
        }

    }

    protected void getR1FormList()
    {

        data_qa _dataTopic = new data_qa();
        _dataTopic.qa_r0_form_create_list = new qa_r0_form_create_detail[1];
        qa_r0_form_create_detail _topic = new qa_r0_form_create_detail();

        _topic.r0_form_create_idx = int.Parse(ViewState["r0_form_idx"].ToString());
        _dataTopic.qa_r0_form_create_list[0] = _topic;

        _dataTopic = callServicePostMasterQA(_urlQaGetR1FormCreate, _dataTopic);
        if (_dataTopic.return_code == 0)
        {
            setGridData(gvR1FormCreate, _dataTopic.qa_r1_form_create_list);

        }
    }

    protected void getFormListLv3()
    {
        setFormData(fvInsertRootSubSet, FormViewMode.Insert, null);
        TextBox tbFormNameLevel3 = (TextBox)fvInsertRootSubSet.FindControl("tbFormNameLevel3");

        data_qa _dataQaR0FormLv3 = new data_qa();
        _dataQaR0FormLv3.qa_r0_form_create_list = new qa_r0_form_create_detail[1];
        qa_r0_form_create_detail _r0FormlistLv3 = new qa_r0_form_create_detail();
        _r0FormlistLv3.condition = 1;
        _r0FormlistLv3.r0_form_create_idx = int.Parse(ViewState["r1_form_idx"].ToString());
        _dataQaR0FormLv3.qa_r0_form_create_list[0] = _r0FormlistLv3;
        _dataQaR0FormLv3 = callServicePostMasterQA(_urlQaGetR1FormCreate, _dataQaR0FormLv3);
        tbFormNameLevel3.Text = _dataQaR0FormLv3.qa_r1_form_create_list[0].form_detail_name.ToString();

    }

    protected void getFormListLv4()
    {
        setFormData(fvInsertDataRootSubSet, FormViewMode.Insert, null);
        TextBox tbFormNameLevel4 = (TextBox)fvInsertDataRootSubSet.FindControl("tbFormNameLevel4");

        data_qa _dataQaR0FormLv4 = new data_qa();
        _dataQaR0FormLv4.qa_r0_form_create_list = new qa_r0_form_create_detail[1];
        qa_r0_form_create_detail _r0FormlistLv4 = new qa_r0_form_create_detail();
        _r0FormlistLv4.condition = 1;
        _r0FormlistLv4.r0_form_create_idx = int.Parse(ViewState["r1_form_idx"].ToString());
        _dataQaR0FormLv4.qa_r0_form_create_list[0] = _r0FormlistLv4;
        _dataQaR0FormLv4 = callServicePostMasterQA(_urlQaGetR1FormCreate, _dataQaR0FormLv4);
        tbFormNameLevel4.Text = _dataQaR0FormLv4.qa_r1_form_create_list[0].text_name_setform.ToString();

    }

    protected void getSelectFormLv3List()
    {

        data_qa _dataQaR0Form = new data_qa();
        _dataQaR0Form.qa_r0_form_create_list = new qa_r0_form_create_detail[1];
        qa_r0_form_create_detail _r0Formlist = new qa_r0_form_create_detail();
        _r0Formlist.condition = 2;
        _r0Formlist.r0_form_create_idx = int.Parse(ViewState["r1_form_idx"].ToString());
        _dataQaR0Form.qa_r0_form_create_list[0] = _r0Formlist;
        _dataQaR0Form = callServicePostMasterQA(_urlQaGetR1FormCreate, _dataQaR0Form);

        setGridData(gvFormListLv3, _dataQaR0Form.qa_r1_form_create_list);
       
    }

    protected void getSelectFormLv4List()
    {

        data_qa _dataQaR0Form = new data_qa();
        _dataQaR0Form.qa_r0_form_create_list = new qa_r0_form_create_detail[1];
        qa_r0_form_create_detail _r0Formlistlv4 = new qa_r0_form_create_detail();
        _r0Formlistlv4.condition = 2;
        _r0Formlistlv4.r0_form_create_idx = int.Parse(ViewState["r1_form_idx"].ToString());
        _dataQaR0Form.qa_r0_form_create_list[0] = _r0Formlistlv4;
        _dataQaR0Form = callServicePostMasterQA(_urlQaGetR1FormCreate, _dataQaR0Form);

        setGridData(gvDataFormListSubset, _dataQaR0Form.qa_r1_form_create_list);

    }


    #endregion

    #region reuse
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void initPage()
    {

        //clearSession();
        //clearViewState();

        setActiveView("pageGenaral", 0,0,0);
       // setFormData(fvformInsert, FormViewMode.ReadOnly, null);
      //  selectFormDetail();
    }

    protected void setActiveView(string activeTab, int uidx, int formidx , int IDlastpage)
    {
        mvMaster.SetActiveView((View)mvMaster.FindControl(activeTab));

        switch (activeTab)
        {
            case "pageGenaral":

                break;

        }

    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected data_qa callServicePostMasterQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        // litDebug.Text = _localJson;

        // call services
        // _localJson = _funcTool.callServicePost(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    #endregion
}