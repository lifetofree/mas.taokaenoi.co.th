﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage_recurit.master" AutoEventWireup="true" CodeFile="qmr_problem_supplier_login.aspx.cs" Inherits="websystem_qa_qmr_problem_supplier_login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <style type="text/css">
        .bg-login {
            background-image: url('../../images/qmr-problem/bg_login.png');
            background-size: 100% 100%;
            /*width: 50%;*/
            height: 440pt;
            text-align: center;
        }
    </style>
    <style type="text/css">
        .bg-changelogin {
            background-image: url('../../images/qmr-problem/bg-changelogin.png');
            background-size: 100% 100%;
            /*width: 50%;*/
            height: 440pt;
            text-align: center;
        }
    </style>


<%--     <style type="text/css">
        .bg-login-color {
            background: #ff9999;
            border-color: #ff9999;
        }
    </style>--%>
    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewLogin" runat="server" >
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <div class="container">
                        <div class="row vertical-offset-50">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="col-md-12">
                                    <div class="row bg-login display-flex">
                                        <div class="col-md-8">
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <div class="form-group">
                                                <asp:TextBox ID="txtUser" runat="server" CssClass="form-control input-lg input-lg bg-control" placeholder="Username" MaxLength="8" ValidationGroup="formLogin"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvEmpCode" ValidationGroup="formLogin" runat="server" Display="None" SetFocusOnError="true" ControlToValidate="txtUser" ErrorMessage="กรุณากรอกรหัสผู้ใช้งาน" />
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceEmpCode" TargetControlID="rfvEmpCode" HighlightCssClass="validatorCalloutHighlight" />
                                            </div>


                                            <div class="form-group">
                                                <asp:TextBox ID="txtPass" runat="server" CssClass="form-control input-lg input-lg bg-control" placeholder="Password" MaxLength="20" TextMode="Password" ValidationGroup="formLogin"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvEmpPass" ValidationGroup="formLogin" runat="server" Display="None" SetFocusOnError="true" ControlToValidate="txtPass" ErrorMessage="กรุณากรอกรหัสผ่าน" />
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceEmpPass" TargetControlID="rfvEmpPass" HighlightCssClass="validatorCalloutHighlight" />
                                            </div>

                                            <div class="form-group">
                                                <asp:LinkButton ID="btnLogin" CssClass="btn btn-lg btn-success btn-block" runat="server" data-original-title="Login" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdLogin" Text="Login" ValidationGroup="formLogin" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewChangePassword" runat="server">

            <div class="panel-heading">
                <div class="form-horizontal" role="form">
                    <div class="panel-heading">
                        <div class="container">

                            <div class="row vertical-offset-50">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="col-md-12">
                                        <div class="row bg-changelogin display-flex">
                                            <div class="col-md-8">
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtnewpass" runat="server" CssClass="form-control input-lg bg-control" ValidationGroup="formreset" placeholder="New Password" MaxLength="8"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="formreset" runat="server" Display="None" SetFocusOnError="true" ControlToValidate="txtnewpass" ErrorMessage="กรุณากรอกรหัสผ่านใหม่" />
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="RequiredFieldValidator2" HighlightCssClass="validatorCalloutHighlight" />

                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                        ValidationGroup="formreset" Display="None"
                                                        ErrorMessage="กรุณากรอกรหัสผ่านอย่างน้อย 6-12 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtnewpass"
                                                        ValidationExpression="^[\s\S]{6,12}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator4" Width="160" />

                                                </div>
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtnewpass_con" runat="server" CssClass="form-control input-lg bg-control" ValidationGroup="formreset" placeholder="Confirm New Password" MaxLength="8"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="formreset" runat="server" Display="None" SetFocusOnError="true" ControlToValidate="txtnewpass_con" ErrorMessage="กรุณากรอกข้อมูลยืนยันรหัสผ่านใหม่" />
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="RequiredFieldValidator3" HighlightCssClass="validatorCalloutHighlight" />

                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                        ValidationGroup="formreset" Display="None"
                                                        ErrorMessage="กรุณากรอกรหัสผ่านอย่างน้อย 6-12 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtnewpass_con"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                </div>

                                                <div class="form-group">
                                                    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-lg btn-success btn-block" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdReset" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')" Text="Save" ValidationGroup="formreset" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>

