﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="itswl_m0_company.aspx.cs" Inherits="websystem_MasterData_itswl_m0_company" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">


    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <!-- Start View Index Form -->
        <asp:View ID="ViewIndex" runat="server">

            <div class="col-md-12">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server" CommandName="btnToInsert" OnCommand="btnCommand"><i class="fa fa-plus-square"></i> ข้อมูลบริษัทที่ซื้อ Software</asp:LinkButton>

                <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="company_idx"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="company_idx" runat="server" CssClass="form-control"
                                    Visible="False" Text='<%# Eval("company_idx")%>' />
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">ชื่อบริษัท</label>
                                            <asp:UpdatePanel ID="panelCompanyNameUpdate" runat="server">
                                                <ContentTemplate>

                                                    <asp:TextBox ID="txtCompanyNameUpdate" runat="server" CssClass="form-control"
                                                        Text='<%# Eval("company_name")%>' />
                                                    <asp:RequiredFieldValidator ID="requiredCompanyNameUpdate"
                                                        ValidationGroup="saveCompanyNameUpdate" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txtCompanyNameUpdate"
                                                        Font-Size="1em" ForeColor="Red"
                                                        ErrorMessage="กรุณากรอกชื่อบริษัท" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">สถานที่ติดต่อ</label>
                                            <asp:UpdatePanel ID="panelPlaceNameUpdate" runat="server">
                                                <ContentTemplate>

                                                    <asp:TextBox ID="txtPlaceNameUpdate" runat="server" CssClass="form-control"
                                                        Text='<%# Eval("place_name")%>' />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                        ValidationGroup="saveCompanyNameUpdate" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txtPlaceNameUpdate"
                                                        Font-Size="1em" ForeColor="Red"
                                                        ErrorMessage="กรุณากรอกสถานที่ติดต่อ" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">เบอร์ติดต่อ</label>
                                            <asp:UpdatePanel ID="panelMobileUpdate" runat="server">
                                                <ContentTemplate>

                                                    <asp:TextBox ID="txtMobileUpdate" runat="server" CssClass="form-control"
                                                        Text='<%# Eval("mobile_no")%>' />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                        ValidationGroup="saveCompanyNameUpdate" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txtMobileUpdate"
                                                        Font-Size="1em" ForeColor="Red"
                                                        ErrorMessage="กรุณากรอกเบอร์ติดต่อ" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </small>
                                    </div>
                                </div>



                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">อีเมล์</label>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>

                                                    <asp:TextBox ID="txtEmailUpdate" runat="server" CssClass="form-control"
                                                        Text='<%# Eval("email")%>' />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                        ValidationGroup="saveCompanyNameUpdate" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txtMobileUpdate"
                                                        Font-Size="1em" ForeColor="Red"
                                                        ErrorMessage="กรุณากรอกเบอร์ติดต่อ" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">สถานะ</label>
                                            <asp:DropDownList ID="ddlCompanyStatusUpdate" AutoPostBack="false" runat="server"
                                                CssClass="form-control" SelectedValue='<%# Eval("company_status") %>'>
                                                <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                            </asp:DropDownList>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                            ValidationGroup="saveCompanyNameUpdate" CommandName="Update"
                                            OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                         บันทึก
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                            CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                    </div>
                                </div>
                            </EditItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อบริษัท" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="CompanyName" runat="server" Text='<%# Eval("company_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานที่ติดต่อ" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="PlaceName" runat="server" Text='<%# Eval("place_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เบอร์ติดต่อ" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Mobile_No" runat="server" Text='<%# Eval("mobile_no") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="อีเมล์" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Email" runat="server" Text='<%# Eval("email") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Statuscompany" runat="server" Text='<%# getStatus((int)Eval("company_status")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("company_idx") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>


            </div>




        </asp:View>
        <!-- End View Index Form -->

        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">

            <div class="col-md-12">
                <div class="form-group">
                    <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                        CommandName="btnCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                </div>
            </div>


            <div class="col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading f-bold">เพิ่มข้อมูลบริษัท</div>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <div class="form-group">

                                <asp:Label ID="lbtxtCompanyName" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อบริษัท : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtCompanyName" CssClass="form-control" placeholder="ชื่อบริษัท ..." runat="server"></asp:TextBox>

                                    <asp:RequiredFieldValidator ID="RequiredtxtCompanyNameinsert" ValidationGroup="saveCompany" runat="server" Display="None"
                                        ControlToValidate="txtCompanyName" Font-Size="11"
                                        ErrorMessage="กรุณากรอกชื่อบริษัท"
                                        ValidationExpression="กรุณากรอกชื่อบริษัท" />

                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtxtCompanyNameinsert" Width="160" />

                                </div>

                                <asp:Label ID="lbtxtplace_name" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtplace_name" CssClass="form-control" placeholder="สถานที่ ..." runat="server"></asp:TextBox>

                                    <asp:RequiredFieldValidator ID="Requiredtxtplace_name" ValidationGroup="saveCompany" runat="server" Display="None"
                                        ControlToValidate="txtplace_name" Font-Size="11"
                                        ErrorMessage="กรุณากรอกสถานที่"
                                        ValidationExpression="กรุณากรอกสถานที่" />

                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtplace_name" Width="160" />


                                </div>




                            </div>

                            <div class="form-group">

                                <asp:Label ID="lbtxtmobile_no" CssClass="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtmobile_no" CssClass="form-control" MaxLength="10" placeholder="เบอร์ติดต่อ ..." runat="server"></asp:TextBox>

                                    <asp:RequiredFieldValidator ID="Requiredtxtmobile_no" ValidationGroup="saveCompany" runat="server" Display="None"
                                        ControlToValidate="txtmobile_no" Font-Size="11"
                                        ErrorMessage="กรุณากรอกเบอร์ติดต่อ"
                                        ValidationExpression="กรุณากรอกเบอร์ติดต่อ" />
                                    <asp:RegularExpressionValidator ID="Requiredtxtmobile_no1" runat="server" ValidationGroup="saveCompany" Display="None"
                                        ErrorMessage="กรอกเฉพาะตัวเลขเท่านั้น" Font-Size="11"
                                        ControlToValidate="txtmobile_no"
                                        ValidationExpression="^\d+" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtmobile_no" Width="160" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtmobile_no1" Width="160" />

                                </div>

                                <asp:Label ID="lbtxtemail" CssClass="col-sm-2 control-label" runat="server" Text="อีเมล์ : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtemail" CssClass="form-control" placeholder="อีเมล์ ..." runat="server"></asp:TextBox>

                                    <asp:RequiredFieldValidator ID="Requiredtxtemail" ValidationGroup="saveCompany" runat="server" Display="None"
                                        ControlToValidate="txtemail" Font-Size="11"
                                        ErrorMessage="กรุณากรอกอีเมล์"
                                        ValidationExpression="กรุณากรอกอีเมล์" />
                                    <asp:RegularExpressionValidator ID="Requiredtxtemail1" runat="server" ValidationGroup="saveCompany" Display="None"
                                        ErrorMessage="กรุณากรอกอีเมล์ให้ถูกต้อง" Font-Size="11"
                                        ControlToValidate="txtemail"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtemail" Width="160" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtemail1" Width="160" />
                                </div>



                            </div>

                            <div class="form-group">

                                <asp:Label ID="lbddlCompanyStatus" CssClass="col-sm-2 control-label" runat="server" Text="สถานะ : " />
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlCompanyStatus" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="1" Text="Online" />
                                        <asp:ListItem Value="0" Text="Offline" />
                                    </asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ID="Requiredtxtsoftware_nameinsert" ValidationGroup="Saveinsert" runat="server" Display="None"
                                        ControlToValidate="ddlsoftware_nameinsert" Font-Size="11"
                                        ErrorMessage="กรุณากรอกชื่อ Software"
                                        ValidationExpression="กรุณากรอกชื่อ Software" InitialValue="00" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtsoftware_nameinsert" Width="160" />--%>
                                </div>

                            </div>

                            <%-------------- บันทึก / ยกเลิก --------------%>
                            <div class="form-group">
                                <%--<div class="col-lg-offset-10 col-sm-2">--%>
                                <div class="col-sm-12">
                                    <div class="pull-right">
                                        <asp:LinkButton ID="btnInsert" CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" ValidationGroup="saveCompany">บันทึก</asp:LinkButton>

                                        <%--<asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>--%>
                                    </div>
                                </div>

                            </div>
                            <%-------------- บันทึก / ยกเลิก --------------%>
                        </div>
                    </div>
                </div>


            </div>




            <%-- <div class="row col-md-12">
                <div class="row">--%>
            <%--<div class="col-md-12">
                        <div class="form-group">
                            <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                                CommandName="btnCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                        </div>
                    </div>--%>

            <%--<div class="form-group">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>ชื่อบริษัท</label>
                                <asp:UpdatePanel ID="panelCompanyName" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtCompanyName" runat="server" CssClass="form-control"
                                            placeholder="ชื่อบริษัท ..." />
                                        <asp:RequiredFieldValidator ID="requiredCompanyName"
                                            ValidationGroup="saveCompany" runat="server"
                                            Display="Dynamic"
                                            SetFocusOnError="true"
                                            ControlToValidate="txtCompanyName"
                                            Font-Size="1em" ForeColor="Red"
                                            ErrorMessage="กรุณากรอกชื่อบริษัท"
                                            ValidationExpression="^\d+" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>--%>

            <%--<div class="col-md-6">
                        <div class="form-group">
                            <label>สถานที่</label>
                            <asp:UpdatePanel ID="panelPlaceName" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtplace_name" runat="server" CssClass="form-control"
                                        placeholder="สถานที่ ..." />

                                    <asp:RequiredFieldValidator ID="Requiredplace_name"
                                        ValidationGroup="saveCompany" runat="server"
                                        Display="Dynamic"
                                        SetFocusOnError="true"
                                        ControlToValidate="txtplace_name"
                                        Font-Size="1em" ForeColor="Red"
                                        ErrorMessage="กรุณากรอกชื่อสถานที่" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>--%>
            <%--</div>--%>

            <%--<div class="col-md-6">
                        <div class="form-group">
                            <label>เบอร์ติดต่อ</label>
                            <asp:UpdatePanel ID="panelMobileName" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtmobile_no" runat="server" MaxLength="10" CssClass="form-control"
                                        placeholder="เบอร์ติดต่อ ..." />
                                    <asp:RequiredFieldValidator ID="Requiredmobile_no"
                                        ValidationGroup="saveCompany" runat="server"
                                        Display="Dynamic"
                                        SetFocusOnError="true"
                                        ControlToValidate="txtmobile_no"
                                        Font-Size="1em" ForeColor="Red"
                                        ErrorMessage="กรุณากรอกเบอร์ติดต่อ" />
                                    
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>--%>


            <%--<div class="col-md-6">
                        <div class="form-group">
                            <label>อีเมล์</label>
                            <asp:UpdatePanel ID="panelEmail" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtemail" runat="server" MaxLength="10" CssClass="form-control"
                                        placeholder="อีเมล์ ..." />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                        ValidationGroup="saveCompany" runat="server"
                                        Display="Dynamic"
                                        SetFocusOnError="true"
                                        ControlToValidate="txtemail"
                                        Font-Size="1em" ForeColor="Red"
                                        ErrorMessage="กรุณากรอกอีเมล์ติดต่อ" />

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                        ValidationGroup="saveCompany" runat="server"
                                        Display="Dynamic"
                                        SetFocusOnError="true"
                                        ControlToValidate="txtemail"
                                        Font-Size="1em" ForeColor="Red"
                                        ErrorMessage="กรุณากรอกเฉพาะตัวเลข"
                                        ValidationExpression="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@
(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" />
                                   
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>--%>

            <%--<div class="col-md-6">
                        <div class="form-group">
                            <label>สถานะ</label>
                            <asp:DropDownList ID="ddlCompanyStatus" runat="server" CssClass="form-control">
                                <asp:ListItem Value="1" Text="Online" />
                                <asp:ListItem Value="0" Text="Offline" />
                            </asp:DropDownList>
                        </div>
                    </div>--%>

            <%--<div class="col-md-12">
                        <div class="form-group">
                            <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="saveCompany" />
                        </div>
                    </div>--%>
            <%-- </div>
            </div>--%>
        </asp:View>
        <!-- End Insert Form -->



    </asp:MultiView>


</asp:Content>

