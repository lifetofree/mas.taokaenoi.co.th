﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="nwd_m0_category.aspx.cs" Inherits="websystem_masterdata_nwd_m0_category" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
             <div class="col-md-12">
                 <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server" CommandName="btnToInsert" OnCommand="btnCommand"><i class="fa fa-plus-square"></i> สร้างชนิดอุปกรณ์ Network</asp:LinkButton>

                 <asp:GridView ID="GvMaster"
                       runat="server"
                       AutoGenerateColumns="false"
                       DataKeyNames="category_idx"
                       CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                       HeaderStyle-CssClass="info"
                       AllowPaging="true"
                       PageSize="10"
                       OnRowEditing="Master_RowEditing"
                       OnRowUpdating="Master_RowUpdating"
                       OnRowCancelingEdit="Master_RowCancelingEdit"
                       OnPageIndexChanging="Master_PageIndexChanging"
                       OnRowDataBound="Master_RowDataBound">
                       <PagerStyle CssClass="pageCustom" />
                       <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                       <EmptyDataTemplate>
                          <div style="text-align: center">No result</div>
                       </EmptyDataTemplate>
                       <Columns>
                          <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" 
                             HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                             <ItemTemplate>
                                  <small>
                                <%# (Container.DataItemIndex +1) %>
                                   </small>
                             </ItemTemplate>
                             <EditItemTemplate>
                                <asp:TextBox ID="category_idx" runat="server" CssClass="form-control"
                                   Visible="False" Text='<%# Eval("category_idx")%>' />

                                 <div class="col-md-6">
                                   <div class="form-group">
                                      <small>
                                      <label class="pull-left">ชื่อประเภทอุปกรณ์</label>
                                      <asp:Label ID="lbddlTypeName" runat="server" Text='<%# Bind("type_idx") %>' Visible="false" />
                                      <asp:DropDownList ID="ddlTypeNameUpdate" AutoPostBack="true" runat="server"
                                         CssClass="form-control">                                        
                                      </asp:DropDownList>
                                      </small>
                                   </div>
                                </div>



                                <div class="col-md-6">
                                   <div class="form-group">
                                      <small>
                                      <label class="pull-left">ชื่อชนิดอุปกรณ์</label>
                                      <asp:UpdatePanel ID="panelTypeUpdate" runat="server">
                                         <ContentTemplate>
                                            
                                            <asp:TextBox ID="txtCategoryNameUpdate" runat="server" CssClass="form-control"
                                               Text='<%# Eval("category_name")%>' />
                                            <asp:RequiredFieldValidator ID="requiredCategoryNameUpdate"
                                               ValidationGroup="saveTypeNameUpdate" runat="server"
                                               Display="Dynamic"
                                               SetFocusOnError="true"
                                               ControlToValidate="txtCategoryNameUpdate"
                                               Font-Size="1em" ForeColor="Red"
                                               ErrorMessage="กรุณากรอกชื่อชนิดอุปกรณ์" />
                                         </ContentTemplate>
                                      </asp:UpdatePanel>
                                      </small>
                                   </div>
                                </div>
                                
                                <div class="col-md-6">
                                   <div class="form-group">
                                      <small>
                                      <label class="pull-left">สถานะ</label>
                                      <asp:DropDownList ID="ddlCategoryStatusUpdate" AutoPostBack="false" runat="server"
                                         CssClass="form-control" SelectedValue='<%# Eval("category_status") %>'>
                                         <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                         <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                      </asp:DropDownList>
                                      </small>
                                   </div>
                                </div>

                                <div class="col-md-12">
                                   <div class="pull-left">
                                      <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                         ValidationGroup="saveTypeNameUpdate" CommandName="Update"
                                         OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                         บันทึก
                                      </asp:LinkButton>
                                      <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                         CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                   </div>
                                </div>
                             </EditItemTemplate>
                          </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left"
                             HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                             <ItemTemplate>
                                <small>
                                   <asp:Label ID="TypeName" runat="server" Text='<%# Eval("type_name") %>' />
                                </small>
                             </ItemTemplate>
                             <EditItemTemplate />
                             <FooterTemplate />
                          </asp:TemplateField>

                           <asp:TemplateField HeaderText="ชื่อชนิดอุปกรณ์" ItemStyle-HorizontalAlign="Left"
                             HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                             <ItemTemplate>
                                <small>
                                   <asp:Label ID="CategoryName" runat="server" Text='<%# Eval("category_name") %>' />
                                </small>
                             </ItemTemplate>
                             <EditItemTemplate />
                             <FooterTemplate />
                          </asp:TemplateField>

                           <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                             HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                             <ItemTemplate>
                                <small>
                                   <asp:Label ID="Statuscategory" runat="server" Text='<%# getStatus((int)Eval("category_status")) %>' />
                                </small>
                             </ItemTemplate>
                             <EditItemTemplate />
                             <FooterTemplate />
                          </asp:TemplateField>

                           <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                             HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                             <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                   data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                   title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                   CommandArgument='<%# Eval("category_idx") %>'
                                   OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                             </ItemTemplate>
                             <EditItemTemplate />
                             <FooterTemplate />
                          </asp:TemplateField>
                            
                       </Columns>
                    </asp:GridView>

             </div>

        </asp:View>

      <!-- Start Insert Form -->
      <asp:View ID="ViewInsert" runat="server">
         <div class="row col-md-12">
            <div class="row">
               <div class="col-md-12">
                  <div class="form-group">
                     <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                        CommandName="btnCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                  </div>
               </div>

                <div class="col-md-6">
                  <div class="form-group">
                     <label>ชื่อประเภทอุปกรณ์</label>
                     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                          <asp:DropDownList ID="ddltype" runat="server" CssClass="form-control">           </asp:DropDownList>
                           <asp:RequiredFieldValidator ID="requiredddltype"
                              ValidationGroup="save" runat="server"
                              Display="Dynamic"
                              SetFocusOnError="true"
                              ControlToValidate="ddltype"
                              Font-Size="1em" ForeColor="Red"
                              InitialValue="00"
                              ErrorMessage="กรุณากรอกประเภทอุปกรณ์" />
                        </ContentTemplate>
                     </asp:UpdatePanel>
                  </div>
               </div>



               <div class="col-md-6">
                  <div class="form-group">
                     <label>ชื่อชนิดอุปกรณ์</label>
                     <asp:UpdatePanel ID="panelcategoryName" runat="server">
                        <ContentTemplate>
                           <asp:TextBox ID="txtcategoryName" runat="server" CssClass="form-control"
                              placeholder="ชื่อชนิดอุปกรณ์..." />
                           <asp:RequiredFieldValidator ID="requiredcategoryName"
                              ValidationGroup="save" runat="server"
                              Display="Dynamic"
                              SetFocusOnError="true"
                              ControlToValidate="txtcategoryName"
                              Font-Size="1em" ForeColor="Red"
                              ErrorMessage="กรุณากรอกชื่อชนิดอุปกรณ์" />
                        </ContentTemplate>
                     </asp:UpdatePanel>
                  </div>
               </div>
               
               <div class="col-md-12">
                  <div class="form-group">
                     <label>สถานะ</label>
                     <asp:DropDownList ID="ddlcategoryStatus" runat="server" CssClass="form-control">
                        <asp:ListItem Value="1" Text="Online" />
                        <asp:ListItem Value="0" Text="Offline" />
                     </asp:DropDownList>
                  </div>
               </div>              

               <div class="col-md-12">
                  <div class="form-group">
                     <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="save" />
                  </div>
               </div>
            </div>
         </div>
      </asp:View>
      <!-- End Insert Form -->

    </asp:MultiView>

</asp:Content>
