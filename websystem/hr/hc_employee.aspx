﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hc_employee.aspx.cs" Inherits="websystem_hr_hc_employee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>
    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">

                        <li id="li0" runat="server" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ข้อมูลส่วนตัว <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <asp:LinkButton ID="lbDetailHistoryList" runat="server" CommandName="cmdDocdetailList" OnCommand="btnCommand" CommandArgument="1"> ประวัติส่วนตัว</asp:LinkButton>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <asp:LinkButton ID="lbDetailHistoryWork" runat="server" CommandName="cmdDocdetailList" OnCommand="btnCommand" CommandArgument="2"> ประวัติการทำงาน</asp:LinkButton>
                                </li>

                            </ul>
                        </li>

                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbCreateList" runat="server" CommandName="cmdCreateList" OnCommand="navCommand" CommandArgument="docCreateList"> กรอกรายละเอียด</asp:LinkButton>
                        </li>

                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbReportList" runat="server" CommandName="cmdReportList" OnCommand="navCommand" CommandArgument="docReportList"> รายงาน</asp:LinkButton>
                        </li>

                        <%-- <li id="li3" runat="server" class="dropdown">
                          
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Master Data <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <asp:LinkButton ID="lbMasterList" runat="server" CommandName="cmdMasterList" OnCommand="navCommand" CommandArgument="docMasterList"> ข้อมูลแพทย์</asp:LinkButton>
                                </li>--%>
                        <%--<li role="separator" class="divider"></li>--%>

                        <%--<asp:LinkButton ID="lbMasterList" runat="server" CommandName="cmdMasterList" OnCommand="navCommand" CommandArgument="docMasterList"> Master Data</asp:LinkButton>--%>
                        <%-- </ul>
                        </li>--%>
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->
            </div>
        </nav>
    </div>
    <!--tab menu-->

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    </div>

    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Detail-->
        <asp:View ID="docDetailList" runat="server">

            <div id="divDetailHistory" runat="server" class="col-md-12" visible="false">
                <asp:FormView ID="fvHistoryEmployee" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <div class="panel panel-primary ">
                            <div class="panel-heading">
                                <h3 class="panel-title">ข้อมูลส่วนตัว</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode_History" runat="server" CssClass="form-control" Text='<%# Eval("EmpCode") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName_History" runat="server" CssClass="form-control" Text='<%# Eval("FullNameTH") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">วัน เดือน ปี เกิด</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbBirthday_History" runat="server" CssClass="form-control" Text='<%# Eval("Birthday_Ex") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">เพศ</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbSexNameTH_History" runat="server" CssClass="form-control" Text='<%# Eval("SexNameTH") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">วันที่เข้างาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpIN_Ex_History" runat="server" CssClass="form-control" Text='<%# Eval("EmpIN_Ex") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">เลขที่บัตรประชาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbIdentityCard_History" runat="server" CssClass="form-control" Text='<%# Eval("IdentityCard") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <hr />

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">ที่อยู่ตามบัตรประชาชน</h3>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ที่อยู่ตามบัตรประชาชน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_EmpAddr_History" runat="server" CssClass="form-control" Text='<%# Eval("EmpAddr") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำบล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDistName_History" runat="server" CssClass="form-control" Text='<%# Eval("DistName") %>' Enabled="false" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">อำเภอ</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbAmpName_History" runat="server" CssClass="form-control" Text='<%# Eval("AmpName") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">จังหวัด</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbProvName_History" runat="server" CssClass="form-control" Text='<%# Eval("ProvName") %>' Enabled="false" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสไปรษณีย์</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_PostCode_History" runat="server" CssClass="form-control" Text='<%# Eval("PostCode") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">โทรศัพท์</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbMobileNo_History" runat="server" CssClass="form-control" Text='<%# Eval("MobileNo") %>' Enabled="false" />
                                        </div>

                                    </div>

                                    <hr />
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">ที่อยู่ที่สามารถติดต่อได้</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-sm-9 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ที่อยู่ปัจจุบัน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpAddrPresent_History" runat="server" CssClass="form-control" Text='<%# Eval("EmpAddr") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำบล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDistNamePresent_History" runat="server" CssClass="form-control" Text='<%# Eval("DistName") %>' Enabled="false" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">อำเภอ</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbAmpNamePresent_History" runat="server" CssClass="form-control" Text='<%# Eval("AmpName") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">จังหวัด</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbProvNamePresent_History" runat="server" CssClass="form-control" Text='<%# Eval("ProvName") %>' Enabled="false" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสไปรษณีย์</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbPostCodePresent_History" runat="server" CssClass="form-control" Text='<%# Eval("PostCode") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">โทรศัพท์</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbMobileNoPresent_History" runat="server" CssClass="form-control" Text='<%# Eval("MobileNo") %>' Enabled="false" />
                                        </div>

                                    </div>

                                    <hr />

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">สถานประกอบการ</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-sm-9 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ที่อยู่</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbOrgAddress_History" runat="server" CssClass="form-control" Text='<%# Eval("OrgAddress") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">รหัสไปรษณีย์</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbPostCode_Org_History" runat="server" CssClass="form-control" Text='<%# Eval("PostCode_Org") %>' Enabled="false" />
                                        </div>

                                    </div>

                                </div>

                            </div>
                    </ItemTemplate>

                </asp:FormView>

            </div>

        </asp:View>
        <!--View Detail-->

        <!--View Create-->
        <asp:View ID="docCreateList" runat="server">

            <div id="divCreate" runat="server" class="col-md-12" visible="false">

                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <%-- <label class="col-sm-2 control-label">องค์กร</label>--%>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <%--  <label class="col-sm-2 control-label">ฝ่าย</label>--%>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <%-- <label class="col-sm-2 control-label">แผนก</label>--%>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <%-- <label class="col-sm-2 control-label">ตำแหน่ง</label>--%>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>

                <asp:FormView ID="fvInsertHistoryHealth" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">รูปแบบการบันทึกรายการ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <%--   <label class="col-sm-2 control-label">รูปแบบรายการ</label>--%>
                                        <div class="col-sm-2">
                                            <label>รูปแบบรายการ</label>
                                            <%-- <asp:Label ID="lbtextformlist" runat="server" CssClass="control-label pull-right" Font-Bold="true">รูปแบบรายการ</asp:Label>--%>
                                        </div>
                                        <div class="col-sm-10">
                                            <asp:RadioButtonList ID="rdoHistoryType" runat="server" RepeatLayout="Table" CssClass="radio-list-inline-emps" Font-Size="14" CellPadding="0" Width="50%" RepeatColumns="2" RepeatDirection="Vertical" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged">
                                            </asp:RadioButtonList>
                                            <%--<asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <asp:Panel ID="PnCreate_EmployeeHistory" Visible="false" runat="server">

                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label class="f-s-13">รหัสพนักงาน</label>
                                                <asp:TextBox ID="txtEmpIDXHidden" runat="server" Visible="false" />
                                                <asp:TextBox ID="txtEmpNewEmpCode" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน..." OnTextChanged="onTextChanged" AutoPostBack="true" MaxLength="8" autocomplete="off" />
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="f-s-13">ชื่อ - นามสกุล</label>
                                                <%--<asp:TextBox ID="txtEmpNewFullNameTHHidden" runat="server" Visible="false" />--%>
                                                <asp:TextBox ID="txtEmpNewFullNameTH" runat="server" CssClass="form-control" placeholder="ชื่อ - สกุล (TH)..." Enabled="false" />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label class="f-s-13">บริษัท</label>
                                                <asp:TextBox ID="txtOrgIDXHidden" runat="server" Visible="false" />
                                                <asp:TextBox ID="txtEmpNewOrgNameTH" runat="server" CssClass="form-control" placeholder="บริษัท..." Enabled="false" />
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="f-s-13">ฝ่าย</label>
                                                <asp:TextBox ID="txtRdepIDXHidden" runat="server" Visible="false" />
                                                <asp:TextBox ID="txtEmpNewDeptNameTH" runat="server" CssClass="form-control" placeholder="ฝ่าย..." Enabled="false" />
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label class="f-s-13">แผนก</label>
                                                <asp:TextBox ID="txtEmpNewSecNameTH" runat="server" CssClass="form-control" placeholder="แผนก..." Enabled="false" />
                                            </div>

                                            <div class="col-sm-6">
                                                <label class="f-s-13">ตำแหน่ง</label>
                                                <asp:TextBox ID="txtRposIDXHidden" runat="server" Visible="false" />
                                                <asp:TextBox ID="txtEmpNewPosNameTH" runat="server" CssClass="form-control" placeholder="ตำแหน่ง..." Enabled="false" />
                                            </div>

                                        </div>

                                    </asp:Panel>


                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>
                </asp:FormView>

                <asp:FormView ID="fvInsertHistoryHealthSick" runat="server" OnDataBound="fvOnDataBound" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>
                        <div class="row">

                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h3 class="panel-title">แบบฟอร์มการกรอกบันทึกรายการ</h3>
                                </div>
                                <div class="panel-body">
                                    <h3 class="text-center">
                                        <asp:Label ID="lbl_title_type" Text="ประวัติการเจ็บป่วย" runat="server" /></h3>
                                    <hr />

                                    <asp:TextBox ID="tb" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("m0_type_idx") %>' Enabled="false" />
                                    <!-- เคยป่วยเป้นโรคหรือบาดเจ็บ -->
                                    <asp:Panel ID="pnDiseaseRoInjury" Visible="true" runat="server">

                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lb_name_topic_1" runat="server" Font-Bold="true" CssClass="control-label"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txt_input_disease" placeholder="ระบุข้อมูล..." runat="server" Visible="true" CssClass="form-control" Enabled="true" />
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <label>เมื่อปี พ.ศ.</label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txt_input_year" placeholder="ระบุปี..." runat="server" Visible="true" CssClass="form-control" Enabled="true" />
                                                </div>
                                            </div>

                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <asp:LinkButton ID="btn_insert_disease" CssClass="btn btn-primary" runat="server" data-original-title="Insert" data-toggle="tooltip" Visible="true"
                                                        OnCommand="btnCommand" CommandName="cmdBackToDocList" title=""><i class="glyphicon glyphicon-plus-circle" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group" id="div_gvDiseaseRoInjuryList" runat="server" visible="false">
                                                    <asp:GridView ID="gvDiseaseRoInjuryList"
                                                        runat="server"
                                                        CssClass="table table-striped table-responsive"
                                                        GridLines="None"
                                                        OnRowCommand="onRowCommand"
                                                        AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                <ItemTemplate>
                                                                    <%# (Container.DataItemIndex + 1) %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="drDiseaseRoInjuryText" HeaderText="เคยป่วยเป็นโรคหรือมีการบาดเจ็บ" ItemStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />
                                                            <asp:BoundField DataField="drYearText" HeaderText="เมื่อปี พ.ศ." ItemStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                HeaderStyle-CssClass="text-center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnRemovePlacePer" runat="server"
                                                                        CssClass="btn btn-danger btn-xs"
                                                                        OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                        CommandName="btnRemovePlacePer">
                                                               <i class="fa fa-times"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>

                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <!-- เคยป่วยเป้นโรคหรือบาดเจ็บ -->

                                    <!-- โรคประจำตัว -->
                                    <asp:Panel ID="pnCongenitalDisease" Visible="true" runat="server">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lb_name_topic_2" runat="server" Font-Bold="true" CssClass="control-label"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:RadioButtonList ID="rdoCongenitalDisease" runat="server" RepeatColumns="2" RepeatLayout="Table" CssClass="radio-list-inline-emps" Font-Size="12" CellPadding="0" Width="100%" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged">
                                                        <asp:ListItem Value="0">ไม่มี</asp:ListItem>
                                                        <asp:ListItem Value="1">มี</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtCongenitalDisease" runat="server" Visible="false" placeholder="ระบุโรคประจำตัวหรือโรคเรื้อรัง..." CssClass="form-control" Enabled="true" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <!-- โรคประจำตัว -->

                                    <!-- การผ่าตัด -->
                                    <asp:Panel ID="pnSurgery" Visible="true" runat="server">
                                        <div class="col-md-12">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lb_name_topic_3" runat="server" Font-Bold="true" CssClass="control-label"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:RadioButtonList ID="rdoSurgery" runat="server" RepeatColumns="2" RepeatLayout="Table" CssClass="radio-list-inline-emps" Font-Size="12" CellPadding="0" Width="100%" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged">
                                                        <asp:ListItem Value="0">ไม่เคย</asp:ListItem>
                                                        <asp:ListItem Value="1">เคย</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>

                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtSurgery" runat="server" Visible="false" placeholder="ระบุการผ่าตัด..." CssClass="form-control" Enabled="true" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <!-- การผ่าตัด -->

                                    <!-- ภูมิคุ้มกัน -->
                                    <asp:Panel ID="pnImmunity" Visible="true" runat="server">
                                        <div class="col-md-12">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lb_name_topic_4" runat="server" Font-Bold="true" CssClass="control-label"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:RadioButtonList ID="rdoImmunity" runat="server" RepeatColumns="2" RepeatLayout="Table" CssClass="radio-list-inline-emps" Font-Size="12" CellPadding="0" Width="100%" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged">
                                                        <asp:ListItem Value="0">ไม่เคย</asp:ListItem>
                                                        <asp:ListItem Value="1">เคย</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>

                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtImmunity" runat="server" Visible="false" placeholder="ระบุข้อมูล..." CssClass="form-control" Enabled="true" />
                                                </div>
                                            </div>
                                        </div>

                                    </asp:Panel>
                                    <!-- ภูมิคุ้มกัน -->

                                    <!-- การเจ็บป่วยของสมาชิกในครอบครัว -->
                                    <asp:Panel ID="pnSickFamily" Visible="true" runat="server">
                                        <div class="col-md-12">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lbl_name_topic_5" runat="server" Font-Bold="true" CssClass="control-label"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:RadioButtonList ID="rdoSickFamily" runat="server" RepeatColumns="2" RepeatLayout="Table" CssClass="radio-list-inline-emps" Font-Size="12" CellPadding="0" Width="100%" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged">
                                                        <asp:ListItem Value="0">ไม่มี</asp:ListItem>
                                                        <asp:ListItem Value="1">มี</asp:ListItem>
                                                    </asp:RadioButtonList>

                                                </div>
                                            </div>

                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <asp:Label ID="lbl_name_sicktopic_6" runat="server" Visible="true" Text="ระบุความสัมพันธ์และโรค" CssClass="control-label" Enabled="true" />

                                                </div>
                                            </div>

                                            <asp:UpdatePanel ID="updatePlaceInsert" runat="server">
                                                <ContentTemplate>
                                                    <div class="col-md-12">
                                                        <div id="div_DetailPlace" class="panel panel-default" runat="server">
                                                            <div class="panel-body">

                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label>ความสัมพันธ์</label>
                                                                        <asp:TextBox ID="txt_relation_family" placeholder="ระบุความสัมพันธ์..." runat="server" Visible="true" CssClass="form-control" Enabled="true" />
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label>โรค</label>
                                                                        <asp:TextBox ID="txt_Disease_family" placeholder="ระบุชื่อโรค..." runat="server" Visible="true" CssClass="form-control" Enabled="true" />

                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label>&nbsp;</label>
                                                                        <div class="clearfix"></div>
                                                                        <asp:LinkButton ID="btn_InsertRelationFamily" CssClass="btn btn-primary" runat="server" data-original-title="Insert"
                                                                            data-toggle="tooltip" Visible="true"
                                                                            OnCommand="btnCommand" CommandName="cmdInsertRelationFamily" title="">
                                                                                <i class="glyphicon glyphicon-plus-circle" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <asp:GridView ID="gvDiseaseFamilyList"
                                                                        runat="server"
                                                                        CssClass="table table-striped table-responsive"
                                                                        GridLines="None"
                                                                        OnRowCommand="onRowCommand"
                                                                        AutoGenerateColumns="false">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-CssClass="text-center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                                <ItemTemplate>
                                                                                    <%# (Container.DataItemIndex + 1) %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="drPlacePerText" HeaderText="ความสัมพันธ์" ItemStyle-CssClass="text-center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                            <asp:BoundField DataField="drPlacePerText" HeaderText="โรค" ItemStyle-CssClass="text-center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-CssClass="text-center">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnRemoveRelation" runat="server"
                                                                                        CssClass="btn btn-danger btn-xs"
                                                                                        OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                                        CommandName="btnRemovePlacePer"><i class="fa fa-times"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                                <div class="clearfix"></div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                    </asp:Panel>
                                    <!-- การเจ็บป่วยของสมาชิกในครอบครัว -->

                                    <!-- ยาที่ทานประจำ -->
                                    <asp:Panel ID="PnDrug" Visible="true" runat="server">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lbl_name_topic_6" runat="server" Font-Bold="true" CssClass="control-label"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:RadioButtonList ID="rdoDrug" runat="server" RepeatColumns="2" RepeatLayout="Table" CssClass="radio-list-inline-emps" Font-Size="12" CellPadding="0" Width="100%" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged">
                                                        <asp:ListItem Value="0">ไม่มี</asp:ListItem>
                                                        <asp:ListItem Value="1">มี</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtDrug" runat="server" Visible="false" placeholder="ระบุยาที่รับประทานเป็นประจำ..." CssClass="form-control" Enabled="true" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <!-- ยาที่ทานประจำ -->

                                    <!-- มีประวัติการแพ้ยาหรือไม่ -->
                                    <asp:Panel ID="PnAllergic" Visible="true" runat="server">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lbl_name_topic_7" runat="server" Font-Bold="true" CssClass="control-label"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:RadioButtonList ID="rdoAllergic" runat="server" RepeatColumns="2" RepeatLayout="Table" CssClass="radio-list-inline-emps" Font-Size="12" CellPadding="0" Width="100%" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged">
                                                        <asp:ListItem Value="0">ไม่มี</asp:ListItem>
                                                        <asp:ListItem Value="1">มี</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtAllergic" runat="server" Visible="false" placeholder="ระบุประวัติการแพ้ยา..." CssClass="form-control" Enabled="true" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <!-- มีประวัติการแพ้ยาหรือไม่ -->

                                    <!-- สูบบุหรี่ -->
                                    <asp:Panel ID="PnSmoking" Visible="true" runat="server">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lbl_name_topic_8" runat="server" Font-Bold="true" CssClass="control-label"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:RadioButtonList ID="rdoSmoking" runat="server" RepeatColumns="2" RepeatLayout="Table" CssClass="radio-list-inline-emps" Font-Size="12" CellPadding="0" Width="100%" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged">
                                                        <asp:ListItem Value="0">ไม่มี</asp:ListItem>
                                                        <asp:ListItem Value="1">มี</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <asp:TextBox ID="TextBox1" runat="server" Visible="false" placeholder="ระบุประวัติการแพ้ยา..." CssClass="form-control" Enabled="true" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                     <!-- สูบบุหรี่ -->


                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
            </div>

        </asp:View>
        <!--View Create-->

        <!--View Create-->
        <asp:View ID="docReportList" runat="server">
            report

        </asp:View>
        <!--View Create-->

    </asp:MultiView>


</asp:Content>

