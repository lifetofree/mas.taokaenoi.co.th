﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_rcm_m0_setquestion : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    data_employee _dtEmployee = new data_employee();
    function_tool _funcTool = new function_tool();
    data_rcm_question _dtrcm = new data_rcm_question();

    string _localJson = String.Empty;

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string extension_q;
    string extension_1;
    string extension_2;
    string extension_3;
    string extension_4;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];

    static string _urlSelectm0typequest = _serviceUrl + ConfigurationManager.AppSettings["urlSelectm0typequest"];
    static string _urlInsertm0topicquest = _serviceUrl + ConfigurationManager.AppSettings["urlInsertm0topicquest"];
    static string _urlSelectm0topicquest = _serviceUrl + ConfigurationManager.AppSettings["urlSelectm0topicquest"];
    static string _urlSelectm0typetopic = _serviceUrl + ConfigurationManager.AppSettings["urlSelectm0typetopic"];
    static string _urlInsertm0quest = _serviceUrl + ConfigurationManager.AppSettings["urlInsertm0quest"];

    static string _urlSelectTypeansQuest = _serviceUrl + ConfigurationManager.AppSettings["urlSelectTypeansQuest"];
    static string _urlSelectQuestion = _serviceUrl + ConfigurationManager.AppSettings["urlSelectQuestion"];
    static string _urlDeleteQuestion = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteQuestion"];
    static string _urlSelectNoChoiceQuestion = _serviceUrl + ConfigurationManager.AppSettings["urlSelectNoChoiceQuestion"];
    static string _urlGetselect_ma_positionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetselect_ma_positionList"];
    static string _urlSelect_PersonRespon = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_PersonRespon"];
    static string _urlDeletePerson_Responsibility = _serviceUrl + ConfigurationManager.AppSettings["urlDeletePerson_Responsibility"];
    static string _urlInsertAddonPersonal = _serviceUrl + ConfigurationManager.AppSettings["urlInsertAddonPersonal"];
    //Data_ScoreLogic_list exam
    static string _urlGetlistscoreexam = _serviceUrl + ConfigurationManager.AppSettings["urlGetlistscoreexam"];
    static string _urlGetlistscoreexam_detail = _serviceUrl + ConfigurationManager.AppSettings["urlGetlistscoreexam_detail"];
    static string _urlGetDataM0Exam = _serviceUrl + ConfigurationManager.AppSettings["urlGetDataM0Exam"];
    static string _urlSetscore_comment_exam = _serviceUrl + ConfigurationManager.AppSettings["urlSetscore_comment_exam"];

    string topic = "0";
    HttpPostedFile UploadedImage_q;
    HttpPostedFile UploadedImage_a;
    HttpPostedFile UploadedImage_b;
    HttpPostedFile UploadedImage_c;
    HttpPostedFile UploadedImage_d;
    HttpPostedFile file_delImage;
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();
            ViewState["Search_Data"] = "0";
            ViewState["topic"] = "0";
            ViewState["path_q"] = "0";
            ViewState["path_1"] = "0";
            ViewState["path_2"] = "0";
            ViewState["path_3"] = "0";
            ViewState["path_4"] = "0";

            ViewState["typequest_search"] = "0";
            ViewState["orgidx_search"] = "0";
            ViewState["rdeptidx_search"] = "0";
            ViewState["rsecidx_search"] = "0";
            ViewState["rpos_search"] = "0";

            if (Request.QueryString["page"] != null)
            {
                switch (Request.QueryString["page"])
                {
                    case "check_score":
                        SetDefaultpage(4);
                        break;
                    default:
                        SetDefaultpage(1);
                        break;
                }
                if (Request.QueryString["cid"] != null)
                {
                    txt_src_score.Text = Request.QueryString["cid"].ToString();

                    object empty_o = string.Empty;
                    CommandEventArgs args = new CommandEventArgs("btn_src_score", empty_o);

                    btnCommand(empty_o, args);
                }
            }
            else
            {
                SetDefaultpage(1);
            }




        }
        linkBtnTrigger(_divMenuBtnToDivIndex);
        linkBtnTrigger(_divMenuBtnToDivAdd);
        linkBtnTrigger(_divMenuBtnToDivAddScore);
        linkBtnTrigger(_divMenuBtnToDivManual);

    }
    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void getOrganizationList(DropDownList ddlName)
    {

        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmp(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dtEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dtEmployee.section_list[0] = _secList;

        _dtEmployee = callServicePostEmp(_urlGetSectionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกแผนก...", "0"));
    }

    protected void getPositionList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dtEmployee.position_list = new position_details[1];
        position_details _positionList = new position_details();
        _positionList.org_idx = _org_idx;
        _positionList.rdept_idx = _rdept_idx;
        _positionList.rsec_idx = _rsec_idx;
        _dtEmployee.position_list[0] = _positionList;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
        _dtEmployee = callServicePostEmp(_urlGetPositionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกตำแหน่ง....", "0"));
    }

    protected void getEmpList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail _empList = new employee_detail();
        _empList.org_idx = _org_idx;
        _empList.rdept_idx = _rdept_idx;
        _empList.rsec_idx = _rsec_idx;
        _empList.emp_status = 1;
        _dtEmployee.employee_list[0] = _empList;


        _dtEmployee = callServicePostEmp(urlGetAll, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.employee_list, "emp_name_th", "emp_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกชื่อผู้รับผิดชอบ...", "0"));
    }

    protected void gettypequestion(DropDownList ddlName)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_typequest = new m0_typequest[1];
        m0_typequest _typequest = new m0_typequest();

        _dtrcm.Boxm0_typequest[0] = _typequest;
        //        txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectm0typequest, _dtrcm);
        setDdlData(ddlName, _dtrcm.Boxm0_typequest, "type_quest", "m0_tqidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทคำถาม...", "0"));
    }

    protected void select_gettypequestion(GridView gvName, int tqidx)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_typequest = new m0_typequest[1];
        m0_typequest _typequest = new m0_typequest();

        _typequest.m0_tqidx = tqidx;
        _dtrcm.Boxm0_typequest[0] = _typequest;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectm0typequest, _dtrcm); //_urlSelectm0typequest
        setGridData(gvName, _dtrcm.Boxm0_typequest);

    }

    protected void gettopicquestion(DropDownList ddlName, int m0_tqidx, int orgidx, int rdeptidx, int rsecidx, int rposidx)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_topicquest = new m0_topicquest[1];
        m0_topicquest _topicquest = new m0_topicquest();

        _topicquest.m0_tqidx = m0_tqidx;
        _topicquest.orgidx = orgidx;
        _topicquest.rdeptidx = rdeptidx;
        _topicquest.rsecidx = rsecidx;
        _topicquest.rposidx = rposidx;

        _dtrcm.Boxm0_topicquest[0] = _topicquest;
        //        txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectm0topicquest, _dtrcm);
        setDdlData(ddlName, _dtrcm.Boxm0_topicquest, "topic_name", "m0_toidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกชุดคำถาม...", "0"));
    }

    protected void select_gettopicquestion(GridView gvName, int m0_tqidx, int orgidx, int rdeptidx, int rsecidx, int rposidx)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_topicquest = new m0_topicquest[1];
        m0_topicquest _topicquest = new m0_topicquest();

        _topicquest.m0_tqidx = m0_tqidx;
        _topicquest.orgidx = orgidx;
        _topicquest.rdeptidx = rdeptidx;
        _topicquest.rsecidx = rsecidx;
        _topicquest.rposidx = rposidx;

        _dtrcm.Boxm0_topicquest[0] = _topicquest;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectm0topicquest, _dtrcm);
        setGridData(gvName, _dtrcm.Boxm0_topicquest);


    }

    protected void select_person_responsibility(GridView gvName, int m0_toidx)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_topicquest = new m0_topicquest[1];
        m0_topicquest _person = new m0_topicquest();

        _person.m0_toidx = m0_toidx;

        _dtrcm.Boxm0_topicquest[0] = _person;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelect_PersonRespon, _dtrcm);
        setGridData(gvName, _dtrcm.Boxm1_personlist);


    }

    protected void gettypeanswer(DropDownList ddlName)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_typeanswer = new m0_typeanswer[1];
        m0_typeanswer _typeanswer = new m0_typeanswer();

        _dtrcm.Boxm0_typeanswer[0] = _typeanswer;
        //        txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectm0typetopic, _dtrcm);
        setDdlData(ddlName, _dtrcm.Boxm0_typeanswer, "type_asn", "m0_taidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทคำถาม...", "0"));
    }

    protected void select_gettypeanswer(GridView gvName, int m0_toidx)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_typeanswer = new m0_typeanswer[1];
        m0_typeanswer _typeans = new m0_typeanswer();

        _typeans.m0_toidx = m0_toidx;

        _dtrcm.Boxm0_typeanswer[0] = _typeans;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectTypeansQuest, _dtrcm);
        setGridData(gvName, _dtrcm.Boxm0_typeanswer);
    }

    protected void select_getquestion(GridView gvName, int m0_toidx, int m0_taidx)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_question = new m0_question[1];
        m0_question _quest = new m0_question();

        _quest.m0_toidx = m0_toidx;
        _quest.m0_taidx = m0_taidx;
        _quest.condition = 2;
        _dtrcm.Boxm0_question[0] = _quest;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectQuestion, _dtrcm);
        setGridData(gvName, _dtrcm.Boxm0_question);
    }

    protected void SelectMaPositionList(DropDownList ddlName)
    {
        _dtEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        _dtEmployee.BoxManage_Position_list[0] = dtemployee;

        _dtEmployee = callServicePostEmp(_urlGetselect_ma_positionList, _dtEmployee);

        setDdlData(ddlName, _dtEmployee.BoxManage_Position_list, "P_Name", "PIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกตำแหน่งงาน...", "0"));

    }


    #endregion

    #region Insert & Delete
    protected void InsertTopicQuest(int m0_toidx, int m0_tqidx, int orgidx, int rdeptidx, int rsecidx, int rposidx, int CEmpIDX, int status, string topic_name)
    {
        _dtrcm = new data_rcm_question();
        int i = 0;
        var ds_udoc1_insert = (DataSet)ViewState["vsBuyequipment_person"];
        var _document1 = new m1_persondetail[ds_udoc1_insert.Tables[0].Rows.Count];


        _dtrcm.Boxm0_topicquest = new m0_topicquest[1];
        m0_topicquest _inserttopic = new m0_topicquest();

        _inserttopic.m0_toidx = m0_toidx;
        _inserttopic.m0_tqidx = m0_tqidx;
        _inserttopic.orgidx = orgidx;
        _inserttopic.rdeptidx = rdeptidx;
        _inserttopic.rsecidx = rsecidx;
        _inserttopic.rposidx = rposidx;
        _inserttopic.CEmpIDX = CEmpIDX;
        _inserttopic.status = status;
        _inserttopic.topic_name = topic_name;
        _dtrcm.Boxm0_topicquest[0] = _inserttopic;

        foreach (DataRow dtrow in ds_udoc1_insert.Tables[0].Rows)
        {

            _document1[i] = new m1_persondetail();

            _document1[i].topic_name_person = dtrow["Topic_Name"].ToString();
            _document1[i].PIDX = int.Parse(dtrow["PIDX"].ToString());
            _document1[i].EmpIDX = int.Parse(dtrow["EmpIDX"].ToString());
            _document1[i].CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

            i++;

            _dtrcm.Boxm1_personlist = _document1;

        }

        _dtrcm = callServicePostRCM(_urlInsertm0topicquest, _dtrcm);
    }

    protected void InsertQuestion()
    {
        Label lbcourse_item = (Label)fv_insert.FindControl("lbcourse_item");
        Label lbquest = (Label)fv_insert.FindControl("lbquest");
        HyperLink images_q = (HyperLink)fv_insert.FindControl("images_q");
        Label lbchoice1 = (Label)fv_insert.FindControl("lbchoice1");
        HyperLink images_1 = (HyperLink)fv_insert.FindControl("images_1");
        Label lbchoice2 = (Label)fv_insert.FindControl("lbchoice2");
        HyperLink images_2 = (HyperLink)fv_insert.FindControl("images_2");
        Label lbchoice3 = (Label)fv_insert.FindControl("lbchoice3");
        HyperLink images_3 = (HyperLink)fv_insert.FindControl("images_3");
        Label lbchoice4 = (Label)fv_insert.FindControl("lbchoice4");
        HyperLink images_4 = (HyperLink)fv_insert.FindControl("images_4");
        Label lbchoice_ansidx = (Label)fv_insert.FindControl("lbchoice_ansidx");



        _dtrcm = new data_rcm_question();
        int i = 0;
        var ds_udoc1_insert = (DataSet)ViewState["vsBuyequipment"];
        var _document1 = new m0_question[ds_udoc1_insert.Tables[0].Rows.Count];

        foreach (DataRow dtrow in ds_udoc1_insert.Tables[0].Rows)
        {

            _document1[i] = new m0_question();

            _document1[i].m0_toidx = int.Parse(ddltopic.SelectedValue);
            _document1[i].m0_taidx = int.Parse(ddltypeans.SelectedValue);
            _document1[i].no_choice = int.Parse(dtrow["No_Quest"].ToString());
            _document1[i].quest_name = dtrow["Question"].ToString();
            _document1[i].choice_a = dtrow["Choice_a"].ToString();
            _document1[i].choice_b = dtrow["Choice_b"].ToString();
            _document1[i].choice_c = dtrow["Choice_c"].ToString();
            _document1[i].choice_d = dtrow["Choice_d"].ToString();
            _document1[i].CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

            if (dtrow["Choice_ansIDX"].ToString() == "")
            {
                _document1[i].choice_ans = 0;

            }
            else
            {
                _document1[i].choice_ans = int.Parse(dtrow["Choice_ansIDX"].ToString());

            }

            if (images_q != null)
            {
                _document1[i].pic_quest = 1;
            }


            if (images_1 != null)
            {
                _document1[i].pic_a = 1;
            }

            if (images_2 != null)
            {
                _document1[i].pic_b = 1;
            }


            if (images_3 != null)
            {
                _document1[i].pic_c = 1;
            }

            if (images_4 != null)
            {
                _document1[i].pic_d = 1;
            }

            i++;

            _dtrcm.Boxm0_question = _document1;
        }

        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));
        _dtrcm = callServicePostRCM(_urlInsertm0quest, _dtrcm);

    }

    protected void InsertAddOnPersonal(int Empidx, int m0toidx, int CEmpIDX, int PIDX)
    {
        _dtrcm.Boxm1_personlist = new m1_persondetail[1];
        m1_persondetail _insertperson = new m1_persondetail();

        _insertperson.P_EmpIDX = Empidx;
        _insertperson.m0_toidx = m0toidx;
        _insertperson.CEmpIDX = CEmpIDX;
        _insertperson.PIDX = PIDX;

        _dtrcm.Boxm1_personlist[0] = _insertperson;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtrcm));

        _dtrcm = callServicePostRCM(_urlInsertAddonPersonal, _dtrcm);
    }


    protected void DeleteQuestion(int m0_quidx)
    {
        _dtrcm.Boxm0_question = new m0_question[1];
        m0_question _deletetopic = new m0_question();

        _deletetopic.m0_quidx = m0_quidx;

        _dtrcm.Boxm0_question[0] = _deletetopic;

        _dtrcm = callServicePostRCM(_urlDeleteQuestion, _dtrcm);
    }

    protected void DeletePerson(int TPIDX, int CEmpIDX)
    {
        _dtrcm.Boxm1_personlist = new m1_persondetail[1];
        m1_persondetail _deleteperson = new m1_persondetail();

        _deleteperson.CEmpIDX = CEmpIDX;
        _deleteperson.TPIDX = TPIDX;


        _dtrcm.Boxm1_personlist[0] = _deleteperson;

        _dtrcm = callServicePostRCM(_urlDeletePerson_Responsibility, _dtrcm);
    }

    #endregion

    #region SetDefault
    protected void SetDefaultpage(int page)
    {

        _divMenuLiToViewIndex.Attributes.Remove("class");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        _divMenuLiToViewAddScore.Attributes.Remove("class");
        switch (page)
        {

            case 1:
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewIndex);
                gettypequestion(ddltypequest_search);
                select_gettypequestion(GvTypeTopic, int.Parse(ViewState["typequest_search"].ToString()));
                getOrganizationList(ddlOrg_search);
                ddltypequest_search.SelectedValue = ViewState["typequest_search"].ToString();
                ddlOrg_search.SelectedValue = ViewState["orgidx_search"].ToString();
                ddlDep_search.SelectedValue = ViewState["rdeptidx_search"].ToString();
                ddlSec_search.SelectedValue = ViewState["rsecidx_search"].ToString();
                ddlPos_search.SelectedValue = ViewState["rpos_search"].ToString();
                setOntop.Focus();

                if (ViewState["typequest_search"].ToString() == "2")
                {
                    divchoosetype_search.Visible = true;
                }
                else
                {
                    divchoosetype_search.Visible = false;
                }
                break;

            case 2:

                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Add("class", "active");
                MvMaster.SetActiveView(ViewAdd);

                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();
                fv_insert.ChangeMode(FormViewMode.Insert);
                fv_insert.DataBind();

                var ddltypequest = (DropDownList)fv_insert.FindControl("ddltypequest");
                var ddltypeans = (DropDownList)fv_insert.FindControl("ddltypeans");
                var ddlOrg = (DropDownList)fv_insert.FindControl("ddlOrg");
                var ddlDep = (DropDownList)fv_insert.FindControl("ddlDep");
                var ddlSec = (DropDownList)fv_insert.FindControl("ddlSec");
                var ddlPos = (DropDownList)fv_insert.FindControl("ddlPos");
                var GvReportAdd = (GridView)fv_insert.FindControl("GvReportAdd");
                var GvWrite = (GridView)fv_insert.FindControl("GvWrite");
                var btnAdddataset = (LinkButton)fv_insert.FindControl("btnAdddataset");
                var ddlposition = (DropDownList)fv_insert.FindControl("ddlposition");

                var ddlorg_person = (DropDownList)fv_insert.FindControl("ddlorg_person");
                var ddlrdept_person = (DropDownList)fv_insert.FindControl("ddlrdept_person");
                var ddlrsec_person = (DropDownList)fv_insert.FindControl("ddlrsec_person");
                var ddlname_person = (DropDownList)fv_insert.FindControl("ddlname_person");

                gettypequestion(ddltypequest);
                gettypeanswer(ddltypeans);
                getOrganizationList(ddlOrg);
                SelectMaPositionList(ddlposition);
                getOrganizationList(ddlorg_person);

                ddltypequest.SelectedValue = "0";
                ddlOrg.SelectedValue = "0";
                ddlDep.SelectedValue = "0";
                ddlSec.SelectedValue = "0";
                ddlPos.SelectedValue = "0";
                ddlposition.SelectedValue = "0";

                ddlorg_person.SelectedValue = "0";
                ddlrdept_person.SelectedValue = "0";
                ddlrsec_person.SelectedValue = "0";
                ddlname_person.SelectedValue = "0";

                linkBtnTrigger(btnAdddataset);

                CleardataSetFormList(GvReportAdd);
                GvReportAdd.Visible = false;

                CleardataSetFormList(GvWrite);
                GvWrite.Visible = false;

                CleardataSetPersonFormList(GvPerson);
                GvPerson.Visible = false;

                break;

            case 3:
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewDetail);
                select_gettypeanswer(GvTypeAns, int.Parse(ViewState["m0_toidx"].ToString()));
                select_person_responsibility(GvPerson_Detail, int.Parse(ViewState["m0_toidx"].ToString()));
                mergeCell(GvPerson_Detail);
                //select_position(GvPosition, int.Parse(ViewState["m0_toidx"].ToString()));
                setOntop.Focus();

                break;
            case 4:
                ViewState["word_src_score"] = string.Empty;
                src_score();
                // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(emp_data));
                _divMenuLiToViewAddScore.Attributes.Add("class", "active");
                MvMaster.SetActiveView(Viewscore);
                score_list.Visible = true;
                score_detail.Visible = false;
                score_detail_sub.Visible = false;
                //GridView Gvscore = (GridView)Viewscore.FindControl("Gvscore");

                break;

        }
    }

    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvPerson_Detail":
                for (int rowIndex = GvPerson_Detail.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvPerson_Detail.Rows[rowIndex];
                    GridViewRow previousRow = GvPerson_Detail.Rows[rowIndex + 1];

                    if (((Literal)currentRow.Cells[0].FindControl("lblPIDX")).Text == ((Literal)previousRow.Cells[0].FindControl("lblPIDX")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                }
                break;
        }
    }

    #endregion

    #region SetDefault Form

    protected void SetViewState_Form()
    {

        DataSet dsFoodMaterialList = new DataSet();
        dsFoodMaterialList.Tables.Add("dsAddListTable");

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("No_Quest", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Question", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Choice_a", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Choice_b", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Choice_c", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Choice_d", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Choice_ans", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Choice_ansIDX", typeof(int));


        ViewState["vsBuyequipment"] = dsFoodMaterialList;

    }

    protected void setAddList_Form(TextBox no, TextBox Question, TextBox Choice_a, TextBox Choice_b, TextBox Choice_c, TextBox Choice_d, DropDownList Choice_ans, GridView gvName)
    {

        if (ViewState["vsBuyequipment"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable"].Rows)
            {
                if (dr["No_Quest"].ToString() == no.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลลำดับนี้อยู่แล้ว!!!');", true);

                    return;
                }
                if (dr["No_Quest"].ToString() == no.Text &&
                   dr["Question"].ToString() == Question.Text &&
                   dr["Choice_a"].ToString() == Choice_a.Text &&
                   dr["Choice_b"].ToString() == Choice_b.Text &&
                   dr["Choice_c"].ToString() == Choice_c.Text &&
                   dr["Choice_d"].ToString() == Choice_d.Text &&
                   int.Parse(dr["Choice_ansIDX"].ToString()) == int.Parse(Choice_ans.SelectedValue))
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);

                    return;
                }
            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable"].NewRow();

            drContacts["No_Quest"] = no.Text;
            drContacts["Question"] = Question.Text;
            drContacts["Choice_a"] = Choice_a.Text;
            drContacts["Choice_b"] = Choice_b.Text;
            drContacts["Choice_c"] = Choice_c.Text;
            drContacts["Choice_d"] = Choice_d.Text;
            drContacts["Choice_ans"] = Choice_ans.SelectedItem.Text;
            drContacts["Choice_ansIDX"] = int.Parse(Choice_ans.SelectedValue);




            dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts);
            ViewState["vsBuyequipment"] = dsContacts;
            setGridData(gvName, dsContacts.Tables["dsAddListTable"]);
            gvName.Visible = true;

        }

    }

    protected void CleardataSetFormList(GridView GvName)
    {
        ViewState["vsBuyequipment"] = null;
        GvName.DataSource = ViewState["vsBuyequipment"];
        GvName.DataBind();
        SetViewState_Form();
    }

    protected void SetViewState_FormWrite()
    {

        DataSet dsFoodMaterialList = new DataSet();
        dsFoodMaterialList.Tables.Add("dsAddListTable");

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("No_Quest", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("Question", typeof(String));

        ViewState["vsBuyequipment"] = dsFoodMaterialList;

    }

    protected void setAddList_FormWrite(TextBox no, TextBox Question, GridView gvName)
    {

        if (ViewState["vsBuyequipment"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable"].Rows)
            {
                if (dr["No_Quest"].ToString() == no.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลลำดับนี้อยู่แล้ว!!!');", true);
                    return;
                }
                if (dr["No_Quest"].ToString() == no.Text &&
                   dr["Question"].ToString() == Question.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }

                setGridData(GvReportAdd, (DataSet)ViewState["vsBuyequipment"]);
                setGridData(GvWrite, (DataSet)ViewState["vsBuyequipment"]);

            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable"].NewRow();


            drContacts["No_Quest"] = no.Text;
            drContacts["Question"] = Question.Text;

            dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts);
            ViewState["vsBuyequipment"] = dsContacts;
            // GvReportAdd.Visible = true;
            setGridData(gvName, dsContacts.Tables["dsAddListTable"]);
            gvName.Visible = true;

        }

    }

    protected void SetViewState_Person()
    {

        DataSet dsPersonList = new DataSet();
        dsPersonList.Tables.Add("dsAddListTable");

        dsPersonList.Tables["dsAddListTable"].Columns.Add("Topic_Name", typeof(String));
        dsPersonList.Tables["dsAddListTable"].Columns.Add("P_Name", typeof(String));
        dsPersonList.Tables["dsAddListTable"].Columns.Add("PIDX", typeof(int));
        dsPersonList.Tables["dsAddListTable"].Columns.Add("Org_Name", typeof(String));
        dsPersonList.Tables["dsAddListTable"].Columns.Add("Dep_Name", typeof(String));
        dsPersonList.Tables["dsAddListTable"].Columns.Add("Sec_Name", typeof(String));
        dsPersonList.Tables["dsAddListTable"].Columns.Add("Person_Name", typeof(String));
        dsPersonList.Tables["dsAddListTable"].Columns.Add("EmpIDX", typeof(int));

        ViewState["vsBuyequipment_person"] = dsPersonList;

    }

    protected void setAddList_Person(TextBox topic, DropDownList position, DropDownList orgidx, DropDownList deptidx, DropDownList secidx, DropDownList empidx, GridView gvName)
    {

        if (ViewState["vsBuyequipment_person"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment_person"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable"].Rows)
            {

                if (dr["Topic_Name"].ToString() == topic.Text &&
                   dr["PIDX"].ToString() == position.SelectedValue &&
                  dr["Org_Name"].ToString() == orgidx.SelectedItem.Text &&
                   dr["Dep_Name"].ToString() == deptidx.SelectedItem.Text &&
                    dr["Sec_Name"].ToString() == secidx.SelectedItem.Text &&
                     dr["EmpIDX"].ToString() == empidx.SelectedValue)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }

                setGridData(gvName, (DataSet)ViewState["vsBuyequipment_person"]);

            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable"].NewRow();


            drContacts["Topic_Name"] = topic.Text;
            drContacts["P_Name"] = position.SelectedItem.Text;
            drContacts["PIDX"] = position.SelectedValue;
            drContacts["Org_Name"] = orgidx.SelectedItem.Text;
            drContacts["Dep_Name"] = deptidx.SelectedItem.Text;
            drContacts["Sec_Name"] = secidx.SelectedItem.Text;
            drContacts["Person_Name"] = empidx.SelectedItem.Text;
            drContacts["EmpIDX"] = empidx.SelectedValue;

            dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts);
            ViewState["vsBuyequipment_person"] = dsContacts;
            // GvReportAdd.Visible = true;
            setGridData(gvName, dsContacts.Tables["dsAddListTable"]);
            gvName.Visible = true;

        }

    }

    protected void CleardataSetPersonFormList(GridView GvName)
    {
        ViewState["vsBuyequipment_person"] = null;
        GvName.DataSource = ViewState["vsBuyequipment_person"];
        GvName.DataBind();
        SetViewState_Person();
    }

    protected void src_score()
    {
        data_employee_recruit emp_data = new data_employee_recruit();
        requset_more_data rm_data = new requset_more_data();
        emp_data.requset_more_list = new requset_more_data[1];

        if (ViewState["word_src_score"] != null)
        {
            rm_data.identity_card = ViewState["word_src_score"].ToString();
        }
        emp_data.requset_more_list[0] = rm_data;
        emp_data = callServicePostEmpRe(_urlGetlistscoreexam, emp_data);
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(emp_data));
        setGridData(Gvscore, emp_data.requset_more_list);
    }

    #endregion

    #region FormView

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewAdd.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }
                break;

        }
    }
    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is DropDownList)
        {
            DropDownList ddName = (DropDownList)sender;

            Panel panel_quest = (Panel)fv_insert.FindControl("panel_quest");
            DropDownList ddlchooseqs = (DropDownList)fv_insert.FindControl("ddlchooseqs");
            ////////////////////// Topic Question ///////////////////////////////
            DropDownList ddltypequest = (DropDownList)fv_insert.FindControl("ddltypequest");
            DropDownList ddlOrg = (DropDownList)fv_insert.FindControl("ddlOrg");
            DropDownList ddlDep = (DropDownList)fv_insert.FindControl("ddlDep");
            DropDownList ddlSec = (DropDownList)fv_insert.FindControl("ddlSec");
            DropDownList ddlPos = (DropDownList)fv_insert.FindControl("ddlPos");
            Control divchoosetype = (Control)fv_insert.FindControl("divchoosetype");
            Control div_save = (Control)fv_insert.FindControl("div_save");
            Control div_txttopic = (Control)fv_insert.FindControl("div_txttopic");
            Control div_typequest = (Control)fv_insert.FindControl("div_typequest");
            ////////////////////// Sub Question ///////////////////////////////
            DropDownList ddltopic = (DropDownList)fv_insert.FindControl("ddltopic");
            Control div_choice = (Control)fv_insert.FindControl("div_choice");

            ////////////////////// Person Topic ///////////////////////////////
            DropDownList ddlorg_person = (DropDownList)fv_insert.FindControl("ddlorg_person");
            DropDownList ddlrdept_person = (DropDownList)fv_insert.FindControl("ddlrdept_person");
            DropDownList ddlrsec_person = (DropDownList)fv_insert.FindControl("ddlrsec_person");
            DropDownList ddlname_person = (DropDownList)fv_insert.FindControl("ddlname_person");

            Panel panel_chooseperson = (Panel)fv_insert.FindControl("panel_chooseperson");
            switch (ddName.ID)
            {
                case "ddlchooseqs":
                    if (ddlchooseqs.SelectedValue == "1")
                    {
                        panel_quest.Visible = false;
                        div_txttopic.Visible = true;
                        div_typequest.Visible = true;
                        panel_chooseperson.Visible = true;
                    }
                    else if (ddlchooseqs.SelectedValue == "2")
                    {
                        div_save.Visible = false;
                        panel_quest.Visible = true;
                        div_txttopic.Visible = false;
                        div_typequest.Visible = true;
                        panel_chooseperson.Visible = false;
                    }
                    else
                    {
                        panel_quest.Visible = false;
                        panel_chooseperson.Visible = false;
                        div_save.Visible = false;
                        div_txttopic.Visible = false;
                        div_typequest.Visible = false;
                        ddltypequest.SelectedValue = "0";
                        ddltopic.SelectedValue = "0";
                    }
                    break;

                case "ddltypequest":

                    if (ddlchooseqs.SelectedValue == "1")
                    {
                        if (ddltypequest.SelectedValue == "2")
                        {
                            divchoosetype.Visible = true;
                            div_txttopic.Visible = true;

                        }
                        else if (ddltypequest.SelectedValue == "1")
                        {
                            divchoosetype.Visible = false;
                            div_txttopic.Visible = true;

                        }
                        else

                        {
                            div_txttopic.Visible = false;
                        }
                    }
                    else if (ddlchooseqs.SelectedValue == "2")
                    {
                        if (ddltypequest.SelectedValue == "1")
                        {
                            divchoosetype.Visible = false;
                        }
                        else
                        {
                            divchoosetype.Visible = true;

                        }
                    }
                    gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), 0, 0, 0, 0);

                    break;

                case "ddlOrg":
                    getDepartmentList(ddlDep, int.Parse(ddlOrg.SelectedValue));
                    ddltopic.SelectedValue = "0";

                    break;

                case "ddlDep":
                    getSectionList(ddlSec, int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue));
                    if (ddlDep.SelectedValue != "0")
                    {
                        if (ddltypequest.SelectedValue == "2")
                        {
                            gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), 0, 0);

                        }
                    }
                    else
                    {
                        gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), 0, 0, 0);

                    }

                    break;

                case "ddlSec":
                    getPositionList(ddlPos, int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue));
                    if (ddlSec.SelectedValue != "0")
                    {
                        if (ddltypequest.SelectedValue == "2")
                        {
                            gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue), 0);

                        }
                    }
                    else
                    {
                        gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue)
                               , 0);
                    }
                    break;
                case "ddlPos":
                    if (ddlPos.SelectedValue != "0")
                    {
                        if (ddltypequest.SelectedValue == "2")
                        {
                            gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue)
                                , int.Parse(ddlPos.SelectedValue));

                        }
                    }
                    else
                    {
                        if (ddltypequest.SelectedValue == "2")
                        {
                            gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue)
                                , 0);

                        }
                    }

                    break;
                case "ddltypeans":
                    if (ddltypeans.SelectedValue == "1")
                    {
                        div_choice.Visible = true;

                    }
                    else
                    {
                        div_choice.Visible = false;
                    }
                    break;

                case "ddltypequest_search":
                    if (ddltypequest_search.SelectedValue != "2")
                    {
                        divchoosetype_search.Visible = false;
                        ddlOrg_search.SelectedValue = "0";
                        ddlDep_search.SelectedValue = "0";
                        ddlSec_search.SelectedValue = "0";
                        ddlPos_search.SelectedValue = "0";
                    }
                    else
                    {
                        divchoosetype_search.Visible = true;
                    }
                    break;

                case "ddlOrg_search":
                    getDepartmentList(ddlDep_search, int.Parse(ddlOrg_search.SelectedValue));
                    break;

                case "ddlDep_search":
                    getSectionList(ddlSec_search, int.Parse(ddlOrg_search.SelectedValue), int.Parse(ddlDep_search.SelectedValue));
                    break;

                case "ddlSec_search":
                    getPositionList(ddlPos_search, int.Parse(ddlOrg_search.SelectedValue), int.Parse(ddlDep_search.SelectedValue), int.Parse(ddlSec_search.SelectedValue));
                    break;

                case "ddlorg_person":
                    getDepartmentList(ddlrdept_person, int.Parse(ddlorg_person.SelectedValue));
                    break;
                case "ddlrdept_person":
                    getSectionList(ddlrsec_person, int.Parse(ddlorg_person.SelectedValue), int.Parse(ddlrdept_person.SelectedValue));
                    break;

                case "ddlrsec_person":
                    getEmpList(ddlname_person, int.Parse(ddlorg_person.SelectedValue), int.Parse(ddlrdept_person.SelectedValue), int.Parse(ddlrsec_person.SelectedValue));

                    break;

                case "ddlorg_person_addon":
                    getDepartmentList(ddlrdept_person_addon, int.Parse(ddlorg_person_addon.SelectedValue));
                    break;
                case "ddlrdept_person_addon":
                    getSectionList(ddlrsec_person_addon, int.Parse(ddlorg_person_addon.SelectedValue), int.Parse(ddlrdept_person_addon.SelectedValue));
                    break;

                case "ddlrsec_person_addon":
                    getEmpList(ddlname_person_addon, int.Parse(ddlorg_person_addon.SelectedValue), int.Parse(ddlrdept_person_addon.SelectedValue), int.Parse(ddlrsec_person_addon.SelectedValue));

                    break;
            }
        }
        if (sender is TextBox)
        {
            TextBox txtName = (TextBox)sender;

            DropDownList ddltypequest = (DropDownList)fv_insert.FindControl("ddltypequest");
            DropDownList ddltypeans = (DropDownList)fv_insert.FindControl("ddltypeans");
            DropDownList ddltopic = (DropDownList)fv_insert.FindControl("ddltopic");
            TextBox txtcourse_item = (TextBox)fv_insert.FindControl("txtcourse_item");
            LinkButton btnAdddataset = (LinkButton)fv_insert.FindControl("btnAdddataset");

            switch (txtName.ID)
            {
                case "txtcourse_item":

                    _dtrcm = new data_rcm_question();
                    _dtrcm.Boxm0_topicquest = new m0_topicquest[1];
                    m0_topicquest _quest = new m0_topicquest();

                    _quest.m0_tqidx = int.Parse(ddltypequest.SelectedValue);
                    _quest.m0_taidx = int.Parse(ddltypeans.SelectedValue);
                    _quest.m0_toidx = int.Parse(ddltopic.SelectedValue);
                    _quest.no_choice = int.Parse(txtcourse_item.Text);

                    _dtrcm.Boxm0_topicquest[0] = _quest;

                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

                    _dtrcm = callServicePostRCM(_urlSelectNoChoiceQuestion, _dtrcm);

                    if (_dtrcm.ReturnCode == "0")
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีลำดับคำถามนี้อยู่แล้ว กรุณาเลือกลำดับอื่น!!!');", true);

                    }

                    setGridData(GvReportAdd, (DataSet)ViewState["vsBuyequipment"]);
                    setGridData(GvWrite, (DataSet)ViewState["vsBuyequipment"]);

                    linkBtnTrigger(btnAdddataset);

                    break;
            }
        }
    }


    #endregion

    #region RowDatabound & RowCommand
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            #region GvReportAdd
            case "GvReportAdd":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbcourse_item = (Label)e.Row.Cells[0].FindControl("lbcourse_item");
                    TextBox txtcourse_item = (TextBox)fv_insert.FindControl("txtcourse_item");
                    DropDownList ddltypeans = (DropDownList)fv_insert.FindControl("ddltypeans");


                    var images_q = (HyperLink)e.Row.FindControl("images_q");
                    var images_1 = (HyperLink)e.Row.FindControl("images_1");
                    var images_2 = (HyperLink)e.Row.FindControl("images_2");
                    var images_3 = (HyperLink)e.Row.FindControl("images_3");
                    var images_4 = (HyperLink)e.Row.FindControl("images_4");


                    string getPathfile = ConfigurationManager.AppSettings["pathfile_recruit"];
                    string fileName_upload = ddltopic.SelectedValue + "/" + ddltypeans.SelectedValue + "/" + lbcourse_item.Text;// + "/" + "q.jpg"; 
                    string filePath_upload = Server.MapPath(getPathfile + fileName_upload);
                    string pic = getPathfile + fileName_upload + "/q.jpg";

                    DirectoryInfo myDirLotus = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus, "q");

                    if (ViewState["path_q"].ToString() != "0")
                    {
                        images_q.Visible = true;
                        images_q.NavigateUrl = pic;

                        HtmlImage img = new HtmlImage();
                        img.Src = pic;
                        img.Height = 100;
                        images_q.Controls.Add(img);
                    }
                    else
                    {
                        images_q.Visible = false;
                    }
                    ViewState["path_q"] = "0";


                    string pic1 = getPathfile + fileName_upload + "/1.jpg";
                    DirectoryInfo myDirLotus1 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus1, "1");

                    if (ViewState["path_1"].ToString() != "0")
                    {
                        images_1.Visible = true;
                        images_1.NavigateUrl = pic1;

                        HtmlImage img1 = new HtmlImage();
                        img1.Src = pic1;
                        img1.Height = 100;
                        images_1.Controls.Add(img1);
                    }
                    else
                    {
                        images_1.Visible = false;
                    }

                    ViewState["path_1"] = "0";

                    string pic2 = getPathfile + fileName_upload + "/2.jpg";
                    DirectoryInfo myDirLotus2 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus2, "2");
                    if (ViewState["path_2"].ToString() != "0")
                    {
                        images_2.Visible = true;
                        images_2.NavigateUrl = pic2;

                        HtmlImage img2 = new HtmlImage();
                        img2.Src = pic2;
                        img2.Height = 100;
                        images_2.Controls.Add(img2);
                    }
                    else
                    {
                        images_2.Visible = false;
                    }
                    ViewState["path_2"] = "0";

                    string pic3 = getPathfile + fileName_upload + "/3.jpg";
                    DirectoryInfo myDirLotus3 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus3, "3");

                    if (ViewState["path_3"].ToString() != "0")
                    {
                        images_3.Visible = true;
                        images_3.NavigateUrl = pic3;

                        HtmlImage img3 = new HtmlImage();
                        img3.Src = pic3;
                        img3.Height = 100;
                        images_3.Controls.Add(img3);
                    }
                    else
                    {
                        images_3.Visible = false;
                    }
                    ViewState["path_3"] = "0";

                    string pic4 = getPathfile + fileName_upload + "/4.jpg";
                    DirectoryInfo myDirLotus4 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus4, "4");

                    if (ViewState["path_4"].ToString() != "0")
                    {
                        images_4.Visible = true;
                        images_4.NavigateUrl = pic4;

                        HtmlImage img4 = new HtmlImage();
                        img4.Src = pic4;
                        img4.Height = 100;
                        images_4.Controls.Add(img4);
                    }
                    else
                    {
                        images_4.Visible = false;
                    }
                    ViewState["path_4"] = "0";

                }
                break;
            #endregion GvReportAdd
            #region GvWrite
            case "GvWrite":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbcourse_item = (Label)e.Row.Cells[0].FindControl("lbcourse_item");
                    TextBox txtcourse_item = (TextBox)fv_insert.FindControl("txtcourse_item");
                    DropDownList ddltypeans = (DropDownList)fv_insert.FindControl("ddltypeans");


                    var images_q = (HyperLink)e.Row.FindControl("images_q");
                    string getPathfile = ConfigurationManager.AppSettings["pathfile_recruit"];
                    string fileName_upload = ddltopic.SelectedValue + "/" + ddltypeans.SelectedValue + "/" + lbcourse_item.Text;// + "/" + "q.jpg"; 
                    string filePath_upload = Server.MapPath(getPathfile + fileName_upload);
                    string pic = getPathfile + fileName_upload + "/q.jpg";

                    DirectoryInfo myDirLotus = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus, "q");

                    if (ViewState["path_q"].ToString() != "0")
                    {
                        images_q.Visible = true;
                        images_q.NavigateUrl = pic;

                        HtmlImage img = new HtmlImage();
                        img.Src = pic;
                        img.Height = 100;
                        images_q.Controls.Add(img);
                    }
                    else
                    {
                        images_q.Visible = false;
                    }
                    ViewState["path_q"] = "0";
                }
                break;
            #endregion GvWrite
            #region GvTypeTopic
            case "GvTypeTopic":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbltypidx = (Label)e.Row.Cells[0].FindControl("lbltypidx");
                    Label lblno = (Label)e.Row.Cells[0].FindControl("lblno");
                    GridView GvTopic = (GridView)e.Row.Cells[0].FindControl("GvTopic");


                    if (ViewState["Search_Data"].ToString() == "1")
                    {
                        select_gettopicquestion(GvTopic, int.Parse(ViewState["typequest_search"].ToString()), int.Parse(ViewState["orgidx_search"].ToString()), int.Parse(ViewState["rdeptidx_search"].ToString()), int.Parse(ViewState["rsecidx_search"].ToString()), int.Parse(ViewState["rpos_search"].ToString()));
                    }
                    else
                    {
                        select_gettopicquestion(GvTopic, int.Parse(lbltypidx.Text), 0, 0, 0, 0);
                    }

                }
                break;
            #endregion GvTypeTopic

            #region GvTypeAns
            case "GvTypeAns":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblm0_taidx = (Label)e.Row.Cells[0].FindControl("lblm0_taidx");
                    GridView GvQuestion = (GridView)e.Row.Cells[0].FindControl("GvQuestion");

                    select_getquestion(GvQuestion, int.Parse(ViewState["m0_toidx"].ToString()), int.Parse(lblm0_taidx.Text));

                    if (lblm0_taidx.Text != "1")
                    {
                        GvQuestion.Columns[2].Visible = false;
                        GvQuestion.Columns[3].Visible = false;
                        GvQuestion.Columns[4].Visible = false;
                        GvQuestion.Columns[5].Visible = false;
                        GvQuestion.Columns[6].Visible = false;

                    }


                }
                break;
            #endregion GvTypeAns
            #region GvQuestion
            case "GvQuestion":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblm0_toidx = (Label)e.Row.Cells[0].FindControl("lblm0_toidx");
                    Label lblm0_taidx = (Label)e.Row.Cells[0].FindControl("lblm0_taidx");
                    Label lblno_choice = (Label)e.Row.Cells[0].FindControl("lblno_choice");
                    Label lblchoice_ans = (Label)e.Row.Cells[6].FindControl("lblchoice_ans");

                    var images_q = (HyperLink)e.Row.FindControl("images_q");
                    var images_1 = (HyperLink)e.Row.FindControl("images_1");
                    var images_2 = (HyperLink)e.Row.FindControl("images_2");
                    var images_3 = (HyperLink)e.Row.FindControl("images_3");
                    var images_4 = (HyperLink)e.Row.FindControl("images_4");

                    if (lblchoice_ans.Text == "1")
                    {
                        lblchoice_ans.Text = "ก";
                    }
                    else if (lblchoice_ans.Text == "2")
                    {
                        lblchoice_ans.Text = "ข";
                    }
                    else if (lblchoice_ans.Text == "3")
                    {
                        lblchoice_ans.Text = "ค";
                    }
                    else if (lblchoice_ans.Text == "4")
                    {
                        lblchoice_ans.Text = "ง";
                    }


                    string getPathfile = ConfigurationManager.AppSettings["pathfile_recruit"];
                    string fileName_upload = lblm0_toidx.Text + "/" + lblm0_taidx.Text + "/" + lblno_choice.Text;// + "/" + "q.jpg"; 
                    string filePath_upload = Server.MapPath(getPathfile + fileName_upload);
                    string pic = getPathfile + fileName_upload + "/q.jpg";

                    DirectoryInfo myDirLotus = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus, "q");

                    if (ViewState["path_q"].ToString() != "0")
                    {
                        images_q.Visible = true;
                        images_q.NavigateUrl = pic;

                        HtmlImage img = new HtmlImage();
                        img.Src = pic;
                        img.Height = 100;
                        images_q.Controls.Add(img);
                    }
                    else
                    {
                        images_q.Visible = false;
                    }
                    ViewState["path_q"] = "0";

                    string pic1 = getPathfile + fileName_upload + "/1.jpg";
                    DirectoryInfo myDirLotus1 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus1, "1");

                    if (ViewState["path_1"].ToString() != "0")
                    {
                        images_1.Visible = true;
                        images_1.NavigateUrl = pic1;

                        HtmlImage img1 = new HtmlImage();
                        img1.Src = pic1;
                        img1.Height = 100;
                        images_1.Controls.Add(img1);
                    }
                    else
                    {
                        images_1.Visible = false;
                    }

                    ViewState["path_1"] = "0";

                    string pic2 = getPathfile + fileName_upload + "/2.jpg";
                    DirectoryInfo myDirLotus2 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus2, "2");
                    if (ViewState["path_2"].ToString() != "0")
                    {
                        images_2.Visible = true;
                        images_2.NavigateUrl = pic2;

                        HtmlImage img2 = new HtmlImage();
                        img2.Src = pic2;
                        img2.Height = 100;
                        images_2.Controls.Add(img2);
                    }
                    else
                    {
                        images_2.Visible = false;
                    }
                    ViewState["path_2"] = "0";

                    string pic3 = getPathfile + fileName_upload + "/3.jpg";
                    DirectoryInfo myDirLotus3 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus3, "3");

                    if (ViewState["path_3"].ToString() != "0")
                    {
                        images_3.Visible = true;
                        images_3.NavigateUrl = pic3;

                        HtmlImage img3 = new HtmlImage();
                        img3.Src = pic3;
                        img3.Height = 100;
                        images_3.Controls.Add(img3);
                    }
                    else
                    {
                        images_3.Visible = false;
                    }
                    ViewState["path_3"] = "0";

                    string pic4 = getPathfile + fileName_upload + "/4.jpg";
                    DirectoryInfo myDirLotus4 = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus4, "4");

                    if (ViewState["path_4"].ToString() != "0")
                    {
                        images_4.Visible = true;
                        images_4.NavigateUrl = pic4;

                        HtmlImage img4 = new HtmlImage();
                        img4.Src = pic4;
                        img4.Height = 100;
                        images_4.Controls.Add(img4);
                    }
                    else
                    {
                        images_4.Visible = false;
                    }
                    ViewState["path_4"] = "0";
                }
                break;
            #endregion GvQuestion
            #region Gvscore
            case "Gvscore":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lb_exam_status = (Label)e.Row.FindControl("lb_exam_status");
                    Label lb_exam_status_true = (Label)e.Row.FindControl("lb_exam_status_true");
                    Label lb_exam_status_false = (Label)e.Row.FindControl("lb_exam_status_false");

                    string txt_exam = lb_exam_status.Text;


                    if (txt_exam.ToString() == "1")
                    {
                        lb_exam_status_true.Visible = true;
                    }
                    else if (txt_exam.ToString() == "0")
                    {
                        lb_exam_status_false.Visible = true;
                    }

                }
                break;
            #endregion Gvscore

            #region Gvscore_detail_u0
            case "Gvscore_detail_u0":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lb_status = (Label)e.Row.FindControl("lb_exam_check_exam");
                    Label lb_true = (Label)e.Row.FindControl("lb_exam__check_exam_true");
                    Label lb_false = (Label)e.Row.FindControl("lb_exam__check_exam_false");

                    string txt_exam = lb_status.Text;


                    if (txt_exam.ToString() == "1")
                    {
                        lb_true.Visible = true;
                    }
                    else if (txt_exam.ToString() == "0")
                    {
                        lb_false.Visible = true;
                    }

                }
                break;
                #endregion Gvscore_detail_u0
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        GvName.PageIndex = e.NewPageIndex;
        GvName.DataBind();
        switch (GvName.ID)
        {
            case "Gvscore":

                src_score();
                break;

        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "bnDeleteList":
                    GridView GvReportAdd = (GridView)fv_insert.FindControl("GvReportAdd");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    DropDownList ddltypeans = (DropDownList)fv_insert.FindControl("ddltypeans");
                    Control div_save = (Control)fv_insert.FindControl("div_save");

                    int rowselect = rowSelect.RowIndex;
                    int rowIndex;


                    foreach (GridViewRow row in GvReportAdd.Rows)
                    {
                        rowIndex = row.RowIndex;
                        Label lbcourse_item = (Label)row.Cells[0].FindControl("lbcourse_item");

                        if (rowIndex == rowselect)
                        {

                            string getPathfile = ConfigurationManager.AppSettings["pathfile_recruit"];
                            string fileName_upload = ddltopic.SelectedValue + "/" + ddltypeans.SelectedValue + "/" + lbcourse_item.Text; //rowIndex.ToString();
                            string filePath_upload = Server.MapPath(getPathfile + fileName_upload);


                            if (Directory.Exists(filePath_upload))
                            {

                                foreach (string file in Directory.GetFiles(filePath_upload))
                                {
                                    File.Delete(file);
                                }
                                Directory.Delete(filePath_upload);

                            }
                        }
                        rowIndex++;
                    }

                    DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];
                    dsContacts.Tables["dsAddListTable"].Rows[rowselect].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(GvReportAdd, dsContacts.Tables["dsAddListTable"]);


                    if (dsContacts.Tables["dsAddListTable"].Rows.Count < 1)
                    {
                        GvReportAdd.Visible = false;
                        div_save.Visible = false;
                    }


                    break;

                case "bnDeleteListWrite":
                    GridView GvWrite = (GridView)fv_insert.FindControl("GvWrite");
                    GridViewRow rowSelect_write = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    DropDownList ddltypeans_write = (DropDownList)fv_insert.FindControl("ddltypeans");
                    Control div_save1 = (Control)fv_insert.FindControl("div_save");

                    int rowselect_write = rowSelect_write.RowIndex;
                    int rowIndex_write;


                    foreach (GridViewRow row in GvWrite.Rows)
                    {
                        rowIndex_write = row.RowIndex;
                        Label lbcourse_item = (Label)row.Cells[0].FindControl("lbcourse_item");

                        if (rowIndex_write == rowselect_write)
                        {

                            string getPathfile = ConfigurationManager.AppSettings["pathfile_recruit"];
                            string fileName_upload = ddltopic.SelectedValue + "/" + ddltypeans_write.SelectedValue + "/" + lbcourse_item.Text; //rowIndex.ToString();
                            string filePath_upload = Server.MapPath(getPathfile + fileName_upload);


                            if (Directory.Exists(filePath_upload))
                            {

                                foreach (string file in Directory.GetFiles(filePath_upload))
                                {
                                    File.Delete(file);
                                }
                                Directory.Delete(filePath_upload);

                            }
                        }
                        rowIndex_write++;
                    }

                    DataSet dsContacts_write = (DataSet)ViewState["vsBuyequipment"];
                    dsContacts_write.Tables["dsAddListTable"].Rows[rowselect_write].Delete();
                    dsContacts_write.AcceptChanges();
                    setGridData(GvWrite, dsContacts_write.Tables["dsAddListTable"]);

                    if (dsContacts_write.Tables["dsAddListTable"].Rows.Count < 1)
                    {
                        GvWrite.Visible = false;
                        div_save1.Visible = false;
                    }


                    break;

                case "bnDeleteListPerson":
                    GridView GvPerson = (GridView)fv_insert.FindControl("GvPerson");
                    GridViewRow rowSelect_person = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    Control div_saveperson = (Control)fv_insert.FindControl("div_save");

                    int rowselect_person = rowSelect_person.RowIndex;


                    DataSet dsContacts_person = (DataSet)ViewState["vsBuyequipment_person"];
                    dsContacts_person.Tables["dsAddListTable"].Rows[rowselect_person].Delete();
                    dsContacts_person.AcceptChanges();
                    setGridData(GvPerson, dsContacts_person.Tables["dsAddListTable"]);

                    if (dsContacts_person.Tables["dsAddListTable"].Rows.Count < 1)
                    {
                        GvPerson.Visible = false;
                        div_saveperson.Visible = false;
                    }
                    break;


            }
        }
    }

    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr = dt.NewRow();
        DataColumn dc = new DataColumn("FileName", typeof(string));
        dc = new DataColumn("Download", typeof(string));
        dt.Columns.Add("FileName");
        dt.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name == "q.jpg" && target == "q")
                {

                    string[] f = Directory.GetFiles(dirfiles, "q.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_q"] = "1";
                }
                else if (file.Name == "1.jpg" && target == "1")
                {

                    string[] f = Directory.GetFiles(dirfiles, "1.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_1"] = "1";
                }
                else if (file.Name == "2.jpg" && target == "2")
                {

                    string[] f = Directory.GetFiles(dirfiles, "2.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_2"] = "1";
                }
                else if (file.Name == "3.jpg" && target == "3")
                {

                    string[] f = Directory.GetFiles(dirfiles, "3.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_3"] = "1";
                }
                else if (file.Name == "4.jpg" && target == "4")
                {

                    string[] f = Directory.GetFiles(dirfiles, "4.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_4"] = "1";
                }


            }


        }
        catch (Exception ex)
        {
            // txt.Text = ex.ToString();
            ViewState["path"] = "0";
        }
    }


    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    protected data_rcm_question callServicePostRCM(string _cmdUrl, data_rcm_question _dtrcm)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtrcm);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtrcm = (data_rcm_question)_funcTool.convertJsonToObject(typeof(data_rcm_question), _localJson);


        return _dtrcm;
    }

    protected data_employee_recruit callServicePostEmpRe(string _cmdUrl, data_employee_recruit _dtrcm)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtrcm);
        // text.Text =  _cmdUrl + _localJson;
        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;
        ////// convert json to object
        _dtrcm = (data_employee_recruit)_funcTool.convertJsonToObject(typeof(data_employee_recruit), _localJson);
        return _dtrcm;
    }
    #endregion


    #region Repeater databound
    protected void Boundtopictype(object sender, RepeaterItemEventArgs args)
    {

        if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater Repquestion = (Repeater)args.Item.FindControl("Repquestion");
            TextBox m0_toidx = (TextBox)args.Item.FindControl("m0_toidx");
            TextBox m0_taidx = (TextBox)args.Item.FindControl("m0_taidx");

            question_data[] question_list = (question_data[])ViewState["question_list"];

            /////////////////  ข้อมูล ของ m0  /////////////////////////
            var _ViewState_m0 = (from m0_viewstate in question_list
                                 where m0_viewstate.m0_toidx == _funcTool.convertToInt(m0_toidx.Text) && m0_viewstate.m0_taidx == _funcTool.convertToInt(m0_taidx.Text)
                                 select m0_viewstate).ToList();

            ////////////////////////////////////////////////////////
            /////
            // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_ViewState_m0));
            Repquestion.DataSource = _ViewState_m0;
            Repquestion.DataBind();


        }
    }
    protected void BoundRepquestion(object sender, RepeaterItemEventArgs args)
    {

        if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
        {
            TextBox m0_toidx = (TextBox)args.Item.FindControl("m0_toidx");
            TextBox m0_taidx = (TextBox)args.Item.FindControl("m0_taidx");
            TextBox m0_quidx = (TextBox)args.Item.FindControl("m0_quidx");
            TextBox choice_ans = (TextBox)args.Item.FindControl("choice_ans");



            recruit_u1_answer_data[] u1_an = (recruit_u1_answer_data[])ViewState["recruit_u1_answer_list"];

            var _ViewState_m0 = (from m0_viewstate in u1_an
                                 where m0_viewstate.m0_toidx == _funcTool.convertToInt(m0_toidx.Text) && m0_viewstate.m0_quidx == _funcTool.convertToInt(m0_quidx.Text)
                                 select m0_viewstate).ToList();
            if (_ViewState_m0.Count() > 0)
            {

                switch (m0_taidx.Text)
                {
                    case "1":

                        string answer_s = null;
                        switch (_ViewState_m0[0].answer.ToString())
                        {
                            case "1":
                                answer_s = "ra_choice_a";
                                break;
                            case "2":
                                answer_s = "ra_choice_b";
                                break;
                            case "3":
                                answer_s = "ra_choice_c";
                                break;
                            case "4":
                                answer_s = "ra_choice_d";
                                break;
                        }
                        if (answer_s != null)
                        {
                            RadioButton answer_n = (RadioButton)args.Item.FindControl(answer_s);
                            answer_n.Checked = true;
                        }
                        string answer_true = null; //เฉลย
                        switch (choice_ans.Text)
                        {
                            case "1":
                                answer_true = "ra_choice_a";
                                break;
                            case "2":
                                answer_true = "ra_choice_b";
                                break;
                            case "3":
                                answer_true = "ra_choice_c";
                                break;
                            case "4":
                                answer_true = "ra_choice_d";
                                break;
                        }
                        if (answer_true != null)
                        {

                            RadioButton answer_n = (RadioButton)args.Item.FindControl(answer_true);

                            answer_n.CssClass = String.Join(" ", answer_n
                                     .CssClass
                                        .Split(' ')
                                        .Except(new string[] { "", "check_true" })
                                        .Concat(new string[] { "check_true" })
                                        .ToArray()
                                        );
                        }
                        break;
                    case "2":
                        TextBox answer_txt = (TextBox)args.Item.FindControl("answer_txt");
                        TextBox txt_score_comment = (TextBox)args.Item.FindControl("txt_score_comment");

                        answer_txt.Text = _ViewState_m0[0].answer.ToString();
                        txt_score_comment.Text = _ViewState_m0[0].score.ToString();
                        if (_funcTool.convertToDecimal(_ViewState_m0[0].score.ToString()) > 0)
                        {
                            txt_score_comment.Enabled = false;
                        }

                        break;
                }
            }
            //string classname = "TestClass";
            //m0_taidx.CssClass = String.Join(" ", m0_taidx
            //   .CssClass
            //   .Split(' ')
            //   .Except(new string[] { "", classname })
            //   .Concat(new string[] { classname })
            //   .ToArray());
        }
    }
    #endregion Repeater databound
    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        #region control btn
        DropDownList ddlchooseqs = (DropDownList)fv_insert.FindControl("ddlchooseqs");
        DropDownList ddltypequest = (DropDownList)fv_insert.FindControl("ddltypequest");
        DropDownList ddlOrg = (DropDownList)fv_insert.FindControl("ddlOrg");
        DropDownList ddlDep = (DropDownList)fv_insert.FindControl("ddlDep");
        DropDownList ddlSec = (DropDownList)fv_insert.FindControl("ddlSec");
        DropDownList ddlPos = (DropDownList)fv_insert.FindControl("ddlPos");
        TextBox txttopic = (TextBox)fv_insert.FindControl("txttopic");
        GridView GvReportAdd = (GridView)fv_insert.FindControl("GvReportAdd");
        TextBox txtcourse_item = (TextBox)fv_insert.FindControl("txtcourse_item");
        TextBox txtquest = (TextBox)fv_insert.FindControl("txtquest");
        TextBox txtchoice1 = (TextBox)fv_insert.FindControl("txtchoice1");
        TextBox txtchoice2 = (TextBox)fv_insert.FindControl("txtchoice2");
        TextBox txtchoice3 = (TextBox)fv_insert.FindControl("txtchoice3");
        TextBox txtchoice4 = (TextBox)fv_insert.FindControl("txtchoice4");
        DropDownList ddl_answer = (DropDownList)fv_insert.FindControl("ddl_answer");
        Control div_save = (Control)fv_insert.FindControl("div_save");
        GridView GvWrite = (GridView)fv_insert.FindControl("GvWrite");
        Control divchoosetype = (Control)fv_insert.FindControl("divchoosetype");
        Control div_choice = (Control)fv_insert.FindControl("div_choice");
        Panel panel_quest = (Panel)fv_insert.FindControl("panel_quest");
        LinkButton btnAdddataset = (LinkButton)fv_insert.FindControl("btnAdddataset");
        GridView GvTypeTopic = (GridView)ViewIndex.FindControl("GvTypeTopic");

        DropDownList ddlposition = (DropDownList)fv_insert.FindControl("ddlposition");
        DropDownList ddlorg_person = (DropDownList)fv_insert.FindControl("ddlorg_person");
        DropDownList ddlrdept_person = (DropDownList)fv_insert.FindControl("ddlrdept_person");
        DropDownList ddlrsec_person = (DropDownList)fv_insert.FindControl("ddlrsec_person");
        DropDownList ddlname_person = (DropDownList)fv_insert.FindControl("ddlname_person");
        #endregion control btn
        switch (cmdName)
        {
            #region _divMenuBtnToDivIndex
            case "_divMenuBtnToDivIndex":
                SetDefaultpage(1);
                break;
            #endregion _divMenuBtnToDivIndex
            #region _divMenuBtnToDivAdd
            case "_divMenuBtnToDivAdd":

                SetDefaultpage(2);
                break;
            #endregion _divMenuBtnToDivAdd
            #region _divMenuBtnToDivAddScore
            case "_divMenuBtnToDivAddScore":
                SetDefaultpage(4);
                break;
            #endregion _divMenuBtnToDivAddScore
            #region _divMenuBtnToDivAddScore_backup
            case "_divMenuBtnToDivAddScore_backup":
                string url = "http://mas.taokaenoi.co.th/rcm-question";
                //Response.Write("<script>window.open('http://demo.taokaenoi.co.th/hr-employee-register','_blank');</script>");
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();

                StringBuilder s = new StringBuilder();
                s.Append("<html>");
                s.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
                s.AppendFormat("<form name='form' action='{0}' method='post'>", url);

                s.Append("</form></body></html>");
                response.Write(s.ToString());
                response.End();
                break;
            #endregion _divMenuBtnToDivAddScore_backup
            #region CmdAddDetail
            case "CmdAddDetail":

                string[] arg1_add = new string[6];
                arg1_add = e.CommandArgument.ToString().Split(';');

                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Add("class", "active");
                MvMaster.SetActiveView(ViewAdd);
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();

                txtchoice1.Text = "";
                txtchoice2.Text = "";
                txtchoice3.Text = "";
                txtchoice4.Text = "";
                txtcourse_item.Text = "";
                ddl_answer.SelectedValue = "0";
                ddlchooseqs.SelectedValue = "2";
                txtquest.Text = "";

                gettypequestion(ddltypequest);
                gettypeanswer(ddltypeans);
                getOrganizationList(ddlOrg);
                getDepartmentList(ddlDep, int.Parse(arg1_add[2]));
                getSectionList(ddlSec, int.Parse(arg1_add[2]), int.Parse(arg1_add[3]));
                getPositionList(ddlPos, int.Parse(arg1_add[2]), int.Parse(arg1_add[3]), int.Parse(arg1_add[4]));
                gettopicquestion(ddltopic, int.Parse(arg1_add[0]), int.Parse(arg1_add[2]), int.Parse(arg1_add[3]), int.Parse(arg1_add[4]), int.Parse(arg1_add[5]));
                ddltypequest.SelectedValue = arg1_add[0];
                ddltopic.SelectedValue = arg1_add[1];
                ddlOrg.SelectedValue = arg1_add[2];
                ddlDep.SelectedValue = arg1_add[3];
                ddlSec.SelectedValue = arg1_add[4];
                ddlPos.SelectedValue = arg1_add[5];

                ddlchooseqs.Enabled = false;
                ddltypequest.Enabled = false;
                ddltopic.Enabled = false;
                ddlOrg.Enabled = false;
                ddlDep.Enabled = false;
                ddlSec.Enabled = false;
                ddlPos.Enabled = false;
                div_choice.Visible = false;
                ddltypeans.Enabled = true;
                div_typequest.Visible = true;

                if (arg1_add[2] != "0")
                {
                    divchoosetype.Visible = true;
                }
                else
                {
                    divchoosetype.Visible = false;
                }

                panel_quest.Visible = true;


                linkBtnTrigger(btnAdddataset);
                div_save.Visible = false;

                CleardataSetFormList(GvReportAdd);
                GvReportAdd.Visible = false;

                CleardataSetFormList(GvWrite);
                GvWrite.Visible = false;

                CleardataSetPersonFormList(GvPerson);
                GvPerson.Visible = false;

                break;
            #endregion CmdAddDetail
            #region btnAdd
            case "btnAdd":
                if (ddlchooseqs.SelectedValue == "1")
                {
                    InsertTopicQuest(0, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue), int.Parse(ddlPos.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), 1, txttopic.Text);
                }
                else
                {
                    InsertQuestion();
                }

                SetDefaultpage(1);
                break;
            #endregion btnAdd
            #region CmdAdddataset
            case "CmdAdddataset":


                if (int.Parse(txtcourse_item.Text) < 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกลำดับให้ถูกต้อง!!!');", true);
                    return;
                }
                else
                {

                    if (imgupload_q.HasFile)
                    {

                        extension_q = Path.GetExtension(imgupload_q.FileName);
                        if (extension_q.ToLower() == ".jpg")
                        {
                            string getPathfile = ConfigurationManager.AppSettings["pathfile_recruit"];
                            string fileName_upload = ddltopic.SelectedValue + "/" + ddltypeans.SelectedValue + "/" + txtcourse_item.Text;
                            string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                            if (!Directory.Exists(filePath_upload))
                            {
                                Directory.CreateDirectory(filePath_upload);
                            }

                            imgupload_q.SaveAs(Server.MapPath(getPathfile + fileName_upload + "\\" + "q" + extension_q));
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะรูปภาพนามสกุลไฟล์.jpg เท่านั้น');", true);
                        }

                    }

                    if (ddltypeans.SelectedValue == "1")
                    {
                        if (imgupload_1.HasFile)
                        {
                            extension_1 = Path.GetExtension(imgupload_1.FileName);

                            if (extension_1.ToLower() == ".jpg")
                            {
                                string getPathfile = ConfigurationManager.AppSettings["pathfile_recruit"];
                                string fileName_upload = ddltopic.SelectedValue + "/" + ddltypeans.SelectedValue + "/" + txtcourse_item.Text;
                                string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                                if (!Directory.Exists(filePath_upload))
                                {
                                    Directory.CreateDirectory(filePath_upload);
                                }

                                imgupload_1.SaveAs(Server.MapPath(getPathfile + fileName_upload + "\\" + "1" + extension_1));
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะรูปภาพนามสกุลไฟล์.jpg เท่านั้น');", true);
                            }
                        }

                        if (imgupload_2.HasFile)
                        {
                            extension_2 = Path.GetExtension(imgupload_2.FileName);
                            if (extension_2.ToLower() == ".jpg")
                            {
                                string getPathfile = ConfigurationManager.AppSettings["pathfile_recruit"];
                                string fileName_upload = ddltopic.SelectedValue + "/" + ddltypeans.SelectedValue + "/" + txtcourse_item.Text;
                                string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                                if (!Directory.Exists(filePath_upload))
                                {
                                    Directory.CreateDirectory(filePath_upload);
                                }

                                imgupload_2.SaveAs(Server.MapPath(getPathfile + fileName_upload + "\\" + "2" + extension_2));
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะรูปภาพนามสกุลไฟล์.jpg เท่านั้น');", true);
                            }
                        }

                        if (imgupload_3.HasFile)
                        {
                            extension_3 = Path.GetExtension(imgupload_3.FileName);

                            if (extension_3.ToLower() == ".jpg")
                            {
                                string getPathfile = ConfigurationManager.AppSettings["pathfile_recruit"];
                                string fileName_upload = ddltopic.SelectedValue + "/" + ddltypeans.SelectedValue + "/" + txtcourse_item.Text;
                                string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                                if (!Directory.Exists(filePath_upload))
                                {
                                    Directory.CreateDirectory(filePath_upload);
                                }

                                imgupload_3.SaveAs(Server.MapPath(getPathfile + fileName_upload + "\\" + "3" + extension_3));
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะรูปภาพนามสกุลไฟล์.jpg เท่านั้น');", true);
                            }
                        }

                        if (imgupload_4.HasFile)
                        {
                            extension_4 = Path.GetExtension(imgupload_4.FileName);
                            if (extension_4.ToLower() == ".jpg")
                            {
                                string getPathfile = ConfigurationManager.AppSettings["pathfile_recruit"];
                                string fileName_upload = ddltopic.SelectedValue + "/" + ddltypeans.SelectedValue + "/" + txtcourse_item.Text;
                                string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                                if (!Directory.Exists(filePath_upload))
                                {
                                    Directory.CreateDirectory(filePath_upload);
                                }

                                imgupload_4.SaveAs(Server.MapPath(getPathfile + fileName_upload + "\\" + "4" + extension_4));
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะรูปภาพนามสกุลไฟล์.jpg เท่านั้น');", true);
                            }

                        }
                        setAddList_Form(txtcourse_item, txtquest, txtchoice1, txtchoice2, txtchoice3, txtchoice4, ddl_answer, GvReportAdd);

                        div_save.Visible = true;
                    }
                    else if (ddltypeans.SelectedValue == "2")
                    {
                        setAddList_FormWrite(txtcourse_item, txtquest, GvWrite);
                        div_save.Visible = true;
                    }
                    ddltypeans.Enabled = false;
                }

                break;
            #endregion CmdAdddataset
            #region CmdViewDetail
            case "CmdViewDetail":
                string[] arg1_ = new string[1];
                arg1_ = e.CommandArgument.ToString().Split(';');

                ViewState["m0_toidx"] = arg1_[0];
                ViewState["PIDX_addon"] = arg1_[1];
                SetDefaultpage(3);

                break;
            #endregion CmdViewDetail
            #region CmdDelQuest
            case "CmdDelQuest":
                string[] arg1_del = new string[4];
                arg1_del = e.CommandArgument.ToString().Split(';');


                string getPathfile_del = ConfigurationManager.AppSettings["pathfile_recruit"];
                string fileName_upload_del = arg1_del[1] + "/" + arg1_del[2] + "/" + arg1_del[3];
                string filePath_upload_del = Server.MapPath(getPathfile_del + fileName_upload_del);


                if (Directory.Exists(filePath_upload_del))
                {

                    foreach (string file in Directory.GetFiles(filePath_upload_del))
                    {
                        File.Delete(file);
                    }
                    Directory.Delete(filePath_upload_del);

                }

                DeleteQuestion(int.Parse(arg1_del[0]));
                SetDefaultpage(3);
                break;
            #endregion CmdDelQuest
            #region btnCancel
            case "btnCancel":
                //ViewState["Search_Data"] = "0";
                SetDefaultpage(1);
                break;
            #endregion btnCancel
            #region btnsearch
            case "btnsearch":
                ViewState["Search_Data"] = "1";

                ViewState["typequest_search"] = ddltypequest_search.SelectedValue;
                ViewState["orgidx_search"] = ddlOrg_search.SelectedValue;
                ViewState["rdeptidx_search"] = ddlDep_search.SelectedValue;
                ViewState["rsecidx_search"] = ddlSec_search.SelectedValue;
                ViewState["rpos_search"] = ddlPos_search.SelectedValue;

                select_gettypequestion(GvTypeTopic, int.Parse(ddltypequest_search.SelectedValue));

                break;
            #endregion btnsearch
            #region btnRefresh
            case "btnRefresh":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;
            #endregion btnRefresh
            #region CmdDataSet_Person
            case "CmdDataSet_Person":
                setAddList_Person(txttopic, ddlposition, ddlorg_person, ddlrdept_person, ddlrsec_person, ddlname_person, GvPerson);
                div_save.Visible = true;
                break;
            #endregion CmdDataSet_Person
            #region CmdAddPerson
            case "CmdAddPerson":
                btnaddperson.Visible = false;
                panel_chooseperson_addon.Visible = true;
                SelectMaPositionList(ddlposition_addon);
                getOrganizationList(ddlorg_person_addon);
                ddlposition_addon.SelectedValue = "0";
                ddlorg_person_addon.SelectedValue = "0";
                ddlrdept_person_addon.SelectedValue = "0";
                ddlrsec_person_addon.SelectedValue = "0";
                ddlname_person_addon.SelectedValue = "0";
                break;
            #endregion CmdAddPerson
            #region btnCancel_addon
            case "btnCancel_addon":
                btnaddperson.Visible = true;
                panel_chooseperson_addon.Visible = false;
                break;
            #endregion btnCancel_addon
            #region Cmdaddon_person
            case "Cmdaddon_person":
                InsertAddOnPersonal(int.Parse(ddlname_person_addon.SelectedValue), int.Parse(ViewState["m0_toidx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ddlposition_addon.SelectedValue)); //int.Parse(ViewState["PIDX_addon"].ToString()));
                select_person_responsibility(GvPerson_Detail, int.Parse(ViewState["m0_toidx"].ToString()));
                btnaddperson.Visible = true;
                panel_chooseperson_addon.Visible = false;
                break;
            #endregion Cmdaddon_person
            #region CmdDel_person
            case "CmdDel_person":
                string[] arg1_delete_person = new string[1];
                arg1_delete_person = e.CommandArgument.ToString().Split(';');

                DeletePerson(int.Parse(arg1_delete_person[0]), int.Parse(ViewState["EmpIDX"].ToString()));
                select_person_responsibility(GvPerson_Detail, int.Parse(ViewState["m0_toidx"].ToString()));
                break;
            #endregion CmdDel_person
            #region _divMenuBtnToDivManual
            case "_divMenuBtnToDivManual":
                Response.Write("<script>window.open('https://docs.google.com/document/d/1IRTsf74ku7P_m7ms_8pjh059RO-DzM0P-utk5gTCV7Q/edit?usp=sharing','_blank');</script>");

                break;
            #endregion _divMenuBtnToDivManual
            #region read_score_detail
            case "read_score_detail":


                int CEmpIDX_ = _funcTool.convertToInt(cmdArg);

                show_read_score_detail(CEmpIDX_);
                break;
            #endregion read_score_detail
            #region _backtolistscore
            case "_backtolistscore":
                if (cmdArg == "1")
                {
                    score_detail.Visible = true;
                    score_detail_sub.Visible = false;
                }
                else
                {
                    score_list.Visible = true;
                    score_detail.Visible = false;
                }

                break;
            #endregion _backtolistscore
            #region read_answer_u0
            case "read_answer_u0":

                LinkButton ddlName = (LinkButton)sender;
                GridViewRow gridrow = (GridViewRow)ddlName.NamingContainer;
                int rowIndex = gridrow.RowIndex;


                TextBox CEmpIDX = (TextBox)Gvscore_detail_u0.Rows[rowIndex].FindControl("CEmpIDX");
                Label name_topic = (Label)Gvscore_detail_u0.Rows[rowIndex].FindControl("topic_name");

                data_employee_recruit employee_recuit_exam = new data_employee_recruit();
                topic_data topic_data = new topic_data();
                topic_data.m0_toidx = _funcTool.convertToInt(cmdArg);
                topic_data.CEmpIDX = _funcTool.convertToInt(CEmpIDX.Text);
                employee_recuit_exam.topic_list = new topic_data[1];
                employee_recuit_exam.topic_list[0] = topic_data;
                // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(employee_recuit_exam));
                employee_recuit_exam = callServicePostEmpRe(_urlGetDataM0Exam, employee_recuit_exam);

                //txt.Text = employee_recuit_exam.topic_list[0].m0_toidx.ToString();
                if (employee_recuit_exam.return_code == "0")
                {
                    ViewState["topic_list"] = employee_recuit_exam.topic_list;
                    ViewState["type_answer_list"] = employee_recuit_exam.type_answer_list;
                    ViewState["question_list"] = employee_recuit_exam.question_list;
                    ViewState["recruit_u1_answer_list"] = employee_recuit_exam.recruit_u1_answer_list;
                    ViewState["recruit_u0_answer_list"] = employee_recuit_exam.recruit_u0_answer_list;


                    Repeater ChildRepeater = (Repeater)Viewscore.FindControl("ChildRepeater");

                    DataSet ds_sample_data = new DataSet();
                    ds_sample_data.Tables.Add("type");
                    ds_sample_data.Tables[0].Columns.Add("type_asn", typeof(String));
                    ds_sample_data.Tables[0].Columns.Add("m0_taidx", typeof(int));
                    ds_sample_data.Tables[0].Columns.Add("m0_toidx", typeof(int));
                    foreach (type_answer_data val in employee_recuit_exam.type_answer_list)
                    {
                        DataRow dr_Add_Sample = ds_sample_data.Tables[0].NewRow();
                        dr_Add_Sample["type_asn"] = val.type_asn;
                        dr_Add_Sample["m0_taidx"] = val.m0_taidx;
                        dr_Add_Sample["m0_toidx"] = employee_recuit_exam.topic_list[0].m0_toidx.ToString();

                        ds_sample_data.Tables[0].Rows.Add(dr_Add_Sample);
                    }
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(ds_sample_data));
                    ChildRepeater.DataSource = ds_sample_data;
                    ChildRepeater.DataBind();

                    s_topic_name.InnerText = name_topic.Text;
                    score_detail.Visible = false;
                    score_detail_sub.Visible = true;
                }
                break;
            #endregion read_answer_u0
            #region btn_confirm_score
            case "btn_confirm_score":
                add_score_comment();
                break;
            #endregion btn_confirm_score
            #region btn_src_score
            case "btn_src_score":
                TextBox txt_src_score = (TextBox)Viewscore.FindControl("txt_src_score");
                ViewState["word_src_score"] = txt_src_score.Text;
                src_score();

                break;
            #endregion btn_src_score
            #region btn_src_score_refresh
            case "btn_src_score_refresh":
                ViewState["word_src_score"] = string.Empty;
                TextBox txt_src_score_ = (TextBox)Viewscore.FindControl("txt_src_score");
                txt_src_score_.Text = string.Empty;
                src_score();
                break;
                #endregion btn_src_score_refresh
        }
    }
    protected void show_read_score_detail(int CEmpIDX = 0)
    {


        data_employee_recruit emp_data = new data_employee_recruit();
        recruit_u0_answer_data u0_answer = new recruit_u0_answer_data();
        emp_data.recruit_u0_answer_list = new recruit_u0_answer_data[1];

        u0_answer.CEmpIDX = CEmpIDX;
        emp_data.recruit_u0_answer_list[0] = u0_answer;
        //_urlGetlistscoreexam_detail
        ViewState["CEmpIDX"] = CEmpIDX;
        emp_data = callServicePostEmpRe(_urlGetlistscoreexam_detail, emp_data);

        setGridData(Gvscore_detail_u0, emp_data.recruit_u0_answer_list);
        score_list.Visible = false;
        score_detail.Visible = true;
    }
    protected void add_score_comment()
    {
        Repeater ChildRepeater = (Repeater)Viewscore.FindControl("ChildRepeater");
        int CEmpIDX = _funcTool.convertToInt(ViewState["CEmpIDX"].ToString());
        if (ChildRepeater.Items.Count > 0)
        {
            foreach (RepeaterItem temp_ChildRepeater in ChildRepeater.Items)
            {
                TextBox m0_taidx_child = (TextBox)temp_ChildRepeater.FindControl("m0_taidx");
                TextBox m0_toidx_child = (TextBox)temp_ChildRepeater.FindControl("m0_toidx");
                if (m0_taidx_child.Text != "2")
                {
                    continue;
                }
                else
                {
                    Repeater Repquestion = (Repeater)temp_ChildRepeater.FindControl("Repquestion");
                    if (Repquestion.Items.Count > 0)
                    {
                        recruit_u1_answer_data[] data_u1 = new recruit_u1_answer_data[Repquestion.Items.Count];
                        int ii = 0;
                        foreach (RepeaterItem temp_Repquestion in Repquestion.Items)
                        {
                            data_u1[ii] = new recruit_u1_answer_data();
                            TextBox m0_taidx_re = (TextBox)temp_Repquestion.FindControl("m0_taidx");
                            TextBox m0_toidx_re = (TextBox)temp_Repquestion.FindControl("m0_toidx");
                            TextBox m0_quidx_child = (TextBox)temp_Repquestion.FindControl("m0_quidx");
                            //TextBox CEmpIDX = (TextBox)temp_Repquestion.FindControl("CEmpIDX");
                            TextBox txt_score_comment = (TextBox)temp_Repquestion.FindControl("txt_score_comment");


                            data_u1[ii].score = _funcTool.convertToDecimal(txt_score_comment.Text);
                            data_u1[ii].CEmpIDX = CEmpIDX;
                            data_u1[ii].m0_quidx = _funcTool.convertToInt(m0_quidx_child.Text);
                            data_u1[ii].m0_toidx = _funcTool.convertToInt(m0_toidx_re.Text);
                            ii++;
                        }
                        data_employee_recruit employee_recuit_exam = new data_employee_recruit();
                        employee_recuit_exam.recruit_u1_answer_list = data_u1;
                        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(employee_recuit_exam));
                        employee_recuit_exam = callServicePostEmpRe(_urlSetscore_comment_exam, employee_recuit_exam);


                        break;
                    }
                    else
                    {
                        //CEmpIDX
                   
                        data_employee_recruit employee_recuit_exam = new data_employee_recruit();
                        recruit_u1_answer_data data_u1 = new recruit_u1_answer_data();
                        employee_recuit_exam.recruit_u1_answer_list = new recruit_u1_answer_data[1];
                        data_u1.CEmpIDX = CEmpIDX;
                        data_u1.m0_toidx = _funcTool.convertToInt(m0_toidx_child.Text);;
                        employee_recuit_exam.recruit_u1_answer_list[0] = data_u1;
                        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(employee_recuit_exam));
                        employee_recuit_exam = callServicePostEmpRe(_urlSetscore_comment_exam, employee_recuit_exam);
                        break;
                    }
                }
            }
        }


        show_read_score_detail(CEmpIDX);
        score_detail_sub.Visible = false;
    }

    #endregion
}
