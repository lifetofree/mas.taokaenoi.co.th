﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="HoldersDevices.aspx.cs" Inherits="websystem_ITServices_HoldersDevices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">




    <script type="text/javascript">
        function openModal() {
            $('#ordine').modal('show');

        }

    </script>


    <%--------- Tab Menu End-----------%>



    <%-- <div class="row">
        <div class="col-lg-12">
            <h3><b>Holder Devices : </b><small>ระบบผู้ถือครอง</small></h3>
        </div>
    </div>--%>

    <asp:Literal ID="text" runat="server"></asp:Literal>


    <%----------- Menu Tab Start---------------%>
    <div id="BoxTabMenuIndex" runat="server">

        <div class="form-group">
            <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Menu1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="sub-navbar">
                            <a class="navbar-brand " href="#"><b>Menu</b></a>
                        </div>
                    </div>

                    <div class="collapse navbar-collapse" id="Menu1">
                        <asp:LinkButton ID="lbindex" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand">ข้อมูลผู้ถือครอง</asp:LinkButton>
                        <asp:LinkButton ID="lbapprove" CssClass="btn_menulist" runat="server" CommandName="btnapprove" OnCommand="btnCommand">รายการรออนุมัติ</asp:LinkButton>
                    </div>
                </div>


            </nav>
        </div>
    </div>
    <%-------------- Menu Tab End--------------%>



    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">

            <div class="col-lg-12" id="div_process" runat="server">
                <div class="form-group">
                    <div class="col-lg-9"></div>
                    <div class="col-lg-3">
                        <asp:LinkButton ID="lblguide" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Manual" runat="server" CommandName="btnguide" OnCommand="btnCommand">คู่มือการใช้งาน</asp:LinkButton>
                        <asp:LinkButton ID="lblflow" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="FlowChart" runat="server" CommandName="btnflow" OnCommand="btnCommand">Flow Process</asp:LinkButton>

                        <%--<asp:HyperLink ID="_divMenuBtnToFlowPermanent" runat="server"
                           NavigateUrl="https://docs.google.com/document/d/1wpi0fRoJ3ur4oEW1zroPexDkZCm1Ge0bUBgIkLFS1zI/edit?usp=sharing" 
                           Target="_blank" Text="สิทธิ์ทั่วไป" />--%>
                    </div>
                </div>
                <hr />
            </div>

            <asp:UpdatePanel ID="update_search" runat="server">
                <ContentTemplate>


                    <%-------------- BoxSearch Start--------------%>
                 
                            <div class="col-lg-12" runat="server" id="BoxSearch">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ค้นหาข้อมูลผู้ถือครอง</strong></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">

                                                <div id="SETBoxAllSearch" runat="server">

                                                    <div class="form-group">
                                                        <asp:Label ID="Label59" runat="server" Text="ค้นหาองค์กร" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlSearchOrg" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:Label ID="Label4" runat="server" Text="ค้นหาฝ่าย" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlSearchDep" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- เลือกฝ่าย ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="ค้นหาแผนก" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlSearchSec" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- เลือกแผนก ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:Label ID="Label3" runat="server" Text="ค้นหาชื่อผู้ถือครอง" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlSearchHolder" AutoPostBack="true" CssClass="form-control" runat="server">
                                                                <asp:ListItem Value="0">--- เลือกผู้ถือครอง ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label1" runat="server" Text="ประเภทอุปกรณ์" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlSearch_devices"  runat="server" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>

                                                        <asp:Label ID="lbSearch" CssClass="col-sm-2 control-label" runat="server" Text="Search" />

                                                        <div class=" col-sm-3">
                                                            <asp:TextBox ID="txtSearchDevice" CssClass="form-control" runat="server" AutoComplete="off" placeholder="Search ....."></asp:TextBox>
                                                        </div>

                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-5 col-sm-offset-1">
                                                            <asp:LinkButton ID="btnsearch" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                                            <asp:LinkButton ID="btnRefresh" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="btnRefresh" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                    <%-- <hr />--%>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                      
                    <%-------------- BoxSearch End--------------%>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div id="BoxGridView" runat="server">
                        <div class="panel-heading">

                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="info"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnPageIndexChanging="Master_PageIndexChanging">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField HeaderText="รหัสอุปกรณ์" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbHoldersIDX" Visible="false" runat="server" Text='<%# Eval("DeviceIDX") %>' />
                                            <asp:Label ID="lbHolders" runat="server" Text='<%# Eval("DeviceCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Serial Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSerialNumber" runat="server" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbEqtName" runat="server" Text='<%# Eval("EqtName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ผู้ถือครอง" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbEmpName" runat="server" Text='<%# Eval("HDEmpFullName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="สถานะเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="25%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbView" CssClass="btn btn-success btn-sm" runat="server" data-original-title="ประวัติผู้ถือครอง" data-toggle="tooltip" Text="รายละเอียด" OnCommand="btnCommand" CommandArgument='<%# Eval("DeviceIDX") %>' CommandName="ViewHolder"><i class="glyphicon glyphicon-plus"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnsubholder" CssClass="btn btn-primary btn-sm" runat="server" data-original-title="โอนย้ายผู้ถือครอง" data-toggle="tooltip" Text="รายละเอียด" OnCommand="btnCommand" CommandArgument='<%# Eval("DeviceIDX") %>' CommandName="ViewChange"><i class="glyphicon glyphicon-share-alt"></i></asp:LinkButton>


                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>

                    </div>


                    <div id="div_showdata" runat="server" visible="false">


                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; รายละเอียดอุปกรณ์</strong></h3>
                            </div>
                            <div class="panel-body">
                                <%--   <div class="form-horizontal" role="form">--%>
                                <%--  <pre><asp:Label ID="Label18" runat="server" Text="รายละเอียดอุปกรณ์" Font-Size="18pt"></asp:Label></pre>--%>
                                
                                <asp:FormView ID="FvDetailUser" runat="server" Width="100%" DefaultMode="ReadOnly">
                                    <ItemTemplate>
                                        <div class="form-horizontal" role="form">
                                            <asp:Label ID="Label37" CssClass=" control-labelnotop" Visible="false" runat="server" Text='<%# Eval("OrgIDX") %>' />
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("unidx") %>' />
                                                    <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("acidx") %>' />
                                                    <asp:Label ID="lblholderidx" Visible="false" CssClass="control-label" runat="server" Text='<%# Eval("HolderIDX") %>' />
                                                    <asp:Label ID="lblDeviceIDX" Visible="false" CssClass="control-label" runat="server" Text='<%# Eval("DeviceIDX") %>' />

                                                    <asp:Label ID="Label23" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสอุปกรณ์ :" />
                                                    <div class="col-xs-3">
                                                        <asp:Label ID="Label22" CssClass="control-labelnotop" runat="server" Text='<%# Eval("DeviceCode") %>' />
                                                        <asp:Label ID="lblm0_tdidx" Visible="false" CssClass="control-labelnotop" runat="server" Text='<%# Eval("m0_tdidx") %>' />


                                                    </div>
                                                    <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ปรเะภทอุปกรณ์ :" />
                                                    <div class="col-xs-4">
                                                        <asp:Label ID="Label17" CssClass="control-labelnotop" runat="server" Text='<%# Eval("EqtName") %>' />
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label30" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท :" />
                                                    <div class="col-xs-3">
                                                        <asp:Label ID="Label34" CssClass=" control-labelnotop" runat="server" Text='<%# Eval("Organization") %>' />
                                                        
                                                        <asp:Label ID="lblorg" CssClass=" control-labelnotop" Visible="false" runat="server" Text='<%# Eval("OrgIDX") %>' />
                                                    </div>
                                                    <asp:Label ID="Label35" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย :" />
                                                    <div class="col-xs-4">
                                                        <asp:Label ID="Label36" CssClass=" control-labelnotop" runat="server" Text='<%# Eval("Dept") %>' />
                                                        <asp:Label ID="lblrdept" CssClass=" control-labelnotop" Visible="false" runat="server" Text='<%# Eval("RDeptIDX") %>' />
                                                        <asp:Label ID="lblNOrgIDX" CssClass=" control-labelnotop" Visible="false" runat="server" Text='<%# Eval("NOrgIDX") %>' />
                                                        <asp:Label ID="lblNRDeptIDX" CssClass=" control-labelnotop" Visible="false" runat="server" Text='<%# Eval("NRDeptIDX") %>' />
                                                        <asp:Label ID="lblNRSecIDX" CssClass=" control-labelnotop" Visible="false" runat="server" Text='<%# Eval("NRSecIDX") %>' />
                                                        <asp:Label ID="lblNHDEmpIDX" CssClass=" control-labelnotop" Visible="false" runat="server" Text='<%# Eval("NHDEmpIDX") %>' />
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-md-12">

                                                    <asp:Label ID="Label15" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Asset Code :" />
                                                    <div class="col-xs-3">
                                                        <asp:Label ID="lbltel" CssClass="control-labelnotop " runat="server" Text='<%# Eval("AccessCode") %>' />
                                                    </div>
                                                    <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="PO CODE :" />
                                                    <div class="col-xs-4">
                                                        <asp:Label ID="Label18" CssClass=" control-labelnotop " runat="server" Text='<%# Eval("POCode") %>' />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">

                                                    <asp:Label ID="Label14" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Serial Number :" />
                                                    <div class="col-xs-3">
                                                        <asp:Label ID="lbledit_sysidx" CssClass="control-labelnotop" runat="server" Text='<%# Eval("SerialNumber") %>' />
                                                    </div>

                                                    <asp:Label ID="Label77" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัทประกัน :" />
                                                    <div class="col-xs-4">
                                                        <asp:Label ID="txtedittopic" CssClass="  control-labelnotop" runat="server" Text='<%# Eval("InsuranceName") %>' />

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">

                                                    <asp:Label ID="Label67" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่ซื้อ :" />
                                                    <div class="col-sm-3">
                                                        <asp:Label ID="lbledit_mtidx" CssClass=" control-labelnotop" runat="server" Text='<%# Eval("PurchaseDate") %>' />
                                                    </div>
                                                    <asp:Label ID="Label16" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่หมดประกัน :" />
                                                    <div class="col-xs-4">
                                                        <asp:Label ID="Label19" CssClass="control-labelnotop" runat="server" Text='<%# Eval("InsuranceExp") %>' />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" runat="server" id="divshowapprover" visible="false">
                                                <div class="col-md-12">

                                                    <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ผู้อนุมัติลำดับที่ 1 :" />
                                                    <div class="col-sm-3">
                                                        <asp:Label ID="lblstatus" Visible="false" CssClass=" control-labelnotop" runat="server" Text='<%# Eval("HolderStatus") %>' />
                                                        <asp:Label ID="Label11" CssClass=" control-labelnotop" runat="server" Text='<%# Eval("approver1") %>' />
                                                    </div>
                                                    <asp:Label ID="Label31" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ผู้อนุมัติลำดับที่ 2 :" />
                                                    <div class="col-xs-4">
                                                        <asp:Label ID="Label32" CssClass="control-labelnotop" runat="server" Text='<%# Eval("approver2") %>' />
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </ItemTemplate>

                                </asp:FormView>


                                <%--   </div>--%>
                            </div>
                        </div>



                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-gift"></i><strong>&nbsp; รายละเอียดซอฟแวร์</strong></h3>
                            </div>
                            <div class="panel-body">

                                <%--  <div class="form-horizontal" role="form">--%>

                                <div class="form-group">
                                    <div class="checkbox checkbox-primary">
                                        <asp:Label ID="Label10" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ซอฟแวร์ : " />
                                        <div class="col-sm-4">
                                            <asp:CheckBoxList ID="chcksoft" Enabled="true"
                                                runat="server"
                                                CellPadding="10"
                                                CellSpacing="10"
                                                RepeatColumns="2"
                                                RepeatDirection="Vertical"
                                                RepeatLayout="Table"
                                                TextAlign="Right"
                                                Width="100%">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>


                                </div>
                                <%-- </div>--%>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-1 col-sm-offset-11">
                                    <asp:LinkButton ID="btnback1" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Back" runat="server" CommandName="btnback" OnCommand="btnCommand"><i class="fa fa-arrow-left"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ประวัติรายการผู้ถือครอง</strong></h3>
                            </div>

                            <div class="panel-body">
                                <%-- <div class="form-horizontal" role="form">--%>
                                <div class="form-group">

                                    <asp:LinkButton ID="btnaddholder" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddHolder" runat="server" CommandName="CmdAddHolder" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                                </div>

                                <%------------------------ Div ADD ผูกผู้ถือครอง Support and Holder ------------------------%>

                                <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; เพิ่มรายการผู้ถือครอง</strong></h4>
                                        <div class="form-horizontal" role="form">
                                            <%--<div class="panel panel-default">--%>
                                            <div class="panel-heading">


                                                <%--  <div class="form-group">
                                                    <asp:Label ID="Label8" runat="server" Text="ค้นหาองค์กร" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddladdholder_org" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatormmd16" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddladdholder_org" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกองค์"
                                                        ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatormmd16" Width="160" />


                                                    <asp:Label ID="Label11" runat="server" Text="ค้นหาฝ่าย" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddladdholder_dept" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">กรุณาเลือกฝ่าย....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidato34r1w10" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddladdholder_dept" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกฝ่าย"
                                                        ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender123" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidato34r1w10" Width="160" />

                                                </div>--%>

                                                <div class="form-group">
                                                    <asp:Label ID="Label12" runat="server" Text="ค้นหาแผนก" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddladdholder_sec" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">กรุณาเลือกแผนก....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <asp:Label ID="Label13" runat="server" Text="ค้นหาชื่อผู้ถือครอง" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddladdholder_emp" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0">กรุณาเลือกชื่อผู้ถือครอง....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddladdholder_emp" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกชื่อผู้ถือครอง"
                                                        ValidationExpression="กรุณาเลือกชื่อผู้ถือครอง" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator19" Width="160" />

                                                    <%--</div>

                                            <div class="form-group">--%>
                                                    <div class="col-sm-2">
                                                        <asp:LinkButton ID="btn_saveadduser" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save" OnCommand="btnCommand" CommandName="CmdSaveAddUser" data-toggle="tooltip" title="Save" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="CmdCancelAddUser" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>

                                            </div>
                                            <hr />
                                            <%--  </div>--%>
                                        </div>
                                    </div>


                                </asp:Panel>

                                <asp:GridView ID="GvHolder" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="primary"
                                    HeaderStyle-Height="40px"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnRowEditing="Master_RowEditing"
                                    OnRowCancelingEdit="Master_RowCancelingEdit"
                                    OnRowInserting="Master_RowInserting">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="#">

                                            <ItemTemplate>
                                                <asp:Label ID="lblUIDX" runat="server" Visible="false" Text='<%# Eval("HolderIDX") %>' />
                                                <%# (Container.DataItemIndex +1) %>
                                            </ItemTemplate>


                                            <EditItemTemplate>
                                                <div class="form-horizontal" role="form">
                                                    <div class="panel-heading">

                                                        <div class="form-group">
                                                            <asp:Label ID="Label5" runat="server" Text="เลือกองค์กร" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbOrg_Edit" runat="server" Text='<%# Bind("NOrgIDX") %>' Visible="false" />
                                                                <asp:DropDownList ID="ddlchangeholder_editorg" Enabled="false" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="ddlchangeholder_editorg" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกองค์"
                                                                ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />


                                                            <asp:Label ID="Label6" runat="server" Text="เลือกฝ่าย" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbDept_Edit" runat="server" Text='<%# Bind("NRDeptIDX") %>' Visible="false" />
                                                                <asp:DropDownList ID="ddlchangeholder_editdept" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">กรุณาเลือกฝ่าย....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="ddlchangeholder_editdept" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกฝ่าย"
                                                                ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label20" runat="server" Text="เลือกแผนก" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbSec_Edit" runat="server" Text='<%# Bind("NRSecIDX") %>' Visible="false" />
                                                                <asp:DropDownList ID="ddlchangeholder_editsec" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">กรุณาเลือกแผนก....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="ddlchangeholder_editsec" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกแผนก"
                                                                ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                                            <asp:Label ID="Label21" runat="server" Text="เลือกชื่อรับโอนย้าย" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbEmpIDX_Edit" runat="server" Text='<%# Bind("NHDEmpIDX") %>' Visible="false" />
                                                                <asp:DropDownList ID="ddlchangeholder_edituser" AutoPostBack="true" CssClass="form-control" runat="server">
                                                                    <asp:ListItem Value="0">กรุณาเลือกชื่อผู้ถือครอง....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="ddlchangeholder_edituser" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกชื่อผู้ถือครอง"
                                                                ValidationExpression="กรุณาเลือกชื่อผู้ถือครอง" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                                        </div>


                                                        <div class="form-group">
                                                            <asp:Label ID="Label27" runat="server" Text="ผู้อนุมัติผู้ถือครอง" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlapp1_edit" Enabled="false" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">กรุณาเลือกผู้อนุมัติ....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="ddlapp1_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกแผนก"
                                                                ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                                            <div id="div_approve2" runat="server" visible="true">
                                                                <asp:Label ID="Label28" runat="server" Text="ผู้อนุมัติผู้รับถือครอง" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                                <div class="col-sm-3">
                                                                    <asp:DropDownList ID="ddlapp2_edit" Enabled="false" AutoPostBack="true" CssClass="form-control" runat="server">
                                                                        <asp:ListItem Value="0">กรุณาเลือกผู้อนุมัติ....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                    ControlToValidate="ddlapp2_edit" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกชื่อผู้ถือครอง"
                                                                    ValidationExpression="กรุณาเลือกชื่อผู้ถือครอง" InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label29" runat="server" Text="สถานะรายการ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddl_status" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">ดำเนินการ</asp:ListItem>
                                                                    <asp:ListItem Value="1">ยกเลิก</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                        </div>



                                                        <div class="form-group">
                                                            <div class="col-sm-2 col-sm-offset-10">
                                                                <asp:LinkButton ID="btnchangeholder" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" OnCommand="btnCommand" CommandName="CmdSaveEditChangeUser" data-toggle="tooltip" title="Save" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>


                                            </EditItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="บริษัท" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lborgIDX" runat="server" Visible="false" Text='<%# Eval("OrgIDX") %>'></asp:Label>
                                                <asp:Label ID="lborg" runat="server" Text='<%# Eval("Organization") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDept" Visible="false" runat="server" Text='<%# Eval("RDeptIDX") %>'></asp:Label>
                                                <asp:Label ID="lblDept1" Visible="false" runat="server" Text='<%# Eval("NRDeptIDX") %>'></asp:Label>
                                                <asp:Label ID="lbdep" runat="server" Text='<%# Eval("Dept") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbsecName" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbposName" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHDEmpIDX" Visible="false" runat="server" Text='<%# Eval("HDEmpIDX") %>'></asp:Label>
                                                <asp:Label ID="lblCEmpIDX" Visible="false" runat="server" Text='<%# Eval("CEmpIDX") %>'></asp:Label>
                                                <%--  <asp:Label ID="lblNHDEmpIDX" Visible="true" runat="server" Text='<%# Eval("NHDEmpIDX") %>'></asp:Label>--%>
                                                <asp:Label ID="lbEmpName" runat="server" Text='<%# Eval("HDEmpFullName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%--  <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="25%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbRemark" runat="server" Text='<%# Eval("Remark") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbstate_ok" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                                <asp:Label ID="lbstate_wait" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-hourglass"></i></asp:Label>
                                                <asp:Label ID="lbstate_no" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-ban-circle"></i></asp:Label>
                                                <asp:Label ID="lblhoderstate" Visible="false" runat="server" Text='<%# Bind("HolderStatus") %>'></asp:Label>
                                                <%--<asp:Label ID="lblrt_Holder" runat="server" Text=""></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="Edit_Log" CssClass="btn btn-default  btn-sm" runat="server" OnCommand="btnCommand" CommandArgument='<%# Eval("HolderIDX") %>' CommandName="Edit_Log" data-toggle="tooltip" title="ประวัติการอนุมัติ"><i class="glyphicon glyphicon-eye-open"></i></asp:LinkButton>
                                                <asp:LinkButton ID="Edit" CssClass="btn btn-primary  btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>





                                <%-- </div>--%>
                            </div>
                        </div>


                        <div class="col-lg-12" runat="server" id="Div_Log">
                            <div id="ordine" class="modal open" role="dialog">
                                <div class="modal-dialog" style="width: 70%;">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">ประวัติผู้ถือครอง</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <asp:Label ID="lblUIDX" runat="server" Visible="false" Text='<%# Eval("HolderIDX") %>' />
                                                    <asp:Repeater ID="rptLog" runat="server">
                                                        <HeaderTemplate>
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label"><small>วัน / เวลา</small></label>
                                                                <label class="col-sm-3 control-label"><small>ผู้ดำเนินการ</small></label>
                                                                <label class="col-sm-2 control-label"><small>ดำเนินการ</small></label>
                                                                <label class="col-sm-2 control-label"><small>ผลการดำเนินการ</small></label>
                                                                <label class="col-sm-2 control-label"><small>ความคิดเห็น</small></label>
                                                            </div>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><%#Eval("createdate")%></small></span>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <span><small><%# Eval("EmpName") %>&nbsp;&nbsp;&nbsp;(<%# Eval("actor_des") %>)</small></span>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <span><small><%# Eval("node_name") %></small></span>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <span><small><%# Eval("status_name") %></small></span>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <span><small><%# Eval("comment") %></small></span>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>

                                                    <br />

                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <%--<asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" OnCommand="btnCommand" CommandName="btnCancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                               <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>--%>
                                                            <asp:LinkButton ID="Cancel" class="btn btn-default btn-sm" data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal" CommandName="Cancel">&nbsp;Close</asp:LinkButton>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%------------------------ Div Approve ผูกผู้ถือครอง Support and Holder ------------------------%>

                        <asp:Panel ID="panel_approveholder" runat="server" Visible="false">

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; ผลการอนุมัติ</strong></h3>
                                </div>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <div class="col-lg-offset-3 control-labelnotop1">

                                                <asp:Label ID="Label33" CssClass="col-sm-3 control-label text_right" runat="server" Text="สถานะอนุมัติ" />
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddl_approveholder" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="0">เลือกสถานะอนุมัติ</asp:ListItem>
                                                        <asp:ListItem Value="5">อนุมัติ</asp:ListItem>
                                                        <asp:ListItem Value="6">ไม่อนุมัติ </asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddl_approveholder" Font-Size="11"
                                                    ErrorMessage="เลือกสถานะอนุมัติ"
                                                    ValidationExpression="เลือกสถานะอนุมัติ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />

                                                <asp:LinkButton ID="btnupdateholder" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdUpdateHolder" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btncancelupdateholder" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefresh" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </asp:Panel>


                        <%------------------------ Div Approveโอนย้ายผู้ถือครอง ------------------------%>

                        <asp:Panel ID="panel_approvechangeHolder" runat="server" Visible="false">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-share-alt"></i><strong>&nbsp; ผลการอนุมัติการโอนย้าย</strong></h3>
                                </div>

                                <div class="panel-body">
                                    <%-- <div class="form-horizontal" role="form">--%>
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ผลการอนุมัติ</strong></h4>
                                        <div class="form-horizontal" role="form">
                                            <%--<div class="panel panel-default">--%>
                                            <div class="panel-heading">


                                                <div class="form-group">
                                                    <asp:Label ID="Label25" CssClass="col-sm-2 control-label" runat="server" Text="สถานะอนุมัติ" />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddl_changeApprove" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0">เลือกสถานะอนุมัติ</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddl_approveholder" Font-Size="11"
                                                        ErrorMessage="เลือกสถานะอนุมัติ"
                                                        ValidationExpression="เลือกสถานะอนุมัติ" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />


                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label26" runat="server" Text="เหตุผลการโอนย้าย" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtremark_approve" TextMode="multiline" Width="510pt" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                    </div>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtremark_approve" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกความคิดเห็น"
                                                        ValidationExpression="กรุณากรอกความคิดเห็น"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                        ValidationGroup="Save" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtcomment"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                </div>


                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-offset-9">
                                                        <asp:LinkButton ID="LinkButton1" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdUpdateHolder" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefresh" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>


                                                <%-- </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </asp:Panel>


                        <%------------------------ Div โอนย้ายผู้ถือครอง ------------------------%>

                        <div class="panel panel-info" runat="server" id="Div_ChangeHolder" visible="false">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-share-alt"></i><strong>&nbsp; โอนย้ายผู้ถือครอง</strong></h3>
                            </div>

                            <div class="panel-body">
                                <%-- <div class="form-horizontal" role="form">--%>
                                <div class="form-group">

                                    <asp:LinkButton ID="lbchangeholder" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Add Holder" runat="server" CommandName="CmdChangeHolder" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                                </div>


                                <asp:Panel ID="Panel_changeholder" runat="server" Visible="false">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; โอนย้ายรายการผู้ถือครอง</strong></h4>
                                        <div class="form-horizontal" role="form">
                                            <%--<div class="panel panel-default">--%>
                                            <div class="panel-heading">


                                                <div class="form-group">
                                                    <asp:Label ID="Label5" runat="server" Text="เลือกองค์กร" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlchangeholder_org" Enabled="false" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlchangeholder_org" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกองค์"
                                                        ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />


                                                    <asp:Label ID="Label6" runat="server" Text="เลือกฝ่าย" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlchangeholder_dept" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกฝ่าย ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlchangeholder_dept" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกฝ่าย"
                                                        ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label20" runat="server" Text="เลือกแผนก" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlchangeholder_sec" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกแผนก ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlchangeholder_sec" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกแผนก"
                                                        ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                                    <asp:Label ID="Label21" runat="server" Text="เลือกชื่อรับโอนย้าย" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlchangeholder_user" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0">--- เลือกผู้ถือครอง ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlchangeholder_user" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกชื่อผู้ถือครอง"
                                                        ValidationExpression="กรุณาเลือกชื่อผู้ถือครอง" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                                </div>


                                                <div class="form-group">
                                                    <asp:Label ID="Label27" runat="server" Text="ผู้อนุมัติผู้ถือครอง" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlapp1" Enabled="false" AutoPostBack="true" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">กรุณาเลือกผู้อนุมัติ....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlapp1" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกผู้อนุมัติ"
                                                        ValidationExpression="กรุณาเลือกผู้อนุมัติ" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                                    <div id="div_approve2" runat="server" visible="true">
                                                        <asp:Label ID="Label28" runat="server" Text="ผู้อนุมัติผู้รับถือครอง" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlapp2" Enabled="false" AutoPostBack="true" CssClass="form-control" runat="server">
                                                                <asp:ListItem Value="0">กรุณาเลือกผู้อนุมัติ....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="ddlapp2" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกชื่อผู้ถือครอง"
                                                            ValidationExpression="กรุณาเลือกชื่อผู้ถือครอง" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <asp:Label ID="Label24" runat="server" Text="เหตุผลการโอนย้าย" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtcomment" TextMode="multiline" Width="510pt" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                    </div>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtcomment" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกความคิดเห็น"
                                                        ValidationExpression="กรุณากรอกความคิดเห็น"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                        ValidationGroup="Save" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtcomment"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                </div>


                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnchangeholder" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save" OnCommand="btnCommand" CommandName="CmdSaveChangeUser" data-toggle="tooltip" title="Save" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelchange" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="CmdCancelChangeUser" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>


                                            </div>

                                            <%--  </div>--%>
                                        </div>
                                    </div>
                                </asp:Panel>



                                <%-- </div>--%>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-1 col-sm-offset-11">
                                <asp:LinkButton ID="btnback" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Back" runat="server" CommandName="btnback" OnCommand="btnCommand"><i class="glyphicon glyphicon-home"></i></asp:LinkButton>
                            </div>
                        </div>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </asp:View>

        <asp:View ID="ViewApprove" runat="server">

            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div id="BoxGridViewApprove" runat="server">
                        <div class="panel-heading">

                            <asp:GridView ID="GvApprove" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="info"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnPageIndexChanging="Master_PageIndexChanging">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField HeaderText="รหัสอุปกรณ์" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbHoldersIDX" Visible="false" runat="server" Text='<%# Eval("DeviceIDX") %>' />
                                            <asp:Label ID="lbHolders" runat="server" Text='<%# Eval("DeviceCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Serial Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSerialNumber" runat="server" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbEqtName" runat="server" Text='<%# Eval("EqtName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ผู้ถือครองปัจจุบัน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbEmpName" runat="server" Text='<%# Eval("HDEmpFullName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="สถานะเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="25%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbView" CssClass="btn btn-info btn-sm" runat="server" data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด" OnCommand="btnCommand" CommandArgument='<%# Eval("DeviceIDX") %>' CommandName="ViewHolderApprove"><i class="glyphicon glyphicon-list-alt"></i></asp:LinkButton>
                                            <%--<asp:LinkButton ID="btnsubholder" CssClass="btn btn-primary btn-sm" runat="server" data-original-title="โอนย้ายผู้ถือครอง" data-toggle="tooltip" Text="รายละเอียด" OnCommand="btnCommand" CommandArgument='<%# Eval("DeviceIDX") %>' CommandName="ViewChangeApprove"><i class="glyphicon glyphicon-user"></i></asp:LinkButton>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>

                    </div>



                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

    </asp:MultiView>


    <%-- </ContentTemplate>
 </asp:UpdatePanel>--%>
</asp:Content>

