﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_news : System.Web.UI.Page
{
    string sScript = @"<script type=""text/javascript"" $(window).on('load', function () {
                $('#myModal').modal('show');
    })";

            
       

    function_tool _function_Tool = new function_tool();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0);
    }

    protected void setActiveTab(string activeTab, int uidx, int doc_decision)
    {
        //mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
        li0.Attributes.Add("class", "");
        li1.Attributes.Add("class", "");
        li2.Attributes.Add("class", "");
        li3.Attributes.Add("class", "");


        table.Visible = false;
        tablelist.Visible = false;
        uploadimg.Visible = false;
        uploadimgMain.Visible = false;
        tablenews.Visible = false;

        lbCreate.Visible = false;
        lbCreatetab1.Visible = false;

        //
        lbCreate.Visible = false;
        lbCreatetab1.Visible = false;
        FvInsertEdit.Visible = false;
        Fvtab1.Visible = false;


        upload.Visible = false;
        Viewtab0.Visible = false;
        fvtest.Visible = false;
       
        uploadMain.Visible = false;
        LinkButton14.Visible = false;
        LinkButton15.Visible = false;
        Fvimg.Visible = false;
        uploadimgMain.Visible = false;
        switch (activeTab)
        {
            case "tab0":
                li0.Attributes.Add("class", "active");
                mvSystem.SetActiveView((View)mvSystem.FindControl("tab0"));
                _function_Tool.setFvData(tablenews, FormViewMode.Insert, null);
                tablenews.Visible = true;
                break;

            case "tab1":
                li1.Attributes.Add("class", "active");
                mvSystem.SetActiveView((View)mvSystem.FindControl("tab1"));
                _function_Tool.setFvData(tablelist, FormViewMode.Insert, null);
                tablelist.Visible = true;
                lbCreatetab1.Visible = true;

                break;
            case "tab2":
                li2.Attributes.Add("class", "active");

                mvSystem.SetActiveView((View)mvSystem.FindControl("tab2"));
                _function_Tool.setFvData(table, FormViewMode.Insert, null);
                table.Visible = true;
                lbCreate.Visible = true;

                break;
            case "tab3":
                li3.Attributes.Add("class", "active");

                mvSystem.SetActiveView((View)mvSystem.FindControl("tabuploadimg"));
                _function_Tool.setFvData(upload, FormViewMode.Insert, null);
                upload.Visible = true;
                LinkButton14.Visible = true;
                //lbCreate.Visible = true;

                break;
            case "tab4":
                li3.Attributes.Add("class", "active");

                mvSystem.SetActiveView((View)mvSystem.FindControl("tabuploadimgMain"));
                _function_Tool.setFvData(uploadMain, FormViewMode.Insert, null);
                uploadMain.Visible = true;
                LinkButton15.Visible = true;

                //lbCreate.Visible = true;

                break;
                

        }
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        lbCreate.Visible = false;
        lbCreatetab1.Visible = false;

        FvInsertEdit.Visible = false;
        Fvtab1.Visible = false;
        upload.Visible = false;
        Viewtab0.Visible = false;
        fvtest.Visible = false;
        tablenews.Visible = false;
        uploadMain.Visible = false;
        LinkButton14.Visible = false;
        LinkButton15.Visible = false;
        Fvimg.Visible = false;
        uploadimgMain.Visible = false;
        table.Visible = false;
        tablelist.Visible = false;
        switch (cmdName)
        {
            case "cmdCreate":
                _function_Tool.setFvData(FvInsertEdit, FormViewMode.Insert, null);
                FvInsertEdit.Visible = true;
                break;

            case "cmdCreate2":
                _function_Tool.setFvData(Fvtab1, FormViewMode.Insert, null);
                Fvtab1.Visible = true;
                break;

            case "cmdCancel":
                lbCreate.Visible = true;
                FvInsertEdit.Visible = false;
                Fvtab1.Visible = false;
                break;

            case "cmdSave":
                lbCreate.Visible = true;
                FvInsertEdit.Visible = false;
                table.Visible = true;
                break;
            case "cmdSaveFvtab1":
                lbCreatetab1.Visible = true;
                Fvtab1.Visible = false;
                tablelist.Visible = true;
                break;

            case "tab0onclick":
                mvSystem.SetActiveView((View)mvSystem.FindControl("tab0onclick"));
                _function_Tool.setFvData(Viewtab0, FormViewMode.Insert, null);
                Viewtab0.Visible = true;
                break;
            case "tab0onclick2":
                //mvSystem.SetActiveView((View)mvSystem.FindControl("tab0onclick2"));
                //_function_Tool.setFvData(Viewtab02, FormViewMode.Insert, null);
                //Viewtab02.Visible = true;

                mvSystem.SetActiveView((View)mvSystem.FindControl("viewtest"));
                _function_Tool.setFvData(fvtest, FormViewMode.Insert, null);
                fvtest.Visible = true;

                break;

            case "cmdre":
                mvSystem.SetActiveView((View)mvSystem.FindControl("tab0onclick"));
                _function_Tool.setFvData(Viewtab0, FormViewMode.Insert, null);
                Viewtab0.Visible = true;
                break;
            case "cmdreset":
                mvSystem.SetActiveView((View)mvSystem.FindControl("tab0"));
                _function_Tool.setFvData(tablenews, FormViewMode.Insert, null);
                tablenews.Visible = true;
                break;

            case "btnuploadimg":
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "load", sScript,true);
                mvSystem.SetActiveView((View)mvSystem.FindControl("tabuploadimg"));
                _function_Tool.setFvData(upload, FormViewMode.Insert, null);
                upload.Visible = true;
                
               
                break;
                case "uploadimgMain":
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "load", sScript,true);
                //mvSystem.SetActiveView((View)mvSystem.FindControl("tabuploadimgMain"));
                _function_Tool.setFvData(uploadimgMain, FormViewMode.Insert, null);
                uploadimgMain.Visible = true;
                break;
                
                case "cmdCreateimg":
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "load", sScript,true);
                
                _function_Tool.setFvData(Fvimg, FormViewMode.Insert, null);
                Fvimg.Visible = true;
                break;


        }
    }



    }