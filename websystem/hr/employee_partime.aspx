﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="employee_partime.aspx.cs" Inherits="websystem_hr_employee_partime" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script type="text/javascript">


        //no post back
        $('.clockpicker').datetimepicker({
            format: 'HH:mm',
            stepping: 15,
            ignoreReadonly: true
        });
        $('.show-time-onclick').click(function () {
            $('.clockpicker').data("DateTimePicker").show();
        });

        $('.clockpickerto').datetimepicker({
            format: 'HH:mm',
            stepping: 15,
            ignoreReadonly: true
        });
        $('.show-timeto-onclick').click(function () {
            $('.clockpickerto').data("DateTimePicker").show();
        });

        //is
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $('.loading-icon-approve').hide();

            $('.clockpicker').datetimepicker({
                format: 'HH:mm',
                stepping: 15,
                ignoreReadonly: true
            });
            $('.show-time-onclick').click(function () {
                $('.clockpicker').data("DateTimePicker").show();
            });

            $('.clockpickerto').datetimepicker({
                format: 'HH:mm',
                stepping: 15,
                ignoreReadonly: true
            });
            $('.show-timeto-onclick').click(function () {
                $('.clockpickerto').data("DateTimePicker").show();
            });
        });


        function getAlertSuccess() {
            $("#success_alert").show();
            $("#success_alert").fadeTo(500, 500).slideUp(500, function () {
                $("#success_alert").slideUp(500);
            });
        }

        function ValidatePage(toHide, toShow) {

            $(toHide).hide();
            $(toShow).show();

        }
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; Partime</strong></h3>
                        </div>

                        <div class="panel-body">
                            <%-- <div class="form-horizontal" role="form">--%>
                            <div class="form-group">

                                <asp:LinkButton ID="btnaddholder" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddStatus" runat="server" CommandName="CmdAdd" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <%------------------------ Div ADD  ------------------------%>
                            <asp:Label ID="fsa" runat="server"></asp:Label>
                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Partime</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <%--<div class="panel panel-default">--%>
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="รหัสข้อตกลง" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" placeholder="Ex. D 08:00 ......." />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="รหัสข้อตกลง (TH)" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="txtname_th" runat="server" CssClass="form-control" placeholder="Ex. D 08:00 [เช้า] 08:00 - 17:00 ......." />
                                                </div>
                                            </div>

                                            <%--<div class="form-group">
                                                <asp:Label ID="Label2" runat="server" Text="รหัสข้อตกลง (EN)" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="txtname_en" runat="server" CssClass="form-control" placeholder="........" />
                                                </div>
                                            </div>--%>

                                            <%--<div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="หมายเหตุ" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="txtetc" runat="server" CssClass="form-control" placeholder="........" />
                                                </div>
                                            </div>--%>

                                            <div class="form-group">
                                                <asp:Label ID="Label8" runat="server" Text="บริษัท" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddl_org_add" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label25" CssClass="col-sm-2 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddl_status" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1">Online.....</asp:ListItem>
                                                        <asp:ListItem Value="0">Offline.....</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label7" runat="server" Text="เวลาเริ่มงาน" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                <div class="col-sm-2">
                                                    <div class="input-group time">
                                                        <asp:TextBox ID="txt_time_day_start" placeholder="เวลาเริ่มงาน ..." runat="server" CssClass="form-control clockpicker cursor-pointer" value="" />
                                                        <span class="input-group-addon show-time-onclick cursor-pointer">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-2">
                                                    <div class="input-group time">
                                                        <asp:TextBox ID="txt_time_day_end" placeholder="เวลาเลิกงาน ..." runat="server" CssClass="form-control clockpicker cursor-pointer" value="" />
                                                        <span class="input-group-addon show-time-onclick cursor-pointer">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label6" runat="server" Text="เวลาพัก" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                <div class="col-sm-2">
                                                    <div class="input-group time">
                                                        <asp:TextBox ID="txt_time_part_start" placeholder="เวลาเริ่มพัก ..." runat="server" CssClass="form-control clockpicker cursor-pointer" value="" />
                                                        <span class="input-group-addon show-time-onclick cursor-pointer">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-2">
                                                    <div class="input-group time">
                                                        <asp:TextBox ID="txt_time_part_end" placeholder="เวลาเลิกพัก ..." runat="server" CssClass="form-control clockpicker cursor-pointer" value="" />
                                                        <span class="input-group-addon show-time-onclick cursor-pointer">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label10" runat="server" Text="วันทำงาน" CssClass="col-sm-2 control-label" Visible="true"></asp:Label>
                                                <div class="col-sm-9 col-sm-offset-1">
                                                    <div class="checkbox checkbox-primary">
                                                        <asp:CheckBoxList ID="YrChkBox"
                                                            runat="server"
                                                            CellPadding="7"
                                                            CellSpacing="7"
                                                            RepeatColumns="1"
                                                            RepeatDirection="Vertical"
                                                            RepeatLayout="Table"
                                                            TextAlign="Right"
                                                            Width="100%">
                                                            <asp:ListItem Value="1" Selected="True"> อาทิตย์ </asp:ListItem>
                                                            <asp:ListItem Value="2" Selected="True"> จันทร์ </asp:ListItem>
                                                            <asp:ListItem Value="3" Selected="True"> อังคาร </asp:ListItem>
                                                            <asp:ListItem Value="4" Selected="True"> พุธ </asp:ListItem>
                                                            <asp:ListItem Value="5" Selected="True"> พฤหัส </asp:ListItem>
                                                            <asp:ListItem Value="6" Selected="False"> ศุกร์ </asp:ListItem>
                                                            <asp:ListItem Value="7" Selected="False"> เสาร์ </asp:ListItem>
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <hr />
                                    <%--  </div>--%>
                                </div>
                            </asp:Panel>

                            <div id="div_search" runat="server">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">

                                        <asp:Label ID="Label160" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlorg_search" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกองค์กร....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Label ID="Label2" runat="server" Text="ชื่อกะ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlshift_search" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกชื่อกะ....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">

                                            <asp:LinkButton ID="LinkButton36" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdSearch" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton37" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefresh" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="m0_parttime_idx"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblIDX" runat="server" Visible="false" Text='<%# Eval("m0_parttime_idx") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_parttime_idx")%>' />
                                                            <asp:TextBox ID="txtparttime_workdays" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("parttime_workdays")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="รหัสข้อตกลง" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-4">
                                                            <asp:TextBox ID="txtcode_update" runat="server" CssClass="form-control" Text='<%# Eval("parttime_code")%>' placeholder="Ex. D 08:00 ......." />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lblnameth_edit" runat="server" Text="ชื่อกะ (TH)" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-4">
                                                            <asp:TextBox ID="txtNameTH_update" runat="server" CssClass="form-control" Text='<%# Eval("parttime_name_th")%>' placeholder="Ex. D 08:00 [เช้า] 08:00 - 17:00 ......." />
                                                        </div>
                                                    </div>

                                                    <%--<div class="form-group">
                                                        <asp:Label ID="lblnameen_edit" runat="server" Text="ชื่อกะ (EN)" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-4">
                                                            <asp:TextBox ID="txtNameEN_update" runat="server" CssClass="form-control" Text='<%# Eval("parttime_name_eng")%>' placeholder="......." />
                                                        </div>
                                                    </div>--%>

                                                    <%--<div class="form-group">
                                                        <asp:Label ID="Label3" runat="server" Text="หมายเหตุ" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                                        <div class="col-sm-4">
                                                            <asp:TextBox ID="txtetc_edit" runat="server" CssClass="form-control" Text='<%# Eval("parttime_note")%>' placeholder="........" />
                                                        </div>
                                                    </div>--%>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label7" runat="server" Text="เวลาเริ่มงาน" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                                        <div class="col-sm-2">
                                                            <div class="input-group time">
                                                                <asp:TextBox ID="txt_time_day_start_update" placeholder="เวลาเริ่มงาน ..." runat="server" Text='<%# Eval("parttime_start_time")%>' CssClass="form-control clockpicker cursor-pointer" value="" />
                                                                <span class="input-group-addon show-time-onclick cursor-pointer">
                                                                    <span class="glyphicon glyphicon-time"></span>
                                                                </span>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-2">
                                                            <div class="input-group time">
                                                                <asp:TextBox ID="txt_time_day_end_update" placeholder="เวลาเลิกงาน ..." runat="server" Text='<%# Eval("parttime_end_time")%>' CssClass="form-control clockpicker cursor-pointer" value="" />
                                                                <span class="input-group-addon show-time-onclick cursor-pointer">
                                                                    <span class="glyphicon glyphicon-time"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label6" runat="server" Text="เวลาพัก" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                                        <div class="col-sm-2">
                                                            <div class="input-group time">
                                                                <asp:TextBox ID="txt_time_part_start_update" placeholder="เวลาเริ่มพัก ..." runat="server" Text='<%# Eval("parttime_break_start_time")%>' CssClass="form-control clockpicker cursor-pointer" />
                                                                <span class="input-group-addon show-time-onclick cursor-pointer">
                                                                    <span class="glyphicon glyphicon-time"></span>
                                                                </span>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-2">
                                                            <div class="input-group time">
                                                                <asp:TextBox ID="txt_time_part_end_update" placeholder="เวลาเลิกพัก ..." runat="server" Text='<%# Eval("parttime_break_end_time")%>' CssClass="form-control clockpicker cursor-pointer" value="" />
                                                                <span class="input-group-addon show-time-onclick cursor-pointer">
                                                                    <span class="glyphicon glyphicon-time"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label8" runat="server" Text="บริษัท" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-4">
                                                            <asp:Label ID="lblorg_idx" runat="server" Visible="false" Text='<%# Eval("org_idx") %>' />
                                                            <asp:DropDownList ID="ddl_org_update" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">กรุณาเลือกสำนักงาน.....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label4" runat="server" Text="สถานะ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("parttime_status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label10" runat="server" Text="วันทำงาน" CssClass="col-sm-2 control-label" Visible="true"></asp:Label>
                                                        <div class="col-sm-9 col-sm-offset-1">
                                                            <div class="checkbox checkbox-primary">
                                                                <asp:CheckBoxList ID="YrChkBox_update"
                                                                    runat="server"
                                                                    CellPadding="7"
                                                                    CellSpacing="7"
                                                                    RepeatColumns="1"
                                                                    RepeatDirection="Vertical"
                                                                    RepeatLayout="Table"
                                                                    TextAlign="Right"
                                                                    Width="100%">
                                                                    <asp:ListItem Value="1"> อาทิตย์ </asp:ListItem>
                                                                    <asp:ListItem Value="2"> จันทร์ </asp:ListItem>
                                                                    <asp:ListItem Value="3"> อังคาร </asp:ListItem>
                                                                    <asp:ListItem Value="4"> พุธ </asp:ListItem>
                                                                    <asp:ListItem Value="5"> พฤหัส </asp:ListItem>
                                                                    <asp:ListItem Value="6"> ศุกร์ </asp:ListItem>
                                                                    <asp:ListItem Value="7"> เสาร์ </asp:ListItem>
                                                                </asp:CheckBoxList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-2">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="บริษัท" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lborg_name11" runat="server" Text='<%# Eval("org_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รหัสกะ" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbcode" runat="server" Text='<%# Eval("parttime_code") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อกะ (TH)" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblnameth" runat="server" Text='<%# Eval("parttime_name_th") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เวลาเข้างาน - เลิกงาน" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstart_day" runat="server" Text='<%# Eval("parttime_start_time") %>'></asp:Label>
                                            -
                                            <asp:Label ID="lblend_day" runat="server" Text='<%# Eval("parttime_end_time") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เวลาเริ่มพัก - เลิกพัก" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstart_break" runat="server" Text='<%# Eval("parttime_break_start_time") %>'></asp:Label>
                                            -
                                            <asp:Label ID="lblend_break" runat="server" Text='<%# Eval("parttime_break_end_time") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_parttime_idx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

