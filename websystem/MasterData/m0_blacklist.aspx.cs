﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_m0_blacklist : System.Web.UI.Page
{
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    //data_testnew _data_testnew = new data_testnew();

    string _local_xml = String.Empty;
    string _localJson = String.Empty;

    //data_qa_cims _data_qa_cims = new data_qa_cims();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//
    int _emp_idx = 0;

    static string _urlSetblacklistM0 = _serviceUrl + ConfigurationManager.AppSettings["urlSetblacklistM0"];
    static string _urlGetblacklistM0 = _serviceUrl + ConfigurationManager.AppSettings["urlGetblacklistM0"];
    static string _urlSetblacklistM0del = _serviceUrl + ConfigurationManager.AppSettings["urlSetblacklistM0del"];
    static string _urlGetblacklistm0list = _serviceUrl + ConfigurationManager.AppSettings["urlGetblacklistm0list"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setFormDatasch(fvsearch, FormViewMode.Insert, null);
            Select_ddlModulesch();
            SelectblacklistDetail();
        }
    }

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    protected void setFormDatasch(FormView fvNamesch, FormViewMode fvModesch, Object obj)
    {
        fvNamesch.ChangeMode(fvModesch);
        fvNamesch.DataSource = obj;
        fvNamesch.DataBind();
    }

    protected void Select_ddlModulesch()
    {
        var ddlModule = (DropDownList)fvsearch.FindControl("ddlModule");
        ddlModule.AppendDataBoundItems = true;
        ddlModule.Items.Clear();
        ddlModule.Items.Add(new ListItem("Select blacklist ....", "0"));

        data_blacklist _dtblacklist_ddl = new data_blacklist();
        Mblacklist blacklistsch_m0 = new Mblacklist();
        _dtblacklist_ddl.blacklist_m0 = new Mblacklist[1];
        _dtblacklist_ddl.blacklist_m0[0] = blacklistsch_m0;

        _dtblacklist_ddl = callServicePostblacklist(_urlGetblacklistm0list, _dtblacklist_ddl);
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtblacklist_ddl));
        ddlModule.DataSource = _dtblacklist_ddl.blacklist_m0;
        ddlModule.DataTextField = "Blacklist_detail";
        ddlModule.DataValueField = "Blacklist_idx";
        ddlModule.DataBind();
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdAdd":
                btnAdd.Visible = false;
                setFormData(FvInsert, FormViewMode.Insert, null);
                FvInsert.Visible = true;
                fvsearch.Visible = false;
                SelectblacklistDetail();
                Select_ddlModulesch();
                break;

            case "CmdCancel":
                btnAdd.Visible = true;
                FvInsert.Visible = false;
                fvsearch.Visible = true;
                break;

            case "CmdSave":
                InsertblacklistDetail();
                fvsearch.Visible = true;
                Select_ddlModulesch();
                break;

            case "CmdDelete":

                int blacklist_del = int.Parse(cmdArg);
                data_blacklist data_m0_blacklist_del = new data_blacklist();
                blacklist_m0_detail m0_blacklist_del = new blacklist_m0_detail();
                data_m0_blacklist_del.blacklist_m0_list = new blacklist_m0_detail[1];

                m0_blacklist_del.Blacklist_idx = blacklist_del;
                data_m0_blacklist_del.blacklist_m0_list[0] = m0_blacklist_del;

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_test_del));
                data_m0_blacklist_del = callServicePostblacklist(_urlSetblacklistM0del, data_m0_blacklist_del);

                Select_ddlModulesch();
                SelectblacklistDetail();
                btnAdd.Visible = true;
                FvInsert.Visible = false;

              break;

            case "Cmdsearch":

                DropDownList ddlModule = (DropDownList)fvsearch.FindControl("ddlModule");
                TextBox txt_blacklist = (TextBox)fvsearch.FindControl("txt_blacklist");

                data_blacklist _dtblacklist_ddl = new data_blacklist();

                Mblacklist blacklistsch_m0 = new Mblacklist();
                _dtblacklist_ddl.blacklist_m0 = new Mblacklist[1];

                blacklistsch_m0.Blacklist_idx = int.Parse(ddlModule.SelectedValue);
                blacklistsch_m0.Blacklist_detail = txt_blacklist.Text;
                blacklistsch_m0.condition = 1;

                _dtblacklist_ddl.blacklist_m0[0] = blacklistsch_m0;

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtblacklist_ddl));
                _dtblacklist_ddl = callServicePostblacklist(_urlGetblacklistm0list, _dtblacklist_ddl);
                setGridData(Gvblacklist, _dtblacklist_ddl.blacklist_m0);

                break;

            case "Cmdclear":

                TextBox txt_blacklist_ = (TextBox)fvsearch.FindControl("txt_blacklist");

                SelectblacklistDetail();
                Select_ddlModulesch();
                txt_blacklist_.Text = string.Empty;


                break;

        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void InsertblacklistDetail()
    {
        TextBox txtBlacklist = (TextBox)FvInsert.FindControl("txtBlacklist");
        DropDownList txtStatus = (DropDownList)FvInsert.FindControl("txtStatus");

        data_blacklist data_m0_blacklist = new data_blacklist();
        blacklist_m0_detail m0_blacklist_insert = new blacklist_m0_detail();
        data_m0_blacklist.blacklist_m0_list = new blacklist_m0_detail[1];

        m0_blacklist_insert.Blacklist_idx = 0;
        m0_blacklist_insert.Blacklist_detail = txtBlacklist.Text;
        m0_blacklist_insert.Blacklist_status = int.Parse(txtStatus.SelectedValue);
        m0_blacklist_insert.Cemp_IDX = _emp_idx;

        data_m0_blacklist.blacklist_m0_list[0] = m0_blacklist_insert;

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0_blacklist));
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_blacklist));
        data_m0_blacklist = callServicePostblacklist(_urlSetblacklistM0, data_m0_blacklist);
        if (data_m0_blacklist.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ข้อมูลซ้ำ", "alert('" + "ข้อมูลซ้ำ : " + "กรุณาแก้ไขใหม่อีกครั้ง" + "')", true);
        }
        else
        {
            SelectblacklistDetail();
            btnAdd.Visible = true;
            FvInsert.Visible = false;
        }

    }

    protected void SelectblacklistDetail()
    {
        data_blacklist data_m0_blacklist_detail = new data_blacklist();
        blacklist_m0_detail m0_blacklist_detail = new blacklist_m0_detail();
        data_m0_blacklist_detail.blacklist_m0_list = new blacklist_m0_detail[1];

        m0_blacklist_detail.condition = 0;

        data_m0_blacklist_detail.blacklist_m0_list[0] = m0_blacklist_detail;

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0_test_detail));
        data_m0_blacklist_detail = callServicePostblacklist(_urlGetblacklistM0, data_m0_blacklist_detail);
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_test_detail));

        setGridData(Gvblacklist, data_m0_blacklist_detail.blacklist_m0_list);
    }

    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected data_blacklist callServicePostblacklist(string _cmdUrl, data_blacklist _data_blacklist)
    {
        _localJson = _funcTool.convertObjectToJson(_data_blacklist);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _data_blacklist = (data_blacklist)_funcTool.convertJsonToObject(typeof(data_blacklist), _localJson);
        return _data_blacklist;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gvblacklist":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_status = (Label)e.Row.Cells[3].FindControl("lbl_status");
                    Label Blacklist_statusOnline = (Label)e.Row.Cells[3].FindControl("Blacklist_statusOnline");
                    Label Blacklist_statusOffline = (Label)e.Row.Cells[3].FindControl("Blacklist_statusOffline");

                    ViewState["vs_status"] = lbl_status.Text;


                    if (ViewState["vs_status"].ToString() == "1")
                    {
                        Blacklist_statusOnline.Visible = true;
                    }
                    else if (ViewState["vs_status"].ToString() == "0")
                    {
                        Blacklist_statusOffline.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    btnAdd.Visible = true;
                    FvInsert.Visible = false;

                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gvblacklist":
                Gvblacklist.EditIndex = e.NewEditIndex;
                SelectblacklistDetail();
                break;

        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gvblacklist":
                Gvblacklist.EditIndex = -1;
                SelectblacklistDetail();
                btnAdd.Visible = true;
                FvInsert.Visible = false;
                break;
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gvblacklist":
                Gvblacklist.PageIndex = e.NewPageIndex;
                Gvblacklist.DataBind();
                SelectblacklistDetail();
                break;

        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gvblacklist":

                var txt_Blacklist_idx_edit = (TextBox)Gvblacklist.Rows[e.RowIndex].FindControl("txt_Blacklist_idx_edit");
                var txt_blacklist_name_edit = (TextBox)Gvblacklist.Rows[e.RowIndex].FindControl("txt_blacklist_name_edit");
                var ddlblacklist_status_edit = (DropDownList)Gvblacklist.Rows[e.RowIndex].FindControl("ddlblacklist_status_edit");

                Gvblacklist.EditIndex = -1;

                data_blacklist data_m0_blacklist_edit = new data_blacklist();
                blacklist_m0_detail m0_blacklist_edit = new blacklist_m0_detail();
                data_m0_blacklist_edit.blacklist_m0_list = new blacklist_m0_detail[1];

                m0_blacklist_edit.Blacklist_idx = int.Parse(txt_Blacklist_idx_edit.Text);
                m0_blacklist_edit.Blacklist_detail = txt_blacklist_name_edit.Text;
                m0_blacklist_edit.Blacklist_status = int.Parse(ddlblacklist_status_edit.SelectedValue);
                m0_blacklist_edit.Cemp_IDX = _emp_idx;

                data_m0_blacklist_edit.blacklist_m0_list[0] = m0_blacklist_edit;
                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_blacklist_edit));
                data_m0_blacklist_edit = callServicePostblacklist(_urlSetblacklistM0, data_m0_blacklist_edit);

                if (data_m0_blacklist_edit.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                    Select_ddlModulesch();
                    SelectblacklistDetail();
                }
                else
                {
                    Select_ddlModulesch();
                    SelectblacklistDetail();
                }
                break;

        }
    }

}