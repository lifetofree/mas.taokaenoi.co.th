﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="drc_m0_typework.aspx.cs" Inherits="websystem_MasterData_drc_m0_typework" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="litdebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewResolution" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btn_Insert" CssClass="btn btn-primary" data-original-title="เพิ่มข้อมูลสถานที่" data-toggle="tooltip" title="เพิ่มข้อมูล" runat="server"
                    CommandName="cmdInsert" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูลสถานที่</asp:LinkButton>
            </div>

            <asp:GridView ID="GvMaster" runat="server" Visible="true"
                AutoGenerateColumns="false"
                DataKeyNames="TWIDX"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>ไม่มีข้อมูล</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="lbl_TWIDX" runat="server" Visible="false" Text='<%# Eval("TWIDX") %>' />

                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_TWIDX_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("TWIDX") %>'></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_Type_Work_edit" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อสถานที่" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_Type_Work_edit" runat="server" CssClass="form-control " Text='<%# Eval("Type_Work") %>'></asp:TextBox>
                                            
                                            <asp:RequiredFieldValidator ID="Re_txt_Type_Work_edit" runat="server"
                                                ControlToValidate="txt_Type_Work_edit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อสถานที่" ValidationGroup="SaveEdit" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txt_Type_Work_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_Type_Work_edit" Width="220" />

                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_Work_Status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">

                                            <asp:DropDownList ID="ddl_Work_Status_edit" AutoPostBack="false" runat="server"
                                                CssClass="form-control" SelectedValue='<%# Eval("Work_Status") %>'>
                                                <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="SaveEdit" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อสถานที่" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_Type_Work" runat="server" Text='<%# Eval("Type_Work") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_Work_Status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("Work_Status") %>'></asp:Label>

                                    <asp:Label ID="lbl_Work_StatusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="lbl_Work_StatusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnDelete" CssClass="text-trash" runat="server" CommandName="cmdDelete"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("TWIDX") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>


        </asp:View>

        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btnCancelInsert" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                    CommandName="cmdCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">เพิ่มข้อมูลสถานที่</h3>

                </div>

                <div class="panel-body">
                    <div class="panel-heading">

                        <div class="form-horizontal" role="form">

                            <div class="panel-heading">

                                <div class="form-group">
                                    <asp:Label ID="lbl_resolution_name" runat="server" Text="ชื่อสถานที่ : " CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtType_Work" runat="server" CssClass="form-control" placeholder="ชื่อสถานที่ ..." Enabled="true" />

                                        <asp:RequiredFieldValidator ID="Re_txtType_Work" runat="server"
                                            ControlToValidate="txtType_Work" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกชื่อสถานที่" ValidationGroup="Save" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txtType_Work" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtType_Work" Width="220" />
                                     
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="lbl_Work_Status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ : " />
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_Work_Status" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <asp:LinkButton ID="btn_SaveType_Work" ValidationGroup="Save" CssClass="btn btn-success" Text="บันทึก" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="cmdSave" OnCommand="btnCommand"></asp:LinkButton>
                                        <asp:LinkButton ID="btn_CancelType_Work" CssClass="btn btn-danger" runat="server" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <!-- End Insert Form -->

    </asp:MultiView>


</asp:Content>

