<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="pms_management.aspx.cs" Inherits="websystem_hr_pms_management" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbList" runat="server" CommandName="navList" OnCommand="navCommand">
                                การจัดการผู้ประเมิน/ผู้อนุมัติ</asp:LinkButton>
                        </li>
                        <!-- <li id="li1" runat="server">
                            <asp:LinkButton ID="lbImport" runat="server" CommandName="navImport" OnCommand="navCommand">
                                นำเข้าข้อมูล</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbReport" runat="server" CommandName="navReport" OnCommand="navCommand">
                                ส่วนของ HR</asp:LinkButton>
                        </li> -->
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->
            </div>
        </nav>
    </div>
    <!--tab menu-->

    <asp:MultiView ID="mvSystem" runat="server">
        <asp:View ID="viewList" runat="server">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">ค้นหาข้อมูลพนักงาน</h3>
                    </div>
                    <div class="panel-body">
                        <asp:FormView ID="fvSearch" runat="server" Width="100%" DefaultMode="Insert">
                            <InsertItemTemplate>
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">รหัสพนักงาน</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control"
                                                    MaxLength="100" placeholder="รหัสพนักงาน"></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">ชื่อ-นามสกุล</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control"
                                                    MaxLength="250" placeholder="ชื่อ-นามสกุล"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">องค๋กร</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlOrganization" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">กลุ่มงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlWorkGroup" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">สายงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlLineWork" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ฝ่าย</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlDepartment" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">แผนก</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlSection" runat="server" Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ตำแหน่ง</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlPosition" runat="server"
                                                    Cssclass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbSearchData" runat="server"
                                                    CssClass="btn btn-md btn-primary" OnCommand="btnCommand"
                                                    CommandName="cmdSearch"><i class="fas fa-search"></i>&nbsp;ค้นหา
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbResetData" runat="server"
                                                    CssClass="btn btn-md btn-info" OnCommand="btnCommand"
                                                    CommandName="cmdReset"><i class="fas fa-redo"></i>&nbsp;เคลียร์ค่า
                                                </asp:LinkButton>
                                            </div>
                                            <label class="col-md-2"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                    </div>
                                </div>
                                <label class="col-md-1"></label>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
                <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="10"
                    OnPageIndexChanging="gvPageIndexChanging"
                    CssClass="table table-striped table-bordered table-responsive">
                    <HeaderStyle CssClass="info" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                        LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รหัสพนักงาน">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("emp_code") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpNameTh" runat="server" Text='<%# Eval("emp_name_th") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Solid">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpNameSolid" runat="server" Text='<%# Eval("emp_name_solid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dotted">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpNameotted" runat="server" Text='<%# Eval("emp_name_dotted") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="การจัดการ">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbEditData" runat="server" CssClass="btn btn-md btn-warning"
                                    OnCommand="btnCommand" CommandName="cmdEditData"
                                    CommandArgument='<%# Eval("emp_idx") %>'><i class="far fa-edit"></i>&nbsp;แก้ไข
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>
        <asp:View ID="viewDetail" runat="server">
            <div class="col-md-12">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title">แก้ไขข้อมูล</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="form-group">
                                <asp:Label ID="lblEmpDetail" runat="server"></asp:Label>
                                <asp:Label ID="lblU0Idx" runat="server" Visible="False"></asp:Label>
                                <asp:Label ID="lblEmpIdx" runat="server" Visible="False"></asp:Label>
                                <asp:Label ID="lblEmpNameTh" runat="server" Visible="False"></asp:Label>
                            </div>
                            <asp:GridView ID="gvDetail" runat="server" AutoGenerateColumns="false" ShowHeader="false"
                                OnRowDataBound="gvRowDataBound" CssClass="table table-responsive" GridLines="None">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hfM0Idx" runat="server" Value='<%# Eval("m0_idx") %>'>
                                            </asp:HiddenField>
                                            <asp:HiddenField ID="hfEmpIdx" runat="server"
                                                Value='<%# Eval("emp_idx") %>'>
                                            </asp:HiddenField>
                                            <asp:HiddenField ID="hfEmpNameTh" runat="server"
                                                Value='<%# Eval("emp_name_th") %>'>
                                            </asp:HiddenField>
                                            <asp:HiddenField ID="hfEvalEmpType" runat="server"
                                                Value='<%# Eval("eval_emp_type") %>'></asp:HiddenField>
                                            <asp:HiddenField ID="hfEvalEmpIdx" runat="server"
                                                Value='<%# Eval("eval_emp_idx") %>'></asp:HiddenField>
                                            <asp:HiddenField ID="hfEvalEmpNameTh" runat="server"
                                                Value='<%# Eval("eval_emp_name_th") %>'>
                                            </asp:HiddenField>
                                            <asp:HiddenField ID="hfApproveStatus" runat="server"
                                                Value='<%# Eval("approve_status") %>'>
                                            </asp:HiddenField>

                                            <asp:Label ID="lblEvalEmpTypeName" runat="server"
                                                Text='<%# Eval("eval_emp_type_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <label>Dotted Line</label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlEmpApproveM0" runat="server"
                                                CssClass="js-example-basic-single form-control">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="ddlEmpDotted" runat="server" CssClass="js-example-basic-single form-control">
                                            </asp:DropDownList>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <asp:TextBox ID="tbCalRatio" runat="server" CssClass="form-control"
                                                    Text='<%# Eval("cal_ratio") %>'></asp:TextBox>
                                                <asp:CheckBox ID="cbDelete" runat="server" Visible="false"
                                                    Text="ลบ Dotted Line"></asp:CheckBox>
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <div class="form-group">
                                                <asp:TextBox ID="tbCalRatioDotted" runat="server"
                                                    CssClass="form-control" placeholder="Ratio"></asp:TextBox>
                                            </div>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <div class="form-group">
                                <label class="col-md-2"></label>
                                <div class="col-md-4">
                                    <asp:LinkButton ID="lbSaveData" runat="server" CssClass="btn btn-md btn-success"
                                        OnCommand="btnCommand" CommandName="cmdSaveData"><i
                                            class="far fa-save"></i>&nbsp;บันทึก
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lbCancelData" runat="server" CssClass="btn btn-md btn-danger"
                                        OnCommand="btnCommand" CommandName="cmdCancelData"><i
                                            class="fas fa-times"></i>&nbsp;ยกเลิก
                                    </asp:LinkButton>
                                </div>
                                <label class="col-md-2"></label>
                                <label class="col-md-4"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
    <script type="text/javascript">
        $(function () {
            $(".js-example-basic-single").select2({
                width: '100%'
            })
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $(".js-example-basic-single").select2({
                    width: '100%'
                })
            });
        });
    </script>
</asp:Content>