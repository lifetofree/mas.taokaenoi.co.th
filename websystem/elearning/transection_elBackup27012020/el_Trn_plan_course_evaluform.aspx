﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="el_Trn_plan_course_evaluform.aspx.cs" Inherits="websystem_el_Trn_plan_course_evaluform" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


    <link href="../../../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/emps-contents/bootstrap-datetimepicker.min.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/custom.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/emps-contents/fa/css/font-awesome.min.css" runat="server" rel="stylesheet" />
    <link href="./../Content/emps-contents/empshift-style-general.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/emps-contents/bootstrap-clockpicker.min.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/emps-contents/colorpicker/css/bootstrap-colorpicker.min.css" runat="server" rel="stylesheet" />

    <link href="../../../Content/jquery-ui-1.12.1/jquery-ui.min.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/fullcalendar.css" rel="stylesheet" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous" />

</head>
<body>
    <form id="form1" runat="server">
        <asp:Literal ID="Litdebug" runat="server" Text=""></asp:Literal>
        <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
        <%--  --%>

        <%--  --%>
        <div style="height: auto; width: 100%; margin: 0 0; display: block; border: 3px solid #D9B4B4; font-family: 'MS Reference Sans Serif';">

            <asp:Panel ID="Panel1" runat="server">
                <%--<div class="row">
                    <asp:Image ID="ImageButton1" CssClass="img-responsive"
                        Width="100%" Visible="true"
                        ImageUrl="~/images/elearning/evaluationform/banner_evaluationform_1.png"
                        runat="server" />
                </div>--%>
                <header >
                    <div runat="server" id="dv_header" >
                        <%-- start header --%>
                        <%--<div class="row">--%>
                            <%--<asp:Panel ID="Panel_course" runat="server" BorderStyle="Groove"  Height="200px">--%>

                               <%-- <asp:Panel ID="Panel2" runat="server" Height="200px"></asp:Panel>--%>
                                <%--  style="margin-top: 17%;" --%>


                               
                            <%--</asp:Panel>--%>
                        <%--</div>--%>
                        
                    </div>
                </header>
                

                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <asp:Label ID="Label2" runat="server"></asp:Label>
                        <asp:HyperLink runat="server" ID="txtfocus" />

                         <div class="row" runat="server" visible="true" >
                                    
                                    <div class="col-md-12" >
                                        <div class="form-group">

                                            <asp:Label ID="lbcourse" Font-Bold="true"
                                                CssClass="col-md-offset-2 col-md-2 control-labelnotop text_right" runat="server"
                                                Font-Size="Large"
                                                Text="คอร์สอบรม :" />

                                            <div class="col-md-8">
                                                <asp:Label ID="litcourse"
                                                    Font-Bold="true"
                                                    Font-Size="Large"
                                                    runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblecturer_type" Font-Bold="true" CssClass="col-md-offset-2 col-md-2 control-labelnotop text_right h4" runat="server" Text="วิทยากร :" />
                                            <div class="col-md-8">
                                                <asp:GridView ID="Gvinstitution"
                                                    CssClass="col-md-12 word-wrap"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    HeaderStyle-CssClass="default"
                                                    ShowFooter="false"
                                                    AllowPaging="false"
                                                    ShowHeader="false"
                                                    GridLines="None">

                                                    <Columns>

                                                        <asp:TemplateField
                                                            HeaderText="ชื่อ - สกุล"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtinstitution_name_edit_L" Font-Bold="true" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>

                                                                <asp:Label ID="txtinstitution_name_item_L"
                                                                    Font-Bold="true" runat="server"
                                                                    CssClass="h4"
                                                                    Text='<%# Eval("zName") %>'
                                                                    Width="100%"></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    

                                </div>


                        <%--  --%>

                        <asp:Panel ID="Panel1xxx" runat="server" Visible="true">
                            <label class="col-sm-2"></label>
                            <div class="col-md-10">
                                <div class="alert-message alert-message-success" runat="server" visible="false">
                                    <blockquote class="danger" style="font-size: small; background-color: lightseagreen; color: aliceblue">
                                        <h4><b>
                                            <asp:Label ID="lbltopic" runat="server"></asp:Label></b></h4>
                                    </blockquote>
                                </div>
                                <asp:Panel ID="pnl_alert" runat="server" Visible="false">
                                    <div class="alert alert-danger col-md-12 h4" style="text-align: center" id="dv_alert" runat="server">
                                        ไม่พบข้อมูล
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnl_complete" runat="server" Visible="false">
                                    <div class="alert alert-success col-md-12 h4" style="text-align: center" id="Div1" runat="server">
                                        ประเมินผลความพึงพอใจหลังการฝึกอบรมเสร็จแล้ว
                                    </div>
                                </asp:Panel>
                                <div runat="server" id="pnldata">

                                    <asp:GridView ID="GvListData"
                                        CssClass="table col-md-10 word-wrap"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        HeaderStyle-CssClass="default"
                                        ShowFooter="false"
                                        AllowPaging="false"
                                        ShowHeader="false"
                                        GridLines="None">

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText=""
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbu0_training_course_idx" Visible="false" runat="server" Text='<%# Eval("u0_training_course_idx") %>'></asp:Label>
                                                    <asp:Label ID="lbm0_evaluation_f_idx_ref" Visible="false" runat="server" Text='<%# Eval("m0_evaluation_f_idx_ref") %>'></asp:Label>
                                                    <asp:Label ID="lbm0_evaluation_group_idx_ref" Visible="false" runat="server" Text='<%# Eval("m0_evaluation_group_idx_ref") %>'></asp:Label>
                                                    <asp:Label ID="lbcourse_item" Visible="false" runat="server" Text='<%# Eval("course_item") %>'></asp:Label>
                                                    <asp:Label ID="lbcourse_evaluation_f_name_th" Visible="false" runat="server" Text='<%# Eval("zName") %>'></asp:Label>

                                                    <div class="word-wrap">

                                                        <asp:Label ID="lbid" runat="server" Visible="false" Text='<%# Eval("id") %>'></asp:Label>

                                                        <div class="row">
                                                            <div class="col-md-12">

                                                                <asp:Label ID="Label1" Visible="true" runat="server" CssClass="pull-left" Font-Bold="true" Text='<%# Eval("evaluation_group_name_th") %>'></asp:Label>

                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-11">

                                                                <asp:Label ID="lbzName" runat="server" Text='<%# Eval("course_item")+". "+Eval("zName") %>'></asp:Label>

                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-11">
                                                                <asp:RadioButtonList ID="rdoItem" runat="server" CssClass="control-labelnotop checkbox text_left">
                                                                    <asp:ListItem Value="5" Text="ดีมาก"></asp:ListItem>
                                                                    <asp:ListItem Value="4" Text="ดี"></asp:ListItem>
                                                                    <asp:ListItem Value="3" Text="ปานกลาง"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="พอใช้"></asp:ListItem>
                                                                    <asp:ListItem Value="1" Text="ควรปรับปรุง"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>
                                                        </div>

                                                    </div>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                    <br />
                                    <div class="col-md-10">
                                    </div>
                                    <%-- <div class="col-md-1"></div>--%>
                                    <div class="col-md-10">
                                        <asp:LinkButton CssClass="btn btn-success" runat="server"
                                            Text="<i class='fa fa-save fa-lg'></i> บันทึก"
                                            data-toggle="tooltip" title="บันทึก"
                                            ID="btnSaveInsert"
                                            OnCommand="btnCommand" CommandName="btnSaveInsert" />
                                    </div>
                                </div>
                                <%--</div>
                            </div>--%>
                            </div>
                        </asp:Panel>

                        <%--  --%>
                    </div>
                </div>
            </asp:Panel>

        </div>

    </form>
</body>
</html>
