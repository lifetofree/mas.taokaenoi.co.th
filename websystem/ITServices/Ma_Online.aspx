﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="Ma_Online.aspx.cs" Inherits="websystem_ITServices_Ma_Online" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>


    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" runat="server">
                    <li id="_divMenuLiToDivIndex" runat="server" class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">รายการรออนุมัติ <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li id="li2">
                                <asp:LinkButton ID="lbindex" runat="server"
                                    CommandName="btnIndex_User"
                                    OnCommand="btnCommand" Text="ผู้ใช้งานทั่วไป" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <asp:LinkButton ID="lbladmin" runat="server"
                                    CommandName="btnInsert_support"
                                    OnCommand="btnCommand" Text="เจ้าหน้าที่" />
                            </li>
                        </ul>
                    </li>
                    <li id="liToInsertList" runat="server" class="dropdown">


                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">สร้างรายการ <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <asp:LinkButton ID="lbinsertma" runat="server"
                                    CommandName="btnInsertma"
                                    OnCommand="btnCommand" Text="สร้าง Master Data" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <asp:LinkButton ID="lbinsert" runat="server"
                                    CommandName="btnInsert"
                                    OnCommand="btnCommand" Text="สร้างรายการ MA" />
                            </li>
                        </ul>




                    </li>

                    <li id="lireport" runat="server">
                        <asp:LinkButton ID="lbreport" runat="server"
                            CommandName="btnReport"
                            OnCommand="btnCommand" Text="รายงาน" />
                    </li>


                </ul>
                <div class="collapse navbar-collapse" id="Menu1">
                    <asp:LinkButton ID="lblguide" CssClass="btn_menulist" runat="server" CommandName="btnguide" OnCommand="btnCommand">คู่มือการใช้งาน</asp:LinkButton>
                    <asp:LinkButton ID="lblflow" CssClass="btn_menulist" runat="server" CommandName="btnflow" OnCommand="btnCommand">Flow Process</asp:LinkButton>
                </div>
            </div>
        </nav>
    </div>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewApprove" runat="server">

            <div class="col-lg-12" runat="server" id="BoxSearch" visible="false">
                <div id="BoxButtonSearchShow_Search" runat="server">
                    <asp:LinkButton ID="LinkButton6" CssClass="btn btn-success" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchShow_Search" OnCommand="btnCommand"><i class="fa fa-search"></i> ShowSearch</asp:LinkButton>
                </div>

                <div id="BoxButtonSearchHide_Search" runat="server">
                    <asp:LinkButton ID="LinkButton7" CssClass="btn btn-warning" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchHide_Search" OnCommand="btnCommand"><i class="fa fa-search"></i> HideSearch</asp:LinkButton>
                </div>
                <br />
                <div id="SETBoxAllSearch" runat="server">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">


                                <div class="form-group">
                                    <asp:Label ID="AddStart" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ค้นหา" />
                                    <div class="col-sm-2">
                                        <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            <asp:ListItem Text="เลือกเงื่อนไข ...." Value="0"></asp:ListItem>
                                            <asp:ListItem Text="มากกว่า >" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="น้อยกว่า <" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="ระหว่าง <>" Value="3"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3 ">

                                        <div class='input-group date'>
                                            <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="btnsearch" runat="server" Display="None"
                                                ControlToValidate="AddStartdate" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <asp:TextBox ID="AddEndDate" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label67" CssClass="col-sm-2 control-label" runat="server" Text="Organization" />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlSearchOrg" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>

                                    <asp:Label ID="Label66" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากฝ่าย" />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlSearchDep" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกฝ่าย....</asp:ListItem>

                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server" Text="ขั้นตอนดำเนินการ" />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlSearchStatus" AutoPostBack="true" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="0" Text="กรุณาเลือกขั้นตอนดำเนินการ ..." />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5 col-sm-offset-2">
                                        <asp:LinkButton ID="btnsearch" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                        <asp:LinkButton ID="btnRefresh" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="BtnBack" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div id="gridviewindex" runat="server">
                <div class="panel-body">
                    <div class="form-horizontal" role="form">

                        <asp:GridView ID="GvSelectMA_User"
                            runat="server"
                            AutoGenerateColumns="false"
                            DataKeyNames="u0idx"
                            CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                            HeaderStyle-CssClass="info"
                            AllowPaging="true"
                            PageSize="10"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            OnRowDataBound="Master_RowDataBound">
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">No result</div>
                            </EmptyDataTemplate>
                            <Columns>


                                <asp:TemplateField HeaderText="เลขรหัสเอกสาร" ItemStyle-HorizontalAlign="center"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lblDocCode" runat="server" Text='<%# Eval("DocCode") %>' />
                                        </small>
                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="เลขทะเบียนอุปกรณ์" ItemStyle-HorizontalAlign="center"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lblu0idx" Visible="false" runat="server" Text='<%# Eval("u0idx") %>' />
                                            <asp:Label ID="lblcode" runat="server" Text='<%# Eval("u0_code") %>' />
                                            <asp:Label ID="lblunidx" Visible="false" runat="server" Text='<%# Eval("unidx") %>' />
                                            <asp:Label ID="lblacidx" Visible="false" runat="server" Text='<%# Eval("acidx") %>' />
                                            <asp:Label ID="Label31" Visible="false" runat="server" Text='<%# Eval("deviceidx") %>' />
                                        </small>
                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="center"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lblcreatedate" runat="server" Text='<%# Eval("CreateDate") %>' />
                                        </small>
                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อผู้สร้าง" ItemStyle-HorizontalAlign="center"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lbladmin" runat="server" Text='<%# Eval("AdminName") %>' />
                                        </small>
                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="ข้อมูลผู้ถือครอง" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <strong>
                                                <asp:Label ID="Label12" runat="server">บริษัท: </asp:Label></strong>
                                            <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                            </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">ฝ่าย: </asp:Label></strong>
                                            <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                            </p>
                                            <strong>
                                                <asp:Label ID="Label13" runat="server">แผนก: </asp:Label></strong>
                                            <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                            </p>
                                                 <strong>
                                                     <asp:Label ID="Label22" runat="server">ชื่อผู้ถือครอง: </asp:Label></strong>
                                            <asp:Label ID="lblempname" runat="server" Text='<%# Eval("EmpName") %>' />
                                            </p>
                                        </small>
                                        <%--<small>
                                            <asp:Label ID="lblempname" runat="server" Text='<%# Eval("EmpName") %>' />
                                        </small>--%>
                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-HorizontalAlign="center"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lblStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                        </small>
                                    </ItemTemplate>
                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbVeiw1" runat="server" CssClass="btn btn-warning btn-sm" Text="" OnCommand="btnCommand" CommandName="ViewMaList" title="View" CommandArgument='<%# Eval("u0idx") + ";" + Eval("deviceidx")+ ";" + 1 %>'><i class="glyphicon glyphicon-share-alt" ></i> </asp:LinkButton>
                                        <asp:LinkButton ID="btnedit" Visible="false" runat="server" CssClass="btn btn-danger btn-sm" Text="" OnCommand="btnCommand" CommandName="ViewMaList" title="View" CommandArgument='<%# Eval("u0idx")+ ";" + Eval("deviceidx") + ";" + 2%>'><i class="glyphicon glyphicon-edit" ></i> </asp:LinkButton>


                                    </ItemTemplate>

                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                            </Columns>

                        </asp:GridView>

                    </div>
                </div>
            </div>



            <div id="div_showdata" runat="server" visible="false">

                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ถือครอง</strong></h3>
                        </div>
                        <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>


                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>


                                        <%-------------- แผนก,ตำแหน่ง --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>

                                        <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                            <div class="col-sm-3">
                                                <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                                <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                            <div class="col-sm-3">
                                                <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                                <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>



                    </div>


                </div>

                <div class="col-lg-12" runat="server">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-tasks"></i><strong>&nbsp; ข้อมูลเครื่อง</strong></h3>
                        </div>
                        <div class="form-horizontal" role="form">

                            <div class="panel-body">
                                <asp:FormView ID="FvDetailDevices" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <ItemTemplate>
                                        <div class="form-horizontal" role="form">

                                            <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("unidx") %>' />
                                            <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("acidx") %>' />


                                            <div class="form-group">

                                                <asp:Label ID="Label29" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="เลขรหัสเอกสาร : " />
                                                <div class="col-sm-3">
                                                    <asp:Label ID="Label30" CssClass="control-labelnotop" runat="server" Text='<%# Eval("DocCode") %>'></asp:Label>
                                                </div>
                                                <asp:Label ID="Label34" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="ผู้ถือครอง : " />
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lblEmpname" CssClass="control-labelnotop" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                    <asp:Label ID="lblAEmpIDX" Visible="false" CssClass="control-labelnotop" runat="server" Text='<%# Eval("AEmpIDX") %>'></asp:Label>

                                                </div>


                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label7" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="ประเภทอุปกรณ์ : " />
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lbltypedevice" CssClass="control-labelnotop" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                    <asp:Label ID="lblu0didx" Visible="false" CssClass="control-labelnotop" runat="server" Text='<%# Eval("u0_didx") %>'></asp:Label>
                                                    <asp:Label ID="lblu2idx" Visible="false" CssClass="control-labelnotop" runat="server" Text='<%# Eval("u2idx") %>'></asp:Label>

                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="เลขทะเบียนอุปกรณ์ : " />
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lblcode" CssClass="control-labelnotop" runat="server" Text='<%# Eval("u0_code") %>'></asp:Label>
                                                </div>
                                            </div>


                                            <div class="form-group">

                                                <asp:Label ID="Label4" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="เลข Asset : " />
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lblacc" CssClass="control-labelnotop" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                                </div>



                                            </div>
                                            <div class="form-group">

                                                <asp:Label ID="Label15" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="ความคิดเห็นเจ้าหน้าที่ : " />
                                                <div class="col-sm-10">
                                                    <asp:Label ID="Label16" CssClass="control-labelnotop" runat="server" Text='<%# Eval("comment_it") %>'></asp:Label>
                                                </div>
                                            </div>

                                            <div class="form-group" id="panel_user_detailcomment" runat="server" visible="false">
                                                <asp:Label ID="Label20" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="สถานะรายการ : " />
                                                <div class="col-sm-3">
                                                    <asp:Label ID="Label21" CssClass="control-labelnotop" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>
                                                </div>
                                                <asp:Label ID="Label17" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="ความคิดเห็นผู้ถือครอง : " />
                                                <div class="col-sm-4">
                                                    <asp:Label ID="Label18" CssClass="control-labelnotop" runat="server" Text='<%# Eval("comment_user") %>'></asp:Label>
                                                </div>
                                            </div>

                                        </div>


                                    </ItemTemplate>
                                </asp:FormView>

                                <div class="col-lg-6" id="div_editsupport" visible="true" runat="server">
                                    <asp:GridView ID="GvMA_Edit"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        DataKeyNames="m0idx"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                        HeaderStyle-CssClass="info"
                                        HeaderStyle-Height="40px"
                                        OnRowDataBound="Master_RowDataBound">

                                        <%--AllowPaging="true"
                                        PageSize="10"
                                        OnPageIndexChanging="Master_PageIndexChanging"--%>

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center;">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>


                                        <Columns>
                                            <asp:TemplateField HeaderText="เลือกรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:CheckBox ID="chkma_edit" Enabled="false" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="หัวข้อ MA." HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Literal ID="lblm0idx_edit" Visible="false" runat="server" Text='<%# Eval("m0idx") %>' />
                                                        <asp:Literal ID="lblma_edit" runat="server" Text='<%# Eval("ma_name") %>' /></small>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>

                                    </asp:GridView>

                                    <div class="form-group" runat="server" id="div_btnedit" visible="false">
                                        <div class="col-sm-3 col-sm-offset-9">
                                            <asp:LinkButton ID="LinkButton8" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdSave_edit" OnCommand="btnCommand" ValidationGroup="SaveUpdate" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton9" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-lg-6" runat="server" visible="false">
                                    <asp:GridView ID="GvDetailMA"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered col-lg-4"
                                        HeaderStyle-CssClass="table_headCenter"
                                        HeaderStyle-Height="40px"
                                        DataKeyNames="m0idx"
                                        ShowFooter="False"
                                        ShowHeaderWhenEmpty="True"
                                        AllowPaging="True"
                                        PageSize="100"
                                        BorderStyle="None"
                                        CellSpacing="2"
                                        OnPageIndexChanging="Master_PageIndexChanging"
                                        OnRowDataBound="Master_RowDataBound">


                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>

                                        <Columns>

                                            <asp:TemplateField HeaderText="#">
                                                <ItemTemplate>
                                                    <div class="panel-heading">
                                                        <%# (Container.DataItemIndex +1) %>
                                                    </div>
                                                </ItemTemplate>


                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รายการMA">
                                                <ItemTemplate>
                                                    <div class="panel-heading">
                                                        <asp:Label ID="lblm0idx" runat="server" Visible="true" Text='<%# Eval("m0idx")%>'></asp:Label>
                                                        <asp:Label ID="Label14" runat="server" Text='<%# Eval("ma_name")%>'></asp:Label>

                                                    </div>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Manage">
                                                <ItemTemplate>
                                                    <div class="panel-heading">

                                                        <asp:LinkButton ID="btndelete_edit" CssClass="btn btn-danger btn-sm" runat="server" data-toggle="tooltip"
                                                            title="ลบ" CommandName="btnDelete_edit" OnCommand="btnCommand"
                                                            CommandArgument='<%# Eval("m0idx") %>'
                                                            OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                                        </asp:LinkButton>


                                                    </div>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                        </Columns>

                                    </asp:GridView>

                                </div>




                                <div class="col-lg-6" runat="server" id="div_approve" visible="false">
                                    <asp:Panel ID="panel_user_comment" runat="server" Visible="false">

                                        <div class="form-group">
                                            <asp:Label ID="Label7" CssClass="col-sm-4 control-label text_right" runat="server" Text="สถานะรายการ : " />
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddl_approve" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกสถานะรายการ"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="อนุมัติ"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="ไม่อนุมัติ"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="SaveUpdate" runat="server" Display="None"
                                                    ControlToValidate="ddl_approve" Font-Size="11"
                                                    ErrorMessage="เลือกสถานะอนุมัติ"
                                                    ValidationExpression="เลือกสถานะอนุมัติ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label19" CssClass="col-sm-4 control-label text_right" runat="server" Text="ความคิดเห็นผู้ถือครอง : " />
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtusercomment" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="SaveUpdate" runat="server" Display="None" ControlToValidate="txtusercomment" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                    ValidationGroup="SaveUpdate" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtusercomment"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-7">
                                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdUpdate" OnCommand="btnCommand" ValidationGroup="SaveUpdate" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>

                                    </asp:Panel>

                                    <asp:Panel ID="panel_admin" runat="server" Visible="false">

                                        <div class="form-group">
                                            <asp:Label ID="Label22" CssClass="col-sm-4 control-label text_right" runat="server" Text="สถานะรายการ : " />
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddladmin_approve" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="ดำเนินการสำเร็จ"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label23" CssClass="col-sm-4 control-label text_right" runat="server" Text="ความคิดเห็นเจ้าหน้าที่ : " />
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtadmincomment" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="SaveUpdate" runat="server" Display="None" ControlToValidate="txtadmincomment" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                    ValidationGroup="SaveUpdate" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtadmincomment"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-7">
                                                <asp:LinkButton ID="LinkButton3" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdUpdate_admin" OnCommand="btnCommand" ValidationGroup="SaveUpdate" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton5" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </asp:Panel>


                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12" runat="server">
                    <div class="form-horizontal" role="form">

                        <div class="form-group">
                            <div class="col-sm-1 col-sm-offset-11">

                                <asp:LinkButton ID="LinkButton4" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-eye-open"></i><strong>&nbsp; ประวัติดำเนินการ</strong></h3>
                        </div>
                        <div class="panel-body">

                            <asp:Repeater ID="rptLog" runat="server">
                                <HeaderTemplate>
                                    <div class="row">
                                        <label class="col-sm-2 control-label"><small>วัน / เวลา</small></label>
                                        <label class="col-sm-4 control-label"><small>ผู้ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ผลการดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ความคิดเห็น</small></label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <span><small><%#Eval("CreateDate")%></small></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <span><small><%# Eval("EmpName") %>&nbsp;(<%# Eval("actor_name") %>)</small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("node_name") %></small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("status_name") %></small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("comment") %></small></span>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>

            </div>

        </asp:View>

        <asp:View ID="ViewInsertMaster" runat="server">

            <div class="row col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div id="BoxButtonSearchShow" runat="server">
                                <asp:LinkButton ID="BtnHideSETBoxAllSearchShow" CssClass="btn btn-primary" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchShow" OnCommand="btnCommand"><i class="glyphicon glyphicon-plus-sign"></i> สร้างหัวข้อ MA</asp:LinkButton>
                            </div>

                            <div id="BoxButtonSearchHide" runat="server">
                                <asp:LinkButton ID="BtnHideSETBoxAllSearchHide" CssClass="btn btn-warning" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchHide" OnCommand="btnCommand"><i class="glyphicon glyphicon-minus-sign"></i> ยกเลิกสร้างหัวข้อ MA</asp:LinkButton>
                            </div>

                        </div>
                    </div>


                    <%------------------------ Div ADD ผูกผู้ถือครอง Support and Holder ------------------------%>

                    <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                        <div class="row col-md-12">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>ประเภทอุปกรณ์</label>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>

                                                <asp:DropDownList ID="ddltype" runat="server" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="save_master" runat="server" Display="None"
                                                    ControlToValidate="ddltype" Font-Size="11"
                                                    ErrorMessage="กรุณาประเภทอุปกรณ์"
                                                    ValidationExpression="กรุณาประเภทอุปกรณ์" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>หัวข้อ MA</label>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtaddma" runat="server" CssClass="form-control" placeHolder="........................."></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldVsssssalidator8" ValidationGroup="save_master" runat="server" Display="None" ControlToValidate="txtaddma" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกหัวข้อ MA"
                                                    ValidationExpression="กรุณากรอกหัวข้อ MA"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVsssssalidator8" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressisssondValidator1" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtaddma"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressisssondValidator1" Width="160" />

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>สถานะ</label>
                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <div class="form-group">
                                        <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsertmaster" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="save_master" />
                                        <asp:Button CssClass="btn btn-danger" runat="server" CommandName="btncancelmaster" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" Text="ยกเลิก" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>



            <div class="col-md-12">

                <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="m0idx"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="m0idx" runat="server" CssClass="form-control"
                                    Visible="False" Text='<%# Eval("m0idx")%>' />

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">ประเภทอุปกรณ์</label>
                                            <asp:Label ID="lbltypeidx" Visible="false" runat="server" Text='<%# Bind("typeidx") %>' Enabled="false" />
                                            <asp:DropDownList ID="ddltypeUpdate" AutoPostBack="true" runat="server"
                                                CssClass="form-control">
                                            </asp:DropDownList>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">หัวข้อ MA</label>
                                            <asp:TextBox ID="txtmanameupdate" CssClass="form-control" runat="server" Text='<%# Bind("ma_name") %>' />

                                        </small>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">สถานะ</label>
                                            <asp:DropDownList ID="ddlStatusUpdate" AutoPostBack="false" runat="server"
                                                CssClass="form-control" SelectedValue='<%# Eval("m0status") %>'>
                                                <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                            </asp:DropDownList>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                            ValidationGroup="saveTypeNameUpdate" CommandName="Update"
                                            OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                         บันทึก
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                            CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="detail" runat="server" Text='<%# Eval("detail") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="หัวข้อ MA" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="ma_name" runat="server" Text='<%# Eval("ma_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Statuscategory" runat="server" Text='<%# getStatus((int)Eval("m0status")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("m0idx") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>

        </asp:View>

        <asp:View ID="ViewInsert" runat="server">

            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวเจ้าหน้าที่</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetaiAdmin" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>


            </div>

            <div class="col-lg-12">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-wrench"></i><strong>&nbsp; ข้อมูลรายการ MA</strong></h3>
                    </div>

                    <asp:FormView ID="FvMaList" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">

                                        <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="เลือประเภทค้นหา : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlseach" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกประเภทค้นหา...</asp:ListItem>
                                                <asp:ListItem Value="1">ประเภทอุปกรณ์</asp:ListItem>
                                                <asp:ListItem Value="2">ผู้ถือครอง</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="btnsearch_ma" runat="server" Display="None"
                                                ControlToValidate="ddlseach" Font-Size="11"
                                                ErrorMessage="กรุณาเลือประเภทค้นหา"
                                                ValidationExpression="กรุณาเลือประเภทค้นหา" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                        </div>

                                    </div>

                                    <asp:Panel ID="panel_typedevice" runat="server" Visible="false">

                                        <div class="form-group">
                                            <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlsearchtype" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control" runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="btnsearch_ma" runat="server" Display="None"
                                                    ControlToValidate="ddlsearchtype" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือประเภทอุปกรณ์"
                                                    ValidationExpression="กรุณาเลือประเภทอุปกรณ์" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                            </div>
                                            <asp:Label ID="Label4" CssClass="col-sm-3 control-label" runat="server" Text="เลือกอุปกรณ์ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddldevices" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกอุปกรณ์...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="btnsearch_ma" runat="server" Display="None"
                                                    ControlToValidate="ddldevices" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกอุปกรณ์"
                                                    ValidationExpression="กรุณาเลือกอุปกรณ์" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                            </div>

                                        </div>

                                    </asp:Panel>

                                    <asp:Panel ID="panel_holder" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlorgidx" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control" runat="server"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="btnsearch_ma" runat="server" Display="None"
                                                    ControlToValidate="ddlorgidx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกบริษัท"
                                                    ValidationExpression="กรุณาเลือกบริษัท" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                            </div>
                                            <asp:Label ID="Label3" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddldeptidx" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="btnsearch_ma" runat="server" Display="None"
                                                    ControlToValidate="ddldeptidx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกฝ่าย"
                                                    ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label10" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlsecidx" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกแผนก...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="btnsearch_ma" runat="server" Display="None"
                                                    ControlToValidate="ddlsecidx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกแผนก"
                                                    ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                            </div>
                                            <asp:Label ID="Label11" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อ-นามสกุล : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlempidx" CssClass="form-control " OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกผู้ถือครอง...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="btnsearch_ma" runat="server" Display="None"
                                                    ControlToValidate="ddlempidx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกผู้ถือครอง"
                                                    ValidationExpression="กรุณาเลือกผู้ถือครอง" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <asp:Label ID="Label12" CssClass="col-sm-2 control-label" runat="server" Text="อุปกรณ์ถือครอง : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddl_holderdevice" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกผู้ถือครอง...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="btnsearch_ma" runat="server" Display="None"
                                                    ControlToValidate="ddl_holderdevice" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกผู้ถือครอง"
                                                    ValidationExpression="กรุณาเลือกผู้ถือครอง" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />

                                            </div>

                                        </div>

                                    </asp:Panel>

                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-2">
                                            <asp:LinkButton ID="btnsearch" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch_insert" ValidationGroup="btnsearch_ma" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>

                                        </div>

                                    </div>


                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-7" runat="server" id="div_FvDevice" visible="false">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-tasks"></i><strong>&nbsp; ข้อมูลเครื่อง</strong></h3>
                    </div>
                    <div class="panel-body">
                        <asp:FormView ID="FvDevice" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <ItemTemplate>
                                <div class="form-horizontal" role="form">


                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="ประเภทอุปกรณ์ : " />
                                        <div class="col-sm-3">
                                            <asp:Label ID="lbltypedevice" CssClass="control-labelnotop" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                            <asp:Label ID="lblu0didx" Visible="false" CssClass="control-labelnotop" runat="server" Text='<%# Eval("u0_didx") %>'></asp:Label>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="เลขทะเบียนอุปกรณ์ : " />
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblcode" CssClass="control-labelnotop" runat="server" Text='<%# Eval("u0_code") %>'></asp:Label>
                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="เลข Asset : " />
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblacc" CssClass="control-labelnotop" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="ผู้ถือครอง : " />
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblEmpname" CssClass="control-labelnotop" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                            <asp:Label ID="lblEmpIDX" CssClass="control-labelnotop" Visible="false" runat="server" Text='<%# Eval("CEmpIDX") %>'></asp:Label>
                                            <asp:Label ID="lblNOrgIDX" CssClass="control-labelnotop" Visible="false" runat="server" Text='<%# Eval("NOrgIDX") %>'></asp:Label>
                                            <asp:Label ID="lblNRDeptIDX" CssClass="control-labelnotop" Visible="false" runat="server" Text='<%# Eval("NRDeptIDX") %>'></asp:Label>
                                            <asp:Label ID="lblNRSecIDX" CssClass="control-labelnotop" Visible="false" runat="server" Text='<%# Eval("NRSecIDX") %>'></asp:Label>
                                        </div>

                                    </div>



                                </div>

                            </ItemTemplate>
                        </asp:FormView>

                        <asp:GridView ID="GvMa"
                            runat="server"
                            AutoGenerateColumns="false"
                            DataKeyNames="m0idx"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            HeaderStyle-CssClass="info"
                            HeaderStyle-Height="40px"
                            AllowPaging="true"
                            PageSize="10"
                            OnPageIndexChanging="Master_PageIndexChanging">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center;">Data Cannot Be Found</div>
                            </EmptyDataTemplate>


                            <Columns>
                                <asp:TemplateField HeaderText="เลือกรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <small>
                                            <asp:CheckBox ID="chkma" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="หัวข้อ MA." HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Literal ID="lblm0idx" Visible="false" runat="server" Text='<%# Eval("m0idx") %>' />
                                            <asp:Literal ID="lblma" runat="server" Text='<%# Eval("ma_name") %>' /></small>

                                    </ItemTemplate>
                                </asp:TemplateField>





                            </Columns>

                        </asp:GridView>


                        <div class="form-group">
                            <div>
                                <%--class="col-sm-5 col-sm-offset-1"--%>
                                <asp:LinkButton ID="btninsert_ma" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btninsert_ma" ValidationGroup="btnsearch_insert" OnCommand="btnCommand"><i class="glyphicon glyphicon-plus-sign"></i> ADD</asp:LinkButton>
                                <%-- <asp:LinkButton ID="LinkButton1" CssClass="btn btn-danger btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch_clear" ValidationGroup="btnsearch_ma" OnCommand="btnCommand"><i class="glyphicon glyphicon-minus-sign"></i> CLEAR </asp:LinkButton>--%>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-5" id="div_ma" runat="server" visible="false">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-briefcase"></i><strong>&nbsp; รายการ MA</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <div class="form-group">

                                <asp:Label ID="Label32" CssClass="col-sm-3 control-label" runat="server" Text="วันที่สร้าง : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtteladd" CssClass="form-control" MaxLength="10" runat="server"></asp:TextBox>

                                    <asp:RegularExpressionValidator ID="RetxtUnit928" runat="server"
                                        ValidationGroup="SaveAdd" Display="None"
                                        ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                        ControlToValidate="txtteladd"
                                        ValidationExpression="^[0-9]{1,12}$" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender72" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtUnit928" Width="160" />

                                </div>
                            </div>


                            <asp:GridView ID="GvReportAdd"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered col-lg-4"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-Height="40px"
                                ShowFooter="False"
                                ShowHeaderWhenEmpty="True"
                                AllowPaging="True"
                                PageSize="5"
                                BorderStyle="None"
                                CellSpacing="2"
                                OnRowDeleting="Master_RowDeleting"
                                OnPageIndexChanging="Master_PageIndexChanging">


                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>

                                <Columns>

                                    <asp:TemplateField HeaderText="#">
                                        <ItemTemplate>
                                            <div class="panel-heading">
                                                <%# (Container.DataItemIndex +1) %>
                                            </div>
                                        </ItemTemplate>


                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เลขทะเบียนอุปกรณ์">
                                        <ItemTemplate>
                                            <div class="panel-heading">
                                                <asp:Label ID="lblu0_didx" Visible="false" runat="server" Text='<%# Eval("u0_didx")%>'></asp:Label>
                                                <asp:Label ID="Label13" runat="server" Text='<%# Eval("u0_code")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายการMA">
                                        <ItemTemplate>
                                            <div class="panel-heading">
                                                <asp:Label ID="lblm0idx" runat="server" Visible="false" Text='<%# Eval("m0idx")%>'></asp:Label>
                                                <asp:Label ID="Label14" runat="server" Text='<%# Eval("ma_name")%>'></asp:Label>
                                                <asp:Literal ID="lblemp" Visible="false" runat="server" Text='<%# Eval("CEmpIDX") %>' />
                                            </div>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="จัดการ">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-sm" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="Delete"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>


                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>

                            </asp:GridView>


                            <asp:Panel ID="panel_comment" runat="server">

                                <%-------------- หมายเหตุการแจ้ง --------------%>
                                <div class="form-group">

                                    <asp:Label ID="Labedle1" class="col-sm-3 control-label" runat="server" Text="ความคิดเห็น : " />
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtremark" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtremark" Font-Size="11"
                                            ErrorMessage="กรุณากรอกความคิดเห็น"
                                            ValidationExpression="กรุณากรอกความคิดเห็น"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                            ValidationGroup="SaveAdd" Display="None"
                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                            ControlToValidate="txtremark"
                                            ValidationExpression="^[\s\S]{0,1000}$"
                                            SetFocusOnError="true" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                    </div>


                                </div>


                            </asp:Panel>

                            <div class="form-group">
                                <div class="col-sm-5 col-sm-offset-5">
                                    <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="glyphicon glyphicon-ok-circle"></i> บันทึก</asp:LinkButton>
                                    <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-remove-circle"></i> ยกเลิก</asp:LinkButton>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewReport" runat="server">
            <asp:UpdatePanel runat="server" ID="Updatepn1">
                <ContentTemplate>
                    <div class="col-lg-12" runat="server" id="Div1">

                        <div id="Div4" runat="server">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-menu-hamburger"></i><strong>&nbsp; ค้นหาข้อมูลรายงาน</strong></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <asp:Label ID="Label28" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทรายงาน" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddltypereport" CssClass="form-control" runat="server">
                                                    <asp:ListItem Text="กรุณาเลือกประเภทรายงาน ...." Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="ตาราง" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="กราฟ" Value="2"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label24" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ค้นหา" />
                                            <div class="col-sm-2">
                                                <asp:DropDownList ID="ddlSearchDate_report" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Text="เลือกเงื่อนไข ...." Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="มากกว่า >" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="น้อยกว่า <" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="ระหว่าง <>" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3 ">

                                                <div class='input-group date'>
                                                    <asp:TextBox ID="AddStartDate_report" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="btnsearch" runat="server" Display="None"
                                                        ControlToValidate="AddStartDate_report" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                        ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="AddEndDate_report" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label25" CssClass="col-sm-2 control-label" runat="server" Text="Organization" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSearchOrg_Report" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>

                                            <asp:Label ID="Label26" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากฝ่าย" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSearchDept_Report" AutoPostBack="true" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกฝ่าย....</asp:ListItem>

                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <asp:Label ID="Label27" CssClass="col-sm-2 control-label" runat="server" Text="ขั้นตอนดำเนินการ" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSearchStatus_Report" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกขั้นตอนดำเนินการ ..." />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 col-sm-offset-2">
                                                <asp:LinkButton ID="btnsearch_report" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch_report" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                                <asp:LinkButton ID="btnexport" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Export Excel" runat="server" CommandName="btnexport" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="glyphicon glyphicon-export"></i> </asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton11" CssClass="btn btn-danger btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="BtnBack" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <%--<div id="Div2" runat="server">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">--%>
                        <div class="panel panel-primary" id="div_table" runat="server" visible="false">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <asp:GridView ID="GvReport"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        DataKeyNames="u0idx"
                                        CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                                        HeaderStyle-CssClass="info"
                                        AllowPaging="true"
                                        PageSize="10"
                                        OnPageIndexChanging="Master_PageIndexChanging"
                                        OnRowDataBound="Master_RowDataBound">
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">No result</div>
                                        </EmptyDataTemplate>
                                        <Columns>


                                            <asp:TemplateField HeaderText="เลขทะเบียนอุปกรณ์" ItemStyle-HorizontalAlign="center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblu0idx" Visible="false" runat="server" Text='<%# Eval("u0idx") %>' />
                                                        <asp:Label ID="lblcode" runat="server" Text='<%# Eval("u0_code") %>' />
                                                    </small>
                                                </ItemTemplate>
                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblcreatedate" runat="server" Text='<%# Eval("CreateDate") %>' />
                                                    </small>
                                                </ItemTemplate>
                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชื่อผู้สร้าง" ItemStyle-HorizontalAlign="center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lbladmin" runat="server" Text='<%# Eval("AdminName") %>' />
                                                    </small>
                                                </ItemTemplate>
                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="ข้อมูลผู้ถือครอง" ItemStyle-HorizontalAlign="left"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <strong>
                                                            <asp:Label ID="Label12" runat="server">บริษัท: </asp:Label></strong>
                                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                        </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">ฝ่าย: </asp:Label></strong>
                                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                        </p>
                                            <strong>
                                                <asp:Label ID="Label13" runat="server">แผนก: </asp:Label></strong>
                                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Label22" runat="server">ชื่อผู้ถือครอง: </asp:Label></strong>
                                                        <asp:Label ID="lblempname" runat="server" Text='<%# Eval("EmpName") %>' />
                                                        </p>
                                                    </small>
                                                    <%--<small>
                                            <asp:Label ID="lblempname" runat="server" Text='<%# Eval("EmpName") %>' />
                                        </small>--%>
                                                </ItemTemplate>
                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-HorizontalAlign="center"
                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                                    </small>
                                                </ItemTemplate>
                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>



                                        </Columns>

                                    </asp:GridView>

                                </div>
                            </div>
                        </div>

                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="true">
                            <Columns>
                                <asp:TemplateField HeaderText="ลำดับที่" ItemStyle-Width="100">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>



                        <div class="panel panel-primary" id="div_grant" runat="server" visible="false">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                            </div>
                            <div class="panel-body">
                                <asp:Literal ID="litReportChart" runat="server" />
                            </div>
                        </div>
                    </div>

                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnsearch_report" />
                    <asp:PostBackTrigger ControlID="btnexport" />
                </Triggers>
            </asp:UpdatePanel>

        </asp:View>


    </asp:MultiView>

</asp:Content>

