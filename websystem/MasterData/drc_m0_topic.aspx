﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="drc_m0_topic.aspx.cs" Inherits="websystem_MasterData_drc_m0_topic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <div class="col-sm-12">
        <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
        <asp:Literal ID="test_lab" runat="server"></asp:Literal>
    </div>

    <asp:MultiView ID="MvMaster_lab" runat="server">
        <asp:View ID="view_Genaral" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btn_addsetname" Visible="true" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มหัวข้อ" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddSetTopicname" CommandArgument="0" title="เพิ่มหัวข้อ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มหัวข้อ</asp:LinkButton>
            </div>

            <%--  DIV ADD--%>
            <asp:FormView ID="FvInsertTopicName" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มหัวข้อ</h3>

                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="lbl_topic_name" runat="server" Text="ชื่อหัวข้อ" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_topic_name" runat="server" CssClass="form-control" placeholder="กรอกชื่อหัวข้อ ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_txt_topic_name" runat="server"
                                                    ControlToValidate="txt_topic_name" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อหัวข้อ" ValidationGroup="saveInsert" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderlabTname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_topic_name" Width="250" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_topic_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddl_topic_status" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-9">
                                                <asp:LinkButton ID="btnSaveInsertTopic" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" OnCommand="btnCommand" CommandName="cmdSave" ValidationGroup="saveInsert" CommandArgument="0"></asp:LinkButton>

                                                <asp:LinkButton ID="btnCancelInsertTopic" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0"></asp:LinkButton>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <hr />
                                <%--  /DIV END ADD--%>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <%--select setname--%>
            <asp:GridView ID="GvTopic" runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่มีข้อมูล --</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="8%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="lbl_sidx" runat="server" Visible="false" Text='<%# Eval("tidx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_tidx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("tidx") %>'></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lbl_topic_name_edit" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อหัวข้อ" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_topic_name_edit" runat="server" CssClass="form-control " Text='<%# Eval("topic_name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Re_txt_topic_name_edit" runat="server"
                                                    ControlToValidate="txt_topic_name_edit" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อหัวข้อ" ValidationGroup="Edit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txt_topic_name_edit" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_topic_name_edit" Width="250" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_topic_status_edit" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="dd_topic_status_edit" Text='<%# Eval("topic_status") %>'
                                                    CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                    <asp:ListItem Value="0">Offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-9">
                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Edit" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อหัวข้อ" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="30%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_topic_name" runat="server" Text='<%# Eval("topic_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_topic_status" Visible="false" runat="server"
                                        Text='<%# Eval("topic_status") %>'></asp:Label>
                                    <asp:Label ID="lbl_topic_statussOpen" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="lbl_topic_statusClose" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="btnViewSettopicname" CssClass="text-read" runat="server" CommandName="cmdViewSetTopicname"
                                    data-toggle="tooltip" OnCommand="btnCommand"
                                    CommandArgument='<%#Eval("tidx")+ ";" + Eval("topic_name")%>' title="view">
                                        <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="cmdDeleteSetTopicname"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบแลปนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("tidx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
        </asp:View>

        <asp:View ID="view_CreateRoot" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnBack" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdback" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>

            </div>
            <div class="form-group">
                <asp:LinkButton ID="btnAddRootSet" CssClass="btn btn-primary" Visible="true" runat="server" data-original-title="เพิ่มหัวข้อย่อย" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddSetTopicname" CommandArgument="1" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มหัวข้อย่อย</asp:LinkButton>
            </div>
            <!--formview insert Level 2-->
            <asp:FormView ID="fvform_Insert_Root" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มหัวข้อย่อย</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ชื่อหัวหลัก</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txt_tidx_root" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>
                                        <%--<asp:TextBox ID="TextBox1" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("sidx") %>'></asp:TextBox>--%>
                                        <asp:TextBox ID="txt_tidx_root_list" runat="server" CssClass="form-control" placeholder="ชื่อหัวหลัก ..." Enabled="false" />

                                    </div>

                                    <div class="col-sm-3"></div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ชื่อหัวข้อย่อย</label>
                                    <div class="col-sm-6">
                                        <%-- <asp:TextBox ID="txt_IDtestDetail" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("test_detail_idx") %>'></asp:TextBox>--%>
                                        <asp:TextBox ID="txt_topic_name_set" CssClass="form-control" runat="server" placeholder="กรอกชื่อหัวข้อย่อย ...">
                                        </asp:TextBox>

                                        <asp:RequiredFieldValidator ID="Re_txt_topic_name_set" runat="server"
                                            ControlToValidate="txt_topic_name_set" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกชื่อหัวข้อย่อย" ValidationGroup="SaveEdit" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txt_topic_name_set" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_topic_name_set" Width="250" />

                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ชื่อชุดข้อมูล</label>
                                    <div class="col-sm-6">
                                        <%-- <asp:TextBox ID="txt_IDtestDetail" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("test_detail_idx") %>'></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlsettopic" CssClass="form-control" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="Re_ddlsettopic1" runat="server" InitialValue="00"
                                            ControlToValidate="ddlsettopic" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกชื่อชุดข้อมูล" ValidationGroup="SaveEdit" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1444" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlsettopic1" Width="250" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Option</label>
                                    <div class="col-sm-6">
                                        <%-- <asp:TextBox ID="txt_IDtestDetail" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("test_detail_idx") %>'></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddloption" CssClass="form-control" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="Re_ddl_Option" runat="server" InitialValue="00"
                                            ControlToValidate="ddloption" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือก Option" ValidationGroup="SaveEdit" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddl_Option" Width="250" />

                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">สถานะ</label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlroot_topicstatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <asp:LinkButton ID="lnkbtnSave" ValidationGroup="SaveEdit" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="1"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <%--select lab M1--%>

            <asp:GridView ID="GvRootSetTopicName" runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่มีข้อมูล --</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="lblUIDX" runat="server" Visible="false" Text='<%# Eval("tidx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">

                                        <div class="form-group">
                                            <asp:Label ID="lb_testdetail" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อหัวข้อย่อย" />
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblUIDX_edit" runat="server" Visible="false" Text='<%# Eval("tidx") %>' />
                                                <asp:TextBox ID="txt_topic_name_edit" Visible="true" runat="server" CssClass="form-control" Text='<%# Eval("topic_name") %>'></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_topic_name_edit" runat="server"
                                                    ControlToValidate="txt_topic_name_edit"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อสถานะ" ValidationGroup="Edit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderlabm1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_topic_name_edit" Width="200" />
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชุดที่" />
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txt_sidx_edit_root" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("sidx") %>'></asp:TextBox>
                                                <asp:DropDownList ID="ddlsettopicsidxedit" AutoPostBack="true" runat="server" CssClass="form-control">
                                                </asp:DropDownList></small>

                                                <asp:RequiredFieldValidator ID="Re_ddlsettopicsidxedit" runat="server" InitialValue="00"
                                                    ControlToValidate="ddlsettopicsidxedit"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกชุดที่" ValidationGroup="Edit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlsettopicsidxedit" Width="200" />
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทการกรอกข้อมูล" />
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txt_OPIDX_edit" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("OPIDX") %>'></asp:TextBox>
                                                <asp:DropDownList ID="ddloptionedit" AutoPostBack="true" runat="server" CssClass="form-control">
                                                </asp:DropDownList></small>

                                                <asp:RequiredFieldValidator ID="Re_ddloptionedit" runat="server" InitialValue="00"
                                                    ControlToValidate="ddloptionedit"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกประเภทการกรอกข้อมูล" ValidationGroup="Edit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddloptionedit" Width="200" />
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="lbupdates_lab" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddltopic_status_edit" Text='<%# Eval("topic_status") %>'
                                                    CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                    <asp:ListItem Value="0">Offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-4">
                                                <asp:LinkButton ID="btnupdate" ValidationGroup="Edit" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อหัวข้อย่อย" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <%-- <asp:Label ID="m2Lab_idx" runat="server" Visible="false" Text='<%# Eval("equipment_idx") %>' />--%>
                                    <asp:Label ID="lb_topic_name_root" Visible="true" runat="server"
                                        Text='<%# Eval("topic_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อชุดข้อมูล" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <%-- <asp:Label ID="m2Lab_idx" runat="server" Visible="false" Text='<%# Eval("equipment_idx") %>' />--%>
                                    <asp:Label ID="lb_set_name_root" Visible="true" runat="server"
                                        Text='<%# Eval("set_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ประเภทที่กรอก" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <%-- <asp:Label ID="m2Lab_idx" runat="server" Visible="false" Text='<%# Eval("equipment_idx") %>' />--%>
                                    <asp:Label ID="lb_Option_name_root" Visible="true" runat="server"
                                        Text='<%# Eval("Option_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_topic_status_root" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("topic_status") %>'></asp:Label>
                                    <asp:Label ID="lbl_topic_status_root_Online" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="lbl_topic_status_root_Offline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="btnViewSettopicnameRoot" CssClass="text-read" runat="server" CommandName="cmdViewSetTopicnameRoot"
                                    data-toggle="tooltip" OnCommand="btnCommand"
                                    CommandArgument='<%#Eval("tidx")+ ";" + Eval("topic_name")%>' title="view">
                                    <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i>
                                </asp:LinkButton>

                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodeleteRoot" CssClass="text-trash" runat="server" CommandName="btnTodeleteTopicRoot"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการที่ตรวจนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("tidx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

        </asp:View>

        <asp:View ID="view_CreateSetRoot" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnBackToSetRoot" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdbackTosetRoot" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
            </div>
            <div class="form-group">
                <asp:LinkButton ID="btnAddSetRootTopicname" CssClass="btn btn-primary" Visible="true" runat="server" data-original-title="เพิ่มรายละเอียดหัวข้อย่อย" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddSetTopicname" CommandArgument="2" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มรายละเอียดหัวข้อย่อย</asp:LinkButton>
            </div>

            <!--formview insert Level 2-->
            <asp:FormView ID="fvform_root" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มรายละเอียดหัวข้อย่อย</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ชื่อหัวข้อย่อย</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txt_tidx_root_lv" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>
                                        <%--<asp:TextBox ID="TextBox1" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("sidx") %>'></asp:TextBox>--%>
                                        <asp:TextBox ID="txt_tidx_root_list_lv" runat="server" CssClass="form-control" placeholder="ชื่อหัวข้อย่อย ..." Enabled="false" />

                                    </div>

                                    <div class="col-sm-3"></div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">รายละเอียดหัวข้อย่อย</label>
                                    <div class="col-sm-6">
                                        <%-- <asp:TextBox ID="txt_IDtestDetail" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("test_detail_idx") %>'></asp:TextBox>--%>
                                        <asp:TextBox ID="txt_topic_name_set_lv" CssClass="form-control" runat="server" placeholder="กรอกรายละเอียดหัวข้อย่อย ...">
                                        </asp:TextBox>

                                        <asp:RequiredFieldValidator ID="Re_txt_topic_name_set_lv" runat="server"
                                            ControlToValidate="txt_topic_name_set_lv" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกชื่อหัวข้อย่อย" ValidationGroup="SaveEdit" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txt_topic_name_set_lv" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_topic_name_set_lv" Width="250" />

                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ชื่อชุดข้อมูล</label>
                                    <div class="col-sm-6">
                                        <%-- <asp:TextBox ID="txt_IDtestDetail" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("test_detail_idx") %>'></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlsettopic_lv" CssClass="form-control" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="Re_ddlsettopic_lv" runat="server" InitialValue="00"
                                            ControlToValidate="ddlsettopic_lv" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกชื่อชุดข้อมูล" ValidationGroup="SaveEdit" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1444" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlsettopic_lv" Width="250" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Option</label>
                                    <div class="col-sm-6">
                                        <%-- <asp:TextBox ID="txt_IDtestDetail" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("test_detail_idx") %>'></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddloption_lv" CssClass="form-control" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="Re_ddloption_lv" runat="server" InitialValue="00"
                                            ControlToValidate="ddloption_lv" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือก Option" ValidationGroup="SaveEdit" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddloption_lv" Width="250" />

                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">สถานะ</label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlroot_topicstatus_lv" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <asp:LinkButton ID="lnkbtnSave" ValidationGroup="SaveEdit" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="2"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="2"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <%--select lab M1--%>

            <asp:GridView ID="GvRootLevel" runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่มีข้อมูล --</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="lblUIDX" runat="server" Visible="false" Text='<%# Eval("tidx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">

                                        <div class="form-group">
                                            <asp:Label ID="lb_testdetail" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดหัวข้อย่อย" />
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblUIDX_edit_root" runat="server" Visible="false" Text='<%# Eval("tidx") %>' />
                                                <asp:TextBox ID="txt_topic_name_edit_root" Visible="true" runat="server" CssClass="form-control" Text='<%# Eval("topic_name") %>'></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_topic_name_edit_root" runat="server"
                                                    ControlToValidate="txt_topic_name_edit_root"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณารายละเอียดหัวข้อย่อย" ValidationGroup="Edit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderlabm1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_topic_name_edit_root" Width="200" />
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อชุดข้อมูล" />
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txt_sidx_edit_root_sub" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("sidx") %>'></asp:TextBox>
                                                <asp:DropDownList ID="ddlsettopicsidxedit_sub" AutoPostBack="true" runat="server" CssClass="form-control">
                                                </asp:DropDownList></small>

                                                <asp:RequiredFieldValidator ID="Re_ddlsettopicsidxedit_sub" runat="server" InitialValue="00"
                                                    ControlToValidate="ddlsettopicsidxedit_sub"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกชุดที่" ValidationGroup="Edit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlsettopicsidxedit_sub" Width="200" />
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทการกรอกข้อมูล" />
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txt_OPIDX_edit_sub" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("OPIDX") %>'></asp:TextBox>
                                                <asp:DropDownList ID="ddloptionedit_sub" AutoPostBack="true" runat="server" CssClass="form-control">
                                                </asp:DropDownList></small>

                                                <asp:RequiredFieldValidator ID="Re_ddloptionedit_sub" runat="server" InitialValue="00"
                                                    ControlToValidate="ddloptionedit_sub"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกประเภทการกรอกข้อมูล" ValidationGroup="Edit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddloptionedit_sub" Width="200" />
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="lbupdates_lab" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddltopic_status_edit_root" Text='<%# Eval("topic_status") %>'
                                                    CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                    <asp:ListItem Value="0">Offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-4">
                                                <asp:LinkButton ID="btnupdate" ValidationGroup="Edit" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="รายละเอียดหัวข้อย่อย" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <%-- <asp:Label ID="m2Lab_idx" runat="server" Visible="false" Text='<%# Eval("equipment_idx") %>' />--%>
                                    <asp:Label ID="lb_topic_name_root" Visible="true" runat="server"
                                        Text='<%# Eval("topic_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อชุดข้อมูล" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <%-- <asp:Label ID="m2Lab_idx" runat="server" Visible="false" Text='<%# Eval("equipment_idx") %>' />--%>
                                    <asp:Label ID="lb_set_name_root" Visible="true" runat="server"
                                        Text='<%# Eval("set_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ประเภทที่กรอก" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <%-- <asp:Label ID="m2Lab_idx" runat="server" Visible="false" Text='<%# Eval("equipment_idx") %>' />--%>
                                    <asp:Label ID="lb_Option_name_root" Visible="true" runat="server"
                                        Text='<%# Eval("Option_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_topic_status_sub" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("topic_status") %>'></asp:Label>
                                    <asp:Label ID="lbl_topic_status_sub_Online" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="lbl_topic_status_sub_Offline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                             
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodeleteRoot" CssClass="text-trash" runat="server" CommandName="btnTodeleteSubTopicRoot"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการที่ตรวจนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("tidx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>


        </asp:View>

    </asp:MultiView>
</asp:Content>

