﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_m0_training_location.aspx.cs" Inherits="websystem_elearning_master_el_el_m0_training_location" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp;Training Location (สถานที่อบรม) </strong></h3>
                        </div>

                        <div class="panel-body">
                            <%-- <div class="form-horizontal" role="form">--%>
                            <div class="form-group">
                                <div class="col-sm-5">
                                    <asp:LinkButton ID="btnaddholder" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddStatus" runat="server" CommandName="CmdAddHolder" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                                </div>
                                <div class="col-sm-3 col-sm-offset-3">
                                    <%--<asp:DropDownList ID="ddl_cost_center_search" runat="server" CssClass="form-control">
                                    </asp:DropDownList>--%>
                                </div>
                                <div class="col-sm-1">
                                    <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary" runat="server" Text="Search" OnCommand="btnCommand" CommandName="CmdSearch" data-toggle="tooltip" title="Search"><i class="fa fa-search"></i></asp:LinkButton>
                                </div>
                            </div>
                            <hr /> 
                            <%------------------------ Div ADD  ------------------------%>

                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Master Training Location</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <%--<div class="panel panel-default">--%>
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="ประเภทสถานที่อบรม" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddllocation_type" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือก ประเภทสถานที่อบรม" />
                                                        <asp:ListItem Value="1" Text="ภายใน" />
                                                        <asp:ListItem Value="2" Text="ภายนอก" />
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="Requidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="ddllocation_type" Font-Size="11"
                                                    ErrorMessage="select location type ...."
                                                    ValidationExpression="select location type ...." InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requidator1" Width="160" />
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="ชื่อสถานที่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:TextBox ID="txtlocation_name" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="Reqto2r1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="txtlocation_name" Font-Size="11"
                                                    ErrorMessage="Plese location name" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validatoender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqto2r1" Width="160" />
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="ที่อยู่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:TextBox ID="txtlocation_address" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                </div>
                                                <asp:RequiredFieldValidator ID="Rewator2" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="txtlocation_address" Font-Size="11"
                                                    ErrorMessage="Plese location address" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rewator2" Width="160" />
                                            </div>

                                            <%--<div class="form-group">
                                                <asp:Label ID="Label25" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddl_status" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1">Online.....</asp:ListItem>
                                                        <asp:ListItem Value="0">Offline.....</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>--%>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="SaveAdd" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <hr />
                                    <%--  </div>--%>
                                </div>
                            </asp:Panel>


                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="LOIDX"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblLOIDX" runat="server" Visible="false" Text='<%# Eval("LOIDX") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtLOIDX_edit" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("LOIDX")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label3" runat="server" Text="ประเภทสถานที่อบรม" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <asp:Label ID="lbllocation_type_edit" runat="server" Visible="false" Text='<%# Eval("location_type")%>'></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddllocation_type_edit" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="1" Text="ภายใน" />
                                                                <asp:ListItem Value="2" Text="ภายนอก" />
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="Requorw1" ValidationGroup="SaveEdit" runat="server" Display="None"
                                                            ControlToValidate="ddllocation_type_edit" Font-Size="11"
                                                            ErrorMessage="select location type ...."
                                                            ValidationExpression="select location type ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requorw1" Width="160" />
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ชื่อสถานที่อบรม" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtlocation_name_edit" runat="server" CssClass="form-control" Text='<%# Eval("location_name")%>' />
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="Req_edit" ValidationGroup="SaveEdit" runat="server" Display="None"
                                                            ControlToValidate="txtlocation_name_edit" Font-Size="11"
                                                            ErrorMessage="Plese location name" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatoer2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_edit" Width="160" />
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="ที่อยู่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtlocation_address_edit" runat="server" CssClass="form-control" Text='<%# Eval("location_address")%>' />
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="Reatasdor2" ValidationGroup="SaveEdit" runat="server" Display="None"
                                                            ControlToValidate="txtlocation_address_edit" Font-Size="11"
                                                            ErrorMessage="Plese address" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValiutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reatasdor2" Width="160" />
                                                    </div>

                                                    <%--<div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />

                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("course_status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>--%>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-3">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="SaveEdit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภทสถานที่อบรม" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbllocation_type" runat="server" Text='<%# Eval("location_type_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อสถานที่อบรม" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbllocation_name" runat="server" Text='<%# Eval("location_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ที่อยู่" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbllocation_address" runat="server" Text='<%# Eval("location_address") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%--<asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcourse_status" runat="server" Text='<%# Eval("course_status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("LOIDX") %>'><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

