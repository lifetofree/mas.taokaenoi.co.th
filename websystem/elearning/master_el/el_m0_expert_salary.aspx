﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_m0_expert_salary.aspx.cs" Inherits="websystem_elearning_master_el_el_m0_expert_salary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="text" runat="server"></asp:Literal>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp;Cost Training (ค่าใช้จ่ายฝึกอบรม) </strong></h3>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <asp:LinkButton ID="btnaddholder" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="เพิ่มค่าใช้จ่าย" runat="server" CommandName="CmdAddHolder" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>



                            <%------------------------ Div ADD  ------------------------%>

                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Master Cost Training</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <%--<div class="panel panel-default">--%>
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="รายละเอียดค่าใช้จ่าย" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:TextBox ID="txtexpert_detail_add" runat="server" CssClass="form-control" />
                                                </div>
                                                <asp:RequiredFieldValidator ID="ee23or1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="txtexpert_detail_add" Font-Size="11"
                                                    ErrorMessage="Plese detail" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Valnder2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ee23or1" Width="160" />
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="งบประมาณ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:TextBox ID="txtexpert_num_add" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                </div>
                                                <asp:RegularExpressionValidator ID="R_course_nub1" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtexpert_num_add" ValidationExpression="^[0-9'.\s]{1,6}$" ValidationGroup="SaveAdd" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="VS_Height2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_course_nub1" Width="160" />

                                                <asp:RequiredFieldValidator ID="Reqtor11" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="txtexpert_num_add" Font-Size="11"
                                                    ErrorMessage="Plese course salary" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validatoender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtor11" Width="160" />
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="Vat %" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:TextBox ID="txtexpert_vat_add" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                </div>
                                                <asp:RegularExpressionValidator ID="Regularator1" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtexpert_vat_add" ValidationExpression="^[0-9'.\s]{1,6}$" ValidationGroup="SaveAdd" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regularator1" Width="160" />

                                                <asp:RequiredFieldValidator ID="Reazr1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="txtexpert_vat_add" Font-Size="11"
                                                    ErrorMessage="Plese course salary" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reazr1" Width="160" />
                                            </div>

                                            <%--<div class="form-group">
                                                <asp:Label ID="Label25" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddl_status" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1">Online.....</asp:ListItem>
                                                        <asp:ListItem Value="0">Offline.....</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>--%>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="SaveAdd" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <hr />
                                    <%--  </div>--%>
                                </div>
                            </asp:Panel>


                            <div class="form-group" id="div_search" runat="server">
                                <asp:Label ID="Label121" runat="server" Text="ค่าใช้จ่าย" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3 ">
                                    <asp:TextBox ID="txtexpert_detail_search" runat="server" CssClass="form-control" PlaceHoldaer="....รายละเอียดค่าใช้จ่าย...." />
                                </div>
                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary" runat="server" Text="Search" OnCommand="btnCommand" CommandName="CmdSearch" data-toggle="tooltip" title="Search"><i class="fa fa-search"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnrefresh" CssClass="btn btn-default" runat="server" Text="Refresh" OnCommand="btnCommand" CommandName="CmdRefresh" data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i></asp:LinkButton>

                            </div>



                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="EPIDX"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblEPIDX" runat="server" Visible="false" Text='<%# Eval("EPIDX") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtEPIDX_edit" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("EPIDX")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label3" runat="server" Text="รายละเอียดค่าใช้จ่าย" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtexpert_detail_edit" runat="server" CssClass="form-control" Text='<%# Eval("expert_detail")%>' />
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="ee3or1" ValidationGroup="SaveEdit" runat="server" Display="None"
                                                            ControlToValidate="txtexpert_detail_edit" Font-Size="11"
                                                            ErrorMessage="Plese detail" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Valnder2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ee3or1" Width="160" />
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="งบประมาณ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtexpert_num_edit" runat="server" CssClass="form-control" Text='<%# Eval("expert_num")%>' />
                                                        </div>
                                                        <asp:RegularExpressionValidator ID="R_nub_edit" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtexpert_num_edit" ValidationExpression="^[0-9'.\s]{1,6}$" ValidationGroup="SaveEdit" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="VS_Height2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_nub_edit" Width="160" />

                                                        <asp:RequiredFieldValidator ID="Reqtor1_2edit" ValidationGroup="SaveEdit" runat="server" Display="None"
                                                            ControlToValidate="txtexpert_num_edit" Font-Size="11"
                                                            ErrorMessage="Plese salary" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatoer2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtor1_2edit" Width="160" />
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="Vat %" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtexpert_vat_edit" runat="server" CssClass="form-control" Text='<%# Eval("expert_vat")%>' />
                                                        </div>
                                                        <asp:RegularExpressionValidator ID="RegularEasor1" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtexpert_num_edit" ValidationExpression="^[0-9'.\s]{1,6}$" ValidationGroup="SaveEdit" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCatender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularEasor1" Width="160" />

                                                        <asp:RequiredFieldValidator ID="Reat3dor2" ValidationGroup="SaveEdit" runat="server" Display="None"
                                                            ControlToValidate="txtexpert_vat_edit" Font-Size="11"
                                                            ErrorMessage="Plese vat%" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValiutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reat3dor2" Width="160" />
                                                    </div>

                                                    <%--<div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />

                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("course_status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>--%>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-3">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="SaveEdit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียดค่าใช้จ่าย" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblexpert_detail" runat="server" Text='<%# Eval("expert_detail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="งบประมาณ" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblexpert_num" runat="server" Text='<%# Eval("expert_num") %>'></asp:Label>
                                            บาท
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Vat %" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblexpert_vat" runat="server" Text='<%# Eval("expert_vat") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%--<asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcourse_status" runat="server" Text='<%# Eval("course_status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("EPIDX") %>'><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

