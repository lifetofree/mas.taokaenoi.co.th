﻿using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_imxls_imxls : System.Web.UI.Page
{
   #region Init
   data_imxls dataImxls = new data_imxls();
   data_imxls dataImxlsStack = new data_imxls();
   service_execute servExec = new service_execute();
   function_tool _funcTool = new function_tool();
   string centralizedConn = "conn_centralized";
   string masConn = "conn_mas";
   string imxlsService = "imxlsService";
   string imxlsDistributeService = "imxlsDistributeService";
   string imxlsEmpcodeStackService = "imxlsEmpcodeStackService";
   string odspeService = "odspeService";
   string _local_xml = null;
   string _local_xml_stack = null;
   #endregion Init

   #region Constant
   public static class Constants
   {
      #region for region Action
      public const int SELECT_ALL = 20;
      public const int SELECT_WHERE = 21;
      public const int SELECT_EMP_PROFILE = 22;
      public const int SELECT_EMP_CODE_STACK = 21;
      public const int UPDATE_STACK_STATUS = 10;
      public const int CREATE = 10;
      public const int CREATE_WHERE = 11;
      public const int UPDATE_ROWS = 12;
      public const int BAN = 30;
      public const int UNBAN = 31;
      public const int NUMBER_NULL = -1;
      #endregion for region Action
   }
   #endregion Constant

   #region Rules
   int[] RulesEmpIdx = {
      4839,
      1335,
      1355,
      1339,
      3636,
      1413,
      172,
      173,
      1374,
      3760,
      3819,
      1347 //1347 empcode 9000001
   };
   #endregion Rules

   #region Page Load
   protected void Page_Load(object sender, EventArgs e)
   {
      if (Session["EmpIDX"] != null)
      {
         ViewState["EmpIDX"] = Session["EmpIDX"];
      }
      else if (Session["emp_idx"] != null)
      {
         ViewState["EmpIDX"] = Session["emp_idx"];
      }
      else
      {
         ViewState["EmpIDX"] = Session["EmpIDX"];
      }
      actionIndexWithRules();
      btnTrigger(btnImport);
   }
   #endregion Page Load

   #region btnCommand
   protected void btnCommand(object sender, CommandEventArgs e)
   {
      string cmdName = e.CommandName.ToString();
      string cmdArg = e.CommandArgument.ToString();
      switch (cmdName)
      {
         case "btnImport":
            if (upload.HasFile)
            {
               string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
               string FileName = Path.GetFileName(upload.PostedFile.FileName);
               string extension = Path.GetExtension(upload.PostedFile.FileName);
               string newFileName = datetimeNow + extension.ToLower();
               string folderPath = ConfigurationManager.AppSettings["imxlsFolderPath"];
               string filePath = Server.MapPath(folderPath + newFileName);
               if (extension.ToLower() == ".xls" || extension.ToLower() == ".xlsx")
               {
                  upload.SaveAs(filePath);
                  actionImport(filePath, extension, "Yes", FileName);
                  File.Delete(filePath);
                  Page.Response.Redirect(Page.Request.Url.ToString(), true);
               }
               else
               {
                  _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");
               }
            }
            break;

         case "btnDescription":
            gvXls.Visible = false;
            divGvXlsWhere.Visible = true;
            btnBack.Visible = true;
            actionRead(int.Parse(cmdArg));
            break;

         case "btnBack":
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;
      }
   }
   #endregion btnCommand

   #region Action
   protected void actionIndex()
   {
      imxls objU0Imxls = new imxls();
      dataImxls.imxls_myimxls_action = new imxls[1];
      dataImxls.imxls_myimxls_action[0] = objU0Imxls;
      _local_xml = servExec.actionExec(masConn, "data_imxls", imxlsService, dataImxls, Constants.SELECT_ALL);
      dataImxls = (data_imxls)_funcTool.convertXmlToObject(typeof(data_imxls), _local_xml);
      setGridData(gvXls, dataImxls.imxls_myimxls_action);
      fileArea.Visible = true;
      notRules.Visible = false;
   }

   protected void actionIndexWithRules()
   {
      int secIdx = getEmpProfile();
      if (Array.IndexOf(RulesEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
      {
         actionIndex();
      }
      else
      {
         getNotRulesMessage();
      }
   }

   protected void actionRead(int u0IdxRef)
   {
      imxls objU1Imxls = new imxls();
      dataImxls.imxls_myimxls_action = new imxls[1];
      objU1Imxls.u0_imxls_idx_ref = u0IdxRef;
      dataImxls.imxls_myimxls_action[0] = objU1Imxls;
      _local_xml = servExec.actionExec(masConn, "data_imxls", imxlsService, dataImxls, Constants.SELECT_WHERE);
      dataImxls = (data_imxls)_funcTool.convertXmlToObject(typeof(data_imxls), _local_xml);
      setGridData(gvXlsWhere, dataImxls.imxls_myimxls_action);
      fileArea.Visible = false;
      notRules.Visible = false;
      ViewState["u0IdxRef"] = u0IdxRef;
   }

   protected void actionImport(string FilePath, string Extension, string isHDR, string fileName)
   {
      string conStr = String.Empty;
      conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
      conStr = String.Format(conStr, FilePath, isHDR);
      OleDbConnection connExcel = new OleDbConnection(conStr);
      OleDbCommand cmdExcel = new OleDbCommand();
      OleDbDataAdapter oda = new OleDbDataAdapter();
      DataTable dt = new DataTable();
      cmdExcel.Connection = connExcel;
      connExcel.Open();
      DataTable dtExcelSchema;
      dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
      string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
      connExcel.Close();
      connExcel.Open();
      cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
      oda.SelectCommand = cmdExcel;
      oda.Fill(dt);
      connExcel.Close();
      imxls objU0Imxls = new imxls();
      dataImxls.imxls_myimxls_action = new imxls[1];
      objU0Imxls.imxls_filename = fileName.ToString();
      objU0Imxls.imxls_rows = 0;
      objU0Imxls.imxls_created_by = int.Parse(ViewState["EmpIDX"].ToString());
      dataImxls.imxls_myimxls_action[0] = objU0Imxls;
      _local_xml = servExec.actionExec(masConn, "data_imxls", imxlsService, dataImxls, Constants.CREATE_WHERE);
      dataImxls = (data_imxls)_funcTool.convertXmlToObject(typeof(data_imxls), _local_xml);
      var insertedU0Idx = dataImxls.return_inserted_u0_id;

      string idxCreated = String.Empty;
      int j = 1;
      for (var i = 0; i < dt.Rows.Count - 1; i++)
      {
         if (dt.Rows[j][1].ToString().Trim() != String.Empty || dt.Rows[j][2].ToString().Trim() != String.Empty || dt.Rows[j][5].ToString().Trim() != String.Empty)
         {
            imxlsempcodestack objImxlsEmpcodeStack = new imxlsempcodestack();
            dataImxlsStack.imxls_empcode_stack_action = new imxlsempcodestack[1];
            dataImxlsStack.imxls_empcode_stack_action[0] = objImxlsEmpcodeStack;
            _local_xml_stack = servExec.actionExec(masConn, "data_imxls", imxlsEmpcodeStackService, dataImxlsStack, Constants.SELECT_EMP_CODE_STACK);
            dataImxlsStack = (data_imxls)_funcTool.convertXmlToObject(typeof(data_imxls), _local_xml_stack);

            imxls objU1Imxls = new imxls();
            dataImxls.imxls_myimxls_action = new imxls[1];
            objU1Imxls.u0_imxls_idx_ref = int.Parse(insertedU0Idx);
            objU1Imxls.count_row = int.Parse(dt.Rows.Count.ToString()) - 1;
            objU1Imxls.empcode_new = dataImxlsStack.return_empcode_stack;
            objU1Imxls.prefix = dt.Rows[j][0].ToString().Trim();
            objU1Imxls.firstname_th = dt.Rows[j][1].ToString().Trim();
            objU1Imxls.lastname_th = dt.Rows[j][2].ToString().Trim();
            objU1Imxls.firstname_en = dt.Rows[j][3].ToString().Trim() == String.Empty ? "" : dt.Rows[j][3].ToString().Trim();
            objU1Imxls.lastname_en = dt.Rows[j][4].ToString().Trim() == String.Empty ? "" : dt.Rows[j][4].ToString().Trim();
            if (int.Parse(dt.Rows.Count.ToString()) - 1 > 1)
            {
               objU1Imxls.emp_in_2 = dt.Rows[j][5].ToString().Trim();
            }
            else
            {
               objU1Imxls.emp_in_1 = dt.Rows[j][5].ToString().Trim();
            }
            objU1Imxls.position = dt.Rows[j][6].ToString().Trim();
            objU1Imxls.section = dt.Rows[j][7].ToString().Trim();
            objU1Imxls.department = dt.Rows[j][8].ToString().Trim();
            objU1Imxls.cost_center = dt.Rows[j][9].ToString().Trim();
            objU1Imxls.sex = dt.Rows[j][10].ToString().Trim();
            objU1Imxls.birthday = dt.Rows[j][11].ToString().Trim();
            objU1Imxls.passport = dt.Rows[j][12].ToString().Trim() == String.Empty ? "" : dt.Rows[j][12].ToString().Trim();
            objU1Imxls.passport_issued_at = dt.Rows[j][13].ToString().Trim() == String.Empty ? "" : dt.Rows[j][13].ToString().Trim();
            objU1Imxls.issued_date = dt.Rows[j][14].ToString().Trim();
            objU1Imxls.passport_exp_date = dt.Rows[j][15].ToString().Trim();
            objU1Imxls.work_permit_no = dt.Rows[j][16].ToString().Trim() == String.Empty ? "" : dt.Rows[j][16].ToString().Trim();
            objU1Imxls.work_permit_issued_at = dt.Rows[j][17].ToString().Trim() == String.Empty ? "" : dt.Rows[j][17].ToString().Trim();
            objU1Imxls.work_permit_issued_date = dt.Rows[j][18].ToString().Trim();
            objU1Imxls.work_permit_exp_date = dt.Rows[j][19].ToString().Trim();
            objU1Imxls.social_id = dt.Rows[j][20].ToString().Trim() == String.Empty ? "" : dt.Rows[j][20].ToString().Trim();
            objU1Imxls.account_no = dt.Rows[j][21].ToString().Trim() == String.Empty ? "" : dt.Rows[j][21].ToString().Trim();
            objU1Imxls.nationality = dt.Rows[j][22].ToString().Trim();
            objU1Imxls.race = dt.Rows[j][23].ToString().Trim();
            objU1Imxls.religion = dt.Rows[j][24].ToString().Trim();
            dataImxls.imxls_myimxls_action[0] = objU1Imxls;
            //test.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataImxls));
            _local_xml = servExec.actionExec(masConn, "data_imxls", imxlsService, dataImxls, Constants.CREATE);
            dataImxls = (data_imxls)_funcTool.convertXmlToObject(typeof(data_imxls), _local_xml);
            j++;
            //}
            if (j < dt.Rows.Count)
            {
               idxCreated += dataImxls.return_id_added + ",";
            }
            else
            {
               idxCreated += dataImxls.return_id_added;
            }
         }
      }
      string[] idxCreatedSplit = idxCreated.Split(',');
      imxls objU0ImxlsUpdate = new imxls();
      dataImxls.imxls_myimxls_action = new imxls[1];
      objU0ImxlsUpdate.u0_imxls_idx = int.Parse(insertedU0Idx);
      objU0ImxlsUpdate.imxls_rows = j - 1;
      dataImxls.imxls_myimxls_action[0] = objU0ImxlsUpdate;
      servExec.actionExec(masConn, "data_imxls", imxlsService, dataImxls, Constants.UPDATE_ROWS);
      if (int.Parse(idxCreatedSplit.Length.ToString()) > 0)
      {
         actionInformationDistribute(idxCreated);
      }
   }

   protected void actionInformationDistribute(string idxCreated)
   {
      imxlsdistribute objImxlsDistribute = new imxlsdistribute();
      dataImxls.imxls_distribute_action = new imxlsdistribute[1];
      objImxlsDistribute.idx_added = idxCreated;
      dataImxls.imxls_distribute_action[0] = objImxlsDistribute;
      servExec.actionExec(masConn, "data_imxls", imxlsDistributeService, dataImxls, Constants.SELECT_WHERE);
   }
   #endregion Action

   #region Custom functions
   protected int getEmpProfile()
   {
      imxls objU0Imxls = new imxls();
      dataImxls.imxls_myimxls_action = new imxls[1];
      objU0Imxls.EmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
      dataImxls.imxls_myimxls_action[0] = objU0Imxls;
      _local_xml = servExec.actionExec(masConn, "data_imxls", imxlsService, dataImxls, Constants.SELECT_EMP_PROFILE);
      dataImxls = (data_imxls)_funcTool.convertXmlToObject(typeof(data_imxls), _local_xml);
      return dataImxls.imxls_myimxls_action[0].RSecIDX;
   }

   protected void getNotRulesMessage()
   {
      fileArea.Visible = false;
      gvXls.Visible = false;
      gvXlsWhere.Visible = false;
      btnBack.Visible = false;
      notRules.Visible = true;
      notRules.Text = "คุณไม่มีสิทธิ์ใช้งานในส่วนนี้";
   }

   protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
   {
      var GvName = (GridView)sender;
      switch (GvName.ID)
      {
         case "gvXlsWhere":
            gvXlsWhere.PageIndex = e.NewPageIndex;
            gvXlsWhere.DataBind();
            actionRead(int.Parse(ViewState["u0IdxRef"].ToString()));
            break;
      }
   }

   protected void btnTrigger(Button btnID)
   {
      UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
      UpdatePanelControlTrigger trigger = new PostBackTrigger();
      trigger.ControlID = btnID.UniqueID;
      updatePanel.Triggers.Add(trigger);
   }

   protected void setGridData(GridView gvName, Object obj)
   {
      gvName.DataSource = obj;
      gvName.DataBind();
   }
   #endregion Custom functions
}
