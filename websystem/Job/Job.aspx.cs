﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Drawing;

using System.IO;
using System.Configuration;
using System.Data.SqlClient;

using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Net.Mail;

public partial class WebSystem_Sir_TEST_Default3 : System.Web.UI.Page
{
    private Random random;
    private int countdown;
    private int remainMin;
    private int remainSec;

    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute _service_execute = new service_execute();
    data_job _data_job = new data_job();

    //DBConn DBConn = new DBConn();
    //private FunctionWeb _functionWeb = new FunctionWeb();
    private string JOBActionAc = "JOB";
    private string _jobs = "jobs";
    private string BoxXML;
    string localXml = String.Empty;

    //string _iBookingConn = "iBookingConn";
    //int _actionType = 0;
    //string _local_xml = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Select_tbposition1();
            Select_tbposition2();
            Select_ddlocationstart();
            Select_ddnationality();
            Select_ddrace();
            Select_ddblood();
            Select_ddreligon();
            Select_ddsex();
            //Select_dd_txtage();
            Select_ddahemother();
            Select_ddageparent1();
            Select_education();
            Select_ddprovinceeducation();
            Select_ddcompleted();
            //Select_ddpositionbusiness();
            //Select_ddpositionpersons();
            Select_ddadvise();
            Select_ddworkcompany();
            Select_ddcriminal();
            Select_ddallergy();
            Select_dddisease();
            Select_ddsick();
            Select_ddPrefix();
            Select_ddPrefixEng();
            Select_ddCountry();
            Select_dd_Y();
            Select_dd_Y_form();
            Select_dd_Y_to();
            Select_tbotherLanguage();
            Select_ddTypesOfCar();
            Select_TypeDoc();
            Select_Faculty();
            Select_ddAmphurstart();
            Select_DistrictStart();

            Label HChoinLoad = (Label)FormView11.FindControl("HChoin");
            HChoinLoad.Text = "1";
            Sum.Text = "0";
            ViewState["Num"] = 0;
            ViewState["JenCh"] = "0";
            ViewState["JenAgeCh"] = "0";

            #region Query String
            if (Request.QueryString["position1"] != null && Request.QueryString["position2"] != null && Request.QueryString["Monney"] != null && Request.QueryString["Prefix"] != null && Request.QueryString["name"] != null && Request.QueryString["last"] != null && Request.QueryString["Email"] != null && Request.QueryString["number"] != null && Request.QueryString["Jen"] != null && Request.QueryString["JenAge"] != null)
            {
                ViewState["po1"] = Request.QueryString["position1"];
                ViewState["po2"] = Request.QueryString["position2"];
                ViewState["mon"] = Request.QueryString["Monney"];
                ViewState["prefix"] = Request.QueryString["Prefix"];
                ViewState["name"] = Request.QueryString["name"];
                ViewState["last"] = Request.QueryString["last"];
                ViewState["email"] = Request.QueryString["Email"];
                ViewState["telephone"] = Request.QueryString["number"];
                ViewState["Jen"] = Request.QueryString["Jen"];
                ViewState["JenAge"] = Request.QueryString["JenAge"];

                string po1 = _funcTool.getDecryptRC4(ViewState["po1"].ToString(), "url");
                string po2 = _funcTool.getDecryptRC4(ViewState["po2"].ToString(), "url");
                string mon = _funcTool.getDecryptRC4(ViewState["mon"].ToString(), "url");
                string prefix = _funcTool.getDecryptRC4(ViewState["prefix"].ToString(), "url");
                string name = _funcTool.getDecryptRC4(ViewState["name"].ToString(), "url");
                string last = _funcTool.getDecryptRC4(ViewState["last"].ToString(), "url");
                string email = _funcTool.getDecryptRC4(ViewState["email"].ToString(), "url");
                string telephone = _funcTool.getDecryptRC4(ViewState["telephone"].ToString(), "url");
                string Jen = _funcTool.getDecryptRC4(ViewState["Jen"].ToString(), "url");
                string JenAge = _funcTool.getDecryptRC4(ViewState["JenAge"].ToString(), "url");
                ViewState["JenCh"] = Jen;
                ViewState["JenAgeCh"] = JenAge;

                
                int dateyear = int.Parse(DateTime.Now.ToString("yyyy")) - 543;
                string sMonth = dateyear.ToString() + DateTime.Now.ToString("MMddHHmmss") + ViewState["JenAgeCh"].ToString().Substring(14);
                ViewState["SumdateEnd"] = sMonth.Substring(10, 6);
                ViewState["Sumdate"] = ViewState["JenAgeCh"].ToString().Substring(10, 6);
                //text.Text = ViewState["JenCh"].ToString();
                //Literal1.Text = ViewState["JenAgeCh"].ToString();
                //test.Text = ViewState["Sumdate"].ToString();
                //Literal2.Text = ViewState["SumdateEnd"].ToString();
                /////////////////////////////////////////////////////////////////////////////////////////////////
                //JenCode
                ShowLink();
                /////////////////////////////////////////////////////////////////////////////////////////////////
                tbposition1.SelectedValue = po1;
                tbposition2.SelectedValue = po2;
                if (prefix.ToString() == "1")
                {
                    ddsex.SelectedValue = "1";
                    hideMilitary_Service.Visible = true;
                }
                else
                {
                    ddsex.SelectedValue = "2";
                    hideMilitary_Service.Visible = false;
                    Military_Service.SelectedValue = "3";
                }
                tbmoney.Text = mon;
                ddPrefix.SelectedValue = prefix;
                ddPrefixEng.SelectedValue = prefix;
                //tbname.Text = name;
                //tblastname.Text = last;
                tbnumberphone.Text = telephone;
                tbemail.Text = email;
            }
            #endregion
        }
        Select_ddlocationHere();
    }

    #region linkครั้งเดียว
    protected void ShowLink()
    {
        Insert_Genlink_list GetGenLink = new Insert_Genlink_list();
        _data_job.Insert_Genlink = new Insert_Genlink_list[1];
        GetGenLink.JenCode = ViewState["JenCh"].ToString();
        _data_job.Insert_Genlink[0] = GetGenLink;

        data_job Get_GenlinkOut = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 85));
        ViewState["Chlink"] = Get_GenlinkOut.Insert_Genlink[0].status_verify;

        if(ViewState["Chlink"].ToString() != "1")
        {
            ChLink.Visible = true;
            EndAge.Visible = false;
        }
        if (ViewState["Chlink"].ToString() == "1")
        {
            ChLink.Visible = false;
            EndAge.Visible = true;
        }
        if (int.Parse(ViewState["Sumdate"].ToString()) > float.Parse(ViewState["SumdateEnd"].ToString()))
        {
            ChLink.Visible = true;
            EndAge.Visible = false;
        }
        if (float.Parse(ViewState["Sumdate"].ToString()) < float.Parse(ViewState["SumdateEnd"].ToString()))
        {
            ChLink.Visible = false;
            EndAge.Visible = true;
        }

    }
    #endregion

    #region bind data
    protected void Showfile()
    {
        GridView gvDocumentFile = (GridView)FormView8.FindControl("gvDocumentFile");

        Insert_FileDoc_list SelectIfileDoc = new Insert_FileDoc_list();
        _data_job.Insert_FileDoc = new Insert_FileDoc_list[1];
        SelectIfileDoc.EmpIDXFile = int.Parse(Session["idDocfile"].ToString());
        _data_job.Insert_FileDoc[0] = SelectIfileDoc;

        data_job EmpIdFileDoc = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 42));

        //text.Text = HttpUtility.HtmlEncode(_functionWeb.ConvertObjectToXml(EmpIdFileDoc));
        gvDocumentFile.DataSource = EmpIdFileDoc.Insert_FileDoc;
        gvDocumentFile.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
 
    protected void Select_tabel_education()
    {
        var ds_Education = new DataSet();
        ds_Education.Tables.Add("Education");

        ds_Education.Tables[0].Columns.Add("tbinstitute", typeof(String));
        ds_Education.Tables[0].Columns.Add("ddeducation", typeof(String));
        ds_Education.Tables[0].Columns.Add("ddprovinceeducation", typeof(String));
        ds_Education.Tables[0].Columns.Add("Addyearattendedstart", typeof(String));
        ds_Education.Tables[0].Columns.Add("tbtoattended", typeof(String));
        ds_Education.Tables[0].Columns.Add("tbcourse", typeof(String));
        ds_Education.Tables[0].Columns.Add("ddcompleted", typeof(String));
        ds_Education.Tables[0].Columns.Add("tbGpa", typeof(String));
        ds_Education.Tables[0].Columns.Add("ddFaculty", typeof(String));
        ds_Education.Tables[0].Columns.Add("ddeducation_txt", typeof(String));
        ds_Education.Tables[0].Columns.Add("ddprovinceeducation_txt", typeof(String));
        ds_Education.Tables[0].Columns.Add("ddcompleted_txt", typeof(String));
        ds_Education.Tables[0].Columns.Add("ddFaculty_txt", typeof(String));
        if (ViewState["vsBuyds_Education"] == null)
        {
            ViewState["vsBuyds_Education"] = ds_Education;
        }
    }

    protected void Select_tabel_parent()
    {
        var dsEquipment = new DataSet();
        dsEquipment.Tables.Add("Equipment");

        dsEquipment.Tables[0].Columns.Add("tbnameparent1", typeof(String));
        dsEquipment.Tables[0].Columns.Add("tblastnameparent1", typeof(String));
        dsEquipment.Tables[0].Columns.Add("ddageparent1", typeof(String));
        dsEquipment.Tables[0].Columns.Add("tboccupationparent1", typeof(String));
        dsEquipment.Tables[0].Columns.Add("tbfirmaddressparent1", typeof(String));
        dsEquipment.Tables[0].Columns.Add("tbnumberparent1", typeof(String));

        if (ViewState["vsBuyequipment"] == null)
        {
            ViewState["vsBuyequipment"] = dsEquipment;

        }
    }

    protected void Select_tabel_Employed()
    {
        var dsEmployed = new DataSet();
        dsEmployed.Tables.Add("Employed");

        dsEmployed.Tables[0].Columns.Add("tbnameemployed", typeof(String));
        dsEmployed.Tables[0].Columns.Add("tbtelbusiness", typeof(String));
        dsEmployed.Tables[0].Columns.Add("tbtypeBusiness", typeof(String));
        dsEmployed.Tables[0].Columns.Add("tbpositionbusiness", typeof(String));
        dsEmployed.Tables[0].Columns.Add("tbaddressbusiness", typeof(String));
        dsEmployed.Tables[0].Columns.Add("Addyearattendedstart_emp", typeof(String));
        dsEmployed.Tables[0].Columns.Add("tbtoattended_emp", typeof(String));
        dsEmployed.Tables[0].Columns.Add("tbsupervisor", typeof(String));
        dsEmployed.Tables[0].Columns.Add("tbsalary_emp", typeof(String));
        dsEmployed.Tables[0].Columns.Add("tbrevenue", typeof(String));
        dsEmployed.Tables[0].Columns.Add("tbwork", typeof(String));
        dsEmployed.Tables[0].Columns.Add("ddpositionbusiness_txt", typeof(String));
        dsEmployed.Tables[0].Columns.Add("tbResignation", typeof(String));

        if (ViewState["vsBuyds_Employed"] == null)
        {
            ViewState["vsBuyds_Employed"] = dsEmployed;
        }
    }

    protected void Select_tabel_OtherLanguages()
    {
        var dsOtherLanguages = new DataSet();
        dsOtherLanguages.Tables.Add("OtherLanguages");

        dsOtherLanguages.Tables[0].Columns.Add("tbotherLanguage", typeof(String));
        dsOtherLanguages.Tables[0].Columns.Add("status_speaking_other", typeof(String));
        dsOtherLanguages.Tables[0].Columns.Add("status_Understanding_other", typeof(String));
        dsOtherLanguages.Tables[0].Columns.Add("status_Reading_other", typeof(String));
        dsOtherLanguages.Tables[0].Columns.Add("status_Wrinting_other", typeof(String));
        dsOtherLanguages.Tables[0].Columns.Add("status_speaking_other_txt", typeof(String));
        dsOtherLanguages.Tables[0].Columns.Add("status_Understanding_other_txt", typeof(String));
        dsOtherLanguages.Tables[0].Columns.Add("status_Reading_other_txt", typeof(String));
        dsOtherLanguages.Tables[0].Columns.Add("status_Wrinting_other_txt", typeof(String));
        dsOtherLanguages.Tables[0].Columns.Add("tbotherLanguage_txt", typeof(String));

        if (ViewState["vsBuyds_dsOtherLanguages"] == null)
        {
            ViewState["vsBuyds_dsOtherLanguages"] = dsOtherLanguages;
        }
    }

    protected void Select_tabel_TypeCar()
    {
        var dsTypeCar = new DataSet();
        dsTypeCar.Tables.Add("TypeCar");

        dsTypeCar.Tables[0].Columns.Add("ddTypesOfCar", typeof(String));
        dsTypeCar.Tables[0].Columns.Add("tbdriving", typeof(String));
        dsTypeCar.Tables[0].Columns.Add("ddTypesOfCar_txt", typeof(String));
  
        if (ViewState["vsBuyds_dsTypeCar"] == null)
        {
            ViewState["vsBuyds_dsTypeCar"] = dsTypeCar;
        }
    }

    #endregion bind data

    #region Insert/Update SQL
    protected void Insert_Master_List()
    {
        #region เก็บข้อมูลหลัก Employee
        MainEmployee_insert_list AddMainEmp = new MainEmployee_insert_list();
        _data_job.MainEmployee_insert = new MainEmployee_insert_list[1];
        AddMainEmp.SexIDX = int.Parse(ViewState["ddsex"].ToString()); //
        AddMainEmp.PrefixIDX = int.Parse(ViewState["ddPrefix"].ToString()); //
        AddMainEmp.PrefixEngIDX = int.Parse(ViewState["ddPrefixEng"].ToString()); //
        AddMainEmp.FirstNameTH = ViewState["tbname"].ToString(); //
        AddMainEmp.LastNameTH = ViewState["tblastname"].ToString(); //
        AddMainEmp.FirstNameEN = ViewState["tbnameeng"].ToString(); //
        AddMainEmp.LastNameEN = ViewState["tblastnameeng"].ToString(); //
        if (ViewState["tbnumberhome"] != null && ViewState["tbnumberhome"].ToString() != "")
        {
            AddMainEmp.PhoneNo = ViewState["tbnumberhome"].ToString(); //
        }
        else
        {
            AddMainEmp.PhoneNo = "-";
        }
        AddMainEmp.MobileNo = ViewState["tbnumberphone"].ToString(); //
        AddMainEmp.Birthday = ViewState["Addbirthday"].ToString(); //
        AddMainEmp.NatIDX = int.Parse(ViewState["ddnationality"].ToString()); //
        AddMainEmp.RaceIDX = int.Parse(ViewState["ddrace"].ToString()); //
        AddMainEmp.RelIDX = int.Parse(ViewState["ddreligon"].ToString()); //
        AddMainEmp.EmpAddress = ViewState["tbperaddress"].ToString(); //
        AddMainEmp.DistIDX = int.Parse(ViewState["ddDistrict"].ToString()); //
        AddMainEmp.AmpIDX = int.Parse(ViewState["ddAmphur"].ToString()); //
        AddMainEmp.ProvIDX = int.Parse(ViewState["ddlocationHere"].ToString()); //
        AddMainEmp.PostCode = ViewState["Postcode"].ToString(); //
        AddMainEmp.CountryIDX = int.Parse(ViewState["ddCountry"].ToString()); //
        AddMainEmp.MarriedStatus = int.Parse(ViewState["Marital_Status"].ToString()); //
        AddMainEmp.InhabitStatus = int.Parse(ViewState["Living_status"].ToString()); //
        AddMainEmp.ProvBirthIDX = int.Parse(ViewState["ddlocationstart"].ToString()); //
        if (ViewState["tbidcard"] != null && ViewState["tbidcard"].ToString() != "")
        {
            AddMainEmp.CardID = ViewState["tbidcard"].ToString(); //
        }
        else
        {
            AddMainEmp.CardID = "-";
        }
        if (ViewState["tbissued_at"] != null && ViewState["tbissued_at"].ToString() != "")
        {
            AddMainEmp.IssuedAtCardID = ViewState["tbissued_at"].ToString(); //
        }
        else
        {
            AddMainEmp.IssuedAtCardID = "-";
        }
        if (ViewState["tbissued_date"] != null && ViewState["tbissued_date"].ToString() != "")
        {
            AddMainEmp.IssuedDateCardID = ViewState["tbissued_date"].ToString(); //
        }
        else
        {
            var smallDateTimeMin = "1900-01-01 00:00:00";
            AddMainEmp.IssuedDateCardID = smallDateTimeMin;
        }
        if (ViewState["tbexpired_date"] != null && ViewState["tbexpired_date"].ToString() != "")
        {
            AddMainEmp.ExpiredDateCardID = ViewState["tbexpired_date"].ToString(); //
        }
        else
        {
            var smallDateTimeMin = "1900-01-01 00:00:00";
            AddMainEmp.ExpiredDateCardID = smallDateTimeMin;
        }
        if (ViewState["tbsocialcard"] != null && ViewState["tbsocialcard"].ToString() != "")
        {
            AddMainEmp.SecurrityCard = ViewState["tbsocialcard"].ToString(); //
        }
        else
        {
            AddMainEmp.SecurrityCard = "-";
        }
        if (ViewState["tbissued_atsocial"] != null && ViewState["tbissued_atsocial"].ToString() != "")
        {
            AddMainEmp.IssuedAtSecurrity = ViewState["tbissued_atsocial"].ToString(); //
        }
        else
        {
            AddMainEmp.IssuedAtSecurrity = "-";
        }
        if (ViewState["tbexpired_date_social"] != null && ViewState["tbexpired_date_social"].ToString() != "")
        {
            AddMainEmp.ExpiredDateSecurrity = ViewState["tbexpired_date_social"].ToString();
        }
        else
        {
            var smallDateTimeMin = "1900-01-01 00:00:00";
            AddMainEmp.ExpiredDateSecurrity = smallDateTimeMin;
        }
        if (ViewState["tbtaxcard"] != null && ViewState["tbtaxcard"].ToString() != "")
        {
            AddMainEmp.TaxCardID = ViewState["tbtaxcard"].ToString(); //
        }
        else
        {
            AddMainEmp.TaxCardID = "-";
        }
        if (ViewState["tbissued_tax"] != null && ViewState["tbissued_tax"].ToString() != "")
        {
            AddMainEmp.IssuedAtTax = ViewState["tbissued_tax"].ToString(); //
        }
        else
        {
            AddMainEmp.IssuedAtTax = "-";
        }
        AddMainEmp.PresentAddress = ViewState["tbaddresspre"].ToString(); //
        AddMainEmp.MilitaryService = int.Parse(ViewState["Military_Service"].ToString()); //
        if (ViewState["tbchildren"] != null && ViewState["tbchildren"].ToString() != "")
        {
            AddMainEmp.Children = ViewState["tbchildren"].ToString(); //
        }
        else
        {
            AddMainEmp.Children = "-";
        }
        if (ViewState["tbchildrenschool"] != null && ViewState["tbchildrenschool"].ToString() != "")
        {
            AddMainEmp.ChildrenSchool = ViewState["tbchildrenschool"].ToString(); //
        }
        else
        {
            AddMainEmp.ChildrenSchool = "-";
        }
        if (ViewState["tbchildren21"] != null && ViewState["tbchildren21"].ToString() != "")
        {
            AddMainEmp.Children21 = ViewState["tbchildren21"].ToString(); //
        }
        else
        {
            AddMainEmp.Children21 = "-";
        }
        AddMainEmp.NickNameTH = ViewState["tbNickname"].ToString();
        AddMainEmp.NickNameEN = ViewState["tbNickEng"].ToString();
        AddMainEmp.Email = ViewState["tbemail"].ToString();
        _data_job.MainEmployee_insert[0] = AddMainEmp;

        data_job add_main = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 25));
        //ViewState["idImage"] = add_main.return_code;
        Session["idImage"] = add_main.return_code;
        //ViewState["idDocfile"] = add_main.return_code;
        Session["idDocfile"] = add_main.return_code;
        Session["idLogic"] = add_main.return_code;
        ViewState["Emp_id"] = add_main.return_code;
        Session["Emp_id"] = add_main.return_code;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_job));
        #endregion

        #region ความสามารถพิเศษ
        Insert_Talent_list AddTalent = new Insert_Talent_list();
        _data_job.Insert_Talent = new Insert_Talent_list[1];
        if (ViewState["tbcom_pro"] != null && ViewState["tbcom_pro"].ToString() != "")
        {
            AddTalent.Computer = ViewState["tbcom_pro"].ToString();
        }
        else
        {
            AddTalent.Computer = "-";
        }
        if (ViewState["tbtypingth"] != null && ViewState["tbtypingth"].ToString() != "")
        {
            AddTalent.TypingTH = ViewState["tbtypingth"].ToString();
        }
        else
        {
            AddTalent.TypingTH = "-";
        }
        if (ViewState["tbtypingeng"] != null && ViewState["tbtypingeng"].ToString() != "")
        {
            AddTalent.TypingENG = ViewState["tbtypingeng"].ToString();
        }
        else
        {
            AddTalent.TypingENG = "-";
        }
        AddTalent.EmpIDX11 = int.Parse(ViewState["Emp_id"].ToString());
        _data_job.Insert_Talent[0] = AddTalent;


        data_job add_Talent = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 53));
        //text.Text = HttpUtility.HtmlEncode(_funcTool.ConvertObjectToXml(_data_job));
        #endregion

        #region อื่นๆ
        Insert_Other_list AddOther = new Insert_Other_list();
        _data_job.Insert_Other = new Insert_Other_list[1];
        AddOther.KnowGet = int.Parse(ViewState["ddadvise"].ToString());
        AddOther.BeenRegistered = int.Parse(ViewState["ddworkcompany"].ToString());
        AddOther.HadCaptured = int.Parse(ViewState["ddcriminal"].ToString());
        AddOther.DrugAllergy = int.Parse(ViewState["ddallergy"].ToString());
        AddOther.Disease = int.Parse(ViewState["dddisease"].ToString());
        AddOther.HadSurgery = int.Parse(ViewState["ddsick"].ToString());
        AddOther.EmergencyFL = ViewState["tbaccident"].ToString();
        AddOther.RelationEmergency = ViewState["tbrelation"].ToString();
        AddOther.TelephoneEmergency = ViewState["tbtelaccident"].ToString();
        if (ViewState["tbpersoncompany"] != null && ViewState["tbpersoncompany"].ToString() != "")
        {
            AddOther.KnowSomeone = ViewState["tbpersoncompany"].ToString();
        }
        else
        {
            AddOther.KnowSomeone = "-";
        }
        if (ViewState["tbrelationpersoncompany"] != null && ViewState["tbrelationpersoncompany"].ToString() != "")
        {
            AddOther.RelationKS = ViewState["tbrelationpersoncompany"].ToString();
        }
        else
        {
            AddOther.RelationKS = "-";
        }
        AddOther.EmpIDX10 = int.Parse(ViewState["Emp_id"].ToString());
        AddOther.tbaddresspreAccident = ViewState["tbaddresspreAccident"].ToString();
        //tbaddresspreAccident
        _data_job.Insert_Other[0] = AddOther;


        data_job add_Other = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 52));
        //text.Text = HttpUtility.HtmlEncode(_functionWeb.ConvertObjectToXml(_data_job));
        #endregion

        #region คนรู้จักที่ไม่ใช่ญาติ
        Insert_NotParent_list AddNotParent = new Insert_NotParent_list();
        _data_job.Insert_NotParent = new Insert_NotParent_list[1];
        AddNotParent.FirstNameNP = ViewState["tbnamepersons"].ToString();
        AddNotParent.LastNameNP = ViewState["tblastnamepersons"].ToString();
        AddNotParent.RelationshipNP = ViewState["tbrelationship"].ToString();
        if (ViewState["tbfirmaddresspersons"] != null && ViewState["tbfirmaddresspersons"].ToString() != "")
        {
            AddNotParent.FirmAddressNP = ViewState["tbfirmaddresspersons"].ToString();
        }
        else
        {
            AddNotParent.FirmAddressNP = "-";
        }
        if (ViewState["tbpositionpersons"] != null && ViewState["tbpositionpersons"].ToString() != "")
        {
            AddNotParent.PositionNP = ViewState["tbpositionpersons"].ToString();
        }
        else
        {
            AddNotParent.PositionNP = "-";
        }
        AddNotParent.TelNP = ViewState["tbtelpersons"].ToString();
        AddNotParent.EmpIDX9 = int.Parse(ViewState["Emp_id"].ToString());
        _data_job.Insert_NotParent[0] = AddNotParent;


        data_job add_NotParent = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 51));
        //text.Text = HttpUtility.HtmlEncode(_functionWeb.ConvertObjectToXml(_data_job));
        #endregion

        #region พ่อมแม่
        Insert_MTFT_list AddMTFT = new Insert_MTFT_list();
        _data_job.Insert_MTFT = new Insert_MTFT_list[1];
        AddMTFT.FirstNameFather = ViewState["tbnamefather"].ToString();
        AddMTFT.LastNameFather = ViewState["tblastnamefather"].ToString();
        if (ViewState["ddage"].ToString() != "0")
        {
            AddMTFT.AgeFather = ViewState["ddage"].ToString();
        }
        else
        {
            AddMTFT.AgeFather = "-";
        }
        AddMTFT.OccupationFather = ViewState["tboccupationfather"].ToString();
        AddMTFT.LifeStatusFather = int.Parse(ViewState["statuslift"].ToString());
        AddMTFT.FirstNameMother = ViewState["tbnamemother"].ToString();
        AddMTFT.LastNameMother = ViewState["tblastnamemother"].ToString();
        if (ViewState["ddahemother"].ToString() != "0")
        {
            AddMTFT.AgeMother = ViewState["ddahemother"].ToString();
        }
        else
        {
            AddMTFT.AgeMother = "-";
        }
        AddMTFT.OccupationMother = ViewState["tboccupationmother"].ToString();
        AddMTFT.LifeStatusMother = int.Parse(ViewState["statusmother"].ToString());
        AddMTFT.EmpIDX8 = int.Parse(ViewState["Emp_id"].ToString());
        _data_job.Insert_MTFT[0] = AddMTFT;


        data_job add_MTFT = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 29));
        //text.Text = HttpUtility.HtmlEncode(_functionWeb.ConvertObjectToXml(_data_job));
        #endregion

        #region ถ้าแต่งงาน
        Insert_ifMarried_list AddIfMarried = new Insert_ifMarried_list();
        _data_job.Insert_ifMarried = new Insert_ifMarried_list[1];
        if (ViewState["spouseincome"] != null && ViewState["spouseincome"].ToString() != "")
        {
            AddIfMarried.SpouseMN = int.Parse(ViewState["spouseincome"].ToString());
        }
        else
        {
            AddIfMarried.SpouseMN = 0;
        }
        if (ViewState["tbnamespouse"] != null && ViewState["tbnamespouse"].ToString() != "")
        {
            AddIfMarried.FirstNameSpouse = ViewState["tbnamespouse"].ToString();
        }
        else
        {
            AddIfMarried.FirstNameSpouse = "ไม่มี";
        }
        if (ViewState["tblastnamespouse"] != null && ViewState["tblastnamespouse"].ToString() != "")
        {
            AddIfMarried.LastNameSpouse = ViewState["tblastnamespouse"].ToString();
        }
        else
        {
            AddIfMarried.LastNameSpouse = "ไม่มี";
        }
        if (ViewState["tboccupation"] != null && ViewState["tboccupation"].ToString() != "")
        {
            AddIfMarried.OccupationSpouse = ViewState["tboccupation"].ToString();
        }
        else
        {
            AddIfMarried.OccupationSpouse = "ไม่มี";
        }
        if (ViewState["tbfirmaddress"] != null && ViewState["tbfirmaddress"].ToString() != "")
        {
            AddIfMarried.FirmAddressS = ViewState["tbfirmaddress"].ToString();
        }
        else
        {
            AddIfMarried.FirmAddressS = "ไม่มี";
        }
        if (ViewState["If_Married"] != null && ViewState["If_Married"].ToString() != "")
        {
            AddIfMarried.RegisterM = int.Parse(ViewState["If_Married"].ToString());
        }
        else
        {
            AddIfMarried.RegisterM = 0;
        }
        AddIfMarried.EmpIDX7 = int.Parse(ViewState["Emp_id"].ToString());
        _data_job.Insert_ifMarried[0] = AddIfMarried;

        data_job add_IfMarried = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 28));
        //text.Text = HttpUtility.HtmlEncode(_functionWeb.ConvertObjectToXml(_data_job));
        #endregion

        #region ร่างกาย 
        Insert_Health_list AddHealth = new Insert_Health_list();
        _data_job.Insert_Health = new Insert_Health_list[1];
        AddHealth.Height = int.Parse(ViewState["tbheight"].ToString());
        AddHealth.Weight = int.Parse(ViewState["tbweight"].ToString());
        AddHealth.Scar = ViewState["tbscar"].ToString();
        AddHealth.BloodIDX = int.Parse(ViewState["ddblood"].ToString());
        AddHealth.EmpIDX6 = int.Parse(ViewState["Emp_id"].ToString());
        _data_job.Insert_Health[0] = AddHealth;

        data_job add_Health = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 27));
        //text.Text = HttpUtility.HtmlEncode(_functionWeb.ConvertObjectToXml(_data_job));
        #endregion

        #region ตำแหน่งงานที่ต้องการ
        BoxAddwentpositionlist u0Document1 = new BoxAddwentpositionlist();
        _data_job.BoxAddwentposition = new BoxAddwentpositionlist[1];
        u0Document1.tbposition1 = int.Parse(ViewState["tbposition1"].ToString());
        u0Document1.tbposition2 = int.Parse(ViewState["tbposition2"].ToString());
        u0Document1.tbmoney = ViewState["tbmoney"].ToString();
        u0Document1.AddStartdate = ViewState["AddStartdate"].ToString();
        u0Document1.EmpIDX0 = int.Parse(ViewState["Emp_id"].ToString());
        _data_job.BoxAddwentposition[0] = u0Document1;

        data_job add_po = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 21));
        #endregion

        #region พี่น้อง
        var dsBuyequipment = (DataSet)ViewState["vsBuyequipment"];

        var Add_Parent = new parents_list[dsBuyequipment.Tables[0].Rows.Count];
        int i = 0;

        if (dsBuyequipment.Tables[0].Rows.Count != 0)
        {
            foreach (DataRow dr in dsBuyequipment.Tables[0].Rows)
            {

                Add_Parent[i] = new parents_list();
                Add_Parent[i].EmpIDX1 = Int32.Parse(ViewState["Emp_id"].ToString());
                if (dr["tbnameparent1"] != null && dr["tbnameparent1"].ToString() != "")
                {
                    Add_Parent[i].tbnameparent1 = dr["tbnameparent1"].ToString();
                }
                else
                {
                    Add_Parent[i].tbnameparent1 = "-";
                }
                if (dr["tblastnameparent1"] != null && dr["tblastnameparent1"].ToString() != "")
                {
                    Add_Parent[i].tblastnameparent1 = dr["tblastnameparent1"].ToString();
                }
                else
                {
                    Add_Parent[i].tblastnameparent1 = "-";
                }
                if (dr["ddageparent1"] != null && dr["ddageparent1"].ToString() != "00")
                {
                    Add_Parent[i].ddageparent1 = dr["ddageparent1"].ToString();
                }
                else
                {
                    Add_Parent[i].ddageparent1 = "-";
                }
                if (dr["tboccupationparent1"] != null && dr["tboccupationparent1"].ToString() != "")
                {
                    Add_Parent[i].tboccupationparent1 = dr["tboccupationparent1"].ToString();
                }
                else
                {
                    Add_Parent[i].tboccupationparent1 = "-";
                }
                if (dr["tbfirmaddressparent1"] != null && dr["tbfirmaddressparent1"].ToString() != "")
                {
                    Add_Parent[i].tbfirmaddressparent1 = dr["tbfirmaddressparent1"].ToString();
                }
                else
                {
                    Add_Parent[i].tbfirmaddressparent1 = "-";
                }
                if (dr["tbnumberparent1"] != null && dr["tbnumberparent1"].ToString() != "")
                {
                    Add_Parent[i].tbnumberparent1 = dr["tbnumberparent1"].ToString();
                }
                else
                {
                    Add_Parent[i].tbnumberparent1 = "-";
                }

                i++;

            }
            data_job Box_Add_Parent = new data_job();
            Box_Add_Parent.parents = Add_Parent;

            //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Box_Add_Parent));
            data_job add_parent = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, Box_Add_Parent, 20));
        }
        else
        {
            parents_list Add_Parent2 = new parents_list();
            _data_job.parents = new parents_list[1];

            Add_Parent2.EmpIDX1 = Int32.Parse(ViewState["Emp_id"].ToString());
            Add_Parent2.tbnameparent1 = "-";
            Add_Parent2.tblastnameparent1 = ".";
            Add_Parent2.ddageparent1 = "-";
            Add_Parent2.tboccupationparent1 = "-";
            Add_Parent2.tbfirmaddressparent1 = "-";
            Add_Parent2.tbnumberparent1 = "-";

            _data_job.parents[0] = Add_Parent2;

            //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_job));
            data_job add_parent = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 20));
        }
        #endregion

        #region การศึกษา
        var drBuyEducation = (DataSet)ViewState["vsBuyds_Education"];

        var Add_Eduction = new education_insert_list[drBuyEducation.Tables[0].Rows.Count];
        int a = 0;

        if (drBuyEducation.Tables[0].Rows.Count != 0)
        {
            foreach (DataRow dr in drBuyEducation.Tables[0].Rows)
            {

                Add_Eduction[a] = new education_insert_list();
                Add_Eduction[a].EmpIDX2 = Int32.Parse(ViewState["Emp_id"].ToString());
                if (dr["tbinstitute"] != null && dr["tbinstitute"].ToString() != "")
                {
                    Add_Eduction[a].tbinstitute = dr["tbinstitute"].ToString();
                }
                else
                {
                    Add_Eduction[a].tbinstitute = "-";
                }
                if (dr["ddeducation"] != null && dr["ddeducation"].ToString() != "00")
                {
                    Add_Eduction[a].ddeducation = Int32.Parse(dr["ddeducation"].ToString());
                }
                else
                {
                    Add_Eduction[a].ddeducation = 0;
                }
                if (dr["ddprovinceeducation"] != null && dr["ddprovinceeducation"].ToString() != "00")
                {
                    Add_Eduction[a].ddprovinceeducation = Int32.Parse(dr["ddprovinceeducation"].ToString());
                }
                else
                {
                    Add_Eduction[a].ddprovinceeducation = 0;
                }
                if (dr["Addyearattendedstart"] != null && dr["Addyearattendedstart"].ToString() != "00")
                {
                    Add_Eduction[a].Addyearattendedstart = dr["Addyearattendedstart"].ToString();
                }
                else
                {
                    Add_Eduction[a].Addyearattendedstart = "-";
                }
                if (dr["tbtoattended"] != null && dr["tbtoattended"].ToString() != "00")
                {
                    Add_Eduction[a].tbtoattended = dr["tbtoattended"].ToString();
                }
                else
                {
                    Add_Eduction[a].tbtoattended = "-";
                }
                if (dr["tbcourse"] != null && dr["tbcourse"].ToString() != "00")
                {
                    Add_Eduction[a].tbcourse = dr["tbcourse"].ToString();
                }
                else
                {
                    Add_Eduction[a].tbcourse = "-";
                }
                if (dr["ddcompleted"] != null && dr["ddcompleted"].ToString() != "00")
                {
                    Add_Eduction[a].ddcompleted = Int32.Parse(dr["ddcompleted"].ToString());
                }
                else
                {
                    Add_Eduction[a].ddcompleted = 0;
                }
                if (dr["ddFaculty"] != null && dr["ddFaculty"].ToString() != "00")
                {
                    Add_Eduction[a].ddFaculty = Int32.Parse(dr["ddFaculty"].ToString());
                }
                else
                {
                    Add_Eduction[a].ddFaculty = 0;
                }
                if (dr["tbGpa"] != null && dr["tbGpa"].ToString() != "")
                {
                    Add_Eduction[a].tbGpa = dr["tbGpa"].ToString();
                }
                else
                {
                    Add_Eduction[a].tbGpa = "-";
                }

                a++;
            }
            data_job Box_Add_Education = new data_job();
            Box_Add_Education.education_insert = Add_Eduction;

            //text.Text = HttpUtility.HtmlEncode(_functionWeb.ConvertObjectToXml(Box_Add_Education));
            data_job add_Education = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, Box_Add_Education, 22));
        }
        else
        {
            education_insert_list Add_Eduction2 = new education_insert_list();
            _data_job.education_insert = new education_insert_list[1];

            Add_Eduction2.EmpIDX2 = Int32.Parse(ViewState["Emp_id"].ToString());
            Add_Eduction2.tbinstitute = "-";
            Add_Eduction2.ddeducation = 0;
            Add_Eduction2.ddprovinceeducation = 0;
            Add_Eduction2.Addyearattendedstart = "-";
            Add_Eduction2.tbtoattended = "-";
            Add_Eduction2.tbcourse = "-";
            Add_Eduction2.ddcompleted = 0;
            Add_Eduction2.ddFaculty = 0;
            Add_Eduction2.tbGpa = "-";

            _data_job.education_insert[0] = Add_Eduction2;

            //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_job));
            //data_job add_Edu = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 22));
        }

        #endregion

        #region ประวัติการทำงาน
        var drBuyEmployed = (DataSet)ViewState["vsBuyds_Employed"];

        var Add_Employed = new Employed_insert_list[drBuyEmployed.Tables[0].Rows.Count];
        int b = 0;

        if (drBuyEmployed.Tables[0].Rows.Count != 0)
        {
            foreach (DataRow dr in drBuyEmployed.Tables[0].Rows)
            {
                Add_Employed[b] = new Employed_insert_list();
                Add_Employed[b].EmpIDX3 = Int32.Parse(ViewState["Emp_id"].ToString());
                if (dr["tbnameemployed"] != null && dr["tbnameemployed"].ToString() != "")
                {
                    Add_Employed[b].tbnameemployed = dr["tbnameemployed"].ToString();
                }
                else
                {
                    Add_Employed[b].tbnameemployed = "-";
                }
                if (dr["tbtelbusiness"] != null && dr["tbtelbusiness"].ToString() != "")
                {
                    Add_Employed[b].tbtelbusiness = dr["tbtelbusiness"].ToString();
                }
                else
                {
                    Add_Employed[b].tbtelbusiness = "-";
                }
                if (dr["tbtypeBusiness"] != null && dr["tbtypeBusiness"].ToString() != "")
                {
                    Add_Employed[b].tbtypeBusiness = dr["tbtypeBusiness"].ToString();
                }
                else
                {
                    Add_Employed[b].tbtypeBusiness = "-";
                }
                if (dr["tbpositionbusiness"] != null && dr["tbpositionbusiness"].ToString() != "")
                {
                    Add_Employed[b].tbpositionbusiness = dr["tbpositionbusiness"].ToString();
                }
                else
                {
                    Add_Employed[b].tbpositionbusiness = "-";
                }
                if (dr["tbaddressbusiness"] != null && dr["tbaddressbusiness"].ToString() != "")
                {
                    Add_Employed[b].tbaddressbusiness = dr["tbaddressbusiness"].ToString();
                }
                else
                {
                    Add_Employed[b].tbaddressbusiness = "-";
                }
                if (dr["Addyearattendedstart_emp"] != null && dr["Addyearattendedstart_emp"].ToString() != "")
                {
                    #region แปลง Smalldate
                    string outputtextdatestart = dr["Addyearattendedstart_emp"].ToString().Substring(6) + "/" + dr["Addyearattendedstart_emp"].ToString().Substring(3, 2) + "/" + dr["Addyearattendedstart_emp"].ToString().Substring(0, 2);
                    #endregion
                    Add_Employed[b].Addyearattendedstart_emp = outputtextdatestart;
                }
                else
                {
                    Add_Employed[b].Addyearattendedstart_emp = "-";
                }
                if (dr["tbtoattended_emp"] != null && dr["tbtoattended_emp"].ToString() != "")
                {
                    #region แปลง Smalldate
                    string outputtexttoattended_emp = dr["tbtoattended_emp"].ToString().Substring(6) + "/" + dr["tbtoattended_emp"].ToString().Substring(3, 2) + "/" + dr["tbtoattended_emp"].ToString().Substring(0, 2);
                    #endregion
                    Add_Employed[b].tbtoattended_emp = outputtexttoattended_emp;
                }
                else
                {
                    Add_Employed[b].tbtoattended_emp = "-";
                }
                if (dr["tbsupervisor"] != null && dr["tbsupervisor"].ToString() != "")
                {
                    Add_Employed[b].tbsupervisor = dr["tbsupervisor"].ToString();
                }
                else
                {
                    Add_Employed[b].tbsupervisor = "-";
                }
                if (dr["tbsalary_emp"] != null && dr["tbsalary_emp"].ToString() != "")
                {
                    Add_Employed[b].tbsalary_emp = Int32.Parse(dr["tbsalary_emp"].ToString());
                }
                else
                {
                    Add_Employed[b].tbsalary_emp = 0;
                }
                if (dr["tbrevenue"] != null && dr["tbrevenue"].ToString() != "")
                {
                    Add_Employed[b].tbrevenue = Int32.Parse(dr["tbrevenue"].ToString());
                }
                else
                {
                    Add_Employed[b].tbrevenue = 0;
                }
                if (dr["tbwork"] != null && dr["tbwork"].ToString() != "")
                {
                    Add_Employed[b].tbwork = dr["tbwork"].ToString();
                }
                else
                {
                    Add_Employed[b].tbwork = "-";
                }
                if (dr["tbResignation"] != null && dr["tbResignation"].ToString() != "")
                {
                    Add_Employed[b].tbResignation = dr["tbResignation"].ToString();
                }
                else
                {
                    Add_Employed[b].tbResignation = "-";
                }

                b++;

            }
            data_job Box_Add_Employed = new data_job();
            Box_Add_Employed.Employed_insert = Add_Employed;

            //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Box_Add_Employed));
            data_job add_Employed_ = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, Box_Add_Employed, 23));
        }
        else
        {
            Employed_insert_list Add_Employed2 = new Employed_insert_list();
            _data_job.Employed_insert = new Employed_insert_list[1];

            Add_Employed2.EmpIDX3 = Int32.Parse(ViewState["Emp_id"].ToString());
            Add_Employed2.tbnameemployed = "-";
            Add_Employed2.tbtelbusiness = "-";
            Add_Employed2.tbtypeBusiness = "-";
            Add_Employed2.tbpositionbusiness = "-";
            Add_Employed2.tbaddressbusiness = "-";
            Add_Employed2.Addyearattendedstart_emp = "1900-01-01 00:00:00";
            Add_Employed2.tbtoattended_emp = "1900-01-01 00:00:00";
            Add_Employed2.tbsupervisor = "-";
            Add_Employed2.tbsalary_emp = 0;
            Add_Employed2.tbrevenue = 0;
            Add_Employed2.tbwork = "-";
            Add_Employed2.tbResignation = "-";

            _data_job.Employed_insert[0] = Add_Employed2;

            //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_job));
            //data_job add_history = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 23));
        }
        #endregion

        #region ภาษาอื่นๆ
        #region Thai
        Languages_insert_list AddLThaiEng = new Languages_insert_list();
        _data_job.Languages_insert = new Languages_insert_list[1];
        AddLThaiEng.EmpIDXTE = int.Parse(ViewState["Emp_id"].ToString());
        AddLThaiEng.tbotherLanguageTE = ViewState["LThai"].ToString();
        AddLThaiEng.status_speaking_otherTE = int.Parse(ViewState["LthaiSP"].ToString());
        AddLThaiEng.status_Understanding_otherTE = int.Parse(ViewState["LthaiUST"].ToString());
        AddLThaiEng.status_Reading_otherTE = int.Parse(ViewState["LthaiRD"].ToString());
        AddLThaiEng.status_Wrinting_otherTE = int.Parse(ViewState["LthaiWT"].ToString());
        _data_job.Languages_insert[0] = AddLThaiEng;

        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_job));
        data_job Add_LThaiEng = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 81));
        #endregion

        #region Eng
        Languages_insertEng_list AddLEng = new Languages_insertEng_list();
        _data_job.Languages_insertEng = new Languages_insertEng_list[1];
        AddLEng.EmpIDXEng = int.Parse(ViewState["Emp_id"].ToString());
        AddLEng.tbotherLanguageEng = ViewState["LEng"].ToString();
        AddLEng.status_speaking_otherEng = int.Parse(ViewState["LEngSP"].ToString());
        AddLEng.status_Understanding_otherEng = int.Parse(ViewState["LEngUST"].ToString());
        AddLEng.status_Reading_otherEng = int.Parse(ViewState["LEngRD"].ToString());
        AddLEng.status_Wrinting_otherEng = int.Parse(ViewState["LEngWT"].ToString());
        _data_job.Languages_insertEng[0] = AddLEng;

        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_job));
        data_job Add_LEng = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 82));
        #endregion

        #region DataSet
        var drBuyOtherLanguages = (DataSet)ViewState["vsBuyds_dsOtherLanguages"];

        var Add_OtherLanguages = new OtherLanguages_insert_list[drBuyOtherLanguages.Tables[0].Rows.Count];
        int c = 0;

        if (drBuyOtherLanguages.Tables[0].Rows.Count != 0)
        {
            foreach (DataRow dr in drBuyOtherLanguages.Tables[0].Rows)
            {

                Add_OtherLanguages[c] = new OtherLanguages_insert_list();
                Add_OtherLanguages[c].EmpIDX4 = Int32.Parse(ViewState["Emp_id"].ToString());
                Add_OtherLanguages[c].tbotherLanguage = Int32.Parse(dr["tbotherLanguage"].ToString());
                Add_OtherLanguages[c].status_speaking_other = Int32.Parse(dr["status_speaking_other"].ToString());
                Add_OtherLanguages[c].status_Understanding_other = Int32.Parse(dr["status_Understanding_other"].ToString());
                Add_OtherLanguages[c].status_Reading_other = Int32.Parse(dr["status_Reading_other"].ToString());
                Add_OtherLanguages[c].status_Wrinting_other = Int32.Parse(dr["status_Wrinting_other"].ToString());

                c++;
            }
            data_job Box_Add_OtherLanguages = new data_job();
            Box_Add_OtherLanguages.OtherLanguages_insert = Add_OtherLanguages;

            //text.Text = HttpUtility.HtmlEncode(_functionWeb.ConvertObjectToXml(Box_Add_OtherLanguages));
            data_job add_OtherLanguages_ = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, Box_Add_OtherLanguages, 24));
        }
        #endregion
        #endregion

        #region รถ
        var drBuyTypeCar = (DataSet)ViewState["vsBuyds_dsTypeCar"];

        var Add_TypeCar = new TypesOfCar_list[drBuyTypeCar.Tables[0].Rows.Count];
        int d = 0;

        if (drBuyTypeCar.Tables[0].Rows.Count != 0)
        {
            foreach (DataRow dr in drBuyTypeCar.Tables[0].Rows)
            {

                Add_TypeCar[d] = new TypesOfCar_list();
                Add_TypeCar[d].EmpIDX5 = Int32.Parse(ViewState["Emp_id"].ToString());
                if (dr["ddTypesOfCar"] != null && dr["ddTypesOfCar"].ToString() != "00")
                {
                    Add_TypeCar[d].ddTypesOfCar = dr["ddTypesOfCar"].ToString();
                }
                else
                {
                    Add_TypeCar[d].ddTypesOfCar = "-";
                }
                if (dr["tbdriving"] != null && dr["tbdriving"].ToString() != "")
                {
                    Add_TypeCar[d].tbdriving = dr["tbdriving"].ToString();
                }
                else
                {
                    Add_TypeCar[d].tbdriving = "-";
                }

                d++;

            }
            data_job Box_Add_TypeCar = new data_job();
            Box_Add_TypeCar.TypesOfCar = Add_TypeCar;

            //text.Text = HttpUtility.HtmlEncode(_functionWeb.ConvertObjectToXml(Box_Add_TypeCar));
            data_job add_OtherLanguages_ = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, Box_Add_TypeCar, 26));
        }
        else
        {
            TypesOfCar_list Add_TypeCar2 = new TypesOfCar_list();
            _data_job.TypesOfCar = new TypesOfCar_list[1];

            Add_TypeCar2.EmpIDX5 = Int32.Parse(ViewState["Emp_id"].ToString());
            Add_TypeCar2.ddTypesOfCar = "-";
            Add_TypeCar2.tbdriving = "-";

            _data_job.TypesOfCar[0] = Add_TypeCar2;

            //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_job));
            //data_job add_parent = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 26));
        }
        #endregion
    }
    #endregion

    #region TextChnag
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        var tbName = (TextBox)sender;
        switch (tbName.ID)
        {
            case "tbmoney":
                string d = string.Format("{0:#,##0}", double.Parse(tbmoney.Text));
                tbmoney.Text = d;
                break;
            case "tbidcard":
                if (VerifyPeopleID(tbidcard.Text))
                {
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('รหัสบัตรประชาชนไม่ถูกต้อง');", true);
                }
                break;
        }
    }
    #endregion
    
    #region ตรวจสอบบัตรประชาชน
    private Boolean VerifyPeopleID(String PID)//เชคเลขบัตรประชาชน
    {
        string digit = null;
        //ตรวจสอบว่าทุก ๆ ตัวอักษรเป็นตัวเลข
        string[] str1 = PID.Split("-".ToCharArray());
        ViewState["cardId"] = str1[0] + str1[1] + str1[2] + str1[3] + str1[4];
        //if (str1.ToCharArray().All(c => char.IsNumber(c)) == false)
        //    return false;
        //ตรวจสอบว่าข้อมูลมีทั้งหมด 13 ตัวอักษร
        //if (PID.Trim().Length != 13)
        //    return false;
        int sumValue = 0;
        for (int i = 0; i < ViewState["cardId"].ToString().Length - 1; i++)
            sumValue += int.Parse(ViewState["cardId"].ToString()[i].ToString()) * (13 - i);
        int v = 11 - (sumValue % 11);
        if (v.ToString().Length == 2)
        {
            digit = v.ToString().Substring(1, 1);
        }
        else
        {
            digit = v.ToString();
        }
        return ViewState["cardId"].ToString()[12].ToString() == digit;
    }
    #endregion

    #region ดึงจาก data master มาใน Dropdown
    protected void Select_Faculty()
    {
        DropDownList ddFaculty = (DropDownList)FormView3.FindControl("ddFaculty");
        ddFaculty.AppendDataBoundItems = true;
        ddFaculty.Items.Add(new ListItem("กรุณาเลือกคณะ", "00"));

        Select_Faculty_list selectFaculty = new Select_Faculty_list();
        _data_job.Select_Faculty = new Select_Faculty_list[1];
        _data_job.Select_Faculty[0] = selectFaculty;


        data_job ddselectFaculty = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 71));
        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ddselectFaculty));
        ddFaculty.DataSource = ddselectFaculty.Select_Faculty;
        ddFaculty.DataTextField = "Faculty_Name";
        ddFaculty.DataValueField = "FacIDX";
        ddFaculty.DataBind();
    }

    protected void Select_TypeDoc()
    {
        DropDownList DDTypeDoc = (DropDownList)FormView8.FindControl("DDTypeDoc");
        DDTypeDoc.AppendDataBoundItems = true;
        DDTypeDoc.Items.Add(new ListItem("เลือกประเภทเอกสาร", "00"));

        Out_TypeDoc_list selectTypeDoc = new Out_TypeDoc_list();
        _data_job.Out_TypeDoc = new Out_TypeDoc_list[1];
        _data_job.Out_TypeDoc[0] = selectTypeDoc;

        data_job ddTtpeDoc = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 45));

        DDTypeDoc.DataSource = ddTtpeDoc.Out_TypeDoc;
        DDTypeDoc.DataTextField = "Name_Doc";
        DDTypeDoc.DataValueField = "Doc_IDX";
        DDTypeDoc.DataBind();
    }

    protected void Select_tbposition1()
    {
        DropDownList tbposition1 = (DropDownList)fvExample.FindControl("tbposition1");
        tbposition1.AppendDataBoundItems = true;
        tbposition1.Items.Add(new ListItem("เลือกตำแหน่งงานที่ต้องการ", "00"));

        BoxSelectwantpositionList1 selectposition1 = new BoxSelectwantpositionList1();
        _data_job.BoxSelectwantposition1 = new BoxSelectwantpositionList1[1];
        _data_job.BoxSelectwantposition1[0] = selectposition1;

        data_job dd1 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 11));

        tbposition1.DataSource = dd1.BoxSelectwantposition1;
        tbposition1.DataTextField = "PosGroupNameTH";
        tbposition1.DataValueField = "PosGroupIDX";
        tbposition1.DataBind();
    }

    protected void Select_tbposition2()
    {
        DropDownList tbposition2 = (DropDownList)fvExample.FindControl("tbposition2");
        tbposition2.AppendDataBoundItems = true;
        tbposition2.Items.Add(new ListItem("เลือกตำแหน่งงานที่ต้องการ", "00"));

        BoxSelectwantpositionList1 selectposition2 = new BoxSelectwantpositionList1();
        _data_job.BoxSelectwantposition1 = new BoxSelectwantpositionList1[1];
        _data_job.BoxSelectwantposition1[0] = selectposition2;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd2 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 11));
        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        tbposition2.DataSource = dd2.BoxSelectwantposition1;
        tbposition2.DataTextField = "PosGroupNameTH";
        tbposition2.DataValueField = "PosGroupIDX";
        tbposition2.DataBind();
    }

    protected void Select_ddlocationstart()
    {
        DropDownList ddlocationstart = (DropDownList)FormView1.FindControl("ddlocationstart");
        ddlocationstart.AppendDataBoundItems = true;
        ddlocationstart.Items.Add(new ListItem("กรุณาเลือกจังหวัด", "00"));

        ddlocationstartlist Selectddlocationstart = new ddlocationstartlist();
        _data_job.ddlocationstart = new ddlocationstartlist[1];
        _data_job.ddlocationstart[0] = Selectddlocationstart;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd3 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 12));
        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddlocationstart.DataSource = dd3.ddlocationstart;
        ddlocationstart.DataTextField = "ProvName";
        ddlocationstart.DataValueField = "ProvIDX";
        ddlocationstart.DataBind();
    }

    protected void Select_ddnationality()
    {
        DropDownList ddnationality = (DropDownList)FormView1.FindControl("ddnationality");
        ddnationality.AppendDataBoundItems = true;
        ddnationality.Items.Add(new ListItem("กรุณาเลือกสัญชาติ", "00"));

        ddnationalitylist Selectddnationality = new ddnationalitylist();
        _data_job.ddnationality = new ddnationalitylist[1];
        _data_job.ddnationality[0] = Selectddnationality;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd3 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 13));
        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddnationality.DataSource = dd3.ddnationality;
        ddnationality.DataTextField = "NatName";
        ddnationality.DataValueField = "NatIDX";
        ddnationality.DataBind();
    }

    protected void Select_ddrace()
    {
        DropDownList ddrace = (DropDownList)FormView1.FindControl("ddrace");
        ddrace.AppendDataBoundItems = true;
        ddrace.Items.Add(new ListItem("กรุณาเลือกเชื้อชาติ", "00"));

        Select_Race_list Selectddrace = new Select_Race_list();
        _data_job.Select_Race = new Select_Race_list[1];
        _data_job.Select_Race[0] = Selectddrace;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd4 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 76));
        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddrace.DataSource = dd4.Select_Race;
        ddrace.DataTextField = "RaceName";
        ddrace.DataValueField = "RaceIDX";
        ddrace.DataBind();
    }

    protected void Select_ddblood()
    {
        DropDownList ddblood = (DropDownList)FormView1.FindControl("ddblood");
        ddblood.AppendDataBoundItems = true;
        ddblood.Items.Add(new ListItem("กรุณาเลือกกรุ๊ปเลือด", "00"));

        ddbloodlist Selectddblood = new ddbloodlist();
        _data_job.ddblood = new ddbloodlist[1];
        _data_job.ddblood[0] = Selectddblood;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd5 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 14));
        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddblood.DataSource = dd5.ddblood;
        ddblood.DataTextField = "BGName";
        ddblood.DataValueField = "BGIDX";
        ddblood.DataBind();
    }

    protected void Select_ddreligon()
    {
        DropDownList ddreligon = (DropDownList)FormView1.FindControl("ddreligon");
        ddreligon.AppendDataBoundItems = true;
        ddreligon.Items.Add(new ListItem("กรุณาเลือกศาสนา", "00"));

        ddreligonlist Selectddreligon = new ddreligonlist();
        _data_job.ddreligon = new ddreligonlist[1];
        _data_job.ddreligon[0] = Selectddreligon;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd6 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 15));
        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddreligon.DataSource = dd6.ddreligon;
        ddreligon.DataTextField = "RelNameTH";
        ddreligon.DataValueField = "RelIDX";
        ddreligon.DataBind();
    }

    protected void Select_ddsex()
    {
        DropDownList ddsex = (DropDownList)FormView1.FindControl("ddsex");
        ddsex.AppendDataBoundItems = true;
        ddsex.Items.Add(new ListItem("กรุณาเลือกเพศ", "00"));

        ddsexlist Selectddsex = new ddsexlist();
        _data_job.ddsex = new ddsexlist[1];
        _data_job.ddsex[0] = Selectddsex;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd7 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 16));
        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddsex.DataSource = dd7.ddsex;
        ddsex.DataTextField = "SexNameTH";
        ddsex.DataValueField = "SexIDX";
        ddsex.DataBind();
    }

    protected void Select_education()
    {
        DropDownList ddeducation = (DropDownList)FormView3.FindControl("ddeducation");
        ddeducation.AppendDataBoundItems = true;
        ddeducation.Items.Add(new ListItem("กรุณาเลือกระดับการศึกษา", "00"));

        education_list Selectddeducation = new education_list();
        _data_job.education = new education_list[1];
        _data_job.education[0] = Selectddeducation;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd8 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 17));
        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddeducation.DataSource = dd8.education;
        ddeducation.DataTextField = "Edu_name";
        ddeducation.DataValueField = "Eduidx";
        ddeducation.DataBind();
    }

    protected void Select_ddprovinceeducation()
    {
        DropDownList ddprovinceeducation = (DropDownList)FormView3.FindControl("ddprovinceeducation");
        ddprovinceeducation.AppendDataBoundItems = true;
        ddprovinceeducation.Items.Add(new ListItem("กรุณาเลือกจังหวัด", "00"));

        ddlocationstartlist Selectddprovinceeducation = new ddlocationstartlist();
        _data_job.ddlocationstart = new ddlocationstartlist[1];
        _data_job.ddlocationstart[0] = Selectddprovinceeducation;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd9 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 12));
        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddprovinceeducation.DataSource = dd9.ddlocationstart;
        ddprovinceeducation.DataTextField = "ProvName";
        ddprovinceeducation.DataValueField = "ProvIDX";
        ddprovinceeducation.DataBind();
    }

    protected void Select_ddcompleted()
    {
        DropDownList ddcompleted = (DropDownList)FormView3.FindControl("ddcompleted");
        ddcompleted.AppendDataBoundItems = true;
        ddcompleted.Items.Add(new ListItem("กรุณาเลือกวุฒิการศึกษา", "00"));

        education_list Selectddcompleted = new education_list();
        _data_job.education = new education_list[1];
        _data_job.education[0] = Selectddcompleted;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd10 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 17));
        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddcompleted.DataSource = dd10.education;
        ddcompleted.DataTextField = "Edu_name";
        ddcompleted.DataValueField = "Eduidx";
        ddcompleted.DataBind();
    }

    protected void Select_ddpositionbusiness()
    {
        DropDownList ddpositionbusiness = (DropDownList)FormView4.FindControl("ddpositionbusiness");
        ddpositionbusiness.AppendDataBoundItems = true;
        ddpositionbusiness.Items.Add(new ListItem("กรุณาเลือกตำแหน่ง", "00"));

        ddpositionbusiness_list Selectddpositionbusiness = new ddpositionbusiness_list();
        _data_job.ddpositionbusiness = new ddpositionbusiness_list[1];
        _data_job.ddpositionbusiness[0] = Selectddpositionbusiness;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd11 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 18));
        
        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddpositionbusiness.DataSource = dd11.ddpositionbusiness;
        ddpositionbusiness.DataTextField = "PosGroupNameTH";
        ddpositionbusiness.DataValueField = "PosGroupIDX";
        ddpositionbusiness.DataBind();
    }

    protected void Select_ddpositionpersons()
    {
        DropDownList ddpositionpersons = (DropDownList)FormView6.FindControl("ddpositionpersons");
        ddpositionpersons.AppendDataBoundItems = true;
        ddpositionpersons.Items.Add(new ListItem("กรุณาเลือกตำแหน่ง", "00"));

        ddpositionbusiness_list Selectddpositionbusiness = new ddpositionbusiness_list();
        _data_job.ddpositionbusiness = new ddpositionbusiness_list[1];
        _data_job.ddpositionbusiness[0] = Selectddpositionbusiness;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd12 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 18));

        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddpositionpersons.DataSource = dd12.ddpositionbusiness;
        ddpositionpersons.DataTextField = "PosGroupNameTH";
        ddpositionpersons.DataValueField = "PosGroupIDX";
        ddpositionpersons.DataBind();
    }

    protected void Select_ddadvise()
    {
        DropDownList ddadvise = (DropDownList)FormView6.FindControl("ddadvise");
        ddadvise.AppendDataBoundItems = true;
        ddadvise.Items.Add(new ListItem("กรุณาเลือกแหล่งข้อมูล", "00"));

        ddadvise_list Selectddadvise = new ddadvise_list();
        _data_job.ddadvise = new ddadvise_list[1];
        _data_job.ddadvise[0] = Selectddadvise;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd13 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 19));

        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddadvise.DataSource = dd13.ddadvise;
        ddadvise.DataTextField = "ADVISEName";
        ddadvise.DataValueField = "ADVISEIDX";
        ddadvise.DataBind();
    }

    protected void Select_ddworkcompany()
    {
        DropDownList ddworkcompany = (DropDownList)FormView6.FindControl("ddworkcompany");
        ddworkcompany.AppendDataBoundItems = true;
        ddworkcompany.Items.Add(new ListItem("กรุณาเลือก เคย/ไม่เคย", "00"));

        decision_list Selectddworkcompany = new decision_list();
        _data_job.decision = new decision_list[1];
        _data_job.decision[0] = Selectddworkcompany;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd14 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 41));

        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddworkcompany.DataSource = dd14.decision;
        ddworkcompany.DataTextField = "DECIName";
        ddworkcompany.DataValueField = "DECIIDX";
        ddworkcompany.DataBind();
    }

    protected void Select_ddcriminal()
    {
        DropDownList ddcriminal = (DropDownList)FormView6.FindControl("ddcriminal");
        ddcriminal.AppendDataBoundItems = true;
        ddcriminal.Items.Add(new ListItem("กรุณาเลือก เคย/ไม่เคย", "00"));

        decision_list Selectddworkcompany = new decision_list();
        _data_job.decision = new decision_list[1];
        _data_job.decision[0] = Selectddworkcompany;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd15 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 41));

        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddcriminal.DataSource = dd15.decision;
        ddcriminal.DataTextField = "DECIName";
        ddcriminal.DataValueField = "DECIIDX";
        ddcriminal.DataBind();
    }

    protected void Select_ddallergy()
    {
        DropDownList ddallergy = (DropDownList)FormView6.FindControl("ddallergy");
        ddallergy.AppendDataBoundItems = true;
        ddallergy.Items.Add(new ListItem("กรุณาเลือก เคย/ไม่เคย", "00"));

        decision_list Selectddworkcompany = new decision_list();
        _data_job.decision = new decision_list[1];
        _data_job.decision[0] = Selectddworkcompany;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd16 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 41));

        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddallergy.DataSource = dd16.decision;
        ddallergy.DataTextField = "DECIName";
        ddallergy.DataValueField = "DECIIDX";
        ddallergy.DataBind();
    }

    protected void Select_dddisease()
    {
        DropDownList dddisease = (DropDownList)FormView6.FindControl("dddisease");
        dddisease.AppendDataBoundItems = true;
        dddisease.Items.Add(new ListItem("กรุณาเลือก เคย/ไม่เคย", "00"));

        decision_list Selectddworkcompany = new decision_list();
        _data_job.decision = new decision_list[1];
        _data_job.decision[0] = Selectddworkcompany;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd17 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 41));

        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        dddisease.DataSource = dd17.decision;
        dddisease.DataTextField = "DECIName";
        dddisease.DataValueField = "DECIIDX";
        dddisease.DataBind();
    }

    protected void Select_ddsick()
    {
        DropDownList ddsick = (DropDownList)FormView6.FindControl("ddsick");
        ddsick.AppendDataBoundItems = true;
        ddsick.Items.Add(new ListItem("กรุณาเลือก เคย/ไม่เคย", "00"));

        decision_list Selectddworkcompany = new decision_list();
        _data_job.decision = new decision_list[1];
        _data_job.decision[0] = Selectddworkcompany;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd18 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 41));

        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddsick.DataSource = dd18.decision;
        ddsick.DataTextField = "DECIName";
        ddsick.DataValueField = "DECIIDX";
        ddsick.DataBind();
    }

    protected void Select_ddlocationHere()
    {
        DropDownList ddlocationHere = (DropDownList)FormView1.FindControl("ddlocationHere");
        ddlocationHere.AppendDataBoundItems = true;
        ddlocationHere.Items.Add(new ListItem("กรุณาเลือกจังหวัด", "00"));

        ddlocationstartlist Selectddlocationstart = new ddlocationstartlist();
        _data_job.ddlocationstart = new ddlocationstartlist[1];
        _data_job.ddlocationstart[0] = Selectddlocationstart;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd19 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 12));
        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddlocationHere.DataSource = dd19.ddlocationstart;
        ddlocationHere.DataTextField = "ProvName";
        ddlocationHere.DataValueField = "ProvIDX";
        ddlocationHere.DataBind();
    }

    protected void Select_ddAmphurstart()
    {
        DropDownList ddAmphur = (DropDownList)FormView1.FindControl("ddAmphur");
        ddAmphur.AppendDataBoundItems = true;
        ddAmphur.Items.Add(new ListItem("กรุณาเลือกจังหวัดปัจจุบันก่อน", "00"));
    }
    protected void Select_ddAmphur()
    {
        DropDownList ddAmphur = (DropDownList)FormView1.FindControl("ddAmphur");
        ddAmphur.AppendDataBoundItems = true;
        ddAmphur.Items.Add(new ListItem("กรุณาเลือกอำเภอ", "00"));

        ddAmphur_list SelectddAmphur = new ddAmphur_list();
        _data_job.ddAmphur = new ddAmphur_list[1];
        SelectddAmphur.ProvIDX_ = int.Parse(ViewState["ProvIDX"].ToString());
        _data_job.ddAmphur[0] = SelectddAmphur;

        data_job dd20 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 30));

        ddAmphur.DataSource = dd20.ddAmphur;
        ddAmphur.DataTextField = "AmpName";
        ddAmphur.DataValueField = "AmpIDX";
        ddAmphur.DataBind();
    }

    protected void Select_DistrictStart()
    {
        DropDownList ddDistrict = (DropDownList)FormView1.FindControl("ddDistrict");
        ddDistrict.AppendDataBoundItems = true;
        ddDistrict.Items.Add(new ListItem("กรุณาเลือกอำเภอก่อน", "00"));
    }
    protected void Select_District()
    {
        DropDownList ddDistrict = (DropDownList)FormView1.FindControl("ddDistrict");
        ddDistrict.AppendDataBoundItems = true;
        ddDistrict.Items.Add(new ListItem("กรุณาเลือกตำบล", "00"));

        ddDistrict_list SelectDistrict = new ddDistrict_list();
        _data_job.ddDistrict = new ddDistrict_list[1];
        SelectDistrict.AmpIDX_ = int.Parse(ViewState["AmpIDX"].ToString());
        _data_job.ddDistrict[0] = SelectDistrict;

        data_job dd21 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 31));

        ddDistrict.DataSource = dd21.ddDistrict;
        ddDistrict.DataTextField = "DistName";
        ddDistrict.DataValueField = "DistIDX";
        ddDistrict.DataBind();
    }

    protected void Select_ddPrefix()
    {
        DropDownList ddPrefix = (DropDownList)FormView1.FindControl("ddPrefix");
        ddPrefix.AppendDataBoundItems = true;
        ddPrefix.Items.Add(new ListItem("กรุณาเลือกคำนำหน้าชื่อ", "00"));

        ddPrefix_list SelectddPrefix = new ddPrefix_list();
        _data_job.ddPrefix = new ddPrefix_list[1];
        _data_job.ddPrefix[0] = SelectddPrefix;

        data_job dd22 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 32));

        ddPrefix.DataSource = dd22.ddPrefix;
        ddPrefix.DataTextField = "PrefixNameTH";
        ddPrefix.DataValueField = "PrefixIDX";
        ddPrefix.DataBind();
    }

    protected void Select_ddPrefixEng()
    {
        DropDownList ddPrefixEng = (DropDownList)FormView1.FindControl("ddPrefixEng");
        ddPrefixEng.AppendDataBoundItems = true;
        ddPrefixEng.Items.Add(new ListItem("Please select title", "00"));

        ddPrefixEng_list SelectddPrefixEng = new ddPrefixEng_list();
        _data_job.ddPrefixEng = new ddPrefixEng_list[1];
        _data_job.ddPrefixEng[0] = SelectddPrefixEng;

        data_job dd23 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 33));

        ddPrefixEng.DataSource = dd23.ddPrefixEng;
        ddPrefixEng.DataTextField = "PrefixNameEN";
        ddPrefixEng.DataValueField = "PrefixIDX";
        ddPrefixEng.DataBind();
    }

    protected void Select_ddCountry()
    {
        DropDownList ddCountry = (DropDownList)FormView1.FindControl("ddCountry");
        ddCountry.AppendDataBoundItems = true;
        ddCountry.Items.Add(new ListItem("กรุณาเลือกประเทศ", "00"));

        ddCountry_list SelectddPrefixEng = new ddCountry_list();
        _data_job.ddCountry = new ddCountry_list[1];
        _data_job.ddCountry[0] = SelectddPrefixEng;

        data_job dd24 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 34));

        ddCountry.DataSource = dd24.ddCountry;
        ddCountry.DataTextField = "CountryName";
        ddCountry.DataValueField = "CountryIDX";
        ddCountry.DataBind();
    }

    protected void Select_dd_Y_form()
    {
        DropDownList Addyearattendedstart = (DropDownList)FormView3.FindControl("Addyearattendedstart");
        Addyearattendedstart.AppendDataBoundItems = true;
        Addyearattendedstart.Items.Add(new ListItem("เลือกปีที่ต้องการ", "00"));
        for (int a = 0; a < 40; a++)
        {
            int b = 1990;
            int id_Y = b + a;
            int st_id_Y = id_Y;
            Addyearattendedstart.Items.Add(new ListItem(st_id_Y.ToString(), st_id_Y.ToString()));
        }
    }

    protected void Select_dd_Y_to()
    {
        DropDownList tbtoattended = (DropDownList)FormView3.FindControl("tbtoattended");
        tbtoattended.AppendDataBoundItems = true;
        tbtoattended.Items.Add(new ListItem("เลือกปีที่ต้องการ", "00"));
        for (int a = 0; a < 40; a++)
        {
            int b = 1990;
            int id_Y = b + a;
            int st_id_Y = id_Y;
            tbtoattended.Items.Add(new ListItem(st_id_Y.ToString(), st_id_Y.ToString()));
        }
    }

    protected void Select_tbotherLanguage()
    {
        DropDownList tbotherLanguage = (DropDownList)FormView5.FindControl("tbotherLanguage");
        tbotherLanguage.AppendDataBoundItems = true;
        tbotherLanguage.Items.Add(new ListItem("กรุณาเลือกภาษา", "00"));

        tbotherLanguage_list SelectDistrict = new tbotherLanguage_list();
        _data_job.tbotherLanguage = new tbotherLanguage_list[1];
        _data_job.tbotherLanguage[0] = SelectDistrict;

        data_job dd25 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 36));

        tbotherLanguage.DataSource = dd25.tbotherLanguage;
        tbotherLanguage.DataTextField = "NameEN";
        tbotherLanguage.DataValueField = "LanIDX";
        tbotherLanguage.DataBind();
    }

    protected void Select_ddTypesOfCar()
    {
        DropDownList ddTypesOfCar = (DropDownList)FormView5.FindControl("ddTypesOfCar");
        ddTypesOfCar.AppendDataBoundItems = true;
        ddTypesOfCar.Items.Add(new ListItem("กรุณาเลือกประเภทรถ", "00"));

        ddTypesOfCar_list SelectddTypesOfCar = new ddTypesOfCar_list();
        _data_job.ddTypesOfCar = new ddTypesOfCar_list[1];
        _data_job.ddTypesOfCar[0] = SelectddTypesOfCar;

        data_job dd26 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 38));

        ddTypesOfCar.DataSource = dd26.ddTypesOfCar;
        ddTypesOfCar.DataTextField = "Name";
        ddTypesOfCar.DataValueField = "DLIDX";
        ddTypesOfCar.DataBind();
    }

    #endregion

    #region ใส่อายุใน dropdown
    protected void Select_dd_Y()
    {
        DropDownList ddage = (DropDownList)FormView2.FindControl("ddage");
        ddage.AppendDataBoundItems = true;
        ddage.Items.Add(new ListItem("เลือกอายุ", "00"));
        for (int a = 35; a < 80; a++)
        {
            int b = 1;
            int id_Y = b + a;
            int st_id_Y = id_Y;
            ddage.Items.Add(new ListItem(st_id_Y.ToString(), st_id_Y.ToString()));
        }
    }

    protected void Select_dd_txtage()
    {
        DropDownList tbage = (DropDownList)FormView1.FindControl("tbage");
        tbage.AppendDataBoundItems = true;
        tbage.Items.Add(new ListItem("เลือกอายุ", "00"));
        for (int a = 0; a < 50; a++)
        {
            int b = 1;
            int id_Y = b + a;
            int st_id_Y = id_Y;
            tbage.Items.Add(new ListItem(st_id_Y.ToString(), st_id_Y.ToString()));
        }
    }

    protected void Select_ddahemother()
    {
        DropDownList ddahemother = (DropDownList)FormView2.FindControl("ddahemother");
        ddahemother.AppendDataBoundItems = true;
        ddahemother.Items.Add(new ListItem("เลือกอายุ", "00"));
        for (int a = 35; a < 80; a++)
        {
            int b = 1;
            int id_Y = b + a;
            int st_id_Y = id_Y;
            ddahemother.Items.Add(new ListItem(st_id_Y.ToString(), st_id_Y.ToString()));
        }
    }

    protected void Select_ddageparent1()
    {
        DropDownList ddageparent1 = (DropDownList)FormView2.FindControl("ddageparent1");
        ddageparent1.AppendDataBoundItems = true;
        ddageparent1.Items.Add(new ListItem("เลือกอายุ", "00"));
        for (int a = 0; a < 50; a++)
        {
            int b = 1;
            int id_Y = b + a;
            int st_id_Y = id_Y;
            ddageparent1.Items.Add(new ListItem(st_id_Y.ToString(), st_id_Y.ToString()));
        }
    }
    #endregion

    #region เลือกแล้วเก็บค่า value
    protected void selectchang(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        DropDownList ddAmphur = (DropDownList)FormView1.FindControl("ddAmphur");
        DropDownList ddDistrict = (DropDownList)FormView1.FindControl("ddDistrict");
        switch (ddName.ID)
        {
            case "ddlocationHere":
                ViewState["ProvIDX"] = int.Parse(((DropDownList)FormView1.FindControl("ddlocationHere")).SelectedValue);
                ddAmphur.Items.Clear();
                //text.Text = ViewState["ProvIDX"].ToString();
                if (ViewState["ProvIDX"].ToString() != null)
                {
                    Select_ddAmphur();
                }
                break;

            case "ddAmphur":
                ViewState["AmpIDX"] = int.Parse(((DropDownList)FormView1.FindControl("ddAmphur")).SelectedValue);
                ddDistrict.Items.Clear();
                if (ViewState["AmpIDX"].ToString() != null)
                {
                    Select_District();
                }
                break;

        }
    }
    #endregion

    #region setgrideview edit/updat/delete

    #region Gvbuyequipment_Editing
    protected void Gvbuyequipment_Editing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        GridView GvReportAddparent = (GridView)FormView2.FindControl("GvReportAddparent");
        GridView GvReportAdd_Education = (GridView)FormView3.FindControl("GvReportAdd_Education");
        GridView GvReportAdd_Employee = (GridView)FormView4.FindControl("GvReportAdd_Employee");
        GridView GvReportAddOtherLanguages = (GridView)FormView5.FindControl("GvReportAddOtherLanguages");
        GridView GvReportAdd_Car = (GridView)FormView5.FindControl("GvReportAdd_Car");

        switch (GvName.ID)
        {
            case "GvReportAddparent":
                GvReportAddparent.EditIndex = e.NewEditIndex;
                GvReportAddparent.DataSource = ViewState["vsBuyequipment"];
                GvReportAddparent.DataBind();
                break;

            case "GvReportAdd_Education":
                GvReportAdd_Education.EditIndex = e.NewEditIndex;
                GvReportAdd_Education.DataSource = ViewState["vsBuyds_Education"];
                GvReportAdd_Education.DataBind();
                break;

            case "GvReportAdd_Employee":
                GvReportAdd_Employee.EditIndex = e.NewEditIndex;
                GvReportAdd_Employee.DataSource = ViewState["vsBuyds_Employed"];
                GvReportAdd_Employee.DataBind();
                break;

            case "GvReportAddOtherLanguages":
                GvReportAddOtherLanguages.EditIndex = e.NewEditIndex;
                GvReportAddOtherLanguages.DataSource = ViewState["vsBuyds_dsOtherLanguages"];
                GvReportAddOtherLanguages.DataBind();
                break;

            case "GvReportAdd_Car":
                GvReportAdd_Car.EditIndex = e.NewEditIndex;
                GvReportAdd_Car.DataSource = ViewState["vsBuyds_dsTypeCar"];
                GvReportAdd_Car.DataBind();
                break;
        }
    }
    #endregion

    #region Gvbuyequipment_RowCancelingEdit
    protected void Gvbuyequipment_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        GridView GvReportAddparent = (GridView)FormView2.FindControl("GvReportAddparent");
        GridView GvReportAdd_Education = (GridView)FormView3.FindControl("GvReportAdd_Education");
        GridView GvReportAdd_Employee = (GridView)FormView4.FindControl("GvReportAdd_Employee");
        GridView GvReportAddOtherLanguages = (GridView)FormView5.FindControl("GvReportAddOtherLanguages");
        GridView GvReportAdd_Car = (GridView)FormView5.FindControl("GvReportAdd_Car");

        switch (GvName.ID)
        {
            case "GvReportAddparent":
                GvReportAddparent.EditIndex = -1;
                GvReportAddparent.DataSource = ViewState["vsBuyequipment"];
                GvReportAddparent.DataBind();
                break;

            case "GvReportAdd_Education":
                GvReportAdd_Education.EditIndex = -1;
                GvReportAdd_Education.DataSource = ViewState["vsBuyds_Education"];
                GvReportAdd_Education.DataBind();
                break;

            case "GvReportAdd_Employee":
                GvReportAdd_Employee.EditIndex = -1;
                GvReportAdd_Employee.DataSource = ViewState["vsBuyds_Employed"];
                GvReportAdd_Employee.DataBind();
                break;

            case "GvReportAddOtherLanguages":
                GvReportAddOtherLanguages.EditIndex = -1;
                GvReportAddOtherLanguages.DataSource = ViewState["vsBuyds_dsOtherLanguages"];
                GvReportAddOtherLanguages.DataBind();
                break;

            case "GvReportAdd_Car":
                GvReportAdd_Car.EditIndex = -1;
                GvReportAdd_Car.DataSource = ViewState["vsBuyds_dsTypeCar"];
                GvReportAdd_Car.DataBind();
                break;
        }
    }
    #endregion

    #region Gvbuyequipment_RowUpdating
    protected void Gvbuyequipment_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        GridView GvReportAddparent = (GridView)FormView2.FindControl("GvReportAddparent");
        GridView GvReportAdd_Education = (GridView)FormView3.FindControl("GvReportAdd_Education");
        GridView GvReportAdd_Employee = (GridView)FormView4.FindControl("GvReportAdd_Employee");
        GridView GvReportAddOtherLanguages = (GridView)FormView5.FindControl("GvReportAddOtherLanguages");
        GridView GvReportAdd_Car = (GridView)FormView5.FindControl("GvReportAdd_Car");

        switch (GvName.ID)
        {
            case "GvReportAddparent":
                var tbnameparent1_edit = (TextBox)GvReportAddparent.Rows[e.RowIndex].FindControl("tbnameparent1_edit");
                var tblastnameparent1_edit = (TextBox)GvReportAddparent.Rows[e.RowIndex].FindControl("tblastnameparent1_edit");
                var ddageparent1_edit = (DropDownList)GvReportAddparent.Rows[e.RowIndex].FindControl("ddageparent1_edit");
                var tboccupationparent1_edit = (TextBox)GvReportAddparent.Rows[e.RowIndex].FindControl("tboccupationparent1_edit");
                var tbfirmaddressparent1_edit = (TextBox)GvReportAddparent.Rows[e.RowIndex].FindControl("tbfirmaddressparent1_edit");
                var tbnumberparent1_edit = (TextBox)GvReportAddparent.Rows[e.RowIndex].FindControl("tbnumberparent1_edit");


                var dsBuyequipmentUpdate = (DataSet)ViewState["vsBuyequipment"];
                var drBuyequipment = dsBuyequipmentUpdate.Tables[0].Rows;

                drBuyequipment[e.RowIndex]["tbnameparent1"] = tbnameparent1_edit.Text;
                drBuyequipment[e.RowIndex]["tblastnameparent1"] = tblastnameparent1_edit.Text;
                drBuyequipment[e.RowIndex]["ddageparent1"] = ddageparent1_edit.Text;
                drBuyequipment[e.RowIndex]["tboccupationparent1"] = tboccupationparent1_edit.Text;
                drBuyequipment[e.RowIndex]["tbfirmaddressparent1"] = tbfirmaddressparent1_edit.Text;
                drBuyequipment[e.RowIndex]["tbnumberparent1"] = tbnumberparent1_edit.Text;

                ViewState["vsBuyequipment"] = dsBuyequipmentUpdate;
                //ViewState["vsBuyequipment1"] = dsBuyequipmentUpdate;

                GvReportAddparent.EditIndex = -1;
                GvReportAddparent.DataSource = ViewState["vsBuyequipment"];
                GvReportAddparent.DataBind();
                break;

            case "GvReportAdd_Education":
                var tbinstitute_edit = (TextBox)GvReportAdd_Education.Rows[e.RowIndex].FindControl("tbinstitute_edit");
                var ddeducation_edit = (DropDownList)GvReportAdd_Education.Rows[e.RowIndex].FindControl("ddeducation_edit");
                var ddprovinceeducation_edit = (DropDownList)GvReportAdd_Education.Rows[e.RowIndex].FindControl("ddprovinceeducation_edit");
                var Addyearattendedstart_edit = (DropDownList)GvReportAdd_Education.Rows[e.RowIndex].FindControl("Addyearattendedstart_edit");
                var tbtoattended_edit = (DropDownList)GvReportAdd_Education.Rows[e.RowIndex].FindControl("tbtoattended_edit");
                var tbcourse_edit = (TextBox)GvReportAdd_Education.Rows[e.RowIndex].FindControl("tbcourse_edit");
                var ddcompleted_edit = (DropDownList)GvReportAdd_Education.Rows[e.RowIndex].FindControl("ddcompleted_edit");
                var ddFaculty_edit_ = (DropDownList)GvReportAdd_Education.Rows[e.RowIndex].FindControl("ddFaculty_edit");
                var tbGpa_edit = (TextBox)GvReportAdd_Education.Rows[e.RowIndex].FindControl("tbGpa_edit");

                var ds_EducationUpdate = (DataSet)ViewState["vsBuyds_Education"];
                var drBuyEducation = ds_EducationUpdate.Tables[0].Rows;

                drBuyEducation[e.RowIndex]["tbinstitute"] = tbinstitute_edit.Text;
                drBuyEducation[e.RowIndex]["ddeducation"] = ddeducation_edit.Text;
                drBuyEducation[e.RowIndex]["ddprovinceeducation"] = ddprovinceeducation_edit.Text;
                drBuyEducation[e.RowIndex]["Addyearattendedstart"] = Addyearattendedstart_edit.Text;
                drBuyEducation[e.RowIndex]["tbtoattended"] = tbtoattended_edit.Text;
                drBuyEducation[e.RowIndex]["tbcourse"] = tbcourse_edit.Text;
                drBuyEducation[e.RowIndex]["ddcompleted"] = ddcompleted_edit.Text;
                drBuyEducation[e.RowIndex]["ddFaculty"] = ddFaculty_edit_.Text;
                drBuyEducation[e.RowIndex]["tbGpa"] = tbGpa_edit.Text;
                drBuyEducation[e.RowIndex]["ddeducation_txt"] = ddeducation_edit.SelectedItem;
                drBuyEducation[e.RowIndex]["ddprovinceeducation_txt"] = ddprovinceeducation_edit.SelectedItem;
                drBuyEducation[e.RowIndex]["ddcompleted_txt"] = ddcompleted_edit.SelectedItem;
                drBuyEducation[e.RowIndex]["ddFaculty_txt"] = ddFaculty_edit_.SelectedItem;

                ViewState["vsBuyds_Education"] = ds_EducationUpdate;
                
                GvReportAdd_Education.EditIndex = -1;
                GvReportAdd_Education.DataSource = ViewState["vsBuyds_Education"];
                GvReportAdd_Education.DataBind();
                break;

            case "GvReportAdd_Employee":
                var tbnameemployed_edit = (TextBox)GvReportAdd_Employee.Rows[e.RowIndex].FindControl("tbnameemployed_edit");
                var tbtelbusiness_edit = (TextBox)GvReportAdd_Employee.Rows[e.RowIndex].FindControl("tbtelbusiness_edit");
                var tbtypeBusiness_edit = (TextBox)GvReportAdd_Employee.Rows[e.RowIndex].FindControl("tbtypeBusiness_edit");
                var tbpositionbusiness_edit = (TextBox)GvReportAdd_Employee.Rows[e.RowIndex].FindControl("tbpositionbusiness_edit");
                var tbaddressbusiness_edit = (TextBox)GvReportAdd_Employee.Rows[e.RowIndex].FindControl("tbaddressbusiness_edit");
                var Addyearattendedstart_emp_edit = (TextBox)GvReportAdd_Employee.Rows[e.RowIndex].FindControl("Addyearattendedstart_emp_edit");
                var tbtoattended_emp_edit = (TextBox)GvReportAdd_Employee.Rows[e.RowIndex].FindControl("tbtoattended_emp_edit");
                var tbsupervisor_edit = (TextBox)GvReportAdd_Employee.Rows[e.RowIndex].FindControl("tbsupervisor_edit");
                var tbsalary_emp_edit = (TextBox)GvReportAdd_Employee.Rows[e.RowIndex].FindControl("tbsalary_emp_edit");
                var tbrevenue_edit = (TextBox)GvReportAdd_Employee.Rows[e.RowIndex].FindControl("tbrevenue_edit");
                var tbwork_edit = (TextBox)GvReportAdd_Employee.Rows[e.RowIndex].FindControl("tbwork_edit");
                var tbResignation_edit = (TextBox)GvReportAdd_Employee.Rows[e.RowIndex].FindControl("tbResignation_edit");

                var ds_EmployeeUpdate = (DataSet)ViewState["vsBuyds_Employed"];
                var drBuyEmployed = ds_EmployeeUpdate.Tables[0].Rows;

                drBuyEmployed[e.RowIndex]["tbnameemployed"] = tbnameemployed_edit.Text;
                drBuyEmployed[e.RowIndex]["tbtelbusiness"] = tbtelbusiness_edit.Text;
                drBuyEmployed[e.RowIndex]["tbtypeBusiness"] = tbtypeBusiness_edit.Text;
                drBuyEmployed[e.RowIndex]["tbpositionbusiness"] = tbpositionbusiness_edit.Text;
                drBuyEmployed[e.RowIndex]["tbaddressbusiness"] = tbaddressbusiness_edit.Text;
                drBuyEmployed[e.RowIndex]["Addyearattendedstart_emp"] = Addyearattendedstart_emp_edit.Text;
                drBuyEmployed[e.RowIndex]["tbtoattended_emp"] = tbtoattended_emp_edit.Text;
                drBuyEmployed[e.RowIndex]["tbsupervisor"] = tbsupervisor_edit.Text;
                drBuyEmployed[e.RowIndex]["tbsalary_emp"] = tbsalary_emp_edit.Text;
                drBuyEmployed[e.RowIndex]["tbrevenue"] = tbrevenue_edit.Text;
                drBuyEmployed[e.RowIndex]["tbwork"] = tbwork_edit.Text;
                drBuyEmployed[e.RowIndex]["tbResignation"] = tbResignation_edit.Text;
                //drBuyEmployed[e.RowIndex]["ddpositionbusiness_txt"] = ddpositionbusiness_edit.SelectedItem;

                ViewState["vsBuyds_Employed"] = ds_EmployeeUpdate;

                GvReportAdd_Employee.EditIndex = -1;
                GvReportAdd_Employee.DataSource = ViewState["vsBuyds_Employed"];
                GvReportAdd_Employee.DataBind();
                break;

            case "GvReportAddOtherLanguages":
                var tbotherLanguage_edit = (DropDownList)GvReportAddOtherLanguages.Rows[e.RowIndex].FindControl("tbotherLanguage_edit");
                var status_speaking_other_edit = (RadioButtonList)GvReportAddOtherLanguages.Rows[e.RowIndex].FindControl("status_speaking_other_edit");
                var status_Understanding_other_edit = (RadioButtonList)GvReportAddOtherLanguages.Rows[e.RowIndex].FindControl("status_Understanding_other_edit");
                var status_Reading_other_edit = (RadioButtonList)GvReportAddOtherLanguages.Rows[e.RowIndex].FindControl("status_Reading_other_edit");
                var status_Wrinting_other_edit = (RadioButtonList)GvReportAddOtherLanguages.Rows[e.RowIndex].FindControl("status_Wrinting_other_edit");


                var dsBuyOtherLanguagesUpdate = (DataSet)ViewState["vsBuyds_dsOtherLanguages"];
                var drBuyOtherLanguages = dsBuyOtherLanguagesUpdate.Tables[0].Rows;

                drBuyOtherLanguages[e.RowIndex]["tbotherLanguage"] = tbotherLanguage_edit.Text;
                ViewState["tbotherLanguageTable"] = tbotherLanguage_edit.SelectedValue;
                drBuyOtherLanguages[e.RowIndex]["status_speaking_other"] = status_speaking_other_edit.Text;
                drBuyOtherLanguages[e.RowIndex]["status_Understanding_other"] = status_Understanding_other_edit.Text;
                drBuyOtherLanguages[e.RowIndex]["status_Reading_other"] = status_Reading_other_edit.Text;
                drBuyOtherLanguages[e.RowIndex]["status_Wrinting_other"] = status_Wrinting_other_edit.Text;
                drBuyOtherLanguages[e.RowIndex]["status_speaking_other_txt"] = status_speaking_other_edit.SelectedItem;
                drBuyOtherLanguages[e.RowIndex]["status_Understanding_other_txt"] = status_Understanding_other_edit.SelectedItem;
                drBuyOtherLanguages[e.RowIndex]["status_Reading_other_txt"] = status_Reading_other_edit.SelectedItem;
                drBuyOtherLanguages[e.RowIndex]["status_Wrinting_other_txt"] = status_Wrinting_other_edit.SelectedItem;
                drBuyOtherLanguages[e.RowIndex]["tbotherLanguage_txt"] = tbotherLanguage_edit.SelectedItem;


                ViewState["vsBuyds_dsOtherLanguages"] = dsBuyOtherLanguagesUpdate;

                GvReportAddOtherLanguages.EditIndex = -1;
                GvReportAddOtherLanguages.DataSource = ViewState["vsBuyds_dsOtherLanguages"];
                GvReportAddOtherLanguages.DataBind();
                
                break;

            case "GvReportAdd_Car":
                var ddTypeOfCar_edit = (DropDownList)GvReportAdd_Car.Rows[e.RowIndex].FindControl("ddTypeOfCar_edit");
                var tbdriving_edit = (TextBox)GvReportAdd_Car.Rows[e.RowIndex].FindControl("tbdriving_edit");

                var dsBuyTypeCarUpdate = (DataSet)ViewState["vsBuyds_dsTypeCar"];
                var drBuyTypeCar = dsBuyTypeCarUpdate.Tables[0].Rows;

                drBuyTypeCar[e.RowIndex]["ddTypesOfCar"] = ddTypeOfCar_edit.Text;
                drBuyTypeCar[e.RowIndex]["tbdriving"] = tbdriving_edit.Text;
                drBuyTypeCar[e.RowIndex]["ddTypesOfCar_txt"] = ddTypeOfCar_edit.SelectedItem;

                ViewState["vsBuyds_dsTypeCar"] = dsBuyTypeCarUpdate;

                GvReportAdd_Car.EditIndex = -1;
                GvReportAdd_Car.DataSource = ViewState["vsBuyds_dsTypeCar"];
                GvReportAdd_Car.DataBind();
                break;
        }

    }
    #endregion

    #region Gvbuyequipment_RowDataBound

    protected void R1_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        ////Panel hiImYes = (Panel)e.Item.FindControl("hiImYes");
        //Panel hiImNo = (Panel)e.Item.FindControl("hiImNo");
        //string getPath = ConfigurationSettings.AppSettings["PathFileJobONLine"];
        ////string pat = getPath + x.ToString() + "/" + x.ToString() + "0.jpg";
        //if (!Directory.Exists(Server.MapPath(getPath + Session["idImage"].ToString())))
        //{
        //    //hiImYes.Visible = false;
        //    hiImNo.Visible = true;
        //}
        //else
        //{
        //    //hiImYes.Visible = true;
        //    hiImNo.Visible = false;
        //}
    }

    protected void Gvbuyequipment_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        GridView GvReportAddparent = (GridView)FormView2.FindControl("GvReportAddparent");
        GridView GvReportAdd_Education = (GridView)FormView3.FindControl("GvReportAdd_Education");
        GridView GvReportAdd_Employee = (GridView)FormView4.FindControl("GvReportAdd_Employee");
        GridView GvReportAddOtherLanguages = (GridView)FormView5.FindControl("GvReportAddOtherLanguages");
        GridView GvReportAdd_Car = (GridView)FormView5.FindControl("GvReportAdd_Car");
        GridView gvDocumentFile = (GridView)FormView8.FindControl("gvDocumentFile");
        switch (GvName.ID)
        {
            case "gvDocumentFile":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;

            case "GvReportAdd_Car":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    #region editdropdown type car
                    var ddTypeOfCar_edit = (DropDownList)e.Row.FindControl("ddTypeOfCar_edit");
                    var ddTypeOfCar_edit_ = ((HiddenField)e.Row.Cells[0].FindControl("ddTypeOfCar_edit_")).Value;
                    ViewState["ddTypeOfCar_edit_"] = ddTypeOfCar_edit_.ToString();

                    ddTypesOfCar_list SelectddTypeCar = new ddTypesOfCar_list();
                    _data_job.ddTypesOfCar = new ddTypesOfCar_list[1];
                    _data_job.ddTypesOfCar[0] = SelectddTypeCar;

                    //    ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
                    data_job dd80x = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 38));
                    //    ///////////////////////////////////////////
                    //localXml = _functionWeb.ConvertObjectToXml(dd8x);
                    //text.Text = ViewState["ddprovinceeducation_edit_"].ToString();

                    ddTypeOfCar_edit.DataSource = dd80x.ddTypesOfCar;
                    ddTypeOfCar_edit.DataTextField = "Name";
                    ddTypeOfCar_edit.DataValueField = "DLIDX";
                    ddTypeOfCar_edit.DataBind();
                    ddTypeOfCar_edit.SelectedValue = ViewState["ddTypeOfCar_edit_"].ToString();
                    #endregion
                }
                break;

            case "GvReportAddparent":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    #region editdropdown age
                    var ddageparent1_edit = (DropDownList)e.Row.FindControl("ddageparent1_edit");
                    ddageparent1_edit.AppendDataBoundItems = true;
                    ddageparent1_edit.Items.Add(new ListItem("เลือกอายุ", "00"));
                    var ddageparent1_edit_ = ((HiddenField)e.Row.Cells[0].FindControl("ddageparent1_edit_")).Value;
                    ViewState["ddageparent1_edit_"] = ddageparent1_edit_.ToString();
                    for (int a = 0; a < 50; a++)
                    {
                        int b = 1;
                        int id_Y = b + a;
                        int st_id_Y = id_Y;
                        ddageparent1_edit.Items.Add(new ListItem(st_id_Y.ToString(), st_id_Y.ToString()));
                    }
                    ddageparent1_edit.SelectedValue = ViewState["ddageparent1_edit_"].ToString();
                    #endregion
                }
                break;

            case "GvReportAdd_Education":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                   // var dd1 = e.Row.FindControl("ddeducation_edit") as DropDownList;

                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    #region edit dropdown education
                    var ddeducation_edit = (DropDownList)e.Row.FindControl("ddeducation_edit");
                    ddeducation_edit.AppendDataBoundItems = true;
                    ddeducation_edit.Items.Add(new ListItem("เลือกระดับการศึกษา", "00"));
                    var ddeducation_edit_ = ((HiddenField)e.Row.Cells[0].FindControl("ddeducation_edit_")).Value;
                    ViewState["ddeducation_edit_"] = ddeducation_edit_.ToString();
                    //ddeducation_edit.Items.Clear();

                    education_list Selectddeducationx = new education_list();
                    _data_job.education = new education_list[1];
                    _data_job.education[0] = Selectddeducationx;

                    //    ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
                    data_job dd8x = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 17));
                    //    ///////////////////////////////////////////
                    //localXml = _functionWeb.ConvertObjectToXml(dd8x);
                    //text.Text = ViewState["ddprovinceeducation_edit_"].ToString();

                    ddeducation_edit.DataSource = dd8x.education;
                    ddeducation_edit.DataTextField = "Edu_name";
                    ddeducation_edit.DataValueField = "Eduidx";
                    ddeducation_edit.DataBind();
                    ddeducation_edit.SelectedValue = ViewState["ddeducation_edit_"].ToString();

                    #endregion

                    #region edit dropdown ddcompleted_edit

                    var ddcompleted_edit = (DropDownList)e.Row.FindControl("ddcompleted_edit");
                    ddcompleted_edit.AppendDataBoundItems = true;
                    ddcompleted_edit.Items.Add(new ListItem("เลือกวุฒิที่ได้รับ", "00"));
                    var ddcompleted_edit_ = ((HiddenField)e.Row.Cells[0].FindControl("ddcompleted_edit_")).Value;
                    ViewState["ddcompleted_edit_"] = ddcompleted_edit_.ToString();
                    //ddeducation_edit.Items.Clear();

                    education_list Selectddeducationx2 = new education_list();
                    _data_job.education = new education_list[1];
                    _data_job.education[0] = Selectddeducationx2;

                    //    ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
                    data_job dd8dd = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 17));
                    //    ///////////////////////////////////////////
                    //localXml = _functionWeb.ConvertObjectToXml(dd8x);
                    //text.Text = ViewState["ddprovinceeducation_edit_"].ToString();

                    ddcompleted_edit.DataSource = dd8dd.education;
                    ddcompleted_edit.DataTextField = "Edu_name";
                    ddcompleted_edit.DataValueField = "Eduidx";
                    ddcompleted_edit.DataBind();
                    ddcompleted_edit.SelectedValue = ViewState["ddcompleted_edit_"].ToString();

                    #endregion

                    #region edit dropdown provinceeducation_edit

                    var ddprovinceeducation_edit = (DropDownList)e.Row.FindControl("ddprovinceeducation_edit");
                    ddprovinceeducation_edit.AppendDataBoundItems = true;
                    ddprovinceeducation_edit.Items.Add(new ListItem("เลือกจังหวัด", "00"));
                    var ddprovinceeducation_edit_ = ((HiddenField)e.Row.Cells[0].FindControl("ddprovinceeducation_edit_")).Value;
                    ViewState["ddprovinceeducation_edit_"] = ddprovinceeducation_edit_.ToString();
                    //ddprovinceeducation_edit.Items.Clear();

                    ddlocationstartlist Selectddlocationstart = new ddlocationstartlist();
                    _data_job.ddlocationstart = new ddlocationstartlist[1];
                    _data_job.ddlocationstart[0] = Selectddlocationstart;

                    ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
                    data_job dd3x = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 12));
                    ///////////////////////////////////////////
                    //localXml = _functionWeb.ConvertObjectToXml(data_job);
                    //text.Text = localXml;

                    ddprovinceeducation_edit.DataSource = dd3x.ddlocationstart;
                    ddprovinceeducation_edit.DataTextField = "ProvName";
                    ddprovinceeducation_edit.DataValueField = "ProvIDX";
                    ddprovinceeducation_edit.DataBind();
                    ddprovinceeducation_edit.SelectedValue = ViewState["ddprovinceeducation_edit_"].ToString();

                    
                    #endregion
                    
                    #region edit dropdown Addyearattendedstart

                    var Addyearattendedstart_edit = (DropDownList)e.Row.FindControl("Addyearattendedstart_edit");
                    Addyearattendedstart_edit.AppendDataBoundItems = true;
                    Addyearattendedstart_edit.Items.Add(new ListItem("เลือกปีการศึกษาที่เริ่ม", "00"));
                    var Addyearattendedstart_edit_ = ((HiddenField)e.Row.Cells[0].FindControl("Addyearattendedstart_edit_")).Value;
                    ViewState["Addyearattendedstart_edit_"] = Addyearattendedstart_edit_.ToString();
                    for (int a = 0; a < 40; a++)
                    {
                        int b = 1990;
                        int id_Y = b + a;
                        int st_id_Y = id_Y;
                        Addyearattendedstart_edit.Items.Add(new ListItem(st_id_Y.ToString(), st_id_Y.ToString()));
                    }
                    Addyearattendedstart_edit.SelectedValue = ViewState["Addyearattendedstart_edit_"].ToString();

                    #endregion

                    #region edit dropdown tbtoattended_edit

                    var tbtoattended_edit = (DropDownList)e.Row.FindControl("tbtoattended_edit");
                    tbtoattended_edit.AppendDataBoundItems = true;
                    tbtoattended_edit.Items.Add(new ListItem("เลือกปีการศึกษาที่จบ", "00"));
                    var tbtoattended_edit_ = ((HiddenField)e.Row.Cells[0].FindControl("tbtoattended_edit_")).Value;
                    ViewState["tbtoattended_edit_"] = tbtoattended_edit_.ToString();
                    for (int a = 0; a < 40; a++)
                    {
                        int b = 1990;
                        int id_Y = b + a;
                        int st_id_Y = id_Y;
                        tbtoattended_edit.Items.Add(new ListItem(st_id_Y.ToString(), st_id_Y.ToString()));
                    }
                    tbtoattended_edit.SelectedValue = ViewState["tbtoattended_edit_"].ToString();

                    #endregion

                    #region edit dropdown คณะ

                    var ddFaculty_edit = (DropDownList)e.Row.FindControl("ddFaculty_edit");
                    ddFaculty_edit.AppendDataBoundItems = true;
                    ddFaculty_edit.Items.Add(new ListItem("เลือกคณะ", "00"));
                    var ddFaculty_edit_ = ((HiddenField)e.Row.Cells[0].FindControl("ddFaculty_edit_")).Value;
                    ViewState["ddFaculty_edit_"] = ddFaculty_edit_.ToString();
                    //ddFaculty_edit.Items.Clear();

                    Select_Faculty_list SelectFaculty = new Select_Faculty_list();
                    _data_job.Select_Faculty = new Select_Faculty_list[1];
                    _data_job.Select_Faculty[0] = SelectFaculty;

                    ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
                    data_job SelectFacultOut = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 71));
                    ///////////////////////////////////////////
                    //localXml = _functionWeb.ConvertObjectToXml(data_job);
                    //text.Text = localXml;

                    ddFaculty_edit.DataSource = SelectFacultOut.Select_Faculty;
                    ddFaculty_edit.DataTextField = "Faculty_Name";
                    ddFaculty_edit.DataValueField = "FacIDX";
                    ddFaculty_edit.DataBind();
                    ddFaculty_edit.SelectedValue = ViewState["ddFaculty_edit_"].ToString();

                    #endregion
                }
                break;

            case "GvReportAdd_Employee":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;

            case "GvReportAddOtherLanguages":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    #region ddLanguages
                    var tbotherLanguage_edit = (DropDownList)e.Row.FindControl("tbotherLanguage_edit");
                    var tbotherLanguage_edit_ = ((HiddenField)e.Row.Cells[0].FindControl("tbotherLanguage_edit_")).Value;
                    ViewState["tbotherLanguage_edit_"] = tbotherLanguage_edit_.ToString();
                    tbotherLanguage_list SelectDistrict = new tbotherLanguage_list();
                    _data_job.tbotherLanguage = new tbotherLanguage_list[1];
                    _data_job.tbotherLanguage[0] = SelectDistrict;

                    data_job dd25x = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 36));

                    tbotherLanguage_edit.DataSource = dd25x.tbotherLanguage;
                    tbotherLanguage_edit.DataTextField = "NameEN";
                    tbotherLanguage_edit.DataValueField = "LanIDX";
                    tbotherLanguage_edit.DataBind();
                    tbotherLanguage_edit.SelectedValue = ViewState["tbotherLanguage_edit_"].ToString();
                    #endregion

                    #region editdropdown speak

                    var status_speaking_other_edit = (RadioButtonList)e.Row.FindControl("status_speaking_other_edit");
                    var status_speaking_other_edit_ = ((HiddenField)e.Row.Cells[0].FindControl("status_speaking_other_edit_")).Value;
                    ViewState["status_speaking_other_edit_"] = status_speaking_other_edit_.ToString();

                    status_speaking_other_edit.SelectedValue = ViewState["status_speaking_other_edit_"].ToString();
                    //status_speaking_other_edit_list Selectstatus_speaking_other_edit = new status_speaking_other_edit_list();
                    //_data_job.status_speaking_other_edit = new status_speaking_other_edit_list[1];
                    //_data_job.status_speaking_other_edit[0] = Selectstatus_speaking_other_edit;

                    //data_job dd12x = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 35));

                    //status_speaking_other_edit.DataSource = dd12x.status_speaking_other_edit;
                    //status_speaking_other_edit.DataTextField = "PROFICName";
                    //status_speaking_other_edit.DataValueField = "PROFICIDX";
                    //status_speaking_other_edit.DataBind();
                    //status_speaking_other_edit.SelectedValue = ViewState["status_speaking_other_edit_"].ToString();

                    #endregion

                    #region editdropdown uderstad

                    var status_Understanding_other_edit = (RadioButtonList)e.Row.FindControl("status_Understanding_other_edit");
                    var status_Understanding_other_ = ((HiddenField)e.Row.Cells[0].FindControl("status_Understanding_other_")).Value;
                    ViewState["status_Understanding_other_"] = status_Understanding_other_.ToString();

                    status_Understanding_other_edit.SelectedValue = ViewState["status_Understanding_other_"].ToString();
                    //status_speaking_other_edit_list Selectstatus_Understanding_other_edit = new status_speaking_other_edit_list();
                    //_data_job.status_speaking_other_edit = new status_speaking_other_edit_list[1];
                    //_data_job.status_speaking_other_edit[0] = Selectstatus_Understanding_other_edit;

                    //data_job dd13x = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 35));

                    //status_Understanding_other_edit.DataSource = dd13x.status_speaking_other_edit;
                    //status_Understanding_other_edit.DataTextField = "PROFICName";
                    //status_Understanding_other_edit.DataValueField = "PROFICIDX";
                    //status_Understanding_other_edit.DataBind();
                    //status_Understanding_other_edit.SelectedValue = ViewState["status_Understanding_other_"].ToString();

                    #endregion

                    #region editdropdown Reading

                    var status_Reading_other_edit = (RadioButtonList)e.Row.FindControl("status_Reading_other_edit");
                    var status_Reading_other_ = ((HiddenField)e.Row.Cells[0].FindControl("status_Reading_other_")).Value;
                    ViewState["status_Reading_other_"] = status_Reading_other_.ToString();

                    status_Reading_other_edit.SelectedValue = ViewState["status_Reading_other_"].ToString();
                    //status_speaking_other_edit_list Selectstatus_Reading_other_edit = new status_speaking_other_edit_list();
                    //_data_job.status_speaking_other_edit = new status_speaking_other_edit_list[1];
                    //_data_job.status_speaking_other_edit[0] = Selectstatus_Reading_other_edit;

                    //data_job dd14x = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 35));

                    //status_Reading_other_edit.DataSource = dd14x.status_speaking_other_edit;
                    //status_Reading_other_edit.DataTextField = "PROFICName";
                    //status_Reading_other_edit.DataValueField = "PROFICIDX";
                    //status_Reading_other_edit.DataBind();
                    //status_Reading_other_edit.SelectedValue = ViewState["status_Reading_other_"].ToString();

                    #endregion

                    #region editdropdown Writing

                    var status_Wrinting_other_edit = (RadioButtonList)e.Row.FindControl("status_Wrinting_other_edit");
                    var status_Wrinting_other_ = ((HiddenField)e.Row.Cells[0].FindControl("status_Wrinting_other_")).Value;
                    ViewState["status_Wrinting_other_"] = status_Wrinting_other_.ToString();

                    status_Wrinting_other_edit.SelectedValue = ViewState["status_Wrinting_other_"].ToString();
                    //status_speaking_other_edit_list Selectstatus_Wrinting_other_edit = new status_speaking_other_edit_list();
                    //_data_job.status_speaking_other_edit = new status_speaking_other_edit_list[1];
                    //_data_job.status_speaking_other_edit[0] = Selectstatus_Wrinting_other_edit;

                    //data_job dd15x = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 35));

                    //status_Wrinting_other_edit.DataSource = dd15x.status_speaking_other_edit;
                    //status_Wrinting_other_edit.DataTextField = "PROFICName";
                    //status_Wrinting_other_edit.DataValueField = "PROFICIDX";
                    //status_Wrinting_other_edit.DataBind();
                    //status_Wrinting_other_edit.SelectedValue = ViewState["status_Wrinting_other_"].ToString();

                    #endregion
                }
                break;
        }
    }
    #endregion

    #region Gvbuyequipment_RowDeleting

    protected void Gvbuyequipment_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;
        GridView GvReportAddparent = (GridView)FormView2.FindControl("GvReportAddparent");
        GridView GvReportAdd_Education = (GridView)FormView3.FindControl("GvReportAdd_Education");
        GridView GvReportAdd_Employee = (GridView)FormView4.FindControl("GvReportAdd_Employee");
        GridView GvReportAddOtherLanguages = (GridView)FormView5.FindControl("GvReportAddOtherLanguages");
        GridView GvReportAdd_Car = (GridView)FormView5.FindControl("GvReportAdd_Car");
        GridView gvDocumentFile = (GridView)FormView8.FindControl("gvDocumentFile");
        switch (GvName.ID)
        {
            case "gvDocumentFile":
                gvDocumentFile.EditIndex = -1;
                Showfile();
                break;

            case "GvReportAddparent":

                var dsvsBuyequipmentDelete = (DataSet)ViewState["vsBuyequipment"];
                var drDriving = dsvsBuyequipmentDelete.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["vsBuyequipment"] = dsvsBuyequipmentDelete;
                GvReportAddparent.EditIndex = -1;
                GvReportAddparent.DataSource = ViewState["vsBuyequipment"];
                GvReportAddparent.DataBind();

                break;

            case "GvReportAdd_Education":
                var ds_EducationDelete = (DataSet)ViewState["vsBuyds_Education"];
                var drEducationDelete = ds_EducationDelete.Tables[0].Rows;

                drEducationDelete.RemoveAt(e.RowIndex);

                ViewState["vsBuyds_Education"] = ds_EducationDelete;
                GvReportAdd_Education.EditIndex = -1;
                GvReportAdd_Education.DataSource = ViewState["vsBuyds_Education"];
                GvReportAdd_Education.DataBind();
                break;

            case "GvReportAdd_Employee":
                var ds_EmployeeDelete = (DataSet)ViewState["vsBuyds_Employed"];
                var drEmployeeDelete = ds_EmployeeDelete.Tables[0].Rows;

                drEmployeeDelete.RemoveAt(e.RowIndex);

                ViewState["vsBuyds_Employed"] = ds_EmployeeDelete;
                GvReportAdd_Employee.EditIndex = -1;
                GvReportAdd_Employee.DataSource = ViewState["vsBuyds_Employed"];
                GvReportAdd_Employee.DataBind();
                break;

            case "GvReportAddOtherLanguages":
                var ds_OtherLanguagesDelete = (DataSet)ViewState["vsBuyds_dsOtherLanguages"];
                var drOtherLanguagesDelete = ds_OtherLanguagesDelete.Tables[0].Rows;

                drOtherLanguagesDelete.RemoveAt(e.RowIndex);

                ViewState["vsBuyds_dsOtherLanguages"] = ds_OtherLanguagesDelete;
                GvReportAddOtherLanguages.EditIndex = -1;
                GvReportAddOtherLanguages.DataSource = ViewState["vsBuyds_dsOtherLanguages"];
                GvReportAddOtherLanguages.DataBind();
                break;

            case "GvReportAdd_Car":
                var ds_TypeCarDelete = (DataSet)ViewState["vsBuyds_dsTypeCar"];
                var drTypeCarDelete = ds_TypeCarDelete.Tables[0].Rows;

                drTypeCarDelete.RemoveAt(e.RowIndex);

                ViewState["vsBuyds_dsTypeCar"] = ds_TypeCarDelete;
                GvReportAdd_Car.EditIndex = -1;
                GvReportAdd_Car.DataSource = ViewState["vsBuyds_dsTypeCar"];
                GvReportAdd_Car.DataBind();
                break;
        }
    }

    #endregion

    #region Master_PageIndexChanging
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvDocumentFile = (GridView)FormView8.FindControl("gvDocumentFile");
        var GvNameTEST = (GridView)sender;
        switch (GvNameTEST.ID)
        {
            case "gvDocumentFile":
                gvDocumentFile.PageIndex = e.NewPageIndex;
                gvDocumentFile.DataBind();
                Showfile();
                break;
        }
    }
    #endregion

    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        Label HChoin = (Label)FormView11.FindControl("HChoin");
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "btnLogic":
                Panel hindback = (Panel)FormView11.FindControl("hindback");
                Label TimCount = (Label)FormView11.FindControl("TimCount");
                Repeater RepeaterEmpMain1 = (Repeater)FormView11.FindControl("RepeaterEmpMain1");
                Repeater Repeater1F11 = (Repeater)FormView11.FindControl("Repeater1");
                Repeater Repeater2F11 = (Repeater)FormView11.FindControl("Repeater2");
                Repeater Repeater3F11 = (Repeater)FormView11.FindControl("Repeater3");
                Repeater Repeater4F11 = (Repeater)FormView11.FindControl("Repeater4");
                Panel hindTime = (Panel)FormView11.FindControl("hindTime");
                RandomLogic();

                #region หัวข้อ
                Label HChoinLoad = (Label)FormView11.FindControl("HChoin");
                string getC0 = HChoinLoad.Text;
                int sum0 = int.Parse(getC0.ToString()) - 1;
                int[] I0;
                ViewState["Sum"] = sum0;
                I0 = (int[])ViewState["Random"];
                for (int i = 0; i <= sum0; i++)
                {
                    ViewState["GetRan"] = I0[i];
                }

                Data_LogicOut_list Data_Logic0 = new Data_LogicOut_list();
                _data_job.Data_LogicOut = new Data_LogicOut_list[1];
                Data_Logic0.Logic_Send = int.Parse(ViewState["GetRan"].ToString());

                _data_job.Data_LogicOut[0] = Data_Logic0;

                data_job OutLogic0 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 86));

                RepeaterEmpMain1.DataSource = OutLogic0.Data_LogicOut;
                RepeaterEmpMain1.DataBind();
                #endregion

                #region Choice
                //////////////////////////////////////////////////////////////////////////////1

                Data_ChoiceOut_list Choice1 = new Data_ChoiceOut_list();
                _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                Choice1.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                _data_job.Data_ChoiceOut[0] = Choice1;

                data_job OutChoice1 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 87));


                Repeater1F11.DataSource = OutChoice1.Data_ChoiceOut;
                Repeater1F11.DataBind();

                ////////////////////////////////////////////////////////////////////////////////2
                Data_ChoiceOut_list Choice2 = new Data_ChoiceOut_list();
                _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                Choice2.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                _data_job.Data_ChoiceOut[0] = Choice2;

                data_job OutChoice2 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 88));


                Repeater2F11.DataSource = OutChoice2.Data_ChoiceOut;
                Repeater2F11.DataBind();

                ////////////////////////////////////////////////////////////////////////////////3
                Data_ChoiceOut_list Choice3 = new Data_ChoiceOut_list();
                _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                Choice3.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                _data_job.Data_ChoiceOut[0] = Choice3;

                data_job OutChoice3 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 89));


                Repeater3F11.DataSource = OutChoice3.Data_ChoiceOut;
                Repeater3F11.DataBind();

                ////////////////////////////////////////////////////////////////////////////////4
                Data_ChoiceOut_list Choice4 = new Data_ChoiceOut_list();
                _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                Choice4.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                _data_job.Data_ChoiceOut[0] = Choice4;

                data_job OutChoice4 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 91));


                Repeater4F11.DataSource = OutChoice4.Data_ChoiceOut;
                Repeater4F11.DataBind();
                #endregion

                #region ตั้งเวลา
                Session["timeS"] = DateTime.Now.AddSeconds(60);
                Session["timeM"] = DateTime.Now.AddMinutes(30);
                hindTime.Visible = true;
                #endregion

                #region ข้อ1ไม่มีปุ่มBack
                if (HChoinLoad.Text == "1")
                {
                    hindback.Visible = false;
                }
                else
                {
                    hindback.Visible = true;
                }
                #endregion

                MvtestMaster.SetActiveView(ViewAdd12);
                break;

            case "NHChoin":
                Panel hindback0 = (Panel)FormView11.FindControl("hindback");
                Repeater RepeaterEmpMain01 = (Repeater)FormView11.FindControl("RepeaterEmpMain1");
                Repeater Repeater01 = (Repeater)FormView11.FindControl("Repeater1");
                Repeater Repeater02 = (Repeater)FormView11.FindControl("Repeater2");
                Repeater Repeater03 = (Repeater)FormView11.FindControl("Repeater3");
                Repeater Repeater04 = (Repeater)FormView11.FindControl("Repeater4");

                string getC = HChoin.Text;
                int sum = int.Parse(getC.ToString()) + 1;
                ViewState["Sum"] = sum;
                HChoin.Text = ViewState["Sum"].ToString();

                if (ViewState["Sum"].ToString() != "36")
                {
                    #region หัวข้อ
                    int GetHC = int.Parse(getC.ToString()) - 1;
                    ViewState["Sum2"] = GetHC;

                    int[] I;
                    I = (int[])ViewState["Random"];
                    for (int i = 0; i < sum; i++)
                    {
                        ViewState["GetRan"] = I[i];
                    }

                    Data_LogicOut_list Data_Logic = new Data_LogicOut_list();
                    _data_job.Data_LogicOut = new Data_LogicOut_list[1];
                    Data_Logic.Logic_Send = int.Parse(ViewState["GetRan"].ToString());
                    _data_job.Data_LogicOut[0] = Data_Logic;

                    data_job OutLogic = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 86));

                    RepeaterEmpMain01.DataSource = OutLogic.Data_LogicOut;
                    RepeaterEmpMain01.DataBind();
                    #endregion

                    #region Choice
                    //////////////////////////////////////////////////////////////////////////////1

                    Data_ChoiceOut_list Choice01 = new Data_ChoiceOut_list();
                    _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                    Choice01.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                    _data_job.Data_ChoiceOut[0] = Choice01;

                    data_job OutChoice01 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 87));

                    Repeater01.DataSource = OutChoice01.Data_ChoiceOut;
                    Repeater01.DataBind();

                    //////////////////////////////////////////////////////////////////////////////2

                    Data_ChoiceOut_list Choice02 = new Data_ChoiceOut_list();
                    _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                    Choice02.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                    _data_job.Data_ChoiceOut[0] = Choice02;

                    data_job OutChoice02 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 88));

                    Repeater02.DataSource = OutChoice02.Data_ChoiceOut;
                    Repeater02.DataBind();

                    //////////////////////////////////////////////////////////////////////////////3

                    Data_ChoiceOut_list Choice03 = new Data_ChoiceOut_list();
                    _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                    Choice03.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                    _data_job.Data_ChoiceOut[0] = Choice03;

                    data_job OutChoice03 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 89));

                    Repeater03.DataSource = OutChoice03.Data_ChoiceOut;
                    Repeater03.DataBind();

                    //////////////////////////////////////////////////////////////////////////////4

                    Data_ChoiceOut_list Choice04 = new Data_ChoiceOut_list();
                    _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                    Choice04.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                    _data_job.Data_ChoiceOut[0] = Choice04;

                    data_job OutChoice04 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 91));


                    Repeater04.DataSource = OutChoice04.Data_ChoiceOut;
                    Repeater04.DataBind();
                    #endregion
                }

                #region เก็บ Choice
                ViewState["BSumGet"] = 0;
                RadioButton A = (RadioButton)FormView11.FindControl("A");
                if (A.Checked)
                {
                    ViewState["BSumGet"] = 1;
                }
                RadioButton B = (RadioButton)FormView11.FindControl("B");
                if (B.Checked)
                {
                    ViewState["BSumGet"] = 2;
                }
                RadioButton C = (RadioButton)FormView11.FindControl("C");
                if (C.Checked)
                {
                    ViewState["BSumGet"] = 3;
                }
                RadioButton D = (RadioButton)FormView11.FindControl("D");
                if (D.Checked)
                {
                    ViewState["BSumGet"] = 4;
                }
                #endregion

                #region เก็บคะแนน1
                ///////////////////////////////////////////////////////////บันทึกคะแนน
                if (ViewState["BSumGet"].ToString() == "1")
                {
                    int GetHCA = int.Parse(getC.ToString()) - 1;
                    ViewState["Sum2"] = GetHCA;
                    int[] AAnswer;
                    AAnswer = (int[])ViewState["AnswerA"];
                    for (int t = 0; t <= GetHCA; t++)
                    {
                        ViewState["SumRang"] = AAnswer[t];
                    }
                    int ARang = int.Parse(ViewState["SumRang"].ToString());
                }

                if (ViewState["BSumGet"].ToString() == "2")
                {
                    int GetHCB = int.Parse(getC.ToString()) - 1;
                    ViewState["Sum2"] = GetHCB;
                    int[] BAnswer;
                    BAnswer = (int[])ViewState["AnswerB"];
                    for (int t = 0; t <= GetHCB; t++)
                    {
                        ViewState["SumRang"] = BAnswer[t];
                    }
                    int BRang = int.Parse(ViewState["SumRang"].ToString());
                }

                if (ViewState["BSumGet"].ToString() == "3")
                {
                    int GetHCC = int.Parse(getC.ToString()) - 1;
                    ViewState["Sum2"] = GetHCC;
                    int[] CAnswer;
                    CAnswer = (int[])ViewState["AnswerC"];
                    for (int t = 0; t <= GetHCC; t++)
                    {
                        ViewState["SumRang"] = CAnswer[t];
                    }
                    int CRang = int.Parse(ViewState["SumRang"].ToString());
                }

                if (ViewState["BSumGet"].ToString() == "4")
                {
                    int GetHCD = int.Parse(getC.ToString()) - 1;
                    ViewState["Sum2"] = GetHCD;
                    int[] DAnswer;
                    DAnswer = (int[])ViewState["AnswerD"];
                    for (int t = 0; t <= GetHCD; t++)
                    {
                        ViewState["SumRang"] = DAnswer[t];
                    }
                    int DRang = int.Parse(ViewState["SumRang"].ToString());
                }
                int GetHCS = int.Parse(getC.ToString()) - 1;
                ViewState["Sum2"] = GetHCS;
                AnswerGetRang(int.Parse(ViewState["SumRang"].ToString()));
                int[] Sussess;
                Sussess = (int[])ViewState["SumRangG"];
                int M = 0;
                for (int j = 0; j <= GetHCS; j++)
                {
                    ViewState["Sussess"] = Sussess[j];
                    M += int.Parse(ViewState["Sussess"].ToString());
                    Sum.Text = M.ToString();
                    ViewState["Sum20"] = M;
                }
                #endregion

                #region เก็บ Choice
                ///////////////////////////////////////////////////////////เรียก ฟังชั่นเก็บArrayChoice
                ArraySend();
                Array(int.Parse(ViewState["BSumGet"].ToString()));
                ///////////////////////////////////////////////////////////
                #endregion

                #region เก็บช้อย Next ไป
                int[] ChoiceN;
                ViewState["GetChoiceN"] = 0;
                int m = int.Parse(ViewState["Sum2"].ToString()) + 1;//-
                ChoiceN = (int[])ViewState["BackArray0"];
                int E = ChoiceN.Length;
                int[] G;
                G = (int[])ViewState["loopw"];
                if (G[m] == 1)
                {
                    for (int n = 0; n <= m; n++)
                    {
                        ViewState["GetChoiceN"] = ChoiceN[n];
                    }
                    //Literal1.Text = ViewState["GetChoice"].ToString();
                    if (ViewState["GetChoiceN"].ToString() == "1")
                    {
                        A.Checked = true;
                        B.Checked = false;
                        C.Checked = false;
                        D.Checked = false;
                    }
                    else if (ViewState["GetChoiceN"].ToString() == "2")
                    {
                        A.Checked = false;
                        B.Checked = true;
                        C.Checked = false;
                        D.Checked = false;
                    }
                    else if (ViewState["GetChoiceN"].ToString() == "3")
                    {
                        A.Checked = false;
                        B.Checked = false;
                        C.Checked = true;
                        D.Checked = false;
                    }
                    else if (ViewState["GetChoiceN"].ToString() == "4")
                    {
                        A.Checked = false;
                        B.Checked = false;
                        C.Checked = false;
                        D.Checked = true;
                    }
                    else
                    {
                        A.Checked = false;
                        B.Checked = false;
                        C.Checked = false;
                        D.Checked = false;
                    }
                }
                else
                {
                    A.Checked = false;
                    B.Checked = false;
                    C.Checked = false;
                    D.Checked = false;
                }
                #endregion

                #region เช็คว่ากด Next ไปไหม
                int[] u;
                u = (int[])ViewState["loopw"];
                for (int i = 0; i <= int.Parse(ViewState["Sum2"].ToString()); i++)
                {
                    u[int.Parse(ViewState["Sum2"].ToString())] = 1;
                    ViewState["loopw"] = u;
                }
                //Literal2.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(u));
                #endregion

                #region ข้อ1ไม่มีปุ่มBack
                if (HChoin.Text == "1")
                {
                    hindback0.Visible = false;
                }
                else
                {
                    hindback0.Visible = true;
                }
                #endregion

                #region เก็บคะแนนลงSQL
                if (ViewState["Sum"].ToString() == "36")
                {
                    int[] Doloop;
                    Doloop = (int[])ViewState["BackArray0"];
                    for (int o = 0; o <= 34; o++)
                    {
                        if (Doloop[o] == 0)
                        {
                            ViewState["Chloop"] = 0;
                            break;
                        }
                        else
                        {
                            ViewState["Chloop"] = 1;
                        }
                    }
                    if (ViewState["Chloop"].ToString() == "0")
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกคำตอบทุกข้อ');", true);
                        #region หัวข้อ
                        int GetHC = int.Parse(getC.ToString());
                        HChoin.Text = GetHC.ToString();
                        ViewState["Sum2"] = GetHC;

                        int[] I;
                        I = (int[])ViewState["Random"];
                        for (int i = 0; i < sum - 1; i++)
                        {
                            ViewState["GetRan"] = I[i];
                        }

                        Data_LogicOut_list Data_Logic = new Data_LogicOut_list();
                        _data_job.Data_LogicOut = new Data_LogicOut_list[1];
                        Data_Logic.Logic_Send = int.Parse(ViewState["GetRan"].ToString());
                        _data_job.Data_LogicOut[0] = Data_Logic;

                        data_job OutLogic = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 86));

                        RepeaterEmpMain01.DataSource = OutLogic.Data_LogicOut;
                        RepeaterEmpMain01.DataBind();
                        #endregion

                        #region Choice
                        //////////////////////////////////////////////////////////////////////////////1

                        Data_ChoiceOut_list Choice01 = new Data_ChoiceOut_list();
                        _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                        Choice01.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                        _data_job.Data_ChoiceOut[0] = Choice01;

                        data_job OutChoice01 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 87));

                        Repeater01.DataSource = OutChoice01.Data_ChoiceOut;
                        Repeater01.DataBind();

                        //////////////////////////////////////////////////////////////////////////////2

                        Data_ChoiceOut_list Choice02 = new Data_ChoiceOut_list();
                        _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                        Choice02.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                        _data_job.Data_ChoiceOut[0] = Choice02;

                        data_job OutChoice02 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 88));

                        Repeater02.DataSource = OutChoice02.Data_ChoiceOut;
                        Repeater02.DataBind();

                        //////////////////////////////////////////////////////////////////////////////3

                        Data_ChoiceOut_list Choice03 = new Data_ChoiceOut_list();
                        _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                        Choice03.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                        _data_job.Data_ChoiceOut[0] = Choice03;

                        data_job OutChoice03 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 89));

                        Repeater03.DataSource = OutChoice03.Data_ChoiceOut;
                        Repeater03.DataBind();

                        //////////////////////////////////////////////////////////////////////////////4

                        Data_ChoiceOut_list Choice04 = new Data_ChoiceOut_list();
                        _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                        Choice04.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                        _data_job.Data_ChoiceOut[0] = Choice04;

                        data_job OutChoice04 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 91));


                        Repeater04.DataSource = OutChoice04.Data_ChoiceOut;
                        Repeater04.DataBind();
                        #endregion
                        break;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการเสร็จสิ้น กรุณารอสักครู่');", true);
                        Data_ScoreLogic_list Score_logic = new Data_ScoreLogic_list();
                        _data_job.Data_ScoreLogic = new Data_ScoreLogic_list[1];
                        Score_logic.Profile_IDXLogic = int.Parse(Session["idLogic"].ToString());
                        Score_logic.Score = int.Parse(ViewState["Sum20"].ToString());

                        _data_job.Data_ScoreLogic[0] = Score_logic;

                        data_job OutScore_logic = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 92));
                        MvtestMaster.SetActiveView(ViewAdd10);
                    }
                    break;
                }
                #endregion
                break;

            case "BHChoin":
                Panel hindback0B = (Panel)FormView11.FindControl("hindback");
                Repeater RepeaterEmpMain01B = (Repeater)FormView11.FindControl("RepeaterEmpMain1");
                Repeater RepeaterB01 = (Repeater)FormView11.FindControl("Repeater1");
                Repeater RepeaterB02 = (Repeater)FormView11.FindControl("Repeater2");
                Repeater RepeaterB03 = (Repeater)FormView11.FindControl("Repeater3");
                Repeater RepeaterB04 = (Repeater)FormView11.FindControl("Repeater4");

                #region หัวข้อ
                string getBC = HChoin.Text;
                int sumB = int.Parse(getBC.ToString()) - 1;
                ViewState["Sum"] = sumB;
                HChoin.Text = ViewState["Sum"].ToString();

                int GetHBC = int.Parse(ViewState["Sum"].ToString()) - 1;
                int[] IB;
                IB = (int[])ViewState["Random"];
                for (int i = 0; i < sumB; i++)
                {
                    ViewState["GetRan"] = IB[i];
                }

                Data_LogicOut_list Data_LogicB = new Data_LogicOut_list();
                _data_job.Data_LogicOut = new Data_LogicOut_list[1];
                Data_LogicB.Logic_Send = int.Parse(ViewState["GetRan"].ToString());
                _data_job.Data_LogicOut[0] = Data_LogicB;

                data_job OutLogicB = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 86));

                RepeaterEmpMain01B.DataSource = OutLogicB.Data_LogicOut;
                RepeaterEmpMain01B.DataBind();
                #endregion

                #region Choice
                //////////////////////////////////////////////////////////////////////////////1

                Data_ChoiceOut_list ChoiceB01 = new Data_ChoiceOut_list();
                _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                ChoiceB01.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                _data_job.Data_ChoiceOut[0] = ChoiceB01;

                data_job OutChoiceB01 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 87));

                RepeaterB01.DataSource = OutChoiceB01.Data_ChoiceOut;
                RepeaterB01.DataBind();

                //////////////////////////////////////////////////////////////////////////////2

                Data_ChoiceOut_list ChoiceB02 = new Data_ChoiceOut_list();
                _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                ChoiceB02.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                _data_job.Data_ChoiceOut[0] = ChoiceB02;

                data_job OutChoiceB02 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 88));

                RepeaterB02.DataSource = OutChoiceB02.Data_ChoiceOut;
                RepeaterB02.DataBind();

                //////////////////////////////////////////////////////////////////////////////3

                Data_ChoiceOut_list ChoiceB03 = new Data_ChoiceOut_list();
                _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                ChoiceB03.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                _data_job.Data_ChoiceOut[0] = ChoiceB03;

                data_job OutChoiceB03 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 89));

                RepeaterB03.DataSource = OutChoiceB03.Data_ChoiceOut;
                RepeaterB03.DataBind();

                //////////////////////////////////////////////////////////////////////////////4

                Data_ChoiceOut_list ChoiceB04 = new Data_ChoiceOut_list();
                _data_job.Data_ChoiceOut = new Data_ChoiceOut_list[1];
                ChoiceB04.Logic_IDX = int.Parse(ViewState["GetRan"].ToString());

                _data_job.Data_ChoiceOut[0] = ChoiceB04;

                data_job OutChoiceB04 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 91));

                RepeaterB04.DataSource = OutChoiceB04.Data_ChoiceOut;
                RepeaterB04.DataBind();
                #endregion

                #region Backแล้วคำตอบเดิม
                RadioButton AB = (RadioButton)FormView11.FindControl("A");
                RadioButton BB = (RadioButton)FormView11.FindControl("B");
                RadioButton CB = (RadioButton)FormView11.FindControl("C");
                RadioButton DB = (RadioButton)FormView11.FindControl("D");
                int[] ChoiceB;
                ChoiceB = (int[])ViewState["BackArray0"];
                for (int n = 0; n < sumB; n++)
                {
                    ViewState["GetChoice"] = ChoiceB[n];
                }
                //Literal1.Text = ViewState["GetChoice"].ToString();
                if (ViewState["GetChoice"].ToString() == "1")
                {
                    AB.Checked = true;
                    BB.Checked = false;
                    CB.Checked = false;
                    DB.Checked = false;
                }
                if (ViewState["GetChoice"].ToString() == "2")
                {
                    AB.Checked = false;
                    BB.Checked = true;
                    CB.Checked = false;
                    DB.Checked = false;
                }
                if (ViewState["GetChoice"].ToString() == "3")
                {
                    AB.Checked = false;
                    BB.Checked = false;
                    CB.Checked = true;
                    DB.Checked = false;
                }
                if (ViewState["GetChoice"].ToString() == "4")
                {
                    AB.Checked = false;
                    BB.Checked = false;
                    CB.Checked = false;
                    DB.Checked = true;
                }
                #endregion  

                #region ข้อ1ไม่มีปุ่มBack
                if (HChoin.Text == "1")
                {
                    hindback0B.Visible = false;
                }
                else
                {
                    hindback0B.Visible = true;
                }
                #endregion
                break;

            case "Deletefile":
                int idfiledoc = int.Parse(cmdArg);
                ViewState["idfiledoc"] = idfiledoc;
                //text.Text = ViewState["idfiledoc"].ToString();
                Insert_FileDoc_list DeletefileDocINS = new Insert_FileDoc_list();
                _data_job.Insert_FileDoc = new Insert_FileDoc_list[1];
                DeletefileDocINS.File_IDX = int.Parse(ViewState["idfiledoc"].ToString());
                _data_job.Insert_FileDoc[0] = DeletefileDocINS;

                data_job EmpIdFileDocINSDelete = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 44));
                Showfile();
                break;

            case "savefile":
                DropDownList DDTypeDoc = (DropDownList)FormView8.FindControl("DDTypeDoc");

                Insert_FileDoc_list SelectIfileDocINS = new Insert_FileDoc_list();
                _data_job.Insert_FileDoc = new Insert_FileDoc_list[1];
                SelectIfileDocINS.EmpIDXFile = int.Parse(Session["idDocfile"].ToString());
                SelectIfileDocINS.NameDoc = DDTypeDoc.SelectedValue;
                _data_job.Insert_FileDoc[0] = SelectIfileDocINS;

                data_job EmpIdFileDocINS = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 43));
                ViewState["totalpath"] = EmpIdFileDocINS.return_code;
                //text.Text = ViewState["totalpath"].ToString();
                HttpFileCollection hfcit2 = Request.Files;

                for (int ii = 0; ii < hfcit2.Count; ii++)
                {
                    HttpPostedFile hpfLo = hfcit2[ii];
                    if (hpfLo.ContentLength > 1)
                    {
                        string getPath = ConfigurationSettings.AppSettings["PathFileJobDocONLine"];
                        string RECode1 = ViewState["totalpath"].ToString();
                        //  string RECode1 = "11";
                        string fileName1 = RECode1 + ii;
                        string filePath1 = Server.MapPath(getPath + RECode1);
                        if (!Directory.Exists(filePath1))
                        {
                            Directory.CreateDirectory(filePath1);
                        }
                        string extension = Path.GetExtension(hpfLo.FileName);

                        hpfLo.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                    }
                }
                Showfile();
                break;

            case "Cmdnext":
                #region เก็บข้อมูลหน้า1
                ViewState["tbposition1"] = int.Parse(((DropDownList)fvExample.FindControl("tbposition1")).SelectedValue);
                ViewState["tbposition2"] = int.Parse(((DropDownList)fvExample.FindControl("tbposition2")).SelectedValue);
                TextBox tbmoney = (TextBox)fvExample.FindControl("tbmoney");
                ViewState["tbmoney"] = tbmoney.Text;
                TextBox AddStartdate = (TextBox)fvExample.FindControl("AddStartdate");
                #region แปลง Smalldate
                string outputtext0 = AddStartdate.Text.Substring(6) + "/" + AddStartdate.Text.Substring(3, 2) + "/" + AddStartdate.Text.Substring(0, 2);
                #endregion
                ViewState["AddStartdate"] = outputtext0;
                #endregion

                MvtestMaster.SetActiveView(ViewAdd2);
                break;

            case "Cmdbackadd1":
                MvtestMaster.SetActiveView(ViewAdd1);
                break;

            case "Cmdnext1":
                #region เก็บข้อมูลหน้า2

                ViewState["ddPrefix"] = int.Parse(((DropDownList)FormView1.FindControl("ddPrefix")).SelectedValue); //
                ViewState["ddPrefixEng"] = int.Parse(((DropDownList)FormView1.FindControl("ddPrefixEng")).SelectedValue); //
                ViewState["ddCountry"] = int.Parse(((DropDownList)FormView1.FindControl("ddCountry")).SelectedValue); //
                ViewState["ddlocationHere"] = int.Parse(((DropDownList)FormView1.FindControl("ddlocationHere")).SelectedValue); //
                ViewState["ddAmphur"] = int.Parse(((DropDownList)FormView1.FindControl("ddAmphur")).SelectedValue); //
                ViewState["ddDistrict"] = int.Parse(((DropDownList)FormView1.FindControl("ddDistrict")).SelectedValue); //
                TextBox Postcode = (TextBox)FormView1.FindControl("Postcode"); //
                ViewState["Postcode"] = Postcode.Text;
                TextBox tbname = (TextBox)FormView1.FindControl("tbname"); //
                ViewState["tbname"] = tbname.Text;
                TextBox tblastname = (TextBox)FormView1.FindControl("tblastname"); //
                ViewState["tblastname"] = tblastname.Text;
                TextBox tbnameeng = (TextBox)FormView1.FindControl("tbnameeng"); //
                ViewState["tbnameeng"] = tbnameeng.Text;
                TextBox tblastnameeng = (TextBox)FormView1.FindControl("tblastnameeng"); //
                ViewState["tblastnameeng"] = tblastnameeng.Text;
                TextBox Addbirthday = (TextBox)FormView1.FindControl("Addbirthday"); //
                #region แปลง Smalldate
                string outputtext1 = Addbirthday.Text.Substring(6) + "/" + Addbirthday.Text.Substring(3, 2) + "/" + Addbirthday.Text.Substring(0, 2);
                #endregion
                ViewState["Addbirthday"] = outputtext1;
                ViewState["ddlocationstart"] = int.Parse(((DropDownList)FormView1.FindControl("ddlocationstart")).SelectedValue); //
                ViewState["ddnationality"] = int.Parse(((DropDownList)FormView1.FindControl("ddnationality")).SelectedValue); //
                ViewState["ddrace"] = int.Parse(((DropDownList)FormView1.FindControl("ddrace")).SelectedValue); //
                ViewState["ddsex"] = int.Parse(((DropDownList)FormView1.FindControl("ddsex")).SelectedValue); //
                //ViewState["tbage"] = int.Parse(((DropDownList)FormView1.FindControl("tbage")).SelectedValue);
                ViewState["ddblood"] = int.Parse(((DropDownList)FormView1.FindControl("ddblood")).SelectedValue);
                TextBox tbweight = (TextBox)FormView1.FindControl("tbweight");
                ViewState["tbweight"] = tbweight.Text;
                TextBox tbscar = (TextBox)FormView1.FindControl("tbscar");
                ViewState["tbscar"] = tbscar.Text;
                TextBox tbheight = (TextBox)FormView1.FindControl("tbheight");
                ViewState["tbheight"] = tbheight.Text;
                ViewState["ddreligon"] = int.Parse(((DropDownList)FormView1.FindControl("ddreligon")).SelectedValue); //
                TextBox tbnumberphone = (TextBox)FormView1.FindControl("tbnumberphone"); //
                ViewState["tbnumberphone"] = tbnumberphone.Text;
                TextBox tbnumberhome = (TextBox)FormView1.FindControl("tbnumberhome"); //
                ViewState["tbnumberhome"] = tbnumberhome.Text;
                TextBox tbaddresspre = (TextBox)FormView1.FindControl("tbaddresspre"); //ที่อยู่ปัจจุบันที่ติดต่อได้สะดวก //
                ViewState["tbaddresspre"] = tbaddresspre.Text;
                TextBox tbperaddress = (TextBox)FormView1.FindControl("tbperaddress"); //
                ViewState["tbperaddress"] = tbperaddress.Text;
                TextBox tbidcard = (TextBox)FormView1.FindControl("tbidcard"); //
                ViewState["tbidcard"] = tbidcard.Text;
                TextBox tbissued_at = (TextBox)FormView1.FindControl("tbissued_at"); //
                ViewState["tbissued_at"] = tbissued_at.Text;
                TextBox tbissued_date = (TextBox)FormView1.FindControl("tbissued_date"); //
                #region แปลง Smalldate
                string outputtext2 = tbissued_date.Text.Substring(6) + "/" + tbissued_date.Text.Substring(3, 2) + "/" + tbissued_date.Text.Substring(0, 2);
                #endregion
                ViewState["tbissued_date"] = outputtext2;
                TextBox tbexpired_date = (TextBox)FormView1.FindControl("tbexpired_date"); //
                #region แปลง Smalldate
                string outputtext3 = tbexpired_date.Text.Substring(6) + "/" + tbexpired_date.Text.Substring(3, 2) + "/" + tbexpired_date.Text.Substring(0, 2);
                #endregion
                ViewState["tbexpired_date"] = outputtext3;
                TextBox tbsocialcard = (TextBox)FormView1.FindControl("tbsocialcard"); //บัตรรับรองสิทธิประกันสังคมเลขที่
                ViewState["tbsocialcard"] = tbsocialcard.Text;
                TextBox tbissued_atsocial = (TextBox)FormView1.FindControl("tbissued_atsocial"); //
                ViewState["tbissued_atsocial"] = tbissued_atsocial.Text;
                TextBox tbexpired_date_social = (TextBox)FormView1.FindControl("tbexpired_date_social"); //
                #region แปลง Smalldate
                if (tbexpired_date_social.Text != "")
                {
                    string outputtext4 = tbexpired_date_social.Text.Substring(6) + "/" + tbexpired_date_social.Text.Substring(3, 2) + "/" + tbexpired_date_social.Text.Substring(0, 2);
                    ViewState["tbexpired_date_social"] = outputtext4;
                }
                else
                {
                    ViewState["tbexpired_date_social"] = tbexpired_date_social.Text;
                }
                #endregion
                TextBox tbtaxcard = (TextBox)FormView1.FindControl("tbtaxcard"); //
                ViewState["tbtaxcard"] = tbtaxcard.Text;
                TextBox tbissued_tax = (TextBox)FormView1.FindControl("tbissued_tax"); //
                ViewState["tbissued_tax"] = tbissued_tax.Text;
                RadioButtonList Living_status = (RadioButtonList)FormView1.FindControl("Living_status"); //
                ViewState["Living_status"] = Living_status.Text;
                RadioButtonList Marital_Status = (RadioButtonList)FormView1.FindControl("Marital_Status"); //
                ViewState["Marital_Status"] = Marital_Status.Text;
                RadioButtonList If_Married = (RadioButtonList)FormView1.FindControl("If_Married");
                ViewState["If_Married"] = If_Married.Text;
                RadioButtonList spouseincome = (RadioButtonList)FormView1.FindControl("spouseincome");
                ViewState["spouseincome"] = spouseincome.Text;
                TextBox tbnamespouse = (TextBox)FormView1.FindControl("tbnamespouse");
                ViewState["tbnamespouse"] = tbnamespouse.Text;
                TextBox tblastnamespouse = (TextBox)FormView1.FindControl("tblastnamespouse");
                ViewState["tblastnamespouse"] = tblastnamespouse.Text;
                TextBox tboccupation = (TextBox)FormView1.FindControl("tboccupation");
                ViewState["tboccupation"] = tboccupation.Text;
                TextBox tbfirmaddress = (TextBox)FormView1.FindControl("tbfirmaddress");
                ViewState["tbfirmaddress"] = tbfirmaddress.Text;
                TextBox tbchildren = (TextBox)FormView1.FindControl("tbchildren"); //
                ViewState["tbchildren"] = tbchildren.Text;
                //text.Text = ViewState["tbchildren"].ToString();
                TextBox tbchildrenschool = (TextBox)FormView1.FindControl("tbchildrenschool"); //
                ViewState["tbchildrenschool"] = tbchildrenschool.Text;
                TextBox tbchildren21 = (TextBox)FormView1.FindControl("tbchildren21"); //
                ViewState["tbchildren21"] = tbchildren21.Text;
                RadioButtonList Military_Service = (RadioButtonList)FormView1.FindControl("Military_Service"); //
                ViewState["Military_Service"] = Military_Service.Text;
                TextBox tbNickname = (TextBox)FormView1.FindControl("tbNickname");
                ViewState["tbNickname"] = tbNickname.Text;
                TextBox tbNickEng = (TextBox)FormView1.FindControl("tbNickEng");
                ViewState["tbNickEng"] = tbNickEng.Text;
                TextBox tbemail = (TextBox)FormView1.FindControl("tbemail");
                ViewState["tbemail"] = tbemail.Text;

                #endregion
                Select_tabel_parent();
                MvtestMaster.SetActiveView(ViewAdd3);
                break;

            case "Cmdbackadd2":
                MvtestMaster.SetActiveView(ViewAdd2);
                break;

            case "Cmdnext2":
                #region เก็บข้อมูลหน้า3

                TextBox tbnamefather = (TextBox)FormView2.FindControl("tbnamefather");
                ViewState["tbnamefather"] = tbnamefather.Text;
                TextBox tblastnamefather = (TextBox)FormView2.FindControl("tblastnamefather");
                ViewState["tblastnamefather"] = tblastnamefather.Text;
                ViewState["ddage"] = int.Parse(((DropDownList)FormView2.FindControl("ddage")).SelectedValue);
                TextBox tboccupationfather = (TextBox)FormView2.FindControl("tboccupationfather");
                ViewState["tboccupationfather"] = tboccupationfather.Text;
                RadioButtonList statuslift = (RadioButtonList)FormView2.FindControl("statuslift");
                ViewState["statuslift"] = statuslift.Text;
                TextBox tbnamemother = (TextBox)FormView2.FindControl("tbnamemother");
                ViewState["tbnamemother"] = tbnamemother.Text;
                TextBox tblastnamemother = (TextBox)FormView2.FindControl("tblastnamemother");
                ViewState["tblastnamemother"] = tblastnamemother.Text;
                ViewState["ddahemother"] = int.Parse(((DropDownList)FormView2.FindControl("ddahemother")).SelectedValue);
                TextBox tboccupationmother = (TextBox)FormView2.FindControl("tboccupationmother");
                ViewState["tboccupationmother"] = tboccupationmother.Text;
                RadioButtonList statusmother = (RadioButtonList)FormView2.FindControl("statusmother");
                ViewState["statusmother"] = statusmother.Text;

                #endregion
                Select_tabel_education();
                MvtestMaster.SetActiveView(ViewAdd4);
                break;

            case "Cmdbackadd3":
                Select_tabel_parent();
                MvtestMaster.SetActiveView(ViewAdd3);
                //txtfocus_vLONList.Focus();
                break;

            case "Cmdnext3":
                Select_tabel_Employed();
                MvtestMaster.SetActiveView(ViewAdd5);
                //txtfocus_vLONList.Focus();
                break;

            case "Cmdbackadd4":
                Select_tabel_education();
                MvtestMaster.SetActiveView(ViewAdd4);
                //txtfocus_vLONList.Focus();
                break;

            case "Cmdnext4":
                Select_tabel_OtherLanguages();
                Select_tabel_TypeCar();
                MvtestMaster.SetActiveView(ViewAdd6);
                //txtfocus_vLONList.Focus();
                break;

            case "Cmdbackadd5":
                Select_tabel_Employed();
                MvtestMaster.SetActiveView(ViewAdd5);
                //txtfocus_vLONList.Focus();
                break;

            case "Cmdnext5":
                #region เก็บข้อมูลหน้า6
                ViewState["Check1"] = "9";
                ViewState["Check2"] = "9";
                ViewState["Check3"] = "9";
                TextBox tbcom_pro = (TextBox)FormView5.FindControl("tbcom_pro");
                ViewState["tbcom_pro"] = tbcom_pro.Text;
                TextBox tbtypingth = (TextBox)FormView5.FindControl("tbtypingth");
                ViewState["tbtypingth"] = tbtypingth.Text;
                TextBox tbtypingeng = (TextBox)FormView5.FindControl("tbtypingeng");
                ViewState["tbtypingeng"] = tbtypingeng.Text;
                ViewState["LThai"] = 116;
                ViewState["LEng"] = 1;
                RadioButtonList LthaiSP = (RadioButtonList)FormView5.FindControl("LthaiSP");
                ViewState["LthaiSP"] = LthaiSP.SelectedValue;
                RadioButtonList LthaiUST = (RadioButtonList)FormView5.FindControl("LthaiUST");
                ViewState["LthaiUST"] = LthaiUST.SelectedValue;
                RadioButtonList LthaiRD = (RadioButtonList)FormView5.FindControl("LthaiRD");
                ViewState["LthaiRD"] = LthaiRD.SelectedValue;
                RadioButtonList LthaiWT = (RadioButtonList)FormView5.FindControl("LthaiWT");
                ViewState["LthaiWT"] = LthaiWT.SelectedValue;
                RadioButtonList LEngSP = (RadioButtonList)FormView5.FindControl("LEngSP");
                ViewState["LEngSP"] = LEngSP.SelectedValue;
                RadioButtonList LEngUST = (RadioButtonList)FormView5.FindControl("LEngUST");
                ViewState["LEngUST"] = LEngUST.SelectedValue;
                RadioButtonList LEngRD = (RadioButtonList)FormView5.FindControl("LEngRD");
                ViewState["LEngRD"] = LEngRD.SelectedValue;
                RadioButtonList LEngWT = (RadioButtonList)FormView5.FindControl("LEngWT");
                ViewState["LEngWT"] = LEngWT.SelectedValue;

                GridView GvReportAddOtherLanguages1 = (GridView)FormView5.FindControl("GvReportAddOtherLanguages");
                DropDownList tbotherLanguage2 = (DropDownList)FormView5.FindControl("tbotherLanguage");

                var dsOtherLanguages0 = (DataSet)ViewState["vsBuyds_dsOtherLanguages"];

                MvtestMaster.SetActiveView(ViewAdd7);
                //foreach (DataRow Ch0 in dsOtherLanguages0.Tables[0].Rows)
                //{
                //    ViewState["ch0"] = Ch0["tbotherLanguage"];
                //    if(ViewState["ch0"].ToString() == "1")
                //    {
                //        ViewState["Check1"] = "1";
                //    }
                //    if (ViewState["ch0"].ToString() == "116")
                //    {
                //        ViewState["Check2"] = "2";
                //    }
                //    if(ViewState["Check1"].ToString() == "1" && ViewState["Check2"].ToString() == "2")
                //    {
                //        ViewState["Check3"] = "0";
                //    }
                //    else
                //    {
                //        ViewState["Check3"] = "1";
                //    }
                //}
                //if(ViewState["Check3"].ToString() == "0")
                //{
                //    MvtestMaster.SetActiveView(ViewAdd7);
                //    //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["vsBuyds_dsOtherLanguages"]));
                //    ViewState["Check1"] = "0";
                //    ViewState["Check2"] = "0";
                //}
                //else
                //{
                //    _funcTool.showAlert(this, "กรุณาเลือกอย่างน้อยภาษาไทยและอังกฤษ");
                //    tbotherLanguage2.Focus();
                //    //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["ch0"].ToString()));
                //}
                #endregion
                break;

            case "Cmdbackadd6":
                Select_tabel_OtherLanguages();
                Select_tabel_TypeCar();
                MvtestMaster.SetActiveView(ViewAdd6);
                //txtfocus_vLONList.Focus();
                break;

            case "NextImmage":
                //txtfocus_vLONList.Focus();
                Repeater Repeater1 = (Repeater)FormView7.FindControl("Repeater1"); 
                Repeater Repeater111 = (Repeater)FormView7.FindControl("Repeater0");
                #region เก็บข้อมูลหน้า7

                TextBox tbnamepersons = (TextBox)FormView6.FindControl("tbnamepersons");
                ViewState["tbnamepersons"] = tbnamepersons.Text;
                TextBox tblastnamepersons = (TextBox)FormView6.FindControl("tblastnamepersons");
                ViewState["tblastnamepersons"] = tblastnamepersons.Text;
                TextBox tbrelationship = (TextBox)FormView6.FindControl("tbrelationship");
                ViewState["tbrelationship"] = tbrelationship.Text;
                TextBox tbfirmaddresspersons = (TextBox)FormView6.FindControl("tbfirmaddresspersons");
                ViewState["tbfirmaddresspersons"] = tbfirmaddresspersons.Text;
                TextBox tbpositionpersons = (TextBox)FormView6.FindControl("tbpositionpersons");
                ViewState["tbpositionpersons"] = tbpositionpersons.Text;
                TextBox tbtelpersons = (TextBox)FormView6.FindControl("tbtelpersons");
                ViewState["tbtelpersons"] = tbtelpersons.Text;
                ViewState["ddadvise"] = int.Parse(((DropDownList)FormView6.FindControl("ddadvise")).SelectedValue);
                ViewState["ddworkcompany"] = int.Parse(((DropDownList)FormView6.FindControl("ddworkcompany")).SelectedValue);
                ViewState["ddcriminal"] = int.Parse(((DropDownList)FormView6.FindControl("ddcriminal")).SelectedValue);
                ViewState["ddallergy"] = int.Parse(((DropDownList)FormView6.FindControl("ddallergy")).SelectedValue);
                ViewState["dddisease"] = int.Parse(((DropDownList)FormView6.FindControl("dddisease")).SelectedValue);
                ViewState["ddsick"] = int.Parse(((DropDownList)FormView6.FindControl("ddsick")).SelectedValue);
                TextBox tbaccident = (TextBox)FormView6.FindControl("tbaccident");
                ViewState["tbaccident"] = tbaccident.Text;
                TextBox tbrelation = (TextBox)FormView6.FindControl("tbrelation");
                ViewState["tbrelation"] = tbrelation.Text;
                TextBox tbtelaccident = (TextBox)FormView6.FindControl("tbtelaccident");
                ViewState["tbtelaccident"] = tbtelaccident.Text;
                TextBox tbaddresspreAccident = (TextBox)FormView6.FindControl("tbaddresspreAccident");
                ViewState["tbaddresspreAccident"] = tbaddresspreAccident.Text;
                TextBox tbpersoncompany = (TextBox)FormView6.FindControl("tbpersoncompany");
                ViewState["tbpersoncompany"] = tbpersoncompany.Text;
                TextBox tbrelationpersoncompany = (TextBox)FormView6.FindControl("tbrelationpersoncompany");
                ViewState["tbrelationpersoncompany"] = tbrelationpersoncompany.Text;

                #endregion
                Insert_Master_List();

                if(ViewState["JenCh"].ToString() != "0")
                { 
                Insert_Genlink_list GetGenLink = new Insert_Genlink_list();
                _data_job.Insert_Genlink = new Insert_Genlink_list[1];
                GetGenLink.JenCode = ViewState["JenCh"].ToString();
                _data_job.Insert_Genlink[0] = GetGenLink;
                }

                data_job Get_GenlinkOut = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 84));

                MainEmployee_insert_list SelectIdIm = new MainEmployee_insert_list();
                _data_job.MainEmployee_insert = new MainEmployee_insert_list[1];
                SelectIdIm.EmpIDXIm = int.Parse(Session["idImage"].ToString());
                _data_job.MainEmployee_insert[0] = SelectIdIm;

                ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
                data_job EmpIdIm = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 39));

                //text.Text = HttpUtility.HtmlEncode(_functionWeb.ConvertObjectToXml(EmpIdIm));
                Repeater111.DataSource = EmpIdIm.MainEmployee_insert;
                Repeater111.DataBind();
                MvtestMaster.SetActiveView(ViewAdd8);
                break;

            case "nextDoc":
                //txtfocus_vLONList.Focus();
                MvtestMaster.SetActiveView(ViewAdd9);
                Showfile();
                break;

            case "BackImmage":
                //txtfocus_vLONList.Focus();
                MvtestMaster.SetActiveView(ViewAdd8);
                break;

            case "Confirme":
                MvtestMaster.SetActiveView(ViewAdd11);
                break;

            case "Selectpo1":
                Select_tbposition1();
                break;

            case "more1":
                GridView GvReportAddparent = (GridView)FormView2.FindControl("GvReportAddparent");
                var tbnameparent1 = ((TextBox)FormView2.HeaderRow.FindControl("tbnameparent1"));
                var tblastnameparent1 = ((TextBox)FormView2.HeaderRow.FindControl("tblastnameparent1"));
                var ddageparent1 = ((DropDownList)FormView2.HeaderRow.FindControl("ddageparent1"));
                var tboccupationparent1 = ((TextBox)FormView2.HeaderRow.FindControl("tboccupationparent1"));
                var tbfirmaddressparent1 = ((TextBox)FormView2.HeaderRow.FindControl("tbfirmaddressparent1"));
                var tbnumberparent1 = ((TextBox)FormView2.HeaderRow.FindControl("tbnumberparent1"));

                if (tbnameparent1.Text != "" || tblastnameparent1.Text != "" || ddageparent1.SelectedValue != "00" || tboccupationparent1.Text != "" || tbfirmaddressparent1.Text != "" || tbnumberparent1.Text != "")
                {
                    var dsEquiment = (DataSet)ViewState["vsBuyequipment"];
                    var drEquiment = dsEquiment.Tables[0].NewRow();

                    drEquiment["tbnameparent1"] = tbnameparent1.Text;
                    drEquiment["tblastnameparent1"] = tblastnameparent1.Text;
                    drEquiment["ddageparent1"] = ddageparent1.Text;
                    drEquiment["tboccupationparent1"] = tboccupationparent1.Text;
                    drEquiment["tbfirmaddressparent1"] = tbfirmaddressparent1.Text;
                    drEquiment["tbnumberparent1"] = tbnumberparent1.Text;

                    dsEquiment.Tables[0].Rows.Add(drEquiment);
                    ViewState["vsBuyequipment"] = dsEquiment;

                    GvReportAddparent.DataSource = dsEquiment.Tables[0];
                    GvReportAddparent.DataBind();
                }
                else
                {
                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ข้อมูล');", true);
                }
                tbnameparent1.Text = String.Empty;
                tblastnameparent1.Text = String.Empty;
                tboccupationparent1.Text = String.Empty;
                ddageparent1.SelectedValue = "00";
                tbfirmaddressparent1.Text = String.Empty;
                tbnumberparent1.Text = String.Empty;
                break;

            case "more2":
                GridView GvReportAdd_Education = (GridView)FormView3.FindControl("GvReportAdd_Education");
                var tbinstitute = ((TextBox)FormView3.HeaderRow.FindControl("tbinstitute"));
                var ddeducation = ((DropDownList)FormView3.HeaderRow.FindControl("ddeducation"));
                var ddprovinceeducation = ((DropDownList)FormView3.HeaderRow.FindControl("ddprovinceeducation"));
                var Addyearattendedstart = ((DropDownList)FormView3.HeaderRow.FindControl("Addyearattendedstart"));
                var tbtoattended = ((DropDownList)FormView3.HeaderRow.FindControl("tbtoattended"));
                var tbcourse = ((TextBox)FormView3.HeaderRow.FindControl("tbcourse"));
                var ddcompleted = ((DropDownList)FormView3.HeaderRow.FindControl("ddcompleted"));
                var ddFaculty = ((DropDownList)FormView3.HeaderRow.FindControl("ddFaculty"));
                var tbGpa = ((TextBox)FormView3.HeaderRow.FindControl("tbGpa"));

                if (tbinstitute.Text != "" || ddeducation.SelectedValue != "00" || ddprovinceeducation.SelectedValue != "00" || Addyearattendedstart.SelectedValue != "00" || tbtoattended.SelectedValue != "00" || tbcourse.Text != "" || ddcompleted.SelectedValue != "00" || ddFaculty.SelectedValue != "00" || tbGpa.Text != "")
                {
                    var ds_Education = (DataSet)ViewState["vsBuyds_Education"];
                    var dr_Education = ds_Education.Tables[0].NewRow();

                    dr_Education["tbinstitute"] = tbinstitute.Text;
                    dr_Education["ddeducation"] = ddeducation.Text;
                    //text.Text = dr_Education["ddeducation"].ToString();
                    dr_Education["ddprovinceeducation"] = ddprovinceeducation.Text;
                    dr_Education["Addyearattendedstart"] = Addyearattendedstart.Text;
                    dr_Education["tbtoattended"] = tbtoattended.Text;
                    dr_Education["tbcourse"] = tbcourse.Text;
                    dr_Education["ddcompleted"] = ddcompleted.Text;
                    dr_Education["tbGpa"] = tbGpa.Text;
                    dr_Education["ddFaculty"] = ddFaculty.Text;
                    dr_Education["ddeducation_txt"] = ddeducation.SelectedItem;
                    dr_Education["ddprovinceeducation_txt"] = ddprovinceeducation.SelectedItem;
                    dr_Education["ddcompleted_txt"] = ddcompleted.SelectedItem;
                    dr_Education["ddFaculty_txt"] = ddFaculty.SelectedItem;

                    ds_Education.Tables[0].Rows.Add(dr_Education);
                    ViewState["vsBuyds_Education"] = ds_Education;

                    GvReportAdd_Education.DataSource = ds_Education.Tables[0];
                    GvReportAdd_Education.DataBind();

                    tbinstitute.Text = String.Empty;
                    ddeducation.SelectedValue = "00";
                    ddprovinceeducation.SelectedValue = "00";
                    Addyearattendedstart.SelectedValue = "00";
                    tbtoattended.SelectedValue = "00";
                    tbcourse.Text = String.Empty;
                    ddcompleted.SelectedValue = "00";
                    tbGpa.Text = String.Empty;
                    ddFaculty.SelectedValue = "00";
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ข้อมูล');", true);
                }
                break;

            case "more3":
                GridView GvReportAdd_Employee = (GridView)FormView4.FindControl("GvReportAdd_Employee");
                var tbnameemployed = ((TextBox)FormView4.HeaderRow.FindControl("tbnameemployed"));
                var tbtelbusiness = ((TextBox)FormView4.HeaderRow.FindControl("tbtelbusiness"));
                var tbtypeBusiness = ((TextBox)FormView4.HeaderRow.FindControl("tbtypeBusiness"));
                var tbpositionbusiness = ((TextBox)FormView4.HeaderRow.FindControl("tbpositionbusiness"));
                var tbaddressbusiness = ((TextBox)FormView4.HeaderRow.FindControl("tbaddressbusiness"));
                var Addyearattendedstart_emp = ((TextBox)FormView4.HeaderRow.FindControl("Addyearattendedstart_emp"));
                var tbtoattended_emp = ((TextBox)FormView4.HeaderRow.FindControl("tbtoattended_emp"));
                var tbsupervisor = ((TextBox)FormView4.HeaderRow.FindControl("tbsupervisor"));
                var tbsalary_emp = ((TextBox)FormView4.HeaderRow.FindControl("tbsalary_emp"));
                var tbrevenue = ((TextBox)FormView4.HeaderRow.FindControl("tbrevenue"));
                var tbwork = ((TextBox)FormView4.HeaderRow.FindControl("tbwork"));
                var tbResignation = ((TextBox)FormView4.HeaderRow.FindControl("tbResignation"));

                if (tbnameemployed.Text != "" || tbtelbusiness.Text != "" || tbtypeBusiness.Text != "" || tbpositionbusiness.Text != "" || tbaddressbusiness.Text != "" || Addyearattendedstart_emp.Text != "" || tbtoattended_emp.Text != "" || tbsupervisor.Text != "" || tbsalary_emp.Text != "" || tbrevenue.Text != "" || tbwork.Text != "" || tbResignation.Text != "")
                {
                    var ds_Employed = (DataSet)ViewState["vsBuyds_Employed"];
                    var dr_Employed = ds_Employed.Tables[0].NewRow();

                    dr_Employed["tbnameemployed"] = tbnameemployed.Text;
                    dr_Employed["tbtelbusiness"] = tbtelbusiness.Text;
                    dr_Employed["tbtypeBusiness"] = tbtypeBusiness.Text;
                    dr_Employed["tbpositionbusiness"] = tbpositionbusiness.Text;
                    dr_Employed["tbaddressbusiness"] = tbaddressbusiness.Text;
                    dr_Employed["Addyearattendedstart_emp"] = Addyearattendedstart_emp.Text;
                    dr_Employed["tbtoattended_emp"] = tbtoattended_emp.Text;
                    dr_Employed["tbsupervisor"] = tbsupervisor.Text;
                    dr_Employed["tbsalary_emp"] = tbsalary_emp.Text;
                    dr_Employed["tbrevenue"] = tbrevenue.Text;
                    dr_Employed["tbwork"] = tbwork.Text;
                    dr_Employed["tbResignation"] = tbResignation.Text;
                    //dr_Employed["ddpositionbusiness_txt"] = ddpositionbusiness.SelectedItem;

                    ds_Employed.Tables[0].Rows.Add(dr_Employed);
                    ViewState["vsBuyds_Employed"] = ds_Employed;

                    GvReportAdd_Employee.DataSource = ds_Employed.Tables[0];
                    GvReportAdd_Employee.DataBind();

                    tbnameemployed.Text = String.Empty;
                    tbtelbusiness.Text = String.Empty;
                    tbtypeBusiness.Text = String.Empty;
                    tbpositionbusiness.Text = String.Empty;
                    tbaddressbusiness.Text = String.Empty;
                    Addyearattendedstart_emp.Text = String.Empty;
                    tbtoattended_emp.Text = String.Empty;
                    tbsupervisor.Text = String.Empty;
                    tbrevenue.Text = String.Empty;
                    tbwork.Text = String.Empty;
                    tbsalary_emp.Text = String.Empty;
                    tbResignation.Text = String.Empty;
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ข้อมูล');", true);
                }
                break;

            case "more4":
                GridView GvReportAddOtherLanguages = (GridView)FormView5.FindControl("GvReportAddOtherLanguages");
                var tbotherLanguage = ((DropDownList)FormView5.HeaderRow.FindControl("tbotherLanguage"));
                var tbotherLanguage0 = ((DropDownList)FormView5.HeaderRow.FindControl("tbotherLanguage")).SelectedValue;
                ViewState["tbotherLanguage"] = tbotherLanguage.SelectedValue;
                var status_speaking_otherx = ((RadioButtonList)FormView5.HeaderRow.FindControl("status_speaking_other"));
                var status_Understanding_otherx = ((RadioButtonList)FormView5.HeaderRow.FindControl("status_Understanding_other"));
                var status_Reading_otherx = ((RadioButtonList)FormView5.HeaderRow.FindControl("status_Reading_other"));
                var status_Wrinting_otherx = ((RadioButtonList)FormView5.HeaderRow.FindControl("status_Wrinting_other"));
                var status_speaking_other = ((RadioButtonList)FormView5.HeaderRow.FindControl("status_speaking_other")).SelectedValue;
                var status_Understanding_other = ((RadioButtonList)FormView5.HeaderRow.FindControl("status_Understanding_other")).SelectedValue;
                var status_Reading_other = ((RadioButtonList)FormView5.HeaderRow.FindControl("status_Reading_other")).SelectedValue;
                var status_Wrinting_other = ((RadioButtonList)FormView5.HeaderRow.FindControl("status_Wrinting_other")).SelectedValue;

                if (tbotherLanguage.SelectedValue != "00" && status_speaking_other != "" && status_Understanding_other != "" && status_Reading_other != "" && status_Wrinting_other != "")
                {
                    Select_tabel_OtherLanguages();
                    var dsOtherLanguages = (DataSet)ViewState["vsBuyds_dsOtherLanguages"];
                    var drOtherLanguages = dsOtherLanguages.Tables[0].NewRow();
                    int numrow = dsOtherLanguages.Tables[0].Rows.Count;
                    if (numrow > 0)
                    {
                        foreach (DataRow Ch in dsOtherLanguages.Tables[0].Rows)
                        {
                            ViewState["ch"] = Ch["tbotherLanguage"];
                            if (ViewState["tbotherLanguage"].ToString() == ViewState["ch"].ToString())
                            {
                                ViewState["CheckDataset"] = "0";
                                break;
                            }
                            else
                            {
                                ViewState["CheckDataset"] = "1";
                            }
                        }
                        if(ViewState["CheckDataset"].ToString() == "1")
                        {
                            drOtherLanguages["tbotherLanguage"] = tbotherLanguage.Text;
                            drOtherLanguages["status_speaking_other"] = status_speaking_other.ToString();
                            drOtherLanguages["status_Understanding_other"] = status_Understanding_other.ToString();
                            drOtherLanguages["status_Reading_other"] = status_Reading_other.ToString();
                            drOtherLanguages["status_Wrinting_other"] = status_Wrinting_other.ToString();
                            drOtherLanguages["status_speaking_other_txt"] = status_speaking_otherx.SelectedItem;
                            drOtherLanguages["status_Understanding_other_txt"] = status_Understanding_otherx.SelectedItem;
                            drOtherLanguages["status_Reading_other_txt"] = status_Reading_otherx.SelectedItem;
                            drOtherLanguages["status_Wrinting_other_txt"] = status_Wrinting_otherx.SelectedItem;
                            drOtherLanguages["tbotherLanguage_txt"] = tbotherLanguage.SelectedItem;

                            dsOtherLanguages.Tables[0].Rows.Add(drOtherLanguages);
                            ViewState["vsBuyds_dsOtherLanguages"] = dsOtherLanguages;

                            GvReportAddOtherLanguages.DataSource = dsOtherLanguages.Tables[0];
                            GvReportAddOtherLanguages.DataBind();

                            tbotherLanguage.SelectedValue = "00";
                            status_speaking_otherx.ClearSelection();
                            status_Understanding_otherx.ClearSelection();
                            status_Reading_otherx.ClearSelection();
                            status_Wrinting_otherx.ClearSelection();

                            //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["tbotherLanguage"]));
                            //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["vsBuyds_dsOtherLanguages"]));
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ');", true);
                            //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["vsBuyds_dsOtherLanguages"]));
                            break;
                        }
                    }
                    else
                    {
                        drOtherLanguages["tbotherLanguage"] = tbotherLanguage.Text;
                        drOtherLanguages["status_speaking_other"] = status_speaking_other.ToString();
                        drOtherLanguages["status_Understanding_other"] = status_Understanding_other.ToString();
                        drOtherLanguages["status_Reading_other"] = status_Reading_other.ToString();
                        drOtherLanguages["status_Wrinting_other"] = status_Wrinting_other.ToString();
                        drOtherLanguages["status_speaking_other_txt"] = status_speaking_otherx.SelectedItem;
                        drOtherLanguages["status_Understanding_other_txt"] = status_Understanding_otherx.SelectedItem;
                        drOtherLanguages["status_Reading_other_txt"] = status_Reading_otherx.SelectedItem;
                        drOtherLanguages["status_Wrinting_other_txt"] = status_Wrinting_otherx.SelectedItem;
                        drOtherLanguages["tbotherLanguage_txt"] = tbotherLanguage.SelectedItem;

                        dsOtherLanguages.Tables[0].Rows.Add(drOtherLanguages);
                        ViewState["vsBuyds_dsOtherLanguages"] = dsOtherLanguages;

                        GvReportAddOtherLanguages.DataSource = dsOtherLanguages.Tables[0];
                        GvReportAddOtherLanguages.DataBind();

                        tbotherLanguage.SelectedValue = "00";
                        status_speaking_otherx.ClearSelection();
                        status_Understanding_otherx.ClearSelection();
                        status_Reading_otherx.ClearSelection();
                        status_Wrinting_otherx.ClearSelection();

                        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["tbotherLanguage"]));
                        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Ch["tbotherLanguage"]));
                    }
                    
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ข้อมูล/กรอกข้อมูลให้ครบถ้วน');", true);
                }
                break;

            case "more5":
                GridView GvReportAdd_Car = (GridView)FormView5.FindControl("GvReportAdd_Car");
                var ddTypesOfCar = ((DropDownList)FormView5.HeaderRow.FindControl("ddTypesOfCar"));
                var tbdriving = ((TextBox)FormView5.HeaderRow.FindControl("tbdriving"));

                if (ddTypesOfCar.SelectedValue != "00" && tbdriving.Text != "")
                {
                    var ds_TypeCar = (DataSet)ViewState["vsBuyds_dsTypeCar"];
                    var dr_TypeCar = ds_TypeCar.Tables[0].NewRow();

                    dr_TypeCar["ddTypesOfCar"] = ddTypesOfCar.Text;
                    dr_TypeCar["tbdriving"] = tbdriving.Text;
                    dr_TypeCar["ddTypesOfCar_txt"] = ddTypesOfCar.SelectedItem;

                    ds_TypeCar.Tables[0].Rows.Add(dr_TypeCar);
                    ViewState["vsBuyds_dsTypeCar"] = ds_TypeCar;

                    GvReportAdd_Car.DataSource = ds_TypeCar.Tables[0];
                    GvReportAdd_Car.DataBind();

                    tbdriving.Text = String.Empty;
                    ddTypesOfCar.SelectedValue = "00";
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ข้อมูล/กรอกข้อมูลให้ครบถ้วน');", true);
                }
                break;

            case "save":
                HttpFileCollection hfcit1 = Request.Files;

                for (int ii = 0; ii < hfcit1.Count; ii++)
                {
                    HttpPostedFile hpfLo = hfcit1[ii];
                    if (hpfLo.ContentLength > 1)
                    {
                        string getPath = ConfigurationSettings.AppSettings["PathFileJobONLine"];
                        string RECode1 = Session["idImage"].ToString();
                        //  string RECode1 = "11";
                        string fileName1 = RECode1 + ii;
                        string filePath1 = Server.MapPath(getPath + RECode1);
                        if (!Directory.Exists(filePath1))
                        {
                            Directory.CreateDirectory(filePath1);
                        }
                        string extension = Path.GetExtension(hpfLo.FileName);

                        hpfLo.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                    }
                }

                Repeater Repeater0 = (Repeater)FormView7.FindControl("Repeater0");
                Panel hiImNo = (Panel)FormView7.FindControl("hiImNo");
                string getPath2 = ConfigurationSettings.AppSettings["PathFileJobONLine"];
                if (Directory.Exists(Server.MapPath(getPath2 + Session["idImage"].ToString())))
                {
                    hiImNo.Visible = false;
                    Repeater0.Visible = true;
                }
                else
                {
                    hiImNo.Visible = true;
                    Repeater0.Visible = false;
                }
                MvtestMaster.SetActiveView(ViewAdd8);

                break;
        }
    }

    #endregion

    #region returnpart 
    protected string retrunpat(int x)
    {
        string getPath = ConfigurationSettings.AppSettings["PathFileJobONLine"];
        string pat = getPath + x.ToString() + "/" + x.ToString() + "0.jpg";
        return pat;
    }
    protected string retrunpatFile(string x)
    {
        string getPath = ConfigurationSettings.AppSettings["PathFileJobDocONLine"];
        string pat = getPath + Session["idDocfile"].ToString() + x.ToString() + "/" + Session["idDocfile"].ToString() + x.ToString() + "0.jpg";
        return pat;

    }
    #endregion

    #region chkSelectedIndexChanged เชคแล้วแสดง
    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        var gv = (RadioButtonList)sender;
        Panel ifmarried = (Panel)FormView1.FindControl("ifmarried");
        Panel Chind = (Panel)FormView1.FindControl("Chind");
        string Marital_StatusX = ((RadioButtonList)FormView1.FindControl("Marital_Status")).SelectedValue;
        string Military_Service = ((RadioButtonList)FormView1.FindControl("Military_Service")).SelectedValue;
        CheckBox hide_parent = (CheckBox)FormView2.FindControl("hide_parent");
        TextBox tbMilitaryother = (TextBox)FormView1.FindControl("tbMilitaryother");
        //_funcTool.showAlert(this, Marital_StatusX); //ฟังชั่นโชว Alert โชวเลย

        switch (gv.ID)
        {
            case "Marital_Status":
                if (Marital_StatusX == "2")
                {
                    ifmarried.Visible = true;
                    Chind.Visible = true;
                }
                else if (Marital_StatusX == "1")
                {
                    ifmarried.Visible = false;
                    Chind.Visible = false;
                }
                else if (Marital_StatusX == "3")
                {
                    ifmarried.Visible = false;
                    Chind.Visible = true;
                }
                else if (Marital_StatusX == "4")
                {
                    ifmarried.Visible = false;
                    Chind.Visible = true;
                }
                else
                {
                    ifmarried.Visible = false;
                    Chind.Visible = true;
                }
                break;
        }
    }

    protected void Checkbox(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        Panel hide_parent_1 = (Panel)FormView2.FindControl("hide_parent_1");
        Panel hide_parent_2 = (Panel)FormView2.FindControl("hide_parent_2");
        CheckBox hide_parent = (CheckBox)FormView2.FindControl("hide_parent");
        Panel hide_education_1 = (Panel)FormView3.FindControl("hide_education_1");
        Panel hide_education_2 = (Panel)FormView3.FindControl("hide_education_2");
        CheckBox hide_education = (CheckBox)FormView3.FindControl("hide_education");
        Panel hide_Emp_1 = (Panel)FormView4.FindControl("hide_Emp_1");
        Panel hide_Emp_2 = (Panel)FormView4.FindControl("hide_Emp_2");
        CheckBox hide_Emp = (CheckBox)FormView4.FindControl("hide_Emp");
        Panel hide_Drive_1 = (Panel)FormView5.FindControl("hide_Drive_1");
        Panel hide_Drive_2 = (Panel)FormView5.FindControl("hide_Drive_2");
        CheckBox hide_drive = (CheckBox)FormView5.FindControl("hide_drive");
        switch (cb.ID)
        {
            case "hide_parent":
                if (hide_parent.Checked)
                {
                    hide_parent_1.Visible = false;
                    hide_parent_2.Visible = false;
                }
                else
                {
                    hide_parent_1.Visible = true;
                    hide_parent_2.Visible = true;
                }
                break;

            case "hide_education":
                if (hide_education.Checked)
                {
                    hide_education_1.Visible = false;
                    hide_education_2.Visible = false;
                }
                else
                {
                    hide_education_1.Visible = true;
                    hide_education_2.Visible = true;
                }
                break;

            case "hide_Emp":
                if (hide_Emp.Checked)
                {
                    hide_Emp_1.Visible = false;
                    hide_Emp_2.Visible = false;
                }
                else
                {
                    hide_Emp_1.Visible = true;
                    hide_Emp_2.Visible = true;
                }
                break;

            case "hide_drive":
                if (hide_drive.Checked)
                {
                    hide_Drive_1.Visible = false;
                    hide_Drive_2.Visible = false;
                }
                else
                {
                    hide_Drive_1.Visible = true;
                    hide_Drive_2.Visible = true;
                }
                break;
        }
    }

    protected void selectchang_dd(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        Panel hideMilitary_Service = (Panel)FormView1.FindControl("hideMilitary_Service");
        RadioButtonList Military_Service = (RadioButtonList)FormView1.FindControl("Military_Service");
        int ddsex = int.Parse(((DropDownList)FormView1.FindControl("ddsex")).SelectedValue);
        DropDownList SexAuto = (DropDownList)FormView1.FindControl("ddsex");
        ViewState["Prefix"] = int.Parse(((DropDownList)FormView1.FindControl("ddPrefix")).SelectedValue);
        DropDownList ddPrefixEng  = (DropDownList)FormView1.FindControl("ddPrefixEng");
        ViewState["PrefixEng"] = int.Parse(((DropDownList)FormView1.FindControl("ddPrefixEng")).SelectedValue);
        switch (ddName.ID)
        {
            case "ddsex":
                if (ddsex == 2)
                {
                    hideMilitary_Service.Visible = false;
                    Military_Service.SelectedValue = "3";
                }
                else if (ddsex == 1)
                {
                    hideMilitary_Service.Visible = true;
                }
                break;

            case "ddPrefix":
                ddPrefixEng.SelectedValue = ViewState["Prefix"].ToString();
                if(ViewState["Prefix"].ToString() == "1")
                {
                    SexAuto.SelectedValue = "1";
                    hideMilitary_Service.Visible = true;
                    Military_Service.SelectedValue = null;
                }
                else
                {
                    SexAuto.SelectedValue = "2";
                    hideMilitary_Service.Visible = false;
                    Military_Service.SelectedValue = "3";
                }
                break;

            case "ddPrefixEng":
                ddPrefix.SelectedValue = ViewState["PrefixEng"].ToString();
                if (ViewState["PrefixEng"].ToString() == "1")
                {
                    SexAuto.SelectedValue = "1";
                    hideMilitary_Service.Visible = true;
                    Military_Service.SelectedValue = null;
                }
                else
                {
                    SexAuto.SelectedValue = "2";
                    hideMilitary_Service.Visible = false;
                    Military_Service.SelectedValue = "3";
                }
                break;

            case "tbotherLanguage":
                int tbotherLanguage = int.Parse(((DropDownList)FormView5.FindControl("tbotherLanguage")).SelectedValue);
                if(tbotherLanguage != 0)
                {
                    hindLOther.Visible = true;
                }
                else
                {
                    hindLOther.Visible = false;
                }
                break;
        }
    }

    protected void TextChanged(object sender, EventArgs e)
    {
        var Txt = (TextBox)sender;
        RadioButtonList Military_Service = (RadioButtonList)FormView1.FindControl("Military_Service");
        switch (Txt.ID)
        {
            case "tbMilitaryother":
                Military_Service.ClearSelection();
                break;
        }
    }
    #endregion

    #region เวลานับถอยหลัง
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        Label TimCount = (Label)FormView11.FindControl("TimCount");
        TimeSpan time1 = new TimeSpan();
        TimeSpan time2 = new TimeSpan();
        time1 = (DateTime)Session["timeS"] - DateTime.Now;
        time2 = (DateTime)Session["timeM"] - DateTime.Now;
        if (time1.Seconds < 0)
        {
            Session["timeS"] = DateTime.Now.AddSeconds(59);
            if (time2.Minutes == 0)
            {
                Timer1.Enabled = false;
                ViewState["Timeout"] = "999";
                Data_ScoreLogic_list Score_logic = new Data_ScoreLogic_list();
                _data_job.Data_ScoreLogic = new Data_ScoreLogic_list[1];
                Score_logic.Profile_IDXLogic = 1;//int.Parse(ViewState[""].ToString());
                Score_logic.Score = int.Parse(ViewState["Timeout"].ToString());

                _data_job.Data_ScoreLogic[0] = Score_logic;

                data_job OutScore_logic = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 92));
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขออภัย เวลาในการทำแบบทดสอบได้หมดลงแล้ว');", true);
                MvtestMaster.SetActiveView(ViewAdd10);
            }
        }
        else
        {
            TimCount.Text = time2.Minutes.ToString() + " : " + time1.Seconds.ToString();
        }
    }
    #endregion

    #region random
    protected void RandomLogic()
    {
        var yourLength = 36;
        var rnd = new Random();

        List<int> oriArray = new List<int>();
        for (int i = 1; i < yourLength; i++)
        {
            oriArray.Add(i);
        }
        List<int> finArray = new List<int>();
        while (oriArray.Count > 0)
        {
            int pos = rnd.Next(0, oriArray.Count);
            finArray.Add(oriArray[pos]);
            oriArray.RemoveAt(pos);
            ViewState["Random"] = finArray.ToArray();
        }

        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["Random"]));
    }
    #endregion

    #region สร้าง Array
    private void ArraySend()
    {
        int[] BackArray0 = new int[1];
        if (ViewState["BackArray0"] == null)
        {
            ViewState["BackArray0"] = BackArray0;
        }

        int[] AnswerA = new int[1];
        if (ViewState["AnswerA"] == null)
        {
            ViewState["AnswerA"] = AnswerA;
        }

        int[] AnswerB = new int[1];
        if (ViewState["AnswerB"] == null)
        {
            ViewState["AnswerB"] = AnswerB;
        }

        int[] AnswerC = new int[1];
        if (ViewState["AnswerC"] == null)
        {
            ViewState["AnswerC"] = AnswerC;
        }

        int[] AnswerD = new int[1];
        if (ViewState["AnswerD"] == null)
        {
            ViewState["AnswerD"] = AnswerD;
        }

        int[] SumRangG = new int[1];
        if (ViewState["SumRangG"] == null)
        {
            ViewState["SumRangG"] = SumRangG;
        }

        int[] Arrayloop = new int[36];
        if (ViewState["loopw"] == null)
        {
            ViewState["loopw"] = Arrayloop;
        }
    }
    #endregion

    #region repeaterDatabound
    protected void Repeater1_PreRender(object sender, RepeaterItemEventArgs e)
    {
        var Repet = (Repeater)sender;
        switch (Repet.ID)
        {
            case "RepeaterEmpMain1":
                Panel HindLogic = (Panel)e.Item.FindControl("HindLogic");
                Panel gg = (Panel)e.Item.FindControl("gg");

                HindLogic.Visible = true;
                gg.Visible = false;
                break;

            case "Repeater1":
                TextBox ATrue = (TextBox)e.Item.FindControl("ATrue");
                Panel HindChoice = (Panel)e.Item.FindControl("HindChoice");
                Panel Panel1 = (Panel)e.Item.FindControl("Panel1");

                HindChoice.Visible = true;
                Panel1.Visible = false;

                int U1 = int.Parse(ATrue.Text);
                ArraySend();
                AnswerGetA(U1);
                break;

            case "Repeater2":
                TextBox BTrue = (TextBox)e.Item.FindControl("BTrue");
                Panel Panel2 = (Panel)e.Item.FindControl("Panel2");
                Panel Panel3 = (Panel)e.Item.FindControl("Panel3");

                Panel2.Visible = true;
                Panel3.Visible = false;

                int U2 = int.Parse(BTrue.Text);
                ArraySend();
                AnswerGetB(U2);
                break;

            case "Repeater3":
                TextBox CTrue = (TextBox)e.Item.FindControl("CTrue");
                Panel Panel5 = (Panel)e.Item.FindControl("Panel5");
                Panel Panel6 = (Panel)e.Item.FindControl("Panel6");

                Panel5.Visible = true;
                Panel6.Visible = false;

                int U3 = int.Parse(CTrue.Text);
                ArraySend();
                AnswerGetC(U3);
                break;

            case "Repeater4":
                TextBox DTrue = (TextBox)e.Item.FindControl("DTrue");
                Panel Panel7 = (Panel)e.Item.FindControl("Panel7");
                Panel Panel8 = (Panel)e.Item.FindControl("Panel8");

                Panel7.Visible = true;
                Panel8.Visible = false;

                int U4 = int.Parse(DTrue.Text);
                ArraySend();
                AnswerGetD(U4);
                break;
        }
    }
    #endregion

    #region returnpartรูป
    protected string retrunpat(string x)
    {
        string getPath = ConfigurationSettings.AppSettings["LogicTest"];
        string pat = getPath + x.ToString() + ".png";
        return pat;
    }

    protected string retrunpatA(string x)
    {
        string getPath = ConfigurationSettings.AppSettings["LogicTest"];
        string pat = getPath + x.ToString() + ".png";
        return pat;
    }

    protected string retrunpatB(string x)
    {
        string getPath = ConfigurationSettings.AppSettings["LogicTest"];
        string pat = getPath + x.ToString() + ".png";
        return pat;
    }

    protected string retrunpatC(string x)
    {
        string getPath = ConfigurationSettings.AppSettings["LogicTest"];
        string pat = getPath + x.ToString() + ".png";
        return pat;
    }

    protected string retrunpatD(string x)
    {
        string getPath = ConfigurationSettings.AppSettings["LogicTest"];
        string pat = getPath + x.ToString() + ".png";
        return pat;
    }
    #endregion

    #region Arrayเก็บ Choice
    private void Array(int e)
    {
        var BackArray0 = (int[])ViewState["BackArray0"];
        int V = int.Parse(ViewState["Sum2"].ToString());
        int H = V + 1;
        if (H > BackArray0.Length)
        {
            int s = Math.Max(H, 1 * BackArray0.Length);
            int[] arr = new int[s];
            for (int b = 0; b < V; b++)
            {
                arr[b] = BackArray0[b];
            }
            BackArray0 = arr;
            ViewState["BackArray0"] = BackArray0;
        }
        BackArray0[V++] = e;
        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(BackArray0));
    }

    private void AnswerGetA(int a)
    {
        Label HChoin = (Label)FormView11.FindControl("HChoin");
        string getC = HChoin.Text;

        int GetHC = int.Parse(getC.ToString()) - 1;
        ViewState["Sum2"] = GetHC;

        var AnswerA0 = (int[])ViewState["AnswerA"];
        int V = int.Parse(ViewState["Sum2"].ToString());
        int H = V + 1;
        if (H > AnswerA0.Length)
        {
            int s = Math.Max(H, 1 * AnswerA0.Length);
            int[] arr1 = new int[s];
            for (int b = 0; b < V; b++)
            {
                arr1[b] = AnswerA0[b];
            }
            AnswerA0 = arr1;
            ViewState["AnswerA"] = AnswerA0;
        }
        AnswerA0[V++] = a;
        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(AnswerA0));
    }

    private void AnswerGetB(int bb)
    {
        Label HChoin = (Label)FormView11.FindControl("HChoin");
        string getC = HChoin.Text;

        int GetHC = int.Parse(getC.ToString()) - 1;
        ViewState["Sum2"] = GetHC;

        var AnswerB0 = (int[])ViewState["AnswerB"];
        int V = int.Parse(ViewState["Sum2"].ToString());
        int H = V + 1;
        if (H > AnswerB0.Length)
        {
            int s = Math.Max(H, 1 * AnswerB0.Length);
            int[] arr2 = new int[s];
            for (int b = 0; b < V; b++)
            {
                arr2[b] = AnswerB0[b];
            }
            AnswerB0 = arr2;
            ViewState["AnswerB"] = AnswerB0;
        }
        AnswerB0[V++] = bb;
        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(AnswerB0));
    }

    private void AnswerGetC(int c)
    {
        Label HChoin = (Label)FormView11.FindControl("HChoin");
        string getC = HChoin.Text;

        int GetHC = int.Parse(getC.ToString()) - 1;
        ViewState["Sum2"] = GetHC;

        var AnswerC0 = (int[])ViewState["AnswerC"];
        int V = int.Parse(ViewState["Sum2"].ToString());
        int H = V + 1;
        if (H > AnswerC0.Length)
        {
            int s = Math.Max(H, 1 * AnswerC0.Length);
            int[] arr3 = new int[s];
            for (int b = 0; b < V; b++)
            {
                arr3[b] = AnswerC0[b];
            }
            AnswerC0 = arr3;
            ViewState["AnswerC"] = AnswerC0;
        }
        AnswerC0[V++] = c;
        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(AnswerB0));
    }

    private void AnswerGetD(int d)
    {
        Label HChoin = (Label)FormView11.FindControl("HChoin");
        string getC = HChoin.Text;

        int GetHC = int.Parse(getC.ToString()) - 1;
        ViewState["Sum2"] = GetHC;

        var AnswerD0 = (int[])ViewState["AnswerD"];
        int V = int.Parse(ViewState["Sum2"].ToString());
        int H = V + 1;
        if (H > AnswerD0.Length)
        {
            int s = Math.Max(H, 1 * AnswerD0.Length);
            int[] arr4 = new int[s];
            for (int b = 0; b < V; b++)
            {
                arr4[b] = AnswerD0[b];
            }
            AnswerD0 = arr4;
            ViewState["AnswerD"] = AnswerD0;
        }
        AnswerD0[V++] = d;
        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(AnswerB0));
    }

    private void AnswerGetRang(int R)
    {
        Label HChoin = (Label)FormView11.FindControl("HChoin");
        string getC = HChoin.Text;

        int GetHC = int.Parse(getC.ToString()) - 2;
        ViewState["Sum2"] = GetHC;

        var SumRang0 = (int[])ViewState["SumRangG"];
        int V = int.Parse(ViewState["Sum2"].ToString());
        int H = V + 1;
        if (H > SumRang0.Length)
        {
            int s = Math.Max(H, 1 * SumRang0.Length);
            int[] arrR = new int[s];
            for (int b = 0; b < V; b++)
            {
                arrR[b] = SumRang0[b];
            }
            SumRang0 = arrR;
            ViewState["SumRangG"] = SumRang0;
        }
        SumRang0[V++] = R;
        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(SumRang0));
    }
    #endregion

}