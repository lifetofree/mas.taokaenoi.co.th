﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.Data;
using System.Data.SqlClient;
using ASP;

public partial class websystem_MasterData_cen_workgroup_m0 : System.Web.UI.Page
{
    function_tool _functionTool = new function_tool();
    data_cen_master _data_cen_master = new data_cen_master();
    data_cen_master _data_cen_master2 = new data_cen_master();
    data_employee _dataEmployee = new data_employee();
    string _localJson = string.Empty;
    string textstatus = string.Empty;
    int _emp_idx = 0;
    //string textstatus = string.Empty;
    //string msg = string.Empty;
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCenMasterList"];
    static string _urlSetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlSetCenMasterList"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["forSearch"] = null;
            ViewState["forWordSearch"] = null;
            setFormData(FormViewS, FormViewMode.Insert, null);
            showWorkGroup();
            DropDownList ddl_s = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
            selectOrg(ddl_s, 0);
            FormViewS.Visible = true;
            

        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {

        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        hlSetTotop.Focus();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        switch (cmdName)
        {
            case "cmdCreate":
                FvInsertEdit.Visible = true;
                lbCreate.Visible = false;
                gvWorkgroup.Visible = false;
                FormViewS.Visible = false;
                _functionTool.setFvData(FvInsertEdit, FormViewMode.Insert, null);
                DropDownList ddlInsert = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");
                selectOrg(ddlInsert, 0);
                break;

            case "cmdSave":

                lbCreate.Visible = true;
                InsertWorkGroup(_functionTool.convertToInt(cmdArg));
                //setFormData(FormViewS, FormViewMode.Insert, null);

                break;

            case "cmdEdit":
                lbCreate.Visible = false;
                gvWorkgroup.Visible = false;
                //FormView2.Visible = true;
                FvInsertEdit.Visible = true;
                FormViewS.Visible = false;
                //search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();

                _search_cen_master_detail.s_wg_idx = cmdArg;
                _data_cen_master.master_mode = "4";
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;

                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setFvData(FvInsertEdit, FormViewMode.Edit, _data_cen_master.cen_wg_list_m0);

                DropDownList ddlEdit = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");
                //setDdlDataEdit(ddl, _data_cen_master.cen_wg_list_m0, "org_name_th", "org_idx");
                //---------------------
                ////DropDownList ddl = (DropDownList)FormView2.FindControl("DropDownList1");
                ////cen_org_detail_m0 _cen_org_detail_m0 = new cen_org_detail_m0();
                ////_data_cen_master2.master_mode = "3";
                ////_cen_org_detail_m0.org_idx = 0;

                ////_data_cen_master2.cen_org_list_m0 = new cen_org_detail_m0[1];
                ////_data_cen_master2.cen_org_list_m0[0] = _cen_org_detail_m0;

                //////litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                ////_data_cen_master2 = callServiceMaster(_urlGetCenMasterList, _data_cen_master2);
                ////setDdlDataEdit(ddl, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");

                //getOrgEdit(ddl, _data_cen_master.cen_wg_list_m0[0].org_idx);
                selectOrg(ddlEdit, _data_cen_master.cen_wg_list_m0[0].org_idx);




                //FormView2.Visible = true;

                break;

            case "editSave":

                InsertWorkGroup(_functionTool.convertToInt(cmdArg));
                ////data_cen_master _data_cen_master2 = new data_cen_master();

                //cen_wg_detail_m0 _cen_wg_detail_m0 = new cen_wg_detail_m0();
                //DropDownList dropD_status = (DropDownList)FvInsertEdit.FindControl("ddStatusedit");
                //DropDownList dropD_statusOrg = (DropDownList)FvInsertEdit.FindControl("DropDownList1");

                ////organization_details _orgDetail = new organization_details();
                //_cen_wg_detail_m0.wg_idx = int.Parse(cmdArg);
                //_cen_wg_detail_m0.wg_name_th = ((TextBox)FvInsertEdit.FindControl("tbNameTH")).Text.Trim();
                //_cen_wg_detail_m0.wg_name_en = ((TextBox)FvInsertEdit.FindControl("tbNameEN")).Text.Trim();
                //_cen_wg_detail_m0.wg_status = _functionTool.convertToInt(dropD_status.SelectedValue);
                //_cen_wg_detail_m0.org_idx = _functionTool.convertToInt(dropD_statusOrg.SelectedValue);
                //_cen_wg_detail_m0.cemp_idx = _emp_idx;
                //_data_cen_master.master_mode = "4";

                //_data_cen_master.cen_wg_list_m0 = new cen_wg_detail_m0[1];
                //_data_cen_master.cen_wg_list_m0[0] = _cen_wg_detail_m0;

                ////litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                //_data_cen_master = callServiceMaster(_urlSetCenMasterList, _data_cen_master);

                //if (_data_cen_master.return_code == 1)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                //    //FormView2.Visible = true;
                //    lbCreate.Visible = false;
                //    FormViewS.Visible = false;



                //}
                //else
                //{
                //    FvInsertEdit.Visible = false;
                //    lbCreate.Visible = true;
                //    FormViewS.Visible = true;
                //    //FormView2.Visible = false;
                //    gvWorkgroup.Visible = true;
                //    setFormData(FormViewS, FormViewMode.Insert, null);
                //    ViewState["forSearch"] = null;
                //    ViewState["forWordSearch"] = null;
                //    showWorkGroup();
                //    DropDownList ddlEditSave = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
                //    selectOrg(ddlEditSave, 0);
                //}



                break;

            case "cmdDelete":

                cmdDelete(int.Parse(cmdArg));



                break;

            case "cmdCancel":
                //setFormData(FormViewS, FormViewMode.Insert, null);
                FvInsertEdit.Visible = false;
                //FormView2.Visible = false;
                lbCreate.Visible = true;
                gvWorkgroup.Visible = true;
                FormViewS.Visible = true;
            
                //DropDownList ddl_c = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
                
                //selectOrg(ddl_c, 0);
                //ViewState["forSearch"] = null;
                if (ViewState["forSearch"] == null)
                {
                    showWorkGroup();
                }
                else
                {
                    _functionTool.setGvData(gvWorkgroup, ((data_cen_master)ViewState["forSearch"]).cen_wg_list_m0);

                }
                break;

                


            case "cmdReset":
                //FormView2.Visible = false;
                FvInsertEdit.Visible = false;
                lbCreate.Visible = true;
                gvWorkgroup.Visible = true;
                FormViewS.Visible = true;
                setFormData(FormViewS, FormViewMode.Insert, null);
                DropDownList ddl_s = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
                selectOrg(ddl_s, 0);
                ViewState["forSearch"] = null;
                ViewState["forWordSearch"] = null;
                showWorkGroup();
                break;


            case "cmdSearch":

                //DropDownList ddl_s = (DropDownList)FormViewS.FindControl("DropDownOrg_s");

                //DropDownList ddl_search = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
                //ViewState["forWordSearch"] = ddl_search.SelectedValue;
                checkBoxSearch();
                lbCreate.Visible = true;
                gvWorkgroup.Visible = true;
                FormViewS.Visible = true;
                break;

        }
    }

    protected void selectOrg(DropDownList ddlName, int _org_idx)
    {
        //DropDownList ddl = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");
        cen_org_detail_m0 _cen_org_detail_m0 = new cen_org_detail_m0();
        _data_cen_master.master_mode = "3";
        _cen_org_detail_m0.org_idx = 0;

        _data_cen_master.cen_org_list_m0 = new cen_org_detail_m0[1];
        _data_cen_master.cen_org_list_m0[0] = _cen_org_detail_m0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
        _functionTool.setDdlData(ddlName, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("---เลือกองค์กร---", ""));
        ddlName.SelectedValue = _org_idx.ToString();
    }

    protected void getOrgEdit(DropDownList ddlName, int _org_idx)
    {

        //DropDownList ddl = (DropDownList)FormView2.FindControl("DropDownOrg");
        cen_org_detail_m0 _cen_org_detail_m0 = new cen_org_detail_m0();
        _data_cen_master.master_mode = "3";
        _cen_org_detail_m0.org_idx = 0;

        _data_cen_master.cen_org_list_m0 = new cen_org_detail_m0[1];
        _data_cen_master.cen_org_list_m0[0] = _cen_org_detail_m0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
        _functionTool.setDdlData(ddlName, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
        //ddlName.Items.Insert(0, new ListItem("---เลือกองค์กร---", ""));
        ddlName.SelectedValue = _org_idx.ToString();

    }


    protected void checkBoxSearch()
    {
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        DropDownList ddl_search = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
        DropDownList ddl_status_for_s = (DropDownList)FormViewS.FindControl("DropDownstatus_s");
        TextBox searchbox = (TextBox)FormViewS.FindControl("tb_wg_s");

        //_search_cen_master_detail.s_org_name = searchbox.Text.Trim();
        _search_cen_master_detail.S_wg_name = searchbox.Text.Trim();
        _search_cen_master_detail.s_org_idx = ddl_search.SelectedValue;
        _search_cen_master_detail.s_status = ddl_status_for_s.SelectedValue;
        _data_cen_master.master_mode = "4";

        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;

        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);

        ViewState["forSearch"] = _data_cen_master;
        //ViewState["forSearch2"] = _data_cen_master;

        showWorkGroup();


    }

    protected void showWorkGroup()
    {
        DropDownList searchddl = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
        TextBox searchbox = (TextBox)FormViewS.FindControl("tb_wg_s");
        
 
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();


        if (ViewState["forSearch"] == null)
        {
            
            _search_cen_master_detail.s_org_idx = "";

            _search_cen_master_detail.S_wg_name = "";
            _data_cen_master.master_mode = "4";

            _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
            _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
            
            _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);

            //ViewState["forSearch"] = _data_cen_master;

            _functionTool.setGvData(gvWorkgroup, _data_cen_master.cen_wg_list_m0);
            
        }
        else
        {
            _functionTool.setGvData(gvWorkgroup, ((data_cen_master)ViewState["forSearch"]).cen_wg_list_m0);
        }


    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        //getWorkgroup();
    }



    protected void getWorkgroup()
    {
        data_cen_master _data_cen_master = new data_cen_master();
        cen_wg_detail_m0 _cen_wg_detail_m0 = new cen_wg_detail_m0();

        //data_cen_master _data_cen_master = new data_cen_master();
        //cen_org_detail_m0 _cen_org_detail_m0 = new cen_org_detail_m0();

        _cen_wg_detail_m0.wg_idx = 0;
        _data_cen_master.master_mode = "4";
        //_cen_org_detail_m0.org_idx = 0;
        //_data_cen_master.cen_org_list_m0 = new cen_org_detail_m0[1];
        _data_cen_master.cen_wg_list_m0 = new cen_wg_detail_m0[1];
        _data_cen_master.cen_wg_list_m0[0] = _cen_wg_detail_m0;
        //_data_cen_master.cen_org_list_m0[0] = _cen_org_detail_m0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);

        _functionTool.setGvData(gvWorkgroup, _data_cen_master.cen_wg_list_m0);

    }

    protected void InsertWorkGroup(int id)
    {

        TextBox tex_TH_name = (TextBox)FvInsertEdit.FindControl("tb_wgnameth");
        TextBox tex_EN_name = (TextBox)FvInsertEdit.FindControl("tb_wgnameen"); 
         DropDownList dropD_status = (DropDownList)FvInsertEdit.FindControl("ddStatus");
        DropDownList DropDownOrg = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");

        data_cen_master _data_cen_master = new data_cen_master();
        cen_wg_detail_m0 _cen_wg_detail_m0 = new cen_wg_detail_m0();

        //data_cen_master _data_cen_master = new data_cen_master();
        //cen_org_detail_m0 _cen_org_detail_m0 = new cen_org_detail_m0();

        _cen_wg_detail_m0.wg_idx = id;
        _cen_wg_detail_m0.wg_name_th = tex_TH_name.Text.Trim();
        _cen_wg_detail_m0.wg_name_en = tex_EN_name.Text.Trim();
        _cen_wg_detail_m0.wg_status = _functionTool.convertToInt(dropD_status.SelectedValue);
        _cen_wg_detail_m0.org_idx = _functionTool.convertToInt(DropDownOrg.SelectedValue);
        _cen_wg_detail_m0.cemp_idx = _emp_idx;
        _data_cen_master.master_mode = "4";
        //_cen_org_detail_m0.org_idx = 0;
        //_data_cen_master.cen_org_list_m0 = new cen_org_detail_m0[1];
        _data_cen_master.cen_wg_list_m0 = new cen_wg_detail_m0[1];
        _data_cen_master.cen_wg_list_m0[0] = _cen_wg_detail_m0;
        //_data_cen_master.cen_org_list_m0[0] = _cen_org_detail_m0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlSetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        if (_data_cen_master.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
            FvInsertEdit.Visible = true;
            lbCreate.Visible = false;
            FormViewS.Visible = false;


        }
        else
        {
            lbCreate.Visible = true;
            FvInsertEdit.Visible = false;
            setFormData(FormViewS, FormViewMode.Insert, null);
            FormViewS.Visible = true;
            ViewState["forSearch"] = null;
            ViewState["forWordSearch"] = null;
            showWorkGroup();
            gvWorkgroup.Visible = true;
            DropDownList ddlEditSave = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
            selectOrg(ddlEditSave, 0);
        }
        //_functionTool.setGvData(gvWorkgroup, _data_cen_master.cen_wg_list_m0);

    }

    protected void cmdDelete(int cmdArg)
    {
        data_cen_master _data_cen_master = new data_cen_master();
        cen_wg_detail_m0 _cen_wg_detail_m0 = new cen_wg_detail_m0();
        _cen_wg_detail_m0.wg_idx = cmdArg;
        _cen_wg_detail_m0.wg_status = 9;
        _cen_wg_detail_m0.cemp_idx = _emp_idx;
        _data_cen_master.master_mode = "4";


        _data_cen_master.cen_wg_list_m0 = new cen_wg_detail_m0[1];
        _data_cen_master.cen_wg_list_m0[0] = _cen_wg_detail_m0;

        //--debug
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        _data_cen_master = callServiceMaster(_urlSetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _functionTool.setGvData(gvWorkgroup, _data_cen_master.cen_wg_list_m0);

        if (_data_cen_master.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถลบข้อมูลนี้ได้!!!');", true);
            gvWorkgroup.Visible = true;
            FormViewS.Visible = true;
            //getWorkgroup();
            ViewState["forSearch"] = null;
            ViewState["forWordSearch"] = null;
            showWorkGroup();
        }
        else
        {
            gvWorkgroup.Visible = true;
            FormViewS.Visible = true;
            //getWorkgroup();
            ViewState["forSearch"] = null;
            ViewState["forWordSearch"] = null;
            showWorkGroup();
        }

    }

    protected data_cen_master callServiceMaster(string _cmdUrl, data_cen_master _data_cen_master)
    {
        _localJson = _functionTool.convertObjectToJson(_data_cen_master);

        _localJson = _functionTool.callServicePost(_cmdUrl, _localJson);

        _data_cen_master = (data_cen_master)_functionTool.convertJsonToObject(typeof(data_cen_master), _localJson);


        return _data_cen_master;
    }

    protected string convertStatus(int status)
    {
        if (status == 1)
            textstatus = "online";
        else
            textstatus = "offline";
        return textstatus;
    }
    protected string convertCss(int status)
    {
        if (status == 1)
            textstatus = "text-success";
        else
            textstatus = "text-danger";
        return textstatus;
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        data_cen_master _data_cen_master = new data_cen_master();
        switch (GvName.ID)
        {
            case "gvWorkgroup":
                gvWorkgroup.PageIndex = e.NewPageIndex;
                showWorkGroup();
                break;

        }
        hlSetTotop.Focus();
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _functionTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_functionTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void setDdlDataEdit(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        //ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }
}