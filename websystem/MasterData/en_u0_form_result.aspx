﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="en_u0_form_result.aspx.cs" Inherits="websystem_MasterData_en_u0_form_result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">


    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left" runat="server">
                    <li id="_divMenuLiToViewIndex" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivIndex" runat="server"
                            CommandName="_divMenuBtnToDivIndex"
                            OnCommand="btnCommand" Text="ข้อมูลรายการ Check Result" />
                    </li>


                    <li id="_divMenuLiToViewAdd" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAdd" runat="server"
                            CommandName="_divMenuBtnToDivAdd"
                            OnCommand="btnCommand" Text="เพิ่มรายการ Check Result" />
                    </li>
                     <li id="_divMenuLiToDocument" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                            NavigateUrl="https://docs.google.com/document/d/1HLd4Dn65bT1-OHSWpx5x9GSW8s4RdsU2v5yqfRs4c2o/edit?usp=sharing"
                            Target="_blank" Text="คู่มือการใช้งาน" />
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">

            <div class="col-lg-12">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; Search Form Result</strong></h3>
                    </div>

                    <div class="panel-body">


                        <div id="SETBoxAllSearch" runat="server">
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label16" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ :" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddllocate_search" runat="server" CssClass="form-control"></asp:DropDownList>
                                                 
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidacctor5" ValidationGroup="btnsearch" runat="server" Display="None"
                                                ControlToValidate="ddllocate_search" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานที่"
                                                ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidadtorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidacctor5" Width="160" />

                                        </div>
                                     
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label67" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทเครื่องจักร :" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltype_search" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <asp:Label ID="Label12" CssClass="col-sm-2 control-label" runat="server" Text="รหัสกลุ่มเครื่องจักร :" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypecode_search" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="กรุณาเลือกรหัสกลุ่มเครื่องจักร..." Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <div class="col-sm-5 col-sm-offset-2">
                                            <asp:LinkButton ID="LinkButton3" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="BtnBack" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="form-group">

                    <div class="ccol-sm-offset-2">
                        <asp:LinkButton ID="LinkButton5" CssClass="btn btn-warning" data-toggle="tooltip" title="Search" runat="server" CommandName="cmdlocate" CommandArgument="2" OnCommand="btnCommand"> นพวงศ์</asp:LinkButton>
                        <asp:LinkButton ID="LinkButton6" CssClass="btn btn-info" data-toggle="tooltip" title="Refresh" runat="server" CommandName="cmdlocate" CommandArgument="14" OnCommand="btnCommand">โรจนะ</asp:LinkButton>
                    </div>

                </div>

                <br />

                <div class="form-group">
                    <asp:GridView ID="GvMaster" runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        HeaderStyle-CssClass="success"
                        HeaderStyle-Height="40px"
                        AllowPaging="false"
                        DataKeyNames="TCIDX"
                        PageSize="10"
                        OnRowDataBound="Master_RowDataBound">
                        <%--OnPageIndexChanging="Master_PageIndexChanging">--%>

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="#" Visible="false">

                                <ItemTemplate>
                                    <asp:Label ID="lblstidx" runat="server" Visible="false" Text='<%# Eval("TCIDX") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานที่" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblocname" runat="server" Text='<%# Eval("LocName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเครื่องจักร" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lbcode" runat="server" Text='<%# Eval("NameEN") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="รหัสกลุ่มเครื่องจักร" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lbname" runat="server" Text='<%# Eval("NameTypecode") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lbstatus" runat="server" Text='<%# Eval("datecerate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblview" CssClass="btn btn-info btn-sm" runat="server" CommandName="CmdViewDetail" OnCommand="btnCommand" data-toggle="tooltip" title="View" CommandArgument='<%# Eval("TCIDX")+ ";" +  Eval("LocIDX")%>'><i class="	glyphicon glyphicon-th-list"></i></asp:LinkButton>
                                </ItemTemplate>

                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </asp:View>


        <asp:View ID="ViewDetail" runat="server">
            <div class="col-lg-12">

                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: lightseagreen; color: aliceblue">
                        <h4><b>
                            <asp:Label ID="lbltopic" runat="server"></asp:Label></b></h4>
                    </blockquote>

                </div>

                <asp:GridView ID="GvGroupCode" runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                    HeaderStyle-Height="40px"
                    AllowPaging="false"
                    DataKeyNames="m1idx"
                    OnRowDataBound="Master_RowDataBound">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText=" " ItemStyle-Font-Size="Medium">
                            <ItemTemplate>


                                <div class="alert-message alert-message-success">
                                    <blockquote class="danger" style="background-color: yellowgreen; color: black;">
                                        <h4><b>
                                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Text='<%# Eval("NameGroupcode") %>'></asp:Label>
                                        </b></h4>
                                    </blockquote>
                                </div>

                                <asp:Label ID="lblGCIDX" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("GCIDX") %>'></asp:Label>
                                <asp:Label ID="lblm1idx" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("m1idx") %>'></asp:Label>


                                <asp:GridView ID="GvItem" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info small"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="false"
                                    DataKeyNames="m1idx"
                                    OnRowDataBound="Master_RowDataBound">



                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="POINT จุดที่ตรวจ" ItemStyle-CssClass="text-center" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbtype" runat="server" Text='<%# Eval("type_name_th") %>'></asp:Label>
                                                <%--  <asp:Label ID="lblm0tyidx" runat="server" Text='<%# Eval("m0tyidx") %>'></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="CONTENT รายละเอียดที่ตรวจ" ItemStyle-CssClass="text-center" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbcontent" runat="server" Text='<%# Eval("contentth") %>'></asp:Label>
                                                <%-- <asp:Label ID="lblm0coidx" runat="server" Text='<%# Eval("m0coidx") %>'></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="STANDARD มาตรฐาน" ItemStyle-CssClass="text-center" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbstandard" runat="server" Text='<%# Eval("standard_name_th") %>'></asp:Label>
                                                <%--<asp:Label ID="lblm0stidx" runat="server" Text='<%# Eval("m0stidx") %>'></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="METHOD วิธีที่ตรวจ" ItemStyle-CssClass="text-center" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbmethod" runat="server" Text='<%# Eval("method_name_th") %>'></asp:Label>
                                                <%-- <asp:Label ID="lblm0meidx" runat="server" Text='<%# Eval("m0meidx") %>'></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เวลาในการปฏิบัติการ" ItemStyle-CssClass="text-center" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbtime" runat="server" Text='<%# Eval("time") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="อะไหล่" ItemStyle-CssClass="text-center" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbtool" runat="server" Text='<%# Eval("tooling_name_th") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Management" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="CmdEditIT" OnCommand="btnCommand" CommandArgument='<%# Eval("m2idx")%>' data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>

                                                <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" CommandArgument='<%# Eval("m2idx")%>' OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"> <i class="fa fa-trash"></i></asp:LinkButton>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>


                                    </Columns>
                                </asp:GridView>



                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

                <div class="form-group" runat="server" id="divbtneditsave">
                    <div class="col-sm-1 col-sm-offset-11">
                        <asp:LinkButton ID="LinkButton1" CssClass="btn btn-danger btn-sm" runat="server" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการกลับสู่หน้าหลักใช่หรือไม่ ?')" CommandName="CmdbackForm"><i class="glyphicon glyphicon-log-out"></i></asp:LinkButton>

                    </div>
                </div>


            </div>

            <div class="col-lg-12">
                <asp:UpdatePanel runat="server" ID="updateedit" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Panel ID="panel_edit" runat="server" Visible="false">
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">


                                        <asp:FormView ID="FvEdit" runat="server" DefaultMode="Edit" OnDataBound="FvDetail_DataBound" Width="100%">
                                            <EditItemTemplate>

                                                <div class="form-group">
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtm2idx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m2idx")%>' />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label11" CssClass="col-sm-3 control-label" runat="server" Text="รหัสกลุ่ม :" />
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtNameGroupcode" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("NameGroupcode")%>' />
                                                    </div>
                                                </div>

                                                <asp:UpdatePanel ID="update_m0tyidx" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="POINT จุดที่ตรวจ :" />
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtGCIDX" Visible="false" CssClass="form-control" runat="server" Text='<%# Eval("GCIDX") %>'></asp:TextBox>
                                                                <asp:TextBox ID="txtm0tyidx" Visible="false" CssClass="form-control" runat="server" Text='<%# Eval("m0tyidx") %>'></asp:TextBox>
                                                                <asp:DropDownList ID="ddltype_edit" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                    ControlToValidate="ddltype_edit" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือก POINT จุดที่ตรวจ"
                                                                    ValidationExpression="กรุณาเลือก POINT จุดที่ตรวจ" InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                                            </div>
                                                        </div>

                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddltype_edit" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>


                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label5" CssClass="col-sm-3 control-label" runat="server" Text="CONTENT รายละเอียดที่ตรวจ :" />
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtm0coidx" Visible="false" CssClass="form-control" runat="server" Text='<%# Eval("m0coidx") %>'></asp:TextBox>
                                                                <asp:DropDownList ID="ddlcontent_edit" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                    ControlToValidate="ddlcontent_edit" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือก CONTENT รายละเอียดที่ตรวจ"
                                                                    ValidationExpression="กรุณาเลือก CONTENT รายละเอียดที่ตรวจ" InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlcontent_edit" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>


                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label7" CssClass="col-sm-3 control-label" runat="server" Text="STANDARD มาตรฐาน :" />
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtm0stidx" Visible="false" CssClass="form-control" runat="server" Text='<%# Eval("m0stidx") %>'></asp:TextBox>
                                                                <asp:DropDownList ID="ddlstandard_edit" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>


                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                    ControlToValidate="ddlstandard_edit" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือก STANDARD มาตรฐาน"
                                                                    ValidationExpression="กรุณาเลือก STANDARD มาตรฐาน" InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />

                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlstandard_edit" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>


                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="METHOD วิธีที่ตรวจ :" />
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txm0meidx" Visible="false" CssClass="form-control" runat="server" Text='<%# Eval("m0meidx") %>'></asp:TextBox>
                                                                <asp:DropDownList ID="ddlmethod_edit" runat="server" CssClass="form-control"></asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                    ControlToValidate="ddlmethod_edit" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือก METHOD วิธีที่ตรวจ"
                                                                    ValidationExpression="กรุณาเลือก METHOD วิธีที่ตรวจ" InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />

                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlmethod_edit" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>


                                                <div class="form-group">
                                                    <asp:Label ID="Label18" CssClass="col-sm-3 control-label" runat="server" Text="เวลาในการปฏิบัติการ :" />
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txttime" CssClass="form-control" runat="server" Text='<%# Eval("time") %>'></asp:TextBox>
                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save_edit" runat="server" Display="None"
                                                    ControlToValidate="txttime" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกเวลาในการปฏิบัติการ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />--%>
                                                    </div>
                                                </div>

                                                <div class="form-group" style="overflow-y: scroll; width: auto; height: 150px" id="idchecklist" runat="server">
                                                    <asp:Label ID="Label15" class="col-sm-3 control-label" runat="server" Text="อะไหล่ : " />
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtchk_tool" Visible="false" CssClass="form-control" runat="server" Text='<%# Eval("m0toidx_comma") %>'></asp:TextBox>
                                                        <asp:CheckBoxList ID="chktooling_edit"
                                                            runat="server"
                                                            CellPadding="5"
                                                            CellSpacing="5"
                                                            RepeatColumns="2"
                                                            RepeatDirection="Vertical"
                                                            RepeatLayout="Table"
                                                            TextAlign="Right"
                                                            Width="100%">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </div>


                                                <asp:UpdatePanel ID="updatepanel" runat="server">
                                                    <ContentTemplate>
                                                        <div class="form-group">
                                                            <div class="col-sm-2 col-sm-offset-10">
                                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" OnCommand="btnCommand" ValidationGroup="Save_edit" CommandName="CmdUpdate" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                <asp:LinkButton ID="lbCmdCancel" class="btn btn-default btn-sm" runat="server" OnCommand="btnCommand" CommandName="CmdCancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                                            </div>
                                                        </div>

                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lbCmdUpdate" />
                                                        <asp:PostBackTrigger ControlID="lbCmdCancel" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </EditItemTemplate>
                                        </asp:FormView>

                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

        </asp:View>

        <asp:View ID="ViewAdd" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-edit"></i><strong>&nbsp; กำหนดฟอร์มกรอกข้อมูล</strong></h3>
                    </div>
                    <asp:FormView ID="FvInsert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label9" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddllocate" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกสถานที่...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddllocate" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานที่"
                                                ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <asp:Label ID="Label17" class="col-sm-2 control-label" runat="server" Text="ประเภทเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypemachine" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกประเภทเครื่องจักร..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltypemachine" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทเครื่องจักร"
                                                ValidationExpression="กรุณาเลือกประเภทเครื่องจักร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                        </div>

                                        <asp:Label ID="Label18" class="col-sm-3 control-label" runat="server" Text="รหัสกลุ่มเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypecode" class="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกรหัสกลุ่มเครื่องจักร..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltypecode" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกรหัสกลุ่มเครื่องจักร"
                                                ValidationExpression="กรุณาเลือกรหัสกลุ่มเครื่องจักร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                        </div>

                                    </div>
                                    <div class="form-group">

                                        <asp:Label ID="Label3" class="col-sm-2 control-label" runat="server" Text="รหัสกลุ่ม : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlgroupmachine" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกรหัสกลุ่ม..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlgroupmachine" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกรหัสกลุ่ม"
                                                ValidationExpression="กรุณาเลือกรหัสกลุ่ม" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />
                                        </div>

                                        <asp:Label ID="Label2" class="col-sm-3 control-label" runat="server" Text="POINT จุดที่ตรวจ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypeitem" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือก POINT จุดที่ตรวจ..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltypeitem" Font-Size="11"
                                                ErrorMessage="กรุณาเลือก POINT จุดที่ตรวจ"
                                                ValidationExpression="กรุณาเลือก POINT จุดที่ตรวจ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label1" class="col-sm-2 control-label" runat="server" Text="CONTENT รายละเอียดที่ตรวจ: " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlcontent" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกรายการ..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlcontent" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกรายการ"
                                                ValidationExpression="กรุณาเลือกรายการ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                        </div>

                                        <asp:Label ID="Label10" class="col-sm-3 control-label" runat="server" Text="STANDARD มาตรฐาน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlspec" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกสเปคเครื่องจักร..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlspec" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสเปคเครื่องจักร"
                                                ValidationExpression="กรุณาเลือกสเปคเครื่องจักร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label13" class="col-sm-2 control-label" runat="server" Text="METHOD วิธีที่ตรวจ  : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlmethod" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกวิธีตรวจสอบเครื่องจักร..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlmethod" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกวิธีตรวจสอบเครื่องจักร"
                                                ValidationExpression="กรุณาเลือกวิธีตรวจสอบเครื่องจักร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                        </div>

                                        <asp:Label ID="Label14" class="col-sm-3 control-label" runat="server" Text="เวลาในการปฏิบัติการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txttime" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>

                                    <div class="form-group" style="overflow-y: scroll; width: auto; height: 100px" id="idchecklist" visible="false" runat="server">
                                        <asp:Label ID="Label15" class="col-sm-2 control-label" runat="server" Text="อะไหล่ : " />
                                        <div class="col-sm-10">

                                            <asp:CheckBoxList ID="chktooling"
                                                runat="server"
                                                CellPadding="5"
                                                CellSpacing="5"
                                                RepeatColumns="2"
                                                RepeatDirection="Vertical"
                                                RepeatLayout="Table"
                                                TextAlign="Right"
                                                Width="100%">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-2">
                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-warning btn-sm" runat="server" Text="ADD +" OnCommand="btnCommand" ValidationGroup="Save" CommandName="CmdAddForm"></asp:LinkButton><%-- OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"--%>
                                        </div>

                                    </div>

                                    <br />
                                    <div class="form-group">
                                        <asp:GridView ID="GvReportAdd"
                                            runat="server"
                                            CssClass="table table-striped table-responsive"
                                            GridLines="None"
                                            OnRowCommand="onRowCommand"
                                            Visible="false"
                                            AutoGenerateColumns="false">


                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>

                                            <Columns>
                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center" ItemStyle-Width="2%"
                                                    HeaderStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                    <ItemTemplate>
                                                        <%# (Container.DataItemIndex + 1) %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="LocName" HeaderText="สถานที่" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="LocIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />




                                                <asp:BoundField DataField="NameEN" HeaderText="ประเภทเครื่องจักร" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="TmcIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />



                                                <asp:BoundField DataField="NameTypecode" HeaderText="รหัสกลุ่มเครื่องจักร" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="TCIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />


                                                <asp:BoundField DataField="NameGroupcode" HeaderText="รหัสกลุ่ม" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="GCIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />


                                                <asp:BoundField DataField="type_name_th" HeaderText="POINT จุดที่ตรวจ" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="m0tyidx" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />


                                                <asp:BoundField DataField="contentth" HeaderText="CONTENT รายละเอียดที่ตรวจ" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="m0coidx" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="standard_name_th" HeaderText="STANDARD มาตรฐาน" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="m0stidx" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />


                                                <asp:BoundField DataField="method_name_th" HeaderText="METHOD วิธีที่ตรวจ" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="m0meidx" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />


                                                <asp:BoundField DataField="time" HeaderText="เวลาในการปฏิบัติการ" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="tooling_name_th" HeaderText="อะไหล่" ItemStyle-CssClass="text-center" ItemStyle-Width="17%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="m0toidx" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                    </ItemTemplate>

                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                            </Columns>

                                        </asp:GridView>
                                    </div>

                                    <div class="form-group" id="divsave" runat="server" visible="false">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success btn-sm" runat="server" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="CmdInsertForm"><i class="glyphicon glyphicon-floppy-disk"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn btn-danger btn-sm" runat="server" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-floppy-remove"></i></asp:LinkButton>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>


                </div>
            </div>

        </asp:View>

    </asp:MultiView>
</asp:Content>

