using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.IO;

/// <summary>
/// Summary description for service_execute
/// </summary>
public class service_mail
{
    public service_mail()
    {
      //
      // TODO: Add constructor logic here
      //
    }

    public void SendHtmlFormattedEmail(string recepientEmailList, string subject, string body)
    {
        using (MailMessage mailMessage = new MailMessage())
        {
            mailMessage.From = new MailAddress("noreply@taokaenoi.co.th");
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            string[] recepientEmail = recepientEmailList.Split(',');
            foreach (string rEmail in recepientEmail)
            {
                if (rEmail != String.Empty)
                {
                    mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                }
            }
            mailMessage.Bcc.Add(new MailAddress("noreply@taokaenoi.co.th"));
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;
            System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
            NetworkCred.UserName = "noreply@taokaenoi.co.th";
            NetworkCred.Password = "knlolksosgsuakcd";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            //smtp.Send(mailMessage);
        }
    }

    public void SendHtmlFormattedEmailFull(string recepientEmailList, string recepientCcEmailList, string replyToEmailList, string subject, string body)
    {

        using (MailMessage mailMessage = new MailMessage())
        {
            mailMessage.From = new MailAddress("noreply@taokaenoi.co.th");
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            // set to mail list
            string[] recepientEmail = recepientEmailList.Split(',');
            foreach (string rEmail in recepientEmail)
            {
                if (rEmail != String.Empty)
                {
                    mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                }
            }
            // set cc mail list
            string[] recepientCcEmail = recepientCcEmailList.Split(',');
            foreach (string cEmail in recepientCcEmail)
            {
                if (cEmail != String.Empty)
                {
                    mailMessage.CC.Add(new MailAddress(cEmail)); //Adding Multiple To email Id
                }
            }
            //set reply to list
            string[] replyToEmail = replyToEmailList.Split(',');
            foreach (string rtEmail in replyToEmail)
            {
                if (rtEmail != String.Empty)
                {
                    mailMessage.ReplyToList.Add(new MailAddress(rtEmail)); //Adding Multiple To email Id
                }
            }

            mailMessage.Bcc.Add(new MailAddress("noreply@taokaenoi.co.th"));
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;
            System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
            NetworkCred.UserName = "noreply@taokaenoi.co.th";
            NetworkCred.Password = "knlolksosgsuakcd";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            //smtp.Send(mailMessage);
        }
    }


    

    #region api_vacation
    public string VacationCreateBody(u0_document_detail _u0_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_vacation_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{emp_code}", _u0_detail.emp_code);
        body = body.Replace("{emp_name_th}", _u0_detail.emp_name_th);
        body = body.Replace("{pos_name_th}", _u0_detail.pos_name_th);
        body = body.Replace("{u0_leave_date}", _u0_detail.u0_leave_start + " - " + _u0_detail.u0_leave_end);
        body = body.Replace("{m0_leavetype_name}", _u0_detail.m0_leavetype_name);
        body = body.Replace("{u0_emp_shift_name}", _u0_detail.u0_emp_shift_name);
        body = body.Replace("{u0_leave_comment}", _u0_detail.u0_leave_comment);
        return body;
    }
    #endregion api_vacation

    #region api e-mail-New Employee 
    public string employeeCreate(employee_detail _u0EmployeeDetails)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_employee_profile.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{emp_code}", _u0EmployeeDetails.emp_code);
        body = body.Replace("{emp_name_th}", _u0EmployeeDetails.emp_name_th);
        body = body.Replace("{pos_name_th}", _u0EmployeeDetails.pos_name_th);
        body = body.Replace("{emp_start_date}", _u0EmployeeDetails.emp_start_date);
        body = body.Replace("{emp_type_name}", _u0EmployeeDetails.emp_type_name);
        return body;
    }
    #endregion api e-mailEmployee

    #region api_itrepair create sap
    public string ITRepairCreateBody(UserRequestList insert)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", insert.DocCode);
        body = body.Replace("{Priority_name}", insert.Priority_name);
        body = body.Replace("{FullNameTH}", insert.EmpName);
        body = body.Replace("{RDeptName}", insert.RDeptName);
        body = body.Replace("{TelETC}", insert.TelETC);
        body = body.Replace("{LocName}", insert.LocName);
        body = body.Replace("{UserLogonName}", insert.UserLogonName); 
        body = body.Replace("{detailUser}", insert.detailUser);
        body = body.Replace("{Email}", insert.Email);

        return body;
    }

    #endregion

    #region api_itrepair create it
    public string ITRepairITCreateBody(UserRequestList insert, string urqidx, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_createIT.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", insert.DocCode);
        body = body.Replace("{FullNameTH}", insert.EmpName);
        body = body.Replace("{RDeptName}", insert.RDeptName);
        body = body.Replace("{RSecName}", insert.SecNameTH);
        body = body.Replace("{TelETC}", insert.TelETC);
        body = body.Replace("{Email}", insert.Email);
        body = body.Replace("{LocName}", insert.LocName);
        body = body.Replace("{detailUser}", insert.detailUser);
        body = body.Replace("URQIDX", urqidx);
        body = body.Replace("{urlendcry}", link);

        return body;
    }

    #endregion

    #region api_itrepair create res
    public string ITRepairCreateRESBody(UserRequestList insert)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_createRES.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", insert.DocCode);
        body = body.Replace("{Priority_name}", insert.Priority_name);
        body = body.Replace("{devices_name}", insert.devices_name);
        body = body.Replace("{FullNameTH}", insert.EmpName);
        body = body.Replace("{RDeptName}", insert.RDeptName);
        body = body.Replace("{TelETC}", insert.TelETC);
        body = body.Replace("{LocName}", insert.LocName);
        body = body.Replace("{UserLogonName}", insert.UserLogonName);
        body = body.Replace("{detailUser}", insert.detailUser);
        body = body.Replace("{Email}", insert.Email);

        return body;
    }

    #endregion


    #region api_itrepair send it alway
    public string ITRepairITSendBody(UserRequestList insert, string urqidx, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_senuser_alway.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", insert.DocCode);
        body = body.Replace("{detailUser}", insert.detailUser);
        body = body.Replace("{AdminName}", insert.AdminName);
        body = body.Replace("{StaName}", insert.StaName);
        body = body.Replace("URQIDX", urqidx);
        body = body.Replace("{urlendcry}", link);

        return body;
    }

    #endregion 

    #region api_itrepair feedback it 
    public string ITRepairITfeedbackBody(UserRequestList insert, string urqidx, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_feedbackit.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", insert.DocCode);
        body = body.Replace("{detailUser}", insert.detailUser);
        body = body.Replace("{CommentAMDoing}", insert.CommentAMDoing);
        body = body.Replace("{AdminDoingName}", insert.AdminDoingName);
        body = body.Replace("{StaName}", insert.StaName);
        body = body.Replace("{urlendcry}", link);

        return body;
    }

    #endregion

    #region api_itrepair comment it
    public string ITRepairCommentITBody(UserRequestList _u0_userrequest)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_commentit.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{FullNameTH}", _u0_userrequest.FullNameTH);
        body = body.Replace("{CommentAuto}", _u0_userrequest.CommentAuto);
        body = body.Replace("{CDate}", _u0_userrequest.CDate);
        body = body.Replace("{CTime}", _u0_userrequest.CTime);

        return body;
    }

    #endregion

    #region api_itrepair send User POS 
    public string ITRepairPOSSendBody(UserRequestList insert, string urqidx, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/templete_ITRepair_senduserPOS.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", insert.DocCode);
        body = body.Replace("{detailUser}", insert.detailUser);
        body = body.Replace("{AdminName}", insert.AdminName);
        body = body.Replace("URQIDX", urqidx);
        body = body.Replace("{urlendcry}", link);

        return body;
    }

    #endregion

    #region api_itrepair send POS 
    public string ITRepairSendPOSBody(UserRequestList insert, string urqidx, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/templete_ITRepair_sendPOS.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", insert.DocCode);
        body = body.Replace("{detailUser}", insert.detailUser);
        body = body.Replace("{AdminName}", insert.AdminName);
        body = body.Replace("URQIDX", urqidx);
        body = body.Replace("{urlendcry}", link);

        return body;
    }

    #endregion 


    #region api_itrepair sapaccept sap
    public string ITRepairSapAcceptBody(UserRequestList _u0_userrequest)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_sapaccept.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{EmpName}", _u0_userrequest.EmpName);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{AdminName}", _u0_userrequest.AdminName);

        return body;
    }

    public string ITRepairBIAcceptBody(UserRequestList _u0_userrequest)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_BIaccept.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{EmpName}", _u0_userrequest.EmpName);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{AdminName}", _u0_userrequest.AdminName);

        return body;
    }

    public string ITRepairBPAcceptBody(UserRequestList _u0_userrequest)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_BPaccept.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{EmpName}", _u0_userrequest.EmpName);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{AdminName}", _u0_userrequest.AdminName);

        return body;
    }

    #endregion /////

    #region api_itrepair senduser sap
    public string ITRepairSendUserBody(UserRequestList _u0_userrequest, string urqidx, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_senduser.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{EmpName}", _u0_userrequest.EmpName);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{AdminName}", _u0_userrequest.AdminName);
        body = body.Replace("URQIDX", urqidx);
        body = body.Replace("{urlendcry}", link);

        return body;
    }

    public string ITRepairSendUserBIBody(UserRequestList _u0_userrequest, string urqidx, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_senduserBI.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{EmpName}", _u0_userrequest.EmpName);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{AdminName}", _u0_userrequest.AdminName);
        body = body.Replace("URQIDX", urqidx);
        body = body.Replace("{urlendcry}", link);

        return body;
    }

    public string ITRepairSendUserBPBody(UserRequestList _u0_userrequest, string urqidx, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_senduserBP.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{EmpName}", _u0_userrequest.EmpName);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{AdminName}", _u0_userrequest.AdminName);
        body = body.Replace("URQIDX", urqidx);
        body = body.Replace("{urlendcry}", link);

        return body;
    }

    #endregion

    #region api_itrepair useraccept sap
    public string ITRepairUserAcceptBody(UserRequestList _u0_userrequest)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_useraccept.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);

        return body;
    }

    #endregion 

    #region api_itrepair senduser sap
    public string ITRepairUsernotAcceptBody(UserRequestList _u0_userrequest, string urqidx,string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_usernotaccept.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{AdminName}", _u0_userrequest.AdminName);
        body = body.Replace("URQIDX", urqidx);
        body = body.Replace("{urlendcry}", link);

        return body;
    }

    #endregion

    #region api_itrepair comment sap
    public string ITRepairCommentBody(UserRequestList _u0_userrequest, string urqidx, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_comment.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{FullNameTH}", _u0_userrequest.FullNameTH);
        body = body.Replace("{CommentAuto}", _u0_userrequest.CommentAuto);
        body = body.Replace("{CDate}", _u0_userrequest.CDate);
        body = body.Replace("{CTime}", _u0_userrequest.CTime);
        body = body.Replace("URQIDX", urqidx);
        body = body.Replace("{urlendcry}", link);


        return body;
    }

    public string ITRepairCommentBIBody(UserRequestList _u0_userrequest, string urqidx, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_commentBI.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{FullNameTH}", _u0_userrequest.FullNameTH);
        body = body.Replace("{CommentAuto}", _u0_userrequest.CommentAuto);
        body = body.Replace("{CDate}", _u0_userrequest.CDate);
        body = body.Replace("{CTime}", _u0_userrequest.CTime);
        body = body.Replace("URQIDX", urqidx);
        body = body.Replace("{urlendcry}", link);


        return body;
    }

    public string ITRepairCommentBPBody(UserRequestList _u0_userrequest, string urqidx, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_commentBP.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{FullNameTH}", _u0_userrequest.FullNameTH);
        body = body.Replace("{CommentAuto}", _u0_userrequest.CommentAuto);
        body = body.Replace("{CDate}", _u0_userrequest.CDate);
        body = body.Replace("{CTime}", _u0_userrequest.CTime);
        body = body.Replace("URQIDX", urqidx);
        body = body.Replace("{urlendcry}", link);


        return body;
    }
    #endregion

    #region api_itrepair useraccept sap
    public string ITRepairUserAcceptBody(UserRequestList _u0_userrequest, string urqidx)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_useraccept.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        //body = body.Replace("{AdminName}", _u0_userrequest.AdminName);
        //body = body.Replace("URQIDX", urqidx);


        return body;
    }

    #endregion

    #region api e-mail network create
    public string networkCreate(network_detail _insertnetwork_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_network_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{name_create}", _insertnetwork_detail.name_create);
        body = body.Replace("{pos_name_th}", _insertnetwork_detail.pos_name_th
            );
        body = body.Replace("{date_create}", _insertnetwork_detail.date_create);
        body = body.Replace("{register_number}", _insertnetwork_detail.register_number);
        body = body.Replace("{type_name}", _insertnetwork_detail.type_name);
        body = body.Replace("{category_name}", _insertnetwork_detail.category_name);
        body = body.Replace("{place_name}", _insertnetwork_detail.place_name);
        body = body.Replace("{room_name}", _insertnetwork_detail.room_name);
        body = body.Replace("{detail_devices}", _insertnetwork_detail.detail_devices);

        return body;
    }

    #endregion

    #region api e-mail network approve create
    public string networkApproveCreate(network_detail _approvenetwork_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_network_approvecreate.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{name_create}", _approvenetwork_detail.name_create);
        body = body.Replace("{pos_name_th}", _approvenetwork_detail.pos_name_th
            );
        body = body.Replace("{date_create}", _approvenetwork_detail.date_create);
        body = body.Replace("{register_number}", _approvenetwork_detail.register_number);
        body = body.Replace("{type_name}", _approvenetwork_detail.type_name);
        body = body.Replace("{category_name}", _approvenetwork_detail.category_name);
        body = body.Replace("{place_name}", _approvenetwork_detail.place_name);
        body = body.Replace("{room_name}", _approvenetwork_detail.room_name);
        body = body.Replace("{status_approve}", _approvenetwork_detail.status_approve);

        return body;
    }

    #endregion

    #region api e-mail network create cut
    public string networkCut(cutnetwork_detail _cutnetwork_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_network_cut.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{name_create}", _cutnetwork_detail.name_create);
        body = body.Replace("{pos_name_th}", _cutnetwork_detail.pos_name_th);
        body = body.Replace("{date_create}", _cutnetwork_detail.date_create);
        body = body.Replace("{register_number}", _cutnetwork_detail.register_number);
        body = body.Replace("{asset_code}", _cutnetwork_detail.asset_code);
        body = body.Replace("{brand_name}", _cutnetwork_detail.brand_name);
        body = body.Replace("{generation_name}", _cutnetwork_detail.generation_name);
        body = body.Replace("{diff_date}", _cutnetwork_detail.diff_date);
        

        return body;
    }

    #endregion

    #region api e-mail network approve cut
    public string networkApproveCut(network_detail _approvecutnetwork_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_network_approvecut.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{name_create}", _approvecutnetwork_detail.name_create);
        body = body.Replace("{pos_name_th}", _approvecutnetwork_detail.pos_name_th);
        body = body.Replace("{date_create}", _approvecutnetwork_detail.date_create);
        body = body.Replace("{register_number}", _approvecutnetwork_detail.register_number);
        body = body.Replace("{asset_code}", _approvecutnetwork_detail.asset_code);
        body = body.Replace("{brand_name}", _approvecutnetwork_detail.brand_name);
        body = body.Replace("{generation_name}", _approvecutnetwork_detail.generation_name);
        body = body.Replace("{status_approve}", _approvecutnetwork_detail.status_approve);


        return body;
    }

    #endregion

    #region api e-mail network create move
    public string networkMove(movenetwork_detail _movenetwork_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_network_move.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{name_create}", _movenetwork_detail.name_create);
        body = body.Replace("{pos_name_th}", _movenetwork_detail.pos_name_th);
        body = body.Replace("{date_create}", _movenetwork_detail.date_create);
        body = body.Replace("{register_number}", _movenetwork_detail.register_number);
        body = body.Replace("{asset_code}", _movenetwork_detail.asset_code);
        body = body.Replace("{brand_name}", _movenetwork_detail.brand_name);
        body = body.Replace("{generation_name}", _movenetwork_detail.generation_name);
        body = body.Replace("{place_name}", _movenetwork_detail.place_name);
        body = body.Replace("{room_name}", _movenetwork_detail.room_name);
        body = body.Replace("{place_new_name}", _movenetwork_detail.place_new_name);
        body = body.Replace("{room_new_name}", _movenetwork_detail.room_new_name);



        //body = body.Replace("{diff_date}", _movenetwork_detail.diff_date);


        return body;
    }

    #endregion

    #region api e-mail network approve move
    public string networkApproveMove(network_detail _approvemovenetwork_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_network_approvemove.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{name_create}", _approvemovenetwork_detail.name_create);
        body = body.Replace("{pos_name_th}", _approvemovenetwork_detail.pos_name_th);
        body = body.Replace("{date_create}", _approvemovenetwork_detail.date_create);
        body = body.Replace("{register_number}", _approvemovenetwork_detail.register_number);
        body = body.Replace("{asset_code}", _approvemovenetwork_detail.asset_code);
        body = body.Replace("{brand_name}", _approvemovenetwork_detail.brand_name);
        body = body.Replace("{generation_name}", _approvemovenetwork_detail.generation_name);      
        body = body.Replace("{place_name}", _approvemovenetwork_detail.place_name);
        body = body.Replace("{room_name}", _approvemovenetwork_detail.room_name);
        body = body.Replace("{place_new_name}", _approvemovenetwork_detail.place_new_name);
        body = body.Replace("{room_new_name}", _approvemovenetwork_detail.room_new_name);
        body = body.Replace("{status_approve}", _approvemovenetwork_detail.status_approve);


        return body;
    }

    #endregion

    #region api e-mail network create ma
    public string networkMa(manetwork_detail _manetwork_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_network_ma.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{name_create}", _manetwork_detail.name_create);
        body = body.Replace("{pos_name_th}", _manetwork_detail.pos_name_th);
        //body = body.Replace("{date_create}", _manetwork_detail.date_create);
        body = body.Replace("{register_number}", _manetwork_detail.register_number);
        body = body.Replace("{company_name_old}", _manetwork_detail.company_name_old);
        body = body.Replace("{date_expire_old}", _manetwork_detail.date_expire_old);
        body = body.Replace("{company_name}", _manetwork_detail.company_name);
        body = body.Replace("{date_start}", _manetwork_detail.date_start);
        body = body.Replace("{date_end}", _manetwork_detail.date_end);
        body = body.Replace("{comment}", _manetwork_detail.comment);
        //body = body.Replace("{room_new_name}", _manetwork_detail.room_new_name);



        //body = body.Replace("{diff_date}", _movenetwork_detail.diff_date);


        return body;
    }

    #endregion

    #region api e-mail network approve ma
    public string networkApproveMA(network_detail _approvemanetwork_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_network_approvema.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{name_create}", _approvemanetwork_detail.name_create);
        body = body.Replace("{pos_name_th}", _approvemanetwork_detail.pos_name_th);
        body = body.Replace("{date_create}", _approvemanetwork_detail.date_create);
        body = body.Replace("{register_number}", _approvemanetwork_detail.register_number);
        body = body.Replace("{company_name}", _approvemanetwork_detail.company_name);
        body = body.Replace("{date_purchase_new}", _approvemanetwork_detail.date_purchase_new);
        body = body.Replace("{date_expire_new}", _approvemanetwork_detail.date_expire_new);   
        body = body.Replace("{status_approve}", _approvemanetwork_detail.status_approve);


        return body;
    }

    #endregion

    #region api e-mail network create edit
    public string networkEdit(network_detail _network_detailedit)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_network_edit.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{name_create}", _network_detailedit.name_create);
        body = body.Replace("{pos_name_th}", _network_detailedit.pos_name_th);
        body = body.Replace("{date_create}", _network_detailedit.date_create);
        body = body.Replace("{register_number}", _network_detailedit.register_number);
       
        return body;
    }

    #endregion

    #region api e-mail network approve edit
    public string networkApproveEdit(network_detail _approveeditnetwork_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_network_approveedit.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{name_create}", _approveeditnetwork_detail.name_create);
        body = body.Replace("{pos_name_th}", _approveeditnetwork_detail.pos_name_th);
        body = body.Replace("{date_create}", _approveeditnetwork_detail.date_create);
        body = body.Replace("{register_number}", _approveeditnetwork_detail.register_number);         
        body = body.Replace("{status_approve}", _approveeditnetwork_detail.status_approve);


        return body;
    }

    #endregion





    #region api e-mail assessor probation
    public string assessment_probation(u0_assessment_detail _assessment_probataion)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_employee_probation.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{emps_code_probation}", _assessment_probataion.emps_code_probation);
        body = body.Replace("{emps_fullname_probation}", _assessment_probataion.emps_fullname_probation);
        body = body.Replace("{name_pos_probation}", _assessment_probataion.name_pos_probation);
        body = body.Replace("{date_start_probation}", _assessment_probataion.date_start_probation);
        //body = body.Replace("{brand_name}", _assessment_probataion.brand_name);
        //body = body.Replace("{generation_name}", _assessment_probataion.generation_name);
        //     body = body.Replace("{status_approve}", _approvecutnetwork_detail.status_approve);

        return body;
    }

    #endregion

    #region api e-mail create purchase
    public string create_purchase(u0_purchase_equipment_details create_purchaselist)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_purchase_create.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{document_code}", create_purchaselist.document_code);
        body = body.Replace("{empcode_purchase}", create_purchaselist.empcode_purchase);
        body = body.Replace("{fullname_purchase}", create_purchaselist.fullname_purchase);
        body = body.Replace("{name_purchase}", create_purchaselist.name_purchase);
        body = body.Replace("{name_equipment}", create_purchaselist.name_equipment);
        body = body.Replace("{details_purchase}", create_purchaselist.details_purchase);
        body = body.Replace("{link}", create_purchaselist.link);

        return body;
    }


   #endregion

   #region api e-mail AD Online
   /* Start Permission Temporary */
   public string permTempAppTemplate(string bodyU0, string bodyU1)
   {
      string body = string.Empty;
      string[] bodyU0Split = bodyU0.Split(',');
      string[] bodyU1Split = bodyU1.Split(',');
      using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ad_perm_temp_app.html")))
      {
         body = reader.ReadToEnd();
      }
      body = body.Replace("{perm_temp_creator}", bodyU0Split[0]);
      body = body.Replace("{perm_temp_org}", bodyU0Split[1]);
      body = body.Replace("{perm_temp_dept}", bodyU0Split[2]);
      body = body.Replace("{perm_temp_comp_name}", bodyU0Split[3]);
      body = body.Replace("{perm_temp_contact_amount}", bodyU1Split[0]);
      body = body.Replace("{perm_temp_bodyU1}", bodyU1Split[1]);
      body = body.Replace("{perm_temp_link}", "http://mas.taokaenoi.co.th/active-directory-online");
      return body;
   }

   public string permTempNotAppTemplate(string bodyU0)
   {
      string body = string.Empty;
      string[] bodyU0Split = bodyU0.Split(',');
      using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ad_perm_temp_notapp.html")))
      {
         body = reader.ReadToEnd();
      }
      body = body.Replace("{perm_temp_approve}", bodyU0Split[0]);
      body = body.Replace("{perm_temp_reason}", bodyU0Split[1]);
      body = body.Replace("{perm_temp_date}", bodyU0Split[2]);
      return body;
   }

   public string permTempAppSuccessTemplate(string bodyU0, string bodyU1)
   {
      string body = string.Empty;
      string[] bodyU0Split = bodyU0.Split(',');
      using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ad_perm_temp_success.html")))
      {
         body = reader.ReadToEnd();
      }
      body = body.Replace("{perm_temp_creator}", bodyU0Split[0]);
      body = body.Replace("{perm_temp_org}", bodyU0Split[1]);
      body = body.Replace("{perm_temp_dept}", bodyU0Split[2]);
      body = body.Replace("{perm_temp_sec}", bodyU0Split[3]);
      body = body.Replace("{perm_temp_pos}", bodyU0Split[4]);
      body = body.Replace("{perm_temp_comp_name}", bodyU0Split[5]);
      body = body.Replace("{perm_temp_reason}", bodyU0Split[6]);
      body = body.Replace("{perm_temp_group_date}", bodyU0Split[7]);
      body = body.Replace("{perm_temp_bodyU1}", bodyU1);
      return body;
   }
   /* End Permission Temporary **/

   /* Start Permission Permanent */
   public string permPermAppTemplate(string bodyU0)
   {
      string body = string.Empty;
      string[] bodyU0Split = bodyU0.Split(',');
      using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ad_perm_perm_app.html")))
      {
         body = reader.ReadToEnd();
      }
      body = body.Replace("{perm_perm_empnew_empcode}", bodyU0Split[0]);
      body = body.Replace("{perm_perm_empnew_fullname}", bodyU0Split[1]);
      body = body.Replace("{perm_perm_empnew_organization}", bodyU0Split[2]);
      body = body.Replace("{perm_perm_empnew_department}", bodyU0Split[3]);
      body = body.Replace("{perm_perm_empnew_section}", bodyU0Split[4]);
      body = body.Replace("{perm_perm_empnew_position}", bodyU0Split[5]);
      body = body.Replace("{perm_perm_link}", "http://mas.taokaenoi.co.th/active-directory-online");
      return body;
   }

   public string permPermNotAppTemplate(string bodyU0)
   {
      string body = string.Empty;
      string[] bodyU0Split = bodyU0.Split(',');
      using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ad_perm_perm_notapp.html")))
      {
         body = reader.ReadToEnd();
      }
      body = body.Replace("{perm_perm_empnew_empcode}", bodyU0Split[0]);
      body = body.Replace("{perm_perm_empnew_fullname}", bodyU0Split[1]);
      body = body.Replace("{perm_perm_empnew_organization}", bodyU0Split[2]);
      body = body.Replace("{perm_perm_empnew_department}", bodyU0Split[3]);
      body = body.Replace("{perm_perm_empnew_section}", bodyU0Split[4]);
      body = body.Replace("{perm_perm_empnew_position}", bodyU0Split[5]);
      body = body.Replace("{perm_perm_reason}", bodyU0Split[6]);
      body = body.Replace("{perm_perm_unapprover}", bodyU0Split[7]);
      body = body.Replace("{perm_perm_date}", DateTime.Now.ToString("dd/MM/yyyy"));
      body = body.Replace("{perm_perm_link}", "http://mas.taokaenoi.co.th/active-directory-online");
      return body;
   }

    public string permPermITNotAppTemplate(string bodyU0)
    {
        string body = string.Empty;
        string[] bodyU0Split = bodyU0.Split(',');
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ad_perm_perm_itnotapp.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{perm_perm_empnew_empcode}", bodyU0Split[0]);
        body = body.Replace("{perm_perm_empnew_fullname}", bodyU0Split[1]);
        body = body.Replace("{perm_perm_empnew_organization}", bodyU0Split[2]);
        body = body.Replace("{perm_perm_empnew_department}", bodyU0Split[3]);
        body = body.Replace("{perm_perm_empnew_section}", bodyU0Split[4]);
        body = body.Replace("{perm_perm_empnew_position}", bodyU0Split[5]);
        body = body.Replace("{perm_perm_reason}", bodyU0Split[6]);
        body = body.Replace("{perm_perm_unapprover}", bodyU0Split[7]);
        body = body.Replace("{perm_perm_date}", DateTime.Now.ToString("dd/MM/yyyy"));
        body = body.Replace("{perm_perm_link}", "http://mas.taokaenoi.co.th/active-directory-online");
        return body;
    }

    public string permPermAppSuccessTemplate(string bodyU0)
   {
      string body = string.Empty;
      string[] bodyU0Split = bodyU0.Split(',');
      using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ad_perm_perm_success.html")))
      {
         body = reader.ReadToEnd();
      }
      body = body.Replace("{perm_perm_empnew_empcode}", bodyU0Split[0]);
      body = body.Replace("{perm_perm_empnew_fullname}", bodyU0Split[1]);
      body = body.Replace("{perm_perm_empnew_organization}", bodyU0Split[2]);
      body = body.Replace("{perm_perm_empnew_department}", bodyU0Split[3]);
      body = body.Replace("{perm_perm_empnew_section}", bodyU0Split[4]);
      body = body.Replace("{perm_perm_empnew_position}", bodyU0Split[5]);
      body = body.Replace("{perm_perm_date}", DateTime.Now.ToString("dd/MM/yyyy"));
      body = body.Replace("{perm_perm_link}", "http://mas.taokaenoi.co.th/active-directory-online");
      return body;
   }

   public string permPermSentCommentTemplate(string bodyU0)
   {
      string body = string.Empty;
      string[] bodyU0Split = bodyU0.Split(',');
      using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ad_perm_perm_comment.html")))
      {
         body = reader.ReadToEnd();
      }
      body = body.Replace("{perm_perm_empnew_empcode}", bodyU0Split[0]);
      body = body.Replace("{perm_perm_empnew_fullname}", bodyU0Split[1]);
      body = body.Replace("{perm_perm_empnew_organization}", bodyU0Split[2]);
      body = body.Replace("{perm_perm_empnew_department}", bodyU0Split[3]);
      body = body.Replace("{perm_perm_empnew_section}", bodyU0Split[4]);
      body = body.Replace("{perm_perm_empnew_position}", bodyU0Split[5]);
      body = body.Replace("{perm_perm_reason}", bodyU0Split[6]);
      body = body.Replace("{perm_perm_date}", DateTime.Now.ToString("dd/MM/yyyy"));
      body = body.Replace("{perm_perm_link}", "http://mas.taokaenoi.co.th/active-directory-online");
      return body;
   }

    public string permPermSentBackUserTemplate(string bodyU0)
    {
        string body = string.Empty;
        string[] bodyU0Split = bodyU0.Split(',');
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ad_perm_perm_sendback_user.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{perm_perm_empnew_empcode}", bodyU0Split[0]);
        body = body.Replace("{perm_perm_empnew_fullname}", bodyU0Split[1]);
        body = body.Replace("{perm_perm_empnew_organization}", bodyU0Split[2]);
        body = body.Replace("{perm_perm_empnew_department}", bodyU0Split[3]);
        body = body.Replace("{perm_perm_empnew_section}", bodyU0Split[4]);
        body = body.Replace("{perm_perm_empnew_position}", bodyU0Split[5]);
        body = body.Replace("{perm_perm_reason}", bodyU0Split[6]);
        body = body.Replace("{perm_perm_date}", DateTime.Now.ToString("dd/MM/yyyy"));
        body = body.Replace("{perm_perm_link}", "http://mas.taokaenoi.co.th/active-directory-online");
        return body;
    }
    /* End Permission Permanent */
    #endregion api e-mail AD Online

    #region api e-mail software license create
    public string softwareLicenseCreate(u0_softwarelicense_detail _insertu0_softwarelicense_detail, string value_software_name, string link_software_insert)
    {
        string body = string.Empty;

        //string[] ToId = value_software_name.Split(',');
        //foreach (string ToEmail in ToId)
        //{
        //    if (ToEmail != String.Empty)
        //    {
        //        m.To.Add(new MailAddress(ToEmail)); //Adding Multiple To email Id
        //    }
        //}

        ////string mail_sent;
        ////var mail_sent_sw = "";
        ////string[] Split_softwate = value_software_name.Split(',');
        //////litDebug.Text = Split_softwate[0];

        ////foreach (string substring in Split_softwate)
        ////{
        ////    //Console.WriteLine(substring);
        ////    mail_sent_sw += substring.ToString() + "<br>" ;
        ////}
        ////mail_sent = mail_sent_sw;

       // string[] Split_softwate = value_software_name.Split(',');
       //var mail_software = "";
       ////String[] substrings = value.Split(delimiter);
       //foreach (var substring in Split_softwate)
       //{
       //    mail_software = substring;
       //}

        //Console.WriteLine(substring);
        //string[] bodyU1Split = bodyU1.Split(',');

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_software_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{u0_code}", _insertu0_softwarelicense_detail.u0_code);
        body = body.Replace("{software_nameemail}", value_software_name);
        body = body.Replace("{link}", link_software_insert);

        return body;
    }

    #endregion

    #region api e-mail software license approve
    public string SoftwareApproveCreate(u0_softwarelicense_detail _approveu0_softwarelicense_detail, string link_approve)
    {
        string body = string.Empty;
        

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_softwareapprove_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{u0_code}", _approveu0_softwarelicense_detail.u0_code);
        body = body.Replace("{comment}", _approveu0_softwarelicense_detail.comment);
        body = body.Replace("{link}", link_approve);

        return body;
    }

    #endregion

    #region api e-mail approve purchase
    public string approve_purchase(u0_purchase_equipment_details approve_purchaselist)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_purchase_approve.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{node_status_purchase}", approve_purchaselist.node_status_purchase);
        body = body.Replace("{details_purchase}", approve_purchaselist.details_purchase);
        body = body.Replace("{document_code}", approve_purchaselist.document_code);
        body = body.Replace("{name_purchase}", approve_purchaselist.name_purchase);
        body = body.Replace("{name_equipment}", approve_purchaselist.name_equipment);
        body = body.Replace("{actor_name_purchase}", approve_purchaselist.actor_name_purchase);
        body = body.Replace("{drept_name_purchase}", approve_purchaselist.drept_name_purchase);
        body = body.Replace("{link}", approve_purchaselist.link);

        return body;
    }


    #endregion

    #region api e-mail purchase complete
    public string purchase_complete(email_purchase_details _purchase_complete)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_purchase_complete.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{document_code}", _purchase_complete.document_code);
        body = body.Replace("{name_purchase}", _purchase_complete.name_purchase);
        body = body.Replace("{name_department}", _purchase_complete.name_department);
        body = body.Replace("{list_items}", _purchase_complete.list_items);
        body = body.Replace("{motive_purchase}", _purchase_complete.motive_purchase);
        body = body.Replace("{comment}", _purchase_complete.comment);
        body = body.Replace("{io_number}", _purchase_complete.io_number);
        body = body.Replace("{asset_number}", _purchase_complete.asset_number);

        return body;
    }


    #endregion

}
