﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_employee_partime : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    service_execute _serviceExecute = new service_execute();

    data_emps _dataEmp = new data_emps();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetSelectParttime = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectParttime"];
    static string _urlGetInsertParttime = _serviceUrl + ConfigurationManager.AppSettings["urlGetInsertParttime"];
    static string _urlGetUpdateParttime = _serviceUrl + ConfigurationManager.AppSettings["urlGetUpdateParttime"];
    static string _urlGetDeleteParttime = _serviceUrl + ConfigurationManager.AppSettings["urlGetDeleteParttime"];

    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlSetOrganizationList_shift = _serviceUrl + ConfigurationManager.AppSettings["urlSetOrganizationList_shift"];

    //static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    //static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    //static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];

    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            MvMaster.SetActiveView(ViewIndex);
            select_org(ddlorg_search);
            SelectMasterList(int.Parse(ddlorg_search.SelectedValue),int.Parse(ddlshift_search.SelectedValue));
            ViewState["work_check"] = null;
        }

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_Status()
    {
        ViewState["work_check"] = null;
        List<String> YrEmpList = new List<string>();
        List<String> YrEmpList_not = new List<string>();
        foreach (ListItem item2 in YrChkBox.Items)
        {
            if (item2.Selected)
            {
                YrEmpList_not.Add(item2.Value);
            }
            else
            {
                YrEmpList.Add(item2.Value);
            }
        }
        String YrEmpList1 = String.Join(",", YrEmpList.ToArray());
        ViewState["work_check"] = YrEmpList1;

        _dataEmp.emps_parttime_action = new parttime[1];
        parttime dtemployee_ = new parttime();

        dtemployee_.parttime_code = txtcode.Text;
        dtemployee_.parttime_name_th = txtname_th.Text;
        dtemployee_.org_idx = int.Parse(ddl_org_add.SelectedValue);
        dtemployee_.parttime_start_time = txt_time_day_start.Text;
        dtemployee_.parttime_end_time = txt_time_day_end.Text;
        dtemployee_.parttime_break_start_time = txt_time_part_start.Text;
        dtemployee_.parttime_break_end_time = txt_time_part_end.Text;
        dtemployee_.parttime_workdays = ViewState["work_check"].ToString();
        dtemployee_.parttime_status = int.Parse(ddl_status.SelectedValue);
        dtemployee_.parttime_created_by = int.Parse(ViewState["EmpIDX"].ToString());

        _dataEmp.emps_parttime_action[0] = dtemployee_;
        //fsa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmp));
        _dataEmp = callService(_urlGetInsertParttime, _dataEmp);
    }

    protected void SelectMasterList(int orgidx,int shiftidx)
    {
        _dataEmp.emps_parttime_action = new parttime[1];
        parttime dtemployee_ = new parttime();

        dtemployee_.org_idx = orgidx;
        dtemployee_.m0_parttime_idx = shiftidx;
        _dataEmp.emps_parttime_action[0] = dtemployee_;

        _dataEmp = callService(_urlGetSelectParttime, _dataEmp);
        setGridData(GvMaster, _dataEmp.emps_parttime_action);
    }

    protected void Delete_Master_List()
    {
        _dataEmp.emps_parttime_action = new parttime[1];
        parttime dtemployee_ = new parttime();

        dtemployee_.m0_parttime_idx = int.Parse(ViewState["m0_parttime_idx"].ToString());

        _dataEmp.emps_parttime_action[0] = dtemployee_;
        _dataEmp = callService(_urlGetDeleteParttime, _dataEmp);
    }

    protected void select_org(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected void select_empshift(DropDownList ddlName, int org_idx)
    {
        _dataEmployee.ShiftTime_details = new ShiftTime[1];
        ShiftTime _orgList = new ShiftTime();
        _orgList.org_idx_shiftTime = org_idx;
        _orgList.type_selection = 1; //ddl
        _dataEmployee.ShiftTime_details[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlSetOrganizationList_shift, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.ShiftTime_details, "TypeWork", "midx");
        ddlName.Items.Insert(0, new ListItem("เลือกกะทำงาน....", "0"));
    }

    protected data_emps callService(string _cmdUrl, data_emps _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_emps)_funcTool.convertJsonToObject(typeof(data_emps), _localJson);

        return _dtmaster;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {
            case "ddlorg":
                //select_dep(ddldep, int.Parse(ddlorg.SelectedValue));
                break;
            case "ddldep":
                //select_sec(ddlsec, int.Parse(ddlorg.SelectedValue), int.Parse(ddldep.SelectedValue));
                break;

            case "ddlorg_search":
                select_empshift(ddlshift_search, int.Parse(ddlorg_search.SelectedValue));
                break;

        }
    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    var YrChkBox_update = (CheckBoxList)e.Row.FindControl("YrChkBox_update");
                    var txtparttime_workdays = (TextBox)e.Row.FindControl("txtparttime_workdays");
                    var ddl_org_update = (DropDownList)e.Row.FindControl("ddl_org_update");
                    var lblorg_idx = (Label)e.Row.FindControl("lblorg_idx");

                    ddl_org_update.SelectedValue = lblorg_idx.Text.ToString();

                    string[] ToId = txtparttime_workdays.Text.Split(',');

                    foreach (ListItem li in YrChkBox_update.Items)
                    {
                        foreach (string To_check in ToId)
                        {

                            if (li.Value == To_check)
                            {
                                li.Selected = false;

                                break;
                            }
                            else
                            {
                                li.Selected = true;
                            }
                        }
                    }

                    select_org(ddl_org_update);
                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddlshift_search.SelectedValue));

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddlshift_search.SelectedValue));

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddlshift_search.SelectedValue));
                break;
        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int txtIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtcode_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcode_update");
                var txtNameTH_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtNameTH_update");
                //var txtNameEN_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtNameEN_update");
                var ddl_org_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddl_org_update");
                var txt_time_day_start_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txt_time_day_start_update");
                var txt_time_day_end_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txt_time_day_end_update");
                var txt_time_part_start_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txt_time_part_start_update");
                var txt_time_part_end_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txt_time_part_end_update");
                var ddStatus_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatus_update");
                var YrChkBox_update = (CheckBoxList)GvMaster.Rows[e.RowIndex].FindControl("YrChkBox_update");

                ViewState["work_check_update"] = null;
                List<String> YrEmpList = new List<string>();
                List<String> YrEmpList_no = new List<string>();
                foreach (ListItem item2 in YrChkBox_update.Items)
                {
                    if (item2.Selected)
                    {
                        YrEmpList_no.Add(item2.Value);
                    }
                    else
                    {
                        YrEmpList.Add(item2.Value);
                    }
                }
                String YrEmpList1 = String.Join(",", YrEmpList.ToArray());
                ViewState["work_check_update"] = YrEmpList1;

                GvMaster.EditIndex = -1;

                _dataEmp.emps_parttime_action = new parttime[1];
                parttime dtemployee_ = new parttime();

                dtemployee_.parttime_code = txtcode_update.Text;
                dtemployee_.parttime_name_th = txtNameTH_update.Text;
                //dtemployee_.parttime_name_eng = txtNameEN_update.Text;
                dtemployee_.org_idx = int.Parse(ddl_org_update.SelectedValue);
                dtemployee_.parttime_start_time = txt_time_day_start_update.Text;
                dtemployee_.parttime_end_time = txt_time_day_end_update.Text;
                dtemployee_.parttime_break_start_time = txt_time_part_start_update.Text;
                dtemployee_.parttime_break_end_time = txt_time_part_end_update.Text;
                dtemployee_.parttime_workdays = ViewState["work_check_update"].ToString();
                dtemployee_.parttime_status = int.Parse(ddStatus_update.SelectedValue);
                dtemployee_.parttime_created_by = int.Parse(ViewState["EmpIDX"].ToString());
                dtemployee_.m0_parttime_idx = txtIDX;

                _dataEmp.emps_parttime_action[0] = dtemployee_;
                _dataEmp = callService(_urlGetUpdateParttime, _dataEmp);

                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddlshift_search.SelectedValue));

                break;
        }
    }

    #endregion


    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnaddholder.Visible = false;
                div_search.Visible = false;
                Panel_Add.Visible = true;
                select_org(ddl_org_add);
                break;

            case "btnCancel":
                btnaddholder.Visible = true;
                Panel_Add.Visible = false;
                div_search.Visible = true;

                break;

            case "btnAdd":
                Insert_Status();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "CmdDel":
                int m0_parttime_idx = int.Parse(cmdArg);
                ViewState["m0_parttime_idx"] = m0_parttime_idx;
                Delete_Master_List();
                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddlshift_search.SelectedValue));
                break;

            case "CmdSearch":
                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddlshift_search.SelectedValue));

                break;

            case "btnRefresh":
                ddlorg_search.SelectedValue = "0";
                ddlshift_search.SelectedValue = "0";
                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddlshift_search.SelectedValue));

                break;
        }



    }
    #endregion
}