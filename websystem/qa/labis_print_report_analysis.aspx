﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="labis_print_report_analysis.aspx.cs" Inherits="websystem_qa_labis_print_report_analysis" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

    <title>Print Sample Code</title>

    <style type="text/css" media="print,screen">
        @page {
            size: A4 landscape;
            margin: 25px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

        .formPrint {
            margin: 10pt; /*from 5 to 10*/
            padding: 10pt; /*add*/
            width: 842pt;
        }

        .wraptocenter * {
            vertical-align: middle;
        }
    </style>


</head>
<body onload="window.print()">
    <form id="form1" runat="server" width="100%">
        <script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>

        <div class="formPrint">

            <div class="headOrg" style="text-align: center; padding: 10px;">
                <div class="wraptocenter">
                    <img src='<%=ResolveUrl("~/images/labis/header.jpg") %>' style="align-content: center;" alt="" />
                </div>
                <strong>Test Report</strong>



                <%--    <asp:Literal ID="Literal1" runat="server"></asp:Literal>--%>
            </div>
            <div class="form-group">

                <table class="table table-striped f-s-12" style="width: 100%">
                    <tr>
                        <td style="width: 70%"></td>
                        <td style="width: 30%">
                            <asp:Label ID="ladatetimereport" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <table class="table table-striped f-s-12" style="width: 100%">
                <asp:Repeater ID="rp_test_sample_detail" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="form-group">
                            <asp:Label ID="lb_labCode" runat="server" CssClass="col-sm-12">
                            <p><b>Lab Code:</b> &nbsp;<%# Eval("lab_name") %></p></>
                            </asp:Label>
                          <%--    <asp:Label ID="Label3" runat="server" CssClass="col-sm-12">
                            <p><b>Lab Code:</b> &nbsp;<%# Eval("m0_lab_idx") %></p></>
                            </asp:Label>--%>
                            <asp:Label ID="lb_Description" runat="server" CssClass="col-sm-12">
                            <p><b>Description : </b> &nbsp;<%# Eval("sample_code") %></p></>
                            </asp:Label>
                            <asp:Label ID="lb_received_sample_date" runat="server" CssClass="col-sm-12">
                            <p><b>Sample Received Date : </b> &nbsp;<%# Eval("received_sample_date") %></p></>
                            </asp:Label>
                            <asp:Label ID="lb_Tested_date" runat="server" CssClass="col-sm-12">
                            <p><b>Tested Date : </b> &nbsp;<%# Eval("test_date") %></p></>
                            </asp:Label>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                    </FooterTemplate>
                </asp:Repeater>
            </table>

            <asp:GridView ID="gvSampleReportAnalysis" runat="server" AutoGenerateColumns="False"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12" Width="100%"
                OnRowDataBound="gvRowDataBound"
                DataKeyNames="u1_qalab_idx">
                <HeaderStyle CssClass="info" Height="30px" Font-Size="Small" />
                <RowStyle Font-Size="Small" />
                <Columns>
                    <asp:TemplateField HeaderText="Sample Name" HeaderStyle-Width="20%">
                        <ItemTemplate>
                            <div style="padding: 15px;">
                                <asp:Label ID="lblu0IDX_Sample" runat="server" Visible="false" Text='<%# Eval("u0_qalab_idx") %>'></asp:Label>
                                <asp:Label ID="lblU1DocIDX_Sample" runat="server" Visible="false" Text='<%# Eval("u1_qalab_idx") %>'></asp:Label>
                                <asp:Label ID="lblSampleCode" runat="server" CssClass="col-sm-12" Text='<%# Eval("sample_name") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Tested Items" HeaderStyle-Width="30%">
                        <ItemTemplate>
                            <div class="panel-body">
                                <div style="padding: 15px;">
                                    <table class="table table-striped f-s-12" style="width: 100%">
                                        <asp:Repeater ID="rp_test_sample" runat="server">
                                            <HeaderTemplate>
                                                <tr>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbltestdetailname" runat="server" Text='<%# " - " + Eval("test_detail_name") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="u2_idx_n3" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblu2qalab_idx" runat="server" Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="test_detail_idx_n11" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lbltest_detail_idx_n11" runat="server" Text='<%# Eval("test_detail_idx") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Result" HeaderStyle-Width="30%">
                        <ItemTemplate>
                            <div class="panel-body">
                                <div style="padding: 15px;">
                                    <table class="table table-striped f-s-12" style="width: 100%">
                                        <asp:Repeater ID="rp_test_sample_result" runat="server">
                                            <HeaderTemplate>
                                                <tr>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbltestdetailname" runat="server" Text='<%# Eval("detail_tested") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="u2_idx_n3" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lblu2qalab_idx" runat="server" Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="test_detail_idx_n11" runat="server" visible="false">
                                                    <td>
                                                        <asp:Label ID="lbltest_detail_idx_n11" runat="server" Text='<%# Eval("test_detail_idx") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Method" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="center">
                        <ItemTemplate>
                            <div class="panel-body">
                                <div style="padding: 15px;">
                                    <table class="table table-striped f-s-12" style="width: 100%">
                                        <asp:Repeater ID="rp_test_sample_method" runat="server">
                                            <HeaderTemplate>
                                                <tr>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbltest_test_type_name_th" runat="server" Text='<%# Eval("test_type_name_th") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </div>

                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
            <div class="col-sm-12" style="margin-top: 1cm;">
                <div class="form-group">

                    <table class="table table-striped f-s-12" style="width: 100%">
                        <tr>
                            <td style="width: 50%">
                                <asp:Label ID="Label6" runat="server" CssClass="col-sm-6">
                            <p>.........................................................</p>
                                </asp:Label>
                                <asp:Label ID="Label7" runat="server" CssClass="col-sm-6">
                          <p>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</p>
                                </asp:Label>
                                <asp:Label ID="Label8" runat="server" CssClass="col-sm-6">
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                               &nbsp;&nbsp;&nbsp;&nbsp;Laboratory Supervisor</p>
                                </asp:Label>
                                <asp:Label ID="Label2" runat="server" CssClass="col-sm-6">
                            <p style=" text-align:left;">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;
                              ............/............./.............</p>
                                </asp:Label>
                            </td>
                            <td style="width: 50%; text-align: right;">
                                <asp:Label ID="Label9" runat="server" CssClass="col-sm-6">
                            <p>.........................................................</p>
                                </asp:Label>
                                <asp:Label ID="Label10" runat="server" CssClass="col-sm-6">
                          <p>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</p>
                                </asp:Label>
                                <asp:Label ID="Label11" runat="server" CssClass="col-sm-6">
                            <p style=" text-align:left;">
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;
                                Laboratory Section Head</p>
                                </asp:Label>
                                <asp:Label ID="Label1" runat="server" CssClass="col-sm-6">
                            <p style=" text-align:left;">
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              &nbsp;&nbsp;
                              ............/............./.............</p>
                                </asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-12" style="text-align: right;">
                <asp:Label ID="lbFMQALB_LabRJN" runat="server" Visible="true" CssClass="col-sm-6">
                            <p>FM-QA-LB-023/03 Rev.00 (1/07/2560)</p>
                </asp:Label>
                  <asp:Label ID="lbFMQALB_LabNPW" runat="server" Visible="true" CssClass="col-sm-6">
                            <p>FM-QA-LB-016/01 Rev.00 (15/09/2559)</p>
                </asp:Label>
            </div>
        </div>
    </form>

</body>
</html>
