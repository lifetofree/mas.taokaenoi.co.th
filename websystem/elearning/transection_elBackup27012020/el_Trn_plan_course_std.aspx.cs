﻿using MessagingToolkit.QRCode.Codec;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_Trn_plan_course_std : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_en_planning _data_en_planning = new data_en_planning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_report = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_Report_Plan"];
    static string _urlGetel_lu_TraningAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_lu_TraningAll"];
    static string _urlGetel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_plan_course"];
    static string _urlSetUpdel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_u_plan_course"];

    string _folder_plan_course_qrcode = "plan_course_qrcode";

    string _localJson = "";
    int _tempInt = 0;

    int emp_idx = 0;


    #endregion Init

    #region Constant
    public static class Constants
    {
        public const int SELECT_ALL = 20;
        public const int SELECT_WHERE = 21;
        public const int NUMBER_NULL = -999;
    }
    #endregion Constant

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        
        if (!IsPostBack)
        {
            getTitle();
            hddf_empty.Value = "full";
            actionIndex();
            hddf_empty.Value = "";

        }
        linkBtnTrigger(btnFilter);
       // linkBtnTrigger(btnexport1);

    }
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    #endregion Page Load
    public void getTitle()
    {
        _func_dmu.zDropDownList(ddlYearSearch_L,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
        _func_dmu.zDropDownList(ddltrn_group_L,
                                "-",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-PLAN-COURSE-GRP"
                                );
    }

    #region Action
    protected void actionIndex()
    {
        zSetMode(0);
        zShowData();
    }

    #endregion Action

    #region Check Session Redirect
    protected void checkSessionRedirect()
    {
        //if (Session["sesLoginIdxBackEnd"] == null)
        //{
        //   Response.Redirect(ResolveUrl("~/login"));
        //}
    }
    #endregion Check Session Redirect

    private void Page_Init(object sender, EventArgs e)
    {
        ClearApplicationCache();
    }
    #region reuse
    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }
    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        string[] _calc = new string[4];
        int _id = 0;
        switch (cmdName)
        {
            case "btnFilter":
                zShowData();
                break;
            case "btnqrcode":
                
                if (cmdArg != "0")
                {
                    _calc = cmdArg.Split('|');
                }
                else
                {
                    _calc = new string[4] { "", "", "", "" };
                }
                genQrCode_Test(_func_dmu.zStringToInt(_calc[0]), _calc[1]);
                break;
            case "btnopen_flag":
                if (cmdArg != "0")
                {
                    _calc = cmdArg.Split('|');
                }
                else
                {
                    _calc = new string[4] { "", "", "", "" };
                }
                update_open_flag(_func_dmu.zStringToInt(_calc[0]), _func_dmu.zStringToInt(_calc[1]));
                break;

        }
    }
    #endregion btnCommand

    #region zSetMode
    public void zSetMode(int AiMode)
    {

        switch (AiMode)
        {
            case 0:  //แผนการทำ PM เครื่องจักร
                {
                    setActiveTab("plan_pm");
                   
                }
                break;
            case 1:  //Report
                {
                    setActiveTab("report");
                }
                break;
            case 2://preview mode
                {


                }
                break;
            case 3://Detail mode
                {


                }
                break;
            case 4:  //insert detail mode
                {


                }
                break;
        }
    }
    #endregion zSetMode

    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        //liplan_pm.Attributes.Add("class", "");
        //switch (activeTab)
        //{
        //    case "plan_pm":
        //        liplan_pm.Attributes.Add("class", "active");
        //        break;
        //    case "report":
        //        lireport.Attributes.Add("class", "active");
        //        break;

        //}
    }
    #endregion setActiveTab

    public string zMonthEN(int AMonth)
    {

        return _func_dmu.zMonthEN(AMonth);
    }
    
    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        hddf_empty.Value = "";
        var ddName = (DropDownList)sender;
        switch (ddName.ID)
        {
            case "ddllocate":
                break;

        }

    }

    #endregion

    private void zShowData()
    {
        
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.filter_keyword = txtFilterKeyword_L.Text;
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_L.SelectedValue);
        obj.course_plan_status = ddlcourse_plan_status_L.SelectedValue;
        obj.idx = _func_dmu.zStringToInt(ddltrn_group_L.SelectedValue);
        obj.emp_idx_ref = emp_idx;
        //obj.zstatus = "trn_course_all";
        obj.operation_status_id = "U0-PLAN-COURSE-STD";
        dataelearning.el_training_course_action[0] = obj;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetGridData(GvList, dataelearning.el_training_course_action);
        


    }
    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {

        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            var GvName = (GridView)sender;
            string sGvName = GvName.ID;
            if (sGvName == "GvHR_AList")
            {

            }

        }
    }
    public string getImageIO(string _img, int _i)
    {
        string SetName = "";
        if (_i == 1)
        {
            if (_img == "I")
            {
                SetName = ResolveUrl("~/images/elearning/inplan.png");
            }
            else if (_img == "O")
            {
                SetName = ResolveUrl("~/images/elearning/outplan.png");
            }
        }
        else
        {
            if (_img == "I")
            {
                SetName = "In Plan";
            }
            else if (_img == "O")
            {
                SetName = "Out Plan";
            }
        }

        return SetName;
    }
    protected void onRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        string sGvName = GvName.ID;
        if (sGvName == "GvList")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtGvtraining_course_no = (TextBox)e.Row.Cells[5].FindControl("txtGvtraining_course_no");
                Label lbu0_training_course_idx = (Label)e.Row.Cells[5].FindControl("lbu0_training_course_idx");
                Label lbu0_course_idx = (Label)e.Row.Cells[5].FindControl("lbu0_course_idx");
                Repeater rptcourse = (Repeater)e.Row.Cells[5].FindControl("rptcourse");
                
                setCoursePass(_func_dmu.zStringToInt(lbu0_course_idx.Text), rptcourse);

                Label lbgrade_status = (Label)e.Row.Cells[6].FindControl("lbgrade_status");
                
                Repeater rptu8trncoursedate = (Repeater)e.Row.Cells[5].FindControl("rptu8trncoursedate");
                setU8StartEndDatetime(_func_dmu.zStringToInt(lbu0_training_course_idx.Text), rptu8trncoursedate);


            }

        }
        
    }
    private void setCoursePass(int id, Repeater rptRepeater)
    {
        data_elearning dataelearning = new data_elearning();
        trainingLoolup obj_Loolup = new trainingLoolup();
        dataelearning.trainingLoolup_action = new trainingLoolup[1];
        obj_Loolup.EmpIDX = emp_idx;
        obj_Loolup.idx = id;
        obj_Loolup.operation_status_id = "course_pass";
        dataelearning.trainingLoolup_action[0] = obj_Loolup;
        dataelearning = _func_dmu.zCallServicePostNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning);
        _func_dmu.zSetRepeaterData(rptRepeater, dataelearning.trainingLoolup_action);
    }
    private void setU8StartEndDatetime(int id, Repeater rptRepeater)
    {
        data_elearning dataelearning = new data_elearning();
        training_course obj_detail = new training_course();
        dataelearning.el_training_course_action = new training_course[1];
        //obj_detail.EmpIDX = emp_idx;
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "U8-FULL";
        dataelearning.el_training_course_action[0] = obj_detail;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetRepeaterData(rptRepeater, dataelearning.el_training_course_action);

    }
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {

            case "GvList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                zShowData();
                SETFOCUS.Focus();
                break;

        }
    }
    public string getstar_level(int ilevel)
    {
        string sStar = "", sStar_all = "";
        //int item = 2;
        sStar = "<i class=\"glyphicon glyphicon-star\" style=\"color: forestgreen; \"></i>";
        for (int i = 1; i <= ilevel; i++)
        {

            if (i > 1)
            {
                sStar_all = sStar_all + "&nbsp;" + sStar;
            }
            else
            {
                sStar_all = sStar;
            }
        }

        return sStar_all;
    }

    public string getLabelExamination(int _iOpen_flag,int _iStatus)
    {
        string sStar = "";
        if (_iStatus == 1) // CssClass
        {
            if (_iOpen_flag == 1)
            {
                sStar = "btn btn-success btn-sm";
            }
            else
            {
                sStar = "btn btn-danger btn-sm";
            }
        }
        else if (_iStatus == 2) // data-original-title
        {
            if (_iOpen_flag == 1)
            {
                sStar = "เปิดให้ทำแบบทดสอบหลังการฝึกอบรม";
            }
            else
            {
                sStar = "ยังไม่เปิดให้ทำแบบทดสอบหลังการฝึกอบรม";
            }
        }
        else if (_iStatus == 3) // i class,icon
        {
            if (_iOpen_flag == 1)
            {
                sStar = "fa fa-folder-open";
            }
            else
            {
                sStar = "fa fa-folder";
            }
        }

        return sStar;
    }
    private void genQrCode_Test(int _id, string _docno)
    {
        if (_id == 0)
        {
            return;
        }

        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];

        string newFilePath = "";
        newFilePath = _PathFile + _folder_plan_course_qrcode + "/" + _docno + "/";
        try
        {
            Directory.CreateDirectory(Server.MapPath(newFilePath));
        }
        catch { }
        data_elearning dataelearning_detail;
        training_course obj_detail;
        dataelearning_detail = new data_elearning();
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.u0_training_course_idx = _id;
        obj_detail.operation_status_id = "U0-FULL-QRCODE";
        dataelearning_detail.el_training_course_action[0] = obj_detail;
        dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
        string _qrcode_test = "", _qrcode_evaluationform = "";
        foreach (var item in dataelearning_detail.el_training_course_action)
        {
            _qrcode_test = item.qrcode_test;
            _qrcode_evaluationform = item.qrcode_evaluationform;
        }
        if (_qrcode_evaluationform == null)
        {
            _qrcode_evaluationform = "";
        }
        if (_qrcode_test == null)
        {
            _qrcode_test = "";
        }
        if (_qrcode_evaluationform == "")
        {
            dataelearning_detail = new data_elearning();
            dataelearning_detail.el_training_course_action = new training_course[1];
            obj_detail = new training_course();
            obj_detail.u0_training_course_idx = _id;
            obj_detail.operation_status_id = "evaluationform";
            dataelearning_detail.el_training_course_action[0] = obj_detail;
            dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
            if (dataelearning_detail.el_training_course_action == null)
            {
                _qrcode_evaluationform = "ไม่พบข้อมูลแบบประเมิน";
            }
        }
        if (_qrcode_evaluationform == "")
        {
            string txtGenQrCode = _func_dmu._LikeGenQrCode_evaluation + _funcTool.getEncryptRC4(_id.ToString(), "id");
            string getPathimages = newFilePath;
            string fileNameimage = _docno + "_qrcode_evaluationform"; // ชื่อรูป QRCode
            string fileimagePath = Server.MapPath(getPathimages + fileNameimage);

            QRCodeEncoder encoder = new QRCodeEncoder();
            Bitmap bi = encoder.Encode(txtGenQrCode.ToString());

            bi.Save(Server.MapPath(getPathimages + fileNameimage + ".jpg"), ImageFormat.Jpeg);

            dataelearning_detail = new data_elearning();
            dataelearning_detail.el_training_course_action = new training_course[1];
            obj_detail = new training_course();
            obj_detail.qrcode_evaluationform = fileNameimage + ".jpg";
            obj_detail.u0_training_course_idx = _id;
            obj_detail.operation_status_id = "U0-QRCODE-EVALUATION";
            dataelearning_detail.el_training_course_action[0] = obj_detail;
            _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, dataelearning_detail);

            _qrcode_evaluationform = fileNameimage + ".jpg";
        }
        if (_qrcode_test == "")
        {
            dataelearning_detail = new data_elearning();
            dataelearning_detail.el_training_course_action = new training_course[1];
            obj_detail = new training_course();
            obj_detail.u0_training_course_idx = _id;
            obj_detail.operation_status_id = "course_test";
            dataelearning_detail.el_training_course_action[0] = obj_detail;
            dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
            if (dataelearning_detail.el_training_course_action == null)
            {
                _qrcode_test = "ไม่พบข้อมูลแบบทดสอบ";
            }
        }
        if (_qrcode_test == "")
        {
            string txtGenQrCode = _func_dmu._LikeGenQrCode_test + _funcTool.getEncryptRC4(_id.ToString(), "id");
            string getPathimages = newFilePath;
            string fileNameimage = _docno + "_qrcode_test"; // ชื่อรูป QRCode
            string fileimagePath = Server.MapPath(getPathimages + fileNameimage);

            QRCodeEncoder encoder = new QRCodeEncoder();
            Bitmap bi = encoder.Encode(txtGenQrCode.ToString());

            bi.Save(Server.MapPath(getPathimages + fileNameimage + ".jpg"), ImageFormat.Jpeg);

            dataelearning_detail = new data_elearning();
            dataelearning_detail.el_training_course_action = new training_course[1];
            obj_detail = new training_course();
            obj_detail.qrcode_test = fileNameimage + ".jpg";
            obj_detail.u0_training_course_idx = _id;
            obj_detail.operation_status_id = "U0-QRCODE-TEST";
            dataelearning_detail.el_training_course_action[0] = obj_detail;
            _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, dataelearning_detail);

            _qrcode_test = fileNameimage + ".jpg";
        }

        if ((_qrcode_evaluationform != "") || (_qrcode_test != ""))
        {
            string URL = ResolveClientUrl(ResolveUrl("~/el_TrnCourse_qrcode/"));
            URL = URL + _funcTool.getEncryptRC4(_id.ToString(), "id");

            ScriptManager.RegisterStartupScript(this, this.GetType(), "show window",
            "shwwindow('" + URL + "');", true);

        }
        
    }
    private void update_open_flag(int _id,int _open_flag)
    {
        if(_open_flag == 1)
        {
            _open_flag = 0;
        }
        else
        {
            _open_flag = 1;
        }
        data_elearning dataelearning_detail;
        training_course obj_detail;
        dataelearning_detail = new data_elearning();
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.open_flag = _open_flag;
        obj_detail.u0_training_course_idx = _id;
        obj_detail.operation_status_id = "U0-OPEN-COURES";
        obj_detail.training_course_updated_by = emp_idx;
        dataelearning_detail.el_training_course_action[0] = obj_detail;
        _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, dataelearning_detail);
        int iPageIndex = 0;
        iPageIndex = GvList.PageIndex;
        GvList.PageIndex = iPageIndex;
        GvList.DataBind();
        actionIndex();
    }

}