﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_epn_certificate : System.Web.UI.Page
{

    #region connect
    data_employee _dtEmployee = new data_employee();
    function_tool _funcTool = new function_tool();
    List<certificate_doc_detail> _data_group_selected = new List<certificate_doc_detail>();

    data_certificate_doc _dtepn = new data_certificate_doc();


    string _localJson = String.Empty;
    public string checkfile;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    static string urlget_InsertSup = _serviceUrl + ConfigurationManager.AppSettings["urlget_InsertCertificate"];
    static string urlget_SelectSup = _serviceUrl + ConfigurationManager.AppSettings["urlget_SelectCertificate"];
    static string urlget_DeleteSup = _serviceUrl + ConfigurationManager.AppSettings["urlget_DeleteCertificate"];
    static string urlget_UpdateSup = _serviceUrl + ConfigurationManager.AppSettings["urlget_UpdateCertificate"];


    static string urlget_Inserttype = _serviceUrl + ConfigurationManager.AppSettings["urlget_InsertCertificate"];
    static string urlget_Selecttype = _serviceUrl + ConfigurationManager.AppSettings["urlget_SelectCertificate"];
    static string urlget_Deletetype = _serviceUrl + ConfigurationManager.AppSettings["urlget_DeleteCertificate"];
    static string urlget_Updatetype = _serviceUrl + ConfigurationManager.AppSettings["urlget_UpdateCertificate"];

    static string urlget_SelecLocation = _serviceUrl + ConfigurationManager.AppSettings["urlget_SelectCertificate"];
    static string urlget_SelectDept = _serviceUrl + ConfigurationManager.AppSettings["urlget_SelectCertificate"];
    static string urlget_Selectfrequency = _serviceUrl + ConfigurationManager.AppSettings["urlget_SelectCertificate"];
    static string urlget_SelectMonth = _serviceUrl + ConfigurationManager.AppSettings["urlget_SelectCertificate"];
    static string urlget_selectAlert = _serviceUrl + ConfigurationManager.AppSettings["urlget_SelectCertificate"];
    static string urlget_selectAlert_m1 = _serviceUrl + ConfigurationManager.AppSettings["urlget_SelectCertificate"];
    static string urlget_selectAllMonth = _serviceUrl + ConfigurationManager.AppSettings["urlget_SelectCertificate"];
    static string urlget_selectepn_document = _serviceUrl + ConfigurationManager.AppSettings["urlget_SelectCertificate"];
    static string urlget_permission = _serviceUrl + ConfigurationManager.AppSettings["urlget_SelectCertificate"];


    static string urlget_insertEpn = _serviceUrl + ConfigurationManager.AppSettings["urlget_InsertCertificate"];
    static string urlget_insertDate = _serviceUrl + ConfigurationManager.AppSettings["urlget_InsertCertificate"];
    static string urlget_insertMonth = _serviceUrl + ConfigurationManager.AppSettings["urlget_InsertCertificate"];




    int checkcomment = 1;
    int checkcount;
    DateTime datemodify;
    DateTime dateexp;
    DateTime dateproblem;
    DateTime dateget;

    int _emp_idx = 0;
    int _temp_idx = 0;

    #endregion

    #region Pageload
    protected void Page_Load(object sender, EventArgs e)
    {
        //ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        if (!Page.IsPostBack)
        {

            select_empIdx_present();
            MvMaster.SetActiveView(ViewIndex);
            checkPermision();
            Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
            Select_MasterSystemCer(GvMasterCer,txtserchCer.Text);
            SetDefaultpage(4);

            
        }

        linkBtnTrigger(_divMenuBtnToDivMasterSupplier);
        GridViewTrigger(GvIndex);
        GridViewTrigger(GvMasterSup);
        linkBtnTrigger(_divMenuBtnMain);
        linkBtnTrigger(_divMenuBtnToDivAdd);
        linkBtnTrigger(_divMenuBtnToDivMasterCertificate);
        linkBtnTrigger(btnAdddata_supplier);
        linkBtnTrigger(btnCanclesup);
        linkBtnTrigger(btnSearchCer);
        linkBtnTrigger(btnRefreshCer);
   
       

    }
    #endregion

    #region select && Insert && Update && Delete Master

    #region search
    protected void selectmaster_group(GridView gvName, string group_name)
    {
        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dataSelect = new certificate_doc_detail();


        dataSelect.condition = 9;
        dataSelect.sup_name = group_name;
        dataSelect.email = group_name;
        dataSelect.status_name = group_name;

        _dtepn.certificate_doc_list[0] = dataSelect;
        //  Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtepn));

        _dtepn = callServicePost(urlget_SelectSup, _dtepn);

        setGridData(gvName, _dtepn.certificate_doc_list);
    }

    


    #endregion

    #region Delete sup
    protected void Delete_MasterSystem()
    {

        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dedelete = new certificate_doc_detail();

        dedelete.condition = 2;
        dedelete.cemp_idx = _emp_idx;
        dedelete.m0_supidx = int.Parse(ViewState["m0_supidx"].ToString());

        _dtepn.certificate_doc_list[0] = dedelete;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        //return;
        _dtepn = callServicePost(urlget_DeleteSup, _dtepn);

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        // setGridData(GvMaster, _dteye.Test_store_eye_list);
        if (_dtepn.ReturnCode == 1)
        {

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('ไม่สามารถลบข้อมูลได้เนื่องจากมีการใช้ข้อมูลดังกล่าวในเอกสารแจ้งเตือนแล้ว ! !')", true);
        }

    }

    #endregion

    #region update sup

    protected void Update_MasterSystem()
    {



        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dataupdate = new certificate_doc_detail();

      //  DropDownList ddlstatussup_Edit = (DropDownList)ViewMastersup.FindControl("ddlstatussup_Edit");

        dataupdate.sup_name = ViewState["sup_name"].ToString();
        dataupdate.email = ViewState["email"].ToString();
        dataupdate.type_system = int.Parse(ViewState["type_system"].ToString());
        dataupdate.condition = 2;
        dataupdate.uemp_idx = _emp_idx;
        dataupdate.m0_supidx = int.Parse(ViewState["m0_supidx"].ToString());
        _dtepn.certificate_doc_list[0] = dataupdate;
         // litDebug.Text = "Test"+ HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        _dtepn = callServicePost(urlget_UpdateSup, _dtepn);

    }
    #endregion

    #region Select sup
    protected void Select_MasterSystem(GridView gvName, string sup_name, string email_sup)
    {
        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dataSelect = new certificate_doc_detail();

        dataSelect.sup_name = txtmaster_searchsupplier.Text;
        dataSelect.email = txtmaster_searchemail.Text;
        //dataSelect.type_system = int.Parse(ddlSearchstatus.SelectedValue);

        dataSelect.condition = 2;
        _dtepn.certificate_doc_list[0] = dataSelect;

        // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

        // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtepn));
        _dtepn = callServicePost(urlget_SelectSup, _dtepn);

        //Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

        _funcTool.setGvData(GvMasterSup, _dtepn.certificate_doc_list);

    }
    //selectmaster_supplier(GvMaster_Supplier, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);

    #endregion 

    #region Insert sup
    protected void Insert_MasterSystem()
    {
        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail datainsert = new certificate_doc_detail();

        datainsert.sup_name = txtsupname.Text;

        datainsert.email = txtemail_sup.Text;
        datainsert.type_system = int.Parse(ddlstatus_sup.SelectedValue);
        datainsert.cemp_idx = _emp_idx;
        datainsert.condition = 2;

       // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        _dtepn.certificate_doc_list[0] = datainsert;

        //    Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dteye));
       // 
        _dtepn = callServicePost(urlget_InsertSup, _dtepn);
        // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

        if (_dtepn.ReturnCode == 1)
        {

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('ขออภัยมีข้อมูลนี้อยู่แล้ว !')", true);
        }


    }
    #endregion


    #region Delete cer
    protected void Delete_MasterSystemCer()
    {

        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dedelete = new certificate_doc_detail();

        dedelete.condition = 1;
        dedelete.cemp_idx = _emp_idx;
        dedelete.type_idx = int.Parse(ViewState["type_idx"].ToString());

        _dtepn.certificate_doc_list[0] = dedelete;

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dteye));

        _dtepn = callServicePost(urlget_Deletetype, _dtepn);

        if (_dtepn.ReturnCode == 1)
        {

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('ไม่สามารถลบข้อมูลได้เนื่องจากมีการใช้ข้อมูลดังกล่าวในเอกสารแจ้งเตือนแล้ว ! !')", true);
        }

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dteye));
        // setGridData(GvMaster, _dteye.Test_store_eye_list);

    }
    #endregion

    #region update cer
    protected void Update_MasterSystemCer()
    {

        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dataupdate = new certificate_doc_detail();

        dataupdate.type_name = ViewState["type_name"].ToString();
        dataupdate.type_status = 1;
        dataupdate.condition = 1;
        dataupdate.cemp_idx = _emp_idx;
        dataupdate.type_idx = int.Parse(ViewState["type_idx"].ToString());
        _dtepn.certificate_doc_list[0] = dataupdate;
        // litDebug.Text = "Test"+ HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        _dtepn = callServicePost(urlget_Updatetype, _dtepn);

    }
    #endregion

    #region select cer

    protected void Select_MasterSystemCer(GridView gvName, string cer_name)
    {
        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dataSelect = new certificate_doc_detail();

        dataSelect.type_name = txtserchCer.Text;
        dataSelect.type_status = int.Parse(ddlSearchCerstatus.SelectedValue);
        dataSelect.condition = 1;
        _dtepn.certificate_doc_list[0] = dataSelect;

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtepn));
        //Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        _dtepn = callServicePost(urlget_Selecttype, _dtepn);

        // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

        _funcTool.setGvData(GvMasterCer, _dtepn.certificate_doc_list);
    }
    #endregion


    #region select Detail



    protected void Select_epn_document_u0() //list หน้ารายการ
    {
        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dataSelect = new certificate_doc_detail();

        dataSelect.LocIDX = int.Parse(ddlLocationsheach.SelectedValue);
        dataSelect.type_idx = int.Parse(ddlTypecertificatesearch.SelectedValue);
        dataSelect.m0_supidx = int.Parse(ddlSuppliersearch.SelectedValue);
        dataSelect.certificate_name = certificatesearch.Text;
        dataSelect.u0_status = int.Parse(ddlStatussearch.SelectedValue);

        dataSelect.admin_idx = _emp_idx;


         dataSelect.condition = 10;
        _dtepn.certificate_doc_list[0] = dataSelect;

         //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtepn));
       // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
         _dtepn = callServicePost(urlget_selectepn_document, _dtepn);

       //  Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

        _funcTool.setGvData(GvIndex, _dtepn.certificate_doc_list);
    }

    protected void Select_update_epn_document_u0(int idx)
    {
        data_certificate_doc _dtepn2 = new data_certificate_doc();


        _dtepn2.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dataSelect = new certificate_doc_detail();




        
        dataSelect.condition = 12;
        dataSelect.u0doc_idx = idx;
        _dtepn2.certificate_doc_list[0] = dataSelect;


         //Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn2));
        _dtepn2 = callServicePost(urlget_selectepn_document, _dtepn2);
        //Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn2));
        // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtepn2));


        setFormViewData(update_FvinsertProblem, _dtepn2.certificate_doc_list);

      //  Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn2));

        if (_dtepn2.certificate_doc_list != null)
        {
            foreach (var item in _dtepn2.certificate_doc_list)
            {
                selectCertificate(item.type_idx.ToString(), "");
                selectSupplier(item.m0_supidx.ToString(), "");
                selectLocation(item.LocIDX.ToString(), "");
                selectDept(item.permission_idx.ToString(), "");
                selectfrequency(item.frequency_idx.ToString(), "");
                Select_Month("", item.u0doc_idx);
                select_Alert(item.m0_notification_idx.ToString(), "");
               // Select_allMonth(item.m1_notification_idx., "");


            }
        }

       // Select_allMonth("0", "");
      // MultiView update_FvinsertProblem = ((MultiView)MvMaster.FindControl("update_FvinsertProblem"));
        DropDownList ddlAlert = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlAlert"));
        Panel pnl_amount = ((Panel)update_FvinsertProblem.FindControl("update_pnl_amount"));
        Panel Pnl_allmounth = ((Panel)update_FvinsertProblem.FindControl("update_Pnl_allmounth"));

        DropDownList ddlallMonth = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlallMonth")); //update_ddlallMonth
         Panel Pnl_month = ((Panel)update_FvinsertProblem.FindControl("update_Pnl_month"));
        TextBox updatem1_notification_idx = (TextBox)update_FvinsertProblem.FindControl("updatem1_notification_idx");




        //update_Pnl_month

        //Select_allMonth(updatem1_notification_idx.Text, "");
        //Select_allMonth(updatem1_notification_idx.Text, "");

        if ((ddlAlert.SelectedValue == "1") && (ddlallMonth.SelectedValue == ""))
        {
            Pnl_allmounth.Visible = false;
            pnl_amount.Visible = true;
            Pnl_month.Visible = false;

            //TextBox txtAddDate_Amount = (TextBox)update_FvinsertProblem.FindControl("update_txtAddDate_Amount");
             Add_Date(); 
            _dtepn2 = new data_certificate_doc();
            _dtepn2.certificate_doc_list = new certificate_doc_detail[1];
            certificate_doc_detail dataSelect_u2 = new certificate_doc_detail();


            dataSelect_u2.condition = 14;
            dataSelect_u2.u0doc_idx = idx;





            _dtepn2.certificate_doc_list[0] = dataSelect_u2;

            //Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn2));
            _dtepn2 = callServicePost(urlget_selectepn_document, _dtepn2);

            

            //ViewState["vsepn_document_u2"] = _dtepn2.certificate_doc_list_u1

            if (_dtepn2.certificate_doc_list_u2 != null)
            {
                foreach (var item in _dtepn2.certificate_doc_list_u2)
                {

                    GridView GvAddDate = (GridView)update_FvinsertProblem.FindControl("Update_GvAddDate");


                    if (ViewState["vsepn_document_u2"] != null)
                    {

                        DataSet dsContacts = (DataSet)ViewState["vsepn_document_u2"];


                        DataRow drContacts = dsContacts.Tables["dsepn_document_u2"].NewRow();


                        drContacts["date_condition"] = item.date_condition;

                        dsContacts.Tables["dsepn_document_u2"].Rows.Add(drContacts);
                        ViewState["vsepn_document_u2"] = dsContacts;
                        setGridData(GvAddDate, dsContacts.Tables["dsepn_document_u2"]);

                    }


                }
            }
        }
        else if ((ddlAlert.SelectedValue == "2") || (ddlallMonth.SelectedValue != ""))//กรณีสร้างใหม่
        {
            DropDownList update_ddlallMonth = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlallMonth"));

            pnl_amount.Visible = false; //จำนวนวัน
            Pnl_allmounth.Visible = true; //ทุกๆวันที่
            Pnl_month.Visible = true; // Grid เดือน
           // Pnl_month

            // Data Bind ทุกๆวันที่  กรณี Edit
            _dtepn2 = new data_certificate_doc();
            _dtepn2.certificate_doc_list = new certificate_doc_detail[1];
            certificate_doc_detail dataSelect_u1 = new certificate_doc_detail();


            dataSelect_u1.condition = 13;
            dataSelect_u1.u0doc_idx = idx;
            //dataSelect_u1.m1_notification_idx = int.Parse(update_ddlallMonth.SelectedValue);


           // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtepn2));

            _dtepn2.certificate_doc_list[0] = dataSelect_u1;
            
            _dtepn2 = callServicePost(urlget_selectepn_document, _dtepn2);

            Literal1.Text = updatem1_notification_idx.Text;

            Select_allMonth(updatem1_notification_idx.Text, "");
            Select_Month("", idx);


        }
        





    }


    protected void Delete_epn_document(int idx,int condition)
    {

        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dedelete = new certificate_doc_detail();

        dedelete.condition = condition;
        dedelete.cemp_idx = _emp_idx;
        dedelete.u0doc_idx = idx;

        _dtepn.certificate_doc_list[0] = dedelete;

       // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

        _dtepn = callServicePost(urlget_DeleteSup, _dtepn);

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dteye));
        // setGridData(GvMaster, _dteye.Test_store_eye_list);

    }
    #endregion

    #region insert cer
    protected void Insert_MasterSystemCer()
    {
        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail datainsert = new certificate_doc_detail();


        datainsert.type_name = txtnamecer.Text;
        datainsert.type_status = int.Parse(ddlstatus.SelectedValue);
        datainsert.cemp_idx = _emp_idx;
        datainsert.condition = 1;

        _dtepn.certificate_doc_list[0] = datainsert;

        // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

        _dtepn = callServicePost(urlget_Inserttype, _dtepn);
        // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        if (_dtepn.ReturnCode == 1)
        {

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('ขออภัยมีข้อมูลนี้อยู่แล้ว !')", true);
        }


    }

    #endregion

    #region Data set
    protected void Add_Date()
    {
        string sDs = "dsepn_document_u2";
        string sVs = "vsepn_document_u2";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);

        ds.Tables[sDs].Columns.Add("id", typeof(int));
        ds.Tables[sDs].Columns.Add("u2doc_idx", typeof(int));
        ds.Tables[sDs].Columns.Add("u0doc_idx", typeof(int));
        ds.Tables[sDs].Columns.Add("date_condition", typeof(int));
        ds.Tables[sDs].Columns.Add("cemp_idx", typeof(int));
        ds.Tables[sDs].Columns.Add("create_date", typeof(String));
        ds.Tables[sDs].Columns.Add("update_date", typeof(String));
        ds.Tables[sDs].Columns.Add("u2_status", typeof(int));
        ds.Tables[sDs].Columns.Add("uemp_idx", typeof(int));

        ViewState[sVs] = ds;

    }

    protected void Add_Month()
    {
        string sDs = "dsepn_document_u1";
        string sVs = "vsepn_document_u1";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);

        ds.Tables[sDs].Columns.Add("id", typeof(int));
        ds.Tables[sDs].Columns.Add("u1doc_idx", typeof(int));
        ds.Tables[sDs].Columns.Add("u0doc_idx", typeof(int));
        ds.Tables[sDs].Columns.Add("month_idx", typeof(int));
        ds.Tables[sDs].Columns.Add("m1_notification_idx", typeof(int));
        ds.Tables[sDs].Columns.Add("cemp_idx", typeof(int));
        ds.Tables[sDs].Columns.Add("create_date", typeof(String));
        ds.Tables[sDs].Columns.Add("update_date", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_status", typeof(int));

        ViewState[sVs] = ds;

    }
    #endregion


    

    #endregion


    #region Select ProfileEmp
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + _emp_idx);

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX"] = _dtEmployee.employee_list[0].jobgrade_idx;
        ViewState["EmpType"] = _dtEmployee.employee_list[0].emp_type_idx;

    }

    #endregion

    #region Select Select ProfileEmp create


    protected void select_empIdx_create()
    {
        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["CEmpIDX_u0"].ToString());

        ViewState["rdept_name_create"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx_create"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName_create"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name_create"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx_create"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode_create"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname_create"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx_create"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email_create"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel_create"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname_create"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx_create"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX_create"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX_create"] = _dtEmployee.employee_list[0].jobgrade_idx;
    }
    
    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    protected data_certificate_doc callServicePost(string _cmdUrl, data_certificate_doc _dtepn)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtepn);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtepn = (data_certificate_doc)_funcTool.convertJsonToObject(typeof(data_certificate_doc), _localJson);


        return _dtepn;

    }


    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        ddlName.Items.Clear();
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setRdoData(RadioButtonList rdoName, Object obj, string _data_text, string _data_value)
    {
        rdoName.Items.Clear();
        rdoName.DataSource = obj;
        rdoName.DataTextField = _data_text;
        rdoName.DataValueField = _data_value;
        rdoName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        chkName.Items.Clear();
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setRepeatData(Repeater rpName, Object obj)
    {
        rpName.DataSource = obj;
        rpName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }


    protected void GridViewTrigger(GridView gridview)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = gridview.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='fa fa-check-circle'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='fa fa-times-circle'></i></span>";
        }
    }

    protected void GenerateddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }


    public string DateToDB(string date)
    {
        if (date != string.Empty)
        {
            var dateSplit = date.Split('/');
            try
            {
                date = dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
            }
            catch
            {

                dateSplit = date.Split('.');
                date = dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
            }

            return date;
        }
        return string.Empty;
    }


    #endregion

    #region Directories_File URL


    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            //GridView gvFile = (GridView)FvdetailProblem.FindControl("gvFile");
            //if (dt1.Rows.Count > 0)
            //{

            //    ds1.Tables.Add(dt1);
            //    // gvFileLo1.Visible = true;
            //    gvFile.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            //    gvFile.DataBind();
            //    ds1.Dispose();
            //}
            //else
            //{

            //    gvFile.DataSource = null;
            //    gvFile.DataBind();

            //}
            //checkfile = "0";
        }
        catch
        {
            checkfile = "11";
        }
    }

    #endregion

    #region SetDefault button
    protected void SetDefaultpage(int page)
    {

        switch (page)
        {
            
            case 1:
               // MultiView update_FvinsertProblem = ((MultiView)MvMaster.FindControl("update_FvinsertProblem"));
                _divMenuLiToViewAdd.Attributes.Add("class", "bg-color-active");
                Add_Date();
                Add_Month();
                MvMaster.SetActiveView(ViewInsert);
                setOntop.Focus();
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();
                FvinsertProblem.ChangeMode(FormViewMode.Insert);
                FvinsertProblem.DataBind();
                FvinsertProblem.Visible = true;
                update_FvinsertProblem.Visible = false;
               
             
                break;

            case 2:
                _divMenuLiToViewMaster.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewMastersup);
                setOntop.Focus();
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);

                break;

            case 3:
                _divMenuBtnToDivMasterCertificate.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewMasterCer);
                setOntop.Focus();
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();
                Select_MasterSystemCer(GvMasterCer, txtserchCer.Text);
                break;

            case 4:

                _divMenuBtnMainPage.Attributes.Add("class", "bg-color-active");
                _divMenuLiToViewMaster.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuBtnToDivMasterCertificate.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewIndex);
                Select_epn_document_u0();
                FvinsertProblem.Visible = false;
                update_FvinsertProblem.Visible = true;
                selectCertificate("", "S");
                selectSupplier("", "S");
                selectLocation("", "S");
               
            



                break;



        }
    }
    #endregion

    #region Gridview
    
    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvIndex":
            


            case "GvMasterSup":

                //litDebug.Text = e.Row.RowState.ToString();
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;


            case "GvMasterCer":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;

            case "update_gvGroupList":
                
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cbGroupSelected = (CheckBox)e.Row.FindControl("update_cbGroupSelected");
                    HiddenField hfM0Idx = (HiddenField)e.Row.FindControl("hfM0Idx");
                    HiddenField update_u1_idx = (HiddenField)e.Row.FindControl("update_u1_idx");
                    CheckBox cbGroupSel = (CheckBox)e.Row.FindControl("cbGroupSel");


                    if (update_u1_idx.Value != null)
                    {

                        if ((update_u1_idx.Value != "0" ) && (update_u1_idx.Value != ""))
                        {
                            cbGroupSelected.Checked = true;
                        }

                    }
                    else
                    {

                        cbGroupSelected.Checked = false;
                    }

                    
                }

                break;
                
            
        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        

        switch (GvName.ID)
        {
            case "GvMasterSup":

                GvMasterSup.PageIndex = e.NewPageIndex;
                GvMasterSup.DataBind();
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);

                break;

            case "GvMasterCer":
                GvMasterCer.PageIndex = e.NewPageIndex;
                GvMasterCer.DataBind();
                Select_MasterSystemCer(GvMasterCer, txtserchCer.Text);
                break;


            case "GvAddDate":

                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();

                break;

            case "GvIndex":

                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                Select_epn_document_u0();
                break;
        }
    }

    #endregion

    #region onRowCommand

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            var GvName = (GridView)sender;
            GridViewRow rowSelect;
            DataSet dsContacts;
            string[] argument = new string[10];
            string cmdName = e.CommandName; ;

            switch (GvName.ID)
            {
                case "update_GvAddDate":
                    rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    if (GvName.Rows[rowSelect.RowIndex].RowType == DataControlRowType.DataRow)
                    {
                        if (cmdName == "update_btndeleteProduct")
                        {
                            dsContacts = (DataSet)ViewState["vsepn_document_u2"];
                            dsContacts.Tables["dsepn_document_u2"].Rows[rowSelect.RowIndex].Delete();
                            dsContacts.AcceptChanges();
                            setGridData(GvName, dsContacts.Tables["dsepn_document_u2"]);
                        }
                    }

                    break;

                case "GvAddDate":
                    rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    if (GvName.Rows[rowSelect.RowIndex].RowType == DataControlRowType.DataRow)
                    {
                        if (cmdName == "btndeleteProduct")
                        {
                            dsContacts = (DataSet)ViewState["vsepn_document_u2"];
                            dsContacts.Tables["dsepn_document_u2"].Rows[rowSelect.RowIndex].Delete();
                            dsContacts.AcceptChanges();
                            setGridData(GvName, dsContacts.Tables["dsepn_document_u2"]);
                        }
                    }

                    break;
            }

        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMasterSup":

                GvMasterSup.EditIndex = e.NewEditIndex;
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                btnshow.Visible = false;
                //selectmaster_group(GvMasterSup, txtmaster_searchgroup.Text);
                break;

            case "GvMasterCer":
                GvMasterCer.EditIndex = e.NewEditIndex;
                Select_MasterSystemCer(GvMasterCer, txtserchCer.Text);
                btnshow.Visible = false;

                break;
        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMasterSup":
                 GvMasterSup.EditIndex = -1;
                 Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                 btnshow.Visible = true;
                 //selectmaster_group(GvMasterSup, txtmaster_searchgroup.Text);
                break;
            case "GvMasterCer":
                GvMasterCer.EditIndex = -1;
                Select_MasterSystemCer(GvMasterCer, txtserchCer.Text);
                btnshow.Visible = true;
                break;
        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMasterSup":

                int m0_supidx = Convert.ToInt32(GvMasterSup.DataKeys[e.RowIndex].Values[0].ToString());
                var txtsup_name_edit = (TextBox)GvMasterSup.Rows[e.RowIndex].FindControl("txtsup_name_edit");
                var txtEmail_sup_edit = (TextBox)GvMasterSup.Rows[e.RowIndex].FindControl("txtEmail_sup_edit");
                var ddlstatussup_Edit = (DropDownList)GvMasterSup.Rows[e.RowIndex].FindControl("ddlstatussup_Edit");


                GvMasterSup.EditIndex = -1;

                ViewState["m0_supidx"] = m0_supidx;
                ViewState["sup_name"] = txtsup_name_edit.Text;
                ViewState["email"] = txtEmail_sup_edit.Text;
                ViewState["type_system"] = ddlstatussup_Edit.SelectedValue;

                btnshow.Visible = true;
                Update_MasterSystem();
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
               // selectmaster_group(GvMasterSup, txtmaster_searchgroup.Text);

                break;

            case "GvMasterCer":
                int type_idx = Convert.ToInt32(GvMasterCer.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_edit = (TextBox)GvMasterCer.Rows[e.RowIndex].FindControl("txtname_edit");
                var ddStatusUpdate = (DropDownList)GvMasterCer.Rows[e.RowIndex].FindControl("ddStatusUpdate");


                GvMasterCer.EditIndex = -1;

                ViewState["type_idx"] = type_idx;
                ViewState["type_name"] = txtname_edit.Text;
                ViewState["type_status"] = ddStatusUpdate.SelectedValue;
                btnshow.Visible = true;
                Update_MasterSystemCer();
                Select_MasterSystemCer(GvMasterCer, txtserchCer.Text);
                break;
        }
    }

    #endregion

    #region mergeCell

    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {

        }
    }

    #endregion
    
    #region SetActive

    protected void setActiveView(string activeView, int page_index, int doc_idx)
    {
        MvMaster.Visible = true;
        
        MvMaster.SetActiveView((View)MvMaster.FindControl(activeView));
           
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    #endregion

    #region FormView

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {


        if (sender is FormView)
        {
            var FvName = (FormView)sender;
            switch (FvName.ID)
            {
                case "FvDetailUser":
                    FormView FvDetailUser = (FormView)ViewInsert.FindControl("FvDetailUser");

                    if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                    {
                        var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                        var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                        var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                        var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                        var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                        var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                        var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                        var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                        txtempcode.Text = ViewState["EmpCode"].ToString();
                        txtrequesname.Text = ViewState["FullName"].ToString();
                        txtorg.Text = ViewState["Org_name"].ToString();
                        txtrequesdept.Text = ViewState["rdept_name"].ToString();
                        txtsec.Text = ViewState["Secname"].ToString();
                        txtpos.Text = ViewState["Positname"].ToString();
                        txttel.Text = ViewState["Tel"].ToString();
                        txtemail.Text = ViewState["Email"].ToString();


                    }
                    break;

                case "FvinsertProblem":
                    //  LinkButton btnAdddata = ((LinkButton)FvinsertProblem.FindControl("btnAdddata"));
                    selectCertificate("0","A");
                    selectSupplier("0","A");
                    selectLocation("0", "A");
                    selectDept("0", "A");
                    selectfrequency("0", "A");
                    Select_Month("A",0);
                    select_Alert("0", "A");
                    //select_Alert_m1("0", "A");
                    Select_allMonth("0", "A");

                    break;


            }

        }
        else if (sender is View)
        {

            var VName = (View)sender;
            switch (VName.ID)
            {

                case "ViewIndex":

                    selectCertificate("", "S");

                    break;

            }
        }
    }
    #endregion

    #endregion
    
    #region check permission
    protected void checkPermision()
    {
        data_certificate_doc _dtepn1 = new data_certificate_doc();

        _dtepn1.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail _select = new certificate_doc_detail();

        _select.admin_idx = _emp_idx;
        _select.condition = 11;

        //litDebug.Text = _emp_idx.ToString();
        _dtepn1.certificate_doc_list[0] = _select;
      //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn1));
        _dtepn1 = callServicePost(urlget_selectepn_document, _dtepn1);
      // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn1));
        //text.Text = _dataEmployee.ToString();

       // litDebug.Text = _dtepn.ReturnCode.ToString();

        if (_dtepn1.certificate_doc_list != null)
        {
            _divMenuBtnToDivAdd.Enabled = true;
            _divMenuBtnToDivMasterSupplier.Enabled = true;
            _divMenuBtnToDivMasterCertificate.Enabled = true;
        }
        else
        {

            _divMenuBtnToDivAdd.Enabled = false;
            _divMenuBtnToDivMasterSupplier.Enabled = false;
            _divMenuBtnToDivMasterCertificate.Enabled = false;
        }
        //_divMenuBtnToDivAdd.Enabled = true;

    }
    #endregion

    #region setAdd_Date_m0List
    protected void setAdd_Date_m0List(int status,string aEdit)
    {

        //FormView _Fv;
        //if (status == 1)
        //{
        //    _Fv = FvinsertProblem;
        //}
        //else
        //{
        //    _Fv = FvinsertProblem;
        //}//dsepn_document_u2


        TextBox txtAddDate_Amount = (TextBox)FvinsertProblem.FindControl("txtAddDate_Amount");
        TextBox update_txtAddDate_Amount = (TextBox)update_FvinsertProblem.FindControl("update_txtAddDate_Amount");
        //  DropDownList ddlAddid_product = (DropDownList)_Fv.FindControl("ddlAddid_product");
        GridView GvAddDate = (GridView)FvinsertProblem.FindControl("GvAddDate");
        GridView update_GvAddDate = (GridView)update_FvinsertProblem.FindControl("update_GvAddDate");

        if (aEdit == "A")
        {
            if (ViewState["vsepn_document_u2"] != null)
            {

                DataSet dsContacts = (DataSet)ViewState["vsepn_document_u2"];


                DataRow drContacts = dsContacts.Tables["dsepn_document_u2"].NewRow();


                drContacts["date_condition"] = int.Parse(txtAddDate_Amount.Text);

                dsContacts.Tables["dsepn_document_u2"].Rows.Add(drContacts);
                ViewState["vsepn_document_u2"] = dsContacts;
                setGridData(GvAddDate, dsContacts.Tables["dsepn_document_u2"]);

            }
        }
        else if (aEdit == "E")
        {
            if (ViewState["vsepn_document_u2"] != null)
            {

                DataSet dsContacts = (DataSet)ViewState["vsepn_document_u2"];


                DataRow drContacts = dsContacts.Tables["dsepn_document_u2"].NewRow();


                drContacts["date_condition"] = int.Parse(update_txtAddDate_Amount.Text);

                dsContacts.Tables["dsepn_document_u2"].Rows.Add(drContacts);
                ViewState["vsepn_document_u2"] = dsContacts;
                setGridData(update_GvAddDate, dsContacts.Tables["dsepn_document_u2"]);

            }


        }

    }
    #endregion

 
    #region SelectedIndexChanged
    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        
            DropDownList ddlName = (DropDownList)sender;
            DropDownList ddlfequncy = ((DropDownList)FvinsertProblem.FindControl("ddlfequncy"));
          
           
            DropDownList ddlAlert = ((DropDownList)FvinsertProblem.FindControl("ddlAlert"));
           
            Panel pnl_alert = ((Panel)FvinsertProblem.FindControl("pnl_alert"));
            Panel PnlAlert = ((Panel)FvinsertProblem.FindControl("PnlAlert"));
            
            
            Panel Panel_AddMasterSupplier = ((Panel)ViewMastersup.FindControl("Panel_AddMasterSupplier"));
            DropDownList ddlchoose_addsup = ((DropDownList)Panel_AddMasterSupplier.FindControl("ddlchoose_addsup"));
            UpdatePanel UpdatePanel = ((UpdatePanel)Panel_AddMasterSupplier.FindControl("UpdatePanel"));

            DropDownList update_ddlallMonth = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlallMonth"));
            DropDownList ddlallMonth = ((DropDownList)FvinsertProblem.FindControl("ddlallMonth"));
        // Panel Pnl_month = ((Panel)FvinsertProblem.FindControl("Pnl_month"));
           Panel Pnl_month = ((Panel)FvinsertProblem.FindControl("Pnl_month"));


        switch (ddlName.ID)
            {
           //ddlAlert
            case "ddlAlert":

                Panel Pnl_allmounth = ((Panel)FvinsertProblem.FindControl("Pnl_allmounth"));
                Panel pnl_amount = ((Panel)FvinsertProblem.FindControl("pnl_amount"));
               // Panel Pnl_month = ((Panel)FvinsertProblem.FindControl("Pnl_month"));


                if (ddlName.SelectedValue == "1")//ล่วงหน้า
                {

                   
                     Pnl_allmounth.Visible = false;
                     pnl_amount.Visible = true;
                     Pnl_month.Visible = false;



                }
                else if (ddlName.SelectedValue == "2")//ทุกวันที่
                {

                    pnl_amount.Visible = false;
                    Pnl_allmounth.Visible = true;
                    Pnl_month.Visible = true;




                }
                break;


            case "update_ddlAlert":

                // Edit
                Panel update_Pnl_allmounth = ((Panel)update_FvinsertProblem.FindControl("update_Pnl_allmounth"));
                Panel update_pnl_amount = ((Panel)update_FvinsertProblem.FindControl("update_pnl_amount"));
                Panel update_Pnl_month = ((Panel)update_FvinsertProblem.FindControl("update_Pnl_month"));
               

                if (ddlName.SelectedValue == "1")
                {
                    
                    update_Pnl_allmounth.Visible = false;
                    update_pnl_amount.Visible = true;
                    update_Pnl_month.Visible = false;

                }
                else if (ddlName.SelectedValue == "2")
                {

                    
                    update_Pnl_allmounth.Visible = true;
                    update_pnl_amount.Visible = false;
                    update_Pnl_month.Visible = true;
                    Select_allMonth("0", "");

                }
                break;

            case "update_ddlallMonth":
                Panel update_Pnl_month_1 = ((Panel)update_FvinsertProblem.FindControl("update_Pnl_month"));
                


                if (update_ddlallMonth.SelectedValue != "")
                {

                    update_Pnl_month_1.Visible = true;
                }
                else
                {

                    update_Pnl_month_1.Visible = false;
                }
                break;


            case "ddlallMonth":

               // Panel Pnl_month = ((Panel)FvinsertProblem.FindControl("Pnl_month"));


                if (ddlallMonth.SelectedValue != "")
                {
                    Pnl_month.Visible = true;
                   
                }
                else
                {
                    Pnl_month.Visible = false;
                   
                }
                break;


            case "ddlchoose_addsup":

               // TextBox Button1 = ((TextBox)FvinsertProblem.FindControl("Button1"));

                if (ddlchoose_addsup.SelectedValue == "1")
                {


                    div_addmanualsup.Visible = true;
                    div_addimportsup.Visible = false;
                    Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                   
                }
                else if (ddlchoose_addsup.SelectedValue == "2")
                {
                
                   
                    div_addimportsup.Visible = true;
                    div_addmanualsup.Visible = false;
                    Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);



                }
                
                break;
        }

    }
    #endregion

    #region data excel
    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName, int _set_value)
    {
        IWorkbook workbook;


        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }

        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);

        ICellStyle testeStyle = workbook.CreateCellStyle();
        testeStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Medium;
        testeStyle.FillForegroundColor = IndexedColors.BrightGreen.Index;
        testeStyle.FillPattern = FillPattern.SolidForeground;

        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }

        //set merge cell
        int max_row = dt.Rows.Count - 1;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);

            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());

            }

        }

        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);

            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));

                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }


    }
    #endregion data excel

    #region BTN
   
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        
        int m0_supidx = 0;
        int type_idx = 0;
        int u0doc_idx = 0;
       // var GvName = (GridView)sender;

        switch (cmdName)
        {


            case "_divMenuBtnToDivAdd":

                SetDefaultpage(1);
                break;

            case "_divMenuBtnToDivMasterSupplier":
                SetDefaultpage(2);
                break;

            case "_divMenuBtnToDivMasterCertificate":
                SetDefaultpage(3);
                break;

            case "_divMenuBtnMain":
                SetDefaultpage(4);
                break;



            //--------------- Master Supplier  --------------------------//

            case "btnRefreshMaster_Supplier":
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                break;
            case "CmdSearchMaster_Supplier":
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);

                break;

            case "btnCmn":
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                Panel_AddMasterSupplier.Visible = true;
                btnshow.Visible = false;
                div_searchmaster_supplier.Visible = false;
                //Pnl_SearchSup.Visible = false;
                break;

            case "Update":
                Insert_MasterSystem();
                btnshow.Visible = true;
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                GvMasterSup.Visible = true;
                break;

            case "CmdInsertMaster_Supplier":

                Insert_MasterSystem();
                div_addmanualsup.Visible = true;
                btnshow.Visible = true;
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                GvMasterSup.Visible = true;
                div_addmanualsup.Visible = false;
                btnshow.Visible = true;
                div_addimportsup.Visible = false;
                Panel_AddMasterSupplier.Visible = false;
                div_searchmaster_supplier.Visible = true;
                ddlchoose_addsup.SelectedValue = "0";
                txtsupname.Text = "";
                txtemail_sup.Text = "";
                break;

            case "CmdDel":
                m0_supidx = int.Parse(cmdArg);
                ViewState["m0_supidx"] = m0_supidx;
                div_addmanualsup.Visible = false;
                btnshow.Visible = true;
              //  div_addmanualsup.Visible = false;
              //  btnshow.Visible = true;
                Delete_MasterSystem();
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                
                break;

            case "btnCmnD":
                div_addmanualsup.Visible = false;
                btnshow.Visible = true;
                Panel_AddMasterSupplier.Visible = false;
                break;

            case "btnCancelMaster_Supplier":
                div_addmanualsup.Visible = false;
                btnshow.Visible = true;
                div_addimportsup.Visible = false;
                Panel_AddMasterSupplier.Visible = false;
                div_searchmaster_supplier.Visible = true;
                break;

            case "Cancel":
                Delete_MasterSystem();
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);

                break;

            case "CmdImportMaster_Sup":
                if (UploadFileSup.HasFile)
                {
                    string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff_import");
                    string FileName = Path.GetFileName(UploadFileSup.PostedFile.FileName);
                    string extension = Path.GetExtension(UploadFileSup.PostedFile.FileName);
                    string newFileName = datetimeNow + extension.ToLower();
                    string folderPath = ConfigurationManager.AppSettings["path_supplier_import"];
                    string filePath = Server.MapPath(folderPath + newFileName);

                    if (extension.ToLower() == ".xls")
                    {
                        UploadFileSup.SaveAs(filePath);
                        ImportFileSuppliername(filePath, extension, "Yes", FileName,_emp_idx, 2);
                        File.Delete(filePath);
                        //SetDefaultpage(10);
                    }
                    else
                    {
                        _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls) เท่านั้น");
                    }
                }
                break;



            //--------------- Master certificate  --------------------------//

            case "btnCmncer":
                Pnl_add.Visible = true;
                btnshowCer.Visible = false;
                PnlSerchCer.Visible = false;
                break;


              
            case "btnCmnDcer":
                Pnl_add.Visible = false;
                btnshowCer.Visible = true;
                PnlSerchCer.Visible = true;

                break;

            case "btnCmnAcer":
                Insert_MasterSystemCer();
                //Insert_MasterSystem();
                Pnl_add.Visible = false;
                btnshowCer.Visible = true;
                Select_MasterSystemCer(GvMasterCer, txtserchCer.Text);
                GvMasterCer.Visible = true;
                break;

            case "CmdDelcer":
                type_idx = int.Parse(cmdArg);
                ViewState["type_idx"] = type_idx;
                Pnl_add.Visible = false;
                btnshowCer.Visible = true;
                Delete_MasterSystemCer();
                Select_MasterSystemCer(GvMasterCer, txtserchCer.Text);
                break;


            case "btnRefreshMaster_cer":
                Select_MasterSystemCer(GvMasterCer, txtserchCer.Text);
                break;
            case "CmdSearchMaster_cer":
                Select_MasterSystemCer(GvMasterCer, txtserchCer.Text);

                break;

            //-------------------   บันทึกค่าลง ----------------//




            case "btnAddItem":
                setAdd_Date_m0List(int.Parse(cmdArg),"A");

                break;

            case "CmdSave":


                Insert_EPN_System(0);
                Select_epn_document_u0();
                SetDefaultpage(4);
                
                break;

            case "ddlTypecertificatesearch":
              //  searchselectCertificate();
                break;

            case "btnItemEdit":
              

                _divMenuLiToViewAdd.Attributes.Add("class", "bg-color-active");
                Add_Date();
                Add_Month();
                MvMaster.SetActiveView(ViewInsert);
                setOntop.Focus();
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();
                update_FvinsertProblem.ChangeMode(FormViewMode.Edit);
                Select_update_epn_document_u0(int.Parse(cmdArg));
               // setAdd_Date_m0List(int.Parse(cmdArg), "E");
                //update_FvinsertProblem.DataSource();
                //update_FvinsertProblem.DataBind();

                break;

            case "update_btnAddItem":
                setAdd_Date_m0List(int.Parse(cmdArg),"E");
                break;


            case "update_CmdSave":
                // updatekey.
                TextBox updatekey = (TextBox)update_FvinsertProblem.FindControl("updatekey");
                Insert_EPN_System(int.Parse(updatekey.Text));
                SetDefaultpage(4);

                break;

            case "btnrefresh_list":


                MvMaster.SetActiveView(ViewIndex);
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                Select_MasterSystemCer(GvMasterCer, txtserchCer.Text);
                SetDefaultpage(4);
                GvIndex.PageIndex = 1;
                GvIndex.DataBind();
                break;

            case "update_BtnBack":

                MvMaster.SetActiveView(ViewIndex);
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                Select_MasterSystemCer(GvMasterCer, txtserchCer.Text);
                SetDefaultpage(4);
                GvIndex.PageIndex = 0;
                GvIndex.DataBind();

                break;

            case "BtnBack":
                MvMaster.SetActiveView(ViewIndex);
                Select_MasterSystem(GvMasterSup, txtmaster_searchsupplier.Text, txtmaster_searchemail.Text);
                Select_MasterSystemCer(GvMasterCer, txtserchCer.Text);
                SetDefaultpage(4);
                GvIndex.PageIndex = 0;
                GvIndex.DataBind();

                break;


            case "btnsearch_list":
                Select_epn_document_u0();
                break;

            case "btnItemDel":
                u0doc_idx  = int.Parse(cmdArg);
                Delete_epn_document(int.Parse(cmdArg),3);
                Select_epn_document_u0();
                break;

        }
    }

    #endregion


    protected void ImportFileSuppliername(string filePath, string Extension, string isHDR, string fileName, int cempidx, int condition)
    {
        string conStr = String.Empty;
        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
        conStr = String.Format(conStr, filePath, "Yes");
        OleDbConnection connExcel = new OleDbConnection(conStr);
        OleDbCommand cmdExcel = new OleDbCommand();
        OleDbDataAdapter oda = new OleDbDataAdapter();
        DataTable dt = new DataTable();

        cmdExcel.Connection = connExcel;
        connExcel.Open();
        DataTable dtExcelSchema;
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
        connExcel.Close();
        connExcel.Open();
        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
        oda.SelectCommand = cmdExcel;
        oda.Fill(dt);
        connExcel.Close();

        
        
        string idxCreated = String.Empty;
        for (var i = 0; i <= dt.Rows.Count - 1;)
        {
            if (dt.Rows[i][0].ToString().Trim() != "")
            {
                _dtepn = new data_certificate_doc();
                _dtepn.certificate_doc_list = new certificate_doc_detail[1];
                certificate_doc_detail import = new certificate_doc_detail();
                

                import.sup_name = dt.Rows[i][0].ToString().Trim();
                import.email = dt.Rows[i][1].ToString().Trim();
                import.type_system = 1;
                import.condition = condition;
                import.cemp_idx = cempidx;
                _dtepn.certificate_doc_list[0] = import;
                _dtepn = callServicePost(urlget_InsertSup, _dtepn);


                //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));


            }
            i++;
        }
        



        //_dtepn.certificate_doc_list_u1 = new certificate_doc_detail_u1[i1];
        //i = 0; i1 = 0;
        //foreach (GridViewRow gridrow in gvGroupList.Rows)
        //{
        //    cbGroupSelected = (CheckBox)gridrow.FindControl("cbGroupSelected");
        //    Label lblmonthName = (Label)gridrow.FindControl("lblmonthName");
        //    HiddenField hfM0Idx = (HiddenField)gridrow.FindControl("hfM0Idx");


        //    certificate_doc_detail_u1 _group_list = new certificate_doc_detail_u1();
        //    _group_list.month_idx = 0;

        //    if (cbGroupSelected.Checked == true)
        //    {
        //        _group_list.month_idx = int.Parse(hfM0Idx.Value);



        //        _dtepn.certificate_doc_list_u1[i1] = _group_list;
        //        i1++;
        //    }

        //}
    }

    #region select and insert ddl && txt
    protected void selectCertificate(string idx,string Astatus) // ประเภทเอกสาร
    {
        _dtepn = new data_certificate_doc();

        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail _select = new certificate_doc_detail();

        _select.condition = 1;
        _dtepn.certificate_doc_list[0] = _select;
       
        _dtepn = callServicePost(urlget_Selecttype, _dtepn);

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        DropDownList ddltype;
        if (Astatus == "A")
        {
             ddltype = ((DropDownList)FvinsertProblem.FindControl("ddltype"));

        }
        else if (Astatus == "S")
        {

            ddltype = ddlTypecertificatesearch;
        }
        else
        {
             ddltype = ((DropDownList)update_FvinsertProblem.FindControl("update_ddltype"));

        }

       
        setDdlData(ddltype, _dtepn.certificate_doc_list, "type_name", "type_idx");
        ddltype.Items.Insert(0, new ListItem("กรุณาเลือกประเภท certificate...", "0"));
        if (idx == null || idx == "")
        {

            idx = "0";
        }
        

        ddltype.SelectedValue = idx;
    }
    
    protected void selectSupplier(string idx, string Astatus) // supplier
    {
        _dtepn = new data_certificate_doc();

        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail _select = new certificate_doc_detail();

        _select.condition = 2;
        _dtepn.certificate_doc_list[0] = _select;

        _dtepn = callServicePost(urlget_SelectSup, _dtepn);

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        DropDownList ddlSupplier;

        if (Astatus == "A")
        {
           ddlSupplier = ((DropDownList)FvinsertProblem.FindControl("ddlSupplier"));

        }
        else if(Astatus == "S")
        {

            ddlSupplier = ddlSuppliersearch;

        }
        else
        {
            ddlSupplier = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlSupplier"));

        }

        setDdlData(ddlSupplier, _dtepn.certificate_doc_list, "sup_name", "m0_supidx");
        ddlSupplier.Items.Insert(0, new ListItem("กรุณาเลือกชื่อ Supplier...", "0"));

        if (idx == null || idx == "")
        {

            idx = "0";
        }
        ddlSupplier.SelectedValue = idx;
    }
    
    protected void selectLocation(string idx,string Astatus) // สถานที่
    {
        _dtepn = new data_certificate_doc();

        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail _select = new certificate_doc_detail();

        _select.condition = 3;
        _dtepn.certificate_doc_list[0] = _select;

        _dtepn = callServicePost(urlget_SelecLocation, _dtepn);

       // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        DropDownList ddlPlan;

        if (Astatus == "A") {
            ddlPlan = ((DropDownList)FvinsertProblem.FindControl("ddlPlan"));
        }
        else if (Astatus == "S")
        {

            ddlPlan = ddlLocationsheach;// ((DropDownList)ViewIndex.FindControl("ddlLocationsheach"));
            //if (int.Parse(ddlPlan.SelectedValue) > 0)
            //{

            //    _select.LocIDX = int.Parse(ddlPlan.SelectedValue);

            //}

        }
        else
        {

            ddlPlan = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlPlan"));

        }

        setDdlData(ddlPlan, _dtepn.certificate_doc_list, "LocName", "LocIDX");
        ddlPlan.Items.Insert(0, new ListItem("กรุณาเลือกสถานที่...", "0"));
        if (idx == null || idx == "")
        {

            idx = "0";
        }
        ddlPlan.SelectedValue = idx;
    }
    
    protected void selectDept(string idx,string Astatus) // ฝ่าย
    {
        _dtepn = new data_certificate_doc();

        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail _select = new certificate_doc_detail();

        _select.condition = 4;
        _dtepn.certificate_doc_list[0] = _select;

        _dtepn = callServicePost(urlget_SelectDept, _dtepn);

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        DropDownList ddlDept;
        if (Astatus == "A")
        {
            ddlDept = ((DropDownList)FvinsertProblem.FindControl("ddlDept"));
        }
        else
        {
            ddlDept = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlDept"));
        }

        setDdlData(ddlDept, _dtepn.certificate_doc_list, "permission_name", "permission_idx");
        ddlDept.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
        ddlDept.SelectedValue = idx;
    }

    protected void selectfrequency(string idx,string Astatus) //ทุก 1 เดือน, ทุก 3 เดือน
    {
        _dtepn = new data_certificate_doc();

        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail _select = new certificate_doc_detail();

        _select.condition = 5;
        _dtepn.certificate_doc_list[0] = _select;

        _dtepn = callServicePost(urlget_Selectfrequency, _dtepn);

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

        DropDownList ddlfequncy;
        if (Astatus == "A")
        {
            ddlfequncy = ((DropDownList)FvinsertProblem.FindControl("ddlfequncy"));
        }
        else
        {
            ddlfequncy = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlfequncy"));
        }

        setDdlData(ddlfequncy, _dtepn.certificate_doc_list, "frequency_name", "frequency_idx");
        ddlfequncy.Items.Insert(0, new ListItem("กรุณาเลือกประเภทความถี่...", "0"));
        ddlfequncy.SelectedValue = idx;
    }

    protected void Select_Month(string Astatus,int idx) // เลือกเดือน
    {
        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dataSelect = new certificate_doc_detail();


        if (Astatus == "A")
        {
            dataSelect.condition = 6;

        }
        else
        {
            dataSelect.condition = 13;
            dataSelect.u0doc_idx = idx;

        }
           
        _dtepn.certificate_doc_list[0] = dataSelect;

       
        // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

       
         _dtepn = callServicePost(urlget_SelectMonth, _dtepn);
        //Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        GridView gvGroupList;
        if (Astatus == "A")
        {
            gvGroupList = ((GridView)FvinsertProblem.FindControl("gvGroupList"));
            _funcTool.setGvData(gvGroupList, _dtepn.certificate_doc_list);
        }
        else
        {

           
            gvGroupList = ((GridView)update_FvinsertProblem.FindControl("update_gvGroupList"));
            _funcTool.setGvData(gvGroupList, _dtepn.certificate_doc_list_u1);
        }


        

    }


    protected void Select_allMonth(string idx,string Aststus) //1 ของเดือน , 15ของเดือน
    {
        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dataSelect = new certificate_doc_detail();

        dataSelect.condition = 8;
        _dtepn.certificate_doc_list[0] = dataSelect;


       

        //  Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
         _dtepn = callServicePost(urlget_selectAllMonth, _dtepn);

       // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

        DropDownList ddlallMonth;
        if (Aststus == "A")
        {
            ddlallMonth = ((DropDownList)FvinsertProblem.FindControl("ddlallMonth"));
        }
        else
        {
            ddlallMonth = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlallMonth"));
        }

        setDdlData(ddlallMonth, _dtepn.certificate_doc_list, "m1_notification_name", "m1_notification_idx");
        ddlallMonth.Items.Insert(0, new ListItem("กรุณาเลือกทุกของวันที่...", "0"));
        if (idx == null || idx == "") {

            idx = "0";
        }
           ddlallMonth.SelectedValue = idx;
        

    }

    protected void select_Alert(string idx,string Astatus)//ทุกวันที่,ล่วงหน้า
    {
        _dtepn = new data_certificate_doc();

        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail _select = new certificate_doc_detail();

        _select.condition = 7;
        _dtepn.certificate_doc_list[0] = _select;

        _dtepn = callServicePost(urlget_selectAlert, _dtepn);

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        DropDownList ddlAlert;
        if (Astatus == "A")
        {
            ddlAlert = ((DropDownList)FvinsertProblem.FindControl("ddlAlert"));
        }
        else
        {
            ddlAlert = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlAlert"));

        }

        setDdlData(ddlAlert, _dtepn.certificate_doc_list, "m0_notification_name", "m0_notification_idx");
        ddlAlert.Items.Insert(0, new ListItem("กรุณาเลือกประเภทการแจ้งเตือน...", "0"));

        ddlAlert.SelectedValue = idx;

    }

    //protected void select_Alert_m1(string idx,string Astatus)
    //{
    //    _dtepn = new data_certificate_doc();

    //    _dtepn.certificate_doc_list = new certificate_doc_detail[1];
    //    certificate_doc_detail _select = new certificate_doc_detail();

    //    _select.condition = 8;
    //    _dtepn.certificate_doc_list[0] = _select;

    //    _dtepn = callServicePost(urlget_selectAlert_m1, _dtepn);

    //    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
    //    DropDownList ddlAlertall;
    //    if (Astatus == "A") {
    //         ddlAlertall = ((DropDownList)FvinsertProblem.FindControl("ddlAlertall"));
    //    }
    //    else
    //    {

    //        ddlAlertall = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlAlertall"));
    //    }
    //    setDdlData(ddlAlertall, _dtepn.certificate_doc_list, "m1_notification_name", "m1_notification_idx");
    //    ddlAlertall.Items.Insert(0, new ListItem("กรุณาเลือกการแจ้งเตือน...", "0"));
    //    ddlAlertall.SelectedValue = idx;

    //}

    protected void Insert_EPN_System(int idx)
    {
        if (idx == 0)
        {
            _dtepn.certificate_doc_list = new certificate_doc_detail[1];
            certificate_doc_detail datainsert = new certificate_doc_detail();

            DropDownList ddltype = ((DropDownList)FvinsertProblem.FindControl("ddltype"));
            DropDownList ddlPlan = ((DropDownList)FvinsertProblem.FindControl("ddlPlan"));
            DropDownList ddlSupplier = ((DropDownList)FvinsertProblem.FindControl("ddlSupplier"));

            DropDownList ddlfequncy = ((DropDownList)FvinsertProblem.FindControl("ddlfequncy"));
            DropDownList ddlAlert = ((DropDownList)FvinsertProblem.FindControl("ddlAlert"));
            TextBox txtCertificate_name = ((TextBox)FvinsertProblem.FindControl("txtCertificate_name"));
            TextBox txtdetail = ((TextBox)FvinsertProblem.FindControl("txtdetail"));
            TextBox txtContact = ((TextBox)FvinsertProblem.FindControl("txtContact"));
            TextBox txtTel = ((TextBox)FvinsertProblem.FindControl("txtTel"));
            TextBox txtdatestart = ((TextBox)FvinsertProblem.FindControl("txtdatestart"));
            TextBox txtdateexp = ((TextBox)FvinsertProblem.FindControl("txtdateexp"));
            TextBox txtremark = ((TextBox)FvinsertProblem.FindControl("txtremark"));
            Panel Pnl_month = ((Panel)FvinsertProblem.FindControl("Pnl_month"));
            GridView gvGroupList = ((GridView)FvinsertProblem.FindControl("gvGroupList"));
            CheckBox cbGroupSelected = ((CheckBox)FvinsertProblem.FindControl("cbGroupSelected"));
            DropDownList ddlAlertall = ((DropDownList)FvinsertProblem.FindControl("ddlAlertall"));
            DropDownList ddlallMonth = ((DropDownList)FvinsertProblem.FindControl("ddlallMonth"));


            // Literal1.Text = "xxxxxxxxxxxxxx : "+txtremark.Text;
            //--------------- epn_document_u0 -------------------//
            datainsert.cemp_idx = _emp_idx;
            datainsert.uemp_idx = _emp_idx;
            datainsert.type_idx = int.Parse(ddltype.SelectedValue);
            datainsert.LocIDX = int.Parse(ddlPlan.SelectedValue);
            datainsert.m0_supidx = int.Parse(ddlSupplier.SelectedValue);
            datainsert.certificate_name = txtCertificate_name.Text;
            datainsert.certificate_detail = txtdetail.Text;
            datainsert.contact = txtContact.Text;
            datainsert.phone_no = txtTel.Text;
            datainsert.date_start = txtdatestart.Text;
            datainsert.date_expire = txtdateexp.Text;
            datainsert.remark = txtremark.Text;
            datainsert.frequency_idx = int.Parse(ddlfequncy.SelectedValue);
            datainsert.m0_notification_idx = int.Parse(ddlAlert.SelectedValue);
            datainsert.notification_idx = int.Parse(ddlallMonth.SelectedValue);
            datainsert.u0_status = 1;
            datainsert.condition = 3;

            _dtepn.certificate_doc_list[0] = datainsert;

            //Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
            //_dtepn = callServicePost(urlget_insertEpn, _dtepn);
            // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

            // ----------------- epn_document_u1 --------------//
            if (ddlAlert.SelectedValue == "2")
            {

                //Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

                gvGroupList = (GridView)FvinsertProblem.FindControl("gvGroupList");

                // _dtepn.certificate_doc_list_u1 = new certificate_doc_detail_u1[gvGroupList.Rows.Count];
                int i = 0, i1 = 0;
                foreach (GridViewRow gridrow in gvGroupList.Rows)
                {
                    cbGroupSelected = (CheckBox)gridrow.FindControl("cbGroupSelected");
                    Label lblmonthName = (Label)gridrow.FindControl("lblmonthName");
                    HiddenField hfM0Idx = (HiddenField)gridrow.FindControl("hfM0Idx");

                    if (cbGroupSelected.Checked == true)
                    {


                        i1++;
                    }


                    i++;
                }
                if (i1 == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือก เดือน .!');", true);
                    return;
                }
                else
                {
                    _dtepn.certificate_doc_list_u1 = new certificate_doc_detail_u1[i1];
                    i = 0; i1 = 0;
                    foreach (GridViewRow gridrow in gvGroupList.Rows)
                    {
                        cbGroupSelected = (CheckBox)gridrow.FindControl("cbGroupSelected");
                        Label lblmonthName = (Label)gridrow.FindControl("lblmonthName");
                        HiddenField hfM0Idx = (HiddenField)gridrow.FindControl("hfM0Idx");


                        certificate_doc_detail_u1 _group_list = new certificate_doc_detail_u1();
                        _group_list.month_idx = 0;

                        if (cbGroupSelected.Checked == true)
                        {
                            _group_list.month_idx = int.Parse(hfM0Idx.Value);
                            _group_list.m1_notification_idx = int.Parse(ddlallMonth.SelectedValue);



                            _dtepn.certificate_doc_list_u1[i1] = _group_list;
                            i1++;
                        }

                    }

                }
                // _dtepn = callServicePost(urlget_insertMonth, _dtepn);
                // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));


            }
            else if (ddlAlert.SelectedValue == "1")  // ----------------- epn_document_u2 --------------//
            {

                //_dtepn = new data_certificate_doc();

                _dtepn.certificate_doc_list_u2 = new certificate_doc_detail_u2[1];
                //certificate_doc_detail_u2 datainsert_u2 = new certificate_doc_detail_u2();

                TextBox txtAddDate_Amount = ((TextBox)FvinsertProblem.FindControl("txtAddDate_Amount"));
                GridView GvAddDate = ((GridView)update_FvinsertProblem.FindControl("GvAddDate"));


                certificate_doc_detail_u2 datainsert_u2 = new certificate_doc_detail_u2();

                int itemObj = 0;
                DataSet sDs = (DataSet)ViewState["vsepn_document_u2"];
                certificate_doc_detail_u2[] obj_data = new certificate_doc_detail_u2[sDs.Tables["dsepn_document_u2"].Rows.Count];
                foreach (DataRow item in sDs.Tables["dsepn_document_u2"].Rows)
                {
                    obj_data[itemObj] = new certificate_doc_detail_u2();
                    obj_data[itemObj].u2_status = 1;
                    obj_data[itemObj].cemp_idx = _emp_idx;
                   // obj_data[itemObj].uemp_idx = _emp_idx;
                    obj_data[itemObj].date_condition = int.Parse(item["date_condition"].ToString());

                    itemObj++;

                }

                _dtepn.certificate_doc_list_u2 = obj_data;
                ///   _dtepn = callServicePost(urlget_insertDate, _dtepn);
                //  Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));




            }

            _dtepn = callServicePost(urlget_insertEpn, _dtepn);
            // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

        }else if (idx != 0)
        {
            _dtepn.certificate_doc_list = new certificate_doc_detail[1];
            certificate_doc_detail datainsert = new certificate_doc_detail();

            DropDownList ddltype = ((DropDownList)update_FvinsertProblem.FindControl("update_ddltype"));
            DropDownList ddlPlan = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlPlan"));
            DropDownList ddlSupplier = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlSupplier"));

            DropDownList ddlfequncy = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlfequncy"));
            DropDownList ddlAlert = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlAlert"));
            TextBox txtCertificate_name = ((TextBox)update_FvinsertProblem.FindControl("update_txtCertificate_name"));
            TextBox txtdetail = ((TextBox)update_FvinsertProblem.FindControl("update_txtdetail"));
            TextBox txtContact = ((TextBox)update_FvinsertProblem.FindControl("update_txtContact"));
            TextBox txtTel = ((TextBox)update_FvinsertProblem.FindControl("update_txtTel"));
            TextBox txtdatestart = ((TextBox)update_FvinsertProblem.FindControl("update_txtdatestart"));
            TextBox txtdateexp = ((TextBox)update_FvinsertProblem.FindControl("update_txtdateexp"));
            TextBox txtremark = ((TextBox)update_FvinsertProblem.FindControl("update_txtremark"));
            Panel Pnl_month = ((Panel)update_FvinsertProblem.FindControl("update_Pnl_month"));
            GridView gvGroupList = ((GridView)update_FvinsertProblem.FindControl("update_gvGroupList"));
            CheckBox cbGroupSelected = ((CheckBox)update_FvinsertProblem.FindControl("update_cbGroupSelected"));
            DropDownList ddlAlertall = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlAlertall"));
            DropDownList update_ddlallMonth = ((DropDownList)update_FvinsertProblem.FindControl("update_ddlallMonth"));
            //TextBox txtAddDate_Amount = ((TextBox)update_FvinsertProblem.FindControl("update_txtAddDate_Amount"));


            // Literal1.Text = "xxxxxxxxxxxxxx : "+txtremark.Text;
            //--------------- epn_document_u0 -------------------//
            datainsert.cemp_idx = _emp_idx;
            datainsert.type_idx = int.Parse(ddltype.SelectedValue);
            datainsert.LocIDX = int.Parse(ddlPlan.SelectedValue);
            datainsert.m0_supidx = int.Parse(ddlSupplier.SelectedValue);
            datainsert.certificate_name = txtCertificate_name.Text;
            datainsert.certificate_detail = txtdetail.Text;
            datainsert.contact = txtContact.Text;
            datainsert.phone_no = txtTel.Text;
            datainsert.date_start = txtdatestart.Text;
            datainsert.date_expire = txtdateexp.Text;
            datainsert.remark = txtremark.Text;
            datainsert.frequency_idx = int.Parse(ddlfequncy.SelectedValue);
            datainsert.m0_notification_idx = int.Parse(ddlAlert.SelectedValue);
            datainsert.u0_status = 1;
            datainsert.condition = 3;
            datainsert.u0doc_idx = idx;
            //datainsert.notification_idx = int.Parse(update_ddlallMonth.SelectedValue);
            //datainsert.date_condition = int.Parse(txtAddDate_Amount.Text);

            _dtepn.certificate_doc_list[0] = datainsert;


            //_dtepn = callServicePost(urlget_insertEpn, _dtepn);
            // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

            // ----------------- epn_document_u1 --------------//
            if (ddlAlert.SelectedValue == "2")
            {

                //Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

                //gvGroupList = (GridView)FvinsertProblem.FindControl("gvGroupList");

                // _dtepn.certificate_doc_list_u1 = new certificate_doc_detail_u1[gvGroupList.Rows.Count];
                int i = 0, i1 = 0;
                foreach (GridViewRow gridrow in gvGroupList.Rows)
                {
                    cbGroupSelected = (CheckBox)gridrow.FindControl("update_cbGroupSelected");
                    Label lblmonthName = (Label)gridrow.FindControl("update_lblmonthName");
                    HiddenField hfM0Idx = (HiddenField)gridrow.FindControl("update_hfM0Idx");

                    if (cbGroupSelected.Checked == true)
                    {


                        i1++;
                    }


                    i++;
                }
                if (i1 == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือก เดือน .!');", true);
                    return;
                }
                else
                {
                    _dtepn.certificate_doc_list_u1 = new certificate_doc_detail_u1[i1];
                    i = 0; i1 = 0;
                    foreach (GridViewRow gridrow in gvGroupList.Rows)
                    {
                        cbGroupSelected = (CheckBox)gridrow.FindControl("update_cbGroupSelected");
                        Label lblmonthName = (Label)gridrow.FindControl("update_lblmonthName");
                        HiddenField hfM0Idx = (HiddenField)gridrow.FindControl("update_hfM0Idx");


                        certificate_doc_detail_u1 _group_list = new certificate_doc_detail_u1();
                        _group_list.month_idx = 0;

                        if (cbGroupSelected.Checked == true)
                        {
                            _group_list.month_idx = int.Parse(hfM0Idx.Value);
                            _group_list.m1_notification_idx = int.Parse(update_ddlallMonth.SelectedValue);
                           // _group_list.uemp


                            _dtepn.certificate_doc_list_u1[i1] = _group_list;
                            i1++;
                        }
                        

                    }

                }
                datainsert.uemp_idx = _emp_idx;
               
                //datainsert.m1_notification_idx = int.Parse(update_ddlallMonth.SelectedValue);
                // _dtepn = callServicePost(urlget_insertMonth, _dtepn);
                // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));


            }
            else if (ddlAlert.SelectedValue == "1")  // ----------------- epn_document_u2 --------------//
            {

                //_dtepn = new data_certificate_doc();

                _dtepn.certificate_doc_list_u2 = new certificate_doc_detail_u2[1];
                

                TextBox txtAddDate_Amount = ((TextBox)update_FvinsertProblem.FindControl("update_txtAddDate_Amount"));
                GridView update_GvAddDate = ((GridView)update_FvinsertProblem.FindControl("update_GvAddDate"));

                //datainsert_u2.date_condition = int.Parse(txtAddDate_Amount.Text);
                //datainsert_u2.cemp_idx = _emp_idx;
                // datainsert_u2.u2_status = 1;
                // certificate_doc_detail_u2[] datainsert_u2 = new certificate_doc_detail_u2[update_GvAddDate.Rows.Count];


                //_dtepn.certificate_doc_list_u2[0] = datainsert_u2;

                ///   _dtepn = callServicePost(urlget_insertDate, _dtepn);
                // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
                certificate_doc_detail_u2 datainsert_u2 = new certificate_doc_detail_u2();

                int itemObj = 0;
                DataSet sDs = (DataSet)ViewState["vsepn_document_u2"];
                certificate_doc_detail_u2[] obj_data = new certificate_doc_detail_u2[sDs.Tables["dsepn_document_u2"].Rows.Count];
                foreach (DataRow item in sDs.Tables["dsepn_document_u2"].Rows)
                {
                    obj_data[itemObj] = new certificate_doc_detail_u2();
                    obj_data[itemObj].u2_status = 1;
                    obj_data[itemObj].cemp_idx = _emp_idx;
                    obj_data[itemObj].date_condition = int.Parse(item["date_condition"].ToString());

                    itemObj++;

                }

                _dtepn.certificate_doc_list_u2 = obj_data;


              
            }
            //Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
            _dtepn = callServicePost(urlget_insertEpn, _dtepn);
            // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));





        }

    }//Insert ลง Table

    
    


    #endregion

    #region checkboxDate
    protected void cbCheckedChanged(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        switch (cb.ID)
        {
            case "cbGroupSelected":
                string[] _val_in = cb.Text.Split('|');
                _temp_idx = _funcTool.convertToInt(_val_in[0]);
                if (ViewState["GroupSelected"] != null)
                {
                    _data_group_selected = (List<certificate_doc_detail>)ViewState["GroupSelected"];
                    if (cb.Checked)
                    {
                        var check_selected = _data_group_selected.Where(d =>
                        d.month_idx == _temp_idx).FirstOrDefault();
                        if (check_selected == null)
                        {
                            certificate_doc_detail _group_list = new certificate_doc_detail();
                            _group_list.month_idx = _temp_idx;
                            _data_group_selected.Add(_group_list);
                        }
                    }
                    else
                    {
                        _data_group_selected.RemoveAll(l => l.month_idx == _temp_idx);
                    }
                }
                
                ViewState["GroupSelected"] = _data_group_selected;
                break;
        }
    }
    #endregion

    #region showMsAlert
    public void showMsAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }

    #endregion
}