﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="import_employee_nat.aspx.cs" Inherits="websystem_hr_import_employee_nat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
        <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litdebug" runat="server"></asp:Literal>

            <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="tabDetail" runat="server">
                            <asp:LinkButton ID="lbtab0" runat="server" CommandName="cmdtab0" OnCommand="navCommand" CommandArgument="tabDetail">Detail</asp:LinkButton>
                        </li>
                        <li id="tabInsert" runat="server">
                            <asp:LinkButton ID="lbtab1" runat="server" CommandName="cmdtab1" OnCommand="navCommand" CommandArgument="tabInsert">Import</asp:LinkButton>
                        </li>
                       
                    </ul>


                </div>
            </div>
        </nav>

     <asp:MultiView ID="mvImport_emp_nat" runat="server">

         <asp:View ID="vImportDetail" runat="server">
             
         </asp:View>

         <asp:View ID="vImportInsert" runat="server">
             <asp:FormView ID="ImportInsert" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Import</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">

                                    <div class="form-horizontal" role="form">

                                        <div class="panel-heading">



                                            <div class="form-group">
                                               
                                                <asp:Label ID="labelFile_Upload" runat="server" class="col-sm-3 control-label">ไฟล์แนบ</asp:Label>
                                                <div class="col-sm-6">
                                                   
                                                    <asp:FileUpload ID="File_Upload" Font-Size="small" ViewStateMode="Enabled" runat="server" ClientIDMode="Static" CssClass="control-label"/>
                                                   
                                                </div>
                                                <div class="col-sm-3">
                                                    </div>
                                                </div>
                                            <div class="form-group">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbsearch" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdCheck" CommandArgument=",Insert">
                                                <i class="fas fa-search" aria-hidden="true"></i>&nbsp;เช็คข้อมูล</asp:LinkButton>

                                                    <asp:LinkButton ID="lbsearch_cancel" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument=",TypeNewsmange" CausesValidation="false">
                                                    <i class="fas fa-redo" aria-hidden="true"></i>&nbsp;รีเซ็ต
                                                
                                                    </asp:LinkButton>
                                                </div>
                                                <%-- <label class="col-sm-3 control-label"></label>--%>
                                         
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>
                                  
                    </InsertItemTemplate>
                </asp:FormView>

         </asp:View>

     </asp:MultiView>
</asp:Content>

