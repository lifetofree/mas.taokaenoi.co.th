﻿using DotNet.Highcharts;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_manpower : System.Web.UI.Page
{
    #region Connect
    data_employee _dtEmployee = new data_employee();
    function_tool _funcTool = new function_tool();
    data_hr_manpower _dtman = new data_hr_manpower();

    string _localJson = String.Empty;
    ////int[] posidx_hradmin = { 5908, 1019 };
    public string checkfile;
    int checkrow_sex = 0;
    int checkrow_nation = 0;
    int checkrow_dtset = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];
    static string _urlSelectMaster_ManPower = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMaster_ManPower"];
    static string _urlInsert_ManPower = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_ManPower"];
    static string _urlApprove_ManPower = _serviceUrl + ConfigurationManager.AppSettings["urlApprove_ManPower"];

    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] =  int.Parse(Session["emp_idx"].ToString());
            ViewState["count_row"] = "0";
            ViewState["checkrow_dtset"] = "0";

            select_empIdx_present();
            SetDefaultpage(1);
            select_hradmin(int.Parse(ViewState["Sec_idx"].ToString()));
        }

        linkBtnTrigger(_divMenuBtnToDivIndex);
        linkBtnTrigger(_divMenuBtnToDivAdd);
        linkBtnTrigger(_divMenuBtnToDivApprove);
        linkBtnTrigger(_divMenuBtnToDivCancel);
        linkBtnTrigger(_divMenuBtnToDivReport);
        linkBtnTrigger(_divMenuBtnToDivManual);

        linkBtnTrigger(_divMenuBtnToDivWaitingNormal);
        linkBtnTrigger(_divMenuBtnToDivWaitingSpecial);
        linkBtnTrigger(_divMenuBtnToDivWaitingCancel);

    }

    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX"] = _dtEmployee.employee_list[0].jobgrade_idx;
        ViewState["EmpType"] = _dtEmployee.employee_list[0].emp_type_idx;

    }

    protected void select_empIdx_create()
    {
        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["CEmpIDX_u0"].ToString());

        ViewState["rdept_name_create"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx_create"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName_create"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name_create"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx_create"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode_create"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname_create"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx_create"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email_create"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel_create"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname_create"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx_create"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX_create"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX_create"] = _dtEmployee.employee_list[0].jobgrade_idx;
    }

    protected void getOrganizationList(DropDownList ddlName)
    {

        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmp(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dtEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dtEmployee.section_list[0] = _secList;

        _dtEmployee = callServicePostEmp(_urlGetSectionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกแผนก...", "0"));
    }

    protected void select_pos(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dtEmployee.position_list = new position_details[1];
        position_details _posList = new position_details();
        _posList.org_idx = _org_idx;
        _posList.rdept_idx = _rdept_idx;
        _posList.rsec_idx = _rsec_idx;
        _dtEmployee.position_list[0] = _posList;

        _dtEmployee = callServicePostEmp(_urlGetPositionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกตำแหน่ง....", "0"));
    }

    protected void select_sex(GridView gvName)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.condition = 1;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setGridData(gvName, _dtman.Boxu0_DocumentDetail);

    }

    protected void select_typehire(RadioButtonList rdoName)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.condition = 2;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setRdoData(rdoName, _dtman.Boxu0_DocumentDetail, "typehire_name", "m0_thidx");
    }

    protected void select_hire(RadioButtonList rdoName)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.condition = 3;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setRdoData(rdoName, _dtman.Boxu0_DocumentDetail, "hire_name", "m0_hridx");

    }

    protected void select_education(DropDownList ddlName)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.condition = 4;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setDdlData(ddlName, _dtman.Boxu0_DocumentDetail, "education_name", "eduidx");
        ddlName.Items.Insert(0, new ListItem("เลือกวุฒิการศึกษา....", "0"));

    }

    protected void select_emptype(DropDownList ddlName)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.condition = 6;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setDdlData(ddlName, _dtman.Boxu0_DocumentDetail, "EmpTypeName", "EmpTypeIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกประเภทพนักงาน....", "0"));

    }

    protected void select_ListIndex(GridView gvName, int rdeptidx)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.rdeptidx = rdeptidx;
        _select.condition = 7;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setGridData(gvName, _dtman.Boxu0_DocumentDetail);

    }

    protected void select_DetailManPower(GridView gvName, FormView fvname, int u0_docidx)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.u0_docidx = u0_docidx;
        _select.condition = 8;
        _dtman.Boxu0_DocumentDetail[0] = _select;

        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));
        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setFormViewData(fvname, _dtman.Boxu1_DocumentDetail);
        setGridData(gvName, _dtman.Boxu1_DocumentDetail);

    }

    protected void select_DetailManPower_Edit(GridView gvName, int u0_docidx)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.u0_docidx = u0_docidx;
        _select.condition = 8;
        _dtman.Boxu0_DocumentDetail[0] = _select;

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));
        setGridData(gvName, _dtman.Boxu1_DocumentDetail);
    }

    protected void select_ApproveListIndex(GridView gvName, int empidx, int rposidx, int rsecidx, int orgidx,int jobgradeidx)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.CEmpIDX = empidx;
        _select.orgidx = orgidx;
        _select.rsecidx = rsecidx;
        _select.rposidx = rposidx;
        _select.JobGradeIDX = jobgradeidx;

        _select.condition = 10;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setGridData(gvName, _dtman.Boxu0_DocumentDetail);

    }

    protected void selectsum_Approve(int empidx, int rposidx, int rsecidx, int orgidx, int jobgradeidx)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.CEmpIDX = empidx;
        _select.orgidx = orgidx;
        _select.rsecidx = rsecidx;
        _select.rposidx = rposidx;
        _select.JobGradeIDX = jobgradeidx;

        _select.condition = 11;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        nav_approve.Text = "<span class='badge progress-bar-danger' >" + _dtman.ReturnMsg.ToString() + "</span>";

    }

    protected void select_Log(Repeater rpName, int u0_docidx, string doccode, int typeselect)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.u0_docidx = u0_docidx;
        _select.doc_code = doccode;
        _select.type_select = typeselect;
        _select.condition = 12;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);

        setRepeatData(rpName, _dtman.Boxu0_DocumentDetail);
    }

    protected void select_hradmin(int rsecidx)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.rsecidx = rsecidx;
        _select.condition = 13;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        ViewState["hr_admin"] = _dtman.ReturnCode.ToString();
    }

    protected void select_location(DropDownList ddlName, int orgidx)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.condition = 14;
        _select.orgidx = orgidx;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setDdlData(ddlName, _dtman.Boxu0_DocumentDetail, "LocName", "LocIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกสถานที่ปฏิบัติงาน....", "0"));

    }

    protected void select_DetailManPowerForPrint(FormView fvname, int u1_docidx)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.u1_docidx = u1_docidx;
        _select.condition = 15;
        _dtman.Boxu0_DocumentDetail[0] = _select;

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));
        setFormViewData(fvname, _dtman.Boxu1_DocumentDetail);
        ViewState["m0_thidx_print"] = _dtman.Boxu1_DocumentDetail[0].m0_thidx.ToString();
        ViewState["m0_hridx_print"] = _dtman.Boxu1_DocumentDetail[0].m0_hridx.ToString();

    }

    protected void selectdetail_sexprint(GridView gvName, int u1_docidx)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.condition = 9;
        _select.u1_docidx = u1_docidx;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setGridData(gvName, _dtman.Boxu2_DocumentDetail);
    }

    protected void select_typehire_Print(CheckBoxList chkName)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.condition = 2;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setChkData(chkName, _dtman.Boxu0_DocumentDetail, "typehire_name", "m0_thidx");
    }

    protected void select_hire_Print(CheckBoxList chkName)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.condition = 3;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setChkData(chkName, _dtman.Boxu0_DocumentDetail, "hire_name", "m0_hridx");

    }

    protected void select_doccode_cancel(DropDownList ddlName, int rsecidx)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.condition = 16;
        _select.rsecidx = rsecidx;
        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setDdlData(ddlName, _dtman.Boxu0_DocumentDetail, "doc_code", "u0_docidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรายการที่ต้องการยกเลิก....", "0"));

    }

    protected void select_DetailManPower_Cancel(FormView fvname, string dococde)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.doc_code = dococde;
        _select.condition = 17;
        _dtman.Boxu0_DocumentDetail[0] = _select;

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));
        setFormViewData(fvname, _dtman.Boxu0_DocumentDetail);

    }

    protected void selectnode(DropDownList ddlName)
    {
        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.condition = 19;

        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        setDdlData(ddlName, _dtman.Boxu0_DocumentDetail, "node_desc", "noidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานะรายการ....", "0"));

    }



    #endregion

    #region Report
    protected void selectreporttable(GridView gvName)
    {
        DropDownList ddlSearchDate = (DropDownList)fvreport.FindControl("ddlSearchDate");
        TextBox AddStartdate = (TextBox)fvreport.FindControl("AddStartdate");
        TextBox AddEndDate = (TextBox)fvreport.FindControl("AddEndDate");
        DropDownList ddlorgidx_search = (DropDownList)fvreport.FindControl("ddlorgidx_search");
        DropDownList ddlrdeptidx_search = (DropDownList)fvreport.FindControl("ddlrdeptidx_search");
        DropDownList ddlrsecidx_search = (DropDownList)fvreport.FindControl("ddlrsecidx_search");
        DropDownList ddlrposidx_search = (DropDownList)fvreport.FindControl("ddlrposidx_search");
        DropDownList ddlemptype_search = (DropDownList)fvreport.FindControl("ddlemptype_search");
        DropDownList ddlLocate_search = (DropDownList)fvreport.FindControl("ddlLocate_search");
        DropDownList ddlstatus_search = (DropDownList)fvreport.FindControl("ddlstatus_search");
        TextBox txtDocCode = (TextBox)fvreport.FindControl("txtDocCode");

        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();

        _select.condition = 18;
        _select.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
        _select.orgidx = int.Parse(ddlorgidx_search.SelectedValue);
        _select.rdeptidx = int.Parse(ddlrdeptidx_search.SelectedValue);
        _select.rsecidx = int.Parse(ddlrsecidx_search.SelectedValue);
        _select.rposidx = int.Parse(ddlrposidx_search.SelectedValue);
        _select.EmpTypeIDX = int.Parse(ddlemptype_search.SelectedValue);
        _select.LocIDX = int.Parse(ddlLocate_search.SelectedValue);
        _select.unidx = int.Parse(ddlstatus_search.SelectedValue);
        _select.createdate = AddStartdate.Text;
        _select.enddate = AddEndDate.Text;
        _select.doc_code = txtDocCode.Text;

        _dtman.Boxu0_DocumentDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
        ViewState["Data_ReportTable"] = _dtman.Boxu0_DocumentDetail;

        setGridData(gvName, _dtman.Boxu0_DocumentDetail);

    }

    public void Select_ChartLV1()
    {
        DropDownList ddlmonth = (DropDownList)fvreport.FindControl("ddlmonth");
        DropDownList ddlyear = (DropDownList)fvreport.FindControl("ddlyear");

        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();


        _select.condition = 20;
        _select.orgidx = int.Parse(ViewState["Org_idx"].ToString());
        _select.DG_Month = ddlmonth.SelectedValue;
        _select.DG_Year = ddlyear.SelectedValue;

        _dtman.Boxu0_DocumentDetail[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);

        if (_dtman.ReturnCode.ToString() == "0")
        {
            int count = _dtman.Boxu0_DocumentDetail.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtman.Boxu0_DocumentDetail)
            {
                lv1code[i] = data.DeptNameTH.ToString();
                lv1count[i] = data.rdeptidx.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Department",
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_ChartLV2()
    {
        DropDownList ddlmonth = (DropDownList)fvreport.FindControl("ddlmonth");
        DropDownList ddlyear = (DropDownList)fvreport.FindControl("ddlyear");
        DropDownList ddlorg_chart = (DropDownList)fvreport.FindControl("ddlorg_chart");
        DropDownList ddlrdept_chart = (DropDownList)fvreport.FindControl("ddlrdept_chart");

        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();


        _select.condition = 21;
        _select.orgidx = int.Parse(ddlorg_chart.SelectedValue);
        _select.rdeptidx = int.Parse(ddlrdept_chart.SelectedValue);
        _select.DG_Month = ddlmonth.SelectedValue;
        _select.DG_Year = ddlyear.SelectedValue;

        _dtman.Boxu0_DocumentDetail[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);

        if (_dtman.ReturnCode.ToString() == "0")
        {
            int count = _dtman.Boxu0_DocumentDetail.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtman.Boxu0_DocumentDetail)
            {
                lv1code[i] = data.DeptNameTH.ToString();
                lv1count[i] = data.rdeptidx.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Department",
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_ChartLV3()
    {
        DropDownList ddlmonth = (DropDownList)fvreport.FindControl("ddlmonth");
        DropDownList ddlyear = (DropDownList)fvreport.FindControl("ddlyear");
        DropDownList ddlrsec_chart = (DropDownList)fvreport.FindControl("ddlrsec_chart");
        DropDownList ddlrdept_chart = (DropDownList)fvreport.FindControl("ddlrdept_chart");

        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();


        _select.condition = 22;
        _select.rdeptidx = int.Parse(ddlrdept_chart.SelectedValue);
        _select.rsecidx = int.Parse(ddlrsec_chart.SelectedValue);
        _select.DG_Month = ddlmonth.SelectedValue;
        _select.DG_Year = ddlyear.SelectedValue;

        _dtman.Boxu0_DocumentDetail[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);

        if (_dtman.ReturnCode.ToString() == "0")
        {
            int count = _dtman.Boxu0_DocumentDetail.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtman.Boxu0_DocumentDetail)
            {
                lv1code[i] = data.SecNameTH.ToString();
                lv1count[i] = data.rsecidx.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Section",
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_ChartLV4()
    {
        DropDownList ddlmonth = (DropDownList)fvreport.FindControl("ddlmonth");
        DropDownList ddlyear = (DropDownList)fvreport.FindControl("ddlyear");
        DropDownList ddlrsec_chart = (DropDownList)fvreport.FindControl("ddlrsec_chart");
        DropDownList ddlrpos_chart = (DropDownList)fvreport.FindControl("ddlrpos_chart");

        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();


        _select.condition = 23;
        _select.rposidx = int.Parse(ddlrpos_chart.SelectedValue);
        _select.rsecidx = int.Parse(ddlrsec_chart.SelectedValue);
        _select.DG_Month = ddlmonth.SelectedValue;
        _select.DG_Year = ddlyear.SelectedValue;

        _dtman.Boxu0_DocumentDetail[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);

        if (_dtman.ReturnCode.ToString() == "0")
        {
            int count = _dtman.Boxu0_DocumentDetail.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtman.Boxu0_DocumentDetail)
            {
                lv1code[i] = data.PosNameTH.ToString();
                lv1count[i] = data.rposidx.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Position",
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_ChartLV5()
    {
        DropDownList ddlmonth = (DropDownList)fvreport.FindControl("ddlmonth");
        DropDownList ddlyear = (DropDownList)fvreport.FindControl("ddlyear");

        _dtman = new data_hr_manpower();

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail _select = new U0_DocumentDetail();


        _select.condition = 24;
        _select.DG_Month = ddlmonth.SelectedValue;
        _select.DG_Year = ddlyear.SelectedValue;

        _dtman.Boxu0_DocumentDetail[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);

        if (_dtman.ReturnCode.ToString() == "0")
        {
            int count = _dtman.Boxu0_DocumentDetail.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtman.Boxu0_DocumentDetail)
            {
                lv1code[i] = data.createdate.ToString();
                lv1count[i] = data.u1_docidx.ToString();

                for (int ii = 0; ii < lv1code[i].Length; ii++)
                {
                    switch (lv1code[i])
                    {
                        case "1":
                            lv1code[i] = "มกราคม";

                            break;
                        case "2":
                            lv1code[i] = "กุมภาพันธ์";

                            break;
                        case "3":
                            lv1code[i] = "มีนาคม";

                            break;
                        case "4":
                            lv1code[i] = "เมษายน";

                            break;
                        case "5":
                            lv1code[i] = "พฤษภาคม";

                            break;
                        case "6":
                            lv1code[i] = "มิถุนายน";

                            break;
                        case "7":
                            lv1code[i] = "กรกฎาคม";

                            break;
                        case "8":
                            lv1code[i] = "สิงหาคม";

                            break;
                        case "9":
                            lv1code[i] = "กันยายน";

                            break;
                        case "10":
                            lv1code[i] = "ตุลาคม";

                            break;
                        case "11":
                            lv1code[i] = "พฤศจิกายน";

                            break;
                        case "12":
                            lv1code[i] = "ธันวาคม";

                            break;
                    }
                }
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Month",
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    #endregion

    #region Insert / Update / Delete
    protected void Insert_Manpower()
    {
        var txtdatelimit = (TextBox)fvinsert.FindControl("txtdatelimit");
        var ddltypeman = (DropDownList)fvinsert.FindControl("ddltypeman");
        var ddllocate = (DropDownList)fvinsert.FindControl("ddllocate");
        var ddlemptype = (DropDownList)fvinsert.FindControl("ddlemptype");

        var txtreason = (TextBox)fvinsert.FindControl("txtreason");
        var UploadFileOrg = (FileUpload)fvinsert.FindControl("UploadFileOrg");
        var UploadFileJD = (FileUpload)fvinsert.FindControl("UploadFileJD");

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail datainsert = new U0_DocumentDetail();

        datainsert.unidx = 1;
        datainsert.acidx = 1;
        datainsert.staidx = 1;
        datainsert.doc_decision = 1;
        datainsert.orgidx = int.Parse(ViewState["Org_idx"].ToString());
        datainsert.rdeptidx = int.Parse(ViewState["rdept_idx"].ToString());
        datainsert.rsecidx = int.Parse(ViewState["Sec_idx"].ToString());
        datainsert.rposidx = int.Parse(ViewState["Pos_idx"].ToString());
        datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        datainsert.JobGradeIDX = int.Parse(ViewState["JobGradeIDX"].ToString());
        datainsert.condition = 1;
        datainsert.date_limit = int.Parse(txtdatelimit.Text);
        datainsert.m0type_listidx = int.Parse(ddltypeman.SelectedValue);
        datainsert.m0_typeidx = 1;
        datainsert.doc_refidx = 0;
        datainsert.reason_comment = txtreason.Text;
        datainsert.LocIDX = int.Parse(ddllocate.SelectedValue);
        datainsert.EmpTypeIDX = int.Parse(ddlemptype.SelectedValue);

        _dtman.Boxu0_DocumentDetail[0] = datainsert;


        int i = 0;
        var ds_udoc1_insert = (DataSet)ViewState["vsManPowerList"];
        var _document1 = new U1_DocumentDetail[ds_udoc1_insert.Tables[0].Rows.Count];

        foreach (DataRow dtrow in ds_udoc1_insert.Tables[0].Rows)
        {

            _document1[i] = new U1_DocumentDetail();

            _document1[i].orgidx = int.Parse(dtrow["OrgIDX"].ToString());
            _document1[i].rdeptidx = int.Parse(dtrow["RdeptIDX"].ToString());
            _document1[i].rsecidx = int.Parse(dtrow["SecIDX"].ToString());
            _document1[i].rposidx = int.Parse(dtrow["PosIDX"].ToString());
            _document1[i].m0_hridx = int.Parse(dtrow["m0_hridx"].ToString());
            _document1[i].m0_thidx = int.Parse(dtrow["m0_thidx"].ToString());
            _document1[i].eduidx = int.Parse(dtrow["eduidx"].ToString());
            _document1[i].qty = int.Parse(dtrow["qty"].ToString());
            _document1[i].experience = dtrow["experience"].ToString();
            _document1[i].language = dtrow["language"].ToString();
            _document1[i].other = dtrow["other"].ToString();
            _document1[i].date_receive = dtrow["datestart"].ToString();

            int a = 0;
            var ds_udoc2_insert = (DataSet)ViewState["vsManPowerList_sex"];
            var _document2 = new U2_DocumentDetail[ds_udoc2_insert.Tables[0].Rows.Count];


            foreach (DataRow dtrow_sex in ds_udoc2_insert.Tables[0].Rows)
            {
                _document2[a] = new U2_DocumentDetail();

                _document2[a].sexidx = int.Parse(dtrow_sex["sexidx"].ToString());
                _document2[a].sub_qty = int.Parse(dtrow_sex["sub_qty"].ToString());
                _document2[a].natidx = dtrow_sex["natidx"].ToString();
                _document2[a].CheckRow_sex = _funcTool.convertToInt(dtrow_sex["CheckRow_sex"].ToString());

                a++;

                _dtman.Boxu2_DocumentDetail = _document2;
            }

            i++;

            _dtman.Boxu1_DocumentDetail = _document1;

        }



        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlInsert_ManPower, _dtman);

        ViewState["ReturnCode_DocCode"] = _dtman.ReturnMsg;


        string getPathfile = ConfigurationManager.AppSettings["path_file_hr_manpower"];
        string fileName_upload = ViewState["ReturnCode_DocCode"].ToString();
        string filePath_upload = Server.MapPath(getPathfile + fileName_upload);
        int x = 0;
        if (UploadFileOrg.HasFile)
        {
            if (!Directory.Exists(filePath_upload))
            {
                Directory.CreateDirectory(filePath_upload);
            }
            string extension = Path.GetExtension(UploadFileOrg.FileName);

            UploadFileOrg.SaveAs(Server.MapPath(getPathfile + ViewState["ReturnCode_DocCode"].ToString()) + "\\" + "OrgChart_" + fileName_upload + extension);

            foreach (HttpPostedFile uploadedFile in UploadFileJD.PostedFiles)
            {
                if (uploadedFile.FileName != "" && uploadedFile.FileName != String.Empty)
                {
                    uploadedFile.SaveAs(Server.MapPath(getPathfile + ViewState["ReturnCode_DocCode"].ToString()) + "\\" + "JD_" + fileName_upload + x + extension);
                }
                x++;
            }
        }



    }

    protected void Approve_Manpower()
    {
        var hfm0_typeidx = (HiddenField)fvdetailmanpower.FindControl("hfm0_typeidx");
        var hfm0type_listidx = (HiddenField)fvdetailmanpower.FindControl("hfm0type_listidx");
        var txtorgidx = (TextBox)Fvdetailusercreate.FindControl("txtorgidx");
        var txtsecidx = (TextBox)Fvdetailusercreate.FindControl("txtsecidx");

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail datainsert = new U0_DocumentDetail();

        datainsert.unidx = int.Parse(ViewState["m0_node"].ToString());
        datainsert.acidx = int.Parse(ViewState["m0_actor"].ToString());
        datainsert.staidx = int.Parse(ddl_approve.SelectedValue);
        datainsert.comment = txtremark_approve.Text;
        datainsert.doc_decision = int.Parse(ddl_approve.SelectedValue);
        datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        datainsert.condition = 1;
        datainsert.u0_docidx = int.Parse(ViewState["u0_docidx"].ToString());
        datainsert.m0_typeidx = int.Parse(hfm0_typeidx.Value);
        datainsert.m0type_listidx = int.Parse(hfm0type_listidx.Value);
        datainsert.orgidx = int.Parse(txtorgidx.Text);
        datainsert.rsecidx = int.Parse(txtsecidx.Text);
        datainsert.JobGradeIDX = int.Parse(ViewState["JobGradeIDX"].ToString());
        _dtman.Boxu0_DocumentDetail[0] = datainsert;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlApprove_ManPower, _dtman);


    }

    protected void Delete_Manpower(int u1idx)
    {
        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail datainsert = new U0_DocumentDetail();

        datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        datainsert.condition = 2;
        datainsert.u1_docidx = u1idx;


        _dtman.Boxu0_DocumentDetail[0] = datainsert;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlApprove_ManPower, _dtman);


    }

    protected void Insert_Manpower_Cancel()
    {

        var ddldoccode_cancel = (DropDownList)fvwheredoccode.FindControl("ddldoccode_cancel");
        var hfm0type_listidx = (HiddenField)fvinsert_cancel.FindControl("hfm0type_listidx");
        var hfLocidx = (HiddenField)fvinsert_cancel.FindControl("hfLocidx");
        var hfemp_typeidx = (HiddenField)fvinsert_cancel.FindControl("hfemp_typeidx");
        var txtdate_limit = (Label)fvinsert_cancel.FindControl("txtdate_limit");
        var txcancel = (TextBox)panel_cancel.FindControl("txcancel");


        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail datainsert = new U0_DocumentDetail();

        datainsert.unidx = 8;
        datainsert.acidx = 1;
        datainsert.staidx = 1;
        datainsert.doc_decision = 1;
        datainsert.orgidx = int.Parse(ViewState["Org_idx"].ToString());
        datainsert.rdeptidx = int.Parse(ViewState["rdept_idx"].ToString());
        datainsert.rsecidx = int.Parse(ViewState["Sec_idx"].ToString());
        datainsert.rposidx = int.Parse(ViewState["Pos_idx"].ToString());
        datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        datainsert.condition = 1;
        datainsert.date_limit = int.Parse(txtdate_limit.Text);
        datainsert.m0type_listidx = int.Parse(hfm0type_listidx.Value);
        datainsert.m0_typeidx = 2;
        datainsert.doc_refidx = int.Parse(ddldoccode_cancel.SelectedValue);
        datainsert.reason_comment = txcancel.Text;
        datainsert.LocIDX = int.Parse(hfLocidx.Value);
        datainsert.EmpTypeIDX = int.Parse(hfemp_typeidx.Value);

        _dtman.Boxu0_DocumentDetail[0] = datainsert;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlInsert_ManPower, _dtman);

    }

    protected void Approve_Cancel()
    {
        var hfm0type_listidx = (HiddenField)fvdetail_cancel.FindControl("hfm0type_listidx");
        var hfm0_typeidx = (HiddenField)fvdetail_cancel.FindControl("hfm0_typeidx");
        var ddlaproove_cancel = (DropDownList)fvdetail_cancel.FindControl("ddlaproove_cancel");
        var txtremark_approve_cancel = (TextBox)fvdetail_cancel.FindControl("txtremark_approve_cancel");
        var txtorgidx = (TextBox)Fvdetailusercreate.FindControl("txtorgidx");
        var txtsecidx = (TextBox)Fvdetailusercreate.FindControl("txtsecidx");

        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
        U0_DocumentDetail datainsert = new U0_DocumentDetail();

        datainsert.unidx = int.Parse(ViewState["m0_node"].ToString());
        datainsert.acidx = int.Parse(ViewState["m0_actor"].ToString());
        datainsert.staidx = int.Parse(ddlaproove_cancel.SelectedValue);
        datainsert.comment = txtremark_approve_cancel.Text;
        datainsert.doc_decision = int.Parse(ddlaproove_cancel.SelectedValue);
        datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        datainsert.condition = 3;
        datainsert.doc_code = ViewState["doc_code"].ToString();
        datainsert.m0_typeidx = int.Parse(hfm0_typeidx.Value);
        datainsert.m0type_listidx = int.Parse(hfm0type_listidx.Value);
        datainsert.orgidx = int.Parse(txtorgidx.Text);
        datainsert.rsecidx = int.Parse(txtsecidx.Text);

        _dtman.Boxu0_DocumentDetail[0] = datainsert;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

        _dtman = callServicePostManPower(_urlApprove_ManPower, _dtman);

    }

    #endregion

    #region Directories_File URL


    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            GridView gvFile = (GridView)fvdetailmanpower.FindControl("gvFile");
            if (dt1.Rows.Count > 0)
            {

                ds1.Tables.Add(dt1);
                // gvFileLo1.Visible = true;
                gvFile.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                gvFile.DataBind();
                ds1.Dispose();
            }
            else
            {

                gvFile.DataSource = null;
                gvFile.DataBind();

            }
        }
        catch
        {
            checkfile = "11";
        }
    }

    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    protected data_hr_manpower callServicePostManPower(string _cmdUrl, data_hr_manpower _dtman)
    {
        _localJson = _funcTool.convertObjectToJson(_dtman);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtman = (data_hr_manpower)_funcTool.convertJsonToObject(typeof(data_hr_manpower), _localJson);


        return _dtman;
    }
    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        ddlName.Items.Clear();
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setRdoData(RadioButtonList rdoName, Object obj, string _data_text, string _data_value)
    {
        rdoName.Items.Clear();
        rdoName.DataSource = obj;
        rdoName.DataTextField = _data_text;
        rdoName.DataValueField = _data_value;
        rdoName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        chkName.Items.Clear();
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setRepeatData(Repeater rpName, Object obj)
    {
        rpName.DataSource = obj;
        rpName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected void SetDefaultApprove(DropDownList ddlName, int unidx)
    {
        HiddenField hfm0type_listidx = (HiddenField)fvdetailmanpower.FindControl("hfm0type_listidx");

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();
        ddlName.Items.Add(new ListItem("เลือกสถานะอนุมัติ", "0"));

        if (unidx == 9)
        {
            ddlName.Items.Add(new ListItem("ดำเนินการ", "1"));
        }
        else if (unidx == 5 || unidx == 6 || unidx == 10)
        {
            ddlName.Items.Add(new ListItem("อนุมัติ", "2"));
            ddlName.Items.Add(new ListItem("ไม่อนุมัติ", "3"));
        }
        else if (unidx == 11)
        {
            ddlName.Items.Add(new ListItem("ยกเลิก", "8"));
        }
        else if (unidx == 4 && hfm0type_listidx.Value == "2")
        {
            ddlName.Items.Add(new ListItem("อนุมัติ", "2"));
            ddlName.Items.Add(new ListItem("ไม่อนุมัติ", "3"));
        }
        else
        {
            ddlName.Items.Add(new ListItem("อนุมัติ", "2"));
            ddlName.Items.Add(new ListItem("ไม่อนุมัติ", "3"));
            ddlName.Items.Add(new ListItem("ส่งกลับแก้ไขรายการ", "4"));
        }
    }

    protected void SetDefaultApprove_Cancel(DropDownList ddlName, int unidx)
    {
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();
        ddlName.Items.Add(new ListItem("เลือกสถานะอนุมัติ", "0"));

        if (unidx == 5 || unidx == 6 | unidx == 10)
        {
            ddlName.Items.Add(new ListItem("รับทราบ", "7"));
        }
        else
        {
            ddlName.Items.Add(new ListItem("อนุมัติ", "5"));
            ddlName.Items.Add(new ListItem("ไม่อนุมัติ", "6"));
        }

    }

    protected void GenerateddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    #endregion

    #region FormView

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewInsert.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }
                break;

            case "FvDetailUser_Cancel":
                FormView FvDetailUser_Cancel = (FormView)ViewCancel.FindControl("FvDetailUser_Cancel");

                if (FvDetailUser_Cancel.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser_Cancel.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser_Cancel.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser_Cancel.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser_Cancel.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser_Cancel.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser_Cancel.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser_Cancel.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser_Cancel.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }
                break;
            case "fvinsert":
                FormView fvinsert = (FormView)ViewInsert.FindControl("fvinsert");
                DropDownList ddlorg = (DropDownList)fvinsert.FindControl("ddlorg");
                DropDownList ddlrdept = (DropDownList)fvinsert.FindControl("ddlrdept");
                DropDownList ddlrsec = (DropDownList)fvinsert.FindControl("ddlrsec");
                DropDownList ddlrpos = (DropDownList)fvinsert.FindControl("ddlrpos");
                RadioButtonList rdotypehire = (RadioButtonList)fvinsert.FindControl("rdotypehire");
                RadioButtonList rdohire = (RadioButtonList)fvinsert.FindControl("rdohire");
                DropDownList ddleducation = (DropDownList)fvinsert.FindControl("ddleducation");
                DropDownList ddlemptype = (DropDownList)fvinsert.FindControl("ddlemptype");
                DropDownList ddllocate = (DropDownList)fvinsert.FindControl("ddllocate");
                GridView Gvsex = (GridView)fvinsert.FindControl("Gvsex");
                Control div_tpeman = (Control)fvinsert.FindControl("div_tpeman");


                if (fvinsert.CurrentMode == FormViewMode.Insert)
                {
                    getOrganizationList(ddlorg);
                    ddlorg.SelectedValue = ViewState["Org_idx"].ToString();

                    getDepartmentList(ddlrdept, int.Parse(ViewState["Org_idx"].ToString()));
                    ddlrdept.SelectedValue = ViewState["rdept_idx"].ToString();

                    getSectionList(ddlrsec, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()));
                    ddlrsec.SelectedValue = ViewState["Sec_idx"].ToString();

                    select_pos(ddlrpos, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()));
                    ddlrpos.SelectedValue = ViewState["Pos_idx"].ToString();

                    select_typehire(rdotypehire);
                    select_hire(rdohire);
                    select_sex(Gvsex);
                    select_emptype(ddlemptype);
                    select_education(ddleducation);
                    select_location(ddllocate, int.Parse(ViewState["Org_idx"].ToString()));

                    if (ViewState["hr_admin"].ToString() == "0" && ViewState["EmpType"].ToString() == "2")
                    {
                        ddlorg.Enabled = true;
                        ddlrdept.Enabled = true;
                        ddlrsec.Enabled = true;
                        div_tpeman.Visible = true;
                    }
                    else
                    {
                        ddlorg.Enabled = false;
                        ddlrdept.Enabled = false;
                        ddlrsec.Enabled = false;
                        div_tpeman.Visible = false;
                    }
                }
                break;

            case "fvwheredoccode":
                FormView fvwheredoccode = (FormView)ViewCancel.FindControl("fvwheredoccode");

                if (fvwheredoccode.CurrentMode == FormViewMode.Insert)
                {
                    var ddldoccode_cancel = (DropDownList)fvwheredoccode.FindControl("ddldoccode_cancel");


                    select_doccode_cancel(ddldoccode_cancel, int.Parse(ViewState["Sec_idx"].ToString()));
                }

                break;

            case "fvinsert_cancel":
                FormView fvinsert_cancel = (FormView)ViewCancel.FindControl("fvinsert_cancel");

                if (fvinsert_cancel.CurrentMode == FormViewMode.Insert)
                {
                    var ddldoccode_cancel = (DropDownList)fvinsert_cancel.FindControl("ddldoccode_cancel");


                    select_doccode_cancel(ddldoccode_cancel, int.Parse(ViewState["Sec_idx"].ToString()));
                }

                break;
            case "Fvdetailusercreate":
                FormView Fvdetailusercreate = (FormView)ViewDetail.FindControl("Fvdetailusercreate");

                if (Fvdetailusercreate.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)Fvdetailusercreate.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)Fvdetailusercreate.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)Fvdetailusercreate.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)Fvdetailusercreate.FindControl("txtsec"));
                    var txtsecidx = ((TextBox)Fvdetailusercreate.FindControl("txtsecidx"));
                    var txtpos = ((TextBox)Fvdetailusercreate.FindControl("txtpos"));
                    var txtemail = ((TextBox)Fvdetailusercreate.FindControl("txtemail"));
                    var txttel = ((TextBox)Fvdetailusercreate.FindControl("txttel"));
                    var txtorg = ((TextBox)Fvdetailusercreate.FindControl("txtorg"));
                    var txtorgidx = ((TextBox)Fvdetailusercreate.FindControl("txtorgidx"));

                    txtempcode.Text = ViewState["EmpCode_create"].ToString();
                    txtrequesname.Text = ViewState["FullName_create"].ToString();
                    txtorg.Text = ViewState["Org_name_create"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name_create"].ToString();
                    txtsec.Text = ViewState["Secname_create"].ToString();
                    txtpos.Text = ViewState["Positname_create"].ToString();
                    txttel.Text = ViewState["Tel_create"].ToString();
                    txtemail.Text = ViewState["Email_create"].ToString();
                    txtorgidx.Text = ViewState["Org_idx_create"].ToString();
                    txtsecidx.Text = ViewState["Sec_idx_create"].ToString();
                }
                break;
            case "FvTemplate_print":

                if (FvTemplate_print.CurrentMode == FormViewMode.ReadOnly)
                {
                    //var GvDetail_Print_BuyNormal = ((GridView)FvTemplate_print.FindControl("GvDetail_Print_BuyNormal"));
                    //var GvDetail_Print_BuySpecial = ((GridView)FvTemplate_print.FindControl("GvDetail_Print_BuySpecial"));

                    //if (ViewState["m0_tmidx"].ToString() == "1")
                    //{
                    //    SelectHistory_PrintMemoDetail_BuyNormal(GvDetail_Print_BuyNormal, int.Parse(ViewState["u0_meidx"].ToString()));
                    //}
                    //else if (ViewState["m0_tmidx"].ToString() == "2")
                    //{
                    //    SelectHistory_PrintMemo_BuySpecial(GvDetail_Print_BuySpecial, int.Parse(ViewState["u0_meidx"].ToString()), 2);
                    //}
                }
                break;

            case "fvreport":

                if (fvreport.CurrentMode == FormViewMode.Insert)
                {
                    var ddlorgidx_search = (DropDownList)fvreport.FindControl("ddlorgidx_search");
                    var ddlemptype_search = (DropDownList)fvreport.FindControl("ddlemptype_search");
                    //var ddlstatus_search = (DropDownList)fvreport.FindControl("ddlstatus_search");
                    var ddltypereport = (DropDownList)fvreport.FindControl("ddltypereport");
                    var ddlLocate_search = (DropDownList)fvreport.FindControl("ddlLocate_search");
                    var panel_table = (Panel)fvreport.FindControl("panel_table");
                    var panel_chart = (Panel)fvreport.FindControl("panel_chart");
                    var ddlyear = (DropDownList)fvreport.FindControl("ddlyear");
                    var ddlorg_chart = (DropDownList)fvreport.FindControl("ddlorg_chart");

                    getOrganizationList(ddlorgidx_search);
                    select_emptype(ddlemptype_search);
                    select_location(ddlLocate_search, int.Parse(ViewState["Org_idx"].ToString()));
                    GenerateddlYear(ddlyear);
                    getOrganizationList(ddlorg_chart);

                    ddltypereport.SelectedValue = "0";
                    panel_table.Visible = false;
                    panel_chart.Visible = false;

                }

                break;



        }
    }
    #endregion

    #region GridView

    #region onRowDataBound

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {

            case "Gvsex":
                GridView Gvsex = (GridView)fvinsert.FindControl("Gvsex");

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (Gvsex.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var chknation = (CheckBoxList)e.Row.FindControl("chknation");
                        _dtman = new data_hr_manpower();

                        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
                        U0_DocumentDetail _select = new U0_DocumentDetail();

                        _select.condition = 5;
                        _dtman.Boxu0_DocumentDetail[0] = _select;
                        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

                        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
                        setChkData(chknation, _dtman.Boxu0_DocumentDetail, "nationality_name", "natidx");
                    }
                }
                break;

            case "GvManPowerAdd":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbCheckRow = (Label)e.Row.FindControl("lbCheckRow");
                    GridView GvSexAdd = (GridView)e.Row.Cells[3].FindControl("GvSexAdd");

                    ViewState["CheckRow_dtset"] = lbCheckRow.Text;
                    int count_row = int.Parse(lbCheckRow.Text);

                    var dsContacts_sex = (DataSet)ViewState["vsManPowerList_sex"];
                    var _document2 = new U2_DocumentDetail[dsContacts_sex.Tables[0].Rows.Count];
                    int a = 0;


                    foreach (DataRow dtrow_sex in dsContacts_sex.Tables[0].Rows)
                    {

                        _document2[a] = new U2_DocumentDetail();

                        _document2[a].sexidx = int.Parse(dtrow_sex["sexidx"].ToString());
                        _document2[a].sub_qty = int.Parse(dtrow_sex["sub_qty"].ToString());
                        _document2[a].natidx = dtrow_sex["natidx"].ToString();
                        _document2[a].sex_name = dtrow_sex["sex_name"].ToString();
                        _document2[a].nationality_name = dtrow_sex["nationality_name"].ToString();
                        _document2[a].CheckRow_sex = int.Parse(dtrow_sex["CheckRow_sex"].ToString());

                        a++;

                        _dtman.Boxu2_DocumentDetail = _document2;

                    }

                    var _linqTotalsex = (from data in _dtman.Boxu2_DocumentDetail
                                         where count_row == data.CheckRow_sex
                                         select new
                                         {
                                             data.sex_name,
                                             data.sub_qty,
                                             data.nationality_name,
                                             data.CheckRow_sex,
                                             data.sexidx,
                                             data.natidx

                                         }).ToList();

                    foreach (var item in _linqTotalsex)
                    {
                        if (count_row == item.CheckRow_sex)
                        {
                            setGridData(GvSexAdd, _linqTotalsex);
                        }
                        else
                        {

                        }
                    }

                    //setGridData(GvSexAdd, dsContacts_sex.Tables["dsAddListTable_sex"]);


                }
                break;


            case "GvDetailManPower":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvDetailManPower.EditIndex != e.Row.RowIndex)
                    {
                        Literal litunidx = (Literal)e.Row.FindControl("litunidx");
                        Literal litstaidx = (Literal)e.Row.FindControl("litstaidx");
                        Literal litu1_docidx = (Literal)e.Row.FindControl("litu1_docidx");
                        GridView GvDetailSex = (GridView)e.Row.Cells[3].FindControl("GvDetailSex");
                        LinkButton btnDelete = (LinkButton)e.Row.Cells[4].FindControl("btnDelete");
                        LinkButton btndetailprint = (LinkButton)e.Row.Cells[5].FindControl("btndetailprint");
                        HiddenField hfm0_typeidx = (HiddenField)fvdetailmanpower.FindControl("hfm0_typeidx");
                        HiddenField hfm0type_listidx = (HiddenField)fvdetailmanpower.FindControl("hfm0type_listidx");
                        HiddenField hfM0NodeIDX = (HiddenField)fvdetailmanpower.FindControl("hfM0NodeIDX");
                        HiddenField hfM0StatusIDX = (HiddenField)fvdetailmanpower.FindControl("hfM0StatusIDX");

                        int countrow = GvDetailManPower.Rows.Count;


                        _dtman = new data_hr_manpower();

                        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
                        U0_DocumentDetail _select = new U0_DocumentDetail();

                        _select.condition = 9;
                        _select.u1_docidx = int.Parse(litu1_docidx.Text);
                        _dtman.Boxu0_DocumentDetail[0] = _select;
                        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

                        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
                        setGridData(GvDetailSex, _dtman.Boxu2_DocumentDetail);

                        if (ViewState["History_Detail"].ToString() == "2" && litunidx.Text == "1" && litstaidx.Text == "1") // && approve_check == 1
                        {
                            if (ViewState["CEmpIDX_u0"].ToString() == ViewState["EmpIDX"].ToString())
                            {
                                GvDetailManPower.Columns[4].Visible = true;

                                if (countrow > 1)
                                {
                                    btnDelete.Visible = true;
                                }
                                else
                                {
                                    btnDelete.Visible = false;
                                }

                            }
                            else
                            {
                                GvDetailManPower.Columns[4].Visible = false;
                            }
                        }
                        else
                        {
                            GvDetailManPower.Columns[4].Visible = false;
                        }


                        if (hfm0type_listidx.Value == "1" && hfm0_typeidx.Value == "1" && hfM0NodeIDX.Value == "9" && hfM0StatusIDX.Value == "1" ||
                            hfm0type_listidx.Value == "2" && hfm0_typeidx.Value == "1" && hfM0NodeIDX.Value == "11" && hfM0StatusIDX.Value == "1")
                        {
                            GvDetailManPower.Columns[5].Visible = true;
                        }
                        else
                        {
                            GvDetailManPower.Columns[5].Visible = false;
                        }


                    }
                    else if (GvDetailManPower.EditIndex == e.Row.RowIndex)
                    {
                        RadioButtonList rdotypehire_edit = (RadioButtonList)e.Row.FindControl("rdotypehire_edit");
                        RadioButtonList rdohire_edit = (RadioButtonList)e.Row.FindControl("rdohire_edit");
                        TextBox txtm0_thidx = (TextBox)e.Row.FindControl("txtm0_thidx");
                        TextBox txtm0_hridx = (TextBox)e.Row.FindControl("txtm0_hridx");
                        TextBox txteduidx = (TextBox)e.Row.FindControl("txteduidx");
                        DropDownList ddleducation_edit = (DropDownList)e.Row.FindControl("ddleducation_edit");
                        GridView Gvsex_edit = (GridView)e.Row.Cells[0].FindControl("Gvsex_edit");
                        TextBox txtu1_docidx = (TextBox)e.Row.FindControl("txtu1_docidx");

                        ViewState["txtu1_docidx"] = txtu1_docidx.Text;


                        select_typehire(rdotypehire_edit);
                        rdotypehire_edit.SelectedValue = txtm0_thidx.Text;
                        select_hire(rdohire_edit);
                        rdohire_edit.SelectedValue = txtm0_hridx.Text;
                        select_education(ddleducation_edit);
                        ddleducation_edit.SelectedValue = txteduidx.Text;
                        // select_sex(Gvsex_edit);



                        _dtman = new data_hr_manpower();

                        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
                        U0_DocumentDetail _select = new U0_DocumentDetail();

                        _select.condition = 9;
                        _select.u1_docidx = int.Parse(txtu1_docidx.Text);
                        _dtman.Boxu0_DocumentDetail[0] = _select;
                        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

                        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
                        setGridData(Gvsex_edit, _dtman.Boxu2_DocumentDetail);
                    }


                }
                break;

            case "Gvsex_edit":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Literal litnatidx = (Literal)e.Row.FindControl("litnatidx");
                    CheckBoxList chknation_edit = (CheckBoxList)e.Row.FindControl("chknation_edit");
                    TextBox txtu1_docidx = (TextBox)e.Row.FindControl("txtu1_docidx");
                    TextBox txtqty_sex_edit = (TextBox)e.Row.FindControl("txtqty_sex_edit");


                    //_dtman = new data_hr_manpower();

                    //_dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
                    //U0_DocumentDetail _select1 = new U0_DocumentDetail();

                    //_select1.condition = 9;
                    //_select1.u1_docidx = int.Parse(ViewState["txtu1_docidx"] .ToString());
                    //_dtman.Boxu0_DocumentDetail[0] = _select1;
                    ////txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

                    //_dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);


                    //txtqty_sex_edit.Text = _dtman.Boxu2_DocumentDetail[0].sub_qty.ToString();


                    _dtman = new data_hr_manpower();

                    _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
                    U0_DocumentDetail _select = new U0_DocumentDetail();

                    _select.condition = 5;
                    _dtman.Boxu0_DocumentDetail[0] = _select;
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

                    _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
                    setChkData(chknation_edit, _dtman.Boxu0_DocumentDetail, "nationality_name", "natidx");

                    string[] ToId = litnatidx.Text.Split(',');

                    foreach (ListItem li in chknation_edit.Items)
                    {
                        foreach (string To_check in ToId)
                        {

                            if (li.Value == To_check)
                            {
                                li.Selected = true;

                                break;
                            }
                            else
                            {
                                li.Selected = false;
                            }
                        }
                    }
                }
                break;

            case "GvDetailSex":
            case "GvDetailSex_Print":
            case "GvDetailSex_Cancel":
            case "GvDetailSex_Report":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Literal litnatidx = (Literal)e.Row.FindControl("litnatidx");
                    CheckBoxList chknation = (CheckBoxList)e.Row.FindControl("chknation");

                    _dtman = new data_hr_manpower();

                    _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
                    U0_DocumentDetail _select = new U0_DocumentDetail();

                    _select.condition = 5;
                    _dtman.Boxu0_DocumentDetail[0] = _select;
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

                    _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
                    setChkData(chknation, _dtman.Boxu0_DocumentDetail, "nationality_name", "natidx");

                    string[] ToId = litnatidx.Text.Split(',');

                    foreach (ListItem li in chknation.Items)
                    {
                        foreach (string To_check in ToId)
                        {

                            if (li.Value == To_check)
                            {
                                li.Selected = true;

                                break;
                            }
                            else
                            {
                                li.Selected = false;
                            }
                        }
                    }
                }
                break;

            case "GvListIndex":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Literal ltunidx = (Literal)e.Row.Cells[4].FindControl("ltunidx");
                    Literal ltstaidx = (Literal)e.Row.Cells[4].FindControl("ltstaidx");
                    Label lblstasus = (Label)e.Row.Cells[4].FindControl("lblstasus");
                    Literal litm0_typeidx = (Literal)e.Row.FindControl("litm0_typeidx");
                    LinkButton btnviewdetail = (LinkButton)e.Row.FindControl("btnviewdetail");
                    LinkButton btnviewdetail_cancel = (LinkButton)e.Row.FindControl("btnviewdetail_cancel");
                    Literal litdoc_code_ref = (Literal)e.Row.FindControl("litdoc_code_ref");
                    Control div_replacecode = (Control)e.Row.FindControl("div_replacecode");

                    if (litm0_typeidx.Text == "1")
                    {
                        btnviewdetail.Visible = true;
                        btnviewdetail_cancel.Visible = false;
                    }
                    else
                    {
                        btnviewdetail.Visible = false;
                        btnviewdetail_cancel.Visible = true;
                    }


                    if (litdoc_code_ref.Text != "0" && litdoc_code_ref.Text != null && litdoc_code_ref.Text != String.Empty)
                    {
                        div_replacecode.Visible = true;
                    }
                    else
                    {
                        div_replacecode.Visible = false;
                    }

                    switch (int.Parse(ltunidx.Text))
                    {
                        case 1:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8c1a");
                            break;
                        case 2:

                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                            break;
                        case 3:

                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                            break;
                        case 4:

                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0040ff");
                            break;
                        case 5:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff6666");
                            break;
                        case 6:

                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#33adff");
                            break;
                        case 7:
                            if (ltstaidx.Text == "3" || ltstaidx.Text == "6")
                            {
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0000");
                                //btncancel.Visible = false;

                            }
                            else
                            {
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                                ////btncancel.Visible = true;

                            }

                            break;
                        case 8:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff3300");
                            //btncancel.Visible = false;
                            break;

                        case 10:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc6699");
                            break;
                        case 11:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff6600");
                            break;
                    }
                }

                break;

            case "GvApproveList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Literal ltunidx = (Literal)e.Row.Cells[4].FindControl("ltunidx");
                    Literal ltstaidx = (Literal)e.Row.Cells[4].FindControl("ltstaidx");
                    Label lblstasus = (Label)e.Row.Cells[4].FindControl("lblstasus");
                    Literal litm0_typeidx = (Literal)e.Row.FindControl("litm0_typeidx");
                    LinkButton btnviewdetail = (LinkButton)e.Row.FindControl("btnviewdetail");
                    LinkButton btnviewdetail_cancel = (LinkButton)e.Row.FindControl("btnviewdetail_cancel");
                    Literal litdoc_code_ref = (Literal)e.Row.FindControl("litdoc_code_ref");
                    Control div_replacecode = (Control)e.Row.FindControl("div_replacecode");

                    if (litm0_typeidx.Text == "1")
                    {
                        btnviewdetail.Visible = true;
                        btnviewdetail_cancel.Visible = false;
                    }
                    else
                    {
                        btnviewdetail.Visible = false;
                        btnviewdetail_cancel.Visible = true;
                    }


                    if (litdoc_code_ref.Text != "0" && litdoc_code_ref.Text != null && litdoc_code_ref.Text != String.Empty)
                    {
                        div_replacecode.Visible = true;
                    }
                    else
                    {
                        div_replacecode.Visible = false;
                    }
                    switch (int.Parse(ltunidx.Text))
                    {
                        case 1:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8c1a");

                            break;
                        case 2:

                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                            break;
                        case 3:

                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                            break;
                        case 4:

                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0040ff");
                            break;
                        case 5:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff6666");
                            break;
                        case 6:

                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#33adff");
                            break;
                        case 7:
                            if (ltstaidx.Text == "3")
                            {
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0000");
                            }
                            else
                            {
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                            }


                            break;
                        case 8:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff3300");
                            break;
                        case 10:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc6699");
                            break;
                        case 11:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff6600");
                            break;
                    }
                }

                break;

            case "gvFile":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;

            case "GvManPowerDetail_Cancel":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvDetailManPower.EditIndex != e.Row.RowIndex)
                    {
                        Literal litu1_docidx = (Literal)e.Row.FindControl("litu1_docidx");
                        GridView GvDetailSex_Cancel = (GridView)e.Row.Cells[3].FindControl("GvDetailSex_Cancel");

                        _dtman = new data_hr_manpower();

                        _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
                        U0_DocumentDetail _select = new U0_DocumentDetail();

                        _select.condition = 9;
                        _select.u1_docidx = int.Parse(litu1_docidx.Text);
                        _dtman.Boxu0_DocumentDetail[0] = _select;
                        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

                        _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
                        setGridData(GvDetailSex_Cancel, _dtman.Boxu2_DocumentDetail);


                    }
                }
                break;

            case "GvReport":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbu1_docidx = (Label)e.Row.Cells[1].FindControl("lbu1_docidx");
                    Literal ltunidx = (Literal)e.Row.Cells[6].FindControl("ltunidx");
                    Literal ltstaidx = (Literal)e.Row.Cells[6].FindControl("ltstaidx");
                    Label lblstasus = (Label)e.Row.Cells[6].FindControl("lblstasus");
                    GridView GvDetailSex_Report = (GridView)e.Row.Cells[6].FindControl("GvDetailSex_Report");

                    _dtman = new data_hr_manpower();

                    _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
                    U0_DocumentDetail _select = new U0_DocumentDetail();

                    _select.condition = 9;
                    _select.u1_docidx = int.Parse(lbu1_docidx.Text);
                    _dtman.Boxu0_DocumentDetail[0] = _select;
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

                    _dtman = callServicePostManPower(_urlSelectMaster_ManPower, _dtman);
                    setGridData(GvDetailSex_Report, _dtman.Boxu2_DocumentDetail);

                    switch (int.Parse(ltunidx.Text))
                    {
                        case 1:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8c1a");

                            break;
                        case 2:

                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                            break;
                        case 3:

                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                            break;
                        case 4:

                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0040ff");
                            break;
                        case 5:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff6666");
                            break;
                        case 6:

                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#33adff");
                            break;
                        case 7:
                            if (ltstaidx.Text == "3")
                            {
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0000");
                            }
                            else
                            {
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                            }
                            break;
                        case 8:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff3300");
                            break;
                        case 10:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc6699");
                            break;
                        case 11:
                            lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff6600");
                            break;

                    }
                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    int sum = 0;
                    int complete = 0;
                    int uncomplete = 0;
                    int waitpower = 0;

                    U0_DocumentDetail[] _item_TotalQTY = (U0_DocumentDetail[])ViewState["Data_ReportTable"];

                    var _linqTotalQty = (from data in _item_TotalQTY
                                         select new
                                         {
                                             data.qty,
                                             data.unidx,
                                             data.staidx

                                         }).ToList();

                    foreach (var item in _linqTotalQty)
                    {

                        sum += item.qty;

                        if (item.unidx == 7 && item.staidx == 8)
                        {
                            complete += 1;
                        }
                        else if (item.unidx == 11)
                        {
                            waitpower += 1;
                        }
                        else if (item.unidx != 7 && item.unidx != 11)
                        {
                            uncomplete += 1;
                        }
                    }


                    Label lit_total_qty = (Label)e.Row.FindControl("lit_total_qty");
                    Label lit_total_waitpower = (Label)e.Row.FindControl("lit_total_waitpower");
                    Label lit_total_uncomplete = (Label)e.Row.FindControl("lit_total_uncomplete");
                    Label lit_total_complete = (Label)e.Row.FindControl("lit_total_complete");

                    lit_total_qty.Text = sum.ToString();
                    lit_total_waitpower.Text = waitpower.ToString();
                    lit_total_uncomplete.Text = uncomplete.ToString();
                    lit_total_complete.Text = complete.ToString();
                }

                break;


        }
    }

    #endregion

    #region onRowCommand

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "bnDeleteList":
                    GridView GvManPowerAdd = (GridView)fvinsert.FindControl("GvManPowerAdd");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsManPowerList"];
                    dsContacts.Tables["dsAddListTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();

                    DataSet dsContacts_sex = (DataSet)ViewState["vsManPowerList_sex"];
                    dsContacts_sex.Tables["dsAddListTable_sex"].Rows[rowIndex].Delete();
                    dsContacts_sex.AcceptChanges();


                    setGridData(GvManPowerAdd, dsContacts.Tables["dsAddListTable"]);

                    if (dsContacts.Tables["dsAddListTable"].Rows.Count < 1)
                    {
                        GvManPowerAdd.Visible = false;
                    }
                    break;
            }
        }
    }
    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetailManPower":

                GvDetailManPower.EditIndex = e.NewEditIndex;
                select_DetailManPower_Edit(GvDetailManPower, int.Parse(ViewState["u0_docidx"].ToString()));
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvDetailManPower":
                GvDetailManPower.EditIndex = -1;
                select_DetailManPower_Edit(GvDetailManPower, int.Parse(ViewState["u0_docidx"].ToString()));
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetailManPower":

                int u1_docidx = Convert.ToInt32(GvDetailManPower.DataKeys[e.RowIndex].Values[0].ToString());
                var datereceive_edit = (TextBox)GvDetailManPower.Rows[e.RowIndex].FindControl("datereceive_edit");
                var rdotypehire_edit = (RadioButtonList)GvDetailManPower.Rows[e.RowIndex].FindControl("rdotypehire_edit");
                var rdohire_edit = (RadioButtonList)GvDetailManPower.Rows[e.RowIndex].FindControl("rdohire_edit");
                var txtqty_edit = (TextBox)GvDetailManPower.Rows[e.RowIndex].FindControl("txtqty_edit");
                var ddleducation_edit = (DropDownList)GvDetailManPower.Rows[e.RowIndex].FindControl("ddleducation_edit");
                var txtexp_edit = (TextBox)GvDetailManPower.Rows[e.RowIndex].FindControl("txtexp_edit");
                var txtlanguage_edit = (TextBox)GvDetailManPower.Rows[e.RowIndex].FindControl("txtlanguage_edit");
                var txtother_edit = (TextBox)GvDetailManPower.Rows[e.RowIndex].FindControl("txtother_edit");
                var Gvsex_edit = (GridView)GvDetailManPower.Rows[e.RowIndex].FindControl("Gvsex_edit");
                var hfm0_typeidx = (HiddenField)fvdetailmanpower.FindControl("hfm0_typeidx");
                var hfm0type_listidx = (HiddenField)fvdetailmanpower.FindControl("hfm0type_listidx");
                var txtorgidx = (TextBox)Fvdetailusercreate.FindControl("txtorgidx");
                var txtsecidx = (TextBox)Fvdetailusercreate.FindControl("txtsecidx");

                GvDetailManPower.EditIndex = -1;

                _dtman = new data_hr_manpower();

                _dtman.Boxu0_DocumentDetail = new U0_DocumentDetail[1];
                U0_DocumentDetail datainsert = new U0_DocumentDetail();

                datainsert.unidx = 1;
                datainsert.acidx = 1;
                datainsert.staidx = 1;
                datainsert.doc_decision = 1;
                datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                datainsert.u0_docidx = int.Parse(ViewState["u0_docidx"].ToString());
                datainsert.condition = 1;
                datainsert.m0_typeidx = int.Parse(hfm0_typeidx.Value);
                datainsert.m0type_listidx = int.Parse(hfm0type_listidx.Value);
                datainsert.orgidx = int.Parse(txtorgidx.Text);
                datainsert.rsecidx = int.Parse(txtsecidx.Text);

                _dtman.Boxu0_DocumentDetail[0] = datainsert;


                _dtman.Boxu1_DocumentDetail = new U1_DocumentDetail[1];
                U1_DocumentDetail u1_detail = new U1_DocumentDetail();

                u1_detail.date_receive = datereceive_edit.Text;
                u1_detail.m0_hridx = int.Parse(rdohire_edit.SelectedValue);
                u1_detail.m0_thidx = int.Parse(rdotypehire_edit.SelectedValue);
                u1_detail.eduidx = int.Parse(ddleducation_edit.SelectedValue);
                u1_detail.qty = int.Parse(txtqty_edit.Text);
                u1_detail.experience = txtexp_edit.Text;
                u1_detail.language = txtlanguage_edit.Text;
                u1_detail.other = txtother_edit.Text;
                u1_detail.u1_docidx = u1_docidx;
                _dtman.Boxu1_DocumentDetail[0] = u1_detail;



                int i = 0;
                var u2_sex = new U2_DocumentDetail[Gvsex_edit.Rows.Count];

                foreach (GridViewRow row_sex in Gvsex_edit.Rows)
                {
                    Label lblsexidx = (Label)row_sex.Cells[0].FindControl("lblsexidx");
                    TextBox txtqty_sex_edit = (TextBox)row_sex.Cells[1].FindControl("txtqty_sex_edit");
                    CheckBoxList chknation_edit = (CheckBoxList)row_sex.Cells[2].FindControl("chknation_edit");

                    int a = 0;
                    _dtman.Boxu2_DocumentDetail = new U2_DocumentDetail[1];
                    u2_sex[i] = new U2_DocumentDetail();

                    u2_sex[i].sub_qty = int.Parse(txtqty_sex_edit.Text);
                    u2_sex[i].sexidx = int.Parse(lblsexidx.Text);

                    foreach (ListItem item in chknation_edit.Items)
                    {
                        if (item.Selected)
                        {
                            u2_sex[i].natidx += item.Value + ",";
                        }
                    }


                    _dtman.Boxu2_DocumentDetail = u2_sex;
                    i++;
                }

                // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtman));

                _dtman = callServicePostManPower(_urlApprove_ManPower, _dtman);

                select_DetailManPower_Edit(GvDetailManPower, int.Parse(ViewState["u0_docidx"].ToString()));
                selectsum_Approve(int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["Pos_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["JobGradeIDX"].ToString()));

                break;
        }
    }

    #endregion

    #region GvPaging
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvListIndex":
                GvListIndex.PageIndex = e.NewPageIndex;
                select_ListIndex(GvListIndex, int.Parse(ViewState["rdept_idx"].ToString()));
                break;

        }
    }
    #endregion


    #endregion

    #region SelectedIndexChanged
    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is DropDownList)
        {
            DropDownList ddlName = (DropDownList)sender;

            DropDownList ddlorg = (DropDownList)fvinsert.FindControl("ddlorg");
            DropDownList ddlrdept = (DropDownList)fvinsert.FindControl("ddlrdept");
            DropDownList ddlrsec = (DropDownList)fvinsert.FindControl("ddlrsec");
            DropDownList ddlrpos = (DropDownList)fvinsert.FindControl("ddlrpos");

            DropDownList ddltypereport = (DropDownList)fvreport.FindControl("ddltypereport");
            DropDownList ddlorgidx_search = (DropDownList)fvreport.FindControl("ddlorgidx_search");
            DropDownList ddlrdeptidx_search = (DropDownList)fvreport.FindControl("ddlrdeptidx_search");
            DropDownList ddlrsecidx_search = (DropDownList)fvreport.FindControl("ddlrsecidx_search");
            DropDownList ddlrposidx_search = (DropDownList)fvreport.FindControl("ddlrposidx_search");
            DropDownList ddlSearchDate = (DropDownList)fvreport.FindControl("ddlSearchDate");
            Panel panel_table = (Panel)fvreport.FindControl("panel_table");
            Panel panel_chart = (Panel)fvreport.FindControl("panel_chart");
            TextBox AddEndDate = (TextBox)fvreport.FindControl("AddEndDate");
            Control div_button = (Control)fvreport.FindControl("div_button");

            DropDownList ddlorg_chart = (DropDownList)fvreport.FindControl("ddlorg_chart");
            DropDownList ddlrdept_chart = (DropDownList)fvreport.FindControl("ddlrdept_chart");
            DropDownList ddlrsec_chart = (DropDownList)fvreport.FindControl("ddlrsec_chart");
            DropDownList ddlrpos_chart = (DropDownList)fvreport.FindControl("ddlrpos_chart");
            DropDownList ddltypechart = (DropDownList)fvreport.FindControl("ddltypechart");
            Control div_organization = (Control)fvreport.FindControl("div_organization");
            Control div_rsecchart = (Control)fvreport.FindControl("div_rsecchart");
            Control div_rposchart = (Control)fvreport.FindControl("div_rposchart");


            switch (ddlName.ID)
            {
                case "ddlorg":
                    getDepartmentList(ddlrdept, int.Parse(ddlorg.SelectedValue));

                    break;
                case "ddlrdept":
                    getSectionList(ddlrsec, int.Parse(ddlorg.SelectedValue), int.Parse(ddlrdept.SelectedValue));
                    break;

                case "ddlrsec":
                    select_pos(ddlrpos, int.Parse(ddlorg.SelectedValue), int.Parse(ddlrdept.SelectedValue), int.Parse(ddlrsec.SelectedValue));

                    break;

                case "ddltypereport":
                    if (ddltypereport.SelectedValue != "0")
                    {
                        if (ddltypereport.SelectedValue == "1")
                        {
                            panel_table.Visible = true;
                            panel_chart.Visible = false;
                        }
                        else if (ddltypereport.SelectedValue == "2")
                        {
                            panel_table.Visible = false;
                            panel_chart.Visible = true;

                        }
                        div_button.Visible = true;
                    }
                    else
                    {
                        panel_table.Visible = false;
                        panel_chart.Visible = false;
                        div_button.Visible = false;
                    }

                    show_report_table.Visible = false;
                    break;


                case "ddlorgidx_search":
                    getDepartmentList(ddlrdeptidx_search, int.Parse(ddlorgidx_search.SelectedValue));
                    break;

                case "ddlrdeptidx_search":
                    getSectionList(ddlrsecidx_search, int.Parse(ddlorgidx_search.SelectedValue), int.Parse(ddlrdeptidx_search.SelectedValue));
                    break;

                case "ddlrsecidx_search":
                    select_pos(ddlrposidx_search, int.Parse(ddlorgidx_search.SelectedValue), int.Parse(ddlrdeptidx_search.SelectedValue), int.Parse(ddlrsecidx_search.SelectedValue));
                    break;

                case "ddlSearchDate":

                    if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                    {
                        AddEndDate.Enabled = true;
                    }
                    else
                    {
                        AddEndDate.Enabled = false;
                        AddEndDate.Text = string.Empty;
                    }

                    break;

                case "ddlorg_chart":
                    getDepartmentList(ddlrdept_chart, int.Parse(ddlorg_chart.SelectedValue));

                    break;
                case "ddlrdept_chart":
                    getSectionList(ddlrsec_chart, int.Parse(ddlorg_chart.SelectedValue), int.Parse(ddlrdept_chart.SelectedValue));
                    break;

                case "ddlrsec_chart":
                    select_pos(ddlrpos_chart, int.Parse(ddlorg_chart.SelectedValue), int.Parse(ddlrdept_chart.SelectedValue), int.Parse(ddlrsec_chart.SelectedValue));

                    break;

                case "ddltypechart":
                    if (ddltypechart.SelectedValue == "2")
                    {
                        div_organization.Visible = true;
                        div_rsecchart.Visible = false;
                        div_rposchart.Visible = false;
                    }
                    else if (ddltypechart.SelectedValue == "3")
                    {
                        div_organization.Visible = true;
                        div_rsecchart.Visible = true;
                        div_rposchart.Visible = false;
                    }
                    else if (ddltypechart.SelectedValue == "4")
                    {
                        div_organization.Visible = true;
                        div_rsecchart.Visible = true;
                        div_rposchart.Visible = true;
                    }
                    else
                    {
                        div_organization.Visible = false;
                        div_rsecchart.Visible = false;
                        div_rposchart.Visible = false;
                    }
                    break;
            }
        }

        else if (sender is CheckBox)
        {
            CheckBox chkName = (CheckBox)sender;

            switch (chkName.ID)
            {
                case "chksex":

                    GridView Gvsex = (GridView)fvinsert.FindControl("Gvsex");

                    foreach (GridViewRow row in Gvsex.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox chksex = (CheckBox)row.Cells[0].FindControl("chksex");
                            TextBox txtqty_sex = (TextBox)row.Cells[2].FindControl("txtqty_sex");
                            CheckBoxList chknation = (CheckBoxList)row.Cells[3].FindControl("chknation");

                            if (chksex.Checked == true)
                            {
                                txtqty_sex.Enabled = true;
                                chknation.Enabled = true;
                            }
                            else
                            {
                                txtqty_sex.Enabled = false;
                                chknation.Enabled = false;
                                txtqty_sex.Text = String.Empty;
                                chknation.ClearSelection();
                            }
                        }
                    }
                    break;

                    //case "chksex_edit":
                    //    GridView Gvsex_edit = (GridView)GvDetailManPower.FindControl("Gvsex_edit");

                    //    foreach (GridViewRow row in Gvsex_edit.Rows)
                    //    {
                    //        if (row.RowType == DataControlRowType.DataRow)
                    //        {
                    //            CheckBox chksex_edit = (CheckBox)row.FindControl("chksex_edit");
                    //            CheckBoxList chknation_edit = (CheckBoxList)row.FindControl("chknation_edit");
                    //            TextBox txtqty_sex_edit = (TextBox)row.FindControl("txtqty_sex_edit");

                    //            if (chksex_edit.Checked == true)
                    //            {
                    //                txtqty_sex_edit.Enabled = true;
                    //                chknation_edit.Enabled = true;
                    //            }
                    //            else
                    //            {
                    //                txtqty_sex_edit.Enabled = false;
                    //                chknation_edit.Enabled = false;
                    //                txtqty_sex_edit.Text = String.Empty;
                    //                chknation_edit.ClearSelection();
                    //            }
                    //        }

                    //    }
                    //            break;

            }
        }
    }
    #endregion

    #region SetDefault
    protected void SetDefaultpage(int page)
    {

        switch (page)
        {

            case 1:
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewCancel.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewIndex);
                setOntop.Focus();
                select_ListIndex(GvListIndex, int.Parse(ViewState["rdept_idx"].ToString()));
                selectsum_Approve(int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["Pos_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["JobGradeIDX"].ToString()));
                break;

            case 2:

                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Add("class", "active");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewCancel.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewInsert);
                setOntop.Focus();
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();

                fvinsert.ChangeMode(FormViewMode.Insert);
                fvinsert.DataBind();

                GridView GvManPowerAdd = (GridView)fvinsert.FindControl("GvManPowerAdd");

                GvManPowerAdd.Visible = false;
                CleardataSetFormList(GvManPowerAdd);
                SetViewState_Form_Sex();
                break;

            case 3:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Add("class", "active");
                _divMenuLiToViewCancel.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                setOntop.Focus();
                MvMaster.SetActiveView(ViewApprove);
                select_ApproveListIndex(GvApproveList, int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["Pos_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["Org_idx"].ToString()),int.Parse(ViewState["JobGradeIDX"].ToString()));
                selectsum_Approve(int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["Pos_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["JobGradeIDX"].ToString()));
                break;

            case 4:
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewCancel.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                setOntop.Focus();
                MvMaster.SetActiveView(ViewDetail);

                select_empIdx_create();
                Fvdetailusercreate.ChangeMode(FormViewMode.Insert);
                Fvdetailusercreate.DataBind();


                fvdetailmanpower.ChangeMode(FormViewMode.ReadOnly);
                select_DetailManPower(GvDetailManPower, fvdetailmanpower, int.Parse(ViewState["u0_docidx"].ToString()));


                div_approve.Visible = false;
                ddl_approve.SelectedValue = "0";
                txtremark_approve.Text = String.Empty;

                selectsum_Approve(int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["Pos_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["JobGradeIDX"].ToString()));
                FvTemplate_print.ChangeMode(FormViewMode.Insert);
                FvTemplate_print.DataBind();

                try
                {

                    string getPathLotus = ConfigurationSettings.AppSettings["path_file_hr_manpower"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["doc_code"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus, ViewState["doc_code"].ToString());
                }
                catch
                {

                }

                fvdetail_cancel.ChangeMode(FormViewMode.ReadOnly);
                select_DetailManPower_Cancel(fvdetail_cancel, ViewState["doc_code"].ToString());

                HiddenField hfm0_typeidx = (HiddenField)fvdetail_cancel.FindControl("hfm0_typeidx");

                if (hfm0_typeidx.Value == "1")
                {
                    div_canceldetail.Visible = false;
                    select_Log(rpLog, int.Parse(ViewState["u0_docidx"].ToString()), "0", 1);
                }
                else
                {
                    div_canceldetail.Visible = true;
                    select_Log(rpLog, 0, ViewState["doc_code"].ToString(), 2);
                }
                break;

            case 5:
                RadioButtonList rdotypehire = (RadioButtonList)fvinsert.FindControl("rdotypehire");
                RadioButtonList rdohire = (RadioButtonList)fvinsert.FindControl("rdohire");
                DropDownList ddleducation = (DropDownList)fvinsert.FindControl("ddleducation");
                DropDownList ddlemptype = (DropDownList)fvinsert.FindControl("ddlemptype");
                DropDownList ddltypeman = (DropDownList)fvinsert.FindControl("ddltypeman");
                DropDownList ddllocate = (DropDownList)fvinsert.FindControl("ddllocate");
                TextBox datestart = (TextBox)fvinsert.FindControl("datestart");
                TextBox txtqty = (TextBox)fvinsert.FindControl("txtqty");
                TextBox txtexp = (TextBox)fvinsert.FindControl("txtexp");
                TextBox txtlanguage = (TextBox)fvinsert.FindControl("txtlanguage");
                TextBox txtother = (TextBox)fvinsert.FindControl("txtother");
                Control div_save = (Control)fvinsert.FindControl("div_save");

                rdotypehire.ClearSelection();
                rdohire.ClearSelection();
                ddleducation.SelectedValue = "0";
                txtqty.Text = String.Empty;
                txtexp.Text = String.Empty;
                txtlanguage.Text = String.Empty;
                txtother.Text = String.Empty;
                div_save.Visible = true;

                ddlemptype.Enabled = false;
                ddltypeman.Enabled = false;
                ddllocate.Enabled = false;


                break;

            case 6: // count row visible delete u1 | case user create edit data
                int countrow = GvDetailManPower.Rows.Count;

                foreach (GridViewRow row in GvDetailManPower.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        LinkButton btnDelete = (LinkButton)row.Cells[4].FindControl("btnDelete");

                        if (countrow > 1)
                        {
                            btnDelete.Visible = true;
                        }
                        else
                        {
                            btnDelete.Visible = false;
                        }
                    }
                }
                break;

            case 7:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewCancel.Attributes.Add("class", "active");
                _divMenuLiToViewReport.Attributes.Remove("class");

                setOntop.Focus();
                MvMaster.SetActiveView(ViewCancel);

                FvDetailUser_Cancel.ChangeMode(FormViewMode.Insert);
                FvDetailUser_Cancel.DataBind();
                fvwheredoccode.ChangeMode(FormViewMode.Insert);
                fvwheredoccode.DataBind();
                div_cancel.Visible = false;
                break;

            case 8:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Add("class", "active");
                _divMenuLiToViewCancel.Attributes.Remove("class");

                setOntop.Focus();
                MvMaster.SetActiveView(ViewReport);
                show_report_table.Visible = false;
                show_report_chart.Visible = false;
                fvreport.ChangeMode(FormViewMode.Insert);
                fvreport.DataBind();


                break;
        }
    }


    #endregion

    #region SetDefault Form

    protected void SetViewState_Form()
    {

        DataSet dManPowerList = new DataSet();
        dManPowerList.Tables.Add("dsAddListTable");

        dManPowerList.Tables["dsAddListTable"].Columns.Add("CheckRow", typeof(int));

        dManPowerList.Tables["dsAddListTable"].Columns.Add("OrgName", typeof(String));
        dManPowerList.Tables["dsAddListTable"].Columns.Add("OrgIDX", typeof(int));

        dManPowerList.Tables["dsAddListTable"].Columns.Add("DeptName", typeof(String));
        dManPowerList.Tables["dsAddListTable"].Columns.Add("RdeptIDX", typeof(int));


        dManPowerList.Tables["dsAddListTable"].Columns.Add("SecName", typeof(String));
        dManPowerList.Tables["dsAddListTable"].Columns.Add("SecIDX", typeof(int));

        dManPowerList.Tables["dsAddListTable"].Columns.Add("PosName", typeof(String));
        dManPowerList.Tables["dsAddListTable"].Columns.Add("PosIDX", typeof(int));

        dManPowerList.Tables["dsAddListTable"].Columns.Add("EmpTypeName", typeof(String));
        dManPowerList.Tables["dsAddListTable"].Columns.Add("EmpTypeIDX", typeof(int));


        dManPowerList.Tables["dsAddListTable"].Columns.Add("datestart", typeof(String));
        dManPowerList.Tables["dsAddListTable"].Columns.Add("qty", typeof(int));

        dManPowerList.Tables["dsAddListTable"].Columns.Add("typehire_name", typeof(String));
        dManPowerList.Tables["dsAddListTable"].Columns.Add("m0_thidx", typeof(int));

        dManPowerList.Tables["dsAddListTable"].Columns.Add("hire_name", typeof(String));
        dManPowerList.Tables["dsAddListTable"].Columns.Add("m0_hridx", typeof(int));

        //dManPowerList.Tables["dsAddListTable"].Columns.Add("sex_name", typeof(String));
        //dManPowerList.Tables["dsAddListTable"].Columns.Add("sexidx_comma", typeof(String));
        //dManPowerList.Tables["dsAddListTable"].Columns.Add("qty_sex", typeof(String));

        //dManPowerList.Tables["dsAddListTable"].Columns.Add("nationality_name", typeof(String));
        //dManPowerList.Tables["dsAddListTable"].Columns.Add("natidx", typeof(String));

        dManPowerList.Tables["dsAddListTable"].Columns.Add("education_name", typeof(String));
        dManPowerList.Tables["dsAddListTable"].Columns.Add("eduidx", typeof(int));
        dManPowerList.Tables["dsAddListTable"].Columns.Add("experience", typeof(String));
        dManPowerList.Tables["dsAddListTable"].Columns.Add("language", typeof(String));
        dManPowerList.Tables["dsAddListTable"].Columns.Add("other", typeof(String));



        ViewState["vsManPowerList"] = dManPowerList;

    }

    protected void SetViewState_Form_Sex()
    {
        DataSet dManPowerList_sex = new DataSet();
        dManPowerList_sex.Tables.Add("dsAddListTable_sex");

        dManPowerList_sex.Tables["dsAddListTable_sex"].Columns.Add("sex_name", typeof(String));
        dManPowerList_sex.Tables["dsAddListTable_sex"].Columns.Add("sexidx", typeof(int));
        dManPowerList_sex.Tables["dsAddListTable_sex"].Columns.Add("sub_qty", typeof(int));
        dManPowerList_sex.Tables["dsAddListTable_sex"].Columns.Add("nationality_name", typeof(String));
        dManPowerList_sex.Tables["dsAddListTable_sex"].Columns.Add("natidx", typeof(String));
        dManPowerList_sex.Tables["dsAddListTable_sex"].Columns.Add("CheckRow_sex", typeof(int));

        ViewState["vsManPowerList_sex"] = dManPowerList_sex;

    }

    protected void setAddList_Form()
    {

        DropDownList ddlorg = (DropDownList)fvinsert.FindControl("ddlorg");
        DropDownList ddlrdept = (DropDownList)fvinsert.FindControl("ddlrdept");
        DropDownList ddlrsec = (DropDownList)fvinsert.FindControl("ddlrsec");
        DropDownList ddlrpos = (DropDownList)fvinsert.FindControl("ddlrpos");
        TextBox datestart = (TextBox)fvinsert.FindControl("datestart");
        TextBox txtqty = (TextBox)fvinsert.FindControl("txtqty");
        RadioButtonList rdotypehire = (RadioButtonList)fvinsert.FindControl("rdotypehire");
        RadioButtonList rdohire = (RadioButtonList)fvinsert.FindControl("rdohire");
        GridView GvManPowerAdd = (GridView)fvinsert.FindControl("GvManPowerAdd");
        GridView Gvsex = (GridView)fvinsert.FindControl("Gvsex");
        DropDownList ddleducation = (DropDownList)fvinsert.FindControl("ddleducation");
        TextBox txtexp = (TextBox)fvinsert.FindControl("txtexp");
        TextBox txtlanguage = (TextBox)fvinsert.FindControl("txtlanguage");
        TextBox txtother = (TextBox)fvinsert.FindControl("txtother");
        DropDownList ddlemptype = (DropDownList)fvinsert.FindControl("ddlemptype");
        int qty = 0;

        foreach (GridViewRow row in Gvsex.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chksex = (CheckBox)row.Cells[0].FindControl("chksex");
                CheckBoxList chknation = (CheckBoxList)row.Cells[2].FindControl("chknation");
                TextBox txtqty_sex = (TextBox)row.Cells[2].FindControl("txtqty_sex");


                if (chksex.Checked)
                {
                    if (txtqty_sex.Text != "" && txtqty_sex.Text != null)
                    {

                        checkrow_sex = 1;


                        foreach (ListItem item in chknation.Items)
                        {
                            if (item.Selected)
                            {
                                checkrow_nation = 1;
                                break;
                            }
                            else
                            {
                                checkrow_nation = 0;
                            }
                        }
                        break;

                    }
                    else
                    {
                        checkrow_sex = 0;
                    }

                }
                else
                {
                    checkrow_sex = 0;
                }


            }
        }


        foreach (GridViewRow row in Gvsex.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chksex = (CheckBox)row.Cells[0].FindControl("chksex");
                TextBox txtqty_sex = (TextBox)row.Cells[2].FindControl("txtqty_sex");


                if (chksex.Checked)
                {
                    qty += int.Parse(txtqty_sex.Text);

                }
            }
        }
        if (qty != int.Parse(txtqty.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกจำนวนคนให้ถูกต้อง!!!');", true);
            return;
        }
        else
        {
            if (ViewState["vsManPowerList"] != null && checkrow_sex == 1 && checkrow_nation == 1)
            {
                DataSet dsContacts = (DataSet)ViewState["vsManPowerList"];

                foreach (DataRow dr in dsContacts.Tables["dsAddListTable"].Rows)
                {

                    if (dr["OrgIDX"].ToString() == ddlorg.SelectedValue &&
                        int.Parse(dr["RdeptIDX"].ToString()) == int.Parse(ddlrdept.SelectedValue) &&
                        int.Parse(dr["SecIDX"].ToString()) == int.Parse(ddlrsec.SelectedValue) &&
                        int.Parse(dr["PosIDX"].ToString()) == int.Parse(ddlrpos.SelectedValue) &&
                        dr["datestart"].ToString() == datestart.Text && int.Parse(dr["qty"].ToString()) == int.Parse(txtqty.Text)
                       )
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                        return;
                    }
                }

                DataRow drContacts = dsContacts.Tables["dsAddListTable"].NewRow();

                checkrow_dtset = int.Parse(ViewState["checkrow_dtset"].ToString()) + 1;
                ViewState["checkrow_dtset"] = checkrow_dtset;

                drContacts["CheckRow"] = int.Parse(ViewState["checkrow_dtset"].ToString());
                drContacts["OrgIDX"] = ddlorg.SelectedValue;
                drContacts["OrgName"] = ddlorg.SelectedItem.Text;
                drContacts["RdeptIDX"] = ddlrdept.SelectedValue;
                drContacts["DeptName"] = ddlrdept.SelectedItem.Text;
                drContacts["SecIDX"] = ddlrsec.SelectedValue;
                drContacts["SecName"] = ddlrsec.SelectedItem.Text;
                drContacts["PosIDX"] = ddlrpos.SelectedValue;
                drContacts["PosName"] = ddlrpos.SelectedItem.Text;
                drContacts["EmpTypeIDX"] = ddlemptype.SelectedValue;
                drContacts["EmpTypeName"] = ddlemptype.SelectedItem.Text;
                drContacts["datestart"] = datestart.Text;
                drContacts["qty"] = int.Parse(txtqty.Text);
                drContacts["m0_thidx"] = int.Parse(rdotypehire.SelectedValue);
                drContacts["typehire_name"] = rdotypehire.SelectedItem.Text;
                drContacts["m0_hridx"] = int.Parse(rdohire.SelectedValue);
                drContacts["hire_name"] = rdohire.SelectedItem.Text;
                drContacts["eduidx"] = int.Parse(ddleducation.SelectedValue);
                drContacts["education_name"] = ddleducation.SelectedItem.Text;
                drContacts["experience"] = txtexp.Text;
                drContacts["language"] = txtlanguage.Text;
                drContacts["other"] = txtother.Text;

                dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts);
                ViewState["vsManPowerList"] = dsContacts;


                if (ViewState["vsManPowerList_sex"] != null)
                {

                    DataSet dsContacts_sex = (DataSet)ViewState["vsManPowerList_sex"];

                    foreach (GridViewRow row in Gvsex.Rows)
                    {
                        DataRow drContacts_sex = dsContacts_sex.Tables["dsAddListTable_sex"].NewRow();

                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox chksex = (CheckBox)row.Cells[0].FindControl("chksex");
                            Label lblsexidx = (Label)row.Cells[1].FindControl("lblsexidx");
                            Label lbsexname = (Label)row.Cells[1].FindControl("lbsexname");
                            TextBox txtqty_sex = (TextBox)row.Cells[2].FindControl("txtqty_sex");
                            CheckBoxList chknation = (CheckBoxList)row.Cells[2].FindControl("chknation");

                            if (chksex.Checked)
                            {
                                drContacts_sex["sexidx"] = lblsexidx.Text;
                                drContacts_sex["sex_name"] = lbsexname.Text;
                                drContacts_sex["sub_qty"] = txtqty_sex.Text;
                                drContacts_sex["CheckRow_sex"] = int.Parse(ViewState["checkrow_dtset"].ToString());

                                foreach (ListItem item in chknation.Items)
                                {
                                    if (item.Selected)
                                    {
                                        drContacts_sex["natidx"] += item.Value + ",";
                                        drContacts_sex["nationality_name"] += item.ToString() + ",";

                                    }
                                }

                                dsContacts_sex.Tables["dsAddListTable_sex"].Rows.Add(drContacts_sex);


                            }
                            chksex.Checked = false;
                            chknation.ClearSelection();
                            txtqty_sex.Text = String.Empty;
                            txtqty_sex.Enabled = false;
                            txtqty_sex.Enabled = false;


                        }
                    }

                    ViewState["vsManPowerList_sex"] = dsContacts_sex;

                    //var ds_dept_insert_excel = (DataSet)ViewState["vsManPowerList_sex"];
                    //var _item_Totalsex = new U2_DocumentDetail[dsContacts_sex.Tables["dsAddListTable_sex"].Rows.Count];
                    //   U2_DocumentDetail[] _item_Totalsex = (U2_DocumentDetail[])ViewState["vsManPowerList_sex"];

                }

                setGridData(GvManPowerAdd, dsContacts.Tables["dsAddListTable"]);
                GvManPowerAdd.Visible = true;
                SetDefaultpage(5);

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาระบุเพศที่ต้องการ และสัญชาติที่ต้องการ!!!');", true);
                Gvsex.Focus();
                return;

            }
        }

    }

    protected void CleardataSetFormList(GridView GvName)
    {
        ViewState["vsManPowerList"] = null;
        GvName.DataSource = ViewState["vsManPowerList"];
        GvName.DataBind();
        SetViewState_Form();

        ViewState["vsManPowerList_sex"] = null;

    }




    #endregion

    #region bind Actor

    protected void setFormDataActor()
    {

        HiddenField hfM0NodeIDX = (HiddenField)fvdetailmanpower.FindControl("hfM0NodeIDX");
        HiddenField hfM0ActoreIDX = (HiddenField)fvdetailmanpower.FindControl("hfM0ActoreIDX");
        HiddenField hfM0StatusIDX = (HiddenField)fvdetailmanpower.FindControl("hfM0StatusIDX");

        ViewState["m0_node"] = hfM0NodeIDX.Value;
        ViewState["m0_actor"] = hfM0ActoreIDX.Value;
        ViewState["m0_status"] = hfM0StatusIDX.Value;

        int m0_actor = int.Parse(hfM0ActoreIDX.Value);

        //txt.Text = ViewState["m0_node"].ToString() + "," + ViewState["m0_actor"].ToString() + ViewState["m0_status"].ToString();

        switch (m0_actor)
        {

            case 1: // ผู้สร้าง
                break;

            case 2: // หัวหน้าฝ่าย
                if (ViewState["m0_node"].ToString() == "2" && ViewState["m0_status"].ToString() == "1" && ViewState["History_Detail"].ToString() == "2")
                {
                    div_approve.Visible = true;
                    btnApprove.Visible = true;
                }
                else
                {
                    div_approve.Visible = false;
                    btnApprove.Visible = false;
                }
                break;

            case 3: // ผู้จัดการฝ่าย
                if (ViewState["m0_node"].ToString() == "3" && ViewState["m0_status"].ToString() == "1" && ViewState["History_Detail"].ToString() == "2")
                {
                    div_approve.Visible = true;
                    btnApprove.Visible = true;
                }
                else
                {
                    div_approve.Visible = false;
                    btnApprove.Visible = false;
                }
                break;
            case 4: // ผู้อำนวยการฝ่าย
                if ((ViewState["m0_node"].ToString() == "4" || ViewState["m0_node"].ToString() == "9") && ViewState["m0_status"].ToString() == "1" && ViewState["History_Detail"].ToString() == "2")
                {
                    div_approve.Visible = true;
                    btnApprove.Visible = true;
                    SetDefaultApprove(ddl_approve, int.Parse(ViewState["m0_node"].ToString()));

                    if (ViewState["m0_node"].ToString() == "9")
                    {
                        div_uploadfilememo.Visible = true;
                    }
                    else
                    {
                        div_uploadfilememo.Visible = false;
                    }
                }
                else
                {
                    div_approve.Visible = false;
                    btnApprove.Visible = false;
                }

                break;

            case 5: // เจ้าหน้าที่ HR
                if ((ViewState["m0_node"].ToString() == "5" || ViewState["m0_node"].ToString() == "11") && ViewState["m0_status"].ToString() == "1" && ViewState["History_Detail"].ToString() == "2")
                {
                    div_approve.Visible = true;
                    btnApprove.Visible = true;
                    SetDefaultApprove(ddl_approve, int.Parse(ViewState["m0_node"].ToString()));

                }
                else
                {
                    div_approve.Visible = false;
                    btnApprove.Visible = false;
                }

                break;

            case 6: // ผู้อำนวยการ HR
                if (ViewState["m0_node"].ToString() == "6" && ViewState["m0_status"].ToString() == "1" && ViewState["History_Detail"].ToString() == "2")
                {
                    div_approve.Visible = true;
                    btnApprove.Visible = true;
                    SetDefaultApprove(ddl_approve, int.Parse(ViewState["m0_node"].ToString()));
                }
                else
                {
                    div_approve.Visible = false;
                    btnApprove.Visible = false;
                }

                break;
            case 7: // MD
                if (ViewState["m0_node"].ToString() == "10" && ViewState["m0_status"].ToString() == "1" && ViewState["History_Detail"].ToString() == "2")
                {
                    div_approve.Visible = true;
                    btnApprove.Visible = true;
                    SetDefaultApprove(ddl_approve, int.Parse(ViewState["m0_node"].ToString()));
                }
                else
                {
                    div_approve.Visible = false;
                    btnApprove.Visible = false;
                }

                break;
        }

    }

    protected void setFormDataActor_Cancel()
    {

        HiddenField hfM0NodeIDX = (HiddenField)fvdetail_cancel.FindControl("hfM0NodeIDX");
        HiddenField hfM0ActoreIDX = (HiddenField)fvdetail_cancel.FindControl("hfM0ActoreIDX");
        HiddenField hfM0StatusIDX = (HiddenField)fvdetail_cancel.FindControl("hfM0StatusIDX");
        Control div_approve_cancel = (Control)fvdetail_cancel.FindControl("div_approve_cancel");
        LinkButton btnApprove_Cancel = (LinkButton)fvdetail_cancel.FindControl("btnApprove_Cancel");
        DropDownList ddlaproove_cancel = (DropDownList)fvdetail_cancel.FindControl("ddlaproove_cancel");
        ViewState["m0_node"] = hfM0NodeIDX.Value;
        ViewState["m0_actor"] = hfM0ActoreIDX.Value;
        ViewState["m0_status"] = hfM0StatusIDX.Value;

        int m0_actor = int.Parse(hfM0ActoreIDX.Value);

        //txt.Text = ViewState["m0_node"].ToString() + "," + ViewState["m0_actor"].ToString() + "," + ViewState["m0_status"].ToString() + "," + ViewState["History_Detail"].ToString();

        switch (m0_actor)
        {

            case 1: // ผู้สร้าง
                break;

            case 2: // หัวหน้าฝ่าย
                if (ViewState["m0_node"].ToString() == "2" && ViewState["m0_status"].ToString() == "1" && ViewState["History_Detail"].ToString() == "2")
                {
                    div_approve_cancel.Visible = true;
                    btnApprove_Cancel.Visible = true;

                }
                else
                {
                    div_approve_cancel.Visible = false;
                    btnApprove_Cancel.Visible = false;


                }
                break;

            case 3: // ผู้จัดการฝ่าย
                if (ViewState["m0_node"].ToString() == "3" && ViewState["m0_status"].ToString() == "1" && ViewState["History_Detail"].ToString() == "2")
                {
                    div_approve_cancel.Visible = true;
                    btnApprove_Cancel.Visible = true;
                }
                else
                {
                    div_approve_cancel.Visible = false;
                    btnApprove_Cancel.Visible = false;
                }
                break;
            case 4: // ผู้อำนวยการฝ่าย
                if ((ViewState["m0_node"].ToString() == "4" || ViewState["m0_node"].ToString() == "9") && ViewState["m0_status"].ToString() == "1" && ViewState["History_Detail"].ToString() == "2")
                {
                    div_approve_cancel.Visible = true;
                    btnApprove_Cancel.Visible = true;
                }
                else
                {
                    div_approve_cancel.Visible = false;
                    btnApprove_Cancel.Visible = false;
                }

                break;

            case 5: // เจ้าหน้าที่ HR
                if ((ViewState["m0_node"].ToString() == "5" || ViewState["m0_node"].ToString() == "11") && ViewState["m0_status"].ToString() == "1" && ViewState["History_Detail"].ToString() == "2")
                {
                    div_approve_cancel.Visible = true;
                    btnApprove_Cancel.Visible = true;
                    SetDefaultApprove_Cancel(ddlaproove_cancel, int.Parse(ViewState["m0_node"].ToString()));

                }
                else
                {
                    div_approve_cancel.Visible = false;
                    btnApprove_Cancel.Visible = false;
                }

                break;

            case 6: // ผู้อำนวยการ HR
                if (ViewState["m0_node"].ToString() == "6" && ViewState["m0_status"].ToString() == "1" && ViewState["History_Detail"].ToString() == "2")
                {
                    div_approve_cancel.Visible = true;
                    btnApprove_Cancel.Visible = true;
                    SetDefaultApprove_Cancel(ddlaproove_cancel, int.Parse(ViewState["m0_node"].ToString()));
                }
                else
                {
                    div_approve_cancel.Visible = false;
                    btnApprove_Cancel.Visible = false;
                }

                break;
            case 7: // ผู้อำนวยการ HR
                if (ViewState["m0_node"].ToString() == "10" && ViewState["m0_status"].ToString() == "1" && ViewState["History_Detail"].ToString() == "2")
                {
                    div_approve_cancel.Visible = true;
                    btnApprove_Cancel.Visible = true;
                    SetDefaultApprove_Cancel(ddlaproove_cancel, int.Parse(ViewState["m0_node"].ToString()));
                }
                else
                {
                    div_approve_cancel.Visible = false;
                    btnApprove_Cancel.Visible = false;
                }

                break;

        }

    }

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "_divMenuBtnToDivIndex":
            case "BtnBack":
                SetDefaultpage(1);
                break;

            case "_divMenuBtnToDivAdd":

                SetDefaultpage(2);
                break;

            case "_divMenuBtnToDivApprove":
                SetDefaultpage(3);
                break;

            case "_divMenuBtnToDivCancel":
                SetDefaultpage(7);
                break;
            case "_divMenuBtnToDivReport":
            case "btnrefresh_report":
                SetDefaultpage(8);
                break;
            case "CmdInsert_Dataset":
                setAddList_Form();

                break;

            case "CmdInsert":
                Insert_Manpower();
                SetDefaultpage(1);
                break;

            case "CmdDetail":
                string[] arg1_detail = new string[5];
                arg1_detail = e.CommandArgument.ToString().Split(';');

                ViewState["u0_docidx"] = arg1_detail[0];
                ViewState["CEmpIDX_u0"] = arg1_detail[1];
                ViewState["History_Detail"] = arg1_detail[2];
                ViewState["doc_code"] = arg1_detail[3];
                ViewState["m0_typeidx"] = arg1_detail[4];

                SetDefaultpage(4);
                if (ViewState["m0_typeidx"].ToString() == "1")
                {
                    setFormDataActor();
                    SetDefaultpage(6);

                }
                else
                {
                    setFormDataActor_Cancel();
                }

                break;
            case "CmdDetail_Print":
                string[] arg_print = new string[2];
                arg_print = e.CommandArgument.ToString().Split(';');
                ViewState["u1_docidx_Print"] = int.Parse(arg_print[0]);
                ViewState["SecNameTH_Print"] = arg_print[1].ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                lblsecname.Text = ViewState["SecNameTH_Print"].ToString();

                FvTemplate_print.ChangeMode(FormViewMode.ReadOnly);
                select_DetailManPowerForPrint(FvTemplate_print, int.Parse(arg_print[0]));

                GridView GvDetailSex_Print = (GridView)FvTemplate_print.FindControl("GvDetailSex_Print");
                CheckBoxList chktypehire = (CheckBoxList)FvTemplate_print.FindControl("chktypehire");
                CheckBoxList chkhire = (CheckBoxList)FvTemplate_print.FindControl("chkhire");

                selectdetail_sexprint(GvDetailSex_Print, int.Parse(arg_print[0]));
                select_typehire_Print(chktypehire);
                select_hire_Print(chkhire);

                chktypehire.SelectedValue = ViewState["m0_thidx_print"].ToString();
                chkhire.SelectedValue = ViewState["m0_hridx_print"].ToString();


                break;
            case "btnCancel":
                SetDefaultpage(3);
                break;

            case "btnback":
                SetDefaultpage(1);
                break;

            case "CmdUpdateApprove":

                string getPath = ConfigurationSettings.AppSettings["path_file_hr_manpower"];
                string filePath = Server.MapPath(getPath + ViewState["doc_code"].ToString());
                DirectoryInfo dir = new DirectoryInfo(filePath);
                HttpFileCollection hfc = Request.Files;

                SearchDirectories(dir, ViewState["doc_code"].ToString());


                if (UploadFileMemo.HasFile)
                {
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    string extension = Path.GetExtension(UploadFileMemo.FileName);

                    UploadFileMemo.SaveAs(Server.MapPath(getPath + ViewState["doc_code"].ToString()) + "\\" + "Memo_" + ViewState["doc_code"].ToString() + extension);
                }
                else
                {
                    if (int.Parse(ViewState["m0_node"].ToString()) == 9)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาอัพโหลดไฟล์ เอกสารลงนามอนุมัติโดยประธานเจ้าหน้าที่บริหาร!!!');", true);
                        return;

                    }
                }

                Approve_Manpower();
                SetDefaultpage(3);
                break;

            case "CmdDel":
                int u1idx = int.Parse(cmdArg);
                Delete_Manpower(u1idx);
                SetDefaultpage(4);
                SetDefaultpage(6);

                break;

            case "CmdSearch_Cancel":
                DropDownList ddldoccode_cancel = (DropDownList)fvwheredoccode.FindControl("ddldoccode_cancel");
                div_cancel.Visible = true;
                fvinsert_cancel.ChangeMode(FormViewMode.ReadOnly);
                select_DetailManPower(GvManPowerDetail_Cancel, fvinsert_cancel, int.Parse(ddldoccode_cancel.SelectedValue));
                panel_cancel.Visible = true;

                break;

            case "CmdSaveCancel":
                Insert_Manpower_Cancel();
                SetDefaultpage(1);
                break;

            case "CmdUpdateApprove_Cancel":
                Approve_Cancel();
                SetDefaultpage(1);
                break;

            case "btnsearch_report":
                DropDownList ddltypereport = (DropDownList)fvreport.FindControl("ddltypereport");
                DropDownList ddltypechart = (DropDownList)fvreport.FindControl("ddltypechart");
                if (ddltypereport.SelectedValue == "1")
                {
                    selectreporttable(GvReport);
                    show_report_table.Visible = true;
                    show_report_chart.Visible = false;
                }
                else if (ddltypereport.SelectedValue == "2")
                {
                    show_report_table.Visible = false;
                    show_report_chart.Visible = true;

                    if (ddltypechart.SelectedValue == "1")
                    {
                        Select_ChartLV1();
                    }
                    else if (ddltypechart.SelectedValue == "2")
                    {
                        Select_ChartLV2();
                    }
                    else if (ddltypechart.SelectedValue == "3")
                    {
                        Select_ChartLV3();
                    }
                    else if (ddltypechart.SelectedValue == "4")
                    {
                        Select_ChartLV4();
                    }
                    else if (ddltypechart.SelectedValue == "5")
                    {
                        Select_ChartLV5();
                    }
                }
                else
                {
                    show_report_table.Visible = false;
                    show_report_chart.Visible = false;
                }
                break;

            case "_divMenuBtnToDivManual":
                Response.Write("<script>window.open('https://docs.google.com/document/d/1q00_fwb4WhCg1cVoBgwGa0KP6HjIyiPAndWEHJ0i2bs/edit?usp=sharing','_blank');</script>");

                break;
            case "_divMenuBtnToDivWaitingNormal":
                Response.Write("<script>window.open('https://drive.google.com/file/d/1fUemWiUymslvKyIxWkBqb7zf0LzTsLup/view?usp=sharing','_blank');</script>");

                break;
            case "_divMenuBtnToDivWaitingSpecial":
                Response.Write("<script>window.open('https://drive.google.com/file/d/1fCSgOx78xM7SkS5RHRstibpCwFINU2mf/view?usp=sharing','_blank');</script>");

                break;
            case "_divMenuBtnToDivWaitingCancel":

                Response.Write("<script>window.open('https://drive.google.com/file/d/1CpSHLgpV5W8EzmI0exNeXwvAITh2SCyT/view?usp=sharing','_blank');</script>");
                break;
        }
    }
    #endregion
}
