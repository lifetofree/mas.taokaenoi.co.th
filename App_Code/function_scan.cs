using System;

using zkemkeeper;

/// <summary>
/// Summary description for function_scan
/// </summary>
public class function_scan
{
    #region initial
    public CZKEM axCZKEM1 = new CZKEM();
    private bool bIsConnected = false;//the boolean value identifies whether the device is connected
    private int iMachineNumber = 1;//the serial number of the device.After connecting

    private string sIp = "";
    private int iPort = 0;
    private int idwErrorCode = 0;
    private int idwValue = 0;
    private int idwStatus = 0;
    private int iFlag = 0;
    private string sTmpData = String.Empty;
    private int iTmpLength = 0;
    private int iGLCount = 0;

    private string sdwEnrollNumber = String.Empty;
    private int idwVerifyMode = 0;
    private int idwInOutMode = 0;
    private int idwYear = 0;
    private int idwMonth = 0;
    private int idwDay = 0;
    private int idwHour = 0;
    private int idwMinute = 0;
    private int idwSecond = 0;
    private int idwWorkcode = 0;

    private int idwBackupNumber = 0;//10:password data, 11:all fingerprint, 12:all fingerprint and password data
    private string sUserName = "";
    private string sUserPassword = "";
    private int iUserPrivilege = 0;
    private bool bEnabled = false;

    private string sACardNumber = "";

    private string retString = String.Empty;
    #endregion

    public function_scan()
    {
    }

    #region machine connect
    public bool setMachineConnect(string sIp, int iPort)
    {
        bIsConnected = axCZKEM1.Connect_Net(sIp, iPort);
        if (bIsConnected)
        {
            //register machine
            iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
            axCZKEM1.RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool setMachineDisconnect()
    {
        axCZKEM1.Disconnect();
        return false;
    }
    #endregion machine connect

    #region machine method
    public int getMachineError()
    {
        axCZKEM1.GetLastError(ref idwErrorCode);
        return idwErrorCode;
    }

    #region device
    public void setEnableDevice(int iMachineNumber, bool bMachineStatus)
    {
        axCZKEM1.EnableDevice(iMachineNumber, bMachineStatus);
    }

    public string getDeviceStatus(int iMachineNumber, int idwStatus)
    {
        if (axCZKEM1.GetDeviceStatus(iMachineNumber, idwStatus, ref idwValue))
        {
            return idwValue.ToString();
        }
        else
        {
            return getMachineError().ToString();
        }
    }
    #endregion device

    #region clear administrator, data, log on machine
    public string clearAdministrators(int iMachineNumber)
    {
        if (axCZKEM1.ClearAdministrators(iMachineNumber))
        {
            return "success";
        }
        else
        {
            return getMachineError().ToString();
        }
    }

    public string clearAdministratorsLog(int iMachineNumber) //clean all administrator log
    {
        if (axCZKEM1.ClearSLog(iMachineNumber))
        {
            return "success";
        }
        else
        {
            return getMachineError().ToString();
        }
    }

    public string clearKeeperData(int iMachineNumber) //clear all data in the standalone fingerprint machine, such as user information, fingerprints, attendance logs, management record. *** this operation maybe arising of loss all data in the standalone machine, be careful to use it.
    {
        if (axCZKEM1.ClearKeeperData(iMachineNumber))
        {
            return "success";
        }
        else
        {
            return getMachineError().ToString();
        }
    }

    public string clearAttendanceLog(int iMachineNumber) //clean all attendance logs
    {
        if (axCZKEM1.ClearGLog(iMachineNumber))
        {
            return "success";
        }
        else
        {
            return getMachineError().ToString();
        }
    }

    public string refreshData(int iMachineNumber) //refresh data
    {
        if (axCZKEM1.RefreshData(iMachineNumber))
        {
            return "success";
        }
        else
        {
            return getMachineError().ToString();
        }
    }
    #endregion clear administrator, data, log on machine

    #region user template
    public string getUserTemplate(int iMachineNumber, string sdwEnrollNumber, int idwFingerIndex)
    {
        if (axCZKEM1.GetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, out iFlag, out sTmpData, out iTmpLength))
        {
            return sTmpData;
        }
        else
        {
            return getMachineError().ToString();
        }
    }

    public string setUserTemplate(int iMachineNumber, string sdwEnrollNumber, int idwFingerIndex, int iFlag, string sTmpData)
    {
        if (axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, "", "", 0, true))
        {
            axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iFlag, sTmpData);
            axCZKEM1.RefreshData(iMachineNumber);
            return "set data success " + sdwEnrollNumber + " finger index = " + idwFingerIndex;
        }
        else
        {
            return getMachineError().ToString();
        }
    }

    public string delUserTemplate(int iMachineNumber, string sdwEnrollNumber, int idwFingerIndex)
    {
        if (axCZKEM1.SSR_DelUserTmp(iMachineNumber, sdwEnrollNumber, idwFingerIndex))
        {
            axCZKEM1.RefreshData(iMachineNumber);
            return "delete success " + sdwEnrollNumber + " finger index = " + idwFingerIndex;
        }
        else
        {
            return getMachineError().ToString();
        }
    }

    public string getAllUserTemplate(int iMachineNumber)
    {
        if (axCZKEM1.ReadAllUserID(iMachineNumber))
        {
            while(axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, out sdwEnrollNumber, out sUserName, out sUserPassword, out iUserPrivilege, out bEnabled))
            {
                string _sdwEnrollNumber = sdwEnrollNumber;

                for (int i=0; i<=9; i++)
                {
                    if (axCZKEM1.GetUserTmpExStr(iMachineNumber, _sdwEnrollNumber, i, out iFlag, out sTmpData, out iTmpLength))
                    {
                        retString += _sdwEnrollNumber + "|" + sTmpData + "|" + i.ToString() + "|" + iFlag.ToString() + "|" + iTmpLength.ToString() + setNewLine();
                    }
                    else
                    {
                        retString += getMachineError().ToString() + setNewLine();
                    }
                }
            }
        }
        else
        {
            retString = getMachineError().ToString();
        }

        return retString;
    }
    #endregion user template

    #region user info
    public string getUserInfo(int iMachineNumber, string sdwEnrollNumber)
    {
        if (axCZKEM1.SSR_GetUserInfo(iMachineNumber, sdwEnrollNumber, out sUserName, out sUserPassword, out iUserPrivilege, out bEnabled))
        {
            return sdwEnrollNumber + "|" + sUserName + "|" + sUserPassword + "|" + iUserPrivilege.ToString() + "|" + bEnabled.ToString();
        }
        else
        {
            return getMachineError().ToString();
        }
    }

    public string setUserInfo(int iMachineNumber, string sdwEnrollNumber, string sUserName, string sUserPassword, int iUserPrivilege, bool bEnabled)
    {
        if (axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sUserName, sUserPassword, iUserPrivilege, bEnabled))
        {
            return "set data success " + sdwEnrollNumber;
        }
        else
        {
            return getMachineError().ToString();
        }
    }

    public string getAllUserInfo(int iMachineNumber)
    {
        if (axCZKEM1.ReadAllUserID(iMachineNumber))
        {
            while(axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, out sdwEnrollNumber, out sUserName, out sUserPassword, out iUserPrivilege, out bEnabled))
            {
                retString += sdwEnrollNumber + "|" + sUserName + "|" + sUserPassword + "|" + iUserPrivilege.ToString() + "|" + bEnabled.ToString() + setNewLine();
            }

            return retString;
        }
        else
        {
            return getMachineError().ToString();
        }
    }
    #endregion user info

    #region attendance log
    public string getAttendanceLog(int iMachineNumber)
    {
        if (axCZKEM1.ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
        {
            while (axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, out sdwEnrollNumber, out idwVerifyMode, out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
            {
                iGLCount++;
                retString += iGLCount.ToString() + " | ";
                retString += sdwEnrollNumber + " | ";
                retString += idwVerifyMode.ToString() + " | ";
                retString += idwInOutMode.ToString() + " | ";
                retString += idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString() + " | ";
                retString += idwWorkcode.ToString() + setNewLine();
            }
            return retString;
        }
        else
        {
            return getMachineError().ToString();
        }
    }

    public string delAttendanceLog(int iMachineNumber)
    {
        if (axCZKEM1.ClearGLog(iMachineNumber))
        {
            if (axCZKEM1.RefreshData(iMachineNumber))
            {
                retString = "Attendance record(s) cleared";
            }
            else
            {
                retString = getMachineError().ToString();
            }
        }
        else
        {
            retString = getMachineError().ToString();
        }

        return retString;
    }
    #endregion attendance log

    #region user enroll
    public string delUserEnroll(int iMachineNumber, string sdwEnrollNumber, int idwBackupNumber)
    {
        if (axCZKEM1.SSR_DeleteEnrollData(iMachineNumber, sdwEnrollNumber, idwBackupNumber))
        {
            axCZKEM1.RefreshData(iMachineNumber);
            retString += "delete success " + sdwEnrollNumber + "<br />----------<br />";
        }
        else
        {
            retString += getMachineError().ToString() + "<br />----------<br />";
        }

        return retString;
    }

    public string delAllUserEnroll(int iMachineNumber, string sdwEnrollNumber, int idwBackupNumber)
    {
        while (axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, out sdwEnrollNumber, out sUserName, out sUserPassword, out iUserPrivilege, out bEnabled))//get all user info
        {
            if (axCZKEM1.SSR_DeleteEnrollData(iMachineNumber, sdwEnrollNumber, idwBackupNumber))
            {
                axCZKEM1.RefreshData(iMachineNumber);
                retString += "delete success " + sdwEnrollNumber + "<br />----------<br />";
            }
            else
            {
                retString += getMachineError().ToString() + "<br />----------<br />";
            }
        }

        return retString;
    }
    #endregion user enroll

    #region user card
    public string getCardUser(int iMachineNumber)
    {
        // while (axCZKEM1.GetStrCardNumber(out sACardNumber))
        // {
        //     retString += sACardNumber + setNewLine();
        // }

        if (axCZKEM1.ReadAllUserID(iMachineNumber))
        {
            while(axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, out sdwEnrollNumber, out sUserName, out sUserPassword, out iUserPrivilege, out bEnabled))
            {
                if (axCZKEM1.GetStrCardNumber(out sACardNumber))//get the card number from the memory
                {
                    retString += sACardNumber + setNewLine();
                }
                else
                {
                    retString += sdwEnrollNumber + setNewLine();
                }
            }
        }
        else
        {
            retString = getMachineError().ToString();
        }

        return retString;
    }

    public string setCardUser(string sACardNumber)
    {
        if (axCZKEM1.SetStrCardNumber(sACardNumber))
        {
            axCZKEM1.RefreshData(iMachineNumber);
            retString += "add card success " + sACardNumber;
        }
        else
        {
            retString += getMachineError().ToString();
        }

        return retString;
    }
    #endregion user card

    #region machine option
    public string beep(int iDelayMS)
    {
        if (axCZKEM1.Beep(iDelayMS))
        {
            return "Beep " + iDelayMS.ToString() + " ms";
        }
        else
        {
            return getMachineError().ToString();
        }
    }

    public string getDeviceTime(int iMachineNumber)
    {
        if(axCZKEM1.GetDeviceTime(iMachineNumber, ref idwYear, ref idwMonth, ref idwDay, ref idwHour, ref idwMinute, ref idwSecond))
        {
            return idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString();
        }
        else
        {
            return getMachineError().ToString();
        }
    }

    public string setDeviceTime(int iMachineNumber) //set the time of machine and the terminal to sync
    {
        if (axCZKEM1.SetDeviceTime(iMachineNumber))
        {
            return "success";
        }
        else
        {
            return getMachineError().ToString();
        }
    }

    public string restartDevice(int iMachineNumber)
    {
        if (axCZKEM1.RestartDevice(iMachineNumber))
        {
            return "success";
        }
        else
        {
            return getMachineError().ToString();
        }
    }
    #endregion machine option
    #endregion machine method

    #region reuse
    private string setNewLine()
    {
        return "<br />----------<br />";
    }
    #endregion reuse
}