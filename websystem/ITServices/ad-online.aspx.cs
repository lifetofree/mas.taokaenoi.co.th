﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_ITServices_ad_online : System.Web.UI.Page
{
    #region Init
    data_adonline dataADOnline = new data_adonline();
    data_googlelicense dataGoogle = new data_googlelicense();
    data_employee dataEmployee = new data_employee();
    function_tool _funcTool = new function_tool();
    service_mail serviceMail = new service_mail();
    string _local_xml = string.Empty;
    string _local_json = string.Empty;
    string _localJson = String.Empty;
    int[] webSecEmpIdx = { 172, 173, 1374, 1413, 3760, 4839 };
    #endregion Init

    #region URL Service
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    // Permission Permanent
    static string _urlCreateU0PermPerm = _serviceUrl + ConfigurationManager.AppSettings["urlADCreateU0PermPerm"];
    static string _urlReadU0IndexPermPerm = _serviceUrl + ConfigurationManager.AppSettings["urlADReadU0IndexPermPerm"];
    static string _urlUpdateU0WaitingPermPerm = _serviceUrl + ConfigurationManager.AppSettings["urlADUpdateU0WaitingPermPerm"];
    static string _urlReadU1IndexDetailPermPerm = _serviceUrl + ConfigurationManager.AppSettings["urlADReadU1IndexDetailPermPerm"];
    static string _urlReadU0WaitingPermPerm = _serviceUrl + ConfigurationManager.AppSettings["urlADReadU0WaitingPermPerm"];
    static string _urlReadU0WaitingDetailPermPerm = _serviceUrl + ConfigurationManager.AppSettings["urlADReadU0WaitingDetailPermPerm"];
    static string _urlReadU1WaitingPermPerm = _serviceUrl + ConfigurationManager.AppSettings["urlADReadU1WaitingPermPerm"];
    static string _urlApproveU0WaitingPermPerm = _serviceUrl + ConfigurationManager.AppSettings["urlADApproveU0WaitingPermPerm"];
    static string _urlADSaveCommentITU0WaitingPermPerm = _serviceUrl + ConfigurationManager.AppSettings["urlADSaveCommentITU0WaitingPermPerm"];
    // Permission Temporary
    static string _urlCreateU0PermTemp = _serviceUrl + ConfigurationManager.AppSettings["urlADCreateU0PermTemp"];
    static string _urlCreateU1PermTemp = _serviceUrl + ConfigurationManager.AppSettings["urlADCreateU1PermTemp"];
    static string _urlReadU0IndexPermTemp = _serviceUrl + ConfigurationManager.AppSettings["urlADReadU0IndexPermTemp"];
    static string _urlReadU1IndexDetailPermTemp = _serviceUrl + ConfigurationManager.AppSettings["urlADReadU1IndexDetailPermTemp"];
    static string _urlReadU0WaitingPermTemp = _serviceUrl + ConfigurationManager.AppSettings["urlADReadU0WaitingPermTemp"];
    static string _urlReadU0WaitingDetailPermTemp = _serviceUrl + ConfigurationManager.AppSettings["urlADReadU0WaitingDetailPermTemp"];
    static string _urlReadU1WaitingDetailPermTemp = _serviceUrl + ConfigurationManager.AppSettings["urlADReadU1WaitingDetailPermTemp"];
    static string _urlApproveU0WaitingPermTemp = _serviceUrl + ConfigurationManager.AppSettings["urlADApproveU0WaitingPermTemp"];
    static string _urlApproveU1WaitingPermTemp = _serviceUrl + ConfigurationManager.AppSettings["urlADApproveU1WaitingPermTemp"];
    static string _urlSaveWaitingPermTemp = _serviceUrl + ConfigurationManager.AppSettings["urlADSaveWaitingPermTemp"];
    // Permission Log
    static string _urlReadL0Perm = _serviceUrl + ConfigurationManager.AppSettings["urlADReadL0Perm"];
    static string _urlCreateL0Perm = _serviceUrl + ConfigurationManager.AppSettings["urlADCreateL0Perm"];
    // Others
    static string _urlReadEmpProfile = _serviceUrl + ConfigurationManager.AppSettings["urlADReadEmpProfile"];
    static string _urlReadEmpProfileByEmpCode = _serviceUrl + ConfigurationManager.AppSettings["urlADReadEmpProfileByEmpCode"];
    static string _urlReadPermType = _serviceUrl + ConfigurationManager.AppSettings["urlADReadPermType"];
    static string _urlReadNode = _serviceUrl + ConfigurationManager.AppSettings["urlADReadNode"];
    static string _urlReadEmails = _serviceUrl + ConfigurationManager.AppSettings["urlADReadEmails"];
    static string _urlReadPermPol = _serviceUrl + ConfigurationManager.AppSettings["urlADReadPermPol"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];
    static string _urlADSetActive_MenuApprove = _serviceUrl + ConfigurationManager.AppSettings["urlADSetActive_MenuApprove"];
    static string _urlADSelectWho_CanApprove = _serviceUrl + ConfigurationManager.AppSettings["urlADSelectWho_CanApprove"];
    static string _urlGetSelectMailCheckAd = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectMailCheckAd"];
    static string _urlADSelect_ADDMORE_DATA = _serviceUrl + ConfigurationManager.AppSettings["urlADSelect_ADDMORE_DATA"];
    static string _urlSetHolderEmailFormAdOnline = _serviceUrl + ConfigurationManager.AppSettings["urlSetHolderEmailFormAdOnline"];
    static string _urlSelectVPNMAIN = _serviceUrl + ConfigurationManager.AppSettings["urlSelectVPNMAIN"];
    static string _urlSelectVPNSUBMAIN = _serviceUrl + ConfigurationManager.AppSettings["urlSelectVPNSUBMAIN"];
    static string _urlSelectU2VPN = _serviceUrl + ConfigurationManager.AppSettings["urlSelectU2VPN"];


    // vpn
    static string _urlInsertU2VPN = _serviceUrl + ConfigurationManager.AppSettings["urlInsertU2VPN"];
    static string _urlApproveU2VPN = _serviceUrl + ConfigurationManager.AppSettings["urlApproveU2VPN"];

    static string _uploadPERM = ConfigurationManager.AppSettings["path_file_adonline_perm"];
    static string _uploadTEMP = ConfigurationManager.AppSettings["path_file_adonline_temp"];

    #endregion URL Service

    #region Constant
    public static class Constants
    {
        public const int NUMBER_NULL = -1;
        public const int CREATE = 10;
        public const int UPDATE = 11;
        public const int UPDATE_NODE = 12;
        public const int UPDATE_STATUS = 13;
        public const int UPDATE_IT = 14;
        public const int SELECT_ALL = 20;
        public const int SELECT_WHERE = 21;
        public const int SELECT_WHERE_WAITING = 22;
        public const int SELECT_WHERE_IN = 23;
        public const int SELECT_ALL_ONLINE = 24;
        public const int SELECT_EMAIL = 26;
        public const int SELECT_WHERE_EMP_CODE = 27;
        public const int SELECT_WHERE_APPROVER = 28;
        public const int SELECT_WHERE_EMP_DATA = 29;
        public const int BAN = 30;
        public const int UNBAN = 31;
        public const string VALID_FALSE = "101";
        public const int RADIO_PERM_PERM = 2;
        public const int RADIO_PERM_TEMP = 1;
        public const int RADIO_EMP_NEW = 1;
        public const int RADIO_EMP_OLD = 2;
        public const int RADIO_EMP_VISITOR = 3;
        public const int CREATE_U0_PERM = 10;
        public const int CREATE_U1_PERM = 11;
        public const string PREFIX_ALPHABET_PERM_PERM = "AD";
        public const string PREFIX_ALPHABET_PERM_TEMP = "GU";
        public const int ORG_TKN_IDX = 1;
        public const int ORG_TOBI_IDX = 2;
        public const int ORG_TKNL_IDX = 3;
        public const int ORG_GENC_IDX = 5;
        public const int ORG_NCP_IDX = 6;
        public const int ORG_GCI_IDX = 18;
        public const string PREFIX_CODE_ID_ORG_TKN = "01";
        public const string PREFIX_CODE_ID_ORG_TKNL = "02";
        public const string PREFIX_CODE_ID_ORG_TOBI = "03";
        public const string PREFIX_CODE_ID_ORG_GENC = "05";
        public const string PREFIX_CODE_ID_ORG_NCP = "04";
        public const string PREFIX_CODE_ID_ORG_GCI = "06";

        public const string WITH_WHERE_YES = "YES";
        public const string WITH_WHERE_NO = "NO";
        public const string KEY_GET_FROM_PERM_TEMP = "TEMP";
        public const string KEY_GET_FROM_PERM_PERM = "PERM";
        public const string TYPE_EMAIL_PERM_TEMP_APP = "APP";
        public const string TYPE_EMAIL_PERM_TEMP_NOTAPP = "NOTAPP";
        public const string TYPE_EMAIL_PERM_TEMP_SUCCESS = "SUCCESS";
        public const int PERM_TEMP_MAX_CONTACT = 10;
        public const int NODE_PERM_TEMP_CREATE_GOTO_DIRECTOR_CREATOR = 1;
        public const int NODE_PERM_TEMP_APP_GOTO_HEADER_IT = 2;
        public const int NODE_PERM_TEMP_NOTAPP_ENDBY_DIRECTOR_CREATOR = 3;
        public const int NODE_PERM_TEMP_APP_GOTO_DIRECTOR_IT = 4;
        public const int NODE_PERM_TEMP_NOTAPP_ENDBY_HEADER_IT = 5;
        public const int NODE_PERM_TEMP_APP_GOTO_IT = 6;
        public const int NODE_PERM_TEMP_NOTAPP_ENDBY_DIRECTOR_IT = 7;
        public const int NODE_PERM_TEMP_SAVE_ENDBY_IT = 8;
        public const int U1_PERM_TEMP_STATUS_APP_OR_ONLINE = 1;
        public const int U1_PERM_TEMP_STATUS_NOTAPP = 2;
        public const int RDEPT_MIS_ID = 20;
        public const int JOBLEVEL_MIS_RDEPT = 6;
        public const int JOBLEVEL_OTHER_RDEPT = 6;
        public static int[] NODE_PERM_TEMP_APPROVE = { 1, 2, 4, 6 };
        public static int[] NODE_PERM_TEMP_NOT_APPROVE = { 3, 5, 7 };
        public static int[] NODE_PERM_TEMP_SUCCESS = { 8 };
        public static int[] JOBLEVELS_OFFICIAL = { 1, 2, 3, 4, 5 };
        public static int[] JOBLEVELS_HEADER = { 6, 7, 8 };
        public static int[] JOBLEVELS_DIRECTOR = { 9, 10, 11, 12, 13, 14 };
        public const int NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_HR = 1;
        public const int NODE_PERM_PERM_CREATE_GOTO_HEADER_EMPNEW = 2;
        public const int NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_EMPNEW = 3;
        public const int NODE_PERM_PERM_DIRECTOR_HR_APP_GOTO_HEADER_IT = 4;
        public const int NODE_PERM_PERM_DIRECTOR_HR_NOTAPP_ENDBY_DIRECTOR_HR = 5;
        public const int NODE_PERM_PERM_HEADER_EMPNEW_APP_GOTO_DIRECTOR_EMPNEW = 6;
        public const int NODE_PERM_PERM_HEADER_EMPNEW_NOTAPP_ENDBY_HEADER_EMPNEW = 7;
        public const int NODE_PERM_PERM_DIRECTOR_EMPNEW_APP_GOTO_HEADER_IT = 8;
        public const int NODE_PERM_PERM_DIRECTOR_EMPNEW_NOTAPP_ENDBY_DIRECTOR_EMPNEW = 9;
        public const int NODE_PERM_PERM_HEADER_IT_APP_GOTO_DIRECTOR_IT = 10;
        public const int NODE_PERM_PERM_HEADER_IT_NOTAPP_ENDBY_HEADER_IT = 11;
        public const int NODE_PERM_PERM_DIRECTOR_IT_APP_GOTO_IT = 12;
        public const int NODE_PERM_PERM_DIRECTOR_IT_NOTAPP_ENDBY_DIRECTOR_IT = 13;
        public const int NODE_PERM_PERM_IT_APP_FINISHBY_IT = 14;
        public const int NODE_PERM_PERM_IT_NOTAPP_ENDBY_IT = 15;
        public const int node_perm_perm_header_empnew_app_goto_it = 16;
        public const int node_perm_perm_director_empnew_app_goto_it = 17;
        public const int node_perm_perm_it_app_goto_headit = 18;
        public const int node_perm_perm_it_notapp_endbyprocess_it = 19;
        public const int node_perm_perm_header_empnew_sendedit_user = 20;
        public const int node_perm_perm_director_empnew_sendedit_user = 21;
        public const int node_perm_perm_director_hr_sendedit_user = 22;
        public const int node_perm_perm_it_sendedit_user = 23;
        public const int node_perm_perm_headerit_sendedit_user = 24;
        public const int node_perm_perm_directorit_sendedit_user = 25;

        public static int[] NODE_PERM_PERM_APPROVE = { 1, 2, 4, 6, 8, 10, 12, 16, 17, 18 };
        public static int[] NODE_PERM_PERM_NOT_APPROVE = { 3, 5, 7, 9, 11, 13 };
        public static int[] NODE_PERM_PERM_NOT_IT_APPROVE = { 15, 19 };
        public static int[] NODE_PERM_PERM_SUCCESS = { 14 };
        public const int NODE_PERM_PERM_SENT_COMMENT = 999;
        public static int[] NODE_PERM_PERM_Edit = { 20, 21, 22, 23, 24, 25 };
        public const int NODE_PERM_PERM_Send_Back_USER = 99;
    }
    #endregion Constant

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            linkBtnTrigger(_divMenuBtnToDivCreate);
            linkBtnTrigger(btnInsertPermPerm);

            linkBtnTrigger(_divMenuBtnToDivAddmore);
            linkBtnTrigger(btnInsertAddmore);

            ViewState["empIDX"] = Session["emp_idx"];
            getContactsList();
            setDefaultVsFilterIndexPermPerm();
            setDefaultVsFilterIndexPermTemp();
            actionSetDefault();



        }

        linkBtnTrigger(_divMenuBtnToDivCreate);
        linkBtnTrigger(btnInsertPermPerm);

        linkBtnTrigger(_divMenuBtnToDivAddmore);
        linkBtnTrigger(btnInsertAddmore);


        visibleWaitingApproveMenu();


    }

    #endregion Page Load

    #region

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }


    protected void SetDefaultAdd()
    {
        txtEmpNewEmpCode.Text = String.Empty;
        txtEmpNewEmpCode.Enabled = true;
        AddStartdate_add.Text = String.Empty;
        getSetPermPolToDDL(ddladonline);
        //vpn
        //actionSelectVpnCreate();

        txtsap_add.Text = String.Empty;
        ddlemail.SelectedValue = "0";
        txtemailad.Text = String.Empty;
        txtEmpNewFullNameTH.Text = String.Empty;
        txtEmpNewOrgNameTH.Text = String.Empty;
        txtEmpNewDeptNameTH.Text = String.Empty;
        txtEmpNewFullNameEN.Text = String.Empty;
        txtEmpNewSecNameTH.Text = String.Empty;
        txtEmpNewCostCenterNo.Text = String.Empty;
        txtEmpNewPosNameTH.Text = String.Empty;

    }

    protected void SetDefaultAddMore()
    {
        txtEmpNewEmpCode_Addmore.Text = String.Empty;
        txtEmpNewEmpCode_Addmore.Enabled = true;
        AddStartdate_more.Text = String.Empty;
        getSetPermPolToDDL(ddladonline_more);
        getSetPermPolIntoToolTip(lblshowad_more);
        txtsapmore.Text = String.Empty;
        ddlemailmore.SelectedValue = "0";
        txtemilmore.Text = String.Empty;
        txtEmpNewFullNameTH_Addmore.Text = String.Empty;
        txtEmpNewOrgNameTH_Addmore.Text = String.Empty;
        txtEmpNewDeptNameTH_Addmore.Text = String.Empty;
        txtEmpNewFullNameEN_Addmore.Text = String.Empty;
        txtEmpNewSecNameTH_Addmore.Text = String.Empty;
        txtEmpNewCostCenterNo_Addmore.Text = String.Empty;
        txtEmpNewPosNameTH_Addmore.Text = String.Empty;
        chkmoread.Checked = false;
        chkmoreusersap.Checked = false;
        chkmoreemail.Checked = false;
        //chkmorevpn.Checked = false;
        divmore_adonline.Visible = false;
        divmore_usersap.Visible = false;
        divmore_email.Visible = false;
        divmore_emaildetail.Visible = false;
        divemailad_more.Visible = false;
        divmore_vpn.Visible = false;

        //divmore_vpn.Visible = false;

    }

    protected void actionSetDefault()
    {
        actionIndexSetApprove();

        if (ViewState["return_code"].ToString() == "0")
        {
            actionIndexPermPerm();
        }
        else if (ViewState["return_code"].ToString() == "1")
        {
            activeMenu("waiting");
            changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
        }
        else if (ViewState["return_code"].ToString() == "2")
        {
            activeMenu("waiting");
            changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_TEMP);
        }
    }

    protected string formatDateTime(string dateIN)
    {
        return DateTime.Parse(dateIN).ToString("dd/MM/yyyy HH:mm");
    }
    #endregion

    #region Action
    protected void actionIndexPermPerm()
    {
        _divMenuLiToDivIndex.Attributes.Add("class", "active");
        divIndex.Visible = true;
        _divIndexPermPerm.Visible = true;
        ad_online objADOnline = new ad_online();
        dataADOnline.ad_online_action = new ad_online[1];
        objADOnline.on_table = Constants.KEY_GET_FROM_PERM_PERM;
        objADOnline.with_where = Constants.WITH_WHERE_NO;
        objADOnline.filter_u0_perm_perm_code_id = ViewState["filterIndexPermPermCodeId"].ToString();
        objADOnline.filter_u0_perm_perm_empcode_fullnameth_applicant = ViewState["filterIndexPermPermEmpCodeFullNameTHApplicant"].ToString();
        objADOnline.filter_u0_perm_perm_org = ViewState["filterIndexPermPermOrg"].ToString();
        objADOnline.filter_u0_perm_perm_dept = ViewState["filterIndexPermPermDept"].ToString();
        objADOnline.filter_u0_perm_perm_sec = ViewState["filterIndexPermPermSec"].ToString();
        objADOnline.filter_u0_perm_perm_pos = ViewState["filterIndexPermPermPos"].ToString();
        objADOnline.filter_u0_perm_perm_node = ViewState["filterIndexPermPermNode"].ToString();
        objADOnline.filter_u0_perm_perm_condition_date = ViewState["filterIndexPermPermConditionDate"].ToString();
        objADOnline.filter_u0_perm_perm_date_only = dateToDB(ViewState["filterIndexPermPermDateOnly"].ToString());
        objADOnline.filter_u0_perm_perm_date_from = dateToDB(ViewState["filterIndexPermPermDateFrom"].ToString());
        objADOnline.filter_u0_perm_perm_date_to = dateToDB(ViewState["filterIndexPermPermDateTo"].ToString());
        dataADOnline.ad_online_action[0] = objADOnline;
        dataADOnline = callServiceADOnline(_urlReadU0IndexPermPerm, dataADOnline);
        setGridData(gvIndexPermPerm, dataADOnline.ad_online_action);
    }

    protected void actionIndexPermPermDetail(int id)
    {
        data_adonline dataADOnlineU0 = new data_adonline();
        ad_online objADOnlineU0 = new ad_online();
        dataADOnlineU0.ad_online_action = new ad_online[1];
        objADOnlineU0.on_table = Constants.KEY_GET_FROM_PERM_PERM;
        objADOnlineU0.with_where = Constants.WITH_WHERE_YES;
        objADOnlineU0.u0_perm_perm_idx = id;
        dataADOnlineU0.ad_online_action[0] = objADOnlineU0;
        dataADOnlineU0 = callServiceADOnline(_urlReadU0IndexPermPerm, dataADOnlineU0);
        lblValueU0PermPermCode.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_code_id;
        lblValueU0PermPermIDX_Perm.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_idx.ToString();
        lblValueU0PermPermCreatorEmpCode.Text = dataADOnlineU0.ad_online_action[0].emp_code_creator;
        lblValueU0PermPermCreatorFullNameTH.Text = dataADOnlineU0.ad_online_action[0].emp_name_th_creator;
        lblValueU0PermPermCreatedAt.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_created_at;
        lblValueU0PermPermEmpCode.Text = dataADOnlineU0.ad_online_action[0].emp_code;
        lblValueU0PermPermEmpFullNameTH.Text = dataADOnlineU0.ad_online_action[0].emp_name_th;
        lblValueU0PermPermEmpFullNameEN.Text = dataADOnlineU0.ad_online_action[0].emp_name_en;
        lblValueU0PermPermOrgNameTH.Text = dataADOnlineU0.ad_online_action[0].org_name_th;
        if(dataADOnlineU0.ad_online_action[0].dept_name_th == null || dataADOnlineU0.ad_online_action[0].dept_name_th == "")
        {
            lblValueU0PermPermDeptNameTH.Text = "-";
        }
        else
        {
            lblValueU0PermPermDeptNameTH.Text = dataADOnlineU0.ad_online_action[0].dept_name_th;
        }
        
        lblValueU0PermPermSecNameTH.Text = dataADOnlineU0.ad_online_action[0].sec_name_th;
        lblValueU0PermPermPosNameTH.Text = dataADOnlineU0.ad_online_action[0].pos_name_th;
        lblValueU0PermPermCostCenter.Text = dataADOnlineU0.ad_online_action[0].costcenter_no.ToString();
        lblValueIndexU0PermPermad.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_ad;
        lblvpnidx.Text = dataADOnlineU0.ad_online_action[0].vpn_name;

        if (lblvpnidx.Text == "0" || lblvpnidx.Text == "" || lblvpnidx.Text == String.Empty)
        {
            GvShowVPNPerm.Visible = false;
        }
        else
        {
            GvShowVPNPerm.Visible = true;

        }

        if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_ad == String.Empty)
        {
            divIndexU0PermPermad.Visible = false;

        }
        else
        {
            divIndexU0PermPermad.Visible = true;
        }

        if (dataADOnlineU0.ad_online_action[0].status_to_1 == "เสร็จสมบูรณ์")
        {
            lblValueU0PermPermNode.Text = dataADOnlineU0.ad_online_action[0].status_to_1;
        }
        else
        {
            lblValueU0PermPermNode.Text = string.Format("{0} โดย {1}", dataADOnlineU0.ad_online_action[0].status_to_1, dataADOnlineU0.ad_online_action[0].status_to_2);
        }
        if (dataADOnlineU0.ad_online_action[0].m0_perm_pol_idx_ref != 0)
        {
            divIndexU0PermPermPol.Visible = true;
            lblValueIndexU0PermPermPol.Text = dataADOnlineU0.ad_online_action[0].perm_pol_name;
        }
        else
        {
            divIndexU0PermPermPol.Visible = false;
            lblValueIndexU0PermPermPol.Text = string.Empty;
        }
        if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_date_use != string.Empty)
        {
            divIndexU0PermPermDate.Visible = true;
            lblValueIndexU0PermPermDate.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_date_use;
        }
        else
        {
            divIndexU0PermPermDate.Visible = false;
            lblValueIndexU0PermPermDate.Text = string.Empty;
        }
        if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_sap != string.Empty)
        {
            divIndexU0PermPermSap.Visible = true;
            lblValueIndexU0PermPermSap.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_sap;
        }
        else
        {
            divIndexU0PermPermSap.Visible = false;
            lblValueIndexU0PermPermSap.Text = string.Empty;
        }
        if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_reason != string.Empty)
        {
            divIndexU0PermPermReason.Visible = true;
            lblValueIndexU0PermPermReason.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_reason;
        }
        else
        {
            divIndexU0PermPermReason.Visible = false;
            lblValueIndexU0PermPermReason.Text = string.Empty;
        }

        

        if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_center != string.Empty)
        {
            divIndexU0PermPermEmail.Visible = true;
            lblValueIndexU0PermPermEmailType.Text = "Email กลาง";
            lblValueIndexU0PermPermEmailText.Text = string.Format("({0})", dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_center);

            if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_type_mail == 1)
            {
                lblValueIndexU0PermPermTypeEmail.Text = "เมล์ใหม่";
            }
            else
            {
                lblValueIndexU0PermPermTypeEmail.Text = "เมล์เดิม";
            }

          

            ddlWaitingPermPermEmailType.AppendDataBoundItems = true;
            ddlWaitingPermPermEmailType.Items.Clear();
            ddlWaitingPermPermEmailType.Items.Add(new ListItem("กรุณาเลือกประเภทอีเมล์ ....", "0"));
            ddlWaitingPermPermEmailType.Items.Add(new ListItem("เมล์ใหม่", "1"));
            ddlWaitingPermPermEmailType.Items.Add(new ListItem("เมล์เดิม", "2"));
            ddlWaitingPermPermEmailType.SelectedValue = dataADOnlineU0.ad_online_action[0].u0_perm_perm_type_mail.ToString();
            divemail.Visible = false;
        }
        else if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_priv != string.Empty)
        {
            divIndexU0PermPermEmail.Visible = true;
            lblValueIndexU0PermPermEmailType.Text = "Email ส่วนตัว";
            lblValueIndexU0PermPermEmailText.Text = string.Format("({0})", dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_priv);

            if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_type_mail == 1)
            {
                lblValueIndexU0PermPermTypeEmail.Text = "เมล์ใหม่";
            }
            else
            {
                lblValueIndexU0PermPermTypeEmail.Text = "เมล์ทดแทน (" + dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_replace + ")";
            }
            ddlWaitingPermPermEmailType.AppendDataBoundItems = true;
            ddlWaitingPermPermEmailType.Items.Clear();
            ddlWaitingPermPermEmailType.Items.Add(new ListItem("กรุณาเลือกประเภทอีเมล์ ....", "0"));
            ddlWaitingPermPermEmailType.Items.Add(new ListItem("เมล์ใหม่", "1"));
            ddlWaitingPermPermEmailType.Items.Add(new ListItem("เมล์ทดแทน", "2"));

            ddlWaitingPermPermEmailType.SelectedValue = dataADOnlineU0.ad_online_action[0].u0_perm_perm_type_mail.ToString();

            if (ddlWaitingPermPermEmailType.SelectedValue == "2")
            {
                divemail.Visible = true;

                // txtemail.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_replace.ToString();
                divIndexU0PermPermEmail_Replace.Visible = true;
                lblValueIndexU0PermPermEmailReplace.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_replace.ToString();

            }
            else
            {
                divemail.Visible = false;
                divIndexU0PermPermEmail_Replace.Visible = false;
            }


        }
        else
        {
            divIndexU0PermPermEmail.Visible = false;
            lblValueIndexU0PermPermEmailType.Text = string.Empty;
            lblValueIndexU0PermPermEmailText.Text = string.Empty;

            
        }
        visibleWaitingPermPermCommentText("index", dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_head_emp, dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_director_emp, dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_director_hr, dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_head_it, dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_director_it, dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_it, dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_it_before);
        actionPermLog(rptPermPermLog, Constants.KEY_GET_FROM_PERM_PERM, id);
    }

    protected void actionIndexPermTemp()
    {
        _divMenuLiToDivIndex.Attributes.Add("class", "active");
        divIndex.Visible = true;
        ad_online objADOnline = new ad_online();
        dataADOnline.ad_online_action = new ad_online[1];
        objADOnline.on_table = Constants.KEY_GET_FROM_PERM_TEMP;
        objADOnline.with_where = Constants.WITH_WHERE_NO;
        objADOnline.filter_u0_perm_temp_code_id = ViewState["filterIndexPermTempCodeId"].ToString();
        objADOnline.filter_u0_perm_temp_creator = ViewState["filterIndexPermTempCreator"].ToString();
        objADOnline.filter_u0_perm_temp_comp_name = ViewState["filterIndexPermTempCompName"].ToString();
        objADOnline.filter_u1_perm_temp_contact = ViewState["filterIndexPermTempContact"].ToString();
        objADOnline.filter_u0_perm_temp_org = ViewState["filterIndexPermTempOrg"].ToString();
        objADOnline.filter_u0_perm_temp_dept = ViewState["filterIndexPermTempDept"].ToString();
        objADOnline.filter_u0_perm_temp_node = ViewState["filterIndexPermTempNode"].ToString();
        objADOnline.filter_u0_perm_temp_condition_date = ViewState["filterIndexPermTempConditionDate"].ToString();
        objADOnline.filter_u0_perm_temp_date_only = dateToDB(ViewState["filterIndexPermTempDateOnly"].ToString());
        objADOnline.filter_u0_perm_temp_date_from = dateToDB(ViewState["filterIndexPermTempDateFrom"].ToString());
        objADOnline.filter_u0_perm_temp_date_to = dateToDB(ViewState["filterIndexPermTempDateTo"].ToString());
        dataADOnline.ad_online_action[0] = objADOnline;


        dataADOnline = callServiceADOnline(_urlReadU0IndexPermTemp, dataADOnline);
        setGridData(gvIndexPermTemp, dataADOnline.ad_online_action);
    }

    protected void actionIndexPermTempDetail(int id)
    {
        data_adonline dataADOnlineU0 = new data_adonline();
        ad_online objADOnlineU0 = new ad_online();
        dataADOnlineU0.ad_online_action = new ad_online[1];
        objADOnlineU0.on_table = Constants.KEY_GET_FROM_PERM_TEMP;
        objADOnlineU0.with_where = Constants.WITH_WHERE_YES;
        objADOnlineU0.u0_perm_temp_idx = id;
        dataADOnlineU0.ad_online_action[0] = objADOnlineU0;
        dataADOnlineU0 = callServiceADOnline(_urlReadU0IndexPermTemp, dataADOnlineU0);
        lblValueU0PermTempCode.Text = dataADOnlineU0.ad_online_action[0].u0_perm_temp_code_id;
        lblValueU0PermTempIDX_Temp.Text = dataADOnlineU0.ad_online_action[0].u0_perm_temp_idx.ToString();

        lblValueU0PermTempCreatorName.Text = dataADOnlineU0.ad_online_action[0].emp_name_th;
        lblValueU0PermTempCreatorOrg.Text = dataADOnlineU0.ad_online_action[0].org_name_th;
        lblValueU0PermTempCreatorDept.Text = dataADOnlineU0.ad_online_action[0].dept_name_th;
        lblValueU0PermTempCreatorSec.Text = dataADOnlineU0.ad_online_action[0].sec_name_th;
        lblValueU0PermTempCreatorPos.Text = dataADOnlineU0.ad_online_action[0].pos_name_th;
        lblValueU0PermTempCreatedAt.Text = dataADOnlineU0.ad_online_action[0].u0_perm_temp_created_at;
        lblValueU0PermTempCompName.Text = dataADOnlineU0.ad_online_action[0].u0_perm_temp_comp_name;
        lblValueU0PermTempReason.Text = dataADOnlineU0.ad_online_action[0].u0_perm_temp_detail;
        lblvpnidxtemp.Text = dataADOnlineU0.ad_online_action[0].vpn_name;

        
        if (lblvpnidxtemp.Text == "0" || lblvpnidxtemp.Text == "" || lblvpnidxtemp.Text == String.Empty)
        {
            divshowvpntemp.Visible = false;
        }
        else
        {
            divshowvpntemp.Visible = true;

        }

        if (dataADOnlineU0.ad_online_action[0].status_to_1 == "เสร็จสมบูรณ์")
        {
            lblValueU0PermTempNode.Text = dataADOnlineU0.ad_online_action[0].status_to_1;
        }
        else
        {
            lblValueU0PermTempNode.Text = string.Format("{0} โดย {1}", dataADOnlineU0.ad_online_action[0].status_to_1, dataADOnlineU0.ad_online_action[0].status_to_2);
        }
        if (dataADOnlineU0.ad_online_action[0].u0_cm_head_it != string.Empty)
        {
            _divIndexDetailCmHeaderIT.Visible = true;
            lblValueU0PermTempCommentHeaderIT.Text = dataADOnlineU0.ad_online_action[0].u0_cm_head_it;
        }
        else
        {
            _divIndexDetailCmHeaderIT.Visible = false;
            lblValueU0PermTempCommentHeaderIT.Text = string.Empty;
        }
        data_adonline dataADOnlineU1 = new data_adonline();
        ad_online objADOnlineU1 = new ad_online();
        dataADOnlineU1.ad_online_action = new ad_online[1];
        objADOnlineU1.on_table = Constants.KEY_GET_FROM_PERM_TEMP;
        objADOnlineU1.u0_perm_temp_idx_ref = id;
        dataADOnlineU1.ad_online_action[0] = objADOnlineU1;
        dataADOnlineU1 = callServiceADOnline(_urlReadU1IndexDetailPermTemp, dataADOnlineU1);
        lblValueAmountContact.Text = dataADOnlineU1.ad_online_action.Length + " คน";
        bool viewMultipleDate = true;
        string dateFirstRow = string.Format("{0}{1}", dataADOnlineU1.ad_online_action[0].u1_perm_temp_start, dataADOnlineU1.ad_online_action[0].u1_perm_temp_end);
        string dateStart = dataADOnlineU1.ad_online_action[0].u1_perm_temp_start;
        string dateEnd = dataADOnlineU1.ad_online_action[0].u1_perm_temp_end;
        foreach (ad_online value in dataADOnlineU1.ad_online_action)
        {
            if (string.Format("{0}{1}", value.u1_perm_temp_start.ToString(), value.u1_perm_temp_end.ToString()) != dateFirstRow)
            {
                viewMultipleDate = !viewMultipleDate;
                break;
            }
        }
        if (!viewMultipleDate)
        {
            rptU1PermTempContactsList.Visible = true;
            rptU1PermTempContactsListCompareDate.Visible = false;
            groupDate.Visible = false;
            setRepeaterData(rptU1PermTempContactsList, dataADOnlineU1.ad_online_action);
        }
        else
        {
            rptU1PermTempContactsList.Visible = false;
            rptU1PermTempContactsListCompareDate.Visible = true;
            groupDate.Visible = true;
            lblValueU0DateStart.Text = dateStart;
            lblValueU0DateEnd.Text = dateEnd;
            setRepeaterData(rptU1PermTempContactsListCompareDate, dataADOnlineU1.ad_online_action);
        }
        actionPermLog(rptPermTempLog, Constants.KEY_GET_FROM_PERM_TEMP, id);
    }

    protected void actionCreatePermPerm()
    {
        


        if (txtEmpNewEmpIdxHidden.Text != string.Empty && txtEmpNewEmpCode.Text != string.Empty)
        {
            getEmpProfile(int.Parse(txtEmpNewEmpIdxHidden.Text));
            string permPermOrg = string.Empty;
            int nodeId = 0;
            if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_TKN_IDX)
            {
                permPermOrg = Constants.PREFIX_CODE_ID_ORG_TKN;
            }
            else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_TKNL_IDX)
            {
                permPermOrg = Constants.PREFIX_CODE_ID_ORG_TKNL;
            }
            else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_TOBI_IDX)
            {
                permPermOrg = Constants.PREFIX_CODE_ID_ORG_TOBI;
            }
            else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_GENC_IDX)
            {
                permPermOrg = Constants.PREFIX_CODE_ID_ORG_GENC;
            }
            else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_NCP_IDX)
            {
                permPermOrg = Constants.PREFIX_CODE_ID_ORG_NCP;
            }
            else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_GCI_IDX)
            {
                permPermOrg = Constants.PREFIX_CODE_ID_ORG_GCI;
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีสิทธิ์สร้างรายการ');", true);
                return;
            }


            data_adonline dataADOnlineU0 = new data_adonline();
            ad_online objADOnlineU0_insert = new ad_online();
            dataADOnlineU0.ad_online_action = new ad_online[1];
            objADOnlineU0_insert.on_table = Constants.KEY_GET_FROM_PERM_PERM;
            if (Array.IndexOf(Constants.JOBLEVELS_OFFICIAL, int.Parse(txtEmpNewJobGradeLevelHidden.Text)) > -1)
            {
                nodeId = Constants.NODE_PERM_PERM_CREATE_GOTO_HEADER_EMPNEW;
            }
            else if (Array.IndexOf(Constants.JOBLEVELS_HEADER, int.Parse(txtEmpNewJobGradeLevelHidden.Text)) > -1)
            {
                nodeId = Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_EMPNEW;
            }
            else if (Array.IndexOf(Constants.JOBLEVELS_DIRECTOR, int.Parse(txtEmpNewJobGradeLevelHidden.Text)) > -1)
            {
                nodeId = Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_HR;
            }
            objADOnlineU0_insert.u0_node_idx_ref = nodeId;
            objADOnlineU0_insert.perm_perm_alphabet = Constants.PREFIX_ALPHABET_PERM_PERM;
            objADOnlineU0_insert.perm_perm_org = permPermOrg;
            objADOnlineU0_insert.m0_perm_pol_idx_ref = 0;
            objADOnlineU0_insert.u0_perm_perm_emp_idx_ref = int.Parse(txtEmpNewEmpIdxHidden.Text);
            objADOnlineU0_insert.u0_perm_perm_sap = string.Empty;
            objADOnlineU0_insert.u0_perm_perm_mail_center = string.Empty;
            objADOnlineU0_insert.u0_perm_perm_mail_priv = string.Empty;
            objADOnlineU0_insert.u0_perm_perm_ad = string.Empty;
            objADOnlineU0_insert.u0_perm_perm_cm_head_emp = string.Empty;
            objADOnlineU0_insert.u0_perm_perm_cm_director_emp = string.Empty;
            objADOnlineU0_insert.u0_perm_perm_cm_head_it = string.Empty;
            objADOnlineU0_insert.u0_perm_perm_cm_director_it = string.Empty;
            objADOnlineU0_insert.u0_perm_perm_cm_it = string.Empty;
            objADOnlineU0_insert.u0_perm_perm_created_by = int.Parse(ViewState["empIDX"].ToString());
            objADOnlineU0_insert.u0_perm_perm_updated_by = int.Parse(ViewState["empIDX"].ToString());
            objADOnlineU0_insert.u0_perm_perm_mail_replace = ddl1or3.SelectedValue;
            objADOnlineU0_insert.u0_perm_perm_date_use = AddStartdate_add.Text;
            objADOnlineU0_insert.m0_perm_pol_idx_ref = int.Parse(ddladonline.SelectedValue);
            objADOnlineU0_insert.u0_perm_perm_sap = txtsap_add.Text;
            objADOnlineU0_insert.u0_perm_perm_ad = txtemailad.Text;
            objADOnlineU0_insert.u0_perm_perm_type_mail = int.Parse(ddlemail.SelectedValue);
            objADOnlineU0_insert.u0_perm_perm_reason = txtremark.Text;
           

            if (rdomail.SelectedValue == "center")
            {
                if (ddlemail.SelectedValue == "1")
                {
                    objADOnlineU0_insert.u0_perm_perm_mail_center = txtemailad.Text + ddldomain.SelectedValue;
                }
                else
                {
                    objADOnlineU0_insert.u0_perm_perm_mail_center = ddl1or3.SelectedValue;
                }
            }
            else if (rdomail.SelectedValue == "private")
            {
                objADOnlineU0_insert.u0_perm_perm_mail_priv = txtemailad.Text + ddldomain.SelectedValue;
            }
            dataADOnlineU0.ad_online_action[0] = objADOnlineU0_insert;

            //-- insert u0 --//
            if (rdomail.SelectedValue == "private" && ddlemail.SelectedValue == "2")
            {
                if (ddl1or3.SelectedValue != "")
                {
                    //litDebug.Text = "yes1";

                    dataADOnlineU0 = callServiceADOnline(_urlCreateU0PermPerm, dataADOnlineU0);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบอีเมลให้เรียบร้อยก่อน');", true);
                    return;
                }

            }
            else
            {
                //litDebug.Text = "yes";
                dataADOnlineU0 = callServiceADOnline(_urlCreateU0PermPerm, dataADOnlineU0);
            }
            ////dataADOnlineU0 = callServiceADOnline(_urlCreateU0PermPerm, dataADOnlineU0);

            //-- insert u0 --//

            //-- insert u2 use vpn --//
            data_adonline dataADOnlineU2 = new data_adonline();
            dataADOnlineU2.ad_online_u2vpn_action = new ad_u2vpn_online[1];

            foreach (GridViewRow gvUseVPN in GvUseVPN.Rows)
            {

                if (gvUseVPN.RowType == DataControlRowType.DataRow)
                {
                    //linkBtnTrigger(btnSaveCarUseHr);
                    //GridView GvExpenses = (GridView)gvDetailCaruse.FindControl("GvExpenses");
                    Label lbl_vpnidx_insert = (Label)gvUseVPN.FindControl("lbl_vpnidx");
                    CheckBox chxtype_vpn_insert = (CheckBox)gvUseVPN.FindControl("chxtype_vpn");
                    CheckBoxList chklevel_vpn_insert = (CheckBoxList)gvUseVPN.FindControl("chklevel_vpn");
                    TextBox txt_remark_insert = (TextBox)gvUseVPN.FindControl("txt_remark");

                    //FileUpload UploadFile_Vpn = (FileUpload)gvUseVPN.FindControl("UploadFile_Vpn");
                    //UpdatePanel Update_PnFileUpload = gvDetailCaruse.FindControl("Update_PnFileUpload") as UpdatePanel;


                    // u2_document
                    //data_carbooking data_u2_document_hr_addcar = new data_carbooking();
                    int count_u2 = 0;
                    var _u2_document = new ad_u2vpn_online[GvUseVPN.Rows.Count];
                    var _u3_document = new ad_u3vpn_online[chklevel_vpn_insert.Items.Count];

                    if (chxtype_vpn_insert.Checked == true)
                    {

                        _u2_document[count_u2] = new ad_u2vpn_online();
                        _u2_document[count_u2].vpnidx = int.Parse(lbl_vpnidx_insert.Text);

                        _u2_document[count_u2].remark = txt_remark_insert.Text;
                        _u2_document[count_u2].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                        _u2_document[count_u2].u0_perm_temp_idx = dataADOnlineU0.return_code; //idx form insert
                        _u2_document[count_u2].cemp_idx = int.Parse(ViewState["empIDX"].ToString());


                        int count_u3 = 0;

                        foreach (ListItem chk_count_insert in chklevel_vpn_insert.Items)
                        {
                            if (chk_count_insert.Selected)
                            {
                                _u3_document[count_u3] = new ad_u3vpn_online();
                                _u3_document[count_u3].u0_perm_temp_idx = dataADOnlineU0.return_code;
                                if (chxtype_vpn_insert.Checked == true)
                                {
                                    _u3_document[count_u3].vpnidx = int.Parse(lbl_vpnidx_insert.Text);
                                }
                                _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                                _u3_document[count_u3].vpn1idx = int.Parse(chk_count_insert.Value);
                                _u3_document[count_u3].cemp_idx = int.Parse(ViewState["empIDX"].ToString());

                                count_u3++;
                            }
                        }


                    }

                    count_u2++;

                    // u2_document

                    dataADOnlineU2.ad_online_u2vpn_action = _u2_document;
                    dataADOnlineU2.ad_online_u3vpn_action = _u3_document;
                    //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnlineU2));
                    dataADOnlineU2 = callServiceADOnline(_urlInsertU2VPN, dataADOnlineU2);
                }

            }

            //-- insert u2 use vpn --//

            //hasfile save file

            FileUpload UploadFile_vpn = (FileUpload)UpdatePanel4.FindControl("UploadFile_vpn");

            int check_file = 0;
            if (UploadFile_vpn != null)
            {

                if (UploadFile_vpn.HasFile)
                {

                    string filepath = Server.MapPath(_uploadPERM);
                    HttpFileCollection uploadedFiles = Request.Files;


                    for (int i = 0; i < uploadedFiles.Count; i++)
                    {
                        HttpPostedFile userPostedFile = uploadedFiles[i];

                        try
                        {
                            if (userPostedFile.ContentLength > 0)
                            {
                                string filePath12 = Constants.KEY_GET_FROM_PERM_PERM + "-" + dataADOnlineU0.return_code.ToString();
                                string filePath1 = Server.MapPath(_uploadPERM + filePath12);
                                string _filepathExtension = Path.GetExtension(userPostedFile.FileName);

                                if (!Directory.Exists(filePath1))
                                {
                                    Directory.CreateDirectory(filePath1);
                                }


                                //litDebug.Text += "<u>File #" + (i + 1) + "</u><br>";
                                //litDebug.Text += "File Content Type: " + userPostedFile.ContentType + "<br>";
                                //litDebug.Text += "File Size: " + userPostedFile.ContentLength + "kb<br>";
                                //litDebug.Text += "File Name: " + userPostedFile.FileName + "<br>";

                                //litDebug.Text += (filepath + filePath12 + "\\" + "1209" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                userPostedFile.SaveAs(filepath + filePath12 + "\\" + dataADOnlineU0.return_code.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                //litDebug.Text += "Location where saved: " + filepath + "\\" + Path.GetFileName(userPostedFile.FileName) + "<p>";
                            }
                        }
                        catch (Exception Ex)
                        {
                            //litDebug.Text += "Error: <br>" + Ex.Message;
                        }
                    }


                }
                else
                {
                    if (int.Parse(ddladonline.SelectedValue) == 1 || int.Parse(ddladonline.SelectedValue) == 2 || int.Parse(ddladonline.SelectedValue) == 3)
                    {
                        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('-- กรุณาแนบไฟล์ ก่อนทำการบันทึก --');", true);
                        _funcTool.showAlert(this, "-- กรุณาแนบไฟล์ ก่อนทำการบันทึก --");
                        return;
                    }
                }

            }
            else
            {
                if (int.Parse(ddladonline.SelectedValue) == 1 || int.Parse(ddladonline.SelectedValue) == 2 || int.Parse(ddladonline.SelectedValue) == 3)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('-- กรุณาแนบไฟล์ ก่อนทำการบันทึก --');", true);
                    return;
                }
            }

            //hasfile save file

            actionCreatePermLog(Constants.KEY_GET_FROM_PERM_PERM, dataADOnlineU0.return_code, nodeId, int.Parse(ViewState["empIDX"].ToString()));

            //sent email
            string bodyU0 = string.Format("{0},{1},{2},{3},{4},{5}",
                txtEmpNewEmpCode.Text.Trim(),
                txtEmpNewFullNameTHHidden.Text.Trim(),
                txtEmpNewOrgNameTHHidden.Text.Trim(),
                txtEmpNewDeptNameTHHidden.Text.Trim(),
                txtEmpNewSecNameTHHidden.Text.Trim(),
                txtEmpNewPosNameTHHidden.Text.Trim());
            string emails = getEmails(nodeId, int.Parse(txtEmpNewEmpIdxHidden.Text), Constants.KEY_GET_FROM_PERM_PERM);
            sentEmail(emails, Constants.KEY_GET_FROM_PERM_PERM, nodeId, bodyU0);
            activeMenu("index");
            changeMenuView("index", Constants.KEY_GET_FROM_PERM_PERM);
            actionIndexPermPerm();
            scrollToTop();
            //Page.Response.Redirect(Page.Request.Url.ToString(), true);


        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลพนักงานดังกล่าว กรุณาติดต่อฝ่ายบุคคล');", true);
            return;
        }
    }

    protected void actionCreatePermPerm_More()
    {

        if (txtEmpNewEmpIdxHidden_Addmore.Text != string.Empty && txtEmpNewEmpCode_Addmore.Text != string.Empty)
        {
            getEmpProfile(int.Parse(txtEmpNewEmpIdxHidden_Addmore.Text));
            string permPermOrg = string.Empty;
            int nodeId = 0;
            if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_TKN_IDX)
            {
                permPermOrg = Constants.PREFIX_CODE_ID_ORG_TKN;
            }
            else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_TKNL_IDX)
            {
                permPermOrg = Constants.PREFIX_CODE_ID_ORG_TKNL;
            }
            else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_TOBI_IDX)
            {
                permPermOrg = Constants.PREFIX_CODE_ID_ORG_TOBI;
            }
            else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_GENC_IDX)
            {
                permPermOrg = Constants.PREFIX_CODE_ID_ORG_GENC;
            }
            else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_NCP_IDX)
            {
                permPermOrg = Constants.PREFIX_CODE_ID_ORG_NCP;
            }
            else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_GCI_IDX)
            {
                permPermOrg = Constants.PREFIX_CODE_ID_ORG_GCI;
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีสิทธิ์สร้างรายการ');", true);
                return;
            }
            data_adonline dataADOnlineU0 = new data_adonline();
            ad_online objADOnlineU0 = new ad_online();
            dataADOnlineU0.ad_online_action = new ad_online[1];
            objADOnlineU0.on_table = Constants.KEY_GET_FROM_PERM_PERM;
            if (Array.IndexOf(Constants.JOBLEVELS_OFFICIAL, int.Parse(txtEmpNewJobGradeLevelHidden_Addmore.Text)) > -1)
            {
                nodeId = Constants.NODE_PERM_PERM_CREATE_GOTO_HEADER_EMPNEW;
            }
            else if (Array.IndexOf(Constants.JOBLEVELS_HEADER, int.Parse(txtEmpNewJobGradeLevelHidden_Addmore.Text)) > -1)
            {
                nodeId = Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_EMPNEW;
            }
            else if (Array.IndexOf(Constants.JOBLEVELS_DIRECTOR, int.Parse(txtEmpNewJobGradeLevelHidden_Addmore.Text)) > -1)
            {
                nodeId = Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_HR;
            }
            objADOnlineU0.u0_node_idx_ref = nodeId;
            objADOnlineU0.perm_perm_alphabet = Constants.PREFIX_ALPHABET_PERM_PERM;
            objADOnlineU0.perm_perm_org = permPermOrg;
            objADOnlineU0.m0_perm_pol_idx_ref = 0;
            objADOnlineU0.u0_perm_perm_emp_idx_ref = int.Parse(txtEmpNewEmpIdxHidden_Addmore.Text);
            objADOnlineU0.u0_perm_perm_sap = string.Empty;
            objADOnlineU0.u0_perm_perm_mail_center = string.Empty;
            objADOnlineU0.u0_perm_perm_mail_priv = string.Empty;
            objADOnlineU0.u0_perm_perm_ad = string.Empty;
            objADOnlineU0.u0_perm_perm_cm_head_emp = string.Empty;
            objADOnlineU0.u0_perm_perm_cm_director_emp = string.Empty;
            objADOnlineU0.u0_perm_perm_cm_head_it = string.Empty;
            objADOnlineU0.u0_perm_perm_cm_director_it = string.Empty;
            objADOnlineU0.u0_perm_perm_cm_it = string.Empty;
            objADOnlineU0.u0_perm_perm_created_by = int.Parse(ViewState["empIDX"].ToString());
            objADOnlineU0.u0_perm_perm_updated_by = int.Parse(ViewState["empIDX"].ToString());
            objADOnlineU0.u0_perm_perm_mail_replace = ddlemail1or3_more.SelectedValue;
            objADOnlineU0.u0_perm_perm_date_use = AddStartdate_more.Text;
            objADOnlineU0.m0_perm_pol_idx_ref = int.Parse(ddladonline_more.SelectedValue);
            objADOnlineU0.u0_perm_perm_reason = txtreason_more.Text;
            objADOnlineU0.u0_perm_perm_sap = txtsapmore.Text;
            objADOnlineU0.u0_perm_perm_ad = txtemilmore.Text;
            objADOnlineU0.u0_perm_perm_type_mail = int.Parse(ddlemailmore.SelectedValue);
            
            if (rdomail_more.SelectedValue == "1")
            {
                if (ddlemail.SelectedValue == "1")
                {
                    objADOnlineU0.u0_perm_perm_mail_center = txtemilmore.Text + ddldomainmore.SelectedValue;
                }
                else
                {
                    objADOnlineU0.u0_perm_perm_mail_center = ddlemail1or3_more.SelectedValue;
                }
            }
            else if (rdomail_more.SelectedValue == "2")
            {
                objADOnlineU0.u0_perm_perm_mail_priv = txtemilmore.Text + ddldomainmore.SelectedValue;
            }

            dataADOnlineU0.ad_online_action[0] = objADOnlineU0;

            //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnlineU0));
            dataADOnlineU0 = callServiceADOnline(_urlCreateU0PermPerm, dataADOnlineU0);

            //-- insert u2 use vpn --//
            data_adonline dataADOnlineU2 = new data_adonline();
            dataADOnlineU2.ad_online_u2vpn_action = new ad_u2vpn_online[1];

            foreach (GridViewRow GvAddmoreVPN in GvAddmore.Rows)
            {

                if (GvAddmoreVPN.RowType == DataControlRowType.DataRow)
                {
                    //linkBtnTrigger(btnSaveCarUseHr);
                    //GridView GvExpenses = (GridView)gvDetailCaruse.FindControl("GvExpenses");
                    Literal litvpnidx_addmore = (Literal)GvAddmoreVPN.FindControl("litvpnidx_addmore");
                    CheckBox chkvpn_addmore = (CheckBox)GvAddmoreVPN.FindControl("chkvpn_addmore");
                    CheckBoxList chksubvpn_addmore = (CheckBoxList)GvAddmoreVPN.FindControl("chksubvpn_addmore");
                    TextBox txtremark_addmore = (TextBox)GvAddmoreVPN.FindControl("txtremark_addmore");

                    //FileUpload UploadFile_Vpn = (FileUpload)gvUseVPN.FindControl("UploadFile_Vpn");
                    //UpdatePanel Update_PnFileUpload = gvDetailCaruse.FindControl("Update_PnFileUpload") as UpdatePanel;


                    // u2_document
                    //data_carbooking data_u2_document_hr_addcar = new data_carbooking();
                    int count_u2 = 0;
                    var _u2_document = new ad_u2vpn_online[GvAddmore.Rows.Count];
                    var _u3_document = new ad_u3vpn_online[chksubvpn_addmore.Items.Count];

                    if (chkvpn_addmore.Checked == true)
                    {

                        _u2_document[count_u2] = new ad_u2vpn_online();
                        _u2_document[count_u2].vpnidx = int.Parse(litvpnidx_addmore.Text);

                        _u2_document[count_u2].remark = txtremark_addmore.Text;
                        _u2_document[count_u2].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                        _u2_document[count_u2].u0_perm_temp_idx = dataADOnlineU0.return_code; //idx form insert
                        _u2_document[count_u2].cemp_idx = int.Parse(ViewState["empIDX"].ToString());


                        int count_u3 = 0;

                        foreach (ListItem chk_count_insert in chksubvpn_addmore.Items)
                        {
                            if (chk_count_insert.Selected)
                            {
                                _u3_document[count_u3] = new ad_u3vpn_online();
                                _u3_document[count_u3].u0_perm_temp_idx = dataADOnlineU0.return_code;
                                if (chkvpn_addmore.Checked == true)
                                {
                                    _u3_document[count_u3].vpnidx = int.Parse(litvpnidx_addmore.Text);
                                }
                                _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                                _u3_document[count_u3].vpn1idx = int.Parse(chk_count_insert.Value);
                                _u3_document[count_u3].cemp_idx = int.Parse(ViewState["empIDX"].ToString());

                                count_u3++;
                            }
                        }


                    }

                    count_u2++;

                    // u2_document

                    dataADOnlineU2.ad_online_u2vpn_action = _u2_document;
                    dataADOnlineU2.ad_online_u3vpn_action = _u3_document;
                    //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnlineU2));
                    dataADOnlineU2 = callServiceADOnline(_urlInsertU2VPN, dataADOnlineU2);
                }

            }

            //-- insert u2 use vpn --//

            //hasfile save file

            FileUpload UploadFile_vpn = (FileUpload)div_Addmore.FindControl("UploadFile_vpnaddmore");
            if (UploadFile_vpn != null)
            {
                //litDebug.Text = "file";
                if (UploadFile_vpn.HasFile)
                {
                    //litDebug.Text = "2";
                    string filepath = Server.MapPath(_uploadPERM);
                    HttpFileCollection uploadedFiles = Request.Files;


                    for (int i = 0; i < uploadedFiles.Count; i++)
                    {
                        HttpPostedFile userPostedFile = uploadedFiles[i];

                        try
                        {
                            if (userPostedFile.ContentLength > 0)
                            {
                                string filePath12 = Constants.KEY_GET_FROM_PERM_PERM + "-" + dataADOnlineU0.return_code.ToString();
                                string filePath1 = Server.MapPath(_uploadPERM + filePath12);
                                string _filepathExtension = Path.GetExtension(userPostedFile.FileName);

                                if (!Directory.Exists(filePath1))
                                {
                                    Directory.CreateDirectory(filePath1);
                                }


                                //litDebug.Text += "<u>File #" + (i + 1) + "</u><br>";
                                //litDebug.Text += "File Content Type: " + userPostedFile.ContentType + "<br>";
                                //litDebug.Text += "File Size: " + userPostedFile.ContentLength + "kb<br>";
                                //litDebug.Text += "File Name: " + userPostedFile.FileName + "<br>";

                                //litDebug.Text += (filepath + filePath12 + "\\" + "1209" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                userPostedFile.SaveAs(filepath + filePath12 + "\\" + dataADOnlineU0.return_code.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                //litDebug.Text += "Location where saved: " + filepath + "\\" + Path.GetFileName(userPostedFile.FileName) + "<p>";
                            }
                        }
                        catch (Exception Ex)
                        {
                            //litDebug.Text += "Error: <br>" + Ex.Message;
                        }
                    }


                }
                else
                {
                    ////litDebug.Text = "3";
                    //if (int.Parse(ddladonline_more.SelectedValue) == 1 || int.Parse(ddladonline_more.SelectedValue) == 2 || int.Parse(ddladonline_more.SelectedValue) == 3)
                    //{
                    //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('-- กรุณาแนบไฟล์ ก่อนทำการบันทึก --');", true);
                    //    return;
                    //}
                }

            }
            else
            {
                ////litDebug.Text = "not file";
                //if (int.Parse(ddladonline_more.SelectedValue) == 1 || int.Parse(ddladonline_more.SelectedValue) == 2 || int.Parse(ddladonline_more.SelectedValue) == 3)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('-- กรุณาแนบไฟล์ ก่อนทำการบันทึก --');", true);
                //    return;
                //}
            }

            //hasfile save file


            actionCreatePermLog(Constants.KEY_GET_FROM_PERM_PERM, dataADOnlineU0.return_code, nodeId, int.Parse(ViewState["empIDX"].ToString()));

            string bodyU0 = string.Format("{0},{1},{2},{3},{4},{5}",
               txtEmpNewEmpCode_Addmore.Text.Trim(),
               txtEmpNewFullNameTHHidden_Addmore.Text.Trim(),
               txtEmpNewOrgNameTHHidden_Addmore.Text.Trim(),
               txtEmpNewDeptNameTHHidden_Addmore.Text.Trim(),
               txtEmpNewSecNameTHHidden_Addmore.Text.Trim(),
               txtEmpNewPosNameTHHidden_Addmore.Text.Trim());
            string emails = getEmails(nodeId, int.Parse(txtEmpNewEmpIdxHidden_Addmore.Text), Constants.KEY_GET_FROM_PERM_PERM);
            sentEmail(emails, Constants.KEY_GET_FROM_PERM_PERM, nodeId, bodyU0);
            activeMenu("index");
            changeMenuView("index", Constants.KEY_GET_FROM_PERM_PERM);
            actionIndexPermPerm();
            scrollToTop();
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลพนักงานดังกล่าว กรุณาติดต่อฝ่ายบุคคล');", true);
            return;
        }
    }

    protected void actionCreatePermTemp()
    {
        getEmpProfile(int.Parse(ViewState["empIDX"].ToString()));
        string permTempOrg = string.Empty;
        if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_TKN_IDX)
        {
            permTempOrg = Constants.PREFIX_CODE_ID_ORG_TKN;
        }
        else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_TKNL_IDX)
        {
            permTempOrg = Constants.PREFIX_CODE_ID_ORG_TKNL;
        }
        else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_TOBI_IDX)
        {
            permTempOrg = Constants.PREFIX_CODE_ID_ORG_TOBI;
        }
        else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_GENC_IDX)
        {
            permTempOrg = Constants.PREFIX_CODE_ID_ORG_GENC;
        }
        else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_NCP_IDX)
        {
            permTempOrg = Constants.PREFIX_CODE_ID_ORG_NCP;
        }
        else if (dataADOnline.ad_online_action[0].org_idx == Constants.ORG_GCI_IDX)
        {
            permTempOrg = Constants.PREFIX_CODE_ID_ORG_GCI;
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีสิทธิ์สร้างรายการ');", true);
            return;
        }
        data_adonline dataADOnlineU0 = new data_adonline();
        ad_online objADOnlineU0 = new ad_online();
        dataADOnlineU0.ad_online_action = new ad_online[1];
        objADOnlineU0.on_table = Constants.KEY_GET_FROM_PERM_TEMP;
        objADOnlineU0.m0_emp_type_idx_ref = Constants.RADIO_EMP_VISITOR;
        objADOnlineU0.u0_node_idx_ref = Constants.NODE_PERM_TEMP_CREATE_GOTO_DIRECTOR_CREATOR;
        objADOnlineU0.perm_temp_alphabet = Constants.PREFIX_ALPHABET_PERM_TEMP;
        objADOnlineU0.perm_temp_org = permTempOrg;
        objADOnlineU0.u0_perm_temp_comp_name = txtEmpVisitorCompany.Text.Trim();
        objADOnlineU0.u0_perm_temp_detail = txtEmpVisitorReason.Text.Trim();
        objADOnlineU0.u0_cm_director_creator = string.Empty;
        objADOnlineU0.u0_cm_head_it = string.Empty;
        objADOnlineU0.u0_cm_director_it = string.Empty;
        objADOnlineU0.u0_perm_temp_created_by = int.Parse(ViewState["empIDX"].ToString());
        objADOnlineU0.u0_perm_temp_updated_by = int.Parse(ViewState["empIDX"].ToString());
        dataADOnlineU0.ad_online_action[0] = objADOnlineU0;
        dataADOnlineU0 = callServiceADOnline(_urlCreateU0PermTemp, dataADOnlineU0);


        //-- insert u2 use vpn --//
        data_adonline dataADOnlineU2 = new data_adonline();
        dataADOnlineU2.ad_online_u2vpn_action = new ad_u2vpn_online[1];

        foreach (GridViewRow gvUseVPNTemporary in GvUseVPNTemporary.Rows)
        {

            if (gvUseVPNTemporary.RowType == DataControlRowType.DataRow)
            {
                //linkBtnTrigger(btnSaveCarUseHr);
                //GridView GvExpenses = (GridView)gvDetailCaruse.FindControl("GvExpenses");
                Label lbl_vpnidx_temporary_insert = (Label)gvUseVPNTemporary.FindControl("lbl_vpnidx_temporary");
                CheckBox chxtype_vpn_temporary_insert = (CheckBox)gvUseVPNTemporary.FindControl("chxtype_vpn_temporary");
                CheckBoxList chklevel_vpn_temporary_insert = (CheckBoxList)gvUseVPNTemporary.FindControl("chklevel_vpn_temporary");
                TextBox txt_remark_temporary_insert = (TextBox)gvUseVPNTemporary.FindControl("txt_remark_temporary");

                //FileUpload UploadFile_Vpntemporary = (FileUpload)gvUseVPNTemporary.FindControl("UploadFile_Vpntemporary");
                //UpdatePanel Update_PnFileUpload = gvDetailCaruse.FindControl("Update_PnFileUpload") as UpdatePanel;


                // u2_document
                //data_carbooking data_u2_document_hr_addcar = new data_carbooking();
                int count_u2 = 0;
                var _u2_document = new ad_u2vpn_online[GvUseVPNTemporary.Rows.Count];
                var _u3_document = new ad_u3vpn_online[chklevel_vpn_temporary_insert.Items.Count];
                if (chxtype_vpn_temporary_insert.Checked == true)
                {

                    _u2_document[count_u2] = new ad_u2vpn_online();
                    _u2_document[count_u2].vpnidx = int.Parse(lbl_vpnidx_temporary_insert.Text);
                    _u2_document[count_u2].remark = txt_remark_temporary_insert.Text;
                    _u2_document[count_u2].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                    _u2_document[count_u2].u0_perm_temp_idx = dataADOnlineU0.return_code; //idx form insert
                    _u2_document[count_u2].cemp_idx = int.Parse(ViewState["empIDX"].ToString());

                    int count_u3 = 0;

                    foreach (ListItem chk_count_insert in chklevel_vpn_temporary_insert.Items)
                    {
                        if (chk_count_insert.Selected)
                        {
                            _u3_document[count_u3] = new ad_u3vpn_online();
                            _u3_document[count_u3].u0_perm_temp_idx = dataADOnlineU0.return_code;
                            if (chxtype_vpn_temporary_insert.Checked == true)
                            {
                                _u3_document[count_u3].vpnidx = int.Parse(lbl_vpnidx_temporary_insert.Text);
                            }
                            _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                            _u3_document[count_u3].vpn1idx = int.Parse(chk_count_insert.Value);
                            _u3_document[count_u3].cemp_idx = int.Parse(ViewState["empIDX"].ToString());

                            count_u3++;
                        }
                    }

                }


                count_u2++;


                // u2_document

                dataADOnlineU2.ad_online_u2vpn_action = _u2_document;
                dataADOnlineU2.ad_online_u3vpn_action = _u3_document;
                //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnlineU2));
                dataADOnlineU2 = callServiceADOnline(_urlInsertU2VPN, dataADOnlineU2);


            }

        }
        //-- insert u2 use vpn --//

        //hasfile save file

        FileUpload UploadFile_vpnTemporary = (FileUpload)UpdatePanel4.FindControl("UploadFile_vpnTemporary");

        int check_file = 0;
        if (UploadFile_vpnTemporary != null)
        {

            if (UploadFile_vpnTemporary.HasFile)
            {

                string filepath = Server.MapPath(_uploadTEMP);
                HttpFileCollection uploadedFiles = Request.Files;


                for (int i = 0; i < uploadedFiles.Count; i++)
                {
                    HttpPostedFile userPostedFile = uploadedFiles[i];

                    try
                    {
                        if (userPostedFile.ContentLength > 0)
                        {
                            string filePath12 = Constants.KEY_GET_FROM_PERM_TEMP + "-" + dataADOnlineU0.return_code.ToString();
                            string filePath1 = Server.MapPath(_uploadTEMP + filePath12);
                            string _filepathExtension = Path.GetExtension(userPostedFile.FileName);

                            if (!Directory.Exists(filePath1))
                            {
                                Directory.CreateDirectory(filePath1);
                            }


                            //litDebug.Text += "<u>File #" + (i + 1) + "</u><br>";
                            //litDebug.Text += "File Content Type: " + userPostedFile.ContentType + "<br>";
                            //litDebug.Text += "File Size: " + userPostedFile.ContentLength + "kb<br>";
                            //litDebug.Text += "File Name: " + userPostedFile.FileName + "<br>";

                            //litDebug.Text += (filepath + filePath12 + "\\" + "1205" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                            userPostedFile.SaveAs(filepath + filePath12 + "\\" + dataADOnlineU0.return_code.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                            //litDebug.Text += "Location where saved: " + filepath + "\\" + Path.GetFileName(userPostedFile.FileName) + "<p>";
                        }
                    }
                    catch (Exception Ex)
                    {
                        //litDebug.Text += "Error: <br>" + Ex.Message;
                    }
                }


            }


        }
        //hasfile save file


        DataSet dsVsContactPermTempList = (DataSet)ViewState["vsContactPermTempList"];
        data_adonline dataADOnlineU1 = new data_adonline();
        dataADOnlineU1.on_table_u1 = Constants.KEY_GET_FROM_PERM_TEMP;
        string contactNameForSentMail = string.Empty;
        int itemContact = 0;
        int itemObj = 0;
        ad_online_perm_temp_insert_u1[] objADOnlineU1 = new ad_online_perm_temp_insert_u1[dsVsContactPermTempList.Tables["dsContactPermTempTable"].Rows.Count];
        foreach (DataRow drVsContactPermTempList in dsVsContactPermTempList.Tables["dsContactPermTempTable"].Rows)
        {
            contactNameForSentMail += string.Format("<tr><td>{0}. {1}</td></tr>", ++itemContact, drVsContactPermTempList["drContactPermTempFullNameTH"].ToString());
            objADOnlineU1[itemObj] = new ad_online_perm_temp_insert_u1();
            objADOnlineU1[itemObj].u0_perm_temp_idx_ref = dataADOnlineU0.return_code;
            
            objADOnlineU1[itemObj].u1_perm_temp_idcard = idCardPattern(drVsContactPermTempList["drContactPermTempIDCard"].ToString(), false);
            objADOnlineU1[itemObj].u1_perm_temp_fullname = drVsContactPermTempList["drContactPermTempFullNameTH"].ToString();
            objADOnlineU1[itemObj].u1_perm_temp_start = string.Format("{0} {1}", dateToDB(drVsContactPermTempList["drContactPermTempStart"].ToString()), DateTime.Now.ToString("HH:mm:ss"));
            objADOnlineU1[itemObj].u1_perm_temp_end = string.Format("{0} {1}", dateToDB(drVsContactPermTempList["drContactPermTempEnd"].ToString()), DateTime.Now.ToString("HH:mm:ss"));
            objADOnlineU1[itemObj].u1_perm_temp_username = string.Empty;
            objADOnlineU1[itemObj].u1_perm_temp_password = string.Empty;
            objADOnlineU1[itemObj].u1_perm_temp_updated_by = int.Parse(ViewState["empIDX"].ToString());
            itemObj++;
        }
        dataADOnlineU1.ad_online_perm_temp_insert_u1_action = objADOnlineU1;
        callServiceADOnline(_urlCreateU1PermTemp, dataADOnlineU1);


        actionCreatePermLog(Constants.KEY_GET_FROM_PERM_TEMP, dataADOnlineU0.return_code, Constants.NODE_PERM_TEMP_CREATE_GOTO_DIRECTOR_CREATOR, int.Parse(ViewState["empIDX"].ToString()));
        string bodyU0 = string.Format("{0},{1},{2},{3}", txtCreatorFullName.Text.Trim(), txtCreatorOrgNameTH.Text, txtCreatorDeptNameTH.Text, txtEmpVisitorCompany.Text.Trim());
        string bodyU1 = string.Format("{0},{1}", itemContact, contactNameForSentMail);
        string emails = getEmails(Constants.NODE_PERM_TEMP_CREATE_GOTO_DIRECTOR_CREATOR, int.Parse(ViewState["empIDX"].ToString()));
        sentEmail(emails, Constants.KEY_GET_FROM_PERM_TEMP, Constants.NODE_PERM_TEMP_CREATE_GOTO_DIRECTOR_CREATOR, bodyU0, bodyU1);
        activeMenu("index");
        changeMenuView("index", Constants.KEY_GET_FROM_PERM_TEMP);
        actionIndexPermTemp();
        scrollToTop();
    }

    protected void actionWaitingPermPerm()
    {
        _divMenuLiToDivWaiting.Attributes.Add("class", "active");
        divWaiting.Visible = true;

        data_adonline dataADOnline1 = new data_adonline();
        ad_online objADOnline = new ad_online();
        dataADOnline1.ad_online_action = new ad_online[1];
        objADOnline.on_table = Constants.KEY_GET_FROM_PERM_PERM;
        objADOnline.emp_idx = int.Parse(ViewState["empIDX"].ToString());
        dataADOnline1.ad_online_action[0] = objADOnline;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline1));

        dataADOnline1 = callServiceADOnline(_urlReadU0WaitingPermPerm, dataADOnline1);
        setGridData(gvWaitingPermPerm, dataADOnline1.ad_online_action);
    }

    protected void actionWaitingPermPermDetail(int id)
    {
        rdoWaitingPermPermEmailType.ClearSelection();
        divnewmail.Visible = false;
        divemail_edit.Visible = false;

        data_adonline dataADOnlineU0 = new data_adonline();
        ad_online objADOnlineU0 = new ad_online();
        dataADOnlineU0.ad_online_action = new ad_online[1];
        objADOnlineU0.on_table = Constants.KEY_GET_FROM_PERM_PERM;
        objADOnlineU0.with_where = Constants.WITH_WHERE_YES;
        objADOnlineU0.u0_perm_perm_idx = id;
        dataADOnlineU0.ad_online_action[0] = objADOnlineU0;
        dataADOnlineU0 = callServiceADOnline(_urlReadU0WaitingDetailPermPerm, dataADOnlineU0);
        lblValueU0PermPermEmpIdxCreator.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_created_by.ToString();
        lblValueU0PermPermJobGradeLevel.Text = dataADOnlineU0.ad_online_action[0].jobgrade_idx.ToString();
        lblValueU0PermPermEmpIdx.Text = dataADOnlineU0.ad_online_action[0].emp_idx.ToString();
        lblValueU0PermPermRSecIDX_EmpIdx.Text = dataADOnlineU0.ad_online_action[0].rsec_idx.ToString();
        lblValueU0PermPermIdx.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_idx.ToString();
        lblValueU0PermPermFnameEN.Text = dataADOnlineU0.ad_online_action[0].emp_firstname_en;
        lblValueU0PermPermLnameEN.Text = dataADOnlineU0.ad_online_action[0].emp_lastname_en;
        lblValueU0PermPermNodeCurrent.Text = dataADOnlineU0.ad_online_action[0].u0_node_idx_ref.ToString();
        lblValueWaitingPermPermCode.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_code_id;
        lblValueWaitingPermPermCreatorEmpCode.Text = dataADOnlineU0.ad_online_action[0].emp_code_creator;
        lblValueWaitingPermPermCreatorFullNameTH.Text = dataADOnlineU0.ad_online_action[0].emp_name_th_creator;
        lblValueWaitingPermPermCreatedAt.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_created_at;
        lblValueWaitingPermPermEmpCode.Text = dataADOnlineU0.ad_online_action[0].emp_code;
        lblValueWaitingPermPermEmpNameTH.Text = dataADOnlineU0.ad_online_action[0].emp_name_th;
        lblValueWaitingPermPermEmpNameEN.Text = dataADOnlineU0.ad_online_action[0].emp_name_en;
        lblValueWaitingPermPermOrgNameTH.Text = dataADOnlineU0.ad_online_action[0].org_name_th;

        if(dataADOnlineU0.ad_online_action[0].dept_name_th == null || dataADOnlineU0.ad_online_action[0].dept_name_th == "")
        {
            lblValueWaitingPermPermDeptNameTH.Text = "-";
        }
        else
        {
            lblValueWaitingPermPermDeptNameTH.Text = dataADOnlineU0.ad_online_action[0].dept_name_th;
        }
        


        lblValueWaitingPermPermSecNameTH.Text = dataADOnlineU0.ad_online_action[0].sec_name_th;
        lblValueWaitingPermPermPosNameTH.Text = dataADOnlineU0.ad_online_action[0].pos_name_th;
        lblValueWaitingPermPermCostCenter.Text = dataADOnlineU0.ad_online_action[0].costcenter_no.ToString();
        AddStartdate.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_date_use.ToString();
        txtWaitingPermPermHeaderITCommentText.Text = string.Empty;
        txtWaitingPermPermDirectorITCommentText.Text = string.Empty;
        txtWaitingPermPermITCommentText.Text = string.Empty;
        ViewState["CostNo"] = lblValueWaitingPermPermCostCenter.Text;
        txtWaitingPermPermITAD.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_ad;
        lblvpnidxedit.Text = dataADOnlineU0.ad_online_action[0].vpn_name;


        if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_ad == String.Empty)
        {
            divWaitingPermPermad.Visible = false;

        }
        else
        {
            divWaitingPermPermad.Visible = true;
            lblValueWaitingPermPermad.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_ad;
        }

        if (dataADOnlineU0.ad_online_action[0].status_to_1 == "เสร็จสมบูรณ์")
        {
            lblValueWaitingPermPermNode.Text = dataADOnlineU0.ad_online_action[0].status_to_1;
        }
        else
        {
            lblValueWaitingPermPermNode.Text = string.Format("{0} โดย {1}", dataADOnlineU0.ad_online_action[0].status_to_1, dataADOnlineU0.ad_online_action[0].status_to_2);
        }
        visibleWaitingPermCommentBox(Constants.KEY_GET_FROM_PERM_PERM, dataADOnlineU0.ad_online_action[0].u0_node_idx_ref);
        visibleWaitingPermPermPanelHeading(lblTitleWaitingPermPermApproverAndConfig, dataADOnlineU0.ad_online_action[0].u0_node_idx_ref);
        if (dataADOnlineU0.ad_online_action[0].m0_perm_pol_idx_ref != 0)
        {
            divWaitingPermPermDetailPermPol.Visible = true;
            lblValueWaitingPermPermPol.Text = dataADOnlineU0.ad_online_action[0].perm_pol_name;
            ddlWaitingPermPermPol.SelectedValue = dataADOnlineU0.ad_online_action[0].m0_perm_pol_idx_ref.ToString();
        }
        else
        {
            divWaitingPermPermDetailPermPol.Visible = false;
            lblValueWaitingPermPermPol.Text = string.Empty;
            ddlWaitingPermPermPol.SelectedValue = "0";
        }
        if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_sap != string.Empty)
        {
            divWaitingPermPermDetailSap.Visible = true;
            lblValueWaitingPermPermSap.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_sap;
            txtWaitingPermPermSap.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_sap;
        }
        else
        {
            divWaitingPermPermDetailSap.Visible = false;
            lblValueWaitingPermPermSap.Text = string.Empty;
            txtWaitingPermPermSap.Text = string.Empty;
        }

        if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_reason != string.Empty)
        {
            divWaitingPermPermDetailReason.Visible = true;
            lblValueWaitingPermReason.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_reason;
        }
        else
        {
            divWaitingPermPermDetailReason.Visible = false;
            lblValueWaitingPermReason.Text = string.Empty;
        }

        if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_date_use != string.Empty)
        {
            divWaitingPermPermDetailDate.Visible = true;
            lblValueWaitingPermPermDate.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_date_use;
        }
        else
        {
            divWaitingPermPermDetailDate.Visible = false;
            lblValueWaitingPermPermDate.Text = string.Empty;
        }

        if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_center != string.Empty)
        {
            divWaitingPermPermDetailEmail.Visible = true;
            lblValueWaitingPermPermEmailType.Text = "Email กลาง";
            ViewState["TypeMail"] = 1;
            lblValueWaitingPermPermEmailText.Text = string.Format("{0}", dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_center);
            rdoWaitingPermPermEmailType.SelectedValue = "center";
            string[] splitEmail = dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_center.Split('@');
            txtWaitingPermPermEmailText.Text = splitEmail[0];
            ddlWaitingPermPermEmailDomain.SelectedValue = "@" + splitEmail[1];
            divemail_edit.Visible = true;

            actionSelectGooglelicense(ddl1or3_edit, "center", int.Parse(dataADOnlineU0.ad_online_action[0].u0_perm_perm_type_mail.ToString()));
            ddl1or3_edit.SelectedValue = dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_replace.ToString();

            ViewState["u0_perm_perm_type_mail"] = dataADOnlineU0.ad_online_action[0].u0_perm_perm_type_mail.ToString();
            if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_type_mail == 1)
            {
                lblValueWaitingPermPermTypeEmail.Text = "เมล์ใหม่";
                divnewmail.Visible = true;
                divemail.Visible = false;
            }
            else
            {
                lblValueWaitingPermPermTypeEmail.Text = "เมล์เดิม";
                divemail.Visible = true;
                divnewmail.Visible = false;
            }

            ddlWaitingPermPermEmailType.AppendDataBoundItems = true;
            ddlWaitingPermPermEmailType.Items.Clear();
            ddlWaitingPermPermEmailType.Items.Add(new ListItem("กรุณาเลือกประเภทอีเมล์ ....", "0"));
            ddlWaitingPermPermEmailType.Items.Add(new ListItem("เมล์ใหม่", "1"));
            ddlWaitingPermPermEmailType.Items.Add(new ListItem("เมล์เดิม", "2"));
            ddlWaitingPermPermEmailType.SelectedValue = dataADOnlineU0.ad_online_action[0].u0_perm_perm_type_mail.ToString();


        }
        else if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_priv != string.Empty)
        {
            divemail_edit.Visible = true;
            divWaitingPermPermDetailEmail.Visible = true;
            lblValueWaitingPermPermEmailType.Text = "Email ส่วนตัว";
            ViewState["TypeMail"] = 2;
            lblValueWaitingPermPermEmailText.Text = string.Format("{0}", dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_priv);
            rdoWaitingPermPermEmailType.SelectedValue = "private";
            string[] splitEmail = dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_priv.Split('@');
            txtWaitingPermPermEmailText.Text = splitEmail[0];
            ddlWaitingPermPermEmailDomain.SelectedValue = "@" + splitEmail[1];

            actionSelectGooglelicense(ddl1or3_edit, "private", int.Parse(dataADOnlineU0.ad_online_action[0].u0_perm_perm_type_mail.ToString()));
            ddl1or3_edit.SelectedValue = dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_replace.ToString();
            divnewmail.Visible = true;
            ViewState["u0_perm_perm_type_mail"] = dataADOnlineU0.ad_online_action[0].u0_perm_perm_type_mail.ToString();
            if (dataADOnlineU0.ad_online_action[0].u0_perm_perm_type_mail == 1)
            {
                lblValueWaitingPermPermTypeEmail.Text = "เมล์ใหม่";

            }
            else
            {
                ViewState["Email_Replace"] = dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_replace.ToString();
                lblValueWaitingPermPermTypeEmail.Text = "เมล์ทดแทน (" + dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_replace + ")";
            }


            ddlWaitingPermPermEmailType.AppendDataBoundItems = true;
            ddlWaitingPermPermEmailType.Items.Clear();
            ddlWaitingPermPermEmailType.Items.Add(new ListItem("กรุณาเลือกประเภทอีเมล์ ....", "0"));
            ddlWaitingPermPermEmailType.Items.Add(new ListItem("เมล์ใหม่", "1"));
            ddlWaitingPermPermEmailType.Items.Add(new ListItem("เมล์ทดแทน", "2"));

            ddlWaitingPermPermEmailType.SelectedValue = dataADOnlineU0.ad_online_action[0].u0_perm_perm_type_mail.ToString();


            if (ddlWaitingPermPermEmailType.SelectedValue == "2")
            {
                divemail.Visible = true;
                // txtemail.Text = dataADOnlineU0.ad_online_action[0].u0_perm_perm_mail_replace.ToString();
                ViewState["txtemail"] = ddl1or3_edit.SelectedValue;//txtemail.Text;

            }
            else
            {
                divemail.Visible = false;
            }

        }
        else
        {
            divnewmail.Visible = false;
            divemail_edit.Visible = false;
            divWaitingPermPermDetailEmail.Visible = false;
            lblValueWaitingPermPermEmailType.Text = string.Empty;
            lblValueWaitingPermPermEmailText.Text = string.Empty;
            txtWaitingPermPermEmailText.Text = string.Empty;
        }
        visibleWaitingPermPermCommentText("waiting", dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_head_emp, dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_director_emp, dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_director_hr, dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_head_it, dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_director_it, dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_it, dataADOnlineU0.ad_online_action[0].u0_perm_perm_cm_it_before);
    }

    protected void actionWaitingPermPermApprove(int permPermId, int nodeIdCurrent, int nodeIdNext, string commentText)
    {
        
        ad_online objADOnline = new ad_online();
        dataADOnline.ad_online_action = new ad_online[1];
        objADOnline.on_table = Constants.KEY_GET_FROM_PERM_PERM;
        objADOnline.u0_perm_perm_idx = permPermId;
        objADOnline.u0_node_idx_ref_current = nodeIdCurrent;
        objADOnline.u0_node_idx_ref = nodeIdNext;

        if (nodeIdCurrent == Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_HR && nodeIdNext == Constants.node_perm_perm_director_empnew_app_goto_it)
        {
            objADOnline.m0_perm_pol_idx_ref = int.Parse(ddlWaitingPermPermPol.SelectedValue);
            objADOnline.u0_perm_perm_sap = txtWaitingPermPermSap.Text.Trim();

            if (rdoWaitingPermPermEmailType.SelectedValue == "center")
            {
                objADOnline.u0_perm_perm_mail_center = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;
                objADOnline.u0_perm_perm_mail_priv = string.Empty;
            }
            else if (rdoWaitingPermPermEmailType.SelectedValue == "private")
            {
                objADOnline.u0_perm_perm_mail_center = string.Empty;
                objADOnline.u0_perm_perm_mail_priv = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;
            }
            objADOnline.u0_perm_perm_cm_director_hr = commentText == string.Empty ? "-" : commentText;
        }
        else if (nodeIdCurrent == Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_HR && nodeIdNext == Constants.node_perm_perm_director_empnew_app_goto_it)
        {
            objADOnline.m0_perm_pol_idx_ref = int.Parse(ddlWaitingPermPermPol.SelectedValue);
            objADOnline.u0_perm_perm_sap = txtWaitingPermPermSap.Text.Trim();
            if (rdoWaitingPermPermEmailType.SelectedValue == "center")
            {
                objADOnline.u0_perm_perm_mail_center = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;
                objADOnline.u0_perm_perm_mail_priv = string.Empty;
            }
            else if (rdoWaitingPermPermEmailType.SelectedValue == "private")
            {
                objADOnline.u0_perm_perm_mail_center = string.Empty;
                objADOnline.u0_perm_perm_mail_priv = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;
            }
            objADOnline.u0_perm_perm_cm_director_hr = commentText == string.Empty ? "-" : commentText;
        }
        else if (nodeIdNext == Constants.NODE_PERM_PERM_DIRECTOR_HR_NOTAPP_ENDBY_DIRECTOR_HR)
        {
            objADOnline.m0_perm_pol_idx_ref = 0;
            objADOnline.u0_perm_perm_sap = string.Empty;
            objADOnline.u0_perm_perm_mail_center = string.Empty;
            objADOnline.u0_perm_perm_mail_priv = string.Empty;
            objADOnline.u0_perm_perm_cm_director_hr = commentText;

        }
        else if (nodeIdCurrent == Constants.NODE_PERM_PERM_CREATE_GOTO_HEADER_EMPNEW && nodeIdNext == Constants.NODE_PERM_PERM_HEADER_EMPNEW_APP_GOTO_DIRECTOR_EMPNEW)
        {
            objADOnline.m0_perm_pol_idx_ref = int.Parse(ddlWaitingPermPermPol.SelectedValue);
            objADOnline.u0_perm_perm_sap = txtWaitingPermPermSap.Text.Trim();

            if (rdoWaitingPermPermEmailType.SelectedValue == "center")
            {
                if (ddlWaitingPermPermEmailType.SelectedValue == "1")
                {
                    objADOnline.u0_perm_perm_mail_center = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;
                }
                else
                {
                    objADOnline.u0_perm_perm_mail_center = ddl1or3_edit.SelectedValue;

                }

                objADOnline.u0_perm_perm_mail_replace = string.Empty;
                objADOnline.u0_perm_perm_mail_priv = string.Empty;
            }
            else if (rdoWaitingPermPermEmailType.SelectedValue == "private")
            {
                objADOnline.u0_perm_perm_mail_center = string.Empty;

                objADOnline.u0_perm_perm_mail_priv = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;
                objADOnline.u0_perm_perm_mail_replace = ddl1or3_edit.SelectedValue;//txtemail.Text;
            }
            objADOnline.u0_perm_perm_cm_head_emp = commentText == string.Empty ? "-" : commentText;
        }
        else if (nodeIdCurrent == Constants.NODE_PERM_PERM_CREATE_GOTO_HEADER_EMPNEW && nodeIdNext == Constants.node_perm_perm_director_empnew_app_goto_it)
        {
            objADOnline.m0_perm_pol_idx_ref = int.Parse(ddlWaitingPermPermPol.SelectedValue);
            objADOnline.u0_perm_perm_sap = txtWaitingPermPermSap.Text.Trim();
            if (rdoWaitingPermPermEmailType.SelectedValue == "center")
            {
                if (ddlWaitingPermPermEmailType.SelectedValue == "1")
                {
                    objADOnline.u0_perm_perm_mail_center = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;

                }
                else
                {
                    objADOnline.u0_perm_perm_mail_center = ddl1or3_edit.SelectedValue;
                }
            }
            else if (rdoWaitingPermPermEmailType.SelectedValue == "private")
            {
                objADOnline.u0_perm_perm_mail_center = string.Empty;
                objADOnline.u0_perm_perm_mail_priv = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;

                objADOnline.u0_perm_perm_mail_replace = ddl1or3_edit.SelectedValue; //txtemail.Text;
            }
            objADOnline.u0_perm_perm_cm_head_emp = commentText == string.Empty ? "-" : commentText;
        }
        else if (nodeIdNext == Constants.NODE_PERM_PERM_HEADER_EMPNEW_NOTAPP_ENDBY_HEADER_EMPNEW)
        {
            objADOnline.m0_perm_pol_idx_ref = 0;
            objADOnline.u0_perm_perm_sap = string.Empty;
            objADOnline.u0_perm_perm_mail_center = string.Empty;
            objADOnline.u0_perm_perm_mail_priv = string.Empty;
            objADOnline.u0_perm_perm_cm_head_emp = commentText;
            objADOnline.u0_perm_perm_mail_replace = ddl1or3_edit.SelectedValue; //txtemail.Text;
        }
        else if (nodeIdCurrent == Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_EMPNEW && nodeIdNext == Constants.node_perm_perm_director_empnew_app_goto_it)
        {
            objADOnline.m0_perm_pol_idx_ref = int.Parse(ddlWaitingPermPermPol.SelectedValue);
            objADOnline.u0_perm_perm_sap = txtWaitingPermPermSap.Text.Trim();
            if (rdoWaitingPermPermEmailType.SelectedValue == "center")
            {
                if (ddlWaitingPermPermEmailType.SelectedValue == "1")
                {

                    objADOnline.u0_perm_perm_mail_center = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;

                }
                else
                {
                    objADOnline.u0_perm_perm_mail_center = ddl1or3_edit.SelectedValue;
                }
            }
            else if (rdoWaitingPermPermEmailType.SelectedValue == "private")
            {
                objADOnline.u0_perm_perm_mail_center = string.Empty;
                objADOnline.u0_perm_perm_mail_priv = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;
            }
            objADOnline.u0_perm_perm_cm_director_emp = commentText == string.Empty ? "-" : commentText;
        }
        else if (nodeIdCurrent == Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_EMPNEW && nodeIdNext == Constants.NODE_PERM_PERM_DIRECTOR_EMPNEW_NOTAPP_ENDBY_DIRECTOR_EMPNEW)
        {
            objADOnline.m0_perm_pol_idx_ref = 0;
            objADOnline.u0_perm_perm_sap = string.Empty;
            objADOnline.u0_perm_perm_mail_center = string.Empty;
            objADOnline.u0_perm_perm_mail_priv = string.Empty;
            objADOnline.u0_perm_perm_cm_director_emp = commentText;
        }
        else if (nodeIdNext == Constants.node_perm_perm_header_empnew_sendedit_user || nodeIdNext == Constants.node_perm_perm_director_empnew_sendedit_user ||
            nodeIdNext == Constants.node_perm_perm_director_hr_sendedit_user || nodeIdNext == Constants.node_perm_perm_it_sendedit_user ||
            nodeIdNext == Constants.node_perm_perm_headerit_sendedit_user || nodeIdNext == Constants.node_perm_perm_directorit_sendedit_user)
        {
            objADOnline.m0_perm_pol_idx_ref = int.Parse(ddlWaitingPermPermPol.SelectedValue);
            objADOnline.u0_perm_perm_sap = txtWaitingPermPermSap.Text.Trim();

            if (rdoWaitingPermPermEmailType.SelectedValue == "center")
            {
                if (ddlWaitingPermPermEmailType.SelectedValue == "1")
                {
                    objADOnline.u0_perm_perm_mail_center = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;

                }
                else
                {
                    objADOnline.u0_perm_perm_mail_center = ddl1or3_edit.SelectedValue;
                }
            }
            else if (rdoWaitingPermPermEmailType.SelectedValue == "private")
            {
                objADOnline.u0_perm_perm_mail_center = string.Empty;
                objADOnline.u0_perm_perm_mail_priv = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;

                objADOnline.u0_perm_perm_mail_replace = ddl1or3_edit.SelectedValue; //txtemail.Text;
            }
            objADOnline.u0_perm_perm_cm_head_emp = commentText == string.Empty ? "-" : commentText;
        }
        else if ((nodeIdCurrent == Constants.NODE_PERM_PERM_HEADER_EMPNEW_APP_GOTO_DIRECTOR_EMPNEW && nodeIdNext == Constants.node_perm_perm_director_empnew_app_goto_it) || nodeIdNext == Constants.NODE_PERM_PERM_DIRECTOR_EMPNEW_NOTAPP_ENDBY_DIRECTOR_EMPNEW)
        {
            objADOnline.u0_perm_perm_cm_director_emp = commentText == string.Empty ? "-" : commentText;
        }
        else if (nodeIdNext == Constants.NODE_PERM_PERM_HEADER_IT_APP_GOTO_DIRECTOR_IT || nodeIdNext == Constants.NODE_PERM_PERM_HEADER_IT_NOTAPP_ENDBY_HEADER_IT)
        {

            objADOnline.m0_perm_pol_idx_ref = int.Parse(ddlWaitingPermPermPol.SelectedValue);
            objADOnline.u0_perm_perm_sap = txtWaitingPermPermSap.Text.Trim();

            if (rdoWaitingPermPermEmailType.SelectedValue == "center")
            {
                if (ddlWaitingPermPermEmailType.SelectedValue == "1")
                {
                    objADOnline.u0_perm_perm_mail_center = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;

                }
                else
                {
                    objADOnline.u0_perm_perm_mail_center = ddl1or3_edit.SelectedValue;
                }
            }
            else if (rdoWaitingPermPermEmailType.SelectedValue == "private")
            {
                objADOnline.u0_perm_perm_mail_center = string.Empty;
                objADOnline.u0_perm_perm_mail_priv = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;

                objADOnline.u0_perm_perm_mail_replace = ddl1or3_edit.SelectedValue; //txtemail.Text;
            }
            objADOnline.u0_perm_perm_cm_head_it = commentText == string.Empty ? "-" : commentText;
        }
        else if (nodeIdNext == Constants.NODE_PERM_PERM_DIRECTOR_IT_APP_GOTO_IT ||
            nodeIdNext == Constants.node_perm_perm_header_empnew_app_goto_it ||
            nodeIdNext == Constants.node_perm_perm_director_empnew_app_goto_it ||
            nodeIdNext == Constants.NODE_PERM_PERM_DIRECTOR_IT_NOTAPP_ENDBY_DIRECTOR_IT)
        {
            objADOnline.m0_perm_pol_idx_ref = int.Parse(ddlWaitingPermPermPol.SelectedValue);
            objADOnline.u0_perm_perm_sap = txtWaitingPermPermSap.Text.Trim();
            if (rdoWaitingPermPermEmailType.SelectedValue == "center")
            {
                if (ddlWaitingPermPermEmailType.SelectedValue == "1")
                {
                    objADOnline.u0_perm_perm_mail_center = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;

                }
                else
                {
                    objADOnline.u0_perm_perm_mail_center = ddl1or3_edit.SelectedValue;
                }
            }
            else if (rdoWaitingPermPermEmailType.SelectedValue == "private")
            {
                objADOnline.u0_perm_perm_mail_center = string.Empty;
                objADOnline.u0_perm_perm_mail_priv = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;

                objADOnline.u0_perm_perm_mail_replace = ddl1or3_edit.SelectedValue; //txtemail.Text;
            }
            objADOnline.u0_perm_perm_cm_director_it = commentText == string.Empty ? "-" : commentText;

        }
        else if (nodeIdNext == Constants.NODE_PERM_PERM_IT_APP_FINISHBY_IT || nodeIdNext == Constants.node_perm_perm_it_app_goto_headit)
        {
            objADOnline.m0_perm_pol_idx_ref = int.Parse(ddlWaitingPermPermPol.SelectedValue);
            objADOnline.u0_perm_perm_sap = txtWaitingPermPermSap.Text.Trim();

            if (nodeIdNext == Constants.node_perm_perm_it_app_goto_headit)
            {
                objADOnline.u0_perm_perm_cm_it_before = txtWaitingPermPermITCommentText.Text;
                objADOnline.u0_perm_perm_ad = txtWaitingPermPermITAD.Text.Trim();

            }

            if (rdoWaitingPermPermEmailType.SelectedValue == "center")
            {
                if (ddlWaitingPermPermEmailType.SelectedValue == "1")
                {
                    objADOnline.u0_perm_perm_mail_center = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;


                }
                else
                {
                    objADOnline.u0_perm_perm_mail_center = ddl1or3_edit.SelectedValue;
                }
            }
            else if (rdoWaitingPermPermEmailType.SelectedValue == "private")
            {
                objADOnline.u0_perm_perm_mail_center = string.Empty;
                objADOnline.u0_perm_perm_mail_priv = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;
            }
            objADOnline.u0_perm_perm_cm_it = commentText == string.Empty ? "-" : commentText;
        }
        else if (nodeIdNext == Constants.NODE_PERM_PERM_IT_NOTAPP_ENDBY_IT || nodeIdNext == Constants.node_perm_perm_it_notapp_endbyprocess_it)
        {
            objADOnline.u0_perm_perm_cm_it = commentText;
            objADOnline.u0_perm_perm_cm_it_before = txtWaitingPermPermITCommentText.Text;
        }
        objADOnline.u0_perm_perm_ad = txtWaitingPermPermITAD.Text.Trim();
        objADOnline.u0_perm_perm_mail_replace = ddl1or3_edit.SelectedValue; //txtemail.Text;
       
        objADOnline.u0_perm_perm_date_use = AddStartdate.Text;
        objADOnline.u0_perm_perm_type_mail = int.Parse(ddlWaitingPermPermEmailType.SelectedValue);
        objADOnline.u0_perm_perm_updated_by = int.Parse(ViewState["empIDX"].ToString());
        dataADOnline.ad_online_action[0] = objADOnline;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline));
        callServiceADOnline(_urlApproveU0WaitingPermPerm, dataADOnline);

        //-- insert u2 use vpn --//
        data_adonline dataADOnlineU2 = new data_adonline();
        dataADOnlineU2.ad_online_u2vpn_action = new ad_u2vpn_online[1];

        foreach (GridViewRow GvEditVPN_ in GvEditVPN.Rows)
        {

            if (GvEditVPN_.RowType == DataControlRowType.DataRow)
            {
                //linkBtnTrigger(btnSaveCarUseHr);   objADOnline.u0_perm_perm_idx = permPermId;
                //GridView GvExpenses = (GridView)gvDetailCaruse.FindControl("GvExpenses");
                Literal litvpnidxedit = (Literal)GvEditVPN_.FindControl("litvpnidxedit");
                CheckBox chkvpnedit = (CheckBox)GvEditVPN_.FindControl("chkvpnedit");
                CheckBoxList chksubvpnedit = (CheckBoxList)GvEditVPN_.FindControl("chksubvpnedit");
                TextBox txtremarkedit = (TextBox)GvEditVPN_.FindControl("txtremarkedit");

                //FileUpload UploadFile_Vpn = (FileUpload)gvUseVPN.FindControl("UploadFile_Vpn");
                //UpdatePanel Update_PnFileUpload = gvDetailCaruse.FindControl("Update_PnFileUpload") as UpdatePanel;


                // u2_document
                //data_carbooking data_u2_document_hr_addcar = new data_carbooking();
                int count_u2 = 0;
                var _u2_document = new ad_u2vpn_online[GvEditVPN.Rows.Count];
                var _u3_document = new ad_u3vpn_online[chksubvpnedit.Items.Count];

                if (chkvpnedit.Checked == true)
                {

                    _u2_document[count_u2] = new ad_u2vpn_online();
                    _u2_document[count_u2].vpnidx = int.Parse(litvpnidxedit.Text);

                    _u2_document[count_u2].remark = txtremarkedit.Text;
                    _u2_document[count_u2].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                    _u2_document[count_u2].u0_perm_temp_idx = permPermId; //idx form insert
                    _u2_document[count_u2].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                    _u2_document[count_u2].u2_perm_perm_status = 1;


                    int count_u3 = 0;

                    foreach (ListItem chk_count_update in chksubvpnedit.Items)
                    {
                        if (chk_count_update.Selected)
                        {
                            _u3_document[count_u3] = new ad_u3vpn_online();
                            _u3_document[count_u3].u0_perm_temp_idx = permPermId;
                            if (chkvpnedit.Checked == true)
                            {
                                _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedit.Text);
                            }
                            _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                            _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                            _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                            _u3_document[count_u3].u3_perm_perm_status = 1;

                            count_u3++;
                        }
                        else
                        {
                            _u3_document[count_u3] = new ad_u3vpn_online();
                            _u3_document[count_u3].u0_perm_temp_idx = permPermId;
                            if (chkvpnedit.Checked == true)
                            {
                                _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedit.Text);
                            }
                            _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                            _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                            _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                            _u3_document[count_u3].u3_perm_perm_status = 9;

                            count_u3++;
                        }
                    }


                }
                else
                {
                    _u2_document[count_u2] = new ad_u2vpn_online();
                    _u2_document[count_u2].vpnidx = int.Parse(litvpnidxedit.Text);

                    _u2_document[count_u2].remark = txtremarkedit.Text;
                    _u2_document[count_u2].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                    _u2_document[count_u2].u0_perm_temp_idx = permPermId; //idx form insert
                    _u2_document[count_u2].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                    _u2_document[count_u2].u2_perm_perm_status = 9;


                    int count_u3 = 0;

                    foreach (ListItem chk_count_update in chksubvpnedit.Items)
                    {
                        if (chk_count_update.Selected)
                        {
                            _u3_document[count_u3] = new ad_u3vpn_online();
                            _u3_document[count_u3].u0_perm_temp_idx = permPermId;
                            if (chkvpnedit.Checked == true)
                            {
                                _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedit.Text);
                            }
                            _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                            _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                            _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                            _u3_document[count_u3].u3_perm_perm_status = 1;

                            count_u3++;
                        }
                        else
                        {
                            _u3_document[count_u3] = new ad_u3vpn_online();
                            _u3_document[count_u3].u0_perm_temp_idx = permPermId;
                            if (chkvpnedit.Checked == true)
                            {
                                _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedit.Text);
                            }
                            _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                            _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                            _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                            _u3_document[count_u3].u3_perm_perm_status = 9;

                            count_u3++;
                        }
                    }
                }

                count_u2++;

                // u2_document

                dataADOnlineU2.ad_online_u2vpn_action = _u2_document;
                dataADOnlineU2.ad_online_u3vpn_action = _u3_document;
                //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnlineU2));
                dataADOnlineU2 = callServiceADOnline(_urlApproveU2VPN, dataADOnlineU2);
            }

        }

        //-- insert u2 use vpn --//

        actionCreatePermLog(Constants.KEY_GET_FROM_PERM_PERM, permPermId, nodeIdNext, int.Parse(ViewState["empIDX"].ToString()));

        getEmpProfile(int.Parse(ViewState["empIDX"].ToString()));
        string bodyU0 = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
           lblValueWaitingPermPermEmpCode.Text,
           lblValueWaitingPermPermEmpNameTH.Text,
           lblValueWaitingPermPermOrgNameTH.Text,
           lblValueWaitingPermPermDeptNameTH.Text,
           lblValueWaitingPermPermSecNameTH.Text,
           lblValueWaitingPermPermPosNameTH.Text,
           commentText == string.Empty ? "-" : commentText,
           dataADOnline.ad_online_action[0].emp_name_th);
        string emails = getEmails(nodeIdNext, int.Parse(lblValueU0PermPermEmpIdx.Text), Constants.KEY_GET_FROM_PERM_PERM, permPermId.ToString());
        sentEmail(emails, Constants.KEY_GET_FROM_PERM_PERM, nodeIdNext, bodyU0);
    }

    protected void actionWaitingPermPermApproveEditUser(int permPermId, int nodeIdCurrent, int nodeIdNext)
    {
       
        ad_online objADOnline = new ad_online();
        dataADOnline.ad_online_action = new ad_online[1];
        objADOnline.on_table = Constants.KEY_GET_FROM_PERM_PERM;
        objADOnline.u0_perm_perm_idx = permPermId;
        objADOnline.u0_node_idx_ref_current = nodeIdCurrent;
        // objADOnline.u0_node_idx_ref = nodeIdNext;

        objADOnline.m0_perm_pol_idx_ref = int.Parse(ddlWaitingPermPermPol.SelectedValue);
        objADOnline.u0_perm_perm_sap = txtWaitingPermPermSap.Text.Trim();

        if (rdoWaitingPermPermEmailType.SelectedValue == "center")
        {
            if (ddlWaitingPermPermEmailType.SelectedValue == "1")
            {
                objADOnline.u0_perm_perm_mail_center = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;

            }
            else
            {
                objADOnline.u0_perm_perm_mail_center = ddl1or3_edit.SelectedValue;
            }
        }
        else if (rdoWaitingPermPermEmailType.SelectedValue == "private")
        {
            objADOnline.u0_perm_perm_mail_center = string.Empty;
            objADOnline.u0_perm_perm_mail_priv = txtWaitingPermPermEmailText.Text.Trim().ToLower() + ddlWaitingPermPermEmailDomain.SelectedValue;
            objADOnline.u0_perm_perm_mail_replace = ddl1or3_edit.SelectedValue; //txtemail.Text;
        }



        if (Array.IndexOf(Constants.JOBLEVELS_OFFICIAL, int.Parse(lblValueU0PermPermJobGradeLevel.Text)) > -1)
        {
            nodeIdNext = Constants.NODE_PERM_PERM_CREATE_GOTO_HEADER_EMPNEW;
        }
        else if (Array.IndexOf(Constants.JOBLEVELS_HEADER, int.Parse(lblValueU0PermPermJobGradeLevel.Text)) > -1)
        {
            nodeIdNext = Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_EMPNEW;
        }
        else if (Array.IndexOf(Constants.JOBLEVELS_DIRECTOR, int.Parse(lblValueU0PermPermJobGradeLevel.Text)) > -1)
        {
            nodeIdNext = Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_HR;
        }



        objADOnline.u0_node_idx_ref = nodeIdNext;
        objADOnline.u0_perm_perm_ad = txtWaitingPermPermITAD.Text.Trim();
        objADOnline.u0_perm_perm_mail_replace = ddl1or3_edit.SelectedValue; //txtemail.Text;
       
        objADOnline.u0_perm_perm_date_use = AddStartdate.Text;
        objADOnline.u0_perm_perm_type_mail = int.Parse(ddlWaitingPermPermEmailType.SelectedValue);
        objADOnline.u0_perm_perm_updated_by = int.Parse(ViewState["empIDX"].ToString());
        dataADOnline.ad_online_action[0] = objADOnline;

        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline));
        callServiceADOnline(_urlApproveU0WaitingPermPerm, dataADOnline);


        // -- update u2, u3 --//

        //-- insert u2 use vpn --//
        data_adonline dataADOnlineU2 = new data_adonline();
        dataADOnlineU2.ad_online_u2vpn_action = new ad_u2vpn_online[1];

        foreach (GridViewRow GvEditVPN_ in GvEditVPN.Rows)
        {

            if (GvEditVPN_.RowType == DataControlRowType.DataRow)
            {
                //linkBtnTrigger(btnSaveCarUseHr);   objADOnline.u0_perm_perm_idx = permPermId;
                //GridView GvExpenses = (GridView)gvDetailCaruse.FindControl("GvExpenses");
                Literal litvpnidxedit = (Literal)GvEditVPN_.FindControl("litvpnidxedit");
                CheckBox chkvpnedit = (CheckBox)GvEditVPN_.FindControl("chkvpnedit");
                CheckBoxList chksubvpnedit = (CheckBoxList)GvEditVPN_.FindControl("chksubvpnedit");
                TextBox txtremarkedit = (TextBox)GvEditVPN_.FindControl("txtremarkedit");

                //FileUpload UploadFile_Vpn = (FileUpload)gvUseVPN.FindControl("UploadFile_Vpn");
                //UpdatePanel Update_PnFileUpload = gvDetailCaruse.FindControl("Update_PnFileUpload") as UpdatePanel;


                // u2_document
                //data_carbooking data_u2_document_hr_addcar = new data_carbooking();
                int count_u2 = 0;
                var _u2_document = new ad_u2vpn_online[GvEditVPN.Rows.Count];
                var _u3_document = new ad_u3vpn_online[chksubvpnedit.Items.Count];

                if (chkvpnedit.Checked == true)
                {

                    _u2_document[count_u2] = new ad_u2vpn_online();
                    _u2_document[count_u2].vpnidx = int.Parse(litvpnidxedit.Text);

                    _u2_document[count_u2].remark = txtremarkedit.Text;
                    _u2_document[count_u2].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                    _u2_document[count_u2].u0_perm_temp_idx = permPermId; //idx form insert
                    _u2_document[count_u2].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                    _u2_document[count_u2].u2_perm_perm_status = 1;


                    int count_u3 = 0;

                    foreach (ListItem chk_count_update in chksubvpnedit.Items)
                    {
                        if (chk_count_update.Selected)
                        {
                            _u3_document[count_u3] = new ad_u3vpn_online();
                            _u3_document[count_u3].u0_perm_temp_idx = permPermId;
                            if (chkvpnedit.Checked == true)
                            {
                                _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedit.Text);
                            }
                            _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                            _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                            _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                            _u3_document[count_u3].u3_perm_perm_status = 1;

                            count_u3++;
                        }
                        else
                        {
                            _u3_document[count_u3] = new ad_u3vpn_online();
                            _u3_document[count_u3].u0_perm_temp_idx = permPermId;
                            if (chkvpnedit.Checked == true)
                            {
                                _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedit.Text);
                            }
                            _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                            _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                            _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                            _u3_document[count_u3].u3_perm_perm_status = 9;

                            count_u3++;
                        }
                    }


                }
                else
                {
                    _u2_document[count_u2] = new ad_u2vpn_online();
                    _u2_document[count_u2].vpnidx = int.Parse(litvpnidxedit.Text);

                    _u2_document[count_u2].remark = txtremarkedit.Text;
                    _u2_document[count_u2].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                    _u2_document[count_u2].u0_perm_temp_idx = permPermId; //idx form insert
                    _u2_document[count_u2].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                    _u2_document[count_u2].u2_perm_perm_status = 9;


                    int count_u3 = 0;

                    foreach (ListItem chk_count_update in chksubvpnedit.Items)
                    {
                        if (chk_count_update.Selected)
                        {
                            _u3_document[count_u3] = new ad_u3vpn_online();
                            _u3_document[count_u3].u0_perm_temp_idx = permPermId;
                            if (chkvpnedit.Checked == true)
                            {
                                _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedit.Text);
                            }
                            _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                            _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                            _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                            _u3_document[count_u3].u3_perm_perm_status = 1;

                            count_u3++;
                        }
                        else
                        {
                            _u3_document[count_u3] = new ad_u3vpn_online();
                            _u3_document[count_u3].u0_perm_temp_idx = permPermId;
                            if (chkvpnedit.Checked == true)
                            {
                                _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedit.Text);
                            }
                            _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_PERM;
                            _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                            _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                            _u3_document[count_u3].u3_perm_perm_status = 9;

                            count_u3++;
                        }
                    }
                }

                count_u2++;

                // u2_document

                dataADOnlineU2.ad_online_u2vpn_action = _u2_document;
                dataADOnlineU2.ad_online_u3vpn_action = _u3_document;
                //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnlineU2));
                dataADOnlineU2 = callServiceADOnline(_urlApproveU2VPN, dataADOnlineU2);
            }

        }

        //-- insert u2 use vpn --//

        // -- update u2, u3 --//
        actionCreatePermLog(Constants.KEY_GET_FROM_PERM_PERM, permPermId, nodeIdNext, int.Parse(ViewState["empIDX"].ToString()));

        getEmpProfile(int.Parse(ViewState["empIDX"].ToString()));
        string bodyU0 = string.Format("{0},{1},{2},{3},{4},{5},{6}",
           lblValueWaitingPermPermEmpCode.Text,
           lblValueWaitingPermPermEmpNameTH.Text,
           lblValueWaitingPermPermOrgNameTH.Text,
           lblValueWaitingPermPermDeptNameTH.Text,
           lblValueWaitingPermPermSecNameTH.Text,
           lblValueWaitingPermPermPosNameTH.Text,
            dataADOnline.ad_online_action[0].emp_name_th);
        string emails = getEmails(nodeIdNext, int.Parse(lblValueU0PermPermEmpIdx.Text), Constants.KEY_GET_FROM_PERM_PERM, permPermId.ToString());
        sentEmail(emails, Constants.KEY_GET_FROM_PERM_PERM, nodeIdNext, bodyU0);
    }

    protected void actionWaitingPermPermSaveComment(int permPermId, string commentText)
    {
        ad_online objADOnline = new ad_online();
        dataADOnline.ad_online_action = new ad_online[1];
        objADOnline.on_table = Constants.KEY_GET_FROM_PERM_PERM;
        objADOnline.u0_perm_perm_idx_ref = permPermId;
        objADOnline.u1_perm_perm_cm_it = commentText;
        objADOnline.u1_perm_perm_created_by = int.Parse(ViewState["empIDX"].ToString());
        dataADOnline.ad_online_action[0] = objADOnline;
        callServiceADOnline(_urlADSaveCommentITU0WaitingPermPerm, dataADOnline);
        string bodyU0 = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
           lblValueWaitingPermPermEmpCode.Text,
           lblValueWaitingPermPermEmpNameTH.Text,
           lblValueWaitingPermPermOrgNameTH.Text,
           lblValueWaitingPermPermDeptNameTH.Text,
           lblValueWaitingPermPermSecNameTH.Text,
           lblValueWaitingPermPermPosNameTH.Text,
           commentText == string.Empty ? "-" : commentText,
           "");
        string emails = getEmails(Constants.NODE_PERM_PERM_SENT_COMMENT, int.Parse(lblValueU0PermPermEmpIdxCreator.Text), Constants.KEY_GET_FROM_PERM_PERM);
        sentEmail(emails, Constants.KEY_GET_FROM_PERM_PERM, Constants.NODE_PERM_PERM_SENT_COMMENT, bodyU0);
    }

    protected void actionWaitingPermTemp()
    {
        _divMenuLiToDivWaiting.Attributes.Add("class", "active");
        divWaiting.Visible = true;
        ad_online objADOnline = new ad_online();
        dataADOnline.ad_online_action = new ad_online[1];
        objADOnline.on_table = Constants.KEY_GET_FROM_PERM_TEMP;
        objADOnline.emp_idx = int.Parse(ViewState["empIDX"].ToString());
        dataADOnline.ad_online_action[0] = objADOnline;
        //   txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline));

        dataADOnline = callServiceADOnline(_urlReadU0WaitingPermTemp, dataADOnline);
        setGridData(gvWaitingPermTemp, dataADOnline.ad_online_action);
    }

    protected void actionWaitingPermTempDetail(int id)
    {
        data_adonline dataADOnlineU0 = new data_adonline();
        ad_online objADOnlineU0 = new ad_online();
        dataADOnlineU0.ad_online_action = new ad_online[1];
        objADOnlineU0.on_table = Constants.KEY_GET_FROM_PERM_TEMP;
        objADOnlineU0.with_where = Constants.WITH_WHERE_YES;
        objADOnlineU0.u0_perm_temp_idx = id;
        dataADOnlineU0.ad_online_action[0] = objADOnlineU0;
        dataADOnlineU0 = callServiceADOnline(_urlReadU0WaitingDetailPermTemp, dataADOnlineU0);
        lblValueU0PermTempIdx.Text = id.ToString();
        lblValueWaitingPermTempCode.Text = dataADOnlineU0.ad_online_action[0].u0_perm_temp_code_id;
        lblValueWaitingPermTempCreator.Text = dataADOnlineU0.ad_online_action[0].emp_name_th;
        lblValueWaitingPermTempOrgName.Text = dataADOnlineU0.ad_online_action[0].org_name_th;
        lblValueWaitingPermTempDeptName.Text = dataADOnlineU0.ad_online_action[0].dept_name_th;
        lblValueWaitingPermTempSecName.Text = dataADOnlineU0.ad_online_action[0].sec_name_th;
        lblValueWaitingPermTempPosName.Text = dataADOnlineU0.ad_online_action[0].pos_name_th;
        lblValueWaitingPermTempCreatedAt.Text = dataADOnlineU0.ad_online_action[0].u0_perm_temp_created_at;
        lblValueWaitingPermTempCompName.Text = dataADOnlineU0.ad_online_action[0].u0_perm_temp_comp_name;
        lblValueWaitingPermTempReason.Text = dataADOnlineU0.ad_online_action[0].u0_perm_temp_detail;
        lblvpnidxedittemp.Text = dataADOnlineU0.ad_online_action[0].vpn_name;
        lblValueWaitingPermTempNode.Text = string.Format("{0} โดย {1}", dataADOnlineU0.ad_online_action[0].status_to_1, dataADOnlineU0.ad_online_action[0].status_to_2);

        if (dataADOnlineU0.ad_online_action[0].u0_cm_director_creator != string.Empty)
        {
            _divWaitingPermTempCmDirectorCreator.Visible = true;
            lblValueWaitingPermTempCmDirectorCreator.Text = dataADOnlineU0.ad_online_action[0].u0_cm_director_creator;
        }
        else
        {
            _divWaitingPermTempCmDirectorCreator.Visible = false;
        }
        if (dataADOnlineU0.ad_online_action[0].u0_cm_head_it != string.Empty)
        {
            _divWaitingPermTempCmHeaderIT.Visible = true;
            lblValueWaitingPermTempCmHeaderIT.Text = dataADOnlineU0.ad_online_action[0].u0_cm_head_it;
        }
        else
        {
            _divWaitingPermTempCmHeaderIT.Visible = false;
        }
        if (dataADOnlineU0.ad_online_action[0].u0_cm_director_it != string.Empty)
        {
            _divWaitingPermTempCmDirectorIT.Visible = true;
            lblValueWaitingPermTempCmDirectorIT.Text = dataADOnlineU0.ad_online_action[0].u0_cm_director_it;
        }
        else
        {
            _divWaitingPermTempCmDirectorIT.Visible = false;
        }
        visibleWaitingPermTempCommentText(dataADOnlineU0.ad_online_action[0].u0_cm_director_creator, dataADOnlineU0.ad_online_action[0].u0_cm_head_it, dataADOnlineU0.ad_online_action[0].u0_cm_director_it);
        visibleWaitingPermCommentBox(Constants.KEY_GET_FROM_PERM_TEMP, dataADOnlineU0.ad_online_action[0].u0_node_idx_ref);
        lblValueU0PermTempNodeId.Text = dataADOnlineU0.ad_online_action[0].u0_node_idx_ref.ToString();

        data_adonline dataADOnlineU1 = new data_adonline();
        ad_online objADOnlineU1 = new ad_online();
        dataADOnlineU1.ad_online_action = new ad_online[1];
        objADOnlineU1.on_table = Constants.KEY_GET_FROM_PERM_TEMP;
        objADOnlineU1.u0_perm_temp_idx_ref = id;
        dataADOnlineU1.ad_online_action[0] = objADOnlineU1;
        dataADOnlineU1 = callServiceADOnline(_urlReadU1WaitingDetailPermTemp, dataADOnlineU1);
        lblValueWaitingAmountContact.Text = dataADOnlineU1.ad_online_action.Length + " คน";
        bool viewMultipleDate = true;
        string dateFirstRow = string.Format("{0}{1}",
           dataADOnlineU1.ad_online_action[0].u1_perm_temp_start,
           dataADOnlineU1.ad_online_action[0].u1_perm_temp_end);
        string dateStart = dataADOnlineU1.ad_online_action[0].u1_perm_temp_start;
        string dateEnd = dataADOnlineU1.ad_online_action[0].u1_perm_temp_end;
        foreach (ad_online value in dataADOnlineU1.ad_online_action)
        {
            if (string.Format("{0}{1}", value.u1_perm_temp_start.ToString(), value.u1_perm_temp_end.ToString()) != dateFirstRow)
            {
                viewMultipleDate = !viewMultipleDate;
                break;
            }
        }
        if (!viewMultipleDate)
        {
            groupDateWaiting.Visible = false;
            lblValueWaitingU0DateStart.Text = string.Empty;
            lblValueWaitingU0DateEnd.Text = string.Empty;
            setGridData(gvWaitingU1PermTempContactsList, dataADOnlineU1.ad_online_action);
            gvWaitingU1PermTempContactsList.Columns[3].Visible = true;
            gvWaitingU1PermTempContactsList.Columns[4].Visible = true;
        }
        else
        {
            groupDateWaiting.Visible = true;
            lblValueWaitingU0DateStart.Text = dateStart;
            lblValueWaitingU0DateEnd.Text = dateEnd;
            setGridData(gvWaitingU1PermTempContactsList, dataADOnlineU1.ad_online_action);
            gvWaitingU1PermTempContactsList.Columns[3].Visible = false;
            gvWaitingU1PermTempContactsList.Columns[4].Visible = false;
        }
        if (lblValueU0PermTempNodeId.Text == Constants.NODE_PERM_TEMP_APP_GOTO_IT.ToString())
        {
            gvWaitingU1PermTempContactsList.Columns[5].Visible = true;
            gvWaitingU1PermTempContactsList.Columns[6].Visible = true;
        }
        else
        {
            gvWaitingU1PermTempContactsList.Columns[5].Visible = false;
            gvWaitingU1PermTempContactsList.Columns[6].Visible = false;
        }
    }

    protected void actionWaitingPermTempApprove(int permTempId, int nodeId, string commentText)
    {
        if (nodeId == Constants.NODE_PERM_TEMP_APP_GOTO_HEADER_IT || nodeId == Constants.NODE_PERM_TEMP_APP_GOTO_DIRECTOR_IT || nodeId == Constants.NODE_PERM_TEMP_APP_GOTO_IT)
        {

            //litDebug.Text = "1";


            bool isChecked = false;
            foreach (GridViewRow gvRow in gvWaitingU1PermTempContactsList.Rows)
            {
                CheckBox chkWaitingU1PermTemp = (CheckBox)gvRow.Cells[0].FindControl("chkWaitingU1PermTempContactList");
                if (chkWaitingU1PermTemp.Checked)
                {
                    isChecked = true;
                    break;
                }
                else
                {
                    isChecked = false;
                }
            }
            if (isChecked)
            {
                ad_online objADOnline = new ad_online();
                dataADOnline.ad_online_action = new ad_online[1];
                objADOnline.on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                objADOnline.u0_perm_temp_idx = permTempId;
                objADOnline.u0_node_idx_ref = nodeId;
                if (nodeId == Constants.NODE_PERM_TEMP_APP_GOTO_HEADER_IT)
                {
                    objADOnline.u0_cm_director_creator = commentText;
                }
                else if (nodeId == Constants.NODE_PERM_TEMP_APP_GOTO_DIRECTOR_IT)
                {
                    objADOnline.u0_cm_head_it = commentText;
                }
                else if (nodeId == Constants.NODE_PERM_TEMP_APP_GOTO_IT)
                {
                    objADOnline.u0_cm_director_it = commentText;
                }
                objADOnline.u0_perm_temp_updated_by = int.Parse(ViewState["empIDX"].ToString());
                dataADOnline.ad_online_action[0] = objADOnline;
                callServiceADOnline(_urlApproveU0WaitingPermTemp, dataADOnline);

                //-- insert u2 use vpn --//
                data_adonline dataADOnlineU2 = new data_adonline();
                dataADOnlineU2.ad_online_u2vpn_action = new ad_u2vpn_online[1];

                foreach (GridViewRow GvEditVPNTemp_ in GvEditVPNTemp.Rows)
                {

                    if (GvEditVPNTemp_.RowType == DataControlRowType.DataRow)
                    {
                        //linkBtnTrigger(btnSaveCarUseHr);   objADOnline.u0_perm_perm_idx = permPermId;
                        //GridView GvExpenses = (GridView)gvDetailCaruse.FindControl("GvExpenses");
                        Literal litvpnidxedittemp = (Literal)GvEditVPNTemp_.FindControl("litvpnidxedittemp");
                        CheckBox chkvpnedittemp = (CheckBox)GvEditVPNTemp_.FindControl("chkvpnedittemp");
                        CheckBoxList chksubvpnedittemp = (CheckBoxList)GvEditVPNTemp_.FindControl("chksubvpnedittemp");
                        TextBox txtremarkedittemp = (TextBox)GvEditVPNTemp_.FindControl("txtremarkedittemp");

                        //FileUpload UploadFile_Vpn = (FileUpload)gvUseVPN.FindControl("UploadFile_Vpn");
                        //UpdatePanel Update_PnFileUpload = gvDetailCaruse.FindControl("Update_PnFileUpload") as UpdatePanel;


                        // u2_document
                        //data_carbooking data_u2_document_hr_addcar = new data_carbooking();
                        int count_u2 = 0;
                        var _u2_document = new ad_u2vpn_online[GvEditVPNTemp.Rows.Count];
                        var _u3_document = new ad_u3vpn_online[chksubvpnedittemp.Items.Count];

                        if (chkvpnedittemp.Checked == true)
                        {

                            _u2_document[count_u2] = new ad_u2vpn_online();
                            _u2_document[count_u2].vpnidx = int.Parse(litvpnidxedittemp.Text);

                            _u2_document[count_u2].remark = txtremarkedittemp.Text;
                            _u2_document[count_u2].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                            _u2_document[count_u2].u0_perm_temp_idx = permTempId; //idx form insert
                            _u2_document[count_u2].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                            _u2_document[count_u2].u2_perm_perm_status = 1;


                            int count_u3 = 0;

                            foreach (ListItem chk_count_update in chksubvpnedittemp.Items)
                            {
                                if (chk_count_update.Selected)
                                {
                                    _u3_document[count_u3] = new ad_u3vpn_online();
                                    _u3_document[count_u3].u0_perm_temp_idx = permTempId;
                                    if (chkvpnedittemp.Checked == true)
                                    {
                                        _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedittemp.Text);
                                    }
                                    _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                                    _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                                    _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                                    _u3_document[count_u3].u3_perm_perm_status = 1;

                                    count_u3++;
                                }
                                else
                                {
                                    _u3_document[count_u3] = new ad_u3vpn_online();
                                    _u3_document[count_u3].u0_perm_temp_idx = permTempId;
                                    if (chkvpnedittemp.Checked == true)
                                    {
                                        _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedittemp.Text);
                                    }
                                    _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                                    _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                                    _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                                    _u3_document[count_u3].u3_perm_perm_status = 9;

                                    count_u3++;
                                }
                            }


                        }
                        else
                        {
                            _u2_document[count_u2] = new ad_u2vpn_online();
                            _u2_document[count_u2].vpnidx = int.Parse(litvpnidxedittemp.Text);

                            _u2_document[count_u2].remark = txtremarkedittemp.Text;
                            _u2_document[count_u2].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                            _u2_document[count_u2].u0_perm_temp_idx = permTempId; //idx form insert
                            _u2_document[count_u2].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                            _u2_document[count_u2].u2_perm_perm_status = 9;


                            int count_u3 = 0;

                            foreach (ListItem chk_count_update in chksubvpnedittemp.Items)
                            {
                                if (chk_count_update.Selected)
                                {
                                    _u3_document[count_u3] = new ad_u3vpn_online();
                                    _u3_document[count_u3].u0_perm_temp_idx = permTempId;
                                    if (chkvpnedittemp.Checked == true)
                                    {
                                        _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedittemp.Text);
                                    }
                                    _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                                    _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                                    _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                                    _u3_document[count_u3].u3_perm_perm_status = 1;

                                    count_u3++;
                                }
                                else
                                {
                                    _u3_document[count_u3] = new ad_u3vpn_online();
                                    _u3_document[count_u3].u0_perm_temp_idx = permTempId;
                                    if (chkvpnedittemp.Checked == true)
                                    {
                                        _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedittemp.Text);
                                    }
                                    _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                                    _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                                    _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                                    _u3_document[count_u3].u3_perm_perm_status = 9;

                                    count_u3++;
                                }
                            }
                        }

                        count_u2++;

                        // u2_document

                        dataADOnlineU2.ad_online_u2vpn_action = _u2_document;
                        dataADOnlineU2.ad_online_u3vpn_action = _u3_document;
                        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnlineU2));
                        dataADOnlineU2 = callServiceADOnline(_urlApproveU2VPN, dataADOnlineU2);
                    }

                }

                //-- insert u2 use vpn --//

                int itemContact = 0;
                string contactNameForSentMail = string.Empty;
                foreach (GridViewRow gvRow in gvWaitingU1PermTempContactsList.Rows)
                {
                    CheckBox chkWaitingU1PermTemp = (CheckBox)gvRow.Cells[0].FindControl("chkWaitingU1PermTempContactList");
                    Label lblU1PermTempIdx = (Label)gvRow.Cells[0].FindControl("lblU1PermTempIdx");
                    Label lblU1PermTempFullNameTH = (Label)gvRow.Cells[1].FindControl("lblU1PermTempFullNameTH");
                    if (chkWaitingU1PermTemp.Checked)
                    {
                        contactNameForSentMail += string.Format("<tr><td>{0}. {1}</td></tr>", ++itemContact, lblU1PermTempFullNameTH.Text);
                    }
                    else if (!chkWaitingU1PermTemp.Checked)
                    {
                        data_adonline dataADOnlineU1 = new data_adonline();
                        ad_online objADOnlineU1 = new ad_online();
                        dataADOnlineU1.ad_online_action = new ad_online[1];
                        objADOnlineU1.on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                        objADOnlineU1.u1_perm_temp_idx = int.Parse(lblU1PermTempIdx.Text);
                        objADOnlineU1.u1_perm_temp_status = Constants.U1_PERM_TEMP_STATUS_NOTAPP;
                        objADOnlineU1.u1_perm_temp_updated_by = int.Parse(ViewState["empIDX"].ToString());
                        dataADOnlineU1.ad_online_action[0] = objADOnlineU1;
                        callServiceADOnline(_urlApproveU1WaitingPermTemp, dataADOnlineU1);
                    }
                }
                actionCreatePermLog(Constants.KEY_GET_FROM_PERM_TEMP, permTempId, nodeId, int.Parse(ViewState["empIDX"].ToString()));
                string bodyU0 = string.Empty;
                string bodyU1 = string.Empty;
                bodyU0 = string.Format("{0},{1},{2},{3}",
                   lblValueWaitingPermTempCreator.Text,
                   lblValueWaitingPermTempOrgName.Text,
                   lblValueWaitingPermTempDeptName.Text,
                   lblValueWaitingPermTempCompName.Text);
                bodyU1 = string.Format("{0},{1}", itemContact, contactNameForSentMail);
                string emails = getEmails(nodeId, int.Parse(ViewState["empIDX"].ToString()));
                sentEmail(emails, Constants.KEY_GET_FROM_PERM_TEMP, nodeId, bodyU0, bodyU1);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกอนุมัติรายชื่อผู้มาติดต่อ 1 รายการหรือมากกว่า');", true);
                return;
            }
        }
        else
        {
            //litDebug.Text = "2";

            ad_online objADOnline = new ad_online();
            dataADOnline.ad_online_action = new ad_online[1];
            objADOnline.on_table = Constants.KEY_GET_FROM_PERM_TEMP;
            objADOnline.u0_perm_temp_idx = permTempId;
            objADOnline.u0_node_idx_ref = nodeId;
            if (nodeId == Constants.NODE_PERM_TEMP_NOTAPP_ENDBY_DIRECTOR_CREATOR)
            {
                objADOnline.u0_cm_director_creator = commentText;
            }
            else if (nodeId == Constants.NODE_PERM_TEMP_NOTAPP_ENDBY_HEADER_IT)
            {
                objADOnline.u0_cm_head_it = commentText;
            }
            else if (nodeId == Constants.NODE_PERM_TEMP_NOTAPP_ENDBY_DIRECTOR_IT)
            {
                objADOnline.u0_cm_director_it = commentText;
            }
            objADOnline.u0_perm_temp_updated_by = int.Parse(ViewState["empIDX"].ToString());
            dataADOnline.ad_online_action[0] = objADOnline;
            callServiceADOnline(_urlApproveU0WaitingPermTemp, dataADOnline);

            //-- insert u2 use vpn --//
            data_adonline dataADOnlineU2 = new data_adonline();
            dataADOnlineU2.ad_online_u2vpn_action = new ad_u2vpn_online[1];

            foreach (GridViewRow GvEditVPNTemp_ in GvEditVPNTemp.Rows)
            {

                if (GvEditVPNTemp_.RowType == DataControlRowType.DataRow)
                {
                    //linkBtnTrigger(btnSaveCarUseHr);   objADOnline.u0_perm_perm_idx = permPermId;
                    //GridView GvExpenses = (GridView)gvDetailCaruse.FindControl("GvExpenses");
                    Literal litvpnidxedittemp = (Literal)GvEditVPNTemp_.FindControl("litvpnidxedittemp");
                    CheckBox chkvpnedittemp = (CheckBox)GvEditVPNTemp_.FindControl("chkvpnedittemp");
                    CheckBoxList chksubvpnedittemp = (CheckBoxList)GvEditVPNTemp_.FindControl("chksubvpnedittemp");
                    TextBox txtremarkedittemp = (TextBox)GvEditVPNTemp_.FindControl("txtremarkedittemp");

                    //FileUpload UploadFile_Vpn = (FileUpload)gvUseVPN.FindControl("UploadFile_Vpn");
                    //UpdatePanel Update_PnFileUpload = gvDetailCaruse.FindControl("Update_PnFileUpload") as UpdatePanel;


                    // u2_document
                    //data_carbooking data_u2_document_hr_addcar = new data_carbooking();
                    int count_u2 = 0;
                    var _u2_document = new ad_u2vpn_online[GvEditVPNTemp.Rows.Count];
                    var _u3_document = new ad_u3vpn_online[chksubvpnedittemp.Items.Count];

                    if (chkvpnedittemp.Checked == true)
                    {

                        _u2_document[count_u2] = new ad_u2vpn_online();
                        _u2_document[count_u2].vpnidx = int.Parse(litvpnidxedittemp.Text);

                        _u2_document[count_u2].remark = txtremarkedittemp.Text;
                        _u2_document[count_u2].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                        _u2_document[count_u2].u0_perm_temp_idx = permTempId; //idx form insert
                        _u2_document[count_u2].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                        _u2_document[count_u2].u2_perm_perm_status = 1;


                        int count_u3 = 0;

                        foreach (ListItem chk_count_update in chksubvpnedittemp.Items)
                        {
                            if (chk_count_update.Selected)
                            {
                                _u3_document[count_u3] = new ad_u3vpn_online();
                                _u3_document[count_u3].u0_perm_temp_idx = permTempId;
                                if (chkvpnedittemp.Checked == true)
                                {
                                    _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedittemp.Text);
                                }
                                _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                                _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                                _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                                _u3_document[count_u3].u3_perm_perm_status = 1;

                                count_u3++;
                            }
                            else
                            {
                                _u3_document[count_u3] = new ad_u3vpn_online();
                                _u3_document[count_u3].u0_perm_temp_idx = permTempId;
                                if (chkvpnedittemp.Checked == true)
                                {
                                    _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedittemp.Text);
                                }
                                _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                                _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                                _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                                _u3_document[count_u3].u3_perm_perm_status = 9;

                                count_u3++;
                            }
                        }


                    }
                    else
                    {
                        _u2_document[count_u2] = new ad_u2vpn_online();
                        _u2_document[count_u2].vpnidx = int.Parse(litvpnidxedittemp.Text);

                        _u2_document[count_u2].remark = txtremarkedittemp.Text;
                        _u2_document[count_u2].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                        _u2_document[count_u2].u0_perm_temp_idx = permTempId; //idx form insert
                        _u2_document[count_u2].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                        _u2_document[count_u2].u2_perm_perm_status = 9;


                        int count_u3 = 0;

                        foreach (ListItem chk_count_update in chksubvpnedittemp.Items)
                        {
                            if (chk_count_update.Selected)
                            {
                                _u3_document[count_u3] = new ad_u3vpn_online();
                                _u3_document[count_u3].u0_perm_temp_idx = permTempId;
                                if (chkvpnedittemp.Checked == true)
                                {
                                    _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedittemp.Text);
                                }
                                _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                                _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                                _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                                _u3_document[count_u3].u3_perm_perm_status = 1;

                                count_u3++;
                            }
                            else
                            {
                                _u3_document[count_u3] = new ad_u3vpn_online();
                                _u3_document[count_u3].u0_perm_temp_idx = permTempId;
                                if (chkvpnedittemp.Checked == true)
                                {
                                    _u3_document[count_u3].vpnidx = int.Parse(litvpnidxedittemp.Text);
                                }
                                _u3_document[count_u3].on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                                _u3_document[count_u3].vpn1idx = int.Parse(chk_count_update.Value);
                                _u3_document[count_u3].uemp_idx = int.Parse(ViewState["empIDX"].ToString());
                                _u3_document[count_u3].u3_perm_perm_status = 9;

                                count_u3++;
                            }
                        }
                    }

                    count_u2++;

                    // u2_document

                    dataADOnlineU2.ad_online_u2vpn_action = _u2_document;
                    dataADOnlineU2.ad_online_u3vpn_action = _u3_document;
                    txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnlineU2));
                    dataADOnlineU2 = callServiceADOnline(_urlApproveU2VPN, dataADOnlineU2);
                }

            }

            //-- insert u2 use vpn --//

            actionCreatePermLog(Constants.KEY_GET_FROM_PERM_TEMP, permTempId, nodeId, int.Parse(ViewState["empIDX"].ToString()));
            string bodyU0 = string.Empty;
            if (nodeId == Constants.NODE_PERM_TEMP_NOTAPP_ENDBY_DIRECTOR_CREATOR)
            {
                getEmpProfile(int.Parse(ViewState["empIDX"].ToString()));
                bodyU0 = string.Format("{0},{1},{2}", dataADOnline.ad_online_action[0].emp_name_th, txtWaitingPermTempCommentDirectorOfCreator.Text.Trim(), DateTime.Now.ToString("dd/MM/yyyy"));
            }
            else if (nodeId == Constants.NODE_PERM_TEMP_NOTAPP_ENDBY_HEADER_IT)
            {
                getEmpProfile(int.Parse(ViewState["empIDX"].ToString()));
                bodyU0 = string.Format("{0},{1},{2}", dataADOnline.ad_online_action[0].emp_name_th, txtWaitingPermTempCommentHeaderIT.Text.Trim(), DateTime.Now.ToString("dd/MM/yyyy"));
            }
            else if (nodeId == Constants.NODE_PERM_TEMP_NOTAPP_ENDBY_DIRECTOR_IT)
            {
                getEmpProfile(int.Parse(ViewState["empIDX"].ToString()));
                bodyU0 = string.Format("{0},{1},{2}", dataADOnline.ad_online_action[0].emp_name_th, txtWaitingPermTempCommentDirectorIT.Text.Trim(), DateTime.Now.ToString("dd/MM/yyyy"));
            }
            string emails = getEmails(nodeId, int.Parse(ViewState["empIDX"].ToString()));
            sentEmail(emails, Constants.KEY_GET_FROM_PERM_TEMP, nodeId, bodyU0);
        }
    }

    protected void actionWaitingPermTempSave(int permTempId, int nodeId)
    {
        ad_online objADOnline = new ad_online();
        dataADOnline.ad_online_action = new ad_online[1];
        objADOnline.on_table = Constants.KEY_GET_FROM_PERM_TEMP;
        objADOnline.u0_perm_temp_idx = permTempId;
        objADOnline.u0_node_idx_ref = nodeId;
        objADOnline.u0_perm_temp_updated_by = int.Parse(ViewState["empIDX"].ToString());
        dataADOnline.ad_online_action[0] = objADOnline;
        callServiceADOnline(_urlApproveU0WaitingPermTemp, dataADOnline);
        string bodyU0 = string.Empty;
        string bodyU1 = string.Empty;
        foreach (GridViewRow gvRow in gvWaitingU1PermTempContactsList.Rows)
        {
            CheckBox chkWaitingU1PermTempContactList = (CheckBox)gvRow.Cells[0].FindControl("chkWaitingU1PermTempContactList");
            if (chkWaitingU1PermTempContactList.Checked)
            {
                Label lblU1PermTempIdx = (Label)gvRow.Cells[0].FindControl("lblU1PermTempIdx");
                Label lblU1PermTempFullNameTH = (Label)gvRow.Cells[2].FindControl("lblU1PermTempFullNameTH");
                Label lblU1PermTempStart = (Label)gvRow.Cells[3].FindControl("lblU1PermTempStart");
                Label lblU1PermTempEnd = (Label)gvRow.Cells[4].FindControl("lblU1PermTempEnd");
                TextBox txtU1PermTempUsername = (TextBox)gvRow.Cells[5].FindControl("txtU1PermTempUsername");
                TextBox txtU1PermTempPassword = (TextBox)gvRow.Cells[6].FindControl("txtU1PermTempPassword");
                data_adonline dataADOnlineU1 = new data_adonline();
                ad_online objADOnlineU1 = new ad_online();
                dataADOnlineU1.ad_online_action = new ad_online[1];
                objADOnlineU1.on_table = Constants.KEY_GET_FROM_PERM_TEMP;
                objADOnlineU1.u1_perm_temp_idx = int.Parse(lblU1PermTempIdx.Text);
                objADOnlineU1.u1_perm_temp_username = txtU1PermTempUsername.Text.Trim();
                objADOnlineU1.u1_perm_temp_password = txtU1PermTempPassword.Text.Trim();
                objADOnlineU1.u1_perm_temp_updated_by = int.Parse(ViewState["empIDX"].ToString());
                dataADOnlineU1.ad_online_action[0] = objADOnlineU1;
                callServiceADOnline(_urlSaveWaitingPermTemp, dataADOnlineU1);
                bodyU1 += string.Format("<tr><td><strong>ชื่อ - สกุล :</strong> {0}</td></tr>", lblU1PermTempFullNameTH.Text);
                bodyU1 += string.Format("<tr><td><strong>Username :</strong> {0}</td></tr>", txtU1PermTempUsername.Text.Trim());
                bodyU1 += string.Format("<tr><td><strong>Password :</strong> {0}</td></tr>", txtU1PermTempPassword.Text.Trim());
                if (lblValueWaitingU0DateStart.Text == string.Empty && lblValueWaitingU0DateEnd.Text == string.Empty)
                {
                    bodyU1 += string.Format("<tr><td><strong>วันที่เริ่มต้นใช้สิทธิ์ :</strong> {0}</td></tr>", lblU1PermTempStart.Text);
                    bodyU1 += string.Format("<tr><td><strong>วันที่สิ้นสุดใช้สิทธิ์ :</strong> {0}</td></tr>", lblU1PermTempEnd.Text);
                }
                bodyU1 += "<tr><td>&nbsp;</td></tr>";
            }
        }
        actionCreatePermLog(Constants.KEY_GET_FROM_PERM_TEMP, permTempId, nodeId, int.Parse(ViewState["empIDX"].ToString()));
        string dateStart = string.Empty;
        string dateEnd = string.Empty;
        string dateGroup = string.Empty;
        if (lblValueWaitingU0DateStart.Text != string.Empty && lblValueWaitingU0DateEnd.Text != string.Empty)
        {
            dateStart = string.Format("<tr><td><strong>วันที่เริ่มต้นใช้สิทธิ์ :</strong> {0}</td></tr>", lblValueWaitingU0DateStart.Text);
            dateEnd = string.Format("<tr><td><strong>วันที่สิ้นสุดใช้สิทธิ์ :</strong> {0}</td></tr>", lblValueWaitingU0DateEnd.Text);
            dateGroup = dateStart + dateEnd;
        }
        bodyU0 = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
           lblValueWaitingPermTempCreator.Text,
           lblValueWaitingPermTempOrgName.Text,
           lblValueWaitingPermTempDeptName.Text,
           lblValueWaitingPermTempSecName.Text,
           lblValueWaitingPermTempPosName.Text,
           lblValueWaitingPermTempCompName.Text,
           lblValueWaitingPermTempReason.Text,
           dateGroup);
        string emails = getEmails(nodeId, int.Parse(ViewState["empIDX"].ToString()), Constants.KEY_GET_FROM_PERM_TEMP, permTempId.ToString());
        sentEmail(emails, Constants.KEY_GET_FROM_PERM_TEMP, nodeId, bodyU0, bodyU1);
    }

    protected void actionPermLog(Repeater rptName, string fromTable, int pkId)
    {
        data_adonline dataADOnlineL0 = new data_adonline();
        ad_log_online objADOnlineL0 = new ad_log_online();
        dataADOnlineL0.ad_log_online_action = new ad_log_online[1];
        objADOnlineL0.from_table = fromTable;
        objADOnlineL0.u0_pk_perm_idx_ref = pkId;
        dataADOnlineL0.ad_log_online_action[0] = objADOnlineL0;
        dataADOnlineL0 = callServiceADOnline(_urlReadL0Perm, dataADOnlineL0);
        setRepeaterData(rptName, dataADOnlineL0.ad_log_online_action);
    }

    protected void actionCreatePermLog(string fromTable, int pkPermIdxRef, int nodeIdx, int creatorIdx)
    {
        data_adonline dataADOnlineL0 = new data_adonline();
        ad_log_online objADOnlineL0 = new ad_log_online();
        dataADOnlineL0.ad_log_online_action = new ad_log_online[1];
        objADOnlineL0.from_table = fromTable;
        objADOnlineL0.u0_pk_perm_idx_ref = pkPermIdxRef;
        objADOnlineL0.u0_node_idx_ref = nodeIdx;
        objADOnlineL0.perm_log_created_by = creatorIdx;
        dataADOnlineL0.ad_log_online_action[0] = objADOnlineL0;
        callServiceADOnline(_urlCreateL0Perm, dataADOnlineL0);
    }

    protected void actionIndexSetApprove()
    {
        int temp = 0;
        int perm = 0;
        int total = 0;

        _divMenuLiToDivIndex.Attributes.Add("class", "active");
        divIndex.Visible = true;
        _divIndexPermPerm.Visible = true;
        ad_online objADOnline = new ad_online();
        dataADOnline.ad_online_action = new ad_online[1];

        objADOnline.emp_idx = int.Parse(ViewState["empIDX"].ToString());
        dataADOnline.ad_online_action[0] = objADOnline;

        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline));
        dataADOnline = callServiceADOnline(_urlADSetActive_MenuApprove, dataADOnline);

        ViewState["return_temp"] = dataADOnline.return_temp;
        ViewState["return_code"] = dataADOnline.return_code;
        ViewState["return_perm"] = dataADOnline.return_perm;
        temp = int.Parse(ViewState["return_temp"].ToString());
        perm = int.Parse(ViewState["return_perm"].ToString());
        total = temp + perm;

        nav_approve.Text = "รายการที่รออนุมัติ  <span class='badge progress-bar-danger' >" + Convert.ToString(total) + "</span>";
        _divMenBtnToDivWaitingPermPerm.Text = "สิทธิ์ทั่วไป <span class='badge progress-bar-danger' style='font-weight:700'>" + ViewState["return_perm"].ToString() + "</span>";
        _divMenuBtnToDivWaitingPermTemp.Text = "สิทธิ์ชั่วคราว <span class='badge progress-bar-danger' style='font-weight:700'>" + ViewState["return_temp"].ToString() + "</span>";

    }

    protected void actionSelectGooglelicense(DropDownList ddlName, string rdoname, int valueddl)
    {

        data_googlelicense dataGoogle = new data_googlelicense();
        email_adonline_ggl_detail objgoogle = new email_adonline_ggl_detail();
        dataGoogle.email_adonline_ggl_list = new email_adonline_ggl_detail[1];

        objgoogle.costcenter_no = ViewState["CostNo"].ToString();

        if (valueddl == 1)
        {
            objgoogle.condition = 2;
        }
        else if (rdoname == "center" && valueddl == 2)
        {
            objgoogle.condition = 1;
        }
        else if (rdoname == "private" && valueddl == 2)
        {
            objgoogle.condition = 3;
        }

        dataGoogle.email_adonline_ggl_list[0] = objgoogle;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataGoogle));
        dataGoogle = callServiceGoogleLicense(_urlGetSelectMailCheckAd, dataGoogle);

        ViewState["countcost"] = dataGoogle.return_code;
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("กรุณาเลือกอีเมล์...", "0"));
        ddlName.DataSource = dataGoogle.email_adonline_ggl_list;
        ddlName.DataTextField = "name_license_gmail";
        ddlName.DataValueField = "name_license_gmail";
        ddlName.DataBind();

    }

    protected void actionAddMoreDataPerm(int id)
    {
        string valuerdo = string.Empty;

        data_adonline data_ad_addmore = new data_adonline();
        ad_online objADOnline = new ad_online();
        data_ad_addmore.ad_online_action = new ad_online[1];
        objADOnline.emp_idx = id;
        data_ad_addmore.ad_online_action[0] = objADOnline;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ad_addmore));
        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

        dataADOnline = callServiceADOnline(_urlADSelect_ADDMORE_DATA, dataADOnline);

        ViewState["CostNo"] = txtEmpNewCostCenterNo_Addmore.Text;

        if (dataADOnline.return_ad.ToString() != "0")
        {
            chkmoread.Checked = true;
            divmore_adonline.Visible = true;
            ddladonline_more.SelectedValue = dataADOnline.return_ad.ToString();
        }
        else
        {
            chkmoread.Checked = false;
            divmore_adonline.Visible = false;
            ddladonline_more.SelectedValue = "0";
        }

        if (dataADOnline.return_user.ToString() != "0")
        {
            chkmoreusersap.Checked = true;
            divmore_usersap.Visible = true;
            txtsapmore.Text = dataADOnline.return_user.ToString();
        }
        else
        {
            chkmoreusersap.Checked = false;
            divmore_usersap.Visible = false;
            txtsapmore.Text = String.Empty;
        }

        if (dataADOnline.return_typeemail.ToString() != "0")
        {
            chkmoreemail.Checked = true;
            divmore_email.Visible = true;
            divmore_emaildetail.Visible = true;

            if (dataADOnline.return_mailcenter.ToString() != "")
            {
                rdomail_more.SelectedValue = "1";
                valuerdo = "center";

                ddlemailmore.AppendDataBoundItems = true;
                ddlemailmore.Items.Clear();
                ddlemailmore.Items.Add(new ListItem("กรุณาเลือกประเภทอีเมล์ ...", "0"));
                ddlemailmore.Items.Add(new ListItem("เมล์ใหม่", "1"));
                ddlemailmore.Items.Add(new ListItem("เมล์เดิม", "2"));

                ddlemailmore.SelectedValue = dataADOnline.return_typeemail.ToString();
                if (ddlemailmore.SelectedValue == "1")
                {
                    divemailad_more.Visible = true;
                    txtemilmore.Text = dataADOnline.return_mailcenter.ToString();

                }
                else
                {
                    divemailad_more.Visible = false;
                    txtemilmore.Text = String.Empty;


                }
                divmore_emaildetail.Visible = true;
            }
            else if (dataADOnline.return_mailprivate.ToString() != "")
            {
                rdomail_more.SelectedValue = "2";
                valuerdo = "private";

                ddlemailmore.AppendDataBoundItems = true;
                ddlemailmore.Items.Clear();
                ddlemailmore.Items.Add(new ListItem("กรุณาเลือกประเภทอีเมล์ ....", "0"));
                ddlemailmore.Items.Add(new ListItem("เมล์ใหม่", "1"));
                ddlemailmore.Items.Add(new ListItem("เมล์ทดแทน", "2"));

                ddlemailmore.SelectedValue = dataADOnline.return_typeemail.ToString();
                txtemilmore.Text = dataADOnline.return_mailprivate.ToString();

                if (ddlemailmore.SelectedValue == "2")
                {
                    divemailad_more.Visible = true;
                    divemail1or3_more.Visible = true;
                }
                else
                {
                    divemailad_more.Visible = true;
                    divemail1or3_more.Visible = false;
                    ddlemail1or3_more.SelectedValue = "0";
                }

            }
            else
            {
                divmore_emaildetail.Visible = false;
            }

            //actionSelectGooglelicense(ddlemail1or3_more, valuerdo, int.Parse(dataADOnline.return_typeemail.ToString()));
            //ddlemail1or3_more.SelectedValue = dataADOnline.return_mailreplace.ToString();

            //txt.Text = dataADOnline.return_mailreplace.ToString();
        }
        else
        {
            divmore_emaildetail.Visible = false;
            divmore_email.Visible = false;
            chkmoreemail.Checked = false;
        }

        
    }

    protected void actionSendEmailtoGoogleLicense()
    {
        data_googlelicense _dtGoogle = new data_googlelicense();
        m0_gglholderemail_detail objGoogleOnline = new m0_gglholderemail_detail();
        _dtGoogle.m0_gglholderemail_list = new m0_gglholderemail_detail[1];

        objGoogleOnline.cemp_idx = int.Parse(lblValueU0PermPermEmpIdxCreator.Text);
        objGoogleOnline.m0_node_idx = 1;
        objGoogleOnline.m0_actor_idx = 1;
        objGoogleOnline.doc_decision = 0;
        objGoogleOnline.m0type_email_idx = int.Parse(ViewState["TypeMail"].ToString());
        objGoogleOnline.rsec_idx = int.Parse(lblValueU0PermPermRSecIDX_EmpIdx.Text);
        objGoogleOnline.emp_idx_holder = int.Parse(lblValueU0PermPermEmpIdx.Text);
        objGoogleOnline.emp_idx_check = lblValueU0PermPermEmpIdx.Text;
        objGoogleOnline.name_license_gmail = lblValueWaitingPermPermEmailText.Text;

        if (ViewState["TypeMail"].ToString() == "2")
        {
            objGoogleOnline.name_license_gmail_old = ViewState["Email_Replace"].ToString();
        }
        if (ViewState["u0_perm_perm_type_mail"].ToString() == "1")
        {
            objGoogleOnline.costcenter_no = lblValueWaitingPermPermCostCenter.Text;
        }


        _dtGoogle.m0_gglholderemail_list[0] = objGoogleOnline;

        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtGoogle));
        _dtGoogle = callServiceGoogleLicense(_urlSetHolderEmailFormAdOnline, _dtGoogle);

    }

    protected void actionSelectVPN(GridView GvName, int id)
    {
        dataADOnline = new data_adonline();
        ad_online objADOnline = new ad_online();
        dataADOnline.ad_online_action = new ad_online[1];

        objADOnline.u0_perm_perm_idx = id;

        dataADOnline.ad_online_action[0] = objADOnline;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline));

        dataADOnline = callServiceADOnline(_urlSelectVPNMAIN, dataADOnline);
        setGridData(GvName, dataADOnline.ad_online_action);
    }

    protected void actionSelectVpnCreate()
    {
        data_adonline dataADOnline_vpn = new data_adonline();
        ad_online objADOnline_vpn = new ad_online();
        dataADOnline_vpn.ad_online_action = new ad_online[1];

        //objADOnline_vpn.filter_u0_perm_temp_code_id = ViewState["filterIndexPermTempCodeId"].ToString();
        //objADOnline_vpn.filter_u0_perm_temp_creator = ViewState["filterIndexPermTempCreator"].ToString();

        dataADOnline_vpn.ad_online_action[0] = objADOnline_vpn;

        dataADOnline_vpn = callServiceADOnline(_urlSelectVPNMAIN, dataADOnline_vpn);
        setGridData(GvUseVPN, dataADOnline_vpn.ad_online_action);
    }

    protected void actionSelectVpnTemporaryCreate()
    {
        data_adonline dataADOnline_vpn = new data_adonline();
        ad_online objADOnline_vpn = new ad_online();
        dataADOnline_vpn.ad_online_action = new ad_online[1];

        //objADOnline_vpn.filter_u0_perm_temp_code_id = ViewState["filterIndexPermTempCodeId"].ToString();
        //objADOnline_vpn.filter_u0_perm_temp_creator = ViewState["filterIndexPermTempCreator"].ToString();

        dataADOnline_vpn.ad_online_action[0] = objADOnline_vpn;

        dataADOnline_vpn = callServiceADOnline(_urlSelectVPNMAIN, dataADOnline_vpn);
        setGridData(GvUseVPNTemporary, dataADOnline_vpn.ad_online_action);
    }

    
    #endregion Action

    protected void SelectPathFile(string u0_perm_perm_idx, string path, GridView gvName)
    {
        try
        {
            string filePathLotus = Server.MapPath(path + u0_perm_perm_idx);
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
            SearchDirectories(myDirLotus, u0_perm_perm_idx, gvName);
            gvName.Visible = true;

        }
        catch (Exception ex)
        {
            gvName.Visible = false;
            //txt.Text = ex.ToString();
        }
    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "_divMenuBtnToDivIndexPermPerm":
                activeMenu("index");
                changeMenuView("index", Constants.KEY_GET_FROM_PERM_PERM);
                break;

            case "_divMenuBtnToDivIndexPermTemp":
                activeMenu("index");
                changeMenuView("index", Constants.KEY_GET_FROM_PERM_TEMP);
                break;

            case "_divMenuBtnToDivCreate":
                activeMenu("create");
                changeMenuView("create");
                SetDefaultAdd();
                break;

            case "_divMenuBtnToDivAddmore":
                activeMenu("addmore");
                changeMenuView("addmore");
                SetDefaultAddMore();

                break;

            case "_divMenuBtnToDivWaitingPermPerm":
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                break;

            case "_divMenuBtnToDivWaitingPermTemp":
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_TEMP);
                break;

            case "btnFilterIndexPermPerm":
                ViewState["filterIndexPermPermCodeId"] = txtFilterIndexPermPermCodeId.Text.Trim();
                ViewState["filterIndexPermPermEmpCodeFullNameTHApplicant"] = txtFilterIndexPermPermEmpCodeFullNameTHApplicant.Text.Trim();
                ViewState["filterIndexPermPermOrg"] = ddlFilterIndexPermPermOrg.SelectedValue;
                ViewState["filterIndexPermPermDept"] = ddlFilterIndexPermPermDept.SelectedValue;
                ViewState["filterIndexPermPermSec"] = ddlFilterIndexPermPermSec.SelectedValue;
                ViewState["filterIndexPermPermPos"] = ddlFilterIndexPermPermPos.SelectedValue;
                ViewState["filterIndexPermPermNode"] = ddlFilterIndexPermPermNode.SelectedValue;
                ViewState["filterIndexPermPermConditionDate"] = ddlFilterIndexPermPermConditionDate.SelectedValue;
                ViewState["filterIndexPermPermDateOnly"] = txtFilterIndexPermPermOnlyHidden.Value;
                ViewState["filterIndexPermPermDateFrom"] = txtFilterIndexPermPermFromHidden.Value;
                ViewState["filterIndexPermPermDateTo"] = txtFilterIndexPermPermToHidden.Value;
                txtFilterIndexPermPermOnly.Text = ViewState["filterIndexPermPermDateOnly"].ToString();
                txtFilterIndexPermPermFrom.Text = ViewState["filterIndexPermPermDateFrom"].ToString();
                txtFilterIndexPermPermTo.Text = ViewState["filterIndexPermPermDateTo"].ToString();
                actionIndexPermPerm();
                break;

            case "btnResetFilterIndexPermPerm":
                txtFilterIndexPermPermCodeId.Text = string.Empty;
                txtFilterIndexPermPermEmpCodeFullNameTHApplicant.Text = string.Empty;
                ddlFilterIndexPermPermOrg.SelectedValue = string.Empty;
                ddlFilterIndexPermPermDept.SelectedValue = string.Empty;
                ddlFilterIndexPermPermSec.SelectedValue = string.Empty;
                ddlFilterIndexPermPermPos.SelectedValue = string.Empty;
                ddlFilterIndexPermPermNode.SelectedValue = string.Empty;
                ddlFilterIndexPermPermConditionDate.SelectedValue = string.Empty;
                txtFilterIndexPermPermOnlyHidden.Value = string.Empty;
                txtFilterIndexPermPermFromHidden.Value = string.Empty;
                txtFilterIndexPermTempToHidden.Value = string.Empty; ;
                ViewState["filterIndexPermPermCodeId"] = string.Empty;
                ViewState["filterIndexPermPermEmpCodeFullNameTHApplicant"] = string.Empty;
                ViewState["filterIndexPermPermOrg"] = string.Empty;
                ViewState["filterIndexPermPermDept"] = string.Empty;
                ViewState["filterIndexPermPermSec"] = string.Empty;
                ViewState["filterIndexPermPermPos"] = string.Empty;
                ViewState["filterIndexPermPermNode"] = string.Empty;
                ViewState["filterIndexPermPermConditionDate"] = string.Empty;
                ViewState["filterIndexPermPermDateOnly"] = string.Empty;
                ViewState["filterIndexPermPermDateFrom"] = string.Empty;
                ViewState["filterIndexPermPermDateTo"] = string.Empty;
                txtFilterIndexPermPermOnly.Text = ViewState["filterIndexPermPermDateOnly"].ToString();
                txtFilterIndexPermPermFrom.Text = ViewState["filterIndexPermPermDateFrom"].ToString();
                txtFilterIndexPermPermTo.Text = ViewState["filterIndexPermPermDateTo"].ToString();
                actionIndexPermPerm();
                scrollToTop();
                break;

            case "btnFilterIndexPermTemp":
                ViewState["filterIndexPermTempCodeId"] = txtFilterIndexPermTempCodeId.Text.Trim();
                ViewState["filterIndexPermTempCreator"] = txtFilterIndexPermTempCreatorName.Text.Trim();
                ViewState["filterIndexPermTempCompName"] = txtFilterIndexPermTempCompName.Text.Trim();
                ViewState["filterIndexPermTempContact"] = txtFilterIndexPermTempContact.Text.Trim();
                ViewState["filterIndexPermTempOrg"] = ddlFilterIndexPermTempOrg.SelectedValue;
                ViewState["filterIndexPermTempDept"] = ddlFilterIndexPermTempDept.SelectedValue;
                ViewState["filterIndexPermTempNode"] = ddlFilterIndexPermTempNode.SelectedValue;
                ViewState["filterIndexPermTempConditionDate"] = ddlFilterIndexPermTempConditionDate.SelectedValue;
                ViewState["filterIndexPermTempDateOnly"] = txtFilterIndexPermTempOnlyHidden.Value;
                ViewState["filterIndexPermTempDateFrom"] = txtFilterIndexPermTempFromHidden.Value;
                ViewState["filterIndexPermTempDateTo"] = txtFilterIndexPermTempToHidden.Value;
                txtFilterIndexPermTempOnly.Text = ViewState["filterIndexPermTempDateOnly"].ToString();
                txtFilterIndexPermTempFrom.Text = ViewState["filterIndexPermTempDateFrom"].ToString();
                txtFilterIndexPermTempTo.Text = ViewState["filterIndexPermTempDateTo"].ToString();
                actionIndexPermTemp();
                break;

            case "btnResetFilterIndexPermTemp":
                txtFilterIndexPermTempCodeId.Text = string.Empty;
                txtFilterIndexPermTempCreatorName.Text = string.Empty;
                txtFilterIndexPermTempCompName.Text = string.Empty;
                txtFilterIndexPermTempContact.Text = string.Empty;
                ddlFilterIndexPermTempOrg.SelectedValue = string.Empty;
                ddlFilterIndexPermTempDept.SelectedValue = string.Empty;
                ddlFilterIndexPermTempNode.SelectedValue = string.Empty;
                ddlFilterIndexPermTempConditionDate.SelectedValue = string.Empty;
                txtFilterIndexPermTempOnlyHidden.Value = string.Empty;
                txtFilterIndexPermTempFromHidden.Value = string.Empty;
                txtFilterIndexPermTempToHidden.Value = string.Empty;
                ViewState["filterIndexPermTempCodeId"] = string.Empty;
                ViewState["filterIndexPermTempCreator"] = string.Empty;
                ViewState["filterIndexPermTempCompName"] = string.Empty;
                ViewState["filterIndexPermTempContact"] = string.Empty;
                ViewState["filterIndexPermTempOrg"] = string.Empty;
                ViewState["filterIndexPermTempDept"] = string.Empty;
                ViewState["filterIndexPermTempNode"] = string.Empty;
                ViewState["filterIndexPermTempConditionDate"] = string.Empty;
                ViewState["filterIndexPermTempDateOnly"] = string.Empty;
                ViewState["filterIndexPermTempDateFrom"] = string.Empty;
                ViewState["filterIndexPermTempDateTo"] = string.Empty;
                txtFilterIndexPermTempOnly.Text = ViewState["filterIndexPermTempDateOnly"].ToString();
                txtFilterIndexPermTempFrom.Text = ViewState["filterIndexPermTempDateFrom"].ToString();
                txtFilterIndexPermTempTo.Text = ViewState["filterIndexPermTempDateTo"].ToString();
                actionIndexPermTemp();
                scrollToTop();
                break;

            case "btnInsertPermPerm":
                linkBtnTrigger(btnInsertPermPerm);
                actionCreatePermPerm();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;


            case "btnInsertPermPerm_More":
                actionCreatePermPerm_More();

                break;

            case "btnIndexPermPerm_ViewDetail":
                string getPath = ConfigurationSettings.AppSettings["path_file_adonline_perm"];
                string idx = "PERM-" + cmdArg.ToString();
                _divIndexPermPerm.Visible = false;
                _divIndexPermPerm_Detail.Visible = true;
                actionIndexPermPermDetail(int.Parse(cmdArg.ToString()));
                actionSelectVPN(GvShowVPNPerm, int.Parse(cmdArg.ToString()));
                getSetPermPolIntoToolTip(lblValueIndexU0PermPermPolEx);
                getSubCommentIT(int.Parse(cmdArg.ToString()), divIndexU1PermPermSubCommentIT, rptIndexPermPermSubCmIT);
                SelectPathFile(idx, getPath, gvFile);


                scrollToTop();
                break;

            case "btnIndexPermTemp_ViewDetail":
                string getPath_temp = ConfigurationSettings.AppSettings["path_file_adonline_temp"];
                string idx_temp = "TEMP-" + cmdArg.ToString();
                _divIndexPermTemp.Visible = false;
                _divIndexPermTemp_Detail.Visible = true;
                actionIndexPermTempDetail(int.Parse(cmdArg.ToString()));
                actionSelectVPN(GvShowVPNTemp, int.Parse(cmdArg.ToString())); 
                SelectPathFile(idx_temp, getPath_temp, gvFileTemp);

                scrollToTop();
                break;

            case "btnWaitingPermPerm_ViewDetail":
                string getPath_waitperm = ConfigurationSettings.AppSettings["path_file_adonline_perm"];
                string idx_waitperm = "PERM-" + cmdArg.ToString();

                _divWaitingPermPerm.Visible = false;
                _divWaitingPermPerm_Detail.Visible = true;
                actionWaitingPermPermDetail(int.Parse(cmdArg.ToString()));
                actionSelectVPN(GvEditVPN, int.Parse(cmdArg.ToString()));

                getSetPermPolToDDL(ddlWaitingPermPermPol);
                getSetPermPolIntoToolTip(lblWaitingPermPermToolTipPol);
                getSetPermPolIntoToolTip(lblValueWaitingPermPermPolEx);
                getSubCommentIT(int.Parse(cmdArg.ToString()), divWaitingU1PermPermSubCommentIT, rptWaitingPermPermSubCmIT);

                SelectPathFile(idx_waitperm, getPath_waitperm, gvFilePerm_Wait);

                scrollToTop();


                break;

            case "btnWaitingPermTemp_ViewDetail":
                string getPath_waittemp = ConfigurationSettings.AppSettings["path_file_adonline_temp"];
                string idx_waittemp = "TEMP-" + cmdArg.ToString();

                _divWaitingPermTemp.Visible = false;
                _divWaitingPermTemp_Detail.Visible = true;
                actionWaitingPermTempDetail(int.Parse(cmdArg.ToString()));
                actionSelectVPN(GvEditVPNTemp, int.Parse(cmdArg.ToString()));
                SelectPathFile(idx_waittemp, getPath_waittemp, gvFileTemp_Wait);

                scrollToTop();
                break;

            case "btnBack_ToDivIndexPermPerm":
                _divIndexPermPerm.Visible = !_divIndexPermPerm.Visible;
                _divIndexPermPerm_Detail.Visible = !_divIndexPermPerm_Detail.Visible;
                break;

            case "btnBack_ToDivIndexPermTemp":
                _divIndexPermTemp.Visible = !_divIndexPermTemp.Visible;
                _divIndexPermTemp_Detail.Visible = !_divIndexPermTemp_Detail.Visible;
                break;

            case "btnBack_ToDivWaitingPermPerm":
                _divWaitingPermPerm.Visible = !_divWaitingPermPerm.Visible;
                _divWaitingPermPerm_Detail.Visible = !_divWaitingPermPerm_Detail.Visible;
                break;

            case "btnBack_ToDivWaitingPermTemp":
                _divWaitingPermTemp.Visible = !_divWaitingPermTemp.Visible;
                _divWaitingPermTemp_Detail.Visible = !_divWaitingPermTemp_Detail.Visible;
                break;

            case "btnInsertPermTemp":
                actionCreatePermTemp();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            #region Button approve permission temporary
            case "btnWaitingPermTempAppDirectorOfCreator":
                actionWaitingPermTempApprove(int.Parse(lblValueU0PermTempIdx.Text), Constants.NODE_PERM_TEMP_APP_GOTO_HEADER_IT, txtWaitingPermTempCommentDirectorOfCreator.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_TEMP);
                gvWaitingPermTemp.EditIndex = -1;
                actionWaitingPermTemp();
                scrollToTop();
                actionSetDefault();
                break;

            case "btnWaitingPermTempNotAppDirectorOfCreator":
                actionWaitingPermTempApprove(int.Parse(lblValueU0PermTempIdx.Text), Constants.NODE_PERM_TEMP_NOTAPP_ENDBY_DIRECTOR_CREATOR, txtWaitingPermTempCommentDirectorOfCreator.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_TEMP);
                gvWaitingPermTemp.EditIndex = -1;
                actionWaitingPermTemp();
                scrollToTop();
                actionSetDefault();
                break;

            case "btnWaitingPermTempAppHeaderIT":
                actionWaitingPermTempApprove(int.Parse(lblValueU0PermTempIdx.Text), Constants.NODE_PERM_TEMP_APP_GOTO_DIRECTOR_IT, txtWaitingPermTempCommentHeaderIT.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_TEMP);
                gvWaitingPermTemp.EditIndex = -1;
                actionWaitingPermTemp();
                scrollToTop();
                actionSetDefault();
                break;

            case "btnWaitingPermTempNotAppHeaderIT":
                actionWaitingPermTempApprove(int.Parse(lblValueU0PermTempIdx.Text), Constants.NODE_PERM_TEMP_NOTAPP_ENDBY_HEADER_IT, txtWaitingPermTempCommentHeaderIT.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_TEMP);
                gvWaitingPermTemp.EditIndex = -1;
                actionWaitingPermTemp();
                scrollToTop();
                actionSetDefault();
                break;

            case "btnWaitingPermTempAppDirectorIT":
                actionWaitingPermTempApprove(int.Parse(lblValueU0PermTempIdx.Text), Constants.NODE_PERM_TEMP_APP_GOTO_IT, txtWaitingPermTempCommentDirectorIT.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_TEMP);
                gvWaitingPermTemp.EditIndex = -1;
                actionWaitingPermTemp();
                scrollToTop();
                actionSetDefault();
                break;

            case "btnWaitingPermTempNotAppDirectorIT":
                actionWaitingPermTempApprove(int.Parse(lblValueU0PermTempIdx.Text), Constants.NODE_PERM_TEMP_NOTAPP_ENDBY_DIRECTOR_IT, txtWaitingPermTempCommentDirectorIT.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_TEMP);
                gvWaitingPermTemp.EditIndex = -1;
                actionWaitingPermTemp();
                scrollToTop();
                actionSetDefault();
                break;

            case "btnWaitingPermTempSaveIT":
                actionWaitingPermTempSave(int.Parse(lblValueU0PermTempIdx.Text), Constants.NODE_PERM_TEMP_SAVE_ENDBY_IT);
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_TEMP);
                gvWaitingPermTemp.EditIndex = -1;
                actionWaitingPermTemp();
                scrollToTop();
                actionSetDefault();
                break;
            #endregion Button approve permission temporary

            #region Button approve permission permanent
            case "btnWaitingPermPermApproveHeaderEmpNew":
                actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.node_perm_perm_director_empnew_app_goto_it, txtWaitingPermPermHeaderEmpNewCommentText.Text.Trim());

                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermNotApproveHeaderEmpNew":
                actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.NODE_PERM_PERM_HEADER_EMPNEW_NOTAPP_ENDBY_HEADER_EMPNEW, txtWaitingPermPermHeaderEmpNewCommentText.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;


            case "btnWaitingPermPermEditHeaderEmpNew":
                actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.node_perm_perm_header_empnew_sendedit_user, txtWaitingPermPermHeaderEmpNewCommentText.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;


            case "btnWaitingPermPermApproveDirectorEmpNew":
                actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.node_perm_perm_director_empnew_app_goto_it, txtWaitingPermPermDirectorEmpNewCommentText.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermNotApproveDirectorEmpNew":
                actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.NODE_PERM_PERM_DIRECTOR_EMPNEW_NOTAPP_ENDBY_DIRECTOR_EMPNEW, txtWaitingPermPermDirectorEmpNewCommentText.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermApproveDirectorHR":
                actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.node_perm_perm_director_empnew_app_goto_it, txtWaitingPermPermDirectorHRCommentText.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermNotApproveDirectorHR":
                actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.NODE_PERM_PERM_DIRECTOR_HR_NOTAPP_ENDBY_DIRECTOR_HR, txtWaitingPermPermDirectorHRCommentText.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermApproveHeaderIT":
                actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.NODE_PERM_PERM_HEADER_IT_APP_GOTO_DIRECTOR_IT, txtWaitingPermPermHeaderITCommentText.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermNotApproveHeaderIT":
                actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.NODE_PERM_PERM_HEADER_IT_NOTAPP_ENDBY_HEADER_IT, txtWaitingPermPermHeaderITCommentText.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermApproveDirectorIT":
                actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.NODE_PERM_PERM_DIRECTOR_IT_APP_GOTO_IT, txtWaitingPermPermDirectorITCommentText.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermNotApproveDirectorIT":
                actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.NODE_PERM_PERM_DIRECTOR_IT_NOTAPP_ENDBY_DIRECTOR_IT, txtWaitingPermPermDirectorITCommentText.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermApproveIT":
                if (int.Parse(lblValueU0PermPermNodeCurrent.Text) == 17)
                {
                    actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.node_perm_perm_it_app_goto_headit, txtWaitingPermPermITCommentText.Text.Trim());
                }
                else
                {
                    actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.NODE_PERM_PERM_IT_APP_FINISHBY_IT, txtWaitingPermPermITCommentText.Text.Trim());
                    actionSendEmailtoGoogleLicense();
                }


                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermNotApproveIT":
                if (int.Parse(lblValueU0PermPermNodeCurrent.Text) == 17)
                {
                    actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.node_perm_perm_it_notapp_endbyprocess_it, txtWaitingPermPermDirectorITCommentText.Text.Trim());

                }
                else
                {
                    actionWaitingPermPermApprove(int.Parse(lblValueU0PermPermIdx.Text), int.Parse(lblValueU0PermPermNodeCurrent.Text), Constants.NODE_PERM_PERM_IT_NOTAPP_ENDBY_IT, txtWaitingPermPermDirectorITCommentText.Text.Trim());

                }
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermBackIT":
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermSaveCommentIT":
                actionWaitingPermPermSaveComment(int.Parse(lblValueU0PermPermIdx.Text), txtWaitingPermPermITCommentText.Text.Trim());
                activeMenu("waiting");
                changeMenuView("waiting", Constants.KEY_GET_FROM_PERM_PERM);
                gvWaitingPermPerm.EditIndex = -1;
                actionWaitingPermPerm();
                scrollToTop();
                actionSetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermEditUser":
                actionWaitingPermPermApproveEditUser(int.Parse(lblValueU0PermPermIdx.Text), Constants.NODE_PERM_PERM_Send_Back_USER, 0);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermEditUser_EmpNew":
                actionWaitingPermPermApproveEditUser(int.Parse(lblValueU0PermPermIdx.Text), Constants.node_perm_perm_header_empnew_sendedit_user, 0);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnWaitingPermPermEditUser_DirectorEmpNew":
                actionWaitingPermPermApproveEditUser(int.Parse(lblValueU0PermPermIdx.Text), Constants.node_perm_perm_director_empnew_sendedit_user, 0);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnWaitingPermPermEditUser_DirectorHR":
                actionWaitingPermPermApproveEditUser(int.Parse(lblValueU0PermPermIdx.Text), Constants.node_perm_perm_director_hr_sendedit_user, 0);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnWaitingPermPermEditUser_IT":
                actionWaitingPermPermApproveEditUser(int.Parse(lblValueU0PermPermIdx.Text), Constants.node_perm_perm_it_sendedit_user, 0);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnWaitingPermPermEditUser_HeaderIT":
                actionWaitingPermPermApproveEditUser(int.Parse(lblValueU0PermPermIdx.Text), Constants.node_perm_perm_headerit_sendedit_user, 0);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;
            case "btnWaitingPermPermEditUser_DirectorIT":
                actionWaitingPermPermApproveEditUser(int.Parse(lblValueU0PermPermIdx.Text), Constants.node_perm_perm_directorit_sendedit_user, 0);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;
            #endregion Button approve permission permanent

            case "btnAddContactToList":
                setContactsList();
                break;

            case "btnIndexPermTempShowFilter":
                _divIndexPermTemp_FilterTool.Visible = true;
                btnIndexPermTempShowFilter.Visible = false;
                btnIndexPermTempHideFilter.Visible = true;
                getODSPToDDL(ddlFilterIndexPermTempOrg, ddlFilterIndexPermTempDept);
                getNodeToDDL(ddlFilterIndexPermTempNode);
                break;

            case "btnIndexPermTempHideFilter":
                _divIndexPermTemp_FilterTool.Visible = false;
                btnIndexPermTempShowFilter.Visible = true;
                btnIndexPermTempHideFilter.Visible = false;
                break;

            case "btnIndexPermPermShowFilter":
                _divIndexPermPerm_FilterTool.Visible = true;
                btnIndexPermPermShowFilter.Visible = false;
                btnIndexPermPermHideFilter.Visible = true;
                getODSPToDDL(ddlFilterIndexPermPermOrg, ddlFilterIndexPermPermDept, ddlFilterIndexPermPermSec, ddlFilterIndexPermPermPos);
                getNodeToDDL(ddlFilterIndexPermPermNode);
                break;

            case "btnIndexPermPermHideFilter":
                _divIndexPermPerm_FilterTool.Visible = false;
                btnIndexPermPermShowFilter.Visible = true;
                btnIndexPermPermHideFilter.Visible = false;
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void setGridData(GridView gvId, Object obj)
    {
        gvId.DataSource = obj;
        gvId.DataBind();
    }

    protected void setRepeaterData(Repeater rptId, Object obj)
    {
        rptId.DataSource = obj;
        rptId.DataBind();
    }

    protected void setDefaultVsFilterIndexPermPerm()
    {
        ViewState["filterIndexPermPermCodeId"] = string.Empty;
        ViewState["filterIndexPermPermEmpCodeFullNameTHApplicant"] = string.Empty;
        ViewState["filterIndexPermPermOrg"] = string.Empty;
        ViewState["filterIndexPermPermDept"] = string.Empty;
        ViewState["filterIndexPermPermSec"] = string.Empty;
        ViewState["filterIndexPermPermPos"] = string.Empty;
        ViewState["filterIndexPermPermNode"] = string.Empty;
        ViewState["filterIndexPermPermConditionDate"] = string.Empty;
        ViewState["filterIndexPermPermDateOnly"] = string.Empty;
        ViewState["filterIndexPermPermDateFrom"] = string.Empty;
        ViewState["filterIndexPermPermDateTo"] = string.Empty;
    }

    protected void setDefaultVsFilterIndexPermTemp()
    {
        ViewState["filterIndexPermTempCodeId"] = string.Empty;
        ViewState["filterIndexPermTempCreator"] = string.Empty;
        ViewState["filterIndexPermTempCompName"] = string.Empty;
        ViewState["filterIndexPermTempContact"] = string.Empty;
        ViewState["filterIndexPermTempOrg"] = string.Empty;
        ViewState["filterIndexPermTempDept"] = string.Empty;
        ViewState["filterIndexPermTempNode"] = string.Empty;
        ViewState["filterIndexPermTempConditionDate"] = string.Empty;
        ViewState["filterIndexPermTempDateOnly"] = string.Empty;
        ViewState["filterIndexPermTempDateFrom"] = string.Empty;
        ViewState["filterIndexPermTempDateTo"] = string.Empty;
    }

    protected void getPermissionTypeToRDO(RadioButtonList rdolId)
    {
        permission_type objADOnline = new permission_type();
        dataADOnline.ad_permission_type_action = new permission_type[1];
        dataADOnline.ad_permission_type_action[0] = objADOnline;
        dataADOnline = callServiceADOnline(_urlReadPermType, dataADOnline);
        rdolId.Items.Clear();
        rdolId.AppendDataBoundItems = true;
        rdolId.Items.Add(new ListItem("สิทธิ์ทั่วไป", "2"));
        rdolId.DataSource = dataADOnline.ad_permission_type_action;
        rdolId.DataTextField = "perm_type_name";
        rdolId.DataValueField = "m0_perm_type_idx";
        rdolId.DataBind();
    }

    protected void getSubCommentIT(int id, Control divId, Repeater rptId)
    {
        data_adonline dataADOnline_ = new data_adonline();
        ad_online objADOnline = new ad_online();
        dataADOnline_.ad_online_action = new ad_online[1];
        objADOnline.on_table = Constants.KEY_GET_FROM_PERM_PERM;
        objADOnline.u0_perm_perm_idx_ref = id;
        dataADOnline_.ad_online_action[0] = objADOnline;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline_));


        dataADOnline_ = callServiceADOnline(_urlReadU1WaitingPermPerm, dataADOnline_);
        if (dataADOnline_.return_code == 404)
        {
            divId.Visible = false;
        }
        else
        {
            divId.Visible = true;
            setRepeaterData(rptId, dataADOnline_.ad_online_action);
        }
    }

    protected void getEmpProfile(int empId)
    {
        /* 
         * var "dataADOnline" at last line of this function for using into the "setEmpProfile" function. 
         */
        ad_online objADOnline = new ad_online();
        dataADOnline.ad_online_action = new ad_online[1];
        objADOnline.emp_idx = empId;
        dataADOnline.ad_online_action[0] = objADOnline;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline));
        dataADOnline = callServiceADOnline(_urlReadEmpProfile, dataADOnline);
    }

    protected void setEmpProfile(TextBox txtEmp, TextBox txtFullName, TextBox txtOrgName, TextBox txtDeptName)
    {
        txtEmp.Text = dataADOnline.ad_online_action[0].emp_code;
        txtFullName.Text = dataADOnline.ad_online_action[0].emp_name_th;
        txtOrgName.Text = dataADOnline.ad_online_action[0].org_name_th;
        txtDeptName.Text = dataADOnline.ad_online_action[0].dept_name_th;
    }

    protected void getODSPToDDL(DropDownList ddlOrgId, DropDownList ddlDeptId = null, DropDownList ddlSecId = null, DropDownList ddlPosId = null)
    {
        if (ddlOrgId.Items.Count == 0)
        {
            organization_details objEmployee = new organization_details();
            dataEmployee.organization_list = new organization_details[1];
            dataEmployee.organization_list[0] = objEmployee;
            dataEmployee = callServiceEmployee(_urlGetOrganizationList, dataEmployee);
            ddlOrgId.Items.Clear();
            ddlOrgId.AppendDataBoundItems = true;
            ddlOrgId.Items.Add(new ListItem("-- เลือกบริษัท— --", string.Empty));
            ddlOrgId.DataSource = dataEmployee.organization_list;
            ddlOrgId.DataTextField = "org_name_th";
            ddlOrgId.DataValueField = "org_idx";
            ddlOrgId.DataBind();

            if (ddlDeptId != null)
            {
                ddlDeptId.Items.Clear();
                ddlDeptId.AppendDataBoundItems = true;
                ddlDeptId.Items.Add(new ListItem("-- เลือกฝ่าย --", string.Empty));
            }

            if (ddlSecId != null)
            {
                ddlSecId.Items.Clear();
                ddlSecId.AppendDataBoundItems = true;
                ddlSecId.Items.Add(new ListItem("-- เลือกแผนก --", string.Empty));
            }

            if (ddlPosId != null)
            {
                ddlPosId.Items.Clear();
                ddlPosId.AppendDataBoundItems = true;
                ddlPosId.Items.Add(new ListItem("-- เลือกตำแหน่ง --", string.Empty));
            }
        }
    }

    protected void getNodeToDDL(DropDownList ddlId)
    {
        if (ddlId.Items.Count == 0)
        {
            ad_crud_node objCRUDNode = new ad_crud_node();
            dataADOnline.ad_crud_node_action = new ad_crud_node[1];
            objCRUDNode.from_table = "node";
            dataADOnline.ad_crud_node_action[0] = objCRUDNode;
            dataADOnline = callServiceADOnline(_urlReadNode, dataADOnline);
            ddlId.Items.Clear();
            ddlId.AppendDataBoundItems = true;
            ddlId.Items.Add(new ListItem("-- เลือกสถานะรายการ --", string.Empty));
            ddlId.DataSource = dataADOnline.ad_crud_node_action;
            ddlId.DataTextField = "node_name";
            ddlId.DataValueField = "m0_node_idx";
            ddlId.DataBind();
        }
    }

    protected void getSetPermPolToDDL(DropDownList ddlId)
    {
        if (ddlId.Items.Count == 0)
        {
            permission_policy objPermPol = new permission_policy();
            dataADOnline.ad_permission_policy_action = new permission_policy[1];
            dataADOnline.ad_permission_policy_action[0] = objPermPol;
            dataADOnline = callServiceADOnline(_urlReadPermPol, dataADOnline);
            ddlId.Items.Clear();
            ddlId.AppendDataBoundItems = true;
            ddlId.Items.Add(new ListItem("กรุณาเลือกระดับสิทธิ์ AD ...", "0"));
            ddlId.DataSource = dataADOnline.ad_permission_policy_action;
            ddlId.DataTextField = "perm_pol_name";
            ddlId.DataValueField = "m0_perm_pol_idx";
            ddlId.DataBind();
        }
    }

    protected void getContactsList()
    {
        DataSet dsContactPermTemp = new DataSet();
        dsContactPermTemp.Tables.Add("dsContactPermTempTable");
        dsContactPermTemp.Tables["dsContactPermTempTable"].Columns.Add("drContactPermTempIDCard", typeof(String));
        //dsContactPermTemp.Tables["dsContactPermTempTable"].Columns.Add("drContactPermTempIDCardForeign", typeof(String));
        dsContactPermTemp.Tables["dsContactPermTempTable"].Columns.Add("drContactPermTempFullNameTH", typeof(String));
        dsContactPermTemp.Tables["dsContactPermTempTable"].Columns.Add("drContactPermTempStart", typeof(String));
        dsContactPermTemp.Tables["dsContactPermTempTable"].Columns.Add("drContactPermTempEnd", typeof(String));
        ViewState["vsContactPermTempList"] = dsContactPermTemp;
    }

    protected void setContactsList()
    {
        if (ViewState["vsContactPermTempList"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            DataSet dsContacts = (DataSet)ViewState["vsContactPermTempList"];
            if (dsContacts.Tables["dsContactPermTempTable"].Rows.Count < Constants.PERM_TEMP_MAX_CONTACT)
            {
                foreach (DataRow dr in dsContacts.Tables["dsContactPermTempTable"].Rows)
                {
                    if (dr["drContactPermTempIDCard"].ToString() == txtEmpVisitorIDCard.Text.Trim() || dr["drContactPermTempIDCard"].ToString() == txtEmpVisitorIDCardForeign.Text.Trim())
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เลขบัตรประชาชน หรือ เลขบัตรประจำตัวคนต่างชาติ นี้มีในรายชื่อผู้มาติดต่อแล้ว');", true);
                        return;
                    }

                    //if (dr["drContactPermTempIDCardForeign"].ToString() == txtEmpVisitorIDCardForeign.Text.Trim())
                    //{
                    //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เลขบัตรประจำตัวคนต่างชาตินี้มีในรายชื่อผู้มาติดต่อแล้ว');", true);
                    //    return;
                    //}
                }
                DataRow drContacts = dsContacts.Tables["dsContactPermTempTable"].NewRow();
                gvEmpVisitorContact.Visible = true;
                litNoresultGvEmpVisitorContact.Visible = false;

                if(txtEmpVisitorIDCard.Text.Trim() != "")
                {
                    drContacts["drContactPermTempIDCard"] = idCardPattern(txtEmpVisitorIDCard.Text.Trim(), true);
                }
                
                if(txtEmpVisitorIDCardForeign.Text.Trim() != "")
                {
                    drContacts["drContactPermTempIDCard"] = txtEmpVisitorIDCardForeign.Text.Trim();
                }

                drContacts["drContactPermTempFullNameTH"] = txtEmpVisitorFullName.Text.Trim();
                drContacts["drContactPermTempStart"] = txtSearchFromHidden.Value.Trim() == String.Empty ? DateTime.Now.ToString("dd/MM/yyyy") : txtSearchFromHidden.Value.Trim();
                drContacts["drContactPermTempEnd"] = txtSearchToHidden.Value.Trim() == String.Empty ? DateTime.Now.ToString("dd/MM/yyyy") : txtSearchToHidden.Value.Trim();
                dsContacts.Tables["dsContactPermTempTable"].Rows.Add(drContacts);
                ViewState["vsContactPermTempList"] = dsContacts;
                setGridData(gvEmpVisitorContact, dsContacts.Tables["dsContactPermTempTable"]);
                txtEmpVisitorIDCard.Text = string.Empty;
                txtEmpVisitorFullName.Text = string.Empty;
                txtSearchFrom.Text = txtSearchFromHidden.Value;
                txtSearchTo.Text = txtSearchToHidden.Value;
                //Div_UsevpnTemporary.Visible = true;
                //actionSelectVpnTemporaryCreate();
                btnInsertPermTemp.Visible = true;

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('รายชื่อผู้มาติดต่อครบ {0} คนแล้วไม่สามารถเพิ่มได้อีก');", dsContacts.Tables["dsContactPermTempTable"].Rows.Count.ToString()), true);
            }
        }
    }

    protected void onSelectedIndexChanged(Object sender, EventArgs e)
    {
        if (sender is RadioButtonList)
        {
            RadioButtonList radioButtonList = (RadioButtonList)sender;


            switch (radioButtonList.ID)
            {
                case "rdoPermissionType":

                    // set clear value //

                    //tab ทั่วไป
                    rdomail.ClearSelection();
                    

                    //tab ชั่วคราว
                    rdotype_visitor.ClearSelection();
                    _DivVisitorIDCard.Visible = false;
                    txtEmpVisitorIDCard.Text = String.Empty;
                    _DivVisitorIDCardForeign.Visible = false;
                    txtEmpVisitorIDCardForeign.Text = String.Empty;

                    txtEmpVisitorFullName.Text = String.Empty;
                    txtEmpVisitorCompany.Text = String.Empty;
                    txtEmpVisitorReason.Text = String.Empty;
                    // set clear value //

                    linkBtnTrigger(_divMenuBtnToDivCreate);
                    linkBtnTrigger(btnInsertPermPerm);

                    if (radioButtonList.SelectedValue == Constants.RADIO_PERM_PERM.ToString())
                    {

                        _divCreate_PermPerm.Visible = true;
                        _divCreate_PermTemp.Visible = false;
                        getSetPermPolToDDL(ddladonline);



                        Div_Usevpn.Visible = true;
                        actionSelectVpnCreate();
                        getSetPermPolIntoToolTip(lblshowad);

                        Div_UsevpnTemporary.Visible = false;
                        setGridData(GvUseVPNTemporary, null);


                    }
                    else if (radioButtonList.SelectedValue == Constants.RADIO_PERM_TEMP.ToString())
                    {
                        _divCreate_PermPerm.Visible = false;
                        _divCreate_PermTemp.Visible = true;

                        Div_UsevpnTemporary.Visible = true;
                        actionSelectVpnTemporaryCreate();

                        Div_Usevpn.Visible = false;
                        setGridData(GvUseVPN, null);

                        lblAmountContact.Text = string.Format("สูงสุด {0} รายชื่อ", Constants.PERM_TEMP_MAX_CONTACT.ToString());
                    }
                    break;

                case "rdoWaitingPermPermEmailType":
                    divemail_edit.Visible = true;
                    if (radioButtonList.SelectedValue == "center")
                    {
                        ddlWaitingPermPermEmailType.AppendDataBoundItems = true;
                        ddlWaitingPermPermEmailType.Items.Clear();
                        ddlWaitingPermPermEmailType.Items.Add(new ListItem("กรุณาเลือกประเภทอีเมล์ ....", "0"));
                        ddlWaitingPermPermEmailType.Items.Add(new ListItem("เมล์ใหม่", "1"));
                        ddlWaitingPermPermEmailType.Items.Add(new ListItem("เมล์เดิม", "2"));

                        divemail.Visible = false;
                        divemail_edit.Visible = true;
                        if (lblValueWaitingPermPermEmailText.Text != string.Empty && lblValueWaitingPermPermEmailType.Text == "Email กลาง")
                        {
                            string[] splitEmail = lblValueWaitingPermPermEmailText.Text.Split('@');
                            txtWaitingPermPermEmailText.Text = splitEmail[0].ToLower();
                        }
                        else
                        {
                            txtWaitingPermPermEmailText.Text = string.Empty;
                        }
                    }
                    else if (radioButtonList.SelectedValue == "private")
                    {

                        ddlWaitingPermPermEmailType.AppendDataBoundItems = true;
                        ddlWaitingPermPermEmailType.Items.Clear();
                        ddlWaitingPermPermEmailType.Items.Add(new ListItem("กรุณาเลือกประเภทอีเมล์ ....", "0"));
                        ddlWaitingPermPermEmailType.Items.Add(new ListItem("เมล์ใหม่", "1"));
                        ddlWaitingPermPermEmailType.Items.Add(new ListItem("เมล์ทดแทน", "2"));

                        divemail.Visible = false;
                        txtWaitingPermPermEmailText.Text = String.Empty;

                        if (lblValueWaitingPermPermEmailText.Text != string.Empty && lblValueWaitingPermPermEmailType.Text == "Email ส่วนตัว")
                        {
                            string[] splitEmail = lblValueWaitingPermPermEmailText.Text.Split('@');
                            txtWaitingPermPermEmailText.Text = splitEmail[0].ToLower();
                        }
                        else
                        {
                            if (lblValueU0PermPermFnameEN.Text != string.Empty && lblValueU0PermPermLnameEN.Text != string.Empty)
                            {
                                string removeWhiteSpace = lblValueU0PermPermLnameEN.Text.Replace(" ", string.Empty);
                                string firstCharacter = removeWhiteSpace.Substring(0, 3);
                                string privateEmail = string.Format("{0}.{1}", lblValueU0PermPermFnameEN.Text, firstCharacter);
                                txtWaitingPermPermEmailText.Text = privateEmail.ToLower();
                            }
                            else
                            {
                                txtWaitingPermPermEmailText.Text = string.Empty;
                            }
                        }
                    }
                    break;

                case "rdomail":
                    if (rdomail.SelectedValue == "center")
                    {
                        ddlemail.AppendDataBoundItems = true;
                        ddlemail.Items.Clear();
                        ddlemail.Items.Add(new ListItem("กรุณาเลือกประเภทอีเมล์ ....", "0"));
                        ddlemail.Items.Add(new ListItem("เมล์ใหม่", "1"));
                        ddlemail.Items.Add(new ListItem("เมล์เดิม", "2"));

                        // divshowmail.Visible = false;
                        div_chooseemail.Visible = true;
                    }
                    else if (rdomail.SelectedValue == "private")
                    {
                        ddlemail.AppendDataBoundItems = true;
                        ddlemail.Items.Clear();
                        ddlemail.Items.Add(new ListItem("กรุณาเลือกประเภทอีเมล์ ....", "0"));
                        ddlemail.Items.Add(new ListItem("เมล์ใหม่", "1"));
                        ddlemail.Items.Add(new ListItem("เมล์ทดแทน", "2"));

                        // divshowmail.Visible = false;
                        div_chooseemail.Visible = true;
                    }
                    else
                    {
                        div_chooseemail.Visible = false;
                    }


                    break;

                case "rdomail_more":
                    if (rdomail_more.SelectedValue == "1")
                    {
                        ddlemailmore.AppendDataBoundItems = true;
                        ddlemailmore.Items.Clear();
                        ddlemailmore.Items.Add(new ListItem("กรุณาเลือกประเภทอีเมล์ ....", "0"));
                        ddlemailmore.Items.Add(new ListItem("เมล์ใหม่", "1"));
                        ddlemailmore.Items.Add(new ListItem("เมล์เดิม", "2"));

                        // divshowmail.Visible = false;
                        divmore_emaildetail.Visible = true;
                    }
                    else if (rdomail_more.SelectedValue == "2")
                    {
                        ddlemailmore.AppendDataBoundItems = true;
                        ddlemailmore.Items.Clear();
                        ddlemailmore.Items.Add(new ListItem("กรุณาเลือกประเภทอีเมล์ ....", "0"));
                        ddlemailmore.Items.Add(new ListItem("เมล์ใหม่", "1"));
                        ddlemailmore.Items.Add(new ListItem("เมล์ทดแทน", "2"));

                        // divshowmail.Visible = false;
                        divmore_emaildetail.Visible = true;
                    }
                    else
                    {
                        divmore_emaildetail.Visible = false;
                    }
                    break;
                case "rdotype_visitor":

                    txtEmpVisitorIDCard.Text = String.Empty;
                    txtEmpVisitorIDCardForeign.Text = String.Empty;

                    if (rdotype_visitor.SelectedValue == "1")
                    {
                        _DivVisitorIDCard.Visible = true;
                        _DivVisitorIDCardForeign.Visible = false;
                    }
                    else
                    {
                        _DivVisitorIDCard.Visible = false;
                        _DivVisitorIDCardForeign.Visible = true;
                    }

                    break;

            }
        }
        else if (sender is DropDownList)
        {
            DropDownList dropDownList = (DropDownList)sender;
            switch (dropDownList.ID)
            {
                case "ddlFilterIndexPermTempOrg":
                    if (dropDownList.SelectedValue == string.Empty)
                    {
                        ddlFilterIndexPermTempDept.Items.Clear();
                        ddlFilterIndexPermTempDept.AppendDataBoundItems = true;
                        ddlFilterIndexPermTempDept.Items.Add(new ListItem("-- เลือกฝ่าย --", string.Empty));
                    }
                    else
                    {
                        department_details objEmployee = new department_details();
                        dataEmployee.department_list = new department_details[1];
                        objEmployee.org_idx = int.Parse(dropDownList.SelectedValue);
                        dataEmployee.department_list[0] = objEmployee;
                        dataEmployee = callServiceEmployee(_urlGetDepartmentList, dataEmployee);
                        ddlFilterIndexPermTempDept.Items.Clear();
                        ddlFilterIndexPermTempDept.AppendDataBoundItems = true;
                        ddlFilterIndexPermTempDept.Items.Add(new ListItem("-- เลือกฝ่าย --", string.Empty));
                        ddlFilterIndexPermTempDept.DataSource = dataEmployee.department_list;
                        ddlFilterIndexPermTempDept.DataTextField = "dept_name_th";
                        ddlFilterIndexPermTempDept.DataValueField = "rdept_idx";
                        ddlFilterIndexPermTempDept.DataBind();
                    }
                    break;

                case "ddlFilterIndexPermPermOrg":
                    if (dropDownList.SelectedValue == string.Empty)
                    {
                        ddlFilterIndexPermPermDept.Items.Clear();
                        ddlFilterIndexPermPermDept.AppendDataBoundItems = true;
                        ddlFilterIndexPermPermDept.Items.Add(new ListItem("-- เลือกฝ่าย --", string.Empty));

                        ddlFilterIndexPermPermSec.Items.Clear();
                        ddlFilterIndexPermPermSec.AppendDataBoundItems = true;
                        ddlFilterIndexPermPermSec.Items.Add(new ListItem("-- เลือกแผนก --", string.Empty));

                        ddlFilterIndexPermPermPos.Items.Clear();
                        ddlFilterIndexPermPermPos.AppendDataBoundItems = true;
                        ddlFilterIndexPermPermPos.Items.Add(new ListItem("-- เลือกตำแหน่ง --", string.Empty));
                    }
                    else
                    {
                        department_details objEmployee = new department_details();
                        dataEmployee.department_list = new department_details[1];
                        objEmployee.org_idx = int.Parse(ddlFilterIndexPermPermOrg.SelectedValue);
                        dataEmployee.department_list[0] = objEmployee;
                        dataEmployee = callServiceEmployee(_urlGetDepartmentList, dataEmployee);
                        ddlFilterIndexPermPermDept.Items.Clear();
                        ddlFilterIndexPermPermDept.AppendDataBoundItems = true;
                        ddlFilterIndexPermPermDept.Items.Add(new ListItem("-- เลือกฝ่าย --", string.Empty));
                        ddlFilterIndexPermPermDept.DataSource = dataEmployee.department_list;
                        ddlFilterIndexPermPermDept.DataTextField = "dept_name_th";
                        ddlFilterIndexPermPermDept.DataValueField = "rdept_idx";
                        ddlFilterIndexPermPermDept.DataBind();

                        ddlFilterIndexPermPermSec.Items.Clear();
                        ddlFilterIndexPermPermSec.AppendDataBoundItems = true;
                        ddlFilterIndexPermPermSec.Items.Add(new ListItem("-- เลือกแผนก --", string.Empty));

                        ddlFilterIndexPermPermPos.Items.Clear();
                        ddlFilterIndexPermPermPos.AppendDataBoundItems = true;
                        ddlFilterIndexPermPermPos.Items.Add(new ListItem("-- เลือกตำแหน่ง --", string.Empty));
                    }
                    break;

                case "ddlFilterIndexPermPermDept":
                    if (dropDownList.SelectedValue == string.Empty)
                    {
                        ddlFilterIndexPermPermSec.Items.Clear();
                        ddlFilterIndexPermPermSec.AppendDataBoundItems = true;
                        ddlFilterIndexPermPermSec.Items.Add(new ListItem("-- เลือกแผนก --", string.Empty));

                        ddlFilterIndexPermPermPos.Items.Clear();
                        ddlFilterIndexPermPermPos.AppendDataBoundItems = true;
                        ddlFilterIndexPermPermPos.Items.Add(new ListItem("-- เลือกตำแหน่ง --", string.Empty));
                    }
                    else
                    {
                        section_details objEmployee = new section_details();
                        dataEmployee.section_list = new section_details[1];
                        objEmployee.org_idx = int.Parse(ddlFilterIndexPermPermOrg.SelectedValue);
                        objEmployee.rdept_idx = int.Parse(ddlFilterIndexPermPermDept.SelectedValue);
                        dataEmployee.section_list[0] = objEmployee;
                        dataEmployee = callServiceEmployee(_urlGetSectionList, dataEmployee);
                        ddlFilterIndexPermPermSec.Items.Clear();
                        ddlFilterIndexPermPermSec.AppendDataBoundItems = true;
                        ddlFilterIndexPermPermSec.Items.Add(new ListItem("-- เลือกแผนก --", string.Empty));
                        ddlFilterIndexPermPermSec.DataSource = dataEmployee.section_list;
                        ddlFilterIndexPermPermSec.DataTextField = "sec_name_th";
                        ddlFilterIndexPermPermSec.DataValueField = "rsec_idx";
                        ddlFilterIndexPermPermSec.DataBind();

                        ddlFilterIndexPermPermPos.Items.Clear();
                        ddlFilterIndexPermPermPos.AppendDataBoundItems = true;
                        ddlFilterIndexPermPermPos.Items.Add(new ListItem("-- เลือกตำแหน่ง --", string.Empty));
                    }
                    break;

                case "ddlFilterIndexPermPermSec":
                    if (dropDownList.SelectedValue == string.Empty)
                    {
                        ddlFilterIndexPermPermPos.Items.Clear();
                        ddlFilterIndexPermPermPos.AppendDataBoundItems = true;
                        ddlFilterIndexPermPermPos.Items.Add(new ListItem("-- เลือกตำแหน่ง --", string.Empty));
                    }
                    else
                    {
                        position_details objEmployee = new position_details();
                        dataEmployee.position_list = new position_details[1];
                        objEmployee.org_idx = int.Parse(ddlFilterIndexPermPermOrg.SelectedValue);
                        objEmployee.rdept_idx = int.Parse(ddlFilterIndexPermPermDept.SelectedValue);
                        objEmployee.rsec_idx = int.Parse(ddlFilterIndexPermPermSec.SelectedValue);
                        dataEmployee.position_list[0] = objEmployee;
                        dataEmployee = callServiceEmployee(_urlGetPositionList, dataEmployee);
                        ddlFilterIndexPermPermPos.Items.Clear();
                        ddlFilterIndexPermPermPos.AppendDataBoundItems = true;
                        ddlFilterIndexPermPermPos.Items.Add(new ListItem("-- เลือกตำแหน่ง --", string.Empty));
                        ddlFilterIndexPermPermPos.DataSource = dataEmployee.position_list;
                        ddlFilterIndexPermPermPos.DataTextField = "pos_name_th";
                        ddlFilterIndexPermPermPos.DataValueField = "rpos_idx";
                        ddlFilterIndexPermPermPos.DataBind();
                    }
                    break;

                case "ddlFilterIndexPermTempConditionDate":
                    if (dropDownList.SelectedValue == "max" || dropDownList.SelectedValue == "min")
                    {
                        divFilterIndexPermTempFrom.Visible = false;
                        divFilterIndexPermTempTo.Visible = false;
                        divFilterIndexPermTempOnly.Visible = true;
                    }
                    else
                    {
                        divFilterIndexPermTempFrom.Visible = true;
                        divFilterIndexPermTempTo.Visible = true;
                        divFilterIndexPermTempOnly.Visible = false;
                    }
                    break;

                case "ddlFilterIndexPermPermConditionDate":
                    if (dropDownList.SelectedValue == "max" || dropDownList.SelectedValue == "min")
                    {
                        divFilterIndexPermPermFrom.Visible = false;
                        divFilterIndexPermPermTo.Visible = false;
                        divFilterIndexPermPermOnly.Visible = true;
                    }
                    else
                    {
                        divFilterIndexPermPermFrom.Visible = true;
                        divFilterIndexPermPermTo.Visible = true;
                        divFilterIndexPermPermOnly.Visible = false;
                    }
                    break;

                case "ddlemail":

                    ViewState["countcost"] = "0";
                    ViewState["CostNo"] = txtEmpNewCostCenterNo.Text;

                    divemail1or3.Visible = false;
                    divemailad.Visible = false;

                    actionSelectGooglelicense(ddl1or3, rdomail.SelectedValue, int.Parse(ddlemail.SelectedValue));

                    if (ddlemail.SelectedValue == "1")
                    {
                        if (ViewState["countcost"].ToString() != "1")
                        {
                            // divshowmail.Visible = true;
                            divemail1or3.Visible = false;
                            divemailad.Visible = true;
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถเพิ่มเมล์ใหม่ได้ เนื่องจากไม่มีเมล์ว่างค่ะ');", true);

                        }

                    }
                    else if (rdomail.SelectedValue == "private" && ddlemail.SelectedValue == "2")
                    {
                        divemail1or3.Visible = true;
                        divemailad.Visible = true;
                    }
                    else
                    {
                        // divshowmail.Visible = false;
                        divemail1or3.Visible = true;
                        divemailad.Visible = false;
                    }


                    break;

                case "ddlWaitingPermPermEmailType":

                    ViewState["countcost"] = "0";
                    ViewState["CostNo"] = lblValueWaitingPermPermCostCenter.Text;
                    divemail.Visible = false;
                    divnewmail.Visible = false;
                    actionSelectGooglelicense(ddl1or3_edit, rdoWaitingPermPermEmailType.SelectedValue, int.Parse(ddlWaitingPermPermEmailType.SelectedValue));

                    if (ddlWaitingPermPermEmailType.SelectedValue == "1")
                    {
                        if (ViewState["countcost"].ToString() != "1")
                        {
                            divemail.Visible = false;
                            divnewmail.Visible = true;
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถเพิ่มเมล์ใหม่ได้ เนื่องจากไม่มีเมล์ว่างค่ะ');", true);

                        }

                    }
                    else if (rdoWaitingPermPermEmailType.SelectedValue == "private" && ddlWaitingPermPermEmailType.SelectedValue == "2")
                    {
                        divemail.Visible = true;
                        divnewmail.Visible = true;
                    }
                    else
                    {
                        // divshowmail.Visible = false;
                        divemail.Visible = true;
                        divnewmail.Visible = false;
                    }

                    break;

                case "ddladonline":
                    if (ddladonline.SelectedValue == "1" || ddladonline.SelectedValue == "2" || ddladonline.SelectedValue == "3")
                    {
                        div_ad.Visible = true;
                        showreasonad.Text = "เหตุผลในการขอสิทธิ์ AD( " + ddladonline.SelectedItem.Text + " )";
                    }
                    else
                    {
                        div_ad.Visible = false;
                    }

                    break;

                case "ddladonline_more":
                    if (ddladonline_more.SelectedValue == "1" || ddladonline_more.SelectedValue == "2" || ddladonline_more.SelectedValue == "3")
                    {
                        divshowreasonmore.Visible = true;
                        showreasonad_more.Text = "เหตุผลในการขอสิทธิ์ AD( " + ddladonline_more.SelectedItem.Text + " )";
                    }
                    else
                    {
                        divshowreasonmore.Visible = false;
                    }

                    break;

                case "ddlemailmore":
                    ViewState["countcost"] = "0";
                    ViewState["CostNo"] = txtEmpNewCostCenterNo_Addmore.Text;
                    string valuerdo = String.Empty;

                    if (rdomail_more.SelectedValue == "1")
                    {
                        valuerdo = "center";
                    }
                    else if (rdomail_more.SelectedValue == "2")
                    {
                        valuerdo = "private";
                    }

                    divemail1or3_more.Visible = false;
                    divemailad_more.Visible = false;

                    actionSelectGooglelicense(ddlemail1or3_more, valuerdo, int.Parse(ddlemailmore.SelectedValue));

                    if (ddlemailmore.SelectedValue == "1")
                    {

                        if (ViewState["countcost"].ToString() != "1")
                        {
                            divemail1or3_more.Visible = false;
                            divemailad_more.Visible = true;
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถเพิ่มเมล์ใหม่ได้ เนื่องจากไม่มีเมล์ว่างค่ะ');", true);

                        }

                    }
                    else if (rdomail_more.SelectedValue == "2" && ddlemailmore.SelectedValue == "2")
                    {
                        divemail1or3_more.Visible = true;
                        divemailad_more.Visible = true;
                    }
                    else
                    {
                        divemail1or3_more.Visible = true;
                        divemailad_more.Visible = false;
                    }


                    break;

            }
        }
        else if (sender is CheckBox)
        {
            CheckBox checkBox = (CheckBox)sender;
            switch (checkBox.ID)
            {
                

                case "chkmoread":

                    if (chkmoread.Checked)
                    {
                        divmore_adonline.Visible = true;
                    }
                    else
                    {
                        divmore_adonline.Visible = false;
                    }

                    break;

                case "chkmoreusersap":
                    if (chkmoreusersap.Checked)
                    {
                        divmore_usersap.Visible = true;
                    }
                    else
                    {
                        divmore_usersap.Visible = false;
                    }
                    break;

                case "chkmoreemail":
                    if (chkmoreemail.Checked)
                    {
                        divmore_email.Visible = true;
                    }
                    else
                    {
                        divmore_email.Visible = false;
                    }
                    break;

                   
            }
        }
    }

    protected void onTextChanged(Object sender, EventArgs e)
    {
        if (sender is TextBox)
        {
            TextBox textbox = (TextBox)sender;

            //linkBtnTrigger(_divMenuBtnToDivCreate);
            //linkBtnTrigger(btnInsertPermPerm);

            switch (textbox.ID)
            {
                case "txtEmpNewEmpCode":

                    ad_online objADOnline = new ad_online();
                    dataADOnline.ad_online_action = new ad_online[1];
                    objADOnline.emp_code = textbox.Text.Trim();
                    dataADOnline.ad_online_action[0] = objADOnline;
                    dataADOnline = callServiceADOnline(_urlReadEmpProfileByEmpCode, dataADOnline);
                    if (dataADOnline.return_code != 0)
                    {
                        txtEmpNewEmpIdxHidden.Text = String.Empty;
                        txtEmpNewJobGradeLevelHidden.Text = String.Empty;
                        txtEmpNewFullNameTH.Text = String.Empty;
                        txtEmpNewFullNameEN.Text = String.Empty;
                        txtEmpNewOrgNameTH.Text = String.Empty;
                        txtEmpNewDeptNameTH.Text = String.Empty;
                        txtEmpNewSecNameTH.Text = String.Empty;
                        txtEmpNewPosNameTH.Text = String.Empty;
                        txtEmpNewCostCenterNo.Text = String.Empty;
                        btnInsertPermPerm.Visible = false;
                        if (textbox.Text.Trim() != string.Empty)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลพนักงานดังกล่าว กรุณาติดต่อฝ่ายบุคคล');", true);
                        }
                    }
                    else
                    {
                        linkBtnTrigger(_divMenuBtnToDivCreate);
                        linkBtnTrigger(btnInsertPermPerm);

                        txtEmpNewEmpIdxHidden.Text = dataADOnline.ad_online_action[0].emp_idx.ToString();
                        txtEmpNewJobGradeLevelHidden.Text = dataADOnline.ad_online_action[0].jobgrade_level.ToString();
                        txtEmpNewFullNameTH.Text = dataADOnline.ad_online_action[0].emp_name_th;
                        txtEmpNewFullNameTHHidden.Text = dataADOnline.ad_online_action[0].emp_name_th;
                        txtEmpNewFullNameEN.Text = dataADOnline.ad_online_action[0].emp_name_en;
                        txtEmpNewFullNameENHidden.Text = dataADOnline.ad_online_action[0].emp_name_en;
                        txtEmpNewOrgNameTH.Text = dataADOnline.ad_online_action[0].org_name_th;
                        txtEmpNewOrgNameTHHidden.Text = dataADOnline.ad_online_action[0].org_name_th;
                        txtEmpNewDeptNameTH.Text = dataADOnline.ad_online_action[0].dept_name_th;
                        txtEmpNewDeptNameTHHidden.Text = dataADOnline.ad_online_action[0].dept_name_th;
                        txtEmpNewSecNameTH.Text = dataADOnline.ad_online_action[0].sec_name_th;
                        txtEmpNewSecNameTHHidden.Text = dataADOnline.ad_online_action[0].sec_name_th;
                        txtEmpNewPosNameTH.Text = dataADOnline.ad_online_action[0].pos_name_th;
                        txtEmpNewPosNameTHHidden.Text = dataADOnline.ad_online_action[0].pos_name_th;
                        txtEmpNewCostCenterNo.Text = dataADOnline.ad_online_action[0].costcenter_no.ToString();
                        txtEmpNewCostCenterNoHidden.Text = dataADOnline.ad_online_action[0].costcenter_no.ToString();
                        btnInsertPermPerm.Visible = true;
                    }
                    break;

                case "txtEmpNewEmpCode_Addmore":
                    ad_online objADOnline_ = new ad_online();
                    dataADOnline.ad_online_action = new ad_online[1];
                    objADOnline_.emp_code = textbox.Text.Trim();
                    dataADOnline.ad_online_action[0] = objADOnline_;


                    dataADOnline = callServiceADOnline(_urlReadEmpProfileByEmpCode, dataADOnline);

                    if (dataADOnline.return_code != 0)
                    {
                        txtEmpNewEmpIdxHidden_Addmore.Text = String.Empty;
                        txtEmpNewJobGradeLevelHidden_Addmore.Text = String.Empty;
                        txtEmpNewFullNameTH_Addmore.Text = String.Empty;
                        txtEmpNewFullNameEN_Addmore.Text = String.Empty;
                        txtEmpNewOrgNameTH_Addmore.Text = String.Empty;
                        txtEmpNewDeptNameTH_Addmore.Text = String.Empty;
                        txtEmpNewSecNameTH_Addmore.Text = String.Empty;
                        txtEmpNewPosNameTH_Addmore.Text = String.Empty;
                        txtEmpNewCostCenterNo_Addmore.Text = String.Empty;

                        div_Addmore.Visible = false;
                        btnInsertAddmore.Visible = false;

                        if (textbox.Text.Trim() != string.Empty)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลพนักงานดังกล่าว กรุณาติดต่อฝ่ายบุคคล');", true);
                        }
                    }
                    else
                    {
                        txtEmpNewEmpIdxHidden_Addmore.Text = dataADOnline.ad_online_action[0].emp_idx.ToString();
                        txtEmpNewJobGradeLevelHidden_Addmore.Text = dataADOnline.ad_online_action[0].jobgrade_level.ToString();
                        txtEmpNewFullNameTH_Addmore.Text = dataADOnline.ad_online_action[0].emp_name_th;
                        txtEmpNewFullNameTHHidden_Addmore.Text = dataADOnline.ad_online_action[0].emp_name_th;
                        txtEmpNewFullNameEN_Addmore.Text = dataADOnline.ad_online_action[0].emp_name_en;
                        txtEmpNewFullNameENHidden_Addmore.Text = dataADOnline.ad_online_action[0].emp_name_en;
                        txtEmpNewOrgNameTH_Addmore.Text = dataADOnline.ad_online_action[0].org_name_th;
                        txtEmpNewOrgNameTHHidden_Addmore.Text = dataADOnline.ad_online_action[0].org_name_th;
                        txtEmpNewDeptNameTH_Addmore.Text = dataADOnline.ad_online_action[0].dept_name_th;
                        txtEmpNewDeptNameTHHidden_Addmore.Text = dataADOnline.ad_online_action[0].dept_name_th;
                        txtEmpNewSecNameTH_Addmore.Text = dataADOnline.ad_online_action[0].sec_name_th;
                        txtEmpNewSecNameTHHidden_Addmore.Text = dataADOnline.ad_online_action[0].sec_name_th;
                        txtEmpNewPosNameTH_Addmore.Text = dataADOnline.ad_online_action[0].pos_name_th;
                        txtEmpNewPosNameTHHidden_Addmore.Text = dataADOnline.ad_online_action[0].pos_name_th;
                        txtEmpNewCostCenterNo_Addmore.Text = dataADOnline.ad_online_action[0].costcenter_no.ToString();
                        txtEmpNewCostCenterNoHidden_Addmore.Text = dataADOnline.ad_online_action[0].costcenter_no.ToString();

                        div_Addmore.Visible = true;
                        btnInsertAddmore.Visible = true;

                        actionAddMoreDataPerm(int.Parse(txtEmpNewEmpIdxHidden_Addmore.Text));
                        txtvpnidx_addmore.Text = dataADOnline.return_vpnsap;
                        actionSelectVPN(GvAddmore, 0);
                        divmore_vpn.Visible = true;

                    }
                    break;
            }
        }
    }

    protected void onRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (sender is GridView)
        {
            GridView gridview = (GridView)sender;
            switch (gridview.ID)
            {
                case "gvWaitingU1PermTempContactsList":
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Label lblAppLastNode = (Label)e.Row.Cells[0].FindControl("lblAppLastNode");
                        CheckBox chkWaitingU1PermTempContactList = (CheckBox)e.Row.Cells[0].FindControl("chkWaitingU1PermTempContactList");
                        Label u1Status = (Label)e.Row.Cells[0].FindControl("lblU1PermTempStatus");
                        Label lblU1PermTempNotApp = (Label)e.Row.Cells[0].FindControl("lblU1PermTempNotApp");
                        TextBox txtU1PermTempUsername = (TextBox)e.Row.Cells[3].FindControl("txtU1PermTempUsername");
                        Label lblU1PermTempNotAppUsername = (Label)e.Row.Cells[4].FindControl("lblU1PermTempNotAppUsername");
                        TextBox txtU1PermTempPassword = (TextBox)e.Row.Cells[3].FindControl("txtU1PermTempPassword");
                        Label lblU1PermTempNotAppPassword = (Label)e.Row.Cells[4].FindControl("lblU1PermTempNotAppPassword");
                        if (u1Status.Text == Constants.U1_PERM_TEMP_STATUS_NOTAPP.ToString())
                        {
                            lblAppLastNode.Visible = false;
                            chkWaitingU1PermTempContactList.Visible = false;
                            chkWaitingU1PermTempContactList.Checked = false;
                            txtU1PermTempUsername.Visible = false;
                            txtU1PermTempPassword.Visible = false;
                            lblU1PermTempNotApp.Visible = true;
                            lblU1PermTempNotAppUsername.Visible = true;
                            lblU1PermTempNotAppPassword.Visible = true;
                        }
                        else
                        {
                            if (lblValueU0PermTempNodeId.Text == Constants.NODE_PERM_TEMP_APP_GOTO_IT.ToString())
                            {
                                lblAppLastNode.Visible = true;
                                chkWaitingU1PermTempContactList.Visible = false;
                                chkWaitingU1PermTempContactList.Checked = true;
                            }
                        }
                    }
                    break;

                case "gvIndexPermTemp":
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Label u0PermTempNodeTo1 = (Label)e.Row.Cells[3].FindControl("u0PermTempNodeTo1");
                        Label connectText = (Label)e.Row.Cells[3].FindControl("connectText");
                        Label u0PermTempNodeTo2 = (Label)e.Row.Cells[3].FindControl("u0PermTempNodeTo2");
                        Label u0node_temp = (Label)e.Row.Cells[1].FindControl("u0node_temp");
                        Label u0temp_rsecidx = (Label)e.Row.Cells[1].FindControl("u0temp_rsecidx");
                        Label u0temp_jobgradeidx = (Label)e.Row.Cells[1].FindControl("u0temp_jobgradeidx");
                        Label lblwhoapprove = (Label)e.Row.Cells[3].FindControl("lblwhoapprove");
                        Label u0Createdtempby = (Label)e.Row.Cells[1].FindControl("u0Createdtempby");


                        if (u0PermTempNodeTo1.Text == "เสร็จสมบูรณ์")
                        {
                            connectText.Visible = false;
                            u0PermTempNodeTo2.Visible = false;
                        }
                        else
                        {
                            connectText.Visible = true;
                            u0PermTempNodeTo2.Visible = true;
                        }



                        getSetPermWhoApproveIntoToolTip(lblwhoapprove, int.Parse(u0node_temp.Text), int.Parse(u0temp_jobgradeidx.Text), int.Parse(u0temp_rsecidx.Text), 0, Constants.KEY_GET_FROM_PERM_TEMP);

                        if (u0node_temp.Text == "3" || u0node_temp.Text == "5" || u0node_temp.Text == "7" || u0node_temp.Text == "8")
                        {
                            lblwhoapprove.Visible = false;
                        }
                        else
                        {
                            lblwhoapprove.Visible = true;
                        }
                    }
                    break;

                case "gvIndexPermPerm":


                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Label u0PermPermNodeTo1 = (Label)e.Row.Cells[3].FindControl("u0PermPermNodeTo1");
                        Label connectText = (Label)e.Row.Cells[3].FindControl("connectText");
                        Label u0PermPermNodeTo2 = (Label)e.Row.Cells[3].FindControl("u0PermPermNodeTo2");
                        Label u0node_perm = (Label)e.Row.Cells[2].FindControl("u0node_perm");
                        Label u0perm_rsecidx = (Label)e.Row.Cells[2].FindControl("u0perm_rsecidx");
                        Label u0perm_jobgradeidx = (Label)e.Row.Cells[2].FindControl("u0perm_jobgradeidx");
                        Label lblwhoapprove = (Label)e.Row.Cells[3].FindControl("lblwhoapprove");
                        Label u0Createdpermby = (Label)e.Row.Cells[1].FindControl("u0Createdpermby");



                        if (u0PermPermNodeTo1.Text == "เสร็จสมบูรณ์")
                        {
                            connectText.Visible = false;
                            u0PermPermNodeTo2.Visible = false;
                        }
                        else
                        {
                            connectText.Visible = true;
                            u0PermPermNodeTo2.Visible = true;
                        }

                        getSetPermWhoApproveIntoToolTip(lblwhoapprove, int.Parse(u0node_perm.Text), int.Parse(u0perm_jobgradeidx.Text), int.Parse(u0perm_rsecidx.Text), int.Parse(u0Createdpermby.Text), Constants.KEY_GET_FROM_PERM_PERM);

                        if (u0node_perm.Text == "5" || u0node_perm.Text == "7" || u0node_perm.Text == "9" || u0node_perm.Text == "11"
                            || u0node_perm.Text == "13" || u0node_perm.Text == "14" || u0node_perm.Text == "15" || u0node_perm.Text == "19")
                        {
                            lblwhoapprove.Visible = false;
                        }
                        else
                        {
                            lblwhoapprove.Visible = true;
                        }
                    }

                    break;
                case "GvUseVPN":
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {

                        Label lbl_vpnidx = (Label)e.Row.FindControl("lbl_vpnidx");
                        CheckBoxList chklevel_vpn = (CheckBoxList)e.Row.FindControl("chklevel_vpn");
                        //CheckBox chktype_vpn = (CheckBox)e.Row.FindControl("chktype_vpn");


                        //chktype_vpn.Checked = 


                        data_adonline dataADOnline_vpn = new data_adonline();
                        ad_online objADOnline_vpn = new ad_online();
                        dataADOnline_vpn.ad_online_action = new ad_online[1];
                        objADOnline_vpn.vpnidx = int.Parse(lbl_vpnidx.Text);
                        //objADOnline_vpn.filter_u0_perm_temp_code_id = ViewState["filterIndexPermTempCodeId"].ToString();
                        //objADOnline_vpn.filter_u0_perm_temp_creator = ViewState["filterIndexPermTempCreator"].ToString();

                        dataADOnline_vpn.ad_online_action[0] = objADOnline_vpn;

                        dataADOnline_vpn = callServiceADOnline(_urlSelectVPNSUBMAIN, dataADOnline_vpn);

                        ViewState["data_submain_vpn"] = dataADOnline_vpn.ad_online_action;

                        chklevel_vpn.Items.Clear();
                        chklevel_vpn.AppendDataBoundItems = true;
                        chklevel_vpn.DataSource = ViewState["data_submain_vpn"];

                        chklevel_vpn.DataTextField = "vpn1_name";
                        chklevel_vpn.DataValueField = "vpn1idx";
                        chklevel_vpn.DataBind();



                        //setGridData(GvUseVPN, dataADOnline_vpn.ad_online_action);

                    }
                    break;
                case "GvUseVPNTemporary":
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {

                        Label lbl_vpnidx_temporary = (Label)e.Row.FindControl("lbl_vpnidx_temporary");
                        CheckBoxList chklevel_vpn_temporary = (CheckBoxList)e.Row.FindControl("chklevel_vpn_temporary");
                        //CheckBox chktype_vpn = (CheckBox)e.Row.FindControl("chktype_vpn");


                        //chktype_vpn.Checked = 


                        data_adonline dataADOnline_vpntemporary = new data_adonline();
                        ad_online objADOnline_vpn = new ad_online();
                        dataADOnline_vpntemporary.ad_online_action = new ad_online[1];
                        objADOnline_vpn.vpnidx = int.Parse(lbl_vpnidx_temporary.Text);
                        //objADOnline_vpn.filter_u0_perm_temp_code_id = ViewState["filterIndexPermTempCodeId"].ToString();
                        //objADOnline_vpn.filter_u0_perm_temp_creator = ViewState["filterIndexPermTempCreator"].ToString();

                        dataADOnline_vpntemporary.ad_online_action[0] = objADOnline_vpn;

                        dataADOnline_vpntemporary = callServiceADOnline(_urlSelectVPNSUBMAIN, dataADOnline_vpntemporary);

                        ViewState["data_submain_vpntemporary"] = dataADOnline_vpntemporary.ad_online_action;

                        chklevel_vpn_temporary.Items.Clear();
                        chklevel_vpn_temporary.AppendDataBoundItems = true;
                        chklevel_vpn_temporary.DataSource = ViewState["data_submain_vpntemporary"];

                        chklevel_vpn_temporary.DataTextField = "vpn1_name";
                        chklevel_vpn_temporary.DataValueField = "vpn1idx";
                        chklevel_vpn_temporary.DataBind();



                        //setGridData(GvUseVPN, dataADOnline_vpn.ad_online_action);

                    }
                    break;
                case "GvAddmore":
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {

                        Label lbl_vpnidx_Addmore = (Label)e.Row.FindControl("lbl_vpnidx_Addmore");
                        RadioButtonList rdolevel_vpn_Addmore = (RadioButtonList)e.Row.FindControl("rdolevel_vpn_Addmore");
                        //CheckBox chktype_vpn = (CheckBox)e.Row.FindControl("chktype_vpn");


                        //chktype_vpn.Checked = 


                        data_adonline dataADOnline_vpnAddmore = new data_adonline();
                        ad_online objADOnline_vpn = new ad_online();
                        dataADOnline_vpnAddmore.ad_online_action = new ad_online[1];
                        objADOnline_vpn.vpnidx = int.Parse(lbl_vpnidx_Addmore.Text);
                        //objADOnline_vpn.filter_u0_perm_temp_code_id = ViewState["filterIndexPermTempCodeId"].ToString();
                        //objADOnline_vpn.filter_u0_perm_temp_creator = ViewState["filterIndexPermTempCreator"].ToString();

                        dataADOnline_vpnAddmore.ad_online_action[0] = objADOnline_vpn;

                        dataADOnline_vpnAddmore = callServiceADOnline(_urlSelectVPNSUBMAIN, dataADOnline_vpnAddmore);

                        ViewState["data_submain_vpnAddmore"] = dataADOnline_vpnAddmore.ad_online_action;

                        rdolevel_vpn_Addmore.Items.Clear();
                        rdolevel_vpn_Addmore.AppendDataBoundItems = true;
                        rdolevel_vpn_Addmore.DataSource = ViewState["data_submain_vpnAddmore"];

                        rdolevel_vpn_Addmore.DataTextField = "vpn1_name";
                        rdolevel_vpn_Addmore.DataValueField = "vpn1idx";
                        rdolevel_vpn_Addmore.DataBind();



                        //setGridData(GvUseVPN, dataADOnline_vpn.ad_online_action);

                    }
                    break;
            }
        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "btnRemoveEmpVisitor":
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsContactPermTempList"];
                    litNoresultGvEmpVisitorContact.Visible = false;
                    dsContacts.Tables["dsContactPermTempTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(gvEmpVisitorContact, dsContacts.Tables["dsContactPermTempTable"]);
                    if (dsContacts.Tables["dsContactPermTempTable"].Rows.Count < 1)
                    {
                        gvEmpVisitorContact.Visible = false;
                        litNoresultGvEmpVisitorContact.Visible = true;

                        //Div_UsevpnTemporary.Visible = false;
                        btnInsertPermTemp.Visible = false;

                    }
                    break;
            }
        }
    }

    protected void onPageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        if (sender is GridView)
        {
            GridView gridview = (GridView)sender;
            switch (gridview.ID)
            {
                case "gvIndexPermTemp":
                    gridview.PageIndex = e.NewPageIndex;
                    gridview.DataBind();
                    actionIndexPermTemp();
                    scrollToTop();
                    break;

                case "gvIndexPermPerm":

                    gridview.PageIndex = e.NewPageIndex;
                    gridview.DataBind();
                    actionIndexPermPerm();
                    scrollToTop();
                    break;

                case "gvWaitingPermTemp":
                    gridview.PageIndex = e.NewPageIndex;
                    gridview.DataBind();
                    actionWaitingPermTemp();
                    scrollToTop();
                    break;

                case "gvWaitingPermPerm":
                    gridview.PageIndex = e.NewPageIndex;
                    gridview.DataBind();
                    actionWaitingPermPerm();
                    scrollToTop();
                    break;
            }
        }
    }

   

    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {

        var chxName = (CheckBox)sender;
        //var chkName = (CheckBoxList)sender;

        switch (chxName.ID)
        {
            case "chxtype_vpn":
                linkBtnTrigger(btnInsertPermPerm);
                setchk_changed();
                break;
            case "chxtype_vpn_temporary":
                linkBtnTrigger(btnInsertPermPerm);
                setchk_changedvpntemporary();
                break;
         
            case "chkvpnedit":
                setchk_changedtypeedit();
                break;
            case "chkvpnedittemp":
                setchk_changedtempedit();
                break;
            case "chkvpn_addmore":
                setchk_changedadmore();
                break;
        }

    }



    protected void setchk_changed()
    {

        foreach (GridViewRow row in GvUseVPN.Rows)
        {
            CheckBox chxtype_vpn = (CheckBox)row.Cells[0].FindControl("chxtype_vpn");
            Label lbl_vpnidx_check = (Label)row.Cells[1].FindControl("lbl_vpnidx_check");
            CheckBoxList chklevel_vpn = (CheckBoxList)row.Cells[1].FindControl("chklevel_vpn");

            TextBox txt_remark = (TextBox)row.Cells[2].FindControl("txt_remark");
            //FileUpload UploadFile_Vpn = (FileUpload)row.Cells[3].FindControl("UploadFile_Vpn");


            if (chxtype_vpn.Checked == true)
            {
                chklevel_vpn.Visible = true;
                txt_remark.Enabled = true;
                //UploadFile_Vpn.Enabled = true;


            }
            else
            {
                chklevel_vpn.Visible = false;
                txt_remark.Enabled = false;

                txt_remark.Text = string.Empty;
                chklevel_vpn.ClearSelection();
                //UploadFile_Vpn.Enabled = false;
            }

        }

    }

    protected void setchk_changedvpntemporary()
    {

        foreach (GridViewRow row in GvUseVPNTemporary.Rows)
        {
            CheckBox chxtype_vpn_temporary = (CheckBox)row.Cells[0].FindControl("chxtype_vpn_temporary");
            Label lbl_vpnidx_check_temporary = (Label)row.Cells[1].FindControl("lbl_vpnidx_check_temporary");
            CheckBoxList chklevel_vpn_temporary = (CheckBoxList)row.Cells[1].FindControl("chklevel_vpn_temporary");

            TextBox txt_remark_temporary = (TextBox)row.Cells[2].FindControl("txt_remark_temporary");
            //FileUpload UploadFile_Vpntemporary = (FileUpload)row.Cells[3].FindControl("UploadFile_Vpntemporary");


            if (chxtype_vpn_temporary.Checked == true)
            {
                chklevel_vpn_temporary.Visible = true;
                txt_remark_temporary.Enabled = true;
                //UploadFile_Vpntemporary.Enabled = true;


            }
            else
            {
                chklevel_vpn_temporary.Visible = false;
                txt_remark_temporary.Enabled = false;

                chklevel_vpn_temporary.ClearSelection();
                txt_remark_temporary.Text = String.Empty;
                //UploadFile_Vpntemporary.Enabled = false;
            }

        }

    }

   

    protected void setchk_changedtypeedit()
    {

        foreach (GridViewRow row in GvEditVPN.Rows)
        {
            CheckBox chkvpnedit = (CheckBox)row.Cells[0].FindControl("chkvpnedit");
            Literal litvpnidxedit = (Literal)row.Cells[1].FindControl("litvpnidxedit");
            CheckBoxList chksubvpnedit = (CheckBoxList)row.Cells[1].FindControl("chksubvpnedit");

            TextBox txtremarkedit = (TextBox)row.Cells[2].FindControl("txtremarkedit");
            //FileUpload UploadFile_Vpn = (FileUpload)row.Cells[3].FindControl("UploadFile_Vpn");


            if (chkvpnedit.Checked == true)
            {
                chksubvpnedit.Visible = true;
                txtremarkedit.Visible = true;
                //UploadFile_Vpn.Enabled = true;

            }
            else
            {
                chksubvpnedit.Visible = false;
                txtremarkedit.Visible = false;
                //UploadFile_Vpn.Enabled = false;
                txtremarkedit.Text = string.Empty;
                chksubvpnedit.ClearSelection();
            }

        }

    }

    protected void setchk_changedtempedit()
    {

        foreach (GridViewRow row in GvEditVPNTemp.Rows)
        {
            CheckBox chkvpnedit = (CheckBox)row.Cells[0].FindControl("chkvpnedittemp");
            Literal litvpnidxedit = (Literal)row.Cells[1].FindControl("litvpnidxedittemp");
            CheckBoxList chksubvpnedit = (CheckBoxList)row.Cells[1].FindControl("chksubvpnedittemp");

            TextBox txtremarkedit = (TextBox)row.Cells[2].FindControl("txtremarkedittemp");
            //FileUpload UploadFile_Vpn = (FileUpload)row.Cells[3].FindControl("UploadFile_Vpn");


            if (chkvpnedit.Checked == true)
            {
                chksubvpnedit.Visible = true;
                txtremarkedit.Visible = true;
                //UploadFile_Vpn.Enabled = true;

            }
            else
            {
                chksubvpnedit.Visible = false;
                txtremarkedit.Visible = false;
                //UploadFile_Vpn.Enabled = false;
                txtremarkedit.Text = string.Empty;
                chksubvpnedit.ClearSelection();
            }

        }

    }

    protected void setchk_changedadmore()
    {

        foreach (GridViewRow row in GvAddmore.Rows)
        {
            CheckBox chkvpnedit = (CheckBox)row.Cells[0].FindControl("chkvpn_addmore");
            Literal litvpnidxedit = (Literal)row.Cells[1].FindControl("litvpnidx_addmore");
            CheckBoxList chksubvpnedit = (CheckBoxList)row.Cells[1].FindControl("chksubvpn_addmore");

            TextBox txtremarkedit = (TextBox)row.Cells[2].FindControl("txtremark_addmore");
            //FileUpload UploadFile_Vpn = (FileUpload)row.Cells[3].FindControl("UploadFile_Vpn");


            if (chkvpnedit.Checked == true)
            {
                chksubvpnedit.Visible = true;
                txtremarkedit.Visible = true;
                //UploadFile_Vpn.Enabled = true;

            }
            else
            {
                chksubvpnedit.Visible = false;
                txtremarkedit.Visible = false;
                //UploadFile_Vpn.Enabled = false;
                txtremarkedit.Text = string.Empty;
                chksubvpnedit.ClearSelection();
            }

        }

    }


    protected void setDefaultValueContactForm()
    {
        CultureInfo culture = new CultureInfo("en-US");
        Thread.CurrentThread.CurrentCulture = culture;
        txtEmpVisitorIDCard.Text = string.Empty;
        txtEmpVisitorFullName.Text = string.Empty;
        txtSearchFromHidden.Value = DateTime.Now.ToString("dd/MM/yyyy");
        txtSearchToHidden.Value = DateTime.Now.ToString("dd/MM/yyyy");
        txtSearchFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtSearchTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
    }

    protected void visibleWaitingPermPermPanelHeading(Label labelId, int nodeIdNext)
    {
        if (nodeIdNext == Constants.NODE_PERM_PERM_CREATE_GOTO_HEADER_EMPNEW)
        {
            labelId.Text = "ผู้มีสิทธิ์อนุมัติ";
        }
        else if (nodeIdNext == Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_EMPNEW || nodeIdNext == Constants.NODE_PERM_PERM_HEADER_EMPNEW_APP_GOTO_DIRECTOR_EMPNEW)
        {
            labelId.Text = "ผู้มีสิทธิ์อนุมัติ";
        }
        else if (nodeIdNext == Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_HR)
        {
            labelId.Text = "ผู้มีสิทธิ์อนุมัติ";
        }
        else if (nodeIdNext == Constants.NODE_PERM_PERM_DIRECTOR_HR_APP_GOTO_HEADER_IT || nodeIdNext == Constants.NODE_PERM_PERM_DIRECTOR_EMPNEW_APP_GOTO_HEADER_IT
            || nodeIdNext == Constants.node_perm_perm_it_app_goto_headit)
        {
            labelId.Text = "หัวหน้า IT";
        }
        else if (nodeIdNext == Constants.NODE_PERM_PERM_HEADER_IT_APP_GOTO_DIRECTOR_IT)
        {
            labelId.Text = " Director IT";
        }
        else if (nodeIdNext == Constants.NODE_PERM_PERM_DIRECTOR_IT_APP_GOTO_IT || nodeIdNext == Constants.node_perm_perm_header_empnew_app_goto_it ||
            nodeIdNext == Constants.node_perm_perm_director_empnew_app_goto_it)
        {
            labelId.Text = " IT";
        }
        else if (nodeIdNext == Constants.node_perm_perm_header_empnew_sendedit_user || nodeIdNext == Constants.node_perm_perm_director_empnew_sendedit_user ||
            nodeIdNext == Constants.node_perm_perm_director_hr_sendedit_user || nodeIdNext == Constants.node_perm_perm_it_sendedit_user ||
            nodeIdNext == Constants.node_perm_perm_headerit_sendedit_user || nodeIdNext == Constants.node_perm_perm_directorit_sendedit_user)
        {
            labelId.Text = "ผู้สร้างแก้ไขข้อมูล";
        }
    }

    protected void visibleWaitingPermCommentBox(string fromTable, int nodeIdNext)
    {
        if (fromTable == Constants.KEY_GET_FROM_PERM_TEMP)
        {
            if (nodeIdNext == Constants.NODE_PERM_TEMP_CREATE_GOTO_DIRECTOR_CREATOR)
            {
                _divWaitingPermTempDirectorOfCreator.Visible = true;
                _divWaitingPermTempHeaderOfIT.Visible = false;
                _divWaitingPermTempDirectorOfIT.Visible = false;
                _divWaitingPermTempIT.Visible = false;
            }
            else if (nodeIdNext == Constants.NODE_PERM_TEMP_APP_GOTO_HEADER_IT)
            {
                _divWaitingPermTempDirectorOfCreator.Visible = false;
                _divWaitingPermTempHeaderOfIT.Visible = true;
                _divWaitingPermTempDirectorOfIT.Visible = false;
                _divWaitingPermTempIT.Visible = false;
            }
            else if (nodeIdNext == Constants.NODE_PERM_TEMP_APP_GOTO_DIRECTOR_IT)
            {
                _divWaitingPermTempDirectorOfCreator.Visible = false;
                _divWaitingPermTempHeaderOfIT.Visible = false;
                _divWaitingPermTempDirectorOfIT.Visible = true;
                _divWaitingPermTempIT.Visible = false;
            }
            else if (nodeIdNext == Constants.NODE_PERM_TEMP_APP_GOTO_IT)
            {
                _divWaitingPermTempDirectorOfCreator.Visible = false;
                _divWaitingPermTempHeaderOfIT.Visible = false;
                _divWaitingPermTempDirectorOfIT.Visible = false;
                _divWaitingPermTempIT.Visible = true;
            }
            else
            {
                _divWaitingPermTempDirectorOfCreator.Visible = false;
                _divWaitingPermTempHeaderOfIT.Visible = false;
                _divWaitingPermTempDirectorOfIT.Visible = false;
                _divWaitingPermTempIT.Visible = false;
            }
        }
        else if (fromTable == Constants.KEY_GET_FROM_PERM_PERM)
        {
            if (nodeIdNext == Constants.NODE_PERM_PERM_CREATE_GOTO_HEADER_EMPNEW)
            {
                _divWaitingPermPermConfig.Visible = true;
                _divWaitingPermPermDirectorHR.Visible = false;
                _divWaitingPermPermHeaderEmpNew.Visible = true;
                _divWaitingPermPermDirectorEmpNew.Visible = false;
                _divWaitingPermPermHeaderIT.Visible = false;
                _divWaitingPermPermDirectorIT.Visible = false;
                _divWaitingPermPermIT.Visible = false;
                _divWaitingPermPermUser.Visible = false;
            }
            else if (nodeIdNext == Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_EMPNEW)
            {
                _divWaitingPermPermConfig.Visible = true;
                _divWaitingPermPermDirectorHR.Visible = false;
                _divWaitingPermPermHeaderEmpNew.Visible = false;
                _divWaitingPermPermDirectorEmpNew.Visible = true;
                _divWaitingPermPermHeaderIT.Visible = false;
                _divWaitingPermPermDirectorIT.Visible = false;
                _divWaitingPermPermIT.Visible = false;
                _divWaitingPermPermUser.Visible = false;
            }
            else if (nodeIdNext == Constants.NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_HR)
            {
                _divWaitingPermPermConfig.Visible = true;
                _divWaitingPermPermDirectorHR.Visible = true;
                _divWaitingPermPermHeaderEmpNew.Visible = false;
                _divWaitingPermPermDirectorEmpNew.Visible = false;
                _divWaitingPermPermHeaderIT.Visible = false;
                _divWaitingPermPermDirectorIT.Visible = false;
                _divWaitingPermPermIT.Visible = false;
                _divWaitingPermPermUser.Visible = false;
            }
            else if (nodeIdNext == Constants.NODE_PERM_PERM_DIRECTOR_HR_APP_GOTO_HEADER_IT)
            {
                _divWaitingPermPermConfig.Visible = false;
                _divWaitingPermPermDirectorHR.Visible = false;
                _divWaitingPermPermHeaderEmpNew.Visible = false;
                _divWaitingPermPermDirectorEmpNew.Visible = false;
                _divWaitingPermPermHeaderIT.Visible = true;
                _divWaitingPermPermDirectorIT.Visible = false;
                _divWaitingPermPermIT.Visible = false;
                _divWaitingPermPermUser.Visible = false;
            }
            else if (nodeIdNext == Constants.NODE_PERM_PERM_HEADER_EMPNEW_APP_GOTO_DIRECTOR_EMPNEW)
            {
                _divWaitingPermPermConfig.Visible = false;
                _divWaitingPermPermDirectorHR.Visible = false;
                _divWaitingPermPermHeaderEmpNew.Visible = false;
                _divWaitingPermPermDirectorEmpNew.Visible = true;
                _divWaitingPermPermHeaderIT.Visible = false;
                _divWaitingPermPermDirectorIT.Visible = false;
                _divWaitingPermPermIT.Visible = false;
                _divWaitingPermPermUser.Visible = false;
            }
            else if (nodeIdNext == Constants.NODE_PERM_PERM_DIRECTOR_EMPNEW_APP_GOTO_HEADER_IT || nodeIdNext == Constants.node_perm_perm_it_app_goto_headit)
            {
                _divWaitingPermPermConfig.Visible = true;
                _divWaitingPermPermDirectorHR.Visible = false;
                _divWaitingPermPermHeaderEmpNew.Visible = false;
                _divWaitingPermPermDirectorEmpNew.Visible = false;
                _divWaitingPermPermHeaderIT.Visible = true;
                _divWaitingPermPermDirectorIT.Visible = false;
                _divWaitingPermPermIT.Visible = false;
                _divWaitingPermPermUser.Visible = false;
            }
            else if (nodeIdNext == Constants.NODE_PERM_PERM_HEADER_IT_APP_GOTO_DIRECTOR_IT)
            {
                _divWaitingPermPermConfig.Visible = true;
                _divWaitingPermPermDirectorHR.Visible = false;
                _divWaitingPermPermHeaderEmpNew.Visible = false;
                _divWaitingPermPermDirectorEmpNew.Visible = false;
                _divWaitingPermPermHeaderIT.Visible = false;
                _divWaitingPermPermDirectorIT.Visible = true;
                _divWaitingPermPermIT.Visible = false;
                _divWaitingPermPermUser.Visible = false;
            }
            else if (nodeIdNext == Constants.NODE_PERM_PERM_DIRECTOR_IT_APP_GOTO_IT)
            {
                _divWaitingPermPermConfig.Visible = false;
                _divWaitingPermPermDirectorHR.Visible = false;
                _divWaitingPermPermHeaderEmpNew.Visible = false;
                _divWaitingPermPermDirectorEmpNew.Visible = false;
                _divWaitingPermPermHeaderIT.Visible = false;
                _divWaitingPermPermDirectorIT.Visible = false;
                _divWaitingPermPermIT.Visible = true;
                divaditclosejob.Visible = false;
                btnWaitingPermPermNotApproveIT.Visible = false;
                btnWaitingPermPermBackIT.Visible = true;
                btnWaitingPermPermEditIT.Visible = false;
                _divWaitingPermPermUser.Visible = false;
            }
            else if (nodeIdNext == Constants.node_perm_perm_header_empnew_app_goto_it || nodeIdNext == Constants.node_perm_perm_director_empnew_app_goto_it)
            {
                _divWaitingPermPermConfig.Visible = true;
                _divWaitingPermPermDirectorHR.Visible = false;
                _divWaitingPermPermHeaderEmpNew.Visible = false;
                _divWaitingPermPermDirectorEmpNew.Visible = false;
                _divWaitingPermPermHeaderIT.Visible = false;
                _divWaitingPermPermDirectorIT.Visible = false;
                _divWaitingPermPermIT.Visible = true;
                divaditclosejob.Visible = true;
                btnWaitingPermPermNotApproveIT.Visible = true;
                btnWaitingPermPermBackIT.Visible = false;
                btnWaitingPermPermEditIT.Visible = true;
                _divWaitingPermPermUser.Visible = false;

            }
            else if (nodeIdNext == Constants.node_perm_perm_header_empnew_sendedit_user || nodeIdNext == Constants.node_perm_perm_director_empnew_sendedit_user ||
            nodeIdNext == Constants.node_perm_perm_director_hr_sendedit_user || nodeIdNext == Constants.node_perm_perm_it_sendedit_user ||
            nodeIdNext == Constants.node_perm_perm_headerit_sendedit_user || nodeIdNext == Constants.node_perm_perm_directorit_sendedit_user)
            {
                _divWaitingPermPermConfig.Visible = true;
                _divWaitingPermPermDirectorHR.Visible = false;
                _divWaitingPermPermHeaderEmpNew.Visible = false;
                _divWaitingPermPermDirectorEmpNew.Visible = false;
                _divWaitingPermPermHeaderIT.Visible = false;
                _divWaitingPermPermDirectorIT.Visible = false;
                _divWaitingPermPermIT.Visible = false;
                _divWaitingPermPermUser.Visible = true;
            }
        }
    }

    protected void visibleWaitingPermPermCommentText(string fromDiv, string cmHeaderEmpNew, string cmDirectorEmpNew, string cmDirectorHR, string cmHeaderIT, string cmDirectorIT, string cmIT, string cmbeforeIT)
    {
        if (fromDiv == "index")
        {
            if (cmHeaderEmpNew != string.Empty)
            {
                divIndexU0PermPermCommentHeaderEmpNew.Visible = true;
                lblValueIndexU0PermPermCommentHeaderEmpNew.Text = nl2br(cmHeaderEmpNew);
            }
            else
            {
                divIndexU0PermPermCommentHeaderEmpNew.Visible = false;
                lblValueIndexU0PermPermCommentHeaderEmpNew.Text = string.Empty;
            }
            if (cmDirectorEmpNew != string.Empty)
            {
                divIndexU0PermPermCommentDirectorEmpNew.Visible = true;
                lblValueIndexU0PermPermCommentDirectorEmpNew.Text = nl2br(cmDirectorEmpNew);
            }
            else
            {
                divIndexU0PermPermCommentDirectorEmpNew.Visible = false;
                lblValueIndexU0PermPermCommentDirectorEmpNew.Text = string.Empty;
            }
            if (cmDirectorHR != string.Empty)
            {
                divIndexU0PermPermCommentDirectorHR.Visible = true;
                lblValueIndexU0PermPermCommentDirectorHR.Text = nl2br(cmDirectorHR);
            }
            else
            {
                divIndexU0PermPermCommentDirectorHR.Visible = false;
                lblValueIndexU0PermPermCommentDirectorHR.Text = string.Empty;
            }
            if (cmbeforeIT != string.Empty)
            {
                divIndexU0PermPermCommentIT.Visible = true;
                lblValueIndexU0PermPermCommentIT.Text = nl2br(cmbeforeIT);
            }
            else
            {
                divIndexU0PermPermCommentIT.Visible = false;
                lblValueIndexU0PermPermCommentIT.Text = string.Empty;
            }
            if (cmHeaderIT != string.Empty)
            {
                divIndexU0PermPermCommentHeaderIT.Visible = true;
                lblValueIndexU0PermPermCommentHeaderIT.Text = nl2br(cmHeaderIT);
            }
            else
            {
                divIndexU0PermPermCommentHeaderIT.Visible = false;
                lblValueIndexU0PermPermCommentHeaderIT.Text = string.Empty;
            }
            if (cmDirectorIT != string.Empty)
            {
                divIndexU0PermPermCommentDirectorIT.Visible = true;
                lblValueIndexU0PermPermCommentDirectorIT.Text = nl2br(cmDirectorIT);
            }
            else
            {
                divIndexU0PermPermCommentDirectorIT.Visible = false;
                lblValueIndexU0PermPermCommentDirectorIT.Text = string.Empty;
            }

            if (cmIT != string.Empty)
            {
                divIndexU0PermPermCommentITClose.Visible = true;
                lblValueIndexU0PermPermCommentITClose.Text = nl2br(cmIT);
            }
            else
            {
                divIndexU0PermPermCommentITClose.Visible = false;
                lblValueIndexU0PermPermCommentITClose.Text = string.Empty;
            }
        }
        else if (fromDiv == "waiting")
        {
            if (cmHeaderEmpNew != string.Empty)
            {
                divWaitingU0PermPermCommentHeaderEmpNew.Visible = true;
                lblValueWaitingU0PermPermCommentHeaderEmpNew.Text = nl2br(cmHeaderEmpNew);
            }
            else
            {
                divWaitingU0PermPermCommentHeaderEmpNew.Visible = false;
                lblValueWaitingU0PermPermCommentHeaderEmpNew.Text = string.Empty;
            }
            if (cmDirectorEmpNew != string.Empty)
            {
                divWaitingU0PermPermCommentDirectorEmpNew.Visible = true;
                lblValueWaitingU0PermPermCommentDirectorEmpNew.Text = nl2br(cmDirectorEmpNew);
            }
            else
            {
                divWaitingU0PermPermCommentDirectorEmpNew.Visible = false;
                lblValueWaitingU0PermPermCommentDirectorEmpNew.Text = string.Empty;
            }
            if (cmDirectorHR != string.Empty)
            {
                divWaitingU0PermPermCommentDirectorHR.Visible = true;
                lblValueWaitingU0PermPermCommentDirectorHR.Text = nl2br(cmDirectorHR);
            }
            else
            {
                divWaitingU0PermPermCommentDirectorHR.Visible = false;
                lblValueWaitingU0PermPermCommentDirectorHR.Text = string.Empty;
            }

            if (cmbeforeIT != string.Empty)
            {
                divWaitingU0PermPermCommentIT.Visible = true;
                lblValueWaitingU0PermPermCommentIT.Text = nl2br(cmbeforeIT);
            }
            else
            {
                divWaitingU0PermPermCommentIT.Visible = false;
                lblValueWaitingU0PermPermCommentIT.Text = string.Empty;
            }

            if (cmHeaderIT != string.Empty)
            {
                divWaitingU0PermPermCommentHeaderIT.Visible = true;
                lblValueWaitingU0PermPermCommentHeaderIT.Text = nl2br(cmHeaderIT);
            }
            else
            {
                divWaitingU0PermPermCommentHeaderIT.Visible = false;
                lblValueWaitingU0PermPermCommentHeaderIT.Text = string.Empty;
            }
            if (cmDirectorIT != string.Empty)
            {
                divWaitingU0PermPermCommentDirectorIT.Visible = true;
                lblValueWaitingU0PermPermCommentDirectorIT.Text = nl2br(cmDirectorIT);
            }
            else
            {
                divWaitingU0PermPermCommentDirectorIT.Visible = false;
                lblValueWaitingU0PermPermCommentDirectorIT.Text = string.Empty;
            }
            if (cmIT != string.Empty)
            {
                divWaitingU0PermPermCommentITClose.Visible = true;
                lblValueWaitingU0PermPermCommentITClose.Text = nl2br(cmIT);
            }
            else
            {
                divWaitingU0PermPermCommentITClose.Visible = false;
                lblValueWaitingU0PermPermCommentITClose.Text = string.Empty;
            }
        }
    }

    protected void visibleWaitingPermTempCommentText(string commentDirectorCreator, string commentHeaderIT, string commentDirectorIT)
    {
        if (commentDirectorCreator != string.Empty)
        {
            _divWaitingPermTempCmDirectorCreator.Visible = true;
            lblValueWaitingPermTempCmDirectorCreator.Text = commentDirectorCreator;
        }
        else
        {
            _divWaitingPermTempCmDirectorCreator.Visible = false;
        }
        if (commentHeaderIT != string.Empty)
        {
            _divWaitingPermTempCmHeaderIT.Visible = true;
            lblValueWaitingPermTempCmHeaderIT.Text = commentHeaderIT;
        }
        else
        {
            _divWaitingPermTempCmHeaderIT.Visible = false;
        }
        if (commentDirectorIT != string.Empty)
        {
            _divWaitingPermTempCmDirectorIT.Visible = true;
            lblValueWaitingPermTempCmDirectorIT.Text = commentDirectorIT;
        }
        else
        {
            _divWaitingPermTempCmDirectorIT.Visible = false;
        }
    }

    protected void visibleWaitingApproveMenu()
    {
        getEmpProfile(int.Parse(ViewState["empIDX"].ToString()));
        if (dataADOnline.ad_online_action[0].rdept_idx == Constants.RDEPT_MIS_ID)
        {
            _divMenuLiToDivWaiting.Visible = true;
        }
        else if (dataADOnline.ad_online_action[0].rdept_idx != Constants.RDEPT_MIS_ID &&
           dataADOnline.ad_online_action[0].jobgrade_level >= Constants.JOBLEVEL_OTHER_RDEPT)
        {
            _divMenuLiToDivWaiting.Visible = true;
        }
        else
        {
            _divMenuLiToDivWaiting.Visible = false;
        }
    }

    protected void scrollToTop()
    {
        ScriptManager.RegisterStartupScript(Page, this.GetType(), "scrollToTop", "window.scrollTo(0, 0);", true);
    }

    protected void getSetPermPolIntoToolTip(Label labelId)
    {
        permission_policy objPermPol = new permission_policy();
        dataADOnline.ad_permission_policy_action = new permission_policy[1];
        dataADOnline.ad_permission_policy_action[0] = objPermPol;
        dataADOnline = callServiceADOnline(_urlReadPermPol, dataADOnline);
        string tableStart = "<table class='table table-bordered'>";
        string tableTh = "<tr><th class='text-center'>สิทธิ์</th><th class='text-center'>ไม่สามารถใช้งาน Websiteได้</th></tr>";
        string tableEnd = "</table>";
        string tableTr = string.Empty;
        foreach (var rows in dataADOnline.ad_permission_policy_action)
        {
            tableTr += string.Format("<tr><td>{0}</td><td class='text-left'>{1}</td></tr>", rows.perm_pol_name, rows.perm_pol_desc);
        }
        labelId.Attributes.Add("data-toggle", "tooltip");
        labelId.Attributes.Add("data-placement", "top");
        labelId.Attributes.Add("data-html", "true");
        labelId.Attributes.Add("title", tableStart + tableTh + tableTr + tableEnd);
    }

    protected void getSetPermWhoApproveIntoToolTip(Label labelId, int node, int jobgrade, int rsecidx, int empidx, string table)
    {
        ad_online objADOnline = new ad_online();
        dataADOnline.ad_online_action = new ad_online[1];
        if (table == Constants.KEY_GET_FROM_PERM_PERM)
        {
            objADOnline.on_table = Constants.KEY_GET_FROM_PERM_PERM;

        }
        else
        {
            objADOnline.on_table = Constants.KEY_GET_FROM_PERM_TEMP;

        }
        objADOnline.with_where = Constants.WITH_WHERE_NO;
        objADOnline.rsec_idx = rsecidx;
        objADOnline.jobgrade_level = jobgrade;
        objADOnline.u0_node_idx_ref = node;
        objADOnline.emp_idx = empidx;
        dataADOnline.ad_online_action[0] = objADOnline;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline));

        dataADOnline = callServiceADOnline(_urlADSelectWho_CanApprove, dataADOnline);
        var rtcode = dataADOnline.return_code;

        if (rtcode == 0)
        {
            string tableStart = "<table>";
            string tableTh = "<tr><th class='text-center'>รายชื่อผู้ที่มีสิทธิ์อนุมัติ</th></tr>";
            string tableEnd = "</table>";
            string tableTr = string.Empty;
            foreach (var rows in dataADOnline.ad_online_action)
            {
                tableTr += string.Format("<tr><td class='text-left'>{0}</td></tr>", rows.FullNameTH);

            }
            labelId.Attributes.Add("data-toggle", "tooltip");
            labelId.Attributes.Add("data-placement", "top");
            labelId.Attributes.Add("data-html", "true");
            labelId.Attributes.Add("title", tableStart + tableTh + tableTr + tableEnd);
        }
    }

    protected string getEmails(int nodeId, int empIdx, string fromTable = "", string permIdxRef = "")
    {


        data_adonline dataADOnlineEmail = new data_adonline();
        ad_online objADOnlineEmail = new ad_online();
        dataADOnlineEmail.ad_online_action = new ad_online[1];
        objADOnlineEmail.on_table = fromTable;
        objADOnlineEmail.u0_node_idx_ref = nodeId;
        if (nodeId == Constants.NODE_PERM_TEMP_SAVE_ENDBY_IT || nodeId == Constants.NODE_PERM_PERM_IT_APP_FINISHBY_IT ||
            nodeId == Constants.NODE_PERM_PERM_DIRECTOR_HR_NOTAPP_ENDBY_DIRECTOR_HR || nodeId == Constants.NODE_PERM_PERM_HEADER_EMPNEW_NOTAPP_ENDBY_HEADER_EMPNEW ||
            nodeId == Constants.NODE_PERM_PERM_DIRECTOR_EMPNEW_NOTAPP_ENDBY_DIRECTOR_EMPNEW || nodeId == Constants.NODE_PERM_PERM_HEADER_IT_NOTAPP_ENDBY_HEADER_IT ||
            nodeId == Constants.NODE_PERM_PERM_DIRECTOR_IT_NOTAPP_ENDBY_DIRECTOR_IT || nodeId == Constants.NODE_PERM_PERM_IT_NOTAPP_ENDBY_IT || nodeId == Constants.node_perm_perm_it_notapp_endbyprocess_it)
        {
            objADOnlineEmail.u0_pk_perm_idx_ref = int.Parse(permIdxRef.ToString());
            objADOnlineEmail.from_table = fromTable;
        }
        objADOnlineEmail.emp_idx = empIdx;
        dataADOnlineEmail.ad_online_action[0] = objADOnlineEmail;
        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnlineEmail));
        dataADOnlineEmail = callServiceADOnline(_urlReadEmails, dataADOnlineEmail);


        if (dataADOnlineEmail.return_code == 0)
        {
            return dataADOnlineEmail.ad_online_action[0].emails_for_send;
        }
        return string.Empty;
    }

    protected void sentEmail(string emails, string permType, int nodeId, string bodyU0, string bodyU1 = "")
    {
        if (permType == Constants.KEY_GET_FROM_PERM_PERM)
        {
            if (emails != string.Empty)
            {
                string subject = "[MIS/AD Online : การขอใช้สิทธิ์ทั่วไป]";
                string body = string.Empty;
                if (Array.IndexOf(Constants.NODE_PERM_PERM_APPROVE, nodeId) > Constants.NUMBER_NULL)
                {
                    body = serviceMail.permPermAppTemplate(bodyU0);
                }
                else if (Array.IndexOf(Constants.NODE_PERM_PERM_NOT_APPROVE, nodeId) > Constants.NUMBER_NULL)
                {
                    body = serviceMail.permPermNotAppTemplate(bodyU0);
                }
                else if (Array.IndexOf(Constants.NODE_PERM_PERM_SUCCESS, nodeId) > Constants.NUMBER_NULL)
                {
                    body = serviceMail.permPermAppSuccessTemplate(bodyU0);
                }
                else if (nodeId == Constants.NODE_PERM_PERM_SENT_COMMENT)
                {
                    body = serviceMail.permPermSentCommentTemplate(bodyU0);
                }
                else if (Array.IndexOf(Constants.NODE_PERM_PERM_Edit, nodeId) > Constants.NUMBER_NULL)
                {
                    body = serviceMail.permPermSentBackUserTemplate(bodyU0);
                }
                else if (Array.IndexOf(Constants.NODE_PERM_PERM_NOT_IT_APPROVE, nodeId) > Constants.NUMBER_NULL)
                {
                    body = serviceMail.permPermITNotAppTemplate(bodyU0);
                }
                serviceMail.SendHtmlFormattedEmailFull(emails, "", "", subject, body); //emails
            }
        }
        else if (permType == Constants.KEY_GET_FROM_PERM_TEMP)
        {
            if (emails != string.Empty)
            {
                string subject = "[MIS/AD Online : การขอใช้สิทธิ์ชั่วคราว]";
                string body = string.Empty;
                if (Array.IndexOf(Constants.NODE_PERM_TEMP_APPROVE, nodeId) > Constants.NUMBER_NULL)
                {
                    body = serviceMail.permTempAppTemplate(bodyU0, bodyU1);
                }
                else if (Array.IndexOf(Constants.NODE_PERM_TEMP_NOT_APPROVE, nodeId) > Constants.NUMBER_NULL)
                {
                    body = serviceMail.permTempNotAppTemplate(bodyU0);
                }
                else if (Array.IndexOf(Constants.NODE_PERM_TEMP_SUCCESS, nodeId) > Constants.NUMBER_NULL)
                {
                    body = serviceMail.permTempAppSuccessTemplate(bodyU0, bodyU1);
                }
                serviceMail.SendHtmlFormattedEmailFull(emails, "", "", subject, body);
            }
        }
    }

    protected string dateToDB(string date)
    {
        if (date != string.Empty)
        {
            string[] dateSplit = date.Split('/');
            return string.Format("{0}-{1}-{2}", dateSplit[2], dateSplit[1], dateSplit[0]);
        }
        return string.Empty;
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {
            return "<span class='status-online f-bold'>Online</span>";
        }
        else if (status == 0)
        {
            return "<span class='status-offline f-bold'>Offline</span>";
        }
        else
        {
            return "<span class='status-offline f-bold'>Delete</span>";
        }
    }

    protected string nl2br(string text)
    {
        if (text != string.Empty)
        {
            return text.Replace("\n", "<br />");
        }
        return string.Empty;
    }

    protected string idCardPattern(string idCard, bool patternDisplay)
    {

        //litDebug.Text = idCard;
        //litDebug.Text = idCard.Length.ToString();
        int[] positionX = { 1, 6, 12, 15 };
        if (idCard != string.Empty && idCard.Length.ToString() == "13")
        {
            if (patternDisplay)
            {

                for (int i = 0; i < positionX.Length; i++)
                {
                    idCard = idCard.Insert(positionX[i], " ");
                }
                return idCard;
            }
            else
            {
                return idCard.Replace(" ", "");
            }
        }
        else
        {
            if(idCard.Length < 13)
            {
                idCard = idCard.ToString();
                return idCard;
            }
            else
            {
                return idCard.Replace(" ", "");
            }

        }
        //return string.Empty;
    }

    protected void changeMenuView(string menuViewName, string typeName = "")
    {
        if (menuViewName == "index")
        {
            if (typeName == Constants.KEY_GET_FROM_PERM_PERM)
            {
                divIndex.Visible = true;
                _divIndexPermPerm.Visible = true;
                _divIndexPermPerm_Detail.Visible = false;
                _divIndexPermTemp.Visible = false;
                _divIndexPermTemp_Detail.Visible = false;
                divCreate.Visible = false;
                divAddmore.Visible = false;
                divWaiting.Visible = false;
                actionIndexPermPerm();
            }
            else if (typeName == Constants.KEY_GET_FROM_PERM_TEMP)
            {
                divIndex.Visible = true;
                _divIndexPermPerm.Visible = false;
                _divIndexPermPerm_Detail.Visible = false;
                _divIndexPermTemp.Visible = true;
                _divIndexPermTemp_Detail.Visible = false;
                divCreate.Visible = false;
                divAddmore.Visible = false;
                divWaiting.Visible = false;
                actionIndexPermTemp();
            }
        }
        else if (menuViewName == "create")
        {

            divIndex.Visible = false;
            divCreate.Visible = true;
            _divCreate_PermPerm.Visible = false;
            _divCreate_PermTemp.Visible = false;
            divAddmore.Visible = false;
            divWaiting.Visible = false;

            getPermissionTypeToRDO(rdoPermissionType);


            getEmpProfile(int.Parse(ViewState["empIDX"].ToString()));
            setEmpProfile(txtCreatorEmpCode, txtCreatorFullName, txtCreatorOrgNameTH, txtCreatorDeptNameTH);
            setDefaultValueContactForm();

        }
        else if (menuViewName == "waiting")
        {
            if (typeName == Constants.KEY_GET_FROM_PERM_PERM)
            {
                divIndex.Visible = false;
                divCreate.Visible = false;
                divWaiting.Visible = true;
                _divWaitingPermPerm.Visible = true;
                _divWaitingPermPerm_Detail.Visible = false;
                _divWaitingPermTemp.Visible = false;
                divAddmore.Visible = false;
                _divWaitingPermTemp_Detail.Visible = false;
                actionWaitingPermPerm();
            }
            else if (typeName == Constants.KEY_GET_FROM_PERM_TEMP)
            {
                divIndex.Visible = false;
                divCreate.Visible = false;
                divWaiting.Visible = true;
                _divWaitingPermPerm.Visible = false;
                _divWaitingPermPerm_Detail.Visible = false;
                _divWaitingPermTemp.Visible = true;
                divAddmore.Visible = false;
                _divWaitingPermTemp_Detail.Visible = false;
                actionWaitingPermTemp();
            }
        }
        else if (menuViewName == "addmore")
        {
            divIndex.Visible = false;
            divCreate.Visible = false;
            divAddmore.Visible = true;
            _divCreate_PermPerm.Visible = false;
            _divCreate_PermTemp.Visible = false;
            divWaiting.Visible = false;
            getEmpProfile(int.Parse(ViewState["empIDX"].ToString()));
            setEmpProfile(txtCreatorEmpCode_Addmore, txtCreatorFullName_Addmore, txtCreatorOrgNameTH_Addmore, txtCreatorDeptNameTH_Addmore);

        }
        scrollToTop();
    }

    protected void activeMenu(string areaName)
    {
        if (areaName == "index")
        {
            _divMenuLiToDivIndex.Attributes.Add("class", "active");
            _divMenuLiToDivCreate.Attributes.Remove("class");
            _divMenuLiToDivAddMore.Attributes.Remove("class");
            _divMenuLiToDivWaiting.Attributes.Remove("class");
        }
        else if (areaName == "create")
        {
            _divMenuLiToDivIndex.Attributes.Remove("class");
            _divMenuLiToDivCreate.Attributes.Add("class", "active");
            _divMenuLiToDivAddMore.Attributes.Remove("class");
            _divMenuLiToDivWaiting.Attributes.Remove("class");
        }
        else if (areaName == "waiting")
        {
            _divMenuLiToDivIndex.Attributes.Remove("class");
            _divMenuLiToDivCreate.Attributes.Remove("class");
            _divMenuLiToDivAddMore.Attributes.Remove("class");
            _divMenuLiToDivWaiting.Attributes.Add("class", "active");
        }
        else if (areaName == "addmore")
        {
            _divMenuLiToDivIndex.Attributes.Remove("class");
            _divMenuLiToDivCreate.Attributes.Remove("class");
            _divMenuLiToDivAddMore.Attributes.Add("class", "active");
            _divMenuLiToDivWaiting.Attributes.Remove("class");
        }
    }

    protected data_adonline callServiceADOnline(string _cmdUrl, data_adonline _data_adonline)
    {
        _local_json = _funcTool.convertObjectToJson(_data_adonline);
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);
        _data_adonline = (data_adonline)_funcTool.convertJsonToObject(typeof(data_adonline), _local_json);
        return _data_adonline;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _data_employee)
    {
        _local_json = _funcTool.convertObjectToJson(_data_employee);
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _local_json);
        return _data_employee;
    }

    protected data_googlelicense callServiceGoogleLicense(string _cmdUrl, data_googlelicense _data_googlelicense)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_googlelicense);
        // litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_googlelicense = (data_googlelicense)_funcTool.convertJsonToObject(typeof(data_googlelicense), _localJson);

        return _data_googlelicense;
    }
    #endregion Custom Functions

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvShowVPNPerm":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var litvpnidx = ((Literal)e.Row.Cells[0].FindControl("litvpnidx"));
                    var chkvpn = ((CheckBox)e.Row.Cells[0].FindControl("chkvpn"));
                    var litvpn1idx = ((Literal)e.Row.Cells[1].FindControl("litvpn1idx"));
                    var litremark = ((Literal)e.Row.Cells[2].FindControl("litremark"));
                    var chksubvpn = ((CheckBoxList)e.Row.Cells[1].FindControl("chksubvpn"));

                    ad_online objADOnline = new ad_online();
                    dataADOnline.ad_online_action = new ad_online[1];

                    objADOnline.vpnidx = int.Parse(litvpnidx.Text);

                    dataADOnline.ad_online_action[0] = objADOnline;

                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline));

                    dataADOnline = callServiceADOnline(_urlSelectVPNSUBMAIN, dataADOnline);

                    setChkData(chksubvpn, dataADOnline.ad_online_action, "vpn1_name", "vpn1idx");
                    //setRadioData(rdovpn_edit, dataADOnline.ad_online_action, "vpn1_name", "vpn1idx");
                    if (lblvpnidx.Text != "0" && lblvpnidx.Text != "")
                    {
                        string[] Tovpn = lblvpnidx.Text.Split(',');
                        foreach (string To_check in Tovpn)
                        {
                            if (int.Parse(To_check) == int.Parse(litvpnidx.Text))
                            {
                                chkvpn.Checked = true;
                                litremark.Visible = true;
                                chksubvpn.Visible = true;
                                break;
                            }
                            else
                            {
                                litremark.Visible = false;
                                chksubvpn.Visible = false;
                            }
                        }

                        SelectU2VPN(litvpn1idx, litremark, int.Parse(lblValueU0PermPermIDX_Perm.Text), int.Parse(litvpnidx.Text), "PERM", 1);

                        string[] Tosubvpn = litvpn1idx.Text.Split(',');


                        foreach (ListItem li in chksubvpn.Items)
                        {
                            foreach (string To_check in Tosubvpn)
                            {
                                if (li.Value.ToString() == To_check.ToString())
                                {
                                    li.Selected = true;
                                    break;
                                }

                            }
                        }
                        litremark.Text = litremark.Text;
                    }

                }

                break;

            case "GvShowVPNTemp":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var litvpnidx_temp = ((Literal)e.Row.Cells[0].FindControl("litvpnidx_temp"));
                    var chkvpn_temp = ((CheckBox)e.Row.Cells[0].FindControl("chkvpn_temp"));
                    var litvpn1idx_temp = ((Literal)e.Row.Cells[1].FindControl("litvpn1idx_temp"));
                    //var rdovpn_edit = ((RadioButtonList)e.Row.Cells[1].FindControl("rdovpn_edit"));
                    var litremark_temp = ((Literal)e.Row.Cells[2].FindControl("litremark_temp"));
                    var chksubvpn_temp = ((CheckBoxList)e.Row.Cells[1].FindControl("chksubvpn_temp"));

                    ad_online objADOnline = new ad_online();
                    dataADOnline.ad_online_action = new ad_online[1];

                    objADOnline.vpnidx = int.Parse(litvpnidx_temp.Text);

                    dataADOnline.ad_online_action[0] = objADOnline;

                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline));

                    dataADOnline = callServiceADOnline(_urlSelectVPNSUBMAIN, dataADOnline);
                    setChkData(chksubvpn_temp, dataADOnline.ad_online_action, "vpn1_name", "vpn1idx");

                    if (lblvpnidxtemp.Text != "0" && lblvpnidxtemp.Text != "")
                    {
                        string[] Tovpn = lblvpnidxtemp.Text.Split(',');
                        foreach (string To_check in Tovpn)
                        {
                            if (To_check != String.Empty && To_check != "0")
                            {
                                if (int.Parse(To_check) == int.Parse(litvpnidx_temp.Text))
                                {

                                    chkvpn_temp.Checked = true;
                                    litremark_temp.Visible = true;
                                    chksubvpn_temp.Visible = true;
                                    break;
                                }
                                else
                                {
                                    litremark_temp.Visible = false;
                                    chksubvpn_temp.Visible = false;
                                }
                            }
                        }

                        SelectU2VPN(litvpn1idx_temp, litremark_temp, int.Parse(lblValueU0PermTempIDX_Temp.Text), int.Parse(litvpnidx_temp.Text), "TEMP", 1);


                        string[] Tosubvpn = litvpn1idx_temp.Text.Split(',');

                        foreach (ListItem li in chksubvpn_temp.Items)
                        {
                            foreach (string To_check in Tosubvpn)
                            {
                                if (li.Value.ToString() == To_check.ToString())
                                {
                                    li.Selected = true;
                                    break;
                                }
                            }
                        }
                        litremark_temp.Text = litremark_temp.Text;
                    }
                }

                break;

            case "GvEditVPN":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var litvpnidxedit = ((Literal)e.Row.Cells[0].FindControl("litvpnidxedit"));
                    var chkvpnedit = ((CheckBox)e.Row.Cells[0].FindControl("chkvpnedit"));
                    var litvpn1idxedit = ((Literal)e.Row.Cells[1].FindControl("litvpn1idxedit"));
                    var litremarkedit = ((Literal)e.Row.Cells[2].FindControl("litremarkedit"));
                    var txtremarkedit = ((TextBox)e.Row.Cells[2].FindControl("txtremarkedit"));
                    var chksubvpnedit = ((CheckBoxList)e.Row.Cells[1].FindControl("chksubvpnedit"));

                    ad_online objADOnline = new ad_online();
                    dataADOnline.ad_online_action = new ad_online[1];

                    objADOnline.vpnidx = int.Parse(litvpnidxedit.Text);

                    dataADOnline.ad_online_action[0] = objADOnline;

                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline));

                    dataADOnline = callServiceADOnline(_urlSelectVPNSUBMAIN, dataADOnline);
                    setChkData(chksubvpnedit, dataADOnline.ad_online_action, "vpn1_name", "vpn1idx");

                    if (lblvpnidxedit.Text != "0" && lblvpnidxedit.Text != "")
                    {
                        string[] Tovpn = lblvpnidxedit.Text.Split(',');
                        foreach (string To_check in Tovpn)
                        {
                            if (int.Parse(To_check) == int.Parse(litvpnidxedit.Text))
                            {
                                chkvpnedit.Checked = true;
                                txtremarkedit.Visible = true;
                                chksubvpnedit.Visible = true;
                                break;
                            }
                            else
                            {
                                txtremarkedit.Visible = false;
                                chksubvpnedit.Visible = false;
                            }
                        }
                        SelectU2VPN(litvpn1idxedit, litremarkedit, int.Parse(lblValueU0PermPermIdx.Text), int.Parse(litvpnidxedit.Text), "PERM", 1);

                        string[] Tosubvpn = litvpn1idxedit.Text.Split(',');


                        foreach (ListItem li in chksubvpnedit.Items)
                        {
                            foreach (string To_check in Tosubvpn)
                            {
                                if (li.Value.ToString() == To_check.ToString())
                                {
                                    li.Selected = true;
                                    break;
                                }

                            }
                        }
                        txtremarkedit.Text = litremarkedit.Text;
                    }
                    else
                    {
                        txtremarkedit.Visible = false;
                        chksubvpnedit.Visible = false;
                    }

                }

                break;

            case "GvEditVPNTemp":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var litvpnidxedittemp = ((Literal)e.Row.Cells[0].FindControl("litvpnidxedittemp"));
                    var chkvpnedittemp = ((CheckBox)e.Row.Cells[0].FindControl("chkvpnedittemp"));
                    var litvpn1idxedittemp = ((Literal)e.Row.Cells[1].FindControl("litvpn1idxedittemp"));
                    var litremarkedittemp = ((Literal)e.Row.Cells[2].FindControl("litremarkedittemp"));
                    var txtremarkedittemp = ((TextBox)e.Row.Cells[2].FindControl("txtremarkedittemp"));
                    var chksubvpnedittemp = ((CheckBoxList)e.Row.Cells[1].FindControl("chksubvpnedittemp"));

                    ad_online objADOnline = new ad_online();
                    dataADOnline.ad_online_action = new ad_online[1];

                    objADOnline.vpnidx = int.Parse(litvpnidxedittemp.Text);

                    dataADOnline.ad_online_action[0] = objADOnline;

                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline));

                    dataADOnline = callServiceADOnline(_urlSelectVPNSUBMAIN, dataADOnline);
                    setChkData(chksubvpnedittemp, dataADOnline.ad_online_action, "vpn1_name", "vpn1idx");


                    if (lblvpnidxedittemp.Text != "0" && lblvpnidxedittemp.Text != "")
                    {
                        string[] Tovpn = lblvpnidxedittemp.Text.Split(',');
                        foreach (string To_check in Tovpn)
                        {
                            if (int.Parse(To_check) == int.Parse(litvpnidxedittemp.Text))
                            {
                                chkvpnedittemp.Checked = true;
                                txtremarkedittemp.Visible = true;
                                chksubvpnedittemp.Visible = true;
                                break;
                            }
                            else
                            {
                                txtremarkedittemp.Visible = false;
                                chksubvpnedittemp.Visible = false;
                            }
                        }

                        SelectU2VPN(litvpn1idxedittemp, litremarkedittemp, int.Parse(lblValueU0PermTempIdx.Text), int.Parse(litvpnidxedittemp.Text), "TEMP", 1);

                        string[] Tosubvpn = litvpn1idxedittemp.Text.Split(',');
                        foreach (ListItem li in chksubvpnedittemp.Items)
                        {
                            foreach (string To_check in Tosubvpn)
                            {
                                if (li.Value.ToString() == To_check.ToString())
                                {
                                    li.Selected = true;
                                    break;
                                }

                            }
                        }
                        txtremarkedittemp.Text = litremarkedittemp.Text;
                    }
                    else
                    {
                        txtremarkedittemp.Visible = false;
                        chksubvpnedittemp.Visible = false;
                    }
                }


                break;


            case "GvAddmore":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var litvpnidx_addmore = ((Literal)e.Row.Cells[0].FindControl("litvpnidx_addmore"));
                    var chkvpn_addmore = ((CheckBox)e.Row.Cells[0].FindControl("chkvpn_addmore"));
                    var litvpn1idx_addmore = ((Literal)e.Row.Cells[1].FindControl("litvpn1idx_addmore"));
                    var litremark_addmore = ((Literal)e.Row.Cells[2].FindControl("litremark_addmore"));
                    var txtremark_addmore = ((TextBox)e.Row.Cells[2].FindControl("txtremark_addmore"));
                    var chksubvpn_addmore = ((CheckBoxList)e.Row.Cells[1].FindControl("chksubvpn_addmore"));

                    ad_online objADOnline = new ad_online();
                    dataADOnline.ad_online_action = new ad_online[1];

                    objADOnline.vpnidx = int.Parse(litvpnidx_addmore.Text);

                    dataADOnline.ad_online_action[0] = objADOnline;

                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline));

                    dataADOnline = callServiceADOnline(_urlSelectVPNSUBMAIN, dataADOnline);
                    setChkData(chksubvpn_addmore, dataADOnline.ad_online_action, "vpn1_name", "vpn1idx");


                    if (txtvpnidx_addmore.Text != "0" && txtvpnidx_addmore.Text != "")
                    {
                        string[] Tovpn = txtvpnidx_addmore.Text.Split(',');
                        foreach (string To_check in Tovpn)
                        {
                            if (int.Parse(To_check) == int.Parse(litvpnidx_addmore.Text))
                            {
                                chkvpn_addmore.Checked = true;
                                txtremark_addmore.Visible = true;
                                chksubvpn_addmore.Visible = true;
                                break;
                            }
                            else
                            {
                                txtremark_addmore.Visible = false;
                                chksubvpn_addmore.Visible = false;
                            }
                        }

                        SelectU2VPN(litvpn1idx_addmore, litremark_addmore, int.Parse(txtEmpNewEmpCode_Addmore.Text), int.Parse(litvpnidx_addmore.Text), "PERM", 2);

                        string[] Tosubvpn = litvpn1idx_addmore.Text.Split(',');
                        foreach (ListItem li in chksubvpn_addmore.Items)
                        {
                            foreach (string To_check in Tosubvpn)
                            {
                                if (li.Value.ToString() == To_check.ToString())
                                {
                                    li.Selected = true;

                                    break;
                                }
                                //txtremark_addmore.Text = litremark_addmore.Text;

                            }
                        }

                    }
                    else
                    {
                        txtremark_addmore.Visible = false;
                        chksubvpn_addmore.Visible = false;
                    }
                }
                break;

            case "gvFile":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;

            case "gvFileTemp":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11_temp = (HyperLink)e.Row.Cells[0].FindControl("btnDL11_temp");
                    HiddenField hidFile11_temp = (HiddenField)e.Row.Cells[0].FindControl("hidFile11_temp");
                    // Display the company name in italics.
                    string LinkHost11_temp = string.Format("http://{0}", Request.Url.Host);

                    btnDL11_temp.NavigateUrl = LinkHost11_temp + MapURL(hidFile11_temp.Value);
                }

                break;

            case "gvFilePerm_Wait":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11_permwait = (HyperLink)e.Row.Cells[0].FindControl("btnDL11_permwait");
                    HiddenField hidFile11_permwait = (HiddenField)e.Row.Cells[0].FindControl("hidFile11_permwait");
                    string LinkHost11_permwait = string.Format("http://{0}", Request.Url.Host);

                    btnDL11_permwait.NavigateUrl = LinkHost11_permwait + MapURL(hidFile11_permwait.Value);
                }

                break;

            case "gvFileTemp_Wait":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11_tempwait = (HyperLink)e.Row.Cells[0].FindControl("btnDL11_tempwait");
                    HiddenField hidFile11_tempwait = (HiddenField)e.Row.Cells[0].FindControl("hidFile11_tempwait");
                    // Display the company name in italics.
                    string LinkHost11_tempwait = string.Format("http://{0}", Request.Url.Host);

                    btnDL11_tempwait.NavigateUrl = LinkHost11_tempwait + MapURL(hidFile11_tempwait.Value);
                }

                break;

        }
    }

    protected void SelectU2VPN(Literal lit, Literal litremarkselect, int id, int vpnidx, string table, int condition)
    {
        //   dataADOnline = new data_adonline();
        ad_online objADOnline = new ad_online();
        dataADOnline.ad_online_action = new ad_online[1];

        objADOnline.u0_perm_perm_idx = id;
        objADOnline.vpnidx = vpnidx;
        objADOnline.on_table = table;
        objADOnline.condition_addmore = condition;


        dataADOnline.ad_online_action[0] = objADOnline;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataADOnline));


        dataADOnline = callServiceADOnline(_urlSelectU2VPN, dataADOnline);

        if (dataADOnline.return_code.ToString() == "0")
        {
            //txt.Text = dataADOnline.return_code.ToString();
            lit.Text = dataADOnline.ad_online_action[0].vpn1_name;
            litremarkselect.Text = dataADOnline.ad_online_action[0].remark;

        }

    }
    protected void setRadioData(RadioButtonList rdoName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        rdoName.Items.Clear();
        // bind items
        rdoName.DataSource = obj;
        rdoName.DataTextField = _data_text;
        rdoName.DataValueField = _data_value;
        rdoName.DataBind();
    }
    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        chkName.Items.Clear();
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    #endregion

    #region Directories_File URL


    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }


    public void SearchDirectories(DirectoryInfo dir, String target, GridView gvName)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");


        FileInfo[] files = dir.GetFiles();
        int i = 0;
        foreach (FileInfo file in files)
        {
            //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
            //{
            string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
            dt1.Rows.Add(file.Name);
            dt1.Rows[i][1] = f[0];
            i++;
            //}
        }


        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gvName.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvName.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvName.DataSource = null;
            gvName.DataBind();

        }


    }

    #endregion

}
