﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_permission.aspx.cs" Inherits="websystem_hr_hr_permission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="text" runat="server"></asp:Literal>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; Permission</strong></h3>
                        </div>

                        <div class="panel-body">
                            <%-- <div class="form-horizontal" role="form">--%>
                            <div class="form-group">

                                <asp:LinkButton ID="btnaddholder" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddStatus" runat="server" CommandName="CmdAddHolder" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <%------------------------ Div ADD  ------------------------%>

                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <div class="form-horizontal" role="form">
                                        <%--<div class="panel panel-default">--%>
                                        <div class="panel-heading">

                                            <asp:Panel ID="BoxPosition_add" runat="server">
                                                <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; ตำแหน่งผู้รับผิดชอบ</strong></h4>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label ">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label13" runat="server" Text="Name System" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label14" runat="server" Text="ระบบงาน" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlsystem" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">เลือกระบบงาน....</asp:ListItem>
                                                            <asp:ListItem Value="1">สิทธิ์ระบบใบลา-ระบบโอที</asp:ListItem>
                                                            <asp:ListItem Value="2">สิทธิ์ระบบกะทำงาน</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredsFieldValidator5" ValidationGroup="AddPosAdd_Add" runat="server" Display="None"
                                                            ControlToValidate="ddlsystem" Font-Size="11"
                                                            ErrorMessage="เลือกระบบงาน ...."
                                                            ValidationExpression="เลือกระบบงาน ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredsFieldValidator5" Width="160" />
                                                    </div>

                                                    <label class="col-sm-2 control-label col-sm-offset-1">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label5" runat="server" Text="Permission Type" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label6" runat="server" Text="ประเภทสิทธิ์" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlpermission_type" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">เลือกประเภทสิทธิ์....</asp:ListItem>
                                                            <asp:ListItem Value="1">สิทธิ์สร้างรายการ</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="AddPosAdd_Add" runat="server" Display="None"
                                                            ControlToValidate="ddlpermission_type" Font-Size="11"
                                                            ErrorMessage="เลือกประเภทสิทธิ์ ...."
                                                            ValidationExpression="เลือกประเภทสิทธิ์ ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requi3redFi54e4ldValidator1" Width="160" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label ">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label186" runat="server" Text="Organization" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label187" runat="server" Text="องค์กร" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlorg_add" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Requi3redFi54e4ldValidator1" ValidationGroup="AddPosAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlorg_add" Font-Size="11"
                                                            ErrorMessage="เลือกองค์กร ...."
                                                            ValidationExpression="เลือกองค์กร ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requi3redFi54e4ldValidator1" Width="160" />
                                                    </div>

                                                    <label class="col-sm-2 control-label col-sm-offset-1">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label184" runat="server" Text="Department" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label185" runat="server" Text="ฝ่าย" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddldep_add" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldV5alidator3" ValidationGroup="AddPosAdd" runat="server" Display="None"
                                                            ControlToValidate="ddldep_add" Font-Size="11"
                                                            ErrorMessage="เลือกฝ่าย ...."
                                                            ValidationExpression="เลือกฝ่าย ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCaleloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldV5alidator3" Width="160" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label188" runat="server" Text="Section" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label189" runat="server" Text="หน่วยงาน" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlsec_add" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">เลือกหน่วยงาน....</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequireddField3Validator3" ValidationGroup="AddPosAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlsec_add" Font-Size="11"
                                                            ErrorMessage="เลือกหน่วยงาน ...."
                                                            ValidationExpression="เลือกหน่วยงาน ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidfatorCalloutExtendder2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequireddField3Validator3" Width="160" />
                                                    </div>

                                                    <label class="col-sm-2 control-label col-sm-offset-1">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label190" runat="server" Text="Position" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label191" runat="server" Text="ตำแหน่ง" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlpos_add" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">เลือกตำแหน่ง....</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFeieldValidator3" ValidationGroup="AddPosAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlpos_add" Font-Size="11"
                                                            ErrorMessage="เลือกตำแหน่ง ...."
                                                            ValidationExpression="เลือกตำแหน่ง ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValiddatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFeieldValidator3" Width="160" />
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <asp:LinkButton ID="btnAdd_Pos_add" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddPosAdd_Add" ValidationGroup="AddPosAdd">ADD + &nbsp;</asp:LinkButton>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-lg-12">
                                                        <asp:GridView ID="GvPosAdd_Add"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover"
                                                            HeaderStyle-CssClass="table_headCenter"
                                                            HeaderStyle-Height="40px"
                                                            ShowFooter="False"
                                                            ShowHeaderWhenEmpty="True"
                                                            AllowPaging="True"
                                                            PageSize="10"
                                                            BorderStyle="None"
                                                            CellSpacing="2"
                                                            OnRowDeleting="gvRowDeleting">

                                                            <PagerStyle CssClass="PageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>

                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <%# (Container.DataItemIndex +1) %>
                                                                            <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("OrgIDX_S")%>' Visible="false"></asp:Label>
                                                                            <asp:Label ID="lbP24ag2e1" runat="server" Text='<%# Eval("RDeptIDX_S")%>' Visible="false"></asp:Label>
                                                                            <asp:Label ID="lb4Pag2e1" runat="server" Text='<%# Eval("RSecIDX_S")%>' Visible="false"></asp:Label>
                                                                            <asp:Label ID="lblrpos_idx" runat="server" Text='<%# Eval("RPosIDX_S")%>' Visible="false"></asp:Label>
                                                                            <asp:Label ID="lblp_system" runat="server" Text='<%# Eval("p_system_s")%>' Visible="false"></asp:Label>
                                                                            <asp:Label ID="lblp_permission_type" runat="server" Text='<%# Eval("p_permission_type_s")%>' Visible="false"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="สำนักงาน">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Label192" runat="server" Text='<%# Eval("OrgNameTH_S")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ฝ่าย">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Label193" runat="server" Text='<%# Eval("DeptNameTH_S")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="แผนก">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Labe2l194" runat="server" Text='<%# Eval("SecNameTH_S")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ตำแหน่ง">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="La3bel195" runat="server" Text='<%# Eval("PosNameTH_S")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ระบบงาน">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Labe4l3195" runat="server" Text='<%# Eval("p_system_name_s")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ประเภทสิทธิ์">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="La42bel195" runat="server" Text='<%# Eval("p_permission_type_name_s")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="จัดการ">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>

                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="BoxSection_add" runat="server">
                                                <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; แผนกที่ดูแล</strong></h4>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label ">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label7" runat="server" Text="Organization" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label8" runat="server" Text="องค์กร" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlorg_add_permis" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="AddSecAdd_Permis" runat="server" Display="None"
                                                            ControlToValidate="ddlorg_add_permis" Font-Size="11"
                                                            ErrorMessage="เลือกองค์กร ...."
                                                            ValidationExpression="เลือกองค์กร ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requi3redFi54e4ldValidator1" Width="160" />
                                                    </div>

                                                    <label class="col-sm-2 control-label col-sm-offset-1">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label9" runat="server" Text="Department" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label10" runat="server" Text="ฝ่าย" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddldep_add_permis" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="AddSecAdd_Permis" runat="server" Display="None"
                                                            ControlToValidate="ddldep_add_permis" Font-Size="11"
                                                            ErrorMessage="เลือกฝ่าย ...."
                                                            ValidationExpression="เลือกฝ่าย ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldV5alidator3" Width="160" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label11" runat="server" Text="Section" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label12" runat="server" Text="หน่วยงาน" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlsec_add_permis" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">เลือกหน่วยงาน....</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="AddSecAdd_Permis" runat="server" Display="None"
                                                            ControlToValidate="ddlsec_add_permis" Font-Size="11"
                                                            ErrorMessage="เลือกหน่วยงาน ...."
                                                            ValidationExpression="เลือกหน่วยงาน ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequireddField3Validator3" Width="160" />
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <asp:LinkButton ID="btnAdd_Sec_permis" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddSecAdd_Permis" ValidationGroup="AddSecAdd_Permis">ADD + &nbsp;</asp:LinkButton>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-lg-12">
                                                        <asp:GridView ID="GvSecAdd_Permis"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover"
                                                            HeaderStyle-CssClass="table_headCenter"
                                                            HeaderStyle-Height="40px"
                                                            ShowFooter="False"
                                                            ShowHeaderWhenEmpty="True"
                                                            AllowPaging="True"
                                                            PageSize="10"
                                                            BorderStyle="None"
                                                            CellSpacing="2"
                                                            OnRowDeleting="gvRowDeleting">

                                                            <PagerStyle CssClass="PageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>

                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <%# (Container.DataItemIndex +1) %>
                                                                            <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("OrgIDX_Se")%>' Visible="false"></asp:Label>
                                                                            <asp:Label ID="lbP24ag2e1" runat="server" Text='<%# Eval("RDeptIDX_Se")%>' Visible="false"></asp:Label>
                                                                            <asp:Label ID="lblsec_idx" runat="server" Text='<%# Eval("RSecIDX_Se")%>' Visible="false"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="สำนักงาน">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Label192" runat="server" Text='<%# Eval("OrgNameTH_Se")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ฝ่าย">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Label193" runat="server" Text='<%# Eval("DeptNameTH_Se")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="แผนก">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Lab3el194" runat="server" Text='<%# Eval("SecNameTH_Se")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="จัดการ">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>

                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <hr />
                                    <%--  </div>--%>
                                </div>
                            </asp:Panel>

                            <div id="div_search" runat="server">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">

                                        <asp:Label ID="Label160" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlorg_search" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Label ID="Label1" runat="server" Text="ฝ่าย" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddldep_search" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <asp:Label ID="Label2" runat="server" Text="แผนก" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsec_search" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกหน่วยงาน....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Label ID="Label3" runat="server" Text="ตำแหน่ง" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlpos_search" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกตำแหน่ง....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label4" runat="server" Text="ระบบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsystem_search" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกระบบงาน....</asp:ListItem>
                                                <asp:ListItem Value="1">สิทธิ์ระบบใบลา-ระบบโอที</asp:ListItem>
                                                <asp:ListItem Value="2">สิทธิ์ระบบกะทำงาน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Label ID="Label15" runat="server" Text="ประเภท" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlpermission_search" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกประเภทสิทธิ์....</asp:ListItem>
                                                <asp:ListItem Value="1">สิทธิ์สร้างรายการ</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">

                                            <asp:LinkButton ID="LinkButton36" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdSearch" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton37" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefresh" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                            </div>



                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="peridx"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ตำแหน่งที่มีสิทธิ์">
                                        <ItemTemplate>
                                            <small>
                                                <b>
                                                    <asp:Label ID="fsse" runat="server" Text="บริษัท : " /></b><asp:Label ID="lbldAsseta" runat="server" Text='<%# Eval("org_name_set") %>'></asp:Label>
                                                </p>                                    
                                    <b>
                                        <asp:Label ID="Labesl53" runat="server" Text="ฝ่าย : " /></b><asp:Label ID="Lafbel59" runat="server" Text='<%# Eval("rdep_name_set") %>'></asp:Label>
                                                </p>                                    
                                    <b>
                                        <asp:Label ID="Labsel30" runat="server" Text="แผนก : " /></b><asp:Label ID="Labrel44" runat="server" Text='<%# Eval("rsec_name_set") %>'></asp:Label>
                                                </p> 
                                                <b>
                                                    <asp:Label ID="Lab3el1" runat="server" Text="ตำแหน่ง : " /></b><asp:Label ID="Lab3el2" runat="server" Text='<%# Eval("rpos_name_set") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ระบบ">
                                        <ItemTemplate>
                                            <div class="panel-heading">
                                                <asp:Label ID="Label193" runat="server" Text='<%# Eval("p_system_name")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภท">
                                        <ItemTemplate>
                                            <div class="panel-heading">
                                                <asp:Label ID="Lab1el194" runat="server" Text='<%# Eval("p_permission_type_name")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="แผนกที่รับผิดชอบ">
                                        <ItemTemplate>
                                            <small>
                                                <b>
                                                    <asp:Label ID="fssde" runat="server" Text="บริษัท : " /></b><asp:Label ID="lbldAsgseta" runat="server" Text='<%# Eval("org_name_use") %>'></asp:Label>
                                                </p>                                    
                                    <b>
                                        <asp:Label ID="Labesdl53" runat="server" Text="ฝ่าย : " /></b><asp:Label ID="Lafbsel59" runat="server" Text='<%# Eval("rdep_name_use") %>'></asp:Label>
                                                </p>                                    
                                    <b>
                                        <asp:Label ID="Labsesl30" runat="server" Text="แผนก : " /></b><asp:Label ID="Labrfel44" runat="server" Text='<%# Eval("rsec_name_use") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <%--<asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>--%>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("peridx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

