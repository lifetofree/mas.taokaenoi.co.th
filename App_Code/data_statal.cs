﻿using System;
using System.Xml.Serialization;

#region data statal
[Serializable]
[XmlRoot("data_statal")]
public class data_statal
{
   [XmlElement("return_code")]
   public int return_code { get; set; }
   [XmlElement("return_msg")]
   public string return_msg { get; set; }
   [XmlElement("statal_ipaddress_action")]
   public ipaddress[] statal_ipaddress_action { get; set; }
   [XmlElement("statal_log_ipaddress_action")]
   public log_ipaddress[] statal_log_ipaddress_action { get; set; }
   [XmlElement("statal_config_action")]
   public config[] statal_config_action { get; set; }
}
#endregion data statal

#region ipaddress
[Serializable]
public class ipaddress
{
   [XmlElement("m0_ipaddress_idx")]
   public int m0_ipaddress_idx { get; set; }
   [XmlElement("ipaddress_name")]
   public string ipaddress_name { get; set; }
   [XmlElement("ipaddress_description")]
   public string ipaddress_description { get; set; }
   [XmlElement("ipaddress_status")]
   public int ipaddress_status { get; set; }
   [XmlElement("keyword_search")]
   public string keyword_search { get; set; }
}
#endregion ipaddress

#region log_ipaddress
[Serializable]
public class log_ipaddress
{
   [XmlElement("l0_ipaddress_idx")]
   public int l0_ipaddress_idx { get; set; }
   [XmlElement("ipaddress_name")]
   public string ipaddress_name { get; set; }
   [XmlElement("ipaddress_status")]
   public string ipaddress_status { get; set; }
   [XmlElement("ipaddress_created_at")]
   public string ipaddress_created_at { get; set; }
   [XmlElement("search_from_date_time")]
   public string search_from_date_time { get; set; }
   [XmlElement("search_to_date_time")]
   public string search_to_date_time { get; set; }
   [XmlElement("keyword_search")]
   public string keyword_search { get; set; }
   [XmlElement("count_ipaddress_name")]
   public int count_ipaddress_name { get; set; }
}
#endregion log_ipaddress

#region config
[Serializable]
public class config
{
   [XmlElement("m0_config_idx")]
   public int m0_config_idx { get; set; }
   [XmlElement("config_timeout")]
   public int config_timeout { get; set; }
   [XmlElement("config_type_msg")]
   public string config_type_msg { get; set; }
   [XmlElement("config_token")]
   public string config_token { get; set; }
   [XmlElement("config_updated_at")]
   public string config_updated_at { get; set; }
   [XmlElement("config_updated_by")]
   public int config_updated_by { get; set; }
}
#endregion config


