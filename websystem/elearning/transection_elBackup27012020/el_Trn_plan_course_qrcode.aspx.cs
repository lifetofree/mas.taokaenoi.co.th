﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_Trn_plan_course_qrcode : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_employee = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_employee"];

    static string _urlGetel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_plan_course"];
    static string _urlSetInsel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_plan_course"];
    static string _urlDelel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_u_plan_course"];
    static string _urlSetUpdel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_u_plan_course"];

    static string _urlsendEmail_plan_course_hrtohrd = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_hrtohrd"];
    static string _urlsendEmail_plan_course_hrdtomd = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_hrdtomd"];
    static string _urlsendEmail_plan_course_hrdtohr_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_hrdtohr_all"];
    static string _urlsendEmail_plan_course_mdtohrd_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_mdtohrd_all"];
    static string _urlsendEmail_outplan_course_usertoleader = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_outplan_course_usertoleader"];
    static string _urlsendemail_outplan_leadertohr = _serviceUrl + ConfigurationManager.AppSettings["urlsendemail_outplan_leadertohr"];
    static string _urlsendemail_outplan_leadertouser_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendemail_outplan_leadertouser_all"];


    static string _urlGetel_Report_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_Report_plan_course"];

    //หลักสูตร 
    static string _urlSetInsel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_course"];

    string _PathFileimage = ConfigurationSettings.AppSettings["path_flieimage_elearning"];
    string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];

    string _localJson = "";
    int _tempInt = 0;
    int emp_idx = 0;
    #endregion Init

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int id = 0;
            ViewState["_id"] = "0";
            try
            {
                if (Page.RouteData.Values["id"] != null)
                {
                    string sid = Page.RouteData.Values["id"].ToString().ToLower();
                    string[] urldecry = (_funcTool.getDecryptRC4(sid, "id")).Split('|');
                    sid = urldecry[0];
                    id = _func_dmu.zStringToInt(sid);
                    ViewState["_id"] = sid;
                }
            }
            catch
            {

            }
            setTitle(id);
        }

    }
    private void setTitle(int id)
    {
        litcourse.Text = "";
        dv_header.Attributes.Add("style", "background-image: url(" + ResolveUrl("~/images/elearning/qrcodeform/qrcode_header.png") + "); height: 100%; background-position: top; background-repeat: no-repeat; background-size: 100% 100%;");
        dv_footer.Attributes.Add("style", "background-image: url(" + ResolveUrl("~/images/elearning/qrcodeform/qrcode_footer.png") + "); height: 40%; background-position: left 0 bottom 0; background-repeat: no-repeat; background-size: 100% 40%;");

        data_elearning dataelearning_detail;
        training_course obj_detail;
        //el_u1_training_course_lecturer
        dataelearning_detail = new data_elearning();
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "U1-FULL";
        dataelearning_detail.el_training_course_action[0] = obj_detail;
        dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
        CreateDs_el_u1_training_course_lecturer();
        CreateDs_el_tc_evaluationform();
        
        if (dataelearning_detail.el_training_course_action != null)
        {
            
            DataSet ds = (DataSet)ViewState["vsel_u1_training_course_lecturer"];
            DataRow dr;
            foreach (var v_item in dataelearning_detail.el_training_course_action)
            {
                dr = ds.Tables["dsel_u1_training_course_lecturer"].NewRow();
                dr["lecturer_type"] = v_item.lecturer_type.ToString();
                dr["m0_institution_idx_ref"] = v_item.m0_institution_idx_ref.ToString();
                dr["zName"] = v_item.institution_name;
                ds.Tables["dsel_u1_training_course_lecturer"].Rows.Add(dr);
            }
            ViewState["vsel_u1_training_course_lecturer"] = ds;
        }
        _func_dmu.zSetGridData(Gvinstitution, ViewState["vsel_u1_training_course_lecturer"]);

        
        dataelearning_detail = new data_elearning();
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "U0-FULL-QRCODE";
        dataelearning_detail.el_training_course_action[0] = obj_detail;
        dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
        if (dataelearning_detail.el_training_course_action != null)
        {
            obj_detail = dataelearning_detail.el_training_course_action[0];
            litcourse.Text = obj_detail.training_course_no + " - " + obj_detail.zcourse_name;
            _func_dmu.zSetGridData(GvListData, dataelearning_detail.el_training_course_action);
        }
        SETFOCUS.Focus();

    }
    private void CreateDs_el_tc_evaluationform()
    {
        string sDs = "dsel_tc_evaluationform";
        string sVs = "vsel_tc_evaluationform";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("m0_evaluation_f_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("m0_evaluation_group_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("course_item", typeof(String));
        ds.Tables[sDs].Columns.Add("evaluation_group_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }
    private void CreateDs_el_u1_training_course_lecturer()
    {
        string sDs = "dsel_u1_training_course_lecturer";
        string sVs = "vsel_u1_training_course_lecturer";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("lecturer_type", typeof(String));
        ds.Tables[sDs].Columns.Add("m0_institution_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        int _idx = 0;
        string[] _calc = new string[4];
        switch (cmdName)
        {
            case "btnSaveInsert":
                
                break;
        }

    }
    #endregion btnCommand
    
    private void showAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }
    
    public string getImgUrl(string AImage = "",string _docno = "")
    {
        string sPathImage = "";
        if ((AImage == "") || (AImage == null))
        {
            sPathImage = _PathFileimage + _func_dmu._IconnoImage;
        }
        else
        {
             sPathImage = _PathFile + "plan_course_qrcode" + "/" + _docno + "/" + AImage;
        }
       // sPathImage = _PathFileimage + _func_dmu._IconnoImage;
        sPathImage = ResolveUrl(sPathImage);

        return sPathImage;
    }
    public string getUrlevaluationform(int id)
    {
        return _func_dmu._LikeGenQrCode_evaluation+ _funcTool.getEncryptRC4(id.ToString(), "id");
    }
    public Boolean getImgVisible(string AImage = "",int _open_flag = 0)
    {
        Boolean _Boolean = true;
        if ((AImage == "") || (AImage == null) || (_open_flag == 0))
        {
            _Boolean = false;
        }

        return _Boolean;
    }
    public string getUrlTest(int id)
    {
        
        return _func_dmu._LikeGenQrCode_test  + _funcTool.getEncryptRC4(id.ToString(), "id") +"/"+ _funcTool.getEncryptRC4("training", "type");
    }

}