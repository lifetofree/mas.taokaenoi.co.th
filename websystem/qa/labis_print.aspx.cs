﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;



public partial class websystem_qa_labis_print : System.Web.UI.Page
{
    function_tool _funcTool = new function_tool();
    data_qa _data_qa = new data_qa();

    string _localJson = "";
    int _tempInt = 0;
    int _tempcounter = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlQaGetSampleView = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSampleView"];
    static string _urlQaGetTestSample = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetTestSample"];
    static string _urlQaGetSetTestDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSetTestDetail"];

    protected void Page_Load(object sender, EventArgs e)
    {
        string url = Request.QueryString["url"];
        if (Session["_SESSION_U0DOCIDX"].ToString() != null)
        {
            ViewState["_PRINTSAMPLECODE"] = Session["_SESSION_U0DOCIDX"].ToString();
            ViewState["_PRINTSHOWQRCODE"] = Session["_SESSION_PrintShowQrCode"].ToString();
            ViewState["_PRINTSHOWTESTITEMS"] = Session["_SESSION_PrintShowTestItems"].ToString();
            ViewState["_PRINTU1DOC"] = Session["_SESSION_U1DOCIDX"].ToString();
            ViewState["_PRINTPLACE"] = Session["_SESSION_Place"].ToString();
            

        }
        else if (Session["_SESSION_U0DOCIDX"].ToString() == null)
        {
            Response.AddHeader("REFRESH", "3;URL=" + ResolveUrl("~/login?url=" + url));
        }

        PRINT_SAMPLECODE();
        _GETPRINTDATE();

        

        //BoundField fieldQrCode = new BoundField();
        //fieldQrCode.HeaderText = "QR Code";
        //DataControlField colQrCode;
        //colQrCode = fieldQrCode;
        //gvSample.Columns.Add(colQrCode);
        //gvSample.DataBind();


    }

    private void PRINT_SAMPLECODE()
    {
        data_qa _data_sample_print = new data_qa();
        _data_sample_print.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
        qa_lab_u1_qalab _u1_doc_sampleeail = new qa_lab_u1_qalab();

        _u1_doc_sampleeail.u0_qalab_idx = int.Parse(ViewState["_PRINTSAMPLECODE"].ToString());
        _data_sample_print.qa_lab_u1_qalab_list[0] = _u1_doc_sampleeail;

        _data_sample_print = callServicePostQA(_urlQaGetSampleView, _data_sample_print);

        setGridData(gvSample, _data_sample_print.qa_lab_u1_qalab_list);

        data_qa dataqa = new data_qa();
        dataqa.bind_qa_m0_test_detail_list = new bindqa_m0_test_detail[1];
        bindqa_m0_test_detail _doc_sampledeail = new bindqa_m0_test_detail();

        _doc_sampledeail.condition = 1;
        _doc_sampledeail.u1_qalab_idx = int.Parse(ViewState["_PRINTU1DOC"].ToString());

        dataqa.bind_qa_m0_test_detail_list[0] = _doc_sampledeail;

        dataqa = callServicePostQA(_urlQaGetTestSample, dataqa);

        for (int a = 0; a < dataqa.bind_qa_m0_test_detail_list.Count(); a++)
        {

            BoundField field = new BoundField();
            field.HeaderText = dataqa.bind_qa_m0_test_detail_list[a].test_detail_name.ToString();
            field.HeaderStyle.Width = 10;
            DataControlField col;
            col = field;
            gvSample.Columns.Add(col);
            gvSample.DataBind();


        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "gvSample":
                if (e.Row.RowType == DataControlRowType.Header)
                {


                }

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    _tempcounter = _tempcounter + 1;


                    if (ViewState["_PRINTSHOWQRCODE"].ToString() == "1")
                    {
                        Image ImagShowQRCode = (Image)e.Row.Cells[0].FindControl("ImagShowQRCode");

                        Label lblSampleCode = (Label)e.Row.Cells[2].FindControl("lblSampleCode");

                        string getPathShowimages = ConfigurationManager.AppSettings["path_qr_code_qa"];
                        string fileimageShowPath = Server.MapPath(getPathShowimages + lblSampleCode.Text + ".jpg");
                        //// Show รูป
                        if (!File.Exists(Server.MapPath(getPathShowimages + ViewState["_PRINTPLACE"].ToString() + lblSampleCode.Text + ".jpg")))
                        {
                            ImagShowQRCode.Visible = false;
                        }
                        else
                        {
                            ImagShowQRCode.ImageUrl = getPathShowimages + ViewState["_PRINTPLACE"].ToString() + lblSampleCode.Text + ".jpg";
                        }
                    }
                    else
                    {
                        gvSample.Columns[0].Visible = false;

                    }

                    // -- barcode -- //
                    if (ViewState["_PRINTSHOWQRCODE"].ToString() == "1")
                    {
                        Image ImagShowBarCode = (Image)e.Row.Cells[1].FindControl("ImagShowBarCode");

                        Label lblSampleCode_ = (Label)e.Row.Cells[2].FindControl("lblSampleCode");

                        string getPathShowimages_barcode = ConfigurationManager.AppSettings["path_bar_code_qa"];
                        string fileimageShowPath_barcode = Server.MapPath(getPathShowimages_barcode + lblSampleCode_.Text + ".jpg");
                        //// Show รูป
                        if (!File.Exists(Server.MapPath(getPathShowimages_barcode + ViewState["_PRINTPLACE"].ToString() + lblSampleCode_.Text + ".jpg")))
                        {
                            //litDebug.Text = "1";
                            ImagShowBarCode.Visible = false;
                        }
                        else
                        {
                            //litDebug.Text = "2";
                            ImagShowBarCode.ImageUrl = getPathShowimages_barcode + ViewState["_PRINTPLACE"].ToString() + lblSampleCode_.Text + ".jpg";
                        }
                    }
                    else
                    {
                        gvSample.Columns[0].Visible = false;

                    }
                    // -- barcode -- //



                    Label lblDocIDX_Sample = (Label)e.Row.Cells[1].FindControl("lblDocIDX_Sample");
                    Repeater rp_test_sample = (Repeater)e.Row.Cells[2].FindControl("rp_test_sample");

                    data_qa _data_test_sample_ = new data_qa();
                    _data_test_sample_.bind_qa_m0_test_detail_list = new bindqa_m0_test_detail[1];
                    bindqa_m0_test_detail _doc_sampledeail = new bindqa_m0_test_detail();

                    _doc_sampledeail.u1_qalab_idx = int.Parse(lblDocIDX_Sample.Text);
                    _data_test_sample_.bind_qa_m0_test_detail_list[0] = _doc_sampledeail;
                    _data_test_sample_ = callServicePostQA(_urlQaGetTestSample, _data_test_sample_);

                    setRepeaterData(rp_test_sample, _data_test_sample_.bind_qa_m0_test_detail_list);

                    ViewState["CountTestedItems"] = _data_test_sample_.bind_qa_m0_test_detail_list.Count();


                    if (ViewState["_PRINTSHOWTESTITEMS"].ToString() == "1")
                    {
                        gvSample.Columns[3].Visible = true;
                    }
                    else
                    {
                        gvSample.Columns[3].Visible = false;
                    }







                    //data_qa dataSetTested = new data_qa();
                    //dataSetTested.qa_r0_set_test_detail_list = new qa_r0_set_test_detail[1];
                    //qa_r0_set_test_detail _selectSet = new qa_r0_set_test_detail();

                    //_selectSet.condition = 1;

                    //dataSetTested.qa_r0_set_test_detail_list[0] = _selectSet;

                    //dataSetTested = callServicePostMasterQA(_urlQaGetSetTestDetail, dataSetTested);


                    //int numtestlist = 0;

                    //foreach (var fideSet in dataSetTested.qa_r0_set_test_detail_list)
                    //{
                    //    Label1.Text += fideSet.set_test_detail_idx.ToString() + ",";

                    //}

                    // QaGetSetTestDetail

                    //string tagTableOpen = "<table class='table table-striped f-s-12' style='background-color: red;'><tr>";
                    //string tagTableColume = "";
                    //for (int m = 0; m < _data_test_sample_.bind_qa_m0_test_detail_list.Count(); m++)
                    //{

                    //   tagTableColume += "<th>" + m +"</th>";

                    //}
                    //string tagTableEnd = "</tr></table>";


                    //Label _columnResult = new Label();
                    //_columnResult.Text += tagTableOpen + tagTableColume + tagTableEnd;
                    //e.Row.Cells[3].Controls.Add(_columnResult);

                    /// string tagColumn = "<td>" 

                }

                break;
        }

    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }


    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void _GETPRINTDATE()
    {
        //lblPrintDate.Text = DateTime.Now.ToString();

        lblPrintDate.Text = DateTime.Now.AddMinutes(55).ToString("dd'/'MM'/'yyyy HH:mm:ss");
    }

    protected data_qa callServicePostQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }


    protected data_qa callServicePostMasterQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }


}