using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_pms_management : System.Web.UI.Page {
    #region initial function/data
    function_tool _funcTool = new function_tool ();

    data_cen_master _data_cen_master = new data_cen_master ();
    data_cen_employee _data_cen_employee = new data_cen_employee ();
    data_pms _data_pms = new data_pms ();

    int _emp_idx = 0;
    int _default_int = 0;
    int _serviceType = 1;
    bool _b_permission = false;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlCenGetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlCenGetCenMasterList"];
    static string _urlGetCenEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCenEmployeeList"];
    static string _urlPmsGetPmsManageData = _serviceUrl + ConfigurationManager.AppSettings["urlPmsGetPmsManageData"];
    static string _urlPmsSetPmsManageData = _serviceUrl + ConfigurationManager.AppSettings["urlPmsSetPmsManageData"];
    static string _urlInsertSystem = _serviceUrl + ConfigurationManager.AppSettings["urlPMSInsertSystem_FormResult"];

    static int[] _permission = { 172, 24047, 1413, 36583, 33301 };

    #endregion initail function/data

    protected void Page_Init (object sender, EventArgs e) {
        _emp_idx = _funcTool.convertToInt (Session["emp_idx"].ToString ());

        // check permission
        // foreach (int _pass in _permission) {
        //     if (_emp_idx == _pass) {
        //         _b_permission = true;
        //         continue;
        //     }
        // }

        // if (!_b_permission) {
        //     Response.Redirect (ResolveUrl ("~/"));
        // }
    }

    protected void Page_Load (object sender, EventArgs e) {
        if (!IsPostBack) {
            initPage ();
        }

        setTrigger ();
    }

    #region event command
    protected void navCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "navList":
                clearViewState ();
                setActiveTab ("viewList", 0);
                break;
                //     case "navImport":
                //         clearViewState ();
                //         setActiveTab ("viewImport", 0);
                //         break;
                //     case "navReport":
                //         clearViewState ();
                //         setActiveTab ("viewReport", 0);
                //         break;
        }
    }

    protected void btnCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "cmdSearch":
                TextBox tbEmpCode = (TextBox) fvSearch.FindControl ("tbEmpCode");
                TextBox tbEmpName = (TextBox) fvSearch.FindControl ("tbEmpName");
                DropDownList ddlOrganization = (DropDownList) fvSearch.FindControl ("ddlOrganization");
                DropDownList ddlWorkGroup = (DropDownList) fvSearch.FindControl ("ddlWorkGroup");
                DropDownList ddlLineWork = (DropDownList) fvSearch.FindControl ("ddlLineWork");
                DropDownList ddlDepartment = (DropDownList) fvSearch.FindControl ("ddlDepartment");
                DropDownList ddlSection = (DropDownList) fvSearch.FindControl ("ddlSection");
                DropDownList ddlPosition = (DropDownList) fvSearch.FindControl ("ddlPosition");

                search_cen_employee_detail _empSearch = new search_cen_employee_detail ();
                _empSearch.s_emp_code = tbEmpCode.Text.Trim ();
                _empSearch.s_emp_name = tbEmpName.Text.Trim ();
                _empSearch.s_org_idx = ddlOrganization.SelectedValue;
                _empSearch.s_wg_idx = ddlWorkGroup.SelectedValue;
                _empSearch.s_lw_idx = ddlLineWork.SelectedValue;
                _empSearch.s_dept_idx = ddlDepartment.SelectedValue;
                _empSearch.s_sec_idx = ddlSection.SelectedValue;
                _empSearch.s_pos_idx = ddlPosition.SelectedValue;
                _data_cen_employee = getEmployeeList ("99", _empSearch);
                ViewState["EmpSearchList"] = _data_cen_employee;
                _funcTool.setGvData (gvList, ((data_cen_employee) ViewState["EmpSearchList"]).cen_employee_list_u0);
                break;
            case "cmdReset":
                _funcTool.setFvData (fvSearch, FormViewMode.Insert, null);
                setActiveTab ("viewList", 0);
                break;
            case "cmdEditData":
                setActiveTab ("viewDetail", _funcTool.convertToInt (cmdArg));
                break;
            case "cmdSaveData":
                DropDownList ddlEmpDotted = (DropDownList) gvDetail.FooterRow.FindControl ("ddlEmpDotted");
                TextBox tbCalRatioDotted = (TextBox) gvDetail.FooterRow.FindControl ("tbCalRatioDotted");

                int _row = 0;
                int _cRow = (ddlEmpDotted.SelectedValue != "" && _funcTool.convertToDecimal (tbCalRatioDotted.Text.Trim ()) != 0) ? gvDetail.Rows.Count + 1 : gvDetail.Rows.Count;

                _data_pms.pms_approve_list_m0 = new pms_approve_detail_m0[_cRow];
                foreach (GridViewRow gvr in gvDetail.Rows) {
                    HiddenField hfM0Idx = (HiddenField) gvr.FindControl ("hfM0Idx");
                    HiddenField hfEmpIdx = (HiddenField) gvr.FindControl ("hfEmpIdx");
                    HiddenField hfEmpNameTh = (HiddenField) gvr.FindControl ("hfEmpNameTh");
                    HiddenField hfEvalEmpType = (HiddenField) gvr.FindControl ("hfEvalEmpType");
                    DropDownList ddlEmpApproveM0 = (DropDownList) gvr.FindControl ("ddlEmpApproveM0");
                    TextBox tbCalRatio = (TextBox) gvr.FindControl ("tbCalRatio");
                    HiddenField hfApproveStatus = (HiddenField) gvr.FindControl ("hfApproveStatus");
                    CheckBox cbDelete = (CheckBox) gvr.FindControl ("cbDelete");

                    pms_approve_detail_m0 _approveData = new pms_approve_detail_m0 ();
                    _approveData.m0_idx = _funcTool.convertToInt (hfM0Idx.Value);
                    _approveData.emp_idx = _funcTool.convertToInt (hfEmpIdx.Value);
                    _approveData.emp_name_th = hfEmpNameTh.Value;
                    _approveData.eval_emp_type = _funcTool.convertToInt (hfEvalEmpType.Value);
                    _approveData.eval_emp_idx = _funcTool.convertToInt (ddlEmpApproveM0.SelectedValue);
                    _approveData.eval_emp_name_th = ddlEmpApproveM0.SelectedItem.Text;
                    _approveData.cal_ratio = _funcTool.convertToDecimal (tbCalRatio.Text.Trim ());
                    _approveData.flag_doing = 0;
                    _approveData.approve_status = (hfEvalEmpType.Value == "3" && cbDelete.Checked) ? 9 : 1;
                    _data_pms.pms_approve_list_m0[_row] = _approveData;

                    //check delete dotted line then change solid cal_ratio to 100
                    if (hfEvalEmpType.Value == "3" && cbDelete.Checked) {
                        _data_pms.pms_approve_list_m0[0].cal_ratio = 100;
                        _data_pms.pms_approve_list_m0[1].cal_ratio = 0;
                    }

                    _row++;
                }

                //add dotted ((_data_pms.pms_approve_list_m0[0].cal_ratio + _funcTool.convertToDecimal (tbCalRatioDotted.Text.Trim ()) == 100)) &&
                if (ddlEmpDotted.SelectedValue != "") {
                    /*if (_funcTool.convertToDecimal (tbCalRatioDotted.Text.Trim ()) != 0 &&
                        (lblEmpNameTh.Text == ddlEmpDotted.SelectedValue)
                    ) {*/
                        pms_approve_detail_m0 _addDotted = new pms_approve_detail_m0 ();
                        _addDotted.m0_idx = 0;
                        _addDotted.emp_idx = _funcTool.convertToInt (lblEmpIdx.Text);
                        _addDotted.emp_name_th = lblEmpNameTh.Text;
                        _addDotted.eval_emp_type = 3;
                        _addDotted.eval_emp_idx = _funcTool.convertToInt (ddlEmpDotted.SelectedValue);
                        _addDotted.eval_emp_name_th = ddlEmpDotted.SelectedItem.Text;
                        _addDotted.cal_ratio = _funcTool.convertToDecimal (tbCalRatioDotted.Text.Trim ());
                        _addDotted.flag_doing = 0;
                        _addDotted.approve_status = 1;
                        _data_pms.pms_approve_list_m0[_row] = _addDotted;
                    /*} else {
                        ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('กรุณากรอกข้อมูลให้ถูกต้อง !!');", true);
                        break;
                    }*/
                } else if ((_data_pms.pms_approve_list_m0[0].cal_ratio + _data_pms.pms_approve_list_m0[1].cal_ratio) != 100) {
                    ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('กรุณากรอกข้อมูลให้ถูกต้อง !!');", true);
                    break;
                }

                _data_pms.pms_mode = "1";
                // litDebug.Text = _funcTool.convertObjectToJson (_data_pms);

                _data_pms = callServicePostPms (_urlPmsSetPmsManageData, _data_pms);
                // litDebug.Text = _funcTool.convertObjectToJson(_data_pms);

                // if lblU0Idx > 0 call pms form services
                int _check_u0 = _funcTool.convertToInt (lblU0Idx.Text);
                if (_check_u0 > 0) {
                    // call pms form services
                    data_pms _data_reset = new data_pms ();
                    //insert u0 document ot month
                    pmsu0_DocFormDetail _u0_reset = new pmsu0_DocFormDetail ();
                    _data_reset.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];
                    //u0 document update
                    _u0_reset.cemp_idx = _emp_idx; //hr
                    _u0_reset.emp_idx = _funcTool.convertToInt (lblEmpIdx.Text); //emp_create performance
                    _u0_reset.u0_docidx = _check_u0; //join pms_docform_u0
                    _u0_reset.acidx = 7;
                    _u0_reset.noidx = 7;
                    _u0_reset.decision = 12; //int.Parse (cmdArg);
                    _u0_reset.condition = 2;
                    _data_reset.Boxpmsu0_DocFormDetail[0] = _u0_reset;
                    // litDebug.Text = _funcTool.convertObjectToJson (data_update_hr);
                    _data_reset = callServicePostPms (_urlInsertSystem, _data_reset);
                }

                _funcTool.setFvData (fvSearch, FormViewMode.Insert, null);
                setActiveTab ("viewList", 0);
                break;
            case "cmdCancelData":
                setActiveTab ("viewList", 0);
                _funcTool.setGvData (gvList, ((data_cen_employee) ViewState["EmpSearchList"]).cen_employee_list_u0);
                break;
        }
    }

    protected void ddlSelectedIndexChanged (object sender, EventArgs e) {
        DropDownList ddlName = (DropDownList) sender;

        DropDownList ddlOrganization = (DropDownList) fvSearch.FindControl ("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList) fvSearch.FindControl ("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList) fvSearch.FindControl ("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList) fvSearch.FindControl ("ddlDepartment");
        DropDownList ddlSection = (DropDownList) fvSearch.FindControl ("ddlSection");
        DropDownList ddlPosition = (DropDownList) fvSearch.FindControl ("ddlPosition");

        switch (ddlName.ID) {
            case "ddlOrganization":
                if (_funcTool.convertToInt (ddlName.SelectedValue) > 0) {
                    search_cen_master_detail _orgSelected = new search_cen_master_detail ();
                    _orgSelected.s_org_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList ("4", _orgSelected);
                    _funcTool.setDdlData (ddlWorkGroup, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                } else {
                    ddlWorkGroup.Items.Clear ();
                }

                ddlLineWork.Items.Clear ();
                ddlDepartment.Items.Clear ();
                ddlSection.Items.Clear ();
                ddlPosition.Items.Clear ();
                ddlWorkGroup.Items.Insert (0, new ListItem ("--- เลือกกลุ่มงาน ---", ""));
                ddlLineWork.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", ""));
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", ""));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                ddlPosition.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", ""));
                break;
            case "ddlWorkGroup":
                if (_funcTool.convertToInt (ddlName.SelectedValue) > 0) {
                    search_cen_master_detail _wgSelected = new search_cen_master_detail ();
                    _wgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _wgSelected.s_wg_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList ("5", _wgSelected);
                    _funcTool.setDdlData (ddlLineWork, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                } else {
                    ddlLineWork.Items.Clear ();
                }

                ddlDepartment.Items.Clear ();
                ddlSection.Items.Clear ();
                ddlPosition.Items.Clear ();
                ddlLineWork.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", ""));
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", ""));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                ddlPosition.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", ""));
                break;
            case "ddlLineWork":
                if (_funcTool.convertToInt (ddlName.SelectedValue) > 0) {
                    search_cen_master_detail _lgSelected = new search_cen_master_detail ();
                    _lgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _lgSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _lgSelected.s_lw_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList ("6", _lgSelected);
                    _funcTool.setDdlData (ddlDepartment, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                } else {
                    ddlDepartment.Items.Clear ();
                }

                ddlSection.Items.Clear ();
                ddlPosition.Items.Clear ();
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", ""));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                ddlPosition.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", ""));
                break;
            case "ddlDepartment":
                if (_funcTool.convertToInt (ddlName.SelectedValue) > 0) {
                    search_cen_master_detail _deptSelected = new search_cen_master_detail ();
                    _deptSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _deptSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _deptSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _deptSelected.s_dept_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList ("7", _deptSelected);
                    _funcTool.setDdlData (ddlSection, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                } else {
                    ddlSection.Items.Clear ();
                }

                ddlPosition.Items.Clear ();
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                ddlPosition.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", ""));
                break;
            case "ddlSection":
                if (_funcTool.convertToInt (ddlName.SelectedValue) > 0) {
                    search_cen_master_detail _secSelected = new search_cen_master_detail ();
                    _secSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _secSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _secSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _secSelected.s_dept_idx = ddlDepartment.SelectedValue;
                    _secSelected.s_sec_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList ("8", _secSelected);
                    _funcTool.setDdlData (ddlPosition, _data_cen_master.cen_pos_list_m0, "pos_name_th", "pos_idx");
                } else {
                    ddlPosition.Items.Clear ();
                }

                ddlPosition.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", ""));
                break;
        }
    }
    #endregion event command

    #region RowDatabound
    protected void gvRowDataBound (object sender, GridViewRowEventArgs e) {
        var gvName = (GridView) sender;

        switch (gvName.ID) {
            case "gvDetail":
                HiddenField hfEvalEmpType = (HiddenField) e.Row.FindControl ("hfEvalEmpType");
                HiddenField hfEvalEmpIdx = (HiddenField) e.Row.FindControl ("hfEvalEmpIdx");
                DropDownList ddlEmpApproveM0 = (DropDownList) e.Row.FindControl ("ddlEmpApproveM0");
                TextBox tbCalRatio = (TextBox) e.Row.FindControl ("tbCalRatio");
                CheckBox cbDelete = (CheckBox) e.Row.FindControl ("cbDelete");
                DropDownList ddlEmpDotted = (DropDownList) e.Row.FindControl ("ddlEmpDotted");

                _data_cen_employee = (data_cen_employee) ViewState["EmpAllList"];

                if (e.Row.RowType == DataControlRowType.DataRow) {
                    _funcTool.setDdlData (ddlEmpApproveM0, _data_cen_employee.cen_employee_list_u0, "emp_name_th", "emp_idx");
                    ddlEmpApproveM0.SelectedValue = hfEvalEmpIdx.Value.ToString ();

                    // check tbCalRatio
                    switch (_funcTool.convertToInt (hfEvalEmpType.Value)) {
                        case 3:
                            cbDelete.Visible = true;
                            tbCalRatio.Visible = true;
                            break;
                        case 4:
                        case 5:
                        case 6:
                            cbDelete.Visible = false;
                            tbCalRatio.Visible = false;
                            break;
                        default:
                            cbDelete.Visible = false;
                            tbCalRatio.Visible = true;
                            break;
                    }
                }

                if (e.Row.RowType == DataControlRowType.Footer) {
                    _funcTool.setDdlData (ddlEmpDotted, _data_cen_employee.cen_employee_list_u0, "emp_name_th", "emp_idx");
                    ddlEmpDotted.Items.Insert (0, new ListItem ("--- เลือก Dotted Line ---", ""));
                }

                if (gvDetail.Rows.Count == 4) {
                    gvDetail.ShowFooter = true;
                } else {
                    gvDetail.ShowFooter = false;
                }
                break;
        }
    }
    #endregion RowDatabound

    #region paging
    protected void gvPageIndexChanging (object sender, GridViewPageEventArgs e) {
        GridView gvName = (GridView) sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID) {
            case "gvList":
                if (ViewState["EmpSearchList"] == null) {
                    search_cen_employee_detail _allEmployee = new search_cen_employee_detail ();
                    _data_cen_employee = getEmployeeList ("99", _allEmployee);
                    ViewState["EmpSearchList"] = _data_cen_employee;
                }

                _funcTool.setGvData (gvList, ((data_cen_employee) ViewState["EmpSearchList"]).cen_employee_list_u0);
                break;
        }
        hlSetTotop.Focus ();
    }
    #endregion paging

    #region reuse
    protected void initPage () {
        clearSession ();
        clearViewState ();

        setActiveTab ("viewList", 0);

        hlSetTotop.Focus ();
    }

    protected void clearSession () {
        // Session["auth"] = null;
    }

    protected void clearViewState () {
        ViewState["EmpSearchList"] = null;
        ViewState["EmpAllList"] = null;
        ViewState["EmpDetail"] = null;
    }

    protected void setActiveTab (string activeTab, int doc_idx) {
        setActiveView (activeTab, doc_idx);
        setActiveTabBar (activeTab);

        switch (activeTab) {
            case "viewList":
                // get organization
                DropDownList ddlOrganization = (DropDownList) fvSearch.FindControl ("ddlOrganization");
                DropDownList ddlWorkGroup = (DropDownList) fvSearch.FindControl ("ddlWorkGroup");
                DropDownList ddlLineWork = (DropDownList) fvSearch.FindControl ("ddlLineWork");
                DropDownList ddlDepartment = (DropDownList) fvSearch.FindControl ("ddlDepartment");
                DropDownList ddlSection = (DropDownList) fvSearch.FindControl ("ddlSection");
                DropDownList ddlPosition = (DropDownList) fvSearch.FindControl ("ddlPosition");

                _data_cen_master = getMasterList ("3", null);
                ddlOrganization.Items.Clear ();
                ddlWorkGroup.Items.Clear ();
                ddlLineWork.Items.Clear ();
                ddlDepartment.Items.Clear ();
                ddlSection.Items.Clear ();
                ddlPosition.Items.Clear ();
                _funcTool.setDdlData (ddlOrganization, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                ddlOrganization.Items.Insert (0, new ListItem ("--- เลือกองค์กร ---", ""));
                ddlWorkGroup.Items.Insert (0, new ListItem ("--- เลือกกลุ่มงาน ---", ""));
                ddlLineWork.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", ""));
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", ""));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                ddlPosition.Items.Insert (0, new ListItem ("--- เลือกตำแหน่ง ---", ""));

                // get employee
                search_cen_employee_detail _allEmployee = new search_cen_employee_detail ();
                _data_cen_employee = getEmployeeList ("99", _allEmployee);
                ViewState["EmpSearchList"] = _data_cen_employee;
                ViewState["EmpAllList"] = _data_cen_employee;
                _funcTool.setGvData (gvList, ((data_cen_employee) ViewState["EmpSearchList"]).cen_employee_list_u0);
                break;
            case "viewDetail":
                pms_approve_detail_m0 _empDetail = new pms_approve_detail_m0 ();
                _empDetail.emp_idx = doc_idx;
                _data_pms.pms_approve_list_m0 = new pms_approve_detail_m0[1];
                _data_pms.pms_approve_list_m0[0] = _empDetail;
                _data_pms.pms_mode = "1";
                _data_pms = callServicePostPms (_urlPmsGetPmsManageData, _data_pms);
                ViewState["EmpDetail"] = _data_pms;
                // int _temp_type = _data_pms.pms_approve_list_m0[1].eval_emp_type;
                _funcTool.setGvData (gvDetail, ((data_pms) ViewState["EmpDetail"]).pms_approve_list_m0);
                lblEmpDetail.Text = "รหัสพนักงาน : " + _data_pms.pms_approve_list_m0[0].emp_code + "<br>ชื่อ-นามสกุล : " + _data_pms.pms_approve_list_m0[0].emp_name_th; // + " | ";
                lblU0Idx.Text = _data_pms.pms_approve_list_m0[0].u0_docidx.ToString ();
                lblEmpIdx.Text = _data_pms.pms_approve_list_m0[0].emp_idx.ToString ();
                lblEmpNameTh.Text = _data_pms.pms_approve_list_m0[0].emp_name_th;
                break;
        }

        hlSetTotop.Focus ();
    }

    protected void setActiveView (string activeTab, int doc_idx) {
        mvSystem.SetActiveView ((View) mvSystem.FindControl (activeTab));
    }

    protected void setActiveTabBar (string activeTab) {
        switch (activeTab) {
            case "viewList":
            case "viewDetail":
                li0.Attributes.Add ("class", "active");
                // li1.Attributes.Add ("class", "");
                // li2.Attributes.Add ("class", "");
                break;
        }
    }

    protected void linkBtnTrigger (LinkButton linkBtnID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger ();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add (triggerLinkBtn);
    }

    protected void setTrigger () {
        // linkBtnTrigger (lbAuth);
        // linkBtnTrigger (lbRelease);

        // // nav trigger
        // linkBtnTrigger (lbList);
        // linkBtnTrigger (lbImport);
        // linkBtnTrigger (lbReport);

        // LinkButton lbImportData = (LinkButton) fvUploadFiles.FindControl ("lbImportData");
        // try { linkBtnTrigger (lbImportData); } catch { }
        // try { linkBtnTrigger (lbSaveData); } catch { }
        // try { linkBtnTrigger (lbExportData); } catch { }

        // // slip list
        // try { linkGvTrigger (gvList); } catch { }
    }

    protected data_cen_master getMasterList (string _masterMode, search_cen_master_detail _searchData) {
        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.master_mode = _masterMode;
        _data_cen_master.search_cen_master_list[0] = _searchData;
        // litDebug.Text = _funcTool.convertObjectToJson(_data_cen_master);

        _data_cen_master = callServicePostCenMaster (_urlCenGetCenMasterList, _data_cen_master);
        return _data_cen_master;
    }

    protected data_cen_employee getEmployeeList (string _employeeMode, search_cen_employee_detail _searchData) {
        _data_cen_employee.search_cen_employee_list = new search_cen_employee_detail[1];
        _data_cen_employee.employee_mode = _employeeMode;
        _data_cen_employee.search_cen_employee_list[0] = _searchData;
        // litDebug.Text = _funcTool.convertObjectToJson(_data_cen_master);

        _data_cen_employee = callServicePostCenEmployee (_urlGetCenEmployeeList, _data_cen_employee);
        return _data_cen_employee;
    }

    protected data_cen_master callServicePostCenMaster (string _cmdUrl, data_cen_master _data_cen_master) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_cen_master);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);

        // convert json to object
        _data_cen_master = (data_cen_master) _funcTool.convertJsonToObject (typeof (data_cen_master), _local_json);

        return _data_cen_master;
    }

    protected data_cen_employee callServicePostCenEmployee (string _cmdUrl, data_cen_employee _data_cen_employee) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_cen_employee);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);

        // convert json to object
        _data_cen_employee = (data_cen_employee) _funcTool.convertJsonToObject (typeof (data_cen_employee), _local_json);

        return _data_cen_employee;
    }

    protected data_pms callServicePostPms (string _cmdUrl, data_pms _data_pms) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_pms);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);
        // litDebug.Text = _local_json;

        // convert json to object
        _data_pms = (data_pms) _funcTool.convertJsonToObject (typeof (data_pms), _local_json);

        return _data_pms;
    }
    #endregion reuse
}