﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="networkdevicesdetail.aspx.cs" Inherits="websystem_ITServices_networkdevicesdetail" %>

<%@ MasterType VirtualPath="~/masterpage/masterpage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
   
    <%--<span style="color: red; font-size: 16px; font-weight: bold; margin-left: 20px;">
        <asp:Literal ID="litShowDetail" runat="server"></asp:Literal></span>--%>
   

    <div id="showdetailsystem" class="form-group" runat="server" visible="false">
    <div class="col-sm-12">
        <h4>Network Devices(QRCode) </h4>
    </div>
    </div>

     <div id="showdetailpermissionsystem" class="form-group" runat="server" visible="false">
    <div class="col-sm-12">
        <asp:Label ID="detailshoe" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="16px" Text="คุณไม่มีสิทธิ์เข้าดูรายละเอียด Network Devices"></asp:Label>
        <%--<h4>คุณไม่มีสิทธิ์เข้าดูรายละเอียด Network Devices </h4>--%>
    </div>
    </div>

    <asp:FormView ID="FvDetailShowQRCode" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
        <ItemTemplate>

            <div class="col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading f-bold">รายละเอียดอุปกรณ์ Network </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <%--<asp:TextBox ID="lbu0idx_networkqrcode" runat="server" Text='<%# Eval("u0_devicenetwork_idx") %>' Visible="false" />--%>

                            <%-------------- เลขทะเบียนอุปกรณ์, วันที่หมดประกัน --------------%>
                            <div class="form-group">
                                <asp:Label ID="lbregister_numberdetailqrcode" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียนอุปกรณ์ : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtregister_numberdetailqrcode" Text='<%# Eval("register_number") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                                

                            </div>
                            <%-------------- เลขทะเบียนอุปกรณ์, วันที่หมดประกัน --------------%>

                            <%-------------- บริษัทที่ต่อประกัน --------------%>
                            <div class="form-group">
                                <asp:Label ID="lbcompany_nameqrcode" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทที่ต่อประกัน : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtcompany_nameqrcode" Text='<%# Eval("company_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                                <asp:Label ID="lbdate_expireqrcode" CssClass="col-sm-2 control-label" runat="server" Text="วันที่หมดประกัน : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtdate_expireqrcode" Text='<%# Eval("date_expire") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                            </div>
                            <%-------------- บริษัทที่ต่อประกัน--------------%>

                            <%-------------- ยี่ห้อ, รุ่น --------------%>
                            <div class="form-group">
                                <asp:Label ID="lbbrand_nameqrcode" CssClass="col-sm-2 control-label" runat="server" Text="ยี่ห้อ : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtbrand_nameqrcode" Text='<%# Eval("brand_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                                <asp:Label ID="lbgeneration_namemove" CssClass="col-sm-2 control-label" runat="server" Text="รุ่น : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtgeneration_nameqrcode" Text='<%# Eval("generation_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                            </div>
                            <%-------------- ยี่ห้อ, รุ่น --------------%>

                            <%-------------- สถานที่ติดตั้ง, อาคาร --------------%>
                            <div class="form-group">
                                <asp:Label ID="lblplace_nameqrcode" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ติดตั้ง : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtplace_nameqrcode" Text='<%# Eval("place_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                                <asp:Label ID="lblroom_nameqrcode" CssClass="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtroom_nameqrcode" Text='<%#getRoomname((string)Eval("room_name")) %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                            </div>
                            <%-------------- สถานที่ติดตั้ง, อาคาร --------------%>

                            <%-------------- IPAddress, Searial Number --------------%>
                            <div class="form-group">
                                <asp:Label ID="lbip_addressqrcode" CssClass="col-sm-2 control-label" runat="server" Text="IPAddress : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtip_addressqrcode" Text='<%# Eval("ip_address") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                                <asp:Label ID="lbserial_numberqrcode" CssClass="col-sm-2 control-label" runat="server" Text="Searial Number : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtserial_numberqrcode" Text='<%#Eval("serial_number") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                            </div>
                            <%-------------- IPAddress, Searial Number --------------%>

                            <%-------------- User Name, Password --------------%>
                            <div class="form-group">
                                <asp:Label ID="lbuser_nameqrcode" CssClass="col-sm-2 control-label" runat="server" Text="User Name : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtuser_nameqrcode" Text='<%# Eval("user_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                                <asp:Label ID="lbpass_wordqrcode" CssClass="col-sm-2 control-label" runat="server" Text="Password : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtpass_wordqrcode" Text='<%#getPass((string)Eval("pass_word")) %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                            </div>
                            <%-------------- User Name, Password --------------%>
                        </div>


                    </div>
                </div>
            </div>

        </ItemTemplate>

    </asp:FormView>

    <asp:Label ID="lblTest" runat="server" />
    <%--<asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />--%>
</asp:Content>
