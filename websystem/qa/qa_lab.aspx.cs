using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class websystem_qa_qa_lab : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    // bool _flag_qmr = false;

    // rdept permission
    // int[] rdept_qmr = { 20 }; //QMR:26
    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        // check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
        }
    }

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0);
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        txt_document_nosearch.Text = string.Empty;
        txt_sample_codesearch.Text = string.Empty;
        txt_mat_search.Text = string.Empty;
        txt_too_search.Text = string.Empty;
        txt_customer_search.Text = string.Empty;
        txt_date_search.Text = string.Empty;
        txt_customer_search.Text = string.Empty;
        txt_date_search.Text = string.Empty;

        switch (cmdName)
        {
            case "cmdDocSave":
                string[] cmdArgSave = cmdArg.Split(',');

                int actor_idx = int.TryParse(cmdArgSave[0].ToString(), out _default_int) ? int.Parse(cmdArgSave[0].ToString()) : _default_int;
                int node_idx = int.TryParse(cmdArgSave[1].ToString(), out _default_int) ? int.Parse(cmdArgSave[1].ToString()) : _default_int;
                int to_node_idx = int.TryParse(cmdArgSave[2].ToString(), out _default_int) ? int.Parse(cmdArgSave[2].ToString()) : _default_int;
                int to_actor_idx = int.TryParse(cmdArgSave[3].ToString(), out _default_int) ? int.Parse(cmdArgSave[3].ToString()) : _default_int;
                int decision_idx = int.TryParse(cmdArgSave[4].ToString(), out _default_int) ? int.Parse(cmdArgSave[4].ToString()) : _default_int;

                // switch action by node
                switch (node_idx)
                {
                    case 1:
                        setActiveTab("tab2", 0, 0);
                        break;
                    case 3:
                        switch (actor_idx)
                        {
                            case 1:
                                setActiveTab("tab1", to_node_idx, 91);
                                break;
                            case 2:
                            case 3:
                                if (to_node_idx == 4)
                                {
                                    setActiveTab("tab1", to_node_idx, decision_idx);
                                }
                                else if (to_node_idx == 3)
                                {
                                    setActiveTab("tab1", to_node_idx, decision_idx);
                                }

                                else if (to_node_idx == 8)
                                {
                                    setActiveTab("tab1", to_node_idx, decision_idx);
                                }
                                else if (to_node_idx == 1)
                                {
                                    setActiveTab("tab2", to_node_idx, decision_idx);
                                }
                                else
                                {
                                    setActiveTab("tab1", to_node_idx, decision_idx);
                                }
                                break;
                            default:
                                setActiveTab("tab1", to_node_idx, decision_idx);
                                break;
                        }
                        break;
                    case 5:
                        switch (actor_idx)
                        {
                            case 3:
                                if (to_node_idx == 6 && decision_idx == 0)
                                {
                                    setActiveTab("tab1", to_node_idx, decision_idx);
                                    //setActiveTab("tab5", to_node_idx, decision_idx);
                                }
                                else if (to_node_idx == 6 && decision_idx == 21)
                                {
                                    setActiveTab("tab5", to_node_idx, decision_idx);
                                }
                                else if (to_node_idx == 6 && decision_idx == 1)
                                {
                                    setActiveTab("tab1", to_node_idx, decision_idx);
                                }

                                break;

                        }

                        break;
                    case 10:
                        if (to_node_idx == 12)
                        {
                            setActiveTab("tab2", to_node_idx, decision_idx);
                        }
                        else
                        {
                            setActiveTab("tab1", to_node_idx, decision_idx);
                        }

                        break;
                    case 4:
                    //case 5:
                    case 6:
                    case 8:
                    case 9:
                    //case 10:
                    case 11:
                        setActiveTab("tab1", to_node_idx, decision_idx);
                        break;
                    case 30:

                        break;
                }
                break;
            case "cmdDocCancel":
                setActiveTab("tab2", 0, 0);
                break;
            case "cmdDocSave_All":
                setActiveTab("tab2", 0, 0);
                break;
            case "cmdDocEvent":
                int _tempAction = int.TryParse(cmdArg, out _default_int) ? int.Parse(cmdArg) : _default_int;
                setActiveTab("tab1", _tempAction, 0);
                break;

            case "btninsert_detail":

                FormView fvDocDetail_insert_ex = (FormView)tab1.FindControl("fvDocDetail");
                Panel show_selecteddetail_ex = (Panel)fvDocDetail_insert_ex.FindControl("show_selecteddetail_ex");
                LinkButton btninsert_detail = (LinkButton)show_selecteddetail_ex.FindControl("btninsert_detail");

                show_selecteddetail_ex.Visible = true;

                btninsert_detail.Focus();

                break;

            case "btninsert_alldetail":

                FormView fvDocDetail_insert_all = (FormView)tab1.FindControl("fvDocDetail");
                Panel show_selecteddetail_all = (Panel)fvDocDetail_insert_all.FindControl("show_selecteddetail_all");
                LinkButton btninsert_alldetail = (LinkButton)show_selecteddetail_all.FindControl("btninsert_alldetail");

                show_selecteddetail_all.Visible = true;

                btninsert_alldetail.Focus();

                break;
            case "btninsert_rdept":

                FormView fvDocDetail_insert_rdept = (FormView)tab1.FindControl("fvDocDetail");
                Panel show_detailselected_rdept = (Panel)fvDocDetail_insert_rdept.FindControl("show_detailselected_rdept");
                LinkButton btninsert_rdept = (LinkButton)fvDocDetail_insert_rdept.FindControl("btninsert_rdept");

                show_detailselected_rdept.Visible = true;
                btninsert_rdept.Focus();

                break;

            case "btninsert_detail_ex":

                FormView fvDocDetail_insert_exall = (FormView)tab1.FindControl("fvDocDetail");
                Panel show_selecteddetail_all_ex = (Panel)fvDocDetail_insert_exall.FindControl("show_selecteddetail_all");
                LinkButton btninsert_detail_ex = (LinkButton)fvDocDetail_insert_exall.FindControl("btninsert_detail_ex");

                show_selecteddetail_all_ex.Visible = true;

                btninsert_detail_ex.Focus();

                break;

            case "btnSearchIndex":

                if (txtsearch_index.Text == "QA600003")
                {
                    show_search_doc.Visible = true;
                    show_data.Visible = false;
                    show_search_sample.Visible = false;
                    txtsearch_index.Text = string.Empty;
                }
                else if (txtsearch_index.Text == "7G07003")
                {
                    show_search_sample.Visible = true;
                    show_data.Visible = false;
                    show_search_doc.Visible = false;
                    txtsearch_index.Text = string.Empty;
                }
                //setActiveTab("tab2", 1, 0);
                break;
            case "btnSearchIndex1":

                if (txt_document_nosearch.Text == "QA600003")
                {
                    show_search_doc.Visible = true;
                    show_data.Visible = false;
                    show_search_sample.Visible = false;
                    txtsearch_index.Text = string.Empty;
                }
                else if (txt_sample_codesearch.Text == "7G07003")
                {
                    show_search_sample.Visible = true;
                    show_data.Visible = false;
                    show_search_doc.Visible = false;
                    txtsearch_index.Text = string.Empty;
                }
                //setActiveTab("tab2", 1, 0);
                break;
            case "btnreset":
                //�Ѻ��� cmdArg ���� ����¹˹�ҿ����
                int _resetAction = int.Parse(cmdArg);
                switch (_resetAction)
                {
                    case 0:
                        setActiveTab("tab2", 0, 0);
                        break;

                    case 1:
                        setActiveTab("tab4", 0, 0);
                        break;
                }

                break;

            case "btnImportExcel":

                show_detail_fileexcel.Visible = true;
                break;

            case "cmdView":
                int _viewAction = int.Parse(cmdArg);
                switch (_viewAction)
                {
                    case 0:
                        setActiveTab("tab3", 0, 0);
                        break;
                    case 1:
                        setActiveTab("tab3", 0, 1);
                        break;
                    case 2:
                        setActiveTab("tab3", 0, 2);
                        break;
                }
                break;
            case "btnSearch":

                fvBacktoIndex.Visible = true;
                showsearch.Visible = true;
                btnSearch.Visible = false;

                break;

            case "btnSearchBack":

                showsearch.Visible = false;
                fvBacktoIndex.Visible = false;
                btnSearch.Visible = true;

                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;
            case "cmdDocCancelresult":

                setActiveTab("tab5", 0, 0);


                break;
            case "btnSearchMat":

                TextBox txt_material_search = (TextBox)fvDocDetail.FindControl("txt_material_search");
                Panel show_another_insert = (Panel)fvDocDetail.FindControl("show_another_insert");
                RadioButton rd_check_another_ex = (RadioButton)show_another_insert.FindControl("rd_check_another_ex");
                Panel show_detailsample_insert = (Panel)fvDocDetail.FindControl("show_detailsample_insert");
                Panel show_noresult = (Panel)fvDocDetail.FindControl("show_noresult");

                Panel show_mat_have = (Panel)show_detailsample_insert.FindControl("show_mat_have");
                Panel show_mat_nohave = (Panel)show_detailsample_insert.FindControl("show_mat_nohave");

                Panel show_selecteddetail_all_mat = (Panel)show_detailsample_insert.FindControl("show_selecteddetail_all");


                if (txt_material_search.Text == "1202884")
                {
                    show_noresult.Visible = false;
                    show_another_insert.Visible = false;
                    show_mat_nohave.Visible = false;
                    show_detailsample_insert.Visible = true;
                    show_mat_have.Visible = true;
                    txt_material_search.Text = string.Empty;
                    rd_check_another_ex.Checked = false;
                    show_selecteddetail_all_mat.Visible = false;

                }
                else
                {
                    show_noresult.Visible = true;
                    show_another_insert.Visible = true;
                    show_mat_have.Visible = false;
                    show_detailsample_insert.Visible = false;
                    show_mat_nohave.Visible = false;
                    txt_material_search.Text = string.Empty;
                    rd_check_another_ex.Checked = false;
                    show_selecteddetail_all_mat.Visible = false;


                }

                break;

            case "cmdDocSaveSup":
                setFormData(fv_supervisor_detail1, FormViewMode.Insert, null);
                setFormData(fv_supervisor_detail, FormViewMode.ReadOnly, null);
                break;






        }
    }
    #endregion event command

    #region Checkbox ત�ʴ�
    protected void CheckboxChanged(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        switch (cb.ID)
        {

            case "chk_selected_rdept":

                // actionSystemall();
                FormView fvDocDetail = (FormView)tab1.FindControl("fvDocDetail");
                CheckBox chk_selected_rdept = (CheckBox)fvDocDetail.FindControl("chk_selected_rdept");
                Panel show_selected_rdept = (Panel)fvDocDetail.FindControl("show_selected_rdept");

                if (chk_selected_rdept.Checked)
                {
                    show_selected_rdept.Visible = true;
                }
                else
                {
                    show_selected_rdept.Visible = false;
                }

                break;

            case "chk_other":

                FormView fvDocDetail_chkother = (FormView)tab1.FindControl("fvDocDetail");
                CheckBox chk_other = (CheckBox)fvDocDetail_chkother.FindControl("chk_other");

                DropDownList ddl_mat = (DropDownList)fvDocDetail_chkother.FindControl("ddl_mat");
                TextBox txt_mat_insert = (TextBox)fvDocDetail_chkother.FindControl("txt_mat_insert");

                if (chk_other.Checked)
                {
                    txt_mat_insert.Visible = true;
                    ddl_mat.Visible = false;
                }
                else
                {
                    txt_mat_insert.Visible = false;
                    ddl_mat.Visible = true;
                }

                break;
        }
    }
    #endregion

    #region RadioButton
    protected void RadioButton_CheckedChanged(object sender, System.EventArgs e)
    {
        var rd = (RadioButton)sender;
        switch (rd.ID)
        {

            case "selected_datail":

                RadioButton selected_datail = (RadioButton)fvDocDetail.FindControl("selected_datail");
                Panel show_detail_before = (Panel)fvDocDetail.FindControl("show_detail_before");
                Panel show_alldetail_before1 = (Panel)fvDocDetail.FindControl("show_alldetail_before");
                Panel show_insert_detail = (Panel)fvDocDetail.FindControl("show_insert_detail");
                LinkButton btninsert_detail = (LinkButton)show_detail_before.FindControl("btninsert_detail");
                Panel show_selecteddetail_ex = (Panel)fvDocDetail.FindControl("show_selecteddetail_ex");
                Panel show_selecteddetail_all = (Panel)fvDocDetail.FindControl("show_selecteddetail_all");


                if (selected_datail.Checked)
                {
                    show_detail_before.Visible = true;
                    show_alldetail_before1.Visible = false;
                    show_selecteddetail_all.Visible = false;
                    //show_insert_detail.Visible = true;
                }
                else
                {
                    show_detail_before.Visible = false;
                    show_alldetail_before1.Visible = false;
                    show_selecteddetail_ex.Visible = false;
                    //show_insert_detail.Visible = false;
                }
                //selected_datail.Focus();
                //btninsert_detail.Focus();

                break;
            case "selected_alldatail":

                RadioButton selected_alldatail = (RadioButton)fvDocDetail.FindControl("selected_alldatail");
                Panel show_detail_before2 = (Panel)fvDocDetail.FindControl("show_detail_before");
                Panel show_alldetail_before = (Panel)fvDocDetail.FindControl("show_alldetail_before");
                Panel show_insert_detail2 = (Panel)fvDocDetail.FindControl("show_insert_detail");
                LinkButton btninsert_alldetail = (LinkButton)show_alldetail_before.FindControl("btninsert_alldetail");

                Panel show_selecteddetail_ex2 = (Panel)fvDocDetail.FindControl("show_selecteddetail_ex");
                Panel show_selecteddetail_all2 = (Panel)fvDocDetail.FindControl("show_selecteddetail_all");
                if (selected_alldatail.Checked)
                {
                    show_alldetail_before.Visible = true;
                    show_detail_before2.Visible = false;
                    show_selecteddetail_ex2.Visible = false;
                    //show_insert_detail2.Visible = true;
                }
                else
                {
                    show_alldetail_before.Visible = false;
                    show_detail_before2.Visible = false;
                    show_selecteddetail_ex2.Visible = false;
                    //show_insert_detail2.Visible = false;
                }
                //btninsert_alldetail.Focus();

                break;

            case "rd_check_another_ex":

                TextBox txt_material_search = (TextBox)fvDocDetail.FindControl("txt_material_search");
                Panel show_another_insert = (Panel)fvDocDetail.FindControl("show_another_insert");
                RadioButton rd_check_another_ex = (RadioButton)show_another_insert.FindControl("rd_check_another_ex");
                Panel show_detailsample_insert = (Panel)fvDocDetail.FindControl("show_detailsample_insert");
                Panel show_noresult = (Panel)fvDocDetail.FindControl("show_noresult");

                Panel show_mat_have = (Panel)show_detailsample_insert.FindControl("show_mat_have");
                Panel show_mat_nohave = (Panel)show_detailsample_insert.FindControl("show_mat_nohave");


                if (rd_check_another_ex.Checked)
                {
                    show_noresult.Visible = false;
                    show_another_insert.Visible = true;
                    show_detailsample_insert.Visible = true;
                    show_mat_have.Visible = false;
                    show_mat_nohave.Visible = true;
                    txt_material_search.Text = string.Empty;

                }
                else
                {
                    show_noresult.Visible = true;
                    show_another_insert.Visible = true;
                    show_mat_have.Visible = false;
                    show_mat_nohave.Visible = false;
                    show_detailsample_insert.Visible = false;
                    txt_material_search.Text = string.Empty;
                    rd_check_another_ex.Checked = false;


                }




                break;



        }
    }

    #endregion RadioButton

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {

        var ddName = (DropDownList)sender;


        switch (ddName.ID)
        {

            case "ddl_approve_received":

                FormView fvDocInsertAnalysis = (FormView)tab1.FindControl("fvDocInsertAnalysis");
                DropDownList ddl_approve_received = (DropDownList)fvDocInsertAnalysis.FindControl("ddl_approve_received");
                LinkButton lbDocSave = (LinkButton)fvDocInsertAnalysis.FindControl("lbDocSave");
                LinkButton lbDocNoSave = (LinkButton)fvDocInsertAnalysis.FindControl("lbDocNoSave");
                Panel Show_comment_noaccept = (Panel)fvDocInsertAnalysis.FindControl("Show_comment_noaccept");

                lbDocSave.Visible = false;
                lbDocNoSave.Visible = false;

                if (ddl_approve_received.SelectedValue == "1")
                {
                    lbDocSave.Visible = true;
                    Show_comment_noaccept.Visible = false;
                }
                else if (ddl_approve_received.SelectedValue == "2")
                {
                    lbDocNoSave.Visible = true;
                    Show_comment_noaccept.Visible = true;
                }
                else
                {
                    lbDocSave.Visible = false;
                    Show_comment_noaccept.Visible = false;
                }


                break;

            case "ddl_approve_invest":
                FormView fvDocInsertApprove = (FormView)tab1.FindControl("fvDocInsertApprove");
                DropDownList ddl_approve_invest = (DropDownList)fvDocInsertApprove.FindControl("ddl_approve_invest");
                LinkButton lbDocSave_invest = (LinkButton)fvDocInsertApprove.FindControl("lbDocSave");
                LinkButton lbDocNoSave_invest = (LinkButton)fvDocInsertApprove.FindControl("lbDocNoSave");


                lbDocSave_invest.Visible = false;
                lbDocNoSave_invest.Visible = false;

                if (ddl_approve_invest.SelectedValue == "1")
                {
                    lbDocSave_invest.Visible = true;
                }
                else if (ddl_approve_invest.SelectedValue == "2")
                {
                    lbDocNoSave_invest.Visible = true;
                }

                break;

            case "ddl_approveuser_out":
                FormView fvDocDetail3_OutUserDecision = (FormView)tab1.FindControl("fvDocDetail3_OutUserDecision");
                DropDownList ddl_approveuser_out = (DropDownList)fvDocDetail3_OutUserDecision.FindControl("ddl_approveuser_out");
                LinkButton lbDocSave_user_out = (LinkButton)fvDocDetail3_OutUserDecision.FindControl("lbDocSave");
                LinkButton lbDocNoSave_user_out = (LinkButton)fvDocDetail3_OutUserDecision.FindControl("lbDocNoSave");


                lbDocSave_user_out.Visible = false;
                lbDocNoSave_user_out.Visible = false;

                if (ddl_approveuser_out.SelectedValue == "1")
                {
                    lbDocSave_user_out.Visible = true;
                }
                else if (ddl_approveuser_out.SelectedValue == "2")
                {
                    lbDocNoSave_user_out.Visible = true;
                }

                break;

                //case "ddlrecieveWork":
                //    FormView fvDocDetail3 = (FormView)tab1.FindControl("fvDocDetail3");
                //    DropDownList ddlrecieveWork = (DropDownList)fvDocDetail3.FindControl("ddlrecieveWork");
                //    Panel formSelectLab = (Panel)fvDocDetail3.FindControl("formSelectLab");
                //    LinkButton btnDontSave = (LinkButton)fvDocDetail3.FindControl("btnDontSave");
                //    LinkButton DocSave = (LinkButton)fvDocDetail3.FindControl("lbDocSave");

                //    if (ddlrecieveWork.SelectedValue == "1")
                //    {
                //        formSelectLab.Visible = true;
                //        btnDontSave.Visible = false;
                //    }

                //    else if(ddlrecieveWork.SelectedValue == "2")
                //    {
                //        formSelectLab.Visible = false;
                //        btnDontSave.Visible = true;
                //    }

                //    break;



        }
    }

    #endregion

    #region TextBoxIndexChanged
    protected void TextBoxChanged(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;
        //var searchingToolID = (TextBox)fvInsertLabCal.FindControl("searchingToolID");
        //var paneldetailTool = (Panel)fvInsertLabCal.FindControl("paneldetailTool");
        //var btnInsertDetail = (LinkButton)fvInsertLabCal.FindControl("btnInsertDetail");
        switch (txtName.ID)
        {
            case "txt_samplecode_result":

                TextBox txt_samplecode_result = (TextBox)Fv_Insert_Result.FindControl("txt_samplecode_result");
                Panel result_insert = (Panel)Fv_Insert_Result.FindControl("result_insert");

                if (txt_samplecode_result.Text == "7G07001")
                {

                    result_insert.Visible = true;

                }
                else
                {
                    result_insert.Visible = false;


                }

                break;

            case "txt_material_search":

                TextBox txt_material_search = (TextBox)fvDocDetail.FindControl("txt_material_search");
                Panel show_another_insert = (Panel)fvDocDetail.FindControl("show_another_insert");
                RadioButton rd_check_another_ex = (RadioButton)show_another_insert.FindControl("rd_check_another_ex");
                Panel show_detailsample_insert = (Panel)fvDocDetail.FindControl("show_detailsample_insert");
                Panel show_noresult = (Panel)fvDocDetail.FindControl("show_noresult");

                Panel show_mat_have = (Panel)show_detailsample_insert.FindControl("show_mat_have");
                Panel show_mat_nohave = (Panel)show_detailsample_insert.FindControl("show_mat_nohave");

                Panel show_selecteddetail_all = (Panel)show_detailsample_insert.FindControl("show_selecteddetail_all");


                if (txt_material_search.Text == "1202884")
                {
                    show_noresult.Visible = false;
                    show_another_insert.Visible = false;
                    show_mat_nohave.Visible = false;
                    show_detailsample_insert.Visible = true;
                    show_mat_have.Visible = true;
                    txt_material_search.Text = string.Empty;
                    rd_check_another_ex.Checked = false;
                    show_selecteddetail_all.Visible = false;

                }
                else
                {
                    show_noresult.Visible = true;
                    show_another_insert.Visible = true;
                    show_mat_have.Visible = false;
                    show_detailsample_insert.Visible = false;
                    show_mat_nohave.Visible = false;
                    txt_material_search.Text = string.Empty;
                    rd_check_another_ex.Checked = false;
                    show_selecteddetail_all.Visible = false;


                }

                break;

        }

    }
    #endregion

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveTab("tab2", 0, 0);
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int doc_decision)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        // clear other formview, repeater
        setFormData(fvDocDetail, FormViewMode.ReadOnly, null);
        setFormData(fvDocDetail2, FormViewMode.ReadOnly, null);
        setFormData(fvDocDetail3, FormViewMode.ReadOnly, null);
        setFormData(fvDocInsertAnalysis, FormViewMode.ReadOnly, null);
        setFormData(fvDocInsertAnalysisResult, FormViewMode.ReadOnly, null);
        setFormData(fvDocInsertApprove, FormViewMode.ReadOnly, null);
        setFormData(fvDocDetail12, FormViewMode.ReadOnly, null);
        setFormData(fvDocDetail10, FormViewMode.ReadOnly, null);
        setFormData(fvDocDetail12_report, FormViewMode.ReadOnly, null);
        setFormData(fvDocDetail3_Out, FormViewMode.ReadOnly, null);
        setFormData(fvDocDetail3_OutUserDecision, FormViewMode.ReadOnly, null);
        setFormData(fvDocDetail3_OutAdmin, FormViewMode.ReadOnly, null);
        setFormData(fvDocInsertAnalysisResultOut, FormViewMode.ReadOnly, null);
        setFormData(fvInsert_Result_LabIn, FormViewMode.ReadOnly, null);
        setFormData(fvInsert_Result_LabOut, FormViewMode.ReadOnly, null);
        sampleList.Visible = false;
        sampleRecieve.Visible = false;
        sampleWaitingSupervisor.Visible = false;
        sampleApprove.Visible = false;
        samplePreview.Visible = false;
        sampleRecieveDecisionUser.Visible = false;
        sampleRecieveforadmin.Visible = false;
        sampleBeforeRecieve.Visible = false;
        logList.Visible = false;
        logListPreview.Visible = false;
        samplePreviewReport.Visible = false;
        sampleRecieveforLabExternal.Visible = false;


        setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);


        // tab 2 //
        show_data.Visible = false;
        show_sucess.Visible = false;
        show_search_doc.Visible = false;
        show_search_sample.Visible = false;
        txt_document_nosearch.Text = string.Empty;
        txt_sample_codesearch.Text = string.Empty;
        txtsearch_index.Text = string.Empty;
        showsearch.Visible = false;
        fvBacktoIndex.Visible = false;
        btnSearch.Visible = true;

        // tab3 //
        show_detail_fileexcel.Visible = false;

        // tab6
        setFormData(fv_supervisor_detail, FormViewMode.ReadOnly, null);
        setFormData(fv_supervisor_detail1, FormViewMode.ReadOnly, null);
        //save_allprocess.Visible = false;  
        // clear other formview, repeater
        switch (activeTab)
        {
            case "tab1":
                switch (uidx)
                {
                    case 0:
                        setFormData(fvDocDetail, FormViewMode.Insert, null);
                        SETFOCUS.Focus();
                        break;
                    case 2:
                        setFormData(fvDocDetail2, FormViewMode.Insert, null);
                        sampleList.Visible = true;
                        logList.Visible = true;
                        SETFOCUS.Focus();
                        break;
                    case 3:
                        switch (doc_decision)
                        {
                            case 0:
                                setFormData(fvDocDetail2, FormViewMode.Insert, null);
                                setFormData(fvDocDetail3, FormViewMode.Insert, null);
                                SETFOCUS.Focus();
                                break;
                            case 2:
                                setFormData(fvDocDetail2, FormViewMode.Insert, null);
                                sampleList.Visible = true;
                                logList.Visible = true;
                                SETFOCUS.Focus();
                                break;
                            case 8:
                                setFormData(fvDocDetail3_Out, FormViewMode.Insert, null);
                                SETFOCUS.Focus();
                                break;
                        }
                        logList.Visible = true;
                        break;
                    case 4:
                        setFormData(fvDocDetail2, FormViewMode.Insert, null);
                        switch (doc_decision)
                        {
                            case 0:
                                setFormData(fvDocDetail3, FormViewMode.Insert, null);
                                SETFOCUS.Focus();
                                break;
                            case 4:
                                sampleBeforeRecieve.Visible = true;
                                SETFOCUS.Focus();
                                break;

                        }
                        logList.Visible = true;
                        SETFOCUS.Focus();
                        break;
                    case 5:

                        switch (doc_decision)
                        {
                            case 0:
                                setFormData(fvDocInsertAnalysis, FormViewMode.Insert, null);
                                SETFOCUS.Focus();
                                break;

                            case 1:
                                setFormData(fvDocDetail2, FormViewMode.Insert, null);
                                sampleRecieve.Visible = true;
                                SETFOCUS.Focus();
                                break;
                            case 2:
                                setFormData(fvDocDetail2, FormViewMode.Insert, null);
                                sampleBeforeRecieve.Visible = true;
                                SETFOCUS.Focus();
                                break;
                        }
                        logList.Visible = true;
                        SETFOCUS.Focus();
                        break;

                    case 6:

                        switch (doc_decision)
                        {
                            case 0:
                                //setFormData(fvInsert_Result_LabIn, FormViewMode.Insert, null);
                                setFormData(fvDocInsertAnalysisResult, FormViewMode.Insert, null);
                                SETFOCUS.Focus();
                                break;
                            case 1:
                                setFormData(fvDocDetail2, FormViewMode.Insert, null);
                                sampleWaitingSupervisor.Visible = true;
                                SETFOCUS.Focus();
                                break;
                            case 6:
                                //setFormData(fvDocDetail2, FormViewMode.Insert, null);
                                setFormData(fvInsert_Result_LabIn, FormViewMode.Insert, null);
                                setFormData(fvDocInsertApprove, FormViewMode.Insert, null);
                                SETFOCUS.Focus();
                                break;
                        }
                        //sampleWaitingSupervisor.Visible = true;
                        logList.Visible = true;
                        SETFOCUS.Focus();
                        break;
                    case 7:
                        switch (doc_decision)
                        {
                            case 0:
                                setFormData(fvDocDetail2, FormViewMode.Insert, null);
                                setFormData(fvDocDetail3_OutAdmin, FormViewMode.Insert, null);
                                break;
                            case 1:
                                setFormData(fvDocDetail12, FormViewMode.Insert, null);
                                sampleRecieveforLabExternal.Visible = true;
                                break;
                        }
                        logList.Visible = true;
                        SETFOCUS.Focus();
                        break;
                    case 8:
                        setFormData(fvDocDetail2, FormViewMode.Insert, null);
                        switch (doc_decision)
                        {
                            case 0:
                                //sampleRecieveDecisionUser.Visible = true;
                                setFormData(fvDocDetail3_Out, FormViewMode.Insert, null);
                                SETFOCUS.Focus();
                                break;
                            case 1:

                                break;
                            case 3:
                                setFormData(fvDocDetail3_OutUserDecision, FormViewMode.Insert, null);
                                SETFOCUS.Focus();
                                break;
                            case 8:
                                sampleRecieveDecisionUser.Visible = true;
                                SETFOCUS.Focus();
                                break;
                            case 91:
                                setFormData(fvDocDetail3_OutUserDecision, FormViewMode.Insert, null);
                                SETFOCUS.Focus();
                                break;
                        }
                        logList.Visible = true;
                        break;
                    case 9:
                        setFormData(fvDocDetail2, FormViewMode.Insert, null);
                        switch (doc_decision)
                        {
                            case 0:
                                setFormData(fvDocDetail3_OutUserDecision, FormViewMode.Insert, null);
                                SETFOCUS.Focus();
                                break;
                            case 1:
                                sampleRecieveforadmin.Visible = true;
                                //setFormData(fvDocDetail3_OutUserDecision, FormViewMode.Insert, null);
                                break;
                        }
                        logList.Visible = true;
                        SETFOCUS.Focus();
                        break;
                    case 10:
                        setFormData(fvDocDetail2, FormViewMode.Insert, null);
                        switch (doc_decision)
                        {
                            case 0:

                                setFormData(fvDocInsertApprove, FormViewMode.Insert, null);
                                break;
                            case 1:

                                sampleApprove.Visible = true;
                                break;
                            case 2:

                                sampleApprove.Visible = true;

                                break;
                            case 3:

                                setFormData(fvDocDetail10, FormViewMode.Insert, null);
                                break;
                            case 10:

                                setFormData(fvDocDetail10, FormViewMode.Insert, null);
                                break;
                            case 20:
                                // setFormData(fvInsert_Result_LabOut, FormViewMode.Insert, null);
                                setFormData(fvDocInsertAnalysisResultOut, FormViewMode.Insert, null);
                                break;

                        }
                        logList.Visible = true;
                        SETFOCUS.Focus();
                        break;
                    case 11:
                        switch (doc_decision)
                        {
                            case 0:
                                sampleRecieveforLabExternal.Visible = true;
                                break;
                            case 1:
                                setFormData(fvDocDetail12, FormViewMode.Insert, null);
                                break;
                        }
                        logList.Visible = true;
                        SETFOCUS.Focus();
                        break;
                    case 12:
                        setFormData(fvDocDetail12, FormViewMode.Insert, null);
                        samplePreview.Visible = true;
                        logListPreview.Visible = true;
                        SETFOCUS.Focus();
                        //sampleList.Visible = true;
                        //logList.Visible = true;
                        break;
                    case 13:
                        switch (doc_decision)
                        {
                            case 0:
                                setFormData(fvDocDetail12, FormViewMode.Insert, null);
                                samplePreviewReport.Visible = true;
                                SETFOCUS.Focus();
                                break;
                            case 1:
                                setFormData(fvDocDetail12, FormViewMode.Insert, null);
                                setFormData(fvDocDetail12_report, FormViewMode.Insert, null);
                                SETFOCUS.Focus();
                                break;
                        }
                        break;
                }
                break;
            case "tab2":
                switch (uidx)
                {
                    case 0:


                        show_data.Visible = true;
                        SETFOCUS.Focus();

                        break;
                    case 1:
                        switch (doc_decision)
                        {
                            case 29:
                                show_data.Visible = true;
                                SETFOCUS.Focus();
                                break;
                        }
                        //if (txtsearch_index.Text == "QA600003")
                        //{
                        //    show_search_doc.Visible = true;
                        //}
                        //else if (txtsearch_index.Text == "7G07003")
                        //{
                        //    show_search_sample.Visible = true;
                        //}
                        //else
                        //{
                        //    show_data.Visible = true;
                        //}
                        //show_search_doc.Visible = true;

                        break;

                    case 12:
                        switch (doc_decision)
                        {

                            case 12:

                                show_sucess.Visible = true;
                                SETFOCUS.Focus();
                                break;

                        }

                        break;
                }
                break;
            case "tab3":
                switch (uidx)
                {
                    case 0:
                        switch (doc_decision)
                        {
                            // set default show  maseter data
                            case 0:

                                master_meterail.Visible = true;
                                master_data_microbiological.Visible = false;
                                master_data_chemical.Visible = false;
                                break;
                            case 1:
                                master_data_microbiological.Visible = true;
                                master_data_chemical.Visible = false;
                                master_meterail.Visible = false;
                                break;
                            case 2:
                                master_data_chemical.Visible = true;
                                master_data_microbiological.Visible = false;
                                master_meterail.Visible = false;
                                break;

                        }

                        break;
                }

                break;
            case "tab4":
                break;
            case "tab5":
                switch (uidx)
                {
                    case 0:
                        setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                        break;
                    case 6:
                        switch (doc_decision)
                        {
                            case 21:
                                setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                                break;
                        }
                        //txt_samplecode_result.Visible = string.Empty;
                        break;
                }


                break;
            case "tab6":
                setFormData(fv_supervisor_detail, FormViewMode.Insert, null);
                break;
        }
    }

    protected void setActiveTab(string activeTab, int uidx, int doc_decision)
    {
        setActiveView(activeTab, uidx, doc_decision);
        switch (activeTab)
        {
            case "tab1":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                break;
            case "tab2":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                break;
            case "tab3":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                break;
            case "tab4":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                break;
            case "tab5":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "active");
                li5.Attributes.Add("class", "");
                break;
            case "tab6":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "active");
                break;
        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }
    #endregion reuse
}