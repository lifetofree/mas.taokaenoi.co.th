﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="T0_FeedBack_1.aspx.cs" Inherits="websystem_masterdata_T0_FeedBack_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="col-md-12">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server" CommandName="btnToInsert" OnCommand="btnCommand"><i class="fa fa-plus-square"></i> สร้างชุดคำถาม</asp:LinkButton>

                <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="TFBIDX1"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TFBIDX1" runat="server" CssClass="form-control"
                                    Visible="False" Text='<%# Eval("TFBIDX1")%>' />
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="row col-md-6 ">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <small>
                                                    <label class="pull-left">ระบบ</label>
                                                    <asp:Label ID="lbddlSysIDX" runat="server" Text='<%# Bind("SysIDX") %>' Visible="false" />
                                                    <asp:DropDownList ID="ddlSysIDXUpdate" AutoPostBack="true" runat="server"
                                                        CssClass="form-control">
                                                    </asp:DropDownList>
                                                </small>
                                            </div>
                                        </div>



                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <small>
                                                    <label class="pull-left">ชุดคำถาม</label>
                                                    <asp:UpdatePanel ID="panelQuestionSetUpdate" runat="server">
                                                        <ContentTemplate>

                                                            <asp:TextBox ID="txtQuestionSetUpdate" runat="server" CssClass="form-control"
                                                                Text='<%# Eval("QuestionSet")%>' />
                                                            <asp:RequiredFieldValidator ID="requiredQuestionSetUpdate"
                                                                ValidationGroup="saveQuestionSetUpdate" runat="server"
                                                                Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txtQuestionSetUpdate"
                                                                Font-Size="1em" ForeColor="Red"
                                                                ErrorMessage="กรุณากรอกชุดคำถาม" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </small>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <small>
                                                    <label class="pull-left">สถานะ</label>
                                                    <asp:DropDownList ID="ddlTFB1StatusUpdate" AutoPostBack="false" runat="server"
                                                        CssClass="form-control" SelectedValue='<%# Eval("TFB1Status") %>'>
                                                        <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                        <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </small>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="pull-left">
                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                                    ValidationGroup="saveTypeNameUpdate" CommandName="Update"
                                                    OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                         บันทึก
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                                    CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ระบบ" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="SysIDX" runat="server" Text='<%# Eval("SysNameEN") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชุดคำถาม" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="QuestionSet" runat="server" Text='<%# Eval("QuestionSet") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="TFB1Status" runat="server" Text='<%# getStatus((int)Eval("TFB1Status")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("TFBIDX1") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>

        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">
            <div class="col-md-10 col-md-offset-1">
                <div class="row col-md-6 ">
                    <div class="row col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                                        CommandName="btnCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ระบบ</label>
                                    <asp:UpdatePanel ID="panelSysIDX" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlSysIDX" runat="server" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="requiredddSysIDX"
                                                ValidationGroup="save" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="ddlSysIDX"
                                                InitialValue="0"
                                                Font-Size="1em" ForeColor="Red"
                                                ErrorMessage="กรุณากรอกชื่อระบบ" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>



                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ชุดคำถาม</label>
                                    <asp:UpdatePanel ID="panelQuestionSet" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtQuestionSet" runat="server" CssClass="form-control"
                                                placeholder="ชุดคำถาม..." />
                                            <asp:RequiredFieldValidator ID="requiredQuestionSet"
                                                ValidationGroup="save" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtQuestionSet"
                                                Font-Size="1em" ForeColor="Red"
                                                ErrorMessage="กรุณากรอกชุดคำถาม" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>สถานะ</label>
                                    <asp:DropDownList ID="ddlTFB1Status" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="1" Text="Online" />
                                        <asp:ListItem Value="0" Text="Offline" />
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="save" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <!-- End Insert Form -->


    </asp:MultiView>

</asp:Content>
