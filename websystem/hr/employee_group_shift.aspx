﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="employee_group_shift.aspx.cs" Inherits="websystem_hr_employee_group_shift" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; Group Diary</strong></h3>
                        </div>
                        <asp:Label ID="sfa" runat="server"></asp:Label>
                        <asp:UpdatePanel ID="PanelAddGroupOTDay" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="panel-body">
                                    <%-- <div class="form-horizontal" role="form">--%>

                                    <div class="form-group">
                                        <asp:LinkButton ID="btnadd" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="ADD DATA" runat="server" CommandName="CmdAdd" OnCommand="btnCommand"><i class="fa fa-plus"> ADD DATA</i></asp:LinkButton>
                                        <%--<asp:LinkButton ID="btnaddImport" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="ADD DATA" runat="server" CommandName="CmdAddImport" OnCommand="btnCommand"><i class="fa fa-plus"></i> IMPORT DATA</asp:LinkButton>--%>
                                        <hr />
                                    </div>

                                    <%------------------------ Div ADD  ------------------------%>

                                    <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Group Diary</strong></h4>
                                            <div class="form-horizontal" role="form">
                                                <%--<div class="panel panel-default">--%>
                                                <div class="panel-heading">

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ชื่อกลุ่ม" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-4">
                                                            <asp:TextBox ID="txtgroupname" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RqDoc2Cwreate" ValidationGroup="AddGroup" runat="server" Display="None"
                                                            ControlToValidate="txtgroupname" Font-Size="11"
                                                            ErrorMessage="Plese groupname" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqDoc2Cwreate" Width="160" />
                                                    </div>

                                                    <asp:Panel runat="server" ID="box_org" Visible="false">
                                                        <div class="form-group">
                                                            <asp:Label ID="Label6" runat="server" Text="ประเภทพนักงาน" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                                            <div class="col-sm-4">
                                                                <asp:DropDownList ID="ddlemp_type" runat="server" CssClass="form-control" AutoPostBack="true">
                                                                    <asp:ListItem Value="0">เลือกประเภทพนักงาน....</asp:ListItem>
                                                                    <asp:ListItem Value="1">รายวัน</asp:ListItem>
                                                                    <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                                                                </asp:DropDownList>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label1" runat="server" Text="บริษัท" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                                            <div class="col-sm-4">
                                                                <asp:DropDownList ID="ddlorg" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label2" runat="server" Text="ฝ่าย" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                                            <div class="col-sm-4">
                                                                <asp:DropDownList ID="ddldep" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label3" runat="server" Text="หน่วยงาน" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddlsec" runat="server" CssClass="form-control" AutoPostBack="true">
                                                                <asp:ListItem Value="0">เลือกหน่วยงาน....</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="R2detor1" ValidationGroup="AddGroup" runat="server" Display="None"
                                                                ControlToValidate="ddlsec" Font-Size="11"
                                                                ErrorMessage="เลือกหน่วยงาน ...."
                                                                ValidationExpression="เลือกหน่วยงาน ...." InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R2detor1" Width="160" />
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="LinkButton2" CssClass="btn btn-primary" data-toggle="tooltip" title="Search" runat="server" CommandName="cmdSearchSection"
                                                                OnCommand="btnCommand" ValidationGroup="AddGroup">Search</asp:LinkButton>
                                                        </div>
                                                    </div>

                                                    <asp:Panel runat="server" ID="box_import" Visible="false">
                                                        <div class="form-group">
                                                            <asp:Label ID="Label25" CssClass="col-sm-2 control-label" runat="server" Text="Status" />
                                                            <div class="col-sm-4">
                                                                <asp:DropDownList ID="ddl_status" AutoPostBack="true" CssClass="form-control" runat="server">
                                                                    <asp:ListItem Value="1">Online.....</asp:ListItem>
                                                                    <asp:ListItem Value="0">Offline.....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label7" runat="server" Text="เลือกไฟล์ Excel" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                            <div class="col-sm-4">
                                                                <asp:FileUpload ID="UploadFileExcel" runat="server" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-4 col-sm-offset-2">
                                                                <asp:LinkButton ID="btnImport" CssClass="btn btn-primary" data-toggle="tooltip" title="Import Excel" runat="server" CommandName="cmdImportExcel"
                                                                    OnCommand="btnCommand">Import Excel</asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelImport" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i> ยกเลิกรายการ</asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>

                                                    <%--<div class="form-group">
                                                        <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>--%>

                                                    <asp:Panel ID="Div_GvDetailExcelImport" runat="server" Visible="false">
                                                        <asp:GridView ID="GvDetailExcelImport"
                                                            runat="server" RowStyle-Font-Size="Small"
                                                            HeaderStyle-Font-Size="Small"
                                                            AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet"
                                                            RowStyle-Wrap="true"
                                                            CssClass="table table-striped table-bordered table-responsive"
                                                            HeaderStyle-Height="25px"
                                                            ShowHeaderWhenEmpty="True"
                                                            ShowFooter="False"
                                                            BorderStyle="None"
                                                            CellSpacing="2">
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:Label ID="lbl_emp_code_excel" runat="server" Text='<%# Eval("emp_code")%>'></asp:Label>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ชื่อ - นามสกุล" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:Label ID="lbl_emp_name_excel" runat="server" Text='<%# Eval("emp_name")%>'></asp:Label>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="3%">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:Label ID="lbl_dept_name_th_excel" runat="server" Text='<%# Eval("DeptNameTH")%>'></asp:Label>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>

                                                        <asp:GridView ID="GvDetailSection"
                                                            runat="server" RowStyle-Font-Size="Small"
                                                            HeaderStyle-Font-Size="Small"
                                                            AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet"
                                                            RowStyle-Wrap="true"
                                                            CssClass="table table-striped table-bordered table-responsive"
                                                            HeaderStyle-Height="25px"
                                                            ShowHeaderWhenEmpty="True"
                                                            ShowFooter="False"
                                                            BorderStyle="None"
                                                            CellSpacing="2">
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:Label ID="lbl_emp_code_excel" runat="server" Text='<%# Eval("emp_code")%>'></asp:Label>
                                                                            <asp:Label ID="lbl_emp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx")%>'></asp:Label>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ชื่อ - นามสกุล" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:Label ID="lbl_emp_name_excel" runat="server" Text='<%# Eval("emp_name_th")%>'></asp:Label>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="3%">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:Label ID="lbl_dept_name_th_excel" runat="server" Text='<%# Eval("pos_name_th")%>'></asp:Label>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                                <%--<asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="3%">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:LinkButton ID="btndelete_emp" CssClass="btn btn-danger" runat="server" CommandName="btndelete_emp" OnCommand="btnCommand" CommandArgument='<%# Eval("emp_idx") %>' data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบข้อมูลรายการใช่หรือไม่ ?')"><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                            </Columns>
                                                        </asp:GridView>

                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnSaveImportExcel" CssClass="btn btn-success" data-toggle="tooltip" title="Import Excel" runat="server" ValidationGroup="AddGroupOTDay" CommandName="cmdSaveImportExcel"
                                                                OnCommand="btnCommand">บันทึกข้อมูล</asp:LinkButton>
                                                        </div>

                                                    </asp:Panel>

                                                </div>

                                            </div>
                                            <hr />
                                            <%--  </div>--%>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="Panel_Add_Import" runat="server" Visible="false">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Import Group Diary</strong></h4>
                                            <div class="form-horizontal" role="form">
                                                <%--<div class="panel panel-default">--%>
                                                <div class="panel-heading">

                                                    <%--<div class="form-group">
                                                        <asp:Label ID="Label7" runat="server" Text="เลือกไฟล์ Excel" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-4">
                                                            <asp:FileUpload ID="UploadFileExcel" runat="server" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label6" runat="server" Text="ชื่อกลุ่ม" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-4">
                                                            <asp:TextBox ID="txtgroupname_Import" runat="server" CssClass="form-control" PlaceHoldaer="........" ValidationGroup="AddGroupOTDay" />
                                                            <asp:RequiredFieldValidator ID="Reqsator1"
                                                                runat="server"
                                                                ControlToValidate="txtgroupname_Import" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอกชื่อกลุ่ม...."
                                                                ValidationGroup="AddGroupOTDay" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqsator1" Width="160" PopupPosition="BottomLeft" />
                                                        </div>
                                                    </div>--%>


                                                    <%--<div class="form-group">
                                                        <asp:LinkButton ID="btnImport" CssClass="btn btn-primary" data-toggle="tooltip" title="Import Excel" runat="server" CommandName="cmdImportExcel"
                                                            OnCommand="btnCommand">Import Excel</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton1" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelImport" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i> ยกเลิกรายการ</asp:LinkButton>
                                                    </div>--%>


                                                    <%--<asp:Panel ID="Div_GvDetailExcelImport" runat="server" Visible="false">
                                                        <asp:GridView ID="GvDetailExcelImport"
                                                            runat="server" RowStyle-Font-Size="Small"
                                                            HeaderStyle-Font-Size="Small"
                                                            AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet"
                                                            RowStyle-Wrap="true"
                                                            CssClass="table table-striped table-bordered table-responsive"
                                                            HeaderStyle-Height="25px"
                                                            ShowHeaderWhenEmpty="True"
                                                            ShowFooter="False"
                                                            BorderStyle="None"
                                                            CellSpacing="2">
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="center">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:Label ID="lbl_emp_code_excel" runat="server" Text='<%# Eval("emp_code")%>'></asp:Label>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ชื่อ - นามสกุล" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:Label ID="lbl_emp_name_excel" runat="server" Text='<%# Eval("emp_name")%>'></asp:Label>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <asp:Label ID="lbl_dept_name_th_excel" runat="server" Text='<%# Eval("DeptNameTH")%>'></asp:Label>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>

                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnSaveImportExcel" CssClass="btn btn-success" data-toggle="tooltip" title="Import Excel" runat="server" ValidationGroup="AddGroupOTDay" CommandName="cmdSaveImportExcel"
                                                                OnCommand="btnCommand">บันทึกข้อมูล</asp:LinkButton>
                                                        </div>

                                                    </asp:Panel>--%>
                                                </div>
                                            </div>
                                        </div>

                                    </asp:Panel>

                                    <div id="div_search" runat="server">
                                        <div class="form-horizontal" role="form">
                                            <div class="form-group">

                                                <asp:Label ID="Label160" runat="server" Text="Name" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                  <asp:TextBox ID="txtname_search" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                               
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-4 col-sm-offset-2">

                                                    <asp:LinkButton ID="LinkButton36" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdSearch" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton37" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefresh" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <asp:Panel ID="Panel_Show_Detail" runat="server" Visible="false">
                                        <asp:GridView ID="GvMaster" runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                            HeaderStyle-CssClass="primary"
                                            HeaderStyle-Height="40px"
                                            AllowPaging="true"
                                            DataKeyNames="m0_group_diary_idx"
                                            PageSize="10"
                                            OnRowDataBound="Master_RowDataBound"
                                            OnRowEditing="Master_RowEditing"
                                            OnPageIndexChanging="Master_PageIndexChanging"
                                            OnRowUpdating="Master_RowUpdating"
                                            OnRowCancelingEdit="Master_RowCancelingEdit">

                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <asp:TemplateField HeaderText="#">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIDX" runat="server" Visible="false" Text='<%# Eval("m0_group_diary_idx") %>' />
                                                        <%# (Container.DataItemIndex +1) %>
                                                    </ItemTemplate>


                                                    <EditItemTemplate>
                                                        <div class="form-horizontal" role="form">
                                                            <div class="panel-heading">
                                                                <div class="form-group">
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txtIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_group_diary_idx")%>' />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-labelnotop1">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label4" runat="server" Text="Group Name" CssClass="control-label"></asp:Label>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-4">
                                                                        <asp:TextBox ID="txtName_update" runat="server" CssClass="form-control" Text='<%# Eval("group_diary_name")%>' />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-labelnotop1">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label65" runat="server" Text="Organization" CssClass="control-label"></asp:Label>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-4">
                                                                        <asp:TextBox ID="txtEditorgIDX_Select" runat="server" Visible="False" Text='<%# Eval("org_idx") %>'></asp:TextBox>
                                                                        <asp:DropDownList ID="ddlOrganizationEdit_Select" runat="server" AutoPostBack="True" class="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-labelnotop1">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label66" runat="server" Text="Department" CssClass="control-label"></asp:Label>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-4">
                                                                        <asp:TextBox ID="txtEditDepIDX_Select" runat="server" Visible="False" Text='<%# Eval("rdept_idx") %>'></asp:TextBox>
                                                                        <asp:DropDownList ID="ddlDepartmentEdit_Select" runat="server" AutoPostBack="True" class="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-labelnotop1">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label67" runat="server" Text="Section" CssClass="control-label"></asp:Label>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-4">
                                                                        <asp:TextBox ID="txtEditSecIDX_Select" runat="server" Visible="False" Text='<%# Eval("rsec_idx") %>'></asp:TextBox>
                                                                        <asp:DropDownList ID="ddlSectionEdit_Select" runat="server" AutoPostBack="True" class="form-control"></asp:DropDownList>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-labelnotop1">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label5" runat="server" Text="Status" CssClass="control-label"></asp:Label>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-4">
                                                                        <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("group_diary_status") %>'>
                                                                            <asp:ListItem Value="1" Text="Online" />
                                                                            <asp:ListItem Value="0" Text="Offline" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-2 col-sm-offset-3">
                                                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </EditItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lborg" runat="server" Text='<%# Eval("group_diary_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Department" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbdept" runat="server" Text='<%# Eval("DeptNameTH") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Section" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbsection" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnedit" CssClass="btn btn-primary" runat="server" CommandName="btnedit_detail" OnCommand="btnCommand" data-toggle="tooltip" title="แก้ไขข้อมูล" CommandArgument='<%#Eval("rsec_idx") %>'><i class="fa fa-cog"></i></asp:LinkButton>
                                                        <%--<asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>--%>
                                                        <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_group_diary_idx") %>'><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                    </ItemTemplate>

                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>

                                    <asp:Panel ID="Panel_shidw_detail_list" runat="server" Visible="false">
                                        <div class="row">
                                            <div class="form-group">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <asp:LinkButton ID="btnBackToDetailOTDay" CssClass="btn btn-success" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                                            CommandName="btnedit_back" OnCommand="btnCommand" CommandArgument="2">< ย้อนกลับ</asp:LinkButton>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnBackToDetailOTDay" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>

                                        <asp:GridView ID="GvDetailSection_list"
                                            runat="server" RowStyle-Font-Size="Small"
                                            HeaderStyle-Font-Size="Small"
                                            AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet"
                                            RowStyle-Wrap="true"
                                            CssClass="table table-striped table-bordered table-responsive"
                                            HeaderStyle-Height="25px"
                                            ShowHeaderWhenEmpty="True"
                                            ShowFooter="False"
                                            BorderStyle="None"
                                            CellSpacing="2">
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lbl_emp_code_excel" runat="server" Text='<%# Eval("emp_code")%>'></asp:Label>
                                                            <asp:Label ID="lbl_emp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx")%>'></asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ชื่อ - นามสกุล" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lbl_emp_name_excel" runat="server" Text='<%# Eval("emp_name_th")%>'></asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="3%">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lbl_dept_name_th_excel" runat="server" Text='<%# Eval("pos_name_th")%>'></asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>



                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnadd" />
                                <asp:PostBackTrigger ControlID="btnImport" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </asp:View>
            </asp:MultiView>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

