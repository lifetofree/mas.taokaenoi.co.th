﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_probation_manage_probation : System.Web.UI.Page
{
    #region initial function/data

    function_tool _funcTool = new function_tool();
    data_probation _data_probation = new data_probation();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetInformationNameEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetInformationNameEmployee"];
    static string _urlGetListNameProbation = _serviceUrl + ConfigurationManager.AppSettings["urlGetListNameProbation"];
    static string _urlGetNameAssessor = _serviceUrl + ConfigurationManager.AppSettings["urlGetNameAssessor"];
    static string _urlSetAssessor = _serviceUrl + ConfigurationManager.AppSettings["urlSetAssessor"];


    string _localJson = "";
    int _tempInt = 0;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        set_defult();

        if (!IsPostBack)
        {
            action_select_listNAme();
        }
    }

    #region SetDefult
    protected void set_defult()
    {
        listMenu0.BackColor = System.Drawing.Color.LightGray;

    }
    #endregion

    #region select data
    protected void action_select_listNAme()
    {
        data_probation _data_probation = new data_probation();
        _data_probation.u0_name_probation_list = new u0_name_probation_detail[1];
        u0_name_probation_detail _name_probation = new u0_name_probation_detail();

        _data_probation.u0_name_probation_list[0] = _name_probation;
        _data_probation = callServiceProbation(_urlGetListNameProbation, _data_probation);

        setGridViewDataBind(gvListNameProbation, _data_probation.u0_name_probation_list);
    }

    protected void action_select_information_employee()
    {
        data_probation _data_probation = new data_probation();
        _data_probation.u0_name_probation_list = new u0_name_probation_detail[1];
        u0_name_probation_detail select_information = new u0_name_probation_detail();

        select_information.emp_probation_idx = int.Parse(ViewState["emp_probation_idx"].ToString());

        _data_probation.u0_name_probation_list[0] = select_information;
        // check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_probation));
        _data_probation = callServiceProbation(_urlGetInformationNameEmployee, _data_probation);

        setRepeaterDataBind(Information_employee, _data_probation.u0_name_probation_list);

    }

    protected void action_select_assessor()
    {
        data_probation _data_probation = new data_probation();
        _data_probation.m0_name_assessor_list = new m0_name_assessor_detail[1];
        m0_name_assessor_detail select_assessor = new m0_name_assessor_detail();
        ddlSetAssessor1.AppendDataBoundItems = true;
        ddlSetAssessor1.Items.Add(new ListItem("กรุณาเลือกผู้ประเมินทดลองงาน", "0"));
        ddlSetAssessor2.AppendDataBoundItems = true;
        ddlSetAssessor2.Items.Add(new ListItem("กรุณาเลือกผู้ประเมินทดลองงาน", "0"));

        select_assessor.emp_assessor_idx = int.Parse(ViewState["emp_probation_idx"].ToString());

        _data_probation.m0_name_assessor_list[0] = select_assessor;
        _data_probation = callServiceProbation(_urlGetNameAssessor, _data_probation);

        ddlSetAssessor1.DataSource = _data_probation.m0_name_assessor_list;
        ddlSetAssessor1.DataTextField = "name_assessor";
        ddlSetAssessor1.DataValueField = "emp_assessor_idx";
        ddlSetAssessor1.DataBind();

        ddlSetAssessor2.DataSource = _data_probation.m0_name_assessor_list;
        ddlSetAssessor2.DataTextField = "name_assessor";
        ddlSetAssessor2.DataValueField = "emp_assessor_idx";
        ddlSetAssessor2.DataBind();

    }

    #endregion

    #region checkboxIndexChange
    protected void checkboxIndexChange(object sender, EventArgs e)
    {
        var checkboxName = (CheckBox)sender;

        var check_haveNot_assessor = (CheckBox)viewPage_Manage_Evaluation.FindControl("check_haveNot_assessor");

        switch (checkboxName.ID)
        {
            case "check_haveNot_assessor":
                if (check_haveNot_assessor.Checked)
                {
                    ddlSetAssessor2.Enabled = false;
                    ddlSetAssessor1.Enabled = false;
                }
                else
                {
                    ddlSetAssessor2.Enabled = true;
                    ddlSetAssessor1.Enabled = true;
                }

                break;
        }

    }

    #endregion

    #region insert data
    protected void action_insert_assessor()
    {
        data_probation _data_probation = new data_probation();
        _data_probation.u0_assessment_list = new u0_assessment_detail[1];
        u0_assessment_detail insert_assessor = new u0_assessment_detail();

        if (check_haveNot_assessor.Checked == false)
        {
            insert_assessor.emp_idx_probation = int.Parse(ViewState["emp_probation_idx"].ToString());
            insert_assessor.m0_actor_idx = 1;
            insert_assessor.m0_node_idx = 1;
            insert_assessor.emp_idx_assessor1 = int.Parse(ddlSetAssessor1.SelectedValue);
            insert_assessor.emp_idx_assessor2 = int.Parse(ddlSetAssessor2.SelectedValue);
            insert_assessor.emp_idx_hr_director = 127;
            insert_assessor.probation_status = 1;

        }
        else if (check_haveNot_assessor.Checked == true)
        {
            insert_assessor.emp_idx_probation = int.Parse(ViewState["emp_probation_idx"].ToString());
            insert_assessor.m0_actor_idx = 1;
            insert_assessor.m0_node_idx = 1;
            insert_assessor.probation_status = 1;
            insert_assessor.emp_idx_assessor1 = 0;
            insert_assessor.emp_idx_assessor2 = 0;
            insert_assessor.emp_idx_hr_director = 127;

        }

        _data_probation.u0_assessment_list[0] = insert_assessor;

        _data_probation = callServiceProbation(_urlSetAssessor, _data_probation);

    }


    #endregion

    #region setRepeaterDataBind
    protected void setRepeaterDataBind(Repeater RepeaterName, Object obj)
    {
        RepeaterName.DataSource = obj;
        RepeaterName.DataBind();
    }
    #endregion

    #region setGridViewDataBind
    protected void setGridViewDataBind(GridView gridViewName, Object obj)
    {
        gridViewName.DataSource = obj;
        gridViewName.DataBind();
    }
    #endregion

    #region gvRowDataBound

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvListNameProbation":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lb_status_node_defult = (Label)e.Row.Cells[3].FindControl("lb_status_node_defult");
                    Label lb_status_node = (Label)e.Row.Cells[3].FindControl("lb_status_node");


                    ViewState["status_node"] = lb_status_node.Text;

                    if (ViewState["status_node"].ToString() == "" && ViewState["status_node"].ToString() == null && ViewState["status_node"].ToString() == "0")
                    {
                        lb_status_node_defult.Visible = true;
                    }
                    else if (ViewState["status_node"].ToString() != "" && ViewState["status_node"].ToString() != null)
                    {
                        lb_status_node.Visible = true;
                    }
                }
                break;
        }
    }

    #endregion

    #region gvPageIndexChanging

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvListNameProbation":
                gvListNameProbation.PageIndex = e.NewPageIndex;
                gvListNameProbation.DataBind();
                action_select_listNAme();
                break;
        }

    }

    #endregion

    #region btn_Command

    protected void btn_command(object sender, CommandEventArgs e)
    {
        string cmd_name = e.CommandName.ToString();
        string cmd_arg = e.CommandArgument.ToString();

        switch (cmd_name)
        {
            case "btnViewSelected":
                listMenu0.BackColor = System.Drawing.Color.LightGray;
                listMenu1.BackColor = System.Drawing.Color.Transparent;

                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                action_select_listNAme();
                break;

            case "btnViewresultRate":
                listMenu0.BackColor = System.Drawing.Color.Transparent;
                listMenu1.BackColor = System.Drawing.Color.LightGray;

                break;

            case "Manage_probation":
                mvMultiview.SetActiveView(viewPage_Manage_Evaluation);
                ViewState["emp_probation_idx"] = int.Parse(cmd_arg);
                action_select_information_employee();
                action_select_assessor();
                setFocus.Focus();

                break;

            case "saveAssessor":
                action_insert_assessor();
                break;
        }

    }

    #endregion

    #region call method
    protected data_probation callServiceProbation(string _cmdUrl, data_probation _data_probation)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_probation);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_probation = (data_probation)_funcTool.convertJsonToObject(typeof(data_probation), _localJson);

        return _data_probation;
    }
    #endregion
}