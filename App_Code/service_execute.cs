using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for service_execute
/// </summary>
public class service_execute
{
    #region initial function/data
    function_db _funcDb = new function_db();
    function_tool _funcTool = new function_tool();

    string _spName = String.Empty;
    string _localXml = String.Empty;
    string _localJson = String.Empty;
    string _resultCode = String.Empty;
    string _resultMsg = String.Empty;

    #endregion  initial function/data

    public service_execute()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string actionExec(string connName, string dataName, string cmdName, Object objIn, int actionType)
    {
        // switch stored procedure name
        switch (cmdName)
        {
            case "imxlsService":
                _spName = "imxls_myimxls_action";
                break;
            case "imxlsDistributeService":
                _spName = "imxls_distribute_action";
                break;
            case "imxlsEmpcodeStackService":
                _spName = "imxls_empcode_stack_action";
                break;
            case "room_list":
                _spName = "sp_u0_room_list_action";
                break;
            case "empsRewardService":
                _spName = "emps_reward_action";
                break;
            case "empsGroupDiaryService":
                _spName = "emps_group_diary_action";
                break;
            case "empsParttimeService":
                _spName = "emps_parttime_action";
                break;
            case "empshiftService":
                _spName = "emps_empshift_action";
                break;
            case "empsodspeService":
                _spName = "emps_odspe_action";
                break;
            case "empsAnnounceDiaryService":
                _spName = "emps_announce_diary_action";
                break;
            case "empsChangeAnnounceDiaryService":
                _spName = "emps_change_announce_diary_action";
                break;
            case "empsAnnounceService":
                _spName = "emps_announce_action";
                break;
            case "empsChangeAnnounceService":
                _spName = "emps_change_announce_action";
                break;
            case "empsApprovalLogService":
                _spName = "emps_approval_log_action";
                break;
            case "empsEmployeeService":
                _spName = "emps_employee_data_action";
                break;
            case "empsExportService":
                _spName = "emps_export_action";
                break;
            case "ODSP_Reletion":
                _spName = "ODSP_Reletion_Action";
                break;
            //Manage Employee
            case "Manage_Employee":
                _spName = "manage_employee_Action";
                break;

            //jOB
            case "JOB":
                _spName = "add_job";
                break;
            case "InMas":
                _spName = "manage_employee_Action";
                break;

            case "HoldersDevice":
                _spName = "HolderDevicesAction";
                break;

            case "ITSupport":
                _spName = "ITSupportAction";
                break;

            case "EquipmentTypeAction":
                _spName = "EquipmentTypeAction";
                break;

            case "service_systemmenu":
                _spName = "sp_service_control_m0_system";
                break;

            case "service_figermanagement":
                _spName = "fm_fingermanagement_action";
                break;

            case "service_ODSP_Reletion":
                _spName = "ODSP_Reletion";
                break;

            case "statalIPAddressService":
                _spName = "statal_ipaddress_action";
                break;

            case "statalLogIPAddressService":
                _spName = "statal_log_ipaddress_action";
                break;

            case "statalConfigService":
                _spName = "statal_config_action";
                break;

            case "adPermissionTypeService":
                _spName = "ad_permission_type_action";
                break;

            case "adPermissionPolicyService":
                _spName = "ad_permission_policy_action";
                break;

            case "adPermissionCategoryService":
                _spName = "ad_permission_category_action";
                break;

            case "adEmployeeLifeTimeTypeService":
                _spName = "ad_employee_lifetime_type_action";
                break;

            case "adOnlineService":
                _spName = "ad_online_action";
                break;

            case "adLogOnlineService":
                _spName = "ad_log_online_action";
                break;

            case "adCRUDNodeService":
                _spName = "ad_crud_node_action";
                break;

            case "cenODSPE":
                _spName = "cen_odspe_action";
                break;

            case "service_CenterCentralize":
                _spName = "CenterCentralize";
                break;

            case "service_CasePOSAction":
                _spName = "CasePOSAction";
                break;

            case "service_CHR":
                _spName = "chr_u0_reportchr";
                break;

            case "services_networklayout":
                _spName = "sp_service_networklayout";
                break;


            case "service_u0_device":
                _spName = "sp_devicesregis_action";
                break;

            case "service_m0_typedevice":
                _spName = "sp_m0_typedevice_action";
                break;

            case "service_m0_moniter":
                _spName = "sp_m0_moniter_action";
                break;

            case "service_m0_band":
                _spName = "sp_m0_band_action";
                break;

            case "service_m0_level":
                _spName = "sp_m0_leve_action";
                break;

            case "service_m0_status":
                _spName = "sp_m0_status_action";
                break;

            case "service_m0_insurance":
                _spName = "sp_m0_insurance_action";
                break;

            case "service_m0_hdd":
                _spName = "sp_m0_hdd_action";
                break;

            case "service_m0_ram":
                _spName = "sp_m0_ram_action";
                break;

            case "service_m0_vga":
                _spName = "sp_m0_vga_action";
                break;

            case "service_m0_cpu":
                _spName = "sp_m0_cpu_action";
                break;

            case "service_m0_printer":
                _spName = "sp_m0_printer_action";
                break;

            case "services_ma":
                _spName = "sp_service_ma_online";
                break;

            case "service_enrepair":
                _spName = "EN_u0_ENRepair";
                break;

            case "service_hny_random":
                _spName = "sp_hny_random_action";
                break;

            case "service_hr_costcenter":
                _spName = "sp_m0_costcenter";
                break;

            case "service_chr_online":
                _spName = "service_changerequest_online_action";
                break;

            case "service_nmon":
                _spName = "service_nmon_action";
                break;

            case "service_planning":
                _spName = "service_enp_planning";
                break;
            case "service_roombooking":
                _spName = "rbk_roombooking_action";
                break;

            case "service_recruit_quest":
                _spName = "service_recruitment_question";
                break;

            default:
                _spName = "";
                break;

        }

        // exec  stored procedure
        if (_spName != null)
        {
            // convert to xml
            _localXml = _funcTool.convertObjectToXml(objIn);
            // execute
            _localXml = _funcDb.execSPXml(connName, _spName, _localXml, actionType);
        }
        else
        {
            // set for null _spName
            _resultCode = "\"return_code\": 000";
            _resultMsg = "\"return_msg\": \"stored procedure is null\"";

            _localJson = "{\"" + dataName + "\": " + "{" + _resultCode + "," + _resultMsg + "}";
            // convert to xml
            _localXml = _funcTool.convertJsonToXml(_localJson);
        }

        return _localXml;
    }
}