﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_rpt_trainingplan : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_en_planning _data_en_planning = new data_en_planning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_report = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_Report_Plan"];
    static string _urlGetel_lu_TraningAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_lu_TraningAll"];
    static string _urlGetel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_plan_course"];

    string _localJson = "";
    int _tempInt = 0;

    int emp_idx = 0;


    #endregion Init

    #region Constant
    public static class Constants
    {
        public const int SELECT_ALL = 20;
        public const int SELECT_WHERE = 21;
        public const int NUMBER_NULL = -999;
    }
    #endregion Constant

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        
        if (!IsPostBack)
        {
            getTitle();
            hddf_empty.Value = "full";
            actionIndex();
            hddf_empty.Value = "";

        }
        linkBtnTrigger(btnFilter);
        linkBtnTrigger(btnexport1);

    }
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    #endregion Page Load
    public void getTitle()
    {
        _func_dmu.zDropDownList(ddlYearSearch_L,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
        _func_dmu.zDropDownList(ddltrn_group_L,
                                "-",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-PLAN-COURSE-GRP"
                                );
    }

    #region Action
    protected void actionIndex()
    {
        zSetMode(0);
        zShowData();
    }

    #endregion Action

    #region Check Session Redirect
    protected void checkSessionRedirect()
    {
        //if (Session["sesLoginIdxBackEnd"] == null)
        //{
        //   Response.Redirect(ResolveUrl("~/login"));
        //}
    }
    #endregion Check Session Redirect

    private void Page_Init(object sender, EventArgs e)
    {
        ClearApplicationCache();
    }
    #region reuse
    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }
    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "btnFilter":
                zShowData();
                break;
            case "btnexport1":
                
                ExportGridToExcel(Gvreport_training_plan, "Training Plan Year", "Training Plan Year");
                break;
        }
    }
    #endregion btnCommand

    #region zSetMode
    public void zSetMode(int AiMode)
    {

        switch (AiMode)
        {
            case 0:  //แผนการทำ PM เครื่องจักร
                {
                    setActiveTab("plan_pm");
                   
                }
                break;
            case 1:  //Report
                {
                    setActiveTab("report");
                }
                break;
            case 2://preview mode
                {


                }
                break;
            case 3://Detail mode
                {


                }
                break;
            case 4:  //insert detail mode
                {


                }
                break;
        }
    }
    #endregion zSetMode

    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        //liplan_pm.Attributes.Add("class", "");
        //switch (activeTab)
        //{
        //    case "plan_pm":
        //        liplan_pm.Attributes.Add("class", "active");
        //        break;
        //    case "report":
        //        lireport.Attributes.Add("class", "active");
        //        break;

        //}
    }
    #endregion setActiveTab

    public string zMonthEN(int AMonth)
    {

        return _func_dmu.zMonthEN(AMonth);
    }
    
    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        hddf_empty.Value = "";
        var ddName = (DropDownList)sender;
        switch (ddName.ID)
        {
            case "ddllocate":
                break;

        }

    }

    #endregion

    private void zShowData()
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_report_plan_action = new training_rpt_plan[1];
        training_rpt_plan obj_trn_rpt = new training_rpt_plan();
        obj_trn_rpt.filter_keyword = txtFilterKeyword_L.Text;
        obj_trn_rpt.zyear = _func_dmu.zStringToInt(ddlYearSearch_L.SelectedValue);
        obj_trn_rpt.course_plan_status = ddlcourse_plan_status_L.SelectedValue;
        obj_trn_rpt.m0_training_group_idx_ref = _func_dmu.zStringToInt(ddltrn_group_L.SelectedValue);
        
        obj_trn_rpt.operation_status_id = "report_training_plan";
        dataelearning.el_report_plan_action[0] = obj_trn_rpt;
        //litDebug.Text = _funcTool.convertObjectToJson(dataelearning);
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_report, dataelearning);
        CreateDs_report_training_plan();
        if (dataelearning.el_report_plan_action != null)
        {
            DataSet ds = (DataSet)ViewState["vsreport_training_plan"];
            DataRow dr;
            int i = 0;
            foreach (var v_item in dataelearning.el_report_plan_action)
            {
                i++;
                decimal _persen = 0;
                dr = ds.Tables["dsreport_training_plan"].NewRow();
                dr["id"] = i.ToString();
                dr["course_plan_status"] = v_item.course_plan_status;
                dr["u0_training_course_idx"] = v_item.u0_training_course_idx.ToString();
                dr["ztype"] = v_item.ztype;
                dr["zstatus_training_plan"] = v_item.zstatus_training_plan;
                dr["zcatergories"] = v_item.zcatergories;
                dr["zyear_plan"] = _func_dmu.zMonthEN(v_item.zyear_plan);
                dr["ztraining_date"] = v_item.ztraining_date;
                dr["zcourse_name"] = v_item.zcourse_name;
                dr["ztrainer"] = v_item.ztrainer;
                dr["zinstitue"] = v_item.zinstitue;
                dr["ztarget_group"] = v_item.ztarget_group;
                dr["zquantity_course"] = _func_dmu.zFormatfloat(v_item.zquantity_course.ToString(),0);
                dr["ztarget"] = _func_dmu.zFormatfloat(v_item.ztarget.ToString(), 0);
                dr["zregister"] = _func_dmu.zFormatfloat(v_item.zregister.ToString(), 0);
                _persen = 0;
                try
                {
                    _persen = (v_item.zregister / v_item.ztarget) * 100;
                }
                catch { }
                dr["zpersen"] = _func_dmu.zFormatfloat(_persen.ToString(), 2)+"%";
                dr["ztime_length"] = v_item.ztime_length;
                dr["zhh"] = _func_dmu.zFormatfloat(v_item.zhh.ToString(), 2);
                _persen = 0;
                try
                {
                    _persen = v_item.zregister * v_item.zhh;
                }
                catch { }
                dr["zhh_total"] = _func_dmu.zFormatfloat(_persen.ToString(), 2);
                dr["zassessment_tool"] = v_item.zassessment_tool;
                dr["zpass"] = _func_dmu.zFormatfloat(v_item.zpass.ToString(), 2);
                _persen = 0;
                try
                {
                    _persen = (v_item.zpass / v_item.zregister) * 100;
                }
                catch { }
                dr["zpass_persen"] = _func_dmu.zFormatfloat(_persen.ToString(), 2);
                _persen = 0;
                try
                {
                    _persen = (v_item.score_qty / v_item.u10_total_qty) * 100;
                }
                catch { }
                dr["zsatisfaction"] = _func_dmu.zFormatfloat(_persen.ToString(), 2);
                dr["zcost_center"] = v_item.zcost_center;
                dr["zplan_amount"] = _func_dmu.zFormatfloat(v_item.zplan_amount.ToString(), 2);
                dr["zcreate_name"] = v_item.zcreate_name;
                dr["lecturer_type"] = v_item.lecturer_type.ToString();
                ds.Tables["dsreport_training_plan"].Rows.Add(dr);
            }
            ViewState["vsreport_training_plan"] = ds;

        }
        _func_dmu.zSetGridData(Gvreport_training_plan, ViewState["vsreport_training_plan"]);
        

    }
    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {

        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            var GvName = (GridView)sender;
            string sGvName = GvName.ID;
            if (sGvName == "GvHR_AList")
            {

            }

        }
    }
    private void CreateDs_report_training_plan()
    {
        
        string sDs = "dsreport_training_plan";
        string sVs = "vsreport_training_plan";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("course_plan_status", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("ztype", typeof(String));
        ds.Tables[sDs].Columns.Add("zstatus_training_plan", typeof(String));
        ds.Tables[sDs].Columns.Add("zcatergories", typeof(String));
        ds.Tables[sDs].Columns.Add("zyear_plan", typeof(String));
        ds.Tables[sDs].Columns.Add("ztraining_date", typeof(String));
        ds.Tables[sDs].Columns.Add("zcourse_name", typeof(String));
        ds.Tables[sDs].Columns.Add("ztrainer", typeof(String));
        ds.Tables[sDs].Columns.Add("zinstitue", typeof(String));
        ds.Tables[sDs].Columns.Add("ztarget_group", typeof(String));
        ds.Tables[sDs].Columns.Add("zquantity_course", typeof(String));
        ds.Tables[sDs].Columns.Add("ztarget", typeof(String));
        ds.Tables[sDs].Columns.Add("zregister", typeof(String));
        ds.Tables[sDs].Columns.Add("zpersen", typeof(String));
        ds.Tables[sDs].Columns.Add("ztime_length", typeof(String));
        ds.Tables[sDs].Columns.Add("zhh", typeof(String));
        ds.Tables[sDs].Columns.Add("zhh_total", typeof(String));
        ds.Tables[sDs].Columns.Add("zassessment_tool", typeof(String));
        ds.Tables[sDs].Columns.Add("zpass", typeof(String));
        ds.Tables[sDs].Columns.Add("zpass_persen", typeof(String));
        ds.Tables[sDs].Columns.Add("zsatisfaction", typeof(String));
        ds.Tables[sDs].Columns.Add("zcost_center", typeof(String));
        ds.Tables[sDs].Columns.Add("zplan_amount", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_created_by", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_created_at", typeof(String));
        ds.Tables[sDs].Columns.Add("zcreate_name", typeof(String));
        ds.Tables[sDs].Columns.Add("lecturer_type", typeof(String));
        ViewState[sVs] = ds;
						
									  
									  
									  
    }
    private string setInstitution(int id, Repeater rptRepeater)
    {
        data_elearning dataelearning = new data_elearning();
        training_course obj_detail = new training_course();
        dataelearning.el_training_course_action = new training_course[1];
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "U1-FULL";
        dataelearning.el_training_course_action[0] = obj_detail;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        string _string = "";
        int ic = 0;
        if (dataelearning.el_training_course_action != null)
        {
            foreach (var item in dataelearning.el_training_course_action)
            {
                ic++;
                if (ic == 1)
                {
                    _string = item.institution_name;
                }
                else
                {
                    _string = _string + "," + item.institution_name;
                }
            }
        }
        _func_dmu.zSetRepeaterData(rptRepeater, dataelearning.el_training_course_action);
        return _string;

    }
    protected void onRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        string sGvName = GvName.ID;
        if (sGvName == "Gvreport_training_plan")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbu0_training_course_idx = (Label)e.Row.Cells[0].FindControl("lbu0_training_course_idx");
                Label lblecturer_type = (Label)e.Row.Cells[0].FindControl("lblecturer_type");
                Label lbcourse_plan_status = (Label)e.Row.Cells[0].FindControl("lbcourse_plan_status");

                Repeater rpt_institue = (Repeater)e.Row.Cells[8].FindControl("rpt_institue");
                Label txtztrainer_item_L = (Label)e.Row.Cells[8].FindControl("txtztrainer_item_L");
                Repeater rpt_institue1 = (Repeater)e.Row.Cells[9].FindControl("rpt_institue1");
                Label txtzinstitue_item_L = (Label)e.Row.Cells[9].FindControl("txtzinstitue_item_L");
                rpt_institue.Visible = false;
                rpt_institue1.Visible = false;
                txtzinstitue_item_L.Visible = false;
                if (_func_dmu.zStringToInt(lblecturer_type.Text) == 0)
                {
                    rpt_institue.Visible = true;
                    txtzinstitue_item_L.Visible = true;
                    txtztrainer_item_L.Text = setInstitution(_func_dmu.zStringToInt(lbu0_training_course_idx.Text), rpt_institue);
                }
                else
                {
                    rpt_institue1.Visible = true;
                    txtzinstitue_item_L.Text = setInstitution(_func_dmu.zStringToInt(lbu0_training_course_idx.Text), rpt_institue1);
                }
                Repeater rptu8trncoursedate = (Repeater)e.Row.Cells[5].FindControl("rptu8trncoursedate");
                Label txtztraining_date_item_L = (Label)e.Row.Cells[5].FindControl("txtztraining_date_item_L");
                txtztraining_date_item_L.Text = setU8StartEndDatetime(_func_dmu.zStringToInt(lbu0_training_course_idx.Text), rptu8trncoursedate);

                Repeater rpt_objective = (Repeater)e.Row.Cells[5].FindControl("rpt_objective");
                Label txtzassessment_tool_item_L = (Label)e.Row.Cells[5].FindControl("txtzassessment_tool_item_L");
                txtzassessment_tool_item_L.Text = setU2objective(_func_dmu.zStringToInt(lbu0_training_course_idx.Text), rpt_objective);

                Panel pnlicon_inplan = (Panel)e.Row.Cells[1].FindControl("pnlicon_inplan");
                Panel pnlicon_outplan = (Panel)e.Row.Cells[1].FindControl("pnlicon_outplan");

                if (lbcourse_plan_status.Text == "I")
                {
                    pnlicon_inplan.Visible = true;
                    pnlicon_outplan.Visible = false;
                }
                else if (lbcourse_plan_status.Text == "O")
                {
                    pnlicon_inplan.Visible = false;
                    pnlicon_outplan.Visible = true;
                }
                else
                {
                    pnlicon_inplan.Visible = false;
                    pnlicon_outplan.Visible = false;
                }

            }


        }

    }
    private string setU8StartEndDatetime(int id, Repeater rptRepeater)
    {
        data_elearning dataelearning = new data_elearning();
        training_course obj_detail = new training_course();
        dataelearning.el_training_course_action = new training_course[1];
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "U8-FULL";
        dataelearning.el_training_course_action[0] = obj_detail;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        string _string = "";
        int ic = 0;
        if (dataelearning.el_training_course_action != null)
        {
            foreach(var item in dataelearning.el_training_course_action)
            {
                ic++;
                if (ic == 1)
                {
                    _string = setTraining_date(item.zday, item.zmonth, item.zyear);
                }
                else
                {
                    _string = _string+","+ setTraining_date(item.zday, item.zmonth, item.zyear);
                }
            }
        }
        _func_dmu.zSetRepeaterData(rptRepeater, dataelearning.el_training_course_action);
        return _string;
    }
    public string setTraining_date(int iDay, int iMonth, int iYear)
    {
        return iDay.ToString() + "-" + _func_dmu.zMonthEN_Short(iMonth) + "-" + ((iYear + 543)-2500).ToString();
    }
    private string setU2objective(int id, Repeater rptRepeater)
    {
        data_elearning dataelearning = new data_elearning();
        training_course obj_detail = new training_course();
        dataelearning.el_training_course_action = new training_course[1];
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "U2-FULL";
        dataelearning.el_training_course_action[0] = obj_detail;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        CreateDs_el_u2_training_course_objective();
        if (dataelearning.el_training_course_action != null)
        {
            DataSet ds = (DataSet)ViewState["vsel_u2_training_course_objective"];
            DataRow dr;
            int ic = 0, ic1 = 0;
            string _string = "";
            foreach (var v_item in dataelearning.el_training_course_action)
            {
                ic++;
                ic1 = 0;
                _string = ic.ToString()+".";
                dr = ds.Tables["dsel_u2_training_course_objective"].NewRow();
                dr["m0_objective_idx_ref"] = v_item.m0_objective_idx_ref.ToString();
                dr["zName_Detail"] = _string+v_item.objective_name;
                ds.Tables["dsel_u2_training_course_objective"].Rows.Add(dr);
                if (v_item.pass_test_flag == 1)
                {
                    ic1++;
                    _string = ic.ToString() + "."+ ic1.ToString() + ".";
                    dr = ds.Tables["dsel_u2_training_course_objective"].NewRow();
                    dr["zName_Detail"] = _string + "ผ่านการทดสอบ " + _func_dmu.zFormatfloat(v_item.pass_test_per.ToString(), 2) + " % ";
                    ds.Tables["dsel_u2_training_course_objective"].Rows.Add(dr);
                }
                
                if (v_item.hour_training_flag == 1)
                {
                    ic1++;
                    _string = ic.ToString() + "." + ic1.ToString() + ".";
                    dr = ds.Tables["dsel_u2_training_course_objective"].NewRow();
                    dr["zName_Detail"] = _string + "ชั่วโมงเข้าอบรมครบ " + _func_dmu.zFormatfloat(v_item.hour_training_per.ToString(), 2) + " % ";
                    ds.Tables["dsel_u2_training_course_objective"].Rows.Add(dr);
                }
               
                if (v_item.write_report_training_flag == 1)
                {
                    ic1++;
                    _string = ic.ToString() + "." + ic1.ToString() + ".";
                    dr = ds.Tables["dsel_u2_training_course_objective"].NewRow();
                    dr["zName_Detail"] = _string + "เขียนรายงานการอบรม";
                    ds.Tables["dsel_u2_training_course_objective"].Rows.Add(dr);
                }
                
                if (v_item.publish_training_flag == 1)
                {
                    ic1++;
                    _string = ic.ToString() + "." + ic1.ToString() + ".";
                    dr = ds.Tables["dsel_u2_training_course_objective"].NewRow();
                    dr["zName_Detail"] = _string + "เผยแพร่ต่อผู้เกี่ยวข้องในรูปแบบ : " + v_item.publish_training_description;
                    ds.Tables["dsel_u2_training_course_objective"].Rows.Add(dr);
                }
                
                if (v_item.course_lecturer_flag == 1)
                {
                    ic1++;
                    _string = ic.ToString() + "." + ic1.ToString() + ".";
                    dr = ds.Tables["dsel_u2_training_course_objective"].NewRow();
                    dr["zName_Detail"] = _string + "จัดทำหลักสูตรและเป็นวิทยากรภายใน";
                    ds.Tables["dsel_u2_training_course_objective"].Rows.Add(dr);
                }
                if (v_item.other_flag == 1)
                {
                    ic1++;
                    _string = ic.ToString() + "." + ic1.ToString() + ".";
                    dr = ds.Tables["dsel_u2_training_course_objective"].NewRow();
                    dr["zName_Detail"] = _string + "อื่นๆ ระบุ : " + v_item.other_description;
                    ds.Tables["dsel_u2_training_course_objective"].Rows.Add(dr);
                }
                if (v_item.hrd_nofollow_flag == 1)
                {
                    ic1++;
                    _string = ic.ToString() + "." + ic1.ToString() + ".";
                    dr = ds.Tables["dsel_u2_training_course_objective"].NewRow();
                    dr["zName_Detail"] = _string + "ไม่ติดตามผล";
                    ds.Tables["dsel_u2_training_course_objective"].Rows.Add(dr);
                }
                if (v_item.hrd_follow_flag == 1)
                {
                    ic1++;
                    _string = ic.ToString() + "." + ic1.ToString() + ".";
                    dr = ds.Tables["dsel_u2_training_course_objective"].NewRow();
                    dr["zName_Detail"] = _string + "ติดตามภายใน " + v_item.hrd_follow_day + " วัน ";
                    ds.Tables["dsel_u2_training_course_objective"].Rows.Add(dr);
                }
                
            }
            ViewState["vsel_u2_training_course_objective"] = ds;
        }
        string _string1 = "";
        int ic2 = 0;
        DataSet dsU1 = (DataSet)ViewState["vsel_u2_training_course_objective"];
        foreach (DataRow item in dsU1.Tables["dsel_u2_training_course_objective"].Rows)
        {
            ic2++;
            if (ic2 == 1)
            {
                _string1 = item["zName_Detail"].ToString();
            }
            else
            {
                _string1 = _string1 + "," + item["zName_Detail"].ToString();
            }

        }
        //  _func_dmu.zSetGridData(_Gvobjective, ViewState["vsel_u2_training_course_objective"]);
        _func_dmu.zSetRepeaterData(rptRepeater, ViewState["vsel_u2_training_course_objective"]);
        return _string1;

    }
    public Boolean getValue(string id = "0")
    {
        Boolean rBoolean = false;

        if (id == "1")
        {
            rBoolean = true;
        }

        return rBoolean;
    }
    private void CreateDs_el_u2_training_course_objective()
    {
        string sDs = "dsel_u2_training_course_objective";
        string sVs = "vsel_u2_training_course_objective";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u2_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("m0_objective_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ds.Tables[sDs].Columns.Add("zName_Title", typeof(String));
        ds.Tables[sDs].Columns.Add("zName_Detail", typeof(String));
        ds.Tables[sDs].Columns.Add("pass_test_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("pass_test_per", typeof(String));
        ds.Tables[sDs].Columns.Add("hour_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hour_training_per", typeof(String));
        ds.Tables[sDs].Columns.Add("write_report_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("publish_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("publish_training_description", typeof(String));
        ds.Tables[sDs].Columns.Add("course_lecturer_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("other_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("other_description", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_nofollow_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_follow_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_follow_day", typeof(String));
        ViewState[sVs] = ds;

    }
    private void ExportGridToExcel(GridView _gv, string sfilename = "ExportToExcel", string ATitle = "Export To Excel")
    {
        if (_gv.Rows.Count == 0)
        {
            return;
        }
        //_gv.AllowPaging = false;
        //_gv.DataBind();
        var totalCols = _gv.Rows[0].Cells.Count;
        var totalRows = _gv.Rows.Count;
        var headerRow = _gv.HeaderRow;

        DataTable tableMaterialLog = new DataTable();

        for (var i = 1; i <= totalCols; i++)
        {
            tableMaterialLog.Columns.Add(headerRow.Cells[i - 1].Text, typeof(String));
        }
        for (var j = 1; j <= totalRows; j++)
        {
            DataRow addMaterialLogRow = tableMaterialLog.NewRow();
            for (var i = 1; i <= totalCols; i++)
            {
                string sdata = ((Label)_gv.Rows[j - 1].Cells[i - 1].Controls[1]).Text;
                addMaterialLogRow[i - 1] = sdata;
            }
            tableMaterialLog.Rows.Add(addMaterialLogRow);
        }
        GridView gv = new GridView();
        gv.DataSource = tableMaterialLog;
        gv.DataBind();
        WriteExcelWithNPOI(tableMaterialLog, "xls", sfilename, ATitle);

    }

    public void WriteExcelWithNPOI(DataTable dt, String extension, String fileName, string ATitle)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");

        IRow row1 = sheet1.CreateRow(0);

        ICell cellTitle = row1.CreateCell(0);
        String columnNameTitle = ATitle;
        cellTitle.SetCellValue(columnNameTitle);


        row1 = sheet1.CreateRow(1);
        cellTitle = row1.CreateCell(0);
        columnNameTitle = "Print Date : " + DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
        cellTitle.SetCellValue(columnNameTitle);

        row1 = sheet1.CreateRow(3);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 4);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }


}