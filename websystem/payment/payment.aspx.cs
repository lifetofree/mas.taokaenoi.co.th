using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_payment_payment : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _data_employee = new data_employee();
    data_sap_attach _data_sap_attach = new data_sap_attach();

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetViewEmployeeListSmall = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewEmployeeListSmall"];

    static string _urlSapAttachGetFileData = _serviceUrl + ConfigurationManager.AppSettings["urlSapAttachGetFileData"];
    static string _urlSapAttachSetFileData = _serviceUrl + ConfigurationManager.AppSettings["urlSapAttachSetFileData"];

    static string _path_file_sap_attach = ConfigurationManager.AppSettings["path_file_sap_load_file"];
    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = _funcTool.convertToInt(Session["emp_idx"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}