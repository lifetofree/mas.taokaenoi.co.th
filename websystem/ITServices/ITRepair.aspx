﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ITRepair.aspx.cs" Inherits="websystem_ITServices_ITRepair" Debug="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">


    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImagesSAP").MultiFile();
            }
        })
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImagesComment").MultiFile();
            }
        })
    </script>


    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImagesClosejob").MultiFile();
            }
        })
    </script>


    <script type="text/javascript" src='<%=ResolveUrl("~/Scripts/jquery.MultiFile.js")%>'></script>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>


    <asp:Literal ID="text" runat="server"></asp:Literal>


    <%----------- Menu Tab Start---------------%>

    <div id="BoxTabMenuIndex" runat="server">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav ulActiveMenu" runat="server">
                    <li id="IndexList" runat="server">
                        <asp:LinkButton ID="lbindex" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand">ข้อมูลทั่วไป</asp:LinkButton>
                    </li>
                    <li id="Li1" runat="server">
                        <asp:LinkButton ID="lbalterrepair" CssClass="btn_menulist" runat="server" CommandName="btnalterrepair" OnCommand="btnCommand">แจ้งซ่อม</asp:LinkButton>
                    </li>
                    <li id="Li5" runat="server">
                        <asp:LinkButton ID="lbReportPOS" CssClass="btn_menulist" runat="server" CommandName="btnReportPOS" OnCommand="btnCommand">สรุปรายงาน</asp:LinkButton>
                    </li>
                    <li id="Li4" runat="server">
                        <asp:LinkButton ID="lbReportQuestIT" CssClass="btn_menulist" runat="server" CommandName="lbReportQuestIT" OnCommand="btnCommand">สรุปรายงานแบบสอบถาม</asp:LinkButton>
                    </li>
                </ul>
            </div>
        </nav>
    </div>


    <%-------------- Menu Tab End--------------%>




    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">


        <%-------- Index Start----------%>
        <asp:View ID="ViewIndex" runat="server">

            <%-------------- BoxSearch Start--------------%>
            <div class="col-lg-12" runat="server" id="BoxSearch">
                <div id="BoxButtonSearchShow" runat="server">
                    <asp:LinkButton ID="BtnHideSETBoxAllSearchShow" CssClass="btn btn-info" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchShow" OnCommand="btnCommand"><i class="fa fa-search"></i> ShowSearch</asp:LinkButton>
                </div>

                <div id="BoxButtonSearchHide" runat="server">
                    <asp:LinkButton ID="BtnHideSETBoxAllSearchHide" CssClass="btn btn-info" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchHide" OnCommand="btnCommand"><i class="fa fa-search"></i> HideSearch</asp:LinkButton>
                </div>
                <br />
                <div id="SETBoxAllSearch" runat="server">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">

                                    <%------------ Search Start----------------%>

                                    <div id="SETBoxAllSearch2" runat="server">

                                        <div id="ViewSearchUser" runat="server">
                                            <div class="form-group">
                                                <asp:Label ID="Label13" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากวันที่" />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlTypeSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Text="เลือกประเภทการค้นหา ...." Value="00"></asp:ListItem>
                                                        <asp:ListItem Text="วันที่รับงาน" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="วันที่ปิดงาน" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="วันที่แจ้งงาน" Value="3"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                            </div>

                                            <div id="Div1" runat="server">
                                                <div class="form-group">
                                                    <asp:Label ID="AddStart" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ค้นหา" />
                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Text="เลือกเงื่อนไข ...." Value="00"></asp:ListItem>
                                                            <asp:ListItem Text="มากกว่า >" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="น้อยกว่า <" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="ระหว่าง <>" Value="3"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-sm-3 ">

                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="AddEndDate" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group" id="BoxSearchName" runat="server" visible="False">
                                            <asp:Label ID="Label1" CssClass="control-label col-sm-2" runat="server" Text="ชื่อ - นามสกุล" />
                                            <div class="col-sm-3">
                                                <style>
                                                    .btn {
                                                        g-top: 7px pading-b t;
                                                </style>
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                        <asp:DropDownList runat="server" ID="ddlIFTHEN" AutoPostBack="true" CssClass="btn btn-default" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Text="TH" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="EN" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <!-- /btn-group -->
                                                    <div id="BoxSearchNameTH" runat="server">
                                                        <asp:TextBox ID="txtSearchName" CssClass="form-control" runat="server" placeholder="ชื่อ ....."></asp:TextBox>
                                                    </div>
                                                    <div id="BoxSearchNameEN" runat="server">
                                                        <asp:TextBox ID="txtSearchNameEN" CssClass="form-control" runat="server" placeholder="ชื่อ ....."></asp:TextBox>
                                                    </div>
                                                </div>
                                                <!-- /input-group -->
                                            </div>


                                            <asp:Label ID="Label8" CssClass="control-label col-sm-2" runat="server" Text="นามสกุล" />
                                            <div class="col-sm-3">
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                        <asp:DropDownList runat="server" ID="ddlSurIFTHEN" AutoPostBack="true" CssClass="btn btn-default" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Text="TH" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="EN" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <!-- /btn-group -->
                                                    <div id="BoxSearchLastTH" runat="server">
                                                        <asp:TextBox ID="txtSearchSurname" CssClass="form-control" runat="server" placeholder="นามสกุล ....."></asp:TextBox>
                                                    </div>
                                                    <div id="BoxSearchLastEN" runat="server">
                                                        <asp:TextBox ID="txtSearchSurnameEN" CssClass="form-control" runat="server" placeholder="นามสกุล ....."></asp:TextBox>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="ระบบ" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddSystemSearch" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" AutoPostBack="true"></asp:DropDownList>

                                            </div>
                                            <asp:RequiredFieldValidator ID="RqddSystemSearch" ValidationGroup="btnsearch" runat="server" Display="None"
                                                ControlToValidate="ddSystemSearch" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกระบบ"
                                                ValidationExpression="กรุณาเลือกระบบ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqddSystemSearch" Width="160" />

                                            <div id="BoxSearchStatus" runat="server">
                                                <asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server" Text="สถานะการดำเนินการ" />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlSearchStatus" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="88" Text="กรุณาเลือกสถานะดำเนินการ ..." />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div id="ViewSearchReport" runat="server">

                                        <div class="form-group">
                                            <asp:Label ID="Label67" CssClass="col-sm-2 control-label" runat="server" Text="Organization" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSearchOrg" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>

                                            <asp:Label ID="Label66" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากฝ่าย" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSearchDep" AutoPostBack="true" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกฝ่าย....</asp:ListItem>

                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group" runat="server">
                                            <asp:Label ID="Label68" CssClass="col-sm-2 control-label" runat="server" Text="รหัสพนักงาน/ชื่อพนักงาน" />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtAdminEmpIDX" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน ....."></asp:TextBox>
                                            </div>
                                            <asp:Label ID="Label109" CssClass="col-sm-2 control-label" runat="server" Text="รหัสเอกสาร" />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtDocCode" runat="server" CssClass="form-control" placeholder="รหัสเอกสาร ....."></asp:TextBox>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-5 col-sm-offset-1">
                                            <asp:LinkButton ID="btnsearch" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="btnRefresh" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="BtnBack" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                        </div>

                                    </div>
                                </div>


                                <%------------- Search End-----------------%>

                                <%------------ ViewIndex Start----------------%>
                            </div>

                        </div>
                    </div>

                </div>

            </div>

            <%-------------- BoxSearch End--------------%>

            <div id="BoxGridIndex" runat="server" visible="True">

                <%-- View Status --%>

                <div id="panel_status" runat="server">

                    <div class="col-lg-12">

                        <div class="form-group">

                            <div class="col-lg-12">

                                <div class="col-lg-4">
                                    <asp:LinkButton ID="lblstatewaitsap1" runat="server" CssClass="btn btn-danger btn-sm" Text="SAP " OnCommand="btnCommand" CommandArgument="1" CommandName="btnsapstatus" title="ยังไม่รับงาน" />

                                    <asp:LinkButton ID="lblstatewaitsap2" runat="server" CssClass="btn btn-warning btn-sm" Text="SAP " OnCommand="btnCommand" CommandArgument="2" CommandName="btnsapstatus" title="กำลังดำเนินการ"></asp:LinkButton>
                                                                        <asp:LinkButton ID="lblstatewaituserrechecksap" runat="server" CssClass="btn btn-success btn-sm" Text="SAP " OnCommand="btnCommand" CommandArgument="47" CommandName="btnsapstatus" title="รอUser Comment ตอบกลับ"></asp:LinkButton>

                                    <asp:LinkButton ID="lblstatedwaitusersap3" runat="server" CssClass="btn btn-primary btn-sm" Text="SAP " OnCommand="btnCommand" CommandArgument="3" CommandName="btnsapstatus" title="ผู้ใช้ตรวจสอบ"></asp:LinkButton>


                                </div>
                                <div class="col-lg-4">
                                    <asp:LinkButton ID="lblstatewaitit" runat="server" CssClass="btn btn-danger btn-sm" Text="IT " OnCommand="btnCommand" CommandArgument="20" CommandName="btnitstatus" title="ยังไม่รับงาน" />

                                    <asp:LinkButton ID="lblstatedoingit" runat="server" CssClass="btn btn-warning btn-sm" Text="IT" OnCommand="btnCommand" CommandArgument="21,43,44,45,46" CommandName="btnitstatus" title="กำลังดำเนินการ"></asp:LinkButton>

                                    <asp:LinkButton ID="lblstatewaituserit" runat="server" CssClass="btn btn-primary btn-sm" Text="IT" OnCommand="btnCommand" CommandArgument="22" CommandName="btnitstatus" title="ผู้ใช้ตรวจสอบ"></asp:LinkButton>

                                </div>
                                <div class="col-lg-4">
                                    <asp:LinkButton ID="lblstatewaitgm" runat="server" CssClass="btn btn-danger btn-sm" Text="GM " OnCommand="btnCommand" CommandArgument="7" CommandName="btngmstatus" title="ยังไม่รับงาน" />

                                    <asp:LinkButton ID="lblstatedoinggm" runat="server" CssClass="btn btn-warning btn-sm" Text="GM " OnCommand="btnCommand" CommandArgument="8" CommandName="btngmstatus" title="กำลังดำเนินการ"></asp:LinkButton>

                                    <asp:LinkButton ID="lblstatewaitusergm" runat="server" CssClass="btn btn-primary btn-sm" Text="GM " OnCommand="btnCommand" CommandArgument="9" CommandName="btngmstatus" title="ผู้ใช้ตรวจสอบ"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <div class="col-lg-12">


                                <div class="col-lg-4">
                                    <asp:LinkButton ID="lblstatewaitpos" runat="server" CssClass="btn btn-danger btn-sm" Text="POS " OnCommand="btnCommand" CommandArgument="26" CommandName="btnposstatus" title="ยังไม่รับงาน" />

                                    <asp:LinkButton ID="lblstatedoingpos" runat="server" CssClass="btn btn-warning btn-sm" Text="POS " OnCommand="btnCommand" CommandArgument="27" CommandName="btnposstatus" title="กำลังดำเนินการ"></asp:LinkButton>
                                    <asp:LinkButton ID="lblstatewaituserrecheckpos" runat="server" CssClass="btn btn-success btn-sm" Text="POS " OnCommand="btnCommand" CommandArgument="60" CommandName="btnposstatus" title="รอผู้ให้บริการภายนอกตรวจสอบ"></asp:LinkButton>

                                    <asp:LinkButton ID="lblstatewaituserpos" runat="server" CssClass="btn btn-primary btn-sm" Text="POS " OnCommand="btnCommand" CommandArgument="28" CommandName="btnposstatus" title="ผู้ใช้ตรวจสอบ"></asp:LinkButton>
                                </div>
                                <div class="col-lg-4">
                                    <asp:LinkButton ID="lblstatewaitbi" runat="server" CssClass="btn btn-danger btn-sm" Text="BI " OnCommand="btnCommand" CommandArgument="1" CommandName="btnbistatus" title="ยังไม่รับงาน" />

                                    <asp:LinkButton ID="lblstatedoingbi" runat="server" CssClass="btn btn-warning btn-sm" Text="BI " OnCommand="btnCommand" CommandArgument="2" CommandName="btnbistatus" title="กำลังดำเนินการ"></asp:LinkButton>

                                    <asp:LinkButton ID="lblstatewaituserbi" runat="server" CssClass="btn btn-primary btn-sm" Text="BI " OnCommand="btnCommand" CommandArgument="3" CommandName="btnbistatus" title="ผู้ใช้ตรวจสอบ"></asp:LinkButton>

                                </div>
                                <div class="col-lg-4">
                                    <asp:LinkButton ID="lblstatewaitbp" runat="server" CssClass="btn btn-danger btn-sm" Text="BP " OnCommand="btnCommand" CommandArgument="1" CommandName="btnbpstatus" title="ยังไม่รับงาน" />

                                    <asp:LinkButton ID="lblstatedoingbp" runat="server" CssClass="btn btn-warning btn-sm" Text="BP" OnCommand="btnCommand" CommandArgument="2" CommandName="btnbpstatus" title="กำลังดำเนินการ"></asp:LinkButton>

                                    <asp:LinkButton ID="lblstatewaituserbp" runat="server" CssClass="btn btn-primary btn-sm" Text="BP" OnCommand="btnCommand" CommandArgument="3" CommandName="btnbpstatus" title="ผู้ใช้ตรวจสอบ"></asp:LinkButton>

                                </div>
                            </div>
                        </div>

                        <br />

                    </div>

                </div>

                <div id="div_res" runat="server" visible="false">

                    <div class="col-lg-12">

                        <div class="form-group">

                            <div class="col-lg-12">

                                <div class="col-lg-4">
                                    <asp:LinkButton ID="lblstatewaitres" runat="server" CssClass="btn btn-danger btn-sm" Text="RES " OnCommand="btnCommand" CommandArgument="48" CommandName="btnresstatus" title="ยังไม่รับงาน" />
                                    <asp:LinkButton ID="lblstatedoingres" runat="server" CssClass="btn btn-warning btn-sm" Text="RES " OnCommand="btnCommand" CommandArgument="49" CommandName="btnresstatus" title="กำลังดำเนินการ"></asp:LinkButton>
                                    <asp:LinkButton ID="lblstatewaituserres" runat="server" CssClass="btn btn-primary btn-sm" Text="RES " OnCommand="btnCommand" CommandArgument="50" CommandName="btnresstatus" title="ผู้ใช้ตรวจสอบ"></asp:LinkButton>

                                </div>

                            </div>
                        </div>
                        <br />

                    </div>

                </div>


                <div id="gridviewindex" runat="server">
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <asp:UpdatePanel ID="upActor1Node1Files" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="GvViewITRepair"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        DataKeyNames="URQIDX"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                        HeaderStyle-CssClass="info"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="true"
                                        PageSize="10"
                                        OnPageIndexChanging="Master_PageIndexChanging"
                                        OnRowDataBound="Master_RowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center;">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>

                                        <Columns>

                                            <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("Doccode") %>' /></small>
                                                    <asp:Literal ID="Literal10" Visible="false" runat="server" Text='<%# Eval("URQIDX") %>' /></small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="ระบบ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Literal ID="ltSys" Visible="false" runat="server" Text='<%# Eval("SysIDX") %>' /></small>
                                                    <asp:Literal ID="lblnode_state" Visible="false" runat="server" Text='<%# Eval("node_status") %>' /></small>

                                            <asp:Literal ID="Literal9" Visible="false" runat="server" Text='<%# Eval("unidx") %>' /></small>

                                            <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("SysName") %>' /></small>
                                             
                                              <asp:Literal ID="lblEmpIDX" Visible="false" runat="server" Text='<%# Eval("EmpIDX") %>' /></small>
                                             <asp:Literal ID="lblNCEmpIDX" Visible="false" runat="server" Text='<%# Eval("NCEmpIDX") %>' /></small>
                                            <asp:Literal ID="DoccodeSys" runat="server" Text='<%# Eval("Doccode") %>' Visible="False" /></small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="วันเดือนปี" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Literal ID="ltSystemName" runat="server" Text='<%# Eval("CreateDateUser") %>' /></small>
                                                    </p>
                                        <small>(
                                        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("TimeCreateJob") %>' />
                                            )</small>
                                                </ItemTemplate>

                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="ผู้แจ้งปัญหา" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("EmpName") %>' />

                                                        <asp:Label ID="ITRepairEmpIDX" runat="server" Visible="false" CssClass="col-sm-10" Text='<%# Eval("EmpIDX") %>'></asp:Label>
                                                        </p>

                                            <strong>
                                                <asp:Label ID="Label15" runat="server">สถานที่แจ้ง: </asp:Label></strong>
                                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("LocName") %>' />
                                                        </p>
                                            <strong>
                                                <asp:Label ID="Label13" runat="server">ฝ่าย: </asp:Label></strong>
                                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("RDeptName") %>' />
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="ปัญหาที่แจ้ง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" ItemStyle-Wrap="True">

                                                <ItemTemplate>
                                                    <small>

                                                        <strong>
                                                            <asp:Label ID="Label17" runat="server"> รายละเอียด: </asp:Label></strong>
                                                        <asp:Label ID="DetailProblem" runat="server" Style="break-word;" Text='<%# Eval("detailUser") %>' /></p>

                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="ผู้รับงาน" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <small>
                                                        <strong>
                                                            <asp:Label ID="Label60" runat="server"> ผู้รับงาน: </asp:Label></strong>
                                                        <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("AdminName") %>' />
                                                        <br />
                                                        <strong>
                                                            <asp:Label ID="Label61" runat="server"> ผู้ปิดงาน: </asp:Label></strong>
                                                        <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("AdminDoingName") %>' />
                                                        <br />
                                                        <asp:Panel ID="BoxDate" runat="server">
                                                            <strong>
                                                                <asp:Label ID="lbdat" runat="server"> วันที่: </asp:Label></strong>
                                                            <asp:Literal ID="lbdaterecive" runat="server" Text='<%# Eval("DatecloseJob") %>' />
                                                            <br />
                                                            <strong>
                                                                <asp:Label ID="lbb1" runat="server"> เวลาปิดงาน: </asp:Label></strong>
                                                            <asp:Literal ID="lbtimerecive" runat="server" Text='<%# Eval("TimecloseJob") %>' />
                                                            <br />
                                                        </asp:Panel>
                                                        <strong>
                                                            <asp:Label ID="Label51" runat="server"> หมายเหตุ: </asp:Label></strong>
                                                        <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("CommentAMDoing") %>' />
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="สถานะ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="txtIDSta" runat="server" Visible="false" Text='<%# Eval("StaIDX")%>'></asp:Label>
                                                        <asp:Label ID="Label19" runat="server" Visible="False" Text='<%# Eval("MS1IDX")%>'></asp:Label>
                                                        <asp:Label ID="lblAdminDoingIDX" runat="server" Visible="False" Text='<%# Eval("AdminDoingIDX")%>'></asp:Label>

                                                        <asp:Label ID="ltCCAstatus" runat="server" Text='<%# (string)Eval("StaName") %>'></asp:Label>

                                                        <%--แบบสอบถาม IT--%>
                                                        <asp:Label ID="lblitfeedsta" runat="server" Visible="False" Text='<%# (string)Eval("Statusans") %>'></asp:Label>

                                                        <%--Status CommentSap--%>
                                                        <asp:Label ID="lbStatusComment" runat="server" Visible="False" Text='<%#Eval("CStatus") %>'></asp:Label>


                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel ID="updatebtn" runat="server" UpdateMode="Always">
                                                        <ContentTemplate>
                                                            <asp:LinkButton ID="lbVeiw1" runat="server" CssClass="btn btn-info btn-sm" Text="" OnCommand="btnCommand" CommandName="ViewITRepair" title="View" CommandArgument='<%# Eval("URQIDX") + ";" + Eval("SysIDX") + ";" + Eval("StaIDX") + ";" + Eval("EmpIDX") + ";" +  Eval("NCEmpIDX") + ";" + Eval("Doccode")+ ";" +  Eval("node_status")%>'><i class="fa fa-file-text" ></i> </asp:LinkButton>

                                                            <%--แบบสอบถาม IT--%>
                                                            <asp:LinkButton ID="lbQuestion1" runat="server" CssClass="btn btn-warning btn-sm" Text="" OnCommand="btnCommand" CommandName="ITRepairQuestion" title="FeedBack" CommandArgument='<%# Eval("URQIDX") + ";" + Eval("STAIDX")%>' PostBackUrl='<%# Questionrepair((int)Eval("URQIDX"),(int)Eval("unidx"),(int)Eval("node_status"),(int)Eval("SysIDX")) %>'><i class="fa fa-user" ></i> </asp:LinkButton>

                                                            <%--CommentSap  Visible='<%#int.Parse(Eval("CStatus").ToString()) == 1 ? false:true %>' --%>
                                                            <div style="text-align: center; padding-top: 10px;">
                                                                <asp:Label ID="StatusComment" runat="server" Visible="false" class='<%# Eval("CStatus").ToString() == "1" ? "fa fa-check" : "fa fa-times" %>'></asp:Label>
                                                            </div>
                                                            <div style="text-align: center; padding-top: 10px;">

                                                                <asp:Label ID="StatusClosejob" runat="server" Visible="false" class='<%# Eval("AdminDoingIDX").ToString() == "0" ? "glyphicon glyphicon-exclamation-sign" : " " %>'></asp:Label>

                                                                <%-- <asp:Label ID="StatusClosejobIT" runat="server" Visible="false" class='<%# Eval("CIT1IDX").ToString() == "0" ? "glyphicon glyphicon-exclamation-sign" : " " %>'></asp:Label>--%>
                                                            </div>

                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="lbVeiw1" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>

                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                    </div>
                </div>

                <div id="div_showdata" runat="server" visible="false">

                    <div class="form-horizontal" role="form">

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้แจ้งซ่อม</strong></h3>
                            </div>
                            <div class="panel-body">


                                <asp:FormView ID="FvDetailUserRepair" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <ItemTemplate>
                                        <%-- <div class="form-horizontal" role="form">--%>

                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสผู้ทำรายการ : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbEmpCode" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("EmpCode") %>' />
                                                    <asp:Label ID="LbEmpIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("EmpIDX")%>' />

                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbEmpName" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("EmpName") %>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbOrgNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("OrgNameTH")%>' />
                                                    <asp:Label ID="LbOrgIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("OrgIDX")%>' />
                                                </div>

                                                <asp:Label ID="Label34" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbDeptNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("RDeptName")%>' />
                                                    <asp:Label ID="LbRDeptIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RDeptID")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- แผนก,ตำแหน่ง --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Labedl1" class="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label23" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                    <asp:Label ID="LbSecIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RSecID")%>' />

                                                </div>

                                                <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbPosNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("PosNameTH")%>' />
                                                    <asp:Label ID="LbPosIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RPosIDX_J")%>' />
                                                </div>
                                            </div>
                                        </div>

                                        <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label5" class="col-xs-2 control-labelnotop text_right" runat="server" Text="เบอร์ติดต่อ : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label25" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("MobileNo") %>' />
                                                </div>

                                                <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lblemail" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("Email") %>' />

                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- Cost Center --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label20" class="col-xs-2 control-labelnotop text_right" runat="server" Text="Cost Center : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LotusCost" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("CostNo")%>' />
                                                    <asp:Label ID="LoCostIDX" runat="server" Visible="false" CssClass="control-label" Text='<%# Eval("CostIDX")%>' />
                                                </div>
                                            </div>
                                        </div>
                                        <%-- </div>--%>
                                    </ItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-wrench"></i><strong>&nbsp; ข้อมูลแจ้งซ่อม</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <asp:FormView ID="FvDetailRepair" DefaultMode="ReadOnly" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                                    <ItemTemplate>

                                        <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("unidx") %>' />
                                        <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("acidx") %>' />
                                        <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />
                                        <div class="panel-body">
                                            <div class="form-horizontal" role="form">

                                                <%-------------- ลำดับความสำคัญ --------------%>
                                                <div class="form-group">
                                                    <div class="col-md-12">

                                                        <asp:Label ID="Label35" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสเอกสาร : " />
                                                        <div class="col-xs-3">
                                                            <asp:Label ID="DocCodeSap" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("DocCode") %>' />
                                                            <asp:Label ID="lblsystem" runat="server" Visible="false" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("SysName")%>' />
                                                            <asp:Label ID="lblorgidx" runat="server" Visible="false" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("OrgIDX")%>' />
                                                            <asp:Label ID="lblsysidx" runat="server" Visible="false" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("SysIDX")%>' />

                                                        </div>
                                                        <div id="prioritysap" runat="server" visible="false">
                                                            <asp:Label ID="Label31" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ลำดับความสำคัญ : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="LbPIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("PIDX")%>' />
                                                                <asp:Label ID="AdminGetJobSapIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("AdminIDX")%>' />

                                                                <asp:Label ID="Label32" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Priority_name")%>' />
                                                            </div>


                                                        </div>
                                                    </div>

                                                </div>


                                                <%-------------- วันที่แจ้งซ่อม,เวลา --------------%>
                                                <div class="form-group">
                                                    <div class="col-md-12">

                                                        <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่แจ้งซ่อม : " />
                                                        <div class="col-xs-3">
                                                            <asp:Label ID="LbDategetJob" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("CreateDateUser") %>' />

                                                        </div>

                                                        <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                                        <div class="col-xs-4">
                                                            <asp:Label ID="LbTimegetJob" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("TimeCreateJob") %>' />
                                                        </div>

                                                    </div>
                                                </div>

                                                <%-------------- User Log on Name,เวลา --------------%>
                                                <div id="div_tranandtcode" runat="server" visible=" false">
                                                    <div class="form-group">
                                                        <div class="col-md-12">

                                                            <asp:Label ID="Label21" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="User Log on Name : " />
                                                            <div class="col-xs-3">
                                                                <asp:Label ID="Label22" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("UserLogonName") %>' />

                                                            </div>

                                                            <asp:Label ID="lbltcode" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="T-Code/หน้าจอใช้งาน : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="Label27" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("TransactionCode") %>' />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="form-group" id="divdevices_res" runat="server">
                                                    <div class="col-md-12">

                                                        <asp:Label ID="Label81" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="อุปกรณ์ : " />
                                                        <div class="col-xs-3">
                                                            <asp:Label ID="Label90" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("res_devicesidx")%>' />
                                                            <asp:Label ID="Label91" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("devices_name")%>' />
                                                        </div>
                                                        <asp:Label ID="Label96" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ช่างดำเนินการ : " />
                                                        <div class="col-xs-4">
                                                            <asp:Label ID="Label98" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("enidx")%>' />
                                                            <asp:Label ID="Label97" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("en_name") %>' />
                                                        </div>
                                                    </div>
                                                </div>

                                                <%-------------- สถานที่การแจ้งซ่อม --------------%>
                                                <div class="form-group">
                                                    <div class="col-md-12">

                                                        <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานที่การแจ้งซ่อม : " />
                                                        <div class="col-xs-3">
                                                            <asp:Label ID="LbLocIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("LocIDX")%>' />
                                                            <asp:Label ID="LbLockTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("LocName")%>' />
                                                        </div>

                                                        <asp:Label ID="Label28" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="หมายเหตุการแจ้ง : " />
                                                        <div class="col-xs-4">
                                                            <asp:Label ID="Lbldetailuser" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("detailUser") %>' />
                                                        </div>
                                                    </div>
                                                </div>


                                                <div id="divRemote" runat="server">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <div id="id_holderit" runat="server" visible="false">
                                                                <asp:Label ID="Label75" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เครื่องผู้ถือครอง : " />
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="Label76" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("u0_code")%>' />
                                                                </div>
                                                            </div>
                                                            <asp:Label ID="Label88" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="โปรแกรม Remote ที่ใช้ : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="Label89" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("RemoteName")%>' />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-12">

                                                            <asp:Label ID="Label74" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Your ID(โปรแกรมรีโมท): " />
                                                            <div class="col-xs-3">
                                                                <asp:Label ID="Label79" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("UserIDRemote")%>' />
                                                            </div>

                                                            <asp:Label ID="Label80" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Password : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="Label82" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("PasswordRemote") %>' />
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div id="divothername" runat="server">
                                                    <div class="form-group">
                                                        <div class="col-md-12">

                                                            <asp:Label ID="Label29" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แจ้งแทน : " />
                                                            <div class="col-xs-3">
                                                                <asp:Label ID="Label64" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("NCEmpIDX")%>' />

                                                                <asp:Label ID="Label53" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("OtherName")%>' />
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>

                                                <br />

                                                <div class="form-group">
                                                    <asp:Label ID="Label17" CssClass="col-sm-1 control-label" runat="server" />

                                                    <div class="col-lg-8">
                                                        <asp:GridView ID="gvFileSAP" Visible="true" runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover table-responsive"
                                                            HeaderStyle-CssClass="warning"
                                                            OnRowDataBound="Master_RowDataBound"
                                                            BorderStyle="None"
                                                            CellSpacing="2"
                                                            Font-Size="Small">

                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                                    <ItemTemplate>
                                                                        <div class="col-lg-10">
                                                                            <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                        </div>
                                                                        <div class="col-lg-2">
                                                                            <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                            <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                        </div>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                        <h4></h4>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>

                    </div>


                    <div id="Div_Comment" runat="server">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; รายละเอียดคอมเม้นต์ / Upload File</strong></h3>
                            </div>
                            <div class="panel-body">

                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>

                                        <asp:GridView ID="GvComment"
                                            runat="server"
                                            AutoGenerateColumns="false"
                                            DataKeyNames="CMIDX"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                            HeaderStyle-CssClass="default"
                                            HeaderStyle-Height="40px"
                                            AllowPaging="true"
                                            PageSize="100"
                                            OnRowEditing="Master_RowEditing"
                                            OnRowCancelingEdit="Master_RowCancelingEdit"
                                            OnRowDataBound="Master_RowDataBound"
                                            OnRowUpdating="Master_RowUpdating">

                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />


                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>

                                            <Columns>
                                                <asp:TemplateField HeaderText="#">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUIDX" runat="server" Visible="false" Text='<%# Eval("CMIDX") %>' />
                                                        <%# (Container.DataItemIndex +1) %>
                                                    </ItemTemplate>

                                                    <EditItemTemplate>

                                                        <div class="panel-body">
                                                            <div class="form-horizontal" role="form">

                                                                <div class="form-group">
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txtUIDX" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("CMIDX")%>' />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <asp:Label ID="Label95" Visible="true" CssClass="col-sm-3 control-label" runat="server" Text="Name" />
                                                                    <div class="col-sm-5">
                                                                        <asp:TextBox ID="txtName" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("FullNameTH")%>' />
                                                                        <asp:TextBox ID="txtEmpIDX" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("EmpIDX")%>' />

                                                                        <%--<asp:TextBox ID="txtNameUpdate" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("FullNameTH")%>' />
                                                                                    <asp:TextBox ID="txtEmpIDXUpdate" runat="server" Visible="true" CssClass="form-control" Text='<%# Eval("EmpIDX")%>' />   Enabled="false"--%>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save_comment" runat="server" Display="None"
                                                                        ControlToValidate="txtEmpIDX" Font-Size="11"
                                                                        ErrorMessage="Please check Name"
                                                                        ValidationExpression="Please check Name" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                                </div>

                                                                <div class="form-group">
                                                                    <asp:Label ID="lbNameTH" CssClass="col-sm-3 control-label" runat="server" Text="Comment" />
                                                                    <div class="col-sm-5">
                                                                        <asp:TextBox ID="txtNameTypecodeEdit" runat="server" CssClass="form-control" Text='<%# Eval("CommentAuto")%>' />
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="RqNameTH" ValidationGroup="Save_comment" runat="server" Display="None"
                                                                        ControlToValidate="txtNameTypecodeEdit" Font-Size="11"
                                                                        ErrorMessage="Please check NameTypecode"
                                                                        ValidationExpression="Please check NameTypecode" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqNameTH" Width="160" />
                                                                </div>


                                                                <div class="form-group">
                                                                    <div class="col-sm-4 col-sm-offset-8">
                                                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_comment" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </EditItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Comment" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Labelw41" runat="server" CssClass="col-sm-10" Text='<%# Eval("FullNameTH") %>'></asp:Label>
                                                        <asp:Label ID="lblIDXCMSAP" runat="server" Visible="false" CssClass="col-sm-10" Text='<%# Eval("EmpIDX") %>'></asp:Label>
                                                        <br />
                                                        <asp:Label ID="lblTypeCode" runat="server" CssClass="col-sm-10" Text='<%# Eval("CommentAuto") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Date/Time" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Labelddw41" runat="server" CssClass="col-sm-10" Text='<%# Eval("CDate") %>'></asp:Label>
                                                        <asp:Label ID="Label41" runat="server" CssClass="col-sm-10" Text='<%# Eval("CTime") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Management">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                                    </ItemTemplate>

                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>



                                            </Columns>

                                        </asp:GridView>

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-comment"></i><strong>&nbsp; เพิ่มรายการคอมเม้นต์ / Upload File</strong></h4>

                                    <div class="form-horizontal" role="form">

                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="Label30" runat="server" Text="เพิ่ม Comment :" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtcomment" TextMode="multiline" Width="510pt" Rows="5" runat="server" CssClass="form-control" placeholder="Comment ..................." />
                                                </div>
                                            </div>


                                            <asp:UpdatePanel ID="Update_Comment" runat="server">
                                                <ContentTemplate>

                                                    <%-------------- Upload File --------------%>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label101" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                        <div class="col-sm-7">

                                                            <asp:FileUpload ID="UploadImagesComment" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                            <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png|pdf</font></p>

                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btn_saveadduser" />

                                                </Triggers>
                                            </asp:UpdatePanel>


                                            <div class="form-group" id="divstatus_comment" runat="server" visible="true">
                                                <asp:Label ID="Label24" runat="server" Text="สถาน :" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                <div class="col-sm-2">
                                                    <asp:DropDownList ID="ddlstatus_comment" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-9">
                                                    <asp:LinkButton ID="btn_saveadduser" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_comment" OnCommand="btnCommand" CommandName="CmdSaveComment" data-toggle="tooltip" title="Save" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnCancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="BtnBack" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <asp:Panel runat="server" ID="getjob_sap" Visible="false">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-list"></i><strong>&nbsp; ส่วนของเจ้าหน้าที่</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">



                                    <div id="div_choosesys" runat="server" visible="false">
                                        <div class="form-group" id="divchangesys" runat="server">
                                            <div class="col-lg-offset-3 control-labelnotop1">
                                                <asp:Label ID="Label56" CssClass="col-sm-3 control-label text_right" runat="server" Text="โอนย้ายงาน : " />
                                                <div class="col-sm-4">
                                                    <asp:RadioButton ID="rdoaccept" CssClass="radio-inline" Checked="true" GroupName="Group1" Text="รับงาน" Value="1" runat="server" AutoPostBack="true" OnCheckedChanged="Group1_CheckedChanged" />&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdochange" CssClass="radio-inline" GroupName="Group1" Text="โอนย้ายงาน" Value="2" runat="server" AutoPostBack="true" OnCheckedChanged="Group1_CheckedChanged" />

                                                </div>
                                            </div>
                                        </div>

                                        <div id="divpriority" runat="server" visible="false">
                                            <div class="form-group">
                                                <div class="col-lg-offset-3 control-labelnotop1">
                                                    <asp:Label ID="Label33" CssClass="col-sm-3 control-label text_right" runat="server" Text="ลำดับความสำคัญ : " />
                                                    <div class="col-sm-4">
                                                        <asp:DropDownList ID="ddl_editpriority" CssClass="form-control" runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="divselectsys" runat="server" visible="false">
                                            <div class="form-group">
                                                <div class="col-lg-offset-3 control-labelnotop1">
                                                    <asp:Label ID="Label59" CssClass="col-sm-3 control-label text_right" runat="server" Text="ระบบงาน : " />
                                                    <div class="col-sm-4">
                                                        <asp:DropDownList ID="ddl_changesys" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0" Text="กรุณาเลือกระบบงาน"></asp:ListItem>
                                                            <asp:ListItem Value="1" Text="Google Apps"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="SAP"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="IT"></asp:ListItem>
                                                            <asp:ListItem Value="22" Text="BI"></asp:ListItem>
                                                            <asp:ListItem Value="23" Text="BP"></asp:ListItem>
                                                            <%--  <asp:ListItem Value="20" Text="POS"></asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="Save_Accept" runat="server" Display="None"
                                                            ControlToValidate="ddl_changesys" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกระบบงาน"
                                                            ValidationExpression="กรุณาเลือกระบบงาน" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />



                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                        <div id="divchooseen" runat="server" visible="false">
                                            <div class="form-group">
                                                <div class="col-lg-offset-3 control-labelnotop1">
                                                    <asp:Label ID="Label92" CssClass="col-sm-3 control-label text_right" runat="server" Text="เลือกช่าง : " />
                                                    <div class="col-sm-4">
                                                        <asp:DropDownList ID="ddlen" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0">กรุณาเลือกช่าง...</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ValidationGroup="Save_Accept" runat="server" Display="None"
                                                            ControlToValidate="ddlen" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกช่าง"
                                                            ValidationExpression="กรุณาเลือกช่าง" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <asp:UpdatePanel ID="update_test" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div id="div_statusitfinish" runat="server" visible="false">

                                                <div class="form-group">
                                                    <div class="col-lg-offset-3 control-labelnotop1">
                                                        <asp:Label ID="Label83" CssClass="col-sm-3 control-label text_right" runat="server" Text="ขั้นตอนดำเนินการ : " />
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="ddl_chooseit" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                                <asp:ListItem Value="0" Text="เลือกสถานะดำเนินการ"></asp:ListItem>
                                                                <%--<asp:ListItem Value="43" Text="รออะไหล่"></asp:ListItem>
                                                        <asp:ListItem Value="44" Text="ส่งเครม"></asp:ListItem>
                                                        <asp:ListItem Value="45" Text="ติดต่อซัพภายนอก"></asp:ListItem>
                                                        <asp:ListItem Value="46" Text="รอ User สะดวกให้ดำเนินการ"></asp:ListItem>
                                                        <asp:ListItem Value="22" Text="ดำเนินการเรียบร้อยแล้ว"></asp:ListItem>--%>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save_close" runat="server" Display="None"
                                                                ControlToValidate="ddl_chooseit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกสถานะดำเนินการ"
                                                                ValidationExpression="กรุณาเลือกสถานะดำเนินการ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddl_chooseit" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group">
                                        <div class="col-sm-3 col-sm-offset-9">

                                            <asp:LinkButton ID="btnadmingetjobsap" ValidationGroup="Save_Accept" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSapGetJob" CommandArgument='<%# Eval("URQIDX") %>' OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-gavel"></i> บันทึก</asp:LinkButton>


                                            <asp:LinkButton ID="btnadminfinishgetjobsap" Visible="false" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSapGetJob" CommandArgument='<%# Eval("URQIDX") %>' OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-ok"></i></i></asp:LinkButton>

                                            <asp:LinkButton ID="btncancelgetjobsap" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="BtnBack" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </asp:Panel>

                    <asp:Panel runat="server" ID="Panel_user_approve" Visible="false">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-list"></i><strong>&nbsp; ส่วนของผู้สร้าง</strong></h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-group">
                                    <div class="col-lg-offset-3 control-labelnotop1">
                                        <asp:Label ID="Label37" CssClass="col-sm-3 control-label text_right" runat="server" Text="สถานะรายการ : " />
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddl_approve" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกสถานะรายการ"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="อนุมัติ"></asp:ListItem>
                                                <asp:ListItem Value="5" Text="ไม่อนุมัติ"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddl_approve" Font-Size="11"
                                                ErrorMessage="เลือกสถานะอนุมัติ"
                                                ValidationExpression="เลือกสถานะอนุมัติ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                        </div>
                                    </div>
                                    <%--                                        </div>

                                        <div class="form-group">--%>
                                    <%--     <div class="col-lg-offset-8 col-sm-4 control-labelnotop1">--%>

                                    <div class="form-group">
                                        <div class="col-sm-3 col-sm-offset-9">
                                            <asp:LinkButton ID="LinkButton2" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSapGetJob" CommandArgument='<%# Eval("URQIDX") %>' OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-ok"></i></asp:LinkButton>


                                            <asp:LinkButton ID="LinkButton3" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="BtnBack" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                    <%-- </div>--%>
                                </div>




                            </div>
                        </div>

                    </asp:Panel>

                    <div class="panel panel-warning" id="AdminAcceptCloseJob" runat="server" visible="false">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ข้อมูลปิดงาน</strong></h3>
                        </div>
                        <asp:FormView ID="FvCloseJob" DefaultMode="Insert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>
                                <div class="panel-body">


                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="Module LV1 : " />
                                            <div class="col-sm-3">
                                                <asp:Label ID="lvlMS1IDX" runat="server" Text='<%# Bind("MS1IDX") %>' Visible="false" />
                                                <asp:DropDownList ID="ddlLV1" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RqddlEquipmentEditDeviceCode1" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlLV1" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกประเภท Module"
                                                    ValidationExpression="กรุณาเลือกประเภท Module" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqddlEquipmentEditDeviceCode1" Width="160" />
                                            </div>

                                            <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="หัวข้อ LV2 : " />
                                            <div class="col-sm-3">
                                                <asp:Label ID="lblMS2IDX" runat="server" Text='<%# Bind("MS2IDX") %>' Visible="false" />
                                                <asp:DropDownList ID="ddlLV2" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Text="Select Topic ...." Value="0"></asp:ListItem>

                                                </asp:DropDownList>
                                                <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlLV2" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกTopic"
                                                    ValidationExpression="กรุณาเลือกประเภทTopic" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />--%>

                                                <asp:DropDownList ID="SAPModule" Visible="false" CssClass="form-control" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>

                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label38" CssClass="col-sm-2 control-label" runat="server" Text="สาเหตุ LV3 : " />
                                            <div class="col-sm-3">
                                                <asp:Label ID="lblMS3IDX" runat="server" Text='<%# Bind("MS3IDX") %>' Visible="false" />
                                                <asp:DropDownList ID="ddlLV3" AutoPostBack="true" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlLV3" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกสาเหตุ"
                                                    ValidationExpression="กรุณาเลือกสาเหตุ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                            </div>

                                            <asp:Label ID="Label39" CssClass="col-sm-3 control-label" runat="server" Text="เกิดจาก LV4 : " />
                                            <div class="col-sm-3">
                                                <asp:Label ID="lblMS4IDX" runat="server" Text='<%# Bind("MS4IDX") %>' Visible="false" />
                                                <asp:DropDownList ID="ddlLV4" AutoPostBack="true" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlLV4" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกเกิดจาก"
                                                    ValidationExpression="กรุณาเลือกเกิดจาก" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label40" CssClass="col-sm-2 control-label" runat="server" Text="แก้ไข LV5 : " />
                                            <div class="col-sm-3">
                                                <asp:Label ID="lblMS5IDX" runat="server" Text='<%# Bind("MS5IDX") %>' Visible="false" />
                                                <asp:DropDownList ID="ddlLV5" AutoPostBack="true" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlLV5" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกประเภทแก้ไข"
                                                    ValidationExpression="กรุณาเลือกประเภทแก้ไข" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />
                                            </div>

                                            <asp:Label ID="Label44" CssClass="col-sm-3 control-label" runat="server" Text="ลำดับความสำคัญ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddl_editpriority_close" CssClass="form-control" runat="server">
                                                </asp:DropDownList>

                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <asp:Label ID="Label42" CssClass="col-sm-2 control-label" runat="server" Text="Man-Hours : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtmanhours" CssClass="form-control" runat="server" placeholder="Man-Hours ......"> </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="txtmanhours" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกจำนวน" />
                                                <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="Save_close" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                    ControlToValidate="txtmanhours"
                                                    ValidationExpression="^[0-9]{1,10}$" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />


                                            </div>
                                            <div class="col-sm-2">
                                                <asp:Label ID="Labeh" CssClass="col-sm-2 control-label" runat="server" Text="นาที" />
                                            </div>

                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label78" CssClass="col-sm-2 control-label" runat="server" Text="Link : " />
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtlink" CssClass="form-control" runat="server" placeholder="Link ......"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_close" runat="server" Display="None" ControlToValidate="txtlink" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                    ValidationGroup="Save_close" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtlink"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblsapsmg" CssClass="col-sm-2 control-label" runat="server" Text="Sap Msg : " />
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtsapmsg" CssClass="form-control" runat="server" placeholder="Sap Msg ......"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="Save_close" runat="server" Display="None" ControlToValidate="txtsapmsg" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                    ValidationGroup="Save_close" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtsapmsg"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-labelnotop text_right">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="CommentAdminSap1" CssClass="control-label" runat="server" Text="ความเห็นผู้ดูแล :" />
                                                </h2>
                                                <h4></h4>
                                            </label>
                                            <div class="col-sm-9">
                                                <asp:TextBox CssClass="txtform form-control" ID="CommentAdminSap" runat="server" placeholder="หมายเหตุการแจ้งซ่อม SAP ....." TextMode="MultiLine" Rows="4"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="Save_close" runat="server" Display="None" ControlToValidate="CommentAdminSap" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                    ValidationGroup="Save_close" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="CommentAdminSap"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                            </div>
                                        </div>


                                    </div>


                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>

                        <asp:FormView ID="FvCloseJobIT" DefaultMode="Insert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>
                                <div class="panel-body">


                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="เคส LV1 :  " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlITLV1" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RqddlEquipmentEditDdeviceCode1" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlITLV1" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกเคส LV1"
                                                    ValidationExpression="กรุณาเลือกเคส LV1" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqddlEquipmentEditDdeviceCode1" Width="160" />
                                            </div>

                                            <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="อาการ LV2 : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlITLV2" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">เลือกอาการแจ้งซ่อม</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlITLV2" Font-Size="11"
                                                    ErrorMessage="เลือกอาการ LV2"
                                                    ValidationExpression="เลือกอาการ LV2" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender26" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator17" Width="160" />


                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label70" CssClass="col-sm-2 control-label" runat="server" Text="สาเหตุ LV3 :  " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlITLV3" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">เลือกสาเหตุแจ้งซ่อม</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlITLV3" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกสาเหตุรแจ้งซ่อม LV3"
                                                    ValidationExpression="กรุณาเลือกสาเหตุรแจ้งซ่อม LV3" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatordCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            </div>

                                            <asp:Label ID="Label73" CssClass="col-sm-3 control-label" runat="server" Text="แก้ไข LV4 : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlITLV4" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">เลือกแก้ไขแจ้งซ่อม</asp:ListItem>

                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlITLV4" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกแก้ไขแจ้งซ่อม LV4"
                                                    ValidationExpression="กรุณาเลือกแก้ไขแจ้งซ่อม LV4" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCaldfloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />


                                            </div>
                                        </div>




                                        <div class="form-group">
                                            <asp:Label ID="Label78" CssClass="col-sm-2 control-label" runat="server" Text="Link : " />
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtlink" CssClass="form-control" runat="server" placeholder="Link ......"></asp:TextBox>

                                                <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="Save_close" runat="server" Display="None" ControlToValidate="txtlink" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                    ValidationGroup="Save_close" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtlink"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-labelnotop text_right">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="CommentAdminSap1" CssClass="control-label" runat="server" Text="ความเห็นผู้ดูแล :" />
                                                </h2>
                                                <h4></h4>
                                            </label>
                                            <div class="col-sm-9">
                                                <asp:TextBox CssClass="txtform form-control" ID="commentitclosejob" runat="server" placeholder="หมายเหตุการแจ้งซ่อม IT ....." TextMode="MultiLine" Rows="4"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="Save_close" runat="server" Display="None" ControlToValidate="commentitclosejob" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                    ValidationGroup="Save_close" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="commentitclosejob"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator4" Width="160" />
                                            </div>
                                        </div>


                                    </div>


                                </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>

                        <asp:FormView ID="FvCloseJobGM" DefaultMode="Insert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>
                                <div class="panel-body">

                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-md-2 control-labelnotop text_right">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="CommentAdminSap1" CssClass="control-label" runat="server" Text="ความเห็นผู้ดูแล :" />
                                                </h2>
                                                <h4></h4>
                                            </label>
                                            <div class="col-sm-9">
                                                <asp:TextBox CssClass="txtform form-control" ID="commentgmclosejob" runat="server" placeholder="หมายเหตุการแจ้งซ่อม GoogleApps ....." TextMode="MultiLine" Rows="4"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidawtor13" ValidationGroup="Save_close" runat="server" Display="None" ControlToValidate="commentgmclosejob" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidawtor13" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionVaslidator4" runat="server"
                                                    ValidationGroup="Save_close" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="commentgmclosejob"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionVaslidator4" Width="160" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>

                        <asp:FormView ID="FvCloseJobPOS" DefaultMode="Insert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <asp:Label ID="Label14" CssClass="col-sm-2 control-label" runat="server" Text="หัวข้อแจ้งซ่อม(Lv1) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlPOSLV1" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RqddlEquipmentEdistDeviceCode1" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlPOSLV1" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกเคส LV1"
                                                    ValidationExpression="กรุณาเลือกเคส LV1" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqddlEquipmentEdistDeviceCode1" Width="160" />

                                            </div>
                                            <asp:Label ID="Label15" CssClass="col-sm-3 control-label" runat="server" Text="อาการแจ้งซ่อม(Lv2) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlPOSLV2" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="เลือกอาการแจ้งซ่อม"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlPOSLV2" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกอาการ LV2"
                                                    ValidationExpression="กรุณาเลือกอาการ LV2" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label16" CssClass="col-sm-2 control-label" runat="server" Text="แก้ไขแจ้งซ่อม(Lv3) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlPOSLV3" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="เลือกแก้ไขแจ้งซ่อม"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlPOSLV3" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกแก้ไข LV3"
                                                    ValidationExpression="กรุณาเลือกแก้ไข LV3" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />


                                            </div>
                                            <asp:Label ID="Label17" CssClass="col-sm-3 control-label" runat="server" Text="สาเหตุแจ้งซ่อม(Lv4) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlPOSLV4" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="เลือกสาเหตุแจ้งซ่อม"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlPOSLV4" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกสาเหตุ LV4"
                                                    ValidationExpression="กรุณาเลือกสาเหตุ LV4" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-labelnotop text_right">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="CommentAdminSap1" CssClass="control-label" runat="server" Text="ความเห็นผู้ดูแล :" />
                                                </h2>
                                                <h4></h4>
                                            </label>
                                            <div class="col-sm-9">
                                                <asp:TextBox CssClass="txtform form-control" ID="commentposclosejob" runat="server" placeholder="หมายเหตุการแจ้งซ่อม POS ....." TextMode="MultiLine" Rows="4"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidcator13" ValidationGroup="Save_close" runat="server" Display="None" ControlToValidate="commentposclosejob" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtendeer20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidcator13" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                    ValidationGroup="Save_close" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="commentposclosejob"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatttorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator4" Width="160" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>

                        <asp:FormView ID="FvCloseJobRES" DefaultMode="Insert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>
                                <div class="panel-body">


                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="หัวข้อ :  " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlRESLV1" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกหัวข้อ</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RqddlEquipmentEditDeviceCode1" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlRESLV1" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกหัวข้อ"
                                                    ValidationExpression="กรุณาเลือกหัวข้อ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqddlEquipmentEditDeviceCode1" Width="160" />
                                            </div>

                                            <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="รายการซ่อม : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlRESLV2" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกรายการซ่อม</asp:ListItem>

                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlRESLV2" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกรายการซ่อม"
                                                    ValidationExpression="กรุณาเลือกรายการซ่อม" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />


                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label70" CssClass="col-sm-2 control-label" runat="server" Text="วิธีแก้ไข :  " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlRESLV3" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกวิธีแก้ไข</asp:ListItem>

                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlRESLV3" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกวิธีแก้ไข"
                                                    ValidationExpression="กรุณาเลือกวิธีแก้ไข" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatordCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            </div>

                                            <asp:Label ID="Label73" CssClass="col-sm-3 control-label" runat="server" Text="คำแนะนำ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlRESLV4" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกคำแนะนำ</asp:ListItem>

                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldddValidator10" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddlRESLV4" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกคำแนะนำ"
                                                    ValidationExpression="กรุณาเลือกคำแนะนำ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatossrCaldfloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldddValidator10" Width="160" />


                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <asp:Label ID="Label42" CssClass="col-sm-2 control-label" runat="server" Text="ค่าใช้จ่าย : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtprices" CssClass="form-control" runat="server" placeholder="ราคา..."> </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RqRetxtpssrice22" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="txtprices" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกราคา" />
                                                <asp:RegularExpressionValidator ID="Retxwwtprice22" runat="server" ValidationGroup="Save_close" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                    ControlToValidate="txtprices"
                                                    ValidationExpression="^[0-9]{1,10}$" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtpssrice22" Width="160" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxwwtprice22" Width="160" />


                                            </div>
                                            <div class="col-sm-1">
                                                <asp:Label ID="Labeh" CssClass="col-sm-2 control-label" runat="server" Text="บาท" />
                                            </div>
                                            <asp:Label ID="Label99" CssClass="col-sm-2 control-label" runat="server" Text="ลำดับความสำคัญ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddl_editpriority_resclose" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกลำดับความสำคัญ</asp:ListItem>

                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ValidationGroup="Save_close" runat="server" Display="None"
                                                    ControlToValidate="ddl_editpriority_resclose" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกลำดับความสำคัญ"
                                                    ValidationExpression="กรุณาเลือกลำดับความสำคัญ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender25" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator16" Width="160" />


                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-2 control-labelnotop text_right">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="CommentAdminSap1" CssClass="control-label" runat="server" Text="ความเห็นผู้ดูแล :" />
                                                </h2>
                                                <h4></h4>
                                            </label>
                                            <div class="col-sm-9">
                                                <asp:TextBox CssClass="txtform form-control" ID="commentresclosejob" runat="server" placeholder="หมายเหตุการแจ้งซ่อม RES ....." TextMode="MultiLine" Rows="4"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="Save_close" runat="server" Display="None" ControlToValidate="commentresclosejob" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                    ValidationGroup="Save_close" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="commentresclosejob"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator4" Width="160" />
                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <asp:Label ID="Label1051" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                            <div class="col-sm-7">

                                                <asp:FileUpload ID="UploadImagesClosejob" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png|pdf</font></p>

                                            </div>
                                        </div>



                                    </div>


                                </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>

                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <div class="col-md-12">

                                        <asp:Label ID="Label43" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เจ้าหน้าที่รับงาน : " />
                                        <div class="col-xs-3">
                                            <%--<asp:Label ID="lblacceptadmin" CssClass="form-control" runat="server"></asp:Label>--%>
                                            <asp:Label ID="lblacceptadmin" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("AdminName") %>' />
                                        </div>
                                    </div>
                                </div>

                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-10">
                                                <asp:LinkButton ID="btnclosejob" ValidationGroup="Save_close" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSapGetJob" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-ok"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="BtnBack" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnclosejob" />

                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                    </div>

                    <div class="panel panel-success" visible="false" runat="server" id="Panel_ViewCloseJob">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ความเห็น Admin</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">


                                <asp:FormView ID="FvViewClostJob" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <ItemTemplate>
                                        <%-- <div class="form-horizontal" role="form">--%>

                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่รับแจ้ง : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbEmpIDX" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DateReciveJobFirst")%>' />

                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbEmpName" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("TimeReciveJobFirst") %>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Module LV1 : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbOrgNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Name1")%>' />
                                                </div>

                                                <asp:Label ID="Label34" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="หัวข้อ LV2 : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbDeptNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Name2")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- แผนก,ตำแหน่ง --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Labedl1" class="col-xs-2 control-labelnotop text_right" runat="server" Text="สาเหตุ LV3 : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label23" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("Name3") %>' />
                                                </div>

                                                <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เกิดจาก LV4 : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbPosNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Name4")%>' />
                                                </div>
                                            </div>
                                        </div>

                                        <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label5" class="col-xs-2 control-labelnotop text_right" runat="server" Text="แก้ไข LV5 : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label25" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("Name5") %>' />
                                                </div>

                                                <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ลำดับความสำคัญ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label26" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("Priority_name") %>' />

                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- Cost Center --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label20" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ManHours : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LotusCost" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("ManHours")%>' />
                                                </div>

                                                <asp:Label ID="Label46" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Downtime : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label49" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("sumtime") %>' />

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label36" class="col-xs-2 control-labelnotop text_right" runat="server" Text="Link : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label45" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Link")%>' />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label47" class="col-xs-2 control-labelnotop text_right" runat="server" Text="SapMsg : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label48" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("SapMsg")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label50" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ความเห็นผู้ดูแล : " />
                                                <div class="col-xs-7">
                                                    <asp:Label ID="Label52" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("CommentAMDoing")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label54" class="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่ปิดงาน : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label55" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DatecloseJob")%>' />
                                                </div>
                                                <asp:Label ID="Label57" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label58" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("TimecloseJob")%>' />
                                                </div>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label62" class="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานะการดำเนินการ : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label63" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("StaName")%>' />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label65" class="col-xs-2 control-labelnotop text_right" runat="server" Text="เจ้าหน้าที่รับงาน : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label69" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("AdminName")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label71" class="col-xs-2 control-labelnotop text_right" runat="server" Text="เจ้าหน้าที่ปิดงาน : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label72" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("AdminDoingName")%>' />
                                                </div>
                                            </div>
                                        </div>

                                        <asp:Literal ID="AMFileSAP" runat="server" Visible="false" Text='<%# Eval("FileAMDoing") %>' />
                                    </ItemTemplate>
                                </asp:FormView>

                                <asp:FormView ID="FvVIewCloseJobIT" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <ItemTemplate>
                                        <%-- <div class="form-horizontal" role="form">--%>

                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่รับแจ้ง : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbEmpIDX" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DateReciveJobFirst")%>' />

                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbEmpName" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("TimeReciveJobFirst") %>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เคส LV1 : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbOrgNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Name6")%>' />
                                                </div>

                                                <asp:Label ID="Label34" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="อาการ LV2 : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbDeptNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Name7")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- แผนก,ตำแหน่ง --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Labedl1" class="col-xs-2 control-labelnotop text_right" runat="server" Text="สาเหตุ LV3 : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label23" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("Name8") %>' />
                                                </div>

                                                <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แก้ไข LV4 : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbPosNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Name9")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- Cost Center --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label46" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Downtime : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label49" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("sumtime") %>' />

                                                </div>
                                                <asp:Label ID="Label93" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานะการดำเนินการ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label94" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("StaName")%>' />
                                                </div>
                                                <%--<asp:Label ID="Label91" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เครื่องถือครอง : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label92" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("u0_code")%>' />
                                                </div>--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label36" class="col-xs-2 control-labelnotop text_right" runat="server" Text="Link : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label45" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Link_IT")%>' />
                                                </div>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label50" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ความเห็นผู้ดูแล : " />
                                                <div class="col-xs-7">
                                                    <asp:Label ID="Label52" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("CommentAMDoing")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label54" class="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่ปิดงาน : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label55" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DatecloseJob")%>' />
                                                </div>
                                                <asp:Label ID="Label57" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label58" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("TimecloseJob")%>' />
                                                </div>

                                            </div>
                                        </div>


                                        <%--                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label62" class="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานะการดำเนินการ : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label63" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("StaName")%>' />
                                                </div>
                                            </div>
                                        </div>--%>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label65" class="col-xs-2 control-labelnotop text_right" runat="server" Text="เจ้าหน้าที่รับงาน : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label69" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("AdminName")%>' />
                                                </div>
                                                <asp:Label ID="Label71" class="col-xs-2 control-labelnotop text_right" runat="server" Text="เจ้าหน้าที่ปิดงาน : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label72" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("AdminDoingName")%>' />
                                                </div>
                                            </div>
                                        </div>




                                        <asp:Literal ID="AMFileSAP" runat="server" Visible="false" Text='<%# Eval("FileAMDoing") %>' />
                                    </ItemTemplate>
                                </asp:FormView>

                                <asp:FormView ID="FvVIewCloseJobGM" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <ItemTemplate>
                                        <%-- <div class="form-horizontal" role="form">--%>

                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่รับแจ้ง : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbEmpIDX" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DateReciveJobFirst")%>' />

                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbEmpName" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("TimeReciveJobFirst") %>' />
                                                </div>
                                            </div>
                                        </div>



                                        <%-------------- Cost Center --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label46" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Downtime : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label49" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("sumtime") %>' />

                                                </div>
                                            </div>
                                        </div>




                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label50" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ความเห็นผู้ดูแล : " />
                                                <div class="col-xs-7">
                                                    <asp:Label ID="Label52" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("CommentAMDoing")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label54" class="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่ปิดงาน : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label55" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DatecloseJob")%>' />
                                                </div>
                                                <asp:Label ID="Label57" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label58" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("TimecloseJob")%>' />
                                                </div>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label62" class="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานะการดำเนินการ : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label63" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("StaName")%>' />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label65" class="col-xs-2 control-labelnotop text_right" runat="server" Text="เจ้าหน้าที่รับงาน : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label69" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("AdminName")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label71" class="col-xs-2 control-labelnotop text_right" runat="server" Text="เจ้าหน้าที่ปิดงาน : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label72" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("AdminDoingName")%>' />
                                                </div>
                                            </div>
                                        </div>

                                        <asp:Literal ID="AMFileSAP" runat="server" Visible="false" Text='<%# Eval("FileAMDoing") %>' />
                                    </ItemTemplate>
                                </asp:FormView>

                                <asp:FormView ID="FvViewClosePOS" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <ItemTemplate>
                                        <%-- <div class="form-horizontal" role="form">--%>

                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่รับแจ้ง : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbEmpIDX" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DateReciveJobFirst")%>' />

                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbEmpName" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("TimeReciveJobFirst") %>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เคส LV1 : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbOrgNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Name10")%>' />
                                                </div>

                                                <asp:Label ID="Label34" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="อาการ LV2 : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbDeptNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Name11")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- แผนก,ตำแหน่ง --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Labedl1" class="col-xs-2 control-labelnotop text_right" runat="server" Text="แก้ไข LV3 : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label23" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("Name12") %>' />
                                                </div>

                                                <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="สาเหตุ LV4 : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbPosNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Name13")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- Cost Center --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label46" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Downtime คิด KPI : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label49" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("sumtime") %>' />

                                                </div>
                                                <asp:Label ID="Label84" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Downtime ไม่คิด KPI : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label85" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("nonkpi")%>' />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label86" class="col-xs-2 control-labelnotop text_right" runat="server" Text="All Downtime : " />
                                                <div class="col-xs-7">
                                                    <asp:Label ID="Label87" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("alltime")%>' />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label50" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ความเห็นผู้ดูแล : " />
                                                <div class="col-xs-7">
                                                    <asp:Label ID="Label52" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("CommentAMDoing")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label54" class="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่ปิดงาน : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label55" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DatecloseJob")%>' />
                                                </div>
                                                <asp:Label ID="Label57" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label58" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("TimecloseJob")%>' />
                                                </div>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label62" class="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานะการดำเนินการ : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label63" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("StaName")%>' />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label65" class="col-xs-2 control-labelnotop text_right" runat="server" Text="เจ้าหน้าที่รับงาน : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label69" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("AdminName")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label71" class="col-xs-2 control-labelnotop text_right" runat="server" Text="เจ้าหน้าที่ปิดงาน : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label72" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("AdminDoingName")%>' />
                                                </div>
                                            </div>
                                        </div>

                                        <asp:Literal ID="AMFileSAP" runat="server" Visible="false" Text='<%# Eval("FileAMDoing") %>' />
                                    </ItemTemplate>
                                </asp:FormView>


                                <asp:FormView ID="FvViewCloseRES" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <ItemTemplate>
                                        <%-- <div class="form-horizontal" role="form">--%>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label100" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="อุปกรณ์ : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label102" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("devices_name") %>' />

                                                </div>
                                                <asp:Label ID="Label103" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ลำดับความสำคัญ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label104" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Priority_name")%>' />
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label105" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ช่างดำเนินการ : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label106" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("en_name") %>' />

                                                </div>
                                                <asp:Label ID="Label107" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ค่าใช้จ่าย : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label108" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("price")%>' />
                                                </div>

                                            </div>
                                        </div>

                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่รับแจ้ง : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbEmpIDX" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DateReciveJobFirst")%>' />

                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbEmpName" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("TimeReciveJobFirst") %>' />
                                                </div>
                                            </div>
                                        </div>






                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="หัวข้อ : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbOrgNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("RES1_Name")%>' />
                                                </div>

                                                <asp:Label ID="Label34" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รายการซ่อม : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbDeptNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("RES2_Name")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- แผนก,ตำแหน่ง --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Labedl1" class="col-xs-2 control-labelnotop text_right" runat="server" Text="วิธีแก้ไข : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label23" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("RES3_Name") %>' />
                                                </div>

                                                <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="คำแนะนำ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbPosNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("RES4_Name")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- Cost Center --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label46" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Downtime : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label49" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("sumtime") %>' />

                                                </div>
                                                <asp:Label ID="Label93" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานะการดำเนินการ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label94" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("StaName")%>' />
                                                </div>

                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label50" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ความเห็นผู้ดูแล : " />
                                                <div class="col-xs-7">
                                                    <asp:Label ID="Label52" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("CommentAMDoing")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label54" class="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่ปิดงาน : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label55" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DatecloseJob")%>' />
                                                </div>
                                                <asp:Label ID="Label57" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label58" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("TimecloseJob")%>' />
                                                </div>

                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label65" class="col-xs-2 control-labelnotop text_right" runat="server" Text="เจ้าหน้าที่รับงาน : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label69" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("AdminName")%>' />
                                                </div>
                                                <asp:Label ID="Label71" class="col-xs-2 control-labelnotop text_right" runat="server" Text="เจ้าหน้าที่ปิดงาน : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label72" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("AdminDoingName")%>' />
                                                </div>
                                            </div>
                                        </div>




                                        <asp:Literal ID="AMFileSAP" runat="server" Visible="false" Text='<%# Eval("FileAMDoing") %>' />
                                    </ItemTemplate>
                                </asp:FormView>


                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-1 col-sm-offset-11">
                            <asp:LinkButton ID="btnback" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Back" runat="server" CommandName="BtnBack" OnCommand="btnCommand"><i class="fa fa-arrow-left"></i></asp:LinkButton>
                        </div>
                    </div>



                </div>
            </div>

        </asp:View>


        <asp:View ID="ViewAdd" runat="server">


            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้แจ้งซ่อม</strong></h3>
                </div>
                <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                    <InsertItemTemplate>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                <div class="form-group">

                                    <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>

                                    <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>

                                </div>


                                <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                <div class="form-group">

                                    <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>

                                    <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>

                                </div>


                                <%-------------- แผนก,ตำแหน่ง --------------%>
                                <div class="form-group">

                                    <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>

                                    <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>

                                </div>

                                <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                <div class="form-group">

                                    <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                    <div class="col-sm-3">
                                        <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                        <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>

                                    <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                    <div class="col-sm-3">
                                        <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                        <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
            </div>


            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-wrench"></i><strong>&nbsp; ระบบแจ้งซ่อม</strong></h3>
                </div>
                <div class="form-horizontal" role="form">
                    <div class="panel-body">

                        <div class="col-lg-12">
                            <div class="col-sm-2">

                                <asp:ImageButton ID="imgit" CssClass="img-responsive" runat="server" ToolTip="IT Support" CommandName="btnsystem" CommandArgument="3" OnCommand="btnCommand" ImageUrl="~/masterpage/images/itservice/btn_it.png" />
                            </div>

                            <div class="col-sm-2">

                                <asp:ImageButton ID="imgsap" CssClass="img-responsive" ToolTip="SAP" runat="server" CommandName="btnsystem" CommandArgument="2" OnCommand="btnCommand" ImageUrl="~/masterpage/images/itservice/btn_sap.png" />
                            </div>
                            <div class="col-sm-2">
                                <asp:ImageButton ID="imggoogle" CssClass="img-responsive" ToolTip="GoogleApps" runat="server" CommandName="btnsystem" CommandArgument="1" OnCommand="btnCommand" ImageUrl="~/masterpage/images/itservice/btn_google.png" />
                            </div>
                            <div class="col-sm-2">
                                <asp:ImageButton ID="imgpos" CssClass="img-responsive" ToolTip="POS" runat="server" CommandName="btnsystem" CommandArgument="20" OnCommand="btnCommand" ImageUrl="~/masterpage/images/itservice/btn_pos.png" />
                            </div>
                            <div class="col-sm-2">
                                <asp:ImageButton ID="imgbi" CssClass="img-responsive" ToolTip="BI" runat="server" CommandName="btnsystem" CommandArgument="22" OnCommand="btnCommand" ImageUrl="~/masterpage/images/itservice/btn_bi.png" />
                            </div>
                            <div class="col-sm-2">
                                <asp:ImageButton ID="imgbp" CssClass="img-responsive" ToolTip="BP" runat="server" CommandName="btnsystem" CommandArgument="23" OnCommand="btnCommand" ImageUrl="~/masterpage/images/itservice/btn_bp.png" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />


                        <div class="col-lg-12">
                            <div class="col-sm-2">

                                <asp:ImageButton ID="imgres" CssClass="img-responsive" runat="server" ToolTip="RES" CommandName="btnsystem" CommandArgument="28" OnCommand="btnCommand" ImageUrl="~/masterpage/images/itservice/btn_res.png" />
                            </div>
                            <div class="col-sm-10">
                            </div>
                        </div>

                    </div>
                </div>


            </div>

            <div class="panel panel-success" visible="false" runat="server" id="Panel_repair">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-wrench"></i><strong>&nbsp; ข้อมูลแจ้งซ่อม
                        <asp:Literal ID="namesystem" runat="server"></asp:Literal>
                    </strong></h3>
                </div>


                <asp:FormView ID="FvInsertSAP" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                    <InsertItemTemplate>

                        <div class="panel-body">
                            <div class="form-horizontal" role="form">


                                <div class="form-group" id="selectother" runat="server">

                                    <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="แจ้งโดย : " />
                                    <div class="col-sm-3">

                                        <asp:DropDownList ID="ddlselect_choose" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="1">แจ้งโดยตนเอง</asp:ListItem>
                                            <asp:ListItem Value="2">แจ้งแทนผู้อื่น</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>

                                    <div id="div_choose" runat="server" visible="false">


                                        <asp:Label ID="Label18" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้แทน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlselectother" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">บุคคลแจ้งซ่อม....</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlselectother" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกบุคคลแจ้งซ่อม"
                                                ValidationExpression="กรุณาเลือกบุคคลแจ้งซ่อม" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />


                                        </div>
                                    </div>

                                </div>

                                <%-------------- เบอร์ติดต่อ สถานที่แจ้งซ่อม --------------%>
                                <div class="form-group">

                                    <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อที่ติดต่อได้ : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtteladd" CssClass="form-control" MaxLength="10" runat="server"></asp:TextBox>

                                        <asp:RegularExpressionValidator ID="RetxtUnit928" runat="server"
                                            ValidationGroup="SaveAdd" Display="None"
                                            ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                            ControlToValidate="txtteladd"
                                            ValidationExpression="^[0-9]{1,12}$" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender72" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtUnit928" Width="160" />

                                    </div>

                                    <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="สถานที่แจ้งซ่อม : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddllocation" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกสถานที่แจ้งซ่อม....</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="SaveAdd" runat="server" Display="None"
                                            ControlToValidate="ddllocation" Font-Size="11"
                                            ErrorMessage="กรุณาเลือกสถานที่แจ้งซ่อม"
                                            ValidationExpression="กรุณาเลือกสถานที่แจ้งซ่อม" InitialValue="0" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                    </div>

                                </div>

                                <div id="insersap" runat="server" visible="false">

                                    <%-------------- User Log on Name,T-Code --------------%>
                                    <div class="form-group" id="div_usersap" runat="server">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="User Log on Name : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtuser" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtuser" Font-Size="11"
                                                ErrorMessage="กรุณากรอก UserLogon"
                                                ValidationExpression="กรุณากรอก UserLogon"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                        </div>

                                        <asp:Label ID="lbltcode" CssClass="col-sm-3 control-label" runat="server" Text="T-Code/หน้าจอที่ใช้งาน : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txttcode" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txttcode" Font-Size="11"
                                                ErrorMessage="กรุณากรอก T-Code"
                                                ValidationExpression="กรุณากรอก T-Code"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                        </div>

                                    </div>

                                    <%-------------- ลำดับความสำคัญ --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="Label11" CssClass="col-sm-2 control-label" runat="server" Text="ลำดับความสำคัญ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlpriority" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกลำดับความสำคัญ....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div id="div_deviceres" runat="server">
                                            <asp:Label ID="Label77" CssClass="col-sm-3 control-label" runat="server" Text="อุปกรณ์ในการแจ้ง : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddldevicesres" AutoPostBack="true" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกอุปกรณ์....</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="ddldevicesres" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกอุปกรณ์"
                                                    ValidationExpression="กรุณาเลือกอุปกรณ์" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="160" />

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <asp:Panel ID="BoxRemoteSAP" runat="server">
                                    <div class="form-group">

                                        <div id="div_holder" runat="server">
                                            <asp:Label ID="lblholder" CssClass="col-sm-2 control-label" runat="server" Text="เครื่องถือครอง :  " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlholder" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0" Text="เลือกเครื่องถือครอง"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidadtor14" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="ddlholder" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกเครื่องถือครอง"
                                                    ValidationExpression="กรุณาเลือกเครื่องถือครอง" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidadtor14" Width="160" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Labeel18" CssClass="col-sm-2 control-label" runat="server" Text="โปรแกรม Remote ที่ใช้ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlprogram" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกโปรแกรมรีโมท....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <%--<div class="form-group" runat="server">
                                           

                                        </div>--%>


                                    <div class="form-group">
                                        <asp:Label ID="Label14" class="col-sm-2 control-label" runat="server" Text="Your ID (โปรแกรมรีโมท) : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtremote" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                        <asp:Label ID="Label16" CssClass="col-sm-3 control-label" runat="server" Text="Password (โปรแกรมรีโมท) : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtremotepass" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                </asp:Panel>


                                <%-------------- หมายเหตุการแจ้ง --------------%>
                                <div class="form-group">

                                    <asp:Label ID="Labedle1" class="col-sm-2 control-label" runat="server" Text="หมายเหตุการแจ้ง : " />
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtremark" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtremark" Font-Size="11"
                                            ErrorMessage="กรุณากรอกความคิดเห็น"
                                            ValidationExpression="กรุณากรอกความคิดเห็น"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                            ValidationGroup="SaveAdd" Display="None"
                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                            ControlToValidate="txtremark"
                                            ValidationExpression="^[\s\S]{0,1000}$"
                                            SetFocusOnError="true" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                    </div>


                                </div>


                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>

                                        <%-------------- Upload File --------------%>

                                        <div class="form-group">

                                            <asp:Label ID="Label10" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                            <div class="col-sm-7">
                                                <asp:FileUpload ID="UploadImagesSAP" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-primary btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                <p class="help-block"><font color="red">**กรุณาแนบภาพ error หรือภาพที่ติดปัญหา เฉพาะนามสกุล gif|jpg|pdf|png</font></p>

                                            </div>
                                        </div>


                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnAdddata" />
                                    </Triggers>
                                </asp:UpdatePanel>


                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-10">
                                        <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                        <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-7 col-sm-5 control-labelnotop1">
                                        <asp:Label ID="lbliso" runat="server" CssClass="text-size-14" Text="FM_IT_MN_001/05 Rev.04 มีผลบังคับใช้วันที่ (30 Oct 15)" />
                                    </div>
                                </div>


                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>


            </div>



        </asp:View>


        <asp:View ID="ViewQuestion" runat="server">

            <div class="form-horizontal" role="form">

                <div class="panel panel-warning">
                    <%--style="background-color: darkmagenta;"--%>
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; แบบสอบถาม</strong></h3>
                    </div>
                    <div class="panel-body">

                        <asp:GridView ID="GvQuestionIT"
                            runat="server"
                            AutoGenerateColumns="false"
                            DataKeyNames="MFBIDX"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            HeaderStyle-CssClass="info"
                            HeaderStyle-Height="40px"
                            AllowPaging="true"
                            OnRowDataBound="Master_RowDataBound">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center;">Data Cannot Be Found</div>
                            </EmptyDataTemplate>

                            <Columns>

                                <asp:TemplateField HeaderText="ข้อที่" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Literal ID="ltSys1" Visible="false" runat="server" Text='<%# Eval("MFBIDX") %>' /></small>
                                        <%# (Container.DataItemIndex +1) %>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="หัวข้อคำถาม" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("Topic") %>' /></small>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="คำถาม" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%" ItemStyle-CssClass="text-left">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Literal ID="ltSystemName" runat="server" Text='<%# Eval("Question") %>' /></small>
                                    </ItemTemplate>

                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="ระดับความพึงพอใจ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <small>
                                            <asp:RadioButtonList ID="rdbChoice" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="4">ดีมาก</asp:ListItem>
                                                <asp:ListItem Value="3">ดี</asp:ListItem>
                                                <asp:ListItem Value="2">พอใช้</asp:ListItem>
                                                <asp:ListItem Value="1">น้อย</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator ID="RqChoice" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="rdbChoice" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกคำตอบ"
                                                ValidationExpression="กรุณาเลือกคำตอบ" Value="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqChoice" Width="160" />
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>


                        <div class="form-group">
                            <div class="col-md-12">

                                <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ข้อเสนอะแนะ : " />
                                <div class="col-xs-7">
                                    <asp:TextBox CssClass="txtform form-control" ID="txtother" runat="server" placeholder="ข้อเสนอแนะ ..................." TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </div>

                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-2 col-sm-offset-10">
                                <asp:LinkButton ID="btnAddquest" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsertQuestion" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


        </asp:View>

    </asp:MultiView>



</asp:Content>

