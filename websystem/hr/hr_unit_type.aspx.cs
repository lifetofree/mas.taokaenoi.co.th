﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_unit_type : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    service_execute _serviceExecute = new service_execute();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlSetM0_UnitType_Insert = _serviceUrl + ConfigurationManager.AppSettings["urlSetM0_UnitType_Insert"];
    static string _urlSetM0_UnitType_Update = _serviceUrl + ConfigurationManager.AppSettings["urlSetM0_UnitType_Update"];
    static string _urlSetM0_UnitType_Delete = _serviceUrl + ConfigurationManager.AppSettings["urlSetM0_UnitType_Delete"];
    static string _urlGetM0_UnitType_Select = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0_UnitType_Select"];

    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();
        }

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_Status()
    {
        _dataEmployee.Unit_Type_List = new Unit_Type_Detail[1];
        Unit_Type_Detail dtemployee = new Unit_Type_Detail();

        dtemployee.m0_name = txtname.Text;
        dtemployee.m0_emp_create = int.Parse(Session["emp_idx"].ToString());
        dtemployee.m0_status = int.Parse(ddl_status.SelectedValue);

        _dataEmployee.Unit_Type_List[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlSetM0_UnitType_Insert, _dataEmployee);
    }

    protected void SelectMasterList()
    {
        _dataEmployee.Unit_Type_List = new Unit_Type_Detail[1];
        Unit_Type_Detail dtemployee = new Unit_Type_Detail();

        _dataEmployee.Unit_Type_List[0] = dtemployee;

        _dataEmployee = callService(_urlGetM0_UnitType_Select, _dataEmployee);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvMaster, _dataEmployee.Unit_Type_List);
    }

    protected data_employee callService(string _cmdUrl, data_employee _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);
        
        return _dtmaster;
    }

    protected void Update_Master_List()
    {
        _dataEmployee.Unit_Type_List = new Unit_Type_Detail[1];
        Unit_Type_Detail dtemployee = new Unit_Type_Detail();

        dtemployee.m0_unidx = int.Parse(ViewState["m0_unidx_update"].ToString());
        dtemployee.m0_name = ViewState["txtname_update"].ToString();
        dtemployee.m0_emp_create = int.Parse(Session["emp_idx"].ToString());
        dtemployee.m0_status = int.Parse(ViewState["ddStatus_update"].ToString());

        _dataEmployee.Unit_Type_List[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlSetM0_UnitType_Update, _dataEmployee);
    }

    protected void Delete_Master_List()
    {
        _dataEmployee.Unit_Type_List = new Unit_Type_Detail[1];
        Unit_Type_Detail dtemployee = new Unit_Type_Detail();

        dtemployee.m0_unidx = int.Parse(ViewState["m0_unidx_delete"].ToString());

        _dataEmployee.Unit_Type_List[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlSetM0_UnitType_Delete, _dataEmployee);
    }



    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        /*var txtstatus_edit = (TextBox)e.Row.FindControl("txtstatus_edit");
                        var ddl_downtime_edit = (DropDownList)e.Row.FindControl("ddl_downtime_edit");
                        var lbDTIDX = (Label)e.Row.FindControl("lbDTIDX");

                        ddl_downtime_edit.AppendDataBoundItems = true;

                        _dtsupport.BoxStatusSAP = new StatusSAP[1];
                        StatusSAP dtsupport = new StatusSAP();

                        _dtsupport.BoxStatusSAP[0] = dtsupport;

                        _dtsupport = callServiceDevices(_urlGetDowntime, _dtsupport);


                        ddl_downtime_edit.DataSource = _dtsupport.BoxStatusSAP;
                        ddl_downtime_edit.DataTextField = "downtime_name";
                        ddl_downtime_edit.DataValueField = "dtidx";
                        ddl_downtime_edit.DataBind();
                        ddl_downtime_edit.SelectedValue = lbDTIDX.Text;*/


                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int m0_unidx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_update");
                var ddStatus_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatus_update");

                GvMaster.EditIndex = -1;

                ViewState["m0_unidx_update"] = m0_unidx;
                ViewState["txtname_update"] = txtname_update.Text;
                ViewState["ddStatus_update"] = ddStatus_update.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion


    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAddHolder":

                btnaddholder.Visible = false;
                Panel_Add.Visible = true;
                break;

            case "btnCancel":
                btnaddholder.Visible = true;
                Panel_Add.Visible = false;

                break;

            case "btnAdd":
                Insert_Status();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "CmdDel":

                int m0_unidx = int.Parse(cmdArg);
                ViewState["m0_unidx_delete"] = m0_unidx;
                Delete_Master_List();
                SelectMasterList();

                break;
        }



    }
    #endregion

}
