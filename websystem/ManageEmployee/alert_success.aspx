﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="alert_success.aspx.cs" Inherits="websystem_ManageEmployee_alert_success" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <div class="container">

        <div style="padding-top: 20px;"></div>
        <asp:Panel ID="showalert" runat="server" Visible="true">
            <div class="col-sm-12" id="divShowalert">
                <div class="form-group">
                     <div class="col-sm-3"></div>
                    <div class="col-sm-4">
                        <div class="alert alert-success" role="alert">
                           <h2><strong><i class="glyphicon glyphicon-ok-sign"></i></strong>  &nbsp;บันทึกข้อมูลสำเร็จ</h2>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>

