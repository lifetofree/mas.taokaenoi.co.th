﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="epn_certificate.aspx.cs" Inherits="websystem_EPN_epn_certificate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <style type="text/css">
        .bg-system {
            background-image: url('../../images/qmr-problem/bg1.png');
            background-size: 100% 100%;
            /*width: 50%;*/
            height: 100%;
            text-align: center;
        }
    </style>

    <style type="text/css">
        .bg-big {
            background-image: url('../../images/qmr-problem/bg.png');
            background-size: 100% 100%;
            /*width: 50%;*/
            height: 100%;
            text-align: left;
        }
    </style>

    <style type="text/css">
        .bg-detail1 {
            background-image: url('../../images/qmr-problem/bg_detail1.png');
            background-size: 100% 100%;
            /*width: 50%;*/
            height: 50%;
            text-align: center;
        }
    </style>


    <style type="text/css">
        .bg-color-active {
            background-color: #979797;
        }
    </style>

    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="color: black;">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden; background-color: #f0f0f0" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left" runat="server">

                    <li id="_divMenuLiToViewIndex" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivIndex" runat="server"
                            CommandName="_divMenuBtnToDivIndex" ForeColor="black"
                            OnCommand="btnCommand" Text="รายการทั่วไป" />
                    </li>


                    <li id="_divMenuLiToViewAdd" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAdd" runat="server"
                            CommandName="_divMenuBtnToDivAdd" ForeColor="black"
                            OnCommand="btnCommand" Text="แจ้งปัญหา" />
                    </li>


                    <li id="_divMenuLiToViewApprove" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivApprove" runat="server"
                            CommandName="_divMenuBtnToDivApprove" ForeColor="black"
                            OnCommand="btnCommand" Text="รายการรออนุมัติ / รายการรอแก้ไข">
                            <asp:Label ID="nav_approve" Font-Bold="true" runat="server"></asp:Label>
                        </asp:LinkButton>

                    </li>


                    <li id="_divMenuLiToViewMaster" runat="server" class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" style="color: black;" aria-haspopup="true" aria-expanded="false">Master Data 
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li id="_divMenuLiToViewMaster_GroupProduct" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterGroupProduct" runat="server"
                                    CommandName="_divMenuBtnToDivMasterGroupProduct" ForeColor="black"
                                    OnCommand="btnCommand" Text="กลุ่มวัตถุดิบ" />
                            </li>

                            <li id="_divMenuLiToViewMaster_Product" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterProduct" runat="server"
                                    CommandName="_divMenuBtnToDivMasterProduct" ForeColor="black"
                                    OnCommand="btnCommand" Text="ประเภทวัตถุดิบ" />
                            </li>

                            <li id="_divMenuLiToViewMaster_Product_M1" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterProduct_M1" runat="server"
                                    CommandName="_divMenuBtnToDivMasterProduct_M1" ForeColor="black"
                                    OnCommand="btnCommand" Text="โครงสร้างวัตถุดิบ" />
                            </li>


                            <li id="_divMenuLiToViewMaster_Product_M2" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterProduct_M2" runat="server"
                                    CommandName="_divMenuBtnToDivMasterProduct_M2" ForeColor="black"
                                    OnCommand="btnCommand" Text="โครงสร้างย่อยวัตถุดิบ" />
                            </li>

                            <li id="_divMenuLiToViewMaster_Unit" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterUnit" runat="server"
                                    CommandName="_divMenuBtnToDivMasterUnit" ForeColor="black"
                                    OnCommand="btnCommand" Text="หน่วยวัด" />
                            </li>
                            <li id="_divMenuLiToViewMaster_Place" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterPlace" runat="server"
                                    CommandName="_divMenuBtnToDivMasterPlace" ForeColor="black"
                                    OnCommand="btnCommand" Text="สถานที่" />
                            </li>
                            <li id="_divMenuLiToViewMaster_Problem" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterProblem" runat="server"
                                    CommandName="_divMenuBtnToDivMasterProblem" ForeColor="black"
                                    OnCommand="btnCommand" Text="หัวข้อปัญหา" />
                            </li>
                            <li id="_divMenuLiToViewMaster_Supplier" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterSupplier" runat="server"
                                    CommandName="_divMenuBtnToDivMasterSupplier" ForeColor="black"
                                    OnCommand="btnCommand" Text="Supplier" />
                            </li>
                            <li id="_divMenuLiToViewMaster_Mat" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterMat" runat="server"
                                    CommandName="_divMenuBtnToDivMasterMat" ForeColor="black"
                                    OnCommand="btnCommand" Text="Material" />
                            </li>
                            <li id="_divMenuLiToViewMaster_MatRisk" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterMatRisk" runat="server"
                                    CommandName="_divMenuBtnToDivMasterMatRisk" ForeColor="black"
                                    OnCommand="btnCommand" Text="Material Risk" />
                            </li>
                        </ul>
                    </li>


                    <li id="_divMenuLiToViewReport" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivReport" runat="server"
                            CommandName="_divMenuBtnToDivReport" ForeColor="black"
                            OnCommand="btnCommand" Text="รายงาน">
                            <asp:Label ID="Label43" Font-Bold="true" runat="server"></asp:Label>
                        </asp:LinkButton>

                    </li>


                </ul>

                <ul class="nav navbar-nav navbar-right" runat="server">

                    <li id="_divMenuLiToViewManual" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDivManual" runat="server"
                            NavigateUrl="https://docs.google.com/document/d/1FgvVEkpyIPkI9wnyo13-sjcoZHpCjdyrcCuOkxR8hBw/edit?usp=sharing" Target="_blank"
                            CommandName="_divMenuBtnToDivManual"
                            OnCommand="btnCommand" Text="คู่มือการใช้งาน" />
                    </li>


                </ul>
            </div>
        </nav>
    </div>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="col-lg-12 ">
                <div class="bg-system">
                    <asp:Image ID="Image4" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr.png" Style="height: 100%; width: 100%;" />
                </div>
            </div>
            <br />
            <br />
            <div class="col-lg-12  bg-big">

                <div class="panel panel-danger" style="border-width: 2pt">
                    <div class="panel-heading" style="background-color: #ff9999; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">


                            <div class="form-group">
                                <asp:Label ID="Label55" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:DropDownList ID="ddlSearchDate_List" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="SelectedIndexChanged">
                                        <asp:ListItem Value="0">เลือกเงื่อนไข</asp:ListItem>
                                        <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                        <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                        <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="col-sm-3 ">

                                    <div class='input-group date'>
                                        <asp:TextBox ID="AddStartdate_List" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator37" ValidationGroup="SearchList" runat="server" Display="None"
                                            ControlToValidate="AddStartdate_List" Font-Size="11"
                                            ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                            ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender55" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator37" Width="160" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class='input-group date'>
                                        <asp:TextBox ID="AddEndDate_List" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                    </div>
                                </div>
                                <p class="help-block"><font color="red">**</font></p>
                            </div>



                            <div class="form-group">
                                <asp:Label ID="Label143" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlgroupproduct_List" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <asp:Label ID="Label123" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทวัตถุดิบ : " />
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddltypeproduct_List" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0">กรุณาเลือกประเภทวัตถุดิบ....</asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            </div>


                            <div class="form-group">
                                <asp:Label ID="Label144" runat="server" Text="โครงสร้างวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddltypeproduct_list_m1" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="0">กรุณาโครงสร้างวัตถุดิบ...</asp:ListItem>
                                    </asp:DropDownList>


                                </div>
                                <asp:Label ID="Label145" runat="server" Text="โครงสร้างย่อยวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddltypeproduct_list_m2" ValidationGroup="Save" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="0">กรุณาเลือกโครงสร้างย่อยวัตถุดิบ...</asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            </div>

                            <div class="form-group">

                                <asp:Label ID="Label124" CssClass="col-sm-2 control-label" runat="server" Text="ปัญหาที่พบ : " />
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlproblem_List" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0">กรุณาเลือกปัญหาที่พบ....</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <asp:Label ID="Label125" CssClass="col-sm-2 control-label" runat="server" Text="Supplier : " />
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlsupplier_List" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0">กรุณาเลือกSupplier....</asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success btn-sm" ValidationGroup="SearchList" runat="server" CommandName="btnsearch_list" OnCommand="btnCommand" title="Search"><i class="fa fa-search"></i> Search</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton17" CssClass="btn btn-default  btn-sm" runat="server" CommandName="btnrefresh_list" OnCommand="btnCommand" title="Refresh"><i class="glyphicon glyphicon-refresh"></i> Refresh</asp:LinkButton>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div style="overflow-x: auto; width: 100%">
                    <asp:GridView ID="GvIndex" runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        HeaderStyle-BackColor="#ff9999"
                        HeaderStyle-Height="40px"
                        AllowPaging="true"
                        BorderStyle="Solid"
                        HeaderStyle-BorderWidth="2pt"
                        PageSize="10"
                        OnRowDataBound="Master_RowDataBound"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        DataKeyNames="u0_docidx">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="รหัสรายการ" ItemStyle-BorderWidth="2pt" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-BackColor="#ffe3dd" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                        <asp:Label ID="lblu1_docidx" runat="server" Visible="false" Text='<%# Eval("u1_docidx") %>'></asp:Label>
                                        <asp:Label ID="lbldoccode" runat="server" Text='<%# Eval("doc_code") %>'></asp:Label>
                                        <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                        <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                        <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ข้อมูลสถานที่" ItemStyle-BorderWidth="2pt" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-BackColor="#fff0ec" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                <ItemTemplate>

                                    <small>
                                        <strong>
                                            <asp:Label ID="Labesl15" runat="server">สถานที่: </asp:Label></strong>
                                        <asp:Literal ID="litLocIDX" Visible="false" runat="server" Text='<%# Eval("LocIDX") %>' />
                                        <asp:Literal ID="LitLocName" runat="server" Text='<%# Eval("LocName") %>' />
                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labeld24" runat="server">อาคาร: </asp:Label></strong>
                                        <asp:Literal ID="litBuildingIDX" Visible="false" runat="server" Text='<%# Eval("BuildingIDX") %>' />
                                        <asp:Literal ID="LitBuildingName" runat="server" Text='<%# Eval("BuildingName") %>' />
                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Label25" runat="server">ไลน์ผลิต: </asp:Label></strong>
                                        <asp:Literal ID="litm0_plidx" Visible="false" runat="server" Text='<%# Eval("m0_plidx") %>' />
                                        <asp:Literal ID="Litproduction_name" runat="server" Text='<%# Eval("production_name") %>' />
                                        </p>
                                 
                                    </small>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ข้อมูลผู้แจ้ง" ItemStyle-BorderWidth="2pt" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-BackColor="#ffe3dd" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                <ItemTemplate>
                                    <small>
                                        <strong>
                                            <asp:Label ID="Labeslr15" runat="server">องค์กร: </asp:Label></strong>
                                        <asp:Literal ID="litorg_idx" Visible="false" runat="server" Text='<%# Eval("orgidx") %>' />
                                        <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("org_name_th") %>' />
                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labelsd24" runat="server">ฝ่าย: </asp:Label></strong>
                                        <asp:Literal ID="litrdept_idx" Visible="false" runat="server" Text='<%# Eval("rdeptidx") %>' />
                                        <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labsel25" runat="server">แผนก: </asp:Label></strong>
                                        <asp:Literal ID="litrsec_idx" Visible="false" runat="server" Text='<%# Eval("rsecidx") %>' />
                                        <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                        </p>
                                                 
                                                 <strong>
                                                     <asp:Label ID="Label26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                        <asp:Literal ID="litcemp_idx" Visible="false" runat="server" Text='<%# Eval("CEmpIDX") %>' />
                                        </p>
                                    </small>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ข้อมูลแจ้งปัญหา" ItemStyle-BorderWidth="2pt" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-BackColor="#fff0ec" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                <ItemTemplate>
                                    <small>
                                        <strong>
                                            <asp:Label ID="Label5" runat="server">กลุ่มวัตถุดิบ: </asp:Label></strong>
                                        <asp:Literal ID="litgroup_name" runat="server" Text='<%# Eval("group_name") %>' />
                                        </p>
                                                <strong>
                                                    <asp:Label ID="Labsel15" runat="server">ประเภทวัตถุดิบ: </asp:Label></strong>
                                        <asp:Literal ID="littypeproduct_name" runat="server" Text='<%# Eval("typeproduct_name") %>' />
                                        </p>
                                                <strong>
                                                    <asp:Label ID="Label10" runat="server">โครงสร้างวัตถุดิบ: </asp:Label></strong>
                                        <asp:Literal ID="littypeproduct_name_m1" runat="server" Text='<%# Eval("typeproduct_name_m1") %>' />
                                        </p>
                                                <strong>
                                                    <asp:Label ID="Label11" runat="server">โครงสร้างย่อยวัตถุดิบ: </asp:Label></strong>
                                        <asp:Literal ID="littypeproduct_name_m2" runat="server" Text='<%# Eval("typeproduct_name_m2") %>' />
                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labqewl24" runat="server">ประเภทปัญหา : </asp:Label></strong>
                                        <asp:Literal ID="litproblem_name" runat="server" Text='<%# Eval("problem_name") %>' />
                                        </p>
                                  <strong>
                                      <asp:Label ID="Label29" runat="server">วันที่แจ้ง : </asp:Label></strong>
                                        <asp:Literal ID="litcreate_date" runat="server" Text='<%# Eval("create_date") %>' />
                                        </p>
                                    </small>
                                    <%--  <small>
                                    <strong>
                                        <asp:Label ID="Labsel15" runat="server">ประเภทวัตถุดิบ: </asp:Label></strong>
                                    <asp:Literal ID="litsumheadcore" runat="server" Text='<%# Eval("typeproduct_name") %>' />
                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labqel24" runat="server">ประเภทปัญหา : </asp:Label></strong>
                                    <asp:Literal ID="litproblem_name" runat="server" Text='<%# Eval("problem_name") %>' />
                                    </p>
                                  <strong>
                                      <asp:Label ID="Label29" runat="server">วันที่แจ้ง : </asp:Label></strong>
                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("create_date") %>' />
                                    </p>
                                </small>--%>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-BorderWidth="2pt" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-BackColor="#ffe3dd" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                <ItemTemplate>
                                    <small>
                                        <b>
                                            <asp:Label ID="lblStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                        </b>
                                    </small>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="10%" ItemStyle-BorderWidth="2pt" HeaderStyle-CssClass="text-center" ItemStyle-BackColor="#fff0ec" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnviewdetail" CssClass="btn btn-warning btn-sm" runat="server" OnCommand="btnCommand" ToolTip="View" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("doc_code")+ ";" + Eval("CEmpIDX")  + ";" + "1"%>'><i class="	fa fa-book"></i></asp:LinkButton>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewInsert" runat="server">
            <%--<div class="col-md-12" id="div1" runat="server" style="color: transparent;">
                <asp:FileUpload ID="FileUpload1" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />
            </div>--%>
            <div class="col-lg-12">
                <div class="bg-system">

                    <asp:Image ID="Image6" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr.png" Style="width: 100%;" />
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-danger" style="border-width: 2pt">
                    <div class="panel-heading" style="background-color: #ff9999; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body ">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel panel-danger" style="border-width: 2pt">
                    <div class="panel-heading" style="background-color: #ff9999; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; ข้อมูลแจ้งปัญหา</strong></h3>
                    </div>
                    <asp:FormView ID="FvinsertProblem" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <asp:Label ID="Label5" runat="server" Text="สถานที่" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddllocation_problem" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddllocation_problem" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานที่"
                                                ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                            <%--                                            <p class="help-block"><font color="red">**</font></p>--%>
                                        </div>

                                        <asp:Label ID="Label6" runat="server" Text="อาคาร" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlbuilding_problem" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกอาคาร...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlbuilding_problem" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกอาคาร"
                                                ValidationExpression="กรุณาเลือกอาคาร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label11" runat="server" Text="ไลน์ผลิต" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlproduction_problem" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกไลน์ผลิต...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlproduction_problem" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกไลน์ผลิต"
                                                ValidationExpression="กรุณาเลือกไลน์ผลิต" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label143" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlgroupproduct_problem" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator41" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlgroupproduct_problem" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกกลุ่มวัตถุดิบ"
                                                ValidationExpression="กรุณาเลือกกลุ่มวัตถุดิบ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender60" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator41" Width="160" />

                                        </div>
                                        <asp:Label ID="Label14" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypeproduct_problem" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกประเภทวัตถุดิบ...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltypeproduct_problem" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทวัตถุดิบ"
                                                ValidationExpression="กรุณาเลือกประเภทวัตถุดิบ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label144" runat="server" Text="โครงสร้างวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypeproduct_problem_m1" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาโครงสร้างวัตถุดิบ...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator42" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltypeproduct_problem_m1" Font-Size="11"
                                                ErrorMessage="กรุณาโครงสร้างวัตถุดิบ"
                                                ValidationExpression="กรุณาโครงสร้างวัตถุดิบ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender61" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator42" Width="160" />

                                        </div>
                                        <asp:Label ID="Label145" runat="server" Text="โครงสร้างย่อยวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypeproduct_problem_m2" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกโครงสร้างย่อยวัตถุดิบ...</asp:ListItem>
                                            </asp:DropDownList>

                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator43" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltypeproduct_problem_m2" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกโครงสร้างย่อยวัตถุดิบ"
                                                ValidationExpression="กรุณาเลือกโครงสร้างย่อยวัตถุดิบ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender62" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator43" Width="160" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label16" runat="server" Text="Mat No" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlmat_no" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlmat_no" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกเลข Mat"
                                                ValidationExpression="กรุณาเลือกเลข Mat" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                        </div>

                                        <asp:Label ID="Label17" runat="server" Text="Mat Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtmat_name" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtmat_name" Font-Size="11"
                                                ErrorMessage="กรุณาระบุ Mat Name"
                                                ValidationExpression="กรุณาระบุ Mat Name"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatoarCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกตัวอักษรมากกว่า 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtmat_name"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                        </div>

                                    </div>

                                    <div class="form-group" runat="server" visible="false">
                                        <asp:Label ID="Label15" runat="server" Text="Mat" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:CheckBox ID="chkmat" runat="server" OnCheckedChanged="SelectedIndexChanged" AutoPostBack="true" />
                                            <label for="chkmat" class="control-label" style="font-weight: normal">เลข Mat สินค้า</label>
                                            <%-- <asp:Label ID="Label16" runat="server" Text="ไม่มีเลข Mat สินค้า" CssClass="col-sm-3 control-label text_right"></asp:Label>--%>
                                        </div>

                                        <div id="divmat_no" runat="server" visible="false">
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label18" runat="server" Text="Batch" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtbatch_name" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtbatch_name" Font-Size="11"
                                                ErrorMessage="กรุณาระบุ Batch Name"
                                                ValidationExpression="กรุณาระบุ Batch Name"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกตัวอักษรมากกว่า 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtbatch_name"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator5" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label19" runat="server" Text="Supplier Name" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:DropDownList ID="ddl_sup" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddl_sup" Font-Size="11"
                                                ErrorMessage="กรุณาเลือก Supplier"
                                                ValidationExpression="กรุณาเลือก Supplier" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />

                                        </div>
                                    </div>

                                    <hr />
                                    <div class="form-group">
                                        <asp:Label ID="Label82" runat="server" Text="วันที่ผลิต" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtdatemodify" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator22" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txtdatemodify" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกวันที่ผลิต"
                                                    ValidationExpression="กรุณาเลือกวันที่ผลิต" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender36" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator22" Width="160" />
                                            </div>
                                        </div>
                                        <asp:Label ID="Label83" runat="server" Text="วันที่หมดอายุ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtdateexp" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label84" runat="server" Text="วันที่พบปัญหา" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtdateproblem" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator25" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txtdateproblem" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกวันที่พบปัญหา"
                                                    ValidationExpression="กรุณาเลือกวันที่พบปัญหา" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender38" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator25" Width="160" />
                                            </div>
                                        </div>
                                        <asp:Label ID="Label20" runat="server" Text="วันที่รับเข้า" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtgetdate" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txtgetdate" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกวันที่รับเข้า"
                                                    ValidationExpression="กรุณาเลือกวันที่รับเข้า" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label21" runat="server" Text="จำนวนที่รับเข้า" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:TextBox ID="txtqtyget" runat="server" CssClass="form-control "></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="txtqtyget" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวนที่รับเข้า" />
                                            <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="Save" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtqtyget"
                                                ValidationExpression="^[0-9]{1,10}.*$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                        </div>
                                        <asp:Label ID="Label22" runat="server" Text="หน่วย" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlunit_get" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกหน่วย...</asp:ListItem>

                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlunit_get" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกหน่วย"
                                                ValidationExpression="กรุณาเลือกหน่วย" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label23" runat="server" Text="จำนวนที่พบปัญหา" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:TextBox ID="txtqty_problem" OnTextChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control "></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="txtqty_problem" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวนที่พบปัญหา" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationGroup="Save" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtqty_problem"
                                                ValidationExpression="^[0-9]{1,10}.*$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator6" Width="160" />

                                        </div>
                                        <asp:Label ID="Label24" runat="server" Text="หน่วย" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlunit_problem" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกหน่วย...</asp:ListItem>

                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlunit_problem" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกหน่วย"
                                                ValidationExpression="กรุณาเลือกหน่วย" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator16" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label25" runat="server" Text="จำนวนสุ่ม" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtqty_random" Text="0" OnTextChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control "></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ValidationGroup="Save" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtqty_random"
                                                ValidationExpression="^[0-9]{1,10}.*$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender25" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator7" Width="160" />

                                        </div>
                                        <asp:Label ID="Label27" runat="server" Text="หน่วย" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlunit_random" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกหน่วย...</asp:ListItem>

                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label28" runat="server" Text="ประเภทปัญหา" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-9">
                                            <asp:DropDownList ID="ddltype_problem" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกประเภทปัญหา...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltype_problem" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทปัญหา"
                                                ValidationExpression="กรุณาเลือกประเภทปัญหา" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator17" Width="160" />
                                            <p class="help-block" style="color: red">
                                                <font color="red">**จำนวนปัญหาที่เคยพบในปีนี้ </font>
                                                <asp:Label ID="lblcount_problem" runat="server" Text="0" CssClass="control-label"></asp:Label>
                                                ครั้ง / จำนวนปัญหาที่พบตั้งแต่ปี 2020 
                                                <asp:Label ID="lblcount_problem_all" runat="server" Text="0" CssClass="control-label"></asp:Label>
                                                ครั้ง
                                            </p>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Labedle1" class="col-sm-2 control-label" runat="server" Text="รายละเอียดเพิ่มเติม : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtremark" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtremark" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียดเพิ่มเติม"
                                                ValidationExpression="กรุณากรอกรายละเอียดเพิ่มเติม"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                        </div>


                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <asp:Label ID="Label10s" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-sm-7">
                                                    <asp:FileUpload ID="UploadFileProblem" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi" accept="jpg|pdf" />

                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator18"
                                                        runat="server" ControlToValidate="UploadFileProblem" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="กรุณาอัพโหลดไฟล์"
                                                        ValidationGroup="Save" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender26" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator18" Width="200" PopupPosition="BottomLeft" />--%>

                                                    <small>
                                                        <p class="help-block"><font color="red">**Attach File name jpg,  pdf</font></p>
                                                    </small>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group" runat="server">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="Save" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </InsertItemTemplate>
                    </asp:FormView>

                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewApprove" runat="server">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12">
                        <div class="bg-system">
                            <asp:Image ID="Image5" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr_approve.png" Style="height: 100%; width: 100%;" />
                        </div>

                    </div>
                    <br />
                    <br />
                    <div class="col-lg-12" style="overflow-x: auto; width: 100%">

                        <%-- <div class="alert-message alert-message-success">
                            <blockquote class="danger" style="font-size: small; background-color: #A7DB42; text-align: left">
                                <h5><b>
                                    <asp:Label ID="Label55" runat="server">รายการแจ้งปัญหา</asp:Label></b></h5>
                            </blockquote>
                        </div>--%>


                        <asp:GridView ID="GvApprove" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            HeaderStyle-Height="40px"
                            HeaderStyle-BackColor="#ff9999"
                            AllowPaging="true"
                            BorderStyle="Solid"
                            HeaderStyle-BorderWidth="2pt"
                            PageSize="10"
                            OnRowDataBound="Master_RowDataBound"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            DataKeyNames="u0_docidx">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="รหัสรายการ" ItemStyle-Width="5%" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#ffe3dd" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                            <asp:Label ID="lblu1_docidx" runat="server" Visible="false" Text='<%# Eval("u1_docidx") %>'></asp:Label>
                                            <asp:Label ID="lbldoccode" runat="server" Text='<%# Eval("doc_code") %>'></asp:Label>
                                            <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                            <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                            <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ข้อมูลสถานที่" ItemStyle-Width="20%" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#fff0ec" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <small>
                                            <strong>
                                                <asp:Label ID="Labesl15" runat="server">สถานที่: </asp:Label></strong>
                                            <asp:Literal ID="litLocIDX" Visible="false" runat="server" Text='<%# Eval("LocIDX") %>' />
                                            <asp:Literal ID="LitLocName" runat="server" Text='<%# Eval("LocName") %>' />
                                            </p>
                                                 <strong>
                                                     <asp:Label ID="Labeld24" runat="server">อาคาร: </asp:Label></strong>
                                            <asp:Literal ID="litBuildingIDX" Visible="false" runat="server" Text='<%# Eval("BuildingIDX") %>' />
                                            <asp:Literal ID="LitBuildingName" runat="server" Text='<%# Eval("BuildingName") %>' />
                                            </p>
                                                 <strong>
                                                     <asp:Label ID="Label25" runat="server">ไลน์ผลิต: </asp:Label></strong>
                                            <asp:Literal ID="litm0_plidx" Visible="false" runat="server" Text='<%# Eval("m0_plidx") %>' />
                                            <asp:Literal ID="Litproduction_name" runat="server" Text='<%# Eval("production_name") %>' />
                                            </p>
                                 
                                        </small>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ข้อมูลผู้แจ้ง" ItemStyle-Width="25%" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#ffe3dd" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <small>
                                            <strong>
                                                <asp:Label ID="Labeslr15" runat="server">องค์กร: </asp:Label></strong>
                                            <asp:Literal ID="litorg_idx" Visible="false" runat="server" Text='<%# Eval("orgidx") %>' />
                                            <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("org_name_th") %>' />
                                            </p>
                                                 <strong>
                                                     <asp:Label ID="Labelsd24" runat="server">ฝ่าย: </asp:Label></strong>
                                            <asp:Literal ID="litrdept_idx" Visible="false" runat="server" Text='<%# Eval("rdeptidx") %>' />
                                            <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                            </p>
                                                 <strong>
                                                     <asp:Label ID="Labsel25" runat="server">แผนก: </asp:Label></strong>
                                            <asp:Literal ID="litrsec_idx" Visible="false" runat="server" Text='<%# Eval("rsecidx") %>' />
                                            <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                            </p>
                                                 
                                                 <strong>
                                                     <asp:Label ID="Label26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                            <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                            <asp:Literal ID="litcemp_idx" Visible="false" runat="server" Text='<%# Eval("CEmpIDX") %>' />
                                            </p>
                                        </small>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ข้อมูลแจ้งปัญหา" ItemStyle-Width="15%" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#fff0ec" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <small>
                                            <strong>
                                                <asp:Label ID="Labsel15" runat="server">ประเภทวัตถุดิบ: </asp:Label></strong>
                                            <asp:Literal ID="litsumheadcore" runat="server" Text='<%# Eval("typeproduct_name") %>' />
                                            </p>
                                                 <strong>
                                                     <asp:Label ID="Labqel24" runat="server">ประเภทปัญหา : </asp:Label></strong>
                                            <asp:Literal ID="litproblem_name" runat="server" Text='<%# Eval("problem_name") %>' />
                                            </p>
                                  <strong>
                                      <asp:Label ID="Label29" runat="server">วันที่แจ้ง : </asp:Label></strong>
                                            <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("create_date") %>' />
                                            </p>
                                        </small>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="10%" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#ffe3dd" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <small>
                                            <b>
                                                <asp:Label ID="lblStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                            </b>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="10%" ItemStyle-BorderWidth="2pt" HeaderStyle-CssClass="text-center" ItemStyle-BackColor="#fff0ec" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnviewdetail" CssClass="btn btn-warning btn-sm" runat="server" OnCommand="btnCommand" ToolTip="View" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("doc_code")+ ";" + Eval("CEmpIDX")  + ";" + "2"%>'><i class="	fa fa-book"></i></asp:LinkButton>

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewDetail" runat="server">

            <div class="col-md-12" id="div2" runat="server" style="color: transparent;">
                <asp:FileUpload ID="FileUpload2" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|pdf" />
            </div>

            <div class="col-lg-12">
                <div class="bg-system">

                    <asp:Image ID="Image7" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr.png" Style="width: 100%;" />
                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel panel-danger" style="border-width: 2pt;">
                    <div class="panel-heading" style="background-color: #ff9999; color: #181818;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="Fvdetailusercreate" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtorgidx" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtsecidx" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel panel-danger" style="border-width: 2pt">
                    <div class="panel-heading" style="background-color: #ff9999; color: #181818;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; รายละเอียดการแจ้งปัญหา</strong></h3>
                    </div>
                    <asp:FormView ID="FvdetailProblem" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <ItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("unidx") %>' />
                                    <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("acidx") %>' />
                                    <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />

                                    <div class="form-group">
                                        <div class="col-md-2 col-sm-offset-10" id="div_btnedit" runat="server" visible="false">
                                            <asp:LinkButton ID="btnedit_form" CssClass="btn btn-sm btn-warning" OnCommand="btnCommand" CommandName="CmdEdit_Problem" CommandArgument='<%# Eval("u1_docidx") %>' runat="server"><i class="glyphicon glyphicon-pencil"></i> แก้ไขรายการ</asp:LinkButton>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label31" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="รหัสรายการ " />
                                        <div class="col-md-3">
                                            <asp:Label ID="Label33" class="control-labelnotop" Text='<%# Eval("doc_code") %>' runat="server"> </asp:Label>
                                            <asp:Label ID="lblu1_docidx" Visible="false" class="control-labelnotop" Text='<%# Eval("u1_docidx") %>' runat="server"> </asp:Label>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label30" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="สถานที่ " />
                                        <div class="col-md-3">
                                            <asp:Label ID="lbllocidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("LocIDX_ref") %>' runat="server"> </asp:Label>
                                            <asp:Label ID="lblLocIDX_original" Visible="false" class="control-labelnotop" Text='<%# Eval("LocIDX") %>' runat="server"> </asp:Label>
                                            <asp:Label ID="lbLocIDX_List" Visible="false" class="control-labelnotop" Text='<%# Eval("LocIDX_List") %>' runat="server"> </asp:Label>


                                            <asp:Label ID="Label32" class="control-labelnotop" Text='<%# Eval("LocName") %>' runat="server"> </asp:Label>
                                        </div>
                                        <asp:Label ID="Label22" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="อาคาร " />
                                        <div class="col-md-3">
                                            <asp:Label ID="txtdoc_code" class="control-labelnotop" Text='<%# Eval("BuildingName") %>' runat="server"> </asp:Label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label35" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="ไลน์ผลิต " />
                                        <div class="col-md-3">
                                            <asp:Label ID="lblm0_plidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_plidx") %>' runat="server"> </asp:Label>

                                            <asp:Label ID="Label36" class="control-labelnotop" Text='<%# Eval("production_name") %>' runat="server"> </asp:Label>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label146" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="กลุ่มวัตถุดิบ " />
                                        <div class="col-md-3">
                                            <asp:Label ID="lblm0_m0_group_tpidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_group_tpidx") %>' runat="server"> </asp:Label>

                                            <asp:Label ID="Label148" class="control-labelnotop" Text='<%# Eval("group_name") %>' runat="server"> </asp:Label>
                                        </div>
                                        <asp:Label ID="Label37" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="ประเภทวัตถุดิบ " />
                                        <div class="col-md-3">
                                            <asp:Label ID="lblm0_tpidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_tpidx") %>' runat="server"> </asp:Label>

                                            <asp:Label ID="Label38" class="control-labelnotop" Text='<%# Eval("typeproduct_name") %>' runat="server"> </asp:Label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label149" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="โครงสร้างวัตถุดิบ " />
                                        <div class="col-md-3">
                                            <asp:Label ID="lblm1_tpidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m1_tpidx") %>' runat="server"> </asp:Label>

                                            <asp:Label ID="Label151" class="control-labelnotop" Text='<%# Eval("typeproduct_name_m1") %>' runat="server"> </asp:Label>
                                        </div>
                                        <asp:Label ID="Label152" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="โครงสร้างย่อยวัตถุดิบ " />
                                        <div class="col-md-3">
                                            <asp:Label ID="lblm2_tpidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m2_tpidx") %>' runat="server"> </asp:Label>

                                            <asp:Label ID="Label154" class="control-labelnotop" Text='<%# Eval("typeproduct_name_m2") %>' runat="server"> </asp:Label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label16" runat="server" Text="Mat No" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:Label ID="lblmatidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("matidx") %>' runat="server"> </asp:Label>

                                            <asp:Label ID="lblmat_no" class="control-labelnotop" Text='<%# Eval("mat_no") %>' runat="server"> </asp:Label>
                                        </div>
                                        <asp:Label ID="Label17" runat="server" Text="Mat Name" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblmast_name_show" class="control-labelnotop" Text='<%# Eval("matname") %>' runat="server"> </asp:Label>

                                        </div>
                                    </div>

                                    <div class="form-group" runat="server" visible="false">
                                        <asp:Label ID="Label15" runat="server" Text="Mat" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:CheckBox ID="chkmat_show" runat="server" />
                                            <label for="chkmat" class="control-labelnotop" style="font-weight: normal">เลข Mat สินค้า</label>
                                            <%-- <asp:Label ID="Label16" runat="server" Text="ไม่มีเลข Mat สินค้า" CssClass="col-sm-3 control-label text_right"></asp:Label>--%>
                                        </div>

                                        <div id="divmatshow_no" runat="server" visible="false">
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label18" runat="server" Text="Batch" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblbatchnumber" class="control-labelnotop" Text='<%# Eval("batchnumber") %>' runat="server"> </asp:Label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label19" runat="server" Text="Supplier Name" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblm0_supidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_supidx") %>' runat="server"> </asp:Label>

                                            <asp:Label ID="Label44" class="control-labelnotop" Text='<%# Eval("sup_name") %>' runat="server"> </asp:Label>

                                        </div>

                                        <asp:Label ID="Label135" runat="server" Text="จำนวนครั้งแจ้งปัญหา" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblcount_sup" class="control-labelnotop" Text='<%# Eval("count_sup") %>' runat="server"> </asp:Label>
                                            <asp:Label ID="Label137" class="control-labelnotop" Text="ครั้ง" runat="server"> </asp:Label>

                                        </div>
                                    </div>

                                    <hr />

                                    <div class="form-group">
                                        <asp:Label ID="Label85" runat="server" Text="วันที่ผลิต" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lbldatemodify" class="control-labelnotop" Text='<%# Eval("datemodify") %>' runat="server"> </asp:Label>

                                        </div>
                                        <asp:Label ID="Label87" runat="server" Text="วันที่หมดอายุ" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lbldateexp" class="control-labelnotop" Text='<%# Eval("dateexp") %>' runat="server"> </asp:Label>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label89" runat="server" Text="วันที่พบปัญหา" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lbldateproblem" class="control-labelnotop" Text='<%# Eval("dateproblem") %>' runat="server"> </asp:Label>

                                        </div>
                                        <asp:Label ID="Label20" runat="server" Text="วันที่รับเข้า" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lbldategetproduct" class="control-labelnotop" Text='<%# Eval("dategetproduct") %>' runat="server"> </asp:Label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label21" runat="server" Text="จำนวนที่รับเข้า" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:Label ID="lblqty_get" class="control-labelnotop" Text='<%# Eval("qty_get") %>' runat="server"> </asp:Label>

                                        </div>
                                        <asp:Label ID="Label39" runat="server" Text="หน่วย" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblunit_show" Visible="false" class="control-labelnotop" Text='<%# Eval("unit_get") %>' runat="server"> </asp:Label>

                                            <asp:Label ID="Label47" class="control-labelnotop" Text='<%# Eval("unit_name") %>' runat="server"> </asp:Label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label23" runat="server" Text="จำนวนที่พบปัญหา" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:Label ID="lblqty_problem" class="control-labelnotop" Text='<%# Eval("qty_problem") %>' runat="server"> </asp:Label>

                                        </div>
                                        <asp:Label ID="Label24" runat="server" Text="หน่วย" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblunit_problem_show" Visible="false" class="control-labelnotop" Text='<%# Eval("unit_problem") %>' runat="server"> </asp:Label>

                                            <asp:Label ID="Label49" class="control-labelnotop" Text='<%# Eval("unit_name_problem") %>' runat="server"> </asp:Label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label25" runat="server" Text="จำนวนสุ่ม" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblqty_random" class="control-labelnotop" Text='<%# Eval("qty_random") %>' runat="server"> </asp:Label>

                                        </div>
                                        <asp:Label ID="Label27" runat="server" Text="หน่วย" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:Label ID="lblunit_random_show" Visible="false" class="control-labelnotop" Text='<%# Eval("unit_random") %>' runat="server"> </asp:Label>

                                            <asp:Label ID="Label51" class="control-labelnotop" Text='<%# Eval("unit_name_random") %>' runat="server"> </asp:Label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label28" runat="server" Text="ประเภทปัญหา" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-9">
                                            <asp:Label ID="lblm0_pridx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_pridx") %>' runat="server"> </asp:Label>

                                            <asp:Label ID="Label52" class="control-labelnotop" Text='<%# Eval("problem_name") %>' runat="server"> </asp:Label>
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Labedle1" class="col-sm-2 control-labelnotop text_right" runat="server" Text="รายละเอียดเพิ่มเติม : " />
                                        <div class="col-sm-9">
                                            <asp:Label ID="lbldetail_remark" class="control-labelnotop" Text='<%# Eval("detail_remark") %>' runat="server"> </asp:Label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label40" CssClass="col-sm-1 control-label" runat="server" />

                                        <div class="col-lg-8">
                                            <asp:GridView ID="gvFile" Visible="true" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                                HeaderStyle-CssClass="default"
                                                OnRowDataBound="Master_RowDataBound"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                Font-Size="Small">

                                                <Columns>
                                                    <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                        <ItemTemplate>
                                                            <div class="col-lg-10">
                                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                            </div>
                                                            <div class="col-lg-2">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                            <h4></h4>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <asp:Label ID="Label5" runat="server" Text="สถานที่" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lbllocidx_edit" Visible="false" class="control-labelnotop" Text='<%# Eval("LocIDX") %>' runat="server"> </asp:Label>
                                            <asp:DropDownList ID="ddllocation_problemedit" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddllocation_problemedit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานที่"
                                                ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                        </div>
                                        <asp:Label ID="Label6" runat="server" Text="อาคาร" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblBuildingIDX_edit" Visible="false" class="control-labelnotop" Text='<%# Eval("BuildingIDX") %>' runat="server"> </asp:Label>

                                            <asp:DropDownList ID="ddlbuilding_problemedit" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกอาคาร...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlbuilding_problemedit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกอาคาร"
                                                ValidationExpression="กรุณาเลือกอาคาร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label11" runat="server" Text="ไลน์ผลิต" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblm0_plidx_edit" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_plidx") %>' runat="server"> </asp:Label>

                                            <asp:DropDownList ID="ddlproduction_problemedit" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกไลน์ผลิต...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlproduction_problemedit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกไลน์ผลิต"
                                                ValidationExpression="กรุณาเลือกไลน์ผลิต" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label146" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="กลุ่มวัตถุดิบ " />


                                        <div class="col-sm-3">
                                            <asp:Label ID="lblm0_m0_group_tpidx_edit" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_group_tpidx") %>' runat="server"> </asp:Label>

                                            <asp:DropDownList ID="ddlgroupproduct_problemedit" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator41" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlgroupproduct_problemedit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกกลุ่มวัตถุดิบ"
                                                ValidationExpression="กรุณาเลือกกลุ่มวัตถุดิบ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender60" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator41" Width="160" />

                                        </div>
                                        <asp:Label ID="Label14" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblm0_tpidx_edit" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_tpidx") %>' runat="server"> </asp:Label>

                                            <asp:DropDownList ID="ddltypeproduct_problemedit" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกประเภทวัตถุดิบ...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltypeproduct_problemedit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทวัตถุดิบ"
                                                ValidationExpression="กรุณาเลือกประเภทวัตถุดิบ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label149" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="โครงสร้างวัตถุดิบ " />
                                        <div class="col-md-3">
                                            <asp:Label ID="lblm1_tpidx_edit" Visible="false" class="control-labelnotop" Text='<%# Eval("m1_tpidx") %>' runat="server"> </asp:Label>

                                            <asp:DropDownList ID="ddltypeproduct_problemedit_m1" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาโครงสร้างวัตถุดิบ...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator42" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltypeproduct_problemedit_m1" Font-Size="11"
                                                ErrorMessage="กรุณาโครงสร้างวัตถุดิบ"
                                                ValidationExpression="กรุณาโครงสร้างวัตถุดิบ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender61" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator42" Width="160" />
                                        </div>
                                        <asp:Label ID="Label152" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="โครงสร้างย่อยวัตถุดิบ " />
                                        <div class="col-md-3">
                                            <asp:Label ID="lblm2_tpidx_edit" Visible="false" class="control-labelnotop" Text='<%# Eval("m2_tpidx") %>' runat="server"> </asp:Label>

                                            <asp:DropDownList ID="ddltypeproduct_problemedit_m2" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกโครงสร้างย่อยวัตถุดิบ...</asp:ListItem>
                                            </asp:DropDownList>

                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator43" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltypeproduct_problemedit_m2" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกโครงสร้างย่อยวัตถุดิบ"
                                                ValidationExpression="กรุณาเลือกโครงสร้างย่อยวัตถุดิบ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender62" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator43" Width="160" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group" runat="server">
                                        <asp:Label ID="Label16" runat="server" Text="Mat No" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblmatidx_edit" Visible="false" class="control-labelnotop" Text='<%# Eval("matidx") %>' runat="server"> </asp:Label>

                                            <asp:DropDownList ID="ddlmat_noedit" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlmat_noedit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกเลข Mat"
                                                ValidationExpression="กรุณาเลือกเลข Mat" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                        </div>
                                        <asp:Label ID="Label17" runat="server" Text="Mat Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtmat_name_edit" Text='<%# Eval("matname") %>' Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtmat_name_edit" Font-Size="11"
                                                ErrorMessage="กรุณาระบุ Mat Name"
                                                ValidationExpression="กรุณาระบุ Mat Name"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatoarCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกตัวอักษรมากกว่า 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtmat_name_edit"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                        </div>

                                    </div>

                                    <div class="form-group" runat="server" visible="false">
                                        <asp:Label ID="Label15" runat="server" Text="Mat" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:CheckBox ID="chkmat_edit" runat="server" OnCheckedChanged="SelectedIndexChanged" AutoPostBack="true" />
                                            <label for="chkmat" class="control-label" style="font-weight: normal">เลข Mat สินค้า</label>
                                            <%-- <asp:Label ID="Label16" runat="server" Text="ไม่มีเลข Mat สินค้า" CssClass="col-sm-3 control-label text_right"></asp:Label>--%>
                                        </div>

                                        <div id="divmat_no_edit" runat="server" visible="false">
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label18" runat="server" Text="Batch" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtbatch_name_edit" Text='<%# Eval("batchnumber") %>' runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtbatch_name_edit" Font-Size="11"
                                                ErrorMessage="กรุณาระบุ Batch Name"
                                                ValidationExpression="กรุณาระบุ Batch Name"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกตัวอักษรมากกว่า 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtbatch_name_edit"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator5" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label19" runat="server" Text="Supplier Name" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblm0_supidx_edit" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_supidx") %>' runat="server"> </asp:Label>

                                            <asp:DropDownList ID="ddl_sup_edit" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddl_sup_edit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือก Supplier"
                                                ValidationExpression="กรุณาเลือก Supplier" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />

                                        </div>
                                    </div>

                                    <hr />
                                    <div class="form-group">
                                        <asp:Label ID="Label91" runat="server" Text="วันที่ผลิต" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtdatemodify_edit" Text='<%# Eval("datemodify") %>' runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator24" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txtdatemodify_edit" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกวันที่ผลิต"
                                                    ValidationExpression="กรุณาเลือกวันที่ผลิต" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender37" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator24" Width="160" />
                                            </div>
                                        </div>
                                        <asp:Label ID="Label92" runat="server" Text="วันที่หมดอายุ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtdateexp_edit" Text='<%# Eval("dateexp") %>' runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label93" runat="server" Text="วันที่พบปัญหา" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtdateproblem_edit" Text='<%# Eval("dateproblem") %>' runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator26" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txtdateproblem_edit" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกวันที่พบปัญหา"
                                                    ValidationExpression="กรุณาเลือกวันที่พบปัญหา" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender39" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator26" Width="160" />
                                            </div>
                                        </div>
                                        <asp:Label ID="Label20" runat="server" Text="วันที่รับเข้า" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtgetdate_edit" Text='<%# Eval("dategetproduct") %>' runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txtgetdate_edit" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกวันที่รับเข้า"
                                                    ValidationExpression="กรุณาเลือกวันที่รับเข้า" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label21" runat="server" Text="จำนวนที่รับเข้า" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:TextBox ID="txtqtyget_edit" Text='<%# Eval("qty_get") %>' runat="server" CssClass="form-control "></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="txtqtyget_edit" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวนที่รับเข้า" />
                                            <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="Save" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtqtyget_edit"
                                                ValidationExpression="^[0-9]{1,10}.*$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                        </div>
                                        <asp:Label ID="Label22" runat="server" Text="หน่วย" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblunit_edit" Visible="false" class="control-labelnotop" Text='<%# Eval("unit_get") %>' runat="server"> </asp:Label>

                                            <asp:DropDownList ID="ddlunit_getedit" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกหน่วย...</asp:ListItem>

                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlunit_getedit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกหน่วย"
                                                ValidationExpression="กรุณาเลือกหน่วย" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label23" runat="server" Text="จำนวนที่พบปัญหา" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:TextBox ID="txtqty_problemedit" Text='<%# Eval("qty_problem") %>' OnTextChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control "></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="txtqty_problemedit" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวนที่พบปัญหา" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationGroup="Save" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtqty_problemedit"
                                                ValidationExpression="^[0-9]{1,10}.*$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator6" Width="160" />

                                        </div>
                                        <asp:Label ID="Label24" runat="server" Text="หน่วย" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblunit_problemedit" Visible="false" class="control-labelnotop" Text='<%# Eval("unit_problem") %>' runat="server"> </asp:Label>

                                            <asp:DropDownList ID="ddlunit_problemedit" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกหน่วย...</asp:ListItem>

                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlunit_problemedit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกหน่วย"
                                                ValidationExpression="กรุณาเลือกหน่วย" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator16" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label25" runat="server" Text="จำนวนสุ่ม" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtqty_randomedit" Text='<%# Eval("qty_random") %>' OnTextChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control "></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ValidationGroup="Save" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtqty_randomedit"
                                                ValidationExpression="^[0-9]{1,10}.*$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender25" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator7" Width="160" />

                                        </div>
                                        <asp:Label ID="Label27" runat="server" Text="หน่วย" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblunit_randomedit" Visible="false" class="control-labelnotop" Text='<%# Eval("unit_random") %>' runat="server"> </asp:Label>

                                            <asp:DropDownList ID="ddlunit_randomedit" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกหน่วย...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator40" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlunit_randomedit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกหน่วย"
                                                ValidationExpression="กรุณาเลือกหน่วย" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender59" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator40" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label28" runat="server" Text="ประเภทปัญหา" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-9">
                                            <asp:Label ID="lblm0_pridxedit" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_pridx") %>' runat="server"> </asp:Label>

                                            <asp:DropDownList ID="ddltype_problemedit" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกประเภทปัญหา...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltype_problemedit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทปัญหา"
                                                ValidationExpression="กรุณาเลือกประเภทปัญหา" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator17" Width="160" />
                                            <%--<p class="help-block" style="color: red">
                                                <font color="red">**จำนวนปัญหาที่เคยพบ </font>
                                                <asp:Label ID="lblcount_problem" runat="server" Text="0" CssClass="control-label"></asp:Label>
                                                ครั้ง
                                            </p>--%>
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Labedle1" class="col-sm-2 control-label" runat="server" Text="รายละเอียดเพิ่มเติม : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtremark_edit" Text='<%# Eval("detail_remark") %>' TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtremark_edit" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียดเพิ่มเติม"
                                                ValidationExpression="กรุณากรอกรายละเอียดเพิ่มเติม"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark_edit"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                        </div>


                                    </div>

                                    <%--   <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <asp:Label ID="Labesl10s" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-sm-7">
                                                    <asp:FileUpload ID="UploadFileProblem_Detail" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi" accept="jpg|pdf" />


                                                    <asp:RequiredFieldValidator ID="RequiredsFieldValidator18"
                                                        runat="server" ControlToValidate="UploadFileProblem_Detail" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="กรุณาอัพโหลดไฟล์"
                                                        ValidationGroup="Save" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatoarCalloutExtender26" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredsFieldValidator18" Width="200" PopupPosition="BottomLeft" />

                                                    <small>
                                                        <p class="help-block"><font color="red">**Attach File name jpg,  pdf</font></p>
                                                    </small>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>--%>

                                    <div class="form-group" runat="server">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdUpdate_Problem" OnCommand="btnCommand" ValidationGroup="Save" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" CommandName="BtnBack_Problem" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </EditItemTemplate>
                    </asp:FormView>

                </div>
            </div>


            <div class="col-lg-12" id="qa_iqi_comment" runat="server" visible="false">
                <div class="panel panel-danger" style="border-width: 2pt">
                    <div class="panel-heading" style="background-color: #ff9999; color: #181818;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-question-sign"></i><strong>&nbsp; ประเมินระดับปัญหา</strong></h3>
                    </div>
                    <asp:FormView ID="FvQA_Comment" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">



                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Label33" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานะทวนสอบ" />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddl_approve_qa" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="0">กรุณาเลือกสถานะทวนสอบ...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                        ControlToValidate="ddl_approve_qa" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกสถานะทวนสอบ"
                                                        ValidationExpression="กรุณาเลือกสถานะทวนสอบ" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddl_approve_qa" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group" id="div_checklist" runat="server" visible="false">
                                                <asp:Label ID="Label56" runat="server" Text="ตรวจสอบงาน" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:RadioButtonList ID="rdochange" runat="server" CssClass="radio-list-inline-emps" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="1">รับงาน</asp:ListItem>
                                                        <asp:ListItem Value="2">โอนย้ายงาน</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="rdochange" />
                                        </Triggers>
                                    </asp:UpdatePanel>--%>

                                    <asp:Panel ID="panel_accept" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label94" runat="server" Text="PO Number" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtpono" runat="server" MaxLength="10" CssClass="form-control "></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator27" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                    ControlToValidate="txtpono" Font-Size="11"
                                                    ErrorMessage="กรุณากรอก PO Number" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" ValidationGroup="SaveApprove" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                    ControlToValidate="txtpono"
                                                    ValidationExpression="^[0-9]{1,10}$" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender40" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator27" Width="160" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender42" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator11" Width="160" />

                                            </div>
                                            <asp:Label ID="Label95" runat="server" Text="Invoice Number" CssClass="col-sm-3 control-label text_right"></asp:Label>

                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txttvno" runat="server" CssClass="form-control "></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator32" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                    ControlToValidate="txttvno" Font-Size="11"
                                                    ErrorMessage="กรุณากรอก Invoice Number" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender48" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator32" Width="160" />

                                            </div>
                                        </div>

                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:Label ID="Label5" runat="server" Text="ระดับปัญหา" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:RadioButtonList ID="rdoproblem" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                        </asp:RadioButtonList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                            ValidationGroup="SaveApprove" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                            Display="None" SetFocusOnError="true" ControlToValidate="rdoproblem" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCaslloutExtenxder7" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                                            PopupPosition="BottomLeft" />
                                                    </div>
                                                    <asp:Label ID="Label41" runat="server" Text="ประเภทการส่งคืน" CssClass="col-sm-3 control-label text_right"></asp:Label>

                                                    <div class="col-sm-3">
                                                        <asp:RadioButtonList ID="rdorollback" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                        </asp:RadioButtonList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20"
                                                            ValidationGroup="SaveApprove" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                            Display="None" SetFocusOnError="true" ControlToValidate="rdorollback" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender28" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator20" Width="160"
                                                            PopupPosition="BottomLeft" />
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="rdoproblem" />
                                                <asp:AsyncPostBackTrigger ControlID="rdorollback" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:Label ID="Label42" runat="server" Text="จำนวนส่งคืน" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtqtyrollback" runat="server" CssClass="form-control "></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RqRetxtpsrice22" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                            ControlToValidate="txtqtyrollback" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกจำนวนที่รับเข้า" />
                                                        <asp:RegularExpressionValidator ID="Retxtprices22" runat="server" ValidationGroup="SaveApprove" Display="None"
                                                            ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                            ControlToValidate="txtqtyrollback"
                                                            ValidationExpression="^[0-9]{1,10}.*$" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalsloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtpsrice22" Width="160" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprices22" Width="160" />

                                                    </div>
                                                    <asp:Label ID="Label22" runat="server" Text="หน่วย" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">

                                                        <asp:DropDownList ID="ddlunit_rollback" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="SaveApprove" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0">กรุณาเลือกหน่วย...</asp:ListItem>

                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="ddlunit_rollback" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกหน่วย"
                                                            ValidationExpression="กรุณาเลือกหน่วย" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="160" />

                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlunit_rollback" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                        <div class="form-group">
                                            <asp:Label ID="Label45" runat="server" Text="สาเหตุส่งคืน" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtremark_rollback" TextMode="multiline" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                <asp:RequiredFieldValidator ID="RequiredsFieldValidator21" ValidationGroup="SaveApprove" runat="server" Display="None" ControlToValidate="txtremark_rollback" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกสาเหตุส่งคืน"
                                                    ValidationExpression="กรุณากรอกสาเหตุส่งคืน"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender29" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredsFieldValidator21" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server"
                                                    ValidationGroup="SaveApprove" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtremark_rollback"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender30" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator9" Width="160" />

                                            </div>
                                        </div>

                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div id="div_seaweed" runat="server" visible="false">


                                                    <div class="form-group">
                                                        <asp:Label ID="Label103" runat="server" Text="Code" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txtcode" runat="server" MaxLength="500" CssClass="form-control "></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator34" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                                ControlToValidate="txtcode" Font-Size="11"
                                                                ErrorMessage="กรุณากรอก Code" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender49" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator34" Width="160" />

                                                        </div>
                                                        <asp:Label ID="Label104" runat="server" Text="คิดเป็นร้อยละ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txtper_seaweed" runat="server" CssClass="form-control "></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator35" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                                ControlToValidate="txtper_seaweed" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกข้อมูล" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server" ValidationGroup="SaveApprove" Display="None"
                                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                ControlToValidate="txtper_seaweed"
                                                                ValidationExpression="^[0-9]{1,10}.*$" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender51" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator35" Width="160" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender52" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator14" Width="160" />


                                                        </div>
                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>


                                        <div class="form-group" id="div_gvmaterial_risk" runat="server" visible="false">
                                            <asp:Label ID="Label97" runat="server" Text="Material Risk" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-9">
                                                <asp:GridView ID="Gv_Material_risk" runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                    HeaderStyle-CssClass="info small"
                                                    HeaderStyle-Height="40px"
                                                    AllowPaging="false"
                                                    OnRowDataBound="Master_RowDataBound"
                                                    DataKeyNames="m0_mridx">

                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkmatrisk" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Material Risk" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbm0_mridx" runat="server" Visible="false" Text='<%# Eval("m0_mridx") %>'></asp:Label>
                                                                <asp:Label ID="material_risk" runat="server" Text='<%# Eval("material_risk") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ความคิดเห็นเพิ่มเติม" ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtcomment" TextMode="MultiLine" CssClass="form-control" Rows="3" runat="server"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>


                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <asp:Label ID="Label57" runat="server" Text="สถานที่ดำเนินการ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddllocation_exchange" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="SaveApprove" CssClass="form-control" runat="server">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                    ControlToValidate="ddllocation_exchange" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกสถานที่"
                                                    ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                            </div>
                                        </div>
                                        <div class="form-group" id="div_reason_exchange" runat="server" visible="false">
                                            <asp:Label ID="Label54" runat="server" Text="ระบุเหตุผล" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtremark_exchange" TextMode="multiline" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ValidationGroup="SaveApprove" runat="server" Display="None" ControlToValidate="txtremark_exchange" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender27" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator19" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server"
                                                    ValidationGroup="SaveApprove" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtremark_exchange"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorsCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator8" Width="160" />

                                            </div>
                                        </div>
                                        <asp:UpdatePanel ID="UpdatePanel_QA" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>

                                                <div class="form-group">
                                                    <asp:Label ID="Label1d0s" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                    <div class="col-sm-7">
                                                        <asp:FileUpload ID="UploadFileProblem_QA" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi" accept="jpg|pdf" />

                                                        <%-- <asp:FileUpload ID="UploadFileProblem" AutoPostBack="true" ViewStateMode="Enabled" ValidationGroup="Save"
                                                        CssClass=" btn btn-warning btn-sm UploadFileProblem multi  accept-jpg|pdf  maxsize-1024 with-preview" runat="server" />--%>

                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldVadlidator18"
                                                            runat="server" ControlToValidate="UploadFileProblem_QA" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="กรุณาอัพโหลดไฟล์"
                                                            ValidationGroup="SaveApprove" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="VdalidatorCalloutExtender26" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVadlidator18" Width="200" PopupPosition="BottomLeft" />--%>

                                                        <small>
                                                            <p class="help-block"><font color="red">**Attach File name jpg,  pdf</font></p>
                                                        </small>
                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnapprove_qa" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-10">
                                                <asp:LinkButton ID="btnapprove_qa" ValidationGroup="SaveApprove" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdApprove_QA" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton8" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel_exchange" OnClientClick="return confirm('คุณต้องการออกจากการทำรายการนี้ใช่หรือไม่ ?')" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>

                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <%-- <asp:Panel ID="panel_change" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label57" runat="server" Text="สถาสนที่" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddllocation_exchange"  ValidationGroup="SaveApprove" CssClass="form-control" runat="server">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                    ControlToValidate="ddllocation_exchange" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกสถานที่"
                                                    ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label54" runat="server" Text="ระบุเหตุผล" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtremark_exchange" TextMode="multiline" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ValidationGroup="SaveApprove" runat="server" Display="None" ControlToValidate="txtremark_exchange" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender27" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator19" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server"
                                                    ValidationGroup="SaveApprove" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtremark_exchange"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorsCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator8" Width="160" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-10">
                                                <asp:LinkButton ID="btnexchange" ValidationGroup="SaveApprove" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdExchange" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btnCancel_exchange" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel_exchange" OnClientClick="return confirm('คุณต้องการออกจากการทำรายการนี้ใช่หรือไม่ ?')" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>

                                            </div>
                                        </div>

                                    </asp:Panel>--%>



                                    <div id="div_approveqa" runat="server" visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label66" runat="server" Text="ความคิดเห็นเพิ่มเติม" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtremark_approveqa" TextMode="multiline" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldVaslidator21" ValidationGroup="SaveApprove" runat="server" Display="None" ControlToValidate="txtremark_approveqa" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCaalloutExtender31" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVaslidator21" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExparessionValidator10" runat="server"
                                                    ValidationGroup="SaveApprove" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtremark_approveqa"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatforCalloutExtender32" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExparessionValidator10" Width="160" />

                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-2">
                                                <asp:LinkButton ID="btnApprove" ValidationGroup="SaveApprove" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdUpdateApprove_Qa" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btnCancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>


                        </InsertItemTemplate>

                        <ItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label96" runat="server" Text="PO Number" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblu2_docidx" Visible="false" class="control-labelnotop" Text='<%# Eval("u2_docidx") %>' runat="server"> </asp:Label>
                                            <asp:Label ID="Label111" class="control-labelnotop" Text='<%# Eval("po_no") %>' runat="server"> </asp:Label>

                                        </div>
                                        <asp:Label ID="Label98" runat="server" Text="Invoice Number" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="Label100" class="control-labelnotop" Text='<%# Eval("tv_no") %>' runat="server"> </asp:Label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label46" runat="server" Text="ระดับปัญหา" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="Label48" class="control-labelnotop" Text='<%# Eval("type_problem_name") %>' runat="server"> </asp:Label>

                                        </div>
                                        <asp:Label ID="Label50" runat="server" Text="ประเภทการส่งคืน" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:Label ID="Label53" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_rbidx") %>' runat="server"> </asp:Label>

                                            <asp:Label ID="Label58" class="control-labelnotop" Text='<%# Eval("rollback_name") %>' runat="server"> </asp:Label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label59" runat="server" Text="จำนวนส่งคืน" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="Label60" class="control-labelnotop" Text='<%# Eval("qty_rollback") %>' runat="server"> </asp:Label>

                                        </div>
                                        <asp:Label ID="Label61" runat="server" Text="หน่วย" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:Label ID="Label62" Visible="false" class="control-labelnotop" Text='<%# Eval("unit_rollback") %>' runat="server"> </asp:Label>

                                            <asp:Label ID="Label63" class="control-labelnotop" Text='<%# Eval("unit_name") %>' runat="server"> </asp:Label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label64" runat="server" Text="สาเหตุส่งคืน" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="Label65" class="control-labelnotop" Text='<%# Eval("detail_rollback") %>' runat="server"> </asp:Label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label57" runat="server" Text="สถานที่ดำเนินการ" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:Label ID="Label105" class="control-labelnotop" Text='<%# Eval("LocName") %>' runat="server"> </asp:Label>

                                        </div>
                                    </div>
                                    <div id="divshow_seaweed" runat="server" visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label106" runat="server" Text="code" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:Label ID="Label107" class="control-labelnotop" Text='<%# Eval("code_seaweed") %>' runat="server"> </asp:Label>
                                            </div>

                                            <asp:Label ID="Label108" runat="server" Text="คิดเป็นร้อยละ" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:Label ID="Label109" class="control-labelnotop" Text='<%# Eval("percent_seaweed") %>' runat="server"> </asp:Label>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="form-group" id="divshow_gvmaterial_risk" runat="server" visible="false">
                                        <asp:Label ID="Label110" runat="server" Text=" " CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-8">

                                            <asp:GridView ID="Gvshow_Material_risk" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                HeaderStyle-CssClass="info small"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="false"
                                                OnRowDataBound="Master_RowDataBound"
                                                DataKeyNames="u3_docidx">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="Material Risk" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbm0_mridx" runat="server" Visible="false" Text='<%# Eval("m0_mridx") %>'></asp:Label>
                                                            <asp:Label ID="material_risk" runat="server" Text='<%# Eval("material_risk") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ความคิดเห็นเพิ่มเติม" ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("comment_risk") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>

                                            <%-- <asp:CheckBoxList ID="chkmat_risk_show" Enabled="false" runat="server" CssClass="radio-list-inline-emps" RepeatColumns="2" RepeatDirection="Vertical">
                                                </asp:CheckBoxList>--%>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </ItemTemplate>

                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <asp:Label ID="lblu2_docidx_edit" Visible="false" class="control-labelnotop" Text='<%# Eval("u2_docidx") %>' runat="server"> </asp:Label>

                                    <div class="form-group">
                                        <asp:Label ID="Label94" runat="server" Text="PO Number" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpono_edit" Text='<%# Eval("po_no") %>' runat="server" MaxLength="10" CssClass="form-control "></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator27" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                ControlToValidate="txtpono_edit" Font-Size="11"
                                                ErrorMessage="กรุณากรอก PO Number" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" ValidationGroup="SaveApprove" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtpono_edit"
                                                ValidationExpression="^[0-9]{1,10}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender40" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator27" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender42" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator11" Width="160" />

                                        </div>
                                        <asp:Label ID="Label95" runat="server" Text="Invoice Number" CssClass="col-sm-3 control-label text_right"></asp:Label>

                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txttvno_edit" Text='<%# Eval("tv_no") %>' runat="server" CssClass="form-control "></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator33" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                ControlToValidate="txttvno_edit" Font-Size="11"
                                                ErrorMessage="กรุณากรอก Invoice Number" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender50" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator33" Width="160" />

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label5" runat="server" Text="ระดับปัญหา" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblpbidx_edit" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_pbidx") %>' runat="server"> </asp:Label>

                                            <asp:RadioButtonList ID="rdoproblem_edit" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                ValidationGroup="SaveApprove" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                Display="None" SetFocusOnError="true" ControlToValidate="rdoproblem_edit" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCaslloutExtenxder7" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                                PopupPosition="BottomLeft" />
                                        </div>
                                        <asp:Label ID="Label41" runat="server" Text="ประเภทการส่งคืน" CssClass="col-sm-3 control-label text_right"></asp:Label>

                                        <div class="col-sm-3">
                                            <asp:Label ID="lblm0_rbidx_edit" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_rbidx") %>' runat="server"> </asp:Label>

                                            <asp:RadioButtonList ID="rdorollback_edit" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator20"
                                                ValidationGroup="SaveApprove" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                Display="None" SetFocusOnError="true" ControlToValidate="rdorollback_edit" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender28" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator20" Width="160"
                                                PopupPosition="BottomLeft" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label42" runat="server" Text="จำนวนส่งคืน" CssClass="col-sm-2 control-label text_right"></asp:Label>

                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtqtyrollback_edit" runat="server" Text='<%# Eval("qty_rollback") %>' CssClass="form-control "></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RqRetxtpsrice22" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                ControlToValidate="txtqtyrollback_edit" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวนที่รับเข้า" />
                                            <asp:RegularExpressionValidator ID="Retxtprices22" runat="server" ValidationGroup="SaveApprove" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtqtyrollback_edit"
                                                ValidationExpression="^[0-9]{1,10}.*$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtpsrice22" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprices22" Width="160" />

                                        </div>
                                        <asp:Label ID="Label22" runat="server" Text="หน่วย" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblunit_rollback" Visible="false" class="control-labelnotop" Text='<%# Eval("unit_rollback") %>' runat="server"> </asp:Label>

                                            <asp:DropDownList ID="ddlunit_rollback_edit" ValidationGroup="SaveApprove" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกหน่วย...</asp:ListItem>

                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlunit_rollback_edit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกหน่วย"
                                                ValidationExpression="กรุณาเลือกหน่วย" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="160" />

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label45" runat="server" Text="สาเหตุส่งคืน" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtremark_rollback_edit" Text='<%# Eval("detail_rollback") %>' TextMode="multiline" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                            <asp:RequiredFieldValidator ID="RequiredsFieldValidator21" ValidationGroup="SaveApprove" runat="server" Display="None" ControlToValidate="txtremark_rollback_edit" Font-Size="11"
                                                ErrorMessage="กรุณากรอกสาเหตุส่งคืน"
                                                ValidationExpression="กรุณากรอกสาเหตุส่งคืน"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender29" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredsFieldValidator21" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server"
                                                ValidationGroup="SaveApprove" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark_rollback_edit"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender30" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator9" Width="160" />

                                        </div>


                                    </div>

                                    <div class="form-group" id="divedit_gvmaterial_risk" runat="server" visible="false">
                                        <asp:Label ID="Label56" runat="server" Text="Material Risk" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-9">
                                            <asp:GridView ID="Gvedit_Material_risk" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                HeaderStyle-CssClass="info small"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="false"
                                                OnRowDataBound="Master_RowDataBound"
                                                DataKeyNames="m0_mridx">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkmatrisk_edit" OnCheckedChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Material Risk" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbm0_mridx_edit" runat="server" Visible="false" Text='<%# Eval("m0_mridx") %>'></asp:Label>
                                                            <asp:Label ID="material_risk" runat="server" Text='<%# Eval("material_risk") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ความคิดเห็นเพิ่มเติม" ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtmatrisk_edit" Visible="false" TextMode="MultiLine" CssClass="form-control" Rows="3" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>


                                        </div>
                                    </div>


                                    <div id="div_seaweed_edit" runat="server" visible="false">


                                        <div class="form-group">
                                            <asp:Label ID="Label103" runat="server" Text="Code" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtcode_edit" Text='<%# Eval("code_seaweed") %>' runat="server" MaxLength="500" CssClass="form-control "></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator34" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                    ControlToValidate="txtcode_edit" Font-Size="11"
                                                    ErrorMessage="กรุณากรอก Code" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender49" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator34" Width="160" />

                                            </div>
                                            <asp:Label ID="Label104" runat="server" Text="คิดเป็นร้อยละ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtper_seaweed_edit" Text='<%# Eval("percent_seaweed") %>' runat="server" CssClass="form-control "></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator35" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                    ControlToValidate="txtper_seaweed_edit" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกข้อมูล" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server" ValidationGroup="SaveApprove" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                    ControlToValidate="txtper_seaweed_edit"
                                                    ValidationExpression="^[0-9]{1,10}.*$" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender51" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator35" Width="160" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender52" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator14" Width="160" />


                                            </div>
                                        </div>



                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Labesl33" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานะอนุมัติ" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_approve_qa_edit" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกสถานะอนุมัติ...</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                ControlToValidate="ddl_approve_qa_edit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานะอนุมัติ"
                                                ValidationExpression="กรุณาเลือกสถานะอนุมัติ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />

                                        </div>
                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel_QA_edit" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <asp:Label ID="Labeldf1d0s" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-sm-7">
                                                    <asp:FileUpload ID="UploadFileProblem_QA_edit" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi" accept="jpg|pdf" />

                                                    <%-- <asp:FileUpload ID="UploadFileProblem" AutoPostBack="true" ViewStateMode="Enabled" ValidationGroup="Save"
                                                        CssClass=" btn btn-warning btn-sm UploadFileProblem multi  accept-jpg|pdf  maxsize-1024 with-preview" runat="server" />--%>

                                                    <%--    <asp:RequiredFieldValidator ID="RequiredFtieldVadlidator18"
                                                        runat="server" ControlToValidate="UploadFileProblem_QA_edit" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="กรุณาอัพโหลดไฟล์"
                                                        ValidationGroup="SaveApprove" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="VdalisdatorCalloutExtender26" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFtieldVadlidator18" Width="200" PopupPosition="BottomLeft" />--%>

                                                    <small>
                                                        <p class="help-block"><font color="red">**Attach File name jpg,  pdf</font></p>
                                                    </small>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnapprove_qa_edit" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="btnapprove_qa_edit" ValidationGroup="SaveApprove" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdApprove_QAEdit" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton8" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnback_editqa" OnClientClick="return confirm('คุณต้องการออกจากการทำรายการนี้ใช่หรือไม่ ?')" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>

                                        </div>
                                    </div>

                                </div>
                            </div>


                        </EditItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12" id="supplier_complete" runat="server" visible="false">
                <div class="panel panel-danger" style="border-width: 2pt">
                    <div class="panel-heading" style="background-color: #ff9999; color: #181818;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-bookmark"></i><strong>&nbsp; ความคิดเห็นเพิ่มเติม Supplier</strong></h3>
                    </div>
                    <asp:FormView ID="FvSupplier_Comment" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <ItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label182" runat="server" Text="วันที่ต้องดำเนินการให้เสร็จ" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="Label183" class="control-labelnotop" Text='<%# Eval("count_date") %>' runat="server"> </asp:Label>

                                        </div>
                                        <asp:Label ID="Label184" runat="server" Text="วันที่ได้รับแจ้งปัญหา" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="Label185" class="control-labelnotop" Text='<%# Eval("send_pur") %>' runat="server"> </asp:Label>
                                            <asp:Label ID="Label186" runat="server" Text="วัน" CssClass="control-labelnotop"></asp:Label>


                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label96" runat="server" Text="ความคิดเห็นเพิ่มเติม" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="Label111" class="control-labelnotop" Text='<%# Eval("comment_sup") %>' runat="server"> </asp:Label>

                                        </div>
                                        <asp:Label ID="Label98" runat="server" Text="ระยะเวลาในการดำเนินการ" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="Label100" class="control-labelnotop" Text='<%# Eval("downtime") %>' runat="server"> </asp:Label>
                                            <asp:Label ID="Label116" runat="server" Text="วัน" CssClass="control-labelnotop"></asp:Label>


                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label112" runat="server" Text="ชื่อ Supplier" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="Label113" class="control-labelnotop" Text='<%# Eval("sup_name") %>' runat="server"> </asp:Label>

                                        </div>
                                        <asp:Label ID="Label114" runat="server" Text="Email Supplier" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:Label ID="Label115" class="control-labelnotop" Text='<%# Eval("email_sup") %>' runat="server"> </asp:Label>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </ItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12" id="div_approve" runat="server" visible="false">
                <div class="panel panel-danger" style="border-width: 2pt">
                    <div class="panel-heading" style="background-color: #ff9999; color: #181818;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-envelope"></i><strong>&nbsp; ข้อมูลส่วนอนุมัติรายการ</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">

                                <asp:Label ID="Labsael33" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานะอนุมัติ" />
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddl_approve" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="0">กรุณาเลือกสถานะอนุมัติ...</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValisdator15" ValidationGroup="SaveApprove" runat="server" Display="None"
                                        ControlToValidate="ddl_approve" Font-Size="11"
                                        ErrorMessage="กรุณาเลือกสถานะอนุมัติ"
                                        ValidationExpression="กรุณาเลือกสถานะอนุมัติ" InitialValue="0" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenader16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValisdator15" Width="160" />

                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label54" runat="server" Text="ความคิดเห็นเพิ่มเติม" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtremark_approve" TextMode="multiline" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ValidationGroup="SaveApprove" runat="server" Display="None" ControlToValidate="txtremark_approve" Font-Size="11"
                                        ErrorMessage="กรุณากรอกความคิดเห็น"
                                        ValidationExpression="กรุณากรอกความคิดเห็น"
                                        SetFocusOnError="true" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender27" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server"
                                        ValidationGroup="SaveApprove" Display="None"
                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                        ControlToValidate="txtremark_approve"
                                        ValidationExpression="^[\s\S]{0,1000}$"
                                        SetFocusOnError="true" />

                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorsCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                </div>


                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <asp:LinkButton ID="btnApprove" ValidationGroup="SaveApprove" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdUpdateApprove" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btnCancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">

                <div class="panel panel-danger" style="border-width: 2pt">
                    <div class="panel-heading" style="background-color: #ff9999; color: #181818;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-eye-open"></i><strong>&nbsp; ประวัติการอนุมัติ</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <asp:Repeater ID="rpLog" runat="server">
                                <HeaderTemplate>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"><small>วัน / เวลา</small></label>
                                        <label class="col-sm-3 control-label"><small>ผู้ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ผลการดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ความคิดเห็น</small></label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <%-- <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><%#Eval("createdate")%></small></span>--%>
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-8">
                                                <span><small><%#Eval("createdate")%></small></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <span>&nbsp;&nbsp;&nbsp;<small><%# Eval("FullNameTH") %>&nbsp;&nbsp;&nbsp;(<%# Eval("actor_name") %>)</small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("node_name") %></small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("status_name") %></small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small style="text-align: center">&nbsp;&nbsp;&nbsp;<%# Eval("comment") %></small></span>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-2 col-sm-offset-10">
                    <asp:LinkButton ID="btnback" CssClass="btn btn-danger btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="BtnBack" data-toggle="tooltip" title="Close"><i class="glyphicon glyphicon-home"></i> กลับ</asp:LinkButton>
                    <asp:LinkButton ID="btnback_report" Visible="false" CssClass="btn btn-warning btn-sm" runat="server" Text="Back" OnCommand="btnCommand" CommandName="BtnBack_Report" data-toggle="tooltip" title="Close"><i class="glyphicon glyphicon-list-alt"></i> กลับหน้ารายงาน</asp:LinkButton>

                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewMaster_GroupProduct" runat="server">
            <div class="col-lg-12">
                <asp:Image ID="Image10" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr_group.png" Style="height: 100%; width: 100%;" />
            </div>

            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading" style="background-color: #FFD133; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-certificate"></i><strong>&nbsp; เพิ่มกลุ่มวัตถุดิบ</strong></h3>
                    </div>

                    <div id="div_index_mastergroup" runat="server">
                        <div class="panel-body">
                            <div class="form-group">
                                <asp:LinkButton ID="btnshow_groupproduct" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="เพิ่มกลุ่มวัตถุดิบ" runat="server" CommandName="CmdAddMaster_GroupProduct" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <asp:Panel ID="Panel_AddMasterGroup" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="Label158" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtgroupproduct" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator45" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtgroupproduct" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกกลุ่มวัตถุดิบ"
                                                    ValidationExpression="กรุณากรอกกลุ่มวัตถุดิบ"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender64" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator45" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtgroupproduct"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender65" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator17" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label159" CssClass="col-sm-3 control-label" runat="server" Text="สถานะการใช้งาน" />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatusadd_mastergroup" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="2" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButton31" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdInsertMaster_Group" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton35" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Group" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <div id="div_searchmaster_groupproduct" runat="server">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">

                                        <asp:Label ID="Label160" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtmaster_searchgroup" runat="server" CssClass="form-control" PlaceHolder="........" />
                                        </div>
                                        <asp:LinkButton ID="LinkButton36" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdSearchMaster_Group" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการค้นหารายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton37" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Group" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>

                                    </div>

                                </div>
                            </div>
                            <div style="overflow-x: auto; width: 100%">
                                <asp:GridView ID="GvMaster_Group" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="primary"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    DataKeyNames="m0_group_tpidx"
                                    PageSize="10"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnRowEditing="Master_RowEditing"
                                    OnRowCancelingEdit="Master_RowCancelingEdit"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowUpdating="Master_RowUpdating">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="กลุ่มวัตถุดิบ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="35%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblm0_group_tpidx" runat="server" Visible="false" Text='<%# Eval("m0_group_tpidx") %>' />
                                                <asp:Label ID="lbgroupname" runat="server" Text='<%# Eval("group_name") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <div class="form-horizontal" role="form">
                                                    <div class="panel-heading">
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtm0_group_tpidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_group_tpidx")%>' />
                                                            </div>
                                                        </div>



                                                        <div class="form-group">
                                                            <asp:Label ID="Label26" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtgroupproduct_edit" runat="server" CssClass="form-control" Text='<%# Eval("group_name")%>' />
                                                            </div>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtgroupproduct_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกกลุ่มวัตถุดิบ"
                                                                ValidationExpression="กรุณากรอกกลุ่มวัตถุดิบ"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                                ValidationGroup="Save_edit" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtgroupproduct_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                        </div>


                                                        <div class="form-group">
                                                            <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList ID="ddStatusUpdate_mastergroup" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                                    <asp:ListItem Value="1" Text="Online" />
                                                                    <asp:ListItem Value="2" Text="Offline" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label197" CssClass="col-sm-3 control-label" runat="server" Text="" />
                                                            <div class="col-sm-8">
                                                                <p style="color: red">หลังจากทำการแก้ไขข้อมูล รบกวนแจ้งหัวหน้างานทำการอนุมัติรายการ</p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2 col-sm-offset-10">
                                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </EditItemTemplate>


                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="13%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatus_edit" runat="server" Visible="false" Text='<%# Eval("status_edit") %>'></asp:Label>
                                                <asp:Label ID="lblstatus_name" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label>
                                                <p />
                                                <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="History_Group" CssClass="btn btn-warning btn-sm" runat="server" OnCommand="btnCommand" CommandName="CmdSeeversion_group" CommandArgument='<%# Eval("m0_group_tpidx") %>' data-toggle="tooltip" title="ดูข้อมูลเปลี่ยนแปลง"><i class="glyphicon glyphicon-eye-open"></i></asp:LinkButton>
                                                <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                                <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelMaster_group" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_group_tpidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-12" runat="server" id="Div_Log_Group" visible="false">

                        <asp:FormView ID="fvapprove_group" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <EditItemTemplate>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <asp:Label ID="Label187" runat="server" Text="ชื่อกลุ่มวัตถุดิบเก่า" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtgroup_old" Enabled="false" Text='<%# Eval("group_name") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="txtgroup_rsecidx" Visible="false" Text='<%# Eval("rsecidx") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="txtgroup_job" Visible="false" Text='<%# Eval("JobGradeIDX") %>' runat="server" CssClass="form-control" />
                                            </div>

                                            <asp:Label ID="Label188" runat="server" Text="ชื่อกลุ่มวัตถุดิบใหม่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtgroup_new" Enabled="false" Text='<%# Eval("group_name_new") %>' runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label190" runat="server" Text="สถานะการใช้งานเก่า" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtgroup_statusold" Visible="false" Text='<%# Eval("status") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="TextBox1" Enabled="false" Text='<%# Eval("statusname_old") %>' runat="server" CssClass="form-control" />
                                            </div>
                                            <asp:Label ID="Label191" runat="server" Text="สถานะการใช้งานใหม่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtgroup_statusnew" Visible="false" Text='<%# Eval("status_new") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="TextBox2" Enabled="false" Text='<%# Eval("statusname_new") %>' runat="server" CssClass="form-control" />
                                            </div>

                                        </div>

                                        <%-- <div class="form-group">
                                            
                                        </div>--%>

                                        <div class="form-group">

                                            <asp:Label ID="Label189" runat="server" Text="ผู้ดำเนินการแก้ไขรายการ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtemp_editgroup" Enabled="false" Text='<%# Eval("emp_name_th") %>' runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <%-- <div class="form-group">
                                            
                                        </div>--%>
                                        <div class="form-group">
                                            <div class="col-sm-3 col-sm-offset-2">
                                                <asp:LinkButton ID="btnapprove_group" CssClass="btn btn-success btn-sm" runat="server" CommandName="btnapprove_group" CommandArgument="1" OnCommand="btnCommand" data-toggle="tooltip" title="Approve" OnClientClick="return confirm('คุณต้องการอนุมัติรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-ok-sign"></i> อนุมัติ</asp:LinkButton>
                                                <asp:LinkButton ID="btnunapprove_group" CssClass="btn btn-danger btn-sm" runat="server" CommandName="btnapprove_group" CommandArgument="2" OnCommand="btnCommand" data-toggle="tooltip" title="Cancel" OnClientClick="return confirm('คุณต้องการไม่อนุมัติรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-remove-sign"></i> ไม่อนุมัติ</asp:LinkButton>
                                                <asp:LinkButton ID="btncancel_group" CssClass="btn btn-default btn-sm" runat="server" CommandName="btnbackapprove_group" OnCommand="btnCommand" data-toggle="tooltip" title="Close"><i class="glyphicon glyphicon-home"></i> Close</asp:LinkButton>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:FormView>
                    </div>



                </div>
            </div>


        </asp:View>

        <asp:View ID="ViewMaster_Product" runat="server">
            <div class="col-lg-12">
                <asp:Image ID="imghead" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr_typeproduct.png" Style="height: 100%; width: 100%;" />
            </div>

            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading" style="background-color: #FFD133; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-certificate"></i><strong>&nbsp; เพิ่มประเภทวัตถุดิบ</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <asp:LinkButton ID="btnshow_product" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="เพิ่มประเภทวัตถุดิบ" runat="server" CommandName="CmdAddMaster_Product" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                        </div>


                        <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="Label156" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlgroupproduct_insertmasterproduct" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator41" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddlgroupproduct_insertmasterproduct" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกกลุ่มวัตถุดิบ"
                                                    ValidationExpression="กรุณาเลือกกลุ่มวัตถุดิบ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender60" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator41" Width="160" />

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label26" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txttypeproduct" runat="server" CssClass="form-control" PlaceHolder="........" />
                                            </div>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txttypeproduct" Font-Size="11"
                                                ErrorMessage="กรุณากรอกประเภทวัตถุดิบ"
                                                ValidationExpression="กรุณากรอกประเภทวัตถุดิบ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txttypeproduct"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl" CssClass="col-sm-3 control-label" runat="server" Text="สถานะการใช้งาน" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="2" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdInsertMaster_Product" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Product" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>


                        <div id="div_searchmaster_typeproduct" runat="server">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Label ID="Label150" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlmaster_searchgroup" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>

                                    <asp:Label ID="Label120" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtmaster_searchproduct" runat="server" CssClass="form-control" PlaceHolder="........" />
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <asp:LinkButton ID="LinkButton16" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSearchMaster_Product" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการค้นหารายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton18" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Product" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="overflow-x: auto; width: 100%">
                            <asp:GridView ID="GvMaster_Product" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="m0_tpidx"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <%--  <asp:TemplateField HeaderText="#" HeaderStyle-Width="5%">

                                    <ItemTemplate>
                                        <%# (Container.DataItemIndex +1) %>
                                    </ItemTemplate>

                                </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="กลุ่มวัตถุดิบ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="35%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblm0_group_tpidx" runat="server" Visible="false" Text='<%# Eval("m0_group_tpidx") %>' />
                                            <asp:Label ID="lblm0_tpidx" runat="server" Visible="false" Text='<%# Eval("m0_tpidx") %>' />
                                            <asp:Label ID="lbgroupname" runat="server" Text='<%# Eval("group_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtm0_tpidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_tpidx")%>' />
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="Label156" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtm0_group_tpidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_group_tpidx")%>' />

                                                            <asp:DropDownList ID="ddlgroupproduct_insertmasterproduct_edit" ValidationGroup="Save_edit" CssClass="form-control" runat="server">
                                                                <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator41edit" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddlgroupproduct_insertmasterproduct_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกกลุ่มวัตถุดิบ"
                                                                ValidationExpression="กรุณาเลือกกลุ่มวัตถุดิบ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender60" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator41edit" Width="160" />

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txttypeproduct_edit" runat="server" CssClass="form-control" Text='<%# Eval("typeproduct_name")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txttypeproduct_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกประเภทวัตถุดิบ"
                                                            ValidationExpression="กรุณากรอกประเภทวัตถุดิบ"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txttypeproduct_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="2" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>


                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภทวัตถุดิบ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="40%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("typeproduct_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="13%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelMaster_product" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_tpidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewMaster_Product_M1" runat="server">
            <div class="col-lg-12">
                <asp:Image ID="Image11" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr_product_m1.png" Style="height: 100%; width: 100%;" />
            </div>

            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading" style="background-color: #FFD133; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-certificate"></i><strong>&nbsp; เพิ่มโครงสร้างวัตถุดิบ</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <asp:LinkButton ID="btnshow_product_m1" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="เพิ่มโครงสร้างวัตถุดิบ" runat="server" CommandName="CmdAddMaster_Product_M1" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                        </div>

                        <asp:Panel ID="Panel_AddMasterProduct_M1" runat="server" Visible="false">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="Label157" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddladdgroup_masterm1" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator44" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddladdgroup_masterm1" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกกลุ่มวัตถุดิบ"
                                                    ValidationExpression="กรุณาเลือกกลุ่มวัตถุดิบ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender63" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator44" Width="160" />

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label165" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddladdproduct_master" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกประเภทวัตถุดิบ...</asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator47" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddladdproduct_master" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกประเภทวัตถุดิบ"
                                                    ValidationExpression="กรุณาเลือกประเภทวัตถุดิบ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender68" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator47" Width="160" />

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label161" runat="server" Text="โครงสร้างวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtaddmaster_product_m1" runat="server" CssClass="form-control" PlaceHolder="........" />
                                            </div>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator46" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtaddmaster_product_m1" Font-Size="11"
                                                ErrorMessage="กรุณากรอกโครงสร้างวัตถุดิบ"
                                                ValidationExpression="กรุณากรอกโครงสร้างวัตถุดิบ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender66" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator46" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtaddmaster_product_m1"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender67" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator18" Width="160" />

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label162" CssClass="col-sm-3 control-label" runat="server" Text="สถานะการใช้งาน" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlstatusinsert_masterproductm1" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="2" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="LinkButton38" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdInsertMaster_Product_M1" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton39" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Product_M1" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>

                        <div id="div_searchmaster_product_m1" runat="server">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Label ID="Label163" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsearchgroup_masterm1" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>

                                    <asp:Label ID="Label164" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsearchproduct_masterm1" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกประเภทวัตถุดิบ...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <div class="form-group">

                                    <asp:Label ID="Label167" runat="server" Text="โครงสร้างวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtsearchproduct_masterm1" runat="server" CssClass="form-control" PlaceHolder="........" />
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <asp:LinkButton ID="LinkButton40" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSearchMaster_Product_M1" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการค้นหารายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton41" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Product_M1" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="overflow-x: auto; width: 100%">
                            <asp:GridView ID="GvMaster_Product_M1" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="m1_tpidx"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <%--  <asp:TemplateField HeaderText="#" HeaderStyle-Width="5%">

                                    <ItemTemplate>
                                        <%# (Container.DataItemIndex +1) %>
                                    </ItemTemplate>

                                </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="กลุ่มวัตถุดิบ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblm0_group_tpidx" runat="server" Visible="false" Text='<%# Eval("m0_group_tpidx") %>' />
                                            <asp:Label ID="lbgroupname" runat="server" Text='<%# Eval("group_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtm1_tpidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m1_tpidx")%>' />
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="Label156" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtm0_group_tpidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_group_tpidx")%>' />

                                                            <asp:DropDownList ID="ddlgroupproduct_insertmasterproduct_edit" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save_edit" CssClass="form-control" runat="server">
                                                                <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator41edit" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddlgroupproduct_insertmasterproduct_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกกลุ่มวัตถุดิบ"
                                                                ValidationExpression="กรุณาเลือกกลุ่มวัตถุดิบ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender60" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator41edit" Width="160" />

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label166" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtm0_tpidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_tpidx")%>' />

                                                            <asp:DropDownList ID="ddlproduct_insertmasterproductm1_edit" ValidationGroup="Save_edit" CssClass="form-control" runat="server">
                                                                <asp:ListItem Value="0">กรุณาเลือกประเภทวัตถุดิบ...</asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator48" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddlproduct_insertmasterproductm1_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกประเภทวัตถุดิบ"
                                                                ValidationExpression="กรุณาเลือกประเภทวัตถุดิบ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender69" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator48" Width="160" />

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="โครงสร้างวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txttypeproductm1_edit" runat="server" CssClass="form-control" Text='<%# Eval("typeproduct_name_m1")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txttypeproductm1_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกโครงสร้างวัตถุดิบ"
                                                            ValidationExpression="กรุณากรอกโครงสร้างวัตถุดิบ"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txttypeproductm1_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate_masterproduct_m1" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="2" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>


                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภทวัตถุดิบ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="25%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblm0_tpidx" runat="server" Visible="false" Text='<%# Eval("m0_tpidx") %>' />

                                            <asp:Label ID="lbtypeproduct_name" runat="server" Text='<%# Eval("typeproduct_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="โครงสร้างวัตถุดิบ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="25%">
                                        <ItemTemplate>

                                            <asp:Label ID="lbtypeproduct_name_m1" runat="server" Text='<%# Eval("typeproduct_name_m1") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="13%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelMaster_product_m1" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m1_tpidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>



                    </div>
                </div>
            </div>


        </asp:View>

        <asp:View ID="ViewMaster_Product_M2" runat="server">
            <div class="col-lg-12">
                <asp:Image ID="Image12" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr_product_m2.png" Style="height: 100%; width: 100%;" />
            </div>

            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading" style="background-color: #FFD133; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-certificate"></i><strong>&nbsp; เพิ่มโครงสร้างย่อยวัตถุดิบ</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <asp:LinkButton ID="btnshow_product_m2" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="เพิ่มโครงสร้างย่อยวัตถุดิบ" runat="server" CommandName="CmdAddMaster_Product_M2" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                        </div>


                        <asp:Panel ID="Panel_AddMasterProduct_M2" runat="server" Visible="false">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="Label168" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddladdgroup_masterm2" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator49" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddladdgroup_masterm2" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกกลุ่มวัตถุดิบ"
                                                    ValidationExpression="กรุณาเลือกกลุ่มวัตถุดิบ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender70" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator49" Width="160" />

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label169" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddladdproduct_master_m2" ValidationGroup="Save" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกประเภทวัตถุดิบ...</asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator50" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddladdproduct_master_m2" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกประเภทวัตถุดิบ"
                                                    ValidationExpression="กรุณาเลือกประเภทวัตถุดิบ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender71" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator50" Width="160" />

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label175" runat="server" Text="โครงสร้างวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddladdproduct_masterm1_m2" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกโครงสร้างวัตถุดิบ...</asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator52" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddladdproduct_masterm1_m2" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกโครงสร้างวัตถุดิบ"
                                                    ValidationExpression="กรุณาเลือกโครงสร้างวัตถุดิบ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender74" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator52" Width="160" />

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label170" runat="server" Text="โครงสร้างย่อยวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtaddmaster_product_m2" runat="server" CssClass="form-control" PlaceHolder="........" />
                                            </div>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator51" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtaddmaster_product_m1" Font-Size="11"
                                                ErrorMessage="กรุณากรอกโครงสร้างย่อยวัตถุดิบ"
                                                ValidationExpression="กรุณากรอกโครงสร้างย่อยวัตถุดิบ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender72" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator51" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtaddmaster_product_m2"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender73" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator19" Width="160" />

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label171" CssClass="col-sm-3 control-label" runat="server" Text="สถานะการใช้งาน" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlstatusinsert_masterproductm2" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="2" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="LinkButton33" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdInsertMaster_Product_M2" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton42" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Product_M2" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>


                        <div id="div_searchmaster_product_m2" runat="server">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Label ID="Label172" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsearchgroup_masterm2" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>

                                    <asp:Label ID="Label173" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsearchproduct_masterm2" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกประเภทวัตถุดิบ...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label176" runat="server" Text="โครงสร้างวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsearchproduct_masterm1_m2" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกโครงสร้างวัตถุดิบ...</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                    <asp:Label ID="Label174" runat="server" Text="โครงสร้างย่อยวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtsearchproduct_masterm2" runat="server" CssClass="form-control" PlaceHolder="........" />
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <asp:LinkButton ID="LinkButton43" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSearchMaster_Product_M2" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการค้นหารายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton44" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Product_M2" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div style="overflow-x: auto; width: 100%">
                            <asp:GridView ID="GvMaster_Product_M2" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="m2_tpidx"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="กลุ่มวัตถุดิบ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblm0_group_tpidx" runat="server" Visible="false" Text='<%# Eval("m0_group_tpidx") %>' />
                                            <asp:Label ID="lbgroupname" runat="server" Text='<%# Eval("group_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtm2_tpidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m2_tpidx")%>' />
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="Label156" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtm0_group_tpidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_group_tpidx")%>' />

                                                            <asp:DropDownList ID="ddlgroupproduct_insertmasterproduct_editm2" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save_edit" CssClass="form-control" runat="server">
                                                                <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator41edit" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddlgroupproduct_insertmasterproduct_editm2" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกกลุ่มวัตถุดิบ"
                                                                ValidationExpression="กรุณาเลือกกลุ่มวัตถุดิบ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender60" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator41edit" Width="160" />

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label166" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtm0_tpidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_tpidx")%>' />

                                                            <asp:DropDownList ID="ddlproduct_insertmasterproduct_editm2" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save_edit" CssClass="form-control" runat="server">
                                                                <asp:ListItem Value="0">กรุณาเลือกประเภทวัตถุดิบ...</asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator48" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddlproduct_insertmasterproduct_editm2" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกประเภทวัตถุดิบ"
                                                                ValidationExpression="กรุณาเลือกประเภทวัตถุดิบ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender69" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator48" Width="160" />

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label177" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtm1_tpidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m1_tpidx")%>' />

                                                            <asp:DropDownList ID="ddlproduct_insertmasterproductm1_editm2" ValidationGroup="Save_edit" CssClass="form-control" runat="server">
                                                                <asp:ListItem Value="0">กรุณาเลือกประเภทวัตถุดิบ...</asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator53" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddlproduct_insertmasterproductm1_editm2" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกโครงสร้างวัตถุดิบ"
                                                                ValidationExpression="กรุณากรอกโครงสร้างวัตถุดิบ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender75" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator48" Width="160" />

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="โครงสร้างย่อยวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txttypeproductm2_edit" runat="server" CssClass="form-control" Text='<%# Eval("typeproduct_name_m2")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txttypeproductm2_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกโครงสร้างย่อยวัตถุดิบ"
                                                            ValidationExpression="กรุณากรอกโครงสร้างย่อยวัตถุดิบ"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txttypeproductm2_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate_masterproduct_m2" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="2" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>


                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภทวัตถุดิบ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblm0_tpidx" runat="server" Visible="false" Text='<%# Eval("m0_tpidx") %>' />

                                            <asp:Label ID="lbtypeproduct_name" runat="server" Text='<%# Eval("typeproduct_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="โครงสร้างวัตถุดิบ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="25%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblm1_tpidx" runat="server" Visible="false" Text='<%# Eval("m1_tpidx") %>' />

                                            <asp:Label ID="lbtypeproduct_name_m1" runat="server" Text='<%# Eval("typeproduct_name_m1") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="โครงสร้างย่อยวัตถุดิบ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="25%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblm2_tpidx" runat="server" Visible="false" Text='<%# Eval("m2_tpidx") %>' />

                                            <asp:Label ID="lbtypeproduct_name_m2" runat="server" Text='<%# Eval("typeproduct_name_m2") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="13%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelMaster_product_m2" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m2_tpidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>

                    </div>
                </div>
            </div>


        </asp:View>

        <asp:View ID="ViewMaster_Unit" runat="server">
            <div class="col-lg-12">
                <asp:Image ID="Image1" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr_unit.png" Style="height: 100%; width: 100%;" />
            </div>

            <div class="col-lg-12">
                <div class="panel panel-danger">
                    <div class="panel-heading" style="background-color: #FFD133; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-scale"></i><strong>&nbsp; เพิ่มหน่วยวัด</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <asp:LinkButton ID="btnshow_unit" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="เพิ่มหน่วยวัด" runat="server" CommandName="CmdAddMaster_Unit" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                        </div>


                        <asp:Panel ID="Panel_AddMasterUnit" runat="server" Visible="false">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">
                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server" Text="หน่วยวัด" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtunit" runat="server" CssClass="form-control" PlaceHolder="........" />
                                            </div>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtunit" Font-Size="11"
                                                ErrorMessage="กรุณากรอกหน่วยวัด"
                                                ValidationExpression="กรุณากรอกหน่วยวัด"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtunit"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="สถานะการใช้งาน" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddStatusadd_masterunit" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="2" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="LinkButton2" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdInsertMaster_Unit" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Unit" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>


                        <div class="form-group" id="div_searchmaster_unit" runat="server">
                            <asp:Label ID="Label121" runat="server" Text="หน่วยวัด" CssClass="col-sm-2 control-label text_right"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtmaster_searchunit" runat="server" CssClass="form-control" PlaceHolder="........" />
                            </div>
                            <asp:LinkButton ID="LinkButton19" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdSearchMaster_Unit" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการค้นหารายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton20" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Unit" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>

                        </div>

                        <div style="overflow-x: auto; width: 100%">
                            <asp:GridView ID="GvMaster_Unit" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="default"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="m0_unidx"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#" HeaderStyle-Width="5%">

                                        <ItemTemplate>
                                            <asp:Label ID="lblm0_tpidx" runat="server" Visible="false" Text='<%# Eval("m0_unidx") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>


                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtm0_unidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_tpidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="หน่วยวัด" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtunit_name_edit" runat="server" CssClass="form-control" Text='<%# Eval("unit_name")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtunit_name_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกหน่วยวัด"
                                                            ValidationExpression="กรุณากรอกหน่วยวัด"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtunit_name_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="2" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="หน่วยวัด" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="70%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("unit_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelMaster_unit" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_unidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewMaster_Place" runat="server">
            <div class="col-lg-12">
                <asp:Image ID="Image2" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr_place.png" Style="height: 100%; width: 100%;" />
            </div>

            <div class="col-lg-12">
                <div class="panel panel-warning">
                    <div class="panel-heading" style="background-color: #FFD133; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-road"></i><strong>&nbsp; เพิ่มไลน์ผลิต</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <asp:LinkButton ID="btnshow_place" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="เพิ่มไลน์ผลิต" runat="server" CommandName="CmdAddMaster_Place" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                        </div>


                        <asp:Panel ID="Panel_AddMasterPlace" runat="server" Visible="false">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">


                                        <div class="form-group">
                                            <asp:Label ID="Lasbel5" runat="server" Text="สถานที่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddllocation" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddllocation" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกสถานที่"
                                                    ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label6" runat="server" Text="อาคาร" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlbuilding" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกอาคาร...</asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddlbuilding" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกอาคาร"
                                                    ValidationExpression="กรุณาเลือกอาคาร" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label3" runat="server" Text="ไลน์ผลิต" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtplace" runat="server" CssClass="form-control" PlaceHolder="........" />
                                            </div>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtplace" Font-Size="11"
                                                ErrorMessage="กรุณากรอกไลน์ผลิต"
                                                ValidationExpression="กรุณากรอกไลน์ผลิต"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtplace"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label4" CssClass="col-sm-3 control-label" runat="server" Text="สถานะการใช้งาน" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddStatusadd_masterplace" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="2" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="LinkButton4" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdInsertMaster_Place" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton5" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Place" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>


                        <div id="div_searchmaster_place" runat="server">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Label ID="Label122" runat="server" Text="สถานที่" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlmaster_searchlocation" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">ค้นหาสถานที่...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label ID="Label126" runat="server" Text="อาคาร" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlmaster_searchbuilding" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">ค้นหาอาคาร...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label127" runat="server" Text="ไลน์ผลิต" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtmaster_searchproduction" runat="server" CssClass="form-control" PlaceHolder="........" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <asp:LinkButton ID="LinkButton21" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSearchMaster_Production" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการค้นหารายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton22" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Production" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="overflow-x: auto; width: 100%">
                            <asp:GridView ID="GvMaster_Place" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="default"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="m0_plidx"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField HeaderText="สถานที่" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblm0_plidx" runat="server" Visible="false" Text='<%# Eval("m0_plidx") %>' />
                                            <asp:Label ID="lbLocName" runat="server" Text='<%# Eval("LocName") %>'></asp:Label>
                                            <asp:Label ID="lblLocIDX" runat="server" Visible="false" Text='<%# Eval("LocIDX") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtm0_plidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_plidx")%>' />
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="Label7" runat="server" Text="สถานที่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtLocIDX_edit" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("LocIDX")%>' />
                                                            <asp:DropDownList ID="ddlLocIDX_edit" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label8" runat="server" Text="อาคาร" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtBuildingIDX_edit" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("BuildingIDX")%>' />
                                                            <asp:DropDownList ID="ddlBuildingIDX_edit" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ไลน์ผลิต" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtproduction_name_edit" runat="server" CssClass="form-control" Text='<%# Eval("production_name")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtproduction_name_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกไลน์ผลิต"
                                                            ValidationExpression="กรุณากรอกไลน์ผลิต"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtproduction_name_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate_masterplace" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="2" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="อาคาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbBuildingName" runat="server" Text='<%# Eval("BuildingName") %>'></asp:Label>
                                            <asp:Label ID="lblBuildingIDX" Visible="false" runat="server" Text='<%# Eval("BuildingIDX") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ไลน์ผลิต" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="30%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbproduction" runat="server" Text='<%# Eval("production_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelMaster_place" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_plidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewMaster_Problem" runat="server">
            <div class="col-lg-12">
                <asp:Image ID="Image3" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr_problem.png" Style="height: 100%; width: 100%;" />
            </div>



            <div class="col-lg-12">
                <div class="panel panel-warning">
                    <div class="panel-heading" style="background-color: #FFD133; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-question-sign"></i><strong>&nbsp; เพิ่มหัวข้อปัญหา</strong></h3>
                    </div>

                    <div id="div_index_masterproblem" runat="server">
                        <div class="panel-body">
                            <div class="form-group">
                                <asp:LinkButton ID="btnshow_problem" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="เพิ่มหัวข้อปัญหา" runat="server" CommandName="CmdAddMaster_Problem" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>


                            <asp:Panel ID="Panel_AddMasterProblem" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="Label178" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddltype_group" ValidationGroup="Save" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator54" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddltype_group" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกกลุ่มวัตถุดิบ"
                                                        ValidationExpression="กรุณาเลือกกลุ่มวัตถุดิบ" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender76" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator54" Width="160" />

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label10" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddltype_product" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddltype_product" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกประเภทวัตถุดิบ"
                                                        ValidationExpression="กรุณาเลือกประเภทวัตถุดิบ" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Label12" runat="server" Text="หัวข้อปัญหา" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txttypeproblem" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txttypeproblem" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกหัวข้อปัญหา"
                                                    ValidationExpression="กรุณากรอกหัวข้อปัญหา"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txttypeproblem"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator4" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label13" CssClass="col-sm-3 control-label" runat="server" Text="สถานะการใช้งาน" />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlstatus_mastertypeproblem" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="2" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButton6" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdInsertMaster_Problem" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton7" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Problem" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>


                            <div id="div_searchmaster_problem" runat="server">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label179" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlmaster_searchgroupproblem" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">ค้นหากลุ่มวัตถุดิบ...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <asp:Label ID="Label129" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlmaster_searchproblem" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">ค้นหาประเภทวัตถุดิบ...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label128" runat="server" Text="หัวข้อปัญหา" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtmaster_searchtopicproblem" runat="server" CssClass="form-control" PlaceHolder="........" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-2">
                                            <asp:LinkButton ID="LinkButton23" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdSearchMaster_Problem" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการค้นหารายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton24" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Problem" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="overflow-x: auto; width: 100%">
                                <asp:GridView ID="GvMaster_Problem" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="default"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    DataKeyNames="m0_pridx"
                                    PageSize="10"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnRowEditing="Master_RowEditing"
                                    OnRowCancelingEdit="Master_RowCancelingEdit"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowUpdating="Master_RowUpdating">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>


                                        <asp:TemplateField HeaderText="กลุ่มวัตถุดิบ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblm0_plidx" runat="server" Visible="false" Text='<%# Eval("m0_pridx") %>' />
                                                <asp:Label ID="lblm0_group_tpidx" runat="server" Visible="false" Text='<%# Eval("m0_group_tpidx") %>' />
                                                <asp:Label ID="lbgroup_name" runat="server" Text='<%# Eval("group_name") %>'></asp:Label>
                                                <asp:Label ID="lblm0_tpidx" runat="server" Visible="false" Text='<%# Eval("m0_tpidx") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <div class="form-horizontal" role="form">
                                                    <div class="panel-heading">
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtm0_pridx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_pridx")%>' />
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <asp:Label ID="Label180" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtm0_group_tpidx_edit" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("m0_group_tpidx")%>' />
                                                                <asp:DropDownList ID="ddlm0_group_tpidx_edit" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0"> กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label7" runat="server" Text="ประเภทวัตถุดิบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtm0_tpidx_edit" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("m0_tpidx")%>' />
                                                                <asp:DropDownList ID="ddlm0_tpidx_edit" runat="server" CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>



                                                        <div class="form-group">
                                                            <asp:Label ID="Label26" runat="server" Text="หัวข้อปัญหา" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtproblem_name_edit" runat="server" CssClass="form-control" Text='<%# Eval("problem_name")%>' />
                                                            </div>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtproblem_name_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกหัวข้อปัญหา"
                                                                ValidationExpression="กรุณากรอกหัวข้อปัญหา"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                                ValidationGroup="Save_edit" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtproblem_name_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                        </div>


                                                        <div class="form-group">
                                                            <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList ID="ddStatusUpdate_masterproblem" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                                    <asp:ListItem Value="1" Text="Online" />
                                                                    <asp:ListItem Value="2" Text="Offline" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label197" CssClass="col-sm-3 control-label" runat="server" Text="" />
                                                            <div class="col-sm-8">
                                                                <p style="color: red">หลังจากทำการแก้ไขข้อมูล รบกวนแจ้งหัวหน้างานทำการอนุมัติรายการ</p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-2 col-sm-offset-10">
                                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>


                                            </EditItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ประเภทวัตถุดิบ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbtypeproduct_name" runat="server" Text='<%# Eval("typeproduct_name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="หัวข้อปัญหา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbBuildingName" runat="server" Text='<%# Eval("problem_name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatus_edit" runat="server" Visible="false" Text='<%# Eval("status_edit") %>'></asp:Label>
                                                <asp:Label ID="lblstatus_name" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label>
                                                <p />
                                                <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="History_problem" CssClass="btn btn-warning btn-sm" runat="server" OnCommand="btnCommand" CommandName="CmdSeeversion_problem" CommandArgument='<%# Eval("m0_pridx") %>' data-toggle="tooltip" title="ดูข้อมูลเปลี่ยนแปลง"><i class="glyphicon glyphicon-eye-open"></i></asp:LinkButton>
                                                <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                                <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelMaster_problem" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_pridx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-12" runat="server" id="Div_Log_Problem" visible="false">

                        <asp:FormView ID="fvapprove_problem" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <EditItemTemplate>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <asp:Label ID="Label187" runat="server" Text="ชื่อกลุ่มวัตถุดิบเก่า" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtproblemgroup_old" Enabled="false" Text='<%# Eval("group_name") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="txtproblem_rsecidx" Visible="false" Text='<%# Eval("rsecidx") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="txtproblem_job" Visible="false" Text='<%# Eval("JobGradeIDX") %>' runat="server" CssClass="form-control" />
                                            </div>

                                            <asp:Label ID="Label188" runat="server" Text="ชื่อกลุ่มวัตถุดิบใหม่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtgroup_new" Enabled="false" Text='<%# Eval("group_name_new") %>' runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <%-- <div class="form-group">
                                          
                                        </div>--%>

                                        <div class="form-group">
                                            <asp:Label ID="Label192" runat="server" Text="ชื่อประเภทวัตถุดิบเก่า" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="TextBox3" Enabled="false" Text='<%# Eval("typeproduct_name") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="txtproblem_m0_tpidx" Visible="false" Text='<%# Eval("m0_tpidx") %>' runat="server" CssClass="form-control" />
                                            </div>

                                            <asp:Label ID="Label193" runat="server" Text="ชื่อประเภทวัตถุดิบใหม่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="TextBox6" Enabled="false" Text='<%# Eval("typeproduct_name_new") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="txtproblem_m0_tpidx_new" Visible="false" Text='<%# Eval("m0_tpidx_new") %>' runat="server" CssClass="form-control" />

                                            </div>
                                        </div>
                                        <%-- <div class="form-group">
                                          
                                        </div>--%>

                                        <div class="form-group">
                                            <asp:Label ID="Label194" runat="server" Text="ชื่อปัญหาเก่า" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtproblem_name_old" Enabled="false" Text='<%# Eval("problem_name") %>' runat="server" CssClass="form-control" />
                                            </div>

                                            <asp:Label ID="Label195" runat="server" Text="ชื่อปัญหาใหม่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtproblem_name_new" Enabled="false" Text='<%# Eval("problem_name_new") %>' runat="server" CssClass="form-control" />
                                            </div>
                                        </div>

                                        <%-- <div class="form-group">
                                            
                                        </div>--%>
                                        <div class="form-group">
                                            <asp:Label ID="Label190" runat="server" Text="สถานะการใช้งานเก่า" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtproblem_statusold" Visible="false" Text='<%# Eval("status") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="TextBox1" Enabled="false" Text='<%# Eval("statusname_old") %>' runat="server" CssClass="form-control" />
                                            </div>

                                            <asp:Label ID="Label191" runat="server" Text="สถานะการใช้งานใหม่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtproblem_statusnew" Visible="false" Text='<%# Eval("status_new") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="TextBox2" Enabled="false" Text='<%# Eval("statusname_new") %>' runat="server" CssClass="form-control" />
                                            </div>
                                        </div>

                                        <%--<div class="form-group">
                                           
                                        </div>--%>
                                        <div class="form-group">
                                            <asp:Label ID="Label189" runat="server" Text="ผู้ดำเนินการแก้ไขรายการ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtemp_editgroup" Enabled="false" Text='<%# Eval("emp_name_th") %>' runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3 col-sm-offset-2">
                                                <asp:LinkButton ID="btnapprove_problem" CssClass="btn btn-success btn-sm" runat="server" CommandName="btnapprove_problem" CommandArgument="1" OnCommand="btnCommand" data-toggle="tooltip" title="Approve" OnClientClick="return confirm('คุณต้องการอนุมัติรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-ok-sign"></i> อนุมัติ</asp:LinkButton>
                                                <asp:LinkButton ID="btnunapprove_problem" CssClass="btn btn-danger btn-sm" runat="server" CommandName="btnapprove_problem" CommandArgument="2" OnCommand="btnCommand" data-toggle="tooltip" title="Cancel" OnClientClick="return confirm('คุณต้องการไม่อนุมัติรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-remove-sign"></i> ไม่อนุมัติ</asp:LinkButton>
                                                <asp:LinkButton ID="btncancel_problem" CssClass="btn btn-default btn-sm" runat="server" CommandName="btnbackapprove_problem" OnCommand="btnCommand" data-toggle="tooltip" title="Close"><i class="glyphicon glyphicon-home"></i> Close</asp:LinkButton>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:FormView>
                    </div>


                </div>
            </div>


        </asp:View>

        <asp:View ID="ViewMaster_Supplier" runat="server">
            <div class="col-lg-12">
                <asp:Image ID="Image_sup" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr_supplier.png" Style="height: 100%; width: 100%;" />
            </div>



            <div class="col-lg-12">
                <div class="panel panel-warning">
                    <div class="panel-heading" style="background-color: #FFD133; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; เพิ่มซัพภายนอก</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <asp:LinkButton ID="btnshow_supplier" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="เพิ่มซัพภายนอก" runat="server" CommandName="CmdAddMaster_Supplier" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                        </div>


                        <asp:Panel ID="Panel_AddMasterSupplier" runat="server" Visible="false">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">
                                        <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:Label ID="Label141" runat="server" Text="ประเภทการสร้าง" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:DropDownList ID="ddlchoose_addsup" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0" Text="กรุณาเลือกประเภทการสร้าง" />
                                                            <asp:ListItem Value="1" Text="สร้างเอง" />
                                                            <asp:ListItem Value="2" Text="Import File" />
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator38" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="ddlchoose_addsup" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกประเภทการสร้าง"
                                                            ValidationExpression="กรุณาเลือกประเภทการสร้าง" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender57" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator38" Width="160" />

                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlchoose_addsup" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                        <div class="col-md-12" id="div3" runat="server" style="color: transparent;">
                                            <asp:FileUpload ID="FileUpload3" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="xls" />
                                        </div>
                                        <div id="div_addmanualsup" runat="server" visible="false">

                                            <div class="form-group">
                                                <asp:Label ID="Label73" runat="server" Text="Supplier Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtsupname" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                    <asp:RequiredFieldValidator ID="RequiredFiheldValidator22" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtsupname" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกหัวข้อปัญหา"
                                                        ValidationExpression="กรุณากรอกหัวข้อปัญหา"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender32" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFiheldValidator22" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpresfsionValidator11" runat="server"
                                                        ValidationGroup="Save" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txttypeproblem"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender35" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpresfsionValidator11" Width="160" />

                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Label77" runat="server" Text="Email" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtemail_sup" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator23" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtemail_sup" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกหัวข้อปัญหา"
                                                    ValidationExpression="กรุณากรอกหัวข้อปัญหา"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender33" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator23" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtemail_sup"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender34" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator10" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label78" CssClass="col-sm-3 control-label" runat="server" Text="สถานะการใช้งาน" />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlstatus_sup" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="2" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButton9" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdInsertMaster_Supplier" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton10" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Supplier" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="div_addimportsup" runat="server" visible="false">

                                            <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label142" class="col-sm-3 control-label text_right" runat="server" Text="Upload File : " />
                                                        <div class="col-sm-6">
                                                            <asp:FileUpload ID="UploadFileSup" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi max-1" accept="xls" />

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator39"
                                                                runat="server" ControlToValidate="UploadFileSup" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="กรุณาอัพโหลดไฟล์"
                                                                ValidationGroup="Save" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender58" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator39" Width="200" PopupPosition="BottomLeft" />

                                                            <small>
                                                                <p class="help-block"><font color="red">**Attach File name xls</font></p>
                                                            </small>
                                                        </div>
                                                    </div>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnAdddata_supplier" />
                                                </Triggers>
                                            </asp:UpdatePanel>


                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="btnAdddata_supplier" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdImportMaster_Sup" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton32" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Supplier" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </asp:Panel>


                        <div id="div_searchmaster_supplier" runat="server">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Label ID="Label130" runat="server" Text="ชื่อ Supplier" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtmaster_searchsupplier" runat="server" CssClass="form-control" PlaceHolder="........" />

                                    </div>
                                    <asp:Label ID="Label131" runat="server" Text="Email" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtmaster_searchemail" runat="server" CssClass="form-control" PlaceHolder="........" />
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <asp:LinkButton ID="LinkButton25" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSearchMaster_Supplier" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการค้นหารายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton26" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Supplier" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="overflow-x: auto; width: 100%">
                            <asp:GridView ID="GvMaster_Supplier" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="default"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="m0_supidx"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField HeaderText="ชื่อ Supplier" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblm0_supidx" runat="server" Visible="false" Text='<%# Eval("m0_supidx") %>' />
                                            <asp:Label ID="lbsup_name" runat="server" Text='<%# Eval("sup_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtm0_supidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_supidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label7" runat="server" Text="ชื่อ Supplier" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtsupname_edit" runat="server" CssClass="form-control" Text='<%# Eval("sup_name")%>' />

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator28" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtemail_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกชื่อ Supplier"
                                                                ValidationExpression="กรุณากรอกชื่อ Supplier"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender41" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                                                ValidationGroup="Save_edit" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtemail_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender43" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                        </div>
                                                    </div>



                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="Email" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtemail_edit" runat="server" CssClass="form-control" Text='<%# Eval("email")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtemail_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกหัวข้อปัญหา"
                                                            ValidationExpression="กรุณากรอกหัวข้อปัญหา"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtemail_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label99" runat="server" Text="User" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtuser_login" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("user_login")%>' />


                                                        </div>
                                                    </div>

                                                    <div class="form-group" runat="server" visible="false">
                                                        <asp:Label ID="Label101" runat="server" Text=" Password" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtpass_login" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("pass_login")%>' />

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate_mastersupplier" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="2" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Email" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbemail" runat="server" Text='<%# Eval("email") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Username" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="12%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbuser_login" runat="server" Text='<%# Eval("user_login") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Password" Visible="false" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbpass_login" runat="server" Text='<%# Eval("pass_login") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="12%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnreset_pass" CssClass="btn btn-default btn-sm" runat="server" CommandName="btnreset_pass" OnCommand="btnCommand" CommandArgument='<%# Eval("m0_supidx") %>' OnClientClick="return confirm('คุณต้องการ Reset Passwordรายการนี้ใช่หรือไม่ ?')" data-toggle="tooltip" title="Reset Password"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelMaster_Supplier" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_supidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>



        </asp:View>

        <asp:View ID="ViewMaster_Mat" runat="server">
            <div class="col-lg-12">
                <asp:Image ID="Image9" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr_mat.png" Style="height: 100%; width: 100%;" />
            </div>


            <div class="col-lg-12">
                <div class="panel panel-warning">
                    <div class="panel-heading" style="background-color: #FFD133; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; เพิ่มเลข Mat</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <asp:LinkButton ID="btnshow_mat" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="เพิ่ม Material" runat="server" CommandName="CmdAddMaster_Mat" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                        </div>


                        <asp:Panel ID="Panel_AddMasterMaterial" runat="server" Visible="false">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">
                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:Label ID="Label102" runat="server" Text="ประเภทการสร้าง" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:DropDownList ID="ddltype_addmat" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0" Text="กรุณาเลือกประเภทการสร้าง" />
                                                            <asp:ListItem Value="1" Text="สร้างเอง" />
                                                            <asp:ListItem Value="2" Text="Import File" />
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator31" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="ddltype_addmat" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกประเภทการสร้าง"
                                                            ValidationExpression="กรุณาเลือกประเภทการสร้าง" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender47" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator31" Width="160" />

                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddltype_addmat" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                        <div class="col-md-12" id="div1" runat="server" style="color: transparent;">
                                            <asp:FileUpload ID="FileUpload1" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="xls" />
                                        </div>
                                        <div id="div_addmanual" runat="server" visible="false">

                                            <div class="form-group">
                                                <asp:Label ID="Label86" runat="server" Text="Mat Number" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtmatno" MaxLength="7" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator29" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtmatno" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกMat Number"
                                                        ValidationExpression="กรุณากรอกMat Number"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender44" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator29" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                                                        ValidationGroup="Save" Display="None"
                                                        ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                        ControlToValidate="txtmatno"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender45" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator13" Width="160" />

                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Label88" runat="server" Text="Mat Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtmatname" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator30" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtmatname" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกMat Name"
                                                    ValidationExpression="กรุณากรอกMat Name"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender46" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator30" Width="160" />
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label90" CssClass="col-sm-3 control-label" runat="server" Text="สถานะการใช้งาน" />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlstatus_mastermat" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="2" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButton11" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdInsertMaster_Mat" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton12" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Mat" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>

                                        </div>

                                        <div id="div_addimport" runat="server" visible="false">

                                            <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div class="form-group">
                                                        <asp:Label ID="Label181" class="col-sm-3 control-label text_right" runat="server" />
                                                        <div class="col-sm-6">
                                                            <u>
                                                                <asp:HyperLink ID="HyperLink1" runat="server" ForeColor="red"
                                                                    NavigateUrl="https://drive.google.com/file/d/1AxXVO4xdaeP9R3I8Zs5bBx4iUBo8jBIX/view?usp=sharing" Target="_blank"
                                                                    CommandName="_divImportMat"
                                                                    OnCommand="btnCommand" Text="Template สำหรับ Import กรุณาอย่าเปลี่ยน Format!!" /></u>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnAdddata_mat" />
                                                </Triggers>
                                            </asp:UpdatePanel>

                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label1h0s" class="col-sm-3 control-label text_right" runat="server" Text="Upload File : " />
                                                        <div class="col-sm-6">
                                                            <asp:FileUpload ID="UploadFileMat" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi max-1" accept="xls" />

                                                            <asp:RequiredFieldValidator ID="RequiredFielkdValidator18"
                                                                runat="server" ControlToValidate="UploadFileMat" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="กรุณาอัพโหลดไฟล์"
                                                                ValidationGroup="Save" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="VajlidatorCalloutExtender26" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFielkdValidator18" Width="200" PopupPosition="BottomLeft" />

                                                            <small>
                                                                <p class="help-block"><font color="red">**Attach File name xls</font></p>
                                                            </small>
                                                        </div>
                                                    </div>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnAdddata_mat" />
                                                </Triggers>
                                            </asp:UpdatePanel>


                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="btnAdddata_mat" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdImportMaster_Mat" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton13" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Mat" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>


                        <div id="div_searchmaster_mat" runat="server">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Label ID="Label132" runat="server" Text="Mat Number" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtmaster_searchmatno" runat="server" CssClass="form-control" PlaceHolder="........" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                                            ValidationGroup="Search_Mat" Display="None"
                                            ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                            ControlToValidate="txtmaster_searchmatno"
                                            ValidationExpression="^[\s\S]{0,1000}$"
                                            SetFocusOnError="true" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender56" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator16" Width="160" />

                                    </div>
                                    <asp:Label ID="Label133" runat="server" Text="Mat Name" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtmaster_searchmatname" runat="server" CssClass="form-control" PlaceHolder="........" />
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <asp:LinkButton ID="LinkButton27" ValidationGroup="Search_Mat" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSearchMaster_Mat" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการค้นหารายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton28" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Mat" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="overflow-x: auto; width: 100%">
                            <asp:GridView ID="GvMaster_Mat" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="default"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="matidx"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField HeaderText="Mat Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblmatidx" runat="server" Visible="false" Text='<%# Eval("matidx") %>' />
                                            <asp:Label ID="lbmat_no" runat="server" Text='<%# Eval("mat_no") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtmatidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("matidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label7" runat="server" Text="Mat Number" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtmatno_edit" MaxLength="7" runat="server" CssClass="form-control" Text='<%# Eval("mat_no")%>' />

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator28" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtmatno_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกMat Number"
                                                                ValidationExpression="กรุณากรอกMat Number"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender41" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator28" Width="160" />

                                                            <asp:RegularExpressionValidator ID="RegularExpressiosnValidator13" runat="server"
                                                                ValidationGroup="Save" Display="None"
                                                                ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                                ControlToValidate="txtmatno_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender45" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressiosnValidator13" Width="160" />

                                                        </div>
                                                    </div>



                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="Mat Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtmatname_edit" runat="server" CssClass="form-control" Text='<%# Eval("matname")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtmatname_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกMat Name"
                                                            ValidationExpression="กรุณากรอกMat Name"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate_mastermat" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="2" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Mat Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbmatname" runat="server" Text='<%# Eval("matname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelMaster_Mat" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("matidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>

                    </div>
                </div>
            </div>


        </asp:View>

        <asp:View ID="ViewMaster_Material_Risk" runat="server">
            <div class="col-lg-12">
                <asp:Image ID="Image_matrisk" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr_matrisk.png" Style="height: 100%; width: 100%;" />
            </div>



            <div class="col-lg-12">
                <div class="panel panel-danger">
                    <div class="panel-heading" style="background-color: #FFD133; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-exclamation-sign"></i><strong>&nbsp; เพิ่ม Material Risk Requires by BRC (only Rojana)</strong></h3>
                    </div>
                    <div id="div_index_mastermatrisk" runat="server">
                        <div class="panel-body">
                            <div class="form-group">
                                <asp:LinkButton ID="btnshow_matrisk" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="เพิ่ม Material Risk" runat="server" CommandName="CmdAddMaster_Risk" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>


                            <asp:Panel ID="Panel_AddMasterMaterialRisk" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">
                                            <div class="form-group">
                                                <asp:Label ID="Label118" runat="server" Text="Material Risk" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtmat_risk" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator36" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtmat_risk" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกหน่วยวัด"
                                                    ValidationExpression="กรุณากรอกหน่วยวัด"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender53" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator36" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtmat_risk"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender54" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator15" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label119" CssClass="col-sm-3 control-label" runat="server" Text="สถานะการใช้งาน" />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlstatus_mastermatrisk" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="2" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButton14" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdInsertMaster_Risk" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton15" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancelMaster_Risk" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <div class="form-group" id="div_searchmaster_matrisk" runat="server">
                                <asp:Label ID="Label134" runat="server" Text="Material Risk" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtmaster_searchmatrisk" runat="server" CssClass="form-control" PlaceHolder="........" />
                                </div>
                                <asp:LinkButton ID="LinkButton29" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdSearchMaster_MatRisk" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการค้นหารายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton30" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_MatRisk" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>

                            </div>

                            <div style="overflow-x: auto; width: 100%">
                                <asp:GridView ID="GvMaster_MatRisk" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="default"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    DataKeyNames="m0_mridx"
                                    PageSize="10"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnRowEditing="Master_RowEditing"
                                    OnRowCancelingEdit="Master_RowCancelingEdit"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowUpdating="Master_RowUpdating">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="#" HeaderStyle-Width="5%">

                                            <ItemTemplate>
                                                <asp:Label ID="lblm0_mridx" runat="server" Visible="false" Text='<%# Eval("m0_mridx") %>' />
                                                <%# (Container.DataItemIndex +1) %>
                                            </ItemTemplate>


                                            <EditItemTemplate>
                                                <div class="form-horizontal" role="form">
                                                    <div class="panel-heading">
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtm0_mridx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_mridx")%>' />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label26" runat="server" Text="Material Risk" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtmaterial_risk_edit" runat="server" CssClass="form-control" Text='<%# Eval("material_risk")%>' />
                                                            </div>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtmaterial_risk_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอก Material Risk"
                                                                ValidationExpression="กรุณากรอก Material Risk"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                                ValidationGroup="Save_edit" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtmaterial_risk_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                        </div>


                                                        <div class="form-group">
                                                            <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList ID="ddStatusUpdate_matrisk" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                                    <asp:ListItem Value="1" Text="Online" />
                                                                    <asp:ListItem Value="2" Text="Offline" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label197" CssClass="col-sm-3 control-label" runat="server" Text="" />
                                                            <div class="col-sm-8">
                                                                <p style="color: red">หลังจากทำการแก้ไขข้อมูล รบกวนแจ้งหัวหน้างานทำการอนุมัติรายการ</p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-2 col-sm-offset-10">
                                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>


                                            </EditItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Material Risk" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="70%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbname" runat="server" Text='<%# Eval("material_risk") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>



                                        <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatus_edit" runat="server" Visible="false" Text='<%# Eval("status_edit") %>'></asp:Label>
                                                <asp:Label ID="lblstatus_name" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label>
                                                <p />
                                                <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="History_Material_Risk" CssClass="btn btn-warning btn-sm" runat="server" OnCommand="btnCommand" CommandName="CmdSeeversion_matrisk" CommandArgument='<%# Eval("m0_mridx") %>' data-toggle="tooltip" title="ดูข้อมูลเปลี่ยนแปลง"><i class="glyphicon glyphicon-eye-open"></i></asp:LinkButton>
                                                <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                                <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelMaster_Risk" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_mridx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-12" runat="server" id="Div_Log_Matrisk" visible="false">

                        <asp:FormView ID="fvapprove_matrisk" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <EditItemTemplate>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <asp:Label ID="Label187" runat="server" Text="ชื่อ Material Risk เก่า" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtmatrisk_old" Enabled="false" Text='<%# Eval("material_risk") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="txtmatrisk_rsecidx" Visible="false" Text='<%# Eval("rsecidx") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="txtmatrisk_job" Visible="false" Text='<%# Eval("JobGradeIDX") %>' runat="server" CssClass="form-control" />
                                            </div>

                                            <asp:Label ID="Label188" runat="server" Text="ชื่อ Material Risk ใหม่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtmatrisk_new" Enabled="false" Text='<%# Eval("material_risk_new") %>' runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <%-- <div class="form-group">
                                          
                                        </div>--%>

                                        <div class="form-group">
                                            <asp:Label ID="Label190" runat="server" Text="สถานะการใช้งานเก่า" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtmatrisk_statusold" Visible="false" Text='<%# Eval("status") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="TextBox1" Enabled="false" Text='<%# Eval("statusname_old") %>' runat="server" CssClass="form-control" />
                                            </div>

                                            <asp:Label ID="Label191" runat="server" Text="สถานะการใช้งานใหม่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtmatrisk_statusnew" Visible="false" Text='<%# Eval("status_new") %>' runat="server" CssClass="form-control" />
                                                <asp:TextBox ID="TextBox2" Enabled="false" Text='<%# Eval("statusname_new") %>' runat="server" CssClass="form-control" />
                                            </div>
                                        </div>

                                        <%--  <div class="form-group">
                                            
                                        </div>--%>
                                        <div class="form-group">
                                            <asp:Label ID="Label189" runat="server" Text="ผู้ดำเนินการแก้ไขรายการ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtemp_editgroup" Enabled="false" Text='<%# Eval("emp_name_th") %>' runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3 col-sm-offset-2">
                                                <asp:LinkButton ID="btnapprove_matrisk" CssClass="btn btn-success btn-sm" runat="server" CommandName="btnapprove_matrisk" CommandArgument="1" OnCommand="btnCommand" data-toggle="tooltip" title="Approve" OnClientClick="return confirm('คุณต้องการอนุมัติรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-ok-sign"></i> อนุมัติ</asp:LinkButton>
                                                <asp:LinkButton ID="btnunapprove_matrisk" CssClass="btn btn-danger btn-sm" runat="server" CommandName="btnapprove_matrisk" CommandArgument="2" OnCommand="btnCommand" data-toggle="tooltip" title="Cancel" OnClientClick="return confirm('คุณต้องการไม่อนุมัติรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-remove-sign"></i> ไม่อนุมัติ</asp:LinkButton>
                                                <asp:LinkButton ID="btncancel_matrisk" CssClass="btn btn-default btn-sm" runat="server" CommandName="btnbackapprove_matrisk" OnCommand="btnCommand" data-toggle="tooltip" title="Close"><i class="glyphicon glyphicon-home"></i> Close</asp:LinkButton>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:FormView>
                    </div>

                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewReport" runat="server">
            <div class="col-lg-12">
                <div class="bg-system">

                    <asp:Image ID="Image8" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr_report.png" Style="width: 100%;" />
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-danger" style="border-width: 2pt">
                    <div class="panel-heading" style="background-color: #ff9999; color: black;">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; รายงาน</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <asp:Panel ID="panel_table" runat="server" Visible="true">


                                <div class="form-group">
                                    <asp:Label ID="Label9" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-2">
                                        <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="SelectedIndexChanged">
                                            <asp:ListItem Value="0">เลือกเงื่อนไข</asp:ListItem>
                                            <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                            <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                            <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-sm-3 ">

                                        <div class='input-group date'>
                                            <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ValidationGroup="SearchReport" runat="server" Display="None"
                                                ControlToValidate="AddStartdate" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender31" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator21" Width="160" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <asp:TextBox ID="AddEndDate" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                        </div>
                                    </div>
                                    <p class="help-block"><font color="red">**</font></p>
                                </div>


                                <div class="form-group">


                                    <asp:Label ID="Label67" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlrdeptidx_search" runat="server" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกฝ่าย....</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>

                                    <asp:Label ID="Label71" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlrsecidx_search" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกแผนก....</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>


                                <div class="form-group">

                                    <asp:Label ID="Label76" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlLocate_search" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกสถานที่....</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <asp:Label ID="Label68" CssClass="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlBuilding_search" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกอาคาร....</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label80" CssClass="col-sm-2 control-label" runat="server" Text="ไลน์ผลิต : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlProduction_name_search" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกไลน์ผลิต....</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>


                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label147" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlgroupproduct_search" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:Label ID="Label81" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทวัตถุดิบ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddltypeproduct_search" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกประเภทวัตถุดิบ....</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <asp:Label ID="Label153" runat="server" Text="โครงสร้างวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddltypeproduct_search_m1" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="0">กรุณาโครงสร้างวัตถุดิบ...</asp:ListItem>
                                        </asp:DropDownList>


                                    </div>
                                    <asp:Label ID="Label155" runat="server" Text="โครงสร้างย่อยวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddltypeproduct_search_m2" ValidationGroup="Save" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="0">กรุณาเลือกโครงสร้างย่อยวัตถุดิบ...</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>


                                <div class="form-group">

                                    <asp:Label ID="Label69" CssClass="col-sm-2 control-label" runat="server" Text="ปัญหาที่พบ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlproblem_search" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกปัญหาที่พบ....</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <asp:Label ID="Label70" CssClass="col-sm-2 control-label" runat="server" Text="Supplier : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsupplier_search" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกSupplier....</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>


                                <div class="form-group">

                                    <asp:Label ID="Label196" CssClass="col-sm-2 control-label" runat="server" Text="ระดับปัญหา : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddltypeproblem_search" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกระดับปัญหา....</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label75" CssClass="col-sm-2 control-label" runat="server" Text="รหัสรายการ : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtDocCode" runat="server" CssClass="form-control" placeholder="รหัสรายการ ....."></asp:TextBox>
                                    </div>
                                    <asp:Label ID="Label72" CssClass="col-sm-2 control-label" runat="server" Text="รหัสพนักงาน : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtEmpCOde_search" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน ....."></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <asp:Label ID="Label74" CssClass="col-sm-2 control-label" runat="server" Text="สถานะผู้ดำเนินการ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlactor_search" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกสถานะผู้ดำเนินการ....</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                    <asp:Label ID="Label117" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานะรายการ" />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlstatus_search" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="0">กรุณาเลือกสถานะอนุมัติ...</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <div id="divcolumn" runat="server" visible="true" style="overflow-y: auto; width: 100%">

                                        <asp:Label ID="Label20" runat="server" Text="เลือกColumns" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-10">
                                            <div class="checkbox checkbox-primary">
                                                <asp:CheckBoxList ID="YrChkBoxColumns"
                                                    runat="server"
                                                    RepeatColumns="2"
                                                    RepeatDirection="Vertical"
                                                    RepeatLayout="Table"
                                                    Width="100%"
                                                    TextAlign="Right">
                                                </asp:CheckBoxList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <asp:UpdatePanel ID="Update_PnTableReportDetail" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <asp:LinkButton ID="btnsearch" CssClass="btn btn-success btn-sm" ValidationGroup="SearchReport" runat="server" CommandName="btnsearch_report" OnCommand="btnCommand" title="Search"><i class="fa fa-search"></i> Search</asp:LinkButton>
                                                <asp:LinkButton ID="btnexport" CssClass="btn btn-primary  btn-sm" runat="server" CommandName="btnexport" OnCommand="btnCommand" title="Export"><i class="fa fa-file-excel-o"></i> Export Excel</asp:LinkButton>
                                                <asp:LinkButton ID="btnrefresh" CssClass="btn btn-info  btn-sm" runat="server" CommandName="btnrefresh_report" OnCommand="btnCommand" title="Refresh"><i class="glyphicon glyphicon-refresh"></i> Refresh</asp:LinkButton>

                                            </div>
                                        </div>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnexport" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>



                            <div style="overflow-x: auto; width: 100%">
                                <asp:GridView ID="GvReport" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-BackColor="#ff9999"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="2pt"
                                    PageSize="10"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    DataKeyNames="u0_docidx">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="รหัสรายการ" ItemStyle-Width="10%" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#ffe3dd" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Labeasl15" runat="server">รหัสรายการ: </asp:Label></strong>
                                                    <asp:Label ID="Label79" runat="server" Visible="false" Text='<%# Eval("CEmpIDX") %>'></asp:Label>
                                                    <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                                    <asp:Label ID="lblu1_docidx" runat="server" Visible="false" Text='<%# Eval("u1_docidx") %>'></asp:Label>
                                                    <asp:Label ID="lbldoccode" runat="server" Text='<%# Eval("doc_code") %>'></asp:Label>
                                                    <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                                    <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                                    <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                                    </p>
                                                <strong>
                                                    <asp:Label ID="Label29" runat="server">วันที่แจ้ง : </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("create_date") %>' />
                                                    </p>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลสถานที่" ItemStyle-Width="10%" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#fff0ec" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>

                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Labesl15" runat="server">สถานที่: </asp:Label></strong>
                                                    <asp:Literal ID="litLocIDX" Visible="false" runat="server" Text='<%# Eval("LocIDX") %>' />
                                                    <asp:Literal ID="LitLocName" runat="server" Text='<%# Eval("LocName") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labeld24" runat="server">อาคาร: </asp:Label></strong>
                                                    <asp:Literal ID="litBuildingIDX" Visible="false" runat="server" Text='<%# Eval("BuildingIDX") %>' />
                                                    <asp:Literal ID="LitBuildingName" runat="server" Text='<%# Eval("BuildingName") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label25" runat="server">ไลน์ผลิต: </asp:Label></strong>
                                                    <asp:Literal ID="litm0_plidx" Visible="false" runat="server" Text='<%# Eval("m0_plidx") %>' />
                                                    <asp:Literal ID="Litproduction_name" runat="server" Text='<%# Eval("production_name") %>' />
                                                    </p>
                                 
                                                </small>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลผู้แจ้ง" ItemStyle-Width="15%" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#ffe3dd" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <small>
                                                    <%--   <strong>
                                                    <asp:Label ID="Labeslr15" runat="server">องค์กร: </asp:Label></strong>
                                                <asp:Literal ID="litorg_idx" Visible="false" runat="server" Text='<%# Eval("orgidx") %>' />
                                                <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("org_name_th") %>' />
                                                </p>--%>
                                                    <strong>
                                                        <asp:Label ID="Labelsddw24" runat="server">ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="litrdept_idx" Visible="false" runat="server" Text='<%# Eval("rdeptidx") %>' />
                                                    <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labsel25" runat="server">แผนก: </asp:Label></strong>
                                                    <asp:Literal ID="litrsec_idx" Visible="false" runat="server" Text='<%# Eval("rsecidx") %>' />
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                                    </p>
                                                 
                                                 <strong>
                                                     <asp:Label ID="Label26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                                    <asp:Literal ID="litcemp_idx" Visible="false" runat="server" Text='<%# Eval("CEmpIDX") %>' />
                                                    </p>
                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลแจ้งปัญหา" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#fff0ec" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>

                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label5" runat="server">กลุ่มวัตถุดิบ: </asp:Label></strong>
                                                    <asp:Literal ID="litgroup_namere" runat="server" Text='<%# Eval("group_name") %>' />
                                                    </p>
                                                <strong>
                                                    <asp:Label ID="Labsdel15" runat="server">ประเภทวัตถุดิบ: </asp:Label></strong>
                                                    <asp:Literal ID="littypeproduct_namere" runat="server" Text='<%# Eval("typeproduct_name") %>' />
                                                    </p>
                                                <strong>
                                                    <asp:Label ID="Label10" runat="server">โครงสร้างวัตถุดิบ: </asp:Label></strong>
                                                    <asp:Literal ID="littypeproduct_name_m1" runat="server" Text='<%# Eval("typeproduct_name_m1") %>' />
                                                    </p>
                                                <strong>
                                                    <asp:Label ID="Label11" runat="server">โครงสร้างย่อยวัตถุดิบ: </asp:Label></strong>
                                                    <asp:Literal ID="littypeproduct_name_m2" runat="server" Text='<%# Eval("typeproduct_name_m2") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labqel24" runat="server">ประเภทปัญหา : </asp:Label></strong>
                                                    <asp:Literal ID="litproblem_name" runat="server" Text='<%# Eval("problem_name") %>' />
                                                    </p>
                                 
                                                </small>
                                                <%-- <small>
                                                <strong>
                                                    <asp:Label ID="Labsel13f5" runat="server">ประเภทวัตถุดิบ: </asp:Label></strong>
                                                <asp:Literal ID="Labsel13f52" runat="server" Text='<%# Eval("typeproduct_name") %>' />
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Labqelww324" runat="server">ประเภทปัญหา : </asp:Label></strong>
                                                <asp:Literal ID="Labqelww3241" runat="server" Text='<%# Eval("problem_name") %>' />
                                                </p>
                                  
                                            </small>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลSupplier" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#fff0ec" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Labsewq1l15" runat="server">Supplier Name: </asp:Label></strong>
                                                    <asp:Literal ID="Labsewq1l151" runat="server" Text='<%# Eval("sup_name") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labqel224" runat="server">PO Number : </asp:Label></strong>
                                                    <asp:Literal ID="Labqel2242" runat="server" Text='<%# Eval("po_no") %>' />
                                                    </p>
                                    <strong>
                                        <asp:Label ID="Labe3l136" runat="server">Invoice No : </asp:Label></strong>
                                                    <asp:Literal ID="Labe3l1363" runat="server" Text='<%# Eval("tv_no") %>' />
                                                    </p>
                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายละเอียดสินค้า" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#fff0ec" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Labsel15" runat="server">Mat No: </asp:Label></strong>
                                                    <asp:Literal ID="litsumheeadcore" runat="server" Text='<%# Eval("mat_no") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labq2el24" runat="server">Mat Name : </asp:Label></strong>
                                                    <asp:Literal ID="litmatname" runat="server" Text='<%# Eval("matname") %>' />
                                                    </p>
                                    <strong>
                                        <asp:Label ID="Label136" runat="server">Batch : </asp:Label></strong>
                                                    <asp:Literal ID="litbatchnumber" runat="server" Text='<%# Eval("batchnumber") %>' />
                                                    </p>

                                                 <strong>
                                                     <asp:Label ID="Label138" runat="server">จำนวนรับเข้า : </asp:Label></strong>
                                                    <asp:Literal ID="litqty_get" runat="server" Text='<%# Eval("qty_get") %>' />
                                                    <asp:Literal ID="Literal10" runat="server" Text='<%# Eval("unit_name") %>' />
                                                    </p>

                                                 <strong>
                                                     <asp:Label ID="Label139" runat="server">จำนวนพบปัญหา : </asp:Label></strong>
                                                    <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("qty_problem") %>' />
                                                    <asp:Literal ID="Literal11" runat="server" Text='<%# Eval("unit_name_problem") %>' />
                                                    </p>

                                                 <strong>
                                                     <asp:Label ID="Label140" runat="server">จำนวนสุ่ม : </asp:Label></strong>
                                                    <asp:Literal ID="Literal9" runat="server" Text='<%# Eval("qty_random") %>' />
                                                    <asp:Literal ID="Literal12" runat="server" Text='<%# Eval("unit_name_random") %>' />
                                                    </p>
                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ประเมินระดับปัญหา" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#fff0ec" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Leabsel15" runat="server">ระดับปัญหา: </asp:Label></strong>
                                                    <asp:Literal ID="Leabsel15w" runat="server" Text='<%# Eval("type_problem_name") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labqewl24" runat="server">ประเภทการส่งคืน : </asp:Label></strong>
                                                    <asp:Literal ID="Labqe3wl24q" runat="server" Text='<%# Eval("rollback_name") %>' />
                                                    </p>
                                    <strong>
                                        <asp:Label ID="Label13q6" runat="server">จำนวนส่งคืน : </asp:Label></strong>
                                                    <asp:Literal ID="Label13q6w" runat="server" Text='<%# Eval("qty_rollback") %>' />
                                                    <asp:Literal ID="Label13q62" runat="server" Text='<%# Eval("unit_name_rollback") %>' />

                                                    </p>

                                                 <strong>
                                                     <asp:Label ID="Labewl138" runat="server">สาเหตุส่งคืน : </asp:Label></strong>
                                                    <asp:Literal ID="Labewl138w" runat="server" Text='<%# Eval("detail_rollback") %>' />
                                                    </p>
                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="8%" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#ffe3dd" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <small>
                                                    <b>
                                                        <asp:Label ID="lblStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                                    </b>
                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#fff0ec" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnviewdetail" CssClass="btn btn-warning btn-sm" runat="server" OnCommand="btnCommand" ToolTip="View" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("doc_code")+ ";" + Eval("CEmpIDX")  + ";" + "3"%>'><i class="	fa fa-book"></i></asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </asp:View>

    </asp:MultiView>
    <%--    <script type="text/javascript">
        function openModal() {
            $('#Seeversion_Group').modal('show');

        }

    </script>--%>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });


    </script>


    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileProblem").MultiFile();
            }
        })
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileProblem_QA").MultiFile();
            }
        })
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileProblem_QA_edit").MultiFile();
            }
        })
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileMat").MultiFile();
            }
        })
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileSup").MultiFile();
            }
        })
    </script>

    <%-- <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileProblem_Detail").MultiFile();
            }
        })
    </script>--%>

    <%--  <script src='<%=ResolveUrl("~/Scripts/jquery.MultiFileNew.js")%>'></script>

    <script>
        $(function () {
            $('.UploadFileProblem').MultiFile({

            });
            alert("1");
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.UploadFileProblem').MultiFile({

                });
                alert("2");
            });
    </script>--%>
</asp:Content>

