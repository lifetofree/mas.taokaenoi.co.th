﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="hr_health_check_print.aspx.cs" Inherits="websystem_hr_hr_health_check_print" %>

<%--<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_health_check.aspx.cs" Inherits="websystem_hr_hr_health_check" %>--%>

<!DOCTYPE html>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

    <title>Print Sample Code</title>

    <style type="text/css" media="print,screen">
        @page {
            size: A4 landscape;
            margin: 25px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

        .formPrint {
            margin: 10pt; /*from 5 to 10*/
            padding: 10pt; /*add*/
            width: 842pt;
        }
    </style>

</head>
<body onload="window.print()">
    <form id="form1" runat="server" width="100%">
        <%--<script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>--%>

        <script src='<%= ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
        <script src='<%= ResolveUrl("~/Scripts/jquery-ui-1.11.2.min.js") %>'></script>
        <script src='<%= ResolveUrl("~/Scripts/moment.min.js") %>'></script>
        <script src='<%= ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%= ResolveUrl("~/Scripts/emps-scripts/bootstrap-datetimepicker.min.js") %>'></script>
        <script src='<%= ResolveUrl("~/Scripts/custom.js") %>'></script>
        <script src='<%= ResolveUrl("~/Scripts/emps-scripts/bootstrap-clockpicker.min.js") %>'></script>
        <script src='<%= ResolveUrl("~/Scripts/emps-scripts/jquery-clockpicker.min.js") %>'></script>
        <script src='<%= ResolveUrl("~/Scripts/emps-scripts/empshift-fullcalendar.js") %>'></script>
        <script src='<%= ResolveUrl("~/Scripts/emps-scripts/empshift-fullcalendar-lang-th.js") %>'></script>
        <script src='<%= ResolveUrl("~/Content/emps-contents/colorpicker/js/bootstrap-colorpicker.min.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/Highcharts-4.0.1/js/highcharts.js")%>'></script>
        <script src='<%=ResolveUrl("~/Scripts/menu-scripts/backoffice.js")%>'></script>
        <script src='<%=ResolveUrl("~/Scripts/menu-scripts/jquery.metisMenu.js")%>'></script>
        <script src='<%=ResolveUrl("~/Scripts/jquery.MultiFile.js")%>'></script>
        <script src='<%=ResolveUrl("~/Scripts/tinymce/tinymce.min.js")%>'></script>

        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <div class="formPrint">
            <div class="headOrg" style="text-align: center; padding: 10px;">
                <h3>
                    <asp:Label ID="lbTitlePrint" runat="server"></asp:Label>
                </h3>
                <div class="col-md-12">
                    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
                </div>

            </div>
            <div id="divHistoryInjuryProfile" runat="server" class="col-md-12" visible="false">
                <asp:GridView ID="gvInjuryProfile"
                    runat="server" Width="100%"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive"
                    HeaderStyle-CssClass="info">
                    <EmptyDataTemplate>
                        <div class="text-center">-- ไม่พบข้อมูล --</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="วันเดือนปี" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_date_injuly_history_Profile" runat="server" Text='<%# Eval("date_injuly") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ส่วนของร่างกายที่บาดเจ็บหรือการเจ็บป่วย" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_injury_detail_history_Profile" runat="server" Text='<%# Eval("injury_detail") %>' />
                                </small>

                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ส่วนของการบาดเจ็บหรือการเจ็บป่วย" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_cause_Injury_history_Profile" runat="server" Text='<%# Eval("cause_Injury") %>' />
                                </small>

                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ระดับความรุนแรง" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                            <ItemTemplate>
                                <small>

                                    <asp:Label ID="Detail_Violence_Profile" runat="server"> 
                                                    <p><b>ทุพพลภาพ :</b> <%# Eval("disability") %></p>
                                                    <p><b>สูญเสียอวัยวะบางส่วน :</b> <%# Eval("lost_organ") %></p>
                                                    <p><b>ทำงานไม่ได้ช่วยคราว :</b> <%# Eval("detail_not_working") %></p>
                                  
                                    </asp:Label>
                                </small>

                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>

            <asp:FormView ID="fvHistoryEmployee" runat="server" DefaultMode="ReadOnly" Width="100%">
                <ItemTemplate>
                    <div class="panel panel-primary">
                        <div style="padding: 50px;">
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <label>รหัสพนักงาน : </label>
                                            <asp:Label ID="tbEmpCode_History" runat="server" Text='<%# Eval("EmpCode") %>'></asp:Label>
                                            <%--<asp:TextBox ID="tbEmpCode_History" runat="server" CssClass="form-control" Text='<%# Eval("EmpCode") %>' Enabled="false" />--%>
                                        </div>

                                        <div class="col-sm-6">
                                            <label>ชื่อ - นามสกุล : </label>
                                            <asp:Label ID="tbEmpName_History" runat="server" Text='<%# Eval("FullNameTH") %>'></asp:Label>
                                            <%-- <asp:TextBox ID="tbEmpName_History" runat="server" CssClass="form-control" Text='<%# Eval("FullNameTH") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label>วัน เดือน ปี เกิด : </label>
                                            <asp:Label ID="tbBirthday_History" runat="server" Text='<%# Eval("Birthday_Ex") %>'></asp:Label>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                            <%--<asp:TextBox ID="tbBirthday_History" runat="server" CssClass="form-control" Text='<%# Eval("Birthday_Ex") %>' Enabled="false" />--%>
                                            <label style="text-align: center; padding: 10px;">เพศ</label>
                                            <asp:Label ID="tbSexNameTH_History" Style="padding: 50px;" runat="server" Text='<%# Eval("SexNameTH") %>'></asp:Label>
                                            <%--<asp:TextBox ID="tbSexNameTH_History" runat="server" CssClass="form-control" Text='<%# Eval("SexNameTH") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label>วันที่เข้างาน : </label>
                                            <asp:Label ID="tbEmpIN_Ex_History" runat="server" Text='<%# Eval("EmpIN_Ex") %>'></asp:Label>
                                            <%--<asp:TextBox ID="tbEmpIN_Ex_History" runat="server" CssClass="form-control" Text='<%# Eval("EmpIN_Ex") %>' Enabled="false" />--%>
                                        </div>
                                       
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label>1. เลขที่บัตรประชาน : </label>
                                            <asp:Label ID="tbIdentityCard_History" runat="server" Text='<%# Eval("IdentityCard") %>'></asp:Label>

                                            <%--<asp:TextBox ID="tbEmpIN_Ex_History" runat="server" CssClass="form-control" Text='<%# Eval("EmpIN_Ex") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <asp:Panel ID="Pn_DistName" runat="server">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>2. ที่อยู่ตามบัตรประชาชน : </label>
                                                
                                            </div>
                                        </div>

                                        <div style="padding: 15px;">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>ที่อยู่ : </label>
                                                    <asp:Label ID="tb_EmpAddr_History" runat="server" Text='<%# Eval("EmpAddr") %>'></asp:Label>

                                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <label>ตำบล : </label>
                                                    <asp:Label ID="tbDistName_History" runat="server" Text='<%# Eval("DistName") %>'></asp:Label>
                                                    <%--<asp:TextBox ID="tb_EmpAddr_History" runat="server" CssClass="form-control" Text='<%# Eval("EmpAddr") %>' Enabled="false" />--%>
                                                    
                                                    
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-12">

                                                    <label>อำเภอ : </label>
                                                    <asp:Label ID="tbAmpName_History" runat="server" Text='<%# Eval("AmpName") %>'></asp:Label>

                                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <label>จังหวัด : </label>
                                                    <asp:Label ID="tbProvName_History" runat="server" Text='<%# Eval("ProvName") %>'></asp:Label>
                                                    <%--<asp:TextBox ID="tb_EmpAddr_History" runat="server" CssClass="form-control" Text='<%# Eval("EmpAddr") %>' Enabled="false" />--%>
                                                    
                                                    
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-12">

                                                    <label>รหัสไปรษณีย์ : </label>
                                                    <asp:Label ID="tb_PostCode_History" runat="server" Text='<%# Eval("PostCode") %>'></asp:Label>

                                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <label>โทรศัพท์</label>
                                                    <asp:Label ID="tbMobileNo_History" runat="server" Text='<%# Eval("MobileNo") %>'></asp:Label>
                                                    <%-- <asp:TextBox ID="tbDistName_History" runat="server" CssClass="form-control" Text='<%# Eval("DistName") %>' Enabled="false" />--%>
                                                </div>

                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <div class="form-group">
                                        <label>3. ที่อยู่ที่สามารถติดต่อได้</label>

                                    </div>

                                    <div style="padding: 15px;">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>ที่อยู่ : </label>
                                                <asp:Label ID="tbEmpAddrPresent_History" runat="server" Text='<%# Eval("EmpAddr") %>'></asp:Label>

                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                <label>ตำบล : </label>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("DistName") %>'></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>อำเภอ : </label>
                                                <asp:Label ID="tbAmpNamePresent_History" runat="server" Text='<%# Eval("AmpName") %>'></asp:Label>

                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                <label>จังหวัด : </label>
                                                <asp:Label ID="tbProvNamePresent_History" runat="server" Text='<%# Eval("ProvName") %>'></asp:Label>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>รหัสไปรษณีย์ : </label>
                                                <asp:Label ID="tbPostCodePresent_History" runat="server" Text='<%# Eval("PostCode") %>'></asp:Label>

                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                <label>โทรศัพท์ : </label>
                                                <asp:Label ID="tbMobileNoPresent_History" runat="server" Text='<%# Eval("MobileNo") %>'></asp:Label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>4. สถานประกอบกิจการ</label>
                                    </div>

                                    <div style="padding: 15px;">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>ที่อยู่ : </label>
                                                <asp:Label ID="tbOrgAddress_History" runat="server" Text='<%# Eval("OrgAddress") %>'></asp:Label>

                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                <label class="col-sm-2 control-label">รหัสไปรษณีย์ : </label>
                                                <asp:Label ID="tbPostCode_Org_History" runat="server" Text='<%# Eval("PostCode_Org") %>'></asp:Label>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:FormView>

            <div id="divHistoryWorkProfile" runat="server" class="col-md-12" visible="false">
                <asp:GridView ID="gvWork"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div class="text-center">-- ไม่พบข้อมูล --</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ชื่อสถานประกอบกิจการ/แผนก" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Detail_company_name" runat="server"> 
                                                    <p><b>ชื่อสถานประกอบกิจการ :</b> <%# Eval("company_name") %></p>
                                                    <p><b>แผนก :</b> <%# Eval("department_name") %></p>
                                  
                                    </asp:Label>

                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภทกิจการ" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_business_type_history" runat="server" Text='<%# Eval("business_type") %>' />
                                </small>

                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ลักษณะงานที่ทำ" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_job_description_history" runat="server" Text='<%# Eval("job_description") %>' />
                                </small>

                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ระยะเวลาที่ทำ" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Detail_DateWork" runat="server"> 
                                                    <p><b>วัน/เดือน/ปี ที่เริ่ม :</b> <%# Eval("job_startdate") %></p>
                                                    <p><b>วัน/เดือน/ปี ที่สิ้นสุด :</b> <%# Eval("job_enddate") %></p>
                                    </asp:Label>
                                </small>

                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ปัจจัยที่เสี่ยงต่อสุขภาพ" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_risk_health_history" runat="server" Text='<%#Eval("risk_health") %>' />
                                </small>

                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="มี/ใช้อุปกรณ์ป้องกันอันตราย" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lbl_protection_equipment_history" runat="server" Text='<%#Eval("protection_equipment") %>' />
                                </small>

                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>

            <asp:FormView ID="fvSick" runat="server" DefaultMode="ReadOnly" Width="100%">
                <ItemTemplate>
                    <div class="panel panel-primary">

                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div id="div_SickCheck" class="panel panel-default" runat="server">

                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>1. เคยป่วยเป็นโรคหรือมีการบาดเจ็บ</label>
                                            </div>
                                            <%--<asp:UpdatePanel ID="update_eversick_hr" runat="server">
                                                <ContentTemplate>--%>
                                            <asp:Panel ID="update_eversick_hr" runat="server">
                                                <div class="col-md-12">
                                                    <div id="div_DetailEverSickCheck_hr" class="panel panel-default" runat="server">
                                                        <div class="panel-body">
                                                            <div style="padding: 15px;">
                                                                <table class="table table-striped f-s-12 table-empshift-responsive">
                                                                    <asp:Repeater ID="rpt_eversick" runat="server">
                                                                        <HeaderTemplate>
                                                                            <%--style="text-align: right; padding-left: 15pt"--%>
                                                                            <tr>
                                                                                <th class="text-center" style="text-align: center;">รายละเอียด</th>
                                                                                <th class="text-center" style="text-align: center;">เมื่อปี พ.ศ.</th>
                                                                            </tr>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td data-th="รายละเอียด"><%# Eval("eversick") %></td>
                                                                                <td data-th="เมื่อปี พ.ศ."><%# Eval("sick_year") %></td>

                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </table>
                                                            </div>
                                                            <%--<div class="clearfix"></div>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <%-- </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>2. มีโรคประจำตัวหรือโรคเรื้อรังหรือไม่</label>
                                            </div>

                                            <div class="col-md-12">
                                                <div style="padding: 15px;">
                                                    <label>ระบุ</label>
                                                    <asp:Label ID="lbl_havedisease" runat="server" Text='<%# Eval("havedisease") %>'></asp:Label>
                                                    <%--<asp:TextBox ID="txt_havedisease_hr" runat="server" CssClass="form-control" Text='<%# Eval("havedisease") %>' placeholder="ระบุโรคประจำตัวหรือโรคเรื้อรัง...." Enabled="false" />--%>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>3. เคยได้รับการผ่าตัดหรือไม่</label>
                                            </div>

                                            <div class="col-md-12">
                                                <div style="padding: 15px;">
                                                    <label>ระบุ</label>
                                                    <asp:Label ID="lbl_surgery" runat="server" Text='<%# Eval("surgery") %>'></asp:Label>
                                                </div>
                                                <%--<asp:TextBox ID="txt_surgery_hr" runat="server" CssClass="form-control" Text='<%# Eval("surgery") %>' placeholder="ระบุเคยได้รับการผ่าตัด...." Enabled="false" />--%>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>4. เคยได้รับภูมิคุ้มกันโรคกรณีเกิดโรคระบาด หรือเพื่อป้องกันโรคติดต่อหรือไม่</label>
                                            </div>

                                            <div class="col-md-12">
                                                <div style="padding: 15px;">
                                                    <label>ระบุ</label>
                                                    <asp:Label ID="lbl_immune" runat="server" Text='<%# Eval("immune") %>'></asp:Label>
                                                    <%--<asp:TextBox ID="txt_Immune_hr" runat="server" CssClass="form-control" Text='<%# Eval("immune") %>' placeholder="ระบุเคยได้รับภูมิคุ้มกันโรคกรณีเกิดโรคระบาด...." Enabled="false" />--%>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>5. ประวัติการเจ็บป่วยของสมาชิกในครอบครัว (เช่น มะเร็ง โลหิตจาง วัณโรค เบาหวาน หอบหืด ภูมิแพ้ เป็นต้น)</label>
                                            </div>

                                            <%--  <asp:UpdatePanel ID="Update_Relationfamily_hr" runat="server">
                                                <ContentTemplate>--%>
                                            <asp:Panel ID="Update_Relationfamily_hr" runat="server">
                                                <div class="col-md-12">
                                                    <div id="div_Identifyrelationship_hr" class="panel panel-default" runat="server">
                                                        <div class="panel-body">
                                                            <div style="padding: 15px;">
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <label>ระบุความสัมพันธ์และโรค</label>
                                                                    </div>
                                                                </div>

                                                                <%--<table class="table table-striped f-s-12 table-empshift-responsive">--%>

                                                                <table class="table table-striped f-s-12 table-bordered m-t-10">
                                                                    <asp:Repeater ID="rptRelationfamily" runat="server">
                                                                        <HeaderTemplate>
                                                                            <tr>
                                                                                <th>ความสัมพันธ์</th>
                                                                                <th>โรค</th>
                                                                            </tr>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td data-th="ความสัมพันธ์"><%# Eval("relation_family") %></td>
                                                                                <td data-th="โรค"><%# Eval("disease_family") %></td>

                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </table>
                                                            </div>

                                                            <div class="clearfix"></div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <%-- </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>6. ปัจจุบันมียาที่จำเป็นต้องรับประทานเป็นประจำบ้างหรือไม่</label>
                                            </div>

                                            <div class="col-md-12">
                                                <div style="padding: 15px;">
                                                    <label>ระบุ</label>
                                                    <asp:Label ID="lbl_drugs_eat" runat="server" Text='<%# Eval("drugs_eat") %>'></asp:Label>
                                                </div>
                                                <%--<asp:TextBox ID="txt_DrugsEat_hr" runat="server" CssClass="form-control" Text='<%# Eval("drugs_eat") %>' placeholder="ระบุปัจจุบันมียาที่จำเป็นต้องรับประทานเป็นประจำ...." Enabled="false" />--%>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>7. มีประวัติการแพ้ยาหรือไม่</label>
                                            </div>

                                            <div class="col-md-12">
                                                <div style="padding: 15px;">
                                                    <label>ระบุ</label>
                                                    <asp:Label ID="lbl_allergy_history" runat="server" Text='<%# Eval("allergy_history") %>'></asp:Label>
                                                </div>
                                                <%--<asp:TextBox ID="txt_Allergyhistory_hr" runat="server" CssClass="form-control" Text='<%# Eval("allergy_history") %>' placeholder="ระบุประวัติการแพ้ยา...." Enabled="false" />--%>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>8. เคยสูบบุหรี่บ้างหรือไม่</label>
                                                <div style="padding: 15px;">
                                                    <asp:RadioButtonList ID="rdoSmoking_hr" runat="server" Text='<%# Eval("smoking") %>' Enabled="false" AutoPostBack="true">
                                                        <asp:ListItem Value="1">&nbsp;ไม่เคย</asp:ListItem>
                                                        <asp:ListItem Value="2">&nbsp;เคยและปัจจุบันยังสูบอยู่ปริมาณ</asp:ListItem>
                                                        <asp:ListItem Value="3">&nbsp;เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>

                                            <%--<asp:UpdatePanel ID="update_stopsmoking_hr" runat="server" Visible="false">
                                                <ContentTemplate>--%>
                                            <asp:Panel ID="update_stopsmoking_hr" runat="server" Visible="false">
                                                <div class="col-md-4">
                                                    <label>ปี</label>
                                                    <asp:Label ID="lbl_year_smoking_hr" runat="server" Text='<%# Eval("smoking_value1") %>'></asp:Label>
                                                    <%--<asp:TextBox ID="txt_year_smoking_hr" runat="server" CssClass="form-control" Text='<%# Eval("smoking_value1") %>' placeholder="ปี...." Enabled="false" />--%>
                                                </div>

                                                <div class="col-md-4">
                                                    <label>เดือน</label>
                                                    <asp:Label ID="lbl_smoking_value2" runat="server" Text='<%# Eval("smoking_value2") %>'></asp:Label>
                                                    <%--<asp:TextBox ID="txt_month_smoking_hr" runat="server" CssClass="form-control" Text='<%# Eval("smoking_value2") %>' placeholder="เดือน...." Enabled="false" />--%>
                                                </div>

                                                <div class="col-md-4">
                                                    <label>ปริมาณขณะก่อนเลิก(มวน/วัน)</label>
                                                    <asp:Label ID="lbl_smoking_value3" runat="server" Text='<%# Eval("smoking_value3") %>'></asp:Label>
                                                    <%--<asp:TextBox ID="txt_before_stopsmoking_hr" runat="server" CssClass="form-control" Text='<%# Eval("smoking_value3") %>' placeholder="มวน/วัน...." Enabled="false" />--%>
                                                </div>
                                            </asp:Panel>
                                            <%--</ContentTemplate>
                                            </asp:UpdatePanel>--%>

                                            <%-- <asp:UpdatePanel ID="update_eversmoking_hr" runat="server" Visible="false">
                                                <ContentTemplate>--%>
                                            <asp:Panel ID="update_eversmoking_hr" runat="server" Visible="false">
                                                <div class="col-md-6">
                                                    <label>ปริมาณ(มวน/วัน)</label>
                                                    <asp:Label ID="lbl_smoking_value4" runat="server" Text='<%# Eval("smoking_value4") %>'></asp:Label>
                                                    <%--<asp:TextBox ID="txt_valum_smoking_hr" runat="server" CssClass="form-control" Text='<%# Eval("smoking_value4") %>' placeholder="เดือน...." Enabled="false" />--%>
                                                </div>

                                                <div class="col-md-6">
                                                    <label>&nbsp;</label>
                                                </div>
                                            </asp:Panel>
                                            <%-- </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>9. เคยดื่มสุรา เบียร์ หรือเครื่องดื่มที่มีแอลกอฮอล์บ้างหรือไม่</label>
                                                <div style="padding: 15px;">
                                                    <asp:RadioButtonList ID="rdo_alcohol_hr" runat="server" Enabled="false" Text='<%# Eval("alcohol") %>' AutoPostBack="true">
                                                        <asp:ListItem Value="1">&nbsp;ไม่เคย</asp:ListItem>
                                                        <asp:ListItem Value="2">&nbsp;โดยปกติดื่มน้อยกว่า 1 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="3">&nbsp;ดื่ม 1 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="4">&nbsp;ดื่ม 2-3 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="5">&nbsp;ดื่มมากกว่า 3 ครั้งต่อสัปดาห์</asp:ListItem>
                                                        <asp:ListItem Value="6">&nbsp;เคยแต่เลิกแล้วระยะเวลาที่ดื่มนาน</asp:ListItem>
                                                    </asp:RadioButtonList>

                                                    <asp:Panel ID="update_alcohol_hr" runat="server" Visible="false">
                                                        <div style="padding: 15px;">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="lbl_alcohol_value1" runat="server" Text='<%# Eval("alcohol_value1") %>'></asp:Label>
                                                                <label>ปี</label>

                                                                <label>&nbsp;</label>
                                                                <asp:Label ID="lbl_alcohol_value2" runat="server" Text='<%# Eval("alcohol_value2") %>'></asp:Label>
                                                                <label>เดือน</label>

                                                                <%--<asp:TextBox ID="txt_year_alcohol_hr" runat="server" Text='<%# Eval("alcohol_value1") %>' CssClass="form-control" placeholder="ปี...." Enabled="false" />--%>
                                                            </div>
                                                            <div class="col-md-6">

                                                                <%--<asp:TextBox ID="txt_month_alcohol_hr" runat="server" Text='<%# Eval("alcohol_value2") %>' CssClass="form-control" placeholder="เดือน...." Enabled="false" />--%>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>

                                                </div>
                                            </div>

                                            <%-- <asp:UpdatePanel ID="update_alcohol_hr" runat="server" Visible="false">
                                                <ContentTemplate>--%>

                                            <%--</ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>10. เคยเสพยาเสพติดหรือสารเสพติดใดๆ บ้างหรือไม่</label>
                                            </div>

                                            <div class="col-md-12">
                                                <div style="padding: 15px;">
                                                    <label>ระบุ</label>
                                                    <asp:Label ID="lbl_addict_drug" runat="server" Text='<%# Eval("addict_drug") %>'></asp:Label>
                                                    <%-- <asp:TextBox ID="txt_addict_drug_hr" runat="server" CssClass="form-control" Text='<%# Eval("addict_drug") %>' placeholder="ระบุเคยเสพยาเสพติดหรือสารเสพติด...." Enabled="false" />--%>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>11. ข้อมูลทางสุขภาพอื่นๆ ที่เป็นประโยชน์</label>

                                                <%-- <asp:TextBox ID="txt_datahealth_hr" runat="server" CssClass="form-control" Text='<%# Eval("datahealth") %>' placeholder="ระบุเข้อมูลทางสุขภาพอื่นๆ ที่เป็นประโยชน์...." Enabled="false" />--%>
                                            </div>
                                            <div class="col-md-12">
                                                <div style="padding: 15px;">
                                                    <asp:Label ID="lbl_datahealth" runat="server" Text='<%# Eval("datahealth") %>'></asp:Label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDetailHealthCheck" runat="server" DefaultMode="ReadOnly" Width="100%">
                <ItemTemplate>
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div id="div_DetailHistoryHealthCheck" class="panel panel-default" runat="server">
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="rao_idx_detailtype" Visible="false" Text='<%# Eval("m0_detail_typecheck_idx") %>' runat="server"></asp:Label>
                                                <asp:RadioButtonList ID="rdoDetailType_detail" Enabled="false" runat="server" CssClass="" Font-Bold="true" RepeatDirection="Vertical"
                                                    AutoPostBack="true">
                                                </asp:RadioButtonList>

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label>ครั้งที่ : </label>
                                                <asp:Label ID="lbl_num_check" runat="server" Text='<%# Eval("num_check") %>'></asp:Label>
                                                <%--<asp:TextBox ID="txt_numcheck_detail" runat="server" Text='<%# Eval("num_check") %>' CssClass="form-control" Enabled="false">
                                                </asp:TextBox>--%>
                                            </div>

                                            <div class="col-md-6">
                                                <label>วันที่ตรวจสุขภาพ :</label>
                                                <%--<div class="input-group date">--%>
                                                <asp:Label ID="lbl_date_check" runat="server" Text='<%# Eval("date_check") %>'></asp:Label>
                                                <%--<asp:TextBox ID="txt_date_healthCheck_detail" runat="server" Text='<%# Eval("date_check") %>' CssClass="form-control from-date-datepicker" Enabled="false">
                                                    </asp:TextBox>--%>
                                                <%-- <span class="input-group-addon">
                                                        <span data-icon-element="" class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>--%>
                                            </div>

                                        </div>

                                        <%--  <asp:UpdatePanel ID="update_detailDoctorDetail" runat="server">
                                            <ContentTemplate>--%>
                                        <asp:Panel ID="update_detailDoctorDetail" runat="server">
                                            <div id="div_DetailDoctorCheck_detail" class="panel panel-default" runat="server">
                                                <div class="panel-body">

                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <label>ชื่อแพทย์ผู้ตรวจ : </label>
                                                            <asp:Label ID="lbl_doctor_name" runat="server" Text='<%# Eval("doctor_name") %>'></asp:Label>
                                                            <%--<asp:TextBox ID="txtEmpIDX_Doctor_detail" Text='<%# Eval("doctor_name") %>' runat="server" CssClass="form-control" Enabled="false" />--%>
                                                            <%-- <asp:DropDownList ID="ddl_Doctorname_detail" runat="server" 
                                                                CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />--%>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label>เลขที่ใบประกอบวิชาชีพ</label>
                                                            <%--<asp:TextBox ID="txtEmpNewFullNameTHHidden" runat="server" Visible="false" />--%>
                                                            <asp:Label ID="lbl_card_number" runat="server" Text='<%# Eval("card_number") %>'></asp:Label>
                                                            <%-- <asp:TextBox ID="txtcard_number_doctor_detail" Text='<%# Eval("card_number") %>' runat="server" CssClass="form-control"
                                                                placeholder="เลขที่ใบประกอบวิชาชีพ..." Enabled="false" />--%>
                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <label>ชื่อหน่วยงานที่ตรวจสุขภาพ</label>
                                                            <asp:Label ID="lbl_health_authority_name" runat="server" Text='<%# Eval("health_authority_name") %>'></asp:Label>
                                                            <%--<asp:TextBox ID="txt_health_authority_name_detail" runat="server" Text='<%# Eval("health_authority_name") %>' CssClass="form-control" placeholder="ชื่อหน่วยงานที่ตรวจสุขภาพ..." Enabled="false" />--%>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label>ตั้งอยู่เลขที่</label>
                                                            <asp:Label ID="lbl_location_name" runat="server" Text='<%# Eval("location_name") %>'></asp:Label>
                                                            <%--<asp:TextBox ID="txt_location_name_detail" runat="server" Text='<%# Eval("location_name") %>' CssClass="form-control"
                                                                placeholder="ตั้งอยู่เลขที่..." Enabled="false" />--%>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <label>หมู่ที่</label>
                                                            <asp:Label ID="lbl_village_no" runat="server" Text='<%# Eval("village_no") %>'></asp:Label>
                                                            <%--<asp:TextBox ID="txt_village_no_detail" runat="server" Text='<%# Eval("village_no") %>' CssClass="form-control"
                                                                placeholder="หมู่ที่..." Enabled="false" />--%>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <label>ถนน</label>
                                                            <asp:Label ID="lbl_road_name" runat="server" Text='<%# Eval("road_name") %>'></asp:Label>
                                                            <%--<asp:TextBox ID="txt_road_name_detail" runat="server" Text='<%# Eval("road_name") %>' CssClass="form-control"
                                                                placeholder="ถนน..." Enabled="false" />--%>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </asp:Panel>
                                        <%-- </ContentTemplate>
                                        </asp:UpdatePanel>--%>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>1. ตรวจสุขภาพทั่วไป</label>
                                            </div>
                                            <%--<asp:UpdatePanel ID="Update_GenaralHealth_Detail" runat="server">
                                                <ContentTemplate>--%>
                                            <asp:Panel ID="Update_GenaralHealth_Detail" runat="server">
                                                <div class="col-md-12">
                                                    <div id="div_genaralHealthCheck_detail" class="panel panel-default" runat="server">

                                                        <div class="panel-body">
                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <label>1.1 ผลการตรวจเบื้องต้น</label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">

                                                                <div class="col-md-4">
                                                                    <label>น้ำหนัก</label>
                                                                    <asp:Label ID="lbl_weight" runat="server" Text='<%# Eval("weight") %>'></asp:Label>
                                                                    <label>&nbsp;</label>
                                                                    <asp:Label ID="txt_unit_weight_detail" runat="server" Text="กิโลกรัม"></asp:Label>
                                                                    <%--<asp:TextBox ID="txt_weight_detail" runat="server" Text='<%# Eval("weight") %>' CssClass="form-control"
                                                                        placeHolder="น้ำหนัก..." Enabled="false">
                                                                    </asp:TextBox>--%>
                                                                </div>
                                                                <%-- <div class="col-md-2">
                                                                    <label>&nbsp;</label>
                                                                    <asp:TextBox ID="txt_unit_weight_detail" runat="server" Text="กิโลกรัม"
                                                                        CssClass="form-control" Enabled="false">
                                                                    </asp:TextBox>

                                                                </div>--%>


                                                                <div class="col-md-4">
                                                                    <label>ความสูง</label>
                                                                    <asp:Label ID="lbl_height" runat="server" Text='<%# Eval("height") %>'></asp:Label>
                                                                    <label>&nbsp;</label>
                                                                    <asp:Label ID="txt_unit_height_detail" runat="server" Text="เซนติเมตร"></asp:Label>
                                                                    <%--<asp:TextBox ID="txt_height_detail" runat="server" Text='<%# Eval("height") %>' CssClass="form-control"
                                                                        placeHolder="ความสูง..." Enabled="false">
                                                                    </asp:TextBox>--%>
                                                                </div>

                                                                <%-- <div class="col-md-2">
                                                                    <label>&nbsp;</label>
                                                                    <asp:TextBox ID="txt_unit_height_detail" runat="server" Text="เซนติเมตร"
                                                                        CssClass="form-control" Enabled="false">
                                                                    </asp:TextBox>
                                                                </div>--%>
                                                            </div>

                                                            <div class="form-group">

                                                                <div class="col-md-2">
                                                                    <label>ดัชนีมวลกาย</label>
                                                                    <asp:Label ID="lbl_body_mass" runat="server" Text='<%# Eval("body_mass") %>'></asp:Label>
                                                                    <%-- <asp:TextBox ID="txt_body_mass_detail" runat="server" Text='<%# Eval("body_mass") %>' CssClass="form-control"
                                                                        placeHolder="ดัชนีมวลกาย..." Enabled="false">
                                                                    </asp:TextBox>--%>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label>ความดันโลหิต</label>
                                                                    <asp:Label ID="lbl_bloodpressure" runat="server" Text='<%# Eval("bloodpressure") %>'></asp:Label>
                                                                    <label>&nbsp;</label>
                                                                    <asp:Label ID="txt_bloodpressure_unit_detail" runat="server" Text="mm.Hg"></asp:Label>
                                                                    <%--<asp:TextBox ID="txt_bloodpressure_detail" runat="server" Text='<%# Eval("bloodpressure") %>' CssClass="form-control"
                                                                        placeHolder="ความดันโลหิต..." Enabled="false">
                                                                    </asp:TextBox>--%>
                                                                </div>

                                                                <%-- <div class="col-md-2">
                                                                    <label>&nbsp;</label>
                                                                    <asp:TextBox ID="txt_bloodpressure_unit_detail" runat="server" Text="mm.Hg"
                                                                        CssClass="form-control" Enabled="false">
                                                                    </asp:TextBox>
                                                                </div>--%>
                                                                <div class="col-md-3">
                                                                    <label>ชีพจร</label>
                                                                    <asp:Label ID="lbl_pulse" runat="server" Text='<%# Eval("pulse") %>'></asp:Label>
                                                                    <label>&nbsp;</label>
                                                                    <asp:Label ID="txt_pulse_unit_detail" runat="server" Text="ครั้ง/นาที"></asp:Label>
                                                                    <%-- <asp:TextBox ID="txt_pulse_detail" runat="server" Text='<%# Eval("pulse") %>' CssClass="form-control"
                                                                        placeHolder="ชีพจร..." Enabled="false">
                                                                    </asp:TextBox>--%>
                                                                </div>

                                                                <%--<div class="col-md-2">
                                                                    <label>&nbsp;</label>
                                                                    <asp:TextBox ID="txt_pulse_unit_detail" runat="server" Text="ครั้ง/นาที"
                                                                        CssClass="form-control" Enabled="false">
                                                                    </asp:TextBox>
                                                                </div>--%>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <label>1.2 ผลการตรวจร่างกายตามระบบ</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <label>ระบุ</label>
                                                                    <asp:Label ID="lbl_specify_resultbody" runat="server" Text='<%# Eval("specify_resultbody") %>'></asp:Label>
                                                                    <%--<asp:TextBox ID="txt_specify_resultbody_detail" runat="server" Text='<%# Eval("specify_resultbody") %>'
                                                                        CssClass="form-control" placeHolder="ระบุผลการตรวจร่างกายตามระบบ..." Enabled="false">
                                                                    </asp:TextBox>--%>
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <label>1.3 ผลการตรวจทางห้องปฏิบัติการ</label>
                                                                    <asp:Label ID="lbl_resultlab" runat="server" Text='<%# Eval("resultlab") %>'></asp:Label>
                                                                    <%--<asp:TextBox ID="txt_resultlab_detail" runat="server" Text='<%# Eval("resultlab") %>' CssClass="form-control"
                                                                        placeHolder="ระบุผลการตรวจทางห้องปฏิบัติการ..." Enabled="false">
                                                                    </asp:TextBox>--%>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <%-- </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>2. ตรวจสุขภาพตามปัจจัยเสี่ยงของงาน</label>
                                            </div>
                                            <%-- <asp:UpdatePanel ID="Update_RikeHealthCheck_Detail" runat="server">
                                                <ContentTemplate>--%>
                                            <asp:Panel ID="Update_RikeHealthCheck_Detail" runat="server">
                                                <div class="col-md-12">
                                                    <div id="div_rikeHealthCheck_detail" class="panel panel-default" runat="server">
                                                        <div class="panel-body">
                                                            <div style="padding: 15px;">
                                                                <table class="table table-striped f-s-12 table-empshift-responsive">
                                                                    <asp:Repeater ID="rpt_RikeDetail" runat="server">
                                                                        <HeaderTemplate>
                                                                            <tr>
                                                                                <th>ปัจจัยเสี่ยง</th>
                                                                                <th>ผลการตรวจ</th>
                                                                            </tr>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td data-th="ปัจจัยเสี่ยง"><%# Eval("rike_health") %></td>
                                                                                <td data-th="ผลการตรวจ"><%# Eval("result_health") %></td>

                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </table>

                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <%-- </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:FormView>

        </div>
    </form>
</body>
</html>
