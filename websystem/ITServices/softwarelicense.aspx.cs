﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_ITServices_softwarelicense : System.Web.UI.Page
{

    #region initial function/data

    service_mail servicemail = new service_mail();


    function_tool _funcTool = new function_tool();

    data_softwarelicense _data_softwarelicense = new data_softwarelicense();
    data_softwarelicense_devices _data_softwarelicense_devices = new data_softwarelicense_devices();
    // data_networkdevices _data_networkdevices = new data_networkdevices();

    data_employee _data_employee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //static string _keynetworkdevices = ConfigurationManager.AppSettings["keynetworkdevices"];  

    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    //Org
    static string _urlGetddlOrganizationSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlOrganizationSW"];
    static string _urlGetddlDeptSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlDeptSW"];
    static string _urlGetddlSectionSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlSectionSW"];
    static string _urlGetddlEmpSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlEmpSW"];

    // Software
    static string _urlGetddlSofware = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlSofware"];
    static string _urlGettypeSoftwareName = _serviceUrl + ConfigurationManager.AppSettings["urlGettypeSoftwareName"];


    // Software License
    static string _urlGetddlSoftware = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlSoftware"];
    static string _urlGetm0SoftwareDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0SoftwareDetail"];
    static string _urlGetCountDeptSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountDeptSW"];
    static string _urlGetCountsw = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountsw"];
    static string _urlGettypeholder = _serviceUrl + ConfigurationManager.AppSettings["urlGettypeholder"];
    static string _urlGetDevices = _serviceUrl + ConfigurationManager.AppSettings["urlGetDevices"];
    static string _urlGetDevicesAsset = _serviceUrl + ConfigurationManager.AppSettings["urlGetDevicesAsset"];
    static string _urlGetTypeSwinsert = _serviceUrl + ConfigurationManager.AppSettings["urlGetTypeSwinsert"];
    static string _urlGetCountAllLicense = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountAllLicense"];
    static string _urlGetCountUse = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountUse"];

    static string _urlGetCountGhost = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountGhost"];
    static string _urlGetCountLicenseOk = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountLicenseOk"];

    //Software Devices  
    static string _urlSetSoftwareLicense = _serviceUrl + ConfigurationManager.AppSettings["urlSetSoftwareLicense"];
    static string _urlGetSoftwareLicense = _serviceUrl + ConfigurationManager.AppSettings["urlGetSoftwareLicense"];
    static string _urlGetCountLicenseDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountLicenseDetail"];
    static string _urlGetSoftwareLicenseView = _serviceUrl + ConfigurationManager.AppSettings["urlGetSoftwareLicenseView"];
    static string _urlGetSoftwareLicenseName = _serviceUrl + ConfigurationManager.AppSettings["urlGetSoftwareLicenseName"];
    static string _urlGetLogSoftwareLicense = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogSoftwareLicense"];

    static string _urlSetApproveLicense = _serviceUrl + ConfigurationManager.AppSettings["urlSetApproveLicense"];
    static string _urlGetSearchIndex = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchIndex"];
    static string _urlGetSearchReportSoftware = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchReportSoftware"];
    static string _urlGetBindReportSoftware = _serviceUrl + ConfigurationManager.AppSettings["urlGetBindReportSoftware"];



    string _localJson = "";
    int _tempInt = 0;

    string _mail_subject = "";
    string _mail_body = "";

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;

    int m0_node = 0;
    int m0_actor = 0;
    int m0_status = 0;

    #endregion initial function/data

    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        //emp_idx = 123;//int.Parse(Session["emp_idx"].ToString());
        ////ViewState["emp_permission_in"] = emp_idx;
    
        if (!IsPostBack)
        {

            /// หา Rsec แผนก
            data_employee _data_employee = new data_employee();

            _data_employee.employee_list = new employee_detail[1];
            employee_detail _employee_detail = new employee_detail();

            _employee_detail.emp_idx = emp_idx;//int.Parse(Session["emp_idx"].ToString());

            _data_employee.employee_list[0] = _employee_detail;

            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
            //_data_employee = callServiceEmpProfile(_urlGetEmpMenu, _data_employee);

            _data_employee = callServiceEmpProfile(_urlGetMyProfile + emp_idx.ToString());

            ViewState["emp_idx_emp"] = _data_employee.employee_list[0].emp_idx;
            ViewState["rdept_idx_emp"] = _data_employee.employee_list[0].rdept_idx;
            ViewState["rsec_idx_emp"] = _data_employee.employee_list[0].rsec_idx;
            //ViewState["rpos_idx_emp"] = _data_employee.employee_list[0].rpos_idx;

            //if (ViewState["rsec_idx_emp"].ToString() == "294" && ViewState["rsec_idx_emp"].ToString() == "210")
            // Support 
            if((ViewState["emp_idx_emp"].ToString() != "0") && ((ViewState["rsec_idx_emp"].ToString() == "294") || ViewState["rsec_idx_emp"].ToString() == "80" || ViewState["rsec_idx_emp"].ToString() == "77" || ViewState["rsec_idx_emp"].ToString() == "210"))
            {


                emp_idx = int.Parse(Session["emp_idx"].ToString());

                /// หา Rsec แผนก
                data_employee _data_employeewhere = new data_employee();

                _data_employeewhere.employee_list = new employee_detail[1];
                employee_detail _employee_detailwhere = new employee_detail();

                _employee_detailwhere.emp_idx = emp_idx;//int.Parse(Session["emp_idx"].ToString());

                _data_employeewhere.employee_list[0] = _employee_detailwhere;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
                //_data_employee = callServiceEmpProfile(_urlGetEmpMenu, _data_employee);

                _data_employeewhere = callServiceEmpProfile(_urlGetMyProfile + emp_idx.ToString());

                ViewState["emp_idx_empwhere"] = _data_employeewhere.employee_list[0].emp_idx;
                ViewState["rdept_idx_empwhere"] = _data_employeewhere.employee_list[0].rdept_idx;
                ViewState["rsec_idx_empwhere"] = _data_employeewhere.employee_list[0].rsec_idx;


                initPage();
                btnSearch.Visible = true;
                actionIndex();
                Set_Defult_Index(); //set tab menu 
                btnToDeviceSoftware.Visible = true;
                btnToReport.Visible = true;

                /// search ////
                ddlOrganizationSearch();
                /// search

                /////////////////////////////////////////////////////
                var dsEquipment = new DataSet();
                dsEquipment.Tables.Add("Equipment");
                dsEquipment.Tables[0].Columns.Add("software_idx_dataset", typeof(int));
                dsEquipment.Tables[0].Columns.Add("swlicense_name_dataset", typeof(String));
                dsEquipment.Tables[0].Columns.Add("swlicense_idx_dataset", typeof(int));
                dsEquipment.Tables[0].Columns.Add("org_idx_dataset", typeof(int));
                dsEquipment.Tables[0].Columns.Add("rdept_idx_dataset", typeof(int));
                dsEquipment.Tables[0].Columns.Add("type_name_dataset", typeof(String));
                dsEquipment.Tables[0].Columns.Add("type_idx_dataset", typeof(int));
                dsEquipment.Tables[0].Columns.Add("ghoststatus_dataset", typeof(int));
                dsEquipment.Tables[0].Columns.Add("countinsert_dataset", typeof(int));

                ViewState["vsBuyequipment"] = dsEquipment;

            }
            else if(ViewState["emp_idx_emp"].ToString() != "0" && ((ViewState["rsec_idx_emp"].ToString() != "294") || ViewState["rsec_idx_emp"].ToString() != "80" || ViewState["rsec_idx_emp"].ToString() != "77" || ViewState["rsec_idx_emp"].ToString() != "210")  || ViewState["emp_idx_emp"].ToString() == "1394")
            { 

                emp_idx = int.Parse(Session["emp_idx"].ToString());

                /// หา Rsec แผนก
                data_employee _data_employeewhere = new data_employee();

                _data_employeewhere.employee_list = new employee_detail[1];
                employee_detail _employee_detailwhere = new employee_detail();

                _employee_detailwhere.emp_idx = emp_idx;//int.Parse(Session["emp_idx"].ToString());

                _data_employeewhere.employee_list[0] = _employee_detailwhere;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
                //_data_employee = callServiceEmpProfile(_urlGetEmpMenu, _data_employee);

                _data_employeewhere = callServiceEmpProfile(_urlGetMyProfile + emp_idx.ToString());

                ViewState["emp_idx_empwhere"] = _data_employeewhere.employee_list[0].emp_idx;
                ViewState["rdept_idx_empwhere"] = _data_employeewhere.employee_list[0].rdept_idx;
                ViewState["rsec_idx_empwhere"] = _data_employeewhere.employee_list[0].rsec_idx;


                initPage();
                actionIndex();
                Set_Defult_Index(); //set tab menu 

                /// search ////
                ddlOrganizationSearch();
                /// search

                /////////////////////////////////////////////////////
                var dsEquipment = new DataSet();
                dsEquipment.Tables.Add("Equipment");
                dsEquipment.Tables[0].Columns.Add("software_idx_dataset", typeof(int));
                dsEquipment.Tables[0].Columns.Add("swlicense_name_dataset", typeof(String));
                dsEquipment.Tables[0].Columns.Add("swlicense_idx_dataset", typeof(int));
                dsEquipment.Tables[0].Columns.Add("org_idx_dataset", typeof(int));
                dsEquipment.Tables[0].Columns.Add("rdept_idx_dataset", typeof(int));
                dsEquipment.Tables[0].Columns.Add("type_name_dataset", typeof(String));
                dsEquipment.Tables[0].Columns.Add("type_idx_dataset", typeof(int));
                dsEquipment.Tables[0].Columns.Add("ghoststatus_dataset", typeof(int));
                dsEquipment.Tables[0].Columns.Add("countinsert_dataset", typeof(int));

                ViewState["vsBuyequipment"] = dsEquipment;
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีสิทธิ์เข้าระบบ');", true);

                divMenu.Visible = false;
                btnSearch.Visible = false;

                ////litShowDetail.Text = "คุณไม่มีสิทธิ์เข้าระบบ Network Devices";

            }

        }


    }

    #region selected 

    #region selected Emp System   
    protected void actionIndexEmp() //เอาไว้ใช้กัน Link ตรง
    {

        data_employee _data_employee = new data_employee();

        _data_employee.employee_list = new employee_detail[1];
        employee_detail _employee_detail = new employee_detail();

        _employee_detail.emp_idx = emp_idx;//int.Parse(Session["emp_idx"].ToString());

        _data_employee.employee_list[0] = _employee_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        //_data_employee = callServiceEmpProfile(_urlGetEmpMenu, _data_employee);

        _data_employee = callServiceEmpProfile(_urlGetMyProfile + emp_idx.ToString());

        ViewState["emp_idx_emp"] = _data_employee.employee_list[0].emp_idx;
        ViewState["rdept_idx_emp"] = _data_employee.employee_list[0].rdept_idx;
        ViewState["rsec_idx_emp"] = _data_employee.employee_list[0].rsec_idx;


    }

    #endregion selected Emp System  

    protected void actionIndex()
    {

        data_softwarelicense_devices _data_softwarelicense_devicesindex = new data_softwarelicense_devices();
        _data_softwarelicense_devicesindex.bind_softwarelicense_list = new bind_softwarelicense_detail[1];

        bind_softwarelicense_detail bind_softwarelicense_detailindex = new bind_softwarelicense_detail();

        bind_softwarelicense_detailindex.emp_idx = emp_idx;//int.Parse(ViewState["emp_permission_in"].ToString());
        bind_softwarelicense_detailindex.rsec_idx = int.Parse(ViewState["rsec_idx_empwhere"].ToString());

        _data_softwarelicense_devicesindex.bind_softwarelicense_list[0] = bind_softwarelicense_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_softwarelicense_devicesindex));
        _data_softwarelicense_devicesindex = callServiceSoftwareLicenseDevices(_urlGetSoftwareLicense, _data_softwarelicense_devicesindex);

        //setGridData(GvMaster, _data_softwarelicense_devicesindex.bind_softwarelicense_list);
     
        if (_data_softwarelicense_devicesindex.bind_softwarelicense_list != null)
        {
            //if (_data_softwarelicense_devicesindex.bind_softwarelicense_list[0].emp_idx != 0)
            //{
                ViewState["emp_permission"] = _data_softwarelicense_devicesindex.bind_softwarelicense_list[0].emp_idx;

                //1347 --> 90000001
                if ((int.Parse(ViewState["emp_permission"].ToString()) == emp_idx) || (emp_idx.ToString() == "1394")) //คนที่ถือครอง
                {

                    ViewState["bind_data_index"] = _data_softwarelicense_devicesindex.bind_softwarelicense_list;
                    setGridData(GvMaster, ViewState["bind_data_index"]);
                    // btnSearch.Visible = false;

                }
            //else if (ViewState["emp_permission_in"].ToString() == "1413" || ViewState["emp_permission_in"].ToString() == "178" || ViewState["emp_permission_in"].ToString() == "3593" || ViewState["emp_permission_in"].ToString() == "3752" || ViewState["emp_permission_in"].ToString() == "3657" || ViewState["emp_permission_in"].ToString() == "3834") // support เห็นข้อมูล 
                else if ((emp_idx != 0) && ((ViewState["rsec_idx_empwhere"].ToString() == "294") || ViewState["rsec_idx_empwhere"].ToString() == "80" || ViewState["rsec_idx_empwhere"].ToString() == "77" || ViewState["rsec_idx_empwhere"].ToString() == "210"))                                                                                                                                                                                                                     
                {

                    ViewState["bind_data_index"] = _data_softwarelicense_devicesindex.bind_softwarelicense_list;
                    setGridData(GvMaster, ViewState["bind_data_index"]);

                    btnSearch.Visible = true;
                }
                else
                {
                    setGridData(GvMaster, null);
                }
        
        }
        else
        {
            ViewState["bind_data_index"] = _data_softwarelicense_devicesindex.bind_softwarelicense_list;
            setGridData(GvMaster, ViewState["bind_data_index"]);
        }


    }

    protected void SelectViewIndex()
    {
        //GridView GvMaster = (GridView)ViewIndex.FindControl("GvMaster");
        //Label u0_idx_view = (Label)GvMaster.FindControl("lbu0_idx");

        data_softwarelicense_devices _data_softwarelicense_devicesview = new data_softwarelicense_devices();
        _data_softwarelicense_devicesview.bind_softwarelicense_list = new bind_softwarelicense_detail[1];

        bind_softwarelicense_detail _bind_softwarelicense_detailview = new bind_softwarelicense_detail();

        _bind_softwarelicense_detailview.u0_software_idx = int.Parse(ViewState["u0_software_idx_index"].ToString());

        _data_softwarelicense_devicesview.bind_softwarelicense_list[0] = _bind_softwarelicense_detailview;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_softwarelicense_devicesview = callServiceSoftwareLicenseDevices(_urlGetSoftwareLicenseView, _data_softwarelicense_devicesview);

        setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_softwarelicense_devicesview.bind_softwarelicense_list);

        //if(_data_softwarelicense_devicesview.bind_softwarelicense_list[0].emp_idx == 0 )
        //{

        //    setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_softwarelicense_devicesview.bind_softwarelicense_list);
        //}
        //else
        //{
        //    setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_softwarelicense_devicesview.bind_softwarelicense_list);
        //}


    }

    protected void SelectNameSoftware()
    {
        
        data_softwarelicense_devices _data_softwarelicense_namesw = new data_softwarelicense_devices();
        _data_softwarelicense_namesw.bind_softwarelicense_list = new bind_softwarelicense_detail[1];

        bind_softwarelicense_detail _bind_softwarelicense_detailnamesw = new bind_softwarelicense_detail();

        _bind_softwarelicense_detailnamesw.u0_software_idx = int.Parse(ViewState["u0_software_idx_index"].ToString());

        _data_softwarelicense_namesw.bind_softwarelicense_list[0] = _bind_softwarelicense_detailnamesw;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_softwarelicense_namesw = callServiceSoftwareLicenseDevices(_urlGetSoftwareLicenseName, _data_softwarelicense_namesw);

        setGridData(GvSoftwaredetail, _data_softwarelicense_namesw.bind_softwarelicense_list);

        //setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_softwarelicense_namesw.bind_softwarelicense_list);

        //rptDetailSoftwareName.DataSource = _data_softwarelicense_namesw.bind_softwarelicense_list;
        //rptDetailSoftwareName.DataBind();

    }

    protected void SelectLogDetail()
    {

        data_softwarelicense_devices _data_softwarelicense_deviceslog = new data_softwarelicense_devices();
        _data_softwarelicense_deviceslog.log_u0software_list = new log_u0software_detail[1];

        log_u0software_detail _log_u0software_detaillog = new log_u0software_detail();

        _log_u0software_detaillog.u0_software_idx = int.Parse(ViewState["u0_software_idx_index"].ToString());

        _data_softwarelicense_deviceslog.log_u0software_list[0] = _log_u0software_detaillog;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_softwarelicense_deviceslog = callServiceSoftwareLicenseDevices(_urlGetLogSoftwareLicense, _data_softwarelicense_deviceslog);


        //setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_networklog.lognetwork_list);


        rptDetailSoftwareName.DataSource = _data_softwarelicense_deviceslog.log_u0software_list;
        rptDetailSoftwareName.DataBind();



    }

    protected void ddlSoftware()
    {

        //Panel PaneldetailLicense = (Panel)ViewInsert.FindControl("PaneldetailLicense");
        //FormView FvInsert = (FormView)ViewSoftwareDevices.FindControl("FvInsert");
        DropDownList ddlsoftware_nameinsert = (DropDownList)ViewSoftwareDevices.FindControl("ddlsoftware_nameinsert");

        //TextBox txtcount_licenseinsert = (TextBox)FvInsert.FindControl("txtcount_licenseinsert");

        ddlsoftware_nameinsert.Items.Clear();
        ddlsoftware_nameinsert.AppendDataBoundItems = true;
        ddlsoftware_nameinsert.Items.Add(new ListItem("กรุณาเลือก Software ...", "00"));

        data_softwarelicense _data_softwarelicense_swddl = new data_softwarelicense();
        _data_softwarelicense_swddl.bind_m0software_list = new bind_m0software_detail[1];
        bind_m0software_detail _bind_m0software_detail = new bind_m0software_detail();

        //_permissionDetail.OrgIDX = 0;

        _data_softwarelicense_swddl.bind_m0software_list[0] = _bind_m0software_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
        _data_softwarelicense_swddl = callServiceSoftwareLicense(_urlGetddlSoftware, _data_softwarelicense_swddl);

        ddlsoftware_nameinsert.DataSource = _data_softwarelicense_swddl.bind_m0software_list;
        ddlsoftware_nameinsert.DataTextField = "software_name";
        ddlsoftware_nameinsert.DataValueField = "software_name_idx";
        //ddlsoftware_idxinsert.DataValueField = "software_idx";
        ddlsoftware_nameinsert.DataBind();


    }

    protected void ddlOrganization()
    {

        FormView FvInsert = (FormView)ViewSoftwareDevices.FindControl("FvInsert");
        DropDownList ddlorg_idxinsert = (DropDownList)FvInsert.FindControl("ddlorg_idxinsert");

        ddlorg_idxinsert.Items.Clear();
        ddlorg_idxinsert.AppendDataBoundItems = true;
        ddlorg_idxinsert.Items.Add(new ListItem("เลือกองค์กร ...", "00"));

        data_softwarelicense _data_softwarelicense_org = new data_softwarelicense();
        _data_softwarelicense_org.organization_list = new organization_detail[1];
        organization_detail _organization_detail = new organization_detail();

        //_permissionDetail.OrgIDX = 0;

        _data_softwarelicense_org.organization_list[0] = _organization_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
        _data_softwarelicense_org = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense_org);

        ddlorg_idxinsert.DataSource = _data_softwarelicense_org.organization_list;
        ddlorg_idxinsert.DataTextField = "OrgNameTH";
        ddlorg_idxinsert.DataValueField = "OrgIDX";
        ddlorg_idxinsert.DataBind();


    }

    protected void ddlTypeHolder()
    {
        FormView FvInsert = (FormView)ViewSoftwareDevices.FindControl("FvInsert");
        DropDownList ddl_type_idxinsert = (DropDownList)FvInsert.FindControl("ddl_type_idxinsert");

        ddl_type_idxinsert.Items.Clear();
        ddl_type_idxinsert.AppendDataBoundItems = true;
        ddl_type_idxinsert.Items.Add(new ListItem("เลือกประเภทอุปกรณ์ ...", "00"));

        data_softwarelicense _data_softwarelicense_type = new data_softwarelicense();
        _data_softwarelicense_type.bindm0_typedevice_list = new bindm0_typedevice_detail[1];
        bindm0_typedevice_detail _data_typedevice_detail = new bindm0_typedevice_detail();

        //_data_typedevice_detail.m0_tdidx = 1;

        _data_softwarelicense_type.bindm0_typedevice_list[0] = _data_typedevice_detail;

        _data_softwarelicense_type = callServiceSoftwareLicense(_urlGettypeholder, _data_softwarelicense_type);

        ddl_type_idxinsert.DataSource = _data_softwarelicense_type.bindm0_typedevice_list;
        ddl_type_idxinsert.DataTextField = "name_m0_typedevice";
        ddl_type_idxinsert.DataValueField = "m0_tdidx";
        ddl_type_idxinsert.DataBind();
    }

    protected void ddlHolderDevices()
    {
        //FormView FvInsert = (FormView)ViewSoftwareDevices.FindControl("FvInsert");
        Panel ShowSelectcom = (Panel)ViewSoftwareDevices.FindControl("ShowSelectcom");
        DropDownList ddlholderdevicesinsert = (DropDownList)ShowSelectcom.FindControl("ddlholderdevicesinsert");


        DropDownList _ddlorg_idxinsert = (DropDownList)FvInsert.FindControl("ddlorg_idxinsert");
        DropDownList _ddlrdept_idxinsert = (DropDownList)FvInsert.FindControl("ddlrdept_idxinsert");
        DropDownList _ddlrsec_idxinsert = (DropDownList)FvInsert.FindControl("ddlrsec_idxinsert");
        DropDownList _ddl_type_idxinsert = (DropDownList)FvInsert.FindControl("ddl_type_idxinsert");
        TextBox txttype_idx = (TextBox)FvInsert.FindControl("txttype_idx");


        ddlholderdevicesinsert.Items.Clear();
        ddlholderdevicesinsert.AppendDataBoundItems = true;
        ddlholderdevicesinsert.Items.Add(new ListItem("กรุณาเลือกรหัสอุปกรณ์ ...", "00"));

        data_softwarelicense _data_softwarelicense_devices = new data_softwarelicense();
        _data_softwarelicense_devices.bindu0_device_list = new bindu0_device_detail[1];
        bindu0_device_detail _data_bindu0_device_detail = new bindu0_device_detail();

        //_data_typedevice_detail.m0_tdidx = 1;

        _data_bindu0_device_detail.u0_code = txttype_idx.Text;
        _data_bindu0_device_detail.m0_tdidx = int.Parse(_ddl_type_idxinsert.SelectedValue);
        _data_bindu0_device_detail.OrgIDX = int.Parse(_ddlorg_idxinsert.SelectedValue);
        _data_bindu0_device_detail.RDeptIDX = int.Parse(_ddlrdept_idxinsert.SelectedValue);
        _data_bindu0_device_detail.RSecIDX = int.Parse(_ddlrsec_idxinsert.SelectedValue);

        _data_softwarelicense_devices.bindu0_device_list[0] = _data_bindu0_device_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devices));
        _data_softwarelicense_devices = callServiceSoftwareLicense(_urlGetDevices, _data_softwarelicense_devices);

        ddlholderdevicesinsert.DataSource = _data_softwarelicense_devices.bindu0_device_list;
        ddlholderdevicesinsert.DataTextField = "u0_code";
        ddlholderdevicesinsert.DataValueField = "u0_didx";
        ddlholderdevicesinsert.DataBind();

        FvInsert.Visible = false;
        fvBacktoSearch.Visible = true;
        ShowSelectcom.Visible = true;
    }


    #endregion selected 

    #region search
    protected void ddlOrganizationSearch()
    {

        //FormView FvInsert = (FormView)ViewSoftwareDevices.FindControl("FvInsert");
        DropDownList ddlorg_idxsearch = (DropDownList)ViewIndex.FindControl("ddlorg_idxsearch");

        ddlorg_idxsearch.Items.Clear();
        ddlorg_idxsearch.AppendDataBoundItems = true;
        ddlorg_idxsearch.Items.Add(new ListItem("เลือกองค์กร ...", "00"));

        data_softwarelicense _data_softwarelicense_orgsearch = new data_softwarelicense();
        _data_softwarelicense_orgsearch.organization_list = new organization_detail[1];
        organization_detail _organization_detail = new organization_detail();

        //_permissionDetail.OrgIDX = 0;

        _data_softwarelicense_orgsearch.organization_list[0] = _organization_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
        _data_softwarelicense_orgsearch = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense_orgsearch);

        ddlorg_idxsearch.DataSource = _data_softwarelicense_orgsearch.organization_list;
        ddlorg_idxsearch.DataTextField = "OrgNameTH";
        ddlorg_idxsearch.DataValueField = "OrgIDX";
        ddlorg_idxsearch.DataBind();


    }
    #endregion search 

    #region report
    protected void ddlSoftwareReport()
    {

        DropDownList ddlsoftware_name_idxreport = (DropDownList)ViewReport.FindControl("ddlsoftware_name_idxreport");

        ddlsoftware_name_idxreport.Items.Clear();
        ddlsoftware_name_idxreport.AppendDataBoundItems = true;
        ddlsoftware_name_idxreport.Items.Add(new ListItem("เลือก Software ...", "00"));

        data_softwarelicense _data_ddl_swreport = new data_softwarelicense();
        _data_ddl_swreport.softwarename_list = new softwarename_detail[1];
        softwarename_detail _softwarename_detailddl = new softwarename_detail();

        _data_ddl_swreport.softwarename_list[0] = _softwarename_detailddl;

        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

        _data_ddl_swreport = callServiceSoftwareLicense(_urlGetddlSofware, _data_ddl_swreport);

        //ViewState["type_idx"] = _data_ddl_sw.softwarename_list[0].software_type;

        ddlsoftware_name_idxreport.DataSource = _data_ddl_swreport.softwarename_list;
        ddlsoftware_name_idxreport.DataTextField = "software_name";
        ddlsoftware_name_idxreport.DataValueField = "software_name_idx";
        ddlsoftware_name_idxreport.DataBind();


    }

    protected void ddltypeSoftwareReport()
    {

        DropDownList ddlsoftware_typereport = (DropDownList)ViewReport.FindControl("ddlsoftware_typereport");

        ddlsoftware_typereport.Items.Clear();
        ddlsoftware_typereport.AppendDataBoundItems = true;
        ddlsoftware_typereport.Items.Add(new ListItem("เลือกประเภท Software ...", "00"));

        _data_softwarelicense.softwarename_list = new softwarename_detail[1];
        softwarename_detail _softwarename_detailddl = new softwarename_detail();

        _data_softwarelicense.softwarename_list[0] = _softwarename_detailddl;

        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

        _data_softwarelicense = callServiceSoftwareLicense(_urlGettypeSoftwareName, _data_softwarelicense);

        ddlsoftware_typereport.DataSource = _data_softwarelicense.softwarename_list;
        ddlsoftware_typereport.DataTextField = "type_name";
        ddlsoftware_typereport.DataValueField = "type_idx";
        ddlsoftware_typereport.DataBind();


    }

    protected void ddlOrganizationReport()
    {

        //FormView FvInsert = (FormView)ViewSoftwareDevices.FindControl("FvInsert");
        DropDownList ddlorg_idxreport = (DropDownList)ViewReport.FindControl("ddlorg_idxreport");

        ddlorg_idxreport.Items.Clear();
        ddlorg_idxreport.AppendDataBoundItems = true;
        ddlorg_idxreport.Items.Add(new ListItem("เลือกองค์กร ...", "00"));

        data_softwarelicense _data_softwarelicense_orgreport = new data_softwarelicense();
        _data_softwarelicense_orgreport.organization_list = new organization_detail[1];
        organization_detail _organization_detail = new organization_detail();

        //_permissionDetail.OrgIDX = 0;

        _data_softwarelicense_orgreport.organization_list[0] = _organization_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
        _data_softwarelicense_orgreport = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense_orgreport);

        ddlorg_idxreport.DataSource = _data_softwarelicense_orgreport.organization_list;
        ddlorg_idxreport.DataTextField = "OrgNameTH";
        ddlorg_idxreport.DataValueField = "OrgIDX";
        ddlorg_idxreport.DataBind();


    }

    #endregion report

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {

        var ddName = (DropDownList)sender;

        FormView FvInsert = (FormView)ViewSoftwareDevices.FindControl("FvInsert");
        DropDownList ddlorg_idxinsert = (DropDownList)FvInsert.FindControl("ddlorg_idxinsert");
        DropDownList ddlrdept_idxinsert = (DropDownList)FvInsert.FindControl("ddlrdept_idxinsert");
        DropDownList ddlrsec_idxinsert = (DropDownList)FvInsert.FindControl("ddlrsec_idxinsert");
        DropDownList ddlemp_idxinsert = (DropDownList)FvInsert.FindControl("ddlemp_idxinsert");

        DropDownList ddlsoftware_idxinsert = (DropDownList)FvInsert.FindControl("ddlsoftware_idxinsert");
        Panel detail_license = (Panel)FvInsert.FindControl("detail_license");
        TextBox txtcount_licenseinsert = (TextBox)FvInsert.FindControl("txtcount_licenseinsert");
        Label lbcount_licenseinsert = (Label)FvInsert.FindControl("lbcount_licenseinsert");

        TextBox txtcount_license_dept = (TextBox)FvInsert.FindControl("txtcount_license_dept");

        //// Panel show_detailinsert = (Panel)FvInsert.FindControl("show_detailinsert");
        Panel show_btninsertdataset = (Panel)FvInsert.FindControl("show_btninsertdataset");


        Panel ShowSelectcom = (Panel)ViewSoftwareDevices.FindControl("ShowSelectcom");
        DropDownList ddlholderdevicesinsert = (DropDownList)ShowSelectcom.FindControl("ddlholderdevicesinsert");
        TextBox _txtasset_codedevices = (TextBox)ShowSelectcom.FindControl("txtasset_codedevices");

        Panel ShowddlSoftware = (Panel)ShowSelectcom.FindControl("ShowddlSoftware");
        TextBox _txtcount_licenseinsert = (TextBox)ShowddlSoftware.FindControl("txtcount_licenseinsert");

        DropDownList ddlsoftware_nameinsert = (DropDownList)ShowddlSoftware.FindControl("ddlsoftware_nameinsert");
        TextBox txttype_insert = (TextBox)ShowddlSoftware.FindControl("txttype_insert");

        // search ///
        DropDownList ddlorg_idxsearch = (DropDownList)ViewIndex.FindControl("ddlorg_idxsearch");
        DropDownList ddlrdept_idxsearch = (DropDownList)ViewIndex.FindControl("ddlrdept_idxsearch");
        DropDownList ddlrsec_idxsearch = (DropDownList)ViewIndex.FindControl("ddlrsec_idxsearch");
        DropDownList ddlemp_idxsearch = (DropDownList)ViewIndex.FindControl("ddlemp_idxsearch");


        // report ///
        DropDownList ddlsoftware_name_idxreport = (DropDownList)ViewReport.FindControl("ddlsoftware_name_idxreport");
        DropDownList ddlsoftware_typereport = (DropDownList)ViewReport.FindControl("ddlsoftware_typereport");

        switch (ddName.ID)
        {
            #region ddlholderdevicesinsert
            case "ddlholderdevicesinsert":

                if (ddlholderdevicesinsert.SelectedValue == "00")
                {
                    _txtasset_codedevices.Text = "";
                    ShowddlSoftware.Visible = false;
                    div_saveinsert.Visible = false;
                }
                else
                {
                    
                    txttype_insert.Text = "";
                    ddlSoftware();
                
                    ShowddlSoftware.Visible = true;
                    div_saveinsert.Visible = true;

                    data_softwarelicense _data_softwarelicense_asset = new data_softwarelicense();
                    _data_softwarelicense_asset.bindu0_device_list = new bindu0_device_detail[1];
                    bindu0_device_detail _data_bindu0_device_detailasset = new bindu0_device_detail();

                    //_data_typedevice_detail.m0_tdidx = 1;

                    //_data_bindu0_device_detailasset.u0_code = txttype_idx.Text;
                    _data_bindu0_device_detailasset.u0_didx = int.Parse(ddlholderdevicesinsert.SelectedValue);

                    _data_softwarelicense_asset.bindu0_device_list[0] = _data_bindu0_device_detailasset;


                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_asset));
                    _data_softwarelicense_asset = callServiceSoftwareLicense(_urlGetDevicesAsset, _data_softwarelicense_asset);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devices));

                    if (_data_softwarelicense_asset.bindu0_device_list[0].u0_didx != 0)
                    {
                        ViewState["devices_u0_didx"] = _data_softwarelicense_asset.bindu0_device_list[0].u0_didx;
                    }
                    else
                    {
                        ViewState["devices_u0_didx"] = "";

                    }

                    if (_data_softwarelicense_asset.bindu0_device_list[0].u0_acc != "0")
                    {
                        ViewState["devices_asset"] = _data_softwarelicense_asset.bindu0_device_list[0].u0_acc;
                    }
                    else
                    {
                        ViewState["devices_asset"] = "-";
                    }

                    if (_data_softwarelicense_asset.bindu0_device_list[0].OrgIDX != 0)
                    {
                        ViewState["devices_OrgIDX"] = _data_softwarelicense_asset.bindu0_device_list[0].OrgIDX;

                    }
                    else
                    {
                        ViewState["devices_OrgIDX"] = "";
                    }

                    if (_data_softwarelicense_asset.bindu0_device_list[0].OrgNameTH != null)
                    {
                        ViewState["devices_OrgNameTH"] = _data_softwarelicense_asset.bindu0_device_list[0].OrgNameTH;
                    }
                    else
                    {
                        ViewState["devices_OrgNameTH"] = "";
                    }

                    if (_data_softwarelicense_asset.bindu0_device_list[0].RDeptIDX != 0)
                    {
                        ViewState["devices_RDeptIDX"] = _data_softwarelicense_asset.bindu0_device_list[0].RDeptIDX;
                    }
                    else
                    {
                        ViewState["devices_RDeptIDX"] = "";
                    }

                    if (_data_softwarelicense_asset.bindu0_device_list[0].DeptNameTH != null)
                    {
                        ViewState["devices_DeptNameTH"] = _data_softwarelicense_asset.bindu0_device_list[0].DeptNameTH;
                    }
                    else
                    {
                        ViewState["devices_DeptNameTH"] = "";
                    }

                    if (_data_softwarelicense_asset.bindu0_device_list[0].RSecIDX != 0)
                    {
                        ViewState["devices_RSecIDX"] = _data_softwarelicense_asset.bindu0_device_list[0].RSecIDX;
                    }
                    else
                    {
                        ViewState["devices_RSecIDX"] = "";
                    }

                    if (_data_softwarelicense_asset.bindu0_device_list[0].SecNameTH != null)
                    {
                        ViewState["devices_SecNameTH"] = _data_softwarelicense_asset.bindu0_device_list[0].SecNameTH;
                    }
                    else
                    {
                        ViewState["devices_SecNameTH"] = "";
                    }

                    if (_data_softwarelicense_asset.bindu0_device_list[0].emp_holder != 0)
                    {
                        ViewState["devices_emp_holder"] = _data_softwarelicense_asset.bindu0_device_list[0].emp_holder;
                    }
                    else
                    {
                        ViewState["devices_emp_holder"] = "";
                    }

                    if (_data_softwarelicense_asset.bindu0_device_list[0].emp_name_th_holder != null)
                    {
                        ViewState["devices_emp_name_th_holder"] = _data_softwarelicense_asset.bindu0_device_list[0].emp_name_th_holder;
                    }
                    else
                    {
                        ViewState["devices_emp_name_th_holder"] = "";
                    }

                    if (_data_softwarelicense_asset.bindu0_device_list[0].email_holder != null)
                    {
                        ViewState["devices_email_holder"] = _data_softwarelicense_asset.bindu0_device_list[0].email_holder;
                    }
                    else
                    {
                        ViewState["devices_email_holder"] = "";
                    }

                    _txtasset_codedevices.Text = ViewState["devices_asset"].ToString();
                    lborg_idx.Text = ViewState["devices_OrgIDX"].ToString();
                    txtorginsert.Text = ViewState["devices_OrgNameTH"].ToString();
                    lbrdept_idx.Text = ViewState["devices_RDeptIDX"].ToString();
                    txtrdeptinsert.Text = ViewState["devices_DeptNameTH"].ToString();
                    lbrsec_idx.Text = ViewState["devices_RSecIDX"].ToString();
                    txtrsecinsert.Text = ViewState["devices_SecNameTH"].ToString();
                    lbemp_idx.Text = ViewState["devices_emp_holder"].ToString();
                    txtcempnameinsert.Text = ViewState["devices_emp_name_th_holder"].ToString();
                    txtemail_holderinsert.Text = ViewState["devices_email_holder"].ToString();

                }


                break;
            #endregion ddlholderdevicesinsert

            #region ddlsoftware_nameinsert
            case "ddlsoftware_nameinsert":
           
                if (ddlsoftware_nameinsert.SelectedValue == "00")
                {
                    txttype_insert.Text = "";
                    _txtcount_licenseinsert.Text = "";

                }
                else
                {
                  
                    data_softwarelicense _data_softwarelicense_typename = new data_softwarelicense();
                    _data_softwarelicense_typename.softwarename_list = new softwarename_detail[1];
                    softwarename_detail _data_softwarename_detailtypename = new softwarename_detail();

                    //_data_typedevice_detail.m0_tdidx = 1;

                    //_data_bindu0_device_detailasset.u0_code = txttype_idx.Text;
                    _data_softwarename_detailtypename.software_name_idx = int.Parse(ddlsoftware_nameinsert.SelectedValue);

                    _data_softwarelicense_typename.softwarename_list[0] = _data_softwarename_detailtypename;


                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devices));
                    _data_softwarelicense_typename = callServiceSoftwareLicense(_urlGetTypeSwinsert, _data_softwarelicense_typename);

                    ViewState["devices_typename"] = _data_softwarelicense_typename.softwarename_list[0].type_name;
                    ViewState["devices_type_idx"] = _data_softwarelicense_typename.softwarename_list[0].type_idx;
                    if (_data_softwarelicense_typename.softwarename_list[0].software_idx != 0)
                    {
                        ViewState["devices_software_idx"] = _data_softwarelicense_typename.softwarename_list[0].software_idx;
                    }
                    else
                    {
                        ViewState["devices_software_idx"] = 0;
                    }

                    //ghost_status
                    if(ViewState["devices_type_idx"].ToString() == "3")
                    {
                        lblghost_statusinsert.Text = "0";

                    }
                    else
                    {
                        lblghost_statusinsert.Text = "1";
                    }


                    txttype_insert.Text = ViewState["devices_typename"].ToString();
                    txttypeidx_insert.Text = ViewState["devices_type_idx"].ToString();

                    lbsoftware_idxinsert.Text = ViewState["devices_software_idx"].ToString();

                    _txtcount_licenseinsert.Text = "1";

                }


                break;
            #endregion ddlsoftware_nameinsert

            #region ddlsoftware_idxupdate
            case "ddlsoftware_idxupdate":

                var ddd = (DropDownList)sender;

                Panel ShowSelectcom2 = (Panel)ViewSoftwareDevices.FindControl("ShowSelectcom");
                Panel ShowddlSoftware2 = (Panel)ShowSelectcom2.FindControl("ShowddlSoftware");
                Panel gidviewlicensedetaillicense = (Panel)ShowddlSoftware2.FindControl("gidviewlicensedetaillicense");
                GridView GVInsertSoftwareLicense = (GridView)gidviewlicensedetaillicense.FindControl("GVInsertSoftwareLicense");
               
                GridViewRow row = GVInsertSoftwareLicense.Rows[GVInsertSoftwareLicense.EditIndex];
                DropDownList ddlsoftware_idxupdate = (DropDownList)row.FindControl("ddlsoftware_idxupdate");
                TextBox txttypename_update = (TextBox)row.FindControl("txttypename_update");
                TextBox texttypeidx_update = (TextBox)row.FindControl("texttypeidx_update");

                if (ddlsoftware_idxupdate.SelectedValue == "00")
                {

                }
                else
                {

                    data_softwarelicense _data_softwarelicense_typenameedit = new data_softwarelicense();
                    _data_softwarelicense_typenameedit.softwarename_list = new softwarename_detail[1];
                    softwarename_detail _data_softwarename_detailtypename = new softwarename_detail();

                    //_data_typedevice_detail.m0_tdidx = 1;

                    //_data_bindu0_device_detailasset.u0_code = txttype_idx.Text;
                    _data_softwarename_detailtypename.software_name_idx = int.Parse(ddlsoftware_idxupdate.SelectedValue);

                    _data_softwarelicense_typenameedit.softwarename_list[0] = _data_softwarename_detailtypename;


                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_typenameedit));
                    _data_softwarelicense_typenameedit = callServiceSoftwareLicense(_urlGetTypeSwinsert, _data_softwarelicense_typenameedit);

                    ViewState["devices_typenameedit"] = _data_softwarelicense_typenameedit.softwarename_list[0].type_name;
                    ViewState["devices_typeidxedit"] = _data_softwarelicense_typenameedit.softwarename_list[0].type_idx;

                    txttypename_update.Text = ViewState["devices_typenameedit"].ToString();
                    texttypeidx_update.Text = ViewState["devices_typeidxedit"].ToString();
                 
                }

                break;
            #endregion ddlsoftware_idxupdate
           
            case "ddlorg_idxinsert":

                if (ddlorg_idxinsert.SelectedValue == "00")
                {

                    ddlrdept_idxinsert.Items.Clear();
                    ddlrdept_idxinsert.AppendDataBoundItems = true;
                    ddlrdept_idxinsert.Items.Add(new ListItem("เลือกฝ่าย ...", "00"));

                    ddlrsec_idxinsert.Items.Clear();
                    ddlrsec_idxinsert.AppendDataBoundItems = true;
                    ddlrsec_idxinsert.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idxinsert.Items.Clear();
                    ddlemp_idxinsert.AppendDataBoundItems = true;
                    ddlemp_idxinsert.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));


                }
                else
                {
                
                    data_softwarelicense _data_softwarelicensevieworg2 = new data_softwarelicense();
                    _data_softwarelicensevieworg2.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _organization_detail.OrgIDX = int.Parse(ddlorg_idxinsert.SelectedValue);

                    _data_softwarelicensevieworg2.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicensevieworg2 = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicensevieworg2);

                    ddlrdept_idxinsert.Items.Clear();
                    ddlrdept_idxinsert.AppendDataBoundItems = true;
                    ddlrdept_idxinsert.Items.Add(new ListItem("เลือกฝ่าย ...", "00"));
                    ddlrdept_idxinsert.DataSource = _data_softwarelicensevieworg2.organization_list;
                    ddlrdept_idxinsert.DataTextField = "DeptNameTH";
                    ddlrdept_idxinsert.DataValueField = "RDeptIDX";
                    ddlrdept_idxinsert.DataBind();

                    ddlrsec_idxinsert.Items.Clear();
                    ddlrsec_idxinsert.AppendDataBoundItems = true;
                    ddlrsec_idxinsert.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idxinsert.Items.Clear();
                    ddlemp_idxinsert.AppendDataBoundItems = true;
                    ddlemp_idxinsert.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));

                }

                break;

            case "ddlrdept_idxinsert":

                if (ddlrdept_idxinsert.SelectedValue == "00")
                {

                    ddlrsec_idxinsert.Items.Clear();
                    ddlrsec_idxinsert.AppendDataBoundItems = true;
                    ddlrsec_idxinsert.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idxinsert.Items.Clear();
                    ddlemp_idxinsert.AppendDataBoundItems = true;
                    ddlemp_idxinsert.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
              
                }
                else
                {

                    
                    data_softwarelicense _data_softwarelicenseviewdept2 = new data_softwarelicense();
                    _data_softwarelicenseviewdept2.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _organization_detail.OrgIDX = int.Parse(ddlorg_idxinsert.SelectedValue);
                    _organization_detail.RDeptIDX = int.Parse(ddlrdept_idxinsert.SelectedValue);

                    _data_softwarelicenseviewdept2.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

                    _data_softwarelicenseviewdept2 = callServiceSoftwareLicense(_urlGetddlSectionSW, _data_softwarelicenseviewdept2);

                    ddlrsec_idxinsert.Items.Clear();
                    ddlrsec_idxinsert.AppendDataBoundItems = true;
                    ddlrsec_idxinsert.Items.Add(new ListItem("เลือกแผนก ...", "00"));
                    ddlrsec_idxinsert.DataSource = _data_softwarelicenseviewdept2.organization_list;
                    ddlrsec_idxinsert.DataTextField = "SecNameTH";
                    ddlrsec_idxinsert.DataValueField = "RSecIDX";
                    ddlrsec_idxinsert.DataBind();

                    ddlemp_idxinsert.Items.Clear();
                    ddlemp_idxinsert.AppendDataBoundItems = true;
                    ddlemp_idxinsert.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                  

                }


                break;

            case "ddlrsec_idxinsert":

                if (ddlrsec_idxinsert.SelectedValue == "00")
                {


                    ddlemp_idxinsert.Items.Clear();
                    ddlemp_idxinsert.AppendDataBoundItems = true;
                    ddlemp_idxinsert.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));

                
                }
                else
                {

                    data_softwarelicense _data_softwarelicenseviewdept331 = new data_softwarelicense();
                    _data_softwarelicenseviewdept331.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _organization_detail.OrgIDX = int.Parse(ddlorg_idxinsert.SelectedValue);
                    _organization_detail.RDeptIDX = int.Parse(ddlrdept_idxinsert.SelectedValue);
                    _organization_detail.RSecIDX = int.Parse(ddlrsec_idxinsert.SelectedValue);

                    _data_softwarelicenseviewdept331.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

                    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlEmpSW, _data_softwarelicenseviewdept331);


                    ddlemp_idxinsert.Items.Clear();
                    ddlemp_idxinsert.AppendDataBoundItems = true;
                    ddlemp_idxinsert.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                    ddlemp_idxinsert.DataSource = _data_softwarelicenseviewdept331.organization_list;
                    ddlemp_idxinsert.DataTextField = "emp_name_th";
                    ddlemp_idxinsert.DataValueField = "emp_idx";
                    ddlemp_idxinsert.DataBind();
             

                }
                break;

            case "ddlemp_idxinsert":

                if (ddlemp_idxinsert.SelectedValue == "00")
                {
                    show_btninsertdataset.Visible = false;
                }
                else
                {
                    show_btninsertdataset.Visible = true;
                }


                break;

            case "ddlorg_idxsearch":

                if (ddlorg_idxsearch.SelectedValue == "00")
                {


                    ddlrdept_idxsearch.Items.Clear();
                    ddlrdept_idxsearch.AppendDataBoundItems = true;
                    ddlrdept_idxsearch.Items.Add(new ListItem("เลือกฝ่าย ...", "00"));

                    ddlrsec_idxsearch.Items.Clear();
                    ddlrsec_idxsearch.AppendDataBoundItems = true;
                    ddlrsec_idxsearch.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));

                }
                else
                {
                   
                    data_softwarelicense _data_softwarelicensevieworg2search = new data_softwarelicense();
                    _data_softwarelicensevieworg2search.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _organization_detail.OrgIDX = int.Parse(ddlorg_idxsearch.SelectedValue);

                    _data_softwarelicensevieworg2search.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicensevieworg2search = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicensevieworg2search);


                    ddlrdept_idxsearch.Items.Clear();
                    ddlrdept_idxsearch.AppendDataBoundItems = true;
                    ddlrdept_idxsearch.Items.Add(new ListItem("เลือกฝ่าย ...", "00"));
                    ddlrdept_idxsearch.DataSource = _data_softwarelicensevieworg2search.organization_list;
                    ddlrdept_idxsearch.DataTextField = "DeptNameTH";
                    ddlrdept_idxsearch.DataValueField = "RDeptIDX";
                    ddlrdept_idxsearch.DataBind();

                    ddlrsec_idxsearch.Items.Clear();
                    ddlrsec_idxsearch.AppendDataBoundItems = true;
                    ddlrsec_idxsearch.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));

                }

                break;

            case "ddlrdept_idxsearch":

                if (ddlrdept_idxsearch.SelectedValue == "00")
                {

                    ddlrsec_idxsearch.Items.Clear();
                    ddlrsec_idxsearch.AppendDataBoundItems = true;
                    ddlrsec_idxsearch.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                               
                }
                else
                {

                    data_softwarelicense _data_softwarelicenseviewdept2search = new data_softwarelicense();
                    _data_softwarelicenseviewdept2search.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _organization_detail.OrgIDX = int.Parse(ddlorg_idxsearch.SelectedValue);
                    _organization_detail.RDeptIDX = int.Parse(ddlrdept_idxsearch.SelectedValue);

                    _data_softwarelicenseviewdept2search.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

                    _data_softwarelicenseviewdept2search = callServiceSoftwareLicense(_urlGetddlSectionSW, _data_softwarelicenseviewdept2search);


                    ddlrsec_idxsearch.Items.Clear();
                    ddlrsec_idxsearch.AppendDataBoundItems = true;
                    ddlrsec_idxsearch.Items.Add(new ListItem("เลือกแผนก ...", "00"));
                    ddlrsec_idxsearch.DataSource = _data_softwarelicenseviewdept2search.organization_list;
                    ddlrsec_idxsearch.DataTextField = "SecNameTH";
                    ddlrsec_idxsearch.DataValueField = "RSecIDX";
                    ddlrsec_idxsearch.DataBind();

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));                  
                 
                }


                break;

            case "ddlrsec_idxsearch":

                if (ddlrsec_idxsearch.SelectedValue == "00")
                {

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));

                }
                else
                {
                 
                    data_softwarelicense _data_softwarelicenseviewemp2search = new data_softwarelicense();
                    _data_softwarelicenseviewemp2search.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _organization_detail.OrgIDX = int.Parse(ddlorg_idxsearch.SelectedValue);
                    _organization_detail.RDeptIDX = int.Parse(ddlrdept_idxsearch.SelectedValue);
                    _organization_detail.RSecIDX = int.Parse(ddlrsec_idxsearch.SelectedValue);

                    _data_softwarelicenseviewemp2search.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

                    _data_softwarelicenseviewemp2search = callServiceSoftwareLicense(_urlGetddlEmpSW, _data_softwarelicenseviewemp2search);


                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                    ddlemp_idxsearch.DataSource = _data_softwarelicenseviewemp2search.organization_list;
                    ddlemp_idxsearch.DataTextField = "emp_name_th";
                    ddlemp_idxsearch.DataValueField = "emp_idx";
                    ddlemp_idxsearch.DataBind();

                 


                }
                break;

            case "ddlorg_idxreport":

                if (ddlorg_idxreport.SelectedValue == "00")
                {

                    ddlrdept_idxreport.Items.Clear();
                    ddlrdept_idxreport.AppendDataBoundItems = true;
                    ddlrdept_idxreport.Items.Add(new ListItem("เลือกฝ่าย ...", "00"));
                              
                }
                else
                {

                
                    data_softwarelicense _data_softwarelicensevieworg2 = new data_softwarelicense();
                    _data_softwarelicensevieworg2.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _organization_detail.OrgIDX = int.Parse(ddlorg_idxreport.SelectedValue);

                    _data_softwarelicensevieworg2.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicensevieworg2 = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicensevieworg2);


                    ddlrdept_idxreport.Items.Clear();
                    ddlrdept_idxreport.AppendDataBoundItems = true;
                    ddlrdept_idxreport.Items.Add(new ListItem("เลือกฝ่าย ...", "00"));
                    ddlrdept_idxreport.DataSource = _data_softwarelicensevieworg2.organization_list;
                    ddlrdept_idxreport.DataTextField = "DeptNameTH";
                    ddlrdept_idxreport.DataValueField = "RDeptIDX";
                    ddlrdept_idxreport.DataBind();
                             
                }

                break;




        }
    }

    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        //int _category_idx;
        //string _category_name;
        int _cemp_idx;

        // *** value insert ***///
        // *** value insert ***///

        u0_softwarelicense_detail _u0_softwarelicense_detail = new u0_softwarelicense_detail();
        u1_softwarelicense_detail _u1_softwarelicense_detail = new u1_softwarelicense_detail();
        u2_softwarelicense_detail _u2_softwarelicense_detail = new u2_softwarelicense_detail();
        approve_u0software_detail _approve_u0software_detail = new approve_u0software_detail();
        report_softwarelicense_detail _report_softwarelicense_detail = new report_softwarelicense_detail();
        bind_softwarelicense_detail _bind_softwarelicense_detail = new bind_softwarelicense_detail();
        search_softwarelicense_detail _search_softwarelicense_detaill = new search_softwarelicense_detail();
        bindreport_softwarelicense_detail _bindreport_softwarelicense_detail = new bindreport_softwarelicense_detail();

        switch (cmdName)
        {
            case "btnToIndexList":

                btnToIndexList.BackColor = System.Drawing.Color.LightGray;
                btnToDeviceSoftware.BackColor = System.Drawing.Color.Transparent;
                btnToReport.BackColor = System.Drawing.Color.Transparent;
              
                MvMaster.SetActiveView(ViewIndex);

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnToDeviceSoftware":


                btnToIndexList.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceSoftware.BackColor = System.Drawing.Color.LightGray;
                btnToReport.BackColor = System.Drawing.Color.Transparent;
                
                MvMaster.SetActiveView(ViewSoftwareDevices);

                FvInsert.ChangeMode(FormViewMode.Insert);
                FvInsert.DataBind();
              
                ddlOrganization();
                ddlTypeHolder();

             
                break;

            case "btnToReport":

                btnToIndexList.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceSoftware.BackColor = System.Drawing.Color.Transparent;
                btnToReport.BackColor = System.Drawing.Color.LightGray;
               
                MvMaster.SetActiveView(ViewReport);

                ddlSoftwareReport();
                ddltypeSoftwareReport();
                ddlOrganizationReport();

                data_softwarelicense_devices _data_softwarelicense_devicesbindreport = new data_softwarelicense_devices();
                _data_softwarelicense_devicesbindreport.bindreport_softwarelicense_list = new bindreport_softwarelicense_detail[1];
            
                _data_softwarelicense_devicesbindreport.bindreport_softwarelicense_list[0] = _bindreport_softwarelicense_detail;

                _data_softwarelicense_devicesbindreport = callServiceSoftwareLicenseDevices(_urlGetBindReportSoftware, _data_softwarelicense_devicesbindreport);

                gridviewreport.Visible = true;

                ViewState["bind_data_report"] = _data_softwarelicense_devicesbindreport.bindreport_softwarelicense_list;


                setGridData(GvReport, ViewState["bind_data_report"]);

                break;

            case "btnSearchIndex":

                ddlHolderDevices();
                ddlSoftware();

                break;

            case "btnSearch":

                fvBacktoIndex.Visible = true;
                showsearch.Visible = true;
                btnSearch.Visible = false;

                break;

          
            case "btnSearchBack":

                showsearch.Visible = false;
                fvBacktoIndex.Visible = false;
                btnSearch.Visible = true;

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnCancelIndex":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnSearchIndex_First":

                DropDownList _ddlorg_idxsearch = (DropDownList)ViewIndex.FindControl("ddlorg_idxsearch");
                DropDownList _ddlrdept_idxsearch = (DropDownList)ViewIndex.FindControl("ddlrdept_idxsearch");
                DropDownList _ddlrsec_idxsearch = (DropDownList)ViewIndex.FindControl("ddlrsec_idxsearch");
                DropDownList _ddlemp_idxsearch = (DropDownList)ViewIndex.FindControl("ddlemp_idxsearch");

                _cemp_idx = emp_idx;

                data_softwarelicense_devices _data_softwarelicense_devicessearchindex = new data_softwarelicense_devices();
                _data_softwarelicense_devicessearchindex.search_softwarelicense_list = new search_softwarelicense_detail[1];

                _search_softwarelicense_detaill.org_idx = int.Parse(_ddlorg_idxsearch.SelectedValue);
                _search_softwarelicense_detaill.rdept_idx = int.Parse(_ddlrdept_idxsearch.SelectedValue);
                _search_softwarelicense_detaill.rsec_idx = int.Parse(_ddlrsec_idxsearch.SelectedValue);
                _search_softwarelicense_detaill.emp_idx = int.Parse(_ddlemp_idxsearch.SelectedValue);

                _data_softwarelicense_devicessearchindex.search_softwarelicense_list[0] = _search_softwarelicense_detaill;

                _data_softwarelicense_devicessearchindex = callServiceSoftwareLicenseDevices(_urlGetSearchIndex, _data_softwarelicense_devicessearchindex);


                ViewState["bind_data_index"] = _data_softwarelicense_devicessearchindex.search_softwarelicense_list;

                setGridData(GvMaster, ViewState["bind_data_index"]);


                break;

            case "btnCancelIndex_First":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnBackToSearch":

                Panel ShowSelectcomreset = (Panel)ViewSoftwareDevices.FindControl("ShowSelectcom");
                Panel ShowddlSoftwarereset = (Panel)ShowSelectcomreset.FindControl("ShowddlSoftware");
                Panel gidviewlicensedetaillicensereset = (Panel)ShowddlSoftwarereset.FindControl("gidviewlicensedetaillicense");
                GridView GVInsertSoftwareLicensereset = (GridView)gidviewlicensedetaillicensereset.FindControl("GVInsertSoftwareLicense");

                div_saveinsert.Visible = false;
                fvBacktoSearch.Visible = false;
                ShowSelectcom.Visible = false;
                ShowddlSoftware.Visible = false;
                txtasset_codedevices.Text = "";

                var dsEquiment3 = (DataSet)ViewState["vsBuyequipment"];
                //dsEquiment3 = null;
                dsEquiment3.Clear();
                GVInsertSoftwareLicensereset.DataSource = dsEquiment3;
                GVInsertSoftwareLicensereset.DataBind();
                

                MvMaster.SetActiveView(ViewSoftwareDevices);

                FvInsert.Visible = true;
                FvInsert.ChangeMode(FormViewMode.Insert);
                FvInsert.DataBind();

                //ddlSoftware();
                ddlOrganization();
                ddlTypeHolder();



                break;

            case "btnInsertSoftware":

                //FormView FvInsert = (FormView)ViewSoftwareDevices.FindControl("FvInsert");
                Panel ShowSelectcominsert = (Panel)ViewSoftwareDevices.FindControl("ShowSelectcom");
                DropDownList ddlholderdevicesinsert = (DropDownList)ShowSelectcominsert.FindControl("ddlholderdevicesinsert");
                TextBox txtasset_codedevicesinsert = (TextBox)ShowSelectcominsert.FindControl("txtasset_codedevices");

                Panel ShowddlSoftwareinsert = (Panel)ShowSelectcominsert.FindControl("ShowddlSoftware");
                TextBox txtcempnameinsert = (TextBox)ShowddlSoftwareinsert.FindControl("txtcempnameinsert");
                Label lborg_idxinsert = (Label)ShowddlSoftwareinsert.FindControl("lborg_idx");
                TextBox txtorginsert = (TextBox)ShowddlSoftwareinsert.FindControl("txtorginsert");
                Label lbrdept_idxinsert = (Label)ShowddlSoftwareinsert.FindControl("lbrdept_idx");
                TextBox txtrdeptinsert = (TextBox)ShowddlSoftwareinsert.FindControl("txtrdeptinsert");
                TextBox txtrsecinsert = (TextBox)ShowddlSoftwareinsert.FindControl("txtrsecinsert");

                DropDownList ddlsoftware_nameinsert = (DropDownList)ShowddlSoftwareinsert.FindControl("ddlsoftware_nameinsert");
                Label lbsoftware_idxinsert_insert = (Label)ShowddlSoftwareinsert.FindControl("lbsoftware_idxinsert");
                TextBox txttype_insert = (TextBox)ShowddlSoftwareinsert.FindControl("txttype_insert");
                Label txttypeidx_insert = (Label)ShowddlSoftwareinsert.FindControl("txttypeidx_insert");
                Label lblghost_statusinsert = (Label)ShowddlSoftwareinsert.FindControl("lblghost_statusinsert");
                TextBox txtcount_licenseinsert = (TextBox)ShowddlSoftwareinsert.FindControl("txtcount_licenseinsert");

                Panel ShowSelectcom1 = (Panel)ViewSoftwareDevices.FindControl("ShowSelectcom");
                Panel ShowddlSoftware1 = (Panel)ShowSelectcom1.FindControl("ShowddlSoftware");
                Panel gidviewlicensedetaillicense = (Panel)ShowddlSoftware1.FindControl("gidviewlicensedetaillicense");
                GridView GVInsertSoftwareLicense = (GridView)gidviewlicensedetaillicense.FindControl("GVInsertSoftwareLicense");

                //////  create data set show detail license  ////////

                var dsEquiment = (DataSet)ViewState["vsBuyequipment"];
                var drEquiment = dsEquiment.Tables[0].NewRow();

                int numrow = dsEquiment.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["ddlsoftware_idxinsertdataset"] = ddlsoftware_nameinsert.SelectedValue;
                ViewState["txttypeidx_insertdataset"] = txttypeidx_insert.Text;

                if(lborg_idxinsert.Text != "" && lbrdept_idxinsert.Text != "")
                {
                    if (numrow > 0)
                    {
                        foreach (DataRow check in dsEquiment.Tables[0].Rows)
                        {
                            ViewState["check_software_idx"] = check["swlicense_idx_dataset"];
                            ViewState["check_txttype_idx"] = check["type_idx_dataset"];


                            //if((int.Parse(ViewState["_ddlorg_idxinsert_check"].ToString()) == int.Parse(ViewState["check_org"].ToString())) && (int.Parse(ViewState["_ddlrdept_idxinsert_check"].ToString()) == int.Parse(ViewState["check_dept"].ToString())))

                            //if (int.Parse(ViewState["_ddlorg_idxinsert_check"].ToString()) == int.Parse(ViewState["ch"].ToString()))
                            if ((int.Parse(ViewState["ddlsoftware_idxinsertdataset"].ToString()) == int.Parse(ViewState["check_software_idx"].ToString())) && (int.Parse(ViewState["txttypeidx_insertdataset"].ToString()) == int.Parse(ViewState["check_txttype_idx"].ToString())))
                            {
                                ViewState["CheckDataset"] = "0";
                                break;

                            }
                            else
                            {
                                ViewState["CheckDataset"] = "1";

                            }
                        }

                        if (ViewState["CheckDataset"].ToString() == "1")
                        {

                            div_saveinsert.Visible = true;

                            //ค่าที่เพิ่มใน dataset
                            drEquiment["software_idx_dataset"] = int.Parse(lbsoftware_idxinsert_insert.Text);
                            drEquiment["swlicense_name_dataset"] = ddlsoftware_nameinsert.SelectedItem;
                            drEquiment["swlicense_idx_dataset"] = ddlsoftware_nameinsert.SelectedValue;

                            drEquiment["org_idx_dataset"] = int.Parse(lborg_idxinsert.Text);
                            drEquiment["rdept_idx_dataset"] = int.Parse(lbrdept_idxinsert.Text);

                            drEquiment["type_name_dataset"] = txttype_insert.Text;
                            drEquiment["type_idx_dataset"] = int.Parse(txttypeidx_insert.Text);
                            drEquiment["ghoststatus_dataset"] = int.Parse(lblghost_statusinsert.Text);

                            drEquiment["countinsert_dataset"] = int.Parse(txtcount_licenseinsert.Text);


                            //drEquiment["OrgNameTH_dataset"] = ddlorg_idxinsert.SelectedItem;
                            //drEquiment["OrgIDX_dataset"] = ddlorg_idxinsert.SelectedValue;

                            dsEquiment.Tables[0].Rows.Add(drEquiment);
                            ViewState["vsBuyequipment"] = dsEquiment;

                            gidviewlicensedetaillicense.Visible = true;

                            GVInsertSoftwareLicense.DataSource = dsEquiment.Tables[0];
                            GVInsertSoftwareLicense.DataBind();

                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลอยู่แล้ว');", true);
                            //break;
                        }

                    }
                    else
                    {


                        div_saveinsert.Visible = true;

                        //ค่าที่เพิ่มใน dataset
                        drEquiment["software_idx_dataset"] = int.Parse(lbsoftware_idxinsert_insert.Text);
                        drEquiment["swlicense_name_dataset"] = ddlsoftware_nameinsert.SelectedItem;
                        drEquiment["swlicense_idx_dataset"] = ddlsoftware_nameinsert.SelectedValue;

                        drEquiment["org_idx_dataset"] = int.Parse(lborg_idxinsert.Text);
                        drEquiment["rdept_idx_dataset"] = int.Parse(lbrdept_idxinsert.Text);

                        drEquiment["type_name_dataset"] = txttype_insert.Text;
                        drEquiment["type_idx_dataset"] = int.Parse(txttypeidx_insert.Text);
                        drEquiment["ghoststatus_dataset"] = int.Parse(lblghost_statusinsert.Text);

                        drEquiment["countinsert_dataset"] = int.Parse(txtcount_licenseinsert.Text);

                        //drEquiment["OrgNameTH_dataset"] = ddlorg_idxinsert.SelectedItem;
                        //drEquiment["OrgIDX_dataset"] = ddlorg_idxinsert.SelectedValue;

                        dsEquiment.Tables[0].Rows.Add(drEquiment);
                        ViewState["vsBuyequipment"] = dsEquiment;

                        gidviewlicensedetaillicense.Visible = true;

                        GVInsertSoftwareLicense.DataSource = dsEquiment.Tables[0];
                        GVInsertSoftwareLicense.DataBind();

                    }

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถเพิ่มรายการ Software ได้!! เนื่องจากข้อมูลไม่ครบ');", true);
                    break;
                }



                break;

            case "btnSaveInsert":

                //////////////
                Panel ShowSelectcomInsert = (Panel)ViewSoftwareDevices.FindControl("ShowSelectcom");
                DropDownList _ddlholderdevicesinsert = (DropDownList)ShowSelectcomInsert.FindControl("ddlholderdevicesinsert");

                Panel ShowddlSoftwareInsert = (Panel)ShowSelectcomInsert.FindControl("ShowddlSoftware");
                Label _lbsoftware_idx_dataset = (Label)ShowddlSoftwareInsert.FindControl("lbsoftware_idx_dataset");
                Label _lborg_idx = (Label)ShowddlSoftwareInsert.FindControl("lborg_idx");
                Label _lbrdept_idx = (Label)ShowddlSoftwareInsert.FindControl("lbrdept_idx");
                Label _lbrsec_idx = (Label)ShowddlSoftwareInsert.FindControl("lbrsec_idx");
                Label _lbemp_idx = (Label)ShowddlSoftwareInsert.FindControl("lbemp_idx");
                TextBox txtemail_holderinsert = (TextBox)ShowddlSoftwareInsert.FindControl("txtemail_holderinsert");// email  แจ้ง user
                TextBox _txtcempnameinsert = (TextBox)ShowddlSoftwareInsert.FindControl("txtcempnameinsert");// ชื่อ user

                //DropDownList _ddlsoftware_nameinsert = (DropDownList)ShowddlSoftwareInsert.FindControl("ddlsoftware_nameinsert");

                ///////////
                _cemp_idx = emp_idx;

                string count_softwareidx = "";

                //ข้อมูลใน dataset
                var dsBuyequipment = (DataSet)ViewState["vsBuyequipment"];

                var u1_softwarelicense_detail_dataset = new u1_softwarelicense_detail[dsBuyequipment.Tables[0].Rows.Count];

                //Check องค์กร ฝ่าย ตอนแบ่ง License
                int dataset = 0;
                int i_software = 1;
                string software_email = string.Empty;
                foreach (DataRow drw in dsBuyequipment.Tables[0].Rows)
                {
                                       
                    u1_softwarelicense_detail_dataset[dataset] = new u1_softwarelicense_detail();
                   
                    ////u1_softwarelicense_detail_dataset[dataset].software_idx = int.Parse(drw["software_idx_dataset"].ToString());
                    u1_softwarelicense_detail_dataset[dataset].software_name_idx = int.Parse(drw["swlicense_idx_dataset"].ToString());
                    u1_softwarelicense_detail_dataset[dataset].software_name_emailidx = drw["swlicense_name_dataset"].ToString();
                    u1_softwarelicense_detail_dataset[dataset].org_idx = int.Parse(drw["org_idx_dataset"].ToString());
                    u1_softwarelicense_detail_dataset[dataset].rdept_idx = int.Parse(drw["rdept_idx_dataset"].ToString());
                    u1_softwarelicense_detail_dataset[dataset].ghost_status = int.Parse(drw["ghoststatus_dataset"].ToString());
                    u1_softwarelicense_detail_dataset[dataset].num_license = int.Parse(drw["countinsert_dataset"].ToString());


                    software_email += string.Format("<tr><td>{0}. {1}</td></tr>", i_software++, drw["swlicense_name_dataset"].ToString());

                  

                    dataset++;

                }              
                int i = 0;
                string software_name_where = string.Empty;
                foreach (DataRow dr in dsBuyequipment.Tables[0].Rows)
                {
                   
                    count_softwareidx += int.Parse(dr["swlicense_idx_dataset"].ToString()) + ",";
                    
                }

                software_name_where = count_softwareidx;

                data_softwarelicense_devices _data_softwarelicense_devicesinsert = new data_softwarelicense_devices();
                _data_softwarelicense_devicesinsert.u0_softwarelicense_list = new u0_softwarelicense_detail[1];

                _u0_softwarelicense_detail.u0_didx = int.Parse(_ddlholderdevicesinsert.SelectedValue);
                _u0_softwarelicense_detail.u0_didx_code = _ddlholderdevicesinsert.SelectedItem.ToString();
                _u0_softwarelicense_detail.org_idx = int.Parse(_lborg_idx.Text);
                _u0_softwarelicense_detail.rdept_idx = int.Parse(_lbrdept_idx.Text);
                _u0_softwarelicense_detail.rsec_idx = int.Parse(_lbrsec_idx.Text);               
                _u0_softwarelicense_detail.software_name_where = software_name_where;
                _u0_softwarelicense_detail.email = txtemail_holderinsert.Text;
                //_u0_softwarelicense_detail.software_name_sentemail = HttpUtility.UrlEncode(software_email);
                if (_lbemp_idx.Text != "")
                {
                    _u0_softwarelicense_detail.emp_idx = int.Parse(_lbemp_idx.Text);
                }
                else
                {
                    _u0_softwarelicense_detail.emp_idx = 0;
                }
                _u0_softwarelicense_detail.num_license = count_softwareidx;

                _u0_softwarelicense_detail.cemp_idx = _cemp_idx;
                _u0_softwarelicense_detail.m0_node_idx = 1;
                _u0_softwarelicense_detail.m0_actor_idx = 1;
                _u0_softwarelicense_detail.doc_decision = 0;


                _data_softwarelicense_devicesinsert.u0_softwarelicense_list[0] = _u0_softwarelicense_detail;
                _data_softwarelicense_devicesinsert.u1_softwarelicense_list = u1_softwarelicense_detail_dataset;

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devicesinsert));
                if (dsBuyequipment.Tables[0].Rows.Count != 0)
                {
                    _data_softwarelicense_devicesinsert = callServiceSoftwareLicenseDevices(_urlSetSoftwareLicense, _data_softwarelicense_devicesinsert);


                    if (_data_softwarelicense_devicesinsert.return_code == 0)
                    {

                        //actionIndex();
                        //MvMaster.SetActiveView(ViewIndex);

                        Page.Response.Redirect(Page.Request.Url.ToString(), true);

                        //if (_txtcempnameinsert.Text == "" || _txtcempnameinsert.Text == null)
                        //{


                        //    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                        //}
                        //else
                        //{

                        //    string email = "waraporn.teoy@gmail.com";
                        //    //string email = txtemail_holderinsert.Text; เมลผู้ถือครอง
                        //    ///string replyempcreate = _txtemailcreate.Text;
                        //    string replyempcreate = "teoy_42@hotmail.com";
                        //    string link_software = "http://demo.taokaenoi.co.th/software-license";

                        //    _mail_subject = "[MIS/Software : Software License ] - แจ้งรายการโปรแกรมลิขสิทธิ์"; //+ ViewState["registerfilecode"].ToString();
                        //    _mail_body = servicemail.softwareLicenseCreate(_data_softwarelicense_devicesinsert.u0_softwarelicense_list[0], software_email, link_software);
                        //    //servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
                        //    servicemail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);

                        //    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                        //}

                    }
                    else
                    {
                        //setError(_data_softwarelicense_devicesinsert.return_code.ToString() + " - " + _data_softwarelicense_devicesinsert.return_msg);
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('License นี้มีอยู่ที่เครื่องนี้แล้ว!! กรุณาเลือก License ใหม่');", true);
                        break;
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเพิ่มข้อมูล Software License ก่อนบันทึก');", true);
                }



                break;

            case "btnCancelInsert":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnCancelReport":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnViewindex":

                string[] arg1 = new string[3];
                arg1 = e.CommandArgument.ToString().Split(';');
                int u0_software_idx_index = int.Parse(arg1[0]);
                //string doc_code = arg1[1];
                int noidx_index = int.Parse(arg1[1]);
                int emp_permission = int.Parse(arg1[2]);

                ViewState["u0_software_idx_index"] = u0_software_idx_index;
                //ViewState["registernamecode"] = doc_code;
                ViewState["noidx_index"] = noidx_index;
                ViewState["emp_permission_check"] = emp_permission;

                gridviewindex.Visible = false;
                btnSearch.Visible = false;
                btnSearchBack.Visible = false;
                showsearch.Visible = false;

                div_showdata.Visible = true;

                SelectViewIndex();


                panel_detail_softwarelicense.Visible = true;
                SelectLogDetail();



                gridview_showsoftware.Visible = true;
                div_showdetailswname.Visible = true;
                div_swname.Visible = true;

                //panel_detail_softwarelicense.Visible = true;
                SelectNameSoftware();

                panelview.Visible = true;
                divshownamedetail.Visible = true;

                div_btn.Visible = true;
                btnCancelView.Visible = true;

                setFormData();



                break;

            case "btnCancelView":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnCancelApprove":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnsaveapprove":

                HiddenField hf_m0_node_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_node_idx");
                HiddenField hf_m0_actor_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_actor_idx");

                DropDownList ddl_approvestatus = (DropDownList)ViewIndex.FindControl("ddl_approvestatus");
                TextBox txtComment_approve = (TextBox)ViewIndex.FindControl("txtComment_approve");
                TextBox txtu0_codeview = (TextBox)FvViewDetail.FindControl("txtu0_codeview");

                int node = int.Parse(hf_m0_node_idx.Value);
                int actor = int.Parse(hf_m0_actor_idx.Value);
                _cemp_idx = emp_idx;

                if (actor == 2)
                {
                    data_softwarelicense_devices _data_softwarelicense_devicesapprove = new data_softwarelicense_devices();
                    _data_softwarelicense_devicesapprove.u0_softwarelicense_list = new u0_softwarelicense_detail[1];

                    _u0_softwarelicense_detail.u0_software_idx = int.Parse(ViewState["u0_software_idx_index"].ToString());
                    _u0_softwarelicense_detail.m0_node_idx = node;
                    _u0_softwarelicense_detail.m0_actor_idx = actor;
                    _u0_softwarelicense_detail.cemp_idx = _cemp_idx;
                    _u0_softwarelicense_detail.doc_decision = int.Parse(ddl_approvestatus.SelectedValue);
                    _u0_softwarelicense_detail.u0_code = txtu0_codeview.Text;

                    if (txtComment_approve.Text != "")
                    {
                        _u0_softwarelicense_detail.comment = txtComment_approve.Text;
                    }
                    else
                    {
                        _u0_softwarelicense_detail.comment = "-";
                    }
                    
             
                    _data_softwarelicense_devicesapprove.u0_softwarelicense_list[0] = _u0_softwarelicense_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devicesapprove));

                   
                    _data_softwarelicense_devicesapprove = callServiceSoftwareLicenseDevices(_urlSetApproveLicense, _data_softwarelicense_devicesapprove);


                    if (_data_softwarelicense_devicesapprove.return_code == 0)
                    {
                        //actionIndex();
                        //MvMaster.SetActiveView(ViewIndex);    

                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                    else
                    {
                        setError(_data_softwarelicense_devicesapprove.return_code.ToString() + " - " + _data_softwarelicense_devicesapprove.return_msg);
                    }

                }

                break;

            case "btnSearchReport":

                gridviewreport.Visible = true;

                DropDownList _ddlsoftware_name_idxreport = (DropDownList)ViewReport.FindControl("ddlsoftware_name_idxreport");
                DropDownList _ddlsoftware_typereport = (DropDownList)ViewReport.FindControl("ddlsoftware_typereport");
                DropDownList _ddlorg_idxreport = (DropDownList)ViewReport.FindControl("ddlorg_idxreport");
                DropDownList _ddlrdept_idxreport = (DropDownList)ViewReport.FindControl("ddlrdept_idxreport");

                _cemp_idx = emp_idx;


                data_softwarelicense_devices _data_softwarelicense_devicesreport = new data_softwarelicense_devices();
                _data_softwarelicense_devicesreport.report_softwarelicense_list = new report_softwarelicense_detail[1];

                _report_softwarelicense_detail.software_name_idx = int.Parse(_ddlsoftware_name_idxreport.SelectedValue);
                _report_softwarelicense_detail.software_type = int.Parse(_ddlsoftware_typereport.SelectedValue);
                _report_softwarelicense_detail.org_idx = int.Parse(_ddlorg_idxreport.SelectedValue);
                _report_softwarelicense_detail.rdept_idx = int.Parse(_ddlrdept_idxreport.SelectedValue);

                _data_softwarelicense_devicesreport.report_softwarelicense_list[0] = _report_softwarelicense_detail;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));
                _data_softwarelicense_devicesreport = callServiceSoftwareLicenseDevices(_urlGetSearchReportSoftware, _data_softwarelicense_devicesreport);


                ViewState["bind_data_report"] = _data_softwarelicense_devicesreport.report_softwarelicense_list;


                setGridData(GvReport, ViewState["bind_data_report"]);

                break;
          
              
        }





    }

    #endregion btnCommand

    #region Set_Defult_Index

    protected void Set_Defult_Index()
    {
        btnToIndexList.BackColor = System.Drawing.Color.LightGray;
        //btnToDeviceSoftware.BackColor = System.Drawing.Color.Transparent;
    }

    #endregion

    #region bind Node

    protected void setFormData()
    {

        FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
        HiddenField hf_m0_node_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_node_idx");

        m0_node = int.Parse(hf_m0_node_idx.Value);// int.Parse(ViewState["M0_Node"].ToString());

        switch (m0_node)
        {
            case 1: // สร้าง/แก้ไข

                setFormDataActor();

                break;

            case 2: // ผู้อนุมติ สร้าง

                setFormDataActor();

                break;           
        }

    }

    #endregion

    #region bind Actor

    protected void setFormDataActor()
    {

        FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
        HiddenField hf_m0_node_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_node_idx");
        HiddenField hf_m0_actor_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_actor_idx");
        HiddenField hf_doc_decision = (HiddenField)FvViewDetail.FindControl("hf_doc_decision");
    
        m0_node = int.Parse(hf_m0_node_idx.Value);
        m0_actor = int.Parse(hf_m0_actor_idx.Value);
        m0_status = int.Parse(hf_doc_decision.Value);

        ViewState["m0_node_idx"] = m0_node;

       
        switch (m0_actor)
        {
            case 1:

                //if ((emp_idx != 0 && emp_idx != 174) || (emp_idx != 0 && emp_idx != 1394))
                //{


                    div_btn.Visible = true;
                    FvViewDetail.ChangeMode(FormViewMode.ReadOnly);
                    FvViewDetail.DataBind();

                //}

                break;

            case 2:

                //if(ViewState["emp_idx_empwhere"].ToString() != "0" || ViewState["emp_idx_empwhere"].ToString() != null) ViewState["emp_permission_check"]
                //{
                    if ((ViewState["emp_permission_check"].ToString() == ViewState["emp_idx_empwhere"].ToString()) || ViewState["emp_idx_empwhere"].ToString() == "1394")
                    {
                        if ((m0_actor == 2 && m0_node == 2))
                        {
                            div_btn.Visible = false;

                            div_approvstatecreate.Visible = true;
                            div_approve.Visible = true;
                            btnsaveapprove.Visible = true;

                            //litDebug.Text = ViewState["emp_permission_check"].ToString();

                        }
                    }
                    else
                    {
                        div_btn.Visible = true;

                        div_approvstatecreate.Visible = false;
                        div_approve.Visible = false;
                        btnsaveapprove.Visible = false;
                        //litDebug.Text = ViewState["emp_permission_check"].ToString();
                        //litDebug.Text = ViewState["emp_permission"].ToString();
                    }
                    //}
                    //else
                    //{
                    //    litDebug.Text = "11222";
                   // }
                
              
                break;               
        }
    }

    #endregion

    #region FvDetail_DataBound
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "FvInsert":

                break;

            case "FvViewDetail":


                TextBox txtemp_idxview = (TextBox)FvViewDetail.FindControl("txtemp_idxview");

                if (FvViewDetail.CurrentMode == FormViewMode.ReadOnly)
                {





                }

                break;
        }

    }

    #endregion

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
            Label txtstatus_deviecsview = (Label)FvViewDetail.FindControl("txtstatus_deviecsview");

            return txtstatus_deviecsview.Text = "Online";
            //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
            Label txtstatus_deviecsview = (Label)FvViewDetail.FindControl("txtstatus_deviecsview");

            return txtstatus_deviecsview.Text = "Offline";
            //return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string getStatus_u0_use(int status_u0_use)
    {
        if (status_u0_use.ToString().Trim() != "")
        {         
            return status_u0_use.ToString();
            //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {                    
            return "0";
            //return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string getRoomView(string roomname_view)
    {
        if (roomname_view != null)
        {

            FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
            Label txtroom_nameview = (Label)FvViewDetail.FindControl("txtroom_nameview");

            return txtroom_nameview.Text = roomname_view;
            //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
            Label txtroom_nameview = (Label)FvViewDetail.FindControl("txtroom_nameview");

            return txtroom_nameview.Text = "-";
            //return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string returnfile(string filenameregister)
    {
        string getPath = ConfigurationManager.AppSettings["upload_networkdevices_file"];
        string path = getPath + filenameregister.ToString() + "/" + filenameregister.ToString() + ".jpg";
        return path;

        //if(pat == )
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {

            case "GVShowdetailLicense":
           
                break;

            case "GVInsertSoftwareLicense":
            
                GVInsertSoftwareLicense.PageIndex = e.NewPageIndex;
                GVInsertSoftwareLicense.DataSource = ViewState["vsBuyequipment"];
                GVInsertSoftwareLicense.DataBind();

                break;

            case "GvMaster":

                setGridData(GvMaster, ViewState["bind_data_index"]);

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                //actionIndex();

                //window.scrollTo(0, 0);

                break;

            case "GvReport":

                setGridData(GvReport, ViewState["bind_data_report"]);

                GvReport.PageIndex = e.NewPageIndex;
                GvReport.DataBind();

                //actionIndex();

                //window.scrollTo(0, 0);

                break;

        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {

            case "GVInsertSoftwareLicense":               
                if (e.Row.RowState.ToString().Contains("Edit"))
                {

                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    #region edit dropdown ddlsoftware_idxupdate
                    var ddlsoftware_idxupdate = (DropDownList)e.Row.FindControl("ddlsoftware_idxupdate");

                    ddlsoftware_idxupdate.AppendDataBoundItems = true;
                    ddlsoftware_idxupdate.Items.Add(new ListItem("กรุณาเลือก Software", "00"));

                    TextBox ddlsoftware_idx = ((TextBox)e.Row.Cells[0].FindControl("ddlsoftware_idx"));
                   // ViewState["ddlsoftware_idx"] = ddlsoftware_idx.ToString();
                    //ddeducation_edit.Items.Clear();

                    data_softwarelicense _data_softwarelicense_editsw = new data_softwarelicense();
                    _data_softwarelicense_editsw.bind_m0software_list = new bind_m0software_detail[1];
                    bind_m0software_detail _bind_m0software_detail = new bind_m0software_detail();

                    //_permissionDetail.OrgIDX = 0;

                    _data_softwarelicense_editsw.bind_m0software_list[0] = _bind_m0software_detail;


                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
                    _data_softwarelicense_editsw = callServiceSoftwareLicense(_urlGetddlSoftware, _data_softwarelicense_editsw);

                    ddlsoftware_idxupdate.DataSource = _data_softwarelicense_editsw.bind_m0software_list;
                    ddlsoftware_idxupdate.DataTextField = "software_name";
                    ddlsoftware_idxupdate.DataValueField = "software_name_idx";
                    ddlsoftware_idxupdate.DataBind();
                    ddlsoftware_idxupdate.SelectedValue = ddlsoftware_idx.Text;

                    #endregion

                }

                break;

            case "GvMaster":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {


                    Label lbu0_idx = (Label)e.Row.Cells[0].FindControl("lbu0_idx");
                    Repeater rpcountogdept = (Repeater)e.Row.Cells[3].FindControl("rpcountogdept");

                    Label lbemp_idx = (Label)e.Row.Cells[1].FindControl("lbemp_idx");
                    LinkButton btnViewindex = (LinkButton)e.Row.Cells[6].FindControl("btnViewindex");
                    Label StatusDesc = (Label)e.Row.Cells[5].FindControl("StatusDesc");
                    Label status_emp_computer = (Label)e.Row.Cells[6].FindControl("status_emp_computer");

                    data_softwarelicense_devices _data_softwarelicense_devices_countdept = new data_softwarelicense_devices();
                    _data_softwarelicense_devices_countdept.bind_softwarelicense_list = new bind_softwarelicense_detail[1];

                    bind_softwarelicense_detail _bind_softwarelicense_detail = new bind_softwarelicense_detail();

                    _bind_softwarelicense_detail.u0_software_idx = int.Parse(lbu0_idx.Text);

                    _data_softwarelicense_devices_countdept.bind_softwarelicense_list[0] = _bind_softwarelicense_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devices_countdept));

                    _data_softwarelicense_devices_countdept = callServiceSoftwareLicenseDevices(_urlGetCountLicenseDetail, _data_softwarelicense_devices_countdept);

                    rpcountogdept.DataSource = _data_softwarelicense_devices_countdept.bind_softwarelicense_list;
                    rpcountogdept.DataBind();

                    // ปุ่ม View
                    if(lbemp_idx.Text == "0")
                    {
                        btnViewindex.Visible = true;
                        StatusDesc.Visible = false;

                    }
                    else
                    {
                        btnViewindex.Visible = true;
                        StatusDesc.Visible = true;
                    }
              
                    

                }

                break;

            case "GvReport":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }



                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    
                    Label lbsoftware_name_idxReport = (Label)e.Row.Cells[1].FindControl("lbsoftware_name_idxReport");
                    Repeater rpcountalllicense = (Repeater)e.Row.Cells[4].FindControl("rpcountalllicense");
                    Repeater rpcountuselicense = (Repeater)e.Row.Cells[5].FindControl("rpcountuselicense");
                    Repeater rpcountuseoklicense = (Repeater)e.Row.Cells[6].FindControl("rpcountuseoklicense");
                    Repeater rpcountghostlicense = (Repeater)e.Row.Cells[7].FindControl("rpcountghostlicense");

                    ////////////////// จำนวน License ทั้งหมด /////////////
                    data_softwarelicense _data_softwarelicense_countall = new data_softwarelicense();
                    _data_softwarelicense_countall.bindu0_device_list = new bindu0_device_detail[1];

                    bindu0_device_detail _bindu0_device_detailall = new bindu0_device_detail();

                    _bindu0_device_detailall.software_name_idx = int.Parse(lbsoftware_name_idxReport.Text);

                    _data_softwarelicense_countall.bindu0_device_list[0] = _bindu0_device_detailall;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_countuse));

                    _data_softwarelicense_countall = callServiceSoftwareLicense(_urlGetCountAllLicense, _data_softwarelicense_countall);

                    rpcountalllicense.DataSource = _data_softwarelicense_countall.bindu0_device_list;
                    rpcountalllicense.DataBind();


                    ////////////////// จำนวน License ที่ถูกใช้ไป /////////////
                    data_softwarelicense _data_softwarelicense_countuse1 = new data_softwarelicense();
                    _data_softwarelicense_countuse1.bindu0_device_list = new bindu0_device_detail[1];

                    bindu0_device_detail _bindu0_device_detailuse = new bindu0_device_detail();

                    _bindu0_device_detailuse.software_name_idx = int.Parse(lbsoftware_name_idxReport.Text);

                    _data_softwarelicense_countuse1.bindu0_device_list[0] = _bindu0_device_detailuse;


                    _data_softwarelicense_countuse1 = callServiceSoftwareLicense(_urlGetCountUse, _data_softwarelicense_countuse1);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_countuse1));


                    rpcountuselicense.DataSource = _data_softwarelicense_countuse1.bindu0_device_list;
                    rpcountuselicense.DataBind();


                    ////////////////// จำนวน License ถูกลิขสิทธิ์  /////////////
                    data_softwarelicense _data_softwarelicense_countuseok = new data_softwarelicense();
                    _data_softwarelicense_countuseok.bindu0_device_list = new bindu0_device_detail[1];

                    bindu0_device_detail _bindu0_device_detailuseok = new bindu0_device_detail();

                    _bindu0_device_detailuseok.software_name_idx = int.Parse(lbsoftware_name_idxReport.Text);

                    _data_softwarelicense_countuseok.bindu0_device_list[0] = _bindu0_device_detailuseok;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_countuse));

                    _data_softwarelicense_countuseok = callServiceSoftwareLicense(_urlGetCountLicenseOk, _data_softwarelicense_countuseok);

                    rpcountuseoklicense.DataSource = _data_softwarelicense_countuseok.bindu0_device_list;
                    rpcountuseoklicense.DataBind();

                    ////////////////// จำนวน License ผี  /////////////
                    data_softwarelicense _data_softwarelicense_countghost = new data_softwarelicense();
                    _data_softwarelicense_countghost.bindu0_device_list = new bindu0_device_detail[1];

                    bindu0_device_detail _bindu0_device_detailghost = new bindu0_device_detail();

                    _bindu0_device_detailghost.software_name_idx = int.Parse(lbsoftware_name_idxReport.Text);

                    _data_softwarelicense_countghost.bindu0_device_list[0] = _bindu0_device_detailghost;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_countuse));

                    _data_softwarelicense_countghost = callServiceSoftwareLicense(_urlGetCountGhost, _data_softwarelicense_countghost);

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_countghost.bindu0_device_list));

                    rpcountghostlicense.DataSource = _data_softwarelicense_countghost.bindu0_device_list;
                    rpcountghostlicense.DataBind();




                }

                break;


        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GVInsertSoftwareLicense":            
                GVInsertSoftwareLicense.EditIndex = e.NewEditIndex;
                GVInsertSoftwareLicense.DataSource = ViewState["vsBuyequipment"];
                GVInsertSoftwareLicense.DataBind();

                break;

            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                //actionIndex();
                break;

                
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GVInsertSoftwareLicense":
                GVInsertSoftwareLicense.EditIndex = -1;
                GVInsertSoftwareLicense.DataSource = ViewState["vsBuyequipment"];
                GVInsertSoftwareLicense.DataBind();


                break;

            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
                
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {

            case "GVInsertSoftwareLicense":

                //////ค่าที่เพิ่มใน dataset
               
                var ddlsoftware_idxupdate = (DropDownList)GVInsertSoftwareLicense.Rows[e.RowIndex].FindControl("ddlsoftware_idxupdate");
                var ddlsoftware_idx = (TextBox)GVInsertSoftwareLicense.Rows[e.RowIndex].FindControl("ddlsoftware_idx");
                var txttypename_update = (TextBox)GVInsertSoftwareLicense.Rows[e.RowIndex].FindControl("txttypename_update");
                var texttypeidx_update = (TextBox)GVInsertSoftwareLicense.Rows[e.RowIndex].FindControl("texttypeidx_update");

                //var tboccupationparent1_edit = (TextBox)GvReportAddparent.Rows[e.RowIndex].FindControl("tboccupationparent1_edit");

                var dsBuyequipmentUpdate = (DataSet)ViewState["vsBuyequipment"];
                var drBuyequipment = dsBuyequipmentUpdate.Tables[0].Rows;

                drBuyequipment[e.RowIndex]["swlicense_name_dataset"] = ddlsoftware_idxupdate.SelectedItem;
                drBuyequipment[e.RowIndex]["swlicense_idx_dataset"] = ddlsoftware_idxupdate.SelectedValue;
                drBuyequipment[e.RowIndex]["type_name_dataset"] = txttypename_update.Text;
                drBuyequipment[e.RowIndex]["type_idx_dataset"] = texttypeidx_update.Text;
                

                ViewState["vsBuyequipment"] = dsBuyequipmentUpdate;
                //ViewState["vsBuyequipment1"] = dsBuyequipmentUpdate;

                GVInsertSoftwareLicense.EditIndex = -1;
                GVInsertSoftwareLicense.DataSource = ViewState["vsBuyequipment"];
                GVInsertSoftwareLicense.DataBind();

                break;

            case "GVShowdetailLicense":
              
                break;              
        }
    }

    protected void Master_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GVShowdetailLicense":

              
                break;

            case "GVInsertSoftwareLicense":

             
                var dsvsBuyequipmentDelete = (DataSet)ViewState["vsBuyequipment"];
                var drDriving = dsvsBuyequipmentDelete.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["vsBuyequipment"] = dsvsBuyequipmentDelete;
                GVInsertSoftwareLicense.EditIndex = -1;
                GVInsertSoftwareLicense.DataSource = ViewState["vsBuyequipment"];
                GVInsertSoftwareLicense.DataBind();

                break;
        }

    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch (fvName.ID)
        {

            case "FvDetailShowCut":
                //setFormData(FvDetailShowCut, FormViewMode.Insert, _data_networkdetailcut.bindcutnetwork_list, 0);

                break;

        }
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region Checkbox เชคแสดง
    protected void CheckboxChanged(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        switch (cb.ID)
        {

            case "chk_license":

               
                break;              
        }
    }
    #endregion

    protected void rptRecords_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            
        }


    }


    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();
     
    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

    }

    protected void setVisible()
    {
       
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected string getStatusemp(int status_emp_computer)
    {
                
        if (status_emp_computer != 0)
        {
           
            return "<span class='statusmaster-online' data-toggle='tooltip' title='มีผู้ถือครอง'><i class='glyphicon glyphicon-user'></i></span>";
        }
        else
        {
            return string.Empty;
        }
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_softwarelicense callServiceSoftwareLicense(string _cmdUrl, data_softwarelicense _data_softwarelicense)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense);
        // litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_softwarelicense = (data_softwarelicense)_funcTool.convertJsonToObject(typeof(data_softwarelicense), _localJson);

        return _data_softwarelicense;
    }

    protected data_softwarelicense_devices callServiceSoftwareLicenseDevices(string _cmdUrl, data_softwarelicense_devices _data_softwarelicense_devices)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense_devices);
        // litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_softwarelicense_devices = (data_softwarelicense_devices)_funcTool.convertJsonToObject(typeof(data_softwarelicense_devices), _localJson);

        return _data_softwarelicense_devices;
    }


    protected data_employee callServiceEmpProfile(string _cmdUrl)

    {
        //// convert to json
        // _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _data_employee;
    }

    #endregion reuse



}