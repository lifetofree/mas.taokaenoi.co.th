﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_health_check : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();
    data_employee _dataEmployee = new data_employee();
    data_hr_healthcheck _data_hr_healthcheck = new data_hr_healthcheck();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"]; 
    static string _urlGetViewEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewEmployeeList"];
    //-- employee --//

    //-- HR HealthCheck --//
    static string _urlGetEmployeeHistory = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployeeHistory"];
    static string _urlGetHistoryType = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryType"];
    static string _urlGetFormTypeHistoryCheck = _serviceUrl + ConfigurationManager.AppSettings["urlGetFormTypeHistoryCheck"];
    static string _urlGetDetailTopicForm = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailTopicForm"];

    static string _urlGetEmployeeProfileHistory = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployeeProfileHistory"];
    static string _urlGetDetailSetTopicForm = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailSetTopicForm"];
    static string _urlGetDetailTypeHealthCheck = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailTypeHealthCheck"];

    static string _urlSetHistoryWorkCheck = _serviceUrl + ConfigurationManager.AppSettings["urlSetHistoryWorkCheck"];
    static string _urlSetHistoryInjuryCheck = _serviceUrl + ConfigurationManager.AppSettings["urlSetHistoryInjuryCheck"];
    static string _urlGetNameDoctorForm = _serviceUrl + ConfigurationManager.AppSettings["urlGetNameDoctorForm"];
    static string _urlGetDetailDoctorInForm = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailDoctorInForm"];
    static string _urlGetHistoryWorkEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryWorkEmployee"];
    static string _urlGetHistoryInjuryEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryInjuryEmployee"];
    static string _urlSetHistoryHealthCheck = _serviceUrl + ConfigurationManager.AppSettings["urlSetHistoryHealthCheck"];
    static string _urlSetHistorySickCheck = _serviceUrl + ConfigurationManager.AppSettings["urlSetHistorySickCheck"];
    static string _urlGetHistoryHealthEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryHealthEmployee"];
    static string _urlGetHistorySickEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistorySickEmployee"];
    static string _urlGetHistorySickEverSick = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistorySickEverSick"];
    static string _urlGetHistorySickRelationFamily = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistorySickRelationFamily"];
    static string _urlGetHistoryHealthDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryHealthDetail"];
    static string _urlGetHistoryHealthRikeDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryHealthRikeDetail"];
    static string _urlSetDeleteHistoryWork = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteHistoryWork"];
    static string _urlSetEditHistoryWork = _serviceUrl + ConfigurationManager.AppSettings["urlSetEditHistoryWork"];
    static string _urlSetUpdateHistoryWork = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateHistoryWork"];
    static string _urlSetEditHistorySick = _serviceUrl + ConfigurationManager.AppSettings["urlSetEditHistorySick"];
    static string _urlSetDeleteHistoryHealth = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteHistoryHealth"];
    static string _urlSetEditHistoryInjury = _serviceUrl + ConfigurationManager.AppSettings["urlSetEditHistoryInjury"];
    static string _urlSetUpdateHistoryInjury = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateHistoryInjury"];
    static string _urlSetDeleteHistoryInjury = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteHistoryInjury"];
    static string _urlGetHistoryHealthPrint = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryHealthPrint"];


    //-- HR HealthCheck --//


    int _emp_idx = 0;
    int _default_int = 0;
    int _r1Regis = 0;
    int ExternalID = 2;
    int ExternalCondition = 6;

    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["jobgrade_permission"] = _dataEmployee.employee_list[0].jobgrade_level;

        //litDebug.Text = ViewState["rpos_permission"].ToString();

    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //setActiveTab("docDetailList", 0, 0, 0, "5", 0);
            initPage();
            getHistoryWorkList();
            getHistoryInjuryList();
            getDetailRikeList();
            getDetailEverSickList();
            getDetailDiseaseFamilyList();

            getDetailEverSickListUpdate();
            getDetailDiseaseFamilyListUpdate();
            getDetailRikeListUpdate();

            //litDebug.Text = "444444";

        }

    }

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, "0", 0, 0, "0");

        switch(cmdName)
        {
            case "cmdHistoryList":
                setActiveTab("docHistory", 0, 0, 0, "0", 0, _emp_idx, "docProfile");
                break;
        }

    }

    protected void navProfileCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

      //  setActiveTabProfile("docHistory", 0, 0, 0, "0", 0, 0 , cmdArg);

        switch (cmdName)
        {

            case "cmdProfile":
                setActiveTab("docHistory", 0, 0, 0, "0", 0, _emp_idx, cmdArg);
                break;

            case "cmdHistoryWork":
                setActiveTab("docHistory", 0, 0, 0, "0", 0, _emp_idx, cmdArg);
                break;

            case "cmdHistorySickness":
                setActiveTab("docHistory", 0, 0, 0, "0", 0, _emp_idx, cmdArg);
                break;

            case "cmdHealthCheck":
                setActiveTab("docHistory", 0, 0, 0, "0", 0, _emp_idx, cmdArg);
                break;

            case "cmdInjury":
                setActiveTab("docHistory", 0, 0, 0, "0", 0, _emp_idx, cmdArg);
                break;

        }

    }
    
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdDocdetailList":

                string idx_DocHistory = cmdArg;

                switch (idx_DocHistory)
                {
                    case "1": //History Employee
                        setActiveTab("docDetailList", 0, 0, 0, idx_DocHistory, 0, 0, "0");
                        break;
                    case "2": // History Work
                        setActiveTab("docDetailList", 0, 0, 0, idx_DocHistory, 0, 0, "0");
                        break;
                    case "3": // History Sick
                        setActiveTab("docDetailList", 0, 0, 0, idx_DocHistory, 0, 0, "0");
                        break;
                    case "4": // History HealthCheck
                        setActiveTab("docDetailList", 0, 0, 0, idx_DocHistory, 0, 0, "0");
                        break;
                    case "5"://History Employee
                        setActiveTab("docDetailList", 0, 0, 0, idx_DocHistory, 0, 0, "0");
                        break;

                }

                break;
            case "cmdViewHistoryEmployee":

                string[] history_emp = new string[2];
                history_emp = e.CommandArgument.ToString().Split(';');
                int emp_idx_history = int.Parse(history_emp[0]);
                string historyemp_idx = history_emp[1];

                setActiveTab("docDetailList", 0, 0, 0, historyemp_idx, 0, emp_idx_history, "0");

                txt_emp_idx_historyhealth.Text = emp_idx_history.ToString();
                txt_historyhealth_idx.Text = historyemp_idx.ToString();

                break;
            case "cmdCancelToHistoryDetail":

                if (ViewState["EmpSearchList_index"] != null)
                {
                    setActiveTab("docDetailList", 0, 0, 0, "0", 0, 0, "0");
                    gvEmployeeDetail.PageIndex = 0;
                    setGridData(gvEmployeeDetail, ViewState["EmpSearchList_index"]);

                }
                else
                {
                    setActiveTab("docDetailList", 0, 0, 0, "0", 0, 0, "0");
                    setGridData(gvEmployeeDetail, ViewState["vs_Employee_Detail"]);
                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลพนักงานดังกล่าว');", true);
                    //setGridData(gvEmployeeDetail, ViewState["EmpSearchList_index"]);
                }

                break;

            case "cmdResetSearchHistoryEmployee":

                ViewState["EmpSearchList_index"] = null;

                TextBox tbEmpCode_HistorySearch = (TextBox)fvSearchHistoryEmployee.FindControl("tbEmpCode_HistorySearch");
                TextBox tbEmpName_HistorySearch = (TextBox)fvSearchHistoryEmployee.FindControl("tbEmpName_HistorySearch");
                tbEmpCode_HistorySearch.Text = String.Empty;
                tbEmpName_HistorySearch.Text = String.Empty;

                setActiveTab("docDetailList", 0, 0, 0, "0", 0, 0, "0");
                break;

            case "cmdViewHistoryWork":

                string[] history_work = new string[2];
                history_work = e.CommandArgument.ToString().Split(';');
                int emp_idx_historywork = int.Parse(history_work[0]);
                string historywork_idx = history_work[1];

                setActiveTab("docDetailList", 0, 0, 0, historywork_idx, 0, emp_idx_historywork, "0");

                txt_emp_idx_historyhealth.Text = emp_idx_historywork.ToString();
                txt_historyhealth_idx.Text = historywork_idx.ToString();

                gvPrintWork.AllowPaging = false;
                getHistoryWork(emp_idx_historywork, gvPrintWork);

                break;
            case "cmdViewHistorySick":

                string[] history_sick = new string[2];
                history_sick = e.CommandArgument.ToString().Split(';');
                int emp_idx_historysick = int.Parse(history_sick[0]);
                string historysick_idx = history_sick[1];

                setActiveTab("docDetailList", 0, 0, 0, historysick_idx, 0, emp_idx_historysick, "0");

                txt_emp_idx_historyhealth.Text = emp_idx_historysick.ToString();
                txt_historyhealth_idx.Text = historysick_idx.ToString();

                break;
            case "cmdViewHistoryHealth":

                string[] history_healyh = new string[2];
                history_healyh = e.CommandArgument.ToString().Split(';');
                int emp_idx_historyhealth = int.Parse(history_healyh[0]);
                string historyhealth_idx = history_healyh[1];

                setActiveTab("docDetailList", 0, 0, 0, historyhealth_idx, 0, emp_idx_historyhealth, "0");

                txt_emp_idx_historyhealth.Text = emp_idx_historyhealth.ToString();
                txt_historyhealth_idx.Text = historyhealth_idx.ToString();

                break;
            case "cmdViewHistoryInjury":

                string[] history_injury = new string[2];
                history_injury = e.CommandArgument.ToString().Split(';');
                int emp_idx_historyinjury = int.Parse(history_injury[0]);
                string historyinjury_idx = history_injury[1];

                setActiveTab("docDetailList", 0, 0, 0, historyinjury_idx, 0, emp_idx_historyinjury, "0");

                txt_emp_idx_historyhealth.Text = emp_idx_historyinjury.ToString();
                txt_historyhealth_idx.Text = historyinjury_idx.ToString();

                break;

            case "cmdNameForm":

                if (cmdArg != "0")
                {

                    setActiveTab("docCreateList", 0, 0, 0, "0", int.Parse(cmdArg), 0, "0");
                    //ConfigureActiveButton(int.Parse(cmdArg));
                    //CallJavaScript(cmdName);
                }
                break;

            case "cmdAddHistoryWorkList":

                setHistoryWorkList();
                break;
            case "cmdInsertHistoryWork":

                TextBox txtEmpIDXHidden_detail = (TextBox)fvDetailEmployeeHistory.FindControl("txtEmpIDXHidden_detail");
               // TextBox txtEmpNewEmpCode_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewEmpCode_Add");

                data_hr_healthcheck _data_u0_history_work = new data_hr_healthcheck();

                _data_u0_history_work.healthcheck_u0_history_work_list = new hr_healthcheck_u0_history_work_detail[1];
                hr_healthcheck_u0_history_work_detail _u0_history_work = new hr_healthcheck_u0_history_work_detail();

                _u0_history_work.cemp_idx = _emp_idx;
                _u0_history_work.emp_idx = int.Parse(txtEmpIDXHidden_detail.Text);
                _u0_history_work.u0_historywork_status = 1;
                _u0_history_work.m0_type_idx = int.Parse(ViewState["vs_TypeFormHistory"].ToString());

                //vs dataset detail u2_history work check
                var _dataset_HistoryWorkList = (DataSet)ViewState["vsHistoryWorkList"];

                var _add_HistoryWorkList = new hr_healthcheck_u2_history_work_detail[_dataset_HistoryWorkList.Tables[0].Rows.Count];
                int row_worklist = 0;

                foreach (DataRow dtrow_HistoryWorkList in _dataset_HistoryWorkList.Tables[0].Rows)
                {
                    _add_HistoryWorkList[row_worklist] = new hr_healthcheck_u2_history_work_detail();

                    _add_HistoryWorkList[row_worklist].company_name = dtrow_HistoryWorkList["drbusiness_workText"].ToString();
                    _add_HistoryWorkList[row_worklist].department_name = dtrow_HistoryWorkList["drDepartment_workText"].ToString();
                    _add_HistoryWorkList[row_worklist].business_type = dtrow_HistoryWorkList["drbusiness_typeworkText"].ToString();
                    _add_HistoryWorkList[row_worklist].job_description = dtrow_HistoryWorkList["drjobdescription_workText"].ToString();
                    _add_HistoryWorkList[row_worklist].job_startdate = dtrow_HistoryWorkList["drStartDate_workText"].ToString();
                    _add_HistoryWorkList[row_worklist].job_enddate = dtrow_HistoryWorkList["drEndDate_workText"].ToString();
                    _add_HistoryWorkList[row_worklist].risk_health = dtrow_HistoryWorkList["drHealthRisk_workText"].ToString();
                    _add_HistoryWorkList[row_worklist].protection_equipment = dtrow_HistoryWorkList["drUseMaterial_workText"].ToString();
                    _add_HistoryWorkList[row_worklist].cemp_idx = _emp_idx;

                    row_worklist++;

                }

                _data_u0_history_work.healthcheck_u0_history_work_list[0] = _u0_history_work;
                _data_u0_history_work.healthcheck_u2_history_work_list = _add_HistoryWorkList;


                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_u0_history_work));
                _data_u0_history_work = callServicePostHRHealthCheck(_urlSetHistoryWorkCheck, _data_u0_history_work);
                if (_data_u0_history_work.return_code == 0)
                {
                    setActiveTab("docCreateList", 0, 0, 0, "0", 0, 0, "0");

                    fvDetailEmployeeHistory.Visible = true;
                    setFormData(fvDetailEmployeeHistory, FormViewMode.ReadOnly, ViewState["vs_SearchEmployee_List"]);
                    div_showname_Form.Visible = true;
                    Update_BackToSearch.Visible = true;

                    div_SearchEmployee.Visible = false;

                    clearViewState();
                    Update_DetailEmployee.Visible = false;
                    gvEmployeeList.PageIndex = 0;
                    setGridData(gvEmployeeList, null);
                    fvDetailEmployeeHistory.Focus();

                }

                break;
            case "cmdAddHistoryInjuryList":

                setHistoryInjuryList();
                break;
            case "cmdInsertHistoryInjury":
                //ViewState["vs_TypeFormHistory"] = type_form;
                TextBox txtEmpIDXHidden_Add_injury = (TextBox)fvDetailEmployeeHistory.FindControl("txtEmpIDXHidden_detail");
                //TextBox txtEmpNewEmpCode_Add_injury = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewEmpCode_Add");

                data_hr_healthcheck _data_u0_history_injury = new data_hr_healthcheck();

                _data_u0_history_injury.healthcheck_u0_history_injury_list = new hr_healthcheck_u0_history_injury_detail[1];
                hr_healthcheck_u0_history_injury_detail _u0_history_injury = new hr_healthcheck_u0_history_injury_detail();

                _u0_history_injury.cemp_idx = _emp_idx;
                _u0_history_injury.emp_idx = int.Parse(txtEmpIDXHidden_Add_injury.Text);
                _u0_history_injury.u0_historyinjury_status = 1;
                _u0_history_injury.m0_type_idx = int.Parse(ViewState["vs_TypeFormHistory"].ToString());

                //vs dataset detail u2_history work check
                var _dataset_HistoryInjuryList = (DataSet)ViewState["vsHistoryInjuryList"];

                var _add_HistoryInjuryList = new hr_healthcheck_u2_history_injury_detail[_dataset_HistoryInjuryList.Tables[0].Rows.Count];
                int row_injurylist = 0;

                foreach (DataRow dtrow_HistoryInjuryList in _dataset_HistoryInjuryList.Tables[0].Rows)
                {
                    _add_HistoryInjuryList[row_injurylist] = new hr_healthcheck_u2_history_injury_detail();

                    _add_HistoryInjuryList[row_injurylist].date_injuly = dtrow_HistoryInjuryList["drdate_injuryText"].ToString();
                    _add_HistoryInjuryList[row_injurylist].injury_detail = dtrow_HistoryInjuryList["drinjurydetailText"].ToString();
                    _add_HistoryInjuryList[row_injurylist].cause_Injury = dtrow_HistoryInjuryList["drcause_InjuryText"].ToString();
                    _add_HistoryInjuryList[row_injurylist].disability = dtrow_HistoryInjuryList["drDisabilityText"].ToString();
                    _add_HistoryInjuryList[row_injurylist].lost_organ = dtrow_HistoryInjuryList["drLostOrganText"].ToString();
                    _add_HistoryInjuryList[row_injurylist].not_working = int.Parse(dtrow_HistoryInjuryList["drnotworkingIDX"].ToString());
                    _add_HistoryInjuryList[row_injurylist].cemp_idx = _emp_idx;

                    row_injurylist++;

                }

                _data_u0_history_injury.healthcheck_u0_history_injury_list[0] = _u0_history_injury;
                _data_u0_history_injury.healthcheck_u2_history_injury_list = _add_HistoryInjuryList;


                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_u0_history_injury));
                _data_u0_history_injury = callServicePostHRHealthCheck(_urlSetHistoryInjuryCheck, _data_u0_history_injury);
                if (_data_u0_history_injury.return_code == 0)
                {
                    setActiveTab("docCreateList", 0, 0, 0, "0", 0, 0, "0");

                    fvDetailEmployeeHistory.Visible = true;
                    setFormData(fvDetailEmployeeHistory, FormViewMode.ReadOnly, ViewState["vs_SearchEmployee_List"]);
                    div_showname_Form.Visible = true;
                    Update_BackToSearch.Visible = true;

                    div_SearchEmployee.Visible = false;

                    clearViewState();
                    Update_DetailEmployee.Visible = false;
                    gvEmployeeList.PageIndex = 0;
                    setGridData(gvEmployeeList, null);
                    fvDetailEmployeeHistory.Focus();
                    //if (txtEmpNewEmpCode_Add_injury.Text != "")
                    //{
                    //    getDetailHistoryCheck(txtEmpNewEmpCode_Add_injury.Text);
                    //    fvAddUserCheckHistory.Focus();
                    //}
                }

                break;
            case "cmdAddRikeTolist":

                setDetailRikeList();
                break;

            case "cmdSearch":

                //GridView gvEmployeeList = (GridView)fvEmpSearch.FindControl("gvEmployeeList");

                data_employee _dataEmployee_search = new data_employee();
                _dataEmployee_search.search_key_emp_list = new search_key_employee[1];
                search_key_employee _search_key = new search_key_employee();

                _search_key.s_emp_code = ((TextBox)fvEmpSearch.FindControl("tbEmpCode_Search")).Text.Trim();
                _search_key.s_emp_name = ((TextBox)fvEmpSearch.FindControl("tbEmpName_Search")).Text.Trim();
                _search_key.s_emp_status = "1";

                _dataEmployee_search.search_key_emp_list[0] = _search_key;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee_search))

                _dataEmployee_search = getViewEmployeeList(_dataEmployee_search);
                ViewState["EmpSearchList"] = _dataEmployee_search.employee_list;


                if(ViewState["EmpSearchList"] != null)
                {
                    Update_DetailEmployee.Visible = true;
                    //gvEmployeeList_scroll.Visible = true;
                    gvEmployeeList.PageIndex = 0;
                    setGridData(gvEmployeeList, ViewState["EmpSearchList"]);

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลพนักงานดังกล่าว');", true);
                    Update_DetailEmployee.Visible = false;
                    setGridData(gvEmployeeList, ViewState["EmpSearchList"]);
                }
               

                break;

            case "btnCancel":

                clearViewState();
                Update_DetailEmployee.Visible = false;
                gvEmployeeList.PageIndex = 0;
                setGridData(gvEmployeeList, null);

                setActiveTab("docCreateList", 0, 0, 0, "0", 0, 0, "0");
                setOntop.Focus();

                break;
            case "cmdReset":
                clearViewState();

                TextBox tbEmpCode_Search = (TextBox)fvEmpSearch.FindControl("tbEmpCode_Search");
                TextBox tbEmpName_Search = (TextBox)fvEmpSearch.FindControl("tbEmpName_Search");

                tbEmpCode_Search.Text = String.Empty;
                tbEmpName_Search.Text = String.Empty;

                Update_DetailEmployee.Visible = false;
                setGridData(gvEmployeeList, ViewState["EmpSearchList"]);

                break;

            case "cmdAddEmployee":

                //litDebug.Text = cmdArg.ToString();

                employee_detail[] _templist_employee_detail = (employee_detail[])ViewState["EmpSearchList"];

                var _linq_employee = (from data_emp in _templist_employee_detail
                                      where data_emp.emp_idx == int.Parse(cmdArg.ToString())
                                      select data_emp);
                         

                ViewState["vs_SearchEmployee_List"] = _linq_employee.ToList();

                fvDetailEmployeeHistory.Visible = true;
                setFormData(fvDetailEmployeeHistory, FormViewMode.ReadOnly, ViewState["vs_SearchEmployee_List"]);
                div_showname_Form.Visible = true;
                Update_BackToSearch.Visible = true;

                div_SearchEmployee.Visible = false;

                clearViewState();
                Update_DetailEmployee.Visible = false;
                gvEmployeeList.PageIndex = 0;
                setGridData(gvEmployeeList, null);
                

                break;

            case "cmdCancelToSerach":

                clearViewState();
                Update_DetailEmployee.Visible = false;
                gvEmployeeList.PageIndex = 0;
                setGridData(gvEmployeeList, null);

                setActiveTab("docCreateList", 0, 0, 0, "0", 0, 0, "0");
                setOntop.Focus();

                break;
            case "cmdInsertHealthCheck":

                //ViewState["vs_TypeFormHistory"] = type_form;
                TextBox txtEmpIDXHidden_Add_health = (TextBox)fvDetailEmployeeHistory.FindControl("txtEmpIDXHidden_detail");

                RadioButtonList rdoDetailType = (RadioButtonList)fvHealcheckForm.FindControl("rdoDetailType");
                TextBox txt_date_healthCheck = (TextBox)fvHealcheckForm.FindControl("txt_date_healthCheck");
                DropDownList ddl_Doctorname = (DropDownList)fvHealcheckForm.FindControl("ddl_Doctorname");
                TextBox txt_weight = (TextBox)fvHealcheckForm.FindControl("txt_weight");
                TextBox txt_height = (TextBox)fvHealcheckForm.FindControl("txt_height");
                TextBox txt_body_mass = (TextBox)fvHealcheckForm.FindControl("txt_body_mass");
                TextBox txt_bloodpressure = (TextBox)fvHealcheckForm.FindControl("txt_bloodpressure");
                TextBox txt_pulse = (TextBox)fvHealcheckForm.FindControl("txt_pulse");
                TextBox txt_specify_resultbody = (TextBox)fvHealcheckForm.FindControl("txt_specify_resultbody");
                TextBox txt_resultlab = (TextBox)fvHealcheckForm.FindControl("txt_resultlab");
                TextBox txt_numcheck = (TextBox)fvHealcheckForm.FindControl("txt_numcheck");


                //TextBox txtEmpNewEmpCode_Add_injury = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewEmpCode_Add");

                data_hr_healthcheck _data_u0_history_health = new data_hr_healthcheck();

                _data_u0_history_health.healthcheck_u0_history_health_list = new hr_healthcheck_u0_history_health_detail[1];
                hr_healthcheck_u0_history_health_detail _u0_history_health = new hr_healthcheck_u0_history_health_detail();

                _data_u0_history_health.healthcheck_u2_history_health_list = new hr_healthcheck_u2_history_health_detail[1];
                hr_healthcheck_u2_history_health_detail _u2_history_health = new hr_healthcheck_u2_history_health_detail();

                //u0
                _u0_history_health.cemp_idx = _emp_idx;
                _u0_history_health.emp_idx = int.Parse(txtEmpIDXHidden_Add_health.Text);
                _u0_history_health.u0_historyhealth_status = 1;
                _u0_history_health.m0_type_idx = int.Parse(ViewState["vs_TypeFormHistory"].ToString());

                //u2
                _u2_history_health.m0_detail_typecheck_idx = int.Parse(rdoDetailType.SelectedValue.ToString());
                _u2_history_health.date_check = txt_date_healthCheck.Text;
                _u2_history_health.m0_doctor_idx = int.Parse(ddl_Doctorname.SelectedValue);
                _u2_history_health.weight = txt_weight.Text;
                _u2_history_health.height = txt_height.Text;
                _u2_history_health.body_mass = txt_body_mass.Text;
                _u2_history_health.bloodpressure = txt_bloodpressure.Text;
                _u2_history_health.pulse = txt_pulse.Text;
                _u2_history_health.specify_resultbody = txt_specify_resultbody.Text;
                _u2_history_health.resultlab = txt_resultlab.Text;
                _u2_history_health.num_check = txt_numcheck.Text;

                //vs dataset detail u3_history health check
                var _dataset_HistoryHealthRikeList = (DataSet)ViewState["vsDetailRikeList"];

                var _add_HistoryHealthRikeList = new hr_healthcheck_u3_history_health_detail[_dataset_HistoryHealthRikeList.Tables[0].Rows.Count];
                int row_rikelist = 0;

                foreach (DataRow dtrow_HistoryHealthRikeList in _dataset_HistoryHealthRikeList.Tables[0].Rows)
                {
                    _add_HistoryHealthRikeList[row_rikelist] = new hr_healthcheck_u3_history_health_detail();

                    _add_HistoryHealthRikeList[row_rikelist].rike_health = dtrow_HistoryHealthRikeList["drrikeHealthText"].ToString();
                    _add_HistoryHealthRikeList[row_rikelist].result_health = dtrow_HistoryHealthRikeList["drResultHealthText"].ToString();
                    _add_HistoryHealthRikeList[row_rikelist].cemp_idx = _emp_idx;

                    row_rikelist++;

                }

                _data_u0_history_health.healthcheck_u0_history_health_list[0] = _u0_history_health;
                _data_u0_history_health.healthcheck_u2_history_health_list[0] = _u2_history_health;
                _data_u0_history_health.healthcheck_u3_history_health_list = _add_HistoryHealthRikeList;

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_u0_history_health));
                _data_u0_history_health = callServicePostHRHealthCheck(_urlSetHistoryHealthCheck, _data_u0_history_health);

                if (_data_u0_history_health.return_code == 0)
                {
                    setActiveTab("docCreateList", 0, 0, 0, "0", 0, 0, "0");

                    fvDetailEmployeeHistory.Visible = true;
                    setFormData(fvDetailEmployeeHistory, FormViewMode.ReadOnly, ViewState["vs_SearchEmployee_List"]);
                    div_showname_Form.Visible = true;
                    Update_BackToSearch.Visible = true;

                    div_SearchEmployee.Visible = false;
                    clearViewState();
                    Update_DetailEmployee.Visible = false;
                    gvEmployeeList.PageIndex = 0;
                    setGridData(gvEmployeeList, null);
                    fvDetailEmployeeHistory.Focus();

                }


                break;

            case "cmdSaveEditHealthCheck":

                //ViewState["vs_TypeFormHistory"] = type_form;
                //TextBox txtEmpIDXHidden_Add_health = (TextBox)fvDetailEmployeeHistory.FindControl("txtEmpIDXHidden_detail");
                TextBox txt_u0_historyhealth_idx_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_u0_historyhealth_idx_edit");
                TextBox txt_u2_historyhealth_idx_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_u2_historyhealth_idx_edit");

                RadioButtonList rdoDetailType_edit = (RadioButtonList)fvDetailHealthCheck.FindControl("rdoDetailType_edit");
                TextBox txt_date_healthCheck_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_date_healthCheck_edit");
                DropDownList ddl_Doctorname_edit = (DropDownList)fvDetailHealthCheck.FindControl("ddl_Doctorname_edit");
                TextBox txt_weight_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_weight_edit");
                TextBox txt_height_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_height_edit");
                TextBox txt_body_mass_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_body_mass_edit");
                TextBox txt_bloodpressure_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_bloodpressure_edit");
                TextBox txt_pulse_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_pulse_edit");
                TextBox txt_specify_resultbody_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_specify_resultbody_edit");
                TextBox txt_resultlab_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_resultlab_edit");
                TextBox txt_numcheck_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_numcheck_edit");
                //TextBox txtEmpNewEmpCode_Add_injury = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewEmpCode_Add");

                data_hr_healthcheck _data_u0_history_health_update = new data_hr_healthcheck();

                _data_u0_history_health_update.healthcheck_u0_history_health_list = new hr_healthcheck_u0_history_health_detail[1];
                hr_healthcheck_u0_history_health_detail _u0_history_health_update = new hr_healthcheck_u0_history_health_detail();

                _data_u0_history_health_update.healthcheck_u2_history_health_list = new hr_healthcheck_u2_history_health_detail[1];
                hr_healthcheck_u2_history_health_detail _u2_history_health_update = new hr_healthcheck_u2_history_health_detail();

                //u0
                _u0_history_health_update.cemp_idx = _emp_idx;
                _u0_history_health_update.u0_historyhealth_idx = int.Parse(txt_u0_historyhealth_idx_edit.Text);
                _u0_history_health_update.u2_historyhealth_idx = int.Parse(txt_u2_historyhealth_idx_edit.Text);
                _u0_history_health_update.u0_historyhealth_status = 1;
                //_u0_history_health_update.m0_type_idx = int.Parse(ViewState["vs_TypeFormHistory"].ToString());

                //u2
                _u2_history_health_update.m0_detail_typecheck_idx = int.Parse(rdoDetailType_edit.SelectedValue.ToString());
                _u2_history_health_update.date_check = txt_date_healthCheck_edit.Text;
                _u2_history_health_update.m0_doctor_idx = int.Parse(ddl_Doctorname_edit.SelectedValue);
                _u2_history_health_update.weight = txt_weight_edit.Text;
                _u2_history_health_update.height = txt_height_edit.Text;
                _u2_history_health_update.body_mass = txt_body_mass_edit.Text;
                _u2_history_health_update.bloodpressure = txt_bloodpressure_edit.Text;
                _u2_history_health_update.pulse = txt_pulse_edit.Text;
                _u2_history_health_update.specify_resultbody = txt_specify_resultbody_edit.Text;
                _u2_history_health_update.resultlab = txt_resultlab_edit.Text;
                _u2_history_health_update.num_check = txt_numcheck_edit.Text;

                //vs dataset detail u3_history health check
                var _dataset_HistoryHealthRikeList_edit = (DataSet)ViewState["vsDetailRikeListUpdate"];

                var _add_HistoryHealthRikeList_edit = new hr_healthcheck_u3_history_health_detail[_dataset_HistoryHealthRikeList_edit.Tables[0].Rows.Count];
                int row_rikelist_edit = 0;

                foreach (DataRow dtrow_HistoryHealthRikeList_edit in _dataset_HistoryHealthRikeList_edit.Tables[0].Rows)
                {
                    _add_HistoryHealthRikeList_edit[row_rikelist_edit] = new hr_healthcheck_u3_history_health_detail();

                    _add_HistoryHealthRikeList_edit[row_rikelist_edit].rike_health = dtrow_HistoryHealthRikeList_edit["drrikeHealthEditText"].ToString();
                    _add_HistoryHealthRikeList_edit[row_rikelist_edit].result_health = dtrow_HistoryHealthRikeList_edit["drResultHealthEditText"].ToString();
                    _add_HistoryHealthRikeList_edit[row_rikelist_edit].cemp_idx = _emp_idx;

                    row_rikelist_edit++;

                }

                _data_u0_history_health_update.healthcheck_u0_history_health_list[0] = _u0_history_health_update;
                _data_u0_history_health_update.healthcheck_u2_history_health_list[0] = _u2_history_health_update;
                _data_u0_history_health_update.healthcheck_u3_history_health_list = _add_HistoryHealthRikeList_edit;

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_u0_history_health_update));
                _data_u0_history_health = callServicePostHRHealthCheck(_urlSetHistoryHealthCheck, _data_u0_history_health_update);

                setActiveTab("docDetailList", 0, 0, 0, txt_historyhealth_idx.Text, 0, int.Parse(txt_emp_idx_historyhealth.Text), "0");

                //if (_data_u0_history_health.return_code == 0)
                //{
                //    setActiveTab("docCreateList", 0, 0, 0, "0", 0, 0, "0");

                //    fvDetailEmployeeHistory.Visible = true;
                //    setFormData(fvDetailEmployeeHistory, FormViewMode.ReadOnly, ViewState["vs_SearchEmployee_List"]);
                //    div_showname_Form.Visible = true;
                //    Update_BackToSearch.Visible = true;

                //    div_SearchEmployee.Visible = false;
                //    clearViewState();
                //    Update_DetailEmployee.Visible = false;
                //    gvEmployeeList.PageIndex = 0;
                //    setGridData(gvEmployeeList, null);
                //    fvDetailEmployeeHistory.Focus();

                //}


                break;

            case "cmdAddEverSickList":

                setDetailEverSickList();
                break;
            case "cmdAddDiseaseFamily":

                setDetailDiseaseFamilyList();
                break;
            case "cmdSaveSickList":

                //ViewState["vs_TypeFormHistory"] = type_form;
                TextBox txtEmpIDXHidden_Add_sick = (TextBox)fvDetailEmployeeHistory.FindControl("txtEmpIDXHidden_detail");

                TextBox txt_havedisease = (TextBox)fvSickCheckForm.FindControl("txt_havedisease");
                TextBox txt_surgery = (TextBox)fvSickCheckForm.FindControl("txt_surgery");
                TextBox txt_Immune = (TextBox)fvSickCheckForm.FindControl("txt_Immune");
                TextBox txt_DrugsEat = (TextBox)fvSickCheckForm.FindControl("txt_DrugsEat");
                TextBox txt_Allergyhistory = (TextBox)fvSickCheckForm.FindControl("txt_Allergyhistory");
                RadioButtonList rdoSmoking = (RadioButtonList)fvSickCheckForm.FindControl("rdoSmoking");
                TextBox txt_year_smoking = (TextBox)fvSickCheckForm.FindControl("txt_year_smoking");
                TextBox txt_month_smoking = (TextBox)fvSickCheckForm.FindControl("txt_month_smoking");
                TextBox txt_before_stopsmoking = (TextBox)fvSickCheckForm.FindControl("txt_before_stopsmoking");
                TextBox txt_valum_smoking = (TextBox)fvSickCheckForm.FindControl("txt_valum_smoking");

                RadioButtonList rdo_alcohol = (RadioButtonList)fvSickCheckForm.FindControl("rdo_alcohol");
                TextBox txt_year_alcohol = (TextBox)fvSickCheckForm.FindControl("txt_year_alcohol");
                TextBox txt_month_alcohol = (TextBox)fvSickCheckForm.FindControl("txt_month_alcohol");

                TextBox txt_addict_drug = (TextBox)fvSickCheckForm.FindControl("txt_addict_drug");
                TextBox txt_datahealth = (TextBox)fvSickCheckForm.FindControl("txt_datahealth");
              

                data_hr_healthcheck _data_u0_history_sick = new data_hr_healthcheck();

                _data_u0_history_sick.healthcheck_u0_history_sick_list = new hr_healthcheck_u0_history_sick_detail[1];
                hr_healthcheck_u0_history_sick_detail _u0_history_sick = new hr_healthcheck_u0_history_sick_detail();

                _data_u0_history_sick.healthcheck_u2_history_sick_list = new hr_healthcheck_u2_history_sick_detail[1];
                hr_healthcheck_u2_history_sick_detail _u2_history_sick = new hr_healthcheck_u2_history_sick_detail();

                //u0
                _u0_history_sick.cemp_idx = _emp_idx;
                _u0_history_sick.emp_idx = int.Parse(txtEmpIDXHidden_Add_sick.Text);
                _u0_history_sick.u0_historysick_status = 1;
                _u0_history_sick.m0_type_idx = int.Parse(ViewState["vs_TypeFormHistory"].ToString());

                //u2
                _u2_history_sick.havedisease = txt_havedisease.Text;
                _u2_history_sick.surgery = txt_surgery.Text;
                _u2_history_sick.immune = txt_Immune.Text;
                _u2_history_sick.drugs_eat = txt_DrugsEat.Text;
                _u2_history_sick.allergy_history = txt_Allergyhistory.Text;
                _u2_history_sick.smoking = int.Parse(rdoSmoking.SelectedValue);
                _u2_history_sick.smoking_value1 = txt_year_smoking.Text;
                _u2_history_sick.smoking_value2 = txt_month_smoking.Text;
                _u2_history_sick.smoking_value3 = txt_before_stopsmoking.Text;
                _u2_history_sick.smoking_value4 = txt_valum_smoking.Text;
                _u2_history_sick.alcohol = int.Parse(rdo_alcohol.SelectedValue);
                _u2_history_sick.alcohol_value1 = txt_year_alcohol.Text;
                _u2_history_sick.alcohol_value2 = txt_month_alcohol.Text;
                _u2_history_sick.addict_drug = txt_addict_drug.Text;
                _u2_history_sick.datahealth = txt_datahealth.Text;


                //vs dataset detail u3_history sick check
                var _dataset_HistoryEverSickList = (DataSet)ViewState["vsDetailEverSickList"];

                var _add_HistoryEverSickList = new hr_healthcheck_u3_history_sick_detail[_dataset_HistoryEverSickList.Tables[0].Rows.Count];
                int row_eversicklist = 0;

                foreach (DataRow dtrow_HistoryEversickList in _dataset_HistoryEverSickList.Tables[0].Rows)
                {
                    _add_HistoryEverSickList[row_eversicklist] = new hr_healthcheck_u3_history_sick_detail();

                    _add_HistoryEverSickList[row_eversicklist].eversick = dtrow_HistoryEversickList["drEversickText"].ToString();
                    _add_HistoryEverSickList[row_eversicklist].sick_year = dtrow_HistoryEversickList["drLastyearText"].ToString();
                    _add_HistoryEverSickList[row_eversicklist].cemp_idx = _emp_idx;

                    row_eversicklist++;

                }

                //vs dataset detail u4_history sick check
                var _dataset_HistoryDiseaseFamilyList = (DataSet)ViewState["vsDetailDiseaseFamilyList"];

                var _add_HistoryDiseaseFamilyList = new hr_healthcheck_u4_history_sick_detail[_dataset_HistoryDiseaseFamilyList.Tables[0].Rows.Count];
                int row_Diseaselist = 0;

                foreach (DataRow dtrow_HistoryDiseaseFamilyList in _dataset_HistoryDiseaseFamilyList.Tables[0].Rows)
                {
                    _add_HistoryDiseaseFamilyList[row_Diseaselist] = new hr_healthcheck_u4_history_sick_detail();

                    _add_HistoryDiseaseFamilyList[row_Diseaselist].relation_family = dtrow_HistoryDiseaseFamilyList["drRelation_familyText"].ToString();
                    _add_HistoryDiseaseFamilyList[row_Diseaselist].disease_family = dtrow_HistoryDiseaseFamilyList["drDisease_FamilyText"].ToString();
                    _add_HistoryDiseaseFamilyList[row_Diseaselist].cemp_idx = _emp_idx;

                    row_Diseaselist++;

                }

                _data_u0_history_sick.healthcheck_u0_history_sick_list[0] = _u0_history_sick;
                _data_u0_history_sick.healthcheck_u2_history_sick_list[0] = _u2_history_sick;
                _data_u0_history_sick.healthcheck_u3_history_sick_list = _add_HistoryEverSickList;
                _data_u0_history_sick.healthcheck_u4_history_sick_list = _add_HistoryDiseaseFamilyList;

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_u0_history_sick));
                _data_u0_history_sick = callServicePostHRHealthCheck(_urlSetHistorySickCheck, _data_u0_history_sick);

                if (_data_u0_history_sick.return_code == 0)
                {

                    //litDebug.Text = "8888";
                    setActiveTab("docCreateList", 0, 0, 0, "0", 0, 0, "0");

                    fvDetailEmployeeHistory.Visible = true;
                    setFormData(fvDetailEmployeeHistory, FormViewMode.ReadOnly, ViewState["vs_SearchEmployee_List"]);
                    div_showname_Form.Visible = true;
                    Update_BackToSearch.Visible = true;

                    div_SearchEmployee.Visible = false;
                    clearViewState();
                    Update_DetailEmployee.Visible = false;
                    gvEmployeeList.PageIndex = 0;
                    setGridData(gvEmployeeList, null);
                    fvDetailEmployeeHistory.Focus();

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลการเจ็บป่วยมีในระบบแล้ว!!');", true);
                }

                break;
            case "cmdSearchHistoryEmployee":

                //GridView gvEmployeeList = (GridView)fvEmpSearch.FindControl("gvEmployeeList");

                data_employee _dataEmployee_searchindex = new data_employee();
                _dataEmployee_searchindex.search_key_emp_list = new search_key_employee[1];
                search_key_employee _search_keyindex = new search_key_employee();

                _search_keyindex.s_emp_code = ((TextBox)fvSearchHistoryEmployee.FindControl("tbEmpCode_HistorySearch")).Text.Trim();
                _search_keyindex.s_emp_name = ((TextBox)fvSearchHistoryEmployee.FindControl("tbEmpName_HistorySearch")).Text.Trim();
                _search_keyindex.s_emp_status = "1";

                _dataEmployee_searchindex.search_key_emp_list[0] = _search_keyindex;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee_search))

                _dataEmployee_searchindex = getViewEmployeeList(_dataEmployee_searchindex);
                ViewState["EmpSearchList_index"] = _dataEmployee_searchindex.employee_list;


                if (ViewState["EmpSearchList_index"] != null)
                {
                     
                    gvEmployeeDetail.PageIndex = 0;
                    setGridData(gvEmployeeDetail, ViewState["EmpSearchList_index"]);

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลพนักงานดังกล่าว');", true);
                    setGridData(gvEmployeeDetail, ViewState["EmpSearchList_index"]);
                }


                break;
            case "cmdViewHealthDetailCheck":

                divHistoryHealth.Visible = false;
                Update_BackTohistoryDetail.Visible = false;

                //litDebug.Text = cmdArg;
                getDetailHistoryHealth(int.Parse(cmdArg));
                UpdatePanel_BackToListHealthDetail.Visible = true;
                div_DetailHealthCheck.Visible = true;
                div_EditHealthCheck.Visible = true;

                txt_u2_history_idx.Text = cmdArg;

                break;
            case "cmdEditHistoryHealth":
                Update_BackTohistoryDetail.Visible = false;
                UpdatePanel_BackToListHealthDetail.Visible = false;

                Update_CancelEditHistory.Visible = true;
                div_DetailHealthCheck.Visible = true;

                fvDetailHealthCheck.Visible = true;
                fvDetailHealthCheck.ChangeMode(FormViewMode.Edit);

                div_EditHealthCheck.Visible = false;
                if (ViewState["vs_u2idx_edit_historyhealth"].ToString() != "0")
                {
                    //litDebug.Text = ViewState["vs_u2_idx_historysick_edit"].ToString();
                    actionReadHistoryHealth(int.Parse(ViewState["vs_u2idx_edit_historyhealth"].ToString()));
                }
                break;

            case "cmdCancelToHistoryHealth":

                setActiveTab("docDetailList", 0, 0, 0, txt_historyhealth_idx.Text, 0, int.Parse(txt_emp_idx_historyhealth.Text), "0");
                break;

            case "cmdViewHealthCheck":
                getDetailHistoryHealthProfile(int.Parse(cmdArg));
                UpdatePanel_BackToListHealthDetail_Profile.Visible = true;

                divHistoryViewHealthProfile.Visible = true;
                divHistoryHealthProfile.Visible = false;
                break;

            case "cmdCancelToHistoryHealthProfile":
                setActiveTab("docHistory", 0, 0, 0, "0", 0, _emp_idx, "docHealthCheck");
                break;

            case "cmdPrint":
                //Session["_SESSION_EMPIDX"] = txt_emp_idx_historyhealth.Text;//_emp_idx;
                //Session["_SESSION_TYPEPRINT"] = cmdArg;
                //Session["_SESSION_U2IDX"] = txt_u2_history_idx.Text;

                //litDebug.Text = txt_emp_idx_historyhealth.Text;

                switch (cmdArg)
                {
                    case "Injury":
                        Session["_SESSION_TITLE_PRINT"] = "บันทึกเกี่ยวกับการบาดเจ็บหรือเจ็บป่วยเนื่องจากการทำงานและสาเหตุ";
                        break;
                    case "HistoryEmployee":
                        Session["_SESSION_TITLE_PRINT"] = "ประวัติส่วนตัว";
                        //litDebug.Text = Session["_SESSION_TITLE_PRINT"].ToString();
                        break;
                    case "HistoryWork":
                        //Session["_SESSION_TITLE_PRINT"] = "ประวัติการทำงาน";

                        gvWork_scroll.Visible = true;
                        //litDebug.Text = Session["_SESSION_TITLE_PRINT"].ToString();
                        break;
                    case "HistorySick":
                        Session["_SESSION_TITLE_PRINT"] = "ประวัติการเจ็บป่วย";
                        //litDebug.Text = Session["_SESSION_TITLE_PRINT"].ToString();
                        break;
                    case "HistoryHealth":
                        Session["_SESSION_TITLE_PRINT"] = "การตรวจสุขภาพ";
                        //litDebug.Text = Session["_SESSION_TITLE_PRINT"].ToString();
                        break;
                }

                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "", "window.open('health-check-print', '', '');", true);
                break;
            case "cmdDelHistoryWorkList":
                actionDeleteHistoryWork(int.Parse(cmdArg));
                break;
            case "cmdDelHistoryInjuryList":
                actionDeleteHistoryInjury(int.Parse(cmdArg));
                break;
            case "cmdDelHistoryHealth":
                actionDeleteHistoryHealth(int.Parse(cmdArg));
                break;
            case "cmdUpdateHistoryWork":

                divHistoryWork.Visible = false;
                Update_BackTohistoryDetail.Visible = false;

                div_fvEditHistoryWork.Visible = true;
                fvEditHistoryWork.Visible = true;
                Update_CancelEditHistory.Visible = true;
                fvEditHistoryWork.ChangeMode(FormViewMode.Edit);

                actionReadHistoryWork(int.Parse(cmdArg));
                setOntop.Focus();

                break;
            case "cmdUpdateHistoryInjury":

                divHistoryInjury.Visible = false;
                Update_BackTohistoryDetail.Visible = false;

                div_fvEditHistoryHealthInjury.Visible = true;
                fvEditHistoryHealthInjury.Visible = true;
                Update_CancelEditHistory.Visible = true;
                fvEditHistoryHealthInjury.ChangeMode(FormViewMode.Edit);

                actionReadHistoryInjury(int.Parse(cmdArg));
                setOntop.Focus();

                break;
            case "cmdSaveEditHistoryWork":


                TextBox txt_business_work_edit = (TextBox)fvEditHistoryWork.FindControl("txt_business_work_edit");
                TextBox txt_Department_work_edit = (TextBox)fvEditHistoryWork.FindControl("txt_Department_work_edit");
                TextBox txt_business_typework_edit = (TextBox)fvEditHistoryWork.FindControl("txt_business_typework_edit");
                TextBox txt_jobdescription_work_edit = (TextBox)fvEditHistoryWork.FindControl("txt_jobdescription_work_edit");
                TextBox txt_StartDate_work_edit = (TextBox)fvEditHistoryWork.FindControl("txt_StartDate_work_edit");
                TextBox txt_EndDate_work_edit = (TextBox)fvEditHistoryWork.FindControl("txt_EndDate_work_edit");
                TextBox txt_HealthRisk_work_edit = (TextBox)fvEditHistoryWork.FindControl("txt_HealthRisk_work_edit");
                TextBox txt_UseMaterial_work_edit = (TextBox)fvEditHistoryWork.FindControl("txt_UseMaterial_work_edit");

                data_hr_healthcheck _data_update_historywork = new data_hr_healthcheck();

                _data_update_historywork.healthcheck_u2_history_work_list = new hr_healthcheck_u2_history_work_detail[1];
                hr_healthcheck_u2_history_work_detail _update_historywork = new hr_healthcheck_u2_history_work_detail();
                _update_historywork.cemp_idx = _emp_idx;
                _update_historywork.company_name = txt_business_work_edit.Text;
                _update_historywork.department_name = txt_Department_work_edit.Text;
                _update_historywork.business_type = txt_business_typework_edit.Text;
                _update_historywork.job_description = txt_jobdescription_work_edit.Text;
                _update_historywork.job_startdate = txt_StartDate_work_edit.Text;
                _update_historywork.job_enddate = txt_EndDate_work_edit.Text;
                _update_historywork.risk_health = txt_HealthRisk_work_edit.Text;
                _update_historywork.protection_equipment = txt_UseMaterial_work_edit.Text;
                _update_historywork.u2_historywork_idx = int.Parse(cmdArg);

                _data_update_historywork.healthcheck_u2_history_work_list[0] = _update_historywork;

                _data_update_historywork = callServicePostHRHealthCheck(_urlSetUpdateHistoryWork, _data_update_historywork);

                setActiveTab("docDetailList", 0, 0, 0, txt_historyhealth_idx.Text, 0, int.Parse(txt_emp_idx_historyhealth.Text), "0");

                break;
            case "cmdSaveEditHistoryInjury":

                TextBox txt_u2_historyinjury_idx_edit = (TextBox)fvEditHistoryHealthInjury.FindControl("txt_u2_historyinjury_idx_edit");
                TextBox txt_date_injury_edit = (TextBox)fvEditHistoryHealthInjury.FindControl("txt_date_injury_edit");
                TextBox txt_injurydetail_edit = (TextBox)fvEditHistoryHealthInjury.FindControl("txt_injurydetail_edit");
                TextBox txt_cause_Injury_edit = (TextBox)fvEditHistoryHealthInjury.FindControl("txt_cause_Injury_edit");
                TextBox txt_Disability_edit = (TextBox)fvEditHistoryHealthInjury.FindControl("txt_Disability_edit");
                TextBox txt_LostOrgan_edit = (TextBox)fvEditHistoryHealthInjury.FindControl("txt_LostOrgan_edit");
                RadioButtonList rdo_notworking_edit = (RadioButtonList)fvEditHistoryHealthInjury.FindControl("rdo_notworking_edit");
               
                data_hr_healthcheck _data_update_historyinjury = new data_hr_healthcheck();

                _data_update_historyinjury.healthcheck_u2_history_injury_list = new hr_healthcheck_u2_history_injury_detail[1];
                hr_healthcheck_u2_history_injury_detail _update_history_injury = new hr_healthcheck_u2_history_injury_detail();
                _update_history_injury.cemp_idx = _emp_idx;
                _update_history_injury.u2_historyinjury_idx = int.Parse(txt_u2_historyinjury_idx_edit.Text);
                _update_history_injury.date_injuly = txt_date_injury_edit.Text;
                _update_history_injury.injury_detail = txt_injurydetail_edit.Text;
                _update_history_injury.cause_Injury = txt_cause_Injury_edit.Text;
                _update_history_injury.disability = txt_Disability_edit.Text;
                _update_history_injury.lost_organ = txt_LostOrgan_edit.Text;
                _update_history_injury.not_working = int.Parse(rdo_notworking_edit.SelectedValue.ToString());

                _data_update_historyinjury.healthcheck_u2_history_injury_list[0] = _update_history_injury;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_update_historyinjury));
                _data_update_historyinjury = callServicePostHRHealthCheck(_urlSetUpdateHistoryInjury, _data_update_historyinjury);

                setActiveTab("docDetailList", 0, 0, 0, txt_historyhealth_idx.Text, 0, int.Parse(txt_emp_idx_historyhealth.Text), "0");

                break;
            case "cmdCancelEditHistory":
                setActiveTab("docDetailList", 0, 0, 0, txt_historyhealth_idx.Text, 0, int.Parse(txt_emp_idx_historyhealth.Text), "0");
                break;

            case "cmdEditHistorySick":
                
                Update_BackTohistoryDetail.Visible = false;

                Update_CancelEditHistory.Visible = true;
                divHistorySick.Visible = true;
                
                fvSick.Visible = true;
                fvSick.ChangeMode(FormViewMode.Edit);
                
                div_btnedit_Historysick.Visible = false;
                if (ViewState["vs_u2_idx_historysick_edit"].ToString() != "0")
                {
                    //litDebug.Text = ViewState["vs_u2_idx_historysick_edit"].ToString();
                    actionReadHistorySick(int.Parse(ViewState["vs_u2_idx_historysick_edit"].ToString()));
                }
                
                break;
            case "cmdAddEditEverSickList":

                setDetailEverSickListUpdate();
                break;
            case "cmdAddDiseaseFamilyEdit":
                setDetailDiseaseFamilyListUpdate();
                break;
            case "cmdAddRikeTolistEdit":
                setDetailRikeListUpdate();
                break;
            case "cmdSaveEditSickList":

                //ViewState["vs_TypeFormHistory"] = type_form;
                TextBox txt_u0_historysick_idx_edit = (TextBox)fvSick.FindControl("txt_u0_historysick_idx_edit");
                TextBox txt_u2_historysick_idx_edit = (TextBox)fvSick.FindControl("txt_u2_historysick_idx_edit");

                TextBox txt_havedisease_edit = (TextBox)fvSick.FindControl("txt_havedisease_edit");
                TextBox txt_surgery_edit = (TextBox)fvSick.FindControl("txt_surgery_edit");
                TextBox txt_Immune_edit = (TextBox)fvSick.FindControl("txt_Immune_edit");
                TextBox txt_DrugsEat_Edit = (TextBox)fvSick.FindControl("txt_DrugsEat_Edit");
                TextBox txt_Allergyhistory_Edit = (TextBox)fvSick.FindControl("txt_Allergyhistory_Edit");
                RadioButtonList rdoSmoking_edit = (RadioButtonList)fvSick.FindControl("rdoSmoking_edit");
                TextBox txt_year_smoking_edit = (TextBox)fvSick.FindControl("txt_year_smoking_edit");
                TextBox txt_month_smoking_edit = (TextBox)fvSick.FindControl("txt_month_smoking_edit");
                TextBox txt_before_stopsmoking_edit = (TextBox)fvSick.FindControl("txt_before_stopsmoking_edit");
                TextBox txt_valum_smoking_edit = (TextBox)fvSick.FindControl("txt_valum_smoking_edit");

                RadioButtonList rdo_alcohol_edit = (RadioButtonList)fvSick.FindControl("rdo_alcohol_edit");
                TextBox txt_year_alcohol_edit = (TextBox)fvSick.FindControl("txt_year_alcohol_edit");
                TextBox txt_month_alcohol_edit = (TextBox)fvSick.FindControl("txt_month_alcohol_edit");

                TextBox txt_addict_drug_edit = (TextBox)fvSick.FindControl("txt_addict_drug_edit");
                TextBox txt_datahealth_edit = (TextBox)fvSick.FindControl("txt_datahealth_edit");


                data_hr_healthcheck _data_u0_history_sick_update = new data_hr_healthcheck();

                _data_u0_history_sick_update.healthcheck_u0_history_sick_list = new hr_healthcheck_u0_history_sick_detail[1];
                hr_healthcheck_u0_history_sick_detail _u0_history_sick_update = new hr_healthcheck_u0_history_sick_detail();

                _data_u0_history_sick_update.healthcheck_u2_history_sick_list = new hr_healthcheck_u2_history_sick_detail[1];
                hr_healthcheck_u2_history_sick_detail _u2_history_sick_update = new hr_healthcheck_u2_history_sick_detail();

                //u0
                _u0_history_sick_update.cemp_idx = _emp_idx;
                _u0_history_sick_update.u0_historysick_status = 1;
                //_u0_history_sick_update.m0_type_idx = int.Parse(ViewState["vs_TypeFormHistory"].ToString());
                _u0_history_sick_update.u0_historysick_idx = int.Parse(txt_u0_historysick_idx_edit.Text);

                //u2
                _u2_history_sick_update.havedisease = txt_havedisease_edit.Text;
                _u2_history_sick_update.surgery = txt_surgery_edit.Text;
                _u2_history_sick_update.immune = txt_Immune_edit.Text;
                _u2_history_sick_update.drugs_eat = txt_DrugsEat_Edit.Text;
                _u2_history_sick_update.allergy_history = txt_Allergyhistory_Edit.Text;
                _u2_history_sick_update.smoking = int.Parse(rdoSmoking_edit.SelectedValue);
                _u2_history_sick_update.smoking_value1 = txt_year_smoking_edit.Text;
                _u2_history_sick_update.smoking_value2 = txt_month_smoking_edit.Text;
                _u2_history_sick_update.smoking_value3 = txt_before_stopsmoking_edit.Text;
                _u2_history_sick_update.smoking_value4 = txt_valum_smoking_edit.Text;
                _u2_history_sick_update.alcohol = int.Parse(rdo_alcohol_edit.SelectedValue);
                _u2_history_sick_update.alcohol_value1 = txt_year_alcohol_edit.Text;
                _u2_history_sick_update.alcohol_value2 = txt_month_alcohol_edit.Text;
                _u2_history_sick_update.addict_drug = txt_addict_drug_edit.Text;
                _u2_history_sick_update.datahealth = txt_datahealth_edit.Text;
                _u2_history_sick_update.u0_historysick_idx = int.Parse(txt_u0_historysick_idx_edit.Text);
                _u2_history_sick_update.u2_historysick_idx = int.Parse(txt_u2_historysick_idx_edit.Text);


                //vs dataset detail u3_history sick check
                var _dataset_HistoryEverSickList_update = (DataSet)ViewState["vsDetailEverSickListUpdate"];

                var _add_HistoryEverSickList_update = new hr_healthcheck_u3_history_sick_detail[_dataset_HistoryEverSickList_update.Tables[0].Rows.Count];
                int row_eversicklist_update = 0;

                foreach (DataRow dtrow_HistoryEversickList in _dataset_HistoryEverSickList_update.Tables[0].Rows)
                {
                    _add_HistoryEverSickList_update[row_eversicklist_update] = new hr_healthcheck_u3_history_sick_detail();

                    _add_HistoryEverSickList_update[row_eversicklist_update].eversick = dtrow_HistoryEversickList["drEversickEditText"].ToString();
                    _add_HistoryEverSickList_update[row_eversicklist_update].sick_year = dtrow_HistoryEversickList["drLastyearEditText"].ToString();
                    _add_HistoryEverSickList_update[row_eversicklist_update].cemp_idx = _emp_idx;

                    row_eversicklist_update++;

                }

                //vs dataset detail u4_history sick check
                var _dataset_HistoryDiseaseFamilyList_update = (DataSet)ViewState["vsDetailDiseaseFamilyListUpdate"];

                var _add_HistoryDiseaseFamilyList_update = new hr_healthcheck_u4_history_sick_detail[_dataset_HistoryDiseaseFamilyList_update.Tables[0].Rows.Count];
                int row_Diseaselist_update = 0;

                foreach (DataRow dtrow_HistoryDiseaseFamilyList in _dataset_HistoryDiseaseFamilyList_update.Tables[0].Rows)
                {
                    _add_HistoryDiseaseFamilyList_update[row_Diseaselist_update] = new hr_healthcheck_u4_history_sick_detail();

                    _add_HistoryDiseaseFamilyList_update[row_Diseaselist_update].relation_family = dtrow_HistoryDiseaseFamilyList["drRelation_familyEditText"].ToString();
                    _add_HistoryDiseaseFamilyList_update[row_Diseaselist_update].disease_family = dtrow_HistoryDiseaseFamilyList["drDisease_FamilyEditText"].ToString();
                    _add_HistoryDiseaseFamilyList_update[row_Diseaselist_update].cemp_idx = _emp_idx;

                    row_Diseaselist_update++;

                }

                _data_u0_history_sick_update.healthcheck_u0_history_sick_list[0] = _u0_history_sick_update;
                _data_u0_history_sick_update.healthcheck_u2_history_sick_list[0] = _u2_history_sick_update;
                _data_u0_history_sick_update.healthcheck_u3_history_sick_list = _add_HistoryEverSickList_update;
                _data_u0_history_sick_update.healthcheck_u4_history_sick_list = _add_HistoryDiseaseFamilyList_update;

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_u0_history_sick_update));
                _data_u0_history_sick_update = callServicePostHRHealthCheck(_urlSetHistorySickCheck, _data_u0_history_sick_update);

                setActiveTab("docDetailList", 0, 0, 0, txt_historyhealth_idx.Text, 0, int.Parse(txt_emp_idx_historyhealth.Text), "0");


                //if (_data_u0_history_sick_update.return_code == 0)
                //{

                //    setActiveTab("docDetailList", 0, 0, 0, txt_historyhealth_idx.Text, 0, int.Parse(txt_emp_idx_historyhealth.Text), "0");


                //    //setActiveTab("docCreateList", 0, 0, 0, "0", 0, 0, "0");

                //    //fvDetailEmployeeHistory.Visible = true;
                //    //setFormData(fvDetailEmployeeHistory, FormViewMode.ReadOnly, ViewState["vs_SearchEmployee_List"]);
                //    //div_showname_Form.Visible = true;
                //    //Update_BackToSearch.Visible = true;

                //    //div_SearchEmployee.Visible = false;
                //    //clearViewState();
                //    //Update_DetailEmployee.Visible = false;
                //    //gvEmployeeList.PageIndex = 0;
                //    //setGridData(gvEmployeeList, null);
                //    //fvDetailEmployeeHistory.Focus();aaaa

                //}


                break;

            case "cmdSearchHistoryEmployeeReport":

                //GridView gvEmployeeList = (GridView)fvEmpSearch.FindControl("gvEmployeeList");

                DropDownList ddlOrgSearchReport = (DropDownList)FvSearchReport.FindControl("ddlOrgSearchReport");
                DropDownList ddlDeptSearchReport = (DropDownList)FvSearchReport.FindControl("ddlDeptSearchReport");
                DropDownList ddlSecSearchReport = (DropDownList)FvSearchReport.FindControl("ddlSecSearchReport");

                data_employee _dataEmployee_searchreport = new data_employee();
                _dataEmployee_searchreport.search_key_emp_list = new search_key_employee[1];
                search_key_employee _search_keyreport = new search_key_employee();

                _search_keyreport.s_emp_code = ((TextBox)FvSearchReport.FindControl("tbEmpCode_HistorySearchreport")).Text.Trim();
                _search_keyreport.s_emp_name = ((TextBox)FvSearchReport.FindControl("tbEmpName_HistorySearchreport")).Text.Trim();
                _search_keyreport.s_org_idx = ddlOrgSearchReport.SelectedItem.Value;
                _search_keyreport.s_rdept_idx = ddlDeptSearchReport.SelectedItem.Value;
                _search_keyreport.s_rsec_idx = ddlSecSearchReport.SelectedItem.Value;
                _search_keyreport.s_emp_status = "1";

                _dataEmployee_searchreport.search_key_emp_list[0] = _search_keyreport;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee_search))

                _dataEmployee_searchreport = getViewEmployeeList(_dataEmployee_searchreport);
                ViewState["EmpSearchList_report"] = _dataEmployee_searchreport.employee_list;


                if (ViewState["EmpSearchList_report"] != null)
                {

                    GvPrintDetail.PageIndex = 0;
                    setGridData(GvPrintDetail, ViewState["EmpSearchList_report"]);

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลพนักงานดังกล่าว');", true);
                    setGridData(GvPrintDetail, ViewState["EmpSearchList_report"]);
                }


                break;

            case "cmdViewPrint":
                ViewState["vsArgEmpIDX_Report"] = int.Parse(cmdArg);
                setActiveTab("docReportList", 0, 0, 0, "0", 0, int.Parse(ViewState["vsArgEmpIDX_Report"].ToString()), "0");

                //printableAreaDetail.Visible = true;
                ////gvWorkPrintReport.DataBind();
                //getHistoryWorkPrintReport(int.Parse(cmdArg), gvWorkPrintReport);


                break;

            case "cmdCancelToSearchReport":
                setActiveTab("docReportList", 0, 0, 0, "0", int.Parse(cmdArg), int.Parse(ViewState["vsArgEmpIDX_Report"].ToString()), "0");

                break;

            case "cmdResetSearchReport":
                setActiveTab("docReportList", 0, 0, 0, "0", 0, 0, "0");
               // setFormData(FvSearchReport, FormViewMode.Insert, null);
                break;

            case "cmdPrintAllReport":
                //ViewState["vs_HistoryworkDetailReport_List"]

                //gvWorkPrintReport.UseAccessibleHeader = true;
                //GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
                //GridView1.FooterRow.TableSection = TableRowSection.TableFooter;

                //setGridData(gvWorkPrintReport, ViewState["vs_HistoryworkDetailReport_List"]);

                //gvWorkPrintReport.Attributes["style"] = "border-collapse:separate";
                //foreach (GridViewRow row in gvWorkPrintReport.Rows)
                //{
                //    if (row.RowIndex % 10 == 0 && row.RowIndex != 0)
                //    {
                //        row.Attributes["style"] = "page-break-after:always;";
                //    }
                //}
                //StringWriter sw = new StringWriter();
                //HtmlTextWriter hw = new HtmlTextWriter(sw);
                //gvWorkPrintReport.RenderControl(hw);
                //string gridHTML = sw.ToString().Replace("\"", "'").Replace(System.Environment.NewLine, "");
                //StringBuilder sb = new StringBuilder();
                //sb.Append("<script type = 'text/javascript'>");
                //sb.Append("window.onload = new function(){");
                //sb.Append("var printWin = window.open('', '', 'left=0");
                //sb.Append(",top=300,width=1000,height=600,status=0');");
                //sb.Append("printWin.document.write(\"");
                //string style = "<style type = 'text/css'>thead {display:table-header-group;} tfoot{display:table-footer-group;}</style>";
                //sb.Append(style + gridHTML);
                //sb.Append("\");");
                //sb.Append("printWin.document.close();");
                //sb.Append("printWin.focus();");
                //sb.Append("printWin.print();");
                //sb.Append("printWin.close();");
                //sb.Append("};");
                //sb.Append("</script>");
                //ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
                //gvWorkPrintReport.DataBind();


                break;


        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "btnRemoveHistoryWorkList":

                   // GridView gvPlaceList = (GridView)fvInsertPerQA.FindControl("gvPlaceList");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsHistoryWorkList"];
                    dsContacts.Tables["dsHistoryWorkTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(gvHistoryWorkList, dsContacts.Tables["dsHistoryWorkTable"]);
                    if (dsContacts.Tables["dsHistoryWorkTable"].Rows.Count < 1)
                    {
                        Update_GridHistoryWork.Visible = false;                      
                        Update_SaveHistoryCheck.Visible = false;
                    }
                    break;
                case "btnRemoveHistoryInjuryList":

                    // GridView gvPlaceList = (GridView)fvInsertPerQA.FindControl("gvPlaceList");
                    GridViewRow rowSelect_injury = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_injury = rowSelect_injury.RowIndex;
                    DataSet dsContacts_injury = (DataSet)ViewState["vsHistoryInjuryList"];
                    dsContacts_injury.Tables["dsHistoryInjuryTable"].Rows[rowIndex_injury].Delete();
                    dsContacts_injury.AcceptChanges();
                    setGridData(gvHistoryInjury, dsContacts_injury.Tables["dsHistoryInjuryTable"]);
                    if (dsContacts_injury.Tables["dsHistoryInjuryTable"].Rows.Count < 1)
                    {
                        Update_GridHistoryInjuly.Visible = false;
                        Update_SaveHistoryInjury.Visible = false;
                    }
                    break;
                case "btnRemoveRikeHealth":

                    // GridView gvPlaceList = (GridView)fvInsertPerQA.FindControl("gvPlaceList");
                    GridViewRow rowSelect_rike = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_rike = rowSelect_rike.RowIndex;
                    DataSet dsContacts_rike = (DataSet)ViewState["vsDetailRikeList"];
                    dsContacts_rike.Tables["dsDetailRikeTable"].Rows[rowIndex_rike].Delete();
                    dsContacts_rike.AcceptChanges();
                    setGridData(gvRikeList, dsContacts_rike.Tables["dsDetailRikeTable"]);

                    if (dsContacts_rike.Tables["dsDetailRikeTable"].Rows.Count < 1)
                    {
                        gvRikeList.Visible = false;
                        
                    }
                    break;
                case "btnRemoveEverSickHealth":

                    // GridView gvPlaceList = (GridView)fvInsertPerQA.FindControl("gvPlaceList");
                    GridViewRow rowSelect_eversick = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_eversick = rowSelect_eversick.RowIndex;
                    DataSet dsContacts_eversick = (DataSet)ViewState["vsDetailEverSickList"];
                    dsContacts_eversick.Tables["dsDetailEverSickTable"].Rows[rowIndex_eversick].Delete();
                    dsContacts_eversick.AcceptChanges();
                    setGridData(gvEverSickList, dsContacts_eversick.Tables["dsDetailEverSickTable"]);

                    if (dsContacts_eversick.Tables["dsDetailEverSickTable"].Rows.Count < 1)
                    {
                        gvEverSickList.Visible = false;

                    }
                    break;
                case "btnRemoveRelationFamily":

                    // GridView gvPlaceList = (GridView)fvInsertPerQA.FindControl("gvPlaceList");
                    GridViewRow rowSelect_relationfamily = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_relationfamily = rowSelect_relationfamily.RowIndex;
                    DataSet dsContacts_refamily = (DataSet)ViewState["vsDetailDiseaseFamilyList"];
                    dsContacts_refamily.Tables["dsDetailDiseaseFamilyTable"].Rows[rowIndex_relationfamily].Delete();
                    dsContacts_refamily.AcceptChanges();
                    setGridData(gvDiseaseFamily, dsContacts_refamily.Tables["dsDetailDiseaseFamilyTable"]);

                    if (dsContacts_refamily.Tables["dsDetailDiseaseFamilyTable"].Rows.Count < 1)
                    {
                        gvDiseaseFamily.Visible = false;

                    }
                    break;
                case "btnRemoveEverSickHealthEdit":
                    //GridView gvPlacePerListUpdate = (GridView)fvInsertPerQA.FindControl("gvPlacePerListUpdate");
                    GridViewRow rowSelectUpdate_eversick = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndexUpdate_eversick = rowSelectUpdate_eversick.RowIndex;

                    DataSet dsContactsUpdate = (DataSet)ViewState["vsDetailEverSickListUpdate"];
                    dsContactsUpdate.Tables["dsDetailEverSickTableUpdate"].Rows[rowIndexUpdate_eversick].Delete();
                    dsContactsUpdate.AcceptChanges();

                    setGridData(gvEverSickListEdit, dsContactsUpdate.Tables["dsDetailEverSickTableUpdate"]);
                    if (dsContactsUpdate.Tables["dsDetailEverSickTableUpdate"].Rows.Count < 1)
                    {
                        gvEverSickListEdit.Visible = false;
                    }
                    break;
                case "btnRemoveRelationFamilyEdit":
                    //GridView gvPlacePerListUpdate = (GridView)fvInsertPerQA.FindControl("gvPlacePerListUpdate");
                    GridViewRow rowSelectUpdate_relation = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndexUpdate_relation = rowSelectUpdate_relation.RowIndex;

                    DataSet dsContactsUpdate_relation = (DataSet)ViewState["vsDetailDiseaseFamilyListUpdate"];
                    dsContactsUpdate_relation.Tables["dsDetailDiseaseFamilyTableUpdate"].Rows[rowIndexUpdate_relation].Delete();
                    dsContactsUpdate_relation.AcceptChanges();

                    setGridData(gvDiseaseFamilyEdit, dsContactsUpdate_relation.Tables["dsDetailDiseaseFamilyTableUpdate"]);
                    if (dsContactsUpdate_relation.Tables["dsDetailDiseaseFamilyTableUpdate"].Rows.Count < 1)
                    {
                        gvDiseaseFamilyEdit.Visible = false;
                    }
                    break;
                case "btnRemoveRikeHealthEdit":
                    //GridView gvPlacePerListUpdate = (GridView)fvInsertPerQA.FindControl("gvPlacePerListUpdate");
                    GridViewRow rowSelectUpdate_rikeedit = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndexUpdate_rike = rowSelectUpdate_rikeedit.RowIndex;

                    DataSet dsContactsUpdate_rike = (DataSet)ViewState["vsDetailRikeListUpdate"];
                    dsContactsUpdate_rike.Tables["dsDetailRikeTableUpdate"].Rows[rowIndexUpdate_rike].Delete();
                    dsContactsUpdate_rike.AcceptChanges();

                    setGridData(gvRikeListEdit, dsContactsUpdate_rike.Tables["dsDetailRikeTableUpdate"]);
                    if (dsContactsUpdate_rike.Tables["dsDetailRikeTableUpdate"].Rows.Count < 1)
                    {
                        gvRikeListEdit.Visible = false;
                    }
                    break;

            }

        }

    }

    protected void onSelectedIndexChanged(object sender, EventArgs e)
    {
        var rdName = (RadioButtonList)sender;
        //var chkName = (CheckBoxList)sender;

        switch (rdName.ID)
        {    
            case "rdoSmoking":

                RadioButtonList rdoSmoking = (RadioButtonList)fvSickCheckForm.FindControl("rdoSmoking");
                UpdatePanel updateInsert_eversmoking = (UpdatePanel)fvSickCheckForm.FindControl("updateInsert_eversmoking");
                UpdatePanel updateInsert_stopsmoking = (UpdatePanel)fvSickCheckForm.FindControl("updateInsert_stopsmoking");

                if (rdoSmoking.SelectedValue == "2") //เคยและปัจจุบันยังสูบอยู่ปริมาณ
                {
                    updateInsert_eversmoking.Visible = true;
                    updateInsert_stopsmoking.Visible = false;

                }
                else if(rdoSmoking.SelectedValue == "3") //เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน
                {
                    updateInsert_eversmoking.Visible = false;
                    updateInsert_stopsmoking.Visible = true;
                }
                else
                {
                    updateInsert_eversmoking.Visible = false;
                    updateInsert_stopsmoking.Visible = false;
                }

                break;
            case "rdo_alcohol":

                RadioButtonList rdo_alcohol = (RadioButtonList)fvSickCheckForm.FindControl("rdo_alcohol");
                UpdatePanel updateInsert_alcohol = (UpdatePanel)fvSickCheckForm.FindControl("updateInsert_alcohol");
               
                if (rdo_alcohol.SelectedValue == "6") //เคยและปัจจุบันยังสูบอยู่ปริมาณ
                {
                    updateInsert_alcohol.Visible = true;

                } 
                else
                {
                    updateInsert_alcohol.Visible = false;
                }

                break;
            case "rdoSmoking_edit":

                //Label idx_rdoSmoking_edit = (Label)fvSick.FindControl("idx_rdoSmoking_edit");
                RadioButtonList rdoSmoking_edit = (RadioButtonList)fvSick.FindControl("rdoSmoking_edit");
                UpdatePanel updateInsert_stopsmoking_edit = (UpdatePanel)fvSick.FindControl("updateInsert_stopsmoking_edit");
                UpdatePanel updateInsert_eversmoking_edit = (UpdatePanel)fvSick.FindControl("updateInsert_eversmoking_edit");


                txt_year_smoking_edit.Text = String.Empty;
                txt_month_smoking_edit.Text = String.Empty;
                txt_before_stopsmoking_edit.Text = String.Empty;
                txt_valum_smoking_edit.Text = String.Empty;


                if (rdoSmoking_edit.SelectedValue == "2") //เคยและปัจจุบันยังสูบอยู่ปริมาณ
                {
                    updateInsert_eversmoking_edit.Visible = true;
                    updateInsert_stopsmoking_edit.Visible = false;
                    //rdoSmoking_edit.SelectedValue = "2";

                }
                else if (rdoSmoking_edit.SelectedValue == "3") //เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน
                {
                    updateInsert_eversmoking_edit.Visible = false;
                    updateInsert_stopsmoking_edit.Visible = true;
                    //rdoSmoking_edit.SelectedValue = "2";
                }
                else
                {
                    updateInsert_eversmoking_edit.Visible = false;
                    updateInsert_stopsmoking_edit.Visible = false;
                    //rdoSmoking_edit.SelectedValue = idx_rdoSmoking_edit.Text;
                }

                break;
            case "rdo_alcohol_edit":

               // Label idx_rdo_alcohol_edit = (Label)fvSick.FindControl("idx_rdo_alcohol_edit");
                RadioButtonList rdo_alcohol_edit = (RadioButtonList)fvSick.FindControl("rdo_alcohol_edit");
                UpdatePanel updateInsert_alcohol_edit = (UpdatePanel)fvSick.FindControl("updateInsert_alcohol_edit");

                txt_year_alcohol_edit.Text = String.Empty;
                txt_month_alcohol_edit.Text = String.Empty;
                if (rdo_alcohol_edit.SelectedValue == "6") //เคยและปัจจุบันยังสูบอยู่ปริมาณ
                {
                    updateInsert_alcohol_edit.Visible = true;
                    //rdo_alcohol_edit.SelectedValue = "6";

                }
                else
                {
                    updateInsert_alcohol_edit.Visible = false;
                    //rdo_alcohol_edit.SelectedValue = idx_rdo_alcohol_edit.Text;
                }

                break;
        }
    }

    //protected void rdo_onSelectedIndexChanged(object sender, EventArgs e)
    //{
    //    var rdName = (RadioButtonList)sender;
    //    //var chkName = (CheckBoxList)sender;

    //    switch (rdName.ID)
    //    {
    //        case "rdoSmoking":

    //            RadioButtonList rdoSmoking = (RadioButtonList)fvSickCheckForm.FindControl("rdoSmoking");
    //            UpdatePanel updateInsert_eversmoking = (UpdatePanel)fvSickCheckForm.FindControl("updateInsert_eversmoking");
    //            UpdatePanel updateInsert_stopsmoking = (UpdatePanel)fvSickCheckForm.FindControl("updateInsert_stopsmoking");

    //            if (rdoSmoking.SelectedValue == "2") //เคยและปัจจุบันยังสูบอยู่ปริมาณ
    //            {
    //                updateInsert_eversmoking.Visible = true;
    //                updateInsert_stopsmoking.Visible = false;

    //            }
    //            else if (rdoSmoking.SelectedValue == "3") //เคยแต่เลิกแล้ว ระยะที่เคยสูบนาน
    //            {
    //                updateInsert_eversmoking.Visible = false;
    //                updateInsert_stopsmoking.Visible = true;
    //            }
    //            else
    //            {
    //                updateInsert_eversmoking.Visible = false;
    //                updateInsert_stopsmoking.Visible = false;
    //            }

    //            break;
    //        case "rdo_alcohol":

    //            RadioButtonList rdo_alcohol = (RadioButtonList)fvSickCheckForm.FindControl("rdo_alcohol");
    //            UpdatePanel updateInsert_alcohol = (UpdatePanel)fvSickCheckForm.FindControl("updateInsert_alcohol");

    //            if (rdo_alcohol.SelectedValue == "6") //เคยและปัจจุบันยังสูบอยู่ปริมาณ
    //            {
    //                updateInsert_alcohol.Visible = true;

    //            }
    //            else
    //            {
    //                updateInsert_alcohol.Visible = false;
    //            }

    //            break;
            
    //    }
    //}

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {
            case "rptTopicForm":

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    var lb_m0_topic_idx = (Label)e.Item.FindControl("lb_m0_topic_idx");
                    var lbFormTopicNameDetails = (Label)e.Item.FindControl("lbFormTopicNameDetails");

                    var rptDetailsTopicForm = (Repeater)e.Item.FindControl("rptDetailsTopicForm");

                    data_hr_healthcheck _data_hr_healthcheck_topic_form = new data_hr_healthcheck();
                    _data_hr_healthcheck_topic_form.healthcheck_historyform_list = new hr_healthcheck_historyform_detail[1];
                    hr_healthcheck_historyform_detail _hr_healthcheck_topic_form = new hr_healthcheck_historyform_detail();
                    _hr_healthcheck_topic_form.m0_topic_idx = int.Parse(lb_m0_topic_idx.Text);
                    _data_hr_healthcheck_topic_form.healthcheck_historyform_list[0] = _hr_healthcheck_topic_form;

                    _data_hr_healthcheck_topic_form = callServicePostHRHealthCheck(_urlGetDetailTopicForm, _data_hr_healthcheck_topic_form);
                    ViewState["vsOption"] = _data_hr_healthcheck_topic_form.healthcheck_historyform_list;
                    setRepeaterData(rptDetailsTopicForm, ViewState["vsOption"]);

                }

                break;

            case "rptDetailsTopicForm":

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    var lb_m0_topic_details_idx = (Label)e.Item.FindControl("lb_m0_topic_details_idx");
                    //var lbFormTopicNameDetails = (Label)e.Item.FindControl("lbFormTopicNameDetails");
                    var txt_front = (TextBox)e.Item.FindControl("txt_front");
                    var txt_back = (TextBox)e.Item.FindControl("txt_back");
                    var option_idx = (Label)e.Item.FindControl("lb_option_idx");
                    var rdoDetailsTopicForm = (RadioButtonList)e.Item.FindControl("rdoDetailsTopicForm");

                    switch (option_idx.Text)
                    {
                        case "1":

                            break;

                        case "2": //textbox
                            txt_front.Visible = true;
                            break;

                        case "3":

                            hr_healthcheck_historyform_detail[] _templist_chkRadio = (hr_healthcheck_historyform_detail[])ViewState["vsOption"];

                            var _linqhistoryform = (from datahistoryform in _templist_chkRadio
                                                    where datahistoryform.m0_topic_idx == int.Parse(lb_m0_topic_details_idx.Text)
                                                    select datahistoryform);

                            // rdoDetailsTopicForm.Items.Clear();
                            rdoDetailsTopicForm.AppendDataBoundItems = true;
                            rdoDetailsTopicForm.DataSource = _linqhistoryform.ToList();
                            rdoDetailsTopicForm.DataTextField = "topic_name";
                            rdoDetailsTopicForm.DataValueField = "m0_topic_idx";
                            rdoDetailsTopicForm.DataBind();
                            rdoDetailsTopicForm.Visible = true;
                            //lbFormTopicNameDetails.Visible = false;
                            break;

                    }

                    //if (lbFormTopicNameDetails.Text == "เคย" || lbFormTopicNameDetails.Text == "มี" || lbFormTopicNameDetails.Text == "เมื่อปี พ.ศ.")
                    //{
                    //    txt_back.Visible = true;
                    //}
                    //else 
                    //{
                    //    txt_back.Visible = false;
                    //}

                }

                break;
            case "rptBindNameForm":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {


                    var lbcheck_coler_nameform = (Label)e.Item.FindControl("lbcheck_coler_nameform");
                    var btnNameForm = (LinkButton)e.Item.FindControl("btnNameForm");

                    for (int num_form = 0; num_form <= rptBindNameForm.Items.Count; num_form++)
                    {
                        btnNameForm.CssClass = ConfigureColorsButtonForm(num_form);
                        //Console.WriteLine(i);
                    }

                }

                break;

            case "rpt_bindPrintHealth":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //ViewState["vs_u2idx_edit_historyhealth"] = int.Parse(_u2_health_idx_detail.ToString());

                    Label lbl_u2_historyhealth_idx_print = (Label)e.Item.FindControl("lbl_u2_historyhealth_idx_print");
                    FormView FvHealthCheckPrint = (FormView)e.Item.FindControl("FvHealthCheckPrint");

                    data_hr_healthcheck _data_m0_detailhistory_helth = new data_hr_healthcheck();

                    _data_m0_detailhistory_helth.healthcheck_u2_history_health_list = new hr_healthcheck_u2_history_health_detail[1];
                    hr_healthcheck_u2_history_health_detail _m0_history_health = new hr_healthcheck_u2_history_health_detail();

                    _m0_history_health.u2_historyhealth_idx = int.Parse(lbl_u2_historyhealth_idx_print.Text); 

                    _data_m0_detailhistory_helth.healthcheck_u2_history_health_list[0] = _m0_history_health;

                    _data_m0_detailhistory_helth = callServicePostHRHealthCheck(_urlGetHistoryHealthDetail, _data_m0_detailhistory_helth);

                    ViewState["vs_PrintHistoryHealth_detailForm"] = _data_m0_detailhistory_helth.healthcheck_u2_history_health_list;

                    setFormData(FvHealthCheckPrint, FormViewMode.ReadOnly, ViewState["vs_PrintHistoryHealth_detailForm"]);

                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_m0_detailhistory_helth));


                    Label _rdo_idx_detailtype_print = (Label)FvHealthCheckPrint.FindControl("rdo_idx_detailtype_print");
                    CheckBoxList rdoDetailType_detail_print = (CheckBoxList)FvHealthCheckPrint.FindControl("rdoDetailType_detail_print");

                    data_hr_healthcheck _data_m0_detailtype = new data_hr_healthcheck();

                    _data_m0_detailtype.healthcheck_m0_detailtype_list = new hr_healthcheck_m0_detailtype_detail[1];
                    hr_healthcheck_m0_detailtype_detail _m0_detailtype = new hr_healthcheck_m0_detailtype_detail();

                    _data_m0_detailtype.healthcheck_m0_detailtype_list[0] = _m0_detailtype;

                    _data_m0_detailtype = callServicePostHRHealthCheck(_urlGetDetailTypeHealthCheck, _data_m0_detailtype);

                    rdoDetailType_detail_print.DataSource = _data_m0_detailtype.healthcheck_m0_detailtype_list;
                    rdoDetailType_detail_print.DataTextField = "detail_typecheck";
                    rdoDetailType_detail_print.DataValueField = "m0_detail_typecheck_idx";
                    rdoDetailType_detail_print.DataBind();
                    rdoDetailType_detail_print.SelectedValue = _rdo_idx_detailtype_print.Text;

                    ////////

                    data_hr_healthcheck _data_hr_healthcheck_u3 = new data_hr_healthcheck();

                    _data_hr_healthcheck_u3.healthcheck_u3_history_health_list = new hr_healthcheck_u3_history_health_detail[1];
                    hr_healthcheck_u3_history_health_detail _hr_healthcheck_list_u3 = new hr_healthcheck_u3_history_health_detail();
                    _hr_healthcheck_list_u3.u2_historyhealth_idx = int.Parse(lbl_u2_historyhealth_idx_print.Text); // int.Parse(ViewState["vs_HistoryHealthDetail_u2"].ToString());

                    _data_hr_healthcheck_u3.healthcheck_u3_history_health_list[0] = _hr_healthcheck_list_u3;

                    _data_hr_healthcheck_u3 = callServicePostHRHealthCheck(_urlGetHistoryHealthRikeDetail, _data_hr_healthcheck_u3);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_healthcheck_u3));
                    Repeater rpt_RikeDetail_print = (Repeater)FvHealthCheckPrint.FindControl("rpt_RikeDetail_print");
                    UpdatePanel Update_RikeHealthCheck_Detail_print = (UpdatePanel)FvHealthCheckPrint.FindControl("Update_RikeHealthCheck_Detail_print");


                    if(_data_hr_healthcheck_u3.return_code == 0)
                    {
                        Update_RikeHealthCheck_Detail_print.Visible = true;
                        setRepeaterData(rpt_RikeDetail_print, _data_hr_healthcheck_u3.healthcheck_u3_history_health_list);

                    }
                    else
                    {
                        Update_RikeHealthCheck_Detail_print.Visible = false ;
                    }


                    ////}
                    Label lbl_checksprcify = (Label)FvHealthCheckPrint.FindControl("lbl_checksprcify");
                    CheckBoxList chk_specify_resultbody = (CheckBoxList)FvHealthCheckPrint.FindControl("chk_specify_resultbody");
                    UpdatePanel Panel_specify_resultbody = (UpdatePanel)FvHealthCheckPrint.FindControl("Panel_specify_resultbody");
                    if (lbl_checksprcify.Text == "ปกติ")
                    {
                        Panel_specify_resultbody.Visible = false;
                        chk_specify_resultbody.SelectedValue = "ปกติ";
                    }
                    else
                    {
                        Panel_specify_resultbody.Visible = true;
                        chk_specify_resultbody.SelectedValue = "ไม่ปกติ";
                    }


                }

                break;
            
            
        }
    }

    protected string ConfigureColorsButtonForm(int _count_form)
    {
        string returnResult11 = "";
        if (_count_form == 0)
        {
            returnResult11 = "btn btn-success";
        }
        else if (_count_form == 1)
        {
            returnResult11 = "btn btn-primary";
        }
        else if (_count_form == 2)
        {
            returnResult11 = "btn btn-warning";
        }
        else if (_count_form == 3)
        {
            returnResult11 = "btn btn-default";
        }
        else if (_count_form == 4)
        {
            returnResult11 = "btn btn-info";
        }
        else
        {
            returnResult11 = "btn btn-default";
        }

        return returnResult11;
    }

    protected string ConfigureActiveButton(int _idx_button_active)
    {
        string return_active = "";
        if (_idx_button_active == 0)
        {
            return_active = "btn btn-success active";
        }
        else if (_idx_button_active == 1)
        {
            return_active = "btn btn-primary active";
        }
        else if (_idx_button_active == 2)
        {
            return_active = "btn btn-warning active";
        }
        else if (_idx_button_active == 3)
        {
            return_active = "btn btn-default active";
        }
        else if (_idx_button_active == 4)
        {
            return_active = "btn btn-info active";
        }
        else
        {
            return_active = "btn btn-default active";
        }

        return return_active;
    }

    protected void onTextChanged(Object sender, EventArgs e)
    {
        if (sender is TextBox)
        {
            TextBox textbox = (TextBox)sender;
            switch (textbox.ID)
            {
               
                case "txtEmpNewEmpCode_Add":

                    TextBox txtEmpIDXHidden_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpIDXHidden_Add");
                    TextBox txtEmpNewEmpCode_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewEmpCode_Add");
                    TextBox txtEmpNewFullNameTH_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewFullNameTH_Add");
                    TextBox txtEmpNewOrgNameTH_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewOrgNameTH_Add");
                    TextBox txtEmpNewDeptNameTH_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewDeptNameTH_Add");
                    TextBox txtEmpNewSecNameTH_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewSecNameTH_Add");
                    TextBox txtEmpNewPosNameTH_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewPosNameTH_Add");

                    data_hr_healthcheck _data_history_emp_profile_add = new data_hr_healthcheck();
                    _data_history_emp_profile_add.healthcheck_employee_profile_list = new hr_healthcheck_employee_profile_detail[1];
                    hr_healthcheck_employee_profile_detail _hr_emp_profile_add = new hr_healthcheck_employee_profile_detail();

                    _hr_emp_profile_add.emp_code = textbox.Text.Trim();

                    _data_history_emp_profile_add.healthcheck_employee_profile_list[0] = _hr_emp_profile_add;
                    
                    _data_history_emp_profile_add = callServicePostHRHealthCheck(_urlGetEmployeeProfileHistory, _data_history_emp_profile_add);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_history_emp_profile));
                    if (_data_history_emp_profile_add.return_code == 1)
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลพนักงานดังกล่าว กรุณาติดต่อฝ่ายบุคคล');", true);

                        //PnCreate_EmployeeHistory.Visible = true;
                        //div_showname_Form.Visible = false;
                        setFormData(fvWorkHistoryForm, FormViewMode.ReadOnly, null);
                        setFormData(fvInjuryHistoryForm, FormViewMode.ReadOnly, null);
                        setFormData(fvHealcheckForm, FormViewMode.ReadOnly, null);
                        setFormData(fvSickCheckForm, FormViewMode.ReadOnly, null);

                        txtEmpIDXHidden_Add.Text = String.Empty;
                        txtEmpNewEmpCode_Add.Text = String.Empty;
                        txtEmpNewFullNameTH_Add.Text = String.Empty;
                        txtEmpNewOrgNameTH_Add.Text = String.Empty;
                        txtEmpNewDeptNameTH_Add.Text = String.Empty;
                        txtEmpNewSecNameTH_Add.Text = String.Empty;
                        txtEmpNewPosNameTH_Add.Text = String.Empty;
                    }
                    else
                    {
                        //setFormData(fvInsertHistoryHealth, FormViewMode.Insert, _data_history_emp_profile.healthcheck_employee_profile_list);
                        //PnCreate_EmployeeHistory.Visible = true;
                        //div_showname_Form.Visible = true;
                        txtEmpIDXHidden_Add.Text = _data_history_emp_profile_add.healthcheck_employee_profile_list[0].emp_idx.ToString();
                        txtEmpNewFullNameTH_Add.Text = _data_history_emp_profile_add.healthcheck_employee_profile_list[0].emp_name_th;
                        txtEmpNewOrgNameTH_Add.Text = _data_history_emp_profile_add.healthcheck_employee_profile_list[0].org_name_th;
                        txtEmpNewDeptNameTH_Add.Text = _data_history_emp_profile_add.healthcheck_employee_profile_list[0].dept_name_th;
                        txtEmpNewSecNameTH_Add.Text = _data_history_emp_profile_add.healthcheck_employee_profile_list[0].sec_name_th;
                        txtEmpNewPosNameTH_Add.Text = _data_history_emp_profile_add.healthcheck_employee_profile_list[0].pos_name_th;
                        //btnInsertPermPerm.Visible = true;
                    }




                    break;
            }
        }
    }

    protected void checkBoxChanged(object sender, EventArgs e)
    {
        var chkName = (CheckBox)sender;
        switch (chkName.ID)
        {
            case "":

                break;
        }
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlOrgSearchReport = (DropDownList)FvSearchReport.FindControl("ddlOrgSearchReport");
        DropDownList ddlDeptSearchReport = (DropDownList)FvSearchReport.FindControl("ddlDeptSearchReport");
        DropDownList ddlSecSearchReport = (DropDownList)FvSearchReport.FindControl("ddlSecSearchReport");

        switch (ddlName.ID)
        {
            case "ddl_Doctorname":

                if(ddl_Doctorname.SelectedValue != "0")
                {
                    data_hr_healthcheck _data_hr_doctor_bind = new data_hr_healthcheck();

                    _data_hr_doctor_bind.healthcheck_m0_doctor_list = new hr_healthcheck_m0_doctor_detail[1];
                    hr_healthcheck_m0_doctor_detail _hr_doctor_bind_list = new hr_healthcheck_m0_doctor_detail();
                    _hr_doctor_bind_list.m0_doctor_idx = int.Parse(ddl_Doctorname.SelectedValue);

                    _data_hr_doctor_bind.healthcheck_m0_doctor_list[0] = _hr_doctor_bind_list;

                    _data_hr_doctor_bind = callServicePostHRHealthCheck(_urlGetDetailDoctorInForm, _data_hr_doctor_bind);

                    if(_data_hr_doctor_bind.return_code == 0)
                    {
                        txtcard_number_doctor.Text = _data_hr_doctor_bind.healthcheck_m0_doctor_list[0].card_number;
                        txt_health_authority_name_.Text = _data_hr_doctor_bind.healthcheck_m0_doctor_list[0].health_authority_name;
                        txt_location_name.Text = _data_hr_doctor_bind.healthcheck_m0_doctor_list[0].location_name;
                        txt_village_no.Text = _data_hr_doctor_bind.healthcheck_m0_doctor_list[0].village_no;
                        txt_road_name.Text = _data_hr_doctor_bind.healthcheck_m0_doctor_list[0].road_name;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลแพทย์!!!');", true);

                        getNameDoctor(ddl_Doctorname);
                        txtcard_number_doctor.Text = String.Empty;
                        txt_health_authority_name_.Text = String.Empty;
                        txt_location_name.Text = String.Empty;
                        txt_village_no.Text = String.Empty;
                        txt_road_name.Text = String.Empty;
                    }   

                }
                else
                {
                    getNameDoctor(ddl_Doctorname);
                    txtcard_number_doctor.Text = String.Empty;
                    txt_health_authority_name_.Text = String.Empty;
                    txt_location_name.Text = String.Empty;
                    txt_village_no.Text = String.Empty;
                    txt_road_name.Text = String.Empty;
                }

                break;
            case "ddl_Doctorname_edit":

                DropDownList ddl_Doctorname_edit = (DropDownList)fvDetailHealthCheck.FindControl("ddl_Doctorname_edit");

                if (ddl_Doctorname_edit.SelectedValue != "0")
                {
                    txtEmpIDX_Doctor_edit.Text = "0";
                    //litDebug.Text = "9999";
                    data_hr_healthcheck _data_hr_doctor_bind = new data_hr_healthcheck();

                    _data_hr_doctor_bind.healthcheck_m0_doctor_list = new hr_healthcheck_m0_doctor_detail[1];
                    hr_healthcheck_m0_doctor_detail _hr_doctor_bind_list = new hr_healthcheck_m0_doctor_detail();
                    _hr_doctor_bind_list.m0_doctor_idx = int.Parse(ddl_Doctorname_edit.SelectedValue);

                    _data_hr_doctor_bind.healthcheck_m0_doctor_list[0] = _hr_doctor_bind_list;

                    _data_hr_doctor_bind = callServicePostHRHealthCheck(_urlGetDetailDoctorInForm, _data_hr_doctor_bind);

                    if (_data_hr_doctor_bind.return_code == 0)
                    {
                        txtcard_number_doctor_edit.Text = _data_hr_doctor_bind.healthcheck_m0_doctor_list[0].card_number;
                        txt_health_authority_name_edit.Text = _data_hr_doctor_bind.healthcheck_m0_doctor_list[0].health_authority_name;
                        txt_location_name_edit.Text = _data_hr_doctor_bind.healthcheck_m0_doctor_list[0].location_name;
                        txt_village_no_edit.Text = _data_hr_doctor_bind.healthcheck_m0_doctor_list[0].village_no;
                        txt_road_name_edit.Text = _data_hr_doctor_bind.healthcheck_m0_doctor_list[0].road_name;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลแพทย์!!!');", true);

                        getNameDoctor(ddl_Doctorname_edit);
                        txtcard_number_doctor_edit.Text = String.Empty;
                        txt_health_authority_name_edit.Text = String.Empty;
                        txt_location_name_edit.Text = String.Empty;
                        txt_village_no_edit.Text = String.Empty;
                        txt_road_name_edit.Text = String.Empty;
                    }

                }
                else
                {
                    txtEmpIDX_Doctor_edit.Text = "0";
                    //litDebug.Text = "77777";
                    getNameDoctorEdit(ddl_Doctorname_edit);
                    txtcard_number_doctor_edit.Text = String.Empty;
                    txt_health_authority_name_edit.Text = String.Empty;
                    txt_location_name_edit.Text = String.Empty;
                    txt_village_no_edit.Text = String.Empty;
                    txt_road_name_edit.Text = String.Empty;
                }

                break;
            case "ddlOrgSearchReport":

                getDepartmentList(ddlDeptSearchReport, int.Parse(ddlOrgSearchReport.SelectedItem.Value));
                ddlSecSearchReport.Items.Clear();
                ddlSecSearchReport.Items.Insert(0, new ListItem("--- แผนก ---", "-1"));

                break;
            case "ddlDeptSearchReport":

                getSectionList(ddlSecSearchReport, int.Parse(ddlOrgSearchReport.SelectedItem.Value), int.Parse(ddlDeptSearchReport.SelectedItem.Value));

                break;
        }
    }

    #endregion event command

    #region drorpdown list
    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- องค์กร ---", "-1"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- ฝ่าย ---", "-1"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- แผนก ---", "-1"));
    }
    #endregion dropdown list

    #region dataset

    protected void CleardataSetHistoryWorkList()
    {
        //var gvHistoryWorkList = (GridView)fvInsertPerQA.FindControl("gvHistoryWorkList");
        ViewState["vsHistoryWorkList"] = null;
        gvHistoryWorkList.DataSource = ViewState["vsHistoryWorkList"];
        gvHistoryWorkList.DataBind();
        getHistoryWorkList();
    }

    protected void getHistoryWorkList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsHistoryWorkList = new DataSet();
        dsHistoryWorkList.Tables.Add("dsHistoryWorkTable");
        
        dsHistoryWorkList.Tables["dsHistoryWorkTable"].Columns.Add("drbusiness_workText", typeof(String));
        dsHistoryWorkList.Tables["dsHistoryWorkTable"].Columns.Add("drDepartment_workText", typeof(String));
        dsHistoryWorkList.Tables["dsHistoryWorkTable"].Columns.Add("drbusiness_typeworkText", typeof(String));
        dsHistoryWorkList.Tables["dsHistoryWorkTable"].Columns.Add("drjobdescription_workText", typeof(String));
        dsHistoryWorkList.Tables["dsHistoryWorkTable"].Columns.Add("drStartDate_workText", typeof(String));
        dsHistoryWorkList.Tables["dsHistoryWorkTable"].Columns.Add("drEndDate_workText", typeof(String));
        dsHistoryWorkList.Tables["dsHistoryWorkTable"].Columns.Add("drHealthRisk_workText", typeof(String));
        dsHistoryWorkList.Tables["dsHistoryWorkTable"].Columns.Add("drUseMaterial_workText", typeof(String));


        //dsFoodMaterialList.Tables["dsFoodMaterialTable"].Columns.Add("drFoodMaterialAmount", typeof(String));
        //dsFoodMaterialList.Tables["dsFoodMaterialTable"].Columns.Add("drFoodMaterialPerUnit", typeof(String));
        ViewState["vsHistoryWorkList"] = dsHistoryWorkList;
    }

    protected void setHistoryWorkList()
    {
        if (ViewState["vsHistoryWorkList"] != null)
        {
            TextBox txt_business_work = (TextBox)fvWorkHistoryForm.FindControl("txt_business_work");
            TextBox txt_Department_work = (TextBox)fvWorkHistoryForm.FindControl("txt_Department_work");
            TextBox txt_business_typework = (TextBox)fvWorkHistoryForm.FindControl("txt_business_typework");
            TextBox txt_jobdescription_work = (TextBox)fvWorkHistoryForm.FindControl("txt_jobdescription_work");
            TextBox txt_StartDate_work = (TextBox)fvWorkHistoryForm.FindControl("txt_StartDate_work");
            TextBox txt_EndDate_work = (TextBox)fvWorkHistoryForm.FindControl("txt_EndDate_work");
            TextBox txt_HealthRisk_work = (TextBox)fvWorkHistoryForm.FindControl("txt_HealthRisk_work");
            TextBox txt_UseMaterial_work = (TextBox)fvWorkHistoryForm.FindControl("txt_UseMaterial_work");
            //Panel gvHistoryWorkList_scroll = (Panel)fvWorkHistoryForm.FindControl("gvHistoryWorkList_scroll");
            //Panel div_saveHistoryWork = (Panel)fvWorkHistoryForm.FindControl("div_saveHistoryWork");


            //GridView gvHistoryWorkList = (GridView)fvWorkHistoryForm.FindControl("gvHistoryWorkList");
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsHistoryWorkList"];

            foreach (DataRow dr in dsContacts.Tables["dsHistoryWorkTable"].Rows)
            {
                if (dr["drbusiness_workText"].ToString() == txt_business_work.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีชื่อสถานประกอบการนี้แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsHistoryWorkTable"].NewRow();
      
            drContacts["drbusiness_workText"] = txt_business_work.Text;
            drContacts["drDepartment_workText"] = txt_Department_work.Text;
            drContacts["drbusiness_typeworkText"] = txt_business_typework.Text;
            drContacts["drjobdescription_workText"] = txt_jobdescription_work.Text;
            drContacts["drStartDate_workText"] = txt_StartDate_work.Text;
            drContacts["drEndDate_workText"] = txt_EndDate_work.Text;

            if(txt_HealthRisk_work.Text == "")
            {
                drContacts["drHealthRisk_workText"] = "-";
            }
            else
            {
                drContacts["drHealthRisk_workText"] = txt_HealthRisk_work.Text;
            }

            if(txt_UseMaterial_work.Text == "")
            {
                drContacts["drUseMaterial_workText"] = "-";
            }
            else
            {
                drContacts["drUseMaterial_workText"] = txt_UseMaterial_work.Text;
            }
            
            
            
            dsContacts.Tables["dsHistoryWorkTable"].Rows.Add(drContacts);
            ViewState["vsHistoryWorkList"] = dsContacts;
            setGridData(gvHistoryWorkList, dsContacts.Tables["dsHistoryWorkTable"]);

            gvHistoryWorkList_scroll.Visible = true;
            Update_GridHistoryWork.Visible = true;
            Update_SaveHistoryCheck.Visible = true;

        }
    }

    protected void CleardataSetHistoryInjuryList()
    {
        //var gvHistoryWorkList = (GridView)fvInsertPerQA.FindControl("gvHistoryWorkList");
        ViewState["vsHistoryInjuryList"] = null;
        gvHistoryInjury.DataSource = ViewState["vsHistoryInjuryList"];
        gvHistoryInjury.DataBind();
        getHistoryInjuryList();
    }

    protected void getHistoryInjuryList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsHistoryInjuryList = new DataSet();
        dsHistoryInjuryList.Tables.Add("dsHistoryInjuryTable");

        dsHistoryInjuryList.Tables["dsHistoryInjuryTable"].Columns.Add("drdate_injuryText", typeof(String));
        dsHistoryInjuryList.Tables["dsHistoryInjuryTable"].Columns.Add("drinjurydetailText", typeof(String));
        dsHistoryInjuryList.Tables["dsHistoryInjuryTable"].Columns.Add("drcause_InjuryText", typeof(String));
        dsHistoryInjuryList.Tables["dsHistoryInjuryTable"].Columns.Add("drDisabilityText", typeof(String));
        dsHistoryInjuryList.Tables["dsHistoryInjuryTable"].Columns.Add("drLostOrganText", typeof(String));
        dsHistoryInjuryList.Tables["dsHistoryInjuryTable"].Columns.Add("drnotworkingText", typeof(String));
        dsHistoryInjuryList.Tables["dsHistoryInjuryTable"].Columns.Add("drnotworkingIDX", typeof(String));

        ViewState["vsHistoryInjuryList"] = dsHistoryInjuryList;
    }

    protected void setHistoryInjuryList()
    {
        if (ViewState["vsHistoryInjuryList"] != null)
        {
            TextBox txt_date_injury = (TextBox)fvInjuryHistoryForm.FindControl("txt_date_injury");
            TextBox txt_injurydetail = (TextBox)fvInjuryHistoryForm.FindControl("txt_injurydetail");
            TextBox txt_cause_Injury = (TextBox)fvInjuryHistoryForm.FindControl("txt_cause_Injury");
            TextBox txt_Disability = (TextBox)fvInjuryHistoryForm.FindControl("txt_Disability");
            TextBox txt_LostOrgan = (TextBox)fvInjuryHistoryForm.FindControl("txt_LostOrgan");
            RadioButtonList rdo_notworking = (RadioButtonList)fvInjuryHistoryForm.FindControl("rdo_notworking");
           
            //GridView gvHistoryWorkList = (GridView)fvWorkHistoryForm.FindControl("gvHistoryWorkList");
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsHistoryInjuryList"];

            foreach (DataRow dr in dsContacts.Tables["dsHistoryInjuryTable"].Rows)
            {
                if (dr["drdate_injuryText"].ToString() == txt_date_injury.Text && dr["drinjurydetailText"].ToString() == txt_injurydetail.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีการบาดเจ็บนี้แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsHistoryInjuryTable"].NewRow();

            drContacts["drdate_injuryText"] = txt_date_injury.Text;
            drContacts["drinjurydetailText"] = txt_injurydetail.Text;
            drContacts["drcause_InjuryText"] = txt_cause_Injury.Text;

            if(txt_Disability.Text == "")
            {
                drContacts["drDisabilityText"] = "-";
            }
            else
            {
                drContacts["drDisabilityText"] = txt_Disability.Text;
            }

            if(txt_LostOrgan.Text == "")
            {
                drContacts["drLostOrganText"] = "-";
            }
            else
            {
                drContacts["drLostOrganText"] = txt_LostOrgan.Text;
            }

            //litDebug.Text = rdo_notworking.SelectedValue.ToString();

            if(rdo_notworking.SelectedValue != "1" && rdo_notworking.SelectedValue != "2")
            {
                //litDebug.Text = "98999";
                drContacts["drnotworkingText"] = "-";
                drContacts["drnotworkingIDX"] = "0";
            }
            else
            {
                //litDebug.Text = "77777";
                drContacts["drnotworkingText"] = rdo_notworking.SelectedItem.ToString();
                drContacts["drnotworkingIDX"] = rdo_notworking.SelectedValue.ToString();
            }
         

            dsContacts.Tables["dsHistoryInjuryTable"].Rows.Add(drContacts);
            ViewState["vsHistoryInjuryList"] = dsContacts;
            setGridData(gvHistoryInjury, dsContacts.Tables["dsHistoryInjuryTable"]);

            gvHistoryInjulyList_scroll.Visible = true;
            Update_GridHistoryInjuly.Visible = true;
            Update_SaveHistoryInjury.Visible = true;

        }
    }

    protected void CleardataSetRikeHealthList()
    {
        //var gvHistoryWorkList = (GridView)fvInsertPerQA.FindControl("gvHistoryWorkList");
        ViewState["vsDetailRikeList"] = null;
        gvRikeList.DataSource = ViewState["vsDetailRikeList"];
        gvRikeList.DataBind();
        getDetailRikeList();
    }

    protected void getDetailRikeList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsDetailRikeList = new DataSet();
        dsDetailRikeList.Tables.Add("dsDetailRikeTable");

        dsDetailRikeList.Tables["dsDetailRikeTable"].Columns.Add("drrikeHealthText", typeof(String));
        dsDetailRikeList.Tables["dsDetailRikeTable"].Columns.Add("drResultHealthText", typeof(String));
       
        ViewState["vsDetailRikeList"] = dsDetailRikeList;
    }

    protected void setDetailRikeList()
    {
        if (ViewState["vsDetailRikeList"] != null)
        {
            TextBox txt_rikeHealthCheck = (TextBox)fvHealcheckForm.FindControl("txt_rikeHealthCheck");
            TextBox txt_ResultHealthCheck = (TextBox)fvHealcheckForm.FindControl("txt_ResultHealthCheck");
           
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsDetailRikeList"];

            foreach (DataRow dr in dsContacts.Tables["dsDetailRikeTable"].Rows)
            {
                if (dr["drrikeHealthText"].ToString() == txt_rikeHealthCheck.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีปัจจัยเสี่ยงนี้แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsDetailRikeTable"].NewRow();

            drContacts["drrikeHealthText"] = txt_rikeHealthCheck.Text;
            drContacts["drResultHealthText"] = txt_ResultHealthCheck.Text;
                 

            dsContacts.Tables["dsDetailRikeTable"].Rows.Add(drContacts);
            ViewState["vsDetailRikeList"] = dsContacts;
            gvRikeList.Visible = true;
            setGridData(gvRikeList, dsContacts.Tables["dsDetailRikeTable"]);
            



        }
    }

    protected void CleardataSetEverSickList()
    {
        //var gvHistoryWorkList vsDetailEverSickList (GridView)fvInsertPerQA.FindControl("gvHistoryWorkList");
        ViewState["vsDetailEverSickList"] = null;
        gvEverSickList.DataSource = ViewState["vsDetailEverSickList"];
        gvEverSickList.DataBind();
        getDetailEverSickList();
    }

    protected void getDetailEverSickList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsDetailEverSickList = new DataSet();
        dsDetailEverSickList.Tables.Add("dsDetailEverSickTable");

        dsDetailEverSickList.Tables["dsDetailEverSickTable"].Columns.Add("drEversickText", typeof(String));
        dsDetailEverSickList.Tables["dsDetailEverSickTable"].Columns.Add("drLastyearText", typeof(String));

        ViewState["vsDetailEverSickList"] = dsDetailEverSickList;
    }

    protected void setDetailEverSickList()
    {
        if (ViewState["vsDetailEverSickList"] != null)
        {

            TextBox txt_eversick_insert = (TextBox)fvSickCheckForm.FindControl("txt_eversick_insert");
            TextBox txt_lastyear_insert = (TextBox)fvSickCheckForm.FindControl("txt_lastyear_insert");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsDetailEverSickList"];

            foreach (DataRow dr in dsContacts.Tables["dsDetailEverSickTable"].Rows)
            {
                if (dr["drEversickText"].ToString() == txt_eversick_insert.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsDetailEverSickTable"].NewRow();

            drContacts["drEversickText"] = txt_eversick_insert.Text;
            drContacts["drLastyearText"] = txt_lastyear_insert.Text;


            dsContacts.Tables["dsDetailEverSickTable"].Rows.Add(drContacts);
            ViewState["vsDetailEverSickList"] = dsContacts;
            gvEverSickList.Visible = true;
            setGridData(gvEverSickList, dsContacts.Tables["dsDetailEverSickTable"]);




        }
    }

    protected void CleardataSetDiseaseFamilyList()
    {
        //var gvHistoryWorkList vsDetailEverSickList (GridView)fvInsertPerQA.FindControl("gvHistoryWorkList");
        ViewState["vsDetailDiseaseFamilyList"] = null;
        gvDiseaseFamily.DataSource = ViewState["vsDetailDiseaseFamilyList"];
        gvDiseaseFamily.DataBind();
        getDetailDiseaseFamilyList();
    }

    protected void getDetailDiseaseFamilyList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsDetailDiseaseFamilyList = new DataSet();
        dsDetailDiseaseFamilyList.Tables.Add("dsDetailDiseaseFamilyTable");

        dsDetailDiseaseFamilyList.Tables["dsDetailDiseaseFamilyTable"].Columns.Add("drRelation_familyText", typeof(String));
        dsDetailDiseaseFamilyList.Tables["dsDetailDiseaseFamilyTable"].Columns.Add("drDisease_FamilyText", typeof(String));

        ViewState["vsDetailDiseaseFamilyList"] = dsDetailDiseaseFamilyList;
    }

    protected void setDetailDiseaseFamilyList()
    {
        if (ViewState["vsDetailDiseaseFamilyList"] != null)
        {

            TextBox txt_eversick_insert = (TextBox)fvSickCheckForm.FindControl("txt_relation_family");
            TextBox txt_Disease_Family = (TextBox)fvSickCheckForm.FindControl("txt_Disease_Family");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsDetailDiseaseFamilyList"];

            foreach (DataRow dr in dsContacts.Tables["dsDetailDiseaseFamilyTable"].Rows)
            {
                if (dr["drRelation_familyText"].ToString() == txt_eversick_insert.Text && dr["drDisease_FamilyText"].ToString() == txt_Disease_Family.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsDetailDiseaseFamilyTable"].NewRow();

            drContacts["drRelation_familyText"] = txt_eversick_insert.Text;
            drContacts["drDisease_FamilyText"] = txt_Disease_Family.Text;


            dsContacts.Tables["dsDetailDiseaseFamilyTable"].Rows.Add(drContacts);
            ViewState["vsDetailDiseaseFamilyList"] = dsContacts;
            gvDiseaseFamily.Visible = true;
            setGridData(gvDiseaseFamily, dsContacts.Tables["dsDetailDiseaseFamilyTable"]);




        }
    }

    protected void CleardataSetPlaceListUpdate()
    {
        //var gvPlacePerListUpdate = (GridView)fvInsertPerQA.FindControl("gvPlacePerListUpdate");
        ViewState["vsDetailEverSickListUpdate"] = null;
        gvEverSickListEdit.DataSource = ViewState["vsDetailEverSickListUpdate"];
        gvEverSickListEdit.DataBind();
        getDetailEverSickListUpdate();
    }

    protected void getDetailEverSickListUpdate()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsDetailEverSickList_Update = new DataSet();
        dsDetailEverSickList_Update.Tables.Add("dsDetailEverSickTableUpdate");

        dsDetailEverSickList_Update.Tables["dsDetailEverSickTableUpdate"].Columns.Add("drEversickEditText", typeof(String));
        dsDetailEverSickList_Update.Tables["dsDetailEverSickTableUpdate"].Columns.Add("drLastyearEditText", typeof(String));

        ViewState["vsDetailEverSickListUpdate"] = dsDetailEverSickList_Update;
    }

    protected void setDetailEverSickListUpdate()
    {
        if (ViewState["vsDetailEverSickListUpdate"] != null)
        {

            TextBox txt_eversick_edit = (TextBox)fvSick.FindControl("txt_eversick_edit");
            TextBox txt_lastyear_edit = (TextBox)fvSick.FindControl("txt_lastyear_edit");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsDetailEverSickListUpdate"];

            foreach (DataRow dr in dsContacts.Tables["dsDetailEverSickTableUpdate"].Rows)
            {
                if (dr["drEversickEditText"].ToString() == txt_eversick_edit.Text && dr["drLastyearEditText"].ToString() == txt_lastyear_edit.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsDetailEverSickTableUpdate"].NewRow();

            drContacts["drEversickEditText"] = txt_eversick_edit.Text;
            drContacts["drLastyearEditText"] = txt_lastyear_edit.Text;


            dsContacts.Tables["dsDetailEverSickTableUpdate"].Rows.Add(drContacts);
            ViewState["vsDetailEverSickListUpdate"] = dsContacts;
            gvEverSickListEdit.Visible = true;
            setGridData(gvEverSickListEdit, dsContacts.Tables["dsDetailEverSickTableUpdate"]);


        }
    }

    protected void CleardataSetDiseaseFamilyListUpdate()
    {
        //var gvPlacePerListUpdate = (GridView)fvInsertPerQA.FindControl("gvPlacePerListUpdate");
        ViewState["vsDetailDiseaseFamilyListUpdate"] = null;
        gvDiseaseFamilyEdit.DataSource = ViewState["vsDetailDiseaseFamilyListUpdate"];
        gvDiseaseFamilyEdit.DataBind();
        getDetailDiseaseFamilyListUpdate();
    }

    protected void getDetailDiseaseFamilyListUpdate()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsDetailDiseaseFamilyList_edit = new DataSet();
        dsDetailDiseaseFamilyList_edit.Tables.Add("dsDetailDiseaseFamilyTableUpdate");

        dsDetailDiseaseFamilyList_edit.Tables["dsDetailDiseaseFamilyTableUpdate"].Columns.Add("drRelation_familyEditText", typeof(String));
        dsDetailDiseaseFamilyList_edit.Tables["dsDetailDiseaseFamilyTableUpdate"].Columns.Add("drDisease_FamilyEditText", typeof(String));

        ViewState["vsDetailDiseaseFamilyListUpdate"] = dsDetailDiseaseFamilyList_edit;
    }

    protected void setDetailDiseaseFamilyListUpdate()
    {
        if (ViewState["vsDetailDiseaseFamilyListUpdate"] != null)
        {

            TextBox txt_relation_family_edit = (TextBox)fvSick.FindControl("txt_relation_family_edit");
            TextBox txt_Disease_Family_edit = (TextBox)fvSick.FindControl("txt_Disease_Family_edit");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsDetailDiseaseFamilyListUpdate"];

            foreach (DataRow dr in dsContacts.Tables["dsDetailDiseaseFamilyTableUpdate"].Rows)
            {
                if (dr["drRelation_familyEditText"].ToString() == txt_relation_family_edit.Text && dr["drDisease_FamilyEditText"].ToString() == txt_Disease_Family_edit.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsDetailDiseaseFamilyTableUpdate"].NewRow();

            drContacts["drRelation_familyEditText"] = txt_relation_family_edit.Text;
            drContacts["drDisease_FamilyEditText"] = txt_Disease_Family_edit.Text;


            dsContacts.Tables["dsDetailDiseaseFamilyTableUpdate"].Rows.Add(drContacts);
            ViewState["vsDetailDiseaseFamilyListUpdate"] = dsContacts;
            gvDiseaseFamilyEdit.Visible = true;
            setGridData(gvDiseaseFamilyEdit, dsContacts.Tables["dsDetailDiseaseFamilyTableUpdate"]);


        }
    }

    protected void CleardataSetRikeHealthListUpdate()
    {
        //var gvPlacePerListUpdate = (GridView)fvInsertPerQA.FindControl("gvPlacePerListUpdate");
        ViewState["vsDetailRikeListUpdate"] = null;
        gvRikeListEdit.DataSource = ViewState["vsDetailRikeListUpdate"];
        gvRikeListEdit.DataBind();
        getDetailRikeListUpdate();
    }

    protected void getDetailRikeListUpdate()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsDetailRikeListUpdate = new DataSet();
        dsDetailRikeListUpdate.Tables.Add("dsDetailRikeTableUpdate");

        dsDetailRikeListUpdate.Tables["dsDetailRikeTableUpdate"].Columns.Add("drrikeHealthEditText", typeof(String));
        dsDetailRikeListUpdate.Tables["dsDetailRikeTableUpdate"].Columns.Add("drResultHealthEditText", typeof(String));

        ViewState["vsDetailRikeListUpdate"] = dsDetailRikeListUpdate;
    }

    protected void setDetailRikeListUpdate()
    {
        if (ViewState["vsDetailRikeListUpdate"] != null)
        {

            TextBox txt_rikeHealthCheck_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_rikeHealthCheck_edit");
            TextBox txt_ResultHealthCheck_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_ResultHealthCheck_edit");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsDetailRikeListUpdate"];

            foreach (DataRow dr in dsContacts.Tables["dsDetailRikeTableUpdate"].Rows)
            {
                if (dr["drrikeHealthEditText"].ToString() == txt_rikeHealthCheck_edit.Text && dr["drResultHealthEditText"].ToString() == txt_ResultHealthCheck_edit.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsDetailRikeTableUpdate"].NewRow();

            drContacts["drrikeHealthEditText"] = txt_rikeHealthCheck_edit.Text;
            drContacts["drResultHealthEditText"] = txt_ResultHealthCheck_edit.Text;


            dsContacts.Tables["dsDetailRikeTableUpdate"].Rows.Add(drContacts);
            ViewState["vsDetailRikeListUpdate"] = dsContacts;
            gvRikeListEdit.Visible = true;
            setGridData(gvRikeListEdit, dsContacts.Tables["dsDetailRikeTableUpdate"]);


        }
    }

    #endregion dataset

    #region bind data
    protected void getNameDoctor(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        data_hr_healthcheck _data_hr_doctor = new data_hr_healthcheck();

        _data_hr_doctor.healthcheck_m0_doctor_list = new hr_healthcheck_m0_doctor_detail[1];
        hr_healthcheck_m0_doctor_detail _hr_doctor_list = new hr_healthcheck_m0_doctor_detail();        
        _data_hr_doctor.healthcheck_m0_doctor_list[0] = _hr_doctor_list;

        _data_hr_doctor = callServicePostHRHealthCheck(_urlGetNameDoctorForm, _data_hr_doctor);

        ddl_Doctorname.DataSource = _data_hr_doctor.healthcheck_m0_doctor_list;

        setDdlData(ddlName, _data_hr_doctor.healthcheck_m0_doctor_list, "doctor_name", "m0_doctor_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกชื่อแพทย์ --", "0"));

    }

    protected void getNameDoctorEdit(DropDownList ddlName)
    {
        //TextBox txtEmpIDX_Doctor_edit = (TextBox)fvDetailHealthCheck.FindControl("txtEmpIDX_Doctor_edit");

        ddlName.Items.Clear();
        data_hr_healthcheck _data_hr_doctor_edit = new data_hr_healthcheck();
        _data_hr_doctor_edit.healthcheck_m0_doctor_list = new hr_healthcheck_m0_doctor_detail[1];
        hr_healthcheck_m0_doctor_detail _hr_doctor_list_edit = new hr_healthcheck_m0_doctor_detail();

        _data_hr_doctor_edit.healthcheck_m0_doctor_list[0] = _hr_doctor_list_edit;

        _data_hr_doctor_edit = callServicePostHRHealthCheck(_urlGetNameDoctorForm, _data_hr_doctor_edit);

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Insert(0, new ListItem("-- เลือกชื่อแพทย์ --", "0"));
        ddlName.DataSource = _data_hr_doctor_edit.healthcheck_m0_doctor_list;
        ddlName.DataTextField = "doctor_name";
        ddlName.DataValueField = "m0_doctor_idx";
        ddlName.DataBind();
        ddlName.SelectedValue = txtEmpIDX_Doctor_edit.Text;

        //ddl_Doctorname.DataSource = _data_hr_doctor.healthcheck_m0_doctor_list;

        //setDdlData(ddlName, _data_hr_doctor.healthcheck_m0_doctor_list, "doctor_name", "m0_doctor_idx");
        //ddlName.Items.Insert(0, new ListItem("-- เลือกชื่อแพทย์ --", "0"));

    }

    protected void getHistoryEmployee(int _emp_idx_history , FormView fvName)
    {

        data_hr_healthcheck _data_hr_healthcheck = new data_hr_healthcheck();
     
        _data_hr_healthcheck.healthcheck_historyemployee_list = new hr_healthcheck_historyemployee_detail[1];
        hr_healthcheck_historyemployee_detail _hr_healthcheck_list = new hr_healthcheck_historyemployee_detail();
        _hr_healthcheck_list.EmpIDX = _emp_idx_history;//int.Parse(rd.ToString());

        _data_hr_healthcheck.healthcheck_historyemployee_list[0] = _hr_healthcheck_list;

        _data_hr_healthcheck = callServicePostHRHealthCheck(_urlGetEmployeeHistory, _data_hr_healthcheck);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_healthcheck));

        if(_data_hr_healthcheck.return_code == 0)
        {
            setFormData(fvName, FormViewMode.ReadOnly, _data_hr_healthcheck.healthcheck_historyemployee_list);

            if(fvName == fvPrintHistoryEmployee)
            {
                Label lbl_SexNameTH_hr = (Label)fvPrintHistoryEmployee.FindControl("lbl_SexNameTH_hr");
                CheckBoxList check_SexNameTH = (CheckBoxList)fvPrintHistoryEmployee.FindControl("check_SexNameTH");

                //litDebug.Text = lbl_SexNameTH_hr.Text;

                if (lbl_SexNameTH_hr.Text == "ชาย")
                {
                    check_SexNameTH.SelectedValue = "ชาย";
                }
                else
                {
                    check_SexNameTH.SelectedValue = "หญิง";
                }
            }

        }



        //setRepeaterData(rpt_PrintHitoryEmployee, _data_hr_healthcheck.healthcheck_historyemployee_list);

    }

    protected void getEmployeeDetail()
    {

        _dataEmployee.search_key_emp_list = new search_key_employee[1];
        search_key_employee _search_key = new search_key_employee();

        _search_key.s_emp_status = "1";//ddlEmpStatus.SelectedItem.Value;

        _dataEmployee.search_key_emp_list[0] = _search_key;

        _dataEmployee = new data_employee();
        _dataEmployee = getViewEmployeeList(_dataEmployee);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        ViewState["vs_Employee_Detail"] = _dataEmployee.employee_list;
        setGridData(gvEmployeeDetail, ViewState["vs_Employee_Detail"]);

    }

    protected void getHistoryWork(int emp_idx_work, GridView GvName)
    {

        data_hr_healthcheck _data_hr_hiswork = new data_hr_healthcheck();

        _data_hr_hiswork.healthcheck_u2_history_work_list = new hr_healthcheck_u2_history_work_detail[1];
        hr_healthcheck_u2_history_work_detail _hr_historywork_list = new hr_healthcheck_u2_history_work_detail();
        _hr_historywork_list.emp_idx = emp_idx_work;//int.Parse(rd.ToString());

        _data_hr_hiswork.healthcheck_u2_history_work_list[0] = _hr_historywork_list;

        _data_hr_hiswork = callServicePostHRHealthCheck(_urlGetHistoryWorkEmployee, _data_hr_hiswork);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_healthcheck));
        
        ViewState["vs_HistoryworkDetail_List"] = _data_hr_hiswork.healthcheck_u2_history_work_list;
        //setGridData(gvWork, ViewState["vs_HistoryworkDetail_List"]);

        if(ViewState["vs_HistoryworkDetail_List"] == null)
        {
            //btnPrintHistoryWork1.Visible = false;
            gvHistoryWork_Scroll.Visible = false;
        }
        else
        {
            //btnPrintHistoryWork1.Visible = true;
            gvHistoryWork_Scroll.Visible = true;
            setGridData(GvName, ViewState["vs_HistoryworkDetail_List"]);
        }
        

    }

    protected void getHistoryWorkPrintReport(int emp_idx_work_report, GridView GvName)
    {

        data_hr_healthcheck _data_hr_hiswork = new data_hr_healthcheck();

        _data_hr_hiswork.healthcheck_u2_history_work_list = new hr_healthcheck_u2_history_work_detail[1];
        hr_healthcheck_u2_history_work_detail _hr_historywork_list = new hr_healthcheck_u2_history_work_detail();
        _hr_historywork_list.emp_idx = emp_idx_work_report;//int.Parse(rd.ToString());

        _data_hr_hiswork.healthcheck_u2_history_work_list[0] = _hr_historywork_list;

        _data_hr_hiswork = callServicePostHRHealthCheck(_urlGetHistoryWorkEmployee, _data_hr_hiswork);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_healthcheck));

        ViewState["vs_HistoryworkDetailReport_List"] = _data_hr_hiswork.healthcheck_u2_history_work_list;
        //setGridData(gvWork, ViewState["vs_HistoryworkDetail_List"]);

        if(ViewState["vs_HistoryworkDetailReport_List"] != null)
        {
            div_HisToryWorkPrint.Visible = true;
            setGridData(GvName, ViewState["vs_HistoryworkDetailReport_List"]);

        }
        else
        {
            div_HisToryWorkPrint.Visible = false;
        }



    }

    protected void getHistorySick(int emp_idx_sick)
    {

        data_hr_healthcheck _data_hr_hissick = new data_hr_healthcheck();

        _data_hr_hissick.healthcheck_u2_history_sick_list = new hr_healthcheck_u2_history_sick_detail[1];
        hr_healthcheck_u2_history_sick_detail _hr_historysick_list = new hr_healthcheck_u2_history_sick_detail();
        _hr_historysick_list.emp_idx = emp_idx_sick;//int.Parse(rd.ToString());

        _data_hr_hissick.healthcheck_u2_history_sick_list[0] = _hr_historysick_list;

        _data_hr_hissick = callServicePostHRHealthCheck(_urlGetHistorySickEmployee, _data_hr_hissick);
        //ViewState["vs_details_history_sick"] = _data_hr_hissick.healthcheck_u2_history_sick_list;
        setFormData(fvSick, FormViewMode.ReadOnly, _data_hr_hissick.healthcheck_u2_history_sick_list);
        div_btnedit_Historysick.Visible = true;

        Label idx_Smoking_hr = (Label)fvSick.FindControl("idx_Smoking_hr");
        CheckBoxList rdoSmoking_hr = (CheckBoxList)fvSick.FindControl("rdoSmoking_hr");
        UpdatePanel update_stopsmoking_hr = (UpdatePanel)fvSick.FindControl("update_stopsmoking_hr");
        UpdatePanel update_eversmoking_hr = (UpdatePanel)fvSick.FindControl("update_eversmoking_hr");

        //bind detail repeater
        if (_data_hr_hissick.return_code == 0)
        {

            ViewState["vs_u2_idx_historysick_edit"] = _data_hr_hissick.healthcheck_u2_history_sick_list[0].u2_historysick_idx;

            div_btnedit_Historysick.Visible = true;
            EmptyData.Visible = false;

            //check radio ever smoking
            if (idx_Smoking_hr.Text == "3")
            {
                update_stopsmoking_hr.Visible = true;
                update_eversmoking_hr.Visible = false;
                rdoSmoking_hr.SelectedValue = "3";
            }
            else if (idx_Smoking_hr.Text == "2")
            {
                update_eversmoking_hr.Visible = true;
                update_stopsmoking_hr.Visible = false;
                rdoSmoking_hr.SelectedValue = "2";
            }
            else
            {
                update_eversmoking_hr.Visible = false;
                update_stopsmoking_hr.Visible = false;
                rdoSmoking_hr.SelectedValue = idx_Smoking_hr.Text;
            }

            //check radio ever alcohol
            Label idx_alcohol_hr = (Label)fvSick.FindControl("idx_alcohol_hr");
            CheckBoxList rdo_alcohol_hr = (CheckBoxList)fvSick.FindControl("rdo_alcohol_hr");
            UpdatePanel update_alcohol_hr = (UpdatePanel)fvSick.FindControl("update_alcohol_hr");
            if (idx_alcohol_hr.Text == "6")
            {
                update_alcohol_hr.Visible = true;
                rdo_alcohol_hr.SelectedValue = "6";

            }
            else
            {
                update_alcohol_hr.Visible = false;
                rdo_alcohol_hr.SelectedValue = idx_alcohol_hr.Text;
            }

            ViewState["vs_u0_historysick_idx_eversick"] = _data_hr_hissick.healthcheck_u2_history_sick_list[0].u0_historysick_idx;
            data_hr_healthcheck _data_hr_eversick = new data_hr_healthcheck();

            _data_hr_eversick.healthcheck_u3_history_sick_list = new hr_healthcheck_u3_history_sick_detail[1];
            hr_healthcheck_u3_history_sick_detail _hr_eversick_list = new hr_healthcheck_u3_history_sick_detail();
            _hr_eversick_list.u0_historysick_idx = int.Parse(ViewState["vs_u0_historysick_idx_eversick"].ToString());

            _data_hr_eversick.healthcheck_u3_history_sick_list[0] = _hr_eversick_list;

            _data_hr_eversick = callServicePostHRHealthCheck(_urlGetHistorySickEverSick, _data_hr_eversick);

            ViewState["vs_ViewHistorySick"] = _data_hr_eversick.healthcheck_u3_history_sick_list;

            UpdatePanel update_eversick_hr = (UpdatePanel)fvSick.FindControl("update_eversick_hr");

            if (_data_hr_eversick.return_code == 0)
            {
                update_eversick_hr.Visible = true;
                setRepeaterData(rpt_eversick, ViewState["vs_ViewHistorySick"]);
            }
            else
            {
                update_eversick_hr.Visible = false;
            }


            UpdatePanel Update_Relationfamily_hr = (UpdatePanel)fvSick.FindControl("Update_Relationfamily_hr");

            data_hr_healthcheck _data_hr_relationfamily = new data_hr_healthcheck();

            _data_hr_relationfamily.healthcheck_u4_history_sick_list = new hr_healthcheck_u4_history_sick_detail[1];
            hr_healthcheck_u4_history_sick_detail _hr_relationfamily_list = new hr_healthcheck_u4_history_sick_detail();
            _hr_relationfamily_list.u0_historysick_idx = int.Parse(ViewState["vs_u0_historysick_idx_eversick"].ToString());

            _data_hr_relationfamily.healthcheck_u4_history_sick_list[0] = _hr_relationfamily_list;

            _data_hr_relationfamily = callServicePostHRHealthCheck(_urlGetHistorySickRelationFamily, _data_hr_relationfamily);
            ViewState["vs_ViewHistoryHealthCheck"] = _data_hr_relationfamily.healthcheck_u4_history_sick_list;

            if (_data_hr_eversick.return_code == 0)
            {
                Update_Relationfamily_hr.Visible = true;
                setRepeaterData(rptRelationfamily, ViewState["vs_ViewHistoryHealthCheck"]);
            }
            else
            {
                Update_Relationfamily_hr.Visible = false;
            }
            

        }
        else
        {
            EmptyData.Visible = true;
            div_btnedit_Historysick.Visible = false;
        }


    }

    protected void getHistorySickPrintReport(int emp_idx_sick_printreport)
    {

        data_hr_healthcheck _data_hr_hissick = new data_hr_healthcheck();

        _data_hr_hissick.healthcheck_u2_history_sick_list = new hr_healthcheck_u2_history_sick_detail[1];
        hr_healthcheck_u2_history_sick_detail _hr_historysick_list = new hr_healthcheck_u2_history_sick_detail();
        _hr_historysick_list.emp_idx = emp_idx_sick_printreport;//int.Parse(rd.ToString());

        _data_hr_hissick.healthcheck_u2_history_sick_list[0] = _hr_historysick_list;

        _data_hr_hissick = callServicePostHRHealthCheck(_urlGetHistorySickEmployee, _data_hr_hissick);


        

        //bind detail repeater
        if (_data_hr_hissick.return_code == 0)
        {


            ViewState["vs_details_history_sick"] = _data_hr_hissick.healthcheck_u2_history_sick_list;
            setFormData(fvSickPrinReport, FormViewMode.ReadOnly, ViewState["vs_details_history_sick"]);

            // div_btnedit_Historysick.Visible = true;
            Label lbl_smoking_print = (Label)fvSickPrinReport.FindControl("lbl_smoking_print");
            CheckBoxList rdoSmoking_hr_print = (CheckBoxList)fvSickPrinReport.FindControl("rdoSmoking_hr_print");
            UpdatePanel update_stopsmoking_hr_print = (UpdatePanel)fvSickPrinReport.FindControl("update_stopsmoking_hr_print");
            UpdatePanel update_eversmoking_hr_print = (UpdatePanel)fvSickPrinReport.FindControl("update_eversmoking_hr_print");

            ViewState["vs_u2_idx_historysick_edit_report"] = _data_hr_hissick.healthcheck_u2_history_sick_list[0].u2_historysick_idx;

            //div_btnedit_Historysick.Visible = true;
            //EmptyData.Visible = false;

            //check radio ever smoking
            if (lbl_smoking_print.Text == "3")
            {
                rdoSmoking_hr_print.SelectedValue = "3";
                update_stopsmoking_hr_print.Visible = true;
                update_eversmoking_hr_print.Visible = false;
            }
            else if (lbl_smoking_print.Text == "2")
            {
                rdoSmoking_hr_print.SelectedValue = "2";
                update_eversmoking_hr_print.Visible = true;
                update_stopsmoking_hr_print.Visible = false;
            }
            else
            {
                rdoSmoking_hr_print.SelectedValue = lbl_smoking_print.Text;
                update_eversmoking_hr_print.Visible = false;
                update_stopsmoking_hr_print.Visible = false;
            }

            //check radio ever alcohol
            Label lbl_alcohol_print = (Label)fvSickPrinReport.FindControl("lbl_alcohol_print");
            CheckBoxList rdo_alcohol_hr_print = (CheckBoxList)fvSickPrinReport.FindControl("rdo_alcohol_hr_print");
            UpdatePanel update_alcohol_hr_print = (UpdatePanel)fvSickPrinReport.FindControl("update_alcohol_hr_print");
            if (lbl_alcohol_print.Text == "6")
            {
                update_alcohol_hr_print.Visible = true;
                rdo_alcohol_hr_print.SelectedValue = "6";

            }
            else
            {
                update_alcohol_hr_print.Visible = false;
                rdo_alcohol_hr_print.SelectedValue = lbl_alcohol_print.Text;
            }

            ViewState["vs_u0_historysick_idx_eversick_report"] = _data_hr_hissick.healthcheck_u2_history_sick_list[0].u0_historysick_idx;
            data_hr_healthcheck _data_hr_eversick = new data_hr_healthcheck();

            _data_hr_eversick.healthcheck_u3_history_sick_list = new hr_healthcheck_u3_history_sick_detail[1];
            hr_healthcheck_u3_history_sick_detail _hr_eversick_list = new hr_healthcheck_u3_history_sick_detail();
            _hr_eversick_list.u0_historysick_idx = int.Parse(ViewState["vs_u0_historysick_idx_eversick_report"].ToString());

            _data_hr_eversick.healthcheck_u3_history_sick_list[0] = _hr_eversick_list;

            _data_hr_eversick = callServicePostHRHealthCheck(_urlGetHistorySickEverSick, _data_hr_eversick);

            setRepeaterData(rpt_eversick_print, _data_hr_eversick.healthcheck_u3_history_sick_list);

            data_hr_healthcheck _data_hr_relationfamily = new data_hr_healthcheck();

            _data_hr_relationfamily.healthcheck_u4_history_sick_list = new hr_healthcheck_u4_history_sick_detail[1];
            hr_healthcheck_u4_history_sick_detail _hr_relationfamily_list = new hr_healthcheck_u4_history_sick_detail();
            _hr_relationfamily_list.u0_historysick_idx = int.Parse(ViewState["vs_u0_historysick_idx_eversick_report"].ToString());

            _data_hr_relationfamily.healthcheck_u4_history_sick_list[0] = _hr_relationfamily_list;

            _data_hr_relationfamily = callServicePostHRHealthCheck(_urlGetHistorySickRelationFamily, _data_hr_relationfamily);


            CheckBoxList rdo_disease_family = (CheckBoxList)fvSickPrinReport.FindControl("rdo_disease_family");
            UpdatePanel Update_Relationfamily_hr_print = (UpdatePanel)fvSickPrinReport.FindControl("Update_Relationfamily_hr_print");


            if (_data_hr_relationfamily.return_code == 0)//มี
            {
                rdo_disease_family.SelectedValue = "0";
                
                Update_Relationfamily_hr_print.Visible = true;
                setRepeaterData(rptRelationfamily_print, _data_hr_relationfamily.healthcheck_u4_history_sick_list);
            }
            else
            {
                rdo_disease_family.SelectedValue = "1";
                Update_Relationfamily_hr_print.Visible = false;
            }


            //set
            Label id_havedisease_idx = (Label)fvSickPrinReport.FindControl("id_havedisease_idx");
            CheckBoxList rdo_havedisease = (CheckBoxList)fvSickPrinReport.FindControl("rdo_havedisease");
            UpdatePanel Panel_havedisease = (UpdatePanel)fvSickPrinReport.FindControl("Panel_havedisease");

            if (id_havedisease_idx.Text == "ไม่มี")
            {
                Panel_havedisease.Visible = false;
                rdo_havedisease.SelectedValue = "ไม่มี";
            }
            else
            {
                Panel_havedisease.Visible = true;
                rdo_havedisease.SelectedValue = "มี";
            }

            //
            Label lbl_surgery_print = (Label)fvSickPrinReport.FindControl("lbl_surgery_print");
            CheckBoxList rdo_surgery = (CheckBoxList)fvSickPrinReport.FindControl("rdo_surgery");
            UpdatePanel Panel_surgery = (UpdatePanel)fvSickPrinReport.FindControl("Panel_surgery");
            if (lbl_surgery_print.Text == "ไม่เคย")
            {
                Panel_surgery.Visible = false;
                rdo_surgery.SelectedValue = "ไม่เคย";
            }
            else
            {
                Panel_surgery.Visible = true;
                rdo_surgery.SelectedValue = "เคย";
            }



            //
            Label lbl_immune_print = (Label)fvSickPrinReport.FindControl("lbl_immune_print");
            CheckBoxList rdo_immune = (CheckBoxList)fvSickPrinReport.FindControl("rdo_immune");
            UpdatePanel Panel_immune = (UpdatePanel)fvSickPrinReport.FindControl("Panel_immune");
            if (lbl_immune_print.Text == "ไม่เคย")
            {
                Panel_immune.Visible = false;
                rdo_immune.SelectedValue = "ไม่เคย";
            }
            else
            {
                Panel_immune.Visible = true;
                rdo_immune.SelectedValue = "เคย";
            }

            //
            Label lbl_drugs_eat = (Label)fvSickPrinReport.FindControl("lbl_drugs_eat");
            CheckBoxList rdo_drugs_eat = (CheckBoxList)fvSickPrinReport.FindControl("rdo_drugs_eat");
            UpdatePanel Panel_drugs_eat = (UpdatePanel)fvSickPrinReport.FindControl("Panel_drugs_eat");
            if (lbl_drugs_eat.Text == "ไม่มี")
            {
                Panel_drugs_eat.Visible = false;
                rdo_drugs_eat.SelectedValue = "ไม่มี";
            }
            else
            {
                Panel_drugs_eat.Visible = true;
                rdo_drugs_eat.SelectedValue = "มี";
            }

            //
            Label lbl_allergy_history = (Label)fvSickPrinReport.FindControl("lbl_allergy_history");
            CheckBoxList rdo_allergy_history = (CheckBoxList)fvSickPrinReport.FindControl("rdo_allergy_history");
            UpdatePanel Panel_allergy_history = (UpdatePanel)fvSickPrinReport.FindControl("Panel_allergy_history");
            if (lbl_drugs_eat.Text == "ไม่มี")
            {
                Panel_allergy_history.Visible = false;
                rdo_allergy_history.SelectedValue = "ไม่มี";
            }
            else
            {
                Panel_allergy_history.Visible = true;
                rdo_allergy_history.SelectedValue = "มี";
            }

            //
            Label lbl_addict_drug = (Label)fvSickPrinReport.FindControl("lbl_addict_drug");
            CheckBoxList rdo_addict_drug = (CheckBoxList)fvSickPrinReport.FindControl("rdo_addict_drug");
            UpdatePanel Panel_addict_drug = (UpdatePanel)fvSickPrinReport.FindControl("Panel_addict_drug");
            if (lbl_addict_drug.Text == "ไม่เคย")
            {
                Panel_addict_drug.Visible = false;
                rdo_addict_drug.SelectedValue = "ไม่เคย";
            }
            else
            {
                Panel_addict_drug.Visible = true;
                rdo_addict_drug.SelectedValue = "เคย";
            }


            //setRepeaterData(rptRelationfamily_print, _data_hr_relationfamily.healthcheck_u4_history_sick_list);
        }
        else
        {
            //EmptyData.Visible = true;
            //div_btnedit_Historysick.Visible = false;
        }

        //

       

    }

    protected void getHistorySickProfile(int emp_idx_sick)
    {

        data_hr_healthcheck _data_hr_hissick_profile = new data_hr_healthcheck();

        _data_hr_hissick_profile.healthcheck_u2_history_sick_list = new hr_healthcheck_u2_history_sick_detail[1];
        hr_healthcheck_u2_history_sick_detail _hr_historysick_profile_list = new hr_healthcheck_u2_history_sick_detail();
        _hr_historysick_profile_list.emp_idx = emp_idx_sick;//int.Parse(rd.ToString());

        _data_hr_hissick_profile.healthcheck_u2_history_sick_list[0] = _hr_historysick_profile_list;

        _data_hr_hissick_profile = callServicePostHRHealthCheck(_urlGetHistorySickEmployee, _data_hr_hissick_profile);
        setFormData(fvHistorySick, FormViewMode.ReadOnly, _data_hr_hissick_profile.healthcheck_u2_history_sick_list);


        Label idx_smoking_profile = (Label)fvHistorySick.FindControl("idx_smoking_profile");
        CheckBoxList rdoSmoking_hr_Profile = (CheckBoxList)fvHistorySick.FindControl("rdoSmoking_hr_Profile");
        UpdatePanel update_stopsmoking_hr_Profile = (UpdatePanel)fvHistorySick.FindControl("update_stopsmoking_hr_Profile");
        UpdatePanel update_eversmoking_hr_Profile = (UpdatePanel)fvHistorySick.FindControl("update_eversmoking_hr_Profile");

        //bind detail repeater
        if (_data_hr_hissick_profile.return_code == 0)
        {
            EmptyDataSick.Visible = false;

            //check radio ever smoking
            if (idx_smoking_profile.Text == "3")
            {
                update_stopsmoking_hr_Profile.Visible = true;
                update_eversmoking_hr_Profile.Visible = false;
                rdoSmoking_hr_Profile.SelectedValue = "3";
            }
            else if (idx_smoking_profile.Text == "2")
            {
                update_eversmoking_hr_Profile.Visible = true;
                update_stopsmoking_hr_Profile.Visible = false;
                rdoSmoking_hr_Profile.SelectedValue = "2";
            }
            else
            {
                update_eversmoking_hr_Profile.Visible = false;
                update_stopsmoking_hr_Profile.Visible = false;
                rdoSmoking_hr_Profile.SelectedValue = idx_smoking_profile.Text;
            }

            //check radio ever alcohol
            Label idx_alcohol_hr_profile = (Label)fvHistorySick.FindControl("idx_alcohol_hr_profile");
            CheckBoxList rdo_alcohol_hr_Profile = (CheckBoxList)fvHistorySick.FindControl("rdo_alcohol_hr_Profile");
            UpdatePanel update_alcohol_hr_Profile = (UpdatePanel)fvHistorySick.FindControl("update_alcohol_hr_Profile");

            if (idx_alcohol_hr_profile.Text == "6")
            {
                update_alcohol_hr_Profile.Visible = true;
                rdo_alcohol_hr_Profile.SelectedValue = "6";

            }
            else
            {
                update_alcohol_hr_Profile.Visible = false;
                rdo_alcohol_hr_Profile.SelectedValue = idx_alcohol_hr_profile.Text;
            }

            ViewState["vs_u0_historysick_idx_eversick_profile"] = _data_hr_hissick_profile.healthcheck_u2_history_sick_list[0].u0_historysick_idx;

            UpdatePanel update_eversick_hr_Profile = (UpdatePanel)fvHistorySick.FindControl("update_eversick_hr_Profile");

            data_hr_healthcheck _data_hr_eversick_profile = new data_hr_healthcheck();

            _data_hr_eversick_profile.healthcheck_u3_history_sick_list = new hr_healthcheck_u3_history_sick_detail[1];
            hr_healthcheck_u3_history_sick_detail _hr_eversick_profile_list = new hr_healthcheck_u3_history_sick_detail();
            _hr_eversick_profile_list.u0_historysick_idx = int.Parse(ViewState["vs_u0_historysick_idx_eversick_profile"].ToString());

            _data_hr_eversick_profile.healthcheck_u3_history_sick_list[0] = _hr_eversick_profile_list;

            _data_hr_eversick_profile = callServicePostHRHealthCheck(_urlGetHistorySickEverSick, _data_hr_eversick_profile);


            if(_data_hr_eversick_profile.return_code == 0)
            {
                update_eversick_hr_Profile.Visible = true;
                setRepeaterData(rpt_eversick_Profile, _data_hr_eversick_profile.healthcheck_u3_history_sick_list);
            }
            else
            {
                update_eversick_hr_Profile.Visible = false;
            }

            UpdatePanel Update_Relationfamily_hr_Profile = (UpdatePanel)fvHistorySick.FindControl("Update_Relationfamily_hr_Profile");

            data_hr_healthcheck _data_hr_relationfamily_profile = new data_hr_healthcheck();

            _data_hr_relationfamily_profile.healthcheck_u4_history_sick_list = new hr_healthcheck_u4_history_sick_detail[1];
            hr_healthcheck_u4_history_sick_detail _hr_relationfamily_profile_list = new hr_healthcheck_u4_history_sick_detail();
            _hr_relationfamily_profile_list.u0_historysick_idx = int.Parse(ViewState["vs_u0_historysick_idx_eversick_profile"].ToString());

            _data_hr_relationfamily_profile.healthcheck_u4_history_sick_list[0] = _hr_relationfamily_profile_list;

            _data_hr_relationfamily_profile = callServicePostHRHealthCheck(_urlGetHistorySickRelationFamily, _data_hr_relationfamily_profile);

            if (_data_hr_relationfamily_profile.return_code == 0)
            {
                Update_Relationfamily_hr_Profile.Visible = true;
                setRepeaterData(rptRelationfamily_Profile, _data_hr_relationfamily_profile.healthcheck_u4_history_sick_list);
            }
            else
            {
                Update_Relationfamily_hr_Profile.Visible = false;
            }
                

        }
        else
        {
            EmptyDataSick.Visible = true;
        }


    }

    protected void getHistoryInjury(int _emp_idx_injury, GridView gvName)
    {

        data_hr_healthcheck _data_hr_hisinjury = new data_hr_healthcheck();

        _data_hr_hisinjury.healthcheck_u2_history_injury_list = new hr_healthcheck_u2_history_injury_detail[1];
        hr_healthcheck_u2_history_injury_detail _hr_historyinjury_list = new hr_healthcheck_u2_history_injury_detail();
        _hr_historyinjury_list.emp_idx = _emp_idx_injury;//int.Parse(rd.ToString());

        _data_hr_hisinjury.healthcheck_u2_history_injury_list[0] = _hr_historyinjury_list;

        _data_hr_hisinjury = callServicePostHRHealthCheck(_urlGetHistoryInjuryEmployee, _data_hr_hisinjury);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_healthcheck));

        ViewState["vs_gvInjury_detailhr"] = _data_hr_hisinjury.healthcheck_u2_history_injury_list;

        if(ViewState["vs_gvInjury_detailhr"] == null)
        {
            //btnPrintInjry.Visible = false;
        }
        else
        {
            //btnPrintInjry.Visible = true;
            setGridData(gvName, ViewState["vs_gvInjury_detailhr"]);
        }
        

    }

    protected void getHistoryInjuryPrintReport(int _emp_idx_injury_printreport, GridView gvName)
    {

        data_hr_healthcheck _data_hr_hisinjury = new data_hr_healthcheck();

        _data_hr_hisinjury.healthcheck_u2_history_injury_list = new hr_healthcheck_u2_history_injury_detail[1];
        hr_healthcheck_u2_history_injury_detail _hr_historyinjury_list = new hr_healthcheck_u2_history_injury_detail();
        _hr_historyinjury_list.emp_idx = _emp_idx_injury_printreport;//int.Parse(rd.ToString());

        _data_hr_hisinjury.healthcheck_u2_history_injury_list[0] = _hr_historyinjury_list;

        _data_hr_hisinjury = callServicePostHRHealthCheck(_urlGetHistoryInjuryEmployee, _data_hr_hisinjury);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_healthcheck));
        ViewState["vs_HistoryInjuryPrint"] = _data_hr_hisinjury.healthcheck_u2_history_injury_list;

        if(ViewState["vs_HistoryInjuryPrint"] != null)
        {
            div_HistoryInjuryPrint.Visible = true;
            setGridData(gvName, ViewState["vs_HistoryInjuryPrint"]);
        }
        else
        {
            div_HistoryInjuryPrint.Visible = false;
        }
        

    }

    protected void getHistoryHealth(int _emp_idx_health , GridView GvName)
    {

        data_hr_healthcheck _data_hr_hishealth = new data_hr_healthcheck();

        _data_hr_hishealth.healthcheck_u2_history_health_list = new hr_healthcheck_u2_history_health_detail[1];
        hr_healthcheck_u2_history_health_detail _hr_historyhealth_list = new hr_healthcheck_u2_history_health_detail();
        _hr_historyhealth_list.emp_idx = _emp_idx_health;

        _data_hr_hishealth.healthcheck_u2_history_health_list[0] = _hr_historyhealth_list;

        _data_hr_hishealth = callServicePostHRHealthCheck(_urlGetHistoryHealthEmployee, _data_hr_hishealth);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_hishealth));
        //setGridData(GvName, _data_hr_hishealth.healthcheck_u2_history_health_list);

        ViewState["vs_HistoryHealthDetail_List"] = _data_hr_hishealth.healthcheck_u2_history_health_list;
        setGridData(GvName, ViewState["vs_HistoryHealthDetail_List"]);

    }

    protected void getDetailTypeHealthCheck()
    {
        RadioButtonList rdoDetailType = (RadioButtonList)fvHealcheckForm.FindControl("rdoDetailType");

        data_hr_healthcheck _data_m0_detailtype = new data_hr_healthcheck();

        _data_m0_detailtype.healthcheck_m0_detailtype_list = new hr_healthcheck_m0_detailtype_detail[1];
        hr_healthcheck_m0_detailtype_detail _m0_detailtype = new hr_healthcheck_m0_detailtype_detail();

        _data_m0_detailtype.healthcheck_m0_detailtype_list[0] = _m0_detailtype;

        _data_m0_detailtype = callServicePostHRHealthCheck(_urlGetDetailTypeHealthCheck, _data_m0_detailtype);

        rdoDetailType.DataSource = _data_m0_detailtype.healthcheck_m0_detailtype_list;
        rdoDetailType.DataTextField = "detail_typecheck";
        rdoDetailType.DataValueField = "m0_detail_typecheck_idx";
        rdoDetailType.DataBind();

    }

    protected void getDetailTypeHealthCheckViewPrint()
    {
        FormView FvHealthCheckPrint = (FormView)rpt_bindPrintHealth.FindControl("FvHealthCheckPrint");
        Label _rdo_idx_detailtype_print = (Label)FvHealthCheckPrint.FindControl("rdo_idx_detailtype_print");
        CheckBoxList rdoDetailType_detail_print = (CheckBoxList)FvHealthCheckPrint.FindControl("rdoDetailType_detail_print");

        data_hr_healthcheck _data_m0_detailtype = new data_hr_healthcheck();

        _data_m0_detailtype.healthcheck_m0_detailtype_list = new hr_healthcheck_m0_detailtype_detail[1];
        hr_healthcheck_m0_detailtype_detail _m0_detailtype = new hr_healthcheck_m0_detailtype_detail();

        _data_m0_detailtype.healthcheck_m0_detailtype_list[0] = _m0_detailtype;

        _data_m0_detailtype = callServicePostHRHealthCheck(_urlGetDetailTypeHealthCheck, _data_m0_detailtype);

        rdoDetailType_detail_print.DataSource = _data_m0_detailtype.healthcheck_m0_detailtype_list;
        rdoDetailType_detail_print.DataTextField = "detail_typecheck";
        rdoDetailType_detail_print.DataValueField = "m0_detail_typecheck_idx";
        rdoDetailType_detail_print.DataBind();
        rdoDetailType_detail_print.SelectedValue = _rdo_idx_detailtype_print.Text;

    }

    protected void getDetailTypeHealthCheckView()
    {
        Label _rao_idx_detailtype = (Label)fvDetailHealthCheck.FindControl("rao_idx_detailtype");
        RadioButtonList rdoDetailType_detail = (RadioButtonList)fvDetailHealthCheck.FindControl("rdoDetailType_detail");

        data_hr_healthcheck _data_m0_detailtype = new data_hr_healthcheck();

        _data_m0_detailtype.healthcheck_m0_detailtype_list = new hr_healthcheck_m0_detailtype_detail[1];
        hr_healthcheck_m0_detailtype_detail _m0_detailtype = new hr_healthcheck_m0_detailtype_detail();

        _data_m0_detailtype.healthcheck_m0_detailtype_list[0] = _m0_detailtype;

        _data_m0_detailtype = callServicePostHRHealthCheck(_urlGetDetailTypeHealthCheck, _data_m0_detailtype);

        rdoDetailType_detail.DataSource = _data_m0_detailtype.healthcheck_m0_detailtype_list;
        rdoDetailType_detail.DataTextField = "detail_typecheck";
        rdoDetailType_detail.DataValueField = "m0_detail_typecheck_idx";
        rdoDetailType_detail.DataBind();
        rdoDetailType_detail.SelectedValue = _rao_idx_detailtype.Text;

    }

    protected void getDetailTypeHealthCheckViewProfile()
    {
        Label rdo_idx_detailtype_profile = (Label)fvHistoryHealthProfile.FindControl("rdo_idx_detailtype_profile");
        CheckBoxList rdoDetailType_detail_Profile = (CheckBoxList)fvHistoryHealthProfile.FindControl("rdoDetailType_detail_Profile");

        data_hr_healthcheck _data_m0_detailtype = new data_hr_healthcheck();

        _data_m0_detailtype.healthcheck_m0_detailtype_list = new hr_healthcheck_m0_detailtype_detail[1];
        hr_healthcheck_m0_detailtype_detail _m0_detailtype = new hr_healthcheck_m0_detailtype_detail();

        _data_m0_detailtype.healthcheck_m0_detailtype_list[0] = _m0_detailtype;

        _data_m0_detailtype = callServicePostHRHealthCheck(_urlGetDetailTypeHealthCheck, _data_m0_detailtype);

        rdoDetailType_detail_Profile.DataSource = _data_m0_detailtype.healthcheck_m0_detailtype_list;
        rdoDetailType_detail_Profile.DataTextField = "detail_typecheck";
        rdoDetailType_detail_Profile.DataValueField = "m0_detail_typecheck_idx";
        rdoDetailType_detail_Profile.DataBind();
        rdoDetailType_detail_Profile.SelectedValue = rdo_idx_detailtype_profile.Text;

    }

    protected void getDetailTypeHealthCheckEdit()
    {
        Label _rao_idx_detailtype_edit = (Label)fvDetailHealthCheck.FindControl("rao_idx_detailtype_edit");
        RadioButtonList rdoDetailType_edit = (RadioButtonList)fvDetailHealthCheck.FindControl("rdoDetailType_edit");

        data_hr_healthcheck _data_m0_detailtype_edit = new data_hr_healthcheck();

        _data_m0_detailtype_edit.healthcheck_m0_detailtype_list = new hr_healthcheck_m0_detailtype_detail[1];
        hr_healthcheck_m0_detailtype_detail _m0_detailtype_edit = new hr_healthcheck_m0_detailtype_detail();

        _data_m0_detailtype_edit.healthcheck_m0_detailtype_list[0] = _m0_detailtype_edit;

        _data_m0_detailtype_edit = callServicePostHRHealthCheck(_urlGetDetailTypeHealthCheck, _data_m0_detailtype_edit);

        rdoDetailType_edit.DataSource = _data_m0_detailtype_edit.healthcheck_m0_detailtype_list;
        rdoDetailType_edit.DataTextField = "detail_typecheck";
        rdoDetailType_edit.DataValueField = "m0_detail_typecheck_idx";
        rdoDetailType_edit.DataBind();
        rdoDetailType_edit.SelectedValue = _rao_idx_detailtype_edit.Text;

    }

    protected void getDetailHistoryCheck(string _temp_empcode)
    {

        TextBox txtEmpIDXHidden_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpIDXHidden_Add");
        TextBox txtEmpNewEmpCode_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewEmpCode_Add");
        TextBox txtEmpNewFullNameTH_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewFullNameTH_Add");
        TextBox txtEmpNewOrgNameTH_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewOrgNameTH_Add");
        TextBox txtEmpNewDeptNameTH_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewDeptNameTH_Add");
        TextBox txtEmpNewSecNameTH_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewSecNameTH_Add");
        TextBox txtEmpNewPosNameTH_Add = (TextBox)fvAddUserCheckHistory.FindControl("txtEmpNewPosNameTH_Add");

        data_hr_healthcheck _data_history_emp_profile_add = new data_hr_healthcheck();
        _data_history_emp_profile_add.healthcheck_employee_profile_list = new hr_healthcheck_employee_profile_detail[1];
        hr_healthcheck_employee_profile_detail _hr_emp_profile_add = new hr_healthcheck_employee_profile_detail();

        _hr_emp_profile_add.emp_code = _temp_empcode.Trim();

        _data_history_emp_profile_add.healthcheck_employee_profile_list[0] = _hr_emp_profile_add;

        _data_history_emp_profile_add = callServicePostHRHealthCheck(_urlGetEmployeeProfileHistory, _data_history_emp_profile_add);

        txtEmpIDXHidden_Add.Text = _data_history_emp_profile_add.healthcheck_employee_profile_list[0].emp_idx.ToString();
        txtEmpNewFullNameTH_Add.Text = _data_history_emp_profile_add.healthcheck_employee_profile_list[0].emp_name_th;
        txtEmpNewOrgNameTH_Add.Text = _data_history_emp_profile_add.healthcheck_employee_profile_list[0].org_name_th;
        txtEmpNewDeptNameTH_Add.Text = _data_history_emp_profile_add.healthcheck_employee_profile_list[0].dept_name_th;
        txtEmpNewSecNameTH_Add.Text = _data_history_emp_profile_add.healthcheck_employee_profile_list[0].sec_name_th;
        txtEmpNewPosNameTH_Add.Text = _data_history_emp_profile_add.healthcheck_employee_profile_list[0].pos_name_th;

       
    }

    protected void getDetailHistoryHealth(int _u2_health_idx_detail)
    {
        
        data_hr_healthcheck _data_m0_detailhistory_helth = new data_hr_healthcheck();

        _data_m0_detailhistory_helth.healthcheck_u2_history_health_list = new hr_healthcheck_u2_history_health_detail[1];
        hr_healthcheck_u2_history_health_detail _m0_history_health = new hr_healthcheck_u2_history_health_detail();

        _m0_history_health.u2_historyhealth_idx = _u2_health_idx_detail;

        _data_m0_detailhistory_helth.healthcheck_u2_history_health_list[0] = _m0_history_health;

        _data_m0_detailhistory_helth = callServicePostHRHealthCheck(_urlGetHistoryHealthDetail, _data_m0_detailhistory_helth);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_m0_detailhistory_helth));
        setFormData(fvDetailHealthCheck, FormViewMode.ReadOnly, _data_m0_detailhistory_helth.healthcheck_u2_history_health_list);

        getDetailTypeHealthCheckView();
        ViewState["vs_u2idx_edit_historyhealth"] = int.Parse(_u2_health_idx_detail.ToString());

        Repeater rpt_RikeDetail = (Repeater)fvDetailHealthCheck.FindControl("rpt_RikeDetail");
        UpdatePanel Update_RikeHealthCheck_Detail = (UpdatePanel)fvDetailHealthCheck.FindControl("Update_RikeHealthCheck_Detail");

        if (_data_m0_detailhistory_helth.return_code == 0)
        {
            ViewState["vs_HistoryHealthDetail_u2"] = _data_m0_detailhistory_helth.healthcheck_u2_history_health_list[0].u2_historyhealth_idx;

            data_hr_healthcheck _data_hr_healthcheck_u3 = new data_hr_healthcheck();

            _data_hr_healthcheck_u3.healthcheck_u3_history_health_list = new hr_healthcheck_u3_history_health_detail[1];
            hr_healthcheck_u3_history_health_detail _hr_healthcheck_list_u3 = new hr_healthcheck_u3_history_health_detail();
            _hr_healthcheck_list_u3.u2_historyhealth_idx = int.Parse(ViewState["vs_HistoryHealthDetail_u2"].ToString());

            _data_hr_healthcheck_u3.healthcheck_u3_history_health_list[0] = _hr_healthcheck_list_u3;

            _data_hr_healthcheck_u3 = callServicePostHRHealthCheck(_urlGetHistoryHealthRikeDetail, _data_hr_healthcheck_u3);
            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_healthcheck_u3));

            ViewState["vs_ViewDetailRike"] = _data_hr_healthcheck_u3.healthcheck_u3_history_health_list;

            if(_data_hr_healthcheck_u3.return_code == 0)
            {
                Update_RikeHealthCheck_Detail.Visible = true;
                setRepeaterData(rpt_RikeDetail, ViewState["vs_ViewDetailRike"]);
            }
            else
            {
                Update_RikeHealthCheck_Detail.Visible = false;
            }

            

        }
    }

    protected void getDetailHistoryHealthPrint(int emp_idx_printhealth)
    {



        data_hr_healthcheck _data_m0_detailhistory_helth = new data_hr_healthcheck();

        _data_m0_detailhistory_helth.healthcheck_u2_history_health_list = new hr_healthcheck_u2_history_health_detail[1];
        hr_healthcheck_u2_history_health_detail _m0_history_health = new hr_healthcheck_u2_history_health_detail();

        _m0_history_health.emp_idx = emp_idx_printhealth;

        _data_m0_detailhistory_helth.healthcheck_u2_history_health_list[0] = _m0_history_health;

        _data_m0_detailhistory_helth = callServicePostHRHealthCheck(_urlGetHistoryHealthPrint, _data_m0_detailhistory_helth);

        ViewState["vs_PrintHistoryHealth"] = _data_m0_detailhistory_helth.healthcheck_u2_history_health_list;
        setRepeaterData(rpt_bindPrintHealth, ViewState["vs_PrintHistoryHealth"]);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_m0_detailhistory_helth));
       
        //getDetailTypeHealthCheckViewPrint();

    }

    protected void actionDeleteHistoryWork(int idx)
    {

        data_hr_healthcheck _data_u2work_del = new data_hr_healthcheck();
        _data_u2work_del.healthcheck_u2_history_work_list = new hr_healthcheck_u2_history_work_detail[1];
        hr_healthcheck_u2_history_work_detail _u2_del_list = new hr_healthcheck_u2_history_work_detail();

        _u2_del_list.u2_historywork_idx = idx;
        _u2_del_list.cemp_idx = _emp_idx;

        _data_u2work_del.healthcheck_u2_history_work_list[0] = _u2_del_list;

        _data_u2work_del = callServicePostHRHealthCheck(_urlSetDeleteHistoryWork, _data_u2work_del);

        ViewState["vs_HistoryworkDetail_List"] = _data_u2work_del.healthcheck_u2_history_work_list;
        setGridData(gvWork, ViewState["vs_HistoryworkDetail_List"]);

        setActiveTab("docDetailList", 0, 0, 0, txt_historyhealth_idx.Text, 0, int.Parse(txt_emp_idx_historyhealth.Text), "0");
    }

    protected void actionDeleteHistoryInjury(int idx_delinjury)
    {

        data_hr_healthcheck _data_u2injury_del = new data_hr_healthcheck();
        _data_u2injury_del.healthcheck_u2_history_injury_list = new hr_healthcheck_u2_history_injury_detail[1];
        hr_healthcheck_u2_history_injury_detail _u2injury_del_list = new hr_healthcheck_u2_history_injury_detail();

        _u2injury_del_list.u2_historyinjury_idx = idx_delinjury;
        _u2injury_del_list.cemp_idx = _emp_idx;

        _data_u2injury_del.healthcheck_u2_history_injury_list[0] = _u2injury_del_list;

        _data_u2injury_del = callServicePostHRHealthCheck(_urlSetDeleteHistoryInjury, _data_u2injury_del);

        ViewState["vs_HistoryInjuryDetail_List"] = _data_u2injury_del.healthcheck_u2_history_injury_list;
        setGridData(gvInjury, ViewState["vs_HistoryInjuryDetail_List"]);

        setActiveTab("docDetailList", 0, 0, 0, txt_historyhealth_idx.Text, 0, int.Parse(txt_emp_idx_historyhealth.Text), "0");
    }

    protected void actionDeleteHistoryHealth(int idx_u2)
    {
        data_hr_healthcheck _data_u2health_del = new data_hr_healthcheck();
        _data_u2health_del.healthcheck_u2_history_health_list = new hr_healthcheck_u2_history_health_detail[1];
        hr_healthcheck_u2_history_health_detail _u2_delhealth_list = new hr_healthcheck_u2_history_health_detail();

        _u2_delhealth_list.u2_historyhealth_idx = idx_u2;
        _u2_delhealth_list.cemp_idx = _emp_idx;

        _data_u2health_del.healthcheck_u2_history_health_list[0] = _u2_delhealth_list;

        _data_u2health_del = callServicePostHRHealthCheck(_urlSetDeleteHistoryHealth, _data_u2health_del);

        ViewState["vs_HistoryHealthDetail_List"] = _data_u2health_del.healthcheck_u2_history_health_list;
        setGridData(gvHealth, ViewState["vs_HistoryHealthDetail_List"]);

        setActiveTab("docDetailList", 0, 0, 0, txt_historyhealth_idx.Text, 0, int.Parse(txt_emp_idx_historyhealth.Text), "0");

    }

    protected void getDetailHistoryHealthProfile(int _u2_health_idx_detail)
    {


        data_hr_healthcheck _data_m0_detailhistory_helth_profile = new data_hr_healthcheck();

        _data_m0_detailhistory_helth_profile.healthcheck_u2_history_health_list = new hr_healthcheck_u2_history_health_detail[1];
        hr_healthcheck_u2_history_health_detail _m0_history_health_profile = new hr_healthcheck_u2_history_health_detail();

        _m0_history_health_profile.u2_historyhealth_idx = _u2_health_idx_detail;

        _data_m0_detailhistory_helth_profile.healthcheck_u2_history_health_list[0] = _m0_history_health_profile;

        _data_m0_detailhistory_helth_profile = callServicePostHRHealthCheck(_urlGetHistoryHealthDetail, _data_m0_detailhistory_helth_profile);

        setFormData(fvHistoryHealthProfile, FormViewMode.ReadOnly, _data_m0_detailhistory_helth_profile.healthcheck_u2_history_health_list);
        getDetailTypeHealthCheckViewProfile();

        Repeater rpt_RikeDetail_Profile = (Repeater)fvHistoryHealthProfile.FindControl("rpt_RikeDetail_Profile");
        UpdatePanel Update_RikeHealthCheck_Detail_Profile = (UpdatePanel)fvHistoryHealthProfile.FindControl("Update_RikeHealthCheck_Detail_Profile");
        //UpdatePanel Update_RikeHealthCheck_Detail_Profile = (UpdatePanel)fvHistoryHealthProfile.FindControl("Update_RikeHealthCheck_Detail_Profile");


        if (_data_m0_detailhistory_helth_profile.return_code == 0)
        {
           
            ViewState["vs_HistoryHealthDetail_u2_profile"] = _data_m0_detailhistory_helth_profile.healthcheck_u2_history_health_list[0].u2_historyhealth_idx;

            data_hr_healthcheck _data_hr_healthcheck_u3_profile = new data_hr_healthcheck();

            _data_hr_healthcheck_u3_profile.healthcheck_u3_history_health_list = new hr_healthcheck_u3_history_health_detail[1];
            hr_healthcheck_u3_history_health_detail _hr_healthcheck_list_u3_profile = new hr_healthcheck_u3_history_health_detail();
            _hr_healthcheck_list_u3_profile.u2_historyhealth_idx = int.Parse(ViewState["vs_HistoryHealthDetail_u2_profile"].ToString());

            _data_hr_healthcheck_u3_profile.healthcheck_u3_history_health_list[0] = _hr_healthcheck_list_u3_profile;

            _data_hr_healthcheck_u3_profile = callServicePostHRHealthCheck(_urlGetHistoryHealthRikeDetail, _data_hr_healthcheck_u3_profile);
            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_healthcheck_u3_profile));

            //ViewState["vs_DetailRikeView"] = _data_hr_healthcheck_u3_profile.healthcheck_u3_history_health_list;

            if (_data_hr_healthcheck_u3_profile.return_code == 0)
            {
                //Update_RikeHealthCheck_Detail_Profile.Visible = true;
                setRepeaterData(rpt_RikeDetail_Profile, _data_hr_healthcheck_u3_profile.healthcheck_u3_history_health_list);
            }
            else
            {
                //Update_RikeHealthCheck_Detail_Profile.Visible = false;
            }

        }
        
    }

    protected void actionReadHistoryWork(int idx_edit_historywork)
    {

        data_hr_healthcheck _data_edit_historywork = new data_hr_healthcheck();

        _data_edit_historywork.healthcheck_u2_history_work_list = new hr_healthcheck_u2_history_work_detail[1];
        hr_healthcheck_u2_history_work_detail _m0_edit_history_work = new hr_healthcheck_u2_history_work_detail();

        _m0_edit_history_work.u2_historywork_idx = idx_edit_historywork;
        _data_edit_historywork.healthcheck_u2_history_work_list[0] = _m0_edit_history_work;

        _data_edit_historywork = callServicePostHRHealthCheck(_urlSetEditHistoryWork, _data_edit_historywork);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_edit_historywork));
        setFormData(fvEditHistoryWork, FormViewMode.Edit, _data_edit_historywork.healthcheck_u2_history_work_list);

    }

    protected void actionReadHistorySick(int idx_edit_historysick)
    {

        data_hr_healthcheck _data_edit_historysick = new data_hr_healthcheck();

        _data_edit_historysick.healthcheck_u2_history_sick_list = new hr_healthcheck_u2_history_sick_detail[1];
        hr_healthcheck_u2_history_sick_detail _m0_edit_history_sick = new hr_healthcheck_u2_history_sick_detail();

        _m0_edit_history_sick.u2_historysick_idx = idx_edit_historysick;
        _data_edit_historysick.healthcheck_u2_history_sick_list[0] = _m0_edit_history_sick;

        _data_edit_historysick = callServicePostHRHealthCheck(_urlSetEditHistorySick, _data_edit_historysick);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_edit_historysick));
        setFormData(fvSick, FormViewMode.Edit, _data_edit_historysick.healthcheck_u2_history_sick_list);

    }

    protected void actionReadHistoryHealth(int idx_edit_historyhealth)
    {

        data_hr_healthcheck _data_m0_detailhistory_helth_edit = new data_hr_healthcheck();

        _data_m0_detailhistory_helth_edit.healthcheck_u2_history_health_list = new hr_healthcheck_u2_history_health_detail[1];
        hr_healthcheck_u2_history_health_detail _m0_history_health_edit = new hr_healthcheck_u2_history_health_detail();

        _m0_history_health_edit.u2_historyhealth_idx = idx_edit_historyhealth;
        _m0_history_health_edit.condition = 1;

        _data_m0_detailhistory_helth_edit.healthcheck_u2_history_health_list[0] = _m0_history_health_edit;

        _data_m0_detailhistory_helth_edit = callServicePostHRHealthCheck(_urlGetHistoryHealthDetail, _data_m0_detailhistory_helth_edit);

        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_m0_detailhistory_helth));
        //setFormData(fvDetailHealthCheck, FormViewMode.ReadOnly, _data_m0_detailhistory_helth.healthcheck_u2_history_health_list);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_edit_historysick));
        setFormData(fvDetailHealthCheck, FormViewMode.Edit, _data_m0_detailhistory_helth_edit.healthcheck_u2_history_health_list);

    }

    protected void actionReadHistoryInjury(int idx_edit_historyinjury)
    {

        data_hr_healthcheck _data_edit_historyinjury = new data_hr_healthcheck();

        _data_edit_historyinjury.healthcheck_u2_history_injury_list = new hr_healthcheck_u2_history_injury_detail[1];
        hr_healthcheck_u2_history_injury_detail _m0_edit_history_injury = new hr_healthcheck_u2_history_injury_detail();

        _m0_edit_history_injury.u2_historyinjury_idx = idx_edit_historyinjury;
        _data_edit_historyinjury.healthcheck_u2_history_injury_list[0] = _m0_edit_history_injury;

        _data_edit_historyinjury = callServicePostHRHealthCheck(_urlSetEditHistoryInjury, _data_edit_historyinjury);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_edit_historyinjury));
        setFormData(fvEditHistoryHealthInjury, FormViewMode.Edit, _data_edit_historyinjury.healthcheck_u2_history_injury_list);

    }


    #endregion end bind data

    #region reuse

    protected void initPage()
    {
        //clearSession();
        //clearViewState();

        setActiveTab("docHistory", 0, 0, 0, "0", 0, _emp_idx, "docProfile");
        clearViewState();
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
        ViewState["EmpSearchList"] = null;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setRadioButtonListData(RadioButtonList rdoName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        rdoName.Items.Clear();
        // bind items
        rdoName.DataSource = obj;
        rdoName.DataTextField = _data_text;
        rdoName.DataValueField = _data_value;
        rdoName.DataBind();
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee getViewEmployeeList(data_employee _data_employee)
    {
        _data_employee = callServicePostEmployee(_urlGetViewEmployeeList, _data_employee);
        return _data_employee;
    }

    protected data_hr_healthcheck callServicePostHRHealthCheck(string _cmdUrl, data_hr_healthcheck _data_hr_healthcheck)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_hr_healthcheck);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_hr_healthcheck = (data_hr_healthcheck)_funcTool.convertJsonToObject(typeof(data_hr_healthcheck), _localJson);

        return _data_hr_healthcheck;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int u1idx, int staidx, string doc_history_idx, int type_form, int emp_idx , string activeTabProfile)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
        setFormData(fvSearchHistoryEmployee, FormViewMode.Insert, null);
        setFormData(fvEmpSearch, FormViewMode.Insert, null);
        setFormData(fvDetailEmployeeHistory, FormViewMode.ReadOnly, null);
        div_showname_Form.Visible = false;
        Update_DetailEmployee.Visible = false;
        setGridData(gvEmployeeList, null);
        Update_BackToSearch.Visible = false;

        //Create
        divCreate.Visible = false;
        //setFormData(fvInsertHistoryHealth, FormViewMode.ReadOnly, null);
        setFormData(fvWorkHistoryForm, FormViewMode.ReadOnly, null);
        setFormData(fvInjuryHistoryForm, FormViewMode.ReadOnly, null);
        setFormData(fvHealcheckForm, FormViewMode.ReadOnly, null);
        setFormData(fvSickCheckForm, FormViewMode.ReadOnly, null);

        Update_CancelEditHistory.Visible = false;


        //detail div
        div_gvEmployeeDetail.Visible = false;
        Update_BackTohistoryDetail.Visible = false;
        div_SearchHistoryEmployee.Visible = false;

        //history work
        Update_GridHistoryWork.Visible = false;
        Update_SaveHistoryCheck.Visible = false;
        div_fvEditHistoryWork.Visible = false;

        //
        Update_GridHistoryInjuly.Visible = false;
        Update_SaveHistoryInjury.Visible = false;

        //detail history
        divDetailHistory.Visible = false;

        //detail history work
        divHistoryWork.Visible = false;
        setGridData(gvWork, null);

        //detail history injury
        divHistoryInjury.Visible = false;
        setGridData(gvInjury, null);
        div_fvEditHistoryHealthInjury.Visible = false;

        //detail history health
        divHistoryHealth.Visible = false;
        UpdatePanel_BackToListHealthDetail.Visible = false;
        setGridData(gvHealth, null);
        div_DetailHealthCheck.Visible = false;
        div_EditHealthCheck.Visible = false;
        setFormData(fvDetailHealthCheck, FormViewMode.ReadOnly, null);

        //detail history sick
        divHistorySick.Visible = false;
        setFormData(fvSick, FormViewMode.ReadOnly, null);
        
        //report print

        div_SearchReport.Visible = false;
        setGridData(GvPrintDetail, null);
        printableAreaDetail.Visible = false;

        setFormData(fvSickPrinReport, FormViewMode.ReadOnly, null);

        switch (activeTab)
        {
            case "docHistory":

                ViewState["EmpSearchList_index"] = null;

                setFormData(fvEmpDetailProfile, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                setFormData(fvEmployeeProfile, FormViewMode.ReadOnly, null);
                setFormData(fvHistorySick, FormViewMode.ReadOnly, null);
                divHistoryWorkProfile.Visible = false;
                divHistorySickProfile.Visible = false;
                divHistoryHealthProfile.Visible = false;
                divHistoryInjuryProfile.Visible = false;
                divHistoryViewHealthProfile.Visible = false;
                UpdatePanel_BackToListHealthDetail_Profile.Visible = false;

                switch (activeTabProfile)
                {
                    case "docProfile":
                        getHistoryEmployee(emp_idx, fvEmployeeProfile);
                        break;

                    case "docHistoryWork":
                        getHistoryWork(emp_idx, gvHistoryWork);

                        ViewState["vs_employee_idxPrint"] = emp_idx;
                        divHistoryWorkProfile.Visible = true;
                        break;

                    case "docSickness":
                        getHistorySickProfile(emp_idx);
                        divHistorySickProfile.Visible = true;

                        break;

                    case "docHealthCheck":
                        getHistoryHealth(emp_idx, gvHistoryHealth);
                        divHistoryHealthProfile.Visible = true;
                        break;

                    case "docInjury":
                        getHistoryInjury(emp_idx, gvInjuryProfile);
                        divHistoryInjuryProfile.Visible = true;
                        break;
                }
                setActiveTabProfile("docHistory", 0, 0, 0, "0", 0, _emp_idx, activeTabProfile);

                setOntop.Focus();

                break;

            case "docDetailList":
               
                TextBox tbEmpCode_HistorySearch = (TextBox)fvSearchHistoryEmployee.FindControl("tbEmpCode_HistorySearch");
                TextBox tbEmpName_HistorySearch = (TextBox)fvSearchHistoryEmployee.FindControl("tbEmpName_HistorySearch");
               
                div_gvEmployeeDetail.Visible = true;

                //ViewState["EmpSearchList_index"] = null;
                getEmployeeDetail();
                div_SearchHistoryEmployee.Visible = true;
                
                switch (doc_history_idx)
                {
                    case "1"://History Work

                        div_gvEmployeeDetail.Visible = false;
                        div_SearchHistoryEmployee.Visible = false;

                        divHistoryWork.Visible = true;
                        Update_BackTohistoryDetail.Visible = true;
                        getHistoryWork(emp_idx, gvWork);
                        setOntop.Focus();
                        break;
                    case "2": //History Sick

                        div_gvEmployeeDetail.Visible = false;
                        div_SearchHistoryEmployee.Visible = false;

                        divHistorySick.Visible = true;
                        Update_BackTohistoryDetail.Visible = true;
                        getHistorySick(emp_idx);
                        setOntop.Focus();
                        break;
                    case "3":
                        div_gvEmployeeDetail.Visible = false;
                        div_SearchHistoryEmployee.Visible = false;

                        divHistoryHealth.Visible = true;
                        Update_BackTohistoryDetail.Visible = true;
                        getHistoryHealth(emp_idx, gvHealth);
                        setOntop.Focus();
                        break;
                    case "4":
                        div_gvEmployeeDetail.Visible = false;
                        div_SearchHistoryEmployee.Visible = false;

                        divHistoryInjury.Visible = true;
                        Update_BackTohistoryDetail.Visible = true;
                        getHistoryInjury(emp_idx, gvInjury);
                        getHistoryInjury(emp_idx, gvPrintInjry);
                        setOntop.Focus();
                        break;
                    case "5":
                        div_gvEmployeeDetail.Visible = false;
                        div_SearchHistoryEmployee.Visible = false;

                        divDetailHistory.Visible = true;
                        Update_BackTohistoryDetail.Visible = true;
                        getHistoryEmployee(emp_idx, fvHistoryEmployee);
                        setOntop.Focus();

                        break;
                }

                setOntop.Focus();


                break;
            case "docCreateList":

                ViewState["EmpSearchList_index"] = null;

                divCreate.Visible = true;
                div_SearchEmployee.Visible = true;
                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                data_hr_healthcheck _data_hr_healthcheck_type = new data_hr_healthcheck();
                _data_hr_healthcheck_type.healthcheck_historytype_list = new hr_healthcheck_historytype_detail[1];
                hr_healthcheck_historytype_detail _hr_healthcheck_type_list = new hr_healthcheck_historytype_detail();

                _data_hr_healthcheck_type.healthcheck_historytype_list[0] = _hr_healthcheck_type_list;

                _data_hr_healthcheck_type = callServicePostHRHealthCheck(_urlGetHistoryType, _data_hr_healthcheck_type);

                setRepeaterData(rptBindNameForm, _data_hr_healthcheck_type.healthcheck_historytype_list);
                //setRadioButtonListData(rdoHistoryType, _data_hr_healthcheck_type.healthcheck_historytype_list, "type_name", "m0_type_idx");
                ViewState["vs_TypeFormHistory"] = type_form;

                switch (type_form)
                {
                    //Form History Work fvInsertHistoryHealthForm
                    case 1:
                        div_showname_Form.Visible = true;

                        setFormData(fvWorkHistoryForm, FormViewMode.Insert, null);
                        CleardataSetHistoryWorkList();


                        fvDetailEmployeeHistory.Visible = true;
                        setFormData(fvDetailEmployeeHistory, FormViewMode.ReadOnly, ViewState["vs_SearchEmployee_List"]);
                        div_showname_Form.Visible = true;
                        Update_BackToSearch.Visible = true;

                        div_SearchEmployee.Visible = false;

                        clearViewState();
                        Update_DetailEmployee.Visible = false;
                        gvEmployeeList.PageIndex = 0;
                        setGridData(gvEmployeeList, null);

                        break;

                    //Form History Work
                    case 2:
                        div_showname_Form.Visible = true;
                        setFormData(fvSickCheckForm, FormViewMode.Insert, null);
                        CleardataSetEverSickList();
                        CleardataSetDiseaseFamilyList();

                        fvDetailEmployeeHistory.Visible = true;
                        setFormData(fvDetailEmployeeHistory, FormViewMode.ReadOnly, ViewState["vs_SearchEmployee_List"]);
                        div_showname_Form.Visible = true;
                        Update_BackToSearch.Visible = true;

                        div_SearchEmployee.Visible = false;

                        clearViewState();
                        Update_DetailEmployee.Visible = false;
                        gvEmployeeList.PageIndex = 0;
                        setGridData(gvEmployeeList, null);

                        break;
                    //Form History Injury
                    case 3:
                        div_showname_Form.Visible = true;
                        setFormData(fvHealcheckForm, FormViewMode.Insert, null);
                        getDetailTypeHealthCheck();
                        getNameDoctor(ddl_Doctorname);
                        CleardataSetRikeHealthList();

                        fvDetailEmployeeHistory.Visible = true;
                        setFormData(fvDetailEmployeeHistory, FormViewMode.ReadOnly, ViewState["vs_SearchEmployee_List"]);
                        div_showname_Form.Visible = true;
                        Update_BackToSearch.Visible = true;

                        div_SearchEmployee.Visible = false;

                        clearViewState();
                        Update_DetailEmployee.Visible = false;
                        gvEmployeeList.PageIndex = 0;
                        setGridData(gvEmployeeList, null);


                        break;
                    case 4:
                        div_showname_Form.Visible = true;
                        setFormData(fvInjuryHistoryForm, FormViewMode.Insert, null);
                        CleardataSetHistoryInjuryList();

                        fvDetailEmployeeHistory.Visible = true;
                        setFormData(fvDetailEmployeeHistory, FormViewMode.ReadOnly, ViewState["vs_SearchEmployee_List"]);
                        div_showname_Form.Visible = true;
                        Update_BackToSearch.Visible = true;

                        div_SearchEmployee.Visible = false;

                        clearViewState();
                        Update_DetailEmployee.Visible = false;
                        gvEmployeeList.PageIndex = 0;
                        setGridData(gvEmployeeList, null);


                        break;
                }


                //setOntop.Focus();

                break;

            case "docReportList":

                ViewState["EmpSearchList_index"] = null;

                setFormData(fvAddUserCheckHistory, FormViewMode.Insert, null);
                setFormData(FvSearchReport, FormViewMode.Insert, null);
                div_SearchReport.Visible = true;
                div_GvPrintDetail.Visible = true;

                getOrganizationList((DropDownList)FvSearchReport.FindControl("ddlOrgSearchReport"));

                if (emp_idx > 0 && type_form == 0)
                {
                    div_SearchReport.Visible = false;
                    div_GvPrintDetail.Visible = false;

                    printableAreaDetail.Visible = true;
                    getHistoryWorkPrintReport(emp_idx, gvWorkPrintReport);

                    getHistoryEmployee(emp_idx, fvPrintHistoryEmployee);
                    getHistoryEmployee(emp_idx, FvDetailCover);
                    getHistorySickPrintReport(emp_idx);
                    getHistoryInjuryPrintReport(emp_idx, gvInjuryPrintReport);

                    getDetailHistoryHealthPrint(emp_idx);

                    //setOntop.Focus();



                    //setFormData(fvPrintSick, FormViewMode.ReadOnly, ViewState["vs_details_history_sick"]);
                }
                else if (type_form == 1)
                {
                    setGridData(GvPrintDetail, ViewState["EmpSearchList_report"]);
                }
                else
                {
                    setGridData(GvPrintDetail, null);
                }

                setOntop.Focus();


                break;

        }
    }

    protected void setActiveTab(string activeTab, int uidx, int u1idx, int staidx, string m0_lab, int type_form, int emp_idx , string activeTabProfile)
    {
        setActiveView(activeTab, uidx, u1idx, staidx, m0_lab, type_form, emp_idx, activeTabProfile);
        switch (activeTab)
        {
            case "docHistory":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                break;
            case "docDetailList":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                break;
            case "docCreateList":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                break;
            case "docReportList":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                break;


        }
    }


    protected void setActiveTabProfile(string activeTab, int uidx, int u1idx, int staidx, string m0_lab, int type_form, int emp_idx, string activeTabProfile)
    {
        switch (activeTabProfile)
        {
            case "docProfile":
                liProfie1.Attributes.Add("class", "active");
                liProfie2.Attributes.Add("class", "");
                liProfie3.Attributes.Add("class", "");
                liProfie4.Attributes.Add("class", "");
                liProfie5.Attributes.Add("class", "");
                break;
            case "docHistoryWork":
                liProfie1.Attributes.Add("class", "");
                liProfie2.Attributes.Add("class", "active");
                liProfie3.Attributes.Add("class", "");
                liProfie4.Attributes.Add("class", "");
                liProfie5.Attributes.Add("class", "");
                break;
            case "docSickness":
                liProfie1.Attributes.Add("class", "");
                liProfie2.Attributes.Add("class", "");
                liProfie3.Attributes.Add("class", "active");
                liProfie4.Attributes.Add("class", "");
                liProfie5.Attributes.Add("class", "");
                break;
            case "docHealthCheck":
                liProfie1.Attributes.Add("class", "");
                liProfie2.Attributes.Add("class", "");
                liProfie3.Attributes.Add("class", "");
                liProfie4.Attributes.Add("class", "active");
                liProfie5.Attributes.Add("class", "");
                break;
            case "docInjury":
                liProfie1.Attributes.Add("class", "");
                liProfie2.Attributes.Add("class", "");
                liProfie3.Attributes.Add("class", "");
                liProfie4.Attributes.Add("class", "");
                liProfie5.Attributes.Add("class", "active");
                break;

        }
     }

    #endregion event reuse

    #region event gridview
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvHistoryHealthCheck":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    var lb_m0_topic_idx = (Label)e.Row.FindControl("lb_m0_topic_idx_check");
                    var lbFormTopicNameDetails = (Label)e.Row.FindControl("lbFormTopicName_check");
                    var lbTopicset_idx = (Label)e.Row.FindControl("lbTopicset_idx");

                    var gvDetailTopicCheck = (GridView)e.Row.FindControl("gvDetailTopicCheck");
                   

                    data_hr_healthcheck _data_hr_healthcheck_topic_form = new data_hr_healthcheck();
                    _data_hr_healthcheck_topic_form.healthcheck_historyform_list = new hr_healthcheck_historyform_detail[1];
                    hr_healthcheck_historyform_detail _hr_healthcheck_topic_form = new hr_healthcheck_historyform_detail();
                    _hr_healthcheck_topic_form.m0_topic_idx = int.Parse(lb_m0_topic_idx.Text);
                    _data_hr_healthcheck_topic_form.healthcheck_historyform_list[0] = _hr_healthcheck_topic_form;

                    _data_hr_healthcheck_topic_form = callServicePostHRHealthCheck(_urlGetDetailTopicForm, _data_hr_healthcheck_topic_form);
                    ViewState["vsOption"] = _data_hr_healthcheck_topic_form.healthcheck_historyform_list;
                    //setRepeaterData(gvDetailTopicCheck, ViewState["vsOption"]);


                    //hr_healthcheck_historyform_detail[] _templist_detail_topic = (hr_healthcheck_historyform_detail[])ViewState["vsOption"];

                    //var _linq_radio_topicdetail = (from data_topicdetail in _templist_radio
                    //                               where data_topicdetail.option_idx == 3
                    //                               && data_topicdetail.root_idx == int.Parse(lbl_root_idx_topic.Text)
                    //                               //&& data_topicdetail.m0_topic_idx == int.Parse(lbl_root_idx_topic.Text)

                    //                               select new
                    //                               {
                    //                                   data_topicdetail.topic_name,
                    //                                   data_topicdetail.m0_topic_idx
                    //                               }).ToList().Distinct();



                    //var _linq_topicdetail = (from data_topicdetail in _templist_detail_topic
                    //                               where data_topicdetail.option_idx != 3
                    //                               //&& data_topicdetail.root_idx == int.Parse(lbl_root_idx_topic.Text)
                    //                               //&& data_topicdetail.m0_topic_idx == int.Parse(lbl_root_idx_topic.Text)

                    //                               select new
                    //                               {
                    //                                   data_topicdetail.topic_name,
                    //                                   data_topicdetail.m0_topic_idx,
                    //                                   data_topicdetail.root_idx,
                    //                                   data_topicdetail.option_idx
                    //                               }).ToList().Distinct();

                    setGridData(gvDetailTopicCheck, ViewState["vsOption"]);
                    //var rdo_DetailTopic = (RadioButtonList)gvDetailTopicCheck.FindControl("rdo_DetailTopic");
                    var rdo_TopicForm = (RadioButtonList)e.Row.FindControl("rdo_TopicForm");
                    if (lbTopicset_idx.Text != "0")
                    {

                        rdo_TopicForm.Visible = true;

                        data_hr_healthcheck _data_hr_healthcheck_settopic_form = new data_hr_healthcheck();
                        _data_hr_healthcheck_settopic_form.healthcheck_historyform_list = new hr_healthcheck_historyform_detail[1];
                        hr_healthcheck_historyform_detail _hr_healthcheck_settopic_form = new hr_healthcheck_historyform_detail();

                        _hr_healthcheck_settopic_form.set_idx = int.Parse(lbTopicset_idx.Text);
                        _data_hr_healthcheck_settopic_form.healthcheck_historyform_list[0] = _hr_healthcheck_settopic_form;

                        _data_hr_healthcheck_settopic_form = callServicePostHRHealthCheck(_urlGetDetailSetTopicForm, _data_hr_healthcheck_settopic_form);

                        rdo_TopicForm.DataSource = _data_hr_healthcheck_settopic_form.healthcheck_historyform_list;
                        rdo_TopicForm.DataTextField = "set_name";
                        rdo_TopicForm.DataValueField = "set_idx";
                        rdo_TopicForm.DataBind();

                    }



                }

                break;

            case "gvDetailTopicCheck":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    var lbl_root_idx_topic = (Label)e.Row.FindControl("lbl_root_idx_topic");
                    var lbldetail_option_idx = (Label)e.Row.FindControl("lbldetail_option_idx");
                    var lbdetailtopic_m0_topic_idx = (Label)e.Row.FindControl("lbdetailtopic_m0_topic_idx");
                    var lbdetailFormTopicName = (Label)e.Row.FindControl("lbdetailFormTopicName");
                    var rdo_DetailTopic = (RadioButtonList)e.Row.FindControl("rdo_DetailTopic");
                    var btnAddTopic1 = (LinkButton)e.Row.FindControl("btnAddTopic1");
                    var btnAddTopic5 = (LinkButton)e.Row.FindControl("btnAddTopic5");

                    var txt_detail_result = (TextBox)e.Row.FindControl("txt_detail_result");

                    hr_healthcheck_historyform_detail[] _templist_radio = (hr_healthcheck_historyform_detail[])ViewState["vsOption"];


                    //var _linq_radio_topicdetail = (from data_topicdetail in _templist_radio
                    //                               where data_topicdetail.option_idx == 3
                    //                               && data_topicdetail.root_idx == int.Parse(lbl_root_idx_topic.Text)
                    //                               //&& data_topicdetail.m0_topic_idx == int.Parse(lbl_root_idx_topic.Text)

                    //                               select new
                    //                               {
                    //                                   data_topicdetail.topic_name,
                    //                                   data_topicdetail.m0_topic_idx
                    //                               }).ToList().Distinct();

                    //rdo_DetailTopic.DataSource = _linq_radio_topicdetail;
                    //rdo_DetailTopic.DataTextField = "topic_name";
                    //rdo_DetailTopic.DataValueField = "m0_topic_idx";
                    //rdo_DetailTopic.DataBind();

                    ////if (lbldetail_option_idx.Text == "3") //RadioButton
                    ////{
                    ////    lbdetailFormTopicName.Visible = true;
                    ////    ////rdo_DetailTopic.Visible = true;

                    ////    var _linq_radio_topicdetail = (from data_topicdetail in _templist_radiodocCreateList
                    ////                                   where data_topicdetail.option_idx == 3
                    ////                                   && data_topicdetail.root_idx == int.Parse(lbl_root_idx_topic.Text)
                    ////                                   //&& data_topicdetail.m0_topic_idx == int.Parse(lbl_root_idx_topic.Text)

                    ////                                   select new
                    ////                                   {
                    ////                                       data_topicdetail.topic_name,
                    ////                                       data_topicdetail.m0_topic_idx
                    ////                                   }).ToList().Distinct();




                    ////    rdo_DetailTopic.DataSource = _linq_radio_topicdetail;
                    ////    rdo_DetailTopic.DataTextField = "topic_name";
                    ////    rdo_DetailTopic.DataValueField = "m0_topic_idx";
                    ////    rdo_DetailTopic.DataBind();


                    ////}
                    if (lbldetail_option_idx.Text == "2") //TextBox
                    {
                        txt_detail_result.Visible = true;
                    }

                    if(lbdetailtopic_m0_topic_idx.Text == "3") //button topic 1 
                    {
                        btnAddTopic1.Visible = true;
                    }

                    if (lbdetailtopic_m0_topic_idx.Text == "12") //button topic 5
                    {
                        btnAddTopic5.Visible = true;
                    }

                }

                break;


        }
    }

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "gvEmployeeList":
                if (ViewState["EmpSearchList"] != null)
                {
                    setGridData(gvEmployeeList, ViewState["EmpSearchList"]);
                }
                else
                {
                    _dataEmployee = new data_employee();
                    _dataEmployee = getViewEmployeeList(_dataEmployee);
                    setGridData(gvEmployeeList, _dataEmployee.employee_list);
                }
                break;

            case "gvEmployeeDetail":
                if (ViewState["EmpSearchList_index"] != null)
                {
                    setGridData(gvEmployeeDetail, ViewState["EmpSearchList_index"]);
                }
                else
                {
                    setGridData(gvEmployeeDetail, ViewState["vs_Employee_Detail"]);
                }
                
                break;
            case "GvPrintDetail":
                setGridData(GvPrintDetail, ViewState["EmpSearchList_report"]);
                setOntop.Focus();
                break;
        }
    }
    #endregion gridview

    #region event formview
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;
            switch (FvName.ID)
            {
                case "fvSick":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {

                    }
                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {

                        CleardataSetPlaceListUpdate();
                        CleardataSetDiseaseFamilyListUpdate();

                        //Label idx_rdoSmoking_edit = (Label)fvSick.FindControl("idx_rdoSmoking_edit");
                        RadioButtonList rdoSmoking_edit = (RadioButtonList)fvSick.FindControl("rdoSmoking_edit");
                        UpdatePanel updateInsert_stopsmoking_edit = (UpdatePanel)fvSick.FindControl("updateInsert_stopsmoking_edit");
                        UpdatePanel updateInsert_eversmoking_edit = (UpdatePanel)fvSick.FindControl("updateInsert_eversmoking_edit");

                        if (rdoSmoking_edit.SelectedValue == "3")
                        {
                            updateInsert_stopsmoking_edit.Visible = true;
                            updateInsert_eversmoking_edit.Visible = false;
                           // rdoSmoking_edit.SelectedValue = "3";
                        }
                        else if (rdoSmoking_edit.SelectedValue == "2")
                        {
                            updateInsert_eversmoking_edit.Visible = true;
                            updateInsert_stopsmoking_edit.Visible = false;
                            //rdoSmoking_edit.SelectedValue = "2";
                        }
                        else
                        {
                            updateInsert_eversmoking_edit.Visible = false;
                            updateInsert_stopsmoking_edit.Visible = false;
                            //rdoSmoking_edit.SelectedValue = idx_rdoSmoking_edit.Text;
                        }

                        //Label idx_rdo_alcohol_edit = (Label)fvSick.FindControl("idx_rdo_alcohol_edit");
                        RadioButtonList rdo_alcohol_edit = (RadioButtonList)fvSick.FindControl("rdo_alcohol_edit");
                        UpdatePanel updateInsert_alcohol_edit = (UpdatePanel)fvSick.FindControl("updateInsert_alcohol_edit");
                        if (rdo_alcohol_edit.SelectedValue == "6")
                        {
                            updateInsert_alcohol_edit.Visible = true;
                            //rdo_alcohol_edit.SelectedValue = "6";

                        }
                        else
                        {
                            updateInsert_alcohol_edit.Visible = false;
                            //rdo_alcohol_edit.SelectedValue = idx_rdo_alcohol_edit.Text;
                        }


                        TextBox txt_u0_historysick_idx_edit = (TextBox)fvSick.FindControl("txt_u0_historysick_idx_edit");

                        data_hr_healthcheck _data_hr_eversick_edit = new data_hr_healthcheck();
                        _data_hr_eversick_edit.healthcheck_u3_history_sick_list = new hr_healthcheck_u3_history_sick_detail[1];
                        hr_healthcheck_u3_history_sick_detail _hr_eversick_list_edit = new hr_healthcheck_u3_history_sick_detail();
                        _hr_eversick_list_edit.u0_historysick_idx = int.Parse(txt_u0_historysick_idx_edit.Text);

                        _data_hr_eversick_edit.healthcheck_u3_history_sick_list[0] = _hr_eversick_list_edit;

                        _data_hr_eversick_edit = callServicePostHRHealthCheck(_urlGetHistorySickEverSick, _data_hr_eversick_edit);
                        
                        foreach (var ever_sick_edit in _data_hr_eversick_edit.healthcheck_u3_history_sick_list)
                        {
                            if (ViewState["vsDetailEverSickListUpdate"] != null)
                            {
                                CultureInfo culture = new CultureInfo("en-US");
                                Thread.CurrentThread.CurrentCulture = culture;
                                DataSet dsContacts = (DataSet)ViewState["vsDetailEverSickListUpdate"];
                                DataRow drContacts = dsContacts.Tables["dsDetailEverSickTableUpdate"].NewRow();

                                //drContacts["drEversickEditIDX"] = ever_sick_edit.u3_historysick_idx;
                                drContacts["drEversickEditText"] = ever_sick_edit.eversick;
                                drContacts["drLastyearEditText"] = ever_sick_edit.sick_year;

                                dsContacts.Tables["dsDetailEverSickTableUpdate"].Rows.Add(drContacts);
                                ViewState["vsDetailEverSickListUpdate"] = dsContacts;
                                gvEverSickListEdit.Visible = true;
                                setGridData(gvEverSickListEdit, dsContacts.Tables["dsDetailEverSickTableUpdate"]);

                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["vsDetailEverSickListUpdate"]));

                            }
                        }

                        /////////
                        data_hr_healthcheck _data_hr_relationfamily_edit = new data_hr_healthcheck();

                        _data_hr_relationfamily_edit.healthcheck_u4_history_sick_list = new hr_healthcheck_u4_history_sick_detail[1];
                        hr_healthcheck_u4_history_sick_detail _hr_relationfamily_list_edit = new hr_healthcheck_u4_history_sick_detail();
                        _hr_relationfamily_list_edit.u0_historysick_idx = int.Parse(txt_u0_historysick_idx_edit.Text);

                        _data_hr_relationfamily_edit.healthcheck_u4_history_sick_list[0] = _hr_relationfamily_list_edit;

                        _data_hr_relationfamily_edit = callServicePostHRHealthCheck(_urlGetHistorySickRelationFamily, _data_hr_relationfamily_edit);

                        foreach (var relationfamily_edit in _data_hr_relationfamily_edit.healthcheck_u4_history_sick_list)
                        {
                            if (ViewState["vsDetailDiseaseFamilyListUpdate"] != null)
                            {
                                CultureInfo culture = new CultureInfo("en-US");
                                Thread.CurrentThread.CurrentCulture = culture;
                                DataSet dsContacts = (DataSet)ViewState["vsDetailDiseaseFamilyListUpdate"];
                                DataRow drContacts = dsContacts.Tables["dsDetailDiseaseFamilyTableUpdate"].NewRow();

                                //drContacts["drRelation_familyEditIDX"] = relationfamily_edit.u4_historysick_idx;
                                drContacts["drRelation_familyEditText"] = relationfamily_edit.relation_family;
                                drContacts["drDisease_FamilyEditText"] = relationfamily_edit.disease_family;

                                dsContacts.Tables["dsDetailDiseaseFamilyTableUpdate"].Rows.Add(drContacts);
                                ViewState["vsDetailDiseaseFamilyListUpdate"] = dsContacts;
                                gvDiseaseFamilyEdit.Visible = true;
                                setGridData(gvDiseaseFamilyEdit, dsContacts.Tables["dsDetailDiseaseFamilyTableUpdate"]);

                            }
                        }

                    }
                    break;
                case "fvDetailHealthCheck":
                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {

                        CleardataSetRikeHealthListUpdate();

                        getDetailTypeHealthCheckEdit();
                        getNameDoctorEdit(ddl_Doctorname_edit);

                        TextBox txt_u2_historyhealth_idx_edit = (TextBox)fvDetailHealthCheck.FindControl("txt_u2_historyhealth_idx_edit");

                        data_hr_healthcheck _data_m0_detailhistory_helth_edit = new data_hr_healthcheck();

                        _data_m0_detailhistory_helth_edit.healthcheck_u3_history_health_list = new hr_healthcheck_u3_history_health_detail[1];
                        hr_healthcheck_u3_history_health_detail _hr_healthcheck_list_u3_edit = new hr_healthcheck_u3_history_health_detail();
                        _hr_healthcheck_list_u3_edit.u2_historyhealth_idx = int.Parse(txt_u2_historyhealth_idx_edit.Text);

                        _data_m0_detailhistory_helth_edit.healthcheck_u3_history_health_list[0] = _hr_healthcheck_list_u3_edit;

                        _data_m0_detailhistory_helth_edit = callServicePostHRHealthCheck(_urlGetHistoryHealthRikeDetail, _data_m0_detailhistory_helth_edit);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_m0_detailhistory_helth_edit));

                        if(_data_m0_detailhistory_helth_edit.return_code == 0)
                        {
                            foreach (var rike_edit in _data_m0_detailhistory_helth_edit.healthcheck_u3_history_health_list)
                            {
                                if (ViewState["vsDetailRikeListUpdate"] != null)
                                {
                                    CultureInfo culture = new CultureInfo("en-US");
                                    Thread.CurrentThread.CurrentCulture = culture;
                                    DataSet dsContacts = (DataSet)ViewState["vsDetailRikeListUpdate"];
                                    DataRow drContacts = dsContacts.Tables["dsDetailRikeTableUpdate"].NewRow();

                                    //drContacts["drRelation_familyEditIDX"] = relationfamily_edit.u4_historysick_idx;
                                    drContacts["drrikeHealthEditText"] = rike_edit.rike_health;
                                    drContacts["drResultHealthEditText"] = rike_edit.result_health;

                                    dsContacts.Tables["dsDetailRikeTableUpdate"].Rows.Add(drContacts);
                                    ViewState["vsDetailRikeListUpdate"] = dsContacts;
                                    gvRikeListEdit.Visible = true;
                                    setGridData(gvRikeListEdit, dsContacts.Tables["dsDetailRikeTableUpdate"]);

                                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["vsDetailEverSickListUpdate"]));

                                }
                            }
                        }

                    }
                    break;
                case "fvEditHistoryHealthInjury":
                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {
                        if (txt_not_working_idx.Text == "0")
                        {
                            rdo_notworking_edit.ClearSelection();
                        }
                        else
                        {
                            rdo_notworking_edit.SelectedValue = txt_not_working_idx.Text;
                        }
                        
                    }
                    break;

                    
            }
        }
        else if (sender is RadioButtonList)
        {
            RadioButtonList rdoListName = (RadioButtonList)sender;
            switch (rdoListName.ID)
            {
                case "rdoListFoodMaterialHaving":
                    //Control panelFoodMaterial = (Control)fvCRUD.FindControl("panelFoodMaterial");
                    //if (rdoListName.SelectedValue == "1")
                    //{
                    //    panelFoodMaterial.Visible = true;
                    //}
                    //else
                    //{
                    //    panelFoodMaterial.Visible = false;
                    //}
                    break;
            }
        }
        else if (sender is DropDownList)
        {
            DropDownList ddlName = (DropDownList)sender;
            switch (ddlName.ID)
            {
                //case "ddlFoodMaterial":
                //    Label lblFoodMaterialUnit = (Label)fvCRUD.FindControl("lblFoodMaterialUnit");
                //    string unitMaterial = getUnitMaterial(int.Parse(ddlName.SelectedValue));
                //    lblFoodMaterialUnit.Text = unitMaterial;
                //    break;

                //case "ddlFoodMaterialUpdate":
                //    Label lblFoodMaterialUnitUpdate = (Label)fvCRUD.FindControl("lblFoodMaterialUnitUpdate");
                //    string unitMaterialUpdate = getUnitMaterial(int.Parse(ddlName.SelectedValue));
                //    lblFoodMaterialUnitUpdate.Text = unitMaterialUpdate;
                //    break;
            }
        }
    }

    void btnAddSave_Click(Object sender, EventArgs e)
    {

        //gvWork_scroll.Visible = true;
        // When the button is clicked,
        // change the button text, and disable it.

        //Button clickedButton = (Button)sender;
        //clickedButton.Text = "...button clicked...";
        //clickedButton.Enabled = false;

        //// Display the greeting label text.
        //GreetingLabel.Visible = true;
    }

    #endregion end formview



}