﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_en_m0_standard : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    data_en_planning _dtenplan = new data_en_planning();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlSelect_MasterData = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_standard"];
    static string urlnsert_MasterData = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Master_standard"];
    static string urlUpdate_MasterData = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Master_standard"];
    static string urlDelete_MasterData = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Master_standard"];
    static string _urlSelect_Master_typeitems_content = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_typeitems_content"];
    static string _urlSelect_TypeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeMachineItem"];
    static string _urlSelect_GroupCodeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_GroupCodeMachine"];
    static string _urlSelect_Group = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Group_Machine"];
    static string _urlSelect_Master_content_standard = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_content_standard"];



    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();
            select_typemachine(ddltype_search);
        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

    }

    #region Manage SQL
    protected void SelectMasterList()
    {

        _dtenplan.BoxEN_StandardList = new Standard_Detail[1];
        Standard_Detail dtsupport = new Standard_Detail();

        dtsupport.TmcIDX = int.Parse(ddltype_search.SelectedValue);
        dtsupport.TCIDX = int.Parse(ddltypecode_search.SelectedValue);
        dtsupport.GCIDX = int.Parse(ddlgroupcode_search.SelectedValue);
        dtsupport.m0tyidx = int.Parse(ddltypeitem_search.SelectedValue);
        dtsupport.m0coidx = int.Parse(ddlcontent_search.SelectedValue);
        dtsupport.standard_name_th = txtsearchstandard.Text;
        _dtenplan.BoxEN_StandardList[0] = dtsupport;

        _dtenplan = callServicePostENPlanning(urlSelect_MasterData, _dtenplan);
        setGridData(GvMaster, _dtenplan.BoxEN_StandardList);
    }

    protected void Insert_CaseLV1()
    {
        _dtenplan.BoxEN_StandardList = new Standard_Detail[1];
        Standard_Detail insert = new Standard_Detail();
        insert.m0coidx = int.Parse(ddlcontent.SelectedValue);
        insert.standard_name_en = txtnameen.Text;
        insert.standard_name_th = txtnameth.Text;
        insert.standard_status = int.Parse(ddStatusadd.SelectedValue);
        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtenplan.BoxEN_StandardList[0] = insert;
        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));
        _dtenplan = callServicePostENPlanning(urlnsert_MasterData, _dtenplan);

    }

    protected void Update_Master_List()
    {
        _dtenplan.BoxEN_StandardList = new Standard_Detail[1];
        Standard_Detail update = new Standard_Detail();

        update.m0coidx = int.Parse(ViewState["ddlcontent_edit"].ToString());
        update.standard_name_th = ViewState["txtnameth_edit"].ToString();
        update.standard_name_en = ViewState["txtnameen_edit"].ToString();
        update.standard_status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        update.m0stidx = int.Parse(ViewState["m0stidx"].ToString());
        _dtenplan.BoxEN_StandardList[0] = update;

        _dtenplan = callServicePostENPlanning(urlUpdate_MasterData, _dtenplan);

    }

    protected void Delete_Master_List()
    {
        _dtenplan.BoxEN_StandardList = new Standard_Detail[1];
        Standard_Detail delete = new Standard_Detail();

        delete.m0stidx = int.Parse(ViewState["m0stidx"].ToString());
        delete.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtenplan.BoxEN_StandardList[0] = delete;

        _dtenplan = callServicePostENPlanning(urlDelete_MasterData, _dtenplan);
    }
    #endregion

    #region Master Form Result
    protected void select_typeitemmachine(DropDownList ddlName, int GCIDX)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeItemList = new TypeItem_Detail[1];
        TypeItem_Detail qtymachine = new TypeItem_Detail();
        qtymachine.GCIDX = GCIDX;
        _dtenplan.BoxEN_TypeItemList[0] = qtymachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_Master_typeitems_content, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeItemList, "type_name_th", "m0tyidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือก Point จุดตรวจ...", "0"));

    }

    protected void select_content(DropDownList ddlName, int m0tyidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_ContentList = new Content_Detail[1];
        Content_Detail qtymachine = new Content_Detail();
        qtymachine.m0tyidx = m0tyidx;
        _dtenplan.BoxEN_ContentList[0] = qtymachine;

       // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtenplan));

        _dtenplan = callServicePostENPlanning(_urlSelect_Master_content_standard, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_ContentList, "contentth", "m0coidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือก Content รายละเอียดที่ตรวจ...", "0"));
    }

    protected void select_typemachine(DropDownList ddlName)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeItemList = new TypeItem_Detail[1];
        TypeItem_Detail typemachine = new TypeItem_Detail();
        _dtenplan.BoxEN_TypeItemList[0] = typemachine;
        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtenplan));
        _dtenplan = callServicePostENPlanning(_urlSelect_TypeMachine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeItemList, "NameEN", "TmcIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทเครื่องจักร...", "0"));

    }

    protected void select_typecodemachine(DropDownList ddlName, int tmcidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeCodeList = new TypeCode_Detail[1];
        TypeCode_Detail groupmachine = new TypeCode_Detail();
        groupmachine.TmcIDX = tmcidx;

        _dtenplan.BoxEN_TypeCodeList[0] = groupmachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_Group, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeCodeList, "NameTypecode", "TCIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรหัสกลุ่มเครื่องจักร...", "0"));

    }

    protected void select_groupmachine(DropDownList ddlName, int tcidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_GroupCodeList = new GroupCode_Detail[1];
        GroupCode_Detail groupmachine = new GroupCode_Detail();
        groupmachine.TCIDX = tcidx;

        _dtenplan.BoxEN_GroupCodeList[0] = groupmachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_GroupCodeMachine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_GroupCodeList, "NameTypecode", "GCIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรหัสกลุ่ม...", "0"));

    }

    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected data_en_planning callServicePostENPlanning(string _cmdUrl, data_en_planning _dtenplan)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtenplan);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtenplan = (data_en_planning)_funcTool.convertJsonToObject(typeof(data_en_planning), _localJson);

        return _dtenplan;
    }

    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        var txtcoidx = (TextBox)e.Row.FindControl("txtcoidx");
                        var txtm0tyidx = (TextBox)e.Row.FindControl("txtm0tyidx");
                        var ddlcontent_edit = (DropDownList)e.Row.FindControl("ddlcontent_edit");

                        select_content(ddlcontent_edit, int.Parse(txtm0tyidx.Text));
                        ddlcontent_edit.SelectedValue = txtcoidx.Text;


                    }
                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                btnshow.Visible = false;
                SETBoxAllSearch.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                btnshow.Visible = true;
                SETBoxAllSearch.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int m0stidx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtnameth_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtnameth_edit");
                var txtnameen_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtnameen_edit");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");
                var ddlcontent_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlcontent_edit");

                GvMaster.EditIndex = -1;

                ViewState["ddlcontent_edit"] = ddlcontent_edit.SelectedValue;
                ViewState["m0stidx"] = m0stidx;
                ViewState["txtnameth_edit"] = txtnameth_edit.Text;
                ViewState["txtnameen_edit"] = txtnameen_edit.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList();
                btnshow.Visible = true;
                SETBoxAllSearch.Visible = true;
                break;
        }
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;


        switch (ddName.ID)
        {
            case "ddltypemachine":

                select_typecodemachine(ddltypecode, int.Parse(ddltypemachine.SelectedValue));

                break;
            case "ddltypecode":
                select_groupmachine(ddlgroupmachine, int.Parse(ddltypecode.SelectedValue));
                break;

            case "ddlgroupmachine":
                select_typeitemmachine(ddltypeitem, int.Parse(ddlgroupmachine.SelectedValue));

                break;

            case "ddltypeitem":
                select_content(ddlcontent, int.Parse(ddltypeitem.SelectedValue));

                break;
            case "ddltype_search":
                select_typecodemachine(ddltypecode_search, int.Parse(ddltype_search.SelectedValue));

                break;
            case "ddltypecode_search":
                select_groupmachine(ddlgroupcode_search, int.Parse(ddltypecode_search.SelectedValue));
                break;
            case "ddlgroupcode_search":
                select_typeitemmachine(ddltypeitem_search, int.Parse(ddlgroupcode_search.SelectedValue));

                break;

            case "ddltypeitem_search":
                select_content(ddlcontent_search, int.Parse(ddltypeitem_search.SelectedValue));
                break;
        }
    }
    #endregion


    protected void SetDefaultAdd()
    {
        txtnameth.Text = String.Empty;
        txtnameen.Text = String.Empty;
        ddStatusadd.SelectedValue = "1";
        SETBoxAllSearch.Visible = false;
        select_typemachine(ddltypemachine);
        ddltypemachine.SelectedValue = "0";
        ddltypecode.SelectedValue = "0";
        ddlgroupmachine.SelectedValue = "0";
        ddltypeitem.SelectedValue = "0";
        ddlcontent.SelectedValue = "0";
    }

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                SETBoxAllSearch.Visible = true;
                break;

            case "btnAdd":
                Insert_CaseLV1();
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                SelectMasterList();
                SETBoxAllSearch.Visible = true;
                break;
            case "CmdDel":
                int m0stidx = int.Parse(cmdArg);
                ViewState["m0stidx"] = m0stidx;
                Delete_Master_List();
                SelectMasterList();

                break;

            case "btnsearch":
                SelectMasterList();
                break;
            case "BtnBack":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;
        }



    }
    #endregion
}