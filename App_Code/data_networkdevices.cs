﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_networkdevices
/// </summary>
/// 

[Serializable]
[XmlRoot("data_networkdevices")]
public class data_networkdevices
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("m0type_list")]
    public m0type_detail[] m0type_list { get; set; }

    [XmlElement("m0category_list")]
    public m0category_detail[] m0category_list { get; set; }

    [XmlElement("m0place_list")]
    public m0place_detail[] m0place_list { get; set; }

    [XmlElement("m0room_list")]
    public m0room_detail[] m0room_list { get; set; }

    [XmlElement("m0company_list")]
    public m0company_detail[] m0company_list { get; set; }

    [XmlElement("m0floor_list")]
    public m0floor_detail[] m0floor_list { get; set; }

    [XmlElement("m0chamber_list")]
    public m0chamber_detail[] m0chamber_list { get; set; }

    [XmlElement("r0position_list")]
    public r0position_detail[] r0position_list { get; set; }

    [XmlElement("m0image_list")]
    public m0image_detail[] m0image_list { get; set; }


}

[Serializable]
public class m0type_detail
{
    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("type_status")]
    public int type_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class m0category_detail
{
    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("category_status")]
    public int category_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }
}

[Serializable]
public class m0place_detail
{
    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("place_status")]
    public int place_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    
}

[Serializable]
public class m0room_detail
{
    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }

    [XmlElement("room_status")]
    public int room_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }
}

[Serializable]
public class m0company_detail
{
    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("company_status")]
    public int company_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class m0floor_detail
{
    [XmlElement("FLIDX")]
    public int FLIDX { get; set; }

    [XmlElement("Floor_name")]
    public string Floor_name { get; set; }

    [XmlElement("FL_status")]
    public int FL_status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }
}

[Serializable]
public class m0chamber_detail
{
    [XmlElement("CHIDX")]
    public int CHIDX { get; set; }

    [XmlElement("Chamber_name")]
    public string Chamber_name { get; set; }

    [XmlElement("CH_status")]
    public int CH_status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }
}

[Serializable]
public class r0position_detail
{
    [XmlElement("REIDX")]
    public int REIDX { get; set; }

    [XmlElement("CHIDX")]
    public int CHIDX { get; set; }

    [XmlElement("row_")]
    public int row_ { get; set; }

    [XmlElement("cell_")]
    public int cell_ { get; set; }

    [XmlElement("status_active")]
    public int status_active { get; set; }

    [XmlElement("Chamber_name")]
    public string Chamber_name { get; set; }

    [XmlElement("FLIDX")]
    public int FLIDX { get; set; }

    [XmlElement("Floor_name")]
    public string Floor_name { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("Locname")]
    public string Locname { get; set; }

    [XmlElement("RE_status")]
    public int RE_status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("u0idx")]
    public int u0idx { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("no_regis")]
    public string no_regis { get; set; }

    [XmlElement("imgidx")]
    public int imgidx { get; set; }

    [XmlElement("img_name")]
    public string img_name { get; set; }

    [XmlElement("FileName")]
    public string FileName { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }



}

[Serializable]
public class m0image_detail
{
    [XmlElement("imgidx")]
    public int imgidx { get; set; }

    [XmlElement("FLIDX")]
    public int FLIDX { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }


    [XmlElement("BUIDX")]
    public int BUIDX { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("img_status")]
    public int img_status { get; set; }

    [XmlElement("img_name")]
    public string img_name { get; set; }

    [XmlElement("Floor_name")]
    public string Floor_name { get; set; }

    [XmlElement("Locname")]
    public string Locname { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }


    [XmlElement("room_name")]
    public string room_name { get; set; }

    [XmlElement("FileName")]
    public string FileName { get; set; }
}

