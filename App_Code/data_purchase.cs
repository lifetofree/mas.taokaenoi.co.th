﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_purchase
/// </summary>
/// 

[Serializable]
[XmlRoot("data_purchase")]
public class data_purchase
{
    [XmlElement("return_code")]
    public string return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_email")]
    public string return_email { get; set; }
    [XmlElement("return_doccode")]
    public string return_doccode { get; set; }
    [XmlElement("return_result")]
    public string return_result { get; set; }
    

    [XmlElement("m0_type_purchase_equipment_list")]
    public m0_type_purchase_equipment_detail[] m0_type_purchase_equipment_list { get; set; }
    [XmlElement("u0_purchase_equipment_list")]
    public u0_purchase_equipment_details[] u0_purchase_equipment_list { get; set; }
    [XmlElement("u1_purchase_equipment_list")]
    public u1_purchase_equipment_details[] u1_purchase_equipment_list { get; set; }
    [XmlElement("u2_purchase_equipment_list")]
    public u2_purchase_equipment_details[] u2_purchase_equipment_list { get; set; }
    [XmlElement("m0_price_equipment_list")]
    public m0_price_equipment_details[] m0_price_equipment_list { get; set; }
    [XmlElement("summary_purchase_requisition_list")]
    public summary_purchase_requisition_details[] summary_purchase_requisition_list { get; set; }
    [XmlElement("search_date_list")]
    public search_date_details[] search_date_list { get; set; }
    [XmlElement("master_spec_list")]
    public master_spec_details[] master_spec_list { get; set; }
    [XmlElement("items_purchase_list")]
    public items_purchase_details[] items_purchase_list { get; set; }
    [XmlElement("search_purchase_list")]
    public search_purchase_details[] search_purchase_list { get; set; }
    [XmlElement("report_purchase_list")]
    public report_purchase_details[] report_purchase_list { get; set; }
    [XmlElement("_organization_list")]
    public _organization_details[] _organization_list { get; set; }
    [XmlElement("export_purchase_list")]
    public export_purchase_details[] export_purchase_list { get; set; }
    [XmlElement("numberio_list")]
    public numberio_details[] numberio_list { get; set; }
    [XmlElement("numberAsset_list")]
    public numberAsset_details[] numberAsset_list { get; set; }
    [XmlElement("email_purchase_complete")]
    public email_purchase_details[] email_purchase_complete { get; set; }

    

}


[Serializable]
public class email_purchase_details
{
    [XmlElement("motive_purchase")]
    public string motive_purchase { get; set; }
    [XmlElement("name_purchase")]
    public string name_purchase { get; set; }
    [XmlElement("document_code")]
    public string document_code { get; set; }
    [XmlElement("name_department")]
    public string name_department { get; set; }
    [XmlElement("list_items")]
    public string list_items { get; set; }
    [XmlElement("comment")]
    public string comment { get; set; }
    [XmlElement("io_number")]
    public string io_number { get; set; }
    [XmlElement("asset_number")]
    public string asset_number { get; set; }
}


[Serializable]
public class numberio_details
{
    [XmlElement("idx")]
    public int idx { get; set; }
    [XmlElement("io")]
    public string io { get; set; }
    [XmlElement("u2idx")]
    public int u2idx { get; set; }
}

[Serializable]
public class numberAsset_details
{
    [XmlElement("idx_asset")]
    public int idx_asset { get; set; }
    [XmlElement("asset_number")]
    public string asset_number { get; set; }
    [XmlElement("u2idx")]
    public int u2idx { get; set; }
}


#region m0_type_purchase_equipment_detail

[Serializable]
public class m0_type_purchase_equipment_detail
{
    [XmlElement("m0_equipment_type_idx")]
    public int m0_equipment_type_idx { get; set; }

    [XmlElement("name_equipment_type")]
    public string name_equipment_type { get; set; }

    [XmlElement("status_equipment")]
    public int status_equipment { get; set; }

    [XmlElement("name_purchase_type")]
    public string name_purchase_type { get; set; }

    [XmlElement("m0_purchase_type_idx")]
    public int m0_purchase_type_idx { get; set; }

    [XmlElement("status_purchase")]
    public int status_purchase { get; set; }

    [XmlElement("status_unit")]
    public int status_unit { get; set; }

    [XmlElement("nameth_unit")]
    public string nameth_unit { get; set; }

    [XmlElement("m0_unit_idx")]
    public int m0_unit_idx { get; set; }
}
#endregion

#region u0_purchase_equipment_details

[Serializable]
public class u0_purchase_equipment_details
{
    [XmlElement("emp_idx_purchase")]
    public int emp_idx_purchase { get; set; }

    [XmlElement("u0_purchase_idx")]
    public int u0_purchase_idx { get; set; }

    [XmlElement("details_purchase")]
    public string details_purchase { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("quantity_equipment")]
    public int quantity_equipment { get; set; }

    [XmlElement("status_purchase")]
    public int status_purchase { get; set; }

    [XmlElement("m0_purchase_idx")]
    public int m0_purchase_idx { get; set; }

    [XmlElement("email_purchase")]
    public string email_purchase { get; set; }

    [XmlElement("m0_equipment_idx")]
    public int m0_equipment_idx { get; set; }

    [XmlElement("org_name_purchase")]
    public string org_name_purchase { get; set; }

    [XmlElement("sec_name_purchase")]
    public string sec_name_purchase { get; set; }

    [XmlElement("drept_name_purchase")]
    public string drept_name_purchase { get; set; }

    [XmlElement("pos_name_purchase")]
    public string pos_name_purchase { get; set; }

    [XmlElement("fullname_purchase")]
    public string fullname_purchase { get; set; }

    [XmlElement("empcode_purchase")]
    public string empcode_purchase { get; set; }

    [XmlElement("org_idx_purchase")]
    public int org_idx_purchase { get; set; }

    [XmlElement("sec_idx_purchase")]
    public int sec_idx_purchase { get; set; }

    [XmlElement("dept_idx_purchase")]
    public int dept_idx_purchase { get; set; }

    [XmlElement("pos_idx_purchase")]
    public int pos_idx_purchase { get; set; }

    [XmlElement("name_purchase")]
    public string name_purchase { get; set; }

    [XmlElement("name_equipment")]
    public string name_equipment { get; set; }

    [XmlElement("actor_name_purchase")]
    public string actor_name_purchase { get; set; }

    [XmlElement("node_status_purchase")]
    public string node_status_purchase { get; set; }

    [XmlElement("node_name_purchase")]
    public string node_name_purchase { get; set; }

    [XmlElement("action_type")]
    public int action_type { get; set; }

    [XmlElement("date_purchasequipment")]
    public string date_purchasequipment { get; set; }

    [XmlElement("time_purchasequipment")]
    public string time_purchasequipment { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("m0_location_idx")]
    public int m0_location_idx { get; set; }

    [XmlElement("name_location")]
    public string name_location { get; set; }

    [XmlElement("m0_price_idx")]
    public int m0_price_idx { get; set; }

    [XmlElement("number_io")]
    public string number_io { get; set; }

    [XmlElement("number_asset")]
    public string number_asset { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }

    [XmlElement("price_purchase")]
    public string price_purchase { get; set; }

    [XmlElement("numrow")]
    public int numrow { get; set; }

    [XmlElement("yearly")]
    public string yearly { get; set; }

    [XmlElement("mountly")]
    public string mountly { get; set; }

    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }

    [XmlElement("numberphone")]
    public string numberphone { get; set; }

    [XmlElement("emp_idx_director")]
    public int emp_idx_director { get; set; }

    [XmlElement("selected")]
    public bool selected { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("emp_check_email")]
    public int emp_check_email { get; set; }

    [XmlElement("expenditure")]
    public string expenditure { get; set; }

    [XmlElement("link")]
    public string link { get; set; }

}
#endregion

#region u1_purchase_equipment_details

[Serializable]
public class u1_purchase_equipment_details
{
    [XmlElement("u1_purchase_idx")]
    public int u1_purchase_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("status_node")]
    public int status_node { get; set; }

    [XmlElement("emp_idx_purchase")]
    public int emp_idx_purchase { get; set; }

    [XmlElement("emp_idx_approve")]
    public int emp_idx_approve { get; set; }

    [XmlElement("comment_details")]
    public string comment_details { get; set; }
}

#endregion

#region m0_price_equipment_details

[Serializable]
public class m0_price_equipment_details
{
    [XmlElement("m0_price_idx")]
    public int m0_price_idx { get; set; }

    [XmlElement("price_equipment")]
    public string price_equipment { get; set; }

    [XmlElement("status_price")]
    public int status_price { get; set; }

}

#endregion

#region summary_purchase_requisition_details

[Serializable]
public class summary_purchase_requisition_details
{
    [XmlElement("u0_purchase_idx")]
    public int u0_purchase_idx { get; set; }

    [XmlElement("u1_purchase_idx")]
    public int u1_purchase_idx { get; set; }

    [XmlElement("u0_actor_idx")]
    public int u0_actor_idx { get; set; }

    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement("u0_node_status")]
    public int u0_node_status { get; set; }

    [XmlElement("u1_actor_idx")]
    public int u1_actor_idx { get; set; }

    [XmlElement("u1_node_idx")]
    public int u1_node_idx { get; set; }

    [XmlElement("u1_node_status")]
    public int u1_node_status { get; set; }

    [XmlElement("m0_price_idx")]
    public int m0_price_idx { get; set; }

    [XmlElement("emp_approve")]
    public int emp_approve { get; set; }
}

#endregion

#region search_date_details


[Serializable]
public class search_date_details
{
    [XmlElement("single_date")]
    public string single_date { get; set; }

    [XmlElement("over_date")]
    public string over_date { get; set; }

    [XmlElement("less_date")]
    public string less_date { get; set; }

    [XmlElement("start_date")]
    public string start_date { get; set; }

    [XmlElement("end_date")]
    public string end_date { get; set; }

    [XmlElement("condition_date_type")]
    public int condition_date_type { get; set; }

}
#endregion

#region master_spec_details
[Serializable]
public class master_spec_details
{
    [XmlElement("m0_spec_idx")]
    public int m0_spec_idx { get; set; }

    [XmlElement("name_spec")]
    public string name_spec { get; set; }

    [XmlElement("details_spec")]
    public string details_spec { get; set; }

    [XmlElement("m0_equipment_idx")]
    public int m0_equipment_idx { get; set; }

    [XmlElement("status_spec")]
    public int status_spec { get; set; }

    [XmlElement("action")]
    public int action { get; set; }

}

#endregion

#region u2_purchase_equipment_details
[Serializable]
public class u2_purchase_equipment_details

{
    [XmlElement("u0_purchasee")]
    public int u0_purchasee { get; set; }

    [XmlElement("quantity_purchase")]
    public int quantity_purchase { get; set; }

    [XmlElement("spec_type_idx")]
    public int spec_type_idx { get; set; }

    [XmlElement("location_idx")]
    public int location_idx { get; set; }

    [XmlElement("rpos_idx_purchase")]
    public int rpos_idx_purchase { get; set; }

    [XmlElement("equipment_type")]
    public int equipment_type { get; set; }

    [XmlElement("purchase_type")]
    public int purchase_type { get; set; }

    [XmlElement("details_spec")]
    public string details_spec { get; set; }

    [XmlElement("expenditure")]
    public string expenditure { get; set; }



}
#endregion

#region items_purchase_details
[Serializable]
public class items_purchase_details

{
    [XmlElement("items_expenditure")]
    public string items_expenditure { get; set; }

    [XmlElement("items_quantity")]
    public int items_quantity { get; set; }

    [XmlElement("u0_purchase_idx")]
    public int u0_purchase_idx { get; set; }

    [XmlElement("items_name")]
    public string items_name { get; set; }

    [XmlElement("items_details")]
    public string items_details { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("type_equipment")]
    public string type_equipment { get; set; }

    [XmlElement("type_purchase")]
    public string type_purchase { get; set; }

    [XmlElement("u2_purchase_idx")]
    public int u2_purchase_idx { get; set; }

    [XmlElement("m0_spec_idx")]
    public int m0_spec_idx { get; set; }

    [XmlElement("m0_equipment_idx")]
    public int m0_equipment_idx { get; set; }

    [XmlElement("m0_purchase_idx")]
    public int m0_purchase_idx { get; set; }

    [XmlElement("argumentation")]
    public string argumentation { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }

    [XmlElement("location_idx")]
    public int location_idx { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("unitname")]
    public string unitname { get; set; }

    [XmlElement("node_idx")]
    public int node_idx { get; set; }

    [XmlElement("actor_idx")]
    public int actor_idx { get; set; }

    [XmlElement("status_node")]
    public int status_node { get; set; }

    [XmlElement("m0_price")]
    public int m0_price { get; set; }

    [XmlElement("commentfromITSupport")]
    public string commentfromITSupport { get; set; }

    [XmlElement("costcentername")]
    public string costcentername { get; set; }

    [XmlElement("price_equipment")]
    public string price_equipment { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("IO_Code")]
    public string IO_Code { get; set; }

    [XmlElement("Asset_Code")]
    public string Asset_Code { get; set; }

    [XmlElement("min_price")]
    public string min_price { get; set; }

    [XmlElement("max_price")]
    public string max_price { get; set; }

    [XmlElement("doc_code")]
    public string doc_code { get; set; }

  

    

}
#endregion

#region search_purchase_details

[Serializable]
public class search_purchase_details
{
    
    [XmlElement("condition_date_type")]
    public int condition_date_type { get; set; }

    [XmlElement("start_date")]
    public string start_date { get; set; }

    [XmlElement("end_date")]
    public string end_date { get; set; }

    [XmlElement("emp_idx_purchase")]
    public int emp_idx_purchase { get; set; }

    [XmlElement("u0_purchase_idx")]
    public int u0_purchase_idx { get; set; }

    [XmlElement("details_purchase")]
    public string details_purchase { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("quantity_equipment")]
    public int quantity_equipment { get; set; }

    [XmlElement("status_purchase")]
    public int status_purchase { get; set; }

    [XmlElement("m0_purchase_idx")]
    public int m0_purchase_idx { get; set; }

    [XmlElement("email_purchase")]
    public string email_purchase { get; set; }

    [XmlElement("m0_equipment_idx")]
    public int m0_equipment_idx { get; set; }

    [XmlElement("org_name_purchase")]
    public string org_name_purchase { get; set; }

    [XmlElement("sec_name_purchase")]
    public string sec_name_purchase { get; set; }

    [XmlElement("drept_name_purchase")]
    public string drept_name_purchase { get; set; }

    [XmlElement("pos_name_purchase")]
    public string pos_name_purchase { get; set; }

    [XmlElement("fullname_purchase")]
    public string fullname_purchase { get; set; }

    [XmlElement("empcode_purchase")]
    public string empcode_purchase { get; set; }

    [XmlElement("org_idx_purchase")]
    public int org_idx_purchase { get; set; }

    [XmlElement("sec_idx_purchase")]
    public int sec_idx_purchase { get; set; }

    [XmlElement("dept_idx_purchase")]
    public int dept_idx_purchase { get; set; }

    [XmlElement("pos_idx_purchase")]
    public int pos_idx_purchase { get; set; }

    [XmlElement("name_purchase")]
    public string name_purchase { get; set; }

    [XmlElement("name_equipment")]
    public string name_equipment { get; set; }

    [XmlElement("actor_name_purchase")]
    public string actor_name_purchase { get; set; }

    [XmlElement("node_status_purchase")]
    public string node_status_purchase { get; set; }

    [XmlElement("node_name_purchase")]
    public string node_name_purchase { get; set; }

    [XmlElement("action_type")]
    public int action_type { get; set; }

    [XmlElement("date_purchasequipment")]
    public string date_purchasequipment { get; set; }

    [XmlElement("time_purchasequipment")]
    public string time_purchasequipment { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("m0_location_idx")]
    public int m0_location_idx { get; set; }

    [XmlElement("name_location")]
    public string name_location { get; set; }

    [XmlElement("m0_price_idx")]
    public int m0_price_idx { get; set; }

    [XmlElement("number_io")]
    public string number_io { get; set; }

    [XmlElement("number_asset")]
    public string number_asset { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }

    [XmlElement("price_purchase")]
    public string price_purchase { get; set; }

    [XmlElement("numrow")]
    public int numrow { get; set; }

    [XmlElement("yearly")]
    public string yearly { get; set; }

    [XmlElement("mountly")]
    public string mountly { get; set; }

    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }

    [XmlElement("numberphone")]
    public string numberphone { get; set; }

    [XmlElement("emp_idx_director")]
    public int emp_idx_director { get; set; }

    [XmlElement("selected")]
    public bool selected { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("emp_check_email")]
    public int emp_check_email { get; set; }

    [XmlElement("expenditure")]
    public string expenditure { get; set; }




}

#endregion

#region report_purchase_details

[Serializable]
public class report_purchase_details
{

    [XmlElement("condition_date_type")]
    public int condition_date_type { get; set; }

    [XmlElement("start_date")]
    public string start_date { get; set; }

    [XmlElement("end_date")]
    public string end_date { get; set; }

    [XmlElement("emp_idx_purchase")]
    public int emp_idx_purchase { get; set; }

    [XmlElement("u0_purchase_idx")]
    public int u0_purchase_idx { get; set; }

    [XmlElement("details_purchase")]
    public string details_purchase { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("quantity_equipment")]
    public int quantity_equipment { get; set; }

    [XmlElement("status_purchase")]
    public int status_purchase { get; set; }

    [XmlElement("m0_purchase_idx")]
    public int m0_purchase_idx { get; set; }

    [XmlElement("email_purchase")]
    public string email_purchase { get; set; }

    [XmlElement("m0_equipment_idx")]
    public int m0_equipment_idx { get; set; }

    [XmlElement("org_name_purchase")]
    public string org_name_purchase { get; set; }

    [XmlElement("sec_name_purchase")]
    public string sec_name_purchase { get; set; }

    [XmlElement("drept_name_purchase")]
    public string drept_name_purchase { get; set; }

    [XmlElement("pos_name_purchase")]
    public string pos_name_purchase { get; set; }

    [XmlElement("fullname_purchase")]
    public string fullname_purchase { get; set; }

    [XmlElement("empcode_purchase")]
    public string empcode_purchase { get; set; }

    [XmlElement("org_idx_purchase")]
    public int org_idx_purchase { get; set; }

    [XmlElement("sec_idx_purchase")]
    public int sec_idx_purchase { get; set; }

    [XmlElement("dept_idx_purchase")]
    public int dept_idx_purchase { get; set; }

    [XmlElement("pos_idx_purchase")]
    public int pos_idx_purchase { get; set; }

    [XmlElement("name_purchase")]
    public string name_purchase { get; set; }

    [XmlElement("name_equipment")]
    public string name_equipment { get; set; }

    [XmlElement("actor_name_purchase")]
    public string actor_name_purchase { get; set; }

    [XmlElement("node_status_purchase")]
    public string node_status_purchase { get; set; }

    [XmlElement("node_name_purchase")]
    public string node_name_purchase { get; set; }

    [XmlElement("action_type")]
    public int action_type { get; set; }

    [XmlElement("date_purchasequipment")]
    public string date_purchasequipment { get; set; }

    [XmlElement("time_purchasequipment")]
    public string time_purchasequipment { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("m0_location_idx")]
    public int m0_location_idx { get; set; }

    [XmlElement("name_location")]
    public string name_location { get; set; }

    [XmlElement("m0_price_idx")]
    public int m0_price_idx { get; set; }

    [XmlElement("number_io")]
    public string number_io { get; set; }

    [XmlElement("number_asset")]
    public string number_asset { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }

    [XmlElement("price_purchase")]
    public string price_purchase { get; set; }

    [XmlElement("numrow")]
    public int numrow { get; set; }

    [XmlElement("yearly")]
    public string yearly { get; set; }

    [XmlElement("mountly")]
    public string mountly { get; set; }

    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }

    [XmlElement("numberphone")]
    public string numberphone { get; set; }

    [XmlElement("emp_idx_director")]
    public int emp_idx_director { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("emp_check_email")]
    public int emp_check_email { get; set; }

    [XmlElement("expenditure")]
    public string expenditure { get; set; }




}

#endregion

#region _organization_details

[Serializable]
public class _organization_details
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("OrgNameEN")]
    public string OrgNameEN { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("DeptIDX")]
    public int DeptIDX { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("DeptNameEN")]
    public string DeptNameEN { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("SecIDX")]
    public int SecIDX { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("SecNameEN")]
    public string SecNameEN { get; set; }

    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }

    [XmlElement("PosIDX")]
    public int PosIDX { get; set; }

    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }

    [XmlElement("PosNameEN")]
    public string PosNameEN { get; set; }

}

#endregion

#region export_purchase_details

[Serializable]
public class export_purchase_details
{

    [XmlElement("รหัสเอกสาร")]
    public string รหัสเอกสาร { get; set; }

    [XmlElement("วันที่สร้างรายการ")]
    public string วันที่สร้างรายการ { get; set; }


    [XmlElement("รหัสพนักงาน")]
    public string รหัสพนักงาน { get; set; }

    [XmlElement("ชื่อผู้ขอซื้อ")]
    public string ชื่อผู้ขอซื้อ { get; set; }

    [XmlElement("บริษัท")]
    public string บริษัท { get; set; }

    [XmlElement("ฝ่าย")]
    public string ฝ่าย { get; set; }

    [XmlElement("แผนก")]
    public string แผนก { get; set; }

    [XmlElement("ตำแหน่ง")]
    public string ตำแหน่ง { get; set; }

    [XmlElement("ประเภทขอซื้อ")]
    public string ประเภทขอซื้อ { get; set; }

    [XmlElement("อุปกรณ์ที่ขอซื้อ")]
    public string อุปกรณ์ที่ขอซื้อ { get; set; }

    [XmlElement("รายละเอียดการขอซื้อ")]
    public string รายละเอียดการขอซื้อ { get; set; }

    [XmlElement("จำนวน")]
    public int จำนวน { get; set; }
   
    [XmlElement("ราคาประมาณ")]
    public string ราคาประมาณ { get; set; }

    [XmlElement("เลขAsset")]
    public string เลขAsset { get; set; }

    [XmlElement("เลขIo")]
    public string เลขIo { get; set; }

    [XmlElement("สถานะเอกสาร")]
    public string สถานะเอกสาร { get; set; }

}

#endregion

