﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MessagingToolkit.QRCode.Codec.Data;
using MessagingToolkit.QRCode.Codec;

using System.Drawing;
using System.Configuration;
using System.Drawing.Imaging;

public partial class websystem_ITServices_QrCode : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {

        string getPath = ConfigurationManager.AppSettings["upload_Qr_Code"];
        string fileName1 = "first.jpg";

        QRCodeEncoder encoder = new QRCodeEncoder();
        Bitmap bi = encoder.Encode("http://www.taokaenoi.co.th/MAS/ITRepair");

        bi.Save(Server.MapPath(getPath + fileName1), ImageFormat.Jpeg);

        Image1.ImageUrl = getPath + fileName1;

    }
}