﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="it_asset_print.aspx.cs" Inherits="websystem_it_asset_print" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <%-- <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />--%>
    <link href="../../Content/bootstrap.css" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

    <title>Print รายการขอซื้ออุปกรณ์</title>

    <style type="text/css" media="print,screen">
        @page {
            size: A4 landscape;
            margin: 25px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

        .formPrint {
            margin: 10pt; /*from 5 to 10*/
            padding: 10pt; /*add*/
            width: 842pt;
        }
    </style>

</head>
<body onload="window.print()">

    <form id="form1" runat="server" width="100%">
        <script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>
        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <div class="formPrint">
            <div class="headOrg" style="text-align: center;">
                <strong>รายการอุปกรณ์ที่ซื้อเดือน &nbsp;
                    <asp:Label ID="lb_round_month" runat="server" Text=""></asp:Label>
                </strong>

            </div>
            <div class="headOrg" style="text-align: center;">
                <strong>
                    <asp:Label ID="lb_org_name_th" runat="server" Text=""></asp:Label>
                </strong>
            </div>
            <div class="headOrg" style="text-align: center;">
                <strong>
                    <asp:Label ID="lb_org_address" runat="server" Text=""></asp:Label>
                </strong>
            </div>
            <asp:GridView ID="gvSample" runat="server" AutoGenerateColumns="False"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                GridLines="None"
                HeaderStyle-CssClass="info"
                HeaderStyle-Height="30px"
                Width="100%"
                OnRowDataBound="gvRowDataBound">
                <HeaderStyle CssClass="info" Height="30px" Font-Size="Small" />
                <RowStyle Font-Size="Small" />
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-Width="5%"
                        HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center"
                        ControlStyle-Font-Size="8" HeaderStyle-Font-Size="8">
                        <ItemTemplate>
                            <small>
                                <%# (Container.DataItemIndex +1) %>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="กลุ่มอุปกรณ์" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small"
                        HeaderStyle-Width="25%">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lb_tdidx_name" runat="server" CssClass="col-sm-12"
                                    Text='<%# Eval("zdevices_name") %>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                        HeaderStyle-Width="7%" HeaderText="จำนวน"
                        ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lb_qty" runat="server" CssClass="col-sm-12"
                                    Text='<%# getformatfloat(((int)Eval("qty")).ToString(),0) %>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                        HeaderStyle-Width="10%" HeaderText="หน่วย"
                        ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lb_NameTH" runat="server" CssClass="col-sm-12"
                                    Text='<%# Eval("NameTH") %>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ราคาต่อหน่วย" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>


                            <asp:Label ID="lb_price" runat="server" CssClass="col-sm-12"
                                Text='<%# string.Format("{0:n2}",Eval("price")) %>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Spec" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lb_spec_remark" runat="server" CssClass="col-sm-12"
                                    Text='<%# Eval("spec_remark") %>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
            <br />
            <br />
            <%-- <div class="headPrint">--%>
            <div class="pull-right" style="text-align: right;">
                PRINT DATE :
                    <asp:Label ID="lblPrintDate" runat="server" />
            </div>

        </div>

    </form>
</body>
</html>
