﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_ITServices_DeviceRegister : System.Web.UI.Page
{

    #region Connect
    //DBConn DBConn = new DBConn();
    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    //FunctionWeb funcWeb = new FunctionWeb();

    private string ODSP_Reletion = "ODSP_Reletion_Mas";
    private string ConCL = "conn_centralized";

    dataODSP_Relation dtODSP = new dataODSP_Relation();
    data_employee _dtEmployee = new data_employee();
    function_scan _funcScan = new function_scan();
    function_db _funcdb = new function_db();
    data_employee _dataEmployee = new data_employee();

    data_device _data_deviceregister = new data_device();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _url_u0_device = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_u0_device_List"];
    static string _url_u0_device_update_12 = _serviceUrl + ConfigurationManager.AppSettings["urlupdate_u0_device_12_List"];
    static string _url_u0_device_update_13 = _serviceUrl + ConfigurationManager.AppSettings["urlupdate_u0_device_13_List"];
    static string _url_u0_device_update_14 = _serviceUrl + ConfigurationManager.AppSettings["urlupdate_u0_device_14_List"];
    static string _url_u0_device_update_15 = _serviceUrl + ConfigurationManager.AppSettings["urlupdate_u0_device_15_List"];
    static string _url_u0_device_update_16 = _serviceUrl + ConfigurationManager.AppSettings["urlupdate_u0_device_16_List"];
    static string _url_u0_device_update_18 = _serviceUrl + ConfigurationManager.AppSettings["urlupdate_u0_device_18_List"];
    static string _url_u0_device_update_19 = _serviceUrl + ConfigurationManager.AppSettings["urlupdate_u0_device_19_List"];
    static string _url_u0_device_code = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_u0_device_21_List"];
    static string _url_u0_device_view = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_u0_device_22_List"];
    static string _url_u0_device_view_23 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_u0_device_23_List"];
    static string _url_u0_device_view_24 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_u0_device_24_List"];
    static string _url_u0_device_view_25 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_u0_device_25_List"];
    static string _url_u0_device_view_26 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_u0_device_26_List"];
    static string _url_u0_device_view_27 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_u0_device_27_List"];
    static string _url_u0_device_view_28 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_u0_device_28_List"];
    static string _url_u0_device_add = _serviceUrl + ConfigurationManager.AppSettings["urlAdd_u0_device_List"];
    static string _url_u0_device_update = _serviceUrl + ConfigurationManager.AppSettings["urlupdate_u0_device_List"];
    static string _url_m0_typedevice = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_typedevice_List"];
    static string _url_m0_insurance = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_insurance_List"];
    static string _url_m0_band = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_band_List"];
    static string _url_m0_status = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_status_List"];
    static string _url_m0_level = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_level_List"];
    static string _url_m0_cpu = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_cpu_List"];
    static string _url_m0_cpu_detail = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_cpu_21_List"];
    static string _url_Insert_m0_cpu_10 = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_m0_cpu_10_List"];
    static string _url_m0_ram = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_ram_List"];
    static string _url_m0_ram_detail = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_ram_21_List"];
    static string _url_Insert_m0_ram_10 = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_m0_ram_10_List"];
    static string _url_m0_hdd = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_hdd_List"];
    static string _url_m0_hdd_detail = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_hdd_21_List"];
    static string _url_Insert_m0_hdd_10 = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_m0_hdd_10_List"];
    static string _url_m0_vga = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_vga_List"];
    static string _url_m0_vga_detail = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_vga_21_List"];
    static string _url_Insert_m0_vga_10 = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_m0_vga_10_List"];
    static string _url_m0_printer = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_printer_List"];
    static string _url_m0_printer_detail = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_printer_21_List"];
    static string _url_m0_moniter = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_moniter_List"];
    static string _url_m0_moniter_detail = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_m0_moniter_21_List"];

    static string _urlSet_m0_printer_detail_List = _serviceUrl + ConfigurationManager.AppSettings["urlSet_m0_printer_detail_List"];
    static string _urlSet_m0_moniter_detail_List = _serviceUrl + ConfigurationManager.AppSettings["urlSet_m0_moniter_detail_List"];
    static string _urlSet_m0_insurance_detail_List = _serviceUrl + ConfigurationManager.AppSettings["urlSet_m0_insurance_detail_List"];
    static string _urlSet_m0_band_detail_List = _serviceUrl + ConfigurationManager.AppSettings["urlSet_m0_band_detail_List"];

    static string urlGetOrganization = _serviceUrl + ConfigurationManager.AppSettings["urlOrganization"];
    static string urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];

    static string imgPath = ConfigurationSettings.AppSettings["PathFile_Device"];

    int emp_idx = 0;
    int defaultInt = 0;

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";
    int _tempInt = 0;
    string temp = "";


    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            #region ViewAddDevice

            var dsEmp = new DataSet();
            dsEmp.Tables.Add("TempDevice");

            dsEmp.Tables[0].Columns.Add("u0_didx", typeof(int));
            dsEmp.Tables[0].Columns.Add("u0_code", typeof(string));
            dsEmp.Tables[0].Columns.Add("m0_tdidx", typeof(int));
            dsEmp.Tables[0].Columns.Add("u0_relation_didx", typeof(int));
            dsEmp.Tables[0].Columns.Add("name_m0_typedevice", typeof(string));

            ViewState["vsTempDevicw"] = dsEmp;

            var dssoftware = new DataSet();
            dssoftware.Tables.Add("Temp_sw");

            dssoftware.Tables[0].Columns.Add("software_idx", typeof(int));
            dssoftware.Tables[0].Columns.Add("name_software", typeof(string));
            dssoftware.Tables[0].Columns.Add("type_name", typeof(string));

            ViewState["vsTemp_sw"] = dssoftware;

            #endregion

            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            select_empIdx_present();
            Defult();

            Menu_Color(1);
            MvMaster.SetActiveView(ViewIndex);
            Select_Detail_Device();

            if (int.Parse(ViewState["Sec_idx"].ToString()) == 80 || int.Parse(ViewState["Sec_idx"].ToString()) == 210)
            {
                lbadd.Visible = true;

            }
            else
            {
                lbadd.Visible = false;
                //lbaddmasterdata.Visible = false;
            }
            //start teppanop //
            if (Session["_sesion_devices"] != null)
            {
                if (Session["_sesion_devices"].ToString() == "create")
                {
                    create_asset();
                }
            }
            //end teppanop //

        }
        linkBtnTrigger(lbapprovecutoff);
        linkBtnTrigger(lb_guidebook);
        linkBtnTrigger(lb_flow);
    }


    #region INSERT&SELECT&UPDATE

    protected void Defult()
    {
        ViewState["DeviceDatabase"] = null;
        ViewState["Seach_DeviceDatabase"] = null;
        ViewState["Search_view_typedevice"] = null;
        ViewState["Search_view_statusdevice"] = null;
        ViewState["Search_view_u0_code"] = null;
        ViewState["Search_view_assetcode"] = null;
        ViewState["Search_view_seria"] = null;
        ViewState["CheckSearch"] = 0;
        ViewState["u0_relation_List"] = "";

        ViewState["Search_view_statusdevice_cpu"] = null;
        ViewState["Search_view_u0_code_cpu"] = null;
        ViewState["Search_view_assetcode_cpu"] = null;
        ViewState["Search_view_seria_cpu"] = null;

        ViewState["Search_view_statusdevice_ram"] = null;
        ViewState["Search_view_u0_code_ram"] = 0;
        ViewState["Search_view_assetcode_ram"] = 0;
        ViewState["Search_view_seria_ram"] = 0;

        ViewState["u0_relation_Approve_cpu"] = "";
        ViewState["u0_relation_Approve_ram"] = "";
        ViewState["u0_relation_Approve_hdd"] = "";
        ViewState["u0_relation_Approve_vga"] = "";
        ViewState["u0_relation_Approve_etc"] = "";
        ViewState["u0_didx_5"] = 0;
        ViewState["check_approve"] = "1,2";
        ViewState["Search_typesoftware"] = "";
        ViewState["Search_orgsoftware"] = "";
        ViewState["Search_softwarecode"] = "";
        ViewState["u0_swidx"] = 0;
        ViewState["u0_software_List"] = "";
        ViewState["u0_didx_cutoff_List"] = "";
        ViewState["Node"] = "";
        ViewState["Actor"] = "";
        ViewState["HDEmpIDX_edit"] = 0;
        ViewState["u0_relation_didx_Approve"] = 0;
        ViewState["u0_didx_Device_Approve_Transfer"] = 0;
        ViewState["Rdept_idx"] = 0;
        ViewState["u0_didx_all_transfer"] = 0;
        ViewState["u0_code_cutoff"] = 0;
        ViewState["Node_c"] = 0;
        ViewState["Actor_c"] = 0;
        ViewState["u0_didx_Device_Main"] = 0;
        ViewState["code_cutoff"] = 0;
        ViewState["lbl_view_u0_didx_view"] = "";
        ViewState["lbl_u0_software_licen_view"] = "";
        ViewState["lbl_view_u0_didx_cut"] = 0;
        ViewState["btnapprovecutoff_u0_didx"] = 0;
        ViewState["Check_Action"] = "";

        GvDevice.DataSource = null;
        GvDevice.DataBind();

        Fv_Search_Device_Index.ChangeMode(FormViewMode.Insert);
        Fv_Search_Device_Index.DataBind();

        Search_status();
        Search_typedevice();

        DropDownList ddl_add_org_Search = (DropDownList)Fv_Search_Device_Index.FindControl("ddl_add_org_Search");
        getOrganizationList(ddl_add_org_Search);
    }

    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["jobgrade_level"] = _dtEmployee.employee_list[0].jobgrade_level;

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected string MapURL(string path)
    {
        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {
        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            GridView gvFileCutoff = (GridView)FvViewDetailDevice_Cutoff.FindControl("gvFileCutoff");
            if (dt1.Rows.Count > 0)
            {
                ds1.Tables.Add(dt1);
                gvFileCutoff.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                gvFileCutoff.DataBind();
                ds1.Dispose();
            }
            else
            {

                gvFileCutoff.DataSource = null;
                gvFileCutoff.DataBind();

            }
        }
        catch
        {
        }
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void Insert_Detail_Device()
    {
        int m0_master = 0;
        DropDownList ddl_add_typedevice = (DropDownList)FvInsertDevice.FindControl("ddl_add_typedevice"); //m0_tdidx
        TextBox txt_add_acc = (TextBox)FvInsertDevice.FindControl("txt_add_acc"); //u0_acc
        TextBox txt_add_code_old = (TextBox)FvInsertDevice.FindControl("txt_add_code_old"); //u0_acc
        TextBox txt_add_po = (TextBox)FvInsertDevice.FindControl("txt_add_po"); //u0_po
        TextBox txt_add_buy = (TextBox)FvInsertDevice.FindControl("txt_add_buy"); //u0_purchase
        TextBox txt_add_expire = (TextBox)FvInsertDevice.FindControl("txt_add_expire"); //u0_expDate
        TextBox txt_add_serial = (TextBox)FvInsertDevice.FindControl("txt_add_serial"); //u0_serial
        DropDownList ddl_add_Insurance = (DropDownList)FvInsertDevice.FindControl("ddl_add_Insurance"); //m0_iridx
        DropDownList ddl_add_band = (DropDownList)FvInsertDevice.FindControl("ddl_add_band"); //m0_bidx
        DropDownList ddl_add_org = (DropDownList)FvInsertDevice.FindControl("ddl_add_org"); //m0_orgidx
        DropDownList ddl_add_dept = (DropDownList)FvInsertDevice.FindControl("ddl_add_dept"); //m0_depidx
        DropDownList ddl_add_sec = (DropDownList)FvInsertDevice.FindControl("ddl_add_sec"); //m0_secidx
        DropDownList ddl_add_status = (DropDownList)FvInsertDevice.FindControl("ddl_add_status"); //m0_orgidx
        DropDownList ddl_add_level = (DropDownList)FvInsertDevice.FindControl("ddl_add_level"); //m0_lvidx 1,2 >> dv_u1_device	

        DropDownList ddl_add_cpu = (DropDownList)FvInsertDevice.FindControl("ddl_add_cpu"); //m0_cidx 6
        DropDownList ddl_add_ram = (DropDownList)FvInsertDevice.FindControl("ddl_add_ram"); //m0_ridx 7
        DropDownList ddl_add_hdd = (DropDownList)FvInsertDevice.FindControl("ddl_add_hdd"); //m0_hidx 8
        DropDownList ddl_add_vga = (DropDownList)FvInsertDevice.FindControl("ddl_add_vga"); //m0_vidx 9
        DropDownList ddl_add_printer = (DropDownList)FvInsertDevice.FindControl("ddl_add_printer"); //m0_pidx 3
        DropDownList ddl_add_monitor = (DropDownList)FvInsertDevice.FindControl("ddl_add_monitor"); //m0_midx 4
        DropDownList ddl_add_slot = (DropDownList)FvInsertDevice.FindControl("ddl_add_slot");
        DropDownList ddl_add_type_detail = (DropDownList)FvInsertDevice.FindControl("ddl_add_type_detail");

        switch (ddl_add_typedevice.SelectedValue)
        {
            case "3": //Printer
                m0_master = int.Parse(ddl_add_printer.SelectedValue);
                break;
            case "4": //Moniter
                m0_master = int.Parse(ddl_add_monitor.SelectedValue);
                break;
            case "6": //CPU
                m0_master = int.Parse(ddl_add_cpu.SelectedValue);
                break;
            case "7": //Ram
                m0_master = int.Parse(ddl_add_ram.SelectedValue);
                break;
            case "8": //Hdd
                m0_master = int.Parse(ddl_add_hdd.SelectedValue);
                break;
            case "9": //Vga
                m0_master = int.Parse(ddl_add_vga.SelectedValue);
                break;
        }

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        _data_device_detail.u0_didx = 0; // New Insert
        if (txt_add_code_old.Text == "")
        {
            _data_device_detail.u0_code_old = "0";
        }
        else
        {
            _data_device_detail.u0_code_old = txt_add_code_old.Text;
        }

        if (txt_add_acc.Text == "")
        {
            _data_device_detail.u0_acc = "0";
        }
        else
        {
            _data_device_detail.u0_acc = txt_add_acc.Text;
        }

        if (txt_add_po.Text == "")
        {
            _data_device_detail.u0_po = "0";
        }
        else
        {
            _data_device_detail.u0_po = txt_add_po.Text;
        }

        _data_device_detail.m0_iridx = int.Parse(ddl_add_Insurance.SelectedValue);
        _data_device_detail.u0_purchase = txt_add_buy.Text;
        _data_device_detail.u0_expDate = txt_add_expire.Text;
        _data_device_detail.m0_bidx = int.Parse(ddl_add_band.SelectedValue);
        _data_device_detail.m0_sidx = int.Parse(ddl_add_status.SelectedValue);
        _data_device_detail.u0_serial = txt_add_serial.Text;
        _data_device_detail.m0_tdidx = int.Parse(ddl_add_typedevice.SelectedValue);
        _data_device_detail.u0_empcreate = int.Parse(ViewState["EmpIDX"].ToString());
        _data_device_detail.m0_orgidx = int.Parse(ddl_add_org.SelectedValue);
        _data_device_detail.R0_depidx = int.Parse(ddl_add_dept.SelectedValue);
        _data_device_detail.R0_secidx = int.Parse(ddl_add_sec.SelectedValue);
        _data_device_detail.m0_lvidx = int.Parse(ddl_add_level.SelectedValue);
        _data_device_detail.m0_Master = m0_master;
        _data_device_detail.u0_relation_didx = 0;
        _data_device_detail.doc_status = "1";
        _data_device_detail.u0_slotram = int.Parse(ddl_add_slot.SelectedValue);
        _data_device_detail.u0_typedeviceetc_idx = int.Parse(ddl_add_type_detail.SelectedValue); // New Insert

        //dv_u0_device
        _data_device_detail.u0_unidx = 1;
        _data_device_detail.u0_acidx = 1;
        _data_device_detail.u0_doc_decision = 1;
        _data_device_detail.Approve_stauts = 1; //ดำเนินการ

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _data_deviceregister = callServicePostDevice(_url_u0_device_add, _data_deviceregister);
    }

    protected void Insert_Detail_Master_Device(int ddltype)
    {
        switch (ddltype)
        {
            case 3:
                Insert_m0_printer();
                Select_Gridview_m0_printer();
                break;
            case 4:
                Insert_m0_moniter();
                Select_Gridview_m0_moniter();
                break;
            case 6:
                Insert_m0_cpu();
                Select_Gridview_m0_cpu();
                break;
            case 7:
                Insert_m0_ram();
                Select_Gridview_m0_ram();
                break;
            case 8:
                Insert_m0_hdd();
                Select_Gridview_m0_hdd();
                break;
            case 9:
                Insert_m0_vga();
                Select_Gridview_m0_vga();
                break;
            case 11:
                Insert_m0_band();
                Select_Gridview_m0_band();
                break;
            case 14:
                Insert_m0_insurance();
                Select_Gridview_m0_insurance();
                break;
        }
    }


    protected void Insert_m0_cpu()
    {
        //CPU
        TextBox txt_addmaster_cpu_band = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_cpu_band");
        TextBox txt_addmaster_cpu_generation = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_cpu_generation");
        DropDownList ddl_addmaster_cpu_status = (DropDownList)FvInsertMasterDevice.FindControl("ddl_addmaster_cpu_status");

        _data_deviceregister.m0_cpu_list = new m0_cpu_detail[1];
        m0_cpu_detail _data_device_detail = new m0_cpu_detail();

        _data_device_detail.band_cpu = txt_addmaster_cpu_band.Text;
        _data_device_detail.generation_cpu = txt_addmaster_cpu_generation.Text;
        _data_device_detail.status_m0_cpu = int.Parse(ddl_addmaster_cpu_status.SelectedValue);

        _data_deviceregister.m0_cpu_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_Insert_m0_cpu_10, _data_deviceregister);
    }

    protected void Insert_m0_ram()
    {
        //RAM
        TextBox txt_addmaster_ram_band = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_ram_band");
        TextBox txt_addmaster_ram_type = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_ram_type");
        TextBox txt_addmaster_ram_size = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_ram_size");
        DropDownList ddl_addmaster_ram_status = (DropDownList)FvInsertMasterDevice.FindControl("ddl_addmaster_ram_status");

        _data_deviceregister.m0_ram_list = new m0_ram_detail[1];
        m0_ram_detail _data_device_detail = new m0_ram_detail();

        _data_device_detail.band_ram = txt_addmaster_ram_band.Text;
        _data_device_detail.type_ram = txt_addmaster_ram_type.Text;
        _data_device_detail.capacity_ram = txt_addmaster_ram_size.Text;
        _data_device_detail.status_m0_ram = int.Parse(ddl_addmaster_ram_status.SelectedValue);

        _data_deviceregister.m0_ram_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_Insert_m0_ram_10, _data_deviceregister);
    }

    protected void Insert_m0_hdd()
    {
        //HDD
        TextBox txt_addmaster_hdd_band = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_hdd_band");
        TextBox txxt_addmaster_hdd_type = (TextBox)FvInsertMasterDevice.FindControl("txxt_addmaster_hdd_type");
        TextBox txt_addmaster_hdd_size = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_hdd_size");
        DropDownList ddl_addmaster_hdd_status = (DropDownList)FvInsertMasterDevice.FindControl("ddl_addmaster_hdd_status");

        _data_deviceregister.m0_hdd_list = new m0_hdd_detail[1];
        m0_hdd_detail _data_device_detail = new m0_hdd_detail();

        _data_device_detail.band_hdd = txt_addmaster_hdd_band.Text;
        _data_device_detail.type_hdd = txxt_addmaster_hdd_type.Text;
        _data_device_detail.capacity_hdd = txt_addmaster_hdd_size.Text;
        _data_device_detail.status_m0_hdd = int.Parse(ddl_addmaster_hdd_status.SelectedValue);

        _data_deviceregister.m0_hdd_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_Insert_m0_hdd_10, _data_deviceregister);
    }

    protected void Insert_m0_vga()
    {
        //vga
        TextBox txt_addmaster_vga_type = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_vga_type");
        TextBox txt_addmaster_vga_gen = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_vga_gen");
        DropDownList ddl_addmaster_vga_status = (DropDownList)FvInsertMasterDevice.FindControl("ddl_addmaster_vga_status");

        _data_deviceregister.m0_vga_list = new m0_vga_detail[1];
        m0_vga_detail _data_device_detail = new m0_vga_detail();

        _data_device_detail.generation_vga = txt_addmaster_vga_type.Text;
        _data_device_detail.type_vga = txt_addmaster_vga_gen.Text;
        _data_device_detail.status_m0_vga = int.Parse(ddl_addmaster_vga_status.SelectedValue);

        _data_deviceregister.m0_vga_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_Insert_m0_vga_10, _data_deviceregister);
    }

    protected void Insert_m0_printer()
    {
        TextBox txt_addmaster_printer_type = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_printer_type");
        TextBox txt_addmaster_band_print = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_band_print");
        TextBox txt_addmaster_ink_print = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_ink_print");
        TextBox txt_addmaster_gen_print = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_gen_print");
        DropDownList ddl_addmaster_printer_status = (DropDownList)FvInsertMasterDevice.FindControl("ddl_addmaster_printer_status");

        _data_deviceregister.m0_printer_list = new m0_printer_detail[1];
        m0_printer_detail _data_device_detail = new m0_printer_detail();

        _data_device_detail.type_print = txt_addmaster_printer_type.Text;
        _data_device_detail.band_print = txt_addmaster_band_print.Text;
        _data_device_detail.type_ink = txt_addmaster_ink_print.Text;
        _data_device_detail.generation_print = txt_addmaster_gen_print.Text;
        _data_device_detail.status_m0_printer = int.Parse(ddl_addmaster_printer_status.SelectedValue);

        _data_deviceregister.m0_printer_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_urlSet_m0_printer_detail_List, _data_deviceregister);
    }

    protected void Insert_m0_moniter()
    {
        TextBox txt_addmaster_band_moniter = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_band_moniter");
        TextBox txt_addmaster_type_moniter = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_type_moniter");
        DropDownList ddl_addmaster_moniter_status = (DropDownList)FvInsertMasterDevice.FindControl("ddl_addmaster_moniter_status");

        _data_deviceregister.m0_moniter_list = new m0_moniter_detail[1];
        m0_moniter_detail _data_device_detail = new m0_moniter_detail();

        _data_device_detail.band_moniter = txt_addmaster_band_moniter.Text;
        _data_device_detail.size_moniter = txt_addmaster_type_moniter.Text;
        _data_device_detail.status_m0_moniter = int.Parse(ddl_addmaster_moniter_status.SelectedValue);

        _data_deviceregister.m0_moniter_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_urlSet_m0_moniter_detail_List, _data_deviceregister);
    }

    protected void Insert_m0_band()
    {
        TextBox txt_addmaster_name_band = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_name_band");
        DropDownList ddl_addmaster_band_status = (DropDownList)FvInsertMasterDevice.FindControl("ddl_addmaster_band_status");

        _data_deviceregister.m0_band_list = new m0_band_detail[1];
        m0_band_detail _data_device_detail = new m0_band_detail();

        _data_device_detail.name_m0_band = txt_addmaster_name_band.Text;
        _data_device_detail.status_m0_band = int.Parse(ddl_addmaster_band_status.SelectedValue);

        _data_deviceregister.m0_band_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_urlSet_m0_band_detail_List, _data_deviceregister);
    }

    protected void Insert_m0_insurance()
    {
        TextBox txt_addmaster_name_insurance = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_name_insurance");
        TextBox txt_addmaster_detail_insurance = (TextBox)FvInsertMasterDevice.FindControl("txt_addmaster_detail_insurance");
        DropDownList ddl_addmaster_insurance_status = (DropDownList)FvInsertMasterDevice.FindControl("ddl_addmaster_insurance_status");

        _data_deviceregister.m0_insurance_list = new m0_insurance_detail[1];
        m0_insurance_detail _data_device_detail = new m0_insurance_detail();

        _data_device_detail.name_m0_insurance = txt_addmaster_name_insurance.Text;
        _data_device_detail.detail_m0_insurance = txt_addmaster_detail_insurance.Text;
        _data_device_detail.status_m0_insurance = int.Parse(ddl_addmaster_insurance_status.SelectedValue);

        _data_deviceregister.m0_insurance_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_urlSet_m0_insurance_detail_List, _data_deviceregister);
    }

    protected void Update_Detail_Device()
    {
        Label lbl_view_u0_didx = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
        TextBox txt_view_acc = (TextBox)FvViewDetailDevice.FindControl("txt_view_acc"); //u0_acc
        TextBox txt_view_code_old = (TextBox)FvViewDetailDevice.FindControl("txt_view_code_old"); //u0_acc
        TextBox txt_view_po = (TextBox)FvViewDetailDevice.FindControl("txt_view_po"); //u0_po
        DropDownList ddl_view_Insurance = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_Insurance"); //m0_iridx
        DropDownList ddl_view_band = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_band"); //m0_bidx
        TextBox txt_view_buy = (TextBox)FvViewDetailDevice.FindControl("txt_view_buy"); //u0_purchase
        TextBox txt_view_expire = (TextBox)FvViewDetailDevice.FindControl("txt_view_expire"); //u0_expDate
        DropDownList ddl_view_org = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_org"); //m0_orgidx
        DropDownList ddl_view_dept = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_dept"); //m0_orgidx
        DropDownList ddl_view_sec = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_sec"); //m0_orgidx
        DropDownList ddl_view_status = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_status"); //m0_orgidx
        TextBox txt_view_serial = (TextBox)FvViewDetailDevice.FindControl("txt_view_serial"); //u0_serial
        DropDownList ddl_view_level = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_level"); //m0_lvidx 1,2 >> dv_u1_device	
        Label lbl_view_typedevice = (Label)FvViewDetailDevice.FindControl("lbl_view_typedevice"); //m0_tdix
        DropDownList ddl_add_slot = (DropDownList)FvViewDetailDevice.FindControl("ddl_add_slot");
        LinkButton btnViewdata = (LinkButton)FvViewDetailDevice.FindControl("btnViewdata");

        DropDownList ddl_view_printer = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_printer");
        DropDownList ddl_view_moniter = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_moniter");
        DropDownList ddl_view_cpu = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_cpu");
        DropDownList ddl_view_ram = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_ram");
        DropDownList ddl_view_hdd = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_hdd");
        DropDownList ddl_view_vga = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_vga");
        Label lbl_u0_relation_didx_view = (Label)FvViewDetailDevice.FindControl("lbl_u0_relation_didx_view");



        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        if (int.Parse(lbl_view_typedevice.Text) == 1 || int.Parse(lbl_view_typedevice.Text) == 2)
        {
            _data_device_detail.u0_didx = int.Parse(lbl_view_u0_didx.Text);
        }
        else
        {
            _data_device_detail.u0_didx = int.Parse(lbl_view_u0_didx.Text);
            _data_device_detail.u0_relation_didx = int.Parse(lbl_u0_relation_didx_view.Text);
        }

        if (txt_view_code_old.Text == "")
        {
            _data_device_detail.u0_code_old = "-";
        }
        else
        {
            _data_device_detail.u0_code_old = txt_view_code_old.Text;
        }

        if (txt_view_acc.Text == "")
        {
            _data_device_detail.u0_acc = "0";
        }
        else
        {
            _data_device_detail.u0_acc = txt_view_acc.Text;
        }

        if (txt_view_po.Text == "")
        {
            _data_device_detail.u0_po = "0";
        }
        else
        {
            _data_device_detail.u0_po = txt_view_po.Text;
        }

        _data_device_detail.m0_iridx = int.Parse(ddl_view_Insurance.SelectedValue);
        _data_device_detail.u0_purchase = txt_view_buy.Text;
        _data_device_detail.u0_expDate = txt_view_expire.Text;
        _data_device_detail.m0_bidx = int.Parse(ddl_view_band.SelectedValue);
        _data_device_detail.m0_sidx = int.Parse(ddl_view_status.SelectedValue);
        _data_device_detail.u0_serial = txt_view_serial.Text;
        _data_device_detail.m0_orgidx = int.Parse(ddl_view_org.SelectedValue);
        //_data_device_detail.R0_depidx = int.Parse(ddl_view_dept.SelectedValue);
        //_data_device_detail.R0_secidx = int.Parse(ddl_view_sec.SelectedValue);

        if (ddl_view_dept.Items.Count != 0)
        {
            _data_device_detail.R0_depidx = int.Parse(ddl_view_dept.SelectedValue);
        }
        else
        {
            _data_device_detail.R0_depidx = 0;
        }

        if (ddl_view_sec.Items.Count != 0)
        {
            _data_device_detail.R0_secidx = int.Parse(ddl_view_sec.SelectedValue);
        }
        else
        {
            _data_device_detail.R0_secidx = 0;
        }


        _data_device_detail.u0_software_licen = ViewState["u0_software_licen"].ToString();
        _data_device_detail.u0_software_free = ViewState["u0_software_free"].ToString();


        switch (lbl_view_typedevice.Text)
        {
            case "3": // Printer
                _data_device_detail.m0_Master = int.Parse(ddl_view_printer.SelectedValue);
                break;
            case "4": // Moniter
                _data_device_detail.m0_Master = int.Parse(ddl_view_moniter.SelectedValue);
                break;
            case "6": // Cpu
                _data_device_detail.m0_Master = int.Parse(ddl_view_cpu.SelectedValue);
                break;
            case "7": // Ram
                _data_device_detail.m0_Master = int.Parse(ddl_view_ram.SelectedValue);
                break;
            case "8": // HDD
                _data_device_detail.m0_Master = int.Parse(ddl_view_hdd.SelectedValue);
                break;
            case "9": // VGA
                _data_device_detail.m0_Master = int.Parse(ddl_view_vga.SelectedValue);
                break;
        }


        if (ddl_view_level.SelectedValue != "")
        {
            _data_device_detail.m0_lvidx = int.Parse(ddl_view_level.SelectedValue);
        }

        _data_device_detail.doc_status = "1"; //กรณีไม่เพิ่ม แก้ไขปกติ

        if (ddl_add_slot.SelectedValue != "")
        {
            _data_device_detail.u0_slotram = int.Parse(ddl_add_slot.SelectedValue);
        }

        _data_device_detail.u0_doc_decision = 1;
        _data_device_detail.Approve_stauts = 1; //รอดำเนินการ

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _data_deviceregister = callServicePostDevice(_url_u0_device_update, _data_deviceregister); //Action 11


        if (ViewState["u0_relation_List"].ToString() != "," || ViewState["u0_relation_List"].ToString() != "")
        {
            string temp = ViewState["u0_relation_List"].ToString();
            string[] Loop = temp.Split(',');
            foreach (string LoopInsert in Loop)
            {
                if (LoopInsert != String.Empty)
                {
                    _data_deviceregister.u0_device_list = new u0_device_detail[1];
                    u0_device_detail _data_device_detail_1 = new u0_device_detail();

                    //dv_u0_device	
                    _data_device_detail_1.u0_didx = int.Parse(LoopInsert);
                    _data_device_detail_1.u0_relation_List = ViewState["u0_relation_List"].ToString();
                    _data_device_detail_1.u0_didx_reference = int.Parse(lbl_view_u0_didx.Text);
                    _data_device_detail_1.doc_status = "3"; //กรณีเพิ่ม
                    _data_device_detail_1.u0_empcreate = int.Parse(ViewState["EmpIDX"].ToString());
                    //_data_device_detail_1.HDEmpIDX = int.Parse(ViewState["HDEmpIDX_edit"].ToString());

                    //dv_u1_device
                    _data_device_detail_1.u0_unidx = 2;
                    _data_device_detail_1.u0_acidx = 1;
                    _data_device_detail_1.u0_doc_decision = 1;
                    _data_device_detail_1.Approve_stauts = 1; //รอดำเนินการ

                    _data_deviceregister.u0_device_list[0] = _data_device_detail_1;
                    _data_deviceregister = callServicePostDevice(_url_u0_device_update_13, _data_deviceregister);
                }
            }
        }

        if (ViewState["u0_didx_cutoff_List"].ToString() != "," || ViewState["u0_didx_cutoff_List"].ToString() != "")
        {
            TextBox tbDocRemark_cutoff = (TextBox)FvViewDetailDevice.FindControl("tbDocRemark_cutoff"); //u0_acc
            RadioButtonList Rdtype_cutoff = (RadioButtonList)FvViewDetailDevice.FindControl("Rdtype_cutoff");

            string temp_cutoff = ViewState["u0_didx_cutoff_List"].ToString();
            string[] Loop = temp_cutoff.Split(',');
            foreach (string LoopInsert in Loop)
            {
                if (LoopInsert != String.Empty)
                {
                    _data_deviceregister.u0_device_list = new u0_device_detail[1];
                    u0_device_detail _data_device_detail_2 = new u0_device_detail();

                    //dv_u0_device	
                    _data_device_detail_2.u0_didx = int.Parse(LoopInsert);
                    _data_device_detail_2.u0_didx_reference = int.Parse(lbl_view_u0_didx.Text);
                    _data_device_detail_2.doc_status = "6"; //ดำเนินการ(ตัดชำรุด)
                    _data_device_detail_2.u0_empcreate = int.Parse(ViewState["EmpIDX"].ToString());
                    _data_device_detail_2.m0_orgidx = int.Parse(ddl_view_org.SelectedValue);

                    //dv_u1_device
                    _data_device_detail_2.u0_unidx = 6;
                    _data_device_detail_2.u0_acidx = 2;
                    _data_device_detail_2.u0_doc_decision = 6;
                    _data_device_detail_2.comment = tbDocRemark_cutoff.Text;
                    _data_device_detail_2.Approve_stauts = 1; //รอดำเนินการ
                    _data_device_detail_2.U1_Doc_status = int.Parse(Rdtype_cutoff.SelectedValue); //สถานะอุปกรณ์ ณ ตอนเก็บ Log

                    _data_deviceregister.u0_device_list[0] = _data_device_detail_2;
                    //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
                    _data_deviceregister = callServicePostDevice(_url_u0_device_update_16, _data_deviceregister);
                }
            }
        }
    }

    protected void Delete_Device(int u0_didx)
    {
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        //u0_device
        _data_device_detail.u0_didx = u0_didx;
        _data_device_detail.u0_unidx = 9;
        _data_device_detail.u0_acidx = 1;
        _data_device_detail.u0_relation_didx = 0;

        //u1_device
        _data_device_detail.u1_unidx = 4;
        _data_device_detail.u1_acidx = 2;
        _data_device_detail.u0_doc_decision = 1;
        _data_device_detail.Approve_stauts = 5; //ลบอุปกรณ์

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _data_deviceregister = callServicePostDevice(_url_u0_device_update_16, _data_deviceregister); //Action 16
    }


    protected void Delete_m0_cpu()
    {
        //CPU
        _data_deviceregister.m0_cpu_list = new m0_cpu_detail[1];
        m0_cpu_detail _data_device_detail = new m0_cpu_detail();

        _data_device_detail.m0_cidx = int.Parse(ViewState["master_cpu_idx"].ToString());
        _data_device_detail.status_m0_cpu = 99;

        _data_deviceregister.m0_cpu_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_Insert_m0_cpu_10, _data_deviceregister);
    }

    protected void Delete_m0_ram()
    {
        _data_deviceregister.m0_ram_list = new m0_ram_detail[1];
        m0_ram_detail _data_device_detail = new m0_ram_detail();

        _data_device_detail.m0_ridx = int.Parse(ViewState["master_ram_idx"].ToString());
        _data_device_detail.status_m0_ram = 99;

        _data_deviceregister.m0_ram_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_Insert_m0_ram_10, _data_deviceregister);
    }

    protected void Delete_m0_hdd()
    {
        //HDD
        _data_deviceregister.m0_hdd_list = new m0_hdd_detail[1];
        m0_hdd_detail _data_device_detail = new m0_hdd_detail();

        _data_device_detail.m0_hidx = int.Parse(ViewState["master_hdd_idx"].ToString());
        _data_device_detail.status_m0_hdd = 99;

        _data_deviceregister.m0_hdd_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_Insert_m0_hdd_10, _data_deviceregister);
    }

    protected void Delete_m0_vga()
    {
        //vga
        _data_deviceregister.m0_vga_list = new m0_vga_detail[1];
        m0_vga_detail _data_device_detail = new m0_vga_detail();

        _data_device_detail.m0_vidx = int.Parse(ViewState["master_vga_idx"].ToString());
        _data_device_detail.status_m0_vga = 99;

        _data_deviceregister.m0_vga_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_Insert_m0_vga_10, _data_deviceregister);
    }

    protected void Delete_m0_printer()
    {
        _data_deviceregister.m0_printer_list = new m0_printer_detail[1];
        m0_printer_detail _data_device_detail = new m0_printer_detail();

        _data_device_detail.m0_pidx = int.Parse(ViewState["master_printer_idx"].ToString());
        _data_device_detail.status_m0_printer = 99;

        _data_deviceregister.m0_printer_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_urlSet_m0_printer_detail_List, _data_deviceregister);
    }

    protected void Delete_m0_moniter()
    {
        _data_deviceregister.m0_moniter_list = new m0_moniter_detail[1];
        m0_moniter_detail _data_device_detail = new m0_moniter_detail();

        _data_device_detail.m0_midx = int.Parse(ViewState["master_moniter_idx"].ToString());
        _data_device_detail.status_m0_moniter = 99;

        _data_deviceregister.m0_moniter_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_urlSet_m0_moniter_detail_List, _data_deviceregister);
    }

    protected void Delete_m0_band()
    {
        _data_deviceregister.m0_band_list = new m0_band_detail[1];
        m0_band_detail _data_device_detail = new m0_band_detail();

        _data_device_detail.m0_bidx = int.Parse(ViewState["master_band_idx"].ToString());
        _data_device_detail.status_m0_band = 99;

        _data_deviceregister.m0_band_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_urlSet_m0_band_detail_List, _data_deviceregister);
    }

    protected void Delete_m0_insurance()
    {
        _data_deviceregister.m0_insurance_list = new m0_insurance_detail[1];
        m0_insurance_detail _data_device_detail = new m0_insurance_detail();

        _data_device_detail.m0_iridx = int.Parse(ViewState["master_insurance_idx"].ToString());
        _data_device_detail.status_m0_insurance = 99;

        _data_deviceregister.m0_insurance_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_urlSet_m0_insurance_detail_List, _data_deviceregister);
    }


    protected void Update_m0_cpu()
    {
        //CPU
        _data_deviceregister.m0_cpu_list = new m0_cpu_detail[1];
        m0_cpu_detail _data_device_detail = new m0_cpu_detail();

        _data_device_detail.m0_cidx = int.Parse(ViewState["txt_master_m0_cidx"].ToString());
        _data_device_detail.band_cpu = ViewState["txt_master_band_cpu"].ToString();
        _data_device_detail.generation_cpu = ViewState["txt_master_gen_cpu"].ToString();
        _data_device_detail.status_m0_cpu = int.Parse(ViewState["ddStatus_update_cpu"].ToString());

        _data_deviceregister.m0_cpu_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_Insert_m0_cpu_10, _data_deviceregister);
    }

    protected void Update_m0_ram()
    {
        //RAM
        _data_deviceregister.m0_ram_list = new m0_ram_detail[1];
        m0_ram_detail _data_device_detail = new m0_ram_detail();

        _data_device_detail.m0_ridx = int.Parse(ViewState["txt_master_idx_ram"].ToString());
        _data_device_detail.band_ram = ViewState["txt_master_band_ram"].ToString();
        _data_device_detail.type_ram = ViewState["txt_master_gen_ram"].ToString();
        _data_device_detail.capacity_ram = ViewState["txt_master_capa_ram"].ToString();
        _data_device_detail.status_m0_ram = int.Parse(ViewState["ddStatus_update_ram"].ToString());

        _data_deviceregister.m0_ram_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_Insert_m0_ram_10, _data_deviceregister);
    }

    protected void Update_m0_hdd()
    {
        //HDD
        _data_deviceregister.m0_hdd_list = new m0_hdd_detail[1];
        m0_hdd_detail _data_device_detail = new m0_hdd_detail();

        _data_device_detail.m0_hidx = int.Parse(ViewState["txt_master_idx_hdd"].ToString());
        _data_device_detail.band_hdd = ViewState["txt_master_band_hdd"].ToString();
        _data_device_detail.type_hdd = ViewState["txt_master_gen_hdd"].ToString();
        _data_device_detail.capacity_hdd = ViewState["txt_master_capa_hdd"].ToString();
        _data_device_detail.status_m0_hdd = int.Parse(ViewState["ddStatus_update_hdd"].ToString());

        _data_deviceregister.m0_hdd_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_Insert_m0_hdd_10, _data_deviceregister);
    }

    protected void Update_m0_vga()
    {
        //vga
        _data_deviceregister.m0_vga_list = new m0_vga_detail[1];
        m0_vga_detail _data_device_detail = new m0_vga_detail();

        _data_device_detail.m0_vidx = int.Parse(ViewState["txt_master_idx_vga"].ToString());
        _data_device_detail.generation_vga = ViewState["txt_master_gen_vga"].ToString();
        _data_device_detail.type_vga = ViewState["txt_master_capa_vga"].ToString();
        _data_device_detail.status_m0_vga = int.Parse(ViewState["ddStatus_update_vga"].ToString());

        _data_deviceregister.m0_vga_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_Insert_m0_vga_10, _data_deviceregister);
    }

    protected void Update_m0_printer()
    {
        _data_deviceregister.m0_printer_list = new m0_printer_detail[1];
        m0_printer_detail _data_device_detail = new m0_printer_detail();

        _data_device_detail.m0_pidx = int.Parse(ViewState["txt_master_idx_poid"].ToString());
        _data_device_detail.type_print = ViewState["txt_master_type_print"].ToString();
        _data_device_detail.band_print = ViewState["txt_addmaster_band_print"].ToString(); 
        _data_device_detail.type_ink = ViewState["txt_master_ink_print"].ToString();
        _data_device_detail.generation_print = ViewState["txt_master_gen_print"].ToString();
        _data_device_detail.status_m0_printer = int.Parse(ViewState["ddStatus_update_print"].ToString());

        _data_deviceregister.m0_printer_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_urlSet_m0_printer_detail_List, _data_deviceregister);
    }

    protected void Update_m0_moniter()
    {
        _data_deviceregister.m0_moniter_list = new m0_moniter_detail[1];
        m0_moniter_detail _data_device_detail = new m0_moniter_detail();

        _data_device_detail.m0_midx = int.Parse(ViewState["txt_master_idx_moniter"].ToString());
        _data_device_detail.band_moniter = ViewState["txt_master_band_moniter"].ToString();
        _data_device_detail.size_moniter = ViewState["txt_master_type_moniter"].ToString();
        _data_device_detail.status_m0_moniter = int.Parse(ViewState["ddStatus_update_moniter"].ToString());

        _data_deviceregister.m0_moniter_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_urlSet_m0_moniter_detail_List, _data_deviceregister);
    }

    protected void Update_m0_band()
    {
        _data_deviceregister.m0_band_list = new m0_band_detail[1];
        m0_band_detail _data_device_detail = new m0_band_detail();

        _data_device_detail.m0_bidx = int.Parse(ViewState["txt_master_m0_bidx"].ToString());
        _data_device_detail.name_m0_band = ViewState["txt_master_name_band"].ToString();
        _data_device_detail.status_m0_band = int.Parse(ViewState["ddStatus_update_band"].ToString());

        _data_deviceregister.m0_band_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_urlSet_m0_band_detail_List, _data_deviceregister);
    }

    protected void Update_m0_insurance()
    {
        _data_deviceregister.m0_insurance_list = new m0_insurance_detail[1];
        m0_insurance_detail _data_device_detail = new m0_insurance_detail();

        _data_device_detail.m0_iridx = int.Parse(ViewState["txt_master_m0_iridx"].ToString());
        _data_device_detail.name_m0_insurance = ViewState["txt_master_name_insurance"].ToString();
        _data_device_detail.detail_m0_insurance = ViewState["txt_master_detail_insurance"].ToString();
        _data_device_detail.status_m0_insurance = int.Parse(ViewState["ddStatus_update_insurance"].ToString());

        _data_deviceregister.m0_insurance_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_urlSet_m0_insurance_detail_List, _data_deviceregister);
    }

    protected void Update_Approve_Detail_Device(int u0_didx_main, int u0_didx_new)
    {
        TextBox tbDocRemark = (TextBox)FvViewApproveDevice.FindControl("tbDocRemark");
        DropDownList ddl_view_Approve2 = (DropDownList)FvViewApproveDevice.FindControl("ddl_view_Approve2");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail_1 = new u0_device_detail();

        //dv_u0_device	
        _data_device_detail_1.u0_status_approve = int.Parse(ddl_view_Approve2.SelectedValue); //ใช้ตัดสถานะ
        _data_device_detail_1.u0_didx = u0_didx_main;
        _data_device_detail_1.u0_relation_List = u0_didx_new.ToString();
        _data_device_detail_1.comment = tbDocRemark.Text;
        _data_device_detail_1.doc_status = "1"; //ปกติ
        _data_device_detail_1.u0_empcreate = int.Parse(ViewState["EmpIDX"].ToString());
        _data_device_detail_1.u0_unidx = 9;
        _data_device_detail_1.u0_acidx = 1;

        if (int.Parse(ddl_view_Approve2.SelectedValue) == 1) //อนุมัติ
        {
            _data_device_detail_1.u0_relation_didx = u0_didx_main; //เครื่องหลัก
            //dv_u1_device
            _data_device_detail_1.u1_unidx = 4;
            _data_device_detail_1.u1_acidx = 2;
            _data_device_detail_1.u0_doc_decision = 2;
            _data_device_detail_1.u0_didx_reference = u0_didx_main;
            _data_device_detail_1.Approve_stauts = 2; //อนุมัติ
        }
        else //ไม่อนุมัติ
        {
            _data_device_detail_1.u0_relation_didx = 0; //เครื่องหลัก

            //dv_u1_device
            _data_device_detail_1.u1_unidx = 4;
            _data_device_detail_1.u1_acidx = 2;
            _data_device_detail_1.u0_doc_decision = 3;
            _data_device_detail_1.u0_didx_reference = u0_didx_main;
            _data_device_detail_1.Approve_stauts = 3; //ไม่อนุมัติ
        }

        _data_deviceregister.u0_device_list[0] = _data_device_detail_1;
        _data_deviceregister = callServicePostDevice(_url_u0_device_update_12, _data_deviceregister);

    }

    protected void Update_Approve_Cutoff_One(int type_doc_status, int u0_didx)
    {
        TextBox tbDocRemark_cutoff = (TextBox)FvViewApprove_cutoff.FindControl("tbDocRemark_cutoff");
        DropDownList ddl_view_Approve = (DropDownList)FvViewApprove_cutoff.FindControl("ddl_view_Approve");
        FileUpload UploadImages_Cutoff = (FileUpload)FvViewApprove_cutoff.FindControl("UploadImages_Cutoff");
        TextBox txt_view_code_cutoff = (TextBox)FvViewDetailDevice_Cutoff.FindControl("txt_view_code_cutoff");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail_1 = new u0_device_detail();

        _data_device_detail_1.u0_status_approve = int.Parse(ddl_view_Approve.SelectedValue); //ใช้ตัดสถานะ
        _data_device_detail_1.u0_didx = u0_didx;


        _data_device_detail_1.comment = tbDocRemark_cutoff.Text;
        _data_device_detail_1.u0_empcreate = int.Parse(ViewState["EmpIDX"].ToString());
        _data_device_detail_1.m0_sidx = 1; //ปกติ

        if (int.Parse(ddl_view_Approve.SelectedValue) == 1) //อนุมัติ
        {
            _data_device_detail_1.u0_doc_decision = 4;
            _data_device_detail_1.Approve_stauts = 2; //อนุมัติ
            _data_device_detail_1.u0_code_cutoff = txt_view_code_cutoff.Text;
            if (type_doc_status == 6)
            {
                _data_device_detail_1.doc_status = "6"; //ทั้งเครื่อง
            }
            else
            {
                _data_device_detail_1.doc_status = "7"; //เฉพาะอุปกรณ์
            }
            _data_device_detail_1.U1_Doc_status = type_doc_status; //6 ทั้งเครื่อง -- 7 เฉพาะอุปกรณ์
        }
        else //ไม่อนุมัติ
        {
            _data_device_detail_1.u0_relation_didx = 0;

            _data_device_detail_1.u0_doc_decision = 5;
            _data_device_detail_1.Approve_stauts = 3; //ไม่อนุมัติ
            _data_device_detail_1.doc_status = "1"; //ปกติ
            _data_device_detail_1.u0_code_cutoff = txt_view_code_cutoff.Text;
        }

        _data_deviceregister.u0_device_list[0] = _data_device_detail_1;
        //fsd.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _data_deviceregister = callServicePostDevice(_url_u0_device_update_18, _data_deviceregister);

        string dirName_u0 = txt_view_code_cutoff.Text;
        setDir(imgPath + dirName_u0);
        HttpFileCollection hfc = Request.Files;
        if (hfc.Count > 0)
        {
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(Server.MapPath(imgPath + dirName_u0));
            int ocount = dir.GetFiles().Length;

            for (int i = ocount; i < (hfc.Count + ocount); i++)
            {
                HttpPostedFile hpf = hfc[i - ocount];
                if (hpf.ContentLength > 0)
                {
                    string fileName = dirName_u0 + "_" + i;
                    string filePath = Server.MapPath(imgPath + dirName_u0);
                    string extension = Path.GetExtension(hpf.FileName);
                    hpf.SaveAs(Path.Combine(filePath, fileName + extension));
                }
            }
        }
        else
        {

        }

    }

    protected void Update_tranfer_newdevice(int u0_didx, int u0_didx_reference, int didx_relation)
    {
        //u0_didx = อุปกรณ์ใหม่
        //u0_didx_reference = อุปกรณ์เก่า
        //didx_relation = เครื่องหลัก PC

        //Label lbl_view_u0_didx = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx"); //เครื่องหลัก

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        //dv_u0_device	
        _data_device_detail.u0_didx = u0_didx; // อุปกรณ์ใหม่
        _data_device_detail.doc_status = "2"; //เปลี่ยนแปลง
        _data_device_detail.u0_empcreate = int.Parse(ViewState["EmpIDX"].ToString());
        _data_device_detail.u0_referent = u0_didx_reference.ToString();
        _data_device_detail.u0_didx_reference = u0_didx_reference; // อุปกรณ์เก่า
        _data_device_detail.u0_didx_reference_transfer = didx_relation; // เครื่องหลัก

        //dv_u0_device
        _data_device_detail.u0_unidx = 3;// int.Parse(ViewState["Node"].ToString()); //3
        _data_device_detail.u0_acidx = 1;// int.Parse(ViewState["Actor"].ToString()); //1
        _data_device_detail.Approve_stauts = 1; //รอดำเนินการ
        _data_device_detail.u0_doc_decision = 1;

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        _data_deviceregister = callServicePostDevice(_url_u0_device_update_14, _data_deviceregister);
    }

    protected void Update_Approve_Transfer(string u0_didx, string didx_reference, string didx_refer)
    {
        //u0_didx = อุปกรณ์ใหม่
        //didx_reference = อุปกรณ์เก่า
        //didx_refer = เครื่องหลัก

        FormView FvViewApproveDevice_1 = (FormView)FvViewApproveDevice_transfer.FindControl("FvViewApprove_transfer");
        TextBox tbDocRemark = (TextBox)FvViewApproveDevice_1.FindControl("tbDocRemark");
        DropDownList ddl_view_Approve = (DropDownList)FvViewApproveDevice_1.FindControl("ddl_view_Approve");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail_1 = new u0_device_detail();

        _data_device_detail_1.u0_didx = int.Parse(u0_didx); //ใหม่
        _data_device_detail_1.u0_empcreate = int.Parse(ViewState["EmpIDX"].ToString());
        _data_device_detail_1.comment = tbDocRemark.Text;
        _data_device_detail_1.doc_status = "1"; //ปกติ
        _data_device_detail_1.u0_referent = didx_reference;  //อุปกรณ์เก่า
        _data_device_detail_1.u0_didx_reference_transfer = int.Parse(didx_reference); //อุปกรณ์เก่า

        if (int.Parse(ddl_view_Approve.SelectedValue) == 1) //อนุมัติ
        {
            _data_device_detail_1.u0_unidx = 4;
            _data_device_detail_1.u0_acidx = 2;
            _data_device_detail_1.u0_doc_decision = 2;
            _data_device_detail_1.u0_relation_didx = 0; //เครื่องใหม่
            _data_device_detail_1.u0_relation_List = didx_refer.ToString(); //เครื่องเก่า
            _data_device_detail_1.Approve_stauts = 2; //อนุมัติ
        }
        else //ไม่อนุมัติ
        {
            _data_device_detail_1.u0_unidx = 4;
            _data_device_detail_1.u0_acidx = 2;
            _data_device_detail_1.u0_doc_decision = 3;
            _data_device_detail_1.u0_relation_didx = int.Parse(didx_refer.ToString());
            _data_device_detail_1.u0_relation_List = "0";
            _data_device_detail_1.Approve_stauts = 3; //ไม่อนุมัติ
        }

        _data_deviceregister.u0_device_list[0] = _data_device_detail_1;
        _data_deviceregister = callServicePostDevice(_url_u0_device_update_15, _data_deviceregister);
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
    }

    protected void Select_Detail_Device()
    {
        DropDownList ddl_search_typedevice_index = (DropDownList)Fv_Search_Device_Index.FindControl("ddl_search_typedevice_index"); //m0_tdidx
        DropDownList ddl_search_statusdevice_index = (DropDownList)Fv_Search_Device_Index.FindControl("ddl_search_statusdevice_index"); //m0_tdidx
        TextBox txt_search_code_index = (TextBox)Fv_Search_Device_Index.FindControl("txt_search_code_index"); //u0_acc
        TextBox txt_search_assetcode_index = (TextBox)Fv_Search_Device_Index.FindControl("txt_search_assetcode_index"); //u0_po
        TextBox txt_search_serial_index = (TextBox)Fv_Search_Device_Index.FindControl("txt_search_serial_index"); //u0_po
        DropDownList ddl_add_org_Search_index = (DropDownList)Fv_Search_Device_Index.FindControl("ddl_add_org_Search"); //m0_tdidx

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.m0_tdidx = int.Parse(ddl_search_typedevice_index.SelectedValue);
        _data_device.m0_sidx = int.Parse(ddl_search_statusdevice_index.SelectedValue);

        if (txt_search_code_index.Text == "")
        {
            _data_device.u0_code = "";
        }
        else
        {
            _data_device.u0_code = txt_search_code_index.Text;
        }
        if (txt_search_assetcode_index.Text == "")
        {
            _data_device.u0_acc = "0";
        }
        else
        {
            _data_device.u0_acc = txt_search_assetcode_index.Text;
        }

        if (txt_search_serial_index.Text == "")
        {
            _data_device.u0_serial = "0";
        }
        else
        {
            _data_device.u0_serial = txt_search_serial_index.Text;
        }

        _data_device.m0_orgidx = int.Parse(ddl_add_org_Search_index.SelectedValue);
        _data_device.HDEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _data_deviceregister.u0_device_list[0] = _data_device;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _data_deviceregister = callServicePostDevice(_url_u0_device, _data_deviceregister);
        setGridData(GvDevice, _data_deviceregister.u0_device_list);
        ViewState["Rdept_idx"] = _data_deviceregister.return_code;

        if (_data_deviceregister.return_code != 20)
        {
            _data_deviceregister.u0_device_list = new u0_device_detail[1];
            u0_device_detail _data_device_1 = new u0_device_detail();

            _data_device_1.HDEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
            _data_device_1.u0_relation_didx = 1;

            _data_deviceregister.u0_device_list[0] = _data_device_1;
            _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
            setGridData(GvTemp_Device_Transfer_all, _data_deviceregister.u0_device_list);

            foreach (GridViewRow row in GvTemp_Device_Transfer_all.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label u0_idx_trans = (Label)row.Cells[0].FindControl("lbl_u0_didx_approve_transfer");
                    ViewState["u0_didx_all_transfer"] += u0_idx_trans.Text + ",";
                }
            }
        }
    }

    protected void Select_FvDetail_Device(int didx)
    {
        //Panel boxviewdetail = (Panel)ViewIndex.FindControl("boxviewdetail");
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.u0_didx = didx;
        _data_device.u0_relation_didx = 0;

        _data_deviceregister.u0_device_list[0] = _data_device;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvViewDetailDevice, _data_deviceregister.u0_device_list);
    }

    protected void Select_FvDetail_Device_Cutoff(int didx)
    {
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.u0_didx = didx;
        _data_device.u0_relation_didx = 0;

        _data_deviceregister.u0_device_list[0] = _data_device;
        //fsd.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvViewDetailDevice_Cutoff, _data_deviceregister.u0_device_list);

        Label hfM0NodeIDX = (Label)FvViewDetailDevice_Cutoff.FindControl("lbl_hfM0NodeIDX_view_c");
        Label hfM0ActoreIDX = (Label)FvViewDetailDevice_Cutoff.FindControl("lbl_hfM0ActorIDX_view_c");
        TextBox txt_view_code_cutoff = (TextBox)FvViewDetailDevice_Cutoff.FindControl("txt_view_code_cutoff");
        ViewState["Node_c"] = hfM0NodeIDX.Text;
        ViewState["Actor_c"] = hfM0ActoreIDX.Text;
        ViewState["code_cutoff"] = txt_view_code_cutoff.Text;
    }

    protected void Select_Gv_Device_cutoff(int didx)
    {
        GridView Gv_cutoff_detail = (GridView)FvViewDetailDevice.FindControl("Gv_cutoff_detail");
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.u0_didx = didx;
        _data_device.u0_relation_didx = didx;

        _data_deviceregister.u0_device_list[0] = _data_device;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setGridData(Gv_cutoff_detail, _data_deviceregister.u0_device_list);
    }

    protected void Select_GvDevice_Cutoff_List(int didx)
    {
        //ViewState["rdept_idx"]
        //ViewState["Sec_idx"]
        //ViewState["EmpIDX"]


        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();
        _data_device.u0_didx = didx;
        _data_device.doc_status = "6"; //ทั้งเครื่อง
        _data_deviceregister.u0_device_list[0] = _data_device;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_28, _data_deviceregister);

        if (_data_deviceregister.u0_device_list != null)
        {
            var linq = from d in _data_deviceregister.u0_device_list
                       where (d.HDEmpIDX == int.Parse(ViewState["EmpIDX"].ToString()))
                       select d;
            ViewState["tempList1"] = linq.ToList();
        }
        else
        {
            ViewState["tempList1"] = null;
        }


        if (_data_deviceregister.return_code != 1)
        {
            GvDetail_Cutoff_List.Visible = true;
            setGridData(GvDetail_Cutoff_List, ViewState["tempList1"]);
        }
        else
        {
            GvDetail_Cutoff_List.Visible = false;
        }

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_1 = new u0_device_detail();
        _data_device_1.u0_didx = didx;
        _data_device_1.doc_status = "7"; //เฉพาะเครื่อง
        _data_deviceregister.u0_device_list[0] = _data_device_1;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_28, _data_deviceregister);

        if (_data_deviceregister.u0_device_list != null)
        {
            var linq1 = from d in _data_deviceregister.u0_device_list
                            //where (d.HDEmpIDX == int.Parse(ViewState["EmpIDX"].ToString()))
                        select d;
            ViewState["tempList2"] = linq1.ToList();
        }
        else
        {
            ViewState["tempList2"] = null;
        }

        if (_data_deviceregister.return_code != 1)
        {
            GvDetail_Cutoff_List_one.Visible = true;
            setGridData(GvDetail_Cutoff_List_one, ViewState["tempList2"]);
        }
        else
        {
            GvDetail_Cutoff_List_one.Visible = false;
        }
    }

    protected void Select_m0_typedevice()
    {
        FormView FvInsertDevice = (FormView)ViewAdd.FindControl("FvInsertDevice");
        DropDownList ddl_typedevice = (DropDownList)FvInsertDevice.FindControl("ddl_add_typedevice");

        ddl_typedevice.Items.Clear();
        ddl_typedevice.AppendDataBoundItems = true;
        ddl_typedevice.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์...", "0"));

        _data_deviceregister.m0_typedevice_list = new m0_typedevice_detail[1];
        m0_typedevice_detail _data_typedevice_detail = new m0_typedevice_detail();

        _data_typedevice_detail.m0_tdidx = 1;

        _data_deviceregister.m0_typedevice_list[0] = _data_typedevice_detail;

        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_deviceregister));
        _data_deviceregister = callServiceDevices(_url_m0_typedevice, _data_deviceregister);

        ddl_typedevice.DataSource = _data_deviceregister.m0_typedevice_list;
        ddl_typedevice.DataTextField = "name_m0_typedevice";
        ddl_typedevice.DataValueField = "m0_tdidx";
        ddl_typedevice.DataBind();
    }

    protected void Select_master_m0_type()
    {
        FormView FvInsertMasterDevice = (FormView)ViewaddMasterdata.FindControl("FvInsertMasterDevice");
        DropDownList ddl_addmaster_typedevice = (DropDownList)FvInsertDevice.FindControl("ddl_addmaster_typedevice");

        ddl_addmaster_typedevice.Items.Clear();
        ddl_addmaster_typedevice.AppendDataBoundItems = true;
        ddl_addmaster_typedevice.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์...", "0"));

        _data_deviceregister.m0_typedevice_list = new m0_typedevice_detail[1];
        m0_typedevice_detail _data_typedevice_detail = new m0_typedevice_detail();

        _data_typedevice_detail.m0_tdidx = 1;

        _data_deviceregister.m0_typedevice_list[0] = _data_typedevice_detail;

        _data_deviceregister = callServiceDevices(_url_m0_typedevice, _data_deviceregister);
        ddl_addmaster_typedevice.DataSource = _data_deviceregister.m0_typedevice_list;
        ddl_addmaster_typedevice.DataTextField = "name_m0_typedevice";
        ddl_addmaster_typedevice.DataValueField = "m0_tdidx";
        ddl_addmaster_typedevice.DataBind();
    }

    protected void Select_m0_insurance()
    {
        FormView FvInsertDevice = (FormView)ViewAdd.FindControl("FvInsertDevice");
        DropDownList ddl_add_Insurance = (DropDownList)FvInsertDevice.FindControl("ddl_add_Insurance");

        ddl_add_Insurance.Items.Clear();
        ddl_add_Insurance.AppendDataBoundItems = true;
        ddl_add_Insurance.Items.Add(new ListItem("กรุณาเลือกบริษัทประกัน...", "0"));

        _data_deviceregister.m0_insurance_list = new m0_insurance_detail[1];
        m0_insurance_detail _data_device_detail = new m0_insurance_detail();

        //_data_device_detail.m0_iridx = 1;

        _data_deviceregister.m0_insurance_list[0] = _data_device_detail;

        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_deviceregister));
        _data_deviceregister = callServiceDevices(_url_m0_insurance, _data_deviceregister);

        ddl_add_Insurance.DataSource = _data_deviceregister.m0_insurance_list;
        ddl_add_Insurance.DataTextField = "name_m0_insurance";
        ddl_add_Insurance.DataValueField = "m0_iridx";
        ddl_add_Insurance.DataBind();
    }

    protected void Select_m0_band()
    {
        FormView FvInsertDevice = (FormView)ViewAdd.FindControl("FvInsertDevice");
        DropDownList ddl_add_band = (DropDownList)FvInsertDevice.FindControl("ddl_add_band");

        ddl_add_band.Items.Clear();
        ddl_add_band.AppendDataBoundItems = true;
        ddl_add_band.Items.Add(new ListItem("กรุณาเลือกยี่ห้อ...", "0"));

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        _data_deviceregister.u0_device_list[0] = _data_device_detail;


        _data_deviceregister = callServicePostDevice(_url_m0_band, _data_deviceregister);
        ddl_add_band.DataSource = _data_deviceregister.m0_band_list;
        ddl_add_band.DataTextField = "name_m0_band";
        ddl_add_band.DataValueField = "m0_bidx";
        ddl_add_band.DataBind();
        //_data_deviceregister = callServicePostDevice_Post(_url_m0_band, _data_deviceregister);
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_deviceregister));
    }

    protected void Select_m0_status()
    {
        FormView FvInsertDevice = (FormView)ViewAdd.FindControl("FvInsertDevice");
        DropDownList ddl_add_status = (DropDownList)FvInsertDevice.FindControl("ddl_add_status");

        ddl_add_status.Items.Clear();
        ddl_add_status.AppendDataBoundItems = true;
        ddl_add_status.Items.Add(new ListItem("กรุณาเลือกสถานะอุปกรณ์...", "0"));

        _data_deviceregister.m0_status_list = new m0_status_detail[1];
        m0_status_detail _data_device_detail = new m0_status_detail();

        _data_device_detail.detail_m0_status = "1,2";

        _data_deviceregister.m0_status_list[0] = _data_device_detail;

        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_deviceregister));
        _data_deviceregister = callServiceDevices(_url_m0_status, _data_deviceregister);

        ddl_add_status.DataSource = _data_deviceregister.m0_status_list;
        ddl_add_status.DataTextField = "name_m0_status";
        ddl_add_status.DataValueField = "m0_sidx";
        ddl_add_status.DataBind();
    }

    protected void Select_m0_level()
    {
        FormView FvInsertDevice = (FormView)ViewAdd.FindControl("FvInsertDevice");
        DropDownList ddl_add_level = (DropDownList)FvInsertDevice.FindControl("ddl_add_level");

        ddl_add_level.Items.Clear();
        ddl_add_level.AppendDataBoundItems = true;
        ddl_add_level.Items.Add(new ListItem("กรุณาเลือกระดับ...", "0"));

        _data_deviceregister.m0_level_list = new m0_level_detail[1];
        m0_level_detail _data_device_detail = new m0_level_detail();

        //_data_device_detail.m0_lvidx = 1;

        _data_deviceregister.m0_level_list[0] = _data_device_detail;

        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_deviceregister));
        _data_deviceregister = callServiceDevices(_url_m0_level, _data_deviceregister);

        ddl_add_level.DataSource = _data_deviceregister.m0_level_list;
        ddl_add_level.DataTextField = "name_m0_level";
        ddl_add_level.DataValueField = "m0_lvidx";
        ddl_add_level.DataBind();
    }

    protected void Select_m0_cpu()
    {
        FormView FvInsertDevice = (FormView)ViewAdd.FindControl("FvInsertDevice");
        DropDownList ddl_add_cpu = (DropDownList)FvInsertDevice.FindControl("ddl_add_cpu");

        ddl_add_cpu.Items.Clear();
        ddl_add_cpu.AppendDataBoundItems = true;
        ddl_add_cpu.Items.Add(new ListItem("กรุณาเลือก cpu...", "0"));

        _data_deviceregister.m0_cpu_list = new m0_cpu_detail[1];
        m0_cpu_detail _data_device_detail = new m0_cpu_detail();

        _data_deviceregister.m0_cpu_list[0] = _data_device_detail;

        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_deviceregister));
        _data_deviceregister = callServiceDevices(_url_m0_cpu, _data_deviceregister);

        ddl_add_cpu.DataSource = _data_deviceregister.m0_cpu_list;
        ddl_add_cpu.DataTextField = "ddl_band_cpu";
        ddl_add_cpu.DataValueField = "m0_cidx";
        ddl_add_cpu.DataBind();
    }

    protected void Select_m0_ram()
    {
        FormView FvInsertDevice = (FormView)ViewAdd.FindControl("FvInsertDevice");
        DropDownList ddl_add_ram = (DropDownList)FvInsertDevice.FindControl("ddl_add_ram");

        ddl_add_ram.Items.Clear();
        ddl_add_ram.AppendDataBoundItems = true;
        ddl_add_ram.Items.Add(new ListItem("กรุณาเลือก ram...", "0"));

        _data_deviceregister.m0_ram_list = new m0_ram_detail[1];
        m0_ram_detail _data_device_detail = new m0_ram_detail();

        //_data_device_detail.m0_ridx = 1;

        _data_deviceregister.m0_ram_list[0] = _data_device_detail;

        _data_deviceregister = callServiceDevices(_url_m0_ram, _data_deviceregister);

        ddl_add_ram.DataSource = _data_deviceregister.m0_ram_list;
        ddl_add_ram.DataTextField = "ddl_ram";
        ddl_add_ram.DataValueField = "m0_ridx";
        ddl_add_ram.DataBind();
    }

    protected void Select_m0_hdd()
    {
        FormView FvInsertDevice = (FormView)ViewAdd.FindControl("FvInsertDevice");
        DropDownList ddl_add_hdd = (DropDownList)FvInsertDevice.FindControl("ddl_add_hdd");

        ddl_add_hdd.Items.Clear();
        ddl_add_hdd.AppendDataBoundItems = true;
        ddl_add_hdd.Items.Add(new ListItem("กรุณาเลือก hdd...", "0"));

        _data_deviceregister.m0_hdd_list = new m0_hdd_detail[1];
        m0_hdd_detail _data_device_detail = new m0_hdd_detail();

        //_data_device_detail.m0_hidx = 1;

        _data_deviceregister.m0_hdd_list[0] = _data_device_detail;

        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_deviceregister));
        _data_deviceregister = callServiceDevices(_url_m0_hdd, _data_deviceregister);

        ddl_add_hdd.DataSource = _data_deviceregister.m0_hdd_list;
        ddl_add_hdd.DataTextField = "band_hdd";
        ddl_add_hdd.DataValueField = "m0_hidx";
        ddl_add_hdd.DataBind();
    }

    protected void Select_m0_vga()
    {
        FormView FvInsertDevice = (FormView)ViewAdd.FindControl("FvInsertDevice");
        DropDownList ddl_add_vga = (DropDownList)FvInsertDevice.FindControl("ddl_add_vga");

        ddl_add_vga.Items.Clear();
        ddl_add_vga.AppendDataBoundItems = true;
        ddl_add_vga.Items.Add(new ListItem("กรุณาเลือก vga...", "0"));

        _data_deviceregister.m0_vga_list = new m0_vga_detail[1];
        m0_vga_detail _data_device_detail = new m0_vga_detail();

        _data_deviceregister.m0_vga_list[0] = _data_device_detail;

        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _data_deviceregister = callServiceDevices(_url_m0_vga, _data_deviceregister);
        ddl_add_vga.DataSource = _data_deviceregister.m0_vga_list;
        ddl_add_vga.DataTextField = "type_vga";
        ddl_add_vga.DataValueField = "m0_vidx";
        ddl_add_vga.DataBind();
    }

    protected void Select_m0_printer()
    {
        //FormView FvInsertDevice = (FormView)ViewAdd.FindControl("FvInsertDevice");
        DropDownList ddl_add_printer = (DropDownList)FvInsertDevice.FindControl("ddl_add_printer");

        ddl_add_printer.Items.Clear();
        ddl_add_printer.AppendDataBoundItems = true;
        ddl_add_printer.Items.Add(new ListItem("กรุณาเลือก printer...", "0"));

        _data_deviceregister.m0_printer_list = new m0_printer_detail[1];
        m0_printer_detail _data_device_detail = new m0_printer_detail();

        //_data_device_detail.m0_pidx = 1;

        _data_deviceregister.m0_printer_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_printer, _data_deviceregister);

        ddl_add_printer.DataSource = _data_deviceregister.m0_printer_list;
        ddl_add_printer.DataTextField = "band_print";
        ddl_add_printer.DataValueField = "m0_pidx";
        ddl_add_printer.DataBind();
    }

    protected void Select_m0_moniter()
    {
        DropDownList ddl_add_monitor = (DropDownList)FvInsertDevice.FindControl("ddl_add_monitor");

        ddl_add_monitor.Items.Clear();
        ddl_add_monitor.AppendDataBoundItems = true;
        ddl_add_monitor.Items.Add(new ListItem("กรุณาเลือก moniter...", "0"));

        _data_deviceregister.m0_moniter_list = new m0_moniter_detail[1];
        m0_moniter_detail _data_device_detail = new m0_moniter_detail();

        //_data_device_detail.m0_midx = 1;

        _data_deviceregister.m0_moniter_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_moniter, _data_deviceregister);

        ddl_add_monitor.DataSource = _data_deviceregister.m0_moniter_list;
        ddl_add_monitor.DataTextField = "band_moniter";
        ddl_add_monitor.DataValueField = "m0_midx";
        ddl_add_monitor.DataBind();
    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_en", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "-1"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_en", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "-1"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_en", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "-1"));
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }




    protected void Select_edit_u0_device_code_cpu(int didx)
    {
        GridView GvDetail_CPU = (GridView)FvViewDetailDevice.FindControl("GvDetail_CPU");
        GridView GvDetail_CPU_Tranfer = (GridView)FvViewDetailDevice.FindControl("GvDetail_CPU_Tranfer");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        _data_device_detail.m0_tdidx = 6;
        _data_device_detail.u0_relation_didx = didx;
        _data_device_detail.doc_status = ViewState["check_approve"].ToString();

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            GvDetail_CPU.Visible = false;
        }
        else
        {
            GvDetail_CPU.Visible = true;
            GvDetail_CPU.DataSource = _data_deviceregister.u0_device_list;
            GvDetail_CPU.DataBind();
        }

        foreach (GridViewRow row in GvDetail_CPU.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                Label lblu0_didx_tranfer = (Label)row.Cells[0].FindControl("lblu0_didx_tranfer");
                temp += lblu0_didx_tranfer.Text + ",";
            }
        }

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail_1 = new u0_device_detail();

        _data_device_detail_1.m0_tdidx = 6;
        _data_device_detail_1.u0_relation_didx = 0;
        _data_device_detail_1.u0_referent = temp;
        _data_device_detail_1.doc_status = "2"; //เฉพาะที่ status ที่เปลี่ยนแปลงเท่านั้น

        _data_deviceregister.u0_device_list[0] = _data_device_detail_1;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister));
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            GvDetail_CPU_Tranfer.Visible = false;
        }
        else
        {
            GvDetail_CPU_Tranfer.Visible = true;
            GvDetail_CPU_Tranfer.DataSource = _data_deviceregister.u0_device_list;
            GvDetail_CPU_Tranfer.DataBind();
        }
    }

    protected void Select_edit_u0_device_code_ram(int didx)
    {
        GridView GvDetail_RAM = (GridView)FvViewDetailDevice.FindControl("GvDetail_RAM");
        GridView GvDetail_RAM_Tranfer = (GridView)FvViewDetailDevice.FindControl("GvDetail_RAM_Tranfer");
        string temp_1 = "";

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        _data_device_detail.m0_tdidx = 7;
        _data_device_detail.u0_relation_didx = didx;
        _data_device_detail.doc_status = ViewState["check_approve"].ToString();

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            GvDetail_RAM.Visible = false;
        }
        else
        {
            GvDetail_RAM.Visible = true;
            GvDetail_RAM.DataSource = _data_deviceregister.u0_device_list;
            GvDetail_RAM.DataBind();
        }

        foreach (GridViewRow row in GvDetail_RAM.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                Label lblu0_didx_tranfer_ram = (Label)row.Cells[0].FindControl("lblu0_didx_tranfer_ram");
                temp_1 += lblu0_didx_tranfer_ram.Text + ",";
            }
        }

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail_1 = new u0_device_detail();

        _data_device_detail_1.m0_tdidx = 7;
        _data_device_detail_1.u0_relation_didx = 0;
        _data_device_detail_1.u0_referent = temp_1;
        _data_device_detail_1.doc_status = "2"; //เฉพาะที่ status ที่เปลี่ยนแปลงเท่านั้น

        _data_deviceregister.u0_device_list[0] = _data_device_detail_1;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister));
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            GvDetail_RAM_Tranfer.Visible = false;
        }
        else
        {
            GvDetail_RAM_Tranfer.Visible = true;
            GvDetail_RAM_Tranfer.DataSource = _data_deviceregister.u0_device_list;
            GvDetail_RAM_Tranfer.DataBind();
        }
    }

    protected void Select_edit_u0_device_code_hdd(int didx)
    {
        GridView GvDetail_HDD = (GridView)FvViewDetailDevice.FindControl("GvDetail_HDD");
        GridView GvDetail_HDD_Tranfer = (GridView)FvViewDetailDevice.FindControl("GvDetail_HDD_Tranfer");
        string temp_2 = "";

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        _data_device_detail.m0_tdidx = 8;
        _data_device_detail.u0_relation_didx = didx;
        _data_device_detail.doc_status = ViewState["check_approve"].ToString();

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            GvDetail_HDD.Visible = false;
        }
        else
        {
            GvDetail_HDD.Visible = true;
            GvDetail_HDD.DataSource = _data_deviceregister.u0_device_list;
            GvDetail_HDD.DataBind();
        }

        foreach (GridViewRow row in GvDetail_HDD.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                Label lblu0_didx_tranfer_hdd = (Label)row.Cells[0].FindControl("lblu0_didx_tranfer_hdd");
                temp_2 += lblu0_didx_tranfer_hdd.Text + ",";
            }
        }

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail_1 = new u0_device_detail();

        _data_device_detail_1.m0_tdidx = 8;
        _data_device_detail_1.u0_relation_didx = 0;
        _data_device_detail_1.u0_referent = temp_2;
        _data_device_detail_1.doc_status = "2"; //เฉพาะที่ status ที่เปลี่ยนแปลงเท่านั้น

        _data_deviceregister.u0_device_list[0] = _data_device_detail_1;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister));
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            GvDetail_HDD_Tranfer.Visible = false;
        }
        else
        {
            GvDetail_HDD_Tranfer.Visible = true;
            GvDetail_HDD_Tranfer.DataSource = _data_deviceregister.u0_device_list;
            GvDetail_HDD_Tranfer.DataBind();
        }
    }

    protected void Select_edit_u0_device_code_vga(int didx)
    {
        GridView GvDetail_VGA = (GridView)FvViewDetailDevice.FindControl("GvDetail_VGA");
        GridView GvDetail_VGA_Tranfer = (GridView)FvViewDetailDevice.FindControl("GvDetail_VGA_Tranfer");
        string temp_3 = "";

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        _data_device_detail.m0_tdidx = 9;
        _data_device_detail.u0_relation_didx = didx;
        _data_device_detail.doc_status = ViewState["check_approve"].ToString();

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            GvDetail_VGA.Visible = false;
        }
        else
        {
            GvDetail_VGA.Visible = true;
            GvDetail_VGA.DataSource = _data_deviceregister.u0_device_list;
            GvDetail_VGA.DataBind();
        }

        foreach (GridViewRow row in GvDetail_VGA.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                Label lblu0_didx_tranfer_vga = (Label)row.Cells[0].FindControl("lblu0_didx_tranfer_vga");
                temp_3 += lblu0_didx_tranfer_vga.Text + ",";
            }
        }

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail_1 = new u0_device_detail();

        _data_device_detail_1.m0_tdidx = 9;
        _data_device_detail_1.u0_relation_didx = 0;
        _data_device_detail_1.u0_referent = temp_3;
        _data_device_detail_1.doc_status = "2"; //เฉพาะที่ status ที่เปลี่ยนแปลงเท่านั้น

        _data_deviceregister.u0_device_list[0] = _data_device_detail_1;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister));
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            GvDetail_VGA_Tranfer.Visible = false;
        }
        else
        {
            GvDetail_VGA_Tranfer.Visible = true;
            GvDetail_VGA_Tranfer.DataSource = _data_deviceregister.u0_device_list;
            GvDetail_VGA_Tranfer.DataBind();
        }
    }

    protected void Select_edit_u0_device_code_etc(int didx)
    {
        GridView GvDetail_ETC = (GridView)FvViewDetailDevice.FindControl("GvDetail_ETC");
        GridView GvDetail_ETC_Tranfer = (GridView)FvViewDetailDevice.FindControl("GvDetail_ETC_Tranfer");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        _data_device_detail.m0_tdidx = 10;
        _data_device_detail.u0_relation_didx = didx;
        _data_device_detail.doc_status = ViewState["check_approve"].ToString();

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            GvDetail_ETC.Visible = false;
        }
        else
        {
            GvDetail_ETC.Visible = true;
            GvDetail_ETC.DataSource = _data_deviceregister.u0_device_list;
            GvDetail_ETC.DataBind();
        }

        foreach (GridViewRow row in GvDetail_ETC.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                Label lblu0_didx_tranfer = (Label)row.Cells[0].FindControl("lblu0_didx_tranfer_etc");
                temp += lblu0_didx_tranfer.Text + ",";
            }
        }

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail_1 = new u0_device_detail();

        _data_device_detail_1.m0_tdidx = 10;
        _data_device_detail_1.u0_relation_didx = 0;
        _data_device_detail_1.u0_referent = temp;
        _data_device_detail_1.doc_status = "2"; //เฉพาะที่ status ที่เปลี่ยนแปลงเท่านั้น

        _data_deviceregister.u0_device_list[0] = _data_device_detail_1;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister));
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            GvDetail_ETC_Tranfer.Visible = false;
        }
        else
        {
            GvDetail_ETC_Tranfer.Visible = true;
            GvDetail_ETC_Tranfer.DataSource = _data_deviceregister.u0_device_list;
            GvDetail_ETC_Tranfer.DataBind();
        }
    }



    protected void select_viewdetail_cpu(int didx)
    {
        DropDownList ddl_viewdetail_cpu = (DropDownList)FvViewDetailDevice.FindControl("ddl_viewdetail_cpu");

        ddl_viewdetail_cpu.Items.Clear();
        ddl_viewdetail_cpu.AppendDataBoundItems = true;
        //ddl_view_cpu.Items.Add(new ListItem("กรุณาเลือก cpu...", "0"));

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        _data_device_detail.m0_tdidx = 6;
        _data_device_detail.u0_relation_didx = didx;
        _data_device_detail.doc_status = "1";

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            ddl_viewdetail_cpu.DataSource = null;
            ddl_viewdetail_cpu.DataBind();
        }
        else
        {
            ddl_viewdetail_cpu.DataSource = _data_deviceregister.u0_device_list;
            ddl_viewdetail_cpu.DataTextField = "ddl_band_cpu";
            ddl_viewdetail_cpu.DataValueField = "u0_didx";
            ddl_viewdetail_cpu.DataBind();
        }
    }

    protected void select_viewdetail_ram(int didx)
    {
        DropDownList ddl_viewdetail_ram = (DropDownList)FvViewDetailDevice.FindControl("ddl_viewdetail_ram");

        ddl_viewdetail_ram.Items.Clear();
        ddl_viewdetail_ram.AppendDataBoundItems = true;
        //ddl_view_cpu.Items.Add(new ListItem("กรุณาเลือก cpu...", "0"));

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        _data_device_detail.m0_tdidx = 7;
        _data_device_detail.u0_relation_didx = didx;
        _data_device_detail.doc_status = "1";

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            ddl_viewdetail_ram.DataSource = null;
            ddl_viewdetail_ram.DataBind();
        }
        else
        {
            ddl_viewdetail_ram.DataSource = _data_deviceregister.u0_device_list;
            ddl_viewdetail_ram.DataTextField = "ddl_band_ram";
            ddl_viewdetail_ram.DataValueField = "u0_didx";
            ddl_viewdetail_ram.DataBind();
        }
    }

    protected void select_viewdetail_hdd(int didx)
    {
        DropDownList ddl_viewdetail_hdd = (DropDownList)FvViewDetailDevice.FindControl("ddl_viewdetail_hdd");

        ddl_viewdetail_hdd.Items.Clear();
        ddl_viewdetail_hdd.AppendDataBoundItems = true;
        //ddl_view_cpu.Items.Add(new ListItem("กรุณาเลือก cpu...", "0"));

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        _data_device_detail.m0_tdidx = 8;
        _data_device_detail.u0_relation_didx = didx;
        _data_device_detail.doc_status = "1";

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            ddl_viewdetail_hdd.DataSource = null;
            ddl_viewdetail_hdd.DataBind();
        }
        else
        {
            ddl_viewdetail_hdd.DataSource = _data_deviceregister.u0_device_list;
            ddl_viewdetail_hdd.DataTextField = "ddl_band_hdd";
            ddl_viewdetail_hdd.DataValueField = "u0_didx";
            ddl_viewdetail_hdd.DataBind();
        }
    }

    protected void select_viewdetail_vga(int didx)
    {
        DropDownList ddl_viewdetail_vga = (DropDownList)FvViewDetailDevice.FindControl("ddl_viewdetail_vga");

        ddl_viewdetail_vga.Items.Clear();
        ddl_viewdetail_vga.AppendDataBoundItems = true;
        //ddl_view_cpu.Items.Add(new ListItem("กรุณาเลือก cpu...", "0"));

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        _data_device_detail.m0_tdidx = 9;
        _data_device_detail.u0_relation_didx = didx;
        _data_device_detail.doc_status = "1";

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            ddl_viewdetail_vga.DataSource = null;
            ddl_viewdetail_vga.DataBind();
        }
        else
        {
            ddl_viewdetail_vga.DataSource = _data_deviceregister.u0_device_list;
            ddl_viewdetail_vga.DataTextField = "ddl_band_vga";
            ddl_viewdetail_vga.DataValueField = "u0_didx";
            ddl_viewdetail_vga.DataBind();
        }
    }



    protected void Search_m0_status()
    {
        DropDownList ddl_add_status = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_search_statusdevice");

        ddl_add_status.Items.Clear();
        ddl_add_status.AppendDataBoundItems = true;
        ddl_add_status.Items.Add(new ListItem("กรุณาเลือกสถานะอุปกรณ์...", "0"));

        _data_deviceregister.m0_status_list = new m0_status_detail[1];
        m0_status_detail _data_device_detail = new m0_status_detail();

        _data_device_detail.m0_sidx = 1;

        _data_deviceregister.m0_status_list[0] = _data_device_detail;

        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_deviceregister));
        _data_deviceregister = callServiceDevices(_url_m0_status, _data_deviceregister);

        ddl_add_status.DataSource = _data_deviceregister.m0_status_list;
        ddl_add_status.DataTextField = "name_m0_status";
        ddl_add_status.DataValueField = "m0_sidx";
        ddl_add_status.DataBind();
    }

    protected void Select_Searchtranfer_u0_device()
    {
        GridView GvDeviceDatabase = (GridView)FvViewDetailDevice.FindControl("GvDeviceDatabase");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.m0_tdidx = int.Parse(ViewState["Search_view_typedevice"].ToString());
        _data_device.m0_sidx = int.Parse(ViewState["Search_view_statusdevice"].ToString());
        _data_device.u0_code = ViewState["Search_view_u0_code"].ToString();
        _data_device.u0_acc = ViewState["Search_view_assetcode"].ToString();
        _data_device.u0_serial = ViewState["Search_view_seria"].ToString();

        _data_deviceregister.u0_device_list[0] = _data_device;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_24, _data_deviceregister);
        ViewState["DeviceDatabase"] = _data_deviceregister;
        ViewState["Seach_DeviceDatabase"] = _data_deviceregister;
        setGridData(GvDeviceDatabase, _data_deviceregister.u0_device_list);
    }

    protected void Select_Searchsoftware_u0_device()
    {
        GridView GvDeviceDatabase_software = (GridView)FvViewDetailDevice.FindControl("GvDeviceDatabase_software");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.software_type = ViewState["Search_typesoftware"].ToString();
        _data_device.m0_orgidx = int.Parse(ViewState["Search_orgsoftware"].ToString());
        //_data_device. = ViewState["Search_softwarecode"].ToString();

        _data_deviceregister.u0_device_list[0] = _data_device;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_26, _data_deviceregister);
        ViewState["DeviceDatabase_software"] = _data_deviceregister;
        ViewState["Seach_DeviceDatabase_software"] = _data_deviceregister;
        setGridData(GvDeviceDatabase_software, _data_deviceregister.u0_device_list);
    }

    protected void Select_Gridview_m0_cpu()
    {
        GridView Gv_Master_cpu = (GridView)ViewaddMasterdata.FindControl("Gv_Master_cpu");
        _data_deviceregister.m0_cpu_list = new m0_cpu_detail[1];
        m0_cpu_detail _data_device_detail = new m0_cpu_detail();

        _data_deviceregister.m0_cpu_list[0] = _data_device_detail;

        _data_deviceregister = callServiceDevices(_url_m0_cpu, _data_deviceregister);

        Gv_Master_cpu.DataSource = _data_deviceregister.m0_cpu_list;
        Gv_Master_cpu.DataBind();
    }

    protected void Select_Gridview_m0_ram()
    {
        GridView Gv_Master_ram = (GridView)ViewaddMasterdata.FindControl("Gv_Master_ram");

        _data_deviceregister.m0_ram_list = new m0_ram_detail[1];
        m0_ram_detail _data_device_detail = new m0_ram_detail();

        //_data_device_detail.m0_ridx = 1;

        _data_deviceregister.m0_ram_list[0] = _data_device_detail;

        _data_deviceregister = callServiceDevices(_url_m0_ram, _data_deviceregister);

        Gv_Master_ram.DataSource = _data_deviceregister.m0_ram_list;
        Gv_Master_ram.DataBind();
    }

    protected void Select_Gridview_m0_hdd()
    {
        GridView Gv_Master_hdd = (GridView)ViewaddMasterdata.FindControl("Gv_Master_hdd");

        _data_deviceregister.m0_hdd_list = new m0_hdd_detail[1];
        m0_hdd_detail _data_device_detail = new m0_hdd_detail();

        //_data_device_detail.m0_hidx = 1;

        _data_deviceregister.m0_hdd_list[0] = _data_device_detail;

        _data_deviceregister = callServiceDevices(_url_m0_hdd, _data_deviceregister);
        Gv_Master_hdd.DataSource = _data_deviceregister.m0_hdd_list;
        Gv_Master_hdd.DataBind();
    }

    protected void Select_Gridview_m0_vga()
    {
        GridView Gv_Master_vga = (GridView)ViewaddMasterdata.FindControl("Gv_Master_vga");

        _data_deviceregister.m0_vga_list = new m0_vga_detail[1];
        m0_vga_detail _data_device_detail = new m0_vga_detail();

        //_data_device_detail.m0_vidx = 1;

        _data_deviceregister.m0_vga_list[0] = _data_device_detail;

        _data_deviceregister = callServiceDevices(_url_m0_vga, _data_deviceregister);
        Gv_Master_vga.DataSource = _data_deviceregister.m0_vga_list;
        Gv_Master_vga.DataBind();
    }

    protected void Select_Gridview_m0_printer()
    {
        GridView Gv_Master_printer = (GridView)ViewaddMasterdata.FindControl("Gv_Master_printer");

        _data_deviceregister.m0_printer_list = new m0_printer_detail[1];
        m0_printer_detail _data_device_detail = new m0_printer_detail();

        //_data_device_detail.m0_pidx = 1;

        _data_deviceregister.m0_printer_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_printer, _data_deviceregister);

        Gv_Master_printer.DataSource = _data_deviceregister.m0_printer_list;
        Gv_Master_printer.DataBind();
    }

    protected void Select_Gridview_m0_moniter()
    {
        GridView Gv_Master_moniter = (GridView)ViewaddMasterdata.FindControl("Gv_Master_moniter");

        _data_deviceregister.m0_moniter_list = new m0_moniter_detail[1];
        m0_moniter_detail _data_device_detail = new m0_moniter_detail();

        //_data_device_detail.m0_midx = 1;

        _data_deviceregister.m0_moniter_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_moniter, _data_deviceregister);

        Gv_Master_moniter.DataSource = _data_deviceregister.m0_moniter_list;
        Gv_Master_moniter.DataBind();
    }

    protected void Select_Gridview_m0_typedevice()
    {
        GridView Gv_Master_typedevice = (GridView)ViewaddMasterdata.FindControl("Gv_Master_typedevice");

        _data_deviceregister.m0_typedevice_list = new m0_typedevice_detail[1];
        m0_typedevice_detail _data_typedevice_detail = new m0_typedevice_detail();

        //_data_typedevice_detail.m0_tdidx = 1;

        _data_deviceregister.m0_typedevice_list[0] = _data_typedevice_detail;
        _data_deviceregister = callServiceDevices(_url_m0_typedevice, _data_deviceregister);
        Gv_Master_typedevice.DataSource = _data_deviceregister.m0_typedevice_list;
        Gv_Master_typedevice.DataBind();
    }

    protected void Select_Gridview_m0_band()
    {
        GridView Gv_Master_band = (GridView)ViewaddMasterdata.FindControl("Gv_Master_band");

        _data_deviceregister.m0_band_list = new m0_band_detail[1];
        m0_band_detail _data_device_detail = new m0_band_detail();

        //_data_device_detail.m0_bidx = 1;

        _data_deviceregister.m0_band_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_band, _data_deviceregister);
        Gv_Master_band.DataSource = _data_deviceregister.m0_band_list;
        Gv_Master_band.DataBind();
    }

    protected void Select_Gridview_m0_level()
    {
        GridView Gv_Master_level = (GridView)ViewaddMasterdata.FindControl("Gv_Master_level");

        _data_deviceregister.m0_level_list = new m0_level_detail[1];
        m0_level_detail _data_device_detail = new m0_level_detail();

        //_data_device_detail.m0_lvidx = 1;

        _data_deviceregister.m0_level_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_level, _data_deviceregister);
        Gv_Master_level.DataSource = _data_deviceregister.m0_level_list;
        Gv_Master_level.DataBind();
    }

    protected void Select_Gridview_m0_status()
    {
        GridView Gv_Master_status = (GridView)ViewaddMasterdata.FindControl("Gv_Master_status");

        _data_deviceregister.m0_status_list = new m0_status_detail[1];
        m0_status_detail _data_device_detail = new m0_status_detail();

        //_data_device_detail.m0_sidx = 1;

        _data_deviceregister.m0_status_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_status, _data_deviceregister);
        Gv_Master_status.DataSource = _data_deviceregister.m0_status_list;
        Gv_Master_status.DataBind();
    }

    protected void Select_Gridview_m0_insurance()
    {
        GridView Gv_Master_insurance = (GridView)ViewaddMasterdata.FindControl("Gv_Master_insurance");

        _data_deviceregister.m0_insurance_list = new m0_insurance_detail[1];
        m0_insurance_detail _data_device_detail = new m0_insurance_detail();

        //_data_device_detail.m0_iridx = 1;

        _data_deviceregister.m0_insurance_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_insurance, _data_deviceregister);
        Gv_Master_insurance.DataSource = _data_deviceregister.m0_insurance_list;
        Gv_Master_insurance.DataBind();
    }


    protected void Edit_m0_insurance()
    {
        Label lbl_view_Insurance = (Label)FvViewDetailDevice.FindControl("lbl_view_Insurance");
        DropDownList ddl_view_Insurance = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_Insurance");

        ddl_view_Insurance.Items.Clear();
        ddl_view_Insurance.AppendDataBoundItems = true;
        ddl_view_Insurance.Items.Add(new ListItem("กรุณาเลือกบริษัทประกัน...", "0"));

        _data_deviceregister.m0_insurance_list = new m0_insurance_detail[1];
        m0_insurance_detail _data_device_detail = new m0_insurance_detail();

        _data_deviceregister.m0_insurance_list[0] = _data_device_detail;

        _data_deviceregister = callServicePostDevice(_url_m0_insurance, _data_deviceregister);
        ddl_view_Insurance.DataSource = _data_deviceregister.m0_insurance_list;
        ddl_view_Insurance.DataTextField = "name_m0_insurance";
        ddl_view_Insurance.DataValueField = "m0_iridx";
        ddl_view_Insurance.DataBind();
        ddl_view_Insurance.SelectedValue = lbl_view_Insurance.Text;
    }

    protected void Edit_m0_cpu()
    {
        Label lbl_view_cpu = (Label)FvViewDetailDevice.FindControl("lbl_view_cpu");
        DropDownList ddl_view_cpu = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_cpu");

        ddl_view_cpu.Items.Clear();
        ddl_view_cpu.AppendDataBoundItems = true;
        ddl_view_cpu.Items.Add(new ListItem("กรุณาเลือก cpu...", "0"));

        _data_deviceregister.m0_cpu_list = new m0_cpu_detail[1];
        m0_cpu_detail _data_device_detail = new m0_cpu_detail();

        _data_deviceregister.m0_cpu_list[0] = _data_device_detail;

        _data_deviceregister = callServiceDevices(_url_m0_cpu, _data_deviceregister);

        ddl_view_cpu.DataSource = _data_deviceregister.m0_cpu_list;
        ddl_view_cpu.DataTextField = "ddl_band_cpu";
        ddl_view_cpu.DataValueField = "m0_cidx";
        ddl_view_cpu.DataBind();
        ddl_view_cpu.SelectedValue = lbl_view_cpu.Text;
    }

    protected void Edit_m0_ram()
    {
        Label lbl_view_ram = (Label)FvViewDetailDevice.FindControl("lbl_view_ram");
        DropDownList ddl_view_ram = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_ram");

        ddl_view_ram.Items.Clear();
        ddl_view_ram.AppendDataBoundItems = true;
        ddl_view_ram.Items.Add(new ListItem("กรุณาเลือก ram...", "0"));

        _data_deviceregister.m0_ram_list = new m0_ram_detail[1];
        m0_ram_detail _data_device_detail = new m0_ram_detail();

        _data_deviceregister.m0_ram_list[0] = _data_device_detail;

        _data_deviceregister = callServiceDevices(_url_m0_ram, _data_deviceregister);

        ddl_view_ram.DataSource = _data_deviceregister.m0_ram_list;
        ddl_view_ram.DataTextField = "ddl_ram";
        ddl_view_ram.DataValueField = "m0_ridx";
        ddl_view_ram.DataBind();
        ddl_view_ram.SelectedValue = lbl_view_ram.Text;
    }

    protected void Edit_m0_hdd()
    {
        Label lbl_view_hdd = (Label)FvViewDetailDevice.FindControl("lbl_view_hdd");
        DropDownList ddl_view_hdd = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_hdd");

        ddl_view_hdd.Items.Clear();
        ddl_view_hdd.AppendDataBoundItems = true;
        ddl_view_hdd.Items.Add(new ListItem("กรุณาเลือก hdd...", "0"));

        _data_deviceregister.m0_hdd_list = new m0_hdd_detail[1];
        m0_hdd_detail _data_device_detail = new m0_hdd_detail();

        //_data_device_detail.m0_hidx = 1;

        _data_deviceregister.m0_hdd_list[0] = _data_device_detail;

        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_deviceregister));
        _data_deviceregister = callServiceDevices(_url_m0_hdd, _data_deviceregister);

        ddl_view_hdd.DataSource = _data_deviceregister.m0_hdd_list;
        ddl_view_hdd.DataTextField = "band_hdd";
        ddl_view_hdd.DataValueField = "m0_hidx";
        ddl_view_hdd.DataBind();
        ddl_view_hdd.SelectedValue = lbl_view_hdd.Text;
    }

    protected void Edit_m0_vga()
    {
        Label lbl_view_vga = (Label)FvViewDetailDevice.FindControl("lbl_view_vga");
        DropDownList ddl_view_vga = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_vga");

        ddl_view_vga.Items.Clear();
        ddl_view_vga.AppendDataBoundItems = true;
        ddl_view_vga.Items.Add(new ListItem("กรุณาเลือก vga...", "0"));

        _data_deviceregister.m0_vga_list = new m0_vga_detail[1];
        m0_vga_detail _data_device_detail = new m0_vga_detail();

        _data_deviceregister.m0_vga_list[0] = _data_device_detail;

        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _data_deviceregister = callServiceDevices(_url_m0_vga, _data_deviceregister);
        ddl_view_vga.DataSource = _data_deviceregister.m0_vga_list;
        ddl_view_vga.DataTextField = "type_vga";
        ddl_view_vga.DataValueField = "m0_vidx";
        ddl_view_vga.DataBind();
        ddl_view_vga.SelectedValue = lbl_view_vga.Text;
    }

    protected void Edit_m0_moniter()
    {
        Label lbl_view_moniter = (Label)FvViewDetailDevice.FindControl("lbl_view_moniter");
        DropDownList ddl_view_moniter = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_moniter");

        ddl_view_moniter.Items.Clear();
        ddl_view_moniter.AppendDataBoundItems = true;
        ddl_view_moniter.Items.Add(new ListItem("กรุณาเลือก moniter...", "0"));

        _data_deviceregister.m0_moniter_list = new m0_moniter_detail[1];
        m0_moniter_detail _data_device_detail = new m0_moniter_detail();

        _data_deviceregister.m0_moniter_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_moniter, _data_deviceregister);

        ddl_view_moniter.DataSource = _data_deviceregister.m0_moniter_list;
        ddl_view_moniter.DataTextField = "band_moniter";
        ddl_view_moniter.DataValueField = "m0_midx";
        ddl_view_moniter.DataBind();
        ddl_view_moniter.SelectedValue = lbl_view_moniter.Text;
    }

    protected void Edit_m0_printer()
    {
        Label lbl_view_printer = (Label)FvViewDetailDevice.FindControl("lbl_view_printer");
        DropDownList ddl_view_printer = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_printer");

        ddl_view_printer.Items.Clear();
        ddl_view_printer.AppendDataBoundItems = true;
        ddl_view_printer.Items.Add(new ListItem("กรุณาเลือก printer...", "0"));

        _data_deviceregister.m0_printer_list = new m0_printer_detail[1];
        m0_printer_detail _data_device_detail = new m0_printer_detail();

        _data_deviceregister.m0_printer_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_printer, _data_deviceregister);

        ddl_view_printer.DataSource = _data_deviceregister.m0_printer_list;
        ddl_view_printer.DataTextField = "band_print";
        ddl_view_printer.DataValueField = "m0_pidx";
        ddl_view_printer.DataBind();
        ddl_view_printer.SelectedValue = lbl_view_printer.Text;
    }

    protected void Edit_m0_band()
    {
        DropDownList ddl_view_band = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_band");
        Label lbl_view_band = (Label)FvViewDetailDevice.FindControl("lbl_view_band");

        ddl_view_band.Items.Clear();
        ddl_view_band.AppendDataBoundItems = true;
        ddl_view_band.Items.Add(new ListItem("กรุณาเลือกยี่ห้อ...", "0"));

        _data_deviceregister.m0_band_list = new m0_band_detail[1];
        m0_band_detail _data_device_detail = new m0_band_detail();

        _data_deviceregister.m0_band_list[0] = _data_device_detail;
        _data_deviceregister = callServicePostDevice(_url_m0_band, _data_deviceregister);
        ddl_view_band.DataSource = _data_deviceregister.m0_band_list;
        ddl_view_band.DataTextField = "name_m0_band";
        ddl_view_band.DataValueField = "m0_bidx";
        ddl_view_band.DataBind();
        ddl_view_band.SelectedValue = lbl_view_band.Text;
    }

    protected void Edit_m0_status()
    {
        DropDownList ddl_view_status = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_status");
        Label lbl_view_statusdevice = (Label)FvViewDetailDevice.FindControl("lbl_view_statusdevice");

        ddl_view_status.Items.Clear();
        ddl_view_status.AppendDataBoundItems = true;
        ddl_view_status.Items.Add(new ListItem("กรุณาเลือกสถานะอุปกรณ์...", "0"));

        _data_deviceregister.m0_status_list = new m0_status_detail[1];
        m0_status_detail _data_device_detail = new m0_status_detail();
        _data_device_detail.detail_m0_status = "1,2";
        _data_deviceregister.m0_status_list[0] = _data_device_detail;

        _data_deviceregister = callServicePostDevice(_url_m0_status, _data_deviceregister);
        ddl_view_status.DataSource = _data_deviceregister.m0_status_list;
        ddl_view_status.DataTextField = "name_m0_status";
        ddl_view_status.DataValueField = "m0_sidx";
        ddl_view_status.DataBind();
        ddl_view_status.SelectedValue = lbl_view_statusdevice.Text;
    }

    protected void Edit_m0_level()
    {
        DropDownList ddl_view_level = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_level");
        Label lbl_view_level = (Label)FvViewDetailDevice.FindControl("lbl_view_level");

        ddl_view_level.Items.Clear();
        ddl_view_level.AppendDataBoundItems = true;
        ddl_view_level.Items.Add(new ListItem("กรุณาเลือกระดับ...", "0"));

        _data_deviceregister.m0_level_list = new m0_level_detail[1];
        m0_level_detail _data_device_detail = new m0_level_detail();

        _data_deviceregister.m0_level_list[0] = _data_device_detail;

        _data_deviceregister = callServicePostDevice(_url_m0_level, _data_deviceregister);
        ddl_view_level.DataSource = _data_deviceregister.m0_level_list;
        ddl_view_level.DataTextField = "name_m0_level";
        ddl_view_level.DataValueField = "m0_lvidx";
        ddl_view_level.DataBind();
        ddl_view_level.SelectedValue = lbl_view_level.Text;
    }

    protected void Edit_m0_orgidx()
    {
        DropDownList ddl_view_org = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_org");
        DropDownList ddl_view_dept = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_dept");
        DropDownList ddl_view_sec = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_sec");
        Label lbl_view_org = (Label)FvViewDetailDevice.FindControl("lbl_view_org");
        Label lbl_view_dept = (Label)FvViewDetailDevice.FindControl("lbl_view_dept");
        Label lbl_view_sec = (Label)FvViewDetailDevice.FindControl("lbl_view_sec");

        ddl_view_org.Items.Clear();
        ddl_view_org.AppendDataBoundItems = true;
        ddl_view_org.Items.Add(new ListItem("เลือกองค์กร....", "0"));

        _dtEmployee = new data_employee();
        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail organite = new employee_detail();
        _dtEmployee.employee_list[0] = organite;
        _dtEmployee = callServicePostEmp(urlGetOrganization, _dtEmployee);
        ddl_view_org.DataSource = _dtEmployee.employee_list;
        ddl_view_org.DataTextField = "org_name_th";
        ddl_view_org.DataValueField = "org_idx";
        ddl_view_org.DataBind();
        ddl_view_org.SelectedValue = lbl_view_org.Text;


        ddl_view_dept.Items.Clear();
        ddl_view_dept.AppendDataBoundItems = true;
        ddl_view_dept.Items.Add(new ListItem("เลือกฝ่าย....", "0"));

        _dtEmployee = new data_employee();
        _dtEmployee.department_list = new department_details[1];
        department_details organite3 = new department_details();

        organite3.org_idx = int.Parse(lbl_view_org.Text);
        //organite3.rdept_idx = int.Parse(lbl_view_dept.Text);

        _dtEmployee.department_list[0] = organite3;
        _dtEmployee = callServicePostEmp(urlGetDepartmentList, _dtEmployee);
        ddl_view_dept.DataSource = _dtEmployee.department_list;
        ddl_view_dept.DataTextField = "dept_name_th";
        ddl_view_dept.DataValueField = "rdept_idx";
        ddl_view_dept.DataBind();
        ddl_view_dept.SelectedValue = lbl_view_dept.Text;


        ddl_view_sec.AppendDataBoundItems = true;
        ddl_view_sec.Items.Clear();
        ddl_view_sec.Items.Add(new ListItem("เลือกแผนก....", "0"));

        _dtEmployee = new data_employee();
        _dtEmployee.section_list = new section_details[1];
        section_details organite4 = new section_details();

        organite4.org_idx = int.Parse(lbl_view_org.Text);
        organite4.rdept_idx = int.Parse(lbl_view_dept.Text);

        _dtEmployee.section_list[0] = organite4;

        _dtEmployee = callServicePostEmp(urlGetSectionList, _dtEmployee);

        ddl_view_sec.DataSource = _dtEmployee.section_list;
        ddl_view_sec.DataTextField = "sec_name_th";
        ddl_view_sec.DataValueField = "rsec_idx";
        ddl_view_sec.DataBind();
        ddl_view_sec.SelectedValue = lbl_view_sec.Text;
    }


    protected void Select_FvDetail_Cpu(int didx)
    {
        FormView FvViewTranferDevice_cpu = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_cpu");
        FormView FvView_Detail_cpu = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_cpu");
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.u0_didx = didx;

        _data_deviceregister.u0_device_list[0] = _data_device;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvViewTranferDevice_cpu, _data_deviceregister.u0_device_list);

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_1 = new u0_device_detail();

        _data_device_1.u0_didx = didx;

        _data_deviceregister.u0_device_list[0] = _data_device_1;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvView_Detail_cpu, _data_deviceregister.u0_device_list);
    }

    protected void Select_Searchtranfer_u0_device_cpu()
    {
        FormView FvViewDetailDevice_2 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
        GridView GvDevice_Detail_Search_cpu = (GridView)FvViewDetailDevice_2.FindControl("GvDevice_Detail_Search_cpu");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.m0_tdidx = 6;
        _data_device.m0_sidx = int.Parse(ViewState["Search_view_statusdevice_cpu"].ToString());
        _data_device.u0_code = ViewState["Search_view_u0_code_cpu"].ToString();
        _data_device.u0_acc = ViewState["Search_view_assetcode_cpu"].ToString();
        _data_device.u0_serial = ViewState["Search_view_seria_cpu"].ToString();

        _data_deviceregister.u0_device_list[0] = _data_device;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister));
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_24, _data_deviceregister);
        setGridData(GvDevice_Detail_Search_cpu, _data_deviceregister.u0_device_list);
    }

    protected void Select_tranfer_status()
    {
        FormView FvViewDetailDevice = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
        FormView FvViewTranferDevice_cpu = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_cpu");
        DropDownList ddl_add_status = (DropDownList)FvViewTranferDevice_cpu.FindControl("ddl_Edit_search_statusdevice");

        ddl_add_status.Items.Clear();
        ddl_add_status.AppendDataBoundItems = true;
        ddl_add_status.Items.Add(new ListItem("กรุณาเลือกสถานะอุปกรณ์...", "0"));

        _data_deviceregister.m0_status_list = new m0_status_detail[1];
        m0_status_detail _data_device_detail = new m0_status_detail();
        _data_device_detail.detail_m0_status = "1,2";
        _data_deviceregister.m0_status_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_status, _data_deviceregister);
        ddl_add_status.DataSource = _data_deviceregister.m0_status_list;
        ddl_add_status.DataTextField = "name_m0_status";
        ddl_add_status.DataValueField = "m0_sidx";
        ddl_add_status.DataBind();
    }

    protected void Select_FvDetail_Ram(int didx)
    {
        FormView FvViewTranferDevice_ram = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_ram");
        FormView FvView_Detail_ram = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_ram");
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.u0_didx = didx;

        _data_deviceregister.u0_device_list[0] = _data_device;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvViewTranferDevice_ram, _data_deviceregister.u0_device_list);

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_1 = new u0_device_detail();

        _data_device_1.u0_didx = didx;

        _data_deviceregister.u0_device_list[0] = _data_device_1;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvView_Detail_ram, _data_deviceregister.u0_device_list);
    }

    protected void Select_Searchtranfer_u0_device_ram()
    {
        FormView FvViewDetailDevice_2 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
        GridView GvDevice_Detail_Search_ram = (GridView)FvViewDetailDevice_2.FindControl("GvDevice_Detail_Search_ram");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.m0_tdidx = 7;
        _data_device.m0_sidx = int.Parse(ViewState["Search_view_statusdevice_ram"].ToString());
        _data_device.u0_code = ViewState["Search_view_u0_code_ram"].ToString();
        _data_device.u0_acc = ViewState["Search_view_assetcode_ram"].ToString();
        _data_device.u0_serial = ViewState["Search_view_seria_ram"].ToString();

        _data_deviceregister.u0_device_list[0] = _data_device;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister));
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_24, _data_deviceregister);
        setGridData(GvDevice_Detail_Search_ram, _data_deviceregister.u0_device_list);
    }

    protected void Select_tranfer_status_ram()
    {
        FormView FvViewDetailDevice = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
        FormView FvViewTranferDevice_cpu = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_ram");
        DropDownList ddl_add_status = (DropDownList)FvViewTranferDevice_cpu.FindControl("ddl_Edit_search_statusdevice_ram");

        ddl_add_status.Items.Clear();
        ddl_add_status.AppendDataBoundItems = true;
        ddl_add_status.Items.Add(new ListItem("กรุณาเลือกสถานะอุปกรณ์...", "0"));

        _data_deviceregister.m0_status_list = new m0_status_detail[1];
        m0_status_detail _data_device_detail = new m0_status_detail();
        _data_device_detail.detail_m0_status = "1,2";
        _data_deviceregister.m0_status_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_status, _data_deviceregister);
        ddl_add_status.DataSource = _data_deviceregister.m0_status_list;
        ddl_add_status.DataTextField = "name_m0_status";
        ddl_add_status.DataValueField = "m0_sidx";
        ddl_add_status.DataBind();
    }

    protected void Select_FvDetail_Hdd(int didx)
    {
        FormView FvViewTranferDevice_hdd = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_hdd");
        FormView FvView_Detail_hdd = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_hdd");
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.u0_didx = didx;

        _data_deviceregister.u0_device_list[0] = _data_device;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvViewTranferDevice_hdd, _data_deviceregister.u0_device_list);

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_1 = new u0_device_detail();

        _data_device_1.u0_didx = didx;

        _data_deviceregister.u0_device_list[0] = _data_device_1;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvView_Detail_hdd, _data_deviceregister.u0_device_list);
    }

    protected void Select_Searchtranfer_u0_device_hdd()
    {
        FormView FvViewDetailDevice_3 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
        GridView GvDevice_Detail_Search_hdd = (GridView)FvViewDetailDevice_3.FindControl("GvDevice_Detail_Search_hdd");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.m0_tdidx = 8;
        _data_device.m0_sidx = int.Parse(ViewState["Search_view_statusdevice_hdd"].ToString());
        _data_device.u0_code = ViewState["Search_view_u0_code_hdd"].ToString();
        _data_device.u0_acc = ViewState["Search_view_assetcode_hdd"].ToString();
        _data_device.u0_serial = ViewState["Search_view_seria_hdd"].ToString();

        _data_deviceregister.u0_device_list[0] = _data_device;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister));
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_24, _data_deviceregister);
        setGridData(GvDevice_Detail_Search_hdd, _data_deviceregister.u0_device_list);
    }

    protected void Select_tranfer_status_hdd()
    {
        FormView FvViewDetailDevice = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
        FormView FvViewTranferDevice_hdd = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_hdd");
        DropDownList ddl_add_status = (DropDownList)FvViewTranferDevice_hdd.FindControl("ddl_Edit_search_statusdevice_hdd");

        ddl_add_status.Items.Clear();
        ddl_add_status.AppendDataBoundItems = true;
        ddl_add_status.Items.Add(new ListItem("กรุณาเลือกสถานะอุปกรณ์...", "0"));

        _data_deviceregister.m0_status_list = new m0_status_detail[1];
        m0_status_detail _data_device_detail = new m0_status_detail();
        _data_device_detail.detail_m0_status = "1,2";
        _data_deviceregister.m0_status_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_status, _data_deviceregister);
        ddl_add_status.DataSource = _data_deviceregister.m0_status_list;
        ddl_add_status.DataTextField = "name_m0_status";
        ddl_add_status.DataValueField = "m0_sidx";
        ddl_add_status.DataBind();
    }

    protected void Select_FvDetail_Vga(int didx)
    {
        FormView FvViewTranferDevice_vga = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_vga");
        FormView FvView_Detail_vga = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_vga");
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.u0_didx = didx;

        _data_deviceregister.u0_device_list[0] = _data_device;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvViewTranferDevice_vga, _data_deviceregister.u0_device_list);

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_1 = new u0_device_detail();

        _data_device_1.u0_didx = didx;

        _data_deviceregister.u0_device_list[0] = _data_device_1;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvView_Detail_vga, _data_deviceregister.u0_device_list);
    }

    protected void Select_Searchtranfer_u0_device_vga()
    {
        FormView FvViewDetailDevice_3 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
        GridView GvDevice_Detail_Search_vga = (GridView)FvViewDetailDevice_3.FindControl("GvDevice_Detail_Search_vga");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.m0_tdidx = 9;
        _data_device.m0_sidx = int.Parse(ViewState["Search_view_statusdevice_vga"].ToString());
        _data_device.u0_code = ViewState["Search_view_u0_code_vga"].ToString();
        _data_device.u0_acc = ViewState["Search_view_assetcode_vga"].ToString();
        _data_device.u0_serial = ViewState["Search_view_seria_vga"].ToString();

        _data_deviceregister.u0_device_list[0] = _data_device;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister));
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_24, _data_deviceregister);
        setGridData(GvDevice_Detail_Search_vga, _data_deviceregister.u0_device_list);
    }

    protected void Select_tranfer_status_vga()
    {
        FormView FvViewDetailDevice = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
        FormView FvViewTranferDevice_vga = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_vga");
        DropDownList ddl_add_status = (DropDownList)FvViewTranferDevice_vga.FindControl("ddl_Edit_search_statusdevice_vga");

        ddl_add_status.Items.Clear();
        ddl_add_status.AppendDataBoundItems = true;
        ddl_add_status.Items.Add(new ListItem("กรุณาเลือกสถานะอุปกรณ์...", "0"));

        _data_deviceregister.m0_status_list = new m0_status_detail[1];
        m0_status_detail _data_device_detail = new m0_status_detail();
        _data_device_detail.detail_m0_status = "1,2";
        _data_deviceregister.m0_status_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_status, _data_deviceregister);
        ddl_add_status.DataSource = _data_deviceregister.m0_status_list;
        ddl_add_status.DataTextField = "name_m0_status";
        ddl_add_status.DataValueField = "m0_sidx";
        ddl_add_status.DataBind();
    }

    protected void Select_FvDetail_etc(int didx)
    {
        FormView FvViewTranferDevice_etc = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_etc");
        FormView FvView_Detail_etc = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_etc");
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.u0_didx = didx;

        _data_deviceregister.u0_device_list[0] = _data_device;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvViewTranferDevice_etc, _data_deviceregister.u0_device_list);

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_1 = new u0_device_detail();

        _data_device_1.u0_didx = didx;

        _data_deviceregister.u0_device_list[0] = _data_device_1;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvView_Detail_etc, _data_deviceregister.u0_device_list);
    }

    protected void Select_Searchtranfer_u0_device_etc()
    {
        FormView FvViewDetailDevice_3 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
        GridView GvDevice_Detail_Search_etc = (GridView)FvViewDetailDevice_3.FindControl("GvDevice_Detail_Search_etc");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.m0_tdidx = 10;
        _data_device.m0_sidx = int.Parse(ViewState["Search_view_statusdevice_etc"].ToString());
        _data_device.u0_code = ViewState["Search_view_u0_code_etc"].ToString();
        _data_device.u0_acc = ViewState["Search_view_assetcode_etc"].ToString();
        _data_device.u0_serial = ViewState["Search_view_seria_etc"].ToString();

        _data_deviceregister.u0_device_list[0] = _data_device;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister));
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_24, _data_deviceregister);
        setGridData(GvDevice_Detail_Search_etc, _data_deviceregister.u0_device_list);
    }

    protected void Select_tranfer_status_etc()
    {
        FormView FvViewDetailDevice = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
        FormView FvViewTranferDevice_vga = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_etc");
        DropDownList ddl_add_status = (DropDownList)FvViewTranferDevice_vga.FindControl("ddl_Edit_search_statusdevice_etc");

        ddl_add_status.Items.Clear();
        ddl_add_status.AppendDataBoundItems = true;
        ddl_add_status.Items.Add(new ListItem("กรุณาเลือกสถานะอุปกรณ์...", "0"));

        _data_deviceregister.m0_status_list = new m0_status_detail[1];
        m0_status_detail _data_device_detail = new m0_status_detail();
        _data_device_detail.detail_m0_status = "1,2";
        _data_deviceregister.m0_status_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_status, _data_deviceregister);
        ddl_add_status.DataSource = _data_deviceregister.m0_status_list;
        ddl_add_status.DataTextField = "name_m0_status";
        ddl_add_status.DataValueField = "m0_sidx";
        ddl_add_status.DataBind();
    }


    protected void Select_Gridview_Approve()
    {
        FormView FvViewApproveDevice = (FormView)Viewapproveadddevice.FindControl("FvViewApproveDevice");
        Panel boxviewapprove = (Panel)Viewapproveadddevice.FindControl("boxviewapprove");
        GridView GvDetail_Approve_New = (GridView)FvViewApproveDevice.FindControl("GvDetail_Approve_New");
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.u0_referent_code = ViewState["u0_didx_Device_Main"].ToString();
        _data_device.HDEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _data_device.doc_status = "3";

        _data_deviceregister.u0_device_list[0] = _data_device;
        //fssq.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);


        if (_data_deviceregister.return_code != 1)
        {
            boxviewapprove.Visible = true;
            lb_guidebook.Visible = false;
            lb_flow.Visible = false;
            setGridData(GvDetail_Approve_New, _data_deviceregister.u0_device_list);
        }
        else
        {
            boxviewapprove.Visible = false;
            lb_guidebook.Visible = false;
            lb_flow.Visible = false;
            setGridData(GvDetail_Approve_New, null);
        }
    }

    protected void set_didx_approve(GridView Gv, int didx) //Rewrite ใหม่
    {
        string _ret = "";

        #region ViewAddDevice
        var dsEquipment_1 = new DataSet();
        dsEquipment_1.Tables.Add("device");

        dsEquipment_1.Tables[0].Columns.Add("u0_didx_List", typeof(string));

        ViewState["vsTempDevice_approve_transfer"] = dsEquipment_1;
        #endregion

        foreach (GridViewRow row in Gv.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow1 = (CheckBox)row.Cells[0].FindControl("CheckBox1");
                Label lbl_u0_didx_approve = (Label)row.Cells[0].FindControl("lbl_u0_didx_approve");

                if (chkRow1.Checked == true)
                {
                    var dsEquiment = (DataSet)ViewState["vsTempDevice_approve_transfer"];
                    var drEquiment = dsEquiment.Tables[0].NewRow();

                    drEquiment["u0_didx_List"] = lbl_u0_didx_approve.Text;

                    dsEquiment.Tables[0].Rows.Add(drEquiment);
                    ViewState["vsTempDevice_approve_transfer"] = dsEquiment;
                }
            }
        }

        var Search_2 = (DataSet)ViewState["vsTempDevice_approve_transfer"];
        string temp2 = "";
        foreach (DataRow dr in Search_2.Tables[0].Rows)
        {
            temp2 += dr["u0_didx_List"].ToString() + ",";
            ViewState["u0_relation_Approve"] = temp2;
        }
        _ret = ViewState["vsTempDevice_approve_transfer"].ToString();
        fs.Text = _ret;
    }


    protected void Search_status()
    {
        DropDownList ddl_add_status = (DropDownList)Fv_Search_Device_Index.FindControl("ddl_search_statusdevice_index");

        ddl_add_status.Items.Clear();
        ddl_add_status.AppendDataBoundItems = true;
        ddl_add_status.Items.Add(new ListItem("กรุณาเลือกสถานะอุปกรณ์...", "0"));

        _data_deviceregister.m0_status_list = new m0_status_detail[1];
        m0_status_detail _data_device_detail = new m0_status_detail();

        _data_deviceregister.m0_status_list[0] = _data_device_detail;
        _data_deviceregister = callServiceDevices(_url_m0_status, _data_deviceregister);
        ddl_add_status.DataSource = _data_deviceregister.m0_status_list;
        ddl_add_status.DataTextField = "name_m0_status";
        ddl_add_status.DataValueField = "m0_sidx";
        ddl_add_status.DataBind();
    }

    protected void Search_typedevice()
    {
        DropDownList ddl_typedevice = (DropDownList)Fv_Search_Device_Index.FindControl("ddl_search_typedevice_index");

        ddl_typedevice.Items.Clear();
        ddl_typedevice.AppendDataBoundItems = true;
        ddl_typedevice.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์...", "0"));

        _data_deviceregister.m0_typedevice_list = new m0_typedevice_detail[1];
        m0_typedevice_detail _data_typedevice_detail = new m0_typedevice_detail();

        _data_typedevice_detail.m0_tdidx = 1;

        _data_deviceregister.m0_typedevice_list[0] = _data_typedevice_detail;
        _data_deviceregister = callServiceDevices(_url_m0_typedevice, _data_deviceregister);

        ddl_typedevice.DataSource = _data_deviceregister.m0_typedevice_list;
        ddl_typedevice.DataTextField = "name_m0_typedevice";
        ddl_typedevice.DataValueField = "m0_tdidx";
        ddl_typedevice.DataBind();
    }

    protected void SelectLogDevice(int u0_didx)
    {
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        _data_device_detail.u0_didx = u0_didx;
        _data_device_detail.doc_status = "0";

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_25, _data_deviceregister);
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        rptLog.DataSource = _data_deviceregister.u0_device_list;
        rptLog.DataBind();
    }

    protected void SelectLogDevice_cutoff(int u0_didx, string code_cutoff)
    {
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail = new u0_device_detail();

        _data_device_detail.u0_didx = u0_didx;
        _data_device_detail.doc_status = "6,7";
        _data_device_detail.u0_code_cutoff = code_cutoff;

        _data_deviceregister.u0_device_list[0] = _data_device_detail;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_25, _data_deviceregister);
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        rplog_cutoff.DataSource = _data_deviceregister.u0_device_list;
        rplog_cutoff.DataBind();
    }

    protected void Select_Gv_software(int didx, string software_licen)
    {
        FormView FvViewDetailDevice = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
        GridView Gv_detail_software = (GridView)FvViewDetailDevice.FindControl("Gv_detail_software");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.u0_didx = didx;
        _data_device.software_type = "1,2"; // Lincence
        _data_device.u0_software_licen = software_licen; // Lincence


        _data_deviceregister.u0_device_list[0] = _data_device;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_26, _data_deviceregister);

        if (_data_deviceregister.return_code == 0)
        {
            setGridData(Gv_detail_software, _data_deviceregister.u0_device_list);
        }
        else
        {
            setGridData(Gv_detail_software, null);
        }
    }

    protected void Select_ck_software()
    {
        FormView FvViewDetailDevice = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
        GridView GvSoftware_free = (GridView)FvViewDetailDevice.FindControl("GvSoftware_free");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.software_type = "3";  //Free

        _data_deviceregister.u0_device_list[0] = _data_device;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_26, _data_deviceregister);
        if (_data_deviceregister.return_code == 0)
        {
            setGridData(GvSoftware_free, _data_deviceregister.u0_device_list);
        }
        else
        {
            setGridData(GvSoftware_free, null);
        }

    }

    protected void Select_Fv_Detail_NewDevice(int u0_didx, int u0_relation)
    {
        FormView FvView_Detail_NewDevice = (FormView)FvViewApproveDevice.FindControl("FvView_Detail_NewDevice");
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.u0_didx = u0_relation;

        _data_deviceregister.u0_device_list[0] = _data_device;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvView_Detail_NewDevice, _data_deviceregister.u0_device_list);


        FormView FvView_Detail_NewDevice_new = (FormView)FvViewApproveDevice.FindControl("FvView_Detail_NewDevice_new");
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_1 = new u0_device_detail();

        _data_device_1.u0_didx = u0_didx;

        _data_deviceregister.u0_device_list[0] = _data_device_1;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvView_Detail_NewDevice_new, _data_deviceregister.u0_device_list);
    }

    protected void Select_Fv_Detail_Transfer(int u0_didx, int u0_relation)
    {
        FormView FvView_Detail_Transfer = (FormView)FvViewApproveDevice_transfer.FindControl("FvView_Detail_Transfer");
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device = new u0_device_detail();

        _data_device.u0_didx = u0_relation;

        _data_deviceregister.u0_device_list[0] = _data_device;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvView_Detail_Transfer, _data_deviceregister.u0_device_list);


        FormView FvView_Detail_Transfer_new = (FormView)FvViewApproveDevice_transfer.FindControl("FvView_Detail_Transfer_new");
        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_1 = new u0_device_detail();

        _data_device_1.u0_didx = u0_didx;

        _data_deviceregister.u0_device_list[0] = _data_device_1;
        _data_deviceregister = callServicePostDevice(_url_u0_device_view, _data_deviceregister);
        setFormData(FvView_Detail_Transfer_new, _data_deviceregister.u0_device_list);
    }

    protected void Select_Approve_Transfer_All()
    {
        GridView GvDetail_Approve_Transfer = (GridView)FvViewApproveDevice_transfer.FindControl("GvDetail_Approve_Transfer");

        _data_deviceregister.u0_device_list = new u0_device_detail[1];
        u0_device_detail _data_device_detail_1 = new u0_device_detail();

        _data_device_detail_1.u0_relation_didx = 0;
        if (ViewState["Rdept_idx"].ToString() == "20")
        {
            _data_device_detail_1.u0_referent = ViewState["u0_didx_Device_Main"].ToString();
        }
        else
        {
            _data_device_detail_1.u0_referent = ViewState["u0_didx_all_transfer"].ToString();
        }

        _data_device_detail_1.doc_status = "2"; //เฉพาะที่ status ที่เปลี่ยนแปลงเท่านั้น


        _data_deviceregister.u0_device_list[0] = _data_device_detail_1;
        //Label71.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister));
        _data_deviceregister = callServicePostDevice(_url_u0_device_view_23, _data_deviceregister);

        if (_data_deviceregister.return_code == 1)
        {
            boxviewapprove_transfer.Visible = false;
            GvDetail_Approve_Transfer.DataSource = null;
            GvDetail_Approve_Transfer.DataBind();
        }
        else
        {
            boxviewapprove_transfer.Visible = true;
            GvDetail_Approve_Transfer.DataSource = _data_deviceregister.u0_device_list;
            GvDetail_Approve_Transfer.DataBind();
        }

    }

    protected void Select_Set_Visible_Btn(string type)
    {
        Panel box_view_adddevice = (Panel)FvViewDetailDevice.FindControl("box_view_adddevice");
        LinkButton btnViewadddata_show = (LinkButton)FvViewDetailDevice.FindControl("btnViewadddata_show");
        LinkButton btnViewadddata_hide = (LinkButton)FvViewDetailDevice.FindControl("btnViewadddata_hide");
        LinkButton btnView_etc_show = (LinkButton)FvViewDetailDevice.FindControl("btnView_etc_show");

        Panel box_edit = (Panel)FvViewDetailDevice.FindControl("box_edit");
        LinkButton btnvieweditdevice_show = (LinkButton)FvViewDetailDevice.FindControl("btnvieweditdevice_show");
        LinkButton btnvieweditdevice_hide = (LinkButton)FvViewDetailDevice.FindControl("btnvieweditdevice_hide");
        Panel boxviewapprove_ram = (Panel)FvViewDetailDevice.FindControl("boxviewapprove_ram");
        Panel boxviewapprove_cpu = (Panel)FvViewDetailDevice.FindControl("boxviewapprove_cpu");
        Panel boxviewapprove_hdd = (Panel)FvViewDetailDevice.FindControl("boxviewapprove_hdd");
        Panel boxviewapprove_vga = (Panel)FvViewDetailDevice.FindControl("boxviewapprove_vga");
        Panel boxviewapprove_etc = (Panel)FvViewDetailDevice.FindControl("boxviewapprove_etc");
        Panel boxviewApprove_all = (Panel)FvViewDetailDevice.FindControl("boxviewApprove_all");

        Panel box_view_addsoftware = (Panel)FvViewDetailDevice.FindControl("box_view_addsoftware");
        LinkButton btnView_software_show = (LinkButton)FvViewDetailDevice.FindControl("btnView_software_show");
        LinkButton btnView_software_hide = (LinkButton)FvViewDetailDevice.FindControl("btnView_software_hide");

        Panel box_cutoff_device = (Panel)FvViewDetailDevice.FindControl("box_cutoff_device");
        LinkButton btnView_cut_show = (LinkButton)FvViewDetailDevice.FindControl("btnView_cut_show");
        LinkButton btnView_cut_hide = (LinkButton)FvViewDetailDevice.FindControl("btnView_cut_hide");
        Label lbl_view_u0_didx_cut = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");

        LinkButton btnSaveCutoff = (LinkButton)FvViewDetailDevice.FindControl("btnSaveCutoff");
        LinkButton btnViewdata = (LinkButton)FvViewDetailDevice.FindControl("btnViewdata");

        switch (type)
        {
            case "2_1":
                box_view_adddevice.Visible = true;
                btnViewadddata_show.Visible = false;
                btnViewadddata_hide.Visible = true;

                btnSaveCutoff.Visible = false;
                btnvieweditdevice_show.Visible = false;
                btnView_software_show.Visible = false;
                btnView_cut_show.Visible = false;
                break;
            case "2_2":
                box_view_adddevice.Visible = false;
                btnViewadddata_show.Visible = true;
                btnViewadddata_hide.Visible = false;

                btnSaveCutoff.Visible = false;
                btnvieweditdevice_show.Visible = true;
                btnView_software_show.Visible = true;
                btnView_cut_show.Visible = true;
                break;
            case "3_1":
                box_edit.Visible = true;
                btnvieweditdevice_show.Visible = false;
                btnvieweditdevice_hide.Visible = true;
                boxviewapprove_ram.Visible = false;
                boxviewapprove_cpu.Visible = false;
                boxviewapprove_hdd.Visible = false;
                boxviewapprove_vga.Visible = false;
                boxviewapprove_etc.Visible = false;
                boxviewApprove_all.Visible = false;

                btnViewdata.Visible = false;
                btnSaveCutoff.Visible = false;
                btnViewadddata_show.Visible = false;
                btnView_software_show.Visible = false;
                btnView_cut_show.Visible = false;
                break;
            case "3_2":
                box_edit.Visible = false;
                btnvieweditdevice_show.Visible = true;
                btnvieweditdevice_hide.Visible = false;

                btnViewdata.Visible = true;
                btnSaveCutoff.Visible = false;
                btnViewadddata_show.Visible = true;
                btnView_software_show.Visible = true;
                btnView_cut_show.Visible = true;
                break;
            case "4_1":
                box_view_addsoftware.Visible = true;
                btnView_software_show.Visible = false;
                btnView_software_hide.Visible = true;

                btnViewdata.Visible = true;
                btnSaveCutoff.Visible = false;
                btnViewadddata_show.Visible = false;
                btnvieweditdevice_show.Visible = false;
                btnView_cut_show.Visible = false;
                break;
            case "4_2":
                box_view_addsoftware.Visible = false;
                btnView_software_show.Visible = true;
                btnView_software_hide.Visible = false;

                btnViewdata.Visible = true;
                btnSaveCutoff.Visible = false;
                btnViewadddata_show.Visible = true;
                btnvieweditdevice_show.Visible = true;
                btnView_cut_show.Visible = true;
                break;
            case "5_1":
                btnView_cut_show.Visible = false;
                btnView_cut_hide.Visible = true;
                box_cutoff_device.Visible = true;

                btnViewdata.Visible = false;
                btnSaveCutoff.Visible = true;
                btnViewadddata_show.Visible = false;
                btnvieweditdevice_show.Visible = false;
                btnView_software_show.Visible = false;
                btnView_cut_show.Visible = false;
                Select_Gv_Device_cutoff(int.Parse(lbl_view_u0_didx_cut.Text));
                ViewState["lbl_view_u0_didx_cut"] = lbl_view_u0_didx_cut.Text;
                break;
            case "5_2":
                btnView_cut_show.Visible = true;
                btnView_cut_hide.Visible = false;
                box_cutoff_device.Visible = false;

                btnViewdata.Visible = true;
                btnSaveCutoff.Visible = false;
                btnViewadddata_show.Visible = true;
                btnvieweditdevice_show.Visible = true;
                btnView_software_show.Visible = true;
                btnView_cut_show.Visible = true;
                break;
        }
    }

    protected void Insert_View_Detail_Device()
    {
        GridView Gv_detail_software = (GridView)FvViewDetailDevice.FindControl("Gv_detail_software");
        GridView GvSoftware_free = (GridView)FvViewDetailDevice.FindControl("GvSoftware_free");
        GridView GvTemp_sotfware_2 = (GridView)FvViewDetailDevice.FindControl("GvTemp_sotfware");
        GridView Gv_detail_software_2 = (GridView)FvViewDetailDevice.FindControl("Gv_detail_software");
        GridView Gv_cutoff_detail = (GridView)FvViewDetailDevice.FindControl("Gv_cutoff_detail");
        RadioButtonList Rdtype_cutoff = (RadioButtonList)FvViewDetailDevice.FindControl("Rdtype_cutoff");
        Label lbl_view_u0_didx_edit = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");

        var Search_1 = (DataSet)ViewState["vsTempDevicw"];
        var Search_3 = (DataSet)ViewState["vsTemp_sw"];
        string temp = "";
        string software = "";
        string software_free = "";
        string software_licen_base = "";
        string software_licen_total = "";
        string software_temp = "";
        string temp_cutoff = "";

        foreach (DataRow dr in Search_1.Tables[0].Rows)
        {
            temp += dr["u0_didx"].ToString() + ",";
        }

        if (int.Parse(ViewState["Check_Action"].ToString()) == 2)
        {
            int temp_gridview_count = int.Parse(Gv_cutoff_detail.Rows.Count.ToString());

            if (temp_gridview_count > 0)
            {
                foreach (GridViewRow row in Gv_cutoff_detail.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRow1 = (CheckBox)row.Cells[0].FindControl("CheckBox_u0_didx");

                        if (Rdtype_cutoff.SelectedValue == "3") // ตัดชำรุด ทั้งเครื่อง 3
                        {
                            Label lbl_u0_didx_cutoff = (Label)row.Cells[0].FindControl("lbl_u0_didx_cutoff");
                            temp_cutoff += lbl_u0_didx_cutoff.Text + ",";
                        }
                        else // ตัดชำรุด เฉพาะบางอุปกรณ์ 6
                        {
                            if (chkRow1.Checked == true)
                            {
                                Label lbl_u0_didx_cutoff = (Label)row.Cells[0].FindControl("lbl_u0_didx_cutoff");
                                temp_cutoff += lbl_u0_didx_cutoff.Text + ",";
                            }
                        }
                    }
                }
            }
            else
            {
                temp_cutoff = ",";
            }
        }
        else
        {
            temp_cutoff = "";
        }

        foreach (GridViewRow row in Gv_detail_software.Rows) // Software Licen On Base
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow1 = (CheckBox)row.Cells[0].FindControl("CheckBox_licence");
                software_temp = (row.Cells[0].FindControl("lbl_software_idx") as Label).Text;

                if (chkRow1.Checked == true)
                {
                    Label lbl_software_idx = (Label)row.Cells[0].FindControl("lbl_software_idx");
                    software_licen_base += lbl_software_idx.Text + ",";
                }
            }
        }

        foreach (GridViewRow row in GvTemp_sotfware_2.Rows) // Software Licen On Temp
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                //CheckBox chkRow1 = (CheckBox)row.Cells[0].FindControl("CheckBox_free");
                software = (row.Cells[0].FindControl("lbl_software_idx") as Label).Text;
                software_licen_total += software + ",";
            }
        }

        software_licen_total = software_licen_base + software_licen_total;

        foreach (GridViewRow row in GvSoftware_free.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow1 = (CheckBox)row.Cells[0].FindControl("CheckBox_free");
                software = (row.Cells[0].FindControl("lbl_u0_software_idx") as Label).Text;

                if (chkRow1.Checked == true)
                {
                    software_free += software + ",";
                }
            }
        }

        foreach (GridViewRow row in GvSoftware_free.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow1 = (CheckBox)row.Cells[0].FindControl("CheckBox_free");
                software = (row.Cells[0].FindControl("lbl_u0_software_idx") as Label).Text;

                if (chkRow1.Checked == true)
                {
                    software_free += software + ",";
                }
            }
        }

        ViewState["u0_software_licen"] = software_licen_total;
        ViewState["u0_software_free"] = software_free;
        ViewState["u0_relation_List"] = temp;
        ViewState["u0_didx_cutoff_List"] = temp_cutoff;

        Update_Detail_Device();
        Menu_Color(1);
        MvMaster.SetActiveView(ViewIndex);
        Select_Detail_Device();
    }

    #endregion


    #region callService

    protected data_device callServiceDevices(string _cmdUrl, data_device _data_device)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_device);
        //fss.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fss.Text = _cmdUrl + _localJson;

        // convert json to object
        _data_device = (data_device)_funcTool.convertJsonToObject(typeof(data_device), _localJson);

        return _data_device;
    }

    protected data_device callServicePostDevice(string _cmdUrl, data_device _data_device)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_data_device);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fss.Text = _cmdUrl + _localJson;

        ////// convert json to object
        _data_device = (data_device)_funcTool.convertJsonToObject(typeof(data_device), _localJson);

        return _data_device;
    }

    protected data_device callServicePostDevice_Post(string _cmdUrl, data_device _data_device)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_data_device);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsd.Text = _cmdUrl + _localJson;
        //_data_device = _funcTool.convertJsonToXml(_localJson);
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertJsonToXml(_localJson)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML

        ////// convert json to object
        _data_device = (data_device)_funcTool.convertJsonToObject(typeof(data_device), _localJson);

        return _data_device;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        DropDownList ddl_add_org = (DropDownList)FvInsertDevice.FindControl("ddl_add_org");
        DropDownList ddl_add_dept = (DropDownList)FvInsertDevice.FindControl("ddl_add_dept");
        DropDownList ddl_add_sec = (DropDownList)FvInsertDevice.FindControl("ddl_add_sec");
        DropDownList ddl_view_org = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_org");
        DropDownList ddl_view_dept = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_dept");
        DropDownList ddl_view_sec = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_sec");

        switch (ddName.ID)
        {

            case "ddl_add_typedevice":

                DropDownList ddl_typedevice = (DropDownList)FvInsertDevice.FindControl("ddl_add_typedevice");
                Panel boxmonitor = (Panel)FvInsertDevice.FindControl("boxmonitor");
                Panel boxmoniter_detail = (Panel)FvInsertDevice.FindControl("boxmoniter_detail");
                Panel boxcomputer = (Panel)FvInsertDevice.FindControl("boxcomputer");
                Panel boxprinter = (Panel)FvInsertDevice.FindControl("boxprinter");
                Panel boxcpu = (Panel)FvInsertDevice.FindControl("boxcpu");
                Panel boxram = (Panel)FvInsertDevice.FindControl("boxram");
                Panel boxhdd = (Panel)FvInsertDevice.FindControl("boxhdd");
                Panel boxvga = (Panel)FvInsertDevice.FindControl("boxvga");
                Panel boxslot = (Panel)FvInsertDevice.FindControl("boxslot");
                Panel boxtype = (Panel)FvInsertDevice.FindControl("boxtype");


                switch (ddl_typedevice.SelectedValue)
                {
                    case "0": //defult
                    case "5": //Handheld 
                    case "11": //band 
                    case "10": //Other
                        boxmonitor.Visible = false;
                        boxprinter.Visible = false;
                        boxcomputer.Visible = false;
                        boxcpu.Visible = false;
                        boxram.Visible = false;
                        boxhdd.Visible = false;
                        boxvga.Visible = false;
                        boxslot.Visible = false;
                        boxtype.Visible = false;
                        break;
                    case "1": //NoteBook
                    case "2": //PC 
                        boxmonitor.Visible = false;
                        boxprinter.Visible = false;
                        boxcomputer.Visible = true;
                        boxcpu.Visible = false;
                        boxram.Visible = false;
                        boxhdd.Visible = false;
                        boxvga.Visible = false;
                        boxslot.Visible = false;
                        boxtype.Visible = false;
                        break;
                    case "3": //Printer
                        boxmonitor.Visible = false;
                        boxprinter.Visible = true;
                        boxcomputer.Visible = false;
                        boxcpu.Visible = false;
                        boxram.Visible = false;
                        boxhdd.Visible = false;
                        boxvga.Visible = false;
                        boxslot.Visible = false;
                        boxtype.Visible = false;
                        break;
                    case "4": //Moniter
                        boxmonitor.Visible = true;
                        boxprinter.Visible = false;
                        boxcomputer.Visible = false;
                        boxcpu.Visible = false;
                        boxram.Visible = false;
                        boxhdd.Visible = false;
                        boxvga.Visible = false;
                        boxslot.Visible = false;
                        boxtype.Visible = false;
                        break;
                    case "6": //cpu
                        boxmonitor.Visible = false;
                        boxprinter.Visible = false;
                        boxcomputer.Visible = false;
                        boxcpu.Visible = true;
                        boxram.Visible = false;
                        boxhdd.Visible = false;
                        boxvga.Visible = false;
                        boxslot.Visible = true;
                        boxtype.Visible = true;
                        break;
                    case "7": //RAM
                        boxmonitor.Visible = false;
                        boxprinter.Visible = false;
                        boxcomputer.Visible = false;
                        boxcpu.Visible = false;
                        boxram.Visible = true;
                        boxhdd.Visible = false;
                        boxvga.Visible = false;
                        boxslot.Visible = true;
                        boxtype.Visible = true;
                        break;
                    case "8": //HDD
                        boxmonitor.Visible = false;
                        boxprinter.Visible = false;
                        boxcomputer.Visible = false;
                        boxcpu.Visible = false;
                        boxram.Visible = false;
                        boxhdd.Visible = true;
                        boxvga.Visible = false;
                        boxslot.Visible = true;
                        boxtype.Visible = true;
                        break;
                    case "9": //VGA
                        boxmonitor.Visible = false;
                        boxprinter.Visible = false;
                        boxcomputer.Visible = false;
                        boxcpu.Visible = false;
                        boxram.Visible = false;
                        boxhdd.Visible = false;
                        boxvga.Visible = true;
                        boxslot.Visible = true;
                        boxtype.Visible = true;
                        break;
                }
                break;
            case "ddl_addmaster_typedevice":
                DropDownList ddl_addmaster_typedevice = (DropDownList)FvInsertMasterDevice.FindControl("ddl_addmaster_typedevice");
                Panel boxmaster_cpu = (Panel)FvInsertMasterDevice.FindControl("boxmaster_cpu");
                Panel boxmaster_ram = (Panel)FvInsertMasterDevice.FindControl("boxmaster_ram");
                Panel boxmaster_hdd = (Panel)FvInsertMasterDevice.FindControl("boxmaster_hdd");
                Panel boxmaster_vga = (Panel)FvInsertMasterDevice.FindControl("boxmaster_vga");
                Panel boxmaster_printer = (Panel)FvInsertMasterDevice.FindControl("boxmaster_printer");
                Panel boxmaster_moniter = (Panel)FvInsertMasterDevice.FindControl("boxmaster_moniter");
                Panel boxmaster_insurance = (Panel)FvInsertMasterDevice.FindControl("boxmaster_insurance");
                Panel boxmaster_band = (Panel)FvInsertMasterDevice.FindControl("boxmaster_band");


                switch (ddl_addmaster_typedevice.SelectedValue)
                {
                    case "0": //defult                                 
                    case "10": //ประเภทอุปกรณ์              
                    case "12": //ระดับ
                    case "13": //สถานะอุปกรณ์
                        boxmaster_cpu.Visible = false;
                        boxmaster_ram.Visible = false;
                        boxmaster_hdd.Visible = false;
                        boxmaster_vga.Visible = false;
                        boxmaster_printer.Visible = false;
                        boxmaster_moniter.Visible = false;
                        boxmaster_insurance.Visible = false;
                        boxmaster_band.Visible = false;

                        box_master_cpu.Visible = false;
                        box_master_ram.Visible = false;
                        box_master_hdd.Visible = false;
                        box_master_vga.Visible = false;
                        box_master_printer.Visible = false;
                        box_master_moniter.Visible = false;
                        box_master_typedevice.Visible = false;
                        box_master_band.Visible = false;
                        box_master_level.Visible = false;
                        box_master_status.Visible = false;
                        box_master_insurance.Visible = false;
                        break;
                    case "11": //ยี่ห้อ
                        boxmaster_cpu.Visible = false;
                        boxmaster_ram.Visible = false;
                        boxmaster_hdd.Visible = false;
                        boxmaster_vga.Visible = false;
                        boxmaster_printer.Visible = false;
                        boxmaster_moniter.Visible = false;
                        boxmaster_insurance.Visible = false;
                        boxmaster_band.Visible = true;

                        box_master_cpu.Visible = false;
                        box_master_ram.Visible = false;
                        box_master_hdd.Visible = false;
                        box_master_vga.Visible = false;
                        box_master_printer.Visible = false;
                        box_master_moniter.Visible = false;
                        box_master_typedevice.Visible = false;
                        box_master_band.Visible = true;
                        box_master_level.Visible = false;
                        box_master_status.Visible = false;
                        box_master_insurance.Visible = false;
                        break;
                    case "14": //บริษัทประกัน
                        boxmaster_cpu.Visible = false;
                        boxmaster_ram.Visible = false;
                        boxmaster_hdd.Visible = false;
                        boxmaster_vga.Visible = false;
                        boxmaster_printer.Visible = false;
                        boxmaster_moniter.Visible = false;
                        boxmaster_insurance.Visible = true;
                        boxmaster_band.Visible = false;

                        box_master_cpu.Visible = false;
                        box_master_ram.Visible = false;
                        box_master_hdd.Visible = false;
                        box_master_vga.Visible = false;
                        box_master_printer.Visible = false;
                        box_master_moniter.Visible = false;
                        box_master_typedevice.Visible = false;
                        box_master_band.Visible = false;
                        box_master_level.Visible = false;
                        box_master_status.Visible = false;
                        box_master_insurance.Visible = true;
                        break;
                    case "3": //PRINTER
                        boxmaster_cpu.Visible = false;
                        boxmaster_ram.Visible = false;
                        boxmaster_hdd.Visible = false;
                        boxmaster_vga.Visible = false;
                        boxmaster_printer.Visible = true;
                        boxmaster_moniter.Visible = false;
                        boxmaster_insurance.Visible = false;
                        boxmaster_band.Visible = false;

                        box_master_cpu.Visible = false;
                        box_master_ram.Visible = false;
                        box_master_hdd.Visible = false;
                        box_master_vga.Visible = false;
                        box_master_printer.Visible = true;
                        box_master_moniter.Visible = false;
                        box_master_typedevice.Visible = false;
                        box_master_band.Visible = false;
                        box_master_level.Visible = false;
                        box_master_status.Visible = false;
                        box_master_insurance.Visible = false;
                        break;
                    case "4": //MONITER
                        boxmaster_cpu.Visible = false;
                        boxmaster_ram.Visible = false;
                        boxmaster_hdd.Visible = false;
                        boxmaster_vga.Visible = false;
                        boxmaster_printer.Visible = false;
                        boxmaster_moniter.Visible = true;
                        boxmaster_insurance.Visible = false;
                        boxmaster_band.Visible = false;

                        box_master_cpu.Visible = false;
                        box_master_ram.Visible = false;
                        box_master_hdd.Visible = false;
                        box_master_vga.Visible = false;
                        box_master_printer.Visible = false;
                        box_master_moniter.Visible = true;
                        box_master_typedevice.Visible = false;
                        box_master_band.Visible = false;
                        box_master_level.Visible = false;
                        box_master_status.Visible = false;
                        box_master_insurance.Visible = false;
                        break;
                    case "6": //cpu
                        boxmaster_cpu.Visible = true;
                        boxmaster_ram.Visible = false;
                        boxmaster_hdd.Visible = false;
                        boxmaster_vga.Visible = false;
                        boxmaster_printer.Visible = false;
                        boxmaster_moniter.Visible = false;
                        boxmaster_insurance.Visible = false;
                        boxmaster_band.Visible = false;

                        box_master_cpu.Visible = true;
                        box_master_ram.Visible = false;
                        box_master_hdd.Visible = false;
                        box_master_vga.Visible = false;
                        box_master_printer.Visible = false;
                        box_master_moniter.Visible = false;
                        box_master_typedevice.Visible = false;
                        box_master_band.Visible = false;
                        box_master_level.Visible = false;
                        box_master_status.Visible = false;
                        box_master_insurance.Visible = false;
                        break;
                    case "7": //ram 
                        boxmaster_cpu.Visible = false;
                        boxmaster_ram.Visible = true;
                        boxmaster_hdd.Visible = false;
                        boxmaster_vga.Visible = false;
                        boxmaster_printer.Visible = false;
                        boxmaster_moniter.Visible = false;
                        boxmaster_insurance.Visible = false;
                        boxmaster_band.Visible = false;

                        box_master_cpu.Visible = false;
                        box_master_ram.Visible = true;
                        box_master_hdd.Visible = false;
                        box_master_vga.Visible = false;
                        box_master_printer.Visible = false;
                        box_master_moniter.Visible = false;
                        box_master_typedevice.Visible = false;
                        box_master_band.Visible = false;
                        box_master_level.Visible = false;
                        box_master_status.Visible = false;
                        box_master_insurance.Visible = false;
                        break;
                    case "8": //hdd
                        boxmaster_cpu.Visible = false;
                        boxmaster_ram.Visible = false;
                        boxmaster_hdd.Visible = true;
                        boxmaster_vga.Visible = false;
                        boxmaster_printer.Visible = false;
                        boxmaster_moniter.Visible = false;
                        boxmaster_insurance.Visible = false;
                        boxmaster_band.Visible = false;

                        box_master_cpu.Visible = false;
                        box_master_ram.Visible = false;
                        box_master_hdd.Visible = true;
                        box_master_vga.Visible = false;
                        box_master_printer.Visible = false;
                        box_master_moniter.Visible = false;
                        box_master_typedevice.Visible = false;
                        box_master_band.Visible = false;
                        box_master_level.Visible = false;
                        box_master_status.Visible = false;
                        box_master_insurance.Visible = false;
                        break;
                    case "9": //vga
                        boxmaster_cpu.Visible = false;
                        boxmaster_ram.Visible = false;
                        boxmaster_hdd.Visible = false;
                        boxmaster_vga.Visible = true;
                        boxmaster_printer.Visible = false;
                        boxmaster_moniter.Visible = false;
                        boxmaster_insurance.Visible = false;
                        boxmaster_band.Visible = false;

                        box_master_cpu.Visible = false;
                        box_master_ram.Visible = false;
                        box_master_hdd.Visible = false;
                        box_master_vga.Visible = true;
                        box_master_printer.Visible = false;
                        box_master_moniter.Visible = false;
                        box_master_typedevice.Visible = false;
                        box_master_band.Visible = false;
                        box_master_level.Visible = false;
                        box_master_status.Visible = false;
                        box_master_insurance.Visible = false;
                        break;
                }
                break;

            case "ddl_add_cpu":

                TextBox txt_detail_bandcpu = (TextBox)FvInsertDevice.FindControl("txt_detail_bandcpu");
                TextBox txt_detail_seriecpu = (TextBox)FvInsertDevice.FindControl("txt_detail_seriecpu");
                DropDownList ddl_add_cpu = (DropDownList)FvInsertDevice.FindControl("ddl_add_cpu");
                Panel boxpc_note_cpu = (Panel)FvInsertDevice.FindControl("boxpc_note_cpu");

                if (ddl_add_cpu.SelectedValue == "0")
                {
                    Select_m0_cpu();
                    boxpc_note_cpu.Visible = false;
                }
                else
                {
                    _data_deviceregister.m0_cpu_list = new m0_cpu_detail[1];
                    m0_cpu_detail _data_device_detail = new m0_cpu_detail();

                    _data_device_detail.m0_cidx = int.Parse(ddl_add_cpu.SelectedValue);

                    _data_deviceregister.m0_cpu_list[0] = _data_device_detail;
                    _data_deviceregister = callServiceDevices(_url_m0_cpu_detail, _data_deviceregister);

                    txt_detail_bandcpu.Text = _data_deviceregister.m0_cpu_list[0].band_cpu;
                    txt_detail_seriecpu.Text = _data_deviceregister.m0_cpu_list[0].generation_cpu;
                    boxpc_note_cpu.Visible = true;
                }
                break;
            case "ddl_add_ram":

                TextBox txt_detail_bandram = (TextBox)FvInsertDevice.FindControl("txt_detail_bandram");
                TextBox txt_detail_typeram = (TextBox)FvInsertDevice.FindControl("txt_detail_typeram");
                TextBox txt_detail_sizeram = (TextBox)FvInsertDevice.FindControl("txt_detail_sizeram");
                DropDownList ddl_add_ram = (DropDownList)FvInsertDevice.FindControl("ddl_add_ram");
                Panel boxpc_note_ram = (Panel)FvInsertDevice.FindControl("boxpc_note_ram");

                if (ddl_add_ram.SelectedValue == "0")
                {
                    Select_m0_ram();
                    boxpc_note_ram.Visible = false;
                }
                else
                {
                    _data_deviceregister.m0_ram_list = new m0_ram_detail[1];
                    m0_ram_detail _data_device_detail = new m0_ram_detail();

                    _data_device_detail.m0_ridx = int.Parse(ddl_add_ram.SelectedValue);

                    _data_deviceregister.m0_ram_list[0] = _data_device_detail;
                    _data_deviceregister = callServiceDevices(_url_m0_ram_detail, _data_deviceregister);

                    txt_detail_bandram.Text = _data_deviceregister.m0_ram_list[0].band_ram;
                    txt_detail_typeram.Text = _data_deviceregister.m0_ram_list[0].type_ram;
                    txt_detail_sizeram.Text = _data_deviceregister.m0_ram_list[0].capacity_ram;
                    boxpc_note_ram.Visible = true;
                }
                break;
            case "ddl_add_hdd":

                TextBox txt_detail_bandhdd = (TextBox)FvInsertDevice.FindControl("txt_detail_bandhdd");
                TextBox txt_detail_typehdd = (TextBox)FvInsertDevice.FindControl("txt_detail_typehdd");
                TextBox txt_detail_sizehdd = (TextBox)FvInsertDevice.FindControl("txt_detail_sizehdd");
                DropDownList ddl_add_hdd = (DropDownList)FvInsertDevice.FindControl("ddl_add_hdd");
                Panel boxpc_note_hdd = (Panel)FvInsertDevice.FindControl("boxpc_note_hdd");

                if (ddl_add_hdd.SelectedValue == "0")
                {
                    Select_m0_hdd();
                    boxpc_note_hdd.Visible = false;
                }
                else
                {
                    _data_deviceregister.m0_hdd_list = new m0_hdd_detail[1];
                    m0_hdd_detail _data_device_detail = new m0_hdd_detail();

                    _data_device_detail.m0_hidx = int.Parse(ddl_add_hdd.SelectedValue);

                    _data_deviceregister.m0_hdd_list[0] = _data_device_detail;
                    _data_deviceregister = callServiceDevices(_url_m0_hdd_detail, _data_deviceregister);

                    txt_detail_bandhdd.Text = _data_deviceregister.m0_hdd_list[0].band_hdd;
                    txt_detail_typehdd.Text = _data_deviceregister.m0_hdd_list[0].type_hdd;
                    txt_detail_sizehdd.Text = _data_deviceregister.m0_hdd_list[0].capacity_hdd;
                    boxpc_note_hdd.Visible = true;
                }
                break;
            case "ddl_add_vga":

                TextBox txt_detail_serievga = (TextBox)FvInsertDevice.FindControl("txt_detail_serievga");
                TextBox txt_detail_typevga = (TextBox)FvInsertDevice.FindControl("txt_detail_typevga");
                DropDownList ddl_add_vga = (DropDownList)FvInsertDevice.FindControl("ddl_add_vga");
                Panel boxpc_note_vga = (Panel)FvInsertDevice.FindControl("boxpc_note_vga");

                if (ddl_add_vga.SelectedValue == "0")
                {
                    Select_m0_vga();
                    boxpc_note_vga.Visible = false;
                }
                else
                {
                    _data_deviceregister.m0_vga_list = new m0_vga_detail[1];
                    m0_vga_detail _data_device_detail = new m0_vga_detail();

                    _data_device_detail.m0_vidx = int.Parse(ddl_add_vga.SelectedValue);

                    _data_deviceregister.m0_vga_list[0] = _data_device_detail;
                    _data_deviceregister = callServiceDevices(_url_m0_vga_detail, _data_deviceregister);

                    txt_detail_serievga.Text = _data_deviceregister.m0_vga_list[0].type_vga;
                    txt_detail_typevga.Text = _data_deviceregister.m0_vga_list[0].generation_vga;
                    boxpc_note_vga.Visible = true;
                }
                break;
            case "ddl_add_monitor":

                Panel boxmoniter = (Panel)FvInsertDevice.FindControl("boxmoniter_detail");
                TextBox txt_detail_bandmoniter = (TextBox)FvInsertDevice.FindControl("txt_detail_bandmoniter");
                TextBox txt_detail_sizemoniter = (TextBox)FvInsertDevice.FindControl("txt_detail_sizemoniter");
                DropDownList ddl_add_monitor = (DropDownList)FvInsertDevice.FindControl("ddl_add_monitor");

                if (ddl_add_monitor.SelectedValue == "0")
                {
                    Select_m0_vga();
                    boxmoniter.Visible = false;
                }
                else
                {
                    _data_deviceregister.m0_moniter_list = new m0_moniter_detail[1];
                    m0_moniter_detail _data_device_detail = new m0_moniter_detail();

                    _data_device_detail.m0_midx = int.Parse(ddl_add_monitor.SelectedValue);

                    _data_deviceregister.m0_moniter_list[0] = _data_device_detail;
                    _data_deviceregister = callServiceDevices(_url_m0_moniter_detail, _data_deviceregister);

                    txt_detail_bandmoniter.Text = _data_deviceregister.m0_moniter_list[0].band_moniter;
                    txt_detail_sizemoniter.Text = _data_deviceregister.m0_moniter_list[0].size_moniter;
                    boxmoniter.Visible = true;
                }
                break;
            case "ddl_add_printer":

                DropDownList ddl_add_printer = (DropDownList)FvInsertDevice.FindControl("ddl_add_printer");
                Panel boxprinter_detail = (Panel)FvInsertDevice.FindControl("boxprinter_detail");
                TextBox txt_detail_bandprinter = (TextBox)FvInsertDevice.FindControl("txt_detail_bandprinter");
                TextBox txt_detail_typeprinter = (TextBox)FvInsertDevice.FindControl("txt_detail_typeprinter");
                TextBox txt_detail_typeink = (TextBox)FvInsertDevice.FindControl("txt_detail_typeink");
                TextBox txt_detail_serizeprinter = (TextBox)FvInsertDevice.FindControl("txt_detail_serizeprinter");

                if (ddl_add_printer.SelectedValue == "0")
                {
                    Select_m0_printer();
                    boxprinter_detail.Visible = false;
                }
                else
                {
                    _data_deviceregister.m0_printer_list = new m0_printer_detail[1];
                    m0_printer_detail _data_device_detail = new m0_printer_detail();

                    _data_device_detail.m0_pidx = int.Parse(ddl_add_printer.SelectedValue);

                    _data_deviceregister.m0_printer_list[0] = _data_device_detail;
                    _data_deviceregister = callServiceDevices(_url_m0_printer_detail, _data_deviceregister);

                    txt_detail_bandprinter.Text = _data_deviceregister.m0_printer_list[0].band_print;
                    txt_detail_typeprinter.Text = _data_deviceregister.m0_printer_list[0].type_print;
                    txt_detail_typeink.Text = _data_deviceregister.m0_printer_list[0].type_ink;
                    txt_detail_serizeprinter.Text = _data_deviceregister.m0_printer_list[0].generation_print;
                    boxprinter_detail.Visible = true;
                }
                break;
            case "ddl_add_org":
                getDepartmentList(ddl_add_dept, int.Parse(ddl_add_org.SelectedItem.Value));
                ddl_add_sec.Items.Clear();
                break;
            case "ddl_add_dept":
                getSectionList(ddl_add_sec, int.Parse(ddl_add_org.SelectedItem.Value), int.Parse(ddl_add_dept.SelectedItem.Value));
                break;
            case "ddl_view_org":
                getDepartmentList(ddl_view_dept, int.Parse(ddl_view_org.SelectedItem.Value));
                ddl_view_sec.Items.Clear();
                break;
            case "ddl_view_dept":
                getSectionList(ddl_view_sec, int.Parse(ddl_view_org.SelectedItem.Value), int.Parse(ddl_view_dept.SelectedItem.Value));
                break;
        }
    }

    #endregion

    #region chkSelectedIndexChanged
    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (CheckBox)sender;

        switch (ddName.ID)
        {
            case "chkAll":

                FormView FvViewDetailDevice = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                CheckBoxList YrChkBox = (CheckBoxList)FvViewDetailDevice.FindControl("YrChkBox");
                bool b = (sender as CheckBox).Checked;

                for (int i = 0; i < YrChkBox.Items.Count; i++)
                {
                    YrChkBox.Items[i].Selected = b;
                }

                break;
        }
    }
    #endregion

    #region radioSelectedIndexChanged

    protected void Cutoff_SelectedIndexChanged(Object sender, EventArgs e)
    {
        var ddName = (RadioButtonList)sender;

        switch (ddName.ID)
        {
            case "Rdtype_cutoff":

                FormView FvViewDetailDevice = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                RadioButtonList Rdtype_cutoff = (RadioButtonList)FvViewDetailDevice.FindControl("Rdtype_cutoff");
                Panel box_cutoff_detail = (Panel)FvViewDetailDevice.FindControl("box_cutoff_detail");

                for (int i = 0; i < Rdtype_cutoff.Items.Count; i++)
                {
                    if (Rdtype_cutoff.Items[i].Selected == true)
                    {
                        if (int.Parse(Rdtype_cutoff.Items[i].Value.ToString()) == 3)
                        {
                            box_cutoff_detail.Visible = false;
                        }
                        else if (int.Parse(Rdtype_cutoff.Items[i].Value.ToString()) == 6)
                        {
                            box_cutoff_detail.Visible = true;
                        }
                    }
                }

                break;
        }
    }


    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetail_CPU":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvDetail_CPU = (GridView)FvViewDetailDevice.FindControl("GvDetail_CPU");
                    if (GvDetail_CPU.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbl_doc_status_cpu = ((Label)e.Row.FindControl("lbl_doc_status_cpu"));
                        var btn_selecr_tranfer_cpu = ((LinkButton)e.Row.FindControl("btn_selecr_tranfer_cpu"));
                        if (int.Parse(lbl_doc_status_cpu.Text) == 2)
                        {
                            btn_selecr_tranfer_cpu.Visible = false;
                        }
                        else
                        {
                            btn_selecr_tranfer_cpu.Visible = true;
                        }
                    }
                }
                break;
            case "GvDetail_RAM":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvDetail_RAM = (GridView)FvViewDetailDevice.FindControl("GvDetail_RAM");
                    if (GvDetail_RAM.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbl_doc_status_ram = ((Label)e.Row.FindControl("lbl_doc_status_ram"));
                        var btn_selecr_tranfer_ram = ((LinkButton)e.Row.FindControl("btn_selecr_tranfer_ram"));
                        if (int.Parse(lbl_doc_status_ram.Text) == 2)
                        {
                            btn_selecr_tranfer_ram.Visible = false;
                        }
                        else
                        {
                            btn_selecr_tranfer_ram.Visible = true;
                        }
                    }
                }
                break;
            case "GvDetail_HDD":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvDetail_HDD = (GridView)FvViewDetailDevice.FindControl("GvDetail_HDD");
                    if (GvDetail_HDD.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbl_doc_status_hdd = ((Label)e.Row.FindControl("lbl_doc_status_hdd"));
                        var btn_selecr_tranfer_hdd = ((LinkButton)e.Row.FindControl("btn_selecr_tranfer_hdd"));
                        if (int.Parse(lbl_doc_status_hdd.Text) == 2)
                        {
                            btn_selecr_tranfer_hdd.Visible = false;
                        }
                        else
                        {
                            btn_selecr_tranfer_hdd.Visible = true;
                        }
                    }
                }
                break;
            case "GvDetail_VGA":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvDetail_VGA = (GridView)FvViewDetailDevice.FindControl("GvDetail_VGA");
                    if (GvDetail_VGA.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbl_doc_status_vga = ((Label)e.Row.FindControl("lbl_doc_status_vga"));
                        var btn_selecr_tranfer_vga = ((LinkButton)e.Row.FindControl("btn_selecr_tranfer_vga"));
                        if (int.Parse(lbl_doc_status_vga.Text) == 2)
                        {
                            btn_selecr_tranfer_vga.Visible = false;
                        }
                        else
                        {
                            btn_selecr_tranfer_vga.Visible = true;
                        }
                    }
                }
                break;
            case "GvDetail_ETC":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvDetail_ETC = (GridView)FvViewDetailDevice.FindControl("GvDetail_ETC");
                    if (GvDetail_ETC.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbl_doc_status_etc = ((Label)e.Row.FindControl("lbl_doc_status_etc"));
                        var btn_selecr_tranfer_etc = ((LinkButton)e.Row.FindControl("btn_selecr_tranfer_etc"));
                        if (int.Parse(lbl_doc_status_etc.Text) == 2)
                        {
                            btn_selecr_tranfer_etc.Visible = false;
                        }
                        else
                        {
                            btn_selecr_tranfer_etc.Visible = true;
                        }
                    }
                }
                break;
            case "GvDevice":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvDevice.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lblstatusdoc = ((Label)e.Row.FindControl("lblstatusdoc"));
                        var lb_m0_tdidx = ((Label)e.Row.FindControl("lb_m0_tdidx"));
                        var btnView = ((LinkButton)e.Row.FindControl("btnView"));
                        var lbl_m0_sidx_status = ((Label)e.Row.FindControl("lbl_m0_sidx_status"));
                        var lbl_u0_didx_main = ((Label)e.Row.FindControl("lbl_u0_didx_main"));
                        Label lbl_code_main_1 = ((Label)e.Row.FindControl("lbl_code_main_1"));
                        Label lbl_code_main_2 = ((Label)e.Row.FindControl("lbl_code_main_2"));
                        Panel box_show_code_main = ((Panel)e.Row.FindControl("box_show_code_main"));


                        if (int.Parse(lblstatusdoc.Text) == 1 || int.Parse(lblstatusdoc.Text) == 6 || int.Parse(lblstatusdoc.Text) == 7) //status ปกติ -- ตัดชำรุด
                        {
                            if (lb_m0_tdidx.Text == "1" || lb_m0_tdidx.Text == "2") // Type PC,NB
                            {
                                btnView.Visible = true;
                            }
                            else if (int.Parse(lblstatusdoc.Text) == 6 || int.Parse(lblstatusdoc.Text) == 7)
                            {
                                btnView.Visible = false;
                            }
                            else
                            {
                                if (int.Parse(lbl_m0_sidx_status.Text) == 3 || int.Parse(lbl_m0_sidx_status.Text) == 6) //เฉพาะอุปกร์ย่อยที่มีสถานะ ตัดชำรุด
                                {
                                    btnView.Visible = false;
                                }
                                else
                                {
                                    btnView.Visible = true;
                                }
                            }
                        }
                        else if (int.Parse(lblstatusdoc.Text) == 2 || int.Parse(lblstatusdoc.Text) == 3 || int.Parse(lblstatusdoc.Text) == 6)
                        {
                            btnView.Visible = false;
                        }
                        else
                        {
                            btnView.Visible = true;
                        }

                        if (lb_m0_tdidx.Text != "1" || lb_m0_tdidx.Text != "2")
                        {
                            ViewState["u0_didx_Device_Main"] += lbl_u0_didx_main.Text + ",";
                            lbl_code_main_1.Visible = true;
                            lbl_code_main_2.Visible = true;
                        }
                        else
                        {
                            lbl_code_main_1.Visible = false;
                            lbl_code_main_2.Visible = false;
                        }

                    }
                }

                break;
            case "GvSoftware_free":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvSoftware_free = (GridView)FvViewDetailDevice.FindControl("GvSoftware_free");
                    Label lbl_u0_software_idx = ((Label)e.Row.FindControl("lbl_u0_software_idx"));
                    Label lbl_u0_software_free = ((Label)FvViewDetailDevice.FindControl("lbl_u0_software_free"));
                    CheckBox CheckBox_free = ((CheckBox)e.Row.FindControl("CheckBox_free"));

                    if (GvSoftware_free.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        string Check = lbl_u0_software_free.Text;//ViewState["software_free_check"].ToString();
                        string[] ToId = Check.Split(',');
                        foreach (string To_check in ToId)
                        {
                            if (To_check != String.Empty)
                            {
                                if (int.Parse(To_check) == int.Parse(lbl_u0_software_idx.Text))
                                {
                                    CheckBox_free.Checked = true;
                                }
                            }
                        }
                    }
                }

                break;
            case "Gv_detail_software":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView Gv_detail_software = (GridView)FvViewDetailDevice.FindControl("Gv_detail_software");
                    Label lbl_software_idx = ((Label)e.Row.FindControl("lbl_software_idx"));
                    Label lbl_u0_software_licen = ((Label)FvViewDetailDevice.FindControl("lbl_u0_software_licen"));
                    CheckBox CheckBox_licence = ((CheckBox)e.Row.FindControl("CheckBox_licence"));

                    if (Gv_detail_software.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        string Check = lbl_u0_software_licen.Text;//ViewState["software_free_check"].ToString();
                        string[] ToId = Check.Split(',');
                        foreach (string To_check in ToId)
                        {
                            if (To_check != String.Empty)
                            {
                                if (int.Parse(To_check) == int.Parse(lbl_software_idx.Text))
                                {
                                    CheckBox_licence.Checked = true;
                                }
                            }
                        }
                    }
                }

                break;

            case "GvDetail_Cutoff_List":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton btnView_cutoff = ((LinkButton)e.Row.FindControl("btnView_cutoff"));
                    Label lbl_HDEmpIDX = ((Label)e.Row.FindControl("lbl_HDEmpIDX"));
                    Label lbl_HD_R0_depidx = ((Label)e.Row.FindControl("lbl_HD_R0_depidx"));
                    Label lbl_HD_R0_secidx = ((Label)e.Row.FindControl("lbl_HD_R0_secidx"));
                    Label lbl_HD_Level = ((Label)e.Row.FindControl("lbl_HD_Level"));

                    Label lbl_u0_unidx = ((Label)e.Row.FindControl("lbl_u0_unidx"));
                    Label lbl_u0_acidx = ((Label)e.Row.FindControl("lbl_u0_acidx"));
                    Label lbl_u0_doc_decision = ((Label)e.Row.FindControl("lbl_u0_doc_decision"));

                    if (GvDetail_Cutoff_List.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        linkBtnTrigger(btnView_cutoff);

                        switch (lbl_u0_unidx.Text)
                        {
                            case "8": // Direc User 8,3,1
                                if (lbl_u0_acidx.Text == "3") //actor
                                {
                                    if (lbl_u0_doc_decision.Text == ViewState["rdept_idx"].ToString()) //dept
                                    {
                                        if (int.Parse(ViewState["jobgrade_level"].ToString()) >= 9) // Grade
                                        {
                                            e.Row.Visible = true;
                                        }
                                        else
                                        {
                                            e.Row.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        e.Row.Visible = false;
                                    }
                                }
                                else
                                {
                                    e.Row.Visible = false;
                                }
                                break;
                            case "10": // It Support 10,1,4
                                if (lbl_u0_acidx.Text == "1") //actor
                                {
                                    if (ViewState["rdept_idx"].ToString() == "20") //dept
                                    {
                                        if (int.Parse(ViewState["jobgrade_level"].ToString()) >= 4 && int.Parse(ViewState["jobgrade_level"].ToString()) <= 5) // Grade
                                        {
                                            e.Row.Visible = true;
                                        }
                                        else
                                        {
                                            e.Row.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        e.Row.Visible = false;
                                    }
                                }
                                else
                                {
                                    e.Row.Visible = false;
                                }
                                break;
                            case "11": // Head it support 11,4,4
                                if (lbl_u0_acidx.Text == "4") //actor
                                {
                                    if (ViewState["rdept_idx"].ToString() == "20") //dept
                                    {
                                        if (int.Parse(ViewState["jobgrade_level"].ToString()) >= 6 && int.Parse(ViewState["jobgrade_level"].ToString()) <= 8) // Grade
                                        {
                                            e.Row.Visible = true;
                                        }
                                        else
                                        {
                                            e.Row.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        e.Row.Visible = false;
                                    }
                                }
                                else
                                {
                                    e.Row.Visible = false;
                                }
                                break;
                            case "12": // direct it 12,5,4
                                if (lbl_u0_acidx.Text == "5") //actor
                                {
                                    if (ViewState["rdept_idx"].ToString() == "20") //dept
                                    {
                                        if (int.Parse(ViewState["jobgrade_level"].ToString()) >= 9) // Grade
                                        {
                                            e.Row.Visible = true;
                                        }
                                        else
                                        {
                                            e.Row.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        e.Row.Visible = false;
                                    }
                                }
                                else
                                {
                                    e.Row.Visible = false;
                                }
                                break;
                            case "13": // asset 13,6,4
                                if (lbl_u0_acidx.Text == "6") //actor
                                {
                                    if (ViewState["rdept_idx"].ToString() == "9") //dept บัญชีต้นทุน
                                    {
                                        if (int.Parse(ViewState["jobgrade_level"].ToString()) >= 4) // Grade
                                        {
                                            e.Row.Visible = true;
                                        }
                                        else
                                        {
                                            e.Row.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        e.Row.Visible = false;
                                    }
                                }
                                else
                                {
                                    e.Row.Visible = false;
                                }
                                break;
                            default:
                                e.Row.Visible = false;
                                break;
                        }

                        if (lbl_HDEmpIDX.Text == ViewState["EmpIDX"].ToString())
                        {
                            e.Row.Visible = true;
                        }

                        if (ViewState["rdept_idx"].ToString() == "20" && ViewState["Sec_idx"].ToString() == "80") //IT Support
                        {
                            e.Row.Visible = true;
                        }
                    }
                }
                break;

            case "GvDetail_Cutoff_List_one":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //ViewState["rdept_idx"]
                    //ViewState["Sec_idx"]
                    //ViewState["EmpIDX"]
                    //ViewState["jobgrade_level"]

                    LinkButton btnView_cutoff_1 = ((LinkButton)e.Row.FindControl("btnView_cutoff_1"));
                    Label lbl_HDEmpIDX_one = ((Label)e.Row.FindControl("lbl_HDEmpIDX_one"));
                    Label lbl_HD_R0_depidx_one = ((Label)e.Row.FindControl("lbl_HD_R0_depidx_one"));
                    Label lbl_HD_R0_secidx_one = ((Label)e.Row.FindControl("lbl_HD_R0_secidx_one"));
                    Label lbl_HD_Level_one = ((Label)e.Row.FindControl("lbl_HD_Level_one"));

                    Label lbl_u0_unidx_one = ((Label)e.Row.FindControl("lbl_u0_unidx_one"));
                    Label lbl_u0_acidx_one = ((Label)e.Row.FindControl("lbl_u0_acidx_one"));
                    Label lbl_u0_doc_decision_one = ((Label)e.Row.FindControl("lbl_u0_doc_decision_one"));

                    if (GvDetail_Cutoff_List_one.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        linkBtnTrigger(btnView_cutoff_1);

                        switch (lbl_u0_unidx_one.Text)
                        {
                            case "8": // Direc User 8,3,1
                                if (lbl_u0_acidx_one.Text == "3") //actor
                                {
                                    if (lbl_HD_R0_depidx_one.Text == ViewState["rdept_idx"].ToString()) //dept
                                    {
                                        if (int.Parse(ViewState["jobgrade_level"].ToString()) >= 9) // Grade
                                        {
                                            e.Row.Visible = true;
                                        }
                                        else
                                        {
                                            e.Row.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        e.Row.Visible = false;
                                    }
                                }
                                else
                                {
                                    e.Row.Visible = false;
                                }
                                break;
                            case "10": // It Support 10,1,4
                                if (lbl_u0_acidx_one.Text == "1") //actor
                                {
                                    if (ViewState["rdept_idx"].ToString() == "20") //dept
                                    {
                                        if (int.Parse(ViewState["jobgrade_level"].ToString()) >= 4 && int.Parse(ViewState["jobgrade_level"].ToString()) <= 5) // Grade
                                        {
                                            e.Row.Visible = true;
                                        }
                                        else
                                        {
                                            e.Row.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        e.Row.Visible = false;
                                    }
                                }
                                else
                                {
                                    e.Row.Visible = false;
                                }
                                break;
                            case "11": // Head it support 11,4,4
                                if (lbl_u0_acidx_one.Text == "4") //actor
                                {
                                    if (ViewState["rdept_idx"].ToString() == "20") //dept
                                    {
                                        if (int.Parse(ViewState["jobgrade_level"].ToString()) >= 6 && int.Parse(ViewState["jobgrade_level"].ToString()) <= 8) // Grade
                                        {
                                            e.Row.Visible = true;
                                        }
                                        else
                                        {
                                            e.Row.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        e.Row.Visible = false;
                                    }
                                }
                                else
                                {
                                    e.Row.Visible = false;
                                }
                                break;
                            case "12": // direct it 12,5,4
                                if (lbl_u0_acidx_one.Text == "5") //actor
                                {
                                    if (ViewState["rdept_idx"].ToString() == "20") //dept
                                    {
                                        if (int.Parse(ViewState["jobgrade_level"].ToString()) >= 9) // Grade
                                        {
                                            e.Row.Visible = true;
                                        }
                                        else
                                        {
                                            e.Row.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        e.Row.Visible = false;
                                    }
                                }
                                else
                                {
                                    e.Row.Visible = false;
                                }
                                break;
                            case "13": // asset 13,6,4
                                if (lbl_u0_acidx_one.Text == "6") //actor
                                {
                                    if (ViewState["rdept_idx"].ToString() == "9") //dept บัญชีต้นทุน
                                    {
                                        if (int.Parse(ViewState["jobgrade_level"].ToString()) >= 4) // Grade
                                        {
                                            e.Row.Visible = true;
                                        }
                                        else
                                        {
                                            e.Row.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        e.Row.Visible = false;
                                    }
                                }
                                else
                                {
                                    e.Row.Visible = false;
                                }
                                break;
                            default:
                                e.Row.Visible = false;
                                break;
                        }

                        if (lbl_HDEmpIDX_one.Text == ViewState["EmpIDX"].ToString())
                        {
                            e.Row.Visible = true;
                        }

                        if (ViewState["rdept_idx"].ToString() == "20" && ViewState["Sec_idx"].ToString() == "80") //IT Support
                        {
                            e.Row.Visible = true;
                        }
                    }
                }
                break;

            case "gvFileCutoff":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;

            case "Gv_Master_cpu":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;
            case "Gv_Master_ram":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;
            case "Gv_Master_hdd":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;
            case "Gv_Master_vga":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;
            case "Gv_Master_printer":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;
            case "Gv_Master_moniter":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;
            case "Gv_Master_typedevice":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;
            case "Gv_Master_band":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;
            case "Gv_Master_level":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;
            case "Gv_Master_status":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;
            case "Gv_Master_insurance":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;

        }
    }
    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDevice":
                GvDevice.PageIndex = e.NewPageIndex;
                GvDevice.DataBind();
                Select_Detail_Device();
                break;
            case "Gv_detail_software":
                GridView Gv_detail_software = (GridView)FvViewDetailDevice.FindControl("Gv_detail_software");
                Gv_detail_software.PageIndex = e.NewPageIndex;
                Gv_detail_software.DataBind();
                Select_Gv_software(int.Parse(ViewState["lbl_view_u0_didx_view"].ToString()), ViewState["lbl_u0_software_licen_view"].ToString());
                break;
            case "GvSoftware_free":
                GridView GvSoftware_free = (GridView)FvViewDetailDevice.FindControl("GvSoftware_free");
                GvSoftware_free.PageIndex = e.NewPageIndex;
                GvSoftware_free.DataBind();
                Select_ck_software();
                break;
            case "GvDeviceDatabase_software":
                GridView GvDeviceDatabase_software = (GridView)FvViewDetailDevice.FindControl("GvDeviceDatabase_software");
                GvDeviceDatabase_software.PageIndex = e.NewPageIndex;
                GvDeviceDatabase_software.DataBind();
                Select_Searchsoftware_u0_device();
                break;
            case "GvTemp_sotfware":
                GridView GvTemp_sotfware = (GridView)FvViewDetailDevice.FindControl("GvTemp_sotfware");
                GvTemp_sotfware.PageIndex = e.NewPageIndex;
                GvTemp_sotfware.DataBind();
                GvTemp_sotfware.DataSource = (DataSet)ViewState["vsTemp_sw"];
                GvTemp_sotfware.DataBind();
                break;
            case "GvDeviceDatabase":
                GridView GvDeviceDatabase = (GridView)FvViewDetailDevice.FindControl("GvDeviceDatabase");
                GvDeviceDatabase.PageIndex = e.NewPageIndex;
                GvDeviceDatabase.DataBind();
                Select_Searchtranfer_u0_device();
                break;
            case "GvEmpTemp":
                GridView GvEmpTemp = (GridView)FvViewDetailDevice.FindControl("GvEmpTemp");
                GvEmpTemp.PageIndex = e.NewPageIndex;
                GvEmpTemp.DataBind();
                GvEmpTemp.DataSource = (DataSet)ViewState["vsTempDevicw"];
                GvEmpTemp.DataBind();
                break;
            case "GvDevice_Detail_Search_cpu":
                GridView GvDevice_Detail_Search_cpu = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_cpu");
                GvDevice_Detail_Search_cpu.PageIndex = e.NewPageIndex;
                GvDevice_Detail_Search_cpu.DataBind();
                Select_Searchtranfer_u0_device_cpu();
                break;
            case "GvDetail_CPU":
                GridView GvDetail_CPU = (GridView)FvViewDetailDevice.FindControl("GvDetail_CPU");
                Label lbl_view_u0_didx = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_CPU.PageIndex = e.NewPageIndex;
                GvDetail_CPU.DataBind();
                Select_edit_u0_device_code_cpu(int.Parse(lbl_view_u0_didx.Text));
                break;
            case "GvDevice_Detail_Search_ram":
                GridView GvDevice_Detail_Search_ram = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_ram");
                GvDevice_Detail_Search_ram.PageIndex = e.NewPageIndex;
                GvDevice_Detail_Search_ram.DataBind();
                Select_Searchtranfer_u0_device_ram();
                break;
            case "GvDetail_RAM":
                GridView GvDetail_RAM = (GridView)FvViewDetailDevice.FindControl("GvDetail_RAM");
                Label lbl_view_u0_didx_ram = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_RAM.PageIndex = e.NewPageIndex;
                GvDetail_RAM.DataBind();
                Select_edit_u0_device_code_ram(int.Parse(lbl_view_u0_didx_ram.Text));
                break;
            case "GvDevice_Detail_Search_hdd":
                GridView GvDevice_Detail_Search_hdd = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_hdd");
                GvDevice_Detail_Search_hdd.PageIndex = e.NewPageIndex;
                GvDevice_Detail_Search_hdd.DataBind();
                Select_Searchtranfer_u0_device_hdd();
                break;
            case "GvDetail_HDD":
                GridView GvDetail_HDD = (GridView)FvViewDetailDevice.FindControl("GvDetail_HDD");
                Label lbl_view_u0_didx_hdd = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_HDD.PageIndex = e.NewPageIndex;
                GvDetail_HDD.DataBind();
                Select_edit_u0_device_code_hdd(int.Parse(lbl_view_u0_didx_hdd.Text));
                break;
            case "GvDevice_Detail_Search_vga":
                GridView GvDevice_Detail_Search_vga = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_vga");
                GvDevice_Detail_Search_vga.PageIndex = e.NewPageIndex;
                GvDevice_Detail_Search_vga.DataBind();
                Select_Searchtranfer_u0_device_vga();
                break;
            case "GvDetail_VGA":
                GridView GvDetail_VGA = (GridView)FvViewDetailDevice.FindControl("GvDetail_VGA");
                Label lbl_view_u0_didx_vga = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_VGA.PageIndex = e.NewPageIndex;
                GvDetail_VGA.DataBind();
                Select_edit_u0_device_code_vga(int.Parse(lbl_view_u0_didx_vga.Text));
                break;
            case "GvDevice_Detail_Search_etc":
                GridView GvDevice_Detail_Search_etc = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_etc");
                GvDevice_Detail_Search_etc.PageIndex = e.NewPageIndex;
                GvDevice_Detail_Search_etc.DataBind();
                Select_Searchtranfer_u0_device_etc();
                break;
            case "GvDetail_ETC":
                GridView GvDetail_ETC = (GridView)FvViewDetailDevice.FindControl("GvDetail_ETC");
                Label lbl_view_u0_didx_etc = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_ETC.PageIndex = e.NewPageIndex;
                GvDetail_ETC.DataBind();
                Select_edit_u0_device_code_etc(int.Parse(lbl_view_u0_didx_etc.Text));
                break;
            case "Gv_cutoff_detail":
                GridView Gv_cutoff_detail = (GridView)FvViewDetailDevice.FindControl("Gv_cutoff_detail");
                Gv_cutoff_detail.PageIndex = e.NewPageIndex;
                Gv_cutoff_detail.DataBind();
                Select_Gv_Device_cutoff(int.Parse(ViewState["lbl_view_u0_didx_cut"].ToString()));
                break;
            case "GvDetail_Cutoff_List":
                GvDetail_Cutoff_List.PageIndex = e.NewPageIndex;
                GvDetail_Cutoff_List.DataBind();
                Select_GvDevice_Cutoff_List(int.Parse(ViewState["btnapprovecutoff_u0_didx"].ToString()));
                break;
            case "GvDetail_Cutoff_List_one":
                GvDetail_Cutoff_List_one.PageIndex = e.NewPageIndex;
                GvDetail_Cutoff_List_one.DataBind();
                Select_GvDevice_Cutoff_List(int.Parse(ViewState["btnapprovecutoff_u0_didx"].ToString()));
                break;
            case "Gv_Master_cpu":
                Gv_Master_cpu.PageIndex = e.NewPageIndex;
                Gv_Master_cpu.DataBind();
                Select_Gridview_m0_cpu();
                break;
            case "Gv_Master_ram":
                Gv_Master_ram.PageIndex = e.NewPageIndex;
                Gv_Master_ram.DataBind();
                Select_Gridview_m0_ram();
                break;
            case "Gv_Master_hdd":
                Gv_Master_hdd.PageIndex = e.NewPageIndex;
                Gv_Master_hdd.DataBind();
                Select_Gridview_m0_hdd();
                break;
            case "Gv_Master_vga":
                Gv_Master_vga.PageIndex = e.NewPageIndex;
                Gv_Master_vga.DataBind();
                Select_Gridview_m0_vga();
                break;
            case "Gv_Master_printer":
                Gv_Master_printer.PageIndex = e.NewPageIndex;
                Gv_Master_printer.DataBind();
                Select_Gridview_m0_printer();
                break;
            case "Gv_Master_moniter":
                Gv_Master_moniter.PageIndex = e.NewPageIndex;
                Gv_Master_moniter.DataBind();
                Select_Gridview_m0_moniter();
                break;
            case "Gv_Master_typedevice":
                Gv_Master_typedevice.PageIndex = e.NewPageIndex;
                Gv_Master_typedevice.DataBind();
                Select_Gridview_m0_typedevice();
                break;
            case "Gv_Master_band":
                Gv_Master_band.PageIndex = e.NewPageIndex;
                Gv_Master_band.DataBind();
                Select_Gridview_m0_band();
                break;
            case "Gv_Master_level":
                Gv_Master_level.PageIndex = e.NewPageIndex;
                Gv_Master_level.DataBind();
                Select_Gridview_m0_level();
                break;
            case "Gv_Master_status":
                Gv_Master_status.PageIndex = e.NewPageIndex;
                Gv_Master_status.DataBind();
                Select_Gridview_m0_status();
                break;
            case "Gv_Master_insurance":
                Gv_Master_insurance.PageIndex = e.NewPageIndex;
                Gv_Master_insurance.DataBind();
                Select_Gridview_m0_insurance();
                break;
            case "GvDetail_Approve_New":
                GridView GvDetail_Approve_New = (GridView)FvViewApproveDevice.FindControl("GvDetail_Approve_New");
                GvDetail_Approve_New.PageIndex = e.NewPageIndex;
                GvDetail_Approve_New.DataBind();
                Select_Gridview_Approve();
                break;
            case "GvDetail_Approve_Transfer":
                GridView GvDetail_Approve_Transfer = (GridView)FvViewApproveDevice_transfer.FindControl("GvDetail_Approve_Transfer");
                GvDetail_Approve_Transfer.PageIndex = e.NewPageIndex;
                GvDetail_Approve_Transfer.DataBind();
                Select_Approve_Transfer_All();
                break;
        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetail_CPU":
                GridView GvDetail_CPU = (GridView)FvViewDetailDevice.FindControl("GvDetail_CPU");
                Label lbl_view_u0_didx = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_CPU.EditIndex = -1;
                Select_edit_u0_device_code_cpu(int.Parse(lbl_view_u0_didx.Text));
                break;
            case "GvDetail_RAM":
                GridView GvDetail_RAM = (GridView)FvViewDetailDevice.FindControl("GvDetail_RAM");
                Label lbl_view_u0_didx_ram = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_RAM.EditIndex = -1;
                Select_edit_u0_device_code_ram(int.Parse(lbl_view_u0_didx_ram.Text));
                break;
            case "GvDetail_HDD":
                GridView GvDetail_HDD = (GridView)FvViewDetailDevice.FindControl("GvDetail_HDD");
                Label lbl_view_u0_didx_hdd = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_HDD.EditIndex = -1;
                Select_edit_u0_device_code_hdd(int.Parse(lbl_view_u0_didx_hdd.Text));
                break;
            case "GvDetail_VGA":
                GridView GvDetail_VGA = (GridView)FvViewDetailDevice.FindControl("GvDetail_VGA");
                Label lbl_view_u0_didx_vga = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_VGA.EditIndex = -1;
                Select_edit_u0_device_code_vga(int.Parse(lbl_view_u0_didx_vga.Text));
                break;
            case "GvDetail_ETC":
                GridView GvDetail_ETC = (GridView)FvViewDetailDevice.FindControl("GvDetail_ETC");
                Label lbl_view_u0_didx_etc = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_ETC.EditIndex = -1;
                Select_edit_u0_device_code_etc(int.Parse(lbl_view_u0_didx_etc.Text));
                break;
            case "Gv_Master_cpu":
                //GridView GvDetail_ETC = (GridView)FvViewDetailDevice.FindControl("GvDetail_ETC");
                //Label lbl_view_u0_didx_etc = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                Gv_Master_cpu.EditIndex = -1;
                Select_Gridview_m0_cpu();
                break;
            case "Gv_Master_ram":
                Gv_Master_ram.EditIndex = -1;
                Select_Gridview_m0_ram();
                break;
            case "Gv_Master_hdd":
                Gv_Master_hdd.EditIndex = -1;
                Select_Gridview_m0_hdd();
                break;
            case "Gv_Master_vga":
                Gv_Master_vga.EditIndex = -1;
                Select_Gridview_m0_vga();
                break;
            case "Gv_Master_printer":
                Gv_Master_printer.EditIndex = -1;
                Select_Gridview_m0_printer();
                break;
            case "Gv_Master_moniter":
                Gv_Master_moniter.EditIndex = -1;
                Select_Gridview_m0_moniter();
                break;
            case "Gv_Master_typedevice":
                Gv_Master_typedevice.EditIndex = -1;
                Select_Gridview_m0_typedevice();
                break;
            case "Gv_Master_band":
                Gv_Master_band.EditIndex = -1;
                Select_Gridview_m0_band();
                break;
            case "Gv_Master_level":
                Gv_Master_level.EditIndex = -1;
                Select_Gridview_m0_level();
                break;
            case "Gv_Master_status":
                Gv_Master_status.EditIndex = -1;
                Select_Gridview_m0_status();
                break;
            case "Gv_Master_insurance":
                Gv_Master_insurance.EditIndex = -1;
                Select_Gridview_m0_insurance();
                break;
        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetail_CPU":
                GridView GvDetail_CPU = (GridView)FvViewDetailDevice.FindControl("GvDetail_CPU");
                Label lbl_view_u0_didx = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_CPU.EditIndex = e.NewEditIndex;
                Select_edit_u0_device_code_cpu(int.Parse(lbl_view_u0_didx.Text));
                break;
            case "GvDetail_RAM":
                GridView GvDetail_RAM = (GridView)FvViewDetailDevice.FindControl("GvDetail_RAM");
                Label lbl_view_u0_didx_ram = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_RAM.EditIndex = e.NewEditIndex;
                Select_edit_u0_device_code_ram(int.Parse(lbl_view_u0_didx_ram.Text));
                break;
            case "GvDetail_HDD":
                GridView GvDetail_HDD = (GridView)FvViewDetailDevice.FindControl("GvDetail_HDD");
                Label lbl_view_u0_didx_hdd = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_HDD.EditIndex = e.NewEditIndex;
                Select_edit_u0_device_code_hdd(int.Parse(lbl_view_u0_didx_hdd.Text));
                break;
            case "GvDetail_VGA":
                GridView GvDetail_VGA = (GridView)FvViewDetailDevice.FindControl("GvDetail_VGA");
                Label lbl_view_u0_didx_vga = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_VGA.EditIndex = e.NewEditIndex;
                Select_edit_u0_device_code_vga(int.Parse(lbl_view_u0_didx_vga.Text));
                break;
            case "GvDetail_ETC":
                GridView GvDetail_ETC = (GridView)FvViewDetailDevice.FindControl("GvDetail_ETC");
                Label lbl_view_u0_didx_etc = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_ETC.EditIndex = e.NewEditIndex;
                Select_edit_u0_device_code_etc(int.Parse(lbl_view_u0_didx_etc.Text));
                break;
            case "Gv_Master_cpu":
                Gv_Master_cpu.EditIndex = e.NewEditIndex;
                Select_Gridview_m0_cpu();
                break;
            case "Gv_Master_ram":
                Gv_Master_ram.EditIndex = e.NewEditIndex;
                Select_Gridview_m0_ram();
                break;
            case "Gv_Master_hdd":
                Gv_Master_hdd.EditIndex = e.NewEditIndex;
                Select_Gridview_m0_hdd();
                break;
            case "Gv_Master_vga":
                Gv_Master_vga.EditIndex = e.NewEditIndex;
                Select_Gridview_m0_vga();
                break;
            case "Gv_Master_printer":
                Gv_Master_printer.EditIndex = e.NewEditIndex;
                Select_Gridview_m0_printer();
                break;
            case "Gv_Master_moniter":
                Gv_Master_moniter.EditIndex = e.NewEditIndex;
                Select_Gridview_m0_moniter();
                break;
            case "Gv_Master_typedevice":
                Gv_Master_typedevice.EditIndex = e.NewEditIndex;
                Select_Gridview_m0_typedevice();
                break;
            case "Gv_Master_band":
                Gv_Master_band.EditIndex = e.NewEditIndex;
                Select_Gridview_m0_band();
                break;
            case "Gv_Master_level":
                Gv_Master_level.EditIndex = e.NewEditIndex;
                Select_Gridview_m0_level();
                break;
            case "Gv_Master_status":
                Gv_Master_level.EditIndex = e.NewEditIndex;
                Select_Gridview_m0_status();
                break;
            case "Gv_Master_insurance":
                Gv_Master_insurance.EditIndex = e.NewEditIndex;
                Select_Gridview_m0_insurance();
                break;
        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetail_CPU":
                GridView GvDetail_CPU = (GridView)FvViewDetailDevice.FindControl("GvDetail_CPU");
                Label lbl_view_u0_didx = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_CPU.EditIndex = -1;
                Select_edit_u0_device_code_cpu(int.Parse(lbl_view_u0_didx.Text));
                break;
            case "GvDetail_RAM":
                GridView GvDetail_RAM = (GridView)FvViewDetailDevice.FindControl("GvDetail_RAM");
                Label lbl_view_u0_didx_ram = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_RAM.EditIndex = -1;
                Select_edit_u0_device_code_ram(int.Parse(lbl_view_u0_didx_ram.Text));
                break;
            case "GvDetail_HDD":
                GridView GvDetail_HDD = (GridView)FvViewDetailDevice.FindControl("GvDetail_HDD");
                Label lbl_view_u0_didx_hdd = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_HDD.EditIndex = -1;
                Select_edit_u0_device_code_hdd(int.Parse(lbl_view_u0_didx_hdd.Text));
                break;
            case "GvDetail_VGA":
                GridView GvDetail_VGA = (GridView)FvViewDetailDevice.FindControl("GvDetail_VGA");
                Label lbl_view_u0_didx_vga = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_VGA.EditIndex = -1;
                Select_edit_u0_device_code_vga(int.Parse(lbl_view_u0_didx_vga.Text));
                break;
            case "GvDetail_ETC":
                GridView GvDetail_ETC = (GridView)FvViewDetailDevice.FindControl("GvDetail_ETC");
                Label lbl_view_u0_didx_etc = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                GvDetail_ETC.EditIndex = -1;
                Select_edit_u0_device_code_etc(int.Parse(lbl_view_u0_didx_etc.Text));
                break;

            case "Gv_Master_cpu":
                var txt_master_m0_cidx = (TextBox)Gv_Master_cpu.Rows[e.RowIndex].FindControl("txt_master_m0_cidx");
                var txt_master_band_cpu = (TextBox)Gv_Master_cpu.Rows[e.RowIndex].FindControl("txt_master_band_cpu");
                var txt_master_gen_cpu = (TextBox)Gv_Master_cpu.Rows[e.RowIndex].FindControl("txt_master_gen_cpu");
                var ddStatus_update_cpu = (DropDownList)Gv_Master_cpu.Rows[e.RowIndex].FindControl("ddStatus_update");

                Gv_Master_cpu.EditIndex = -1;

                ViewState["txt_master_m0_cidx"] = txt_master_m0_cidx.Text;
                ViewState["txt_master_band_cpu"] = txt_master_band_cpu.Text;
                ViewState["txt_master_gen_cpu"] = txt_master_gen_cpu.Text;
                ViewState["ddStatus_update_cpu"] = ddStatus_update_cpu.SelectedValue;

                Update_m0_cpu();
                Select_Gridview_m0_cpu();
                break;
            case "Gv_Master_ram":
                var txt_master_idx_ram = (TextBox)Gv_Master_ram.Rows[e.RowIndex].FindControl("txt_master_idx_ram");
                var txt_master_band_ram = (TextBox)Gv_Master_ram.Rows[e.RowIndex].FindControl("txt_master_band_ram");
                var txt_master_gen_ram = (TextBox)Gv_Master_ram.Rows[e.RowIndex].FindControl("txt_master_gen_ram");
                var txt_master_capa_ram = (TextBox)Gv_Master_ram.Rows[e.RowIndex].FindControl("txt_master_capa_ram");
                var ddStatus_update_ram = (DropDownList)Gv_Master_ram.Rows[e.RowIndex].FindControl("ddStatus_update");

                Gv_Master_ram.EditIndex = -1;

                ViewState["txt_master_idx_ram"] = txt_master_idx_ram.Text;
                ViewState["txt_master_band_ram"] = txt_master_band_ram.Text;
                ViewState["txt_master_gen_ram"] = txt_master_gen_ram.Text;
                ViewState["txt_master_capa_ram"] = txt_master_capa_ram.Text;
                ViewState["ddStatus_update_ram"] = ddStatus_update_ram.SelectedValue;

                Update_m0_ram();
                Select_Gridview_m0_ram();
                break;
            case "Gv_Master_hdd":
                var txt_master_idx_hdd = (TextBox)Gv_Master_hdd.Rows[e.RowIndex].FindControl("txt_master_idx_hdd");
                var txt_master_band_hdd = (TextBox)Gv_Master_hdd.Rows[e.RowIndex].FindControl("txt_master_band_hdd");
                var txt_master_gen_hdd = (TextBox)Gv_Master_hdd.Rows[e.RowIndex].FindControl("txt_master_gen_hdd");
                var txt_master_capa_hdd = (TextBox)Gv_Master_hdd.Rows[e.RowIndex].FindControl("txt_master_capa_hdd");
                var ddStatus_update_hdd = (DropDownList)Gv_Master_hdd.Rows[e.RowIndex].FindControl("ddStatus_update");

                Gv_Master_hdd.EditIndex = -1;

                ViewState["txt_master_idx_hdd"] = txt_master_idx_hdd.Text;
                ViewState["txt_master_band_hdd"] = txt_master_band_hdd.Text;
                ViewState["txt_master_gen_hdd"] = txt_master_gen_hdd.Text;
                ViewState["txt_master_capa_hdd"] = txt_master_capa_hdd.Text;
                ViewState["ddStatus_update_hdd"] = ddStatus_update_hdd.SelectedValue;

                Update_m0_hdd();
                Select_Gridview_m0_hdd();
                break;
            case "Gv_Master_vga":
                var txt_master_idx_vga = (TextBox)Gv_Master_vga.Rows[e.RowIndex].FindControl("txt_master_idx_vga");
                var txt_master_gen_vga = (TextBox)Gv_Master_vga.Rows[e.RowIndex].FindControl("txt_master_gen_vga");
                var txt_master_capa_vga = (TextBox)Gv_Master_vga.Rows[e.RowIndex].FindControl("txt_master_capa_vga");
                var ddStatus_update_vga = (DropDownList)Gv_Master_vga.Rows[e.RowIndex].FindControl("ddStatus_update");

                Gv_Master_vga.EditIndex = -1;

                ViewState["txt_master_idx_vga"] = txt_master_idx_vga.Text;
                ViewState["txt_master_gen_vga"] = txt_master_capa_vga.Text; 
                ViewState["txt_master_capa_vga"] = txt_master_gen_vga.Text;
                ViewState["ddStatus_update_vga"] = ddStatus_update_vga.SelectedValue;

                Update_m0_vga();
                Select_Gridview_m0_vga();
                break;
            case "Gv_Master_printer":
                var txt_master_idx_poid = (TextBox)Gv_Master_printer.Rows[e.RowIndex].FindControl("txt_master_idx_poid");
                var txt_addmaster_band_print = (TextBox)Gv_Master_printer.Rows[e.RowIndex].FindControl("txt_addmaster_band_print");
                var txt_master_type_print = (TextBox)Gv_Master_printer.Rows[e.RowIndex].FindControl("txt_master_type_print");
                var txt_master_ink_print = (TextBox)Gv_Master_printer.Rows[e.RowIndex].FindControl("txt_master_ink_print");
                var txt_master_gen_print = (TextBox)Gv_Master_printer.Rows[e.RowIndex].FindControl("txt_master_gen_print");
                var ddStatus_update_print = (DropDownList)Gv_Master_printer.Rows[e.RowIndex].FindControl("ddStatus_update");

                Gv_Master_printer.EditIndex = -1;

                ViewState["txt_master_idx_poid"] = txt_master_idx_poid.Text;
                ViewState["txt_addmaster_band_print"] = txt_addmaster_band_print.Text;
                ViewState["txt_master_type_print"] = txt_master_type_print.Text;
                ViewState["txt_master_ink_print"] = txt_master_ink_print.Text;
                ViewState["txt_master_gen_print"] = txt_master_gen_print.Text;
                ViewState["ddStatus_update_print"] = ddStatus_update_print.SelectedValue;

                Update_m0_printer();
                Select_Gridview_m0_printer();
                break;
            case "Gv_Master_moniter":
                var txt_master_idx_moniter = (TextBox)Gv_Master_moniter.Rows[e.RowIndex].FindControl("txt_master_idx_moniter");
                var txt_master_band_moniter = (TextBox)Gv_Master_moniter.Rows[e.RowIndex].FindControl("txt_master_band_moniter");
                var txt_master_type_moniter = (TextBox)Gv_Master_moniter.Rows[e.RowIndex].FindControl("txt_master_type_moniter");
                var ddStatus_update_moniter = (DropDownList)Gv_Master_moniter.Rows[e.RowIndex].FindControl("ddStatus_update");

                Gv_Master_moniter.EditIndex = -1;

                ViewState["txt_master_idx_moniter"] = txt_master_idx_moniter.Text;
                ViewState["txt_master_band_moniter"] = txt_master_band_moniter.Text;
                ViewState["txt_master_type_moniter"] = txt_master_type_moniter.Text;
                ViewState["ddStatus_update_moniter"] = ddStatus_update_moniter.SelectedValue;

                Update_m0_moniter();
                Select_Gridview_m0_moniter();
                break;
            case "Gv_Master_insurance":
                var txt_master_m0_iridx = (TextBox)Gv_Master_insurance.Rows[e.RowIndex].FindControl("txt_master_m0_iridx");
                var txt_master_name_insurance = (TextBox)Gv_Master_insurance.Rows[e.RowIndex].FindControl("txt_master_name_insurance");
                var txt_master_detail_insurance = (TextBox)Gv_Master_insurance.Rows[e.RowIndex].FindControl("txt_master_detail_insurance");
                var ddStatus_update_insurance = (DropDownList)Gv_Master_insurance.Rows[e.RowIndex].FindControl("ddStatus_update");

                Gv_Master_insurance.EditIndex = -1;

                ViewState["txt_master_m0_iridx"] = txt_master_m0_iridx.Text;
                ViewState["txt_master_name_insurance"] = txt_master_name_insurance.Text;
                ViewState["txt_master_detail_insurance"] = txt_master_detail_insurance.Text;
                ViewState["ddStatus_update_insurance"] = ddStatus_update_insurance.SelectedValue;

                Update_m0_insurance();
                Select_Gridview_m0_insurance();
                break;
            case "Gv_Master_band":
                var txt_master_m0_bidx = (TextBox)Gv_Master_band.Rows[e.RowIndex].FindControl("txt_master_m0_bidx");
                var txt_master_name_band = (TextBox)Gv_Master_band.Rows[e.RowIndex].FindControl("txt_master_name_band");
                var ddStatus_update_band = (DropDownList)Gv_Master_band.Rows[e.RowIndex].FindControl("ddStatus_update");

                Gv_Master_band.EditIndex = -1;

                ViewState["txt_master_m0_bidx"] = txt_master_m0_bidx.Text;
                ViewState["txt_master_name_band"] = txt_master_name_band.Text;
                ViewState["ddStatus_update_band"] = ddStatus_update_band.SelectedValue;

                Update_m0_band();
                Select_Gridview_m0_band();
                break;
        }
    }

    #endregion

    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "FvInsertDevice":
                FormView FvInsertDevice = (FormView)ViewAdd.FindControl("FvInsertDevice");
                //FormView _FvViewDetailDevice = (FormView)ViewAdd.FindControl("FvViewDetailDevice");
                DropDownList _ddl_add_org = (DropDownList)FvInsertDevice.FindControl("ddl_add_org");
                if (FvInsertDevice.CurrentMode == FormViewMode.Insert)
                {
                    Select_m0_typedevice();
                    Select_m0_insurance();
                    Select_m0_band();
                    Select_m0_status();
                    Select_m0_level();
                    Select_m0_cpu();
                    Select_m0_ram();
                    Select_m0_hdd();
                    Select_m0_vga();
                    Select_m0_printer();
                    Select_m0_moniter();
                    getOrganizationList(_ddl_add_org);
                }
                break;

            case "FvInsertMasterDevice":
                FormView FvInsertMasterDevice = (FormView)ViewAdd.FindControl("FvInsertMasterDevice");
                if (FvInsertMasterDevice.CurrentMode == FormViewMode.Insert)
                {

                }
                break;

            case "FvViewDetailDevice":

                FormView FvViewDetailDevice = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                Label lbl_view_typedevice = (Label)FvViewDetailDevice.FindControl("lbl_view_typedevice");
                Label lbl_view_u0_didx = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                Label lbl_m0_orgidx_view = (Label)FvViewDetailDevice.FindControl("lbl_m0_orgidx_view");
                Label lbl_R0_depidx_view = (Label)FvViewDetailDevice.FindControl("lbl_R0_depidx_view");
                Label lbl_R0_secidx_view = (Label)FvViewDetailDevice.FindControl("lbl_R0_secidx_view");
                Panel boxmonitor = (Panel)FvViewDetailDevice.FindControl("boxview_moniter");
                Panel boxprinter = (Panel)FvViewDetailDevice.FindControl("boxview_printer");
                Panel boxcpu = (Panel)FvViewDetailDevice.FindControl("boxview_cpu");
                Panel boxram = (Panel)FvViewDetailDevice.FindControl("boxview_ram");
                Panel boxhdd = (Panel)FvViewDetailDevice.FindControl("boxview_hdd");
                Panel boxvga = (Panel)FvViewDetailDevice.FindControl("boxview_vga");
                Panel boxlevel = (Panel)FvViewDetailDevice.FindControl("boxcomputer");
                Panel boxslot = (Panel)FvViewDetailDevice.FindControl("boxslot");
                Panel boxtype = (Panel)FvViewDetailDevice.FindControl("boxtype");
                Panel Box_view_dept = (Panel)FvViewDetailDevice.FindControl("Box_view_dept");
                Panel Box_view_sec = (Panel)FvViewDetailDevice.FindControl("Box_view_sec");
                FormView FvViewApproveDevice = (FormView)FvViewDetailDevice.FindControl("FvViewApproveDevice");
                GridView GvDevice_Detail_Search_cpu_hide = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_cpu");
                GridView GvDevice_Detail_Search_ram_hide = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_ram");
                GridView GvDevice_Detail_Search_hdd_hide = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_hdd");
                GridView GvDevice_Detail_Search_vga_hide = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_vga");
                GridView GvDevice_Detail_Search_etc_hide = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_etc");
                DropDownList _ddl_view_org = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_org");
                DropDownList _ddl_view_dept = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_dept");
                DropDownList _ddl_view_sec = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_sec");
                Panel Box_viewdetail = (Panel)FvViewDetailDevice.FindControl("Box_viewdetail");
                GvDevice_Detail_Search_cpu_hide.Visible = false;
                GvDevice_Detail_Search_ram_hide.Visible = false;
                GvDevice_Detail_Search_hdd_hide.Visible = false;
                GvDevice_Detail_Search_vga_hide.Visible = false;
                GvDevice_Detail_Search_etc_hide.Visible = false;

                Edit_m0_insurance();
                Edit_m0_band();
                Edit_m0_status();

                getOrganizationList(_ddl_view_org);
                _ddl_view_org.SelectedValue = lbl_m0_orgidx_view.Text;
                getDepartmentList(_ddl_view_dept, int.Parse(_ddl_view_org.SelectedItem.Value));
                _ddl_view_dept.SelectedValue = lbl_R0_depidx_view.Text;
                getSectionList(_ddl_view_sec, int.Parse(_ddl_view_org.SelectedItem.Value), int.Parse(_ddl_view_dept.SelectedItem.Value));
                _ddl_view_sec.SelectedValue = lbl_R0_secidx_view.Text;

                switch (lbl_view_typedevice.Text)
                {
                    case "0": //defult
                    case "5": //Handheld                  
                    case "10": //Other
                        boxmonitor.Visible = false;
                        boxprinter.Visible = false;
                        boxcpu.Visible = false;
                        boxram.Visible = false;
                        boxhdd.Visible = false;
                        boxvga.Visible = false;
                        boxlevel.Visible = false;
                        boxslot.Visible = false;
                        boxtype.Visible = false;
                        Box_view_dept.Visible = false;
                        Box_view_sec.Visible = false;
                        Box_viewdetail.Visible = false;
                        break;
                    case "1": //NoteBook
                    case "2": //PC 
                        boxmonitor.Visible = false;
                        boxprinter.Visible = false;
                        boxcpu.Visible = false;
                        boxram.Visible = false;
                        boxhdd.Visible = false;
                        boxvga.Visible = false;
                        boxlevel.Visible = true;
                        boxslot.Visible = false;
                        boxtype.Visible = false;
                        Box_view_dept.Visible = true;
                        Box_view_sec.Visible = true;
                        Box_viewdetail.Visible = true;
                        Edit_m0_level();
                        Edit_m0_insurance();
                        Edit_m0_band();
                        Edit_m0_status();
                        Select_edit_u0_device_code_cpu(int.Parse(lbl_view_u0_didx.Text));
                        Select_edit_u0_device_code_ram(int.Parse(lbl_view_u0_didx.Text));
                        Select_edit_u0_device_code_hdd(int.Parse(lbl_view_u0_didx.Text));
                        Select_edit_u0_device_code_vga(int.Parse(lbl_view_u0_didx.Text));
                        Select_edit_u0_device_code_etc(int.Parse(lbl_view_u0_didx.Text));
                        select_viewdetail_cpu(int.Parse(lbl_view_u0_didx.Text));
                        select_viewdetail_ram(int.Parse(lbl_view_u0_didx.Text));
                        select_viewdetail_hdd(int.Parse(lbl_view_u0_didx.Text));
                        select_viewdetail_vga(int.Parse(lbl_view_u0_didx.Text));
                        FvViewApproveDevice.ChangeMode(FormViewMode.Insert);
                        FvViewApproveDevice.DataBind();
                        break;
                    case "3": //Printer
                        boxmonitor.Visible = false;
                        boxprinter.Visible = true;
                        boxcpu.Visible = false;
                        boxram.Visible = false;
                        boxhdd.Visible = false;
                        boxvga.Visible = false;
                        boxlevel.Visible = false;
                        boxslot.Visible = false;
                        boxtype.Visible = false;
                        Box_view_dept.Visible = false;
                        Box_view_sec.Visible = false;
                        Box_viewdetail.Visible = false;
                        Edit_m0_printer();
                        break;
                    case "4": //Moniter
                        boxmonitor.Visible = true;
                        boxprinter.Visible = false;
                        boxcpu.Visible = false;
                        boxram.Visible = false;
                        boxhdd.Visible = false;
                        boxvga.Visible = false;
                        boxlevel.Visible = false;
                        boxslot.Visible = false;
                        boxtype.Visible = false;
                        Box_view_dept.Visible = false;
                        Box_view_sec.Visible = false;
                        Box_viewdetail.Visible = false;
                        Edit_m0_moniter();
                        break;
                    case "6": //cpu
                        boxmonitor.Visible = false;
                        boxprinter.Visible = false;
                        boxcpu.Visible = true;
                        boxram.Visible = false;
                        boxhdd.Visible = false;
                        boxvga.Visible = false;
                        boxlevel.Visible = false;
                        boxslot.Visible = true;
                        boxtype.Visible = true;
                        Box_view_dept.Visible = false;
                        Box_view_sec.Visible = false;
                        Box_viewdetail.Visible = false;
                        Edit_m0_cpu();
                        break;
                    case "7": //RAM
                        boxmonitor.Visible = false;
                        boxprinter.Visible = false;
                        boxcpu.Visible = false;
                        boxram.Visible = true;
                        boxhdd.Visible = false;
                        boxvga.Visible = false;
                        boxlevel.Visible = false;
                        boxslot.Visible = true;
                        boxtype.Visible = true;
                        Box_view_dept.Visible = false;
                        Box_view_sec.Visible = false;
                        Box_viewdetail.Visible = false;
                        Edit_m0_ram();
                        break;
                    case "8": //HDD
                        boxmonitor.Visible = false;
                        boxprinter.Visible = false;
                        boxcpu.Visible = false;
                        boxram.Visible = false;
                        boxhdd.Visible = true;
                        boxvga.Visible = false;
                        boxlevel.Visible = false;
                        boxslot.Visible = true;
                        boxtype.Visible = true;
                        Box_view_dept.Visible = false;
                        Box_view_sec.Visible = false;
                        Box_viewdetail.Visible = false;
                        Edit_m0_hdd();
                        break;
                    case "9": //VGA
                        boxmonitor.Visible = false;
                        boxprinter.Visible = false;
                        boxcpu.Visible = false;
                        boxram.Visible = false;
                        boxhdd.Visible = false;
                        boxvga.Visible = true;
                        boxlevel.Visible = false;
                        boxslot.Visible = true;
                        boxtype.Visible = true;
                        Box_view_dept.Visible = false;
                        Box_view_sec.Visible = false;
                        Box_viewdetail.Visible = false;
                        Edit_m0_vga();
                        break;
                }



                break;

            case "FvViewDetailDevice_Cutoff":

                FormView FvViewDetailDevice_Cutoff = (FormView)Viewapprovecutoff.FindControl("FvViewDetailDevice_Cutoff");
                Label lbl_view_typedevice_c = (Label)FvViewDetailDevice_Cutoff.FindControl("lbl_view_typedevice_c");
                Label lbl_view_u0_didx_c = (Label)FvViewDetailDevice_Cutoff.FindControl("lbl_view_u0_didx_c");
                Label lbl_m0_orgidx_view_c = (Label)FvViewDetailDevice_Cutoff.FindControl("lbl_m0_orgidx_view_c");
                Label lbl_R0_depidx_view_c = (Label)FvViewDetailDevice_Cutoff.FindControl("lbl_R0_depidx_view_c");
                Label lbl_R0_secidx_view_c = (Label)FvViewDetailDevice_Cutoff.FindControl("lbl_R0_secidx_view_c");
                //Panel boxmonitor_c = (Panel)FvViewDetailDevice_Cutoff.FindControl("boxview_moniter_c");
                //Panel boxprinter_c = (Panel)FvViewDetailDevice_Cutoff.FindControl("boxview_printer_c");
                Panel boxcpu_c = (Panel)FvViewDetailDevice_Cutoff.FindControl("boxview_cpu_c");
                Panel boxram_c = (Panel)FvViewDetailDevice_Cutoff.FindControl("boxview_ram_c");
                Panel boxhdd_c = (Panel)FvViewDetailDevice_Cutoff.FindControl("boxview_hdd_c");
                Panel boxvga_c = (Panel)FvViewDetailDevice_Cutoff.FindControl("boxview_vga_c");
                Panel boxlevel_c = (Panel)FvViewDetailDevice_Cutoff.FindControl("boxcomputer_c");
                Panel boxslot_c = (Panel)FvViewDetailDevice_Cutoff.FindControl("boxslot_c");

                switch (lbl_view_typedevice_c.Text)
                {
                    case "0": //defult
                    case "5": //Handheld                  
                    case "10": //Other
                        //boxmonitor_c.Visible = false;
                        //boxprinter_c.Visible = false;
                        boxcpu_c.Visible = false;
                        boxram_c.Visible = false;
                        boxhdd_c.Visible = false;
                        boxvga_c.Visible = false;
                        boxlevel_c.Visible = false;
                        boxslot_c.Visible = false;
                        break;
                    case "1": //NoteBook
                    case "2": //PC 
                        //boxmonitor_c.Visible = false;
                        //boxprinter_c.Visible = false;
                        boxcpu_c.Visible = false;
                        boxram_c.Visible = false;
                        boxhdd_c.Visible = false;
                        boxvga_c.Visible = false;
                        boxlevel_c.Visible = true;
                        boxslot_c.Visible = false;
                        break;
                    case "3": //Printer
                        //boxmonitor_c.Visible = false;
                        //boxprinter_c.Visible = true;
                        boxcpu_c.Visible = false;
                        boxram_c.Visible = false;
                        boxhdd_c.Visible = false;
                        boxvga_c.Visible = false;
                        boxlevel_c.Visible = false;
                        boxslot_c.Visible = false;
                        break;
                    case "4": //Moniter
                        //boxmonitor_c.Visible = true;
                        //boxprinter_c.Visible = false;
                        boxcpu_c.Visible = false;
                        boxram_c.Visible = false;
                        boxhdd_c.Visible = false;
                        boxvga_c.Visible = false;
                        boxlevel_c.Visible = false;
                        boxslot_c.Visible = false;
                        break;
                    case "6": //cpu
                        //boxmonitor_c.Visible = false;
                        //boxprinter_c.Visible = false;
                        boxcpu_c.Visible = true;
                        boxram_c.Visible = false;
                        boxhdd_c.Visible = false;
                        boxvga_c.Visible = false;
                        boxlevel_c.Visible = false;
                        boxslot_c.Visible = true;

                        break;
                    case "7": //RAM
                        //boxmonitor_c.Visible = false;
                        //boxprinter_c.Visible = false;
                        boxcpu_c.Visible = false;
                        boxram_c.Visible = true;
                        boxhdd_c.Visible = false;
                        boxvga_c.Visible = false;
                        boxlevel_c.Visible = false;
                        boxslot_c.Visible = true;
                        break;
                    case "8": //HDD
                        //boxmonitor_c.Visible = false;
                        //boxprinter_c.Visible = false;
                        boxcpu_c.Visible = false;
                        boxram_c.Visible = false;
                        boxhdd_c.Visible = true;
                        boxvga_c.Visible = false;
                        boxlevel_c.Visible = false;
                        boxslot_c.Visible = true;
                        break;
                    case "9": //VGA
                        //boxmonitor_c.Visible = false;
                        //boxprinter_c.Visible = false;
                        boxcpu_c.Visible = false;
                        boxram_c.Visible = false;
                        boxhdd_c.Visible = false;
                        boxvga_c.Visible = true;
                        boxlevel_c.Visible = false;
                        boxslot_c.Visible = true;
                        break;
                }
                break;

            case "FvViewTranferDevice_cpu":
                FormView FvViewDetailDevice_1 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                FormView FvViewTranferDevice_cpu = (FormView)FvViewDetailDevice_1.FindControl("FvViewTranferDevice_cpu");
                if (FvViewTranferDevice_cpu.CurrentMode == FormViewMode.Insert)
                {
                    Select_tranfer_status();
                }
                break;

            case "FvView_Detail_Search_cpu":
                FormView FvViewDetailDevice_2 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                FormView FvView_Detail_Search_cpu = (FormView)FvViewDetailDevice_2.FindControl("FvView_Detail_Search_cpu");
                if (FvView_Detail_Search_cpu.CurrentMode == FormViewMode.ReadOnly)
                {

                }
                break;

            case "FvViewTranferDevice_ram":
                FormView FvViewDetailDevice_3 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                FormView FvViewTranferDevice_ram = (FormView)FvViewDetailDevice_3.FindControl("FvViewTranferDevice_ram");
                if (FvViewTranferDevice_ram.CurrentMode == FormViewMode.Insert)
                {
                    Select_tranfer_status_ram();
                }
                break;

            case "FvView_Detail_Search_ram":
                FormView FvViewDetailDevice_4 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                FormView FvView_Detail_Search_ram = (FormView)FvViewDetailDevice_4.FindControl("FvView_Detail_Search_ram");
                if (FvView_Detail_Search_ram.CurrentMode == FormViewMode.ReadOnly)
                {

                }
                break;

            case "FvViewTranferDevice_hdd":
                FormView FvViewDetailDevice_5 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                FormView FvViewTranferDevice_hdd = (FormView)FvViewDetailDevice_5.FindControl("FvViewTranferDevice_hdd");
                if (FvViewTranferDevice_hdd.CurrentMode == FormViewMode.Insert)
                {
                    Select_tranfer_status_hdd();
                }
                break;

            case "FvView_Detail_Search_hdd":
                FormView FvViewDetailDevice_6 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                FormView FvView_Detail_Search_hdd = (FormView)FvViewDetailDevice_6.FindControl("FvView_Detail_Search_hdd");
                if (FvView_Detail_Search_hdd.CurrentMode == FormViewMode.ReadOnly)
                {

                }
                break;

            case "FvViewTranferDevice_vga":
                FormView FvViewDetailDevice_7 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                FormView FvViewTranferDevice_vga = (FormView)FvViewDetailDevice_7.FindControl("FvViewTranferDevice_vga");
                if (FvViewTranferDevice_vga.CurrentMode == FormViewMode.Insert)
                {
                    Select_tranfer_status_vga();
                }
                break;

            case "FvView_Detail_Search_vga":
                FormView FvViewDetailDevice_8 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                FormView FvView_Detail_Search_vga = (FormView)FvViewDetailDevice_8.FindControl("FvView_Detail_Search_vga");
                if (FvView_Detail_Search_vga.CurrentMode == FormViewMode.ReadOnly)
                {

                }
                break;

            case "FvViewTranferDevice_etc":
                FormView FvViewDetailDevice_9 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                FormView FvViewTranferDevice_etc = (FormView)FvViewDetailDevice_9.FindControl("FvViewTranferDevice_etc");
                if (FvViewTranferDevice_etc.CurrentMode == FormViewMode.Insert)
                {
                    Select_tranfer_status_etc();
                }
                break;

            case "FvView_Detail_Search_etc":
                FormView FvViewDetailDevice_10 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                FormView FvView_Detail_Search_etc = (FormView)FvViewDetailDevice_10.FindControl("FvView_Detail_Search_etc");
                if (FvView_Detail_Search_etc.CurrentMode == FormViewMode.ReadOnly)
                {

                }
                break;

            case "FvViewApprove_cutoff":
                FormView FvViewApprove_cutoff = (FormView)Viewapprovecutoff.FindControl("FvViewApprove_cutoff");
                Panel box_fileupload_cutoff = (Panel)FvViewApprove_cutoff.FindControl("Box_fileupload_cutoff");

                if (ViewState["Node_c"].ToString() == "10" && ViewState["Actor_c"].ToString() == "1") //เจ้าหน้าที่ IT
                {
                    box_fileupload_cutoff.Visible = true;
                }

                break;
        }
    }
    #endregion

    #region Switch-Munu Color
    protected void Menu_Color(int choice)
    {
        switch (choice)
        {
            case 1: //Index
                lbindex.BackColor = System.Drawing.Color.LightGray;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbaddmasterdata.BackColor = System.Drawing.Color.Transparent;
                lbapprovecutoff.BackColor = System.Drawing.Color.Transparent;
                lbapproveadddevice.BackColor = System.Drawing.Color.Transparent;
                lbapprovetransfer.BackColor = System.Drawing.Color.Transparent;
                boxindex.Visible = true;
                boxviewdetail.Visible = false;
                boxviewapprove.Visible = false;
                box_log.Visible = false;
                break;
            case 10: //Index - View
                lbindex.BackColor = System.Drawing.Color.LightGray;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbaddmasterdata.BackColor = System.Drawing.Color.Transparent;
                lbapprovecutoff.BackColor = System.Drawing.Color.Transparent;
                lbapproveadddevice.BackColor = System.Drawing.Color.Transparent;
                lbapprovetransfer.BackColor = System.Drawing.Color.Transparent;
                boxindex.Visible = false;
                boxviewdetail.Visible = true;
                boxviewapprove.Visible = false;
                box_log.Visible = true;
                FormView FvViewDetailDevice_1 = (FormView)ViewIndex.FindControl("FvViewDetailDevice");
                LinkButton btnViewdata = (LinkButton)FvViewDetailDevice.FindControl("btnViewdata");
                LinkButton btnViewadddata_show = (LinkButton)FvViewDetailDevice.FindControl("btnViewadddata_show");
                LinkButton btnvieweditdevice_show = (LinkButton)FvViewDetailDevice.FindControl("btnvieweditdevice_show");
                LinkButton btnView_software_show = (LinkButton)FvViewDetailDevice.FindControl("btnView_software_show");
                LinkButton btnView_cut_show = (LinkButton)FvViewDetailDevice.FindControl("btnView_cut_show");
                Label lbl_view_typedevice = (Label)FvViewDetailDevice.FindControl("lbl_view_typedevice");
                Label lbl_view_statusdevice = (Label)FvViewDetailDevice.FindControl("lbl_view_statusdevice");

                if (lbl_view_typedevice.Text == "1" || lbl_view_typedevice.Text == "2")
                {
                    if (lbl_view_statusdevice.Text == "3") //ตัดชำรุดทั้งเครื่อง
                    {
                        btnViewdata.Visible = false;
                        btnViewadddata_show.Visible = false;
                        btnvieweditdevice_show.Visible = false;
                        btnView_software_show.Visible = false;
                        btnView_cut_show.Visible = false;
                    }
                    else
                    {
                        if (int.Parse(ViewState["Sec_idx"].ToString()) == 80 || int.Parse(ViewState["Sec_idx"].ToString()) == 210) //สิทธิ์ผู้ใช้งาน
                        {
                            btnViewdata.Visible = true;
                            btnViewadddata_show.Visible = true;
                            btnvieweditdevice_show.Visible = true;
                            btnView_software_show.Visible = true;
                            btnView_cut_show.Visible = true;
                        }
                        else
                        {
                            btnViewdata.Visible = false;
                            btnViewadddata_show.Visible = false;
                            btnvieweditdevice_show.Visible = false;
                            btnView_software_show.Visible = false;
                            btnView_cut_show.Visible = false;
                        }
                    }

                }
                else
                {
                    if (int.Parse(ViewState["Sec_idx"].ToString()) == 80 || int.Parse(ViewState["Sec_idx"].ToString()) == 210) //สิทธิ์ผู้ใช้งาน
                    {
                        btnViewdata.Visible = true;
                        btnViewadddata_show.Visible = false;
                        btnvieweditdevice_show.Visible = false;
                        btnView_software_show.Visible = false;
                        btnView_cut_show.Visible = false;
                    }
                    else
                    {
                        btnViewadddata_show.Visible = false;
                        btnvieweditdevice_show.Visible = false;
                        btnView_software_show.Visible = false;
                        btnView_cut_show.Visible = false;
                    }
                }
                break;
            case 11: //Index - View Approve
                lbindex.BackColor = System.Drawing.Color.LightGray;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbaddmasterdata.BackColor = System.Drawing.Color.Transparent;
                lbapprovecutoff.BackColor = System.Drawing.Color.Transparent;
                lbapproveadddevice.BackColor = System.Drawing.Color.Transparent;
                lbapprovetransfer.BackColor = System.Drawing.Color.Transparent;
                boxindex.Visible = false;
                boxviewdetail.Visible = false;
                boxviewapprove.Visible = true;
                box_log.Visible = false;
                break;
            case 12: //Index - View Approve Tranfer
                lbindex.BackColor = System.Drawing.Color.LightGray;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbaddmasterdata.BackColor = System.Drawing.Color.Transparent;
                lbapprovecutoff.BackColor = System.Drawing.Color.Transparent;
                lbapproveadddevice.BackColor = System.Drawing.Color.Transparent;
                lbapprovetransfer.BackColor = System.Drawing.Color.Transparent;
                Panel box_edit_detail = (Panel)FvViewDetailDevice.FindControl("box_edit_detail");
                Panel box_view_adddevice = (Panel)FvViewDetailDevice.FindControl("box_view_adddevice");
                Panel box_edit = (Panel)FvViewDetailDevice.FindControl("box_edit");
                boxindex.Visible = false;
                boxviewdetail.Visible = true;
                box_edit_detail.Visible = false;
                box_view_adddevice.Visible = false;
                box_edit.Visible = true;
                box_log.Visible = false;
                break;
            case 13: //Index - View - Cutoff
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbaddmasterdata.BackColor = System.Drawing.Color.Transparent;
                lbapprovecutoff.BackColor = System.Drawing.Color.LightGray;
                lbapproveadddevice.BackColor = System.Drawing.Color.Transparent;
                lbapprovetransfer.BackColor = System.Drawing.Color.Transparent;
                pn_gv_cutoff.Visible = false;
                pn_fv_cutoff.Visible = true;

                //ViewState["Node_c"]
                //ViewState["Actor_c"]
                pn_fv_approve_cutoff.Visible = false;

                switch (ViewState["Node_c"].ToString())
                {
                    case "10": // It Support 10,1,4
                        if (ViewState["Actor_c"].ToString() == "1") //actor
                        {
                            if (ViewState["rdept_idx"].ToString() == "20") //dept
                            {
                                if (int.Parse(ViewState["jobgrade_level"].ToString()) >= 4 && int.Parse(ViewState["jobgrade_level"].ToString()) <= 5) // Grade
                                {
                                    pn_fv_approve_cutoff.Visible = true;
                                }
                                else
                                {
                                    pn_fv_approve_cutoff.Visible = false;
                                }
                            }
                            else
                            {
                                pn_fv_approve_cutoff.Visible = false;
                            }
                        }
                        else
                        {
                            pn_fv_approve_cutoff.Visible = false;
                        }
                        break;
                    case "11": // Head it support 11,4,4
                        if (ViewState["Actor_c"].ToString() == "4") //actor
                        {
                            if (ViewState["rdept_idx"].ToString() == "20") //dept
                            {
                                if (int.Parse(ViewState["jobgrade_level"].ToString()) >= 6 && int.Parse(ViewState["jobgrade_level"].ToString()) <= 8) // Grade
                                {
                                    pn_fv_approve_cutoff.Visible = true;
                                }
                                else
                                {
                                    pn_fv_approve_cutoff.Visible = false;
                                }
                            }
                            else
                            {
                                pn_fv_approve_cutoff.Visible = false;
                            }
                        }
                        else
                        {
                            pn_fv_approve_cutoff.Visible = false;
                        }
                        break;
                }

                if (int.Parse(ViewState["EmpIDX"].ToString()) == 173)
                {
                    pn_fv_approve_cutoff.Visible = true;
                }

                pn_rp_log_approve.Visible = true;
                break;
            case 2: //Add
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbadd.BackColor = System.Drawing.Color.LightGray;
                lbaddmasterdata.BackColor = System.Drawing.Color.Transparent;
                lbapprovecutoff.BackColor = System.Drawing.Color.Transparent;
                lbapproveadddevice.BackColor = System.Drawing.Color.Transparent;
                lbapprovetransfer.BackColor = System.Drawing.Color.Transparent;
                break;
            case 3: //Software
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbaddmasterdata.BackColor = System.Drawing.Color.Transparent;
                lbapprovecutoff.BackColor = System.Drawing.Color.Transparent;
                lbapproveadddevice.BackColor = System.Drawing.Color.Transparent;
                lbapprovetransfer.BackColor = System.Drawing.Color.Transparent;
                break;
            case 4: //Ma
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbaddmasterdata.BackColor = System.Drawing.Color.Transparent;
                lbapprovecutoff.BackColor = System.Drawing.Color.Transparent;
                lbapproveadddevice.BackColor = System.Drawing.Color.Transparent;
                lbapprovetransfer.BackColor = System.Drawing.Color.Transparent;
                break;
            case 5: //Master Data
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbaddmasterdata.BackColor = System.Drawing.Color.LightGray;
                lbapprovecutoff.BackColor = System.Drawing.Color.Transparent;
                lbapproveadddevice.BackColor = System.Drawing.Color.Transparent;
                lbapprovetransfer.BackColor = System.Drawing.Color.Transparent;
                break;
            case 6: //Cut off
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbaddmasterdata.BackColor = System.Drawing.Color.Transparent;
                lbapprovecutoff.BackColor = System.Drawing.Color.LightGray;
                lbapproveadddevice.BackColor = System.Drawing.Color.Transparent;
                lbapprovetransfer.BackColor = System.Drawing.Color.Transparent;
                break;
            case 7: //approve adddevice
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbaddmasterdata.BackColor = System.Drawing.Color.Transparent;
                lbapprovecutoff.BackColor = System.Drawing.Color.Transparent;
                lbapproveadddevice.BackColor = System.Drawing.Color.LightGray;
                lbapprovetransfer.BackColor = System.Drawing.Color.Transparent;
                FvViewApproveDevice.ChangeMode(FormViewMode.Insert);
                FvViewApproveDevice.DataBind();
                break;
            case 8: //View Detail adddevice
                FvViewApproveDevice.ChangeMode(FormViewMode.Insert);
                FvViewApproveDevice.DataBind();
                Panel Box_Approve_NewDevice = (Panel)FvViewApproveDevice.FindControl("Box_Approve_NewDevice");
                FormView FvView_Detail_NewDevice_1 = (FormView)FvViewApproveDevice.FindControl("FvView_Detail_NewDevice");
                GridView GvDetail_Approve_New = (GridView)FvViewApproveDevice.FindControl("GvDetail_Approve_New");
                Box_Approve_NewDevice.Visible = true;
                GvDetail_Approve_New.Visible = false;
                FvView_Detail_NewDevice_1.ChangeMode(FormViewMode.ReadOnly);
                FvView_Detail_NewDevice_1.DataBind();
                break;
            case 9: //approve transfer device
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbadd.BackColor = System.Drawing.Color.Transparent;
                lbaddmasterdata.BackColor = System.Drawing.Color.Transparent;
                lbapprovecutoff.BackColor = System.Drawing.Color.Transparent;
                lbapproveadddevice.BackColor = System.Drawing.Color.Transparent;
                lbapprovetransfer.BackColor = System.Drawing.Color.LightGray;
                FvViewApproveDevice_transfer.ChangeMode(FormViewMode.Insert);
                FvViewApproveDevice_transfer.DataBind();
                boxviewapprove_transfer.Visible = true;
                break;
            case 14: //View Detail adddevice
                FvViewApproveDevice_transfer.ChangeMode(FormViewMode.Insert);
                FvViewApproveDevice_transfer.DataBind();
                Panel Box_Approve_Transfer = (Panel)FvViewApproveDevice_transfer.FindControl("Box_Approve_Transfer");
                //FormView FvViewApprove_transfer_1 = (FormView)FvViewApproveDevice_transfer.FindControl("FvViewApprove_transfer");
                FormView FvViewApprove_transfer = (FormView)FvViewApproveDevice_transfer.FindControl("FvViewApprove_transfer");
                GridView GvDetail_Approve_Transfer_1 = (GridView)FvViewApproveDevice_transfer.FindControl("GvDetail_Approve_Transfer");
                boxviewapprove_transfer.Visible = true;
                Box_Approve_Transfer.Visible = true;
                GvDetail_Approve_Transfer_1.Visible = false;
                FvViewApprove_transfer.ChangeMode(FormViewMode.Insert);
                FvViewApprove_transfer.DataBind();
                break;

        }
    }
    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "btn_search_index":
                Select_Detail_Device();
                break;
            case "Cmdsearch_clear":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnIndex":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewIndex);
                break;

            case "btnapproveadddevice":
                Menu_Color(7);
                MvMaster.SetActiveView(Viewapproveadddevice);
                Select_Gridview_Approve();
                break;

            case "btnView_approve_new":
                string[] arg1_ = new string[2];
                arg1_ = e.CommandArgument.ToString().Split(';');
                int u0_didx_approve_new = int.Parse(arg1_[0]);
                int u0_relation_approve_new = int.Parse(arg1_[1]);
                Menu_Color(8);
                Select_Fv_Detail_NewDevice(u0_didx_approve_new, u0_relation_approve_new);
                break;

            case "btnapprovetransfer":
                Menu_Color(9);
                MvMaster.SetActiveView(Viewapprovetransfer);
                Select_Approve_Transfer_All();
                break;

            case "btnView_approve_transfer":
                string[] arg1_2 = new string[2];
                arg1_2 = e.CommandArgument.ToString().Split(';');
                int u0_didx_approve_transfer = int.Parse(arg1_2[0]);
                int u0_relation_approve_transfer = int.Parse(arg1_2[1]);
                Menu_Color(14);
                Select_Fv_Detail_Transfer(u0_didx_approve_transfer, u0_relation_approve_transfer);
                break;

            case "btnView_Approve_Device_transfer":

                FormView FvView_Detail_Transfer = (FormView)FvViewApproveDevice_transfer.FindControl("FvView_Detail_Transfer"); //เก่า
                Label lbl_view_u0_didx = (Label)FvView_Detail_Transfer.FindControl("lbl_view_u0_didx");
                Label lbl_u0_relation_didx_main = (Label)FvView_Detail_Transfer.FindControl("lbl_u0_relation_didx_main");

                FormView FvView_Detail_Transfer_new = (FormView)FvViewApproveDevice_transfer.FindControl("FvView_Detail_Transfer_new"); //ใหม่
                Label lbl_view_u0_didx_new = (Label)FvView_Detail_Transfer_new.FindControl("lbl_view_u0_didx_new");

                //u0_didx = อุปกรณ์ใหม่
                //didx_reference = อุปกรณ์เก่า
                //didx_refer = เครื่องหลัก

                Update_Approve_Transfer(lbl_view_u0_didx_new.Text, lbl_view_u0_didx.Text, lbl_u0_relation_didx_main.Text);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnapprovecutoff":
                Menu_Color(6);
                MvMaster.SetActiveView(Viewapprovecutoff);
                ViewState["btnapprovecutoff_u0_didx"] = cmdArg;
                Select_GvDevice_Cutoff_List(int.Parse(cmdArg));
                FvViewApprove_cutoff.ChangeMode(FormViewMode.Insert);

                break;

            case "btnadd":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewAdd);
                FvInsertDevice.ChangeMode(FormViewMode.Insert);
                FvInsertDevice.DataBind();
                break;

            case "btnaddmasterdata":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewaddMasterdata);
                FvInsertMasterDevice.ChangeMode(FormViewMode.Insert);
                FvInsertMasterDevice.DataBind();
                Select_Gridview_m0_cpu();
                Select_Gridview_m0_ram();
                Select_Gridview_m0_hdd();
                Select_Gridview_m0_vga();
                Select_Gridview_m0_printer();
                Select_Gridview_m0_moniter();
                Select_Gridview_m0_typedevice();
                Select_Gridview_m0_band();
                Select_Gridview_m0_level();
                Select_Gridview_m0_status();
                Select_Gridview_m0_insurance();
                break;

            case "btnaddmaster":
                Menu_Color(int.Parse(cmdArg));
                break;

            case "btnAdd_Device": //บันทึกข้อมูล เพิ่มทะเบียนคอม
                Insert_Detail_Device();
                Menu_Color(1);
                MvMaster.SetActiveView(ViewIndex);
                Select_Detail_Device();
                break;

            case "btnView_GvDevice":
                MvMaster.SetActiveView(ViewIndex);
                ViewState["check_approve"] = "1,2";
                FvViewDetailDevice.ChangeMode(FormViewMode.Edit);
                Select_FvDetail_Device(int.Parse(cmdArg));
                Label lbl_view_u0_didx_view = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                Label lbl_u0_software_licen_view = (Label)FvViewDetailDevice.FindControl("lbl_u0_software_licen");
                LinkButton btnViewdata = (LinkButton)FvViewDetailDevice.FindControl("btnViewdata");
                ViewState["lbl_view_u0_didx_view"] = lbl_view_u0_didx_view.Text;
                ViewState["lbl_u0_software_licen_view"] = lbl_u0_software_licen_view.Text;
                Select_ck_software();
                Select_Gv_software(int.Parse(lbl_view_u0_didx_view.Text), lbl_u0_software_licen_view.Text);

                if (int.Parse(ViewState["Sec_idx"].ToString()) == 80 || int.Parse(ViewState["Sec_idx"].ToString()) == 210)
                {
                    btnViewdata.Visible = false;
                }

                SelectLogDevice(int.Parse(cmdArg));
                Menu_Color(10);
                txtfocus.Focus();
                break;

            case "btnView_GvDevice_Cutoff":
                MvMaster.SetActiveView(Viewapprovecutoff);
                Select_FvDetail_Device_Cutoff(int.Parse(cmdArg));
                SelectLogDevice_cutoff(int.Parse(cmdArg), ViewState["code_cutoff"].ToString());
                Menu_Color(13);

                try
                {
                    string filePath = Server.MapPath(imgPath + ViewState["code_cutoff"].ToString());
                    DirectoryInfo myDir = new DirectoryInfo(filePath);
                    SearchDirectories(myDir, ViewState["code_cutoff"].ToString());
                }
                catch
                {

                }

                break;

            case "BtnBack":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnView_addDevice_show": //2_1
                Select_Set_Visible_Btn("2_1");
                Search_m0_status();
                break;

            case "btnView_addDevice_hide": //2_2
                Select_Set_Visible_Btn("2_2");
                break;

            case "btnView_editDevice_show": //3_1
                Select_Set_Visible_Btn("3_1");
                break;

            case "btnView_editDevice_hide": //3_2
                Select_Set_Visible_Btn("3_2");
                break;

            case "btnView_editDevice_software_show": //4_1
                Select_Set_Visible_Btn("4_1");
                break;

            case "btnView_editDevice_software_hide": //4_2
                Select_Set_Visible_Btn("4_2");
                break;

            case "btnView_editDevice_cut_show": //5_1
                Select_Set_Visible_Btn("5_1");
                break;

            case "btnView_editDevice_cut_hide": //5_2
                Select_Set_Visible_Btn("5_2");
                break;

            case "btnsearchtranfer":
                DropDownList ddl_typedevice = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_search_typedevice");
                DropDownList ddl_statusdevice = (DropDownList)FvViewDetailDevice.FindControl("ddl_view_search_statusdevice");
                TextBox txt_u0_code = (TextBox)FvViewDetailDevice.FindControl("txt_view_search_u0_code");
                TextBox txt_assetcode = (TextBox)FvViewDetailDevice.FindControl("txt_view_search_assetcode");
                TextBox txt_seria = (TextBox)FvViewDetailDevice.FindControl("txt_view_search_seria");

                if (txt_assetcode.Text == "")
                {
                    txt_assetcode.Text = "0";
                }
                if (txt_seria.Text == "")
                {
                    txt_seria.Text = "0";
                }
                ViewState["Search_view_typedevice"] = ddl_typedevice.SelectedValue;
                ViewState["Search_view_statusdevice"] = ddl_statusdevice.SelectedValue;
                ViewState["Search_view_u0_code"] = txt_u0_code.Text;
                ViewState["Search_view_assetcode"] = txt_assetcode.Text;
                ViewState["Search_view_seria"] = txt_seria.Text;
                Select_Searchtranfer_u0_device();
                break;

            case "btnselect":
                GridView GvEmpTemp = (GridView)FvViewDetailDevice.FindControl("GvEmpTemp");
                string[] arg_detail = new string[6];
                arg_detail = e.CommandArgument.ToString().Split(';');
                int u0_didx = int.Parse(arg_detail[0]);
                string u0_code = arg_detail[1];
                string name_m0_typedevice = arg_detail[2];
                int m0_tdidx = int.Parse(arg_detail[3]);
                //int u0_acc = int.Parse(arg_detail[4]);
                //int u0_serial = int.Parse(arg_detail[5]);

                var dsDevice = (DataSet)ViewState["vsTempDevicw"];

                var drDevice = dsDevice.Tables[0].NewRow();
                drDevice["u0_didx"] = u0_didx;
                drDevice["u0_code"] = u0_code;
                drDevice["m0_tdidx"] = m0_tdidx;
                drDevice["name_m0_typedevice"] = name_m0_typedevice;

                dsDevice.Tables[0].Rows.Add(drDevice);
                ViewState["vsTempDevicw"] = dsDevice;
                GvEmpTemp.DataSource = dsDevice.Tables[0];
                GvEmpTemp.DataBind();
                break;

            case "btnsearch_software":
                DropDownList ddl_type_software = (DropDownList)FvViewDetailDevice.FindControl("ddl_type_software");
                DropDownList ddl_org_software = (DropDownList)FvViewDetailDevice.FindControl("ddl_org_software");
                TextBox txt_softwarecode = (TextBox)FvViewDetailDevice.FindControl("txt_softwarecode");

                ViewState["Search_typesoftware"] = ddl_type_software.SelectedValue;
                ViewState["Search_orgsoftware"] = ddl_org_software.SelectedValue;
                ViewState["Search_softwarecode"] = txt_softwarecode.Text;
                Select_Searchsoftware_u0_device();
                break;

            case "btnselect_software":
                GridView GvTemp_sotfware = (GridView)FvViewDetailDevice.FindControl("GvTemp_sotfware");
                string[] arg_sw = new string[3];
                arg_sw = e.CommandArgument.ToString().Split(';');
                int software_idx = int.Parse(arg_sw[0]);
                string name_software = arg_sw[1];
                string type_name = arg_sw[2];
                var dsDevice_sw = (DataSet)ViewState["vsTemp_sw"];

                var drDevice_sw = dsDevice_sw.Tables[0].NewRow();
                drDevice_sw["software_idx"] = software_idx;
                drDevice_sw["name_software"] = name_software;
                drDevice_sw["type_name"] = type_name;

                dsDevice_sw.Tables[0].Rows.Add(drDevice_sw);
                ViewState["vsTemp_sw"] = dsDevice_sw;
                GvTemp_sotfware.DataSource = dsDevice_sw.Tables[0];
                GvTemp_sotfware.DataBind();
                break;

            case "CmdDel":
                string u0_didx_1 = cmdArg;
                GridView GvEmpTemp_1 = (GridView)FvViewDetailDevice.FindControl("GvEmpTemp");
                var dsDevice_1 = (DataSet)ViewState["vsTempDevicw"];
                ViewState["u0_didx"] = u0_didx_1;

                for (int counter = 0; counter < dsDevice_1.Tables[0].Rows.Count; counter++)
                {
                    if (dsDevice_1.Tables[0].Rows[counter]["u0_didx"].ToString() == ViewState["u0_didx"].ToString())
                    {
                        dsDevice_1.Tables[0].Rows[counter].Delete();
                        break;
                    }
                }

                GvEmpTemp_1.DataSource = (DataSet)ViewState["vsTempDevicw"];
                GvEmpTemp_1.DataBind();
                break;

            case "CmdDel_sw":
                string software_idx_1 = cmdArg;
                GridView GvTemp_sotfware_1 = (GridView)FvViewDetailDevice.FindControl("GvTemp_sotfware");
                var dsDevice_sw_1 = (DataSet)ViewState["vsTemp_sw"];
                ViewState["software_idx"] = software_idx_1;

                for (int counter = 0; counter < dsDevice_sw_1.Tables[0].Rows.Count; counter++)
                {
                    if (dsDevice_sw_1.Tables[0].Rows[counter]["software_idx"].ToString() == ViewState["software_idx"].ToString())
                    {
                        dsDevice_sw_1.Tables[0].Rows[counter].Delete();
                        break;
                    }
                }

                GvTemp_sotfware_1.DataSource = (DataSet)ViewState["vsTemp_sw"];
                GvTemp_sotfware_1.DataBind();
                break;

            case "btninsert_masterdata":
                DropDownList ddl_addmaster_typedevice = (DropDownList)FvInsertMasterDevice.FindControl("ddl_addmaster_typedevice"); //m0_tdidx
                Insert_Detail_Master_Device(int.Parse(ddl_addmaster_typedevice.SelectedValue));
                //FvInsertMasterDevice.ChangeMode(FormViewMode.Insert);
                //FvInsertMasterDevice.DataBind();
                break;

            case "btnView_Device":
                ViewState["Check_Action"] = 1;
                Insert_View_Detail_Device();
                txtfocus.Focus();
                break;

            case "btnSave_Cutoff": //ตัดชำรุด
                ViewState["Check_Action"] = 2;
                Insert_View_Detail_Device();
                //Insert_View_Detail_Device_Cutoff();
                txtfocus.Focus();
                break;

            case "btn_SelectTranfer_device":
                string u0_didx_2 = cmdArg;
                FormView FvViewTranferDevice_cpu = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_cpu");
                FormView _FvView_Detail_cpu_1 = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_cpu");
                FormView _FvViewTranferDevice_cpu_1 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_cpu");
                FvViewTranferDevice_cpu.ChangeMode(FormViewMode.Insert);
                FvViewTranferDevice_cpu.DataBind();
                Select_FvDetail_Cpu(int.Parse(u0_didx_2));
                _FvView_Detail_cpu_1.Visible = true;
                _FvViewTranferDevice_cpu_1.Visible = true;
                ViewState["u0_didx_cpu"] = u0_didx_2;
                break;

            case "btnsearchtranfer_cpu":
                FormView FvViewTranferDevice_cpu_1 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_cpu");
                DropDownList ddl_Edit_search_statusdevice = (DropDownList)FvViewTranferDevice_cpu_1.FindControl("ddl_Edit_search_statusdevice");
                TextBox txt_edit_search_u0_code = (TextBox)FvViewTranferDevice_cpu_1.FindControl("txt_edit_search_u0_code");
                TextBox txt_edit_search_assetcode = (TextBox)FvViewTranferDevice_cpu_1.FindControl("txt_edit_search_assetcode");
                TextBox txt_edit_search_seria = (TextBox)FvViewTranferDevice_cpu_1.FindControl("txt_edit_search_seria");
                GridView GvDevice_Detail_Search_cpu_hide = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_cpu");
                GvDevice_Detail_Search_cpu_hide.Visible = true;
                if (txt_edit_search_assetcode.Text == "")
                {
                    txt_edit_search_assetcode.Text = "0";
                }
                if (txt_edit_search_seria.Text == "")
                {
                    txt_edit_search_seria.Text = "0";
                }
                ViewState["Search_view_statusdevice_cpu"] = ddl_Edit_search_statusdevice.SelectedValue;
                ViewState["Search_view_u0_code_cpu"] = txt_edit_search_u0_code.Text;
                ViewState["Search_view_assetcode_cpu"] = txt_edit_search_assetcode.Text;
                ViewState["Search_view_seria_cpu"] = txt_edit_search_seria.Text;
                Select_Searchtranfer_u0_device_cpu();
                break;

            case "btn_SelectTranfer_ram":
                string u0_didx_3 = cmdArg;
                FormView FvViewTranferDevice_ram = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_ram");
                FormView _FvView_Detail_ram_1 = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_ram");
                FormView _FvViewTranferDevice_ram_1 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_ram");
                FvViewTranferDevice_ram.ChangeMode(FormViewMode.Insert);
                FvViewTranferDevice_ram.DataBind();
                //Select_FvDetail_Ram(int.Parse(u0_didx_3));
                _FvView_Detail_ram_1.Visible = true;
                _FvViewTranferDevice_ram_1.Visible = true;
                ViewState["u0_didx_ram"] = u0_didx_3;
                break;

            case "btnsearchtranfer_ram":
                FormView FvViewTranferDevice_ram_1 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_ram");
                DropDownList ddl_Edit_search_statusdevice_ram = (DropDownList)FvViewTranferDevice_ram_1.FindControl("ddl_Edit_search_statusdevice_ram");
                TextBox txt_edit_search_u0_code_ram = (TextBox)FvViewTranferDevice_ram_1.FindControl("txt_edit_search_u0_code_ram");
                TextBox txt_edit_search_assetcode_ram = (TextBox)FvViewTranferDevice_ram_1.FindControl("txt_edit_search_assetcode_ram");
                TextBox txt_edit_search_seria_ram = (TextBox)FvViewTranferDevice_ram_1.FindControl("txt_edit_search_seria_ram");
                GridView GvDevice_Detail_Search_ram_hide = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_ram");
                GvDevice_Detail_Search_ram_hide.Visible = true;
                if (txt_edit_search_assetcode_ram.Text == "")
                {
                    txt_edit_search_assetcode_ram.Text = "0";
                }
                if (txt_edit_search_seria_ram.Text == "")
                {
                    txt_edit_search_seria_ram.Text = "0";
                }
                ViewState["Search_view_statusdevice_ram"] = ddl_Edit_search_statusdevice_ram.SelectedValue;
                ViewState["Search_view_u0_code_ram"] = txt_edit_search_u0_code_ram.Text;
                ViewState["Search_view_assetcode_ram"] = txt_edit_search_assetcode_ram.Text;
                ViewState["Search_view_seria_ram"] = txt_edit_search_seria_ram.Text;
                Select_Searchtranfer_u0_device_ram();

                break;

            case "btn_SelectTranfer_hdd":
                string u0_didx_4 = cmdArg;
                FormView FvViewTranferDevice_hdd = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_hdd");
                FormView _FvView_Detail_hdd_1 = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_hdd");
                FormView _FvViewTranferDevice_hdd_1 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_hdd");
                FvViewTranferDevice_hdd.ChangeMode(FormViewMode.Insert);
                FvViewTranferDevice_hdd.DataBind();
                //Select_FvDetail_Hdd(int.Parse(u0_didx_4));
                _FvView_Detail_hdd_1.Visible = true;
                _FvViewTranferDevice_hdd_1.Visible = true;
                ViewState["u0_didx_hdd"] = u0_didx_4;
                break;

            case "btnsearchtranfer_hdd":
                FormView FvViewTranferDevice_hdd_1 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_hdd");
                DropDownList ddl_Edit_search_statusdevice_hdd = (DropDownList)FvViewTranferDevice_hdd_1.FindControl("ddl_Edit_search_statusdevice_hdd");
                TextBox txt_edit_search_u0_code_hdd = (TextBox)FvViewTranferDevice_hdd_1.FindControl("txt_edit_search_u0_code_hdd");
                TextBox txt_edit_search_assetcode_hdd = (TextBox)FvViewTranferDevice_hdd_1.FindControl("txt_edit_search_assetcode_hdd");
                TextBox txt_edit_search_seria_hdd = (TextBox)FvViewTranferDevice_hdd_1.FindControl("txt_edit_search_seria_hdd");
                GridView GvDevice_Detail_Search_hdd_hide = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_hdd");
                GvDevice_Detail_Search_hdd_hide.Visible = true;
                if (txt_edit_search_assetcode_hdd.Text == "")
                {
                    txt_edit_search_assetcode_hdd.Text = "0";
                }
                if (txt_edit_search_seria_hdd.Text == "")
                {
                    txt_edit_search_seria_hdd.Text = "0";
                }
                ViewState["Search_view_statusdevice_hdd"] = ddl_Edit_search_statusdevice_hdd.SelectedValue;
                ViewState["Search_view_u0_code_hdd"] = txt_edit_search_u0_code_hdd.Text;
                ViewState["Search_view_assetcode_hdd"] = txt_edit_search_assetcode_hdd.Text;
                ViewState["Search_view_seria_hdd"] = txt_edit_search_seria_hdd.Text;
                Select_Searchtranfer_u0_device_hdd();
                break;

            case "btn_SelectTranfer_vga":
                string u0_didx_7 = cmdArg;
                FormView FvViewTranferDevice_vga = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_vga");
                FormView _FvView_Detail_vga_1 = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_vga");
                FormView _FvViewTranferDevice_vga_1 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_vga");
                FvViewTranferDevice_vga.ChangeMode(FormViewMode.Insert);
                FvViewTranferDevice_vga.DataBind();
                //Select_FvDetail_Vga(int.Parse(u0_didx_7));
                _FvView_Detail_vga_1.Visible = true;
                _FvViewTranferDevice_vga_1.Visible = true;
                ViewState["u0_didx_vga"] = u0_didx_7;
                break;

            case "btnsearchtranfer_vga":
                FormView FvViewTranferDevice_vga_1 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_vga");
                DropDownList ddl_Edit_search_statusdevice_vga = (DropDownList)FvViewTranferDevice_vga_1.FindControl("ddl_Edit_search_statusdevice_vga");
                TextBox txt_edit_search_u0_code_vga = (TextBox)FvViewTranferDevice_vga_1.FindControl("txt_edit_search_u0_code_vga");
                TextBox txt_edit_search_assetcode_vga = (TextBox)FvViewTranferDevice_vga_1.FindControl("txt_edit_search_assetcode_vga");
                TextBox txt_edit_search_seria_vga = (TextBox)FvViewTranferDevice_vga_1.FindControl("txt_edit_search_seria_vga");
                GridView GvDevice_Detail_Search_vga_hide = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_vga");
                GvDevice_Detail_Search_vga_hide.Visible = true;
                if (txt_edit_search_assetcode_vga.Text == "")
                {
                    txt_edit_search_assetcode_vga.Text = "0";
                }
                if (txt_edit_search_seria_vga.Text == "")
                {
                    txt_edit_search_seria_vga.Text = "0";
                }
                ViewState["Search_view_statusdevice_vga"] = ddl_Edit_search_statusdevice_vga.SelectedValue;
                ViewState["Search_view_u0_code_vga"] = txt_edit_search_u0_code_vga.Text;
                ViewState["Search_view_assetcode_vga"] = txt_edit_search_assetcode_vga.Text;
                ViewState["Search_view_seria_vga"] = txt_edit_search_seria_vga.Text;
                //FvViewTranferDevice_vga_1.ChangeMode(FormViewMode.ReadOnly);
                Select_Searchtranfer_u0_device_vga();
                break;

            case "btn_SelectTranfer_etc":
                string u0_didx_12 = cmdArg;
                FormView FvViewTranferDevice_etc = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_etc");
                FormView _FvView_Detail_etc_1 = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_etc");
                FormView _FvViewTranferDevice_etc_1 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_etc");
                FvViewTranferDevice_etc.ChangeMode(FormViewMode.Insert);
                FvViewTranferDevice_etc.DataBind();
                //Select_FvDetail_etc(int.Parse(u0_didx_12));
                _FvView_Detail_etc_1.Visible = true;
                _FvViewTranferDevice_etc_1.Visible = true;
                ViewState["u0_didx_etc"] = u0_didx_12;
                break;

            case "btnsearchtranfer_etc":
                FormView FvViewTranferDevice_etc_1 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_etc");
                DropDownList ddl_Edit_search_statusdevice_etc = (DropDownList)FvViewTranferDevice_etc_1.FindControl("ddl_Edit_search_statusdevice_etc");
                TextBox txt_edit_search_u0_code_etc = (TextBox)FvViewTranferDevice_etc_1.FindControl("txt_edit_search_u0_code_etc");
                TextBox txt_edit_search_assetcode_etc = (TextBox)FvViewTranferDevice_etc_1.FindControl("txt_edit_search_assetcode_etc");
                TextBox txt_edit_search_seria_etc = (TextBox)FvViewTranferDevice_etc_1.FindControl("txt_edit_search_seria_etc");
                GridView GvDevice_Detail_Search_etc_hide = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_etc");
                GvDevice_Detail_Search_etc_hide.Visible = true;
                if (txt_edit_search_assetcode_etc.Text == "")
                {
                    txt_edit_search_assetcode_etc.Text = "0";
                }
                if (txt_edit_search_seria_etc.Text == "")
                {
                    txt_edit_search_seria_etc.Text = "0";
                }
                ViewState["Search_view_statusdevice_etc"] = ddl_Edit_search_statusdevice_etc.SelectedValue;
                ViewState["Search_view_u0_code_etc"] = txt_edit_search_u0_code_etc.Text;
                ViewState["Search_view_assetcode_etc"] = txt_edit_search_assetcode_etc.Text;
                ViewState["Search_view_seria_etc"] = txt_edit_search_seria_etc.Text;
                //FvViewTranferDevice_vga_1.ChangeMode(FormViewMode.ReadOnly);
                Select_Searchtranfer_u0_device_etc();
                break;

            case "btnView_Approve_Device":

                FormView FvView_Detail_NewDevice_new = (FormView)FvViewApproveDevice.FindControl("FvView_Detail_NewDevice_new");
                Label u0_didx_Main = (Label)FvView_Detail_NewDevice_new.FindControl("lbl_view_u0_didx");
                Label u0_didx_new = (Label)FvView_Detail_NewDevice_new.FindControl("lbl_view_relation_didx_new");

                Update_Approve_Detail_Device(int.Parse(u0_didx_Main.Text), int.Parse(u0_didx_new.Text));
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "Cancel_cpu":
                FormView FvView_Detail_cpu = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_cpu");
                FormView FvViewTranferDevice_cpu_2 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_cpu");
                GridView GvDevice_Detail_Search_cpu_hide1 = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_cpu");
                GvDevice_Detail_Search_cpu_hide1.Visible = false;
                FvViewTranferDevice_cpu_2.Visible = false;
                FvView_Detail_cpu.Visible = false;
                break;
            case "Cancel_ram":
                FormView FvView_Detail_ram = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_ram");
                FormView FvViewTranferDevice_ram_2 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_ram");
                GridView GvDevice_Detail_Search_ram_hide1 = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_ram");
                GvDevice_Detail_Search_ram_hide1.Visible = false;
                FvViewTranferDevice_ram_2.Visible = false;
                FvView_Detail_ram.Visible = false;
                break;
            case "Cancel_hdd":
                FormView FvView_Detail_hdd = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_hdd");
                FormView FvViewTranferDevice_hdd_2 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_hdd");
                GridView GvDevice_Detail_Search_hdd_hide1 = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_hdd");
                GvDevice_Detail_Search_hdd_hide1.Visible = false;
                FvViewTranferDevice_hdd_2.Visible = false;
                FvView_Detail_hdd.Visible = false;
                break;
            case "Cancel_vga":
                FormView FvView_Detail_vga = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_vga");
                FormView FvViewTranferDevice_vga_2 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_vga");
                GridView GvDevice_Detail_Search_vga_hide1 = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_vga");
                GvDevice_Detail_Search_vga_hide1.Visible = false;
                FvViewTranferDevice_vga_2.Visible = false;
                FvView_Detail_vga.Visible = false;
                break;
            case "Cancel_etc":
                FormView FvView_Detail_etc = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_etc");
                FormView FvViewTranferDevice_etc_2 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_etc");
                GridView GvDevice_Detail_Search_etc_hide1 = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_etc");
                GvDevice_Detail_Search_etc_hide1.Visible = false;
                FvViewTranferDevice_etc_2.Visible = false;
                FvView_Detail_etc.Visible = false;
                break;

            case "btnsave_tranfer_cpu":
                string u0_didx_10 = cmdArg;
                GridView GvDevice_Detail_Search_cpu = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_cpu");
                Label lbl_didx_tranfer_cpu = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                FormView _FvView_Detail_cpu = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_cpu");
                FormView _FvViewTranferDevice_cpu_2 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_cpu");
                GridView _GvDevice_Detail_Search_cpu_hide1 = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_cpu");

                Update_tranfer_newdevice(int.Parse(u0_didx_10), int.Parse(ViewState["u0_didx_cpu"].ToString()), int.Parse(lbl_didx_tranfer_cpu.Text));
                _GvDevice_Detail_Search_cpu_hide1.Visible = false;
                _FvViewTranferDevice_cpu_2.Visible = false;
                _FvView_Detail_cpu.Visible = false;
                Select_edit_u0_device_code_cpu(int.Parse(lbl_didx_tranfer_cpu.Text));
                break;
            case "btnsave_tranfer_ram":
                string u0_didx_11 = cmdArg;
                GridView GvDevice_Detail_Search_ram = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_ram");
                Label lbl_didx_tranfer_ram = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                FormView _FvView_Detail_ram = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_ram");
                FormView _FvViewTranferDevice_ram_2 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_ram");
                GridView _GvDevice_Detail_Search_ram_hide1 = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_ram");

                Update_tranfer_newdevice(int.Parse(u0_didx_11), int.Parse(ViewState["u0_didx_ram"].ToString()), int.Parse(lbl_didx_tranfer_ram.Text));
                _GvDevice_Detail_Search_ram_hide1.Visible = false;
                _FvViewTranferDevice_ram_2.Visible = false;
                _FvView_Detail_ram.Visible = false;
                Select_edit_u0_device_code_ram(int.Parse(lbl_didx_tranfer_ram.Text));
                break;
            case "btnsave_tranfer_hdd":
                string u0_didx_14 = cmdArg;
                GridView GvDevice_Detail_Search_hdd = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_hdd");
                Label lbl_didx_tranfer_hdd = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                FormView _FvView_Detail_hdd = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_hdd");
                FormView _FvViewTranferDevice_hdd_2 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_hdd");
                GridView _GvDevice_Detail_Search_hdd_hide1 = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_hdd");

                Update_tranfer_newdevice(int.Parse(u0_didx_14), int.Parse(ViewState["u0_didx_hdd"].ToString()), int.Parse(lbl_didx_tranfer_hdd.Text));
                _GvDevice_Detail_Search_hdd_hide1.Visible = false;
                _FvViewTranferDevice_hdd_2.Visible = false;
                _FvView_Detail_hdd.Visible = false;
                Select_edit_u0_device_code_hdd(int.Parse(lbl_didx_tranfer_hdd.Text));
                break;
            case "btnsave_tranfer_vga":
                string u0_didx_13 = cmdArg;
                GridView GvDevice_Detail_Search_vga = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_vga");
                Label lbl_didx_tranfer_vga = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                FormView _FvView_Detail_vga = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_vga");
                FormView _FvViewTranferDevice_vga_2 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_vga");
                GridView _GvDevice_Detail_Search_vga_hide1 = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_vga");

                Update_tranfer_newdevice(int.Parse(u0_didx_13), int.Parse(ViewState["u0_didx_vga"].ToString()), int.Parse(lbl_didx_tranfer_vga.Text));
                _GvDevice_Detail_Search_vga_hide1.Visible = false;
                _FvViewTranferDevice_vga_2.Visible = false;
                _FvView_Detail_vga.Visible = false;
                Select_edit_u0_device_code_vga(int.Parse(lbl_didx_tranfer_vga.Text));
                break;
            case "btnsave_tranfer_etc":
                string u0_didx_15 = cmdArg;
                GridView GvDevice_Detail_Search_etc = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_etc");
                Label lbl_didx_tranfer_etc = (Label)FvViewDetailDevice.FindControl("lbl_view_u0_didx");
                FormView _FvView_Detail_etc = (FormView)FvViewDetailDevice.FindControl("FvView_Detail_etc");
                FormView _FvViewTranferDevice_etc_2 = (FormView)FvViewDetailDevice.FindControl("FvViewTranferDevice_etc");
                GridView _GvDevice_Detail_Search_etc_hide1 = (GridView)FvViewDetailDevice.FindControl("GvDevice_Detail_Search_etc");

                Update_tranfer_newdevice(int.Parse(u0_didx_15), int.Parse(ViewState["u0_didx_etc"].ToString()), int.Parse(lbl_didx_tranfer_etc.Text));
                _GvDevice_Detail_Search_etc_hide1.Visible = false;
                _FvViewTranferDevice_etc_2.Visible = false;
                _FvView_Detail_etc.Visible = false;
                Select_edit_u0_device_code_etc(int.Parse(lbl_didx_tranfer_etc.Text));
                break;
            case "btn_Approve_cutoff":
                Label lbl_view_u0_didx_c = (Label)FvViewDetailDevice_Cutoff.FindControl("lbl_view_u0_didx_c");
                Label lbl_view_typedevice_c = (Label)FvViewDetailDevice_Cutoff.FindControl("lbl_view_typedevice_c");

                string temp_typedevice = "";

                switch (int.Parse(lbl_view_typedevice_c.Text))
                {
                    case 1:
                    case 2:
                        temp_typedevice = "6"; //ทั้งเครื่อง
                        break;
                    default:
                        temp_typedevice = "7"; //เฉพาะเครื่อง
                        break;
                }
                Update_Approve_Cutoff_One(int.Parse(temp_typedevice), int.Parse(lbl_view_u0_didx_c.Text));
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;
            case "cmd_guidebook":
                Response.Write("<script>window.open('https://docs.google.com/a/taokaenoi.co.th/document/d/1r3PJUUMSPnJKz853AAG18ulFOA3ijoQCJlZA--UQHKs/edit?usp=sharing','_blank');</script>");
                break;
            case "cmd_flow":
                Response.Write("<script>window.open('http://mas.taokaenoi.co.th/images/device/DeviceFlow.jpg','_blank');</script>");
                break;
            case "CmdDel_master_cpu":
                ViewState["master_cpu_idx"] = cmdArg;
                Delete_m0_cpu();
                Select_Gridview_m0_cpu();
                break;
            case "CmdDel_master_ram":
                ViewState["master_ram_idx"] = cmdArg;
                Delete_m0_ram();
                Select_Gridview_m0_ram();
                break;
            case "CmdDel_master_hdd":
                ViewState["master_hdd_idx"] = cmdArg;
                Delete_m0_hdd();
                Select_Gridview_m0_hdd();
                break;
            case "CmdDel_master_vga":
                ViewState["master_vga_idx"] = cmdArg;
                Delete_m0_vga();
                Select_Gridview_m0_vga();
                break;
            case "CmdDel_master_printer":
                ViewState["master_printer_idx"] = cmdArg;
                Delete_m0_printer();
                Select_Gridview_m0_printer();
                break;
            case "CmdDel_master_moniter":
                ViewState["master_moniter_idx"] = cmdArg;
                Delete_m0_moniter();
                Select_Gridview_m0_moniter();
                break;
            case "CmdDel_master_insurance":
                ViewState["master_insurance_idx"] = cmdArg;
                Delete_m0_insurance();
                Select_Gridview_m0_insurance();
                break;
            case "CmdDel_master_band":
                ViewState["master_band_idx"] = cmdArg;
                Delete_m0_band();
                Select_Gridview_m0_band();
                break;
        }
    }
    #endregion

    //start teppanop //
    private void create_asset()
    {
        ViewState["asset_u0idx"] = Session["_sesion_u0idx"];
        ViewState["asset_u2idx"] = Session["_sesion_u2idx"];
        ViewState["asset_u0_docket_idx"] = Session["_sesion_u0_docket_idx"];
        ViewState["asset_asset_no"] = Session["_sesion_asset_no"];
        ViewState["asset_pono"] = Session["_sesion_pono"];
        ViewState["asset_tdidx"] = Session["_sesion_tdidx"];

        ViewState["asset_org_idx_its"] = Session["_sesion_org_idx_its"];
        ViewState["asset_rdept_idx_its"] = Session["_sesion_rdept_idx_its"];
        ViewState["asset_rsec_idx_its"] = Session["_sesion_rsec_idx_its"];

        Session.Remove("_sesion_u0idx");
        Session.Remove("_sesion_u2idx");
        Session.Remove("_sesion_u0_docket_idx");
        Session.Remove("_sesion_pono");
        Session.Remove("_sesion_tdidx");
        Session.Remove("_sesion_devices");
        Session.Remove("_sesion_org_idx_its");
        Session.Remove("_sesion_rdept_idx_its");
        Session.Remove("_sesion_rsec_idx_its");

        string cmdArg = "2";
        Menu_Color(int.Parse(cmdArg));
        MvMaster.SetActiveView(ViewAdd);
        FvInsertDevice.ChangeMode(FormViewMode.Insert);
        FvInsertDevice.DataBind();

        TextBox txt_add_acc = (TextBox)FvInsertDevice.FindControl("txt_add_acc");
        TextBox txt_add_po = (TextBox)FvInsertDevice.FindControl("txt_add_po");
        DropDownList ddl_add_typedevice = (DropDownList)FvInsertDevice.FindControl("ddl_add_typedevice");
        DropDownList ddl_add_org = (DropDownList)FvInsertDevice.FindControl("ddl_add_org");
        DropDownList ddl_add_dept = (DropDownList)FvInsertDevice.FindControl("ddl_add_dept");
        DropDownList ddl_add_sec = (DropDownList)FvInsertDevice.FindControl("ddl_add_sec");

        txt_add_acc.Text = ViewState["asset_asset_no"].ToString();
        txt_add_po.Text = ViewState["asset_pono"].ToString();
        ddl_add_typedevice.SelectedValue = ViewState["asset_tdidx"].ToString();
        ddl_add_org.SelectedValue = ViewState["asset_org_idx_its"].ToString();
        getDepartmentList(ddl_add_dept, int.Parse(ddl_add_org.SelectedItem.Value));
        ddl_add_dept.SelectedValue = ViewState["asset_rdept_idx_its"].ToString();
        getSectionList(ddl_add_sec, int.Parse(ddl_add_org.SelectedItem.Value), int.Parse(ddl_add_dept.SelectedItem.Value));
        ddl_add_sec.SelectedValue = ViewState["asset_rsec_idx_its"].ToString();

    }
    //end teppanop //

}