﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

#region data emps
[Serializable]
[XmlRoot("data_emps")]
public class data_emps
{
   [XmlElement("return_code")]
   public int return_code { get; set; }
   [XmlElement("return_msg")]
   public string return_msg { get; set; }
   [XmlElement("return_empidx")]
   public string return_empidx { get; set; }
   [XmlElement("emps_empshift_action")]
   public empshift[] emps_empshift_action { get; set; }
   [XmlElement("emps_parttime_action")]
   public parttime[] emps_parttime_action { get; set; }
   [XmlElement("emps_reward_action")]
   public rewards[] emps_reward_action { get; set; }
   [XmlElement("emps_group_diary_action")]
   public group_diary[] emps_group_diary_action { get; set; }
   [XmlElement("emps_odspe_action")]
   public odspe[] emps_odspe_action { get; set; }
   [XmlElement("emps_announce_action")]
   public announce[] emps_announce_action { get; set; }
   [XmlElement("emps_change_announce_action")]
   public change_announce[] emps_change_announce_action { get; set; }
   [XmlElement("emps_approval_log_action")]
   public approval_log[] emps_approval_log_action { get; set; }
   [XmlElement("emps_employee_data_action")]
   public empsemployee[] emps_employee_data_action { get; set; }
   [XmlElement("emps_export_action")]
   public export[] emps_export_action { get; set; }
   [XmlElement("emps_announce_diary_action")]
   public announce_diary[] emps_announce_diary_action { get; set; }
   [XmlElement("emps_change_announce_diary_action")]
   public change_announce_diary[] emps_change_announce_diary_action { get; set; }
    [XmlElement("m1_emps_group_diary_action")]
    public m1_group_diary[] m1_emps_group_diary_action { get; set; }
}
#endregion data emps

#region empshift
[Serializable]
public class empshift
{
   [XmlElement("u0_empshift_idx")]
   public int u0_empshift_idx { get; set; }
   [XmlElement("m0_parttime_idx_ref")]
   public int m0_parttime_idx_ref { get; set; }
   [XmlElement("emp_idx_ref")]
   public int emp_idx_ref { get; set; }
   [XmlElement("empshift_status")]
   public int empshift_status { get; set; }
   [XmlElement("empshift_created_at")]
   public string empshift_created_at { get; set; }
   [XmlElement("empshift_created_by")]
   public int empshift_created_by { get; set; }
   [XmlElement("empshift_updated_at")]
   public string empshift_updated_at { get; set; }

   [XmlElement("m0_parttime_idx")]
   public int m0_parttime_idx { get; set; }
   [XmlElement("m0_reward_idx_ref")]
   public int m0_reward_idx_ref { get; set; }
   [XmlElement("parttime_code")]
   public string parttime_code { get; set; }
   [XmlElement("parttime_name_th")]
   public string parttime_name_th { get; set; }
   [XmlElement("parttime_name_eng")]
   public string parttime_name_eng { get; set; }
   [XmlElement("parttime_note")]
   public string parttime_note { get; set; }
   [XmlElement("parttime_bg_color")]
   public string parttime_bg_color { get; set; }
   [XmlElement("parttime_name_and_color")]
   public string parttime_name_and_color { get; set; }
   [XmlElement("parttime_start_datetime")]
   public string parttime_start_datetime { get; set; }
   [XmlElement("parttime_end_datetime")]
   public string parttime_end_datetime { get; set; }
   [XmlElement("parttime_btw_start_datetime")]
   public string parttime_btw_start_datetime { get; set; }
   [XmlElement("parttime_btw_end_datetime")]
   public string parttime_btw_end_datetime { get; set; }
   [XmlElement("parttime_break_start_datetime")]
   public string parttime_break_start_datetime { get; set; }
   [XmlElement("parttime_break_end_datetime")]
   public string parttime_break_end_datetime { get; set; }
   [XmlElement("parttime_amount_scan_card")]
   public int parttime_amount_scan_card { get; set; }
   [XmlElement("parttime_time_stand")]
   public int parttime_time_stand { get; set; }
   [XmlElement("parttime_amount_work_hours")]
   public float parttime_amount_work_hours { get; set; }
   [XmlElement("parttime_amount_break_hours")]
   public float parttime_amount_break_hours { get; set; }
   [XmlElement("parttime_status")]
   public int parttime_status { get; set; }
   [XmlElement("parttime_created_at")]
   public string parttime_created_at { get; set; }
   [XmlElement("parttime_created_by")]
   public int parttime_created_by { get; set; }
   [XmlElement("parttime_updated_at")]
   public string parttime_updated_at { get; set; }

   /** Start convert Date Time to Show */
   [XmlElement("reward_name")]
   public string reward_name { get; set; }
   [XmlElement("pt_start_date_only")]
   public string pt_start_date_only { get; set; }
   [XmlElement("pt_end_date_only")]
   public string pt_end_date_only { get; set; }
   [XmlElement("pt_start_date")]
   public string pt_start_date { get; set; }
   [XmlElement("pt_start_time")]
   public string pt_start_time { get; set; }
   [XmlElement("pt_end_date")]
   public string pt_end_date { get; set; }
   [XmlElement("pt_end_time")]
   public string pt_end_time { get; set; }
   [XmlElement("pt_btw_start_date")]
   public string pt_btw_start_date { get; set; }
   [XmlElement("pt_btw_start_time")]
   public string pt_btw_start_time { get; set; }
   [XmlElement("pt_btw_end_date")]
   public string pt_btw_end_date { get; set; }
   [XmlElement("pt_btw_end_time")]
   public string pt_btw_end_time { get; set; }
   [XmlElement("pt_break_start_date")]
   public string pt_break_start_date { get; set; }
   [XmlElement("pt_break_start_time")]
   public string pt_break_start_time { get; set; }
   [XmlElement("pt_break_end_date")]
   public string pt_break_end_date { get; set; }
   [XmlElement("pt_break_end_time")]
   public string pt_break_end_time { get; set; }
   [XmlElement("status_time_stand")]
   public string status_time_stand { get; set; }

    [XmlElement("announce_diary_date_start")]
    public string announce_diary_date_start { get; set; }
    [XmlElement("announce_diary_date_end")]
    public string announce_diary_date_end { get; set; }
    [XmlElement("m0_group_rsec_idx_ref")]
    public string m0_group_rsec_idx_ref { get; set; }
    /** End convert Date Time to Show */

    [XmlElement("m0_group_emp_idx_ref")]
    public string m0_group_emp_idx_ref { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("Approve_status")]
    public int Approve_status { get; set; }

    [XmlElement("comment_approve")]
    public string comment_approve { get; set; }

    [XmlElement("u0_unidx")]
    public int u0_unidx { get; set; }

    [XmlElement("u0_acidx")]
    public int u0_acidx { get; set; }

    [XmlElement("u0_doc_decision")]
    public int u0_doc_decision { get; set; }

    [XmlElement("type_action")]
    public int type_action { get; set; }

    [XmlElement("emp_createdate")]
    public string emp_createdate { get; set; }
    
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("m0_group_diary_idx")]
    public int m0_group_diary_idx { get; set; }
    [XmlElement("group_diary_name")]
    public string group_diary_name { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("countdown")]
    public int countdown { get; set; }
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("holiday")]
    public string holiday { get; set; }
    [XmlElement("day_1")]
    public string day_1 { get; set; }
    [XmlElement("day_2")]
    public string day_2 { get; set; }
    [XmlElement("day_3")]
    public string day_3 { get; set; }
    [XmlElement("day_4")]
    public string day_4 { get; set; }
    [XmlElement("day_5")]
    public string day_5 { get; set; }
    [XmlElement("day_6")]
    public string day_6 { get; set; }
    [XmlElement("day_7")]
    public string day_7 { get; set; }
    [XmlElement("day_8")]
    public string day_8 { get; set; }
    [XmlElement("day_9")]
    public string day_9 { get; set; }
    [XmlElement("day_10")]
    public string day_10 { get; set; }
    [XmlElement("day_11")]
    public string day_11 { get; set; }
    [XmlElement("day_12")]
    public string day_12 { get; set; }
    [XmlElement("day_13")]
    public string day_13 { get; set; }
    [XmlElement("day_14")]
    public string day_14 { get; set; }
    [XmlElement("day_15")]
    public string day_15 { get; set; }
    [XmlElement("day_16")]
    public string day_16 { get; set; }
    [XmlElement("day_17")]
    public string day_17 { get; set; }
    [XmlElement("day_18")]
    public string day_18 { get; set; }
    [XmlElement("day_19")]
    public string day_19 { get; set; }
    [XmlElement("day_20")]
    public string day_20 { get; set; }
    [XmlElement("day_21")]
    public string day_21 { get; set; }
    [XmlElement("day_22")]
    public string day_22 { get; set; }
    [XmlElement("day_23")]
    public string day_23 { get; set; }
    [XmlElement("day_24")]
    public string day_24 { get; set; }
    [XmlElement("day_25")]
    public string day_25 { get; set; }
    [XmlElement("day_26")]
    public string day_26 { get; set; }
    [XmlElement("day_27")]
    public string day_27 { get; set; }
    [XmlElement("day_28")]
    public string day_28 { get; set; }
    [XmlElement("day_29")]
    public string day_29 { get; set; }
    [XmlElement("day_30")]
    public string day_30 { get; set; }
    [XmlElement("day_31")]
    public string day_31 { get; set; }

    [XmlElement("check_1")]
    public string check_1 { get; set; }
    [XmlElement("check_2")]
    public string check_2 { get; set; }
    [XmlElement("check_3")]
    public string check_3 { get; set; }
    [XmlElement("check_4")]
    public string check_4 { get; set; }
    [XmlElement("check_5")]
    public string check_5 { get; set; }
    [XmlElement("check_6")]
    public string check_6 { get; set; }
    [XmlElement("check_7")]
    public string check_7 { get; set; }
    [XmlElement("check_8")]
    public string check_8 { get; set; }
    [XmlElement("check_9")]
    public string check_9 { get; set; }
    [XmlElement("check_10")]
    public string check_10 { get; set; }
    [XmlElement("check_11")]
    public string check_11 { get; set; }
    [XmlElement("check_12")]
    public string check_12 { get; set; }
    [XmlElement("check_13")]
    public string check_13 { get; set; }
    [XmlElement("check_14")]
    public string check_14 { get; set; }
    [XmlElement("check_15")]
    public string check_15 { get; set; }
    [XmlElement("check_16")]
    public string check_16 { get; set; }
    [XmlElement("check_17")]
    public string check_17 { get; set; }
    [XmlElement("check_18")]
    public string check_18 { get; set; }
    [XmlElement("check_19")]
    public string check_19 { get; set; }
    [XmlElement("check_20")]
    public string check_20 { get; set; }
    [XmlElement("check_21")]
    public string check_21 { get; set; }
    [XmlElement("check_22")]
    public string check_22 { get; set; }
    [XmlElement("check_23")]
    public string check_23 { get; set; }
    [XmlElement("check_24")]
    public string check_24 { get; set; }
    [XmlElement("check_25")]
    public string check_25 { get; set; }
    [XmlElement("check_26")]
    public string check_26 { get; set; }
    [XmlElement("check_27")]
    public string check_27 { get; set; }
    [XmlElement("check_28")]
    public string check_28 { get; set; }
    [XmlElement("check_29")]
    public string check_29 { get; set; }
    [XmlElement("check_30")]
    public string check_30 { get; set; }
    [XmlElement("check_31")]
    public string check_31 { get; set; }

    [XmlElement("month")]
    public string month { get; set; }
    [XmlElement("year")]
    public string year { get; set; }
}
#endregion empshift

#region parttime
[Serializable]
public class parttime
{
   [XmlElement("m0_parttime_idx")]
   public int m0_parttime_idx { get; set; }
   [XmlElement("m0_reward_idx_ref")]
   public int m0_reward_idx_ref { get; set; }
   [XmlElement("parttime_code")]
   public string parttime_code { get; set; }
   [XmlElement("parttime_name_th")]
   public string parttime_name_th { get; set; }
   [XmlElement("parttime_name_eng")]
   public string parttime_name_eng { get; set; }
   [XmlElement("parttime_note")]
   public string parttime_note { get; set; }
   [XmlElement("parttime_bg_color")]
   public string parttime_bg_color { get; set; }
   [XmlElement("parttime_start_date")]
   public int parttime_start_date { get; set; }
   [XmlElement("parttime_start_time")]
   public string parttime_start_time { get; set; }
   [XmlElement("parttime_end_date")]
   public int parttime_end_date { get; set; }
   [XmlElement("parttime_end_time")]
   public string parttime_end_time { get; set; }
   [XmlElement("parttime_btw_start_date")]
   public int parttime_btw_start_date { get; set; }
   [XmlElement("parttime_btw_start_time")]
   public string parttime_btw_start_time { get; set; }
   [XmlElement("parttime_btw_end_date")]
   public int parttime_btw_end_date { get; set; }
   [XmlElement("parttime_btw_end_time")]
   public string parttime_btw_end_time { get; set; }
   [XmlElement("parttime_break_start_date")]
   public int parttime_break_start_date { get; set; }
   [XmlElement("parttime_break_start_time")]
   public string parttime_break_start_time { get; set; }
   [XmlElement("parttime_break_end_date")]
   public int parttime_break_end_date { get; set; }
   [XmlElement("parttime_break_end_time")]
   public string parttime_break_end_time { get; set; }
   [XmlElement("parttime_amount_scan_card")]
   public int parttime_amount_scan_card { get; set; }
   [XmlElement("parttime_time_stand")]
   public int parttime_time_stand { get; set; }
   [XmlElement("parttime_amount_work_hours")]
   public float parttime_amount_work_hours { get; set; }
   [XmlElement("parttime_amount_break_hours")]
   public float parttime_amount_break_hours { get; set; }
   [XmlElement("parttime_status")]
   public int parttime_status { get; set; }
   [XmlElement("parttime_created_at")]
   public string parttime_created_at { get; set; }
   [XmlElement("parttime_created_by")]
   public int parttime_created_by { get; set; }
   [XmlElement("parttime_updated_at")]
   public string parttime_updated_at { get; set; }
   [XmlElement("parttime_updated_by")]
   public int parttime_updated_by { get; set; }
   
   [XmlElement("reward_name")]
   public string reward_name { get; set; }
   [XmlElement("pt_start_time")]
   public string pt_start_time { get; set; }
   [XmlElement("pt_end_time")]
   public string pt_end_time { get; set; }
   [XmlElement("pt_btw_start_time")]
   public string pt_btw_start_time { get; set; }
   [XmlElement("pt_btw_end_time")]
   public string pt_btw_end_time { get; set; }
   [XmlElement("pt_break_start_time")]
   public string pt_break_start_time { get; set; }
   [XmlElement("pt_break_end_time")]
   public string pt_break_end_time { get; set; }
   [XmlElement("status_time_stand")]
   public string status_time_stand { get; set; }

   [XmlElement("keyword_parttime_search")]
   public string keyword_parttime_search { get; set; }
   [XmlElement("parttime_workdays")]
   public string parttime_workdays { get; set; }
   [XmlElement("org_idx")]
   public int org_idx { get; set; }
   [XmlElement("org_name")]
   public string org_name { get; set; }
}
#endregion parttime

#region rewards
[Serializable]
public class rewards
{
   [XmlElement("m0_reward_idx")]
   public int m0_reward_idx { get; set; }
   [XmlElement("reward_name")]
   public string reward_name { get; set; }
   [XmlElement("reward_value")]
   public float reward_value { get; set; }
   [XmlElement("reward_status")]
   public int reward_status { get; set; }
   [XmlElement("reward_created_at")]
   public string reward_created_at { get; set; }
   [XmlElement("reward_created_by")]
   public int reward_created_by { get; set; }
   [XmlElement("reward_updated_at")]
   public string reward_updated_at { get; set; }
   [XmlElement("reward_updated_by")]
   public int reward_updated_by { get; set; }
}
#endregion rewards

#region group_diary
[Serializable]
public class group_diary
{
    [XmlElement("m0_group_diary_idx")]
    public int m0_group_diary_idx { get; set; }
    [XmlElement("rsec_idx_ref")]
    public int rsec_idx_ref { get; set; }
    [XmlElement("group_diary_name")]
    public string group_diary_name { get; set; }
    [XmlElement("group_diary_status")]
    public int group_diary_status { get; set; }
    [XmlElement("group_diary_created_at")]
    public string group_diary_created_at { get; set; }
    [XmlElement("group_diary_created_by")]
    public int group_diary_created_by { get; set; }
    [XmlElement("group_diary_updated_at")]
    public string group_diary_updated_at { get; set; }
    [XmlElement("group_diary_updated_by")]
    public int group_diary_updated_by { get; set; }
    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }
    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }
    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }
    [XmlElement("text_to_ddl")]
    public string text_to_ddl { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_name")]
    public string emp_name { get; set; }
}
#endregion group_diary

#region odspe
[Serializable]
public class odspe
{
   [XmlElement("organization_idx")]
   public int organization_idx { get; set; }
   [XmlElement("department_idx")]
   public int department_idx { get; set; }
   [XmlElement("section_idx")]
   public int section_idx { get; set; }
   [XmlElement("position_idx")]
   public int position_idx { get; set; }
   [XmlElement("emp_idx")]
   public int emp_idx { get; set; }
   [XmlElement("OrgIDX")]
   public int OrgIDX { get; set; }
   [XmlElement("OrgNameTH")]
   public string OrgNameTH { get; set; }
   [XmlElement("RDeptIDX")]
   public int RDeptIDX { get; set; }
   [XmlElement("RDeptID")]
   public int RDeptID { get; set; }
   [XmlElement("DeptNameTH")]
   public string DeptNameTH { get; set; }
   [XmlElement("RSecIDX")]
   public int RSecIDX { get; set; }
   [XmlElement("RSecID")]
   public int RSecID { get; set; }
   [XmlElement("SecNameTH")]
   public string SecNameTH { get; set; }
   [XmlElement("RPosIDX_J")]
   public int RPosIDX_J { get; set; }
   [XmlElement("PosNameTH")]
   public string PosNameTH { get; set; }
   [XmlElement("JobLevel")]
   public string JobLevel { get; set; }
   [XmlElement("NameApprover1")]
   public string NameApprover1 { get; set; }
   [XmlElement("NameApprover2")]
   public string NameApprover2 { get; set; }
   [XmlElement("EmpCode")]
   public string EmpCode { get; set; }

   [XmlElement("EmpIDX")]
   public int EmpIDX { get; set; }
   [XmlElement("FullNameTH")]
   public string FullNameTH { get; set; }
}
#endregion odspe

#region announce
[Serializable]
public class announce
{
   [XmlElement("organization_idx")]
   public int organization_idx { get; set; }
   [XmlElement("department_idx")]
   public int department_idx { get; set; }
   [XmlElement("section_idx")]
   public int section_idx { get; set; }
   [XmlElement("keyword_search")]
   public string keyword_search { get; set; }
   [XmlElement("u0_announce_idx")]
   public int u0_announce_idx { get; set; }
   [XmlElement("u0_node_idx_ref")]
   public int u0_node_idx_ref { get; set; }
   [XmlElement("announce_start")]
   public string announce_start { get; set; }
   [XmlElement("announce_end")]
   public string announce_end { get; set; }
   [XmlElement("announce_status")]
   public int announce_status { get; set; }
   [XmlElement("announce_created_at")]
   public string announce_created_at { get; set; }
   [XmlElement("announce_updated_at")]
   public string announce_updated_at { get; set; }
   [XmlElement("announce_updated_by")]
   public int announce_updated_by { get; set; }
   [XmlElement("count_emp")]
   public string count_emp { get; set; }
   [XmlElement("emp_idx_ref")]
   public int emp_idx_ref { get; set; }
   [XmlElement("EmpCode")]
   public string EmpCode { get; set; }
   [XmlElement("JobLevel")]
   public string JobLevel { get; set; }
   [XmlElement("FullNameTH")]
   public string FullNameTH { get; set; }
   [XmlElement("OrgNameTH")]
   public string OrgNameTH { get; set; }
   [XmlElement("PosNameTH")]
   public string PosNameTH { get; set; }
   [XmlElement("SecNameTH")]
   public string SecNameTH { get; set; }
   [XmlElement("DeptNameTH")]
   public string DeptNameTH { get; set; }
   [XmlElement("NameApprover1")]
   public string NameApprover1 { get; set; }
   [XmlElement("NameApprover2")]
   public string NameApprover2 { get; set; }
   [XmlElement("announce_created_at_convert")]
   public string announce_created_at_convert { get; set; }

   [XmlElement("from_node_name")]
   public string from_node_name { get; set; }
   [XmlElement("to_node_name")]
   public string to_node_name { get; set; }
   [XmlElement("from_actor_name")]
   public string from_actor_name { get; set; }
   [XmlElement("to_actor_name")]
   public string to_actor_name { get; set; }
   [XmlElement("node_decision_statnode")]
   public string node_decision_statnode { get; set; }
   [XmlElement("m0_statnode_statnode_ref")]
   public string m0_statnode_statnode_ref { get; set; }
}
#endregion announce

#region change_announce
[Serializable]
public class change_announce
{
   [XmlElement("organization_idx")]
   public int organization_idx { get; set; }
   [XmlElement("department_idx")]
   public int department_idx { get; set; }
   [XmlElement("u0_change_announce_idx")]
   public int u0_change_announce_idx { get; set; }
   [XmlElement("u0_announce_idx_ref")]
   public int u0_announce_idx_ref { get; set; }
   [XmlElement("u0_announce_idx")]
   public int u0_announce_idx { get; set; }
   [XmlElement("change_u0_node_idx_ref")]
   public int change_u0_node_idx_ref { get; set; }
   [XmlElement("change_announce_start")]
   public string change_announce_start { get; set; }
   [XmlElement("change_announce_end")]
   public string change_announce_end { get; set; }
   [XmlElement("change_announce_updated_at")]
   public string change_announce_updated_at { get; set; }
   [XmlElement("change_announce_updated_by")]
   public int change_announce_updated_by { get; set; }
   [XmlElement("emp_idx_ref")]
   public int emp_idx_ref { get; set; }
   [XmlElement("EmpCode")]
   public string EmpCode { get; set; }
   [XmlElement("JobLevel")]
   public string JobLevel { get; set; }
   [XmlElement("FullNameTH")]
   public string FullNameTH { get; set; }
   [XmlElement("OrgNameTH")]
   public string OrgNameTH { get; set; }
   [XmlElement("PosNameTH")]
   public string PosNameTH { get; set; }
   [XmlElement("SecNameTH")]
   public string SecNameTH { get; set; }
   [XmlElement("DeptNameTH")]
   public string DeptNameTH { get; set; }
   [XmlElement("NameApprover1")]
   public string NameApprover1 { get; set; }
   [XmlElement("NameApprover2")]
   public string NameApprover2 { get; set; }
   [XmlElement("change_announce_created_at_convert")]
   public string change_announce_created_at_convert { get; set; }

   [XmlElement("from_node_name")]
   public string from_node_name { get; set; }
   [XmlElement("to_node_name")]
   public string to_node_name { get; set; }
   [XmlElement("from_actor_name")]
   public string from_actor_name { get; set; }
   [XmlElement("to_actor_name")]
   public string to_actor_name { get; set; }
   [XmlElement("node_decision_statnode")]
   public string node_decision_statnode { get; set; }
   [XmlElement("m0_statnode_statnode_ref")]
   public string m0_statnode_statnode_ref { get; set; }
}
#endregion change_announce

#region announce_diary
[Serializable]
public class announce_diary
{
   [XmlElement("organization_idx")]
   public int organization_idx { get; set; }
   [XmlElement("department_idx")]
   public int department_idx { get; set; }
   [XmlElement("section_idx")]
   public int section_idx { get; set; }
   [XmlElement("keyword_search")]
   public string keyword_search { get; set; }
   [XmlElement("u0_announce_diary_idx")]
   public int u0_announce_diary_idx { get; set; }
   [XmlElement("u0_node_idx_ref")]
   public int u0_node_idx_ref { get; set; }
   [XmlElement("announce_diary_start")]
   public string announce_diary_start { get; set; }
   [XmlElement("announce_diary_end")]
   public string announce_diary_end { get; set; }
   [XmlElement("announce_diary_status")]
   public int announce_diary_status { get; set; }
   [XmlElement("announce_diary_created_at")]
   public string announce_diary_created_at { get; set; }
   [XmlElement("announce_diary_updated_at")]
   public string announce_diary_updated_at { get; set; }
   [XmlElement("announce_diary_updated_by")]
   public int announce_diary_updated_by { get; set; }
   [XmlElement("m0_group_diary_idx_ref")]
   public int m0_group_diary_idx_ref { get; set; }
   [XmlElement("group_diary_name")]
   public string group_diary_name { get; set; }
   [XmlElement("OrgNameTH")]
   public string OrgNameTH { get; set; }
   [XmlElement("DeptNameTH")]
   public string DeptNameTH { get; set; }
   [XmlElement("SecNameTH")]
   public string SecNameTH { get; set; }
   [XmlElement("EmpCode")]
   public int EmpCode { get; set; }
   [XmlElement("FullNameTH")]
   public string FullNameTH { get; set; }
   [XmlElement("announce_diary_created_at_convert")]
   public string announce_diary_created_at_convert { get; set; }

   [XmlElement("from_node_name")]
   public string from_node_name { get; set; }
   [XmlElement("to_node_name")]
   public string to_node_name { get; set; }
   [XmlElement("from_actor_name")]
   public string from_actor_name { get; set; }
   [XmlElement("to_actor_name")]
   public string to_actor_name { get; set; }
   [XmlElement("node_decision_statnode")]
   public string node_decision_statnode { get; set; }
   [XmlElement("m0_statnode_statnode_ref")]
   public string m0_statnode_statnode_ref { get; set; }
}
#endregion announce_diary

#region change_announce_diary
[Serializable]
public class change_announce_diary
{
   [XmlElement("organization_idx")]
   public int organization_idx { get; set; }
   [XmlElement("department_idx")]
   public int department_idx { get; set; }
   [XmlElement("section_idx")]
   public int section_idx { get; set; }
   [XmlElement("u0_announce_diary_idx")]
   public int u0_announce_diary_idx { get; set; }
   [XmlElement("u0_node_idx_ref")]
   public int u0_node_idx_ref { get; set; }
   [XmlElement("announce_diary_start")]
   public string announce_diary_start { get; set; }
   [XmlElement("announce_diary_end")]
   public string announce_diary_end { get; set; }
   [XmlElement("announce_diary_status")]
   public int announce_diary_status { get; set; }
   [XmlElement("announce_diary_created_at")]
   public string announce_diary_created_at { get; set; }
   [XmlElement("announce_diary_updated_at")]
   public string announce_diary_updated_at { get; set; }
   [XmlElement("announce_diary_updated_by")]
   public int announce_diary_updated_by { get; set; }
   [XmlElement("m0_group_diary_idx_ref")]
   public int m0_group_diary_idx_ref { get; set; }
   [XmlElement("group_diary_name")]
   public string group_diary_name { get; set; }
   [XmlElement("OrgNameTH")]
   public string OrgNameTH { get; set; }
   [XmlElement("DeptNameTH")]
   public string DeptNameTH { get; set; }
   [XmlElement("SecNameTH")]
   public string SecNameTH { get; set; }
   [XmlElement("EmpCode")]
   public int EmpCode { get; set; }
   [XmlElement("FullNameTH")]
   public string FullNameTH { get; set; }
   [XmlElement("announce_diary_created_at_convert")]
   public string announce_diary_created_at_convert { get; set; }

   [XmlElement("from_node_name")]
   public string from_node_name { get; set; }
   [XmlElement("to_node_name")]
   public string to_node_name { get; set; }
   [XmlElement("from_actor_name")]
   public string from_actor_name { get; set; }
   [XmlElement("to_actor_name")]
   public string to_actor_name { get; set; }
   [XmlElement("node_decision_statnode")]
   public string node_decision_statnode { get; set; }
   [XmlElement("m0_statnode_statnode_ref")]
   public string m0_statnode_statnode_ref { get; set; }

   [XmlElement("u0_change_announce_diary_idx")]
   public int u0_change_announce_diary_idx { get; set; }
   [XmlElement("u0_announce_diary_idx_ref")]
   public int u0_announce_diary_idx_ref { get; set; }
   [XmlElement("change_announce_diary_start")]
   public string change_announce_diary_start { get; set; }
   [XmlElement("change_announce_diary_end")]
   public string change_announce_diary_end { get; set; }
   [XmlElement("change_announce_diary_status")]
   public int change_announce_diary_status { get; set; }
   [XmlElement("change_announce_diary_created_at")]
   public string change_announce_diary_created_at { get; set; }
   [XmlElement("change_announce_diary_created_by")]
   public int change_announce_diary_created_by { get; set; }
   [XmlElement("change_announce_diary_updated_at")]
   public string change_announce_diary_updated_at { get; set; }
   [XmlElement("change_announce_diary_updated_by")]
   public int change_announce_diary_updated_by { get; set; }
   [XmlElement("change_announce_diary_created_at_convert")]
   public string change_announce_diary_created_at_convert { get; set; }
}
#endregion change_announce_diary

#region approval log
[Serializable]
public class approval_log
{
   [XmlElement("u0_approval_log_idx")]
   public int u0_approval_log_idx { get; set; }
   [XmlElement("u0_typeof_idx_ref")]
   public int u0_typeof_idx_ref { get; set; }
   [XmlElement("u0_node_idx_ref")]
   public int u0_node_idx_ref { get; set; }
   [XmlElement("approval_log_types")]
   public int approval_log_types { get; set; }
   [XmlElement("approval_log_emp_types")]
   public int approval_log_emp_types { get; set; }
   [XmlElement("approval_log_created_by")]
   public int approval_log_created_by { get; set; }

   [XmlElement("FullNameTH")]
   public string FullNameTH { get; set; }
   [XmlElement("from_node_name")]
   public string from_node_name { get; set; }
   [XmlElement("node_decision_statnode")]
   public string node_decision_statnode { get; set; }
   [XmlElement("m0_statnode_statnode_ref")]
   public string m0_statnode_statnode_ref { get; set; }
   [XmlElement("approval_log_date")]
   public string approval_log_date { get; set; }
   [XmlElement("approval_log_time")]
   public string approval_log_time { get; set; }
}
#endregion approval log

#region employee data
[Serializable]
public class empsemployee
{
   [XmlElement("u0_announce_idx")]
   public int u0_announce_idx { get; set; }
   [XmlElement("u0_change_announce_idx")]
   public int u0_change_announce_idx { get; set; }
   [XmlElement("EmpCode")]
   public string EmpCode { get; set; }
   [XmlElement("JobLevel")]
   public string JobLevel { get; set; }
   [XmlElement("FullNameTH")]
   public string FullNameTH { get; set; }
   [XmlElement("OrgNameTH")]
   public string OrgNameTH { get; set; }
   [XmlElement("PosNameTH")]
   public string PosNameTH { get; set; }
   [XmlElement("SecNameTH")]
   public string SecNameTH { get; set; }
   [XmlElement("DeptNameTH")]
   public string DeptNameTH { get; set; }
   [XmlElement("NameApprover1")]
   public string NameApprover1 { get; set; }
   [XmlElement("NameApprover2")]
   public string NameApprover2 { get; set; }
}
#endregion employee data

#region export
[Serializable]
public class export
{
   [XmlElement("m0_group_diary_idx_ref")]
   public int m0_group_diary_idx_ref { get; set; }
   [XmlElement("organization_idx")]
   public int organization_idx { get; set; }
   [XmlElement("department_idx")]
   public int department_idx { get; set; }
   [XmlElement("section_idx")]
   public int section_idx { get; set; }
   [XmlElement("status_doc")]
   public string status_doc { get; set; }
   [XmlElement("keyword_search")]
   public string keyword_search { get; set; }
   [XmlElement("EmpCode")]
   public string EmpCode { get; set; }
   [XmlElement("FullNameTH")]
   public string FullNameTH { get; set; }
   [XmlElement("OrgNameTH")]
   public string OrgNameTH { get; set; }
   [XmlElement("DeptNameTH")]
   public string DeptNameTH { get; set; }
   [XmlElement("SecNameTH")]
   public string SecNameTH { get; set; }
   [XmlElement("group_diary_name")]
   public string group_diary_name { get; set; }
   [XmlElement("parttime_code")]
   public string parttime_code { get; set; }
   [XmlElement("empshift_date_convert")]
   public string empshift_date_convert { get; set; }
   [XmlElement("approver_name")]
   public string approver_name { get; set; }
   [XmlElement("announce_status_convert")]
   public string announce_status_convert { get; set; }
   [XmlElement("empshift_old_date_convert")]
   public string empshift_old_date_convert { get; set; }
   [XmlElement("parttime_old")]
   public string parttime_old { get; set; }
   [XmlElement("empshift_new_date_convert")]
   public string empshift_new_date_convert { get; set; }
   [XmlElement("parttime_new")]
   public string parttime_new { get; set; }
   [XmlElement("m0_parttime_idx_ref")]
   public int m0_parttime_idx_ref { get; set; }
   [XmlElement("new_editor")]
   public string new_editor { get; set; }
   [XmlElement("emp_idx_ref")]
   public int emp_idx_ref { get; set; }
   [XmlElement("count_parttime")]
   public string count_parttime { get; set; }
   [XmlElement("count_change_parttime")]
   public string count_change_parttime { get; set; }
}
#endregion export

#region m1_group_diary
[Serializable]
public class m1_group_diary
{
    [XmlElement("m0_group_diary_idx")]
    public int m0_group_diary_idx { get; set; }
    [XmlElement("rsec_idx_ref")]
    public int rsec_idx_ref { get; set; }
    [XmlElement("group_diary_name")]
    public string group_diary_name { get; set; }
    [XmlElement("group_diary_status")]
    public int group_diary_status { get; set; }
    [XmlElement("group_diary_created_at")]
    public string group_diary_created_at { get; set; }
    [XmlElement("group_diary_created_by")]
    public int group_diary_created_by { get; set; }
    [XmlElement("group_diary_updated_at")]
    public string group_diary_updated_at { get; set; }
    [XmlElement("group_diary_updated_by")]
    public int group_diary_updated_by { get; set; }
    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }
    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }
    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }
    [XmlElement("text_to_ddl")]
    public string text_to_ddl { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_name")]
    public string emp_name { get; set; }
}
#endregion m1_group_diary
