<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="dev_auth.aspx.cs" Inherits="websystem_playground_dev_auth" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:MultiView ID="mvSystem" runat="server">
        <asp:View ID="viewAuth" runat="server">
            <div class="col-md-12">
                <div class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-md-3 control-label">authentication<span class="text-danger">*</span> :</label>
                        <div class="col-md-3">
                            <asp:TextBox ID="tbPassword" runat="server" CssClass="form-control"
                                placeholder="2nd authentication" TextMode="Password"></asp:TextBox>
                        </div>
                        <label class="col-md-6"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-3">
                            <asp:LinkButton ID="lbSubmit" runat="server" CssClass="btn btn-success"
                                OnCommand="btnCommand" CommandName="cmdAuth">
                                <i class="fas fa-random"></i>&nbsp;authentication</asp:LinkButton>
                        </div>
                        <label class="col-md-6"></label>
                    </div>
                </div>
            </div>
        </asp:View>
        <asp:View ID="viewList" runat="server">
            <div class="col-md-12">
                2nd page
                <asp:LinkButton ID="lbRelease" runat="server" CssClass="btn btn-warning" OnCommand="btnCommand"
                    CommandName="cmdRelease">
                    <i class="fas fa-random"></i>&nbsp;release</asp:LinkButton>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>