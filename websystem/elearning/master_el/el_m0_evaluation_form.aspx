﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_m0_evaluation_form.aspx.cs" Inherits="websystem_el_m0_evaluation_form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="row">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary pull-right" runat="server"
                    data-toggle="tooltip" title="สร้างแบบประเมิน"
                    CommandName="btnToInsert" OnCommand="btnCommand">
                     <i class="fa fa-plus-square"></i> สร้างแบบประเมิน</asp:LinkButton>
            </div>

            <asp:Panel ID="pnlsearch" runat="server">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>กลุ่มหัวข้อการประเมิน</label>
                                <asp:DropDownList ID="ddlm0_evaluation_group_idx_refsel" runat="server"
                                    CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>หัวข้อการประเมิน</label>
                                <asp:TextBox ID="txtFilterKeyword" runat="server"
                                    CssClass="form-control"
                                    placeholder="กลุ่มหัวข้อการประเมิน..." />
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnFilter" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnFilter" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="m0_evaluation_f_idx"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>

                                <asp:TextBox ID="m0_evaluation_f_idx" runat="server" CssClass="form-control"
                                    Visible="False" Text='<%# Eval("m0_evaluation_f_idx")%>' />

                                <div class="col-md-8 col-md-offset-2">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">กลุ่มหัวข้อการประเมิน</label>
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="hdtxtm0_evaluation_group_idx_ref" runat="server" CssClass="form-control"
                                                            Visible="False" Text='<%# Eval("m0_evaluation_group_idx_ref")%>' />

                                                        <asp:DropDownList ID="ddlm0_evaluation_group_idx_refUpdate" runat="server"
                                                            CssClass="form-control">
                                                        </asp:DropDownList>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <%-- <label class="pull-left">ลำดับ</label>--%>
                                                <asp:UpdatePanel ID="panelevaluation_f_itemUpdate" Visible="false" runat="server">
                                                    <ContentTemplate>

                                                        <asp:TextBox ID="txtevaluation_f_itemUpdate" runat="server" CssClass="form-control"
                                                            Text='<%# Eval("evaluation_f_item")%>' />
                                                        <%-- <asp:RequiredFieldValidator ID="Requiredtxtevaluation_f_itemUpdate"
                                                            ValidationGroup="saveevaluation_f_name_enUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txtevaluation_f_itemUpdate"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกลำดับ" />--%>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">ชื่อหัวข้อการประเมิน(EN)</label>
                                                <asp:UpdatePanel ID="panelevaluation_f_name_enUpdate" runat="server">
                                                    <ContentTemplate>

                                                        <asp:TextBox ID="txtevaluation_f_name_enUpdate" runat="server" CssClass="form-control"
                                                            Text='<%# Eval("evaluation_f_name_en")%>' />
                                                        <asp:RequiredFieldValidator ID="requiredevaluation_f_name_enUpdate"
                                                            ValidationGroup="saveevaluation_f_name_enUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txtevaluation_f_name_enUpdate"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกชื่อหัวข้อการประเมิน(EN)" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">ชื่อหัวข้อการประเมิน(TH)</label>
                                                <asp:UpdatePanel ID="panelevaluation_f_name_thUpdate" runat="server">
                                                    <ContentTemplate>

                                                        <asp:TextBox ID="txtevaluation_f_name_thUpdate" runat="server" CssClass="form-control"
                                                            Text='<%# Eval("evaluation_f_name_th")%>' />
                                                        <asp:RequiredFieldValidator ID="requiredevaluation_f_name_thUpdate"
                                                            ValidationGroup="saveevaluation_f_name_enUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txtevaluation_f_name_thUpdate"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกชื่อหัวข้อการประเมิน(TH)" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">หมายเหตุ</label>
                                                <asp:UpdatePanel ID="panelevaluation_f_remarkUpdate" runat="server">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtevaluation_f_remarkUpdate" runat="server" CssClass="form-control"
                                                            TextMode="MultiLine" Rows="4"
                                                            Text='<%# Eval("evaluation_f_remark")%>'
                                                            placeholder="หมายเหตุ..." />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">สถานะคะแนน</label>
                                                <asp:DropDownList ID="ddlevaluation_f_scoreUpdate" AutoPostBack="false" runat="server"
                                                    CssClass="form-control" SelectedValue='<%# Eval("evaluation_f_score") %>'>
                                                    <asp:ListItem Value="1" Text="คิดคะแนน"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="ไม่คิดคะแนน"></asp:ListItem>
                                                </asp:DropDownList>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">สถานะ</label>
                                                <asp:DropDownList ID="ddlevaluation_f_statusUpdate" AutoPostBack="false" runat="server"
                                                    CssClass="form-control" SelectedValue='<%# Eval("evaluation_f_status") %>'>
                                                    <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                                </asp:DropDownList>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                                ValidationGroup="saveevaluation_f_name_enUpdate" CommandName="Update"
                                                OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"
                                                data-toggle="tooltip" title="บันทึก"
                                                Text="<i class='fa fa-save fa-lg'></i> บันทึก">
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                                Text="<i class='fa fa-close fa-lg'></i> ยกเลิก"
                                                data-toggle="tooltip" title="ยกเลิก"
                                                CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="กลุ่มหัวข้อการประเมิน" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Gv_lbevaluation_group_name_th" runat="server" Text='<%# Eval("evaluation_group_name_th") %>' />
                                    <asp:Label ID="Gv_m0_evaluation_group_idx_ref1" runat="server" Text='<%# Eval("m0_evaluation_group_idx_ref") %>' Visible="false" />
                                </small>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <%-- <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="evaluation_f_item" runat="server" Text='<%# Eval("evaluation_f_item") %>' />
                                </small>
                            </ItemTemplate>

                        </asp:TemplateField>--%>

                        <asp:TemplateField HeaderText="ชื่อหัวข้อการประเมิน(EN)" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Gv_evaluation_f_name_en" runat="server" Text='<%# Eval("evaluation_f_name_en") %>' />
                                </small>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหัวข้อการประเมิน(TH)" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Gv_evaluation_f_name_th" runat="server" Text='<%# Eval("evaluation_f_name_th") %>' />
                                </small>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะคะแนน" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>

                                    <asp:Label ID="Gv_evaluation_f_score" runat="server" Text='<%# getValueTitle((int)Eval("evaluation_f_score")) %>' />
                                </small>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Gv_evaluation_f_status" runat="server" Text='<%# getStatus((int)Eval("evaluation_f_status")) %>' />
                                </small>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("m0_evaluation_f_idx") %>'
                                    OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>

        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <%--<i class="glyphicon glyphicon-blackboard"></i>--%>
                        <strong>&nbsp; แบบประเมิน
                        </strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-8 col-md-offset-2">

                        <div class="row">


                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>กลุ่มหัวข้อการประเมิน</label>
                                    <asp:DropDownList ID="ddlm0_evaluation_group_idx_ref" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="form-group">
                                    <%--<label>ลำดับ</label>--%>
                                    <asp:UpdatePanel ID="pnlevaluation_f_item" Visible="false" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtevaluation_f_item" runat="server" CssClass="form-control"
                                                onkeydown="return (!(event.keyCode>=65) && event.keyCode!=32);"
                                                TextMode="Number" placeholder="ลำดับ..." />
                                            <%-- <asp:RequiredFieldValidator ID="Requiredevaluation_f_item"
                                                ValidationGroup="save" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtevaluation_f_item"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกลำดับ" />--%>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ชื่อหัวข้อการประเมิน(EN)</label>
                                    <asp:UpdatePanel ID="pnlevaluation_f_name_en" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtevaluation_f_name_en" runat="server" CssClass="form-control"
                                                placeholder="หัวข้อการประเมิน..." />
                                            <asp:RequiredFieldValidator ID="requiredevaluation_f_name_en"
                                                ValidationGroup="save" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtevaluation_f_name_en"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกชื่อหัวข้อการประเมิน(EN)" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ชื่อหัวข้อการประเมิน(TH)</label>
                                    <asp:UpdatePanel ID="pnlevaluation_f_name_th" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtevaluation_f_name_th" runat="server" CssClass="form-control"
                                                placeholder="หัวข้อการประเมิน..." />
                                            <asp:RequiredFieldValidator ID="requiredevaluation_f_name_th"
                                                ValidationGroup="save" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtevaluation_f_name_th"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกชื่อหัวข้อการประเมิน(TH)" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>หมายเหตุ</label>
                                    <asp:UpdatePanel ID="pnlevaluation_f_remark" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtevaluation_f_remark" runat="server" CssClass="form-control"
                                                TextMode="MultiLine" Rows="4"
                                                placeholder="หมายเหตุ..." />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <small>
                                        <label class="pull-left">สถานะคะแนน</label>
                                        <asp:DropDownList ID="ddlevaluation_f_score" AutoPostBack="false" runat="server"
                                            CssClass="form-control">
                                            <asp:ListItem Value="1" Text="คิดคะแนน"></asp:ListItem>
                                            <asp:ListItem Value="0" Text="ไม่คิดคะแนน"></asp:ListItem>
                                        </asp:DropDownList>
                                    </small>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>สถานะ</label>
                                    <asp:DropDownList ID="ddlevaluation_f_status" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="1" Text="Online" />
                                        <asp:ListItem Value="0" Text="Offline" />
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">

                                    <asp:LinkButton CssClass="btn btn-success" runat="server"
                                        CommandName="btnInsert" OnCommand="btnCommand"
                                        Text="<i class='fa fa-save fa-lg'></i> บันทึก"
                                        data-toggle="tooltip" title="บันทึก"
                                        ValidationGroup="save" />
                                    <asp:LinkButton CssClass="btn btn-danger"
                                        data-toggle="tooltip" title="ยกเลิก" runat="server"
                                        Text="<i class='fa fa-close fa-lg'></i> ยกเลิก"
                                        CommandName="btnCancel" OnCommand="btnCommand" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </asp:View>
        <!-- End Insert Form -->


    </asp:MultiView>

</asp:Content>
