﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_drc_m0_form : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_roombooking _data_roombooking = new data_roombooking();
    data_drc _data_drc = new data_drc();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    // drc
    static string _urlGetM0TopicNameDRC = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0TopicNameDRC"];
    static string _urlSetM0FormDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetM0FormDRC"];
    static string _urlGetFormRootDetailNameDRC = _serviceUrl + ConfigurationManager.AppSettings["urlGetFormRootDetailNameDRC"];
    static string _urlGetFormDetailNameDRC = _serviceUrl + ConfigurationManager.AppSettings["urlGetFormDetailNameDRC"];
    static string _urlSetDelFormDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelFormDRC"];


    //-- roombooking --//
    //static string _urlSetRbkm0Food = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0Food"];
    //static string _urlGetRbkm0Food = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Food"];
    //static string _urlSetRbkm0FoodDel = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0FoodDel"];

    //static string _urlGetRbkm0Place = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Place"];
    //static string _urlGetRbkm0Equiment = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Equiment"];

    //static string _urlSetRbkm0Room = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0Room"];
    //static string _urlGetRbkm0Room = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Room"];
    //static string _urlGetRbkm0EquimentDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0EquimentDetail"];
    //static string _urlSetRbkm0RoomDel = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0RoomDel"];
   

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            SelectFormDetail();
            getTopicNameList();
            

        }

    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MvSystem.SetActiveView(ViewIndex);
        //SelectRoomDetail();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddForm":

                GvFormDetail.Visible = false;
                btnAddForm.Visible = false;
                fvInsertForm.Visible = true;
                setFormData(fvInsertForm, FormViewMode.Insert, null);
                CleardataSetTopicList();
                //DropDownList ddlPlace = (DropDownList)fvRoomBooking.FindControl("ddlPlace");

                getTopicName(ddlTopicname, 0);
                //getEquiment(ddlEquiment, 0);

                break;

            case "CmdSave":
                InsertFormDetail();

                break;

            case "CmdCancel":
                btnAddForm.Visible = true;
                GvFormDetail.Visible = true;
                fvInsertForm.Visible = false;
                setOntop.Focus();
                break;

            case "CmdDeleteForm":
                int R0IDX_del = int.Parse(cmdArg);

                //litdebug.Text = "222";

                data_drc _data_drc_form_del = new data_drc();
                SetFormR0MasterDetail m0_formdetail_del = new SetFormR0MasterDetail();
                _data_drc_form_del.BoxFormR0MasterList = new SetFormR0MasterDetail[1];

                m0_formdetail_del.R0IDX = R0IDX_del;
                m0_formdetail_del.CEmpIDX = _emp_idx;

                _data_drc_form_del.BoxFormR0MasterList[0] = m0_formdetail_del;
                _data_drc_form_del = callServicePostDRC(_urlSetDelFormDRC, _data_drc_form_del);

                SelectFormDetail();
                //Gv_select_unit.Visible = false;
                btnAddForm.Visible = true;
                fvInsertForm.Visible = false;

                break;
            case "cmdAddTopicName":
                setTopicNameList();

                break;
           

        }
    }
    #endregion btnCommand

    #region data table
    protected void getTopicNameList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsTopicList = new DataSet();
        dsTopicList.Tables.Add("dsTopicTable");
        dsTopicList.Tables["dsTopicTable"].Columns.Add("drTopicIdx", typeof(String));
        dsTopicList.Tables["dsTopicTable"].Columns.Add("drTopicText", typeof(String));

        ViewState["vsTopicNameList"] = dsTopicList;
    }

    protected void setTopicNameList()
    {
        if (ViewState["vsTopicNameList"] != null)
        {
            DropDownList ddlTopicname = (DropDownList)fvInsertForm.FindControl("ddlTopicname");
            GridView gvTopicList = (GridView)fvInsertForm.FindControl("gvTopicList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsTopicNameList"];

            foreach (DataRow dr in dsContacts.Tables["dsTopicTable"].Rows)
            {
                if (dr["drTopicIdx"].ToString() == ddlTopicname.SelectedValue)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลอยู่แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsTopicTable"].NewRow();
            drContacts["drTopicIdx"] = ddlTopicname.SelectedValue;
            drContacts["drTopicText"] = ddlTopicname.SelectedItem.Text;

            dsContacts.Tables["dsTopicTable"].Rows.Add(drContacts);
            ViewState["vsTopicNameList"] = dsContacts;
            setGridData(gvTopicList, dsContacts.Tables["dsTopicTable"]);
            gvTopicList.Visible = true;
        }
    }

    protected void CleardataSetTopicList()
    {
        var gvTopicList = (GridView)fvInsertForm.FindControl("gvTopicList");
        ViewState["vsTopicNameList"] = null;

        gvTopicList.DataSource = ViewState["vsTopicNameList"];
        gvTopicList.DataBind();
        getTopicNameList();
    }  

    #endregion data table

    #region Custom Functions
    protected void InsertFormDetail()
    {

        TextBox txt_formname = (TextBox)fvInsertForm.FindControl("txt_formname");

        data_drc _data_drc_topic_insert = new data_drc();
        SetFormR0MasterDetail m0_topic_insert = new SetFormR0MasterDetail();
        _data_drc_topic_insert.BoxFormR0MasterList = new SetFormR0MasterDetail[1];

        m0_topic_insert.R0IDX = 0;
        m0_topic_insert.R0_Name = txt_formname.Text;
        m0_topic_insert.cempidx = _emp_idx;

        //r1 master
        var _dataset_TopicNameList = (DataSet)ViewState["vsTopicNameList"];

        var _add_TopicList = new SetFormR1MasterDetail[_dataset_TopicNameList.Tables[0].Rows.Count];
        int row_Topiclist = 0;

        foreach (DataRow dtrow_Topiclist in _dataset_TopicNameList.Tables[0].Rows)
        {
            _add_TopicList[row_Topiclist] = new SetFormR1MasterDetail();

            _add_TopicList[row_Topiclist].tidx = int.Parse(dtrow_Topiclist["drTopicIdx"].ToString());
            _add_TopicList[row_Topiclist].CEmpIDX = _emp_idx;

            row_Topiclist++;

        }

        _data_drc_topic_insert.BoxFormR0MasterList[0] = m0_topic_insert;
        _data_drc_topic_insert.BoxFormR1MasterList = _add_TopicList;
        //litdebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_room));

        //litdebug.Text = _dataset_EquimentList.Tables[0].Rows.Count.ToString();

        if (_dataset_TopicNameList.Tables[0].Rows.Count > 0)
        {
            //litdebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_drc_topic_insert));
            _data_drc_topic_insert = callServicePostDRC(_urlSetM0FormDRC, _data_drc_topic_insert);

            // _data_drc_topic_insert = callServicePostRoomBooking(_urlSetRbkm0Room, _data_drc_topic_insert);

            //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
            if (_data_drc_topic_insert.return_code == 1)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

            }
            else
            {
                SelectFormDetail();
                GvFormDetail.Visible = true;
                //SelectFoodDetail();
                btnAddForm.Visible = true;
                fvInsertForm.Visible = false;
                setOntop.Focus();

                //txtfood_name.Text = String.Empty;
                //tex_place_code.Text = String.Empty;
            }

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรอกข้อมูลให้ครบถ้วนก่อนทำการบันทึก');", true);

        }


    }

    protected void SelectFormDetail()
    {

        data_drc _data_drc_formdetail = new data_drc();
        SetFormR0MasterDetail m0_formdetail = new SetFormR0MasterDetail();
        _data_drc_formdetail.BoxFormR0MasterList = new SetFormR0MasterDetail[1];
        m0_formdetail.R0IDX = 0;//int.Parse(ViewState["vs_setname_sidx"].ToString());

        _data_drc_formdetail.BoxFormR0MasterList[0] = m0_formdetail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        _data_drc_formdetail = callServicePostDRC(_urlGetFormDetailNameDRC, _data_drc_formdetail);
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_drc_formdetail));
        setGridData(GvFormDetail, _data_drc_formdetail.BoxFormR0MasterList);

    }



    protected void getTopicName(DropDownList ddlName, int _topic_idx)
    {

        data_drc _data_drc_topicdetail = new data_drc();
        SetTopicNameDetail m0_topicdetail = new SetTopicNameDetail();
        _data_drc_topicdetail.BoxTopicNameList = new SetTopicNameDetail[1];
        m0_topicdetail.root_idx = 0;//int.Parse(ViewState["vs_setname_sidx"].ToString());

        _data_drc_topicdetail.BoxTopicNameList[0] = m0_topicdetail;

        _data_drc_topicdetail = callServicePostDRC(_urlGetM0TopicNameDRC, _data_drc_topicdetail);
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_drc_topicdetail));
        setDdlData(ddlName, _data_drc_topicdetail.BoxTopicNameList, "topic_name", "tidx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกชื่อหัวข้อ ---", "0"));
        ddlName.SelectedValue = _topic_idx.ToString();

    }


    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }


    #endregion

    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {
                case "fvRoomBooking":
                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {
                        ////linkBtnTrigger(btnSaveUpdate);
                        //CleardataSetEquimentListUpdate();

                        //Label lblm0_room_idx_edit = (Label)FvName.FindControl("lblm0_room_idx_edit");
                        //Label lblplace_idx_edit = (Label)FvName.FindControl("lblplace_idx_edit");
                        //Label lbl_place_name_edit = (Label)FvName.FindControl("lbl_place_name_edit");
                        //Label lbl_room_name_en_edit = (Label)FvName.FindControl("lbl_room_name_en_edit");
                        //DropDownList ddlPlaceUpdate = (DropDownList)FvName.FindControl("ddlPlaceUpdate");

                        ////getPlace(ddlPlaceUpdate, int.Parse(lblplace_idx_edit.Text));
                        ////getEquiment(ddlEquimentUpdate, 0);

                        //data_roombooking data_m1_room_detail_edit = new data_roombooking();
                        //rbk_m1_room_detail m1_room_detail_edit = new rbk_m1_room_detail();
                        //data_m1_room_detail_edit.rbk_m1_room_list = new rbk_m1_room_detail[1];

                        //m1_room_detail_edit.m0_room_idx = int.Parse(lblm0_room_idx_edit.Text);

                        //data_m1_room_detail_edit.rbk_m1_room_list[0] = m1_room_detail_edit;

                        //// litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m1_room_detail_edit));
                        //data_m1_room_detail_edit = callServicePostRoomBooking(_urlGetRbkm0EquimentDetail, data_m1_room_detail_edit);

                        ////litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m1_room_detail_edit));
                        //if (data_m1_room_detail_edit.return_code == 0)
                        //{
                        //    foreach (var equiment_edit in data_m1_room_detail_edit.rbk_m1_room_list)
                        //    {
                        //        if (ViewState["vsEquimentUpdate"] != null)
                        //        {
                        //            CultureInfo culture = new CultureInfo("en-US");
                        //            Thread.CurrentThread.CurrentCulture = culture;
                        //            DataSet dsContacts = (DataSet)ViewState["vsEquimentUpdate"];
                        //            DataRow drContacts = dsContacts.Tables["dsEquimentTableUpdate"].NewRow();

                        //            drContacts["drEquimentIdxUpdate"] = equiment_edit.equiment_idx;
                        //            drContacts["drEquimentTextUpdate"] = equiment_edit.equiment_name;

                        //            dsContacts.Tables["dsEquimentTableUpdate"].Rows.Add(drContacts);
                        //            ViewState["vsEquimentUpdate"] = dsContacts;
                        //            //setGridData(gvEquimentListUpdate, dsContacts.Tables["dsEquimentTableUpdate"]);

                        //            //gvEquimentListUpdate.Visible = true;
                        //        }
                        //    }
                        //}


                        ////File Datil Edit
                        ////var lbl_m0_room_idx_file = (Label)FvName.FindControl("lbl_m0_room_idx");
                        ////var lbl_place_name_file = (Label)FvName.FindControl("lbl_place_name");
                        ////var lbl_room_name_en_view_file = (Label)FvName.FindControl("lbl_room_name_en_view");

                        //var btnViewFileRoomEdit = (HyperLink)FvName.FindControl("btnViewFileRoomEdit");

                        //string filePath_View_edit = ConfigurationManager.AppSettings["path_flie_roombooking"];
                        //string directoryName_View_edit = lblm0_room_idx_edit.Text;//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text;

                        //if (Directory.Exists(Server.MapPath(filePath_View_edit + directoryName_View_edit)))
                        //{
                        //    string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View_edit + directoryName_View_edit));
                        //    List<ListItem> files = new List<ListItem>();
                        //    foreach (string path in filesPath)
                        //    {
                        //        string getfiles = "";
                        //        getfiles = Path.GetFileName(path);
                        //        btnViewFileRoomEdit.NavigateUrl = filePath_View_edit + directoryName_View_edit + "/" + getfiles;
                        //    }
                        //    btnViewFileRoomEdit.Visible = true;
                        //}
                        //else
                        //{
                        //    btnViewFileRoomEdit.Visible = false;
                        //}

                    }
                    break;

            }
        }
    }
    #endregion Formview Databind

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvFormDetail":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_R0IDX = (Label)e.Row.FindControl("lbl_R0IDX");
                    Repeater rptTopicDetail = (Repeater)e.Row.FindControl("rptTopicDetail");

                    //Detail Topic
                    data_drc _data_drc_topicdetail_r1 = new data_drc();
                    SetFormR1MasterDetail m0_topicdetail_r1 = new SetFormR1MasterDetail();
                    _data_drc_topicdetail_r1.BoxFormR1MasterList = new SetFormR1MasterDetail[1];

                    m0_topicdetail_r1.R0IDX = int.Parse(lbl_R0IDX.Text);

                    _data_drc_topicdetail_r1.BoxFormR1MasterList[0] = m0_topicdetail_r1;
                    //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                    _data_drc_topicdetail_r1 = callServicePostDRC(_urlGetFormRootDetailNameDRC, _data_drc_topicdetail_r1);
                    setRepeaterData(rptTopicDetail, _data_drc_topicdetail_r1.BoxFormR1MasterList);
                }
                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvFoodInRoom":
                ////GvFoodInRoom.EditIndex = e.NewEditIndex;
                ////SelectFoodDetail();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvFoodInRoom":

                ////var txt_food_idx_edit = (TextBox)GvFoodInRoom.Rows[e.RowIndex].FindControl("txt_food_idx_edit");
                ////var txt_food_name_edit = (TextBox)GvFoodInRoom.Rows[e.RowIndex].FindControl("txt_food_name_edit");
                ////var ddlfood_status_edit = (DropDownList)GvFoodInRoom.Rows[e.RowIndex].FindControl("ddlfood_status_edit");


                ////GvFoodInRoom.EditIndex = -1;


                ////data_roombooking data_m0_food_edit = new data_roombooking();
                ////rbk_m0_food_detail m0_food_edit = new rbk_m0_food_detail();
                ////data_m0_food_edit.rbk_m0_food_list = new rbk_m0_food_detail[1];

                ////m0_food_edit.food_idx = int.Parse(txt_food_idx_edit.Text);
                ////m0_food_edit.food_name = txt_food_name_edit.Text;
                ////m0_food_edit.cemp_idx = _emp_idx;
                ////m0_food_edit.food_status = int.Parse(ddlfood_status_edit.SelectedValue);

                ////data_m0_food_edit.rbk_m0_food_list[0] = m0_food_edit;

                //////litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_resultuse_edit));

                ////data_m0_food_edit = callServicePostRoomBooking(_urlSetRbkm0Food, data_m0_food_edit);

                ////if (data_m0_food_edit.return_code == 1)
                ////{
                ////    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                ////    SelectFoodDetail();
                ////}
                ////else
                ////{
                ////    SelectFoodDetail();
                ////}
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvFoodInRoom":
                ////GvFoodInRoom.EditIndex = -1;
                ////SelectFoodDetail();

                ////btnAddFood.Visible = true;
                ////FvInsert.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvFormDetail":
                GvFormDetail.PageIndex = e.NewPageIndex;
                GvFormDetail.DataBind();
                SelectFormDetail();
                break;

        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;

            switch (cmdName)
            {
                case "btnRemoveTopicName":
                    GridView gvTopicList = (GridView)fvInsertForm.FindControl("gvTopicList");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsTopicNameList"];
                    dsContacts.Tables["dsTopicTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(gvTopicList, dsContacts.Tables["dsTopicTable"]);
                    if (dsContacts.Tables["dsTopicTable"].Rows.Count < 1)
                    {
                        gvTopicList.Visible = false;
                    }
                    break;
            }
        }
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    #endregion

    #region callService 
    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_roombooking callServicePostRoomBooking(string _cmdUrl, data_roombooking _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_roombooking = (data_roombooking)_funcTool.convertJsonToObject(typeof(data_roombooking), _localJson);


        return _data_roombooking;
    }

    protected data_drc callServicePostDRC(string _cmdUrl, data_drc _data_drc)
    {
        _localJson = _funcTool.convertObjectToJson(_data_drc);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_drc = (data_drc)_funcTool.convertJsonToObject(typeof(data_drc), _localJson);


        return _data_drc;
    }

    #endregion callService Functions

    #region Trigger 
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    #endregion Trigger
}
