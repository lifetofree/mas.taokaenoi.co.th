﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Xml.Serialization;

[Serializable]
[XmlRoot("DataHoldersDevices")]
public class DataHoldersDevices
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }

    [XmlElement("ReturnName")]
    public string ReturnName { get; set; }

    [XmlElement("ReturnDept")]
    public string ReturnDept { get; set; }

    [XmlElement("Return_Node")]
    public string Return_Node { get; set; }

    [XmlElement("Return_Actor")]
    public string Return_Actor { get; set; }

    [XmlElement("Return_Status")]
    public string Return_Status { get; set; }


    [XmlElement("ReturnDept_Accept")]
    public string ReturnDept_Accept { get; set; }

    [XmlElement("ReturnDate")]
    public string ReturnDate { get; set; }

    [XmlElement("ReturnDeviceCode")]
    public string ReturnDeviceCode { get; set; }

    [XmlElement("ReturnAsset")]
    public string ReturnAsset { get; set; }

    [XmlElement("ReturnType")]
    public string ReturnType { get; set; }

    [XmlElement("ReturnName_Own")]
    public string ReturnName_Own { get; set; }

    [XmlElement("ReturnName_Accept")]
    public string ReturnName_Accept { get; set; }

    [XmlElement("ReturnName_Before")]
    public string ReturnName_Before { get; set; }

    [XmlElement("ReturnIDX")]
    public string ReturnIDX { get; set; }

    [XmlElement("ReturnCount")]
    public string ReturnCount { get; set; }

    [XmlElement("return_code")]
    public string return_code { get; set; }

    [XmlElement("ReturnEmpNode")]
    public string ReturnEmpNode { get; set; }

    [XmlElement("ReturnEmail")]
    public string ReturnEmail { get; set; }

    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }

    public string CountRows { get; set; }

  
    [XmlElement("BoxHoldersDevices")]
    public HoldersDevices[] BoxHoldersDevices { get; set; }

    [XmlElement("BoxAddHoldersDevices")]
    public AddHoldersDevices[] BoxAddHoldersDevices { get; set; }
}

[Serializable]
public class HoldersDevices
{
    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("JobGradeIDX")]
    public int JobGradeIDX { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("NRDeptIDX")]
    public int NRDeptIDX { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("DeviceIDX")]
    public int DeviceIDX { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("EqtIDX")]
    public int EqtIDX { get; set; }
    
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("HolderIDX")]
    public int HolderIDX { get; set; }

    [XmlElement("HDEmpIDX")]
    public int HDEmpIDX { get; set; }


    [XmlElement("Search")]
   // private string _Search;
    public string Search { get; set; }


    [XmlElement("DeviceCode")]
    //private string _DeviceCode;
    public string DeviceCode { get; set; }

    [XmlElement("POCode")]
    //private string _POCode;
    public string POCode { get; set; }

    [XmlElement("AccessCode")]
    //private string _AccessCode;
    public string AccessCode { get; set; }

   [XmlElement("InsuranceName")]
  // private string _InsuranceName;
   public string InsuranceName { get; set; }

    [XmlElement("SerialNumber")]
   // private string _SerialNumber;
    public string SerialNumber { get; set; }

    [XmlElement("StatusDoc")]
   // private string _StatusDocr;
    public string StatusDoc { get; set; }

    [XmlElement("InsuranceExp")]
   // private string _InsuranceExp;
    public string InsuranceExp { get; set; }

    [XmlElement("PurchaseDate")]
  //  private string _PurchaseDate;
    public string PurchaseDate { get; set; }

    [XmlElement("Organization")]
   // private string _Organization;
    public string Organization { get; set; }

    [XmlElement("Dept")]
  //  private string _Dept;
    public string Dept { get; set; }


    [XmlElement("HDEmpFullName")]
   // private string _HDEmpFullName;
    public string HDEmpFullName { get; set; }


    [XmlElement("EqtName")]
   // private string _EqtName;
    public string EqtName { get; set; }

    [XmlElement("PosNameTH")]
  //  private string _PosNameTH;
    public string PosNameTH { get; set; }

    [XmlElement("SecNameTH")]
 //   private string _SecNameTH;
    public string SecNameTH { get; set; }

    [XmlElement("HolderStatus_")]
   // private string _HolderStatus_;
    public string HolderStatus_ { get; set; }

    [XmlElement("HolderStatus")]
  //  private string _HolderStatus;
    public string HolderStatus { get; set; }


    [XmlElement("Remark")]
  //  private string _Remark;
    public string Remark { get; set; }



    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("noidx")]
    public int noidx { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("status_approve")]
    public int status_approve { get; set; }

    [XmlElement("EmpName")]
   // private string _EmpName;
    public string EmpName { get; set; }

    [XmlElement("createdate")]
   // private string _createdate;
    public string createdate { get; set; }

    [XmlElement("actor_des")]
 //   private string _actor_des;
    public string actor_des { get; set; }

    [XmlElement("node_name")]
  //  private string _node_name;
    public string node_name { get; set; }

    [XmlElement("status_name")]
//    private string _status_name;
    public string status_name { get; set; }

    [XmlElement("comment")]
  //  private string _comment;
    public string comment { get; set; }

    [XmlElement("FullNameTH")]
  //  private string _FullNameTH;
    public string FullNameTH { get; set; }

    [XmlElement("NOrgIDX")]
    public int NOrgIDX { get; set; }

    [XmlElement("NRSecIDX")]
    public int NRSecIDX { get; set; }

    [XmlElement("NHDEmpIDX")]
    public int NHDEmpIDX { get; set; }

    [XmlElement("m0_tdidx")]
    public int m0_tdidx { get; set; }

    [XmlElement("detail")]
    public string detail { get; set; }

    [XmlElement("approver1")]
    public string approver1 { get; set; }

    [XmlElement("approver2")]
    public string approver2 { get; set; }

}

[Serializable]
public class AddHoldersDevices
{
    [XmlElement("NOrgIDX_add")]
    public int NOrgIDX_add { get; set; }

    [XmlElement("RDeptIDX_add")]
    public int RDeptIDX_add { get; set; }

    [XmlElement("NRDeptIDX_add")]
    public int NRDeptIDX_add { get; set; }

    [XmlElement("RSecIDX_add")]
    public int RSecIDX_add { get; set; }

    [XmlElement("NRSecIDX_add")]
    public int NRSecIDX_add { get; set; }

    [XmlElement("PosIDX_add")]
    public int PosIDX_add { get; set; }

    [XmlElement("NPosIDX_add")]
    public int NPosIDX_add { get; set; }

    [XmlElement("EqtIDX_add")]
    public int EqtIDX_add { get; set; }


    [XmlElement("EmpIDX_add")]
    public int EmpIDX_add { get; set; }

    [XmlElement("DeviceIDX_add")]
    public int DeviceIDX_add { get; set; }

    [XmlElement("HDEmpIDX_add")]
    public int HDEmpIDX_add { get; set; }

    [XmlElement("NHDEmpIDX_add")]
    public int NHDEmpIDX_add { get; set; }

    [XmlElement("HolderIDX")]
    public int HolderIDX { get; set; }

    [XmlElement("Remark")]
   // private string _Remark;
    public string Remark { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("noidx")]
    public int noidx { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("NEmpIDX1")]
    public int NEmpIDX1 { get; set; }

    [XmlElement("NEmpIDX2")]
    public int NEmpIDX2 { get; set; }

  
}
