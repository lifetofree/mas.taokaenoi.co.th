﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hc-doctor-history.aspx.cs" Inherits="websystem_MasterData_hc_doctor_history" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <div class="col-md-12">
        <asp:Literal ID="litdebug" runat="server"></asp:Literal>
    </div>

    <asp:MultiView ID="mvSystem" runat="server" ActiveViewIndex="0">
        <!--View Detail-->
        <asp:View ID="docDetailList" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btn_InsertDoctor" CssClass="btn btn-primary" data-original-title="เพิ่มข้อมูลแพทย์" data-toggle="tooltip" title="เพิ่มข้อมูลแพทย์" runat="server"
                    CommandName="cmdInsertDoctor" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูลแพทย์</asp:LinkButton>
            </div>

            <asp:GridView ID="gvDoctor"
                runat="server"
                AutoGenerateColumns="false"
                DataKeyNames="m0_doctor_idx"
                CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                HeaderStyle-CssClass="info"
                AllowPaging="true"
                PageSize="10"
                OnRowEditing="Master_RowEditing"
                OnRowUpdating="Master_RowUpdating"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                OnRowDataBound="Master_RowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div class="text-center">No result</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                        HeaderStyle-Font-Size="Small" ItemStyle-CssClass="text-center" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <%# (Container.DataItemIndex + 1) %>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ข้อมูลแพทย์" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="Detail_Doctor" runat="server"> 
                                    <p><b>ชื่อแพทย์ :</b> <%# Eval("doctor_name") %></p>
                                    <p><b>เลขที่ใบประกอบวิชาชีพ :</b> <%# Eval("card_number") %></p>
                                    <p><b>ชื่อหน่วยงานที่ตรวจสุขภาพ :</b> <%# Eval("health_authority_name") %></p>
                                </asp:Label>
                                <%--<asp:Label ID="employeeTypeName" runat="server" Text='<%# Eval("emp_type_name") %>' />--%>
                            </small>
                        </ItemTemplate>
                        <EditItemTemplate />
                        <FooterTemplate />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="รายละเอียดที่อยู่" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="Detail_DoctorLocation" runat="server"> 
                                    <p><b>ตั้งอยู่เลขที่ :</b> <%# Eval("location_name") %></p>
                                    <p><b>หมู่ที่ :</b> <%# Eval("village_no") %></p>
                                    <p><b>ถนน :</b> <%# Eval("road_name") %></p>
                                    <p><b>จังหวัด :</b> <%# Eval("ProvName") %></p>
                                    <p><b>อำเภอ :</b> <%# Eval("AmpName") %></p>
                                    <p><b>ตำบล :</b> <%# Eval("DistName") %></p>
                                    <p><b>เบอร์โทร :</b> <%# Eval("phone_number") %></p>
                                </asp:Label>
                                <%--<asp:Label ID="employeeTypeName" runat="server" Text='<%# Eval("emp_type_name") %>' />--%>
                            </small>
                        </ItemTemplate>
                        <EditItemTemplate />
                        <FooterTemplate />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="doctor_Status" runat="server" Text='<%# getStatus((int)Eval("doctor_status")) %>' />
                            </small>
                        </ItemTemplate>
                        <EditItemTemplate />
                        <FooterTemplate />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                        <ItemTemplate>
                            <asp:LinkButton ID="btn_UpdateDoctor" CssClass="btn btn-warning btn-sm" runat="server" CommandArgument='<%# Eval("m0_doctor_idx") %>' OnCommand="btnCommand" CommandName="cmdUpdateDoctor"
                                data-toggle="tooltip" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </asp:LinkButton>

                            <asp:LinkButton ID="btn_DelDoctor" CssClass="btn btn-danger btn-sm" runat="server" data-toggle="tooltip"
                                title="Remove" CommandName="cmdDelDoctor" OnCommand="btnCommand"
                                CommandArgument='<%# Eval("m0_doctor_idx") %>'
                                OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่?')">
                                <i class="fa fa-trash"></i>
                            </asp:LinkButton>

                        </ItemTemplate>
                        <EditItemTemplate />
                        <FooterTemplate />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>
        <!--View Detail-->

        <!--View Insert-->
        <asp:View ID="docDetailInsert" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btnCancelInsert" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                    CommandName="cmdCancel" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
            </div>

            <asp:FormView ID="fvInsertDataDoctor" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มข้อมูลแพทย์</h3>
                        </div>

                        <div class="panel-body">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="lbl_doctorname_insert" runat="server" Text="ชื่อแพทย์ผู้ตรวจ" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_doctorname_insert" runat="server" CssClass="form-control" placeholder="กรอกชื่อแพทย์ผู้ตรวจ ..." Enabled="true" />

                                                <asp:RequiredFieldValidator ID="Re_txt_doctorname_insert" runat="server"
                                                    ControlToValidate="txt_doctorname_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อแพทย์ผู้ตรวจ" ValidationGroup="Save" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txt_doctorname_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_doctorname_insert" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_card_number_insert" runat="server" Text="เลขที่ใบประกอบวิชาชีพ" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_card_number_insert" runat="server" CssClass="form-control" placeholder="กรอกเลขที่ใบประกอบวิชาชีพ ..." Enabled="true" />

                                                <asp:RequiredFieldValidator ID="Re_txt_card_number_insert" runat="server"
                                                    ControlToValidate="txt_card_number_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกเลขที่ใบประกอบวิชาชีพ" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_card_number_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_card_number_insert" Width="220" />

                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_health_authority_name_insert" runat="server" Text="ชื่อหน่วยงานที่ตรวจสุขภาพ" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_health_authority_name_insert" runat="server" CssClass="form-control" placeholder="กรอกชื่อหน่วยงานที่ตรวจสุขภาพ ..." Enabled="true" />

                                                <asp:RequiredFieldValidator ID="Re_txt_health_authority_name_insert" runat="server"
                                                    ControlToValidate="txt_health_authority_name_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อหน่วยงานที่ตรวจสุขภาพ" ValidationGroup="Save" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_health_authority_name_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_health_authority_name_insert" Width="220" />

                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_location_name_insert" runat="server" Text="ตั้งอยู่เลขที่" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_location_name_insert" runat="server" CssClass="form-control" placeholder="กรอกเลขที่ ..." Enabled="true" />

                                                <asp:RequiredFieldValidator ID="Re_txt_location_name_insert" runat="server"
                                                    ControlToValidate="txt_location_name_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกกรอกเลขที่" ValidationGroup="Save" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_location_name_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_location_name_insert" Width="220" />

                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_village_no_insert" runat="server" Text="หมู่ที่" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_village_no_insert" runat="server" CssClass="form-control" placeholder="กรอกหมู่ที่ ..." Enabled="true" />

                                                <asp:RequiredFieldValidator ID="Re_txt_village_no_insert" runat="server"
                                                    ControlToValidate="txt_village_no_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกหมู่ที่" ValidationGroup="Save" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_village_no_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_village_no_insert" Width="220" />

                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_road_name_insert" runat="server" Text="ถนน" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_road_name_insert" runat="server" CssClass="form-control" placeholder="กรอกถนน ..." Enabled="true" />

                                                <asp:RequiredFieldValidator ID="Re_txt_road_name_insert" runat="server"
                                                    ControlToValidate="txt_road_name_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกถนน" ValidationGroup="Save" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_road_name_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_road_name_insert" Width="220" />

                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_ddl_Province_insert" runat="server" Text="จังหวัด" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddl_Province_insert" runat="server"
                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Re_ddl_Province_insert" runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddl_Province_insert"
                                                    Display="None"
                                                    SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกจังหวัด"
                                                    ValidationGroup="Save" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validator_ddl_Province_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddl_Province_insert" Width="220" />

                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_ddl_Amphoe_insert" runat="server" Text="อำเภอ" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddl_Amphoe_insert" runat="server"
                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Re_ddl_Amphoe_insert" runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddl_Amphoe_insert"
                                                    Display="None"
                                                    SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกอำเภอ"
                                                    ValidationGroup="Save" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validator_ddl_Amphoe_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddl_Amphoe_insert" Width="220" />

                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_ddl_District_insert" runat="server" Text="ตำบล" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddl_District_insert" runat="server"
                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Re_ddl_District_insert" runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddl_District_insert"
                                                    Display="None"
                                                    SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกตำบล"
                                                    ValidationGroup="Save" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validator_ddl_District_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddl_District_insert" Width="220" />

                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_phone_number_insert" runat="server" Text="เบอร์โทร" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_phone_number_insert" runat="server" CssClass="form-control" placeholder="กรอกเบอร์โทร ..." Enabled="true" MaxLength="10" />

                                                <asp:RequiredFieldValidator ID="Re_txt_phone_number_insert" runat="server"
                                                    ControlToValidate="txt_phone_number_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกเบอร์โทร" ValidationGroup="Save" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_phone_number_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_phone_number_insert" Width="220" />

                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="btn_SaveDoctor" ValidationGroup="Save" CssClass="btn btn-success" Text="บันทึก" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="cmdSave" OnCommand="btnCommand"></asp:LinkButton>
                                                <asp:LinkButton ID="btn_CancelDoctor" CssClass="btn btn-danger" runat="server" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </InsertItemTemplate>
                <EditItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">แก้ไขข้อมูลแพทย์</h3>
                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <asp:Label ID="lbl_doctorname_insert" runat="server" Text="ชื่อแพทย์ผู้ตรวจ" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_doctorname_insert" runat="server" Text='<%# Eval("doctor_name")%>' CssClass="form-control" placeholder="กรอกชื่อแพทย์ผู้ตรวจ ..." Enabled="true" />

                                            <asp:RequiredFieldValidator ID="Re_txt_doctorname_insert" runat="server"
                                                ControlToValidate="txt_doctorname_insert" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อแพทย์ผู้ตรวจ" ValidationGroup="Save" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txt_doctorname_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_doctorname_insert" Width="220" />
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbl_card_number_insert" runat="server" Text="เลขที่ใบประกอบวิชาชีพ" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_card_number_insert" runat="server" Text='<%# Eval("card_number")%>' CssClass="form-control" placeholder="กรอกเลขที่ใบประกอบวิชาชีพ ..." Enabled="true" />

                                            <asp:RequiredFieldValidator ID="Re_txt_card_number_insert" runat="server"
                                                ControlToValidate="txt_card_number_insert" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกเลขที่ใบประกอบวิชาชีพ" ValidationGroup="Save" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_card_number_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_card_number_insert" Width="220" />

                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbl_health_authority_name_insert" runat="server" Text="ชื่อหน่วยงานที่ตรวจสุขภาพ" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_health_authority_name_insert" runat="server" Text='<%# Eval("health_authority_name")%>' CssClass="form-control" placeholder="กรอกชื่อหน่วยงานที่ตรวจสุขภาพ ..." Enabled="true" />

                                            <asp:RequiredFieldValidator ID="Re_txt_health_authority_name_insert" runat="server"
                                                ControlToValidate="txt_health_authority_name_insert" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อหน่วยงานที่ตรวจสุขภาพ" ValidationGroup="Save" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_health_authority_name_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_health_authority_name_insert" Width="220" />

                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbl_location_name_insert" runat="server" Text="ตั้งอยู่เลขที่" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_location_name_insert" runat="server" Text='<%# Eval("location_name")%>' CssClass="form-control" placeholder="กรอกเลขที่ ..." Enabled="true" />

                                            <asp:RequiredFieldValidator ID="Re_txt_location_name_insert" runat="server"
                                                ControlToValidate="txt_location_name_insert" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกกรอกเลขที่" ValidationGroup="Save" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_location_name_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_location_name_insert" Width="220" />

                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbl_village_no_insert" runat="server" Text="หมู่ที่" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_village_no_insert" runat="server" Text='<%# Eval("village_no")%>' CssClass="form-control" placeholder="กรอกหมู่ที่ ..." Enabled="true" />

                                            <asp:RequiredFieldValidator ID="Re_txt_village_no_insert" runat="server"
                                                ControlToValidate="txt_village_no_insert" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกหมู่ที่" ValidationGroup="Save" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_village_no_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_village_no_insert" Width="220" />

                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbl_road_name_insert" runat="server" Text="ถนน" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_road_name_insert" runat="server" Text='<%# Eval("road_name")%>' CssClass="form-control" placeholder="กรอกถนน ..." Enabled="true" />

                                            <asp:RequiredFieldValidator ID="Re_txt_road_name_insert" runat="server"
                                                ControlToValidate="txt_road_name_insert" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกถนน" ValidationGroup="Save" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_road_name_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_road_name_insert" Width="220" />

                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbl_ddl_Province_insert" runat="server" Text="จังหวัด" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:Label ID="lbl_ddl_Province_update" runat="server" Text='<%# Eval("ProvIDX") %>' Visible="false" />
                                            <asp:DropDownList ID="ddl_Province_insert" runat="server"
                                                CssClass="form-control fa-align-left" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Re_ddl_Province_insert" runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddl_Province_insert"
                                                Display="None"
                                                SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกจังหวัด"
                                                ValidationGroup="Save" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validator_ddl_Province_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddl_Province_insert" Width="220" />

                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbl_ddl_Amphoe_insert" runat="server" Text="อำเภอ" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:Label ID="lbl_ddl_Amphoe_update" runat="server" Text='<%# Eval("AmpIDX") %>' Visible="false" />
                                            <asp:DropDownList ID="ddl_Amphoe_insert" runat="server"
                                                CssClass="form-control fa-align-left" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Re_ddl_Amphoe_insert" runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddl_Amphoe_insert"
                                                Display="None"
                                                SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกอำเภอ"
                                                ValidationGroup="Save" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validator_ddl_Amphoe_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddl_Amphoe_insert" Width="220" />

                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbl_ddl_District_insert" runat="server" Text="ตำบล" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:Label ID="lbl_ddl_District_update" runat="server" Text='<%# Eval("DistIDX") %>' Visible="false" />
                                            <asp:DropDownList ID="ddl_District_insert" runat="server"
                                                CssClass="form-control fa-align-left" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Re_ddl_District_insert" runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddl_District_insert"
                                                Display="None"
                                                SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกตำบล"
                                                ValidationGroup="Save" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validator_ddl_District_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddl_District_insert" Width="220" />

                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbl_phone_number_insert" runat="server" Text="เบอร์โทร" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_phone_number_insert" runat="server" Text='<%# Eval("phone_number")%>' CssClass="form-control" placeholder="กรอกเบอร์โทร ..." Enabled="true" MaxLength="10" />

                                            <asp:RequiredFieldValidator ID="Re_txt_phone_number_insert" runat="server"
                                                ControlToValidate="txt_phone_number_insert" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกเบอร์โทร" ValidationGroup="Save" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txt_phone_number_insert" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_phone_number_insert" Width="220" />

                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbl_Doctor_Status" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddl_DoctorStatus_insert" runat="server" CssClass="form-control"
                                                SelectedValue='<%# Eval("doctor_status") %>'>
                                                <asp:ListItem Value="1" Text="ใช้งาน" />
                                                <asp:ListItem Value="0" Text="ยกเลิกใช้งาน" />
                                            </asp:DropDownList>

                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-3">
                                            <asp:LinkButton ID="btn_SaveUpdateDoctor" ValidationGroup="Save" CssClass="btn btn-success" Text="บันทึกการแก้ไข" data-toggle="tooltip" title="บันทึกการแก้ไข" runat="server" CommandName="cmdSaveUpdate" CommandArgument='<%# Eval("m0_doctor_idx") %>' OnCommand="btnCommand"></asp:LinkButton>

                                            <asp:LinkButton ID="btn_CancelDoctor" CssClass="btn btn-danger" runat="server" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>

                </EditItemTemplate>


            </asp:FormView>

        </asp:View>
        <!--View Insert-->



    </asp:MultiView>



</asp:Content>


