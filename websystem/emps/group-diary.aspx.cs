﻿using System;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_emps_group_diary : System.Web.UI.Page
{
   #region Init
   data_emps dataEmps = new data_emps();
   service_execute servExec = new service_execute();
   function_tool _funcTool = new function_tool();
   string centralizedConn = "conn_centralized";
   string masConn = "conn_mas";
   string empsRewardService = "empsRewardService";
   string empsodspeService = "empsodspeService";
   string empsGroupDiaryService = "empsGroupDiaryService";
   string _local_xml = null;
   #endregion Init

   #region Constant
   public static class Constants
   {
      public const int ALL = 0;
      public const int CREATE = 10;
      public const int UPDATE = 11;
      public const int SELECT_ALL = 20;
      public const int SELECT_WHERE = 21;
      public const int SELECT_WHERE_ORG = 22;
      public const int SELECT_WHERE_ORG_DEP = 23;
      public const int SELECT_WHERE_ORG_DEP_SEC = 24;
      public const int SELECT_WHERE_SEC = 25;
      public const int SELECT_WHERE_EXISTS_GROUP_DIARY_SECTION = 28;
      public const int SELECT_WHERE_EXISTS_GROUP_DIARY_NAME = 29;
      public const string VALID_FALSE = "101";

      public const int BAN = 30;
      public const int UNBAN = 31;
      public const int NUMBER_NULL = -1;
   }
   #endregion Constant

   #region Page Load
   protected void Page_Load(object sender, EventArgs e)
   {
      if (!IsPostBack)
      {
         getEmpLogin(int.Parse(Session["emp_idx"].ToString()));
         //actionIndex();
      }
   }
   #endregion Page Load

   #region Action
   protected void actionIndex()
   {
      group_diary objGroupDiary = new group_diary();
      dataEmps.emps_group_diary_action = new group_diary[1];
      objGroupDiary.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
      dataEmps.emps_group_diary_action[0] = objGroupDiary;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsGroupDiaryService, dataEmps, Constants.SELECT_ALL);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      setGridData(gvGroupDiary, dataEmps.emps_group_diary_action);
   }

   protected void actionCreate()
   {
      Page.Validate();
      if (Page.IsValid == true)
      {
         group_diary objGroupDiary = new group_diary();
         dataEmps.emps_group_diary_action = new group_diary[1];
         objGroupDiary.rsec_idx_ref = int.Parse(ddlGroupDiarySec.SelectedValue);
         objGroupDiary.group_diary_name = txtGroupDiaryName.Text;
         objGroupDiary.group_diary_status = int.Parse(ddlGroupDiaryStatus.SelectedValue);
         objGroupDiary.group_diary_created_by = int.Parse(ViewState["EmpIDX"].ToString());
         objGroupDiary.group_diary_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
         dataEmps.emps_group_diary_action[0] = objGroupDiary;
         _local_xml = servExec.actionExec(masConn, "data_emps", empsGroupDiaryService, dataEmps, Constants.CREATE);
         dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
         if (dataEmps.return_code.ToString() == "0")
         {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
         }
         else
         {
            _funcTool.showAlert(this, "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง");
         }
      }
   }

   protected void actionToUpdate(int groupIdx)
   {
      group_diary objGroupDiary = new group_diary();
      dataEmps.emps_group_diary_action = new group_diary[1];
      objGroupDiary.m0_group_diary_idx = groupIdx;
      dataEmps.emps_group_diary_action[0] = objGroupDiary;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsGroupDiaryService, dataEmps, Constants.SELECT_WHERE);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      //dataE
   }

   protected void actionUpdate(int m0_group_diary_idx, int rsec_idx_ref, string group_diary_name, int group_diary_status)
   {
      group_diary objGroupDiary = new group_diary();
      dataEmps.emps_group_diary_action = new group_diary[1];
      objGroupDiary.m0_group_diary_idx = m0_group_diary_idx;
      objGroupDiary.rsec_idx_ref = rsec_idx_ref;
      objGroupDiary.group_diary_name = group_diary_name;
      objGroupDiary.group_diary_status = group_diary_status;
      objGroupDiary.group_diary_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
      dataEmps.emps_group_diary_action[0] = objGroupDiary;
      servExec.actionExec(masConn, "data_emps", empsGroupDiaryService, dataEmps, Constants.UPDATE);
   }

   protected void actionBan(int id)
   {
      group_diary objGroupDiary = new group_diary();
      dataEmps.emps_group_diary_action = new group_diary[1];
      objGroupDiary.m0_group_diary_idx = id;
      objGroupDiary.group_diary_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
      dataEmps.emps_group_diary_action[0] = objGroupDiary;
      servExec.actionExec(masConn, "data_emps", empsGroupDiaryService, dataEmps, Constants.BAN);
   }

   protected void actionUnban(int id)
   {
      group_diary objGroupDiary = new group_diary();
      dataEmps.emps_group_diary_action = new group_diary[1];
      objGroupDiary.m0_group_diary_idx = id;
      objGroupDiary.group_diary_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
      dataEmps.emps_group_diary_action[0] = objGroupDiary;
      servExec.actionExec(masConn, "data_emps", empsGroupDiaryService, dataEmps, Constants.UNBAN);
   }
   #endregion Action

   #region btnCommand
   protected void btnCommand(object sender, CommandEventArgs e)
   {
      string cmdName = e.CommandName.ToString();
      string cmdArg = e.CommandArgument.ToString();
      switch (cmdName)
      {
         case "btnToInsert":
            divInsertForm.Visible = true;
            divGvGroupDiary.Visible = false;
            getDDLOrgToDeptToSecToPosToEmp(ddlGroupDiaryOrg, ddlGroupDiaryDept, ddlGroupDiarySec);
            break;

         case "btnInsert":
            actionCreate();
            break;

         case "btnBan":
            actionBan(int.Parse(cmdArg));
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnUnBan":
            actionUnban(int.Parse(cmdArg));
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnCancel":
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;
      }
   }
   #endregion btnCommand

   #region Get data from DB
   protected void getEmpLogin(int empId)
   {
      odspe objODSPE = new odspe();
      dataEmps.emps_odspe_action = new odspe[1];
      objODSPE.emp_idx = empId;
      dataEmps.emps_odspe_action[0] = objODSPE;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      ViewState["EmpIDX"] = empId;
   }

   protected void getDDLOrgToDeptToSecToPosToEmp(DropDownList ddlOrg, DropDownList ddlDept = null, DropDownList ddlSec = null, DropDownList ddlPos = null, DropDownList ddlEmp = null)
   {
      odspe objODSPE = new odspe();
      dataEmps.emps_odspe_action = new odspe[1];
      dataEmps.emps_odspe_action[0] = objODSPE;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE_ORG);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      ddlOrg.Items.Clear();
      ddlOrg.AppendDataBoundItems = true;
      ddlOrg.Items.Add(new ListItem("-- เลือกองค์กร --", String.Empty));
      ddlOrg.DataSource = dataEmps.emps_odspe_action;
      ddlOrg.DataTextField = "OrgNameTH";
      ddlOrg.DataValueField = "OrgIDX";
      ddlOrg.DataBind();
      if (ddlDept != null)
      {
         ddlDept.Items.Clear();
         ddlDept.Items.Add(new ListItem("-- เลือกฝ่าย --", String.Empty));
      }
      if (ddlSec != null)
      {
         ddlSec.Items.Clear();
         ddlSec.Items.Add(new ListItem("-- เลือกแผนก --", String.Empty));
      }
      if (ddlPos != null)
      {
         ddlPos.Items.Clear();
         ddlPos.Items.Add(new ListItem("-- เลือกตำแหน่ง --", String.Empty));
      }
      if (ddlEmp != null)
      {
         ddlEmp.Items.Clear();
         ddlEmp.Items.Add(new ListItem("-- เลือกพนักงาน --", String.Empty));
      }
   }
   #endregion Get data from DB

   #region Custom Functions
   protected void setGridData(GridView gvName, Object obj)
   {
      gvName.DataSource = obj;
      gvName.DataBind();
   }

   protected string getStatus(int status)
   {
      if (status == 1)
      {
         return "<span class='status-online' data-toggle='tooltip' title='ONLINE'><i class='fa fa-check-circle fa-lg'></i></span>";
      }
      else
      {
         return "<span class='status-offline' data-toggle='tooltip' title='OFFLINE'><i class='fa fa-times-circle fa-lg'></i></span>";
      }
   }

   protected void onPageIndexChanging(object sender, GridViewPageEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvGroupDiary":
            gvName.PageIndex = e.NewPageIndex;
            gvName.DataBind();
            actionIndex();
            break;
      }
   }

   protected void onRowDatabound(object sender, GridViewRowEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvGroupDiary":
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
               var groupDiaryStatus = (e.Row.Cells[3].FindControl("groupDiaryStatus") as Label).Text;
               var btnBan = e.Row.Cells[4].FindControl("btnBan");
               var btnUnBan = e.Row.Cells[4].FindControl("btnUnBan");
               if (ViewState["EmpIDX"].ToString() == "4839")
               {
                  if (groupDiaryStatus == "1")
                  {
                     btnBan.Visible = true;
                     btnUnBan.Visible = false;
                  }
                  else
                  {
                     btnBan.Visible = false;
                     btnUnBan.Visible = true;
                  }
               }
               else
               {
                  btnBan.Visible = true;
                  btnUnBan.Visible = false;
               }
            }
            break;
      }
   }

   protected void onSelectedIndexChanged(object sender, EventArgs e)
   {
      if (sender is DropDownList)
      {
         var ddlName = (DropDownList)sender;
         switch (ddlName.ID)
         {
            case "ddlGroupDiaryOrg":
               odspe objGroupDiaryO = new odspe();
               dataEmps.emps_odspe_action = new odspe[1];
               objGroupDiaryO.organization_idx = int.Parse(ddlGroupDiaryOrg.SelectedValue);
               dataEmps.emps_odspe_action[0] = objGroupDiaryO;
               _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE_ORG_DEP);
               dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
               if (ddlGroupDiaryOrg.SelectedValue == String.Empty)
               {
                  ddlGroupDiaryDept.Items.Clear();
                  ddlGroupDiaryDept.AppendDataBoundItems = true;
                  ddlGroupDiaryDept.Items.Add(new ListItem("-- เลือกฝ่าย --", String.Empty));
                  //
                  ddlGroupDiarySec.Items.Clear();
                  ddlGroupDiarySec.AppendDataBoundItems = true;
                  ddlGroupDiarySec.Items.Add(new ListItem("-- เลือกแผนก --", String.Empty));
               }
               else
               {
                  ddlGroupDiaryDept.Items.Clear();
                  ddlGroupDiaryDept.AppendDataBoundItems = true;
                  ddlGroupDiaryDept.Items.Add(new ListItem("-- เลือกฝ่าย --", String.Empty));
                  ddlGroupDiaryDept.DataSource = dataEmps.emps_odspe_action;
                  ddlGroupDiaryDept.DataTextField = "DeptNameTH";
                  ddlGroupDiaryDept.DataValueField = "RDeptIDX";
                  ddlGroupDiaryDept.DataBind();
                  //
                  ddlGroupDiarySec.Items.Clear();
                  ddlGroupDiarySec.AppendDataBoundItems = true;
                  ddlGroupDiarySec.Items.Add(new ListItem("-- เลือกแผนก --", String.Empty));
               }
               break;

            case "ddlGroupDiaryDept":
               odspe objGroupDiaryOD = new odspe();
               dataEmps.emps_odspe_action = new odspe[1];
               objGroupDiaryOD.organization_idx = int.Parse(ddlGroupDiaryOrg.SelectedValue);
               objGroupDiaryOD.department_idx = int.Parse(ddlGroupDiaryDept.SelectedValue);
               dataEmps.emps_odspe_action[0] = objGroupDiaryOD;
               _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE_ORG_DEP_SEC);
               dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
               if (ddlGroupDiaryDept.SelectedValue == String.Empty)
               {
                  ddlGroupDiarySec.Items.Clear();
                  ddlGroupDiarySec.AppendDataBoundItems = true;
                  ddlGroupDiarySec.Items.Add(new ListItem("-- เลือกแผนก --", String.Empty));
               }
               else
               {
                  ddlGroupDiarySec.Items.Clear();
                  ddlGroupDiarySec.AppendDataBoundItems = true;
                  ddlGroupDiarySec.Items.Add(new ListItem("-- เลือกแผนก --", String.Empty));
                  ddlGroupDiarySec.DataSource = dataEmps.emps_odspe_action;
                  ddlGroupDiarySec.DataTextField = "SecNameTH";
                  ddlGroupDiarySec.DataValueField = "RSecIDX";
                  ddlGroupDiarySec.DataBind();
               }
               break;
         }
      }
   }

   //protected void checkExistsGroupDiaryName(object sender, ServerValidateEventArgs e)
   //{
   //   group_diary objGroupDiary = new group_diary();
   //   dataEmps.emps_group_diary_action = new group_diary[1];
   //   objGroupDiary.group_diary_name = txtGroupDiaryName.Text.Trim();
   //   dataEmps.emps_group_diary_action[0] = objGroupDiary;
   //   _local_xml = servExec.actionExec(masConn, "data_emps", empsGroupDiaryService, dataEmps, Constants.SELECT_WHERE_EXISTS_GROUP_DIARY_NAME);
   //   dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
   //   if (dataEmps.return_code.ToString() == Constants.VALID_FALSE)
   //   {
   //      e.IsValid = false;
   //   }
   //   else
   //   {
   //      e.IsValid = true;
   //   }
   //}

   protected void checkExistsGroupDiarySection(object sender, ServerValidateEventArgs e)
   {
      group_diary objGroupDiary = new group_diary();
      dataEmps.emps_group_diary_action = new group_diary[1];
      objGroupDiary.rsec_idx_ref = int.Parse(ddlGroupDiarySec.SelectedValue);
      dataEmps.emps_group_diary_action[0] = objGroupDiary;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsGroupDiaryService, dataEmps, Constants.SELECT_WHERE_EXISTS_GROUP_DIARY_SECTION);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      if (dataEmps.return_code.ToString() == Constants.VALID_FALSE)
      {
         e.IsValid = false;
      }
      else
      {
         e.IsValid = true;
      }
   }
   #endregion Custom Functions
}
