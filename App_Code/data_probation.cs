﻿using System;
using System.Xml.Serialization;


[Serializable]
[XmlRoot("data_probation")]
public class data_probation
{
    [XmlElement("return_code")]
    public string return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("m0_type_evaluation_list")]
    public m0_type_evaluation_detail[] m0_type_evaluation_list { get; set; }
    [XmlElement("m0_name_evaluation_list")]
    public m0_name_evaluation_detail[] m0_name_evaluation_list { get; set; }
    [XmlElement("u0_name_probation_list")]
    public u0_name_probation_detail[] u0_name_probation_list { get; set; }
    [XmlElement("m0_name_assessor_list")]
    public m0_name_assessor_detail[] m0_name_assessor_list { get; set; }
    [XmlElement("u0_assessment_list")]
    public u0_assessment_detail[] u0_assessment_list { get; set; }
    [XmlElement("u0_result_assessment_list")]
    public u0_result_assessment_detail[] u0_result_assessment_list { get; set; }
}

#region m0_type_evaluation_detail

[Serializable]
public class m0_type_evaluation_detail
{
    [XmlElement("m0_type_idx")]
    public int m0_type_idx { get; set; }

    [XmlElement("type_evaluation_name")]
    public string type_evaluation_name { get; set; }

    [XmlElement("type_evaluation_ststus")]
    public int type_evaluation_ststus { get; set; }
}
#endregion

#region m0_name_evaluation_detail
[Serializable]
public class m0_name_evaluation_detail
{
    [XmlElement("m0_topics_idx")]
    public int m0_topics_idx { get; set; }

    [XmlElement("topics_name")]
    public string topics_name { get; set; }

    [XmlElement("topics_status")]
    public int topics_status { get; set; }

    [XmlElement("root_m0_topics_idx")]
    public int root_m0_topics_idx { get; set; }

    [XmlElement("emps_type_probation")]
    public int emps_type_probation { get; set; }

    [XmlElement("emps_idx_topics")]
    public int emps_idx_topics { get; set; }


}

#endregion

#region u0_name_probation_detail
[Serializable]
public class u0_name_probation_detail
{
    [XmlElement("u0_probation_idx")]
    public int u0_probation_idx { get; set; }

    [XmlElement("emp_probation_idx")]
    public int emp_probation_idx { get; set; }

    [XmlElement("org_probation_idx")]
    public int org_probation_idx { get; set; }

    [XmlElement("sec_probation_idx")]
    public int sec_probation_idx { get; set; }

    [XmlElement("dept_probation_idx")]
    public int dept_probation_idx { get; set; }

    [XmlElement("pos_probation_idx")]
    public int pos_probation_idx { get; set; }

    [XmlElement("status_probation")]
    public int status_probation { get; set; }

    [XmlElement("node_idx_probation")]
    public int node_idx_probation { get; set; }

    [XmlElement("actor_idx_probation")]
    public int actor_idx_probation { get; set; }

    [XmlElement("emp_assessor1_idx")]
    public int emp_assessor1_idx { get; set; }

    [XmlElement("emp_idx_hr_director")]
    public int emp_idx_hr_director { get; set; }

    [XmlElement("emp_assessor2_idx")]
    public int emp_assessor2_idx { get; set; }

    [XmlElement("jobgrade_level")]
    public int jobgrade_level { get; set; }

    [XmlElement("emps_fullname_probation")]
    public string emps_fullname_probation { get; set; }

    [XmlElement("emps_code_probation")]
    public string emps_code_probation { get; set; }

    [XmlElement("name_org_probation")]
    public string name_org_probation { get; set; }

    [XmlElement("name_sec_probation")]
    public string name_sec_probation { get; set; }

    [XmlElement("name_dept_probation")]
    public string name_dept_probation { get; set; }

    [XmlElement("name_pos_probation")]
    public string name_pos_probation { get; set; }

    [XmlElement("date_start_probation")]
    public string date_start_probation { get; set; }

    [XmlElement("date_end_probation")]
    public string date_end_probation { get; set; }

    [XmlElement("date_doing_evaluation")]
    public string date_doing_evaluation { get; set; }

    [XmlElement("actor_probation_name")]
    public string actor_probation_name { get; set; }

    [XmlElement("node_probation_name")]
    public string node_probation_name { get; set; }

    [XmlElement("node_probation_status")]
    public string node_probation_status { get; set; }
}
#endregion

#region m0_name_assessor_detail
[Serializable]
public class m0_name_assessor_detail
{
    [XmlElement("emp_assessor_idx")]
    public int emp_assessor_idx { get; set; }

    [XmlElement("name_assessor")]
    public string name_assessor { get; set; }

}
#endregion

#region m0_name_assessor_detail
[Serializable]
public class u0_assessment_detail
{
    [XmlElement("u0_assessment_idx")]
    public int u0_assessment_idx { get; set; }

    [XmlElement("emp_idx_assessor1")]
    public int emp_idx_assessor1 { get; set; }

    [XmlElement("emp_idx_assessor2")]
    public int emp_idx_assessor2 { get; set; }

    [XmlElement("emp_idx_probation")]
    public int emp_idx_probation { get; set; }

    [XmlElement("probation_status")]
    public int probation_status { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("emp_idx_hr_director")]
    public int emp_idx_hr_director { get; set; }

    [XmlElement("probation_datetime")]
    public string probation_datetime { get; set; }

    [XmlElement("probation_createdate")]
    public string probation_createdate { get; set; }

    [XmlElement("emps_code_probation")]
    public string emps_code_probation { get; set; }

    [XmlElement("emps_fullname_probation")]
    public string emps_fullname_probation { get; set; }

    [XmlElement("name_pos_probation")]
    public string name_pos_probation { get; set; }

    [XmlElement("date_start_probation")]
    public string date_start_probation { get; set; }

}
#endregion

#region u0_result_assessment_detail
[Serializable]
public class u0_result_assessment_detail
{
    [XmlElement("idTopics")]
    public int idTopics { get; set; }
    [XmlElement("average_score_result")]
    public float average_score_result { get; set; }
    [XmlElement("full_score_result")]
    public int full_score_result { get; set; }
    [XmlElement("status_result")]
    public int status_result { get; set; }
    [XmlElement("emp_idx_result")]
    public int emp_idx_result { get; set; }
    [XmlElement("type_idx_evaluation")]
    public int type_idx_evaluation { get; set; }
    [XmlElement("note_assessment")]
    public string note_assessment { get; set; }
    [XmlElement("number_of_assessment")]
    public int number_of_assessment { get; set; }
    [XmlElement("score_result")]
    public int score_result { get; set; }
    [XmlElement("emp_idx_assessor")]
    public int emp_idx_assessor { get; set; }


}

#endregion