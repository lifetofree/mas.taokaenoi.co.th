<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="visitors.aspx.cs" Inherits="websystem_visitors_visitors" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbList" runat="server" CommandName="navList" OnCommand="navCommand"> รายการทั่วไป</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="navCreate" OnCommand="navCommand"> สร้างรายการ</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbApprove" runat="server" CommandName="navApprove" OnCommand="navCommand"> รายการที่รออนุมัติ</asp:LinkButton>
                        </li>
                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbApprove_zone" runat="server" CommandName="navApprove_zone" OnCommand="navCommand"> รายการที่รออนุมัติโซน</asp:LinkButton>
                        </li>
                        <li id="li4" runat="server">
                            <asp:LinkButton ID="lbReport" runat="server" CommandName="navReport" OnCommand="navCommand" Visible="False"> รายงาน</asp:LinkButton>
                        </li>
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->
            </div>
        </nav>
    </div>
    <!--tab menu-->
    <div class="row">
        <!--multiview-->
        <asp:MultiView ID="mvSystem" runat="server">
            <asp:View ID="viewList" runat="server">
                <div class="col-md-12">
                    <div class="panel panel-default" id="div_list_heading" runat="server" visible="true">
                        <%--  style="display: none;" --%>
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <asp:Literal ID="litListHeadingTitle" runat="server" Text="ค้นหารายการ"></asp:Literal></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">หมายเลขบัตรประชาชน :</label>
                                    <div class="col-md-3">
                                        <asp:TextBox ID="tbSearchIdCard" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <label class="col-md-2 control-label">ชื่อผู้มาติดต่อ :</label>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="tbSearchName" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">ชื่อผู้ที่เข้าพบ :</label>
                                    <div class="col-md-3">
                                        <asp:TextBox ID="tbSearchVisitName" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-4">
                                        <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearch" CommandArgument="0">
                                            <i class="fas fa-search" aria-hidden="true"></i>&nbsp;ค้นหา</asp:LinkButton>
                                        <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                            <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                    </div>
                                    <label class="col-md-1 control-label"></label>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:GridView ID="gvDocList" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnRowDataBound="gvRowDataBound"
                        DataKeyNames="u0_idx"
                        OnPageIndexChanging="gvPageIndexChanging"
                        AllowPaging="true" PageSize="10">
                        <HeaderStyle CssClass="info" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                            FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                    <asp:HiddenField ID="hfFlagSafety" runat="server" Value='<%# Eval("flag_safety") %>'></asp:HiddenField>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่เข้าพบ">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitDate" runat="server" Text='<%# Eval("visit_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อผู้ที่เข้าพบ">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitEmpNameTh" runat="server" Text='<%# Eval("visit_emp_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่ทำรายการ">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ผู้ทำรายการ">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpNameTh" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ">
                                <ItemTemplate>
                                    <asp:Label ID="lblDocStatus" runat="server" Text='<%# Eval("doc_status_name") + " " + Eval("node_name") + " โดย " + Eval("actor_name") %>'></asp:Label>
                                    <asp:Literal ID="litFlagSafety" runat="server" Visible="False"><span class="" style="background-color:#FF0000; color:#FFFFFF;">***(จป)***</span></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="การจัดการ">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbEdit" runat="server" CssClass="btn btn-sm btn-info" OnCommand="btnCommand" CommandName="cmdEdit" CommandArgument='<%# Eval("u0_idx") %>'><i class="far fa-file-alt"></i>&nbsp;รายละเอียด</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </asp:View>
            <asp:View ID="viewCreate" runat="server">
                <div class="col-md-12">
                    <div class="panel panel-info" id="div_create_heading" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <asp:Literal ID="litCreateHeadingTitle" runat="server" Text="ข้อมูลผู้ทำรายการ"></asp:Literal></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <asp:FormView ID="fvEmpProfile" runat="server" Width="100%" DefaultMode="ReadOnly" OnDataBound="fvDataBound">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hfRdeptIdx" runat="server" Value='<%# Eval("rdept_idx")%>'></asp:HiddenField>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">รหัสพนักงาน :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Enabled="False" Text='<%# Eval("emp_code") %>'></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">ชื่อผู้ทำรายการ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Enabled="False" Text='<%# Eval("emp_name_th") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">บริษัท :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbOrgNameTh" runat="server" CssClass="form-control" Enabled="False" Text='<%# Eval("org_name_th") %>'></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">ฝ่าย :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbDeptNameTh" runat="server" CssClass="form-control" Enabled="False" Text='<%# Eval("dept_name_th") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">แผนก :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbSecNameTh" runat="server" CssClass="form-control" Enabled="False" Text='<%# Eval("sec_name_th") %>'></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">ตำแหน่ง :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbPosNameTh" runat="server" CssClass="form-control" Enabled="False" Text='<%# Eval("pos_name_th") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-success" id="div_create_form" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <asp:Literal ID="litCreateFormTitle" runat="server" Text="สร้างรายการ"></asp:Literal></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <asp:FormView ID="fvDocCreate" runat="server" Width="100%" DefaultMode="Insert" OnDataBound="fvDataBound">
                                    <InsertItemTemplate>
                                        <asp:HiddenField ID="hfU0Idx" runat="server" Value="0"></asp:HiddenField>
                                        <asp:HiddenField ID="hfNodeIdx" runat="server" Value="1"></asp:HiddenField>
                                        <asp:HiddenField ID="hfActorIdx" runat="server" Value="1"></asp:HiddenField>
                                        <asp:HiddenField ID="hfflag_loading" runat="server" Value="0"></asp:HiddenField>
                                        <asp:HiddenField ID="hfflag_walkin" runat="server" Value="0"></asp:HiddenField>
                                        <asp:HiddenField ID="hfflag_arrived" runat="server" Value="0"></asp:HiddenField>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">วันที่ทำรายการ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbDocCreateDate" runat="server" CssClass="form-control" placeholder="วันที่ทำรายการ" Enabled="False" Text='<%# DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") %>'></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">วันที่เข้าพบ<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitDate" runat="server" CssClass="form-control date-datepicker" placeholder="วันที่เข้าพบ"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredtbVisitDate"
                                                    ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="tbVisitDate" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกวันที่เข้าพบ"
                                                    ValidationExpression="กรุณากรอกวันที่เข้าพบ"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortbVisitDate" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredtbVisitDate" Width="160" />

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">หัวข้อ<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitTitle" runat="server" CssClass="form-control" placeholder="หัวข้อ" MaxLength="100"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredtbVisitTitle"
                                                    ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="tbVisitTitle" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกหัวข้อ"
                                                    ValidationExpression="กรุณากรอกหัวข้อ"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredtbVisitTitle" Width="160" />

                                            </div>
                                            <label class="col-md-2 control-label">รายละเอียด<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitDetail" runat="server" CssClass="form-control" placeholder="รายละเอียด" MaxLength="500"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredtbVisitDetail"
                                                    ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="tbVisitDetail" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกรายละเอียด"
                                                    ValidationExpression="กรุณากรอกรายละเอียด"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredtbVisitDetail" Width="160" />

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ชื่อผู้ที่ต้องการเข้าพบ<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlEmpList" runat="server" CssClass="form-control"></asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredddlEmpList"
                                                    ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="ddlEmpList" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกชื่อผู้ที่ต้องการเข้าพบ"
                                                    ValidationExpression="กรุณาเลือกชื่อผู้ที่ต้องการเข้าพบ"
                                                    SetFocusOnError="true"
                                                    InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredddlEmpList" Width="160" />

                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                                <asp:Checkbox ID="cbFlagSafety" runat="server" CssClass="form-check-input" Text="Hot work"></asp:Checkbox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ประเภทผู้มาติดต่อ<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlVisitorType" runat="server" CssClass="form-control"></asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredddlVisitorType"
                                                    ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="ddlVisitorType" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกประเภทผู้มาติดต่อ"
                                                    ValidationExpression="กรุณาเลือกประเภทผู้มาติดต่อ"
                                                    SetFocusOnError="true"
                                                    InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredddlVisitorType" Width="160" />
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Zone ที่มาติดต่อ<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:GridView ID="gvGroupList" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive" DataKeyNames="m0_idx">
                                                    <HeaderStyle CssClass="info" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbGroupSelected" runat="server" AutoPostBack="true" OnCheckedChanged="cbCheckedChanged" Text='<%# Eval("m0_idx") + "|" + Eval("group_name") %>' CssClass="hiddenText" Style="color: transparent;"></asp:CheckBox>
                                                                <asp:HiddenField ID="hfM0Idx" runat="server" Value='<%# Eval("m0_idx")%>'></asp:HiddenField>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Zone">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGroupName" runat="server" Text='<%# Eval("group_name") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <label class="col-md-2"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <label class="col-md-4 control-label-static">รายละเอียดผู้มาติดต่อ</label>
                                            <label class="col-md-2 control-label"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-2 control-label" style="padding-top: 0px;">
                                                <asp:DropDownList ID="ddlIdentifyType" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="หมายเลขบัตรประชาชน * :"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="หมายเลขพาสปอร์ต * :"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitCardId" runat="server" CssClass="form-control" placeholder="หมายเลขบัตรประชาชน/พาสปอร์ต" MaxLength="13"></asp:TextBox>
                                            
                                                <asp:RequiredFieldValidator ID="RequiredtbVisitCardId"
                                                    ValidationGroup="AddRow" runat="server" Display="None"
                                                    ControlToValidate="tbVisitCardId" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกหมายเลขบัตรประชาชน/พาสปอร์ต"
                                                    ValidationExpression="กรุณาเลือกหมายเลขบัตรประชาชน/พาสปอร์ต" SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtbVisitCardId" Width="160" />

                                            </div>
                                            <label class="col-md-2 control-label">หมายเลขโทรศัพท์ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitPhoneNo" runat="server" CssClass="form-control" placeholder="หมายเลขโทรศัพท์" MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ชื่อ<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitFirstname" runat="server" CssClass="form-control" placeholder="ชื่อ"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredtbVisitFirstname"
                                                    ValidationGroup="AddRow" runat="server" Display="None"
                                                    ControlToValidate="tbVisitFirstname" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกชื่อ"
                                                    ValidationExpression="กรุณากรอกชื่อ"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredtbVisitFirstname" Width="160" />

                                            </div>
                                            <label class="col-md-2 control-label">นามสกุล<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitLastname" runat="server" CssClass="form-control" placeholder="นามสกุล"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredtbVisitLastname"
                                                    ValidationGroup="AddRow" runat="server" Display="None"
                                                    ControlToValidate="tbVisitLastname" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกนามสกุล"
                                                    ValidationExpression="กรุณากรอกนามสกุล"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredtbVisitLastname" Width="160" />

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand"
                                                    CommandName="cmdItemInsert" CommandArgument="0"
                                                    ValidationGroup="AddRow"><i
                                                class="fas fa-plus"></i>&nbsp;เพิ่มข้อมูลผู้มาติดต่อ</asp:LinkButton>
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-10">
                                                <asp:GridView ID="gvVisitorList" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                                    OnRowDataBound="gvRowDataBound">
                                                    <HeaderStyle CssClass="info" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="หมายเลขบัตรประชาชน/พาสปอร์ต">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblIdentifyNo" runat="server" Text='<%# Eval("visit_identify_no") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="หมายเลขโทรศัพท์">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPhoneNo" runat="server" Text='<%# Eval("visit_phone_no") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ชื่อ">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVistFirstname" runat="server" Text='<%# Eval("visit_firstname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="นามสกุล">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVistLastname" runat="server" Text='<%# Eval("visit_lastname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="การจัดการ">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbDelete" runat="server" CssClass="btn btn-xs btn-danger" OnCommand="btnCommand" CommandName="cmdItemDelete" CommandArgument='<%# Eval("visit_identify_no") %>'><i class="far fa-trash-alt"></i></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">หมายเลขทะเบียนรถ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVehiclePlateNo" runat="server" CssClass="form-control" placeholder="หมายเลขทะเบียนรถ"></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">จังหวัด :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlProvince" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="1"
                                                    ValidationGroup="SaveAdd">
                                                    <i class="fas fa-save"></i>&nbsp;บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-times"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                            </div>
                                            <label class="col-md-6"></label>
                                        </div>
                                    </InsertItemTemplate>
                                    <EditItemTemplate>
                                        <asp:HiddenField ID="hfU0Idx" runat="server" Value='<%# Eval("u0_idx") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hfNodeIdx" runat="server" Value='<%# Eval("node_idx") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hfActorIdx" runat="server" Value='<%# Eval("actor_idx") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hfzcount" runat="server" Value='<%# Eval("zcount") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hfflag_loading" runat="server" Value='<%# Eval("flag_loading") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hfflag_walkin" runat="server" Value='<%# Eval("flag_walkin") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hfvisit_emp_idx" runat="server" Value='<%# Eval("visit_emp_idx") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hfflag_arrived" runat="server" Value='<%# Eval("flag_arrived") %>'></asp:HiddenField>

<%--                                        <asp:Label ID="HiddenField1" runat="server" Text='<%# Eval("node_idx") %>'></asp:Label>
                                        <br />
                                        <asp:Label ID="HiddenField2" runat="server" Text='<%# Eval("actor_idx") %>'></asp:Label>--%>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">วันที่ทำรายการ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbDocCreateDate" runat="server" CssClass="form-control" placeholder="วันที่ทำรายการ" Enabled="False" Text='<%# Eval("create_date") %>'></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">วันที่เข้าพบ<span class="text-danger">* :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitDate" runat="server" CssClass="form-control" placeholder="วันที่เข้าพบ" Enabled="False" Text='<%# Eval("visit_date") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">หัวข้อ<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitTitle" runat="server" CssClass="form-control" placeholder="หัวข้อ" Enabled="False" Text='<%# Eval("visit_title") %>'></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">รายละเอียด<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitDetail" runat="server" CssClass="form-control" placeholder="รายละเอียด" Enabled="False" Text='<%# Eval("visit_detail") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ชื่อผู้ที่ต้องการเข้าพบ<span class="text-danger">* :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlEmpList" runat="server" CssClass="form-control" Enabled="False"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                                <asp:Checkbox ID="cbFlagSafety" runat="server" CssClass="form-check-input" Text="Hot work"></asp:Checkbox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Zone ที่มาติดต่อ<span class="text-danger">* :</label>
                                            <div class="col-md-4">
                                                <asp:GridView ID="gvGroupList" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive" OnRowDataBound="gvRowDataBound" DataKeyNames="m0_idx">
                                                    <HeaderStyle CssClass="info" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbGroupSelected" runat="server" AutoPostBack="true" OnCheckedChanged="cbCheckedChanged" Text='<%# Eval("m0_idx") + "|" + Eval("group_name") %>' CssClass="hiddenText" Style="color: transparent;" Enabled="False"></asp:CheckBox>
                                                                <asp:HiddenField ID="hfM0Idx" runat="server" Value='<%# Eval("m0_idx")%>'></asp:HiddenField>
                                                                <asp:Label ID="lbm0_idx" runat="server" Visible="false" Text='<%# Eval("m0_idx") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Zone">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGroupName" runat="server" Text='<%# Eval("group_name") %>'></asp:Label>
                                                                <asp:Label ID="lbapprove_status" Visible="false" runat="server" Text=""></asp:Label>
                                                                <asp:Label ID="lbapprove_status1" runat="server" Text=""></asp:Label>
                                                                <asp:CheckBox ID="cbGroupSel" runat="server" CssClass="hiddenText" Style="color: transparent;"></asp:CheckBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <label class="col-md-2 control-label">ประเภทผู้มาติดต่อ<span class="text-danger">* :</label>
                                            <div class="col-md-4">
                                                <asp:HiddenField ID="hfVisitorTypeIdx" runat="server" Value='<%# Eval("visitor_type_idx")%>'></asp:HiddenField>
                                                <asp:DropDownList ID="ddlVisitorType" runat="server" CssClass="form-control" Enabled="False"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <label class="col-md-4 control-label-static">รายละเอียดผู้มาติดต่อ</label>
                                            <label class="col-md-2 control-label"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-10">
                                                <asp:GridView ID="gvVisitorList" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                                    OnRowDataBound="gvRowDataBound">
                                                    <HeaderStyle CssClass="info" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="หมายเลขบัตรประชาชน/พาสปอร์ต">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lb_u1_idx" runat="server" Visible="false" Text='<%# Eval("u1_idx") %>'></asp:Label>
                                                                <asp:Label ID="lblIdentifyNo" runat="server" Text='<%# Eval("visit_identify_no") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="หมายเลขโทรศัพท์">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPhoneNo" runat="server" Text='<%# Eval("visit_phone_no") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ชื่อ">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVistFirstname" runat="server" Text='<%# Eval("visit_firstname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="นามสกุล">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVistLastname" runat="server" Text='<%# Eval("visit_lastname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="หมายเลขบัตร Visitor">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="lblVistorCardNo" runat="server" CssClass="form-control" Text='<%# Eval("visit_card_ex_no") %>' MaxLength="10"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="การจัดการ">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbCamera" runat="server" CssClass="btn btn-sm btn-primary" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0" Visible="False">
                                                                    <i class="fas fa-camera"></i>&nbsp;ถ่ายรูป</asp:LinkButton>
                                                                <asp:LinkButton ID="lbIdCard" runat="server" CssClass="btn btn-sm btn-info" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0" Visible="False">
                                                                    <i class="far fa-address-card"></i>&nbsp;สแกนบัตร</asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">หมายเลขทะเบียนรถ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVehiclePlateNo" runat="server" CssClass="form-control" placeholder="หมายเลขทะเบียนรถ" Enabled="False"></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">จังหวัด :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlProvince" runat="server" CssClass="form-control" Enabled="False"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <asp:HiddenField ID="hfFlagSafety" runat="server" Value='<%# Eval("flag_safety")%>'></asp:HiddenField>
                                        <asp:Panel ID="pnNode6Comment" runat="server" Visible="False">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Comment :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbSafetyComment" runat="server" CssClass="form-control"
                                                        Enabled="False" Text='<%# Eval("safety_comment") %>'></asp:TextBox>
                                                </div>
                                                <label class="col-md-2 control-label"></label>
                                                <label class="col-md-4"></label>
                                            </div>
                                        </asp:Panel>
                                        <div class="form-group">
                                            <div class="col-md-2">
                                                <asp:LinkButton ID="lbBack" runat="server" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-chevron-left"></i>&nbsp;กลับ</asp:LinkButton>
                                            </div>
                                            <div class="col-md-6">
                                                <asp:Panel ID="pnNode2" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN2Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="2">
                                                        <i class="fas fa-check"></i>&nbsp;อนุมัติ</asp:LinkButton>
                                                    <asp:LinkButton ID="lbN2Reject" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="3">
                                                        <i class="fas fa-times"></i>&nbsp;ไม่อนุมัติ</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode3" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN3Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="4">
                                                        <i class="fas fa-check"></i>&nbsp;อนุมัติ Zone</asp:LinkButton>
                                                    <asp:LinkButton ID="lbN3Reject" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="5">
                                                        <i class="fas fa-times"></i>&nbsp;ไม่อนุมัติ Zone</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode4" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN4Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="6">
                                                        <i class="fas fa-save"></i>&nbsp;บันทึก</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode5" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN5Safety" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="8">
                                                        <i class="fas fa-plus"></i>&nbsp;ติดต่อ Safety</asp:LinkButton>
                                                    
                                                    <asp:LinkButton ID="lbN5NoSafety" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="9">
                                                        <i class="fas fa-user"></i>&nbsp;ติดต่อ ต้นสังกัด</asp:LinkButton>

                                                    <asp:LinkButton ID="lbN5RejectSafety" runat="server" CssClass="btn btn-warning" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="20">
                                                        <i class="fas fa-gavel"></i>&nbsp;Safety ตรวจไม่ผ่าน</asp:LinkButton>
                                                   
                                                    <asp:LinkButton ID="lbN5ApproveZone" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="18">
                                                        <i class="fas fa-adjust"></i>&nbsp;เพิ่ม Zone</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode6" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN6Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="10">
                                                        <i class="fas fa-save"></i>&nbsp;บันทึก</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode7" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN7Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="12">
                                                        <i class="fas fa-save"></i>&nbsp;บันทึก</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode8" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN8Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="14">
                                                        <i class="fas fa-save"></i>&nbsp;ยืนยัน(จบงาน)</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode9" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN9Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="16">
                                                        <i class="fas fa-save"></i>&nbsp;ยืนยัน(ออกนอกพื้นที่)</asp:LinkButton>
                                                </asp:Panel>
                                            </div>
                                            <label class="col-md-4"></label>
                                        </div>
                                    </EditItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default" id="div_display_log" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <asp:Literal ID="litLogFormTitle" runat="server" Text="ประวัติรายการ"></asp:Literal></h3>
                        </div>
                        <div class="panel-body">
                            <asp:Repeater ID="rptLog" runat="server">
                                <HeaderTemplate>
                                    <div class="row">
                                        <label class="col-sm-2 control-label">วันที่ / เวลา</label>
                                        <label class="col-sm-3 control-label">ผู้ดำเนินการ</label>
                                        <label class="col-sm-2 control-label">ดำเนินการ</label>
                                        <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                                        <label class="col-sm-3 control-label">หมายเหตุ</label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <span><%# Eval("create_date") %></span>
                                        </div>
                                        <div class="col-sm-3">
                                            <span><%# Eval("emp_name_th") %>(<%# Eval("actor_name") %>)</span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><%# Eval("node_name") %></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><%# Eval("decision_name") %></span>
                                        </div>
                                        <div class="col-sm-3">
                                            <span><%# Eval("remark") %></span>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="viewApprove" runat="server">
                <div class="col-md-12">
                    <div class="panel panel-default" id="div_list_heading_app" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <asp:Literal ID="litListHeadingTitle_app" runat="server" Text="ค้นหารายการ"></asp:Literal></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">หมายเลขบัตรประชาชน :</label>
                                    <div class="col-md-3">
                                        <asp:TextBox ID="tbSearchIdCard_app" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <label class="col-md-2 control-label">ชื่อผู้มาติดต่อ :</label>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="tbSearchName_app" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">ชื่อผู้ที่เข้าพบ :</label>
                                    <div class="col-md-3">
                                        <asp:TextBox ID="tbSearchVisitName_app" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-4"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-4">
                                        <asp:LinkButton ID="lbSearch_app" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearch_app" CommandArgument="0">
                                            <i class="fas fa-search" aria-hidden="true"></i>&nbsp;ค้นหา</asp:LinkButton>
                                        <asp:LinkButton ID="lbReset_app" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset_app" CommandArgument="0">
                                            <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                    </div>
                                    <label class="col-md-1 control-label"></label>
                                    <div class="col-md-4">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <asp:GridView ID="gvDocList_app" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnRowDataBound="gvRowDataBound"
                        OnPageIndexChanging="gvPageIndexChanging"
                        AllowPaging="true" PageSize="10"
                        DataKeyNames="u0_idx">
                        <HeaderStyle CssClass="info" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                            FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่เข้าพบ">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitDate" runat="server" Text='<%# Eval("visit_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อผู้ที่เข้าพบ">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitEmpNameTh" runat="server" Text='<%# Eval("visit_emp_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่ทำรายการ">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ผู้ทำรายการ">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpNameTh" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ">
                                <ItemTemplate>
                                    <asp:Label ID="lblDocStatus" runat="server" Text='<%# Eval("doc_status_name") + " | " + Eval("node_name") + " | " + Eval("actor_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="การจัดการ">
                                <ItemTemplate>
                                    <asp:Label ID="lbu0_idx" runat="server" Visible="false" Text='<%# Eval("u0_idx") %>'></asp:Label>
                                    <asp:LinkButton ID="lbEdit" runat="server" CssClass="btn btn-sm btn-info" OnCommand="btnCommand" CommandName="cmdEdit_approve" CommandArgument='<%# Eval("u0_idx") %>'><i class="far fa-file-alt"></i>&nbsp;รายละเอียด</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </asp:View>
            <asp:View ID="viewReport" runat="server">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">เลือกรายงาน</h3>
                        </div>
                        <div class="panel-body">
                            <asp:FormView ID="fvReportSearch" runat="server" Width="100%" DefaultMode="Insert">
                                <InsertItemTemplate>
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ประเภทวัน<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlDateType" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1" Text="วันที่เข้าพบ"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="วันที่ทำรายการ"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ประเภทรายงาน<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlReportType" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1" Text="จำนวนรายการต่อวัน (แยกตามประเภทผู้มาติดต่อ)"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="จำนวนรายการต่อเดือน (แยกตามประเภทผู้มาติดต่อ)"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="รายละเอียดรายการ"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">วันที่<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbStartDate" runat="server" CssClass="form-control date-datepicker-date"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvTbStartDate" runat="server" ControlToValidate="tbStartDate" ErrorMessage="กรุณาเลือกวันที่ด้วยค่ะ" Display="None" SetFocusOnError="false" ValidationGroup="fvReportSearch"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceTbStartDate" TargetControlID="rfvTbStartDate" HighlightCssClass="validatorCalloutHighlight" />
                                            </div>
                                            <label class="col-md-2 control-label">ถึงวันที่ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbEndDate" runat="server" CssClass="form-control date-datepicker-date"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">องค์กร :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlReportOrg" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ฝ่าย :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlReportDept" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">แผนก :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlReportSec" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ชื่อผู้ที่เข้าพบ :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlReportEmpList" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ประเภท Visitor :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlReportVisitorType" runat="server" CssClass="form-control" Visible="False"></asp:DropDownList>
                                                <asp:CheckBoxList ID="cblReportVisitorType" runat="server"></asp:CheckBoxList>
                                            </div>
                                            <label class="col-md-2 control-label">Zone :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlReportGroup" runat="server" CssClass="form-control" Visible="False"></asp:DropDownList>
                                                <asp:CheckBoxList ID="cblReportGroup" runat="server"></asp:CheckBoxList>
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: none;">
                                            <label class="col-md-2 control-label">จัดกลุ่มตาม :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlReport1" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbReportSearch" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdReportSearch" CommandArgument="0" ValidationGroup="fvReportSearch">
                                                    <i class="fas fa-search" aria-hidden="true"></i>&nbsp;ค้นหา</asp:LinkButton>
                                                <asp:LinkButton ID="lbReportReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReportReset" CommandArgument="0">
                                                    <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                                <asp:LinkButton ID="lbReportExport" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdReportExport" CommandArgument="0">
                                                    <i class="far fa-file-excel" aria-hidden="true"></i>&nbsp;Export</asp:LinkButton>
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                            </div>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                    <asp:GridView ID="gvReportSearch1" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-bordered table-responsive"
                    OnRowDataBound="gvRowDataBound" ShowFooter="True">
                        <HeaderStyle CssClass="info" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <Columns>
                            <asp:TemplateField HeaderText="วันที่เข้าพบ">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitDate" runat="server" Text='<%# Eval("visit_date") %>'></asp:Label>
                                    <asp:HiddenField ID="hfVisitDate" runat="server" Value='<%# Eval("visit_date") %>'></asp:HiddenField>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ประเภทผู้มาติดต่อ">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitType" runat="server" Text='<%# Eval("visitor_type_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="จำนวน (รายการ)">
                                <ItemStyle CssClass="text-right"></ItemStyle>
                                <FooterStyle CssClass="text-right"></FooterStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitCount" runat="server" Text='<%# Eval("zcount") %>'></asp:Label>
                                    <asp:HiddenField ID="hfCount" runat="server" Value='<%# Eval("zcount") %>'></asp:HiddenField>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblSum" runat="server" Text='-'></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:Literal ID="litReportSearch1" runat="server"></asp:Literal>
                    <asp:GridView ID="gvReportSearch2" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-bordered table-responsive"
                    OnRowDataBound="gvRowDataBound">
                        <HeaderStyle CssClass="info" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <Columns>
                            <asp:TemplateField HeaderText="วันที่ต้องการเข้าพบ">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblU0Idx" runat="server" Text='<%# Eval("u0_idx") %>'></asp:Label> |--%>
                                    <asp:Label ID="lblVisitDate" runat="server" Text='<%# Eval("visit_date") %>'></asp:Label>
                                    <asp:HiddenField ID="hfU0Idx" runat="server" Value='<%# Eval("u0_idx") %>'></asp:HiddenField>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ผู้ที่ต้องการเข้าพบ">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitEmpNameTh" runat="server" Text='<%# Eval("visit_emp_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ประเภทผู้มาติดต่อ">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitType" runat="server" Text='<%# Eval("visitor_type_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่เข้าพบ">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitArrived" runat="server" Text='<%# Eval("update_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หัวข้อ">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitTitle" runat="server" Text='<%# Eval("visit_title") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อผู้มาติดต่อ">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblU1Idx" runat="server" Text='<%# Eval("u1_idx") %>'></asp:Label> |--%>
                                    <asp:Label ID="lblVisitorName" runat="server" Text='<%# Eval("visit_prefix") + "" + Eval("visit_firstname") + " " + Eval("visit_lastname") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หมายเลขบัตรประชาชน">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitIdentifyNo" runat="server" Text='<%# Eval("visit_identify_no") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หมายเลขทะเบียนรถ">
                                <ItemTemplate>
                                    <asp:Label ID="lblVehiclePlateNo" runat="server" Text='<%# Eval("vehicle_plate_no") + " " + Eval("vehicle_plate_province_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>

    <script type="text/javascript">
        $(function () {
            $('.date-datepicker').datetimepicker({
                format: 'YYYY-MM-DD HH:mm'
            });

            $('.date-datepicker-date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.date-datepicker').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm'
                });
            });

            $(function () {
                $('.date-datepicker-date').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
            });
        });
    </script>


    <%--    <script type="text/javascript">
        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                menubar: false,
                resize: false,
                statusbar: false,
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }
            });

            $(".multi").MultiFile();
        })
    </script>--%>
</asp:Content>
