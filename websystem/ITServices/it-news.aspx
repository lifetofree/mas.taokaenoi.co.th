﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="it-news.aspx.cs" Inherits="websystem_ITServices_it_news" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <style type="text/css">
        html {
            font-family: Lato, 'Helvetica Neue', Arial, Helvetica, sans-serif;
            font-size: 14px;
        }

        h5 {
            font-size: 1.28571429em;
            font-weight: 700;
            line-height: 1.2857em;
            margin: 0;
        }

        .card {
            font-size: 1em;
            overflow: hidden;
            padding: 0;
            border: none;
            border-radius: .28571429rem;
            box-shadow: 0 1px 3px 0 #d4d4d5, 0 0 0 1px #d4d4d5;
        }

        .card-block {
            font-size: 1em;
            position: relative;
            margin: 0;
            padding: 1em;
            border: none;
            border-top: 1px solid rgba(34, 36, 38, .1);
            box-shadow: none;
        }

        .card-img-top {
            display: block;
            width: 100%;
            height: 100%;
            /*height: auto;*/
        }

        .card-title {
            font-size: 1.28571429em;
            font-weight: 700;
            line-height: 1.2857em;
        }

        .card-text {
            clear: both;
            margin-top: .5em;
            color: rgba(0, 0, 0, .68);
        }

        .card-footer {
            font-size: 1em;
            position: static;
            top: 0;
            left: 0;
            max-width: 100%;
            padding: .75em 1em;
            color: rgba(0, 0, 0, .4);
            border-top: 1px solid rgba(0, 0, 0, .05) !important;
            background: #fff;
        }

        .card-inverse .btn {
            border: 1px solid rgba(0, 0, 0, .05);
        }

        .profile-avatar {
            display: block;
            width: 100%;
            height: auto;
            border-radius: 50%;
        }

        .profile-inline {
            position: relative;
            top: 0;
            display: inline-block;
        }

            .profile-inline ~ .card-title {
                display: inline-block;
                margin-left: 4px;
                vertical-align: top;
            }

        .text-bold {
            font-weight: 700;
        }

        .meta {
            font-size: 1em;
            color: rgba(0, 0, 0, .4);
        }

            .meta a {
                text-decoration: none;
                color: rgba(0, 0, 0, .4);
            }

                .meta a:hover {
                    color: rgba(0, 0, 0, .87);
                }

        .more, .less {
            font-weight: 600;
            color: #000;
            text-decoration: none;
        }
    </style>
    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    <div class="col-sm-12">

        <div class="wraptocenter">
            <img src='<%=ResolveUrl("~/uploadfiles/it_news/default/logo5.jpg") %>' style="align-content: center; width: 100%;" alt="" />
        </div>
        <%--  <section class="color" style="background-color: lavender; width: auto; height: 200px;">
        </section>--%>
    </div>


    <%--    <div class="col-xs-12">
        <div class="list-group">
            <asp:Table ID="MyTable" Width="100%" Height="45px" runat="server" CellSpacing="0" CellPadding="5" border="1" BorderColor="#149BC4">
                <asp:TableRow runat="server">

                    <asp:TableCell runat="server" Width="23%" BackColor="#149BC4" ForeColor="White">
                        &nbsp;&nbsp;<asp:Label ID="Label2" Font-Bold="true" runat="Server"><i class="glyphicon glyphicon-globe"></i> ข่าวไอที</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell runat="server" BackColor="#149BC4" ForeColor="Black">
                        <form>
                            <div class="input-group">
                                <div class="col-sm-7"></div>
                                <div class="col-sm-5">
                                    <asp:TextBox ID="txtsearch" runat="server" placeholder="search..."
                                        CssClass="form-control blockquote"></asp:TextBox>
                                </div>
                                <span class="input-group-btn">
                                    <asp:LinkButton ID="btnsearch" runat="server" CommandName="btnsearch" data-toggle="tooltip" title="search"
                                        OnCommand="btncommand" BackColor="#000000" CssClass="btn btn-block">search</asp:LinkButton>
                                </span>
                            </div>
                        </form>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </div>--%>
    <%-- <div class="col-sm-6 col-md-4 col-lg-3 mt-4">--%>
    <div class="col-sm-12">
        <div class="panel panel-default" style="background-color: #E6E6FA;">
            <div class="panel-body">
                <%--    <div class="form-control" style="background-color: #0A122A;">--%>
                <div class="input-group">
                    <div class="col-sm-7">
                        <asp:LinkButton ID="btnHome" runat="server" Font-Bold="true" CommandName="btnHome" OnCommand="btncommand" CssClass="btn btn-default" data-toggle="tooltip" title="Home"><i class="fa fa-home" aria-hidden="true"></i>  หน้าแรก</asp:LinkButton>
                    </div>
                    <div class="col-sm-5">
                        <asp:TextBox ID="txtsearch" runat="server" placeholder="search..."
                            CssClass="form-control blockquote"></asp:TextBox>
                    </div>
                    <span class="input-group-btn">
                        <asp:LinkButton ID="btnsearch" runat="server" CommandName="btnsearch" data-toggle="tooltip" title="search"
                            OnCommand="btncommand" BackColor="#000000" CssClass="btn btn-block">search</asp:LinkButton>
                    </span>
                </div>
            </div>
            <%--   <p class="mb-0 info">ข่าวประชาสัมพันธ์จากแผนก Network</p>--%>
        </div>
    </div>
    <asp:HyperLink ID="setOnTop" runat="server"></asp:HyperLink>
    <div class="col-sm-9">
        <asp:Label ID="lbtitleSearchNews" ForeColor="Blue" Font-Bold="true" Font-Size="12" runat="server"></asp:Label>
        <asp:Repeater ID="rptNewsIT" runat="server" OnItemDataBound="rpDataBound">
            <HeaderTemplate>
                <table class="items">
            </HeaderTemplate>
            <ItemTemplate>
                <%# (Container.ItemIndex + 2) % 2 == 0 ? "<tr>" : string.Empty%>

                <td style="width: 50%; height: 100%">

                    <asp:Label ID="lbIdxDetailsNews1" runat="server" Visible="false">
                                    <p>
                                      <%# Eval("u0_news_idx") %>
                                    </p>
                    </asp:Label>
                    <div class="card card-inverse">

                        <img class="card-img-top img-thumbnail figure" id="imgListRoom" width="100" height="100" style="margin-right: 1em;" runat="server" />
                        <div class="card-footer">
                            <span>
                                <asp:Label ID="lbTypeNews" CssClass="label control-label" Width="25%" Height="20%" Font-Size="Small" ForeColor="White" runat="server" Style="background-color: orangered;">หมวดข่าว</asp:Label>
                                <%# Eval("name_type_news") %></span>
                        </div>

                        <div class="card-block">
                            <p class="card-title" style="font-size: 1.2em"><i class="fa fa-yelp" aria-hidden="true"></i><%# Eval("title_it_news") %></p>
                            <div class="card-text">
                                &nbsp; &nbsp; &nbsp; <%# Eval("details_news") %>
                            </div>
                        </div>
                        <div class="card-footer">
                            <asp:LinkButton ID="btnReadMore" CssClass="btn btn-md btn-info" runat="server" CommandName="cmdReadMore" OnCommand="btncommand" Text="อ่านเพิ่มเติม.." CommandArgument='<%# Eval("u0_news_idx") %>' data-toggle="tooltip" title="Read More..."></asp:LinkButton>

                        </div>
                        <%# (Container.ItemIndex + 2) % 2 == 1 ? "</tr>" : string.Empty%>
                    </div>

                </td>

            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>

        <asp:Repeater ID="rptNewsShowDetailsAll" runat="server" OnItemDataBound="rpDataBound">
            <HeaderTemplate>
                <table class="items">
            </HeaderTemplate>
            <ItemTemplate>
                <%# (Container.ItemIndex + 1) % 1 == 0 ? "<tr>" : string.Empty%>

                <td style="width: 50%; height: 100%">

                    <asp:Label ID="lbIdxDetailsNews1" runat="server" Visible="false">
                                    <p>
                                      <%# Eval("u0_news_idx") %>
                                    </p>
                    </asp:Label>
                    <div class="card card-inverse">

                        <img class="card-img-top img-thumbnail figure" id="imgListRoom" width="100" height="100" style="margin-right: 1em;" runat="server" />
                        <div class="card-footer">
                            <span>
                                <asp:Label ID="lbTypeNewsDetails" CssClass="label control-label" Width="25%" Height="20%" Font-Size="Small" ForeColor="White" runat="server" Style="background-color: orangered;">หมวดข่าว</asp:Label>
                                <%# Eval("name_type_news") %></span>
                        </div>

                        <div class="card-block">
                            <p class="card-title" style="font-size: 1.2em"><i class="fa fa-yelp" aria-hidden="true"></i><%# Eval("title_it_news") %></p>
                            <div class="card-text">
                                &nbsp; &nbsp; &nbsp; <%# Eval("details_news") %>
                            </div>
                        </div>
                        <div class="card-footer">
                            <asp:LinkButton ID="btnHiddenReadMore" CssClass="btn btn-md btn-info" runat="server" CommandName="cmdHiddenReadMore" OnCommand="btncommand" Text="ซ่อนรายละเอียด.." CommandArgument='<%# Eval("u0_news_idx") %>' data-toggle="tooltip" title="Hidden Details..."></asp:LinkButton>

                        </div>
                        <%# (Container.ItemIndex + 1) % 1 == 1 ? "</tr>" : string.Empty%>
                    </div>

                </td>

            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>

    <div class="col-sm-3">

        <%--  <div class="card card-inverse card-info">--%>
        <%--  <div class="row full-height">--%>

        <div class="list-group">
            <asp:LinkButton ID="LinkButton1" runat="server" BackColor="#58ACFA" Font-Bold="true" ForeColor="White" CssClass="list-group-item"><h4>ข่าวไอทีทั่วไป</h4></asp:LinkButton>
            <asp:Repeater ID="rplistnews" runat="server">
                <ItemTemplate>

                    <asp:LinkButton ID="btnmenuNewsPR" runat="server" data-toggle="tooltip" data-placement="top" title=""
                        CommandName="cmdReadMore" OnCommand="btncommand"
                        CommandArgument='<%#Eval("u0_news_idx") %>' BorderColor="SkyBlue" CssClass="list-group-item btn-success">
                               <p class="card-title" style="font-size: 1.2em"><i class="fa fa-tags" aria-hidden="true"></i>  <%# Eval("title_it_news") %></h5>
                                  <p class="list-group-item-text">&nbsp; &nbsp; &nbsp; <%# Eval("details_news") %></p>
                    </asp:LinkButton>

                </ItemTemplate>
            </asp:Repeater>
            <asp:LinkButton ID="btnmenuall_news_genr" runat="server" Text="ดูหัวข้อข่าวไอทีทั้งหมด...." data-toggle="tooltip" data-placement="top" title=""
                data-original-title="ดูหัวข้อข่าวไอทีทั้งหมด" CommandName="btnmenuall_news" OnCommand="btncommand"
                CommandArgument='<%#Eval("u0_news_idx") %>' BackColor="#2B1B17" ForeColor="White" CssClass="list-group-item btn-success"></asp:LinkButton>
            <asp:LinkButton ID="btnmenuall_HiddenGenr" runat="server" Text="ซ่อนข่าวไอทีทั่วไป...." data-toggle="tooltip" data-placement="top" title=""
                data-original-title="ซ่อนข่าวไอทีทั่วไป" Visible="false" CommandName="btnHiddenmenuall_newsGenr" OnCommand="btncommand" BackColor="#2B1B17" ForeColor="White" CssClass="list-group-item btn-success"></asp:LinkButton>
            <asp:HyperLink ID="HyperLink1" runat="server"></asp:HyperLink>
        </div>
        <div class="list-group">
            <asp:HyperLink ID="setFocus" runat="server"></asp:HyperLink>
            <asp:LinkButton ID="LinkButton2" runat="server" BackColor="#58ACFA" Font-Bold="true" ForeColor="White" CssClass="list-group-item"><h4>ประชาสัมพันธ์ข่าวสารระบบ Network ในองค์กร</h4></asp:LinkButton>
            <asp:Repeater ID="rplistnewsPR" runat="server">
                <ItemTemplate>

                    <asp:LinkButton ID="btnmenuNewsPR1" runat="server" data-toggle="tooltip" data-placement="top" title=""
                        CommandName="cmdReadMore" OnCommand="btncommand"
                        CommandArgument='<%#Eval("u0_news_idx") %>' BorderColor="SkyBlue" CssClass="list-group-item btn-success">
                        <p class="card-title" style="font-size: 1.2em">
                            <i class="fa fa-tags" aria-hidden="true"></i><%# Eval("title_it_news") %></h5>
                                  <p class="list-group-item-text">
                                      &nbsp; &nbsp; &nbsp; <%# Eval("details_news") %>
                                      <asp:Label ID="lbTypeNews" Font-Size="Small" ForeColor="Blue" runat="server">  รายละเอียดเพิ่มเติม..</asp:Label>
                                  </p>
                            <span></span>
                    </asp:LinkButton>

                </ItemTemplate>
            </asp:Repeater>
            <asp:LinkButton ID="btn_allnewspr" runat="server" Text="ดูหัวข้อข่าวประชาสัมพันธ์ทั้งหมด...." data-toggle="tooltip" data-placement="top" title=""
                data-original-title="ดูหัวข้อข่าวประชาสัมพันธ์ทั้งหมด" CommandName="btnmenuall_newspr" OnCommand="btncommand"
                CommandArgument='<%#Eval("u0_news_idx") %>' BackColor="#2B1B17" ForeColor="White" CssClass="list-group-item btn-success"></asp:LinkButton>
            <asp:LinkButton ID="btnHiddenMenuPr" runat="server" Text="ซ่อนหัวข้อข่าวประชาสัมพันธ์...." data-toggle="tooltip" data-placement="top" title=""
                data-original-title="ซ่อนหัวข้อข่าวประชาสัมพันธ์" Visible="false" CommandName="btnHiddenmenuall_newspr" OnCommand="btncommand" BackColor="#2B1B17" ForeColor="White" CssClass="list-group-item btn-success"></asp:LinkButton>
            <asp:HyperLink ID="setBelow" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="panel panel-default" style="background-color: #E6E6FA;">
            <div class="panel-body">
                <h4><strong>ข่าวไอทีทั่วไป</strong></h4>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">

            <asp:Repeater ID="rtpNews" runat="server" OnItemDataBound="rpDataBound">
                <HeaderTemplate>
                    <table class="items">
                </HeaderTemplate>
                <ItemTemplate>
                    <%# (Container.ItemIndex + 2) % 2 == 0 ? "<tr>" : string.Empty%>

                    <td style="width: 25%; height: 100%">

                        <asp:Label ID="lbIdxDetailsNews1" runat="server" Visible="false">
                                    <p>
                                      <%# Eval("u0_news_idx") %>
                                    </p>
                        </asp:Label>
                        <div class="card card-inverse">

                            <img class="card-img-top img-thumbnail figure" id="imgListRoom" width="100" height="100" style="margin-right: 1em;" runat="server" />
                            <div class="card-footer">
                                <span>
                                    <asp:Label ID="lbTypeNews" CssClass="label control-label" Width="25%" Height="20%" Font-Size="Small" ForeColor="White" runat="server" Style="background-color: orangered;">หมวดข่าว</asp:Label>
                                    <%# Eval("name_type_news") %></span>
                            </div>

                            <div class="card-block">
                                <p class="card-title" style="font-size: 1.2em"><i class="fa fa-yelp" aria-hidden="true"></i><%# Eval("title_it_news") %></p>
                                <div class="card-text">
                                    &nbsp; &nbsp; &nbsp; <%# Eval("details_news") %>
                                </div>
                            </div>
                            <div class="card-footer">
                                <asp:LinkButton ID="btnReadMore" CssClass="btn btn-md btn-info" runat="server" CommandName="cmdReadMore" OnCommand="btncommand" Text="อ่านเพิ่มเติม.." CommandArgument='<%# Eval("u0_news_idx") %>' data-toggle="tooltip" title="Read More..."></asp:LinkButton>

                            </div>
                            <%# (Container.ItemIndex + 2) % 2 == 1 ? "</tr>" : string.Empty%>
                        </div>

                    </td>

                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>

        </div>
    </div>
    <div class="col-sm-6"></div>

    <%--    </div>--%>


    <%-- <table class="table">
        <asp:Repeater ID="rpNewsDetail" runat="server">
            <ItemTemplate>
                <tr style="border: hidden;">
                    <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
                        <td>


                        </td>

                    </div>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>--%>
    <%--  </div>--%>
    <%--    <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
        <div class="card card-inverse card-info">
            <img class="card-img-top" src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif">
            <div class="card-block">
                <figure class="profile profile-inline">
                    <img src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif" class="profile-avatar" alt="">
                </figure>
                <h4 class="card-title">Tawshif Ahsan Khan</h4>
                <div class="card-text">
                    Tawshif is a web designer living in Bangladesh.
                       
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-info btn-sm">Read More..</button>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
        <div class="card card-inverse card-info">
            <img class="card-img-top" src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif">
            <div class="card-block">
                <figure class="profile profile-inline">
                    <img src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif" class="profile-avatar" alt="">
                </figure>
                <h4 class="card-title">Tawshif Ahsan Khan</h4>
                <div class="card-text">
                    Tawshif is a web designer living in Bangladesh.
                       
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-info btn-sm">ReadMore..</button>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4 col-lg-3 mt-4">
        <div class="card card-inverse card-info">
            <img class="card-img-top" src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif">
            <div class="card-block">
                <figure class="profile profile-inline">
                    <img src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif" class="profile-avatar" alt="">
                </figure>
                <h4 class="card-title">Tawshif Ahsan Khan</h4>
                <div class="card-text">
                    Tawshif is a web designer living in Bangladesh.
                       
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-info btn-sm">ReadMore..</button>
            </div>
        </div>
    </div>--%>




    <%--
    <asp:GridView ID="gvShowNews" runat="server"
        AutoGenerateColumns="False" ShowHeader="false"
        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
        OnPageIndexChanging="gvPageIndexChanging"
        OnRowDataBound="gvRowDataBound"
        AllowPaging="True"
        PageSize="1"
        DataKeyNames="u0_news_idx">
        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
        <RowStyle Font-Size="Small" />
        <PagerStyle CssClass="pageCustom" />
        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
        <EmptyDataTemplate>
            <div style="text-align: center">ไม่พบข้อมูล</div>
        </EmptyDataTemplate>
        <Columns>
            <asp:TemplateField HeaderStyle-Width="30%" ItemStyle-BorderColor="Transparent">
                <ItemTemplate>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <img id="ImagShowNews" width="490" height="350" style="margin-right: 1em;" runat="server" class="thumbnail" />
                            <asp:Label ID="lbIdxU0News" runat="server" Visible="false" Text='<%# Eval("u0_news_idx") %>'></asp:Label>
                        </div>
                    </div>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="60%" ItemStyle-BorderColor="Transparent">
                <ItemTemplate>
                    <div class="form-group">
                        <div class="alert-message alert-message-success">
                            <blockquote class="danger" style="font-size: small; background-color: darkseagreen;">
                                <h4><b>ปรเภทข่าว: <%# Eval("name_type_news") %></b></h4>

                            </blockquote>
                        </div>
                    </div>
                    <div class="form-group">

                        <asp:Label ID="lbIdxDetailsNews" runat="server" Visible="true">
                                    <p>
                                        &nbsp; &nbsp; &nbsp; <%# Eval("details_news") %>
                                    </p>
                        </asp:Label>
                    </div>

                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
    </asp:GridView>
    --%>



    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.carousel').carousel()

            $('.carousel-control.left').click(function (e) {
                e.stopPropagation();
                $('.carousel').carousel('prev');
                return false;
            });

            $('.carousel-control.right').click(function (e) {
                e.stopPropagation();
                $('.carousel').carousel('next');
                return false;
            });
        });


        $(function () {
            $('.carousel').carousel()

            $('.carousel-control.left').click(function (e) {
                e.stopPropagation();
                $('.carousel').carousel('prev');
                return false;
            });

            $('.carousel-control.right').click(function (e) {
                e.stopPropagation();
                $('.carousel').carousel('next');
                return false;
            });
        });
        $(function () {
            // here the code for text minimiser and maxmiser by faisal khan
            var minimized_elements = $('p.text-viewer');

            minimized_elements.each(function () {
                var t = $(this).text();
                if (t.length < 500) return;

                $(this).html(
                    t.slice(0, 500) + '<span>... </span><a href="#" class="more"> Read More>> </a>' +
                    '<span style="display:none;">' + t.slice(500, t.length) + ' <a href="#" class="less"> << Less </a></span>'
                );
            });

            $('a.more', minimized_elements).click(function (event) {
                event.preventDefault();
                $(this).hide().prev().hide();
                $(this).next().show();
            });

            $('a.less', minimized_elements).click(function (event) {
                event.preventDefault();
                $(this).parent().hide().prev().show().prev().show();
            });
        });
    </script>

    <br />




</asp:Content>

