﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_masterdata_nwd_m0_room : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_networkdevices _data_networkdevices = new data_networkdevices();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetddlplace = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlplace"];
    static string _urlSetm0Room = _serviceUrl + ConfigurationManager.AppSettings["urlSetm0Room"];
    static string _urlGetm0Room = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0Room"];
    static string _urlDeletem0Room = _serviceUrl + ConfigurationManager.AppSettings["urlDeletem0Room"];



    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
    }

    #region selected   
    protected void actionIndex()
    {

        data_networkdevices _data_networkdevicesindex = new data_networkdevices();
        _data_networkdevicesindex.m0room_list = new m0room_detail[1];

        m0room_detail _m0room_detailindex = new m0room_detail();

        _m0room_detailindex.room_idx = 0;

        _data_networkdevicesindex.m0room_list[0] = _m0room_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_networkdevicesindex));
        _data_networkdevicesindex = callServiceNetwork(_urlGetm0Room, _data_networkdevicesindex);

        setGridData(GvMaster, _data_networkdevicesindex.m0room_list);

    }

    protected void actionddlplace()
    {

        DropDownList ddlplace = (DropDownList)ViewInsert.FindControl("ddlplace");

        ddlplace.Items.Clear();
        ddlplace.AppendDataBoundItems = true;
        ddlplace.Items.Add(new ListItem("กรุณาเลือกชื่อสถานที่ติดตั้งอุปกรณ์...", "00"));

        _data_networkdevices.m0room_list = new m0room_detail[1];
        m0room_detail _m0roomDetail = new m0room_detail();

        _data_networkdevices.m0room_list[0] = _m0roomDetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

        _data_networkdevices = callServiceNetwork(_urlGetddlplace, _data_networkdevices);

        ddlplace.DataSource = _data_networkdevices.m0room_list;
        ddlplace.DataTextField = "place_name";
        ddlplace.DataValueField = "place_idx";
        ddlplace.DataBind();


    }


    #endregion selected 

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _room_idx;
        string _room_name;
        int _cemp_idx;

        m0room_detail _m0room_detail = new m0room_detail();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                actionddlplace();
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":

                _room_name = ((TextBox)ViewInsert.FindControl("txtroomName")).Text.Trim();
                DropDownList _ddlroomStatus = (DropDownList)ViewInsert.FindControl("ddlroomStatus");
                DropDownList _ddlplace = (DropDownList)ViewInsert.FindControl("ddlplace");
                _cemp_idx = emp_idx;

                _data_networkdevices.m0room_list = new m0room_detail[1];
                _m0room_detail.room_idx = 0;//_type_idx; 
                _m0room_detail.room_name = _room_name;
                _m0room_detail.room_status = int.Parse(_ddlroomStatus.SelectedValue);
                _m0room_detail.cemp_idx = _cemp_idx;
                _m0room_detail.place_idx = int.Parse(_ddlplace.SelectedValue);

                _data_networkdevices.m0room_list[0] = _m0room_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));
                _data_networkdevices = callServiceNetwork(_urlSetm0Room, _data_networkdevices);

                if (_data_networkdevices.return_code == 0)
                {

                    actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_networkdevices.return_code.ToString() + " - " + _data_networkdevices.return_msg);
                }


                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnDelete":

                _room_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_networkdevices.m0room_list = new m0room_detail[1];
                _m0room_detail.room_idx = _room_idx;
                _m0room_detail.cemp_idx = _cemp_idx;

                _data_networkdevices.m0room_list[0] = _m0room_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                _data_networkdevices = callServiceNetwork(_urlDeletem0Room, _data_networkdevices);


                if (_data_networkdevices.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_networkdevices.return_code.ToString() + " - " + _data_networkdevices.return_msg);
                }

                break;




        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                        var lbddlPlaceName = (Label)e.Row.FindControl("lbddlPlaceName");
                        var ddlPlaceNameUpdate = (DropDownList)e.Row.FindControl("ddlPlaceNameUpdate");

                        ddlPlaceNameUpdate.AppendDataBoundItems = true;

                        _data_networkdevices.m0room_list = new m0room_detail[1];
                        m0room_detail _m0roomDetail = new m0room_detail();

                        _data_networkdevices.m0room_list[0] = _m0roomDetail;

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                        _data_networkdevices = callServiceNetwork(_urlGetddlplace, _data_networkdevices);

                        ddlPlaceNameUpdate.DataSource = _data_networkdevices.m0room_list;
                        ddlPlaceNameUpdate.DataTextField = "place_name";
                        ddlPlaceNameUpdate.DataValueField = "place_idx";
                        ddlPlaceNameUpdate.DataBind();
                        ddlPlaceNameUpdate.Items.Insert(0, new ListItem("กรุณาเลือกชื่อสถานที่ติดตั้งอุปกรณ์....", "0"));
                        ddlPlaceNameUpdate.SelectedValue = lbddlPlaceName.Text;
                    }
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int room_idx_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlPlaceNameUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlPlaceNameUpdate");
                var txtRoomNameUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtRoomNameUpdate");
                var ddlRoomStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlRoomStatusUpdate");

                GvMaster.EditIndex = -1;

                _data_networkdevices.m0room_list = new m0room_detail[1];
                m0room_detail _m0room_detail = new m0room_detail();

                _m0room_detail.room_idx = room_idx_update;
                _m0room_detail.place_idx = int.Parse(ddlPlaceNameUpdate.SelectedValue);
                _m0room_detail.room_name = txtRoomNameUpdate.Text;
                _m0room_detail.room_status = int.Parse(ddlRoomStatusUpdate.SelectedValue);
                _m0room_detail.cemp_idx = emp_idx;

                _data_networkdevices.m0room_list[0] = _m0room_detail;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));
                _data_networkdevices = callServiceNetwork(_urlSetm0Room, _data_networkdevices);

                if (_data_networkdevices.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    actionIndex();


                }
                else
                {
                    setError(_data_networkdevices.return_code.ToString() + " - " + _data_networkdevices.return_msg);
                }


                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch (fvName.ID)
        {



        }
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_networkdevices callServiceNetwork(string _cmdUrl, data_networkdevices _data_networkdevices)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_networkdevices);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_networkdevices = (data_networkdevices)_funcTool.convertJsonToObject(typeof(data_networkdevices), _localJson);

        return _data_networkdevices;
    }


    #endregion reuse

}