﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="MachineRegister.aspx.cs" Inherits="websystem_EN_MachineRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" runat="server">

                    <li id="litoindex" runat="server">
                        <asp:LinkButton ID="lblindex" runat="server"
                            CommandName="btnindex"
                            OnCommand="btnCommand" Text="ข้อมูลทั่วไป" />
                    </li>


                    <li id="litoinsert" runat="server">
                        <asp:LinkButton ID="LinkButton1" runat="server"
                            CommandName="btninsert"
                            OnCommand="btnCommand" Text="เพิ่มข้อมูล" />
                    </li>
                    <li id="litoapprove" runat="server">
                        <asp:LinkButton ID="lblinsert" runat="server"
                            CommandName="btnapprove"
                            OnCommand="btnCommand" Text="รายการรอนุมัติ" />
                    </li>




                    <%--  <li id="litoMaster" runat="server" class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">MasterData <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li id="limastertypemachine" runat="server">
                                <asp:LinkButton ID="lblmastertypemachine" runat="server"
                                    CommandName="btnmastertypemachine"
                                    OnCommand="btnCommand" Text="MasterData ประเภทเครื่องจักร" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li id="listatus" runat="server">
                                <asp:LinkButton ID="lblmasterstatus" runat="server"
                                    CommandName="btnmasterstatus"
                                    OnCommand="btnCommand" Text="MasterData สถานะรายการ" />
                            </li>
                        </ul>
                    </li>--%>
                </ul>

            </div>
        </nav>
    </div>
    <!--*** END Menu ***-->


    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">


        <%-------- Index Start----------%>
        <asp:View ID="ViewIndex" runat="server">

            <div id="gridviewindex" runat="server">
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <asp:GridView ID="GvViewMachine"
                            runat="server"
                            AutoGenerateColumns="false"
                            DataKeyNames="MCIDX"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            HeaderStyle-CssClass="info"
                            HeaderStyle-Height="40px"
                            AllowPaging="true"
                            PageSize="10"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            OnRowDataBound="Master_RowDataBound">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center;">Data Cannot Be Found</div>
                            </EmptyDataTemplate>

                            <Columns>

                                <asp:TemplateField HeaderText="ประเภทเครื่องจักร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("NameTypeTH") %>' /></small>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="รหัสเครื่องจักร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                    <ItemTemplate>
                                        <small>
                                            <strong>
                                                <asp:Label ID="Label2" runat="server"> Class : </asp:Label></strong>
                                            <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("TC_Name") %>' />
                                            <br />
                                            <strong>
                                                <asp:Label ID="Label60" runat="server"> ชื่อเครื่องจักร : </asp:Label></strong>
                                            <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("NameMachine") %>' />
                                            <br />
                                            <strong>
                                                <asp:Label ID="Label61" runat="server"> รหัสเก่า : </asp:Label></strong>
                                            <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("MCCode") %>' />
                                            <br />
                                            <strong>
                                                <asp:Label ID="lbdat" runat="server"> รหัสใหม่: </asp:Label></strong>
                                            <asp:Literal ID="lbdaterecive" runat="server" Text='<%# Eval("Abbreviation") %>' />

                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="รายละเอียดอุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <small>
                                            <strong>
                                                <asp:Label ID="Label60" runat="server"> สถานที่ : </asp:Label></strong>
                                            <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("LocName") %>' />
                                            <br />
                                            <strong>
                                                <asp:Label ID="Label61" runat="server"> อาคาร : </asp:Label></strong>
                                            <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("PlName") %>' />
                                            <br />
                                            <strong>
                                                <asp:Label ID="lbdat" runat="server"> ห้อง: </asp:Label></strong>
                                            <asp:Literal ID="lbdaterecive" runat="server" Text='<%# Eval("RoomName") %>' />

                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="ข้อมูลผู้ถือครอง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                    <ItemTemplate>
                                        <small>
                                            <strong>
                                                <asp:Label ID="Label60" runat="server"> องค์กร : </asp:Label></strong>
                                            <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("LocName") %>' />
                                            <br />
                                            <strong>
                                                <asp:Label ID="Label61" runat="server"> ฝ่าย : </asp:Label></strong>
                                            <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("PlName") %>' />
                                            <br />
                                            <strong>
                                                <asp:Label ID="lbdat" runat="server"> แผนก: </asp:Label></strong>
                                            <asp:Literal ID="lbdaterecive" runat="server" Text='<%# Eval("RoomName") %>' />
                                            <br />
                                            <strong>
                                                <asp:Label ID="Label1" runat="server"> ผู้ถือครอง: </asp:Label></strong>
                                            <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("RoomName") %>' />

                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="สถานะ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="txtIDSta" runat="server" Text='<%# Eval("STApprove")%>'></asp:Label>
                                            <br />
                                            <asp:Label ID="Label19" runat="server" Text='<%# Eval("MCStatusDetail")%>'></asp:Label>

                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>

                        </asp:GridView>
                    </div>
                </div>
            </div>


        </asp:View>

        <asp:View ID="ViewAdd" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>

                    <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>

            </div>

            <div class="col-lg-12">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-cog"></i><strong>&nbsp; ระบบทะเบียนเครื่องจักร</strong></h3>
                    </div>


                    <asp:FormView ID="FvInsertMachine" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group" runat="server">

                                        <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="รหัสกลุ่มเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypecode" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">Select TypeCode...</asp:ListItem>
                                            </asp:DropDownList>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddltypecode" Font-Size="11"
                                                ErrorMessage="เลือกประเภทเครื่องจักร"
                                                ValidationExpression="เลือกประเภทเครื่องจักร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />


                                        </div>
                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="รหัสกลุ่ม : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlgroupcode" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">Select GroupCode...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlgroupcode" Font-Size="11"
                                                ErrorMessage="เลือกสถานที่แจ้งซ่อม"
                                                ValidationExpression="เลือกสถานที่แจ้งซ่อม" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group" runat="server">

                                        <asp:Label ID="Label1" class="col-sm-2 control-label" runat="server" Text="ประเภทเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypemachine" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">Select Type...</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddltypemachine" Font-Size="11"
                                                ErrorMessage="เลือกอาคาร"
                                                ValidationExpression="เลือกอาคาร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                        </div>
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="Class : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypeclass" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">Select Class...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group" runat="server">

                                        <asp:Label ID="Label3" class="col-sm-2 control-label" runat="server" Text="ชื่อเครื่องจักร TH : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtnameth" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtnameth" Font-Size="11"
                                                ErrorMessage="กรุณากรอกชื่อเครื่องจักร TH"
                                                ValidationExpression="กรุณากรอกคชื่อเครื่องจักร TH"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtnameth"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                        </div>
                                        <asp:Label ID="Label10" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อเครื่องจักร EN : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtnameen" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtnameen" Font-Size="11"
                                                ErrorMessage="กรุณากรอกชื่อเครื่องจักร EN"
                                                ValidationExpression="กรุณากรอกคชื่อเครื่องจักร EN"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtnameen"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group" runat="server">

                                        <asp:Label ID="Label11" class="col-sm-2 control-label" runat="server" Text="บริษัทผู้ผลิต : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtcompany" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtcompany" Font-Size="11"
                                                ErrorMessage="กรุณากรอกชื่อบริษัทผู้ผลิต"
                                                ValidationExpression="กรุณากรอกชื่อบริษัทผู้ผลิต"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtcompany"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                        </div>
                                        <asp:Label ID="Label13" CssClass="col-sm-3 control-label" runat="server" Text="รุ่นเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtgen" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtgen" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรุ่นเครื่องจักร"
                                                ValidationExpression="กรุณากรอกรุ่นเครื่องจักร"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtgen"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator4" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group" runat="server">

                                        <asp:Label ID="Label14" class="col-sm-2 control-label" runat="server" Text="หมายเลขเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtserial" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtserial" Font-Size="11"
                                                ErrorMessage="กรุณากรอกชื่อบริษัทผู้ผลิต"
                                                ValidationExpression="กรุณากรอกชื่อบริษัทผู้ผลิต"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtserial"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator5" Width="160" />

                                        </div>
                                        <asp:Label ID="Label15" CssClass="col-sm-3 control-label" runat="server" Text="บริษัทประกันเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtcom_machine" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtcom_machine" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรุ่นเครื่องจักร"
                                                ValidationExpression="กรุณากรอกรุ่นเครื่องจักร"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtcom_machine"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator6" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group" runat="server">

                                        <asp:Label ID="Label16" class="col-sm-2 control-label" runat="server" Text="วันที่ซื้อเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>
                                        <asp:Label ID="Label17" CssClass="col-sm-3 control-label" runat="server" Text="วันที่หมดประกันเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="AddEndDate" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" runat="server">

                                        <asp:Label ID="Label21" class="col-sm-2 control-label" runat="server" Text="เลขที่ทรัพย์สิน : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtaccess" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtaccess" Font-Size="11"
                                                ErrorMessage="กรุณากรอกเลขที่ทรัพย์สิน"
                                                ValidationExpression="กรุณากรอกเลขที่ทรัพย์สิน"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtaccess"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator7" Width="160" />


                                        </div>
                                        <asp:Label ID="Label22" CssClass="col-sm-3 control-label" runat="server" Text="สถานที่ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddllocate" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">Select Location...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlroom" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานที่"
                                                ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group" runat="server">

                                        <asp:Label ID="Label18" class="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlbuilding" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">Select Building...</asp:ListItem>
                                            </asp:DropDownList>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlbuilding" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกอาคาร"
                                                ValidationExpression="กรุณาเลือกอาคาร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />


                                        </div>
                                        <asp:Label ID="Label20" CssClass="col-sm-3 control-label" runat="server" Text="ห้อง : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlroom" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">Select Room...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlroom" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกห้อง"
                                                ValidationExpression="กรุณาเลือกห้อง" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                        </div>
                                    </div>

                                    <%-- <hr />--%>

                                    <div class="form-group">
                                        <asp:Label ID="Label59" runat="server" Text="องค์กรถือครอง :" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlholder_orgidx" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlholder_orgidx" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกองค์กร"
                                                ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />

                                        </div>
                                        <asp:Label ID="Label4" runat="server" Text="ฝ่ายถือครอง :" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlholder_rdeptidx" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">Select Department...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlholder_rdeptidx" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกฝ่าย"
                                                ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label23" runat="server" Text="แผนกถือครอง :" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlholder_rdsecidx" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">Select Section...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlholder_rdsecidx" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกแผนก"
                                                ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />

                                        </div>
                                        <asp:Label ID="Label24" runat="server" Text="ชื่อผู้ถือครอง :" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlholder_empidx" AutoPostBack="true" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">Select Employee...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlholder_empidx" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกพนักงาน"
                                                ValidationExpression="กรุณาเลือกพนักงาน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator16" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label25" runat="server" Text="สถานะ :" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlstatusmachine" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlholder_rdsecidx" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกแผนก"
                                                ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender25" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />


                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label26" runat="server" Text="เพิ่มเติม :" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtremark" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtremark" Font-Size="11"
                                                ErrorMessage="กรุณากรอกข้อมูลเพิ่มเติม"
                                                ValidationExpression="กรุณากรอกข้อมูลเพิ่มเติม"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender26" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator18" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender27" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator8" Width="160" />


                                        </div>
                                    </div>

                                    <asp:UpdatePanel ID="UpdatePnFileUpload" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <asp:Label ID="Label6" CssClass="col-sm-2 control-label" runat="server" Text="แนบไฟล์" />

                                                <div class="col-md-6">
                                                    <asp:FileUpload ID="UploadFileRoom" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                        CssClass="btn btn-warning btn-sm" accept="jpg|png" />
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-10">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbladd" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>

            </div>

        </asp:View>
    </asp:MultiView>

</asp:Content>

