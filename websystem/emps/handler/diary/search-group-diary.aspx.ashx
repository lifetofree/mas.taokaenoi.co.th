﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{
   public void ProcessRequest(HttpContext context)
   {
      string callback = context.Request.QueryString["callback"];
      string keyword = String.Empty;
      keyword = context.Request.QueryString["keyword"];
      string json = this.getGroupDiary(keyword);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string getGroupDiary(string keyword)
   {
      List<object> emp = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            if (keyword != String.Empty)
            {
               cmd.CommandText = "SELECT m0_group_diary_idx, group_diary_name, OrgNameTH, DeptNameTH, SecNameTH FROM view_emps_m0_group_diary WHERE (group_diary_name LIKE '%' + @keyword + '%' OR OrgNameTH LIKE '%' + @keyword + '%' OR DeptNameTH LIKE '%' + @keyword + '%' OR SecNameTH LIKE '%' + @keyword + '%') AND group_diary_status <> 9";
               cmd.Parameters.AddWithValue("@keyword", keyword);
            }
            cmd.Connection = conn;
            conn.Open();
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
               while (sdr.Read())
               {
                  emp.Add(new
                  {
                     groupDiaryIdx = sdr["m0_group_diary_idx"],
                     groupDiaryName = sdr["group_diary_name"],
                     groupDiaryOrgNameTH = sdr["OrgNameTH"],
                     groupDiaryDeptNameTH = sdr["DeptNameTH"],
                     groupDiarySecNameTH = sdr["SecNameTH"]
                  });
               }
            }
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emp));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}
