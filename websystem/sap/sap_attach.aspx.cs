using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;
using System.IO;
using System.Net;

public partial class websystem_sap_sap_attach : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _data_employee = new data_employee();
    data_sap_attach _data_sap_attach = new data_sap_attach();

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetViewEmployeeListSmall = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewEmployeeListSmall"];

    static string _urlSapAttachGetFileData = _serviceUrl + ConfigurationManager.AppSettings["urlSapAttachGetFileData"];
    static string _urlSapAttachSetFileData = _serviceUrl + ConfigurationManager.AppSettings["urlSapAttachSetFileData"];

    static string _path_file_sap_attach = ConfigurationManager.AppSettings["path_file_sap_load_file"];
    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = _funcTool.convertToInt(Session["emp_idx"].ToString());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
        }

        setTrigger();
    }

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "navList":
                clearViewState();
                setActiveTab("viewList", 0);
                break;
            case "navCreate":
                clearViewState();
                setActiveTab("viewCreate", 0);
                break;
        }
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdUpload":
                TextBox tbPrNo = (TextBox)fvUploadFiles.FindControl("tbPrNo");
                TextBox tbComment = (TextBox)fvUploadFiles.FindControl("tbComment");
                FileUpload fuUpload = (FileUpload)fvUploadFiles.FindControl("fuUpload");

                if (tbPrNo.Text.Trim() == String.Empty)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอก PR No. ค่ะ');", true);
                    return;
                }

                if (!fuUpload.HasFile)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('รบกวนเลือกไฟล์ที่ต้องการอัพโหลดค่ะ');", true);
                    return;
                }

                sap_attach_detail_u0 _createData = new sap_attach_detail_u0();
                _createData.u0_idx = 0;
                _createData.emp_idx = _emp_idx;
                _createData.pr_no = tbPrNo.Text.Trim();
                _createData.file_comment = tbComment.Text.Trim();

                _data_sap_attach.sap_attach_list_u0 = new sap_attach_detail_u0[1];
                _data_sap_attach.sap_attach_list_u0[0] = _createData;

                _data_sap_attach = callServicePostSapAttach(_urlSapAttachSetFileData, _data_sap_attach);
                // litDebug.Text = _funcTool.convertObjectToJson(_data_sap_attach);

                if (_data_sap_attach.return_code != 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถเชื่อมต่อเซิฟเวอร์ได้ กรุณาลองใหม่อีกครั้งค่ะ');", true);
                    return;
                }

                if (fuUpload.HasFile)
                {
                    string FileName = Path.GetFileName(fuUpload.PostedFile.FileName);
                    string extension = Path.GetExtension(fuUpload.PostedFile.FileName);
                    string newFileName = _data_sap_attach.sap_attach_list_u0[0].file_name + extension.ToLower();
                    string folderPath = _path_file_sap_attach;
                    string filePath = Server.MapPath(folderPath + newFileName);

                    fuUpload.SaveAs(filePath);

                    _funcTool.uploadToFtp("ftp://172.16.11.24/uploadfiles/sap/", "clouddrive01", "v-PZ_XuSdy=5zGa!", filePath, newFileName);

                    File.Delete(filePath);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('รบกวนเลือกไฟล์ที่ต้องการอัพโหลดค่ะ');", true);
                    return;
                }

                _funcTool.setFvData(fvUploadFiles, FormViewMode.ReadOnly, _data_sap_attach.sap_attach_list_u0);
                // setActiveTab("viewList", 0);
                break;
            case "cmdCancel":
                _funcTool.setFvData(fvUploadFiles, FormViewMode.Insert, null);
                setActiveTab("viewList", 0);
                break;
            case "cmdItemSearch":
                _data_sap_attach = (data_sap_attach)ViewState["FileList"];
                sap_attach_detail_u0[] _tempData = (sap_attach_detail_u0[])_data_sap_attach.sap_attach_list_u0;
                // select all
                var _linqSearch = from o in _tempData
                                  select o;
                if (tbItemSearch.Text.Trim() != "")
                    _linqSearch = _linqSearch
                                    .Where(o => o.pr_no.Contains(tbItemSearch.Text.Trim())
                                    );

                _funcTool.setGvData(gvFileList, _linqSearch.ToArray());
                break;
            case "cmdItemReset":
                tbItemSearch.Text = String.Empty;
                _funcTool.setGvData(gvFileList, ((data_sap_attach)ViewState["FileList"]).sap_attach_list_u0);
                break;
        }
    }
    #endregion event command

    #region databound
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvFileList":
                // if (e.Row.RowType == DataControlRowType.DataRow)
                // {
                //     Label lblScoreCal1 = (Label)e.Row.FindControl("lblScoreCal1");
                //     Label lblScoreCal2 = (Label)e.Row.FindControl("lblScoreCal2");
                //     Label lblScoreCal3 = (Label)e.Row.FindControl("lblScoreCal3");
                //     Label lblScoreCal4 = (Label)e.Row.FindControl("lblScoreCal4");
                //     Label lblScoreCal5 = (Label)e.Row.FindControl("lblScoreCal5");
                //     Label lblScoreCalSum = (Label)e.Row.FindControl("lblScoreCalSum");

                //     lblScoreCalSum.Text = _funcTool.setDecimalPlaces(((
                //         _funcTool.convertToDecimal(lblScoreCal1.Text) +
                //         _funcTool.convertToDecimal(lblScoreCal2.Text) +
                //         _funcTool.convertToDecimal(lblScoreCal3.Text) +
                //         _funcTool.convertToDecimal(lblScoreCal4.Text) +
                //         _funcTool.convertToDecimal(lblScoreCal5.Text))), 4).ToString();
                // }
                break;
        }
    }
    #endregion databound

    #region gridview paging
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvFileList":
                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                _funcTool.setGvData(gridViewName, ((data_sap_attach)ViewState["FileList"]).sap_attach_list_u0);
                break;
        }

        hlSetTotop.Focus();
    }
    #endregion gridview paging

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveTab("viewList", 0);

        hlSetTotop.Focus();
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        ViewState["FileList"] = null;
        // ViewState["ReportListExport"] = null; //linq result
    }

    protected void setTrigger()
    {
        // nav trigger
        linkBtnTrigger(lbList);
        linkBtnTrigger(lbCreate);

        // // fvSearch trigger
        // LinkButton lbHeadSearch = (LinkButton)fvSearch.FindControl("lbSearch");
        // LinkButton lbHeadClear = (LinkButton)fvSearch.FindControl("lbClear");
        // LinkButton lbHeadExport = (LinkButton)fvSearch.FindControl("lbExport");
        // try { linkBtnTrigger(lbHeadSearch); } catch { }
        // try { linkBtnTrigger(lbHeadClear); } catch { }
        // try { linkBtnTrigger(lbHeadExport); } catch { }

        // fvUploadFiles trigger
        LinkButton lbUpload = (LinkButton)fvUploadFiles.FindControl("lbUpload");
        LinkButton lbCancel = (LinkButton)fvUploadFiles.FindControl("lbCancel");
        try { linkBtnTrigger(lbUpload); } catch { }
        try { linkBtnTrigger(lbCancel); } catch { }

        gridViewTrigger(gvFileList);
    }

    protected void setActiveTab(string activeTab, int doc_idx)
    {
        setActiveView(activeTab, doc_idx);
        setActiveTabBar(activeTab);

        switch (activeTab)
        {
            case "viewList":
                search_sap_attach_detail _search_list = new search_sap_attach_detail();
                _data_sap_attach.search_sap_attach_list = new search_sap_attach_detail[1];
                _data_sap_attach.search_sap_attach_list[0] = _search_list;

                _data_sap_attach = callServicePostSapAttach(_urlSapAttachGetFileData, _data_sap_attach);
                _funcTool.setGvData(gvFileList, _data_sap_attach.sap_attach_list_u0);
                ViewState["FileList"] = _data_sap_attach;
                break;
            case "viewCreate":
                search_key_employee _search_key = new search_key_employee();
                _search_key.s_emp_idx = _emp_idx.ToString();
                _data_employee.search_key_emp_list = new search_key_employee[1];
                _data_employee.search_key_emp_list[0] = _search_key;
                _data_employee = callServicePostEmployee(_urlGetViewEmployeeListSmall, _data_employee);
                _funcTool.setFvData(fvEmpProfile, FormViewMode.ReadOnly, _data_employee.employee_list_small);

                _funcTool.setFvData(fvUploadFiles, FormViewMode.Insert, null);
                break;
        }
    }

    protected void setActiveView(string activeTab, int doc_idx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
    }

    protected void setActiveTabBar(string activeTab)
    {
        switch (activeTab)
        {
            case "viewList":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                break;
            case "viewCreate":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                break;
        }
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void gridViewTrigger(GridView gridViewID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerGridView = new PostBackTrigger();
        triggerGridView.ControlID = gridViewID.UniqueID;
        updatePanel.Triggers.Add(triggerGridView);
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _data_employee)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_employee);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _local_json);

        return _data_employee;
    }

    protected data_sap_attach callServicePostSapAttach(string _cmdUrl, data_sap_attach _data_sap_attach)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_sap_attach);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_sap_attach = (data_sap_attach)_funcTool.convertJsonToObject(typeof(data_sap_attach), _local_json);

        return _data_sap_attach;
    }
    #endregion reuse
}