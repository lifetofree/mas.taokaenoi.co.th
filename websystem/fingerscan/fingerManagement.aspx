﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="fingerManagement.aspx.cs" Inherits="websystem_fingerscan_fingerManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">


    <script type="text/javascript">
        function openModal() {
            $('#ordine').modal('show');
        }
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <%--<div class="panel panel-primary">--%>
                    <%--<div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; FingerScan List</strong></h3>
                        </div>--%>

                    <asp:Literal ID="fs" runat="server" Visible="false"></asp:Literal>
                    <asp:Literal ID="da" runat="server" Visible="true"></asp:Literal>
                    <asp:Literal ID="litResult" runat="server" Visible="false"></asp:Literal>

                    <div class="panel-body">

                        <div id="BoxPermission" runat="server" visible="true">

                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">ข้อมูลทั่วไป</div>
                                    <div class="panel-body">
                                        <%--<div class="col-lg-12">--%>
                                        <div class="col-lg-6">

                                            <div class="form-group" runat="server">

                                                <div class="row">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <div class="col-lg-12">
                                                                <asp:Label ID="Label21" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร" />
                                                                <div class="col-sm-9">
                                                                    <asp:DropDownList ID="ddlOrganization" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RqOrganization_search" ValidationGroup="Search" runat="server" Display="None"
                                                                        ControlToValidate="ddlOrganization" Font-Size="11"
                                                                        ErrorMessage="... Select Organization"
                                                                        ValidationExpression="... Select Organization" InitialValue="0" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqOrganization_search" Width="160" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <div class="col-lg-12">
                                                                <asp:Label ID="Label23" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย" />
                                                                <div class="col-sm-9">
                                                                    <asp:DropDownList ID="ddlDepartment" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                        <asp:ListItem Text="Select Department...." Value="0"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <%--<asp:RequiredFieldValidator ID="RqDepartment_ApproveAll" ValidationGroup="Approve_All" runat="server" Display="None"
                                        ControlToValidate="ddlDepartment_ApproveAll" Font-Size="11"
                                        ErrorMessage="... Select Department"
                                        ValidationExpression="... Select Department" InitialValue="0" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqDepartment_ApproveAll" Width="160" />--%>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <div class="col-lg-12">
                                                                <asp:Label ID="Label29" CssClass="control-label col-sm-2" runat="server" Text="แผนก" />
                                                                <div class="col-sm-9">
                                                                    <asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Text="Select Section...." Value="0"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <div class="col-lg-12">
                                                                <asp:Label ID="Label5" runat="server" Text="รหัสพนักงาน" CssClass="col-sm-2 control-label"></asp:Label>
                                                                <div class="col-sm-9">
                                                                    <asp:TextBox ID="txtempid" CssClass="form-control" runat="server" placeholder="Ex. 57000001"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <div class="col-lg-12">
                                                            <div class="col-sm-1 col-sm-offset-1">

                                                                <asp:Button ID="btnsearch" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch" OnCommand="btnCommand" />
                                                            </div>

                                                            <div class="col-sm-1 col-sm-offset-1">
                                                                <asp:Button ID="btnclearsearch" class="btn btn-primary" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="col-lg-12">

                                                <hr />

                                            </div>


                                            <asp:GridView ID="GvTemp" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                HeaderStyle-CssClass="primary"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="true"
                                                PageSize="10"
                                                OnPageIndexChanging="Master_PageIndexChanging">
                                                <HeaderStyle CssClass="success" />

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                        <ItemTemplate>
                                                            <%#(Container.DataItemIndex + 1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="เครื่องสแกนนิ้ว" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_ip_finger" runat="server" Text='<%# Eval("ip_finger") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbfm_empcode" runat="server" Text='<%# Eval("empcode") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="จำนวนลายนิ้วมือ" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbfm_name" runat="server" Text='<%# Eval("finger_index") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="โอนสำเร็จ" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbfm_finger_status_database" runat="server" Text='<%# Eval("finger_status_database") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>

                                            <asp:GridView ID="GvEmpDatabase" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                HeaderStyle-CssClass="primary"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="true"
                                                DataKeyNames="EmpIDX"
                                                PageSize="10"
                                                OnPageIndexChanging="Master_PageIndexChanging">
                                                <HeaderStyle CssClass="success" />

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnSelect" CssClass="btn btn-success btn-xs" data-toggle="tooltip" title="Select" runat="server" CommandName="btnselect" OnCommand="btnCommand"
                                                                CommandArgument='<%# Eval("EmpIDX")%>'><i class="fa fa-plus-square"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbfm_empcode" runat="server" Text='<%# Eval("EmpCode") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbfm_name" runat="server" Text='<%# Eval("FullNameTH") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>

                                        </div>

                                        <div class="col-lg-6">

                                            <asp:GridView ID="GvEmpTemp" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                HeaderStyle-CssClass="primary"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="true"
                                                DataKeyNames="EmpIDX"
                                                PageSize="17"
                                                OnPageIndexChanging="Master_PageIndexChanging">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <%#(Container.DataItemIndex + 1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbfm_empcode" runat="server" Text='<%# Eval("EmpCode") %>'></asp:Label>
                                                            <asp:Label ID="lbEmpstatus" runat="server" Visible="false" Text='<%# Eval("EmpStatus") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbfm_name" runat="server" Text='<%# Eval("FullNameTH") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-xs" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" CommandArgument='<%# Eval("EmpIDX") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>


                                        </div>

                                    </div>
                                </div>
                            </div>


                            <div class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">ข้อมูลการโอนย้าย</div>
                                    <div class="panel-body">


                                        <div class="row">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <asp:Label ID="Label2" runat="server" Text="ประเภทดำเนินการ" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-4">
                                                        <asp:DropDownList ID="ddlTranfer_Type" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem Text="Select Tranfer Type" Selected="True" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Tranfer-Finger (โอนย้ายลายนิ้วมือ)" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Delete-Finger (ลบลายนิ้วมือ)" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form-group" runat="server" id="Admin_FingerCenter" visible="false">
                                                    <asp:Label ID="Labeel21" runat="server" Text="เครื่อง Center" CssClass="col-sm-2 control-label" Visible="true"></asp:Label>
                                                    <div class="col-sm-4">
                                                        <asp:DropDownList ID="ddlfinger_center" runat="server" CssClass="form-control"></asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="Tranfer" runat="server" Display="None"
                                                            ControlToValidate="ddlfinger_center" Font-Size="11"
                                                            ErrorMessage="... Select finger_center"
                                                            ValidationExpression="... Select finger_center" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                                    </div>
                                                </div>
                                                <div class="form-group" runat="server" id="Admin_FingerCheck" visible="false">
                                                    <asp:Label ID="Label1" runat="server" Text="Check Finger" CssClass="col-sm-2 control-label" Visible="true"></asp:Label>
                                                    <div class="col-sm-4">
                                                        <asp:DropDownList ID="ddlfinger_center_check" runat="server" CssClass="form-control"></asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidato2r1" ValidationGroup="Tranfer_check" runat="server" Display="None"
                                                            ControlToValidate="ddlfinger_center_check" Font-Size="11"
                                                            ErrorMessage="... Select finger_center"
                                                            ValidationExpression="... Select finger_center" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidato2r1" Width="160" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr />


                                        <div class="row">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <div class="col-sm-11 col-sm-offset-1">

                                                        <div class="checkbox checkbox-primary">
                                                            <asp:CheckBox ID="chkAll" Text="เลือกเครื่องที่โอนย้าย" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged"
                                                                CellPadding="10"
                                                                CellSpacing="10"
                                                                RepeatColumns="2"
                                                                RepeatDirection="Vertical"
                                                                RepeatLayout="Table"
                                                                TextAlign="Right"
                                                                Width="100%" />
                                                        </div>
                                                    </div>



                                                    <asp:Label ID="Label10" runat="server" CssClass="col-sm-2 control-label" Visible="true"></asp:Label>
                                                    <div class="col-sm-11 col-sm-offset-1">
                                                        <div class="checkbox checkbox-primary">
                                                            <asp:CheckBoxList ID="YrChkBox"
                                                                runat="server"
                                                                CellPadding="10"
                                                                CellSpacing="10"
                                                                RepeatColumns="4"
                                                                RepeatDirection="Vertical"
                                                                RepeatLayout="Table"
                                                                TextAlign="Right"
                                                                Width="100%">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <hr />


                                        <div class="row">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <div class="col-sm-6">
                                                        <asp:LinkButton ID="lbltranfer" CssClass="btn btn-success" runat="server" data-original-title="Tranfer" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdtranfer" OnClientClick="return confirm('คุณต้องการ Tranfer ลายนิ้วมือใช่หรือไม่ ?')" ValidationGroup="Tranfer" Visible="false"><span class="fa fa-exchange" aria-hidden="true"> Tranfer</span></asp:LinkButton>
                                                        <asp:LinkButton ID="lbltranfer_delete" CssClass="btn btn-danger" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdtranfer_delete" OnClientClick="return confirm('คุณต้องการลบลายนิ้วมือใช่หรือไม่ ?')" ValidationGroup="Tranfer_Delete" Visible="false"><i class="fa fa-exchange"> Delete</i></span></asp:LinkButton>
                                                        <asp:LinkButton ID="lblcheck" CssClass="btn btn-defult" runat="server" data-original-title="Check" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdcheck_tranfer" ValidationGroup="Tranfer_Delete"><i class="fa fa-exchange"> Check</i></span></asp:LinkButton>
                                                        <asp:LinkButton ID="lbltranfer_view" CssClass="btn btn-primary" runat="server" data-original-title="Tranfer" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdtranfer_view" ValidationGroup="Tranfer_check" Visible="false"><span class="fa fa-file-text" aria-hidden="true"> Check Finger</span></asp:LinkButton>
                                                        <%--fa fa-trash-o--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <div id="ordine" class="modal open" role="dialog">
                                            <div class="modal-dialog" style="width: 70%;">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title">ตรวจเช็คลายนิ้วมือ</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="panel-body">
                                                            <div class="form-horizontal" role="form">

                                                                <asp:GridView ID="GvCheck_Finger" runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                                    HeaderStyle-CssClass="primary"
                                                                    HeaderStyle-Height="40px"
                                                                    AllowPaging="true"
                                                                    PageSize="10"
                                                                    OnPageIndexChanging="Master_PageIndexChanging">
                                                                    <HeaderStyle CssClass="success" />

                                                                    <PagerStyle CssClass="pageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>
                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <%#(Container.DataItemIndex + 1) %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="เครื่องสแกนนิ้ว" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_ip_finger" runat="server" Text='<%# Eval("ip_finger") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbfm_empcode" runat="server" Text='<%# Eval("empcode") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จำนวนลายนิ้วมือ" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbfm_name" runat="server" Text='<%# Eval("finger_index") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                </asp:GridView>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>


                </asp:View>
            </asp:MultiView>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

