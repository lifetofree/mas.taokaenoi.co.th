﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="drc_m0_form.aspx.cs" Inherits="websystem_MasterData_drc_m0_form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="litdebug" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <asp:MultiView ID="MvSystem" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btnAddForm" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มฟอร์ม" data-toggle="tooltip" title="เพิ่มฟอร์ม DRC" runat="server"
                    CommandName="cmdAddForm" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มฟอร์ม DRC</asp:LinkButton>
            </div>

            <asp:FormView ID="fvInsertForm" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <InsertItemTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading f-bold">เพิ่มฟอร์ม DRC</div>
                                <div class="panel-body">
                                    <div class="col-md-10 col-md-offset-1">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>ชื่อฟอร์ม Daily Check</label>
                                                <asp:TextBox ID="txt_formname" runat="server" CssClass="form-control" placeholder="กรอกชื่อฟอร์ม ..." />
                                                <asp:RequiredFieldValidator ID="Re_txt_formname"
                                                    runat="server"
                                                    ControlToValidate="txt_formname" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อฟอร์ม"
                                                    ValidationGroup="btnSave" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_formname" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <asp:UpdatePanel ID="updateEquimentInsert" runat="server">
                                            <ContentTemplate>
                                                <div class="col-md-12">
                                                    <div id="div_DetailEquiment" class="panel panel-default" runat="server">
                                                        <div class="panel-body">
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <label>ชื่อหัวข้อ</label>
                                                                    <asp:DropDownList ID="ddlTopicname" runat="server" CssClass="form-control" />
                                                                    <asp:RequiredFieldValidator ID="Re_ddlTopicname"
                                                                        runat="server"
                                                                        InitialValue="0"
                                                                        ControlToValidate="ddlTopicname" Display="None" SetFocusOnError="true"
                                                                        ErrorMessage="*กรุณาเลือกชื่อหัวข้อ"
                                                                        ValidationGroup="AddlTopicname" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlTopicname" Width="250" />

                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label>&nbsp;</label>
                                                                    <div class="clearfix"></div>
                                                                    <asp:LinkButton ID="btnAddTopicName" runat="server"
                                                                        CssClass="btn btn-primary col-md-12"
                                                                        Text="เพิ่มหัวข้อ" OnCommand="btnCommand" CommandName="cmdAddTopicName"
                                                                        ValidationGroup="AddlTopicname" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <asp:GridView ID="gvTopicList"
                                                                    runat="server"
                                                                    CssClass="table table-striped table-responsive"
                                                                    GridLines="None"
                                                                    OnRowCommand="onRowCommand"
                                                                    AutoGenerateColumns="false">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                            HeaderStyle-CssClass="text-center"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                            <ItemTemplate>
                                                                                <%# (Container.DataItemIndex + 1) %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="drTopicText" HeaderText="ชื่อหัวข้อ" ItemStyle-CssClass="text-center"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                            HeaderStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnRemoveTopicName" runat="server"
                                                                                    CssClass="btn btn-danger btn-xs"
                                                                                    OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                                    CommandName="btnRemoveTopicName"><i class="fa fa-times"></i>
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                        <asp:UpdatePanel ID="UpdatePnFileUpload" runat="server">
                                            <ContentTemplate>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <asp:LinkButton ID="btnInsertForm" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                            CommandName="CmdSave" Text="บันทึก" ValidationGroup="btnSave"
                                                            OnClientClick="return confirm('ยืนยันการบันทึก')" />
                                                        <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                            CommandName="CmdCancel" Text="ยกเลิก" />
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnInsertForm" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>

            </asp:FormView>

            <asp:GridView ID="GvFormDetail" runat="server" Visible="true"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="lbl_R0IDX" runat="server" Visible="false" Text='<%# Eval("R0IDX") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อฟอร์ม" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    
                                    <asp:Label ID="lbl_R0_Name" runat="server" Text='<%# Eval("R0_Name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="หัวข้อหลัก" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <table class="table table-striped f-s-12 table-empshift-responsive">
                                    <asp:Repeater ID="rptTopicDetail" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td data-th="หัวข้อหลัก"><%# Eval("topic_name") %></td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:UpdatePanel ID="UpdatePnFileUploadEdit" runat="server">
                                <ContentTemplate>
                                    <div style="padding-top: 5px">

                                        <asp:LinkButton ID="btnTodeleteForm" CssClass="text-edit" runat="server" CommandName="CmdDeleteForm" CommandArgument='<%# Eval("R0IDX") %>' data-toggle="tooltip" OnCommand="btnCommand" title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                       
                                    </div>
                                </ContentTemplate>
                               
                            </asp:UpdatePanel>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>
    </asp:MultiView>

</asp:Content>
