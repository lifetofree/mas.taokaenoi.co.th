﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="logManagement.aspx.cs" Inherits="websystem_controlpanel_logManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script type="text/javascript">

        $(document).ready(function () {

            var SciptIDDate = '.datepickerbasic';
            var datestart = new Date();
            var dateend = new Date();


            // ส่วนของ หน้าหลัก  dateend.setDate(dateend.getDate() - 1);
            datestart.setYear(datestart.getYear() - 100);

            $(SciptIDDate).datetimepicker({
                pickdate: null,
                pickTime: true,
                useMinutes: true,
                minuteStepping: 15,
                icons: {
                    time: "fa fa-clock-o fa-5x",
                    date: "fa fa-calendar fa-5x",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: 'DD/MM/YYYY HH:mm',
            });


            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
            function getme() {

                var SciptIDExp = '.datepickerExp';
                var date = new Date();
                date.setDate(date.getDate() - 1);
                $(SciptIDExp).datetimepicker({
                    pickdate: null,
                    pickTime: true,
                    useMinutes: true,
                    minuteStepping: 15,
                    icons: {
                        time: "fa fa-clock-o fa-5x",
                        date: "fa fa-calendar fa-5x",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    format: 'DD/MM/YYYY HH:mm',
                });


                var SciptID = '.datepickerbasic';
                var datestart = new Date();
                var dateend = new Date();
                // ส่วนของ หน้าหลัก  dateend.setDate(dateend.getDate() - 1);
                datestart.setYear(datestart.getYear() - 100);

                $(SciptID).datetimepicker({

                    pickdate: null,
                    pickTime: true,
                    useMinutes: true,
                    minuteStepping: 15,
                    icons: {
                        time: "fa fa-clock-o fa-5x",
                        date: "fa fa-calendar fa-5x",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    format: 'DD/MM/YYYY HH:mm',
                    /*startDate: datestart,
                  endDate: dateend,*/

                });

            }


        });

    </script>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <%----------- Menu Tab Start---------------%>
    <div id="BoxTabMenuIndex" runat="server">
        <div class="form-group">
            <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Menu1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="sub-navbar">
                            <a class="navbar-brand " href="#"><b>Menu</b></a>
                        </div>
                    </div>

                    <div class="collapse navbar-collapse" id="Menu1">
                        <asp:LinkButton ID="lbindex" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand" CommandArgument="1">General</asp:LinkButton>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
    </div>
    <%-------------- Menu Tab End--------------%>
    <asp:Literal ID="litDebug" runat="server" />
    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">
        <asp:View ID="ViewIndex" runat="server">
            <div class="col-lg-12">
                <asp:Label ID="fs" runat="server"></asp:Label>
                <asp:HyperLink runat="server" ID="txtfocus" />
                <%-------------- BoxSearch Start--------------%>
                <asp:Panel ID="boxsearch" runat="server" Visible="true">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Selection Search</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" id="BoxSearch_1" runat="server">
                                <asp:FormView ID="Fv_Search_log_Index" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <InsertItemTemplate>
                                        <div class="form-group">
                                            <asp:Label ID="Label23" CssClass="col-sm-2 control-label" runat="server" Text="Log Detail :" />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_search_logdetail" CssClass="form-control" runat="server" placeholder="Ex. xxx,xxx,xxx"></asp:TextBox>
                                            </div>
                                            <asp:Label ID="Label4" runat="server" Text="Emp Code :" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_search_empcode" CssClass="form-control" runat="server" placeholder="Ex. 56xxxxxx,57xxxxxx"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label67" runat="server" Text="IP Address :" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_search_ipaddress" CssClass="form-control" runat="server" placeholder="Ex. xxx,xxx,xxx"></asp:TextBox>
                                            </div>
                                            <asp:Label ID="Label48" runat="server" Text="Emp Name :" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_search_empname" CssClass="form-control" runat="server" placeholder="Ex. xxx,xxx,xxx"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div id="BoxIFDate" runat="server">
                                            <div class="form-group">
                                                <asp:Label ID="AddStart" CssClass="col-sm-2 control-label" runat="server" Text="Date :" />
                                                <div class="col-sm-2">
                                                    <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Text="เลือกเงื่อนไข ...." Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="มากกว่า >" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="น้อยกว่า <" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="ระหว่าง <>" Value="3"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3 ">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control from-date-datepicker" AutoComplete="off" MaxLengh="100%" placeholder="Ex. 24/05/2017 00:00"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3 ">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="AddEndDate" AutoComplete="off" MaxLength="10" CssClass="form-control from-date-datepicker" Enabled="False" runat="server" placeholder="Ex. 24/05/2017 00:00"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label2" runat="server" Text="Event Type :" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-5">
                                                <div class="checkbox checkbox-primary">
                                                    <asp:CheckBoxList ID="YrChkBox"
                                                        runat="server"
                                                        CellPadding="10"
                                                        CellSpacing="10"
                                                        RepeatColumns="2"
                                                        RepeatDirection="Vertical"
                                                        RepeatLayout="Table"
                                                        TextAlign="Right"
                                                        Width="100%">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2"></label>
                                            <div class="col-sm-6">
                                                <asp:Button ID="btnsearch_index" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="Search" data-original-title="Search" data-toggle="tooltip" CommandName="btn_search_index" OnCommand="btnCommand" />
                                                <asp:Button ID="btnclearsearch_index" class="btn btn-default" runat="server" Text="Clear" data-original-title="Clear" data-toggle="tooltip" CommandName="btn_search_clear" OnCommand="btnCommand" />
                                            </div>
                                        </div>
                                    </InsertItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <%-------------- BoxSearch End--------------%>

                <asp:Panel ID="boxindex" runat="server" Visible="true">
                    <asp:GridView ID="Gv_Log"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="info"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10"
                        DataKeyNames="uidx"
                        OnPageIndexChanging="Master_PageIndexChanging">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>
                            <asp:TemplateField HeaderText="#" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <%# (Container.DataItemIndex +1) %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Event Type" HeaderStyle-Width="20%">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_event" runat="server" Text='<%# Eval("event_type") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="IP Address" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="lb_ip_address" runat="server" Text='<%# Eval("ip_address") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Create Date" HeaderStyle-Width="20%">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_create_date" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Employee Name" HeaderStyle-Width="30%">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_emp_idx" runat="server" Text='<%# Eval("emp_idx") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lbl_emp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Management" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnView" CssClass="btn-sm btn-info" runat="server" CommandName="btnView_log" OnCommand="btnCommand" CommandArgument='<%# Eval("uidx") %>' data-toggle="tooltip" title="View"><i class="fa fa-file-text-o"></i></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </asp:Panel>

                <asp:Panel ID="boxview" runat="server" Visible="false">
                    <asp:Literal ID="lbl_view_log" runat="server"></asp:Literal>
                    <asp:FormView ID="FvViewDetaillog" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <EditItemTemplate>
                            <div class="panel panel-success class">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Log Detail</strong></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <asp:Label ID="Label1" CssClass="text_right col-sm-2 control-labelnotop" runat="server" Text="Employee Name : " />
                                            <div class="col-sm-3">
                                                <asp:Label ID="lbl_view_emp_name_th" CssClass="control-labelnotop" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                            </div>

                                            <asp:Label ID="Label3" CssClass="text_right col-sm-2 control-labelnotop" runat="server" Text="IP Address : " />
                                            <div class="col-sm-3">
                                                <asp:Label ID="lbl_view_ip_address" CssClass="control-labelnotop" runat="server" Text='<%# Eval("ip_address") %>'></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label7" CssClass="text_right col-sm-2 control-labelnotop" runat="server" Text="Event Type : " />
                                            <div class="col-sm-3">
                                                <asp:Label ID="lbl_view_event_type" CssClass="control-labelnotop" runat="server" Text='<%# Eval("event_type") %>'></asp:Label>
                                            </div>

                                            <asp:Label ID="Label9" CssClass="text_right col-sm-2 control-labelnotop" runat="server" Text="Create Date : " />
                                            <div class="col-sm-3">
                                                <asp:Label ID="lbl_view_create_date" CssClass="control-labelnotop" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label12" class="text_right col-sm-2 control-labelnotop" runat="server" Text="Detail : " />
                                            <div class="col-sm-10">
                                                <asp:TextBox id="txt_view_log_detail" TextMode="MultiLine" Width="100%" Rows="20" BorderStyle="None" BackColor="White" runat="server" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <asp:LinkButton ID="lbBack" CssClass="btn btn-default" runat="server" ValidationGroup="Cancel" CommandName="btn_view_back" OnCommand="btnCommand" CommandArgument="1" title="Back"><i class="fa fa-reply"> Back</i></asp:LinkButton>
                            </div>
                        </EditItemTemplate>
                    </asp:FormView>
                </asp:Panel>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>
