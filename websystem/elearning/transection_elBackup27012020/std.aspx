﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="std.aspx.cs" Inherits="websystem_el_std" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Label ID="lbindex" runat="server" Text=""></asp:Label>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/JavaScript.js"></script>--%>

    <script type="text/javascript">

        function openfunc(value) {
            winGoogle = window.open("http://google.com");
            winGmail = window.open("http://gmail.com");
            winMsdn = window.open("http://msdn.com");
            winyahoo = window.open("http://yahoo.com");
            windeveloper = window.open("http://developercenter.azurewebsites.net/");

        }

        function closefunc() {
            winGoogle.close();
            winGmail.close();
            winMsdn.close();
            winyahoo.close();
            windeveloper.close();
        }

    </script>
    <input type="button" value="Open" onclick="openfunc(5)" />
<input type="button" value="Close" onclick="closefunc()" />
    <asp:Panel ID="Panel1" runat="server">

        <div class="row">
            <div class="col-md-2">
                <asp:Label ID="Label73" runat="server" Text="jsonIntoxml"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txt_jsonIn" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnjsonIntoxml" runat="server" Text="ok" OnClick="btnjsonIntoxml_Click" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <asp:Label ID="Label74" runat="server" Text="flow item"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txt_flow_item" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-2">
            </div>
        </div>

    </asp:Panel>


    <div class="row">
        <table style="width: 100%;">
            <tr>
                <td colspan="3">
                    <input id="xx" class="btn-default pull-left " />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <%--        <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!-- BEGIN PAGE CONTENT-->
                <div class="col-md-12">
                    <div id="UpdatePanel1">
                        <div id="wrapper" style="text-align: left">
                            <table class="style1">
                                <tr>
                                    <td style="text-align: center">
                                        <span id="ContentPlaceHolder1_lblshopTitle" style="font-family: High Tower Text; font-size: 23px;">Walmart inc</span>
                                        <br />
                                        <span id="ContentPlaceHolder1_lblshopAddress" style="font-size: 11px;">Central Office, NY NER2AE, USA</span>
                                        <br />
                                        <span id="ContentPlaceHolder1_lblwebAddress" style="font-size: 11px;">SYS89</span>
                                        <br />

                                        <span id="ContentPlaceHolder1_Label16" style="font-size: 11px;">Phone:</span>
                                        <span id="ContentPlaceHolder1_lblPhone" style="font-size: 11px;">3318976622</span>
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span id="ContentPlaceHolder1_Label1" style="font-size: 11px;">Date:</span>
                                        <span id="ContentPlaceHolder1_lblDatetime" style="font-size: 11px;">Mar 25, 2018.  11:10:06 PM</span>
                                        <br />
                                        <table class="style2">
                                            <tr>
                                                <td>
                                                    <span id="ContentPlaceHolder1_Label3" style="font-size: 11px;">VAT REG:</span>
                                                    <span id="ContentPlaceHolder1_lblVATRegiNo" style="font-size: 11px;">13.000</span>
                                                </td>
                                                <td>

                                                    <span id="ContentPlaceHolder1_Label4" style="font-size: 11px;">Shop ID:</span>
                                                    <span id="ContentPlaceHolder1_lblShopID" style="font-size: 11px;">SYS89</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style1">
                                                    <span id="ContentPlaceHolder1_Label2" style="font-size: 11px;">Customer ID:</span>
                                                    <span id="ContentPlaceHolder1_lblCustID" style="font-size: 11px;">00</span>
                                                </td>
                                                <td class="auto-style1">

                                                    <span id="ContentPlaceHolder1_Label6" style="font-size: 11px;">Contact No:</span>
                                                    <span id="ContentPlaceHolder1_lblCustContactNo" style="font-size: 11px;">121</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span id="ContentPlaceHolder1_Label5" style="font-size: 11px;">Name:</span>
                                                    <span id="ContentPlaceHolder1_lblCustName" style="font-size: 11px;">Guest</span>
                                                </td>
                                                <td>
                                                    <span id="ContentPlaceHolder1_Label7" style="font-size: 11px;">ServedBy:</span>
                                                    <span id="ContentPlaceHolder1_lblServedBy" style="font-size: 11px;">admin</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <span id="ContentPlaceHolder1_Label15" style="font-size: 11px;">Invoice #</span>
                                        <span id="ContentPlaceHolder1_lblInvoice" style="font-size: 11px;">1993</span>
                                        <br />
                                        <div>
                                            <table cellspacing="0" cellpadding="4" rules="rows" id="ContentPlaceHolder1_grdItemList" style="color: Black; background-color: White; border-color: #CCCCCC; border-width: 1px; border-style: None; font-size: 11px; width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <th scope="col">Code</th>
                                                    <th scope="col">Items</th>
                                                    <th scope="col">Qty</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Disc%</th>
                                                    <th scope="col">Total</th>
                                                </tr>
                                                <tr>
                                                    <td>00001</td>
                                                    <td>nokia 3310</td>
                                                    <td>1</td>
                                                    <td>20.00</td>
                                                    <td>0.00</td>
                                                    <td>20.00</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="style1">
                                            <tr>
                                                <td><span id="ContentPlaceHolder1_Label17" style="font-size: 11px;">Total Sales Qty:</span>
                                                    <br />
                                                </td>
                                                <td style="text-align: right;" class="auto-style2"><span id="ContentPlaceHolder1_lblTotalQty" style="font-size: 11px; font-weight: bold;">1</span>
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span id="ContentPlaceHolder1_Label14" style="font-size: 11px;">Total Amount:</span>  </td>
                                                <td class="text-right" style="text-align: right"><span id="ContentPlaceHolder1_lblsubTotal" style="font-size: 11px; font-weight: bold;">20</span>   </td>
                                            </tr>
                                            <tr>
                                                <td style="border-bottom-style: solid; border-bottom-width: thin;">
                                                    <span id="ContentPlaceHolder1_lblvatRate" style="font-size: 11px;">VAT:13.000%</span></td>
                                                <td style="border-bottom-style: solid; border-bottom-width: thin; text-align: right;" class="text-right"><span id="ContentPlaceHolder1_lblvat" style="font-size: 11px;">2.6</span></td>
                                            </tr>
                                            <tr>
                                                <td><span id="ContentPlaceHolder1_Label9" style="font-size: 12px;">Net Amount:</span>
                                                    <br />
                                                    <br />
                                                </td>
                                                <td style="text-align: right;" class="text-right">
                                                    <span id="ContentPlaceHolder1_lbltotalpay" style="font-size: 12px; font-weight: bold;">22.6</span>
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span id="ContentPlaceHolder1_Label8" style="font-size: 11px;">Pay Type:</span>
                                                    <br />
                                                </td>
                                                <td class="text-right" style="text-align: right"><span id="ContentPlaceHolder1_lblpaidby" style="font-size: 11px;">Cash</span>
                                                    <br />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><span id="ContentPlaceHolder1_Label10" style="font-size: 11px;">Paid Amount</span> </td>
                                                <td class="text-right" style="text-align: right"><span id="ContentPlaceHolder1_lblPaidAmt" style="font-size: 11px;">50</span></td>
                                            </tr>
                                            <tr>
                                                <td><span id="ContentPlaceHolder1_Label11" style="font-size: 11px;">Changed Amount:</span>
                                                    <br />
                                                    <br />
                                                </td>
                                                <td class="text-right" style="text-align: right"><span id="ContentPlaceHolder1_lblChange" style="font-size: 11px;">27.4</span>
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="border-bottom-style: solid; border-bottom-width: thin;">
                                                    <span id="ContentPlaceHolder1_Label12" style="font-size: 11px;">Due Amount:</span> </td>
                                                <td style="border-bottom-style: solid; border-bottom-width: thin; text-align: right">
                                                    <span id="ContentPlaceHolder1_lblDue" style="font-size: 11px;">0</span> </td>
                                                <div style="border-top: 1px solid #000; padding-top: 10px;"></div>
                                            </tr>
                                        </table>

                                        <span style="text-align: center">
                                            <span id="ContentPlaceHolder1_lblFooterMessage" style="font-size: 12px;">Item sold will not be refund only EXCHANGE will be executed with in 5 days.
                                                    <br />
                                                Email: sales@waltmart.com</span>
                                        </span>

                                    </td>
                                </tr>

                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTAINER -->
    --%>
</asp:Content>

