<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="managemenu.aspx.cs" Inherits="websystem_controlpanel_managemenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
	<div class="col-md-12" role="main">
		<asp:Literal ID="litDebug" runat="server"></asp:Literal>

        <!--*** START SystemList ***-->
		<div class="col-md-6">
			<div id="fvViewSystemList" runat="server" class="panel panel-info">
				<div class="panel-heading">System list</div>
				<div class="panel-body">
					<asp:DataList ID="dtlSystem" runat="server" RepeatDirection="Vertical"
            RepeatColumns="1">
						<ItemTemplate>
							<div class="form-group">
								<span class="glyphicon glyphicon-tasks text-default" aria-hidden="true"></span>

                                <asp:Label ID="lblSysteIDX" runat="server" Text='<%# Eval("system_idx")%>' Visible="false"></asp:Label>
								<asp:Label ID="lblSystemNameTH" runat="server" Text='<%# Eval("system_name_th")%>'></asp:Label>
								
								<span class="glyphicon glyphicon-option-vertical text-default" aria-hidden="true"></span>
								<asp:LinkButton ID="lbEditSystem" CssClass="" runat="server" data-original-title="Edit" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdEditSystem" CommandArgument='<%# Eval("system_idx")%>'><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></asp:LinkButton>
								<asp:LinkButton ID="lbInsertMenu" CssClass="" runat="server" data-original-title="ADD Menu" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdInsertMenu" CommandArgument='<%# Eval("system_idx")%>'><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></asp:LinkButton>
                                <asp:LinkButton ID="lbDeleteSystem" CssClass="" runat="server" data-original-title="Delete" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdDeleteSystem" OnClientClick="return confirm('Do you want to delete system?')" CommandArgument='<%# Eval("system_idx")%>'><span class="glyphicon glyphicon-remove" style="color:#FF0000;" aria-hidden="true"></span></asp:LinkButton>
                                 <asp:LinkButton ID="lbRoleSystem" CssClass="" runat="server" data-original-title="AddRoleSystem" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdAddRoleSystem" CommandArgument='<%# Eval("system_idx")%>'><span class="glyphicon glyphicon-user" aria-hidden="true"></span></asp:LinkButton>
							</div>
						</ItemTemplate>
					</asp:DataList>
				</div>
			</div>
		</div>
        <!--*** END SystemList ***-->

        <!--*** START System ***-->
        <div class="col-md-6">            
            <div id="fvViewSystem" runat="server" class="panel panel-info">
                <div class="panel-heading">System</div>
                    <div class="panel-body">                       
                        <asp:Formview ID="fvSystem" runat="server" DefaultMode="Insert" width="100%">
                            <InsertItemTemplate>   
                                 <asp:HiddenField ID="hfSystemIdx" runat="server" Value="0"></asp:HiddenField>                    
							<div class="form-horizontal" role="form">
								<div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblSysNameTH" runat="server" Text="SysName TH"></asp:Label></label>
                                    <asp:Label ID="Tesredit" runat="server" Text="insert" Visible="false"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbSysNameTH" runat="server" CssClass="form-control" placeholder="SysName TH" MaxLength="30" ValidationGroup="formInsert1" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="formInsert1" runat="server" Display="None"
                                            ControlToValidate="tbSysNameTH" Font-Size="11"
                                            ErrorMessage="Please Enter SystemTH "
                                            ValidationExpression="Please Enter SystemTH" InitialValue="" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight"   TargetControlID="RequiredFieldValidator6" Width="160" />


									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblSysNameEN" runat="server" Text="SysName EN"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbSysNameEN" runat="server" CssClass="form-control" placeholder="SysName EN" MaxLength="30" ValidationGroup="formInsert1" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator111" ValidationGroup="formInsert1" runat="server" Display="None"
                                            ControlToValidate="tbSysNameEN" Font-Size="11"
                                            ErrorMessage="Please Enter SystemEN"
                                            ValidationExpression="Please Enter SystemEN" InitialValue="" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight"   TargetControlID="RequiredFieldValidator111" Width="160" />
									</div>
								</div>															
								<div class="form-group">
									<div class="col-sm-offset-4 col-sm-8">
										<asp:LinkButton ID="lbInsertSystem" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdSaveSystem" CommandArgument="0" ValidationGroup="formInsert1"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>
										<asp:LinkButton ID="lbClear" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelSystem"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
									</div>
								</div>
							</div>
						    </InsertItemTemplate>    
                              
                            <EditItemTemplate>   
                             <asp:HiddenField ID="hfSystemIdx" runat="server" Value='<%# Eval("system_idx")%>'></asp:HiddenField>          
							<div class="form-horizontal" role="form">
                              
								<div class="form-group">
									<label class="col-sm-4 control-label">
                                        
									<asp:Label ID="lblSysNameTH" runat="server" Text="SysName TH"></asp:Label></label>
                                    <asp:Label ID="Tesredit" runat="server" Text="update" Visible="false"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbSysNameTH" runat="server" CssClass="form-control" placeholder="SysName TH" MaxLength="30" Text='<%# Eval("system_name_th")%>' ValidationGroup="formEditsystem" />
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblSysNameEN" runat="server" Text="SysName EN"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbSysNameEN" runat="server" CssClass="form-control" placeholder="SysName EN" MaxLength="30" Text='<%# Eval("system_name_en")%>' ValidationGroup="formEditsystem" />
									</div>
								</div>
																
								<div class="form-group">
									<div class="col-sm-offset-4 col-sm-8">
										<asp:LinkButton ID="lbInsertSystem" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdSaveSystem" CommandArgument='<%# Eval("system_idx")%>' ValidationGroup="formEditsystem"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>
										<asp:LinkButton ID="lbClear" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelSystem"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
									</div>
								</div>
							</div>
						    </EditItemTemplate>   
                                  
                        </asp:Formview>
                    </div>
            </div>

        </div>
        <!--*** END System ***-->

        <!--*** START Back To System ***-->   
                 <div class="form-group">
                <div id="fvBacktoSystem" class="row" runat="server" visible="false">
					<div class="col-md-1">						
						<asp:LinkButton ID="lbBlack" CssClass="btn btn-info" runat="server" data-original-title="Back to System" data-toggle="tooltip" Text="Back" OnCommand="btnCommand" CommandName="cmdBacktoSystem"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span></asp:LinkButton>
					</div>
				</div>
                </div>   
         <!--*** END Back To System ***-->

        <!--*** START MenuList ***-->
		<div class="col-md-6">
			<div id="fvViewMenu" runat="server" class="panel panel-info" visible="false">
				<div class="panel-heading">Menu list</div>
				<div class="panel-body">
					<asp:DataList ID="dtlMenu" runat="server" RepeatDirection="Vertical"
            RepeatColumns="1">
						<ItemTemplate>
							<div class="form-group">
								<span class="glyphicon glyphicon-unchecked text-default" aria-hidden="true"></span>
                                <asp:Label ID="lbllistsysidx" runat="server" Text='<%# Eval("system_idx")%>' Visible="false" ></asp:Label>
								<%--<asp:Label ID="lblMenuNameTH" runat="server" Text='<%# Eval("menu_name_th")%>'></asp:Label>--%>
                                <asp:Label ID="lblMenuNameTH" runat="server" Text='<%# Eval("menu_name_thtest")%>'></asp:Label>
                                <asp:Label ID="lblMenuLevel" runat="server" Text='<%# Eval("menu_level")%>' Visible="false"></asp:Label>

                               <%--  <asp:Label ID="lblMenuLevel" runat="server" Visible="true" class='<%# Eval("menu_level").ToString() == "1" ? "fa fa-check" : "fa fa-times"  %>'></asp:Label>--%>

                                 <%--<asp:HiddenField ID="hfSysMenuIdxRelation" runat="server" Value='<%# Eval("system_idx")%>'></asp:HiddenField>--%>
								<asp:HiddenField ID="hfMenuRelation" runat="server" Value='<%# Eval("menu_relation")%>'></asp:HiddenField>
                                <%--<%# Eval("CStatus").ToString() == "1" ? "fa fa-check" : "fa fa-times" %>'--%>

								<span class="glyphicon glyphicon-option-vertical text-default" aria-hidden="true"></span>
								<asp:LinkButton ID="lbEdit" CssClass="" runat="server" data-original-title="Edit menu" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdEditMenu" CommandArgument='<%# Eval("menu_idx")+ ";" + Eval("system_idx")%>'><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></asp:LinkButton>
								<asp:LinkButton ID="lbSubMenuInsert" CssClass="" runat="server" data-original-title="Insert sub menu" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdInsertSubMenu" CommandArgument='<%# Eval("menu_idx")+ ";" + Eval("system_idx")%>'><span class="glyphicon glyphicon-plus text-success" aria-hidden="true"></span></asp:LinkButton>
                                <asp:LinkButton ID="lbSubMenuDelete" CssClass="" runat="server" data-original-title="Delete" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdDeleteSubMenu" OnClientClick="return confirm('Do you want to delete menu?')" CommandArgument='<%# Eval("menu_idx")+ ";" + Eval("system_idx")%>'><span class="glyphicon glyphicon-remove" style="color:#FF0000;" aria-hidden="true"></span></asp:LinkButton>
                                 <asp:LinkButton ID="lbPermissionmenu" CssClass="" runat="server" data-original-title="AddPermissionMenu" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdAddPermission" CommandArgument='<%# Eval("menu_idx")+ ";" + Eval("system_idx")%>'><span class="glyphicon glyphicon-user" aria-hidden="true"></span></asp:LinkButton>
							</div>
						</ItemTemplate>
					</asp:DataList>
				</div>
			</div>
		</div>
        <!--*** END MenuList ***-->

        <!--*** START MenuDetail ***-->
		<div class="col-md-6">
			<div id="fvViewMenudetail" runat="server" class="panel panel-info" visible="false">
				<div class="panel-heading">Menu detail</div>
				<div class="panel-body">
					<asp:Formview ID="fvMenu" runat="server" DefaultMode="Insert" width="100%">
						<InsertItemTemplate>
							<asp:HiddenField ID="hfMenuIdx" runat="server" Value="0"></asp:HiddenField>
							<asp:HiddenField ID="hfMenuRelation" runat="server" Value="0"></asp:HiddenField>
                            <asp:HiddenField ID="hfSysMenuIdx" runat="server" Value='<%# Eval("system_idx")%>'></asp:HiddenField>
							<div class="form-horizontal" role="form">
                                <asp:Label ID="fvinsertsysidx" runat="server" visible="false"></asp:Label></label>
                                <div class="form-group">
                                     
									<label class="col-sm-4 control-label">                                  
									<asp:Label ID="lbsysidxinsert" runat="server" Text="SysIDXMenuinsert" Visible="false"></asp:Label></label>
									<%--<div class="col-sm-8">
										<asp:TextBox ID="tbsysidx" runat="server" CssClass="form-control" placeholder="Sys IDX" MaxLength="30" Text='<%# Eval("system_idx")%>' ValidationGroup="formInsert" />
									</div>--%>
								</div>
								<div class="form-group">                                 
									<label class="col-sm-4 control-label">                                  
									<asp:Label ID="lblNameTH" runat="server" Text="Name TH"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbNameTH" runat="server" CssClass="form-control" placeholder="Name TH" MaxLength="30" Text='<%# Eval("menu_relation")%>' ValidationGroup="formInsert" />
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator65" ValidationGroup="formInsert" runat="server" Display="None"
                                            ControlToValidate="tbNameTH" Font-Size="11"
                                            ErrorMessage="Please Enter NameTH "
                                            ValidationExpression="Please Enter NameTH" InitialValue="" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight"   TargetControlID="RequiredFieldValidator65" Width="160" />

									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblNameEN" runat="server" Text="Name EN"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbNameEN" runat="server" CssClass="form-control" placeholder="Name EN" MaxLength="30" ValidationGroup="formInsert" />
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1777" ValidationGroup="formInsert" runat="server" Display="None"
                                            ControlToValidate="tbNameEN" Font-Size="11"
                                            ErrorMessage="Please Enter NameEN "
                                            ValidationExpression="Please Enter NameEN" InitialValue="" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight"   TargetControlID="RequiredFieldValidator1777" Width="160" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblMenuRelation" runat="server" Text="Parent Menu"></asp:Label></label>
									<div class="col-sm-8">
										<asp:DropDownList ID="ddlMenuRelation" runat="server" CssClass="form-control" ValidationGroup="formInsert" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblURL" runat="server" Text="URL"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbURL" runat="server" CssClass="form-control" placeholder="URL" MaxLength="500" ValidationGroup="formInsert" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-4 col-sm-8">
										<asp:LinkButton ID="lbInsert" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdSaveMenu" CommandArgument="0" ValidationGroup="formInsert"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>
										<asp:LinkButton ID="lbClear" CssClass="btn btn-warning" runat="server" data-original-title="Clear" data-toggle="tooltip" Text="Clear" OnCommand="btnCommand" CommandName="cmdClearMenu"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span></asp:LinkButton>
									</div>
								</div>
							</div>
						</InsertItemTemplate>
						<EditItemTemplate>
							<asp:HiddenField ID="hfMenuIdx" runat="server" Value='<%# Eval("menu_idx")%>'></asp:HiddenField>
							<asp:HiddenField ID="hfMenuRelation" runat="server" Value='<%# Eval("menu_relation")%>'></asp:HiddenField>
                            <asp:HiddenField ID="hfSysMenuIdx" runat="server" Value='<%# Eval("system_idx")%>'></asp:HiddenField>
							<div class="form-horizontal" role="form">

                                <asp:Label ID="fvinsertsysidx" runat="server" Visible="false"></asp:Label></label>
                                <div class="form-group">
                                     
									<label class="col-sm-4 control-label">                                  
									<asp:Label ID="lbsysidxinsert" runat="server" Text="SysIDXMenuupdate" Visible="false"></asp:Label></label>
									<%--<div class="col-sm-8">
										<asp:TextBox ID="tbsysidx" runat="server" CssClass="form-control" placeholder="Sys IDX" MaxLength="30" Text='<%# Eval("system_idx")%>' ValidationGroup="formEdit" />
									</div>--%>
								</div>

								<div class="form-group">
                                    
									<label class="col-sm-4 control-label">                                 
									<asp:Label ID="lblNameTH" runat="server" Text="Name TH"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbNameTH" runat="server" CssClass="form-control" placeholder="Name TH" MaxLength="30" Text='<%# Eval("menu_name_th")%>' ValidationGroup="formEdit" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblNameEN" runat="server" Text="Name EN"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbNameEN" runat="server" CssClass="form-control" placeholder="Name EN" MaxLength="30" Text='<%# Eval("menu_name_en")%>' ValidationGroup="formEdit" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblMenuRelation" runat="server" Text="Parent Menu"></asp:Label></label>
									<div class="col-sm-8">
										<asp:DropDownList ID="ddlMenuRelation" runat="server" CssClass="form-control" ValidationGroup="formEdit" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblURL" runat="server" Text="URL"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbURL" runat="server" CssClass="form-control" placeholder="URL" MaxLength="500" Text='<%# Eval("menu_url")%>' ValidationGroup="formEdit" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-4 col-sm-8">
										<asp:LinkButton ID="lbInsert" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdSaveMenu" CommandArgument='<%# Eval("menu_idx")%>' ValidationGroup="formEdit"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>
										<asp:LinkButton ID="lbCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelMenu"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
									</div>
								</div>
							</div>
						</EditItemTemplate>
					</asp:Formview>
				</div>
			</div>
		</div>
        <!--*** END MenuDetail ***-->

         <!--*** START Back To System in ADDRole ***-->   
                <div class="form-group">
                <div id="fvBacktoSysteminAddRole" class="row" runat="server" visible="false">
					<div class="col-md-12">						
						<asp:LinkButton ID="lbBlacktoSys" CssClass="btn btn-info" runat="server" data-original-title="Back to System" data-toggle="tooltip" Text="Back" OnCommand="btnCommand" CommandName="cmdBacktoSysteminAddRole"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span></asp:LinkButton>
                       <%-- <asp:LinkButton ID="lbADDRoleinSystem" CssClass="btn btn-success" runat="server" data-original-title="ADDRole" data-toggle="tooltip" Text="ADD Role" OnCommand="btnCommand" CommandName="cmdADDRoleinSystem"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></asp:LinkButton>--%>
					</div>
				</div>
                </div>
         
         <!--*** END Back To System in ADDRole ***-->

         <!--*** START ADDRoleList ***-->
        <div class="col-md-6">
            <div id="fvViewRole" runat="server" class="panel panel-info" visible="false">
               <div class="panel-heading">Role list</div>
                <div class="panel-body">
                    <asp:DataList ID="dtlAddRole" runat="server" RepeatDirection="Vertical"
            RepeatColumns="1">
                        <ItemTemplate>
                            <div class="form-group">
								<span class="glyphicon glyphicon-unchecked text-default" aria-hidden="true"></span>
                                <asp:Label ID="lbllistroleidx" runat="server" Text='<%# Eval("role_idx")%>' Visible="false" ></asp:Label>
								<asp:Label ID="lblRoleNameTH" runat="server" Text='<%# Eval("role_name_th")%>'></asp:Label>
                                 <%--<asp:HiddenField ID="hfSysMenuIdxRelation" runat="server" Value='<%# Eval("system_idx")%>'></asp:HiddenField>--%>
								<%--<asp:HiddenField ID="hfMenuRelation" runat="server" Value='<%# Eval("menu_relation")%>'></asp:HiddenField>--%>


								<span class="glyphicon glyphicon-option-vertical text-default" aria-hidden="true"></span>
								<asp:LinkButton ID="lbEditRole" CssClass="" runat="server" data-original-title="Edit role" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdEditRole" CommandArgument='<%# Eval("role_idx")+ ";" + Eval("system_idx")+ ";" + Eval("menu_idx")%>'><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></asp:LinkButton>
							<%--	<asp:LinkButton ID="lbSubMenuInsert" CssClass="" runat="server" data-original-title="Insert sub menu" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdInsertSubMenu" CommandArgument='<%# Eval("role_idx")+ ";" + Eval("system_idx")%>'><span class="glyphicon glyphicon-plus text-success" aria-hidden="true"></span></asp:LinkButton>--%>
                                <asp:LinkButton ID="lbRoleDelete" CssClass="" runat="server" data-original-title="Delete" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdDeleteRole" OnClientClick="return confirm('Do you want to delete role?')" CommandArgument='<%# Eval("role_idx")+ ";" + Eval("system_idx")+ ";" + Eval("menu_idx")%>'><span class="glyphicon glyphicon-remove" style="color:#FF0000;" aria-hidden="true"></span></asp:LinkButton>
                                <%-- <asp:LinkButton ID="lbPermissionmenu" CssClass="" runat="server" data-original-title="AddPermissionMenu" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdAddPermission" CommandArgument='<%# Eval("role_idx")+ ";" + Eval("system_idx")%>'><span class="glyphicon glyphicon-user" aria-hidden="true"></span></asp:LinkButton>--%>
							</div>

                        </ItemTemplate>

                    </asp:DataList>

                </div>

            </div>

        </div>

         <!--*** END ADDRoleList ***-->
   
         <!--*** START ADD Role in System ***-->
        <div class="col-md-6">         
                <div id="fvViewAddRole" runat="server" class="panel panel-info" visible="false">
                   <%-- <asp:TextBox ID="lblShowMenuIdx" runat="server"></asp:TextBox>--%>
                    <div class="panel-heading">ADD Role</div>
                        <div class="panel-body">
                         <div class="col-md-12"> 
                        <asp:Formview ID="fvAddRole" runat="server" DefaultMode="Insert" width="100%">
                            <InsertItemTemplate>
							    <asp:HiddenField ID="hfAddRoleIdx" runat="server" Value="0"></asp:HiddenField>
							    <asp:HiddenField ID="hfAddRoleRelationMenu" runat="server" Value="0"></asp:HiddenField>
                                <asp:HiddenField ID="hfAddPermission" runat="server" Value="0"></asp:HiddenField>
                                
							    <div class="form-horizontal" role="form">
                                    <asp:Label ID="rolesysidx" runat="server" Visible="false" ></asp:Label></label>
                                    <asp:Label ID="rolemenuidx" runat="server" Visible="false" ></asp:Label></label>

                                     <div class="form-group">
                                       <asp:Label ID="lbroleidxinsert" runat="server" Text="ADDRoleinsert" Visible="false"></asp:Label></label>
									    <label class="col-sm-4 control-label">                                       
									    <asp:Label ID="lblRoleNameTH" runat="server" Text="RoleName TH"></asp:Label></label>
									    <div class="col-sm-8"> <%--Text='<%# Eval("menu_relation")%>'--%>
										    <asp:TextBox ID="tbRoleNameTH" runat="server" CssClass="form-control" placeholder="RoleName TH" MaxLength="30" ValidationGroup="formInsert" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator32" ValidationGroup="formInsert" runat="server" Display="None"
                                            ControlToValidate="tbRoleNameTH" Font-Size="11"
                                            ErrorMessage="Please Enter RoleNameTH"
                                            ValidationExpression="Please Enter RoleNameTH" InitialValue="" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight"   TargetControlID="RequiredFieldValidator32" Width="160" />

									    </div>
								    </div>

								    <div class="form-group">
									    <label class="col-sm-4 control-label">
									    <asp:Label ID="lblRoleNameEN" runat="server" Text="RoleName EN"></asp:Label></label>
									    <div class="col-sm-8">
										    <asp:TextBox ID="tbRoleNameEN" runat="server" CssClass="form-control" placeholder="RoleName EN" MaxLength="30" ValidationGroup="formInsert" />
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator144" ValidationGroup="formInsert" runat="server" Display="None"
                                            ControlToValidate="tbRoleNameEN" Font-Size="11"
                                            ErrorMessage="Please Enter RoleNameEN"
                                            ValidationExpression="Please Enter RoleNameEN" InitialValue="" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight"   TargetControlID="RequiredFieldValidator144" Width="160" />

									    </div>
								    </div>

                                    <div class="form-group">                                         
									    <label class="col-sm-4 control-label">
									    <asp:Label ID="lblMenuNameTH" runat="server" Text="MenuName TH"></asp:Label></label>
									    <div class="col-sm-8">
										    <asp:DropDownList ID="ddlMenuNameTH" AutoPostBack="true" runat="server" CssClass="form-control" ValidationGroup="formInsert" OnSelectedIndexChanged="ddlSelectedIndexChanged" />

                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="formInsert" runat="server" Display="None"
                                            ControlToValidate="ddlMenuNameTH" Font-Size="11"
                                            ErrorMessage="Please Selected Menu"
                                            ValidationExpression="Please Selected Menu" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
									    </div>
								    </div>

                                    
                                     <div class="form-group">                                         
									   <%-- <label class="col-sm-4 control-label">
									    <asp:Label ID="Label1" runat="server" ></asp:Label></label>--%>
									    <div class="col-sm-offset-4 col-sm-8">
										    <asp:CheckBoxList ID="rdopermissiontype" runat="server" 
                                                                CellPadding="1"
                                                                CellSpacing="5"
                                                                RepeatColumns="1"
                                                                Width="100%"
                                                                RepeatDirection="Vertical"
                                                                RepeatLayout="Table"
                                                                TextAlign="Right"
                                                                ValidationGroup="formInsert">
                                            </asp:CheckBoxList> 
                                                                        
                                            
									    </div>
								    </div>
                                    
                                     <div id="fvpertypeddl" runat="server" class="form-group" visible="false" >                                          
									    <label class="col-sm-4 control-label">
									    <asp:Label ID="lbPermission" runat="server" Text="Permission Type"></asp:Label></label>
									    <div class="col-sm-6">
										    <asp:DropDownList ID="ddlPermission" runat="server" CssClass="form-control" ValidationGroup="formInsert" >
                                                <asp:ListItem Text="Select Permission...." Value="00"></asp:ListItem>
                                             </asp:DropDownList>
									    </div>

                                         <div class="col-md-2">
                                                <asp:LinkButton ID="btadd" CssClass="btn btn-default" runat="server" ValidationGroup="formInsert" CommandName="btaddperinsys" OnCommand="btnCommand" data-toggle="tooltip" title="ADD"><i class="fa fa-plus-square" aria-hidden="true"></i> </asp:LinkButton>
                                          </div>
								    </div>
                           

                                    <%--*** END SET VIEW Show Permission ***--%>

                       
								    <div id="fvbtnrole" runat="server" class="form-group " visible="true">
									    <div class="col-sm-offset-4 col-sm-8">
										    <asp:LinkButton ID="lbRoleInsert" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdSaveRole" CommandArgument="0" ValidationGroup="formInsert"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>
										    <asp:LinkButton ID="lbClear" CssClass="btn btn-warning" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Clear" OnCommand="btnCommand" CommandName="cmdClearRole"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span></asp:LinkButton>
									    </div>
								    </div>
							    </div>
						    </InsertItemTemplate>

                            <EditItemTemplate>
                                <asp:HiddenField ID="hfAddRoleIdx" runat="server" Value='<%# Eval("role_idx")%>'></asp:HiddenField>
							    <asp:HiddenField ID="hfAddRoleRelationMenu" runat="server" Value="0"></asp:HiddenField>
                               <asp:HiddenField ID="hfAddPermission" runat="server" Value='<%# Eval("permission_name")%>'></asp:HiddenField>
							    <div class="form-horizontal" role="form">
                                    <asp:Label ID="rolesysidx" runat="server" Visible="false" ></asp:Label></label>
                                    <asp:Label ID="rolemenuidx" runat="server" Visible="false" ></asp:Label></label>

                                     <div class="form-group">
                                       <asp:Label ID="lbroleidxinsert" runat="server" Text="ADDRoleupdate" Visible="false"></asp:Label></label>
									    <label class="col-sm-4 control-label">                                       
									    <asp:Label ID="lblRoleNameTH" runat="server" Text="RoleName TH"></asp:Label></label>
									    <div class="col-sm-8"> <%--Text='<%# Eval("menu_relation")%>'--%>
										    <asp:TextBox ID="tbRoleNameTH" runat="server" CssClass="form-control" placeholder="RoleName TH" MaxLength="30" Text='<%# Eval("role_name_th")%>' ValidationGroup="formEdit" />
									    </div>
								    </div>

								    <div class="form-group">
									    <label class="col-sm-4 control-label">
									    <asp:Label ID="lblRoleNameEN" runat="server" Text="RoleName EN"></asp:Label></label>
									    <div class="col-sm-8">
										    <asp:TextBox ID="tbRoleNameEN" runat="server" CssClass="form-control" placeholder="RoleName EN" MaxLength="30" Text='<%# Eval("role_name_en")%>' ValidationGroup="formEdit" />
									    </div>
								    </div>

                                    <div class="form-group">                                         
									    <label class="col-sm-4 control-label">
									    <asp:Label ID="lblMenuNameTH" runat="server" Text="MenuName TH"></asp:Label></label>
									    <div class="col-sm-8">
										    <asp:DropDownList ID="ddlMenuNameTH" AutoPostBack="true" runat="server" CssClass="form-control" ValidationGroup="formEdit" OnSelectedIndexChanged="ddlSelectedIndexChanged" />

                                               <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="formEdit" runat="server" Display="None"
                                            ControlToValidate="ddlMenuNameTH" Font-Size="11"
                                            ErrorMessage="Please Selected Menu"
                                            ValidationExpression="Please Selected Menu" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

									    </div>
								    </div>

                                    
                                     <div class="form-group">                                         
									    <%--<label class="col-sm-4 control-label">
									    <asp:Label ID="Label1" runat="server" ></asp:Label></label>--%>
									    <div class="col-sm-offset-4 col-sm-8">
                                          
										    <asp:CheckBoxList ID="rdopermissiontype" runat="server" 
                                                                CellPadding="1"
                                                                CellSpacing="5"
                                                                RepeatColumns="1"
                                                                Width="100%"
                                                                RepeatDirection="Vertical"
                                                                RepeatLayout="Table"
                                                                TextAlign="Right"
                                                                visible ="true">
                                            </asp:CheckBoxList>
                                            

                                             
									    </div>
								    </div>
                                    
                                     <div id="fvpertypeddl" runat="server" class="form-group" visible="false" >                                          
									    <label class="col-sm-4 control-label">
									    <asp:Label ID="lbPermission" runat="server" Text="Permission Type"></asp:Label></label>
									    <div class="col-sm-6">
										    <asp:DropDownList ID="ddlPermission" runat="server" CssClass="form-control" ValidationGroup="formEdit" >
                                                <asp:ListItem Text="Select Permission...." Value="00"></asp:ListItem>
                                             </asp:DropDownList>
									    </div>

                                         <div class="col-md-2">
                                                <asp:LinkButton ID="btadd" CssClass="btn btn-default" runat="server" ValidationGroup="formEdit" CommandName="btaddperinsys" OnCommand="btnCommand" data-toggle="tooltip" title="ADD"><i class="fa fa-plus-square" aria-hidden="true"></i> </asp:LinkButton>
                                          </div>
								    </div>



                                    <%--*** END SET VIEW Show Permission ***--%>

                       
								    <div id="fvbtnrole" runat="server" class="form-group " visible="true">
									    <div class="col-sm-offset-4 col-sm-8">
										    <asp:LinkButton ID="lbRoleInsert" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdSaveRole" CommandArgument='<%# Eval("role_idx")%>' ValidationGroup="formEdit"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>
										    <asp:LinkButton ID="lbClear" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Clear" OnCommand="btnCommand" CommandName="cmdCancelRole"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
									    </div>
								    </div>
							    </div>


                            </EditItemTemplate>

                        </asp:Formview>
                         </div> 
                    </div>
                </div>
            
        </div>
         <!--*** END ADD Role in System ***-->

        <!--*** START Back To Menu ***-->     
                <div class="form-group">
                    <div id="fvBacktoMenu" class="row" runat="server" visible="false">
					    <div class="col-md-12">						
						    <asp:LinkButton ID="lbBlacktoMenu" CssClass="btn btn-info" runat="server" data-original-title="Back to Menu" data-toggle="tooltip" Text="Back" OnCommand="btnCommand" CommandName="cmdBacktoMenu"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span></asp:LinkButton>
					    </div>
				    </div>
                </div>
 
         <!--*** END Back To Menu ***-->

      
          <!--*** START PermissionList ***-->
        <div class="col-md-6">
            <div id="fvDataPermissionlist" runat="server" class="panel panel-info" visible="false">
                <div class="panel-heading">Permission list</div>
                    <div class="panel-body">
                        <asp:DataList ID="dtlpermissionmenu" runat="server" RepeatDirection="Vertical"
            RepeatColumns="1">
                            <ItemTemplate>
                                <div class="form-group">
								<span class="glyphicon glyphicon-unchecked text-default" aria-hidden="true"></span>
                                    
                                <asp:Label ID="lbllistperidx" runat="server" Text='<%# Eval("permission_idx")%>' Visible="false" ></asp:Label>
								<asp:Label ID="lblPermissionName" runat="server" Text='<%# Eval("permission_name")%>'></asp:Label>
                                 <asp:HiddenField ID="hfPerIDXRelation" runat="server" Value='<%# Eval("permission_idx")%>'></asp:HiddenField>
								<%--<asp:HiddenField ID="hfPerRelation" runat="server" Value='<%# Eval("menu_relation")%>'></asp:HiddenField>--%>


								<span class="glyphicon glyphicon-option-vertical text-default" aria-hidden="true"></span>
								<asp:LinkButton ID="lbEditpermission" CssClass="" runat="server" data-original-title="Edit permission" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdEditPermission" CommandArgument='<%# Eval("permission_idx")+ ";" + Eval("system_idx")+ ";" + Eval("menu_idx")%>'><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></asp:LinkButton>
								<%--<asp:LinkButton ID="lbSubMenuInsert1" CssClass="" runat="server" data-original-title="Insert sub menu" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdInsertSubMenu1" CommandArgument='<%# Eval("permission_idx")%>'><span class="glyphicon glyphicon-plus text-success" aria-hidden="true"></span></asp:LinkButton>--%>
                                <asp:LinkButton ID="lbDeletePermission" CssClass="" runat="server" data-original-title="Delete" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdDeletePermission" OnClientClick="return confirm('Do you want to delete permission?')" CommandArgument='<%# Eval("permission_idx")+ ";" + Eval("system_idx")+ ";" + Eval("menu_idx")%>'><span class="glyphicon glyphicon-remove" style="color:#FF0000;" aria-hidden="true"></span></asp:LinkButton>
                                <%-- <asp:LinkButton ID="lbPermissionmenu" CssClass="" runat="server" data-original-title="AddPermissionMenu" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdAddPermission1" CommandArgument='<%# Eval("permission_idx")%>'><span class="glyphicon glyphicon-user" aria-hidden="true"></span></asp:LinkButton>
							</div>--%>

                            </ItemTemplate>

                        </asp:DataList>

                    </div>

            </div>

        </div>

         <!--*** END PermissionList ***-->

        <!--*** START ADD Permission in Menu ***-->
        <div class="col-md-6">  
            <div id="fvViewPermissionMenu" runat="server" class="panel panel-info" visible="false">
                <div class="panel-heading">Permission of Menu</div>
                    <div class="panel-body">
                        <asp:Formview ID="fvPermission" runat="server" DefaultMode="Insert" width="100%">
                            <InsertItemTemplate>
                                <asp:HiddenField ID="hfPermissionIdx" runat="server" Value="0"></asp:HiddenField>
                                <asp:HiddenField ID="hfddlPermissionIdx" runat="server" Value="0"></asp:HiddenField>
                                <div class="form-horizontal" role="form">
                                <asp:Label ID="permissionsysidx" runat="server" Visible="false" ></asp:Label></label>
                                <asp:Label ID="permissionmenuidx" runat="server" Visible="false" ></asp:Label></label>
                                  <div class="form-group">
                                     
									<label class="col-sm-4 control-label">                                  
									<asp:Label ID="lbsysidxpermissioninsert" runat="server" Text="Permissioninsert" Visible="false"></asp:Label></label>
									<%--<div class="col-sm-8">
										<asp:TextBox ID="tbsysidx" runat="server" CssClass="form-control" placeholder="Sys IDX" MaxLength="30" Text='<%# Eval("system_idx")%>' ValidationGroup="formInsert" />
									</div>--%>
								</div>

                                <div class="form-group">
                                   
									<label class="col-sm-4 control-label">                                  
									<asp:Label ID="lblNameTH" runat="server" Text="PermissionName"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbPermissionName" runat="server" CssClass="form-control" placeholder="Permission Name" MaxLength="30" ValidationGroup="formInsert" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6522" ValidationGroup="formInsert" runat="server" Display="None"
                                            ControlToValidate="tbPermissionName" Font-Size="11"
                                            ErrorMessage="Please Enter PermissionTH "
                                            ValidationExpression="Please Enter PermissionTH" InitialValue="" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight"   TargetControlID="RequiredFieldValidator6522" Width="160" />

									</div>

								</div>

                                <div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblPermissionType" runat="server" Text="Permission Type"></asp:Label></label>
									<div class="col-sm-8">
										<asp:DropDownList ID="ddlPermissionType" runat="server" CssClass="form-control" ValidationGroup="formInsert" />
                                       <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="formInsert" runat="server" Display="None"
                                                            ControlToValidate="ddlPermissionType" Font-Size="11"
                                                            ErrorMessage="Please Enter PermissionType"
                                                            ValidationExpression="Please Enter PermissionType" InitialValue="00" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />


									</div>
								</div>

                                <div class="form-group">
									<div class="col-sm-offset-4 col-sm-8">
										<asp:LinkButton ID="lbInsertPermission" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdSavePermission" CommandArgument="0" ValidationGroup="formInsert"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>
										<asp:LinkButton ID="lbClearPermission" CssClass="btn btn-warning" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdClearPermission"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span></asp:LinkButton>
									</div>
								</div>

                                </div>
                            </InsertItemTemplate>

                            <EditItemTemplate>
                                <asp:HiddenField ID="hfPermissionIdx" runat="server" Value='<%# Eval("permission_idx")%>'></asp:HiddenField>
                                 <asp:HiddenField ID="hfddlPermissionIdx" runat="server" Value='<%# Eval("permission_type")%>'></asp:HiddenField>
                                <div class="form-horizontal" role="form">
                                <asp:Label ID="permissionsysidx" runat="server" Visible="false" ></asp:Label></label>
                                <asp:Label ID="permissionmenuidx" runat="server" Visible="false" ></asp:Label></label>
                                  <div class="form-group">
                                     
									<label class="col-sm-4 control-label">                                  
									<asp:Label ID="lbsysidxpermissioninsert" runat="server" Text="Permissionupdate" Visible="false"></asp:Label></label>
									<%--<div class="col-sm-8">
										<asp:TextBox ID="tbsysidx" runat="server" CssClass="form-control" placeholder="Sys IDX" MaxLength="30" Text='<%# Eval("system_idx")%>' ValidationGroup="formInsert" />
									</div>--%>
								</div>

                                <div class="form-group">
                                   
									<label class="col-sm-4 control-label">                                  
									<asp:Label ID="lblNameTH" runat="server" Text="PermissionName"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbPermissionName" runat="server" CssClass="form-control" placeholder="Permission Name" MaxLength="30" Text='<%# Eval("permission_name")%>' ValidationGroup="formEdit" />
									</div>
								</div>

                                <div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblPermissionType" runat="server" Text="Permission Type"></asp:Label></label>
									<div class="col-sm-8">
										<asp:DropDownList ID="ddlPermissionType" runat="server" CssClass="form-control" ValidationGroup="formEdit" />
									</div>
								</div>

                                <div class="form-group">
									<div class="col-sm-offset-4 col-sm-8">
										<asp:LinkButton ID="lbInsertPermission" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdSavePermission" CommandArgument='<%# Eval("permission_idx")%>' ValidationGroup="formEdit"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>
										<asp:LinkButton ID="lbClearPermission" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelPermission"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
									</div>
								</div>

                                </div>

                            </EditItemTemplate>

                        </asp:Formview>

                    </div>

            </div>

        </div>  
        <!--*** END ADD Permission in Menu ***-->

	</div>	
</asp:Content>