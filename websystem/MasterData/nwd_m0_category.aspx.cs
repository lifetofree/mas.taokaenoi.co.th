﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_masterdata_nwd_m0_category : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_networkdevices _data_networkdevices = new data_networkdevices();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetddltype = _serviceUrl + ConfigurationManager.AppSettings["urlGetddltype"];
    static string _urlSetm0Category = _serviceUrl + ConfigurationManager.AppSettings["urlSetm0Category"];
    static string _urlGetm0Category = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0Category"];
    static string _urlDeletem0Category = _serviceUrl + ConfigurationManager.AppSettings["urlDeletem0Category"];

    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
    }

    #region selected   
    protected void actionIndex()
    {

        data_networkdevices _data_networkdevicesindex = new data_networkdevices();
        _data_networkdevicesindex.m0category_list = new m0category_detail[1];

        m0category_detail _m0category_detailindex = new m0category_detail();

        _m0category_detailindex.category_idx = 0;

        _data_networkdevicesindex.m0category_list[0] = _m0category_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _data_networkdevicesindex = callServiceNetwork(_urlGetm0Category, _data_networkdevicesindex);

        setGridData(GvMaster, _data_networkdevicesindex.m0category_list);

    }

    protected void actionddltype()
    {

        DropDownList ddltype = (DropDownList)ViewInsert.FindControl("ddltype");

        ddltype.Items.Clear();
        ddltype.AppendDataBoundItems = true;
        ddltype.Items.Add(new ListItem("กรุณาเลือกชื่อประเภทอุปกรณ์...", "00"));

        _data_networkdevices.m0category_list = new m0category_detail[1];
        m0category_detail _m0categoryDetail = new m0category_detail();
       
        _data_networkdevices.m0category_list[0] = _m0categoryDetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

        _data_networkdevices = callServiceNetwork(_urlGetddltype, _data_networkdevices);
     
        ddltype.DataSource = _data_networkdevices.m0category_list;
        ddltype.DataTextField = "type_name";
        ddltype.DataValueField = "type_idx";
        ddltype.DataBind();


    }


    #endregion selected 

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _category_idx;
        string _category_name;
        int _cemp_idx;

        m0category_detail _m0category_detail = new m0category_detail();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                actionddltype();
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":

                _category_name = ((TextBox)ViewInsert.FindControl("txtcategoryName")).Text.Trim();
                DropDownList _ddlcategoryStatus = (DropDownList)ViewInsert.FindControl("ddlcategoryStatus");
                DropDownList _ddltype = (DropDownList)ViewInsert.FindControl("ddltype");
                _cemp_idx = emp_idx;

                _data_networkdevices.m0category_list = new m0category_detail[1];
                _m0category_detail.category_idx = 0;//_type_idx; 
                _m0category_detail.category_name = _category_name;
                _m0category_detail.category_status = int.Parse(_ddlcategoryStatus.SelectedValue);
                _m0category_detail.cemp_idx = _cemp_idx;
                _m0category_detail.type_idx = int.Parse(_ddltype.SelectedValue);

                _data_networkdevices.m0category_list[0] = _m0category_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));
                _data_networkdevices = callServiceNetwork(_urlSetm0Category, _data_networkdevices);

                if (_data_networkdevices.return_code == 0)
                {

                    actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_networkdevices.return_code.ToString() + " - " + _data_networkdevices.return_msg);
                }


                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnDelete":

                _category_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_networkdevices.m0category_list = new m0category_detail[1];
                _m0category_detail.category_idx = _category_idx;
                _m0category_detail.cemp_idx = _cemp_idx;

                _data_networkdevices.m0category_list[0] = _m0category_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                _data_networkdevices = callServiceNetwork(_urlDeletem0Category, _data_networkdevices);


                if (_data_networkdevices.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_networkdevices.return_code.ToString() + " - " + _data_networkdevices.return_msg);
                }

                break;




        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                        var lbddlTypeName = (Label)e.Row.FindControl("lbddlTypeName");
                        var ddlTypeNameUpdate = (DropDownList)e.Row.FindControl("ddlTypeNameUpdate");

                        ddlTypeNameUpdate.AppendDataBoundItems = true;

                        _data_networkdevices.m0category_list = new m0category_detail[1];
                        m0category_detail _m0categoryDetail = new m0category_detail();

                        _data_networkdevices.m0category_list[0] = _m0categoryDetail;

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                        _data_networkdevices = callServiceNetwork(_urlGetddltype, _data_networkdevices);

                        ddlTypeNameUpdate.DataSource = _data_networkdevices.m0category_list;
                        ddlTypeNameUpdate.DataTextField = "type_name";
                        ddlTypeNameUpdate.DataValueField = "type_idx";
                        ddlTypeNameUpdate.DataBind();
                        ddlTypeNameUpdate.Items.Insert(0, new ListItem("กรุณาเลือกชื่อประเภทอุปกรณ์....", "0"));
                        ddlTypeNameUpdate.SelectedValue = lbddlTypeName.Text;
                    }
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int category_idx_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlTypeNameUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlTypeNameUpdate");
                var txtCategoryNameUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtCategoryNameUpdate");
                var ddlCategoryStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlCategoryStatusUpdate");

                GvMaster.EditIndex = -1;

                _data_networkdevices.m0category_list = new m0category_detail[1];
                m0category_detail _m0category_detail = new m0category_detail();

                _m0category_detail.category_idx = category_idx_update;
                _m0category_detail.type_idx = int.Parse(ddlTypeNameUpdate.SelectedValue);
                _m0category_detail.category_name = txtCategoryNameUpdate.Text;
                _m0category_detail.category_status = int.Parse(ddlCategoryStatusUpdate.SelectedValue);           
                _m0category_detail.cemp_idx = emp_idx;

                _data_networkdevices.m0category_list[0] = _m0category_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));
                _data_networkdevices = callServiceNetwork(_urlSetm0Category, _data_networkdevices);

                if (_data_networkdevices.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    actionIndex();


                }
                else
                {
                    setError(_data_networkdevices.return_code.ToString() + " - " + _data_networkdevices.return_msg);
                }


                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch (fvName.ID)
        {



        }
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_networkdevices callServiceNetwork(string _cmdUrl, data_networkdevices _data_networkdevices)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_networkdevices);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_networkdevices = (data_networkdevices)_funcTool.convertJsonToObject(typeof(data_networkdevices), _localJson);

        return _data_networkdevices;
    }


    #endregion reuse

}