﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_chr_m0_typeclose1 : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_chr _dtchr = new data_chr();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlSelect_TypeCloseLV1 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_TypeCloseLV1"];
    static string urlInsert_TypeCloseLV1 = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Master_TypeCloseLV1"];
    static string urlUpdate_TypeCloseLV1 = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Master_TypeCloseLV1"];
    static string urlDelete_TypeCloseLV1 = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Master_TypeCloseLV1"];



    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();

        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
    }
    #endregion

    #region Method SQL 
    protected void SelectMasterList()
    {

        _dtchr.BoxMaster_TypeClose1List = new MasterTypeClose1List[1];
        MasterTypeClose1List dataselect = new MasterTypeClose1List();

        _dtchr.BoxMaster_TypeClose1List[0] = dataselect;

        _dtchr = callServicePostCHR(urlSelect_TypeCloseLV1, _dtchr);
        setGridData(GvMaster, _dtchr.BoxMaster_TypeClose1List);
    }

    protected void Insert_MasterSystem()
    {
        _dtchr.BoxMaster_TypeClose1List = new MasterTypeClose1List[1];
        MasterTypeClose1List datainsert = new MasterTypeClose1List();

        datainsert.TypeClose1_name = txtname.Text;
        datainsert.TypeClose1_Status = int.Parse(ddStatusadd.SelectedValue);
        datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtchr.BoxMaster_TypeClose1List[0] = datainsert;

        _dtchr = callServicePostCHR(urlInsert_TypeCloseLV1, _dtchr);
    }

    protected void Update_MasterSystem()
    {

        _dtchr.BoxMaster_TypeClose1List = new MasterTypeClose1List[1];
        MasterTypeClose1List dataupdate = new MasterTypeClose1List();

        dataupdate.TypeClose1_name = ViewState["TypeClose1_name"].ToString();
        dataupdate.TypeClose1_Status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        dataupdate.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        dataupdate.TC1IDX = int.Parse(ViewState["TC1IDX"].ToString());
        _dtchr.BoxMaster_TypeClose1List[0] = dataupdate;

        _dtchr = callServicePostCHR(urlUpdate_TypeCloseLV1, _dtchr);

    }

    protected void Delete_MasterSystem()
    {

        _dtchr.BoxMaster_TypeClose1List = new MasterTypeClose1List[1];
        MasterTypeClose1List dataupdate = new MasterTypeClose1List();

        dataupdate.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        dataupdate.TC1IDX = int.Parse(ViewState["TC1IDX"].ToString());
        _dtchr.BoxMaster_TypeClose1List[0] = dataupdate;

        _dtchr = callServicePostCHR(urlDelete_TypeCloseLV1, _dtchr);

    }

    #endregion

    #region CallService

    protected data_chr callServicePostCHR(string _cmdUrl, data_chr _dtchr)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtchr);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtchr = (data_chr)_funcTool.convertJsonToObject(typeof(data_chr), _localJson);




        return _dtchr;
    }
    #endregion

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }



                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                btnshow.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                btnshow.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int TC1IDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_edit");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["TC1IDX"] = TC1IDX;
                ViewState["TypeClose1_name"] = txtname_edit.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;

                Update_MasterSystem();
                SelectMasterList();

                break;
        }
    }

    #endregion

    #region SetDefault
    protected void SetDefaultAdd()
    {
        txtname.Text = String.Empty;
        ddStatusadd.SelectedValue = "1";
    }
    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "btnAdd":
                Insert_MasterSystem();
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                SelectMasterList();
                break;
            case "CmdDel":
                int TC1IDX = int.Parse(cmdArg);
                ViewState["TC1IDX"] = TC1IDX;
                Delete_MasterSystem();
                SelectMasterList();

                break;
        }



    }
    #endregion
}