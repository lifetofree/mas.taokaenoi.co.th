﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_nmon_topic : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_nmon _dtnmon = new data_nmon();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlSelect_Master = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_Topic"];
    static string urlSelect_DDLTypeMaster = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_DDLTypeTopic"];
    static string urlInsert_Master = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Master_Topic"];
    static string urlUpdate_Master = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Master_Topic"];
    static string urlDelete_Master = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Master_Topic"];



    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();

        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

    }

    protected void SelectDDLTypeTopic(DropDownList ddlName)
    {
        _dtnmon = new data_nmon();
        _dtnmon.BoxTypeTopicNmonList = new MasterTypeTopicNmonList[1];
        MasterTypeTopicNmonList selectddl = new MasterTypeTopicNmonList();

        _dtnmon.BoxTypeTopicNmonList[0] = selectddl;

        _dtnmon = callServicePostNmon(urlSelect_DDLTypeMaster, _dtnmon);
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));
        setDdlData(ddlName, _dtnmon.BoxTypeTopicNmonList, "TypeMaster", "TMIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทหัวข้อ...", "0"));
    }

    protected void SelectMasterList()
    {

        _dtnmon.BoxTopicNmonList = new MasterTopicNmonList[1];
        MasterTopicNmonList select = new MasterTopicNmonList();

        _dtnmon.BoxTopicNmonList[0] = select;

        _dtnmon = callServicePostNmon(urlSelect_Master, _dtnmon);
        //   text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnmon));

        setGridData(GvMaster, _dtnmon.BoxTopicNmonList);
    }

    protected void Insert_Master_List()
    {
        _dtnmon.BoxTopicNmonList = new MasterTopicNmonList[1];
        MasterTopicNmonList insert = new MasterTopicNmonList();

        insert.TMIDX = int.Parse(ddltypetopic.SelectedValue);
        insert.topic = txtname.Text;
        insert.m0status = int.Parse(ddStatusadd.SelectedValue);
        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtnmon.BoxTopicNmonList[0] = insert;

        _dtnmon = callServicePostNmon(urlInsert_Master, _dtnmon);

    }

    protected void Update_Master_List()
    {
        _dtnmon.BoxTopicNmonList = new MasterTopicNmonList[1];
        MasterTopicNmonList update = new MasterTopicNmonList();

        update.m0idx = int.Parse(ViewState["m0idx"].ToString());
        update.topic = ViewState["txtname_edit"].ToString();
        update.m0status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        update.TMIDX = int.Parse(ViewState["ddlTMIDX_Update"].ToString());
        _dtnmon.BoxTopicNmonList[0] = update;

        _dtnmon = callServicePostNmon(urlUpdate_Master, _dtnmon);

    }

    protected void Delete_Master_List()
    {
        _dtnmon.BoxTopicNmonList = new MasterTopicNmonList[1];
        MasterTopicNmonList delete = new MasterTopicNmonList();

        delete.m0idx = int.Parse(ViewState["m0idx"].ToString());
        delete.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtnmon.BoxTopicNmonList[0] = delete;

        _dtnmon = callServicePostNmon(urlDelete_Master, _dtnmon);
    }

    protected data_nmon callServicePostNmon(string _cmdUrl, data_nmon _dtnmon)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtnmon);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtnmon = (data_nmon)_funcTool.convertJsonToObject(typeof(data_nmon), _localJson);

        return _dtnmon;
    }


    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region SetDDL
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        var ddlTMIDX_Update = (DropDownList)e.Row.FindControl("ddlTMIDX_Update");
                        var lbTMIDX = (Label)e.Row.FindControl("lbTMIDX");

                        SelectDDLTypeTopic(ddlTMIDX_Update);
                        ddlTMIDX_Update.SelectedValue = lbTMIDX.Text;

                    }
                }

                        break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                btnshow.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                btnshow.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int m0idx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlTMIDX_Update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlTMIDX_Update");
                var txtname_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_edit");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["m0idx"] = m0idx;
                ViewState["ddlTMIDX_Update"] = ddlTMIDX_Update.SelectedValue;
                ViewState["txtname_edit"] = txtname_edit.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion

    protected void SetDefaultAdd()
    {
        txtname.Text = String.Empty;
        ddStatusadd.SelectedValue = "1";
        SelectDDLTypeTopic(ddltypetopic);
    }

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "btnAdd":
                Insert_Master_List();
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                SelectMasterList();
                break;
            case "CmdDel":
                int m0idx = int.Parse(cmdArg);
                ViewState["m0idx"] = m0idx;
                Delete_Master_List();
                SelectMasterList();

                break;
        }



    }
    #endregion
}