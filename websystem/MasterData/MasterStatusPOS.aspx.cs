﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_MasterStatusPOS : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();


    private string ODSP_Reletion = "ODSP_Reletion";


    data_employee dtEmployee = new data_employee();

    dataODSP_Relation dtODSP = new dataODSP_Relation();
    DataReservation datareservation = new DataReservation();
    DataSupportIT _dtsupport = new DataSupportIT();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetStatus = _serviceUrl + ConfigurationManager.AppSettings["urlSelectm0StatusPOS"];
    static string _urlGetDowntime = _serviceUrl + ConfigurationManager.AppSettings["urlGetlistDowntimeList"];
    static string urlInsertStatusSAP = _serviceUrl + ConfigurationManager.AppSettings["urlInsertStatusPOS"];
    static string urlUpdateStatusSAP = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateStatusSAP"];
    static string urlDeleteStatusSAP = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteStatusSAP"];



    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();
            SelectDowntimes();

        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_Status()
    {

        _dtsupport.BoxStatusSAP = new StatusSAP[1];
        StatusSAP dtsupport = new StatusSAP();

        dtsupport.status_name = txtstatus.Text;
        dtsupport.status_desc = txtstatus.Text;
        dtsupport.dtidx = int.Parse(ddl_downtime.SelectedValue);
        dtsupport.status_state = int.Parse(ddStatusadd.SelectedValue);
        dtsupport.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport.BoxStatusSAP[0] = dtsupport;

        _dtsupport = callServiceDevices(urlInsertStatusSAP, _dtsupport);

    }

    protected void SelectMasterList()
    {
        _dtsupport.BoxStatusSAP = new StatusSAP[1];
        StatusSAP dtsupport = new StatusSAP();

        _dtsupport.BoxStatusSAP[0] = dtsupport;

        _dtsupport = callServiceDevices(_urlGetStatus, _dtsupport);
        setGridData(GvMaster, _dtsupport.BoxStatusSAP);
    }

    protected DataSupportIT callServiceDevices(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);

        return _dtsupport;
    }

    protected void SelectDowntimes()
    {

        ddl_downtime.Items.Clear();
        ddl_downtime.AppendDataBoundItems = true;
        ddl_downtime.Items.Add(new ListItem("กรุณาเลือกสถานะดาวไทม์.....", "0"));


        _dtsupport.BoxStatusSAP = new StatusSAP[1];
        StatusSAP dtsupport = new StatusSAP();

        _dtsupport.BoxStatusSAP[0] = dtsupport;

        _dtsupport = callServiceDevices(_urlGetDowntime, _dtsupport);


        ddl_downtime.DataSource = _dtsupport.BoxStatusSAP;
        ddl_downtime.DataTextField = "downtime_name";
        ddl_downtime.DataValueField = "dtidx";
        ddl_downtime.DataBind();

    }

    protected void Update_Master_List()
    {
        _dtsupport.BoxStatusSAP = new StatusSAP[1];
        StatusSAP dtsupport = new StatusSAP();


        dtsupport.stidx = int.Parse(ViewState["stidx_Update"].ToString());
        dtsupport.status_name = ViewState["txtstatus_edit_Update"].ToString();
        dtsupport.status_desc = ViewState["txtstatus_edit_Update"].ToString();
        dtsupport.dtidx = int.Parse(ViewState["ddl_downtime_edit_Update"].ToString());
        dtsupport.status_state = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        dtsupport.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());


        _dtsupport.BoxStatusSAP[0] = dtsupport;

        _dtsupport = callServiceDevices(urlUpdateStatusSAP, _dtsupport);

    }

    protected void Delete_Master_List()
    {

        _dtsupport.BoxStatusSAP = new StatusSAP[1];
        StatusSAP dtsupport = new StatusSAP();


        dtsupport.stidx = int.Parse(ViewState["stidx_Update"].ToString());
        dtsupport.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());


        _dtsupport.BoxStatusSAP[0] = dtsupport;

        _dtsupport = callServiceDevices(urlDeleteStatusSAP, _dtsupport);
    }




    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        var txtstatus_edit = (TextBox)e.Row.FindControl("txtstatus_edit");
                        var ddl_downtime_edit = (DropDownList)e.Row.FindControl("ddl_downtime_edit");
                        var lbDTIDX = (Label)e.Row.FindControl("lbDTIDX");

                        ddl_downtime_edit.AppendDataBoundItems = true;

                        _dtsupport.BoxStatusSAP = new StatusSAP[1];
                        StatusSAP dtsupport = new StatusSAP();

                        _dtsupport.BoxStatusSAP[0] = dtsupport;

                        _dtsupport = callServiceDevices(_urlGetDowntime, _dtsupport);


                        ddl_downtime_edit.DataSource = _dtsupport.BoxStatusSAP;
                        ddl_downtime_edit.DataTextField = "downtime_name";
                        ddl_downtime_edit.DataValueField = "dtidx";
                        ddl_downtime_edit.DataBind();
                        ddl_downtime_edit.SelectedValue = lbDTIDX.Text;

                        //  DataSupportIT dtsupport = new DataSupportIT();
                        //  StatusSAP select = new StatusSAP();
                        // // dtsupport.BoxStatusSAP = new StatusSAP[1];

                        ////  select.
                        ////  dtsupport.BoxStatusSAP[0] = select;

                        //  _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", dtsupport, 202);
                        //  dtsupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

                        //  //test.Text = HttpUtility.HtmlEncode(_functionWeb.ConvertObjectToXml(dd2));
                        //  ddl_downtime_edit.DataSource = dtsupport.BoxStatusSAP;
                        //  ddl_downtime_edit.DataTextField = "downtime_name";
                        //  ddl_downtime_edit.DataValueField = "dtidx";
                        //  ddl_downtime_edit.DataBind();
                        //  ddl_downtime_edit.SelectedValue = lbDTIDX.Text;

                        //txtstatus_edit.Text = "dddd";//ViewState["txtstatus_edit_Update"].ToString();


                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int stidx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtstatus_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtstatus_edit");
                var ddl_downtime_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddl_downtime_edit");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["stidx_Update"] = stidx;
                ViewState["txtstatus_edit_Update"] = txtstatus_edit.Text;
                ViewState["ddl_downtime_edit_Update"] = ddl_downtime_edit.SelectedValue;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAddHolder":

                btnaddholder.Visible = false;
                Panel_Add.Visible = true;
                break;

            case "btnCancel":
                btnaddholder.Visible = true;
                Panel_Add.Visible = false;
                txtstatus.Text = String.Empty;
                ddl_downtime.SelectedValue = "0";

                break;

            case "btnAdd":
                Insert_Status();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "CmdDel":

                int stidx = int.Parse(cmdArg);
                ViewState["stidx_Update"] = stidx;
                Delete_Master_List();
                SelectMasterList();


                break;
        }



    }
    #endregion
}