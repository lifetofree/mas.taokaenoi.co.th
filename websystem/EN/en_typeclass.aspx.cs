﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_en_en_typeclass : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    DataMachine _datamachine = new DataMachine();
    service_execute _serviceExecute = new service_execute();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    static string _urlGetInsertCostcenter = _serviceUrl + ConfigurationManager.AppSettings["urlGetInsertCostcenter"];
    static string _urlGetUpdateCostcenter = _serviceUrl + ConfigurationManager.AppSettings["urlGetUpdateCostcenter"];
    static string _urlGetDeleteCostcenter = _serviceUrl + ConfigurationManager.AppSettings["urlGetDeleteCostcenter"];


    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            //SelectMasterList();
        }

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_Status()
    {
        _datamachine.TypeClassDetail = new TypeClass[1];
        TypeClass dtmachine = new TypeClass();

        //_datamachine.CEmpIDX = txtnametypecode.Text;
        dtmachine.TC_Name = txtnametypecode.Text;
        dtmachine.TCLStatus = int.Parse(ddl_status.SelectedValue);


        _datamachine.TypeClassDetail[0] = dtmachine;
        //text.Text = dtemployee.ToString();
        _datamachine = callService(_urlGetInsertCostcenter, _datamachine);
    }

    protected void SelectMasterList()
    {
        _datamachine.TypeClassDetail = new TypeClass[1];
        TypeClass dtmachine = new TypeClass();

        _datamachine.TypeClassDetail[0] = dtmachine;

        _datamachine = callService(_urlGetCostcenterOld, _datamachine);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvMaster, _datamachine.TypeClassDetail);
    }

    protected DataMachine callService(string _cmdUrl, DataMachine _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (DataMachine)_funcTool.convertJsonToObject(typeof(DataMachine), _localJson);
        
        return _dtmaster;
    }

    protected void Update_Master_List()
    {
        _datamachine.TypeClassDetail = new TypeClass[1];
        TypeClass dtmachine = new TypeClass();

        dtmachine.TCLIDX = int.Parse(ViewState["CostIDX_update"].ToString());
        dtmachine.TC_Name = ViewState["txtcusname_update"].ToString();
        //dtmachine.CostNo = ViewState["txtcusno_update"].ToString();
        dtmachine.TCLStatus = int.Parse(ViewState["ddStatus_update"].ToString());

        _datamachine.TypeClassDetail[0] = dtmachine;
        //text.Text = dtemployee.ToString();
        _datamachine = callService(_urlGetUpdateCostcenter, _datamachine);
    }

    protected void Delete_Master_List()
    {
        _datamachine.TypeClassDetail = new TypeClass[1];
        TypeClass dtmachine = new TypeClass();

        dtmachine.TCLIDX = int.Parse(ViewState["CostIDX_delete"].ToString());

        _datamachine.TypeClassDetail[0] = dtmachine;
        //text.Text = dtemployee.ToString();
        _datamachine = callService(_urlGetDeleteCostcenter, _datamachine);
    }



    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        /*var txtstatus_edit = (TextBox)e.Row.FindControl("txtstatus_edit");
                        var ddl_downtime_edit = (DropDownList)e.Row.FindControl("ddl_downtime_edit");
                        var lbDTIDX = (Label)e.Row.FindControl("lbDTIDX");

                        ddl_downtime_edit.AppendDataBoundItems = true;

                        _dtsupport.BoxStatusSAP = new StatusSAP[1];
                        StatusSAP dtsupport = new StatusSAP();

                        _dtsupport.BoxStatusSAP[0] = dtsupport;

                        _dtsupport = callServiceDevices(_urlGetDowntime, _dtsupport);


                        ddl_downtime_edit.DataSource = _dtsupport.BoxStatusSAP;
                        ddl_downtime_edit.DataTextField = "downtime_name";
                        ddl_downtime_edit.DataValueField = "dtidx";
                        ddl_downtime_edit.DataBind();
                        ddl_downtime_edit.SelectedValue = lbDTIDX.Text;*/


                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int CostIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtcusno_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcusno_update");
                var txtcusname_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcusname_update");
                var ddStatus_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatus_update");

                GvMaster.EditIndex = -1;

                ViewState["CostIDX_update"] = CostIDX;
                ViewState["txtcusno_update"] = txtcusno_update.Text;
                ViewState["txtcusname_update"] = txtcusname_update.Text;
                ViewState["ddStatus_update"] = ddStatus_update.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion


    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAddHolder":

                //btnaddholder.Visible = false;
                //Panel_Add.Visible = true;

                string temp = "1234.5678";
                string temp1 = "";

                double d = Convert.ToDouble(temp)-1;
                string s = d.ToString("#,###");

                string[] Loop = temp.Split('.');
                foreach (string LoopInsert in Loop)
                {
                    if (LoopInsert != String.Empty)
                    {
                        temp1 = LoopInsert;
                    }
                }

                lblt1.Text = s + " . "+ temp1.Substring(0,2);

                break;

            case "btnCancel":
                btnaddholder.Visible = true;
                Panel_Add.Visible = false;

                break;

            case "btnAdd":
                Insert_Status();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "CmdDel":

                int CostIDX = int.Parse(cmdArg);
                ViewState["CostIDX_delete"] = CostIDX;
                Delete_Master_List();
                SelectMasterList();

                break;
        }



    }
    #endregion

}
