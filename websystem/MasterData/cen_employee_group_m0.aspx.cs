﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.Data;
using System.Data.SqlClient;
using ASP;

public partial class websystem_MasterData_cen_employee_group_m0 : System.Web.UI.Page
{
    function_tool _functionTool = new function_tool();
    data_cen_master _data_cen_master = new data_cen_master();
    data_cen_master _data_cen_master2 = new data_cen_master();
    data_employee _dataEmployee = new data_employee();
    string _localJson = string.Empty;
    string textstatus = string.Empty;
    int _emp_idx = 0;
    //string textstatus = string.Empty;
    //string msg = string.Empty;
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCenMasterList"];
    static string _urlSetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlSetCenMasterList"];

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            ViewState["forSearch"] = null;
            _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);
            showEmpgroup();
            FormViewS.Visible = true;


        }
    }



    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        lbCreate.Visible = false;
        gvEmpgroup.Visible = false;
        FvInsertEdit.Visible = false;
        FormViewS.Visible = false;
        hlSetTotop.Focus();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        switch (cmdName)
        {
            case "cmdCreate":
                _functionTool.setFvData(FvInsertEdit, FormViewMode.Insert, null);
                FvInsertEdit.Visible = true;
               




                break;
            case "cmdEdit":
                FvInsertEdit.Visible = true;

                _search_cen_master_detail.s_empgroup_idx = cmdArg;
                _data_cen_master.master_mode = "2";
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;

                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setFvData(FvInsertEdit, FormViewMode.Edit, _data_cen_master.cen_empgroup_list_m0);
                

                break;
            case "cmdCancel":
                
                //FormViewS.Visible = true;
                lbCreate.Visible = true;
                gvEmpgroup.Visible = true;
                FormViewS.Visible = true;
                if (ViewState["forSearch"] == null)
                {
                    showEmpgroup();
                }
                else
                {
                    _functionTool.setGvData(gvEmpgroup, ((data_cen_master)ViewState["forSearch"]).cen_empgroup_list_m0);

                }

                break;

            case "cmdReset":
              
                FormViewS.Visible = true;
                lbCreate.Visible = true;
                gvEmpgroup.Visible = true;
                _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);
                ViewState["forSearch"] = null;
                showEmpgroup();
                break;

            case "cmdDelete":
                cmdDelete(int.Parse(cmdArg));


                break;

            case "editSave":
                InsertEmpgroup(_functionTool.convertToInt(cmdArg));


                break;
            case "cmdSave":
                InsertEmpgroup(_functionTool.convertToInt(cmdArg));



                break;

            case "cmdSearch":
                checkBoxSearch();
                lbCreate.Visible = true;
                gvEmpgroup.Visible = true;
                FormViewS.Visible = true;
                break;
        }
    }

    protected void InsertEmpgroup(int id)
    {
        TextBox tex_TH_name = (TextBox)FvInsertEdit.FindControl("tb_nameth");
        TextBox tex_EN_name = (TextBox)FvInsertEdit.FindControl("tb_nameen");
        DropDownList dropD_status = (DropDownList)FvInsertEdit.FindControl("ddStatus");
       
        data_cen_master _data_cen_master = new data_cen_master();
        cen_empgroup_detail_m0 _cen_empgroup_detail_m0 = new cen_empgroup_detail_m0();

        _data_cen_master.master_mode = "2";
        _cen_empgroup_detail_m0.empgroup_idx = id;
        _cen_empgroup_detail_m0.empgroup_name_th = tex_TH_name.Text.Trim();
        _cen_empgroup_detail_m0.empgroup_name_en = tex_EN_name.Text.Trim();
        _cen_empgroup_detail_m0.empgroup_status = _functionTool.convertToInt(dropD_status.SelectedValue);
        _cen_empgroup_detail_m0.cemp_idx = _emp_idx;

        _data_cen_master.cen_empgroup_list_m0 = new cen_empgroup_detail_m0[1];
        _data_cen_master.cen_empgroup_list_m0[0] = _cen_empgroup_detail_m0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlSetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_cen_master));

        if (_data_cen_master.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
            FvInsertEdit.Visible = true;
            lbCreate.Visible = false;
        }
        else
        {
            lbCreate.Visible = true;
            FvInsertEdit.Visible = false;
            showEmpgroup();
            gvEmpgroup.Visible = true;
            FormViewS.Visible = true;


        }



    }

    protected void showEmpgroup()
    {
        data_cen_master _data_cen_master = new data_cen_master();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();


        if (ViewState["forSearch"] == null)
        {
            _search_cen_master_detail.s_empgroup_idx = "";
            _data_cen_master.master_mode = "2";

            _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
            _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;

            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
            _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));


            _functionTool.setGvData(gvEmpgroup, _data_cen_master.cen_empgroup_list_m0);
        
        }
        else
        {

            _functionTool.setGvData(gvEmpgroup, ((data_cen_master)ViewState["forSearch"]).cen_empgroup_list_m0);
            //ViewState["forSearch"] = null;
        }


    }

    protected void checkBoxSearch()
    {
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        DropDownList ddl_status_for_s = (DropDownList)FormViewS.FindControl("DropDownstatus_s");
        TextBox searchbox = (TextBox)FormViewS.FindControl("tb_emp_s");

        //_search_cen_master_detail.s_org_name = searchbox.Text.Trim();
        _search_cen_master_detail.s_empgroup_name = searchbox.Text.Trim();
        _search_cen_master_detail.s_status = ddl_status_for_s.SelectedValue;
        _data_cen_master.master_mode = "2";

        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);

        ViewState["forSearch"] = _data_cen_master;
        //ViewState["forSearch2"] = _data_cen_master;

        showEmpgroup();


    }

    protected void cmdDelete(int cmdArg)
    {
        data_cen_master _data_cen_master = new data_cen_master();
        cen_empgroup_detail_m0 _cen_empgroup_detail_m0 = new cen_empgroup_detail_m0();
        _cen_empgroup_detail_m0.empgroup_idx = cmdArg;
        _cen_empgroup_detail_m0.empgroup_status = 9;
        _cen_empgroup_detail_m0.cemp_idx = _emp_idx;
        _data_cen_master.master_mode = "2";


        _data_cen_master.cen_empgroup_list_m0 = new cen_empgroup_detail_m0[1];
        _data_cen_master.cen_empgroup_list_m0[0] = _cen_empgroup_detail_m0;

        //--debug
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        _data_cen_master = callServiceMaster(_urlSetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        //_functionTool.setGvData(gvEmpgroup, _data_cen_master.cen_empgroup_list_m0);
        showEmpgroup();
        lbCreate.Visible = true;
        gvEmpgroup.Visible = true;
        FormViewS.Visible = true;


    }


    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        data_cen_master _data_cen_master = new data_cen_master();
        switch (GvName.ID)
        {
            case "gvEmpgroup":
                gvEmpgroup.PageIndex = e.NewPageIndex;
                showEmpgroup();
                break;

        }
        hlSetTotop.Focus();
    }

    protected data_cen_master callServiceMaster(string _cmdUrl, data_cen_master _data_cen_master)
    {
        _localJson = _functionTool.convertObjectToJson(_data_cen_master);

        _localJson = _functionTool.callServicePost(_cmdUrl, _localJson);

        _data_cen_master = (data_cen_master)_functionTool.convertJsonToObject(typeof(data_cen_master), _localJson);


        return _data_cen_master;
    }

    protected string convertStatus(int status)
    {
        if (status == 1)
            textstatus = "online";
        else
            textstatus = "offline";
        return textstatus;
    }
    protected string convertCss(int status)
    {
        if (status == 1)
            textstatus = "text-success";
        else
            textstatus = "text-danger";
        return textstatus;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _functionTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_functionTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
}