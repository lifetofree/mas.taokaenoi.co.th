﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_roombooking
/// </summary>
/// 
[Serializable]
[XmlRoot("data_qa_report")]
public class data_qa_report
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    // --- master ---//
    [XmlElement("qa_report_m0_testdetail_list")]
    public qa_report_m0_testdetail_detail[] qa_report_m0_testdetail_list { get; set; }

}

[Serializable]
public class qa_report_m0_testdetail_detail
{

    [XmlElement("test_detail_idx")]
    public int test_detail_idx { get; set; }

    [XmlElement("test_detail_name")]
    public string test_detail_name { get; set; }

    [XmlElement("test_detail_status")]
    public int test_detail_status { get; set; }

}