﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_manpower.aspx.cs" Inherits="websystem_hr_hr_manpower" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script type="text/javascript">
        function openModal() {
            $('#editman').modal('show');

        }
    </script>
    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden; background-color: darksalmon;" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left" runat="server">
                    <li id="_divMenuLiToViewIndex" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivIndex" runat="server"
                            CommandName="_divMenuBtnToDivIndex"
                            OnCommand="btnCommand" Text="รายการทั่วไป" />
                    </li>


                    <li id="_divMenuLiToViewAdd" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAdd" runat="server"
                            CommandName="_divMenuBtnToDivAdd"
                            OnCommand="btnCommand" Text="ขอกำลังคน" />
                    </li>

                    <li id="_divMenuLiToViewCancel" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivCancel" runat="server"
                            CommandName="_divMenuBtnToDivCancel"
                            OnCommand="btnCommand" Text="ยกเลิกรายการ" />
                    </li>

                    <li id="_divMenuLiToViewApprove" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivApprove" runat="server"
                            CommandName="_divMenuBtnToDivApprove"
                            OnCommand="btnCommand" Text="รายการรออนุมัติ">
                            <asp:Label ID="nav_approve" Font-Bold="true" runat="server"></asp:Label>
                        </asp:LinkButton>

                    </li>

                    <li id="_divMenuLiToViewReport" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivReport" runat="server"
                            CommandName="_divMenuBtnToDivReport"
                            OnCommand="btnCommand" Text="รายงาน">
                            <asp:Label ID="Label43" Font-Bold="true" runat="server"></asp:Label>
                        </asp:LinkButton>

                    </li>

                </ul>

                <ul class="nav navbar-nav navbar-right" runat="server">

                    <li id="_divMenuLiToViewManual" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivManual" runat="server"
                            CommandName="_divMenuBtnToDivManual"
                            OnCommand="btnCommand" Text="คู่มือการใช้งาน" />
                    </li>
                    <li id="_divMenuLiToViewWaiting" runat="server" class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Flow Chart
                                                             <asp:Label ID="Label88" Font-Bold="true" runat="server"></asp:Label>

                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li id="_divMenuBtnToViewFlowNowmal" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivWaitingNormal" runat="server" CssClass="col-lg-9"
                                    CommandName="_divMenuBtnToDivWaitingNormal"
                                    OnCommand="btnCommand" Text="Flow Chart กรณีขอปกติ" />
                                <asp:Label ID="nav_approve1" Font-Bold="true" runat="server" CssClass="col-lg-3"></asp:Label>
                            </li>

                            <li id="_divMenuBtnToViewFlowSpecial" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivWaitingSpecial" runat="server"
                                    CommandName="_divMenuBtnToDivWaitingSpecial"
                                    OnCommand="btnCommand" Text="Flow Chart กรณีขอพิเศษ" />
                            </li>
                            <li id="_divMenuBtnToViewFlowCancel" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivWaitingCancel" runat="server"
                                    CommandName="_divMenuBtnToDivWaitingCancel"
                                    OnCommand="btnCommand" Text="Flow Chart กรณียกเลิก" />
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </div>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <asp:UpdatePanel ID="update_viewindex" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12">
                        <asp:Image ID="imghead" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/hr_manpower/Header_Manpower-01.png" Style="height: 100%; width: 100%;" />
                    </div>

                    <div class="col-lg-12">

                        <div class="alert-message alert-message-success">
                            <blockquote class="danger" style="font-size: small; background-color: goldenrod; text-align: left">
                                <h4><b>รายการขอกำลังคน</b></h4>
                            </blockquote>

                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <asp:GridView ID="GvListIndex" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info small"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound"
                                    DataKeyNames="u0_docidx">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="รายการที่" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                                <asp:Label ID="lbdoc_code" runat="server" Text='<%# Eval("doc_code") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันที่ทำรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbcreatedate" runat="server" Text='<%# Eval("createdate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลผู้ทำรายการ" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label15" runat="server">องค์กร: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label24" runat="server">ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label25" runat="server">แผนก: </asp:Label></strong>
                                                    <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                    </p>
                                                           <strong>
                                                               <asp:Label ID="Label19" runat="server">ตำแหน่ง: </asp:Label></strong>
                                                    <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("PosNameTH") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                                    </p>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ข้อมูลรายการ" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Labelc15" runat="server">ระบบ: </asp:Label></strong>
                                                    <asp:Literal ID="Lwiteral3" runat="server" Text='<%# Eval("type_man") %>' />
                                                    <asp:Literal ID="litm0_typeidx" Visible="false" runat="server" Text='<%# Eval("m0_typeidx") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labelw24" runat="server">ประเภทการขอคน: </asp:Label></strong>
                                                    <asp:Literal ID="Litderal1" runat="server" Text='<%# Eval("type_list") %>' />

                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labedl25" runat="server">สถานที่: </asp:Label></strong>
                                                    <asp:Literal ID="Litefral2" runat="server" Text='<%# Eval("LocName") %>' />
                                                    </p>
                                                         <strong>
                                                             <asp:Label ID="Label42" runat="server">ประเภทพนักงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("EmpTypeName") %>' />
                                                    </p>

                                                           <strong>
                                                               <asp:Label ID="Lvabel19" runat="server">สาเหตุการขอคน: </asp:Label></strong>
                                                    <asp:Literal ID="Litweral5" runat="server" Text='<%# Eval("reason_comment") %>' />
                                                    </p>


                                                    <div id="div_replacecode" runat="server" visible="false">
                                                        <strong>
                                                            <asp:Label ID="Label69" runat="server">ยกเลิกรายการ: </asp:Label></strong>
                                                        <asp:Literal ID="litdoc_code_ref" runat="server" Text='<%# Eval("doc_code_ref") %>' />
                                                        </p>
                                                    </div>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltunidx" Visible="false" runat="server" Text='<%# Eval("unidx") %>' /></small>
                                                       <asp:Literal ID="ltacidx" Visible="false" runat="server" Text='<%# Eval("acidx") %>' /></small>
                                                    <asp:Literal ID="ltstaidx" Visible="false" runat="server" Text='<%# Eval("staidx") %>' /></small>
                                                     <asp:Label ID="lblstasus" Font-Bold="true" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                                <asp:Label ID="Label66" Visible="false" runat="server" Text='<%# Eval("doc_refidx") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnviewdetail" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" ToolTip="View" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("CEmpIDX") + ";" +"1"+  ";" +  Eval("doc_code")+  ";" +  Eval("m0_typeidx") %>'><i class="	fa fa-book"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btnviewdetail_cancel" Visible="false" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" ToolTip="View" CommandName="CmdDetail" CommandArgument='<%#  Eval("doc_refidx") + ";" + Eval("CEmpIDX") + ";" +"1"+  ";" +  Eval("doc_code")+  ";" +  Eval("m0_typeidx") %>'><i class="	fa fa-book"></i></asp:LinkButton>

                                            </ItemTemplate>

                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewInsert" runat="server">
            <div class="col-md-12" id="div1" runat="server" style="color: transparent;">
                <asp:FileUpload ID="FileUpload1" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />
            </div>
            <asp:UpdatePanel ID="updatepic" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="col-lg-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                            </div>
                            <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>


                                            <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>


                                            <%-------------- แผนก,ตำแหน่ง --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>

                                            <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                                <div class="col-sm-3">
                                                    <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                                    <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                                <div class="col-sm-3">
                                                    <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                                    <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-heart-empty"></i><strong>&nbsp; รายละเอียดข้อมูล</strong></h3>
                            </div>
                            <asp:FormView ID="fvinsert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="font-size: small; background-color: burlywood; text-align: left">
                                                    <h4><b><i class="glyphicon glyphicon-user"></i>&nbsp;ข้อมูลขอกำลังคน</b></h4>
                                                </blockquote>

                                            </div>
                                            <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlorg" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" CssClass="form-control">
                                                        <asp:ListItem Value="0">เลือกองค์กร....</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save_DataSet" runat="server" Display="None"
                                                        ControlToValidate="ddlorg" Font-Size="11"
                                                        ErrorMessage="เลือกองค์กร"
                                                        ValidationExpression="เลือกองค์กร" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlrdept" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" CssClass="form-control">
                                                        <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save_DataSet" runat="server" Display="None"
                                                        ControlToValidate="ddlrdept" Font-Size="11"
                                                        ErrorMessage="เลือกฝ่าย"
                                                        ValidationExpression="เลือกฝ่าย" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />


                                                </div>

                                            </div>

                                            <div class="form-group">

                                                <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlrsec" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" CssClass="form-control">
                                                        <asp:ListItem Value="0">เลือกแผนก....</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save_DataSet" runat="server" Display="None"
                                                        ControlToValidate="ddlrsec" Font-Size="11"
                                                        ErrorMessage="เลือกแผนก"
                                                        ValidationExpression="เลือกแผนก" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />


                                                </div>

                                                <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlrpos" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">เลือกตำแหน่ง....</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save_DataSet" runat="server" Display="None"
                                                        ControlToValidate="ddlrpos" Font-Size="11"
                                                        ErrorMessage="เลือกตำแหน่ง"
                                                        ValidationExpression="เลือกตำแหน่ง" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />


                                                </div>

                                            </div>

                                            <div class="form-group">

                                                <asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server" Text="วันที่ต้องการ : " />
                                                <div class="col-sm-3">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="datestart" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save_DataSet" runat="server" Display="None"
                                                            ControlToValidate="datestart" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                            ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                                    </div>
                                                </div>

                                                <asp:Label ID="Label10" CssClass="col-sm-3 control-label" runat="server" Text="จำนวน : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtqty" CssClass="form-control" runat="server" placeholder="0"> </asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="Save_DataSet" runat="server" Display="None"
                                                        ControlToValidate="txtqty" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกจำนวน" />
                                                    <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="Save_DataSet" Display="None"
                                                        ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                        ControlToValidate="txtqty"
                                                        ValidationExpression="^[0-9]{1,10}$" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                                </div>

                                            </div>

                                            <div class="form-group">

                                                <asp:Label ID="Label17" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทพนักงาน : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlemptype" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">เลือกประเภทพนักงาน....</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="Save_DataSet" runat="server" Display="None"
                                                        ControlToValidate="ddlemptype" Font-Size="11"
                                                        ErrorMessage="เลือกประเภทพนักงาน"
                                                        ValidationExpression="เลือกประเภทพนักงาน" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />

                                                </div>

                                                <div id="div_tpeman" runat="server" visible="false">
                                                    <asp:Label ID="Label27" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทขอคน : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddltypeman" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="1">ขอปกติ</asp:ListItem>
                                                            <asp:ListItem Value="2">ขอพิเศษ</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="Save_DataSet" runat="server" Display="None"
                                                            ControlToValidate="ddltypeman" Font-Size="11"
                                                            ErrorMessage="เลือกประเภทขอคน"
                                                            ValidationExpression="เลือกประเภทขอคน" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />

                                                    </div>
                                                </div>

                                            </div>

                                            <div class="form-group">

                                                <asp:Label ID="Label28" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ปฏิบัติงาน : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddllocate" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">เลือกสถานที่ปฏิบัติงาน....</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="Save_DataSet" runat="server" Display="None"
                                                        ControlToValidate="ddllocate" Font-Size="11"
                                                        ErrorMessage="เลือกสถานที่ปฏิบัติงาน"
                                                        ValidationExpression="เลือกสถานที่ปฏิบัติงาน" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />

                                                </div>
                                            </div>

                                            <div class="form-group">

                                                <asp:Label ID="Label11" CssClass="col-sm-2 control-label" runat="server" Text="ลักษณะการว่าจ้าง : " />
                                                <div class="col-sm-3">
                                                    <asp:RadioButtonList ID="rdotypehire" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                        ValidationGroup="Save_DataSet" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกลักษณะการว่าจ้าง"
                                                        Display="None" SetFocusOnError="true" ControlToValidate="rdotypehire" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160"
                                                        PopupPosition="BottomLeft" />
                                                </div>

                                                <asp:Label ID="Label12" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทการว่าจ้าง : " />
                                                <div class="col-sm-3">
                                                    <asp:RadioButtonList ID="rdohire" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                        ValidationGroup="Save_DataSet" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกประเภทการว่าจ้าง"
                                                        Display="None" SetFocusOnError="true" ControlToValidate="rdohire" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenxder7" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                                        PopupPosition="BottomLeft" />
                                                </div>

                                            </div>





                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="font-size: small; background-color: chocolate; text-align: left">
                                                    <h4><b><i class="glyphicon glyphicon-folder-open"></i>&nbsp;คุณสมบัติที่ต้องการ</b></h4>
                                                </blockquote>

                                            </div>


                                            <div class="form-group">

                                                <asp:Label ID="Label13" CssClass="col-sm-2 control-label" runat="server" Text="วุฒิการศึกษา : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddleducation" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" CssClass="form-control">
                                                        <asp:ListItem Value="0">เลือกวุฒิการศึกษา....</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_DataSet" runat="server" Display="None"
                                                        ControlToValidate="ddleducation" Font-Size="11"
                                                        ErrorMessage="เลือกวุฒิการศึกษา"
                                                        ValidationExpression="เลือกวุฒิการศึกษา" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label14" CssClass="col-sm-2 control-label" runat="server" Text="ประสบการณ์ทำงาน : " />
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="txtexp" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidsator8" ValidationGroup="Save_DataSet" runat="server" Display="None" ControlToValidate="txtexp" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกความคิดเห็น"
                                                        ValidationExpression="กรุณากรอกความคิดเห็น"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallodutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidsator8" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                        ValidationGroup="Save_DataSet" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtexp"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutEqxtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label15" CssClass="col-sm-2 control-label" runat="server" Text="ความรู้ทางภาษา : " />
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="txtlanguage" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save_DataSet" runat="server" Display="None" ControlToValidate="txtlanguage" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกความรู้ทางภาษา"
                                                        ValidationExpression="กรุณากรอกความรู้ทางภาษา"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidsator8" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                        ValidationGroup="Save_DataSet" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtlanguage"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label16" CssClass="col-sm-2 control-label" runat="server" Text="คุณสมบัติอื่นๆ : " />
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="txtother" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save_DataSet" runat="server" Display="None" ControlToValidate="txtother" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกคุณสมบัติอื่น"
                                                        ValidationExpression="กรุณากรอกคุณสมบัติอื่น"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidsator8" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                        ValidationGroup="Save_DataSet" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtother"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                </div>

                                            </div>

                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="font-size: small; background-color: darkgoldenrod; text-align: left">
                                                    <h4><b><i class="glyphicon glyphicon-fire"></i>&nbsp;ระบุเพศ</b></h4>
                                                </blockquote>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-lg-2">
                                                </div>

                                                <div class="col-lg-9">
                                                    <div class="panel-body">
                                                        <div class="form-horizontal" role="form">

                                                            <asp:GridView ID="Gvsex" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                HeaderStyle-CssClass="success small"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="false"
                                                                OnRowDataBound="Master_RowDataBound"
                                                                DataKeyNames="sexidx">

                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chksex" OnCheckedChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="เพศ" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbsexname" runat="server" Text='<%# Eval("sex_name") %>'></asp:Label>
                                                                            <asp:Label ID="lblsexidx" runat="server" Visible="false" Text='<%# Eval("sexidx") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="จำนวน" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtqty_sex" Enabled="false" CssClass="form-control" runat="server" placeholder="0"> </asp:TextBox>

                                                                            <asp:RegularExpressionValidator ID="Retaxtprice22" runat="server" ValidationGroup="Save_DataSet" Display="None"
                                                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                                ControlToValidate="txtqty_sex"
                                                                                ValidationExpression="^[0-9]{1,10}$" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retaxtprice22" Width="160" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="สัญชาติ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBoxList ID="chknation" Enabled="false" runat="server"></asp:CheckBoxList>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>


                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-lg-1">
                                                </div>
                                            </div>


                                            <div class="form-group" runat="server" id="div_btndataset">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <asp:LinkButton ID="btnAdddataset" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdInsert_Dataset" OnCommand="btnCommand" ValidationGroup="Save_DataSet" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save">ADD +</asp:LinkButton>
                                                </div>
                                            </div>


                                            <div class="form-group">

                                                <div class="col-lg-12">
                                                    <asp:GridView ID="GvManPowerAdd"
                                                        runat="server"
                                                        CssClass="table table-striped table-responsive info"
                                                        HeaderStyle-CssClass="info small"
                                                        GridLines="None"
                                                        OnRowDataBound="Master_RowDataBound"
                                                        OnRowCommand="onRowCommand"
                                                        AutoGenerateColumns="false">


                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>

                                                        <Columns>

                                                            <asp:TemplateField HeaderText="ข้อมูล" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" Visible="true" ItemStyle-Font-Size="Small">

                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblorgidx" Visible="false" runat="server" Text='<%# Eval("OrgIDX") %>' />
                                                                    <asp:Label ID="lblrdeptidx" Visible="false" runat="server" Text='<%# Eval("RdeptIDX") %>' />
                                                                    <asp:Label ID="lblrsecidx" Visible="false" runat="server" Text='<%# Eval("SecIDX") %>' />
                                                                    <asp:Label ID="lblrposidx" Visible="false" runat="server" Text='<%# Eval("PosIDX") %>' />
                                                                    <asp:Label ID="lblEmpTypeIDX" Visible="false" runat="server" Text='<%# Eval("EmpTypeIDX") %>' />
                                                                    <asp:Label ID="lbCheckRow" Visible="false" runat="server" Text='<%# Eval("CheckRow") %>' />


                                                                    <strong>
                                                                        <asp:Label ID="Label60" runat="server"> องค์กร: </asp:Label></strong>
                                                                    <asp:Literal ID="lbOrgName" runat="server" Text='<%# Eval("OrgName") %>' />
                                                                    <br />
                                                                    <strong>
                                                                        <asp:Label ID="Label61" runat="server"> ฝ่าย: </asp:Label></strong>
                                                                    <asp:Label ID="lbDeptName" runat="server" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                                    <br />
                                                                    <strong>
                                                                        <asp:Label ID="lbdat" runat="server"> แผนก: </asp:Label></strong>
                                                                    <asp:Label ID="lbSecName" runat="server" Text='<%# Eval("SecName") %>'></asp:Label>
                                                                    <br />
                                                                    <strong>
                                                                        <asp:Label ID="Label31" runat="server"> ตำแหน่ง: </asp:Label></strong>
                                                                    <asp:Label ID="lbPosName" runat="server" Text='<%# Eval("PosName") %>'></asp:Label>

                                                                    <br />
                                                                    <strong>
                                                                        <asp:Label ID="Label18" runat="server"> ประเภทพนักงาน: </asp:Label></strong>
                                                                    <asp:Label ID="lbEmpTypeName" runat="server" Text='<%# Eval("EmpTypeName") %>'></asp:Label>

                                                                    <br />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" Visible="true" ItemStyle-Font-Size="Small">

                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblm0_thidx" Visible="false" runat="server" Text='<%# Eval("m0_thidx") %>' />
                                                                    <asp:Label ID="lblm0_hridx" Visible="false" runat="server" Text='<%# Eval("m0_hridx") %>' />

                                                                    <strong>
                                                                        <asp:Label ID="Label601" runat="server"> วันที่ต้องการ: </asp:Label></strong>
                                                                    <asp:Literal ID="lbdatestart" runat="server" Text='<%# Eval("datestart") %>' />
                                                                    <br />
                                                                    <strong>
                                                                        <asp:Label ID="Label60t1" runat="server"> จำนวนคน: </asp:Label></strong>
                                                                    <asp:Label ID="lbqty" runat="server" Text='<%# Eval("qty") %>'></asp:Label>
                                                                    <br />
                                                                    <strong>
                                                                        <asp:Label ID="lbda2t" runat="server"> ลักษณะการว่าจ้าง: </asp:Label></strong>
                                                                    <asp:Label ID="lbtypehire_name" runat="server" Text='<%# Eval("typehire_name") %>'></asp:Label>
                                                                    <br />
                                                                    <strong>
                                                                        <asp:Label ID="La3bel31" runat="server"> ประเภทการว่าจ้าง: </asp:Label></strong>
                                                                    <asp:Label ID="lbhire_name" runat="server" Text='<%# Eval("hire_name") %>'></asp:Label>

                                                                    <br />
                                                                </ItemTemplate>

                                                            </asp:TemplateField>



                                                            <asp:TemplateField HeaderText="คุณสมบัติ" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="30%" ItemStyle-Font-Size="Small">

                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbleduidx" Visible="false" runat="server" Text='<%# Eval("eduidx") %>' />

                                                                    <strong>
                                                                        <asp:Label ID="Label602" runat="server"> วุฒิการศึกษา: </asp:Label></strong>
                                                                    <asp:Literal ID="lbeducation_name" runat="server" Text='<%# Eval("education_name") %>' />
                                                                    <br />
                                                                    <strong>
                                                                        <asp:Label ID="Label6027" runat="server"> ประสบการณ์ทำงาน: </asp:Label></strong>
                                                                    <asp:Label ID="lbexperience" runat="server" Text='<%# Eval("experience") %>'></asp:Label>
                                                                    <br />
                                                                    <strong>
                                                                        <asp:Label ID="lbdat1" runat="server"> ความรู้ภาษา: </asp:Label></strong>
                                                                    <asp:Label ID="lblanguage" runat="server" Text='<%# Eval("language") %>'></asp:Label>
                                                                    <br />
                                                                    <strong>
                                                                        <asp:Label ID="Label531" runat="server"> คุณสมบัติอื่นๆ: </asp:Label></strong>
                                                                    <asp:Label ID="lbother" runat="server" Text='<%# Eval("other") %>'></asp:Label>

                                                                    <br />
                                                                </ItemTemplate>

                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="เพศสภาพ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                <ItemTemplate>

                                                                    <asp:GridView ID="GvSexAdd"
                                                                        runat="server"
                                                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                        HeaderStyle-CssClass="success small"
                                                                        AutoGenerateColumns="false">


                                                                        <PagerStyle CssClass="pageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>

                                                                            <asp:TemplateField HeaderText="เพศ" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" ItemStyle-Font-Size="Small">

                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblsexidx" Visible="false" runat="server" Text='<%# Eval("sexidx") %>' />
                                                                                    <asp:Literal ID="lbsex_name" runat="server" Text='<%# Eval("sex_name") %>' />
                                                                                    <asp:Label ID="lbCheckRow_sex" Visible="false" runat="server" Text='<%# Eval("CheckRow_sex") %>' />

                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>


                                                                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" ItemStyle-Font-Size="Small">

                                                                                <ItemTemplate>
                                                                                    <asp:Literal ID="lbqty_sex" runat="server" Text='<%# Eval("sub_qty") %>' />
                                                                                    <asp:Literal ID="textcount" runat="server" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>


                                                                            <asp:TemplateField HeaderText="สัญชาติ" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" Visible="true" ItemStyle-Font-Size="Small">

                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblnatidx" Visible="false" runat="server" Text='<%# Eval("natidx") %>' />
                                                                                    <asp:Literal ID="lbnationality_name" runat="server" Text='<%# Eval("nationality_name") %>' />

                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>

                                                                </ItemTemplate>
                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="bnDeleteList" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                                </ItemTemplate>
                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                        </Columns>

                                                    </asp:GridView>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Label29" CssClass="col-sm-2 control-label" runat="server" Text="ระบุเหตุผลในการขอคน : " />
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="txtreason" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtreason" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกระบุเหตุผลในการขอคน"
                                                        ValidationExpression="กรุณากรอกระบุเหตุผลในการขอคน"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                        ValidationGroup="SaveAdd" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtreason"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator4" Width="160" />

                                                </div>

                                            </div>


                                            <asp:UpdatePanel ID="UpdatePnFileUpload" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group">
                                                        <asp:Label ID="Label6" CssClass="col-sm-2 control-label" runat="server" Text="แนบไฟล์ Organization Chart : " />

                                                        <div class="col-md-6">
                                                            <asp:FileUpload ID="UploadFileOrg" AutoPostBack="true" ViewStateMode="Enabled" ValidationGroup="SaveAdd"
                                                                CssClass="btn btn-info btn-sm UploadFileOrg multi max-1 accept-png|jpg|pdf maxsize-1024 with-preview" runat="server" />

                                                            <asp:RequiredFieldValidator ID="Req_UploadFile"
                                                                runat="server" ControlToValidate="UploadFileOrg" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="กรุณาอัพโหลดไฟล์ Org Chart"
                                                                ValidationGroup="SaveAdd" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadFile" Width="200" PopupPosition="BottomLeft" />

                                                            <small>
                                                                <p class="help-block"><font color="red">**Attach File name jpg, png, pdf</font></p>
                                                            </small>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnAdddata" />
                                                </Triggers>
                                            </asp:UpdatePanel>

                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group">
                                                        <asp:Label ID="Label41" CssClass="col-sm-2 control-label" runat="server" Text="แนบไฟล์ Job Description: " />

                                                        <div class="col-md-6">
                                                            <asp:FileUpload ID="UploadFileJD" AutoPostBack="true" ViewStateMode="Enabled" ValidationGroup="SaveAdd"
                                                                CssClass=" btn btn-success btn-sm UploadFileJD multi  accept-png|jpg|pdf maxsize-1024 with-preview" runat="server" />

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16"
                                                                runat="server" ControlToValidate="UploadFileJD" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="กรุณาอัพโหลดไฟล์ Job Description"
                                                                ValidationGroup="SaveAdd" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="RequiredFieldValidator16" TargetControlID="RequiredFieldValidator16" Width="200" PopupPosition="BottomLeft" />

                                                            <small>
                                                                <p class="help-block"><font color="red">**Attach File name jpg, png, pdf</font></p>
                                                            </small>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnAdddata" />
                                                </Triggers>
                                            </asp:UpdatePanel>

                                            <div class="form-group">
                                                <asp:Label ID="Label21" CssClass="col-sm-3 control-label" runat="server" Text="หมายเหตุ : กำหนดวันที่ต้องการภายใน" />
                                                <div class="col-md-1">
                                                    <asp:TextBox ID="txtdatelimit" CssClass="form-control" runat="server"> </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RqRetxtsssprice22" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                        ControlToValidate="txtdatelimit" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกจำนวน" />
                                                    <asp:RegularExpressionValidator ID="Retaxtpraaice22" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                        ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                        ControlToValidate="txtdatelimit"
                                                        ValidationExpression="^[0-9]{1,10}$" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalwloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtsssprice22" Width="160" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallsoutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retaxtpraaice22" Width="160" />
                                                </div>
                                                <div class="col-md-8">
                                                    <p class="help-block">วันทำการ นับตั้งแต่ได้รับอนุมัติ (กระบวนการสรรหา คัดเลือก และจ้างงาน ใช้เวลาประมาณ 30 วัน ไม่รวมระยะเวลาที่ใช้ในการให้ผู้ได้รับคัดเลือกลาออกจากที่ทำงานเดิม 15-30 วัน)</p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-2">
                                                </div>
                                                <div class="col-md-10">
                                                    <b><u>การลงนามอนุมัติ</u></b>
                                                    <p>
                                                        - ทุกตำแหน่งงานต้อนผ่านการลงนามอนุมัติโดยประธานเจ้าหน้าที่บริหารเท่านั้น
                                                    </p>
                                                    <%-- <p>
                                                        - พนักงานรายวัน ลงนามอนุมัติโดยหัวหน้าฝ่าย ผู้ช่วย/ผู้จัดการฝ่าย ผู้อำนวยการฝ่าย
                                                    </p>
                                                    <p>
                                                        - พนักงานรายเดือนระดับเจ้าหน้าที่ - หัวหน้าส่วน ลงนามอนุมัติโดย ผู้ช่วย/ผู้จัดการฝ่าย ผู้อำนวยการฝ่าย
                                                    </p>
                                                    <p>
                                                        - พนักงานรายเดือนระดับผู้ช่วยผู้จัดการ - ผู้จัดการฝ่าย ลงนามอนุมัติโดย ผู้อำนวยการฝ่าย และ ประธานเจ้าหน้าที่บริหารฝ่าย
                                                    </p>
                                                    <p>
                                                        - พนักงานรายเดือนระดับผู้อำนวยการฝ่าย ลงนามอนุมัติโดย ประธานเจ้าหน้าที่บริหารฝ่าย และ กรรมการผู้จัดการ
                                                    </p>
                                                    <p>
                                                        - พนักงานรายเดือนระดับประธานเจ้าหน้าที่ฝ่าย ลงนามอนุมัติโดย ประธานเจ้าหน้าที่บริหาร

                                                    </p>--%>
                                                </div>
                                            </div>

                                            <div class="form-group" runat="server" visible="false" id="div_save">
                                                <div class="col-sm-2 col-sm-offset-10">
                                                    <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>



                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewApprove" runat="server">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12">
                        <div class="col-lg-12">
                            <asp:Image ID="Image1" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/hr_manpower/Header_Manpower-01.png" Style="height: 100%; width: 100%;" />
                        </div>

                        <div class="col-lg-12">

                            <div class="alert-message alert-message-success">
                                <blockquote class="danger" style="font-size: small; background-color: goldenrod; text-align: left">
                                    <h4><b>รายการรออนุมัติ</b></h4>
                                </blockquote>

                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:GridView ID="GvApproveList" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                        HeaderStyle-CssClass="info small"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="false"
                                        OnRowDataBound="Master_RowDataBound"
                                        DataKeyNames="u0_docidx">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="รายการที่" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                                    <asp:Label ID="lbdoc_code" runat="server" Text='<%# Eval("doc_code") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วันที่ทำรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbname" runat="server" Text='<%# Eval("createdate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ข้อมูลผู้ทำรายการ" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <strong>
                                                            <asp:Label ID="Label15" runat="server">องค์กร: </asp:Label></strong>
                                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Label24" runat="server">ฝ่าย: </asp:Label></strong>
                                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Label25" runat="server">แผนก: </asp:Label></strong>
                                                        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                        </p>
                                                           <strong>
                                                               <asp:Label ID="Label19" runat="server">ตำแหน่ง: </asp:Label></strong>
                                                        <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("PosNameTH") %>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Label26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                                        </p>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ข้อมูลรายการ" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <strong>
                                                            <asp:Label ID="Labelc15" runat="server">ระบบ: </asp:Label></strong>
                                                        <asp:Literal ID="Lwiteral3" runat="server" Text='<%# Eval("type_man") %>' />
                                                        <asp:Literal ID="litm0_typeidx" Visible="false" runat="server" Text='<%# Eval("m0_typeidx") %>' />

                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labelw24" runat="server">ประเภทการขอคน: </asp:Label></strong>
                                                        <asp:Literal ID="Litderal1" runat="server" Text='<%# Eval("type_list") %>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labedl25" runat="server">สถานที่: </asp:Label></strong>
                                                        <asp:Literal ID="Litefral2" runat="server" Text='<%# Eval("LocName") %>' />
                                                        </p>
                                                         <strong>
                                                             <asp:Label ID="Label42" runat="server">ประเภทพนักงาน: </asp:Label></strong>
                                                        <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("EmpTypeName") %>' />
                                                        </p>
                                                           <strong>
                                                               <asp:Label ID="Lvabel19" runat="server">สาเหตุการขอคน: </asp:Label></strong>
                                                        <asp:Literal ID="Litweral5" runat="server" Text='<%# Eval("reason_comment") %>' />
                                                        </p>
                                                <div id="div_replacecode" runat="server" visible="false">
                                                    <strong>
                                                        <asp:Label ID="Label69" runat="server">ยกเลิกรายการ: </asp:Label></strong>
                                                    <asp:Literal ID="litdoc_code_ref" runat="server" Text='<%# Eval("doc_code_ref") %>' />
                                                    </p>
                                                </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Literal ID="ltunidx" Visible="false" runat="server" Text='<%# Eval("unidx") %>' /></small>
                                                       <asp:Literal ID="ltacidx" Visible="false" runat="server" Text='<%# Eval("acidx") %>' /></small>
                                                    <asp:Literal ID="ltstaidx" Visible="false" runat="server" Text='<%# Eval("staidx") %>' /></small>
                                                     <asp:Label ID="lblstasus" Font-Bold="true" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                                    <asp:Label ID="Label66" Visible="false" runat="server" Text='<%# Eval("doc_refidx") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnviewdetail" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("CEmpIDX")  + ";" +"2" +  ";" + Eval("doc_code")+  ";" +  Eval("m0_typeidx")  %>'><i class="	fa fa-book"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnviewdetail_cancel" Visible="false" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" ToolTip="View" CommandName="CmdDetail" CommandArgument='<%#  Eval("doc_refidx") + ";" + Eval("CEmpIDX") + ";" +"2"+  ";" +  Eval("doc_code")+  ";" +  Eval("m0_typeidx") %>'><i class="	fa fa-book"></i></asp:LinkButton>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewDetail" runat="server">
            <div class="col-md-12" id="div2" runat="server" style="color: transparent;">
                <asp:FileUpload ID="FileUpload2" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />
            </div>
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="Fvdetailusercreate" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtorgidx" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtsecidx" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-heart-empty"></i><strong>&nbsp; รายละเอียดข้อมูล</strong></h3>
                    </div>

                    <asp:FormView ID="fvdetailmanpower" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <ItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("unidx") %>' />
                                    <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("acidx") %>' />
                                    <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />
                                    <asp:HiddenField ID="hfm0_typeidx" runat="server" Value='<%# Eval("m0_typeidx") %>' />
                                    <asp:HiddenField ID="hfm0type_listidx" runat="server" Value='<%# Eval("m0type_listidx") %>' />
                                    <asp:HiddenField ID="hfLocidx" runat="server" Value='<%# Eval("LocIDX") %>' />
                                    <asp:HiddenField ID="hfdoc_refidx" runat="server" Value='<%# Eval("doc_refidx") %>' />


                                    <div class="form-group">
                                        <asp:Label ID="Label30" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="ระบบ :" />
                                        <div class="col-md-3">
                                            <asp:Label ID="Label32" class="control-labelnotop" Text='<%# Eval("type_man") %>' runat="server"> </asp:Label>
                                        </div>
                                        <asp:Label ID="Label22" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="รหัสรายการ :" />
                                        <div class="col-md-3">
                                            <asp:Label ID="txtdoc_code" class="control-labelnotop" Text='<%# Eval("doc_code") %>' runat="server"> </asp:Label>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label39" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="ประเภทการขอคน :" />
                                        <div class="col-md-3">
                                            <asp:Label ID="Label40" class="control-labelnotop" Text='<%# Eval("type_list") %>' runat="server"> </asp:Label>
                                        </div>
                                        <asp:Label ID="Label35" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="สถานที่ :" />
                                        <div class="col-md-3">
                                            <asp:Label ID="Label36" class="control-labelnotop" Text='<%# Eval("LocName") %>' runat="server"> </asp:Label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label37" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="สาเหตุการขอคน :" />
                                        <div class="col-md-9">
                                            <asp:Label ID="Label38" class="control-labelnotop" Text='<%# Eval("reason_comment") %>' runat="server"> </asp:Label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label21" CssClass=" control-label" runat="server" Text="หมายเหตุ : กำหนดวันที่ต้องการภายใน" />
                                            <asp:Label ID="txtdate_limit" class="control-labelnotop" Text='<%# Eval("date_limit") %>' runat="server"> </asp:Label>
                                            <asp:Label ID="Label23" CssClass="control-label" runat="server" Text="วันทำการ นับตั้งแต่ได้รับอนุมัติ (กระบวนการสรรหา คัดเลือก และจ้างงาน ใช้เวลาประมาณ 30 วัน ไม่รวมระยะเวลาที่ใช้ในการให้ผู้ได้รับคัดเลือกลาออกจากที่ทำงานเดิม 15-30 วัน)" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label17" CssClass="col-sm-1 control-label" runat="server" />

                                        <div class="col-lg-8">
                                            <asp:GridView ID="gvFile" Visible="true" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                                HeaderStyle-CssClass="danger"
                                                OnRowDataBound="Master_RowDataBound"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                Font-Size="Small">

                                                <Columns>
                                                    <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                        <ItemTemplate>
                                                            <div class="col-lg-10">
                                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                            </div>
                                                            <div class="col-lg-2">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                            <h4></h4>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:FormView>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <asp:GridView ID="GvDetailManPower" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="success small"
                                HeaderStyle-Height="40px"
                                AllowPaging="false"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnRowUpdating="Master_RowUpdating"
                                DataKeyNames="u1_docidx">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField HeaderText="ข้อมูลตำแหน่งที่ต้องการ" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="litunidx" Visible="false" runat="server" Text='<%# Eval("unidx") %>' />
                                                <asp:Literal ID="litstaidx" Visible="false" runat="server" Text='<%# Eval("staidx") %>' />
                                                <asp:Literal ID="litu1_docidx" Visible="false" runat="server" Text='<%# Eval("u1_docidx") %>' />
                                                <strong>
                                                    <asp:Label ID="Labael15" runat="server">องค์กร: </asp:Label></strong>
                                                <asp:Literal ID="Literaal3" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Labeal24" runat="server">ฝ่าย: </asp:Label></strong>
                                                <asp:Literal ID="Litearal1" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Label2a5" runat="server">แผนก: </asp:Label></strong>
                                                <asp:Literal ID="Litaeral2" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                </p>
                                                           <strong>
                                                               <asp:Label ID="Lwabel19" runat="server">ตำแหน่ง: </asp:Label></strong>
                                                <asp:Literal ID="Literwal5" runat="server" Text='<%# Eval("PosNameTH") %>' />
                                                </p>
                                                        <strong>
                                                            <asp:Label ID="Labela21" runat="server">ประเภทพนักงาน: </asp:Label></strong>
                                                <asp:Literal ID="Litersal7" runat="server" Text='<%# Eval("EmpTypeName") %>' />
                                                </p>
                                            </small>
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtu1_docidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("u1_docidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="วันที่ต้องการ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-4">
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="datereceive_edit" Text='<%# Eval("date_receive")%>' runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save_Edit" runat="server" Display="None"
                                                                    ControlToValidate="datereceive_edit" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                                    ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label11" CssClass="col-sm-3 control-label" runat="server" Text="ลักษณะการว่าจ้าง : " />
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtm0_thidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_thidx")%>' />
                                                            <asp:RadioButtonList ID="rdotypehire_edit" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Horizontal">
                                                            </asp:RadioButtonList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                                ValidationGroup="Save_Edit" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกลักษณะการว่าจ้าง"
                                                                Display="None" SetFocusOnError="true" ControlToValidate="rdotypehire_edit" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160"
                                                                PopupPosition="BottomLeft" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label12" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทการว่าจ้าง : " />
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtm0_hridx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_hridx")%>' />

                                                            <asp:RadioButtonList ID="rdohire_edit" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Horizontal">
                                                            </asp:RadioButtonList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                                ValidationGroup="Save_Edit" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกประเภทการว่าจ้าง"
                                                                Display="None" SetFocusOnError="true" ControlToValidate="rdohire_edit" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenxder7" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                                                PopupPosition="BottomLeft" />
                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label10" CssClass="col-sm-3 control-label" runat="server" Text="จำนวน : " />
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtqty_edit" Text='<%# Eval("qty")%>' CssClass="form-control" runat="server" placeholder="0"> </asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="Save_Edit" runat="server" Display="None"
                                                                ControlToValidate="txtqty_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกจำนวน" />
                                                            <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="Save_DataSet" Display="None"
                                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                ControlToValidate="txtqty_edit"
                                                                ValidationExpression="^[0-9]{1,10}$" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                                        </div>
                                                    </div>


                                                    <div class="form-group">

                                                        <asp:Label ID="Label13" CssClass="col-sm-3 control-label" runat="server" Text="วุฒิการศึกษา : " />
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txteduidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("eduidx")%>' />

                                                            <asp:DropDownList ID="ddleducation_edit" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกวุฒิการศึกษา....</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_Edit" runat="server" Display="None"
                                                                ControlToValidate="ddleducation_edit" Font-Size="11"
                                                                ErrorMessage="เลือกวุฒิการศึกษา"
                                                                ValidationExpression="เลือกวุฒิการศึกษา" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label14" CssClass="col-sm-3 control-label" runat="server" Text="ประสบการณ์ทำงาน : " />
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtexp_edit" Text='<%# Eval("experience")%>' TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidsator8" ValidationGroup="Save_Edit" runat="server" Display="None" ControlToValidate="txtexp_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallodutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidsator8" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                                ValidationGroup="Save_Edit" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtexp_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutEqxtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Labela15" CssClass="col-sm-3 control-label" runat="server" Text="ความรู้ทางภาษา : " />
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtlanguage_edit" Text='<%# Eval("language")%>' TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save_Edit" runat="server" Display="None" ControlToValidate="txtlanguage_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกความรู้ทางภาษา"
                                                                ValidationExpression="กรุณากรอกความรู้ทางภาษา"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidsator8" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                                ValidationGroup="Save_Edit" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtlanguage_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label16" CssClass="col-sm-3 control-label" runat="server" Text="คุณสมบัติอื่นๆ : " />
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtother_edit" Text='<%# Eval("other")%>' TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save_Edit" runat="server" Display="None" ControlToValidate="txtother_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกคุณสมบัติอื่น"
                                                                ValidationExpression="กรุณากรอกคุณสมบัติอื่น"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidsator8" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                                ValidationGroup="Save_Edit" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtother_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-lg-3">
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="panel-body">
                                                                <div class="form-horizontal" role="form">

                                                                    <asp:GridView ID="Gvsex_edit" runat="server"
                                                                        AutoGenerateColumns="false"
                                                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                        HeaderStyle-CssClass="info small"
                                                                        HeaderStyle-Height="40px"
                                                                        AllowPaging="false"
                                                                        OnRowDataBound="Master_RowDataBound"
                                                                        DataKeyNames="sexidx">

                                                                        <PagerStyle CssClass="pageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>
                                                                        <Columns>
                                                                            <%--   <asp:TemplateField HeaderText="" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chksex_edit" OnCheckedChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>--%>

                                                                            <asp:TemplateField HeaderText="เพศ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lbsexname" runat="server" Text='<%# Eval("sex_name") %>'></asp:Label>
                                                                                    <asp:Label ID="lblsexidx" runat="server" Visible="false" Text='<%# Eval("sexidx") %>'></asp:Label>
                                                                                    <asp:Label ID="Label86" runat="server" Visible="false" Text='<%# Eval("sexidx") %>'></asp:Label>

                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="txtqty_sex_edit" Text='<%# Eval("sub_qty") %>' CssClass="form-control" runat="server" placeholder="0"> </asp:TextBox>

                                                                                    <%--  <asp:RequiredFieldValidator ID="RqRetxtsprice22" ValidationGroup="Save_DataSet" runat="server" Display="None"
                                                                            ControlToValidate="txtqty_sex" Font-Size="11"
                                                                            ErrorMessage="กรุณากรอกจำนวน" />--%>
                                                                                    <%--  <asp:RegularExpressionValidator ID="Retaxtprice22" runat="server" ValidationGroup="Save_DataSet" Display="None"
                                                                            ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                            ControlToValidate="txtqty_sex"
                                                                            ValidationExpression="^[0-9]{1,10}$" />--%>
                                                                                    <%--                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtsprice22" Width="160" />--%>
                                                                                    <%--                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retaxtprice22" Width="160" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="สัญชาติ" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                                                <ItemTemplate>
                                                                                    <asp:Literal ID="litnatidx" Visible="false" runat="server" Text='<%# Eval("natidx") %>' />
                                                                                    <asp:CheckBoxList ID="chknation_edit" runat="server" RepeatColumns="3"></asp:CheckBoxList>

                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>


                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="col-lg-3">
                                                        </div>
                                                    </div>



                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_Edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ข้อมูลประกอบตำแหน่ง" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <strong>
                                                    <asp:Label ID="Labele15" runat="server">วันที่ต้องการ: </asp:Label></strong>
                                                <asp:Literal ID="Literarl3" runat="server" Text='<%# Eval("date_receive") %>' />
                                                </p>
                                               
                                                 <strong>
                                                     <asp:Label ID="Labelt25" runat="server">ลักษณะการว่าจ้าง: </asp:Label></strong>
                                                <asp:Literal ID="Litbeeral2" runat="server" Text='<%# Eval("typehire_name") %>' />
                                                </p>
                                                           <strong>
                                                               <asp:Label ID="Laebel19" runat="server">ประเภทการว่าจ้าง: </asp:Label></strong>
                                                <asp:Literal ID="Literajl5" runat="server" Text='<%# Eval("hire_name") %>' />
                                                </p>
                                               
                                                        <strong>
                                                            <asp:Label ID="Labrel20" runat="server">จำนวน: </asp:Label></strong>
                                                <asp:Literal ID="Literalh6" runat="server" Text='<%# Eval("qty") %>' />
                                                </p>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="คุณสมบัติที่ต้องการ" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <strong>
                                                    <asp:Label ID="Label15" runat="server">วุฒิการศึกษา: </asp:Label></strong>
                                                <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("education_name") %>' />
                                                </p>
                                               
                                                 <strong>
                                                     <asp:Label ID="Label25" runat="server">ประสบการณ์ทำงาน: </asp:Label></strong>
                                                <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("experience") %>' />
                                                </p>
                                                           <strong>
                                                               <asp:Label ID="Label19" runat="server">ความรู้ทางภาษา: </asp:Label></strong>
                                                <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("language") %>' />
                                                </p>
                                               
                                                        <strong>
                                                            <asp:Label ID="Label20" runat="server">คุณสมบัติอื่นๆ: </asp:Label></strong>
                                                <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("other") %>' />
                                                </p>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="เพศสภาพ" ItemStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:GridView ID="GvDetailSex" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                HeaderStyle-CssClass="info small"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="false"
                                                OnRowDataBound="Master_RowDataBound"
                                                DataKeyNames="u2_docidx">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>


                                                    <asp:TemplateField HeaderText="เพศ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>

                                                                <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("sex_name") %>' />
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="จำนวน" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("sub_qty") %>' />
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="สัญชาติ" ItemStyle-Width="70%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <strong>
                                                                    <asp:Literal ID="litnatidx" Visible="false" runat="server" Text='<%# Eval("natidx") %>' />
                                                                    <asp:CheckBoxList ID="chknation" Enabled="false" runat="server" RepeatColumns="3"></asp:CheckBoxList>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                </Columns>
                                            </asp:GridView>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Management" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>

                                                <asp:LinkButton ID="btnedit" runat="server" CssClass="btn btn-sm btn-warning" data-toggle="tooltip" title="Edit" CommandName="Edit"><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btnDelete" CssClass="btn btn-danger" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการยืนยันการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("u1_docidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                            </small>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:LinkButton ID="btndetailprint" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" CommandName="CmdDetail_Print" CommandArgument='<%# Eval("u1_docidx") + ";" +  Eval("SecNameTH") %>'><i class="glyphicon glyphicon-eye-open"></i> </asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>


                            <div id="div_approve" runat="server" visible="false">
                                <div class="form-group">

                                    <asp:Label ID="Label33" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานะอนุมัติ" />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddl_approve" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="0">เลือกสถานะอนุมัติ</asp:ListItem>
                                            <asp:ListItem Value="2">อนุมัติ</asp:ListItem>
                                            <asp:ListItem Value="3">ไม่อนุมัติ </asp:ListItem>
                                            <asp:ListItem Value="4">ส่งกลับแก้ไขรายการ </asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ValidationGroup="SaveApprove" runat="server" Display="None"
                                            ControlToValidate="ddl_approve" Font-Size="11"
                                            ErrorMessage="เลือกสถานะอนุมัติ"
                                            ValidationExpression="เลือกสถานะอนุมัติ" InitialValue="0" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />

                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label26" runat="server" Text="ความคิดเห็นเพิ่มเติม" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtremark_approve" TextMode="multiline" Width="510pt" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveApprove" runat="server" Display="None" ControlToValidate="txtremark_approve" Font-Size="11"
                                            ErrorMessage="กรุณากรอกความคิดเห็น"
                                            ValidationExpression="กรุณากรอกความคิดเห็น"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                            ValidationGroup="SaveApprove" Display="None"
                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                            ControlToValidate="txtremark_approve"
                                            ValidationExpression="^[\s\S]{0,1000}$"
                                            SetFocusOnError="true" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorsCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                    </div>


                                </div>
                                <asp:UpdatePanel ID="UpdatePnFileUpload" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group" id="div_uploadfilememo" runat="server" visible="false">
                                            <asp:Label ID="Labedl6" CssClass="col-sm-2 control-label" runat="server" Text="แนบไฟล์ เอกสารลงนามอนุมัติโดยประธานเจ้าหน้าที่บริหาร : " />

                                            <div class="col-md-6">
                                                <asp:FileUpload ID="UploadFileMemo" AutoPostBack="true" ViewStateMode="Enabled" ValidationGroup="SaveApprove"
                                                    CssClass="btn btn-info btn-sm UploadFileMemo multi max-1 accept-png|jpg|pdf maxsize-1024 with-preview" runat="server" />

                                                <asp:RequiredFieldValidator ID="Req_UploadFileMemo"
                                                    runat="server" ControlToValidate="UploadFileMemo" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="กรุณาอัพโหลดไฟล์ เอกสารลงนามอนุมัติโดยประธานเจ้าหน้าที่บริหาร"
                                                    ValidationGroup="SaveApprove" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtendewr19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadFileMemo" Width="200" PopupPosition="BottomLeft" />

                                                <small>
                                                    <p class="help-block"><font color="red">**Attach File name jpg, png, pdf</font></p>
                                                </small>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnApprove" />
                                    </Triggers>
                                </asp:UpdatePanel>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <asp:LinkButton ID="btnApprove" ValidationGroup="SaveApprove" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdUpdateApprove" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                    </div>
                                </div>


                            </div>

                            <div class="form-group">
                                <div class="col-sm-1 col-sm-offset-11">
                                    <asp:LinkButton ID="btnback" CssClass="btn btn-danger btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnback" data-toggle="tooltip" title="Close"><i class="glyphicon glyphicon-home"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-12" id="div_canceldetail" runat="server" visible="false">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-ban-circle"></i><strong>&nbsp; เหตุผลในการยกเลิก</strong></h3>
                    </div>
                    <asp:FormView ID="fvdetail_cancel" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <ItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <asp:HiddenField ID="hfm0type_listidx" runat="server" Value='<%# Eval("m0type_listidx") %>' />
                                    <asp:HiddenField ID="hfLocidx" runat="server" Value='<%# Eval("LocIDX") %>' />
                                    <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("unidx") %>' />
                                    <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("acidx") %>' />
                                    <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />
                                    <asp:HiddenField ID="hfm0_typeidx" runat="server" Value='<%# Eval("m0_typeidx") %>' />

                                    <div class="form-group">
                                        <asp:Label ID="Label30" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="ระบบ :" />
                                        <div class="col-md-3">
                                            <asp:Label ID="Label32" class="control-labelnotop" Text='<%# Eval("type_man") %>' runat="server"> </asp:Label>
                                        </div>
                                        <asp:Label ID="Label22" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="รหัสรายการ :" />
                                        <div class="col-md-3">
                                            <asp:Label ID="txtdoc_code" class="control-labelnotop" Text='<%# Eval("doc_code") %>' runat="server"> </asp:Label>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label37" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="สาเหตุการยกเลิก :" />
                                        <div class="col-md-9">
                                            <asp:Label ID="Label38" class="control-labelnotop" Text='<%# Eval("reason_comment") %>' runat="server"> </asp:Label>
                                        </div>
                                    </div>


                                    <div id="div_approve_cancel" runat="server" visible="false">
                                        <div class="panel-body">
                                            <div class="form-horizontal" role="form">
                                                <div class="form-group">

                                                    <asp:Label ID="Label67" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานะอนุมัติ" />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlaproove_cancel" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0">เลือกสถานะอนุมัติ</asp:ListItem>
                                                            <asp:ListItem Value="5">อนุมัติ</asp:ListItem>
                                                            <asp:ListItem Value="6">ไม่อนุมัติ </asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ValidationGroup="SaveApprove_Cancel" runat="server" Display="None"
                                                            ControlToValidate="ddlaproove_cancel" Font-Size="11"
                                                            ErrorMessage="เลือกสถานะอนุมัติ"
                                                            ValidationExpression="เลือกสถานะอนุมัติ" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator17" Width="160" />

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label68" runat="server" Text="ความคิดเห็นเพิ่มเติม" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtremark_approve_cancel" TextMode="multiline" Width="510pt" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" ValidationGroup="SaveApprove_Cancel" runat="server" Display="None" ControlToValidate="txtremark_approve_cancel" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกความคิดเห็น"
                                                            ValidationExpression="กรุณากรอกความคิดเห็น"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator18" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                            ValidationGroup="SaveApprove_Cancel" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtremark_approve_cancel"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator5" Width="160" />

                                                    </div>


                                                </div>


                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-offset-2">
                                                        <asp:LinkButton ID="btnApprove_Cancel" ValidationGroup="SaveApprove_Cancel" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdUpdateApprove_Cancel" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton4" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>



                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:FormView>

                    <hr />



                </div>
            </div>


            <div class="col-lg-12">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-eye-open"></i><strong>&nbsp; ประวัติการอนุมัติ</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <asp:Repeater ID="rpLog" runat="server">
                                <HeaderTemplate>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"><small>วัน / เวลา</small></label>
                                        <label class="col-sm-3 control-label"><small>ผู้ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ผลการดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ความคิดเห็น</small></label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <%-- <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><%#Eval("createdate")%></small></span>--%>
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-8">
                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><%#Eval("createdate")%></small></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <span>&nbsp;&nbsp;&nbsp;<small><%# Eval("FullNameTH") %>&nbsp;&nbsp;&nbsp;(<%# Eval("actor_name") %>)</small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("node_name") %></small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("status_name") %></small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("comment") %></small></span>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12" runat="server">
                <div id="ordine" class="modal open" role="dialog">
                    <div class="modal-dialog" style="width: 70%;">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">ยืนยันการออกแบบฟอร์ม</h4>
                            </div>
                            <div class="modal-body">
                                <div class="panel-body">

                                    <div class="form-horizontal" role="form">

                                        <h5>คุณต้องการจะปริ้นท์แบบฟอร์มใบขอกำลังคนตำแหน่ง
                                                            <asp:Label ID="lblsecname" runat="server" /></h5>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-10">
                                                <asp:LinkButton ID="btnprint_memo" CssClass="btn btn-warning btn-sm" runat="server" data-dismiss="modal" OnClientClick="return printDiv('printable_memo');" CommandName="CmdPrint"><i class="fa fa-print"></i> Print</asp:LinkButton>
                                                <asp:LinkButton ID="Cancel" class="btn btn-default btn-sm" data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal" CommandName="Cancel">&nbsp;Close</asp:LinkButton>

                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Panel ID="box_print" runat="server" Visible="true">
                <asp:FormView ID="FvTemplate_print" runat="server" HorizontalAlign="Center" DefaultMode="ReadOnly" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>

                        <div id="printable_memo" class="print_report hidden">
                            <div class="panel panel-default">

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 18px">
                                    <tr>
                                        <div class="col-lg-3">
                                            <td style="text-align: right; vertical-align: top;">
                                                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/probation/logo.jpg" />
                                            </td>
                                        </div>
                                        <div class="col-lg-9">
                                            <td style="text-align: center; vertical-align: middle;">
                                                <h3 style="text-align: center;">บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด (มหาชน)</h3>
                                                <h3 style="text-align: center;">แบบฟอร์มการร้องขอให้จัดหาพนักงาน</h3>
                                            </td>
                                        </div>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10" style="border: 1px solid black; border-collapse: collapse; font-family: 'Angsana New'; font-size: 18px">
                                    <tr>
                                        <td style="text-align: left; vertical-align: middle; width: 280px; border: 1px solid black;"><b>ฝ่าย  : 
                                                <asp:Label ID="txtdoc_code" class="control-labelnotop" Text='<%# Eval("DeptNameTH") %>' runat="server"> </asp:Label>
                                            <asp:Label ID="lblu1_docidx" Visible="false" class="control-labelnotop" Text='<%# Eval("u1_docidx") %>' runat="server"> </asp:Label>
                                        </td>
                                        <td style="text-align: left; vertical-align: middle; width: 200px; border: 1px solid black"><b>แผนก :
                                             <asp:Label ID="Label44" class="control-labelnotop" Text='<%# Eval("SecNameTH") %>' runat="server"> </asp:Label></td>
                                        <td style="text-align: left; vertical-align: middle; width: 350px; border: 1px solid black" colspan="3"><b>ตำแหน่ง  :
                                              <asp:Label ID="Label45" class="control-labelnotop" Text='<%# Eval("PosNameTH") %>' runat="server"> </asp:Label></td>

                                    </tr>


                                </table>

                                <table class="table f-s-12 m-t-10" style="border: 1px solid black; border-collapse: collapse; font-family: 'Angsana New'; font-size: 18px">
                                    <tr>
                                        <td style="text-align: left; vertical-align: middle; width: 460px; border-right: 1px solid black"><b>วันที่ </b>
                                            <asp:Label ID="Label51" class="control-labelnotop" Text='<%# Eval("createdate") %>' runat="server"> </asp:Label>
                                        </td>
                                        <td style="text-align: left; vertical-align: middle; width: 380px;"><b>จำนวนที่ต้องการ : </b>
                                            <asp:Label ID="Label52" class="control-labelnotop" Text='<%# Eval("qty") %>' runat="server"> </asp:Label>
                                            <asp:Label ID="Label87" class="control-labelnotop" Text="คน" runat="server"> </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: middle; width: 400px; border-right: 1px solid black"><b>Cost Center </b>
                                            <asp:Label ID="Label50" class="control-labelnotop" Text='<%# Eval("CostNo") %>' runat="server"> </asp:Label>
                                        </td>
                                        <td style="text-align: left; vertical-align: middle; width: 480px;"><b>วันที่ต้องการ : </b>
                                            <asp:Label ID="Label53" class="control-labelnotop" Text='<%# Eval("date_receive") %>' runat="server"> </asp:Label>
                                        </td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10" style="border: 1px solid black; border-collapse: collapse; font-family: 'Angsana New'; font-size: 18px">
                                    <tr>
                                        <td style="text-align: left; vertical-align: middle; width: 480px; border-right: 1px solid black; border-bottom: 1px solid black"><u><b>คุณสมบัติที่ต้องการ </b></u>
                                        </td>
                                        <td style="text-align: left; vertical-align: middle; width: 480px; border-bottom: 1px solid black"><b>แนบ Job Description / เหตุผลในการขอ </b>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: middle; width: 480px; border-right: 1px solid black;"><b>1. วุฒิการศึกษา </b>
                                            <br />
                                            <asp:Label ID="Label46" class="control-labelnotop" Text='<%# Eval("education_name") %>' runat="server"> </asp:Label>

                                            <td style="text-align: left; vertical-align: middle; width: 480px;" rowspan="4"><b>องค์กร </b>
                                                <asp:Label ID="Label56" class="control-labelnotop" Text='<%# Eval("OrgNameTH") %>' runat="server"> </asp:Label>

                                                <br />
                                                <b>
                                                    <asp:Label ID="Labeel24" runat="server">ฝ่าย: </asp:Label></b>
                                                <asp:Literal ID="Litereal1" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                <br />
                                                <strong>
                                                    <asp:Label ID="Labeel25" runat="server">แผนก: </asp:Label></strong>
                                                <asp:Literal ID="Liteeral2" runat="server" Text='<%# Eval("SecNameTH") %>' />

                                                <br />
                                                <strong>
                                                    <asp:Label ID="Label54" runat="server">ตำแหน่ง: </asp:Label></strong>
                                                <asp:Literal ID="Literal9" runat="server" Text='<%# Eval("PosNameTH") %>' />

                                                <br />
                                                <strong>
                                                    <asp:Label ID="Labeld15_lv" runat="server">สถานที่: </asp:Label></strong>
                                                <asp:Literal ID="lblevel" runat="server" Text='<%# Eval("LocName") %>' />
                                                <br />
                                                <strong>
                                                    <asp:Label ID="Label55" runat="server">ประเภทพนักงาน: </asp:Label></strong>
                                                <asp:Literal ID="Literal10" runat="server" Text='<%# Eval("EmpTypeName") %>' />
                                                <br />


                                                <asp:GridView ID="GvDetailSex_Print" runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                    HeaderStyle-CssClass="info small"
                                                    HeaderStyle-Height="40px"
                                                    AllowPaging="false"
                                                    OnRowDataBound="Master_RowDataBound"
                                                    DataKeyNames="u2_docidx">

                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>


                                                        <asp:TemplateField HeaderText="เพศ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>

                                                                    <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("sex_name") %>' />
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="จำนวน" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("sub_qty") %>' />
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="สัญชาติ" ItemStyle-Width="70%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <strong>
                                                                        <asp:Literal ID="litnatidx" Visible="false" runat="server" Text='<%# Eval("natidx") %>' />
                                                                        <asp:CheckBoxList ID="chknation" runat="server" RepeatColumns="3"></asp:CheckBoxList>
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                    </Columns>
                                                </asp:GridView>

                                            </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: middle; width: 480px; border-right: 1px solid black;"><b>2. ประสบการณ์ทำงาน </b>
                                            <br />
                                            <asp:Label ID="Label47" class="control-labelnotop" Text='<%# Eval("experience") %>' runat="server"> </asp:Label>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: middle; width: 480px; border-right: 1px solid black;"><b>3. ความรู้ทางภาษา </b>
                                            <br />
                                            <asp:Label ID="Label48" class="control-labelnotop" Text='<%# Eval("language") %>' runat="server"> </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: middle; width: 480px; border-right: 1px solid black;"><b>4. คุณสมบัติอื่นๆ </b>
                                            <br />
                                            <asp:Label ID="Label49" class="control-labelnotop" Text='<%# Eval("other") %>' runat="server"> </asp:Label>
                                        </td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10" style="border: 1px solid black; border-collapse: collapse; font-family: 'Angsana New'; font-size: 18px">
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle; width: 400px; border: 1px solid black" rowspan="3"><b>ลักษณะการว่าจ้าง </b>
                                            <asp:CheckBoxList ID="chktypehire" runat="server" RepeatColumns="2"></asp:CheckBoxList>
                                            <asp:CheckBoxList ID="chkhire" runat="server" RepeatColumns="2"></asp:CheckBoxList>

                                        </td>
                                        <td style="text-align: left; vertical-align: middle; width: 700px;">หมายเหตุ : กำหนดวันที่ต้องการภายใน 
                                                    <asp:Label ID="Label57" class="control-labelnotop" Text='<%# Eval("date_limit") %>' runat="server"> </asp:Label>
                                            &nbsp; วันทำการ นับตั้งแต่ได้รับอนุมัติ (กระบวนการสรรหา คัดเลือก และจ้างงาน 
                                                       ใช้เวลาประมาณ 30 วัน ไม่รวมระยะเวลาที่ใช้ในการให้ผู้ได้รับคัดเลือกลาออกจากที่ทำงานเดิม 15-30 วัน)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: middle; width: 880px;"><u><b>การลงนามอนุมัติ</b></u>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: middle; width: 880px; border-bottom: 1px solid black">
                                        &nbsp;&nbsp;&nbsp;- ทุกตำแหน่งงานต้องผ่านการลง
                                             นามอนุมัติโดยประธานเจ้าหน้าที่บริหารเท่านั้น

                                    </tr>

                                </table>

                                <table class="table f-s-12 m-t-10" style="border: 1px solid black; border-collapse: collapse; font-family: 'Angsana New'; font-size: 18px">
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle; width: 1024px; border: 1px solid black" colspan="3"><b>การพิจารณาอนุมัติ </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle; width: 400px; border: 1px solid black"><b>หัวหน้าฝ่าย </b>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle; width: 400px; border: 1px solid black"><b>ผช.ผจก./ผู้จัดการฝ่าย </b>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle; width: 400px; border: 1px solid black"><b>ผู้อำนวยการฝ่าย / ประธานเจ้าหน้าที่บริหารฝ่าย </b>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td style="text-align: center; vertical-align: middle; width: 400px; border: 1px solid black" rowspan="3">
                                             ลงชื่อ  .......................................................
                                           
                                            <br />
                                            (&nbsp;&nbsp; 
                                            <asp:Label ID="Label58" class="control-labelnotop" Text='<%# Eval("Approve1") %>' runat="server"> </asp:Label>
                                            &nbsp;&nbsp;)                                            
                                            <br />
                                            วันที่ 
                                            <asp:Label ID="Label63" class="control-labelnotop" Text='<%# Eval("ApproveDate1") %>' runat="server"> </asp:Label>
                                            <br />
                                        </td>

                                        <td style="text-align: center; vertical-align: middle; width: 400px; border: 1px solid black" rowspan="3">
                                             ลงชื่อ ..........................................................
                                            <br />
                                            (&nbsp;&nbsp; 
                                            <asp:Label ID="Label59" class="control-labelnotop" Text='<%# Eval("Approve2") %>' runat="server"> </asp:Label>
                                            &nbsp;&nbsp;)                                            
                                            <br />
                                            วันที่ 
                                            <asp:Label ID="Label64" class="control-labelnotop" Text='<%# Eval("ApproveDate2") %>' runat="server"> </asp:Label>
                                            <br />
                                        </td>
                                        <td style="text-align: center; vertical-align: middle; width: 400px; border: 1px solid black" rowspan="3">
                                            ลงชื่อ .......................................................
                                            <br />
                                            (&nbsp;&nbsp; 
                                            <asp:Label ID="Label62" class="control-labelnotop" Text='<%# Eval("Approve3") %>' runat="server"> </asp:Label>
                                            &nbsp;&nbsp;)                                            
                                             <br />
                                            วันที่ 
                                            <asp:Label ID="Label65" class="control-labelnotop" Text='<%# Eval("ApproveDate3") %>' runat="server"> </asp:Label>
                                            <br />
                                        </td>
                                    </tr>

                                </table>

                                <table class="table f-s-12 m-t-10" style="border: 1px solid black; border-collapse: collapse; font-family: 'Angsana New'; font-size: 18px">
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle; width: 480px; border: 1px solid black"><b>บันทึกฝ่ายทรัพยากรบุคคล </b>

                                        </td>
                                        <td style="text-align: center; vertical-align: middle; width: 700px; border: 1px solid black"><b>ความเห็นของ กรรมการผู้จัดการ / ประธานเจ้าหน้าที่บริหาร  </b>
                                        </td>

                                    </tr>
                                    <tr>

                                        <td style="text-align: center; vertical-align: middle; width: 480px; border: 1px solid black">
                                            <br />
                                            ...............................................................
                                            <br />
                                            ...............................................................
                                            <br />
                                            <br />
                                            ลงชื่อ  ..............................................
                                             <br />
                                            (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
                                            <br />
                                            วันที่  ........../........../..........
                                        </td>

                                        <td style="text-align: left; vertical-align: middle; width: 700px; border: 1px solid black">
                                            <br />
                                            &nbsp;&nbsp;&nbsp; ..........................................................................................................................................................
                                            <br />
                                            &nbsp;&nbsp;&nbsp; ..........................................................................................................................................................
                                              <br />
                                            &nbsp;&nbsp;&nbsp;  ☐ &nbsp;&nbsp;&nbsp; อนุมัติ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;☐ &nbsp;&nbsp;&nbsp; ไม่อนุมัติ
                                            <br />

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    ลงชื่อ  
                                             ...................................................................................&nbsp;&nbsp;&nbsp;&nbsp;
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
                                            <br />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  วันที่  ............../.................../....................
                                        </td>

                                    </tr>

                                </table>

                                <table style="font-family: 'Angsana New'; font-size: 16px">
                                    <tr>
                                        <td style="text-align: right; vertical-align: middle; width: 120px;">FM-HR-RS-001/03  Rev.05 (14/05/2018)
                                        </td>
                                    </tr>

                                </table>



                            </div>
                        </div>


                    </ItemTemplate>
                </asp:FormView>
            </asp:Panel>

        </asp:View>

        <asp:View ID="ViewCancel" runat="server">
            <div class="col-lg-12">

                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: goldenrod; text-align: left">
                        <h4><b>ยกเลิกรายการขอคน</b></h4>
                    </blockquote>

                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser_Cancel" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-ban-circle"></i><strong>&nbsp; รายละเอียดรายการยกเลิก</strong></h3>
                    </div>
                    <asp:FormView ID="fvwheredoccode" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รายการที่ต้องการยกเลิก : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddldoccode_cancel" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกรายการที่ต้องการยกเลิก....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:LinkButton ID="btnviewdetail" CssClass="btn btn-warning btn-sm" runat="server" OnCommand="btnCommand" CommandName="CmdSearch_Cancel"><i class="	glyphicon glyphicon-search"></i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                    <div id="div_cancel" runat="server" visible="false">
                        <asp:FormView ID="fvinsert_cancel" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <ItemTemplate>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <asp:HiddenField ID="hfm0type_listidx" runat="server" Value='<%# Eval("m0type_listidx") %>' />
                                            <asp:HiddenField ID="hfLocidx" runat="server" Value='<%# Eval("LocIDX") %>' />
                                            <asp:HiddenField ID="hfemp_typeidx" runat="server" Value='<%# Eval("emp_typeidx") %>' />
                                            <asp:Label ID="Label30" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="ระบบ :" />
                                            <div class="col-md-3">
                                                <asp:Label ID="Label32" class="control-labelnotop" Text='<%# Eval("type_man") %>' runat="server"> </asp:Label>
                                            </div>
                                            <asp:Label ID="Label22" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="รหัสรายการ :" />
                                            <div class="col-md-3">
                                                <asp:Label ID="txtdoc_code" class="control-labelnotop" Text='<%# Eval("doc_code") %>' runat="server"> </asp:Label>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label39" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="ประเภทการขอคน :" />
                                            <div class="col-md-3">
                                                <asp:Label ID="Label40" class="control-labelnotop" Text='<%# Eval("type_list") %>' runat="server"> </asp:Label>
                                            </div>
                                            <asp:Label ID="Label35" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="สถานที่ :" />
                                            <div class="col-md-3">
                                                <asp:Label ID="Label36" class="control-labelnotop" Text='<%# Eval("LocName") %>' runat="server"> </asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label37" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="สาเหตุการขอคน :" />
                                            <div class="col-md-9">
                                                <asp:Label ID="Label38" class="control-labelnotop" Text='<%# Eval("reason_comment") %>' runat="server"> </asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label21" CssClass=" control-label" runat="server" Text="หมายเหตุ : กำหนดวันที่ต้องการภายใน" />
                                                <asp:Label ID="txtdate_limit" class="control-labelnotop" Text='<%# Eval("date_limit") %>' runat="server"> </asp:Label>
                                                <asp:Label ID="Label23" CssClass="control-label" runat="server" Text="วันทำการ นับตั้งแต่ได้รับอนุมัติ (กระบวนการสรรหา คัดเลือก และจ้างงาน ใช้เวลาประมาณ 30 วัน ไม่รวมระยะเวลาที่ใช้ในการให้ผู้ได้รับคัดเลือกลาออกจากที่ทำงานเดิม 15-30 วัน)" />
                                            </div>
                                        </div>
                            </ItemTemplate>
                        </asp:FormView>

                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <asp:GridView ID="GvManPowerDetail_Cancel" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="success small"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="false"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnRowEditing="Master_RowEditing"
                                    OnRowCancelingEdit="Master_RowCancelingEdit"
                                    OnRowUpdating="Master_RowUpdating"
                                    DataKeyNames="u1_docidx">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>


                                        <asp:TemplateField HeaderText="ข้อมูลตำแหน่งที่ต้องการ" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="litunidx" Visible="false" runat="server" Text='<%# Eval("unidx") %>' />
                                                    <asp:Literal ID="litstaidx" Visible="false" runat="server" Text='<%# Eval("staidx") %>' />
                                                    <asp:Literal ID="litu1_docidx" Visible="false" runat="server" Text='<%# Eval("u1_docidx") %>' />
                                                    <strong>
                                                        <asp:Label ID="Labael15" runat="server">องค์กร: </asp:Label></strong>
                                                    <asp:Literal ID="Literaal3" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labeal24" runat="server">ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="Litearal1" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label2a5" runat="server">แผนก: </asp:Label></strong>
                                                    <asp:Literal ID="Litaeral2" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                    </p>
                                                           <strong>
                                                               <asp:Label ID="Lwabel19" runat="server">ตำแหน่ง: </asp:Label></strong>
                                                    <asp:Literal ID="Literwal5" runat="server" Text='<%# Eval("PosNameTH") %>' />
                                                    </p>
                                                        <strong>
                                                            <asp:Label ID="Labela21" runat="server">ประเภทพนักงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Litersal7" runat="server" Text='<%# Eval("EmpTypeName") %>' />
                                                    </p>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ข้อมูลประกอบตำแหน่ง" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Labele15" runat="server">วันที่ต้องการ: </asp:Label></strong>
                                                    <asp:Literal ID="Literarl3" runat="server" Text='<%# Eval("date_receive") %>' />
                                                    </p>
                                               
                                                 <strong>
                                                     <asp:Label ID="Labelt25" runat="server">ลักษณะการว่าจ้าง: </asp:Label></strong>
                                                    <asp:Literal ID="Litbeeral2" runat="server" Text='<%# Eval("typehire_name") %>' />
                                                    </p>
                                                           <strong>
                                                               <asp:Label ID="Laebel19" runat="server">ประเภทการว่าจ้าง: </asp:Label></strong>
                                                    <asp:Literal ID="Literajl5" runat="server" Text='<%# Eval("hire_name") %>' />
                                                    </p>
                                               
                                                        <strong>
                                                            <asp:Label ID="Labrel20" runat="server">จำนวน: </asp:Label></strong>
                                                    <asp:Literal ID="Literalh6" runat="server" Text='<%# Eval("qty") %>' />
                                                    </p>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="คุณสมบัติที่ต้องการ" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label15" runat="server">วุฒิการศึกษา: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("education_name") %>' />
                                                    </p>
                                               
                                                 <strong>
                                                     <asp:Label ID="Label25" runat="server">ประสบการณ์ทำงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("experience") %>' />
                                                    </p>
                                                           <strong>
                                                               <asp:Label ID="Label19" runat="server">ความรู้ทางภาษา: </asp:Label></strong>
                                                    <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("language") %>' />
                                                    </p>
                                               
                                                        <strong>
                                                            <asp:Label ID="Label20" runat="server">คุณสมบัติอื่นๆ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("other") %>' />
                                                    </p>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="เพศสภาพ" ItemStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>

                                                <asp:GridView ID="GvDetailSex_Cancel" runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                    HeaderStyle-CssClass="info small"
                                                    HeaderStyle-Height="40px"
                                                    AllowPaging="false"
                                                    OnRowDataBound="Master_RowDataBound"
                                                    DataKeyNames="u2_docidx">

                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>


                                                        <asp:TemplateField HeaderText="เพศ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>

                                                                    <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("sex_name") %>' />
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="จำนวน" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("sub_qty") %>' />
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="สัญชาติ" ItemStyle-Width="70%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <strong>
                                                                        <asp:Literal ID="litnatidx" Visible="false" runat="server" Text='<%# Eval("natidx") %>' />
                                                                        <asp:CheckBoxList ID="chknation" Enabled="false" runat="server" RepeatColumns="3"></asp:CheckBoxList>
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                    </Columns>
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                                <asp:Panel ID="panel_cancel" runat="server" Visible="false">
                                    <div class="form-group">
                                        <asp:Label ID="Label16" CssClass="col-sm-2 control-label" runat="server" Text="สาเหตุที่ยกเลิกรายการ : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txcancel" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="SaveCancel" runat="server" Display="None" ControlToValidate="txcancel" Font-Size="11"
                                                ErrorMessage="กรุณากรอกสาเหตุที่ยกเลิกรายการ"
                                                ValidationExpression="กรุณากรอกสาเหตุที่ยกเลิกรายการ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveCancel" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txcancel"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-2">
                                            <asp:LinkButton ID="LinkButton1" ValidationGroup="SaveCancel" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSaveCancel" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewReport" runat="server">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                    <div class="col-lg-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; สรุปรายงานการขอกำลังคน</strong></h3>
                            </div>

                            <asp:FormView ID="fvreport" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทรายงาน : " />
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddltypereport" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกประเภทรายงาน....</asp:ListItem>
                                                                <asp:ListItem Value="1">ตาราง</asp:ListItem>
                                                                <asp:ListItem Value="2">กราฟ</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="SearchReport" runat="server" Display="None" ControlToValidate="ddltypereport" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกประเภทระบบงาน"
                                                                ValidationExpression="กรุณาเลือกประเภทระบบงาน"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallosutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                                        </div>
                                                    </div>

                                                    <asp:Panel ID="panel_table" runat="server" Visible="false">


                                                        <div class="form-group" id="panel_searchdate" runat="server">
                                                            <asp:Label ID="Label9" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>
                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="SelectedIndexChanged">
                                                                    <asp:ListItem Value="00">เลือกเงื่อนไข</asp:ListItem>
                                                                    <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                                                    <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                                    <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="col-sm-3 ">

                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control from-date-datepicker-report" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SearchReport" runat="server" Display="None"
                                                                        ControlToValidate="AddStartdate" Font-Size="11"
                                                                        ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                                        ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="AddEndDate" runat="server" CssClass="form-control from-date-datepicker-report" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>
                                                            <p class="help-block"><font color="red">**</font></p>
                                                        </div>


                                                        <div class="form-group">
                                                            <asp:Label ID="Label70" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlorgidx_search" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">กรุณาเลือกองค์กร....</asp:ListItem>
                                                                </asp:DropDownList>

                                                            </div>

                                                            <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlrdeptidx_search" runat="server" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                                    <asp:ListItem Value="0">กรุณาเลือกฝ่าย....</asp:ListItem>
                                                                </asp:DropDownList>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label71" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlrsecidx_search" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">กรุณาเลือกแผนก....</asp:ListItem>
                                                                </asp:DropDownList>

                                                            </div>

                                                            <asp:Label ID="Label72" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlrposidx_search" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">กรุณาเลือกตำแหน่ง....</asp:ListItem>
                                                                </asp:DropDownList>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label73" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทพนักงาน : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlemptype_search" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">กรุณาเลือกประเภทพนักงาน....</asp:ListItem>
                                                                </asp:DropDownList>

                                                            </div>


                                                            <asp:Label ID="Label76" CssClass="col-sm-3 control-label" runat="server" Text="สถานที่ : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlLocate_search" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">กรุณาเลือกประเภทพนักงาน....</asp:ListItem>
                                                                </asp:DropDownList>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">

                                                            <asp:Label ID="Label74" CssClass="col-sm-2 control-label" runat="server" Text="สถานะรายการ : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlstatus_search" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">รวมทั้งหมด</asp:ListItem>
                                                                    <asp:ListItem Value="7">ปิดจบการดำเนินการ</asp:ListItem>
                                                                    <asp:ListItem Value="11">อยู่ระหว่างดำเนินการ</asp:ListItem>
                                                                    <asp:ListItem Value="99">อยู่ระหว่างขั้นตอนการอนุมัติ</asp:ListItem>

                                                                </asp:DropDownList>

                                                            </div>
                                                            <asp:Label ID="Label75" CssClass="col-sm-3 control-label" runat="server" Text="รหัสรายการ : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtDocCode" runat="server" CssClass="form-control" placeholder="รหัสรายการ ....."></asp:TextBox>


                                                            </div>
                                                        </div>

                                                    </asp:Panel>

                                                    <asp:Panel ID="panel_chart" runat="server" Visible="false">

                                                        <div class="form-group">
                                                            <asp:Label ID="Label79" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทกราฟ : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddltypechart" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกประเภทกราฟ....</asp:ListItem>
                                                                    <asp:ListItem Value="1">สถิติขอคนของทุกฝ่าย</asp:ListItem>
                                                                    <asp:ListItem Value="2">สถิติขอคนของฝ่าย</asp:ListItem>
                                                                    <asp:ListItem Value="3">สถิติขอคนของแผนก</asp:ListItem>
                                                                    <asp:ListItem Value="4">สถิติขอคนของตำแหน่ง</asp:ListItem>
                                                                    <asp:ListItem Value="5">สถิติจำนวนรายการ</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ValidationGroup="SearchReport" runat="server" Display="None" ControlToValidate="ddltypechart" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกประเภทระบบงาน"
                                                                    ValidationExpression="กรุณาเลือกประเภทระบบงาน"
                                                                    SetFocusOnError="true" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label80" CssClass="col-sm-2 control-label" runat="server" Text="เดือน : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlmonth" CssClass="form-control" runat="server">
                                                                    <asp:ListItem Value="0">เลือกเดือน....</asp:ListItem>
                                                                    <asp:ListItem Value="1">มกราคม</asp:ListItem>
                                                                    <asp:ListItem Value="2">กุมภาพันธ์</asp:ListItem>
                                                                    <asp:ListItem Value="3">มีนาคม</asp:ListItem>
                                                                    <asp:ListItem Value="4">เมษายน</asp:ListItem>
                                                                    <asp:ListItem Value="5">พฤกษภาคม</asp:ListItem>
                                                                    <asp:ListItem Value="6">มิถุนายน</asp:ListItem>
                                                                    <asp:ListItem Value="7">กรกฎาคม</asp:ListItem>
                                                                    <asp:ListItem Value="8">สิงหาคม</asp:ListItem>
                                                                    <asp:ListItem Value="9">กันยายน</asp:ListItem>
                                                                    <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                                    <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                                    <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:Label ID="Label81" CssClass="col-sm-3 control-label" runat="server" Text="ปี : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlyear" CssClass="form-control" runat="server">
                                                                </asp:DropDownList>

                                                            </div>
                                                        </div>

                                                        <div id="div_organization" runat="server" visible="false">

                                                            <div class="form-group">
                                                                <asp:Label ID="Label82" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                                                <div class="col-sm-3">
                                                                    <asp:DropDownList ID="ddlorg_chart" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">กรุณาเลือกองค์กร....</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                </div>

                                                                <asp:Label ID="Label83" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                                                <div class="col-sm-3">
                                                                    <asp:DropDownList ID="ddlrdept_chart" runat="server" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                                        <asp:ListItem Value="0">กรุณาเลือกฝ่าย....</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div id="div_rsecchart" runat="server" visible="false">
                                                                    <asp:Label ID="Label84" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                                                    <div class="col-sm-3">
                                                                        <asp:DropDownList ID="ddlrsec_chart" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">กรุณาเลือกแผนก....</asp:ListItem>
                                                                        </asp:DropDownList>

                                                                    </div>
                                                                </div>

                                                                <div id="div_rposchart" runat="server" visible="false">
                                                                    <asp:Label ID="Label85" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                                                    <div class="col-sm-3">
                                                                        <asp:DropDownList ID="ddlrpos_chart" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">กรุณาเลือกตำแหน่ง....</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </asp:Panel>

                                                    <div class="form-group" id="div_button" runat="server" visible="false">
                                                        <div class="col-sm-4 col-sm-offset-2">
                                                            <asp:LinkButton ID="btnsearch" CssClass="btn btn-success btn-sm" ValidationGroup="SearchReport" runat="server" CommandName="btnsearch_report" OnCommand="btnCommand" title="Search"><i class="fa fa-search"></i> Search</asp:LinkButton>
                                                            <%--                                                    <asp:LinkButton ID="btnexport" CssClass="btn btn-primary  btn-sm" runat="server" CommandName="btnexport" OnCommand="btnCommand" title="Export"><i class="glyphicon glyphicon-export"></i> Export Excel</asp:LinkButton>--%>
                                                            <asp:LinkButton ID="btnrefresh" CssClass="btn btn-info  btn-sm" runat="server" CommandName="btnrefresh_report" OnCommand="btnCommand" title="Refresh"><i class="glyphicon glyphicon-refresh"></i> Refresh</asp:LinkButton>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnsearch" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                </InsertItemTemplate>
                            </asp:FormView>


                        </div>
                    </div>

                    <asp:Panel ID="show_report_table" runat="server" Visible="false">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-heart"></i><strong>&nbsp; รายละเอียดข้อมูล</strong></h3>
                                </div>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <asp:GridView ID="GvReport" runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                            HeaderStyle-CssClass="success small"
                                            HeaderStyle-Height="40px"
                                            ShowFooter="true"
                                            AllowPaging="false"
                                            OnRowDataBound="Master_RowDataBound"
                                            DataKeyNames="u1_docidx">

                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <asp:TemplateField HeaderText="ลำดับที่" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center">

                                                    <ItemTemplate>
                                                        <%# (Container.DataItemIndex +1) %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รายการที่" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbu1_docidx" runat="server" Visible="false" Text='<%# Eval("u1_docidx") %>'></asp:Label>
                                                        <asp:Label ID="lbdoc_code" runat="server" Text='<%# Eval("doc_code") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="วันที่ทำรายการ" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbname" runat="server" Text='<%# Eval("createdate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ข้อมูลผู้ทำรายการ" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <strong>
                                                                <asp:Label ID="Label15" runat="server">องค์กร: </asp:Label></strong>
                                                            <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                            </p>
                                                 <strong>
                                                     <asp:Label ID="Label24" runat="server">ฝ่าย: </asp:Label></strong>
                                                            <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                            </p>
                                                 <strong>
                                                     <asp:Label ID="Label25" runat="server">แผนก: </asp:Label></strong>
                                                            <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                            </p>
                                                           <strong>
                                                               <asp:Label ID="Label19" runat="server">ตำแหน่ง: </asp:Label></strong>
                                                            <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("PosNameTH") %>' />
                                                            </p>
                                                 <strong>
                                                     <asp:Label ID="Label26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                                            <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                                            </p>
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="คุณสมบัติที่ต้องการ" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <strong>
                                                                <asp:Label ID="Labelw24" runat="server">ประเภทการขอคน: </asp:Label></strong>
                                                            <asp:Literal ID="Litderal1" runat="server" Text='<%# Eval("type_list") %>' />
                                                            </p>
                                                 <strong>
                                                     <asp:Label ID="Labedl25" runat="server">สถานที่: </asp:Label></strong>
                                                            <asp:Literal ID="Litefral2" runat="server" Text='<%# Eval("LocName") %>' />
                                                            </p>
                                                         <strong>
                                                             <asp:Label ID="Label42" runat="server">ประเภทพนักงาน: </asp:Label></strong>
                                                            <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("EmpTypeName") %>' />
                                                            </p>
                                                               <strong>
                                                                   <asp:Label ID="Label77" runat="server">จำนวนคน: </asp:Label></strong>
                                                            <asp:Literal ID="Literal12" runat="server" Text='<%# Eval("qty") %>' />
                                                            </p>

                                                               <strong>
                                                                   <asp:Label ID="Label78" runat="server">วันที่ต้องการ: </asp:Label></strong>
                                                            <asp:Literal ID="Literal13" runat="server" Text='<%# Eval("date_receive") %>' />
                                                            </p>
                                                           <strong>
                                                               <asp:Label ID="Lvabel19" runat="server">สาเหตุการขอคน: </asp:Label></strong>
                                                            <asp:Literal ID="Litweral5" runat="server" Text='<%# Eval("reason_comment") %>' />
                                                            </p>
                                                          
                                                        </small>
                                                    </ItemTemplate>

                                                    <FooterTemplate>
                                                        <div style="text-align: right;">
                                                            <p>
                                                                <asp:Literal ID="lit_total_report" runat="server" Text="จำนวนคนรวม :"></asp:Literal>
                                                            </p>
                                                            <p>
                                                                <asp:Literal ID="lit_total_report_uncomplete" runat="server" Text="อยู่ระหว่างดำเนินการ :"></asp:Literal>
                                                            </p>

                                                            <p>
                                                                <asp:Literal ID="Literal14" runat="server" Text="อยู่ระหว่างขั้นตอนอนุมัติ :"></asp:Literal>
                                                            </p>
                                                            <p>
                                                                <asp:Literal ID="lit_total_report_complete" runat="server" Text="จบดำเนินการ :"></asp:Literal>
                                                            </p>
                                                        </div>
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ตำแหน่งที่ต้องการ" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <strong>
                                                                <asp:Label ID="Label151" runat="server">องค์กร: </asp:Label></strong>
                                                            <asp:Literal ID="Literal31" runat="server" Text='<%# Eval("OrgNameTH_Req") %>' />
                                                            </p>
                                                 <strong>
                                                     <asp:Label ID="Label241" runat="server">ฝ่าย: </asp:Label></strong>
                                                            <asp:Literal ID="Literal11" runat="server" Text='<%# Eval("DeptNameTH_Req") %>' />
                                                            </p>
                                                 <strong>
                                                     <asp:Label ID="Label251" runat="server">แผนก: </asp:Label></strong>
                                                            <asp:Literal ID="Literal21" runat="server" Text='<%# Eval("SecNameTH_Req") %>' />
                                                            </p>
                                                           <strong>
                                                               <asp:Label ID="Label191" runat="server">ตำแหน่ง: </asp:Label></strong>
                                                            <asp:Literal ID="Literal51" runat="server" Text='<%# Eval("PosNameTH_Req") %>' />
                                                            </p>

                                                            <hr />

                                                            <asp:GridView ID="GvDetailSex_Report" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                HeaderStyle-CssClass="info small"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="false"
                                                                OnRowDataBound="Master_RowDataBound"
                                                                DataKeyNames="u2_docidx">

                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>


                                                                    <asp:TemplateField HeaderText="เพศ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>

                                                                                <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("sex_name") %>' />
                                                                            </small>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="จำนวน" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("sub_qty") %>' />
                                                                            </small>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="สัญชาติ" ItemStyle-Width="70%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <strong>
                                                                                    <asp:Literal ID="litnatidx" Visible="false" runat="server" Text='<%# Eval("natidx") %>' />
                                                                                    <asp:CheckBoxList ID="chknation" Enabled="false" runat="server" RepeatColumns="3"></asp:CheckBoxList>
                                                                            </small>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>


                                                                </Columns>
                                                            </asp:GridView>

                                                        </small>
                                                    </ItemTemplate>

                                                    <FooterTemplate>
                                                        <div style="text-align: right; background-color: gold;">
                                                            <p>
                                                                <asp:Label ID="lit_total_qty" runat="server" Font-Bold="true"></asp:Label>
                                                            </p>
                                                            <p>
                                                                <asp:Label ID="lit_total_waitpower" runat="server" Font-Bold="true"></asp:Label>
                                                            </p>
                                                            <p>
                                                                <asp:Label ID="lit_total_uncomplete" runat="server" Font-Bold="true"></asp:Label>
                                                            </p>
                                                            <p>
                                                                <asp:Label ID="lit_total_complete" runat="server" Font-Bold="true"></asp:Label>
                                                            </p>
                                                        </div>
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltunidx" Visible="false" runat="server" Text='<%# Eval("unidx") %>' /></small>
                                                    <asp:Literal ID="ltstaidx" Visible="false" runat="server" Text='<%# Eval("staidx") %>' /></small>
                                                          <asp:Label ID="lblstasus" Font-Bold="true" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="show_report_chart" runat="server" Visible="false">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-heart"></i><strong>&nbsp; รายละเอียดข้อมูล</strong></h3>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel_count_employee" runat="server">
                                    <ContentTemplate>
                                        <div class="panel-body">
                                            <div class="form-horizontal" role="form">

                                                <asp:Literal ID="litReportChart" runat="server" />

                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>



        </asp:View>
    </asp:MultiView>


    <script>
        $(function () {
            $('.from-date-datepicker-report').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker-report').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>

        $(function () {
            var string = $(".from-date-datepicker").val();
            //alert(string);
            if (string == '') {
                ////alert(11);
                $('.from-date-datepicker').datetimepicker({

                    minDate: moment().add(+1, 'day'),
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true,

                });

            }
            else {
                //alert(12);
                $('.from-date-datepicker').datetimepicker({

                    minDate: moment().add(+1, 'day'),
                    format: 'DD/MM/YYYY', //$('.datetimepicker-fromsearch').data("DateTimePicker").date(e.date.format("DD/MM/YYYY")),//'DD/MM/YYYY',
                    //ignoreReadonly: true,

                }).val(string);
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                var string = $(".from-date-datepicker").val();
                //alert(string);
                if (string == '') {
                    //alert(1);
                    $('.from-date-datepicker').datetimepicker({

                        minDate: moment().add(+1, 'day'),
                        format: 'DD/MM/YYYY',
                        ignoreReadonly: true,

                    });

                }
                else {
                    //alert(2);
                    $('.from-date-datepicker').datetimepicker({

                        minDate: moment().add(+1, 'day'),
                        format: 'DD/MM/YYYY', //$('.datetimepicker-fromsearch').data("DateTimePicker").date(e.date.format("DD/MM/YYYY")),//'DD/MM/YYYY',
                        //ignoreReadonly: true,

                    }).val(string);
                }
            });
        });
    </script>

    <script src='<%=ResolveUrl("~/Scripts/jquery.MultiFileNew.js")%>'></script>

    <script>
        $(function () {
            $('.UploadFileOrg').MultiFile({

            });

        });
        $(function () {
            $('.UploadFileJD').MultiFile({

            });

        });
        $(function () {
            $('.UploadFileMemo').MultiFile({

            });

        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.UploadFileOrg').MultiFile({

                });

            });

            $(function () {
                $('.UploadFileJD').MultiFile({

                });

            });

            $(function () {
                $('.UploadFileMemo').MultiFile({

                });

            });
        });




    </script>

    <script type="text/javascript">
        function openModal() {
            $('#ordine').modal('show');

        }

    </script>

    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            //var gvname_ = document.getElementById(gvname);
            var printWindow = window.open('', '', 'height=1000,width=800');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            //document.getElementById(gvname).Enable = true;
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            //document.getElementById('gvWork_scroll').Visible = true;
            //return false;

        }
    </script>
</asp:Content>

