﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="websystem_login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MAS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />
    <%--<link rel="shortcut icon" href="./../images/favicontkn.ico" type="image/x-icon" />
    <link rel="icon" href="./../images/favicontkn.ico" type="image/x-icon" />--%>

    <link rel="shortcut icon" href="./../images/Logo_TKN-01.png" type="image/x-icon" />
    <%--<link rel="icon" href="./../images/favicontkn.ico" type="image/x-icon" />--%>
    <link rel="icon" type="image/png" sizes="16x16" href="./../images/Logo_TKN-01.png" />



</head>
<body>
    <form id="formMaster" runat="server">
        <asp:ScriptManager ID="tsmMaster" runat="server"></asp:ScriptManager>
        <script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>

        <script type="text/javascript">
            function openModal() {
                $('#ordine').modal('show');
            }
        </script>

        <script type="text/javascript">
            function openModal_Contract() {
                $('#ordine_contract').modal('show');
            }
        </script>

        <style>
            .bg-template-top {
                background-color: #F1F1F2;
                background-size: 100% 100%;
            }

            .bg-template-top-image {
                background-size: 100% 100%;
                background-color: #F1F1F2;
            }

            .bg-template-center {
                /*background: url('../../masterpage/images/hr/modal_center.png') no-repeat;*/
                background-color: #F1F1F2;
                background-size: 100% 100%;
            }

            .bg-template-center-image {
                background: url('../../masterpage/images/hr/contract_center.png') no-repeat;
                background-size: 100% 100%;
                background-color: #F1F1F2;
            }

            .bg-template-buttom {
                background: url('../../masterpage/images/hr/modal_buttom.png') no-repeat;
                background-size: 100% 100%;
                background-color: #818285;
                height: 50px;
            }

            .stylink {
                font: 22px;
                color: white;
                background-color: #004A97;
            }

            .stylink1 {
                font: 22px;
                color: white;
                background-color: #A7A9AB;
            }
        </style>

        <asp:UpdatePanel ID="upLogin" runat="server">
            <ContentTemplate>
                <asp:Literal ID="litDebug" runat="server"></asp:Literal>
                <div class="container">
                    <div class="row vertical-offset-50">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <img src='<%= ResolveUrl("~/masterpage/images/logo_mas_01.png") %>' class="img-fluid" width="50%" alt="MAS" />
                                </div>
                                <div class="row bg-login display-flex">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control input-lg bg-control" placeholder="Employee Code" MaxLength="8" ValidationGroup="formLogin"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEmpCode" ValidationGroup="formLogin" runat="server" Display="None" SetFocusOnError="true" ControlToValidate="tbEmpCode" ErrorMessage="กรุณากรอกรหัสพนักงาน" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceEmpCode" TargetControlID="rfvEmpCode" HighlightCssClass="validatorCalloutHighlight" />
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="tbEmpPass" runat="server" CssClass="form-control input-lg bg-control" placeholder="Password" MaxLength="20" TextMode="Password" ValidationGroup="formLogin"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEmpPass" ValidationGroup="formLogin" runat="server" Display="None" SetFocusOnError="true" ControlToValidate="tbEmpPass" ErrorMessage="กรุณากรอกรหัสผ่าน" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceEmpPass" TargetControlID="rfvEmpPass" HighlightCssClass="validatorCalloutHighlight" />
                                        </div>

                                        <div class="form-group">
                                            <asp:Button ID="btnLogin" CssClass="btn btn-lg btn-success btn-block" runat="server" data-original-title="Login" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdLogin" Text="Login" ValidationGroup="formLogin" />
                                        </div>

                                        <div class="form-group" style="margin-bottom: 0px;">
                                            <div class="col-xs-7 col-md-6 clearpm">
                                                <asp:LinkButton ID="Button1" CssClass="btn btn-sm pull-left" BackColor="#6D6E70" ForeColor="White" Width="99%" runat="server" data-original-title="Reset Password" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdResetpass" Text="Reset Password" ValidationGroup="formLogin1"><img src='<%= ResolveUrl("~/masterpage/images/hr/button_icon_left.png") %>' class="img-fluid" width="20" /> Reset Password</asp:LinkButton>
                                            </div>
                                            <div class="col-xs-5 col-md-6 clearpm">
                                                <asp:LinkButton ID="Button2" CssClass="btn btn-sm pull-right" BackColor="#939597" ForeColor="White" Width="99%" runat="server" data-original-title="Contract HR" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdContracthr" Text="Contract Hr" ValidationGroup="formLogin2" data-target="#exampleModal"><img src='<%= ResolveUrl("~/masterpage/images/hr/button_icon_right.png") %>' class="img-fluid" width="20" /> Contract Hr</asp:LinkButton>
                                            </div>
                                        </div>


                                        <!-- Modal Reset Password-->
                                        <div id="ordine" class="modal open" role="dialog">
                                            <div class="modal-dialog" style="width: 40%;">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header bg-template-top-image">
                                                        <h3 class="modal-title"></h3>
                                                        <label class="col-sm-12">
                                                            <asp:Image ID="ImageButton1" CssClass="img-responsive container-fluid" Width="80%" Height="80%" ImageUrl="~/masterpage/images/hr/reset_password_head.png" runat="server" />
                                                        </label>
                                                        <center><asp:Label ID="lbltextalert" runat="server"></asp:Label></center>
                                                    </div>
                                                    <div class="modal-body bg-template-center">
                                                        <div class="panel-body">
                                                            <div class="form-group col-sm-12">
                                                                <asp:Label ID="Label2" runat="server" Text="รหัสพนักงาน" ForeColor="#808080" CssClass="col-sm-4 control-label text_right"></asp:Label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txtempcode" CssClass="form-control" MaxLength="8" runat="server" placeholder="Ex. 57000001" ValidationGroup="ChangePass"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RPidcard" ValidationGroup="ChangePass" runat="server" Display="None"
                                                                        ControlToValidate="txtempcode" Font-Size="11"
                                                                        ErrorMessage="รหัสพนักงาน ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallo2utExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RPidcard" Width="160" />
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="***" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-sm-12">
                                                                <asp:Label ID="Label3" runat="server" Text="รหัสบัตรประชาชน" ForeColor="#808080" CssClass="col-sm-4 control-label text_right"></asp:Label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txtidcard" CssClass="form-control" MaxLength="13" runat="server" placeholder="Ex. 1000000000000" ValidationGroup="ChangePass"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RPempcode" ValidationGroup="ChangePass" runat="server" Display="None"
                                                                        ControlToValidate="txtidcard" Font-Size="11"
                                                                        ErrorMessage="รหัสบัตรประชาชน ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Vsal3idlwoer6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RPempcode" Width="160" />
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <asp:Label ID="Label214" runat="server" ForeColor="Red" Text="***" />
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <center><asp:Label ID="Label5" CssClass="center-block" runat="server" ForeColor="#808080" Text="***เมื่อทำรายการสำเร็จ ข้อมูลจะถูกแจ้งเข้า Email ของท่าน" /></center>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <div class="col-sm-12">
                                                                <asp:LinkButton ID="btnupdate" runat="server" CssClass="btn btn-primary col-sm-12 stylink" OnCommand="btnCommand" CommandName="btnChange_Password" ValidationGroup="ChangePass">ยืนยัน &nbsp;</asp:LinkButton>
                                                            </div>
                                                            <br />
                                                            <br />
                                                            <div class="col-sm-12">
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-default col-sm-12 stylink1" OnCommand="btnCommand" CommandName="btnClose_Change_Password">ยกเลิก &nbsp;</asp:LinkButton>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <!-- Modal Contract -->
                                        <div id="ordine_contract" class="modal open" role="dialog">
                                            <div class="modal-dialog" style="width: 40%;">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <%--<div class="modal-header bg-template-top-image">
                                                        <h3 class="modal-title"></h3>
                                                        <label class="col-sm-12">
                                                            <asp:Image ID="Image1" CssClass="img-responsive container-fluid" Width="80%" Height="80%" ImageUrl="~/masterpage/images/hr/contract_center_1.png" runat="server" />
                                                        </label>
                                                    </div>--%>
                                                    <div class="modal-body bg-template-center">
                                                        <div class="panel-body">
                                                            <label class="col-sm-12">
                                                                <asp:Image ID="Image2" CssClass="img-responsive container-fluid" Width="100%" Height="100%" ImageUrl="~/masterpage/images/hr/contract_center_1.png" runat="server" />
                                                            </label>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="col-sm-12">
                                                                <asp:LinkButton ID="LinkButton4" runat="server" CssClass="btn btn-default col-sm-12 stylink1" OnCommand="btnCommand" CommandName="btnClose_Change_Password">ยกเลิก &nbsp;</asp:LinkButton>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div id="divShowError" runat="server" class="alert alert-danger" style="margin-top: 10px;" role="alert">
                                    <strong>Error
                                        <asp:Literal ID="litErrorCode" runat="server"></asp:Literal>
                                        : </strong>
                                    รหัสพนักงาน หรือ รหัสผ่าน ไม่ถูกต้อง
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <div id="modalPopup" runat="server" class="modalPopup">
                        <div class="centerPopup">
                            <asp:Image ID="imgWaiting" ImageUrl="~/masterpage/images/loading.gif" AlternateText="Processing" runat="server" Width="50" Height="50" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
		<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-body">
					<img src='<%= ResolveUrl("~/images/preload/RamaX.jpg") %>' class="img-responsive">
                  
				</div>
			</div>
		  </div>
		</div>
		<script type="text/javascript">
			/*$(window).on('load',function(){
				$('#myModal').modal('show');
			});*/
		</script>
    </form>
    <script type="text/javascript">
            $(document).ready(function () {
                $("body").tooltip({ selector: '[data-toggle=tooltip]' });
            });
    </script>
</body>
</html>
