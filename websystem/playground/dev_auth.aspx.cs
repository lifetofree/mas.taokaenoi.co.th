using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_playground_dev_auth : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_employee _data_employee = new data_employee();

    int _emp_idx = 0;
    int _default_int = 0;
    bool _b_permission = false;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static int[] _permission = { 172, 23069 };
    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = _funcTool.convertToInt(Session["emp_idx"].ToString());

        // check permission
        foreach (int _pass in _permission)
        {
            if (_emp_idx == _pass)
            {
                _b_permission = true;
                continue;
            }
        }

        if (!_b_permission)
        {
            Response.Redirect(ResolveUrl("~/"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
        }

        if (ViewState["auth"] == null)
        {
            setActiveTab("viewAuth", 0);
        }
    }

    #region event command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdAuth":
                // tkn@123*
                if (_funcTool.getMd5Sum(tbPassword.Text.Trim()) == "576bc189c337e250d9af3f5c6f16a9df")
                {
                    ViewState["auth"] = "123456";
                    setActiveTab("viewList", 0);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('incorrect password');", true);
                }
                break;
            case "cmdRelease":
                clearViewState();
                setActiveTab("viewAuth", 0);
                break;
        }
    }
    #endregion event command

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveTab("viewAuth", 0);

        hlSetTotop.Focus();
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        ViewState["auth"] = null;
        // ViewState["VisitorSelected"] = null;
        // ViewState["ReportList"] = null;
    }

    protected void setActiveTab(string activeTab, int doc_idx)
    {
        setActiveView(activeTab, doc_idx);

        switch (activeTab)
        {
            case "viewAuth":
                break;
            case "viewList":
                break;
        }
    }

    protected void setActiveView(string activeTab, int doc_idx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _data_employee)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_employee);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _local_json);

        return _data_employee;
    }
    #endregion reuse
}