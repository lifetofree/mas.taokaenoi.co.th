﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="itswl_m0_softwarename.aspx.cs" Inherits="websystem_MasterData_itswl_m0_softwarename" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <!-- Start View Index Form -->
        <asp:View ID="ViewIndex" runat="server">

            <div class="col-md-12">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server" CommandName="btnToInsert" OnCommand="btnCommand"><i class="fa fa-plus-square"></i> ข้อมูล Software</asp:LinkButton>

                <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="software_name_idx"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtsoftware_name_idx" runat="server" CssClass="form-control"
                                    Visible="False" Text='<%# Eval("software_name_idx")%>' />
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">ชื่อ Software</label>
                                            <asp:UpdatePanel ID="panelNameUpdate" runat="server">
                                                <ContentTemplate>

                                                    <asp:TextBox ID="txtNameUpdate" runat="server" CssClass="form-control"
                                                        Text='<%# Eval("name")%>' />
                                                    <asp:RequiredFieldValidator ID="requiredtxtNameUpdate"
                                                        ValidationGroup="saveSoftwareNameUpdate" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txtNameUpdate"
                                                        Font-Size="1em" ForeColor="Red"
                                                        ErrorMessage="กรุณากรอกชื่อ Software" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">Version</label>
                                            <asp:UpdatePanel ID="panelversionUpdate" runat="server">
                                                <ContentTemplate>

                                                    <asp:TextBox ID="txtversionUpdate" runat="server" CssClass="form-control"
                                                        Text='<%# Eval("version")%>' />
                                                    <asp:RequiredFieldValidator ID="RequiredtxtversionUpdate"
                                                        ValidationGroup="saveSoftwareNameUpdate" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txtversionUpdate"
                                                        Font-Size="1em" ForeColor="Red"
                                                        ErrorMessage="กรุณากรอก Version" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </small>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">ประเภท Software</label>
                                            <asp:Label ID="lbtype_nameUpdat" runat="server" Text='<%# Bind("software_type") %>' Visible="false" />
                                            <asp:DropDownList ID="ddltype_nameUpdat" AutoPostBack="true" runat="server"
                                                CssClass="form-control">
                                            </asp:DropDownList>
                                            <%-- <asp:RequiredFieldValidator ID="Requiredddltype_nameUpdat"
                                                ValidationGroup="saveSoftwareNameUpdate" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="ddltype_nameUpdat"
                                                Font-Size="1em" ForeColor="Red"
                                                ErrorMessage="กรุณากรอกประเภท Software" />
                                            </ContentTemplate>--%>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">สถานะ</label>
                                            <asp:DropDownList ID="ddlStatusUpdate" AutoPostBack="false" runat="server"
                                                CssClass="form-control" SelectedValue='<%# Eval("status") %>'>
                                                <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                            </asp:DropDownList>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                            ValidationGroup="saveSoftwareNameUpdate" CommandName="Update"
                                            OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                         บันทึก
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                            CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                    </div>
                                </div>
                            </EditItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อ Software" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:TextBox ID="txt_software_name_idx" Text='<%# Eval("software_name_idx") %>' Visible="false" runat="server"></asp:TextBox>
                                    <asp:Label ID="Name" runat="server" Text='<%# Eval("name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Version" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Version" runat="server" Text='<%# Eval("version") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภท Software" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Type" runat="server" Text='<%# Eval("type_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียด License" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-CssClass="small">
                            <ItemTemplate>
                                <%--<small>--%>
                                <%-- <asp:Label ID="Count_Software" runat="server" Text='<%# Eval("count_software_license") %>' />--%>



                                <%-- <asp:Label ID="t1" runat="server" Text='<%# Eval("count_software_license") %>' />--%>

                                <asp:Label ID="DetailLicense" Visible="true" runat="server">
                                            <p><b>&nbsp;&nbsp;&nbsp;จำนวน License ทั้งหมด :</b> <%# Eval("count_software_license") %></p> 
                                          
                                </asp:Label>

                                <asp:Label ID="lbsoftware_useview" Visible="true" runat="server">
                                            <p><b>&nbsp;&nbsp;&nbsp;จำนวน License ที่ถูกใช้ไป :</b> <%# Eval("software_use") %></p> 
                                          
                                </asp:Label>

                                <asp:Label ID="lbsoftware_licenseuseassetview" Visible="true" runat="server">
                                            <p><b>&nbsp;&nbsp;&nbsp;จำนวน License ถูกลิขสิทธิ์ที่ใช้ไป :</b> <%# Eval("software_licenseuseasset") %></p> 
                                          
                                </asp:Label>

                                <asp:Label ID="lbsoftware_ghostview" Visible="true" runat="server">
                                            <p><b>&nbsp;&nbsp;&nbsp;จำนวน License ไม่ถูกลิขสิทธิ์ที่ใช้ไป :</b> <%# Eval("software_ghost") %></p> 
                                          
                                </asp:Label>

                                <%--<asp:TextBox ID="txtcount_use" runat="server" ></asp:TextBox>--%>

                                <%--<div class="panel-body">--%>
                                <table class="table table-striped f-s-4">

                                    <asp:Repeater ID="rpcountalllicense" Visible="false" runat="server" OnItemDataBound="rptRecords_ItemDataBound">
                                        <HeaderTemplate>
                                            <tr>
                                                <%--<th>#</th>--%>
                                                <%--<th>จำนวน license ที่ใช้ไป </th> --%>
                                                <%-- <th>ฝ่าย</th>--%>
                                                <%--<th>เลขที่ Asset</th>--%>
                                                <%-- <th>จำนวน License</th>--%>
                                                <%--<th>จำนวน License</th>--%>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <%--<td>
                                                    <asp:LinkButton ID="lbdept" runat="server"></asp:LinkButton>
                                                    <asp:Label ID="lbltemp" Style="border: 1px solid blue; width: 20px; height: 20px; background: green" runat="server" Text="TempLabel"></asp:Label>
                                                </td>--%>

                                                <%--<td data-th="#"><%# (Container.ItemIndex + 1) %></td>--%>
                                                <%-- <td data-th="จำนวน license ที่ใช้ไป"><%# Eval("u0_use") %></td>--%>
                                                <%--<td data-th="#" >--%>
                                                <asp:Label ID="detail_licensecouny_all" runat="server">
                                                   <%-- <asp:Label ID="t2" runat="server" Text='<%# Eval("u0_use") %>' />--%>
                                                    <p><b>&nbsp;&nbsp;&nbsp;จำนวน License ทั้งหมด :</b> <%# Eval("software_count_all") %></p>
                                                    <%--<asp:TextBox ID="test" runat="server"></asp:TextBox>--%>

                                                    <%--<asp:Label ID="Label1" runat="server" />--%>
                                                </asp:Label>
                                                <%-- </td>--%>
                                                <%--<td data-th="ฝ่าย"><%# Eval("DeptNameTH") %></td>
                                                            <td data-th="จำนวน License ทั้งหมก"><%# Eval("count_indept") %></td>--%>
                                                <%--     <td data-th="เลขที่ Asset"><%# Eval("asset_code") %></td>
                                                <td data-th="จำนวน License"><%# Eval("count_asset") %></td>--%>
                                            </tr>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                        </FooterTemplate>
                                    </asp:Repeater>


                                    <asp:Repeater ID="rpcountuselicense" Visible="false" runat="server" OnItemDataBound="rptRecords_ItemDataBound">
                                        <HeaderTemplate>
                                            <tr>
                                                <%--<th>#</th>--%>
                                                <%--<th>จำนวน license ที่ใช้ไป </th> --%>
                                                <%-- <th>ฝ่าย</th>--%>
                                                <%--<th>เลขที่ Asset</th>--%>
                                                <%-- <th>จำนวน License</th>--%>
                                                <%--<th>จำนวน License</th>--%>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <%--<td>
                                                    <asp:LinkButton ID="lbdept" runat="server"></asp:LinkButton>
                                                    <asp:Label ID="lbltemp" Style="border: 1px solid blue; width: 20px; height: 20px; background: green" runat="server" Text="TempLabel"></asp:Label>
                                                </td>--%>

                                                <%--<td data-th="#"><%# (Container.ItemIndex + 1) %></td>--%>
                                                <%-- <td data-th="จำนวน license ที่ใช้ไป"><%# Eval("u0_use") %></td>--%>
                                                <%--<td data-th="#" >--%>
                                                <asp:Label ID="detail_use" runat="server">
                                                   <%-- <asp:Label ID="t2" runat="server" Text='<%# Eval("u0_use") %>' />--%>
                                                    <p><b>&nbsp;&nbsp;&nbsp;จำนวน License ที่ถูกใช้ไป  :</b> <%# Eval("u0_use") %></p>
                                                    <%--<asp:TextBox ID="test" runat="server"></asp:TextBox>--%>

                                                    <%--<asp:Label ID="Label1" runat="server" />--%>
                                                </asp:Label>
                                                <%-- </td>--%>
                                                <%--<td data-th="ฝ่าย"><%# Eval("DeptNameTH") %></td>
                                                            <td data-th="จำนวน License ทั้งหมก"><%# Eval("count_indept") %></td>--%>
                                                <%--     <td data-th="เลขที่ Asset"><%# Eval("asset_code") %></td>
                                                <td data-th="จำนวน License"><%# Eval("count_asset") %></td>--%>
                                            </tr>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <%-- </table>--%>
                                    <%-- </div>--%>

                                    <%--<div class="panel-body">--%>
                                    <%-- <table class="table table-striped f-s-4">--%>
                                    <asp:Repeater ID="rpcountghostlicense" Visible="false" runat="server" OnItemDataBound="rptRecords_ItemDataBound">
                                        <HeaderTemplate>
                                            <tr>
                                                <%--<th>#</th>--%>
                                                <%--<th>จำนวน license ที่ใช้ไป </th> --%>
                                                <%-- <th>ฝ่าย</th>--%>
                                                <%--<th>เลขที่ Asset</th>--%>
                                                <%-- <th>จำนวน License</th>--%>
                                                <%--<th>จำนวน License</th>--%>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <%--<td>
                                                    <asp:LinkButton ID="lbdept" runat="server"></asp:LinkButton>
                                                    <asp:Label ID="lbltemp" Style="border: 1px solid blue; width: 20px; height: 20px; background: green" runat="server" Text="TempLabel"></asp:Label>
                                                </td>--%>

                                                <%--<td data-th="#"><%# (Container.ItemIndex + 1) %></td>--%>
                                                <%-- <td data-th="จำนวน license ที่ใช้ไป"><%# Eval("u0_use") %></td>--%>
                                                <%--<td data-th="#" >--%>
                                                <asp:Label ID="detail_countghost" runat="server">
                                                    <%--<asp:Label ID="t2" runat="server" Text='<%# Eval("u0_ghost") %>' />--%>
                                                    <p><b>&nbsp;&nbsp;&nbsp;จำนวน License ไม่ถูกลิขสิทธิ์ที่ใช้ไป  :</b> <%# Eval("u0_ghost") %></p>
                                                   <%-- <asp:TextBox ID="test" runat="server"></asp:TextBox>--%>

                                                    <%--<asp:Label ID="Label1" runat="server" />--%>
                                                </asp:Label>
                                                <%-- </td>--%>
                                                <%--<td data-th="ฝ่าย"><%# Eval("DeptNameTH") %></td>
                                                            <td data-th="จำนวน License ทั้งหมก"><%# Eval("count_indept") %></td>--%>
                                                <%--     <td data-th="เลขที่ Asset"><%# Eval("asset_code") %></td>
                                                <td data-th="จำนวน License"><%# Eval("count_asset") %></td>--%>
                                            </tr>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                        </FooterTemplate>
                                    </asp:Repeater>

                                    <asp:Repeater ID="rpcountuseoklicense" Visible="false" runat="server" OnItemDataBound="rptRecords_ItemDataBound">
                                        <HeaderTemplate>
                                            <tr>
                                                <%--<th>#</th>--%>
                                                <%--<th>จำนวน license ที่ใช้ไป </th> --%>
                                                <%-- <th>ฝ่าย</th>--%>
                                                <%--<th>เลขที่ Asset</th>--%>
                                                <%-- <th>จำนวน License</th>--%>
                                                <%--<th>จำนวน License</th>--%>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <%--<td>
                                                    <asp:LinkButton ID="lbdept" runat="server"></asp:LinkButton>
                                                    <asp:Label ID="lbltemp" Style="border: 1px solid blue; width: 20px; height: 20px; background: green" runat="server" Text="TempLabel"></asp:Label>
                                                </td>--%>

                                                <%--<td data-th="#"><%# (Container.ItemIndex + 1) %></td>--%>
                                                <%-- <td data-th="จำนวน license ที่ใช้ไป"><%# Eval("u0_use") %></td>--%>
                                                <%--<td data-th="#" >--%>
                                                <asp:Label ID="detail_countuseok" runat="server">
                                                    <%--<asp:Label ID="t2" runat="server" Text='<%# Eval("u0_ghost") %>' />--%>
                                                    <p><b>&nbsp;&nbsp;&nbsp;จำนวน License ถูกลิขสิทธิ์ที่ใช้ไป  :</b> <%# Eval("u0_uselicense") %></p>
                                                   <%-- <asp:TextBox ID="test" runat="server"></asp:TextBox>--%>

                                                    <%--<asp:Label ID="Label1" runat="server" />--%>
                                                </asp:Label>
                                                <%-- </td>--%>
                                                <%--<td data-th="ฝ่าย"><%# Eval("DeptNameTH") %></td>
                                                            <td data-th="จำนวน License ทั้งหมก"><%# Eval("count_indept") %></td>--%>
                                                <%--     <td data-th="เลขที่ Asset"><%# Eval("asset_code") %></td>
                                                <td data-th="จำนวน License"><%# Eval("count_asset") %></td>--%>
                                            </tr>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </table>
                                <%-- </div>--%>


                                <%--<div class="row panel panel-default" visible="true" runat="server" id="panel_detail_software">
                                        <div class="panel panel-info">--%>
                                <%-- <h3 class="panel-heading f-bold"><strong>ประวัติข้อมูล</strong></h3>--%>
                                <%--<div class="panel-heading f-bold">จำนวน License ตามฝ่าย</div>
                                        </div>--%>
                                <%-- <div class="panel-body">--%>
                                <table class="table table-striped f-s-12">
                                    <asp:Repeater ID="rpcountogdept" Visible="false" runat="server">
                                        <HeaderTemplate>
                                            <tr>
                                                <%--<th>#</th>--%>
                                                <th>องค์กร</th>
                                                <th>ฝ่าย</th>
                                                <%--<th>เลขที่ Asset</th>--%>
                                                <th>จำนวน License</th>
                                                <%--<th>จำนวน License</th>--%>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <%--<td>
                                                    <asp:LinkButton ID="lbdept" runat="server"></asp:LinkButton>
                                                    <asp:Label ID="lbltemp" Style="border: 1px solid blue; width: 20px; height: 20px; background: green" runat="server" Text="TempLabel"></asp:Label>
                                                </td>--%>

                                                <%--<td data-th="#"><%# (Container.ItemIndex + 1) %></td>--%>
                                                <td data-th="องค์กร"><%# Eval("OrgNameTH") %></td>
                                                <td data-th="ฝ่าย"><%# Eval("DeptNameTH") %></td>
                                                <td data-th="จำนวน License ทั้งหมก"><%# Eval("count_indept") %></td>
                                                <%--     <td data-th="เลขที่ Asset"><%# Eval("asset_code") %></td>
                                                <td data-th="จำนวน License"><%# Eval("count_asset") %></td>--%>
                                            </tr>



                                        </ItemTemplate>
                                        <FooterTemplate>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </table>
                                <%-- </div>--%>

                                <%--</div>--%>


                                <%--</small>--%>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Status" runat="server" Text='<%# getStatus((int)Eval("status")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("software_name_idx") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>
        <!-- End View Index Form -->

        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">

            <div class="col-md-12">
                <div class="form-group">
                    <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                        CommandName="btnCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                </div>
            </div>


            <div class="col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading f-bold">เพิ่มข้อมูล Software</div>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <div class="form-group">

                                <asp:Label ID="lbtxtSoftwareName" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ Software : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtSoftwareName" CssClass="form-control" placeholder="ชื่อ Software ..." runat="server"></asp:TextBox>

                                    <asp:RequiredFieldValidator ID="RequiredtxtSoftwareName" ValidationGroup="saveSoftware" runat="server" Display="None"
                                        ControlToValidate="txtSoftwareName" Font-Size="11"
                                        ErrorMessage="กรุณากรอกชื่อ Software"
                                        ValidationExpression="กรุณากรอกชื่อ Software" />

                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtxtSoftwareName" Width="160" />

                                </div>

                                <asp:Label ID="lbtxtversion" CssClass="col-sm-2 control-label" runat="server" Text="Version : " />
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtversion" CssClass="form-control" placeholder="Version ..." runat="server"></asp:TextBox>

                                    <asp:RequiredFieldValidator ID="Requiredtxtversion" ValidationGroup="saveSoftware" runat="server" Display="None"
                                        ControlToValidate="txtversion" Font-Size="11"
                                        ErrorMessage="กรุณากรอก Version"
                                        ValidationExpression="กรุณากรอก Version" />

                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtversion" Width="160" />


                                </div>


                            </div>

                            <div class="form-group">

                                <asp:Label ID="lbddltype_name" CssClass="col-sm-2 control-label" runat="server" Text="ประเภท Software : " />
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddltype_name" CssClass="form-control" runat="server">
                                        <%--<asp:ListItem Value="0" Text="กรุณาเลือกประเภท Software" />
                                        <asp:ListItem Value="1" Text="รายปี" />
                                        <asp:ListItem Value="2" Text="ซื้อขาด" />
                                        <asp:ListItem Value="3" Text="freeware" />--%>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="Requiredddltype_name" ValidationGroup="saveSoftware" runat="server" Display="None"
                                        ControlToValidate="ddltype_name" Font-Size="11"
                                        ErrorMessage="กรุณาเลือกประเภท Software"
                                        ValidationExpression="กรุณาเลือกประเภท Software" InitialValue="00" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddltype_name" Width="160" />
                                </div>

                            </div>

                            <div class="form-group">

                                <asp:Label ID="lbddlSoftwareStatus" CssClass="col-sm-2 control-label" runat="server" Text="สถานะ : " />
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlSoftwareStatus" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="1" Text="Online" />
                                        <asp:ListItem Value="0" Text="Offline" />
                                    </asp:DropDownList>

                                </div>

                            </div>

                            <%-------------- บันทึก / ยกเลิก --------------%>
                            <div class="form-group">
                                <%--<div class="col-lg-offset-10 col-sm-2">--%>
                                <div class="col-sm-12">
                                    <div class="pull-right">
                                        <asp:LinkButton ID="btnInsert" CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" ValidationGroup="saveSoftware">บันทึก</asp:LinkButton>

                                        <%--<asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>--%>
                                    </div>
                                </div>

                            </div>
                            <%-------------- บันทึก / ยกเลิก --------------%>
                        </div>
                    </div>
                </div>

            </div>
        </asp:View>
        <!-- End Insert Form -->


    </asp:MultiView>


</asp:Content>

