<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="payment.aspx.cs" Inherits="websystem_payment_payment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <div id="ContentMain_div_list_heading" class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">
                <i class="far fa-user"></i>&nbsp;ข้อมูลพนักงาน
            </h3>
        </div>
        <div class="panel-body">
            <div class="form-horizontal" role="form">
                <table cellspacing="0" id="ContentMain_fvEmpProfile" style="width:100%;border-collapse:collapse;">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <input type="hidden" name="ctl00$ContentMain$fvEmpProfile$hfRdeptIdx"
                                    id="ContentMain_fvEmpProfile_hfRdeptIdx" value="20">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">รหัสพนักงาน :</label>
                                    <div class="col-md-4">
                                        <input name="ctl00$ContentMain$fvEmpProfile$tbEmpCode" type="text"
                                            value="56000088" id="ContentMain_fvEmpProfile_tbEmpCode" disabled="disabled"
                                            class="aspNetDisabled form-control">
                                    </div>
                                    <label class="col-md-2 control-label">ชื่อ-นามสกุล :</label>
                                    <div class="col-md-4">
                                        <input name="ctl00$ContentMain$fvEmpProfile$tbEmpName" type="text"
                                            value="ชลพร จันทรรัตน์" id="ContentMain_fvEmpProfile_tbEmpName"
                                            disabled="disabled" class="aspNetDisabled form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">บริษัท :</label>
                                    <div class="col-md-4">
                                        <input name="ctl00$ContentMain$fvEmpProfile$tbOrgNameTh" type="text"
                                            value="บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด (มหาชน)"
                                            id="ContentMain_fvEmpProfile_tbOrgNameTh" disabled="disabled"
                                            class="aspNetDisabled form-control">
                                    </div>
                                    <label class="col-md-2 control-label">ฝ่าย :</label>
                                    <div class="col-md-4">
                                        <input name="ctl00$ContentMain$fvEmpProfile$tbDeptNameTh" type="text"
                                            value="การจัดการระบบสารสนเทศ" id="ContentMain_fvEmpProfile_tbDeptNameTh"
                                            disabled="disabled" class="aspNetDisabled form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">แผนก :</label>
                                    <div class="col-md-4">
                                        <input name="ctl00$ContentMain$fvEmpProfile$tbSecNameTh" type="text"
                                            value="พัฒนาแอพพลิเคชั่น" id="ContentMain_fvEmpProfile_tbSecNameTh"
                                            disabled="disabled" class="aspNetDisabled form-control">
                                    </div>
                                    <label class="col-md-2 control-label">Cost Center :</label>
                                    <div class="col-md-4">
                                        <input name="ctl00$ContentMain$fvEmpProfile$tbPosNameTh" type="text"
                                            value="106100" id="ContentMain_fvEmpProfile_tbPosNameTh" disabled="disabled"
                                            class="aspNetDisabled form-control">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <table class="table table-striped table-bordered table-responsive" cellspacing="0" rules="all" border="1"
        id="ContentMain_gvFileList" style="border-collapse:collapse;">
        <tbody>
            <tr class="info" style="font-size:Small;">
                <th scope="col">ลำดับที่</th>
                <th scope="col">รายการ</th>
                <th scope="col">ทดรองจ่าย</th>
                <th scope="col">ยอดเคลียร์เงิน</th>
                <th scope="col">###</th>
            </tr>
            <tr style="font-size:Small;">
                <td>
                    1</span>
                </td>
                <td>
                    test
                </td>
                <td>
                    500
                </td>
                <td>
                    -
                </td>
            </tr>
            <tr style="font-size:Small;">
                <td>
                    <span id="ContentMain_gvFileList_lblNo_0"></span>
                </td>
                <td>
                    <input name="ctl00$ContentMain$fvEmpProfile$tbDeptNameTh1" type="text"
                        id="ContentMain_fvEmpProfile_tbDeptNameTh" class="aspNetDisabled form-control">
                </td>
                <td>
                    <input name="ctl00$ContentMain$fvEmpProfile$tbDeptNameTh2" type="text"
                        id="ContentMain_fvEmpProfile_tbDeptNameTh" class="aspNetDisabled form-control">
                </td>
                <td>
                    <input name="ctl00$ContentMain$fvEmpProfile$tbDeptNameTh3" type="text"
                        id="ContentMain_fvEmpProfile_tbDeptNameTh" class="aspNetDisabled form-control">
                </td>
                <td>
                    <a id="ContentMain_fvUploadFiles_lbUpload" class="btn btn-sm btn-success"
                        href="javascript:__doPostBack('ctl00$ContentMain$fvUploadFiles$lbUpload','')"><i
                            class="fas fa-upload"></i>&nbsp;add
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- <div class="form-group">
        <div class="col-md-12">
            <a id="ContentMain_fvUploadFiles_lbUpload" class="btn btn-sm btn-success"
                href="javascript:__doPostBack('ctl00$ContentMain$fvUploadFiles$lbUpload','')"><i
                    class="fas fa-upload"></i>&nbsp;add
            </a>
        </div>
    </div>
    <br /><br />
    <table class="table table-striped table-bordered table-responsive" cellspacing="0" rules="all" border="1"
        id="ContentMain_gvFileList" style="border-collapse:collapse;" width="100%">
        <tbody>
            <tr class="info" style="font-size:Small;">
                <th scope="col">ลำดับที่</th>
                <th scope="col">รายการ</th>
                <th scope="col">ทดรองจ่าย</th>
                <th scope="col">ยอดเคลียร์เงิน</th>
            </tr>
            <tr style="font-size:Small;">
                <td>
                    1</span>
                </td>
                <td>
                    test
                </td>
                <td>
                    500
                </td>
                <td>
                    -
                </td>
            </tr>
        </tbody>
    </table> -->
    <div class="form-group">
        <label class="col-md-2 col-md-offset-3 control-label">File :<span class="text-danger">*</span></label>
        <div class="col-md-4">
            <div class="MultiFile-wrap" id="fuUpload_wrap"><input type="file"
                    name="ctl00$ContentMain$fvUploadFiles$fuUpload" id="fuUpload"
                    class="btn btn-md multi max-1 accept-pdf MultiFile-applied" autopostback="true" value="">
                <div class="MultiFile-list" id="fuUpload_wrap_list"></div>
            </div>
            <span class="text-danger">*เฉพาะไฟล์ pdf เท่านั้น</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 col-md-offset-3 control-label">หมายเหตุ :<span class="text-danger">*</span></label>
        <div class="col-md-4">
            <input name="ctl00$ContentMain$fvEmpProfile$tbDeptNameTh4" type="text"
                        id="ContentMain_fvEmpProfile_tbDeptNameTh" class="aspNetDisabled form-control">
        </div>
    </div>
    <div class="form-group" style="margin-bottom: 10px;">
        <label class="col-md-2 col-md-offset-3 control-label"></label>
        <div class="col-md-4">
            <a id="ContentMain_fvUploadFiles_lbUpload" class="btn btn-sm btn-success"
                href="javascript:__doPostBack('ctl00$ContentMain$fvUploadFiles$lbUpload','')"><i
                    class="fas fa-upload"></i>&nbsp;Save
            </a>
            <a id="ContentMain_fvUploadFiles_lbCancel" class="btn btn-sm btn-danger"
                href="javascript:__doPostBack('ctl00$ContentMain$fvUploadFiles$lbCancel','')"><i
                    class="fas fa-times"></i>&nbsp;Cancel
            </a>
        </div>
    </div>
</asp:Content>