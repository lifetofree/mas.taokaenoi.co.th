﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_ITServices_it_asset_print_org : System.Web.UI.Page
{
    function_tool _funcTool = new function_tool();
    function_dmu _func_dmu = new function_dmu();
    service_execute serviceexcute = new service_execute();
    service_mail servicemail = new service_mail();
    data_itasset _dtitseet = new data_itasset();

    string _localJson = "";
    int _tempInt = 0;
    int _tempcounter = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //its_u_document
    static string _urlGetits_u_document = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_u_document"];

    protected void Page_Load(object sender, EventArgs e)
    {
        //int u0_docket_idx = 0;
        //try
        //{
        //    string su0_docket_idx = Request.QueryString["u0_docket_idx"].ToString().Trim();
        //    u0_docket_idx = int.Parse(su0_docket_idx);
        //}
        //catch
        //{

        //}
        //if (u0_docket_idx > 0)
        //{
        //    showdata_print_detail(7);
        //}
        if (!IsPostBack)
        {
            showdata_print_detail(7);
        }
    }
    private void showdata_print_detail(int id)
    {
        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.u0_docket_idx = id;
        select_its.operation_status_id = "docket_u0_org";
        _dtitseet.its_u_document_action[0] = select_its;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        CreateDsits_u1_document();

        if(_dtitseet.its_u_document_action != null)
        {
            int ic = 0;
            DataSet dsContacts = (DataSet)ViewState["vsits_print_org"];
            foreach (var item in _dtitseet.its_u_document_action)
            {
                DataRow drContacts = dsContacts.Tables["dsits_print_org"].NewRow();
                ic++;
                drContacts["id"] = ic;
                drContacts["u0_docket_idx"] = item.u0_docket_idx;
                drContacts["org_idx_its"] = item.org_idx_its;
                drContacts["org_name_th"] = item.org_name_th;
                drContacts["file_name"] = ic.ToString()+"_"+ DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                dsContacts.Tables["dsits_print_org"].Rows.Add(drContacts);

                

            }
            ViewState["vsits_print_org"] = dsContacts;

        }
        
        _func_dmu.zSetGridData(gvlist_org, ViewState["vsits_print_org"]);
      

    }

    protected void CreateDsits_u1_document()
    {
        string sDs = "dsits_print_org";
        string sVs = "vsits_print_org";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);

        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_docket_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("org_idx_its", typeof(String));
        ds.Tables[sDs].Columns.Add("org_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("file_name", typeof(String));

        ViewState[sVs] = ds;

    }

    protected data_itasset callServicePostITAsset(string _cmdUrl, data_itasset _dtitseet)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtitseet);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtitseet = (data_itasset)_funcTool.convertJsonToObject(typeof(data_itasset), _localJson);




        return _dtitseet;
    }

    protected void btnExportPDF1_Click(object sender, EventArgs e)
    {

        string pageurl = "it-asset-print-org-detail";
        Response.Write("<script> window.open('" + pageurl + "',''); </script>");
    }

}