﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{

   public void ProcessRequest(HttpContext context)
   {
      string callback = context.Request.QueryString["callback"];
      int announceDiaryIdx = 0;
      int.TryParse(context.Request.QueryString["announceDiaryIdx"], out announceDiaryIdx);
      string json = this.getEmployeeDiaryByAnnounceDiaryIdx(announceDiaryIdx);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string getEmployeeDiaryByAnnounceDiaryIdx(int announceDiaryIdx)
   {
      List<object> emps = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            cmd.CommandText = "SELECT u2_announce_diary.announce_diary_emp_idx_ref, view_emp.EmpCode, view_emp.FullNameTH FROM emps_u0_announce_diary AS u0_announce_diary INNER JOIN emps_u2_announce_diary AS u2_announce_diary ON u0_announce_diary.u0_announce_diary_idx = u2_announce_diary.u0_announce_diary_idx_ref INNER JOIN [Centralized].[dbo].[ViewEmployee] AS view_emp ON u2_announce_diary.announce_diary_emp_idx_ref = view_emp.EmpIDX WHERE u0_announce_diary.u0_announce_diary_idx = @announceDiaryIdx AND u2_announce_diary.announce_diary_status = 1 GROUP BY u2_announce_diary.announce_diary_emp_idx_ref, view_emp.FullNameTH, view_emp.EmpCode ORDER BY view_emp.EmpCode ASC";
            cmd.Parameters.AddWithValue("@announceDiaryIdx", announceDiaryIdx);
            cmd.Connection = conn;
            conn.Open();
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
               while (sdr.Read())
               {
                  emps.Add(new
                  {
                     empIdx = sdr["announce_diary_emp_idx_ref"],
                     empCode = sdr["EmpCode"],
                     fullNameTH = sdr["FullNameTH"]
                  });
               }
            }
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emps));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}


