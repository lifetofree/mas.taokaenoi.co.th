﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class websystem_employee_recruit_vdo : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();

    function_tool _funcTool = new function_tool();

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    data_employee_recruit data_employee_recruit = new data_employee_recruit();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetVDOuploadPersonal = _serviceUrl + ConfigurationManager.AppSettings["urlGetVDOuploadPersonal"];
    static string _urlsetVDOuploadPersonal = _serviceUrl + ConfigurationManager.AppSettings["urlSetVDOuploadPersonal"];


    #endregion



    protected void Page_InIt(object sender, EventArgs e)
    {


    }

    protected void Page_Load(object sender, EventArgs e)
    {
        LinkButton btn_uploads = (LinkButton)FvuploadVDO.FindControl("btn_upload");
        string jscript = "function FileUploadPostBack(){" + ClientScript.GetPostBackEventReference(btn_uploads, "") + "};";
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Key", jscript, true);

        //vdo_src.Src = "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4";
        //vdo_src.Src = "";
        if (!Page.IsPostBack)
        {

            initPage();
            Session["base64vdo"] = null;
            //ViewState["base64_vdo"] = string.Empty;




        }

    }
    protected void initPage()
    {
        string view_page = string.Empty;
        ViewState["EmpIDX"] = string.Empty;
        ViewState["vdo_name"] = string.Empty;
        ViewState["data_emp"] = null;
        if (Request.QueryString["u"] != null || Request.QueryString["k"] != null || Request.QueryString["d"] != null)
        {
            int data_now_totime = _funcTool.convertToInt(DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            string key_code = "TKNRecruit"; // key ในการเข้ารหัส
            string date_end = Request.QueryString["d"].ToString().Trim(); // วันที่ ที่สามารถให้เข้ามาได้           
            string user_id = Request.QueryString["u"].ToString().Trim(); // EmpIDX
            string sum_check = Request.QueryString["k"].ToString().Trim(); // sum check
            string sum_md = MD5Hash(user_id + date_end + key_code).Trim();
            if (sum_md == sum_check && data_now_totime >= _funcTool.convertToInt(date_end) && user_id != string.Empty)
            {
                employee_vdo employee_vdo_detail = new employee_vdo();
                data_employee_recruit.employee_vdo_list = new employee_vdo[1];
                employee_vdo_detail.EmpIDX = _funcTool.convertToInt(user_id.ToString());

                data_employee_recruit.employee_vdo_list[0] = employee_vdo_detail;
                data_employee_recruit = callServicePostAPI(_urlGetVDOuploadPersonal, data_employee_recruit);
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_employee_recruit));
                if (data_employee_recruit.return_code == "0")
                {
                   
                    ViewState["data_emp"] = data_employee_recruit.employee_vdo_list;
                    ViewState["EmpIDX"] = data_employee_recruit.employee_vdo_list[0].EmpIDX.ToString();
                    ViewState["vdo_name"] = data_employee_recruit.employee_vdo_list[0].vdo_name.ToString();
                    view_page = "viewIndex";

                    if (data_employee_recruit.employee_vdo_list[0].vdo_d_edit.ToString() == "1")
                    {
                        view_page = "viewreadonly";
                    }

                }

            }


        }

        if (view_page == string.Empty)
        {
            //string key_code = "TKNRecruit";
            //string date_end = DateTime.Now.AddDays(10).ToString("yyyyMMddHHmmssffff");

            //string user_id = "293";
            //string sum_md = MD5Hash(user_id + date_end + key_code);
            //litDebug.Text = date_end;
            //litDebug.Text += "<br>";
            //litDebug.Text += user_id;
            //litDebug.Text += "<br>";
            //litDebug.Text += sum_md;
    
            setActiveView("viewerror");
        }
        else
        {
            setActiveView(view_page);
        }

    }
    public static string MD5Hash(string input)
    {
        StringBuilder hash = new StringBuilder();
        MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
        byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

        for (int i = 0; i < bytes.Length; i++)
        {
            hash.Append(bytes[i].ToString("x2"));
        }
        return hash.ToString();
    }
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        string empidx = string.Empty;
        switch (cmdName)
        {
            #region btn_upload
            case "btn_upload":
                FormView fvname = FvuploadVDO;
                FileUpload FileUpload1 = (FileUpload)fvname.FindControl("upload");
                Label txtupload = (Label)fvname.FindControl("txtupload");

                LinkButton btn_save = (LinkButton)fvname.FindControl("btn_save");
                RegularExpressionValidator rvaupload = (RegularExpressionValidator)fvname.FindControl("rvaupload");
                CustomValidator cusv_upload = (CustomValidator)fvname.FindControl("cusv_upload");
                vdo_src.Src = "";
                btn_save.Visible = false;
                box_video_preview.Visible = false;
                rvaupload.IsValid = false;
                cusv_upload.IsValid = false;
                Session["base64vdo"] = string.Empty;
                if (FileUpload1.HasFile)
                {
                    string dd = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                    string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                    string extension = Path.GetExtension(FileUpload1.PostedFile.FileName);
                    string directoryName = Path.GetDirectoryName(FileUpload1.PostedFile.FileName);
                    string temp_name = dd + FileName;
                    txtnamevdo.InnerHtml = FileName.ToString();
                    int max_length_file = 15;
                    decimal b_mb = max_length_file * 1048576;


                    if (extension.ToLower() == ".mp4" || extension.ToLower() == ".m4v")
                    {
                        rvaupload.IsValid = true;
                    }
                    if (b_mb >= FileUpload1.PostedFile.ContentLength)
                    {
                        cusv_upload.IsValid = true;
                    }

                    if (cusv_upload.IsValid && rvaupload.IsValid)
                    {
                         empidx = ViewState["EmpIDX"].ToString();
                        string folderPath_v = ConfigurationManager.AppSettings["path_hr_recuit_vdo_upload"];
                        string FilePath_v = System.Web.Hosting.HostingEnvironment.MapPath(folderPath_v) + @"temp\" + empidx;

                        System.IO.Stream fs = FileUpload1.PostedFile.InputStream;
                        System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                        string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);

                        Session["base64vdo"] = "data:video/mp4;base64," + base64String;
                        string path = FilePath_v;

                        if (Directory.Exists(path))
                        {
                            Directory.Delete(path, true);
                        }
                        DirectoryInfo di = Directory.CreateDirectory(path);
                        string filePath_m = Server.MapPath(folderPath_v + @"\temp\" + empidx + @"\" + temp_name);

                        FileUpload1.SaveAs(filePath_m);
                        vdo_src.Src = "/mas.taokaenoi.co.th/uploadfiles/vdo_recuit/temp/" + empidx + "/" + temp_name;
                        box_video_preview.Visible = true;
                        btn_save.Visible = true;
                    }

                }
                break;
            #endregion btn_upload
            #region btn_save
            case "btn_save":
                employee_vdo[] data_emp = (employee_vdo[])ViewState["data_emp"];
                string emp_idx = ViewState["EmpIDX"].ToString();
                string file_name = upload_data(emp_idx);
                string folderPath = ConfigurationManager.AppSettings["path_hr_recuit_vdo_upload"];
                string FilePath = System.Web.Hosting.HostingEnvironment.MapPath(folderPath) + @"\";
                string old_vdo = ViewState["vdo_name"].ToString();
                string FullFilePath = FilePath + old_vdo;
                ViewState["vdo_name"] = file_name;
                employee_vdo employee_vdo_detail = new employee_vdo();
                data_employee_recruit.employee_vdo_list = new employee_vdo[1];
                employee_vdo_detail.EmpIDX = _funcTool.convertToInt(emp_idx);
                employee_vdo_detail.vdo_name = file_name;

                employee_vdo_detail.unidx = _funcTool.convertToInt(data_emp[0].unidx.ToString());
                employee_vdo_detail.acidx = _funcTool.convertToInt(data_emp[0].acidx.ToString());
                employee_vdo_detail.doc_decision = _funcTool.convertToInt(data_emp[0].doc_decision.ToString());

                data_employee_recruit.employee_vdo_list[0] = employee_vdo_detail;
                // litDebug.Text = old_vdo;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_employee_recruit));
                data_employee_recruit = callServicePostAPI(_urlsetVDOuploadPersonal, data_employee_recruit);


                if (data_employee_recruit.return_code != "0")
                {
                    //  litDebug.Text = data_employee_recruit.return_msg;
                }
                string temp_path = FilePath + @"temp\" + emp_idx;

                if (Directory.Exists(temp_path))
                {
                    Directory.Delete(temp_path, true);
                }
                setActiveView("viewreadonly");
                break;
            #endregion btn_save

            case "backTomain":
                 empidx = ViewState["EmpIDX"].ToString();
                string key_code = "tkn_toedit_recuit";
                string url_ = "~/hr-employee-register-v2?pageview=editview&idx=" + empidx+"&sum="+_funcTool.getMd5Sum(key_code+ empidx);
                Response.Redirect(url_);
                break;
        }
    }
    string upload_data(string id_user)
    {
        string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
        FormView fvname = FvuploadVDO;
        string data = Session["base64vdo"].ToString();

        string FileName = string.Empty;
        if (data != string.Empty)
        {
            string base64data = data.Replace("data:video/mp4;base64,", "");
            byte[] imageBytes = Convert.FromBase64String(base64data);

            string folderPath = ConfigurationManager.AppSettings["path_hr_recuit_vdo_upload"];
            string unq_folder = @"\" + id_user + @"\";
            string FilePath = System.Web.Hosting.HostingEnvironment.MapPath(folderPath) + unq_folder;
            int num = 0;
            FileName = id_user + "_" + datetimeNow + num + ".mp4";

            string FullFilePath = folderPath + unq_folder + FileName;


            //while (File.Exists(FullFilePath))
            //{
            //    num++;
            //    FileName = id_user + "_" + datetimeNow + num + ".mp4";
            //    FilePath = System.Web.Hosting.HostingEnvironment.MapPath(folderPath) + "\\";
            //    FullFilePath = FilePath + FileName;
            //}

            string path = FilePath;

            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
            DirectoryInfo di = Directory.CreateDirectory(path);
            string filePath_m = Server.MapPath(FullFilePath);


            File.WriteAllBytes(filePath_m, imageBytes);


        }
        return FileName;
        //}
    }
    protected void setActiveView(string activeTab, int doc_idx = 0)
    {
        MvMaster.SetActiveView((View)MvMaster.FindControl(activeTab));
        employee_vdo[] data_emp = (employee_vdo[])ViewState["data_emp"];

        b_backTomain.Visible = false;

        switch (activeTab)
        {
            case "viewIndex":
                title_name.InnerText = data_emp[0].FirstNameTH.ToString() + " " + data_emp[0].LastNameTH.ToString();
                b_backTomain.Visible = true;
                break;
            case "view_tnkso":
                break;
            case "viewreadonly":
                b_backTomain.Visible = true;
                title_readonly.InnerText = data_emp[0].FirstNameTH.ToString() + " " + data_emp[0].LastNameTH.ToString();
                if (ViewState["vdo_name"].ToString() != string.Empty)
                {
                    //string folderPath = ConfigurationManager.AppSettings["path_hr_recuit_vdo_upload"];
                    //string FilePath = System.Web.Hosting.HostingEnvironment.MapPath(folderPath);
                    //vdo_src.Src = FilePath + ViewState["vdo_name"].ToString();
                    vdo_readonly.Src = "/mas.taokaenoi.co.th/uploadfiles/vdo_recuit/" + ViewState["EmpIDX"] + "/" + ViewState["vdo_name"].ToString();
                    //box_video_preview.Visible = true;
                }
                break;
        }
    }

    protected data_employee_recruit callServicePostAPI(string _cmdUrl, data_employee_recruit _data)
    {
        _localJson = _funcTool.convertObjectToJson(_data);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data = (data_employee_recruit)_funcTool.convertJsonToObject(typeof(data_employee_recruit), _localJson);


        return _data;
    }
}