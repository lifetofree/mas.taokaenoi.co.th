﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_ITServices_CHR_Approve : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    // DataSupportIT _dtsupport = new DataSupportIT();
    data_chr _dtchr = new data_chr();
    data_employee _dtEmployee = new data_employee();
    data_employee _dataEmployee = new data_employee();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _ret_val = "";
    string _localJson = "";
    string dept = null;
    string[] deptemp = null;
    string filename = null;
    string[] filename_ = null;
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    static string urlSelect_CHR_EmpCreate = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_CHR_EmpCreate"];
    static string urlSelect_CHR_ApproveEmpCreate = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_CHR_ApproveEmpCreate"];
    static string urlSelect_CHR_CEO = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_CHR_CEO"];
    static string urlSelect_CHR_Quit = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_CHR_Quit"];
    static string urlSelect_CHR_Examiner = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_CHR_Examiner"];
    static string urlSelect_CHR_Assign = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_CHR_Assign"];
    static string urlSelect_U0IDX = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_U0IDX"];
    static string urlUpdate_Approver = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Approver"];
    static string urlUpdate_ApproveExam_User = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_ApproveExam_User"];
    static string urlComment_Reject = _serviceUrl + ConfigurationManager.AppSettings["urlComment_Reject"];





    // string url = "http://www.taokaenoi.co.th/MAS/CHR";

    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack != true)
        {
            ViewState["EmpIDX"] =  int.Parse(Session["emp_idx"].ToString());
            ViewState["rtCode_Assign"] = 0;
            ViewState["rtCode_EmpAssign"] = 0;
            select_empIdx_present();
            SelectList();
            SelectAssignList();
            SetDefault();

            //  Select_SetDefault();

            dept = ViewState["rtCode_rdepidx"].ToString();
            deptemp = dept.Split(',');
            foreach (string rtdept in deptemp)
            {

                if (rtdept != String.Empty)
                {
                    ViewState["rtdept"] = rtdept;


                    if (int.Parse(ViewState["rtCode_empidx"].ToString()) == int.Parse(ViewState["EmpIDX"].ToString()) && int.Parse(ViewState["EmpIDX"].ToString()) != 174)
                    {
                        //กรณีโชว์เฉพาะคนสร้าง
                        SelectApproveEmpIDXCreate();
                        // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('1');", true);
                    }
                    else if (int.Parse(ViewState["rtCode_empidx"].ToString()) == 0 && int.Parse(ViewState["rtdept"].ToString()) == int.Parse(ViewState["rdept_idx"].ToString()) &&
                        int.Parse(ViewState["EmpIDX"].ToString()) != 174 && int.Parse(ViewState["EmpIDX"].ToString()) != 1423)
                    {
                        // กรณีแสดงเฉพาะคนลาออกต้องการคน Approve แทน และไม่มีเอกสารของตัวเอง
                        SelectApproveSameEmpIDXCreate();
                        // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('2');", true);


                    }

                    else
                    {
                        if (int.Parse(ViewState["EmpIDX"].ToString()) == 174 || int.Parse(ViewState["rtCode_empidx"].ToString()) == 174 || int.Parse(ViewState["EmpIDX"].ToString()) == 1423)
                        {
                            SelectApproveExaminer();
                        }
                        else if (int.Parse(ViewState["EmpIDX"].ToString()) == 3639 || int.Parse(ViewState["EmpIDX"].ToString()) == 3640 || int.Parse(ViewState["EmpIDX"].ToString()) == 3641 || int.Parse(ViewState["EmpIDX"].ToString()) == 3631
                            || ViewState["rtCode_Assign"].ToString() == "1" && int.Parse(ViewState["EmpIDX"].ToString()) == int.Parse(ViewState["rtCode_EmpAssign"].ToString()))
                        {
                            SelectApproveCEO();
                        }
                    }
                }
                else
                {
                    if (int.Parse(ViewState["rtCode_empidx"].ToString()) == int.Parse(ViewState["EmpIDX"].ToString()) && int.Parse(ViewState["EmpIDX"].ToString()) != 174)
                    {
                        //กรณีโชว์เฉพาะคนสร้าง
                        SelectApproveEmpIDXCreate();
                        // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('1');", true);
                    }
                    else if (int.Parse(ViewState["EmpIDX"].ToString()) == 174 || int.Parse(ViewState["rtCode_empidx"].ToString()) == 174 || int.Parse(ViewState["EmpIDX"].ToString()) == 1423)
                    {
                        SelectApproveExaminer();
                    }
                    else if (int.Parse(ViewState["EmpIDX"].ToString()) == 3639 || int.Parse(ViewState["EmpIDX"].ToString()) == 3640 || int.Parse(ViewState["EmpIDX"].ToString()) == 3641 || int.Parse(ViewState["EmpIDX"].ToString()) == 3631 || ViewState["rtCode_Assign"].ToString() == "1" && int.Parse(ViewState["EmpIDX"].ToString()) == int.Parse(ViewState["rtCode_EmpAssign"].ToString()))
                    {
                        SelectApproveCEO();

                    }
                    else
                    {
                        SelectApproveSameEmpIDXCreate();
                    }
                }
            }




        }


        // Page.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }
    #endregion

    #region CallService

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    protected data_chr callServicePostCHR(string _cmdUrl, data_chr _dtchr)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtchr);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtchr = (data_chr)_funcTool.convertJsonToObject(typeof(data_chr), _localJson);




        return _dtchr;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }


    #endregion

    #region Trigger
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    #endregion

    #region Select

    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void SelectList()
    {
        _dtchr = new data_chr();

        _dtchr.BindData_U0Document = new BindData[1];
        BindData select = new BindData();

        select.CEmpidx = int.Parse(ViewState["EmpIDX"].ToString());
        _dtchr.BindData_U0Document[0] = select;

        //   text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));
        //string _localJson1 = _funcTool.convertObjectToJson(_dtchr);
        // text.Text = _localJson1;

        _dtchr = callServicePostCHR(urlSelect_CHR_EmpCreate, _dtchr);

        ViewState["rtCode_empidx"] = _dtchr.ReturnCode;
        ViewState["rtCode_rdepidx"] = _dtchr.ReturnTemp;

    }

    protected void SelectAssignList()
    {
        _dtchr = new data_chr();

        _dtchr.BindData_U0Document = new BindData[1];
        BindData select = new BindData();

        select.CEmpidx = int.Parse(ViewState["EmpIDX"].ToString());
        _dtchr.BindData_U0Document[0] = select;

        _dtchr = callServicePostCHR(urlSelect_CHR_Assign, _dtchr);
        ViewState["rtCode_Assign"] = _dtchr.ReturnCode;
        ViewState["rtCode_EmpAssign"] = _dtchr.ReturnIDX;

    }

    protected void SelectApproveEmpIDXCreate()
    {
        _dtchr = new data_chr();

        _dtchr.BindData_U0Document = new BindData[1];
        BindData select = new BindData();

        select.CEmpidx = int.Parse(ViewState["EmpIDX"].ToString());

        _dtchr.BindData_U0Document[0] = select;

        _dtchr = callServicePostCHR(urlSelect_CHR_ApproveEmpCreate, _dtchr);

        setGridData(GvApprove, _dtchr.BindData_U0Document);

    }

    protected void SelectApproveSameEmpIDXCreate()
    {
        _dtchr = new data_chr();

        _dtchr.BindData_U0Document = new BindData[1];
        BindData select = new BindData();

        select.CEmpidx = int.Parse(ViewState["EmpIDX"].ToString());
        select.rdept_idx = int.Parse(ViewState["rdept_idx"].ToString());

        _dtchr.BindData_U0Document[0] = select;

        _dtchr = callServicePostCHR(urlSelect_CHR_Quit, _dtchr);

        setGridData(GvApprove, _dtchr.BindData_U0Document);
    }

    protected void SelectApproveExaminer()
    {
        _dtchr = new data_chr();

        _dtchr.BindData_U0Document = new BindData[1];
        BindData select = new BindData();

        select.CEmpidx = int.Parse(ViewState["EmpIDX"].ToString());

        _dtchr.BindData_U0Document[0] = select;

        _dtchr = callServicePostCHR(urlSelect_CHR_Examiner, _dtchr);

        setGridData(GvApprove, _dtchr.BindData_U0Document);
    }

    protected void SelectApproveCEO()
    {
        _dtchr = new data_chr();

        _dtchr.BindData_U0Document = new BindData[1];
        BindData select = new BindData();

        select.CEmpidx = int.Parse(ViewState["EmpIDX"].ToString());

        _dtchr.BindData_U0Document[0] = select;

        _dtchr = callServicePostCHR(urlSelect_CHR_CEO, _dtchr);

        setGridData(GvApprove, _dtchr.BindData_U0Document);
    }

    protected void Select_U0IDX()
    {


        _dtchr = new data_chr();

        _dtchr.BindData_U0Document = new BindData[1];
        BindData select = new BindData();

        select.u0idx = int.Parse(ViewState["u0idx"].ToString());

        _dtchr.BindData_U0Document[0] = select;
       // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));
        _dtchr = callServicePostCHR(urlSelect_U0IDX, _dtchr);

        setFormViewData(FvDetailUser, _dtchr.BindData_U0Document);

        ViewState["DocCode"] = _dtchr.BindData_U0Document[0].doc_code;

        Label lblnewsys = (Label)FvDetailUser.FindControl("lblnewsys");
        Label lbloldsys = (Label)FvDetailUser.FindControl("lbloldsys");
        Label lblcomment = (Label)FvDetailUser.FindControl("lblcomment");
        LinkButton lbloldmore = (LinkButton)FvDetailUser.FindControl("lbloldmore");
        LinkButton lblnewmore = (LinkButton)FvDetailUser.FindControl("lblnewmore");
        LinkButton lblcommentmore = (LinkButton)FvDetailUser.FindControl("lblcommentmore");
        Control div_comment = (Control)FvDetailUser.FindControl("div_comment");
        Label lblappok1 = (Label)FvDetailUser.FindControl("lblappok1");
        Label lblappok2 = (Label)FvDetailUser.FindControl("lblappok2");
        Label lblappok3 = (Label)FvDetailUser.FindControl("lblappok3");
        Label lblappokabout = (Label)FvDetailUser.FindControl("lblappokabout");

        LinkButton lbtcomment1show = (LinkButton)FvDetailUser.FindControl("lbtcomment1show");
        LinkButton lbtcomment2show = (LinkButton)FvDetailUser.FindControl("lbtcomment2show");
        LinkButton lbtcomment3show = (LinkButton)FvDetailUser.FindControl("lbtcomment3show");
        LinkButton lbtcommentaboutshow = (LinkButton)FvDetailUser.FindControl("lbtcommentaboutshow");

        Control div_approveabout = (Control)FvDetailUser.FindControl("div_approveabout");
        Control div_commentabout = (Control)FvDetailUser.FindControl("div_commentabout");

        string newsys = _dtchr.BindData_U0Document[0].new_sys;
        string oldsys = _dtchr.BindData_U0Document[0].old_sys;
        string comment = "";// _dtchr.BindData_U0Document[0].comment;
        string commentapp1 = "";
        string commentapp2 = "";
        string commentapp3 = "";
        string commentappabout = "";
        int noidx = _dtchr.BindData_U0Document[0].noidx;
        int acidx = _dtchr.BindData_U0Document[0].acidx;
        string appabout = "";

        // text.Text = Convert.ToString(noidx) + "," + Convert.ToString(acidx);

        if (_dtchr.BindData_U0Document[0].NEmpidxAbout == null)
        {
            div_approveabout.Visible = false;
            div_commentabout.Visible = false;
        }
        else
        {
            div_approveabout.Visible = true;
            appabout = _dtchr.BindData_U0Document[0].NEmpidxAbout;
        }



        if (_dtchr.BindData_U0Document[0].comment == null)
        {
            comment = "0";
        }
        else
        {
            comment = _dtchr.BindData_U0Document[0].comment;
        }

        if (_dtchr.BindData_U0Document[0].commentapp1 == null)
        {
            commentapp1 = "0";
            lbtcomment1show.Visible = false;
        }
        else
        {
            commentapp1 = _dtchr.BindData_U0Document[0].commentapp1;
            lbtcomment1show.Visible = true;
        }

        if (_dtchr.BindData_U0Document[0].commentapp2 == null)
        {
            commentapp2 = "0";
            lbtcomment2show.Visible = false;
        }
        else
        {
            commentapp2 = _dtchr.BindData_U0Document[0].commentapp2;
            lbtcomment2show.Visible = true;
        }

        if (_dtchr.BindData_U0Document[0].commentapp3 == null)
        {
            commentapp3 = "0";
            lbtcomment3show.Visible = false;
        }
        else
        {
            commentapp3 = _dtchr.BindData_U0Document[0].commentapp3;
            lbtcomment3show.Visible = true;
        }

        if (_dtchr.BindData_U0Document[0].commentabout == null)
        {
            commentappabout = "0";
            lbtcommentaboutshow.Visible = false;


        }
        else
        {
            commentappabout = _dtchr.BindData_U0Document[0].commentabout;
            lbtcommentaboutshow.Visible = true;

        }

        if (noidx != 2 && acidx != 2 && comment != "0" || noidx != 3 && acidx != 3 && comment != "0" || noidx != 4 && acidx != 4 && comment != "0")
        {
            div_comment.Visible = true;
        }
        else
        {
            div_comment.Visible = false;
        }

        if (noidx == 2 && acidx == 2 || noidx == 1 && acidx == 1)
        {
            lblappok1.Visible = false;
            lblappok2.Visible = false;
            lblappok3.Visible = false;
            lblappokabout.Visible = false;
        }
        else if (noidx == 3 && acidx == 3)
        {
            lblappok1.Visible = true;
            lblappok2.Visible = false;
            lblappok3.Visible = false;
            lblappokabout.Visible = false;
        }
        else if (noidx == 4 && acidx == 4)
        {
            lblappok1.Visible = true;
            lblappok2.Visible = true;
            lblappok3.Visible = false;
            lblappokabout.Visible = false;
        }
        else if (noidx <= 5 || noidx == 8 || noidx == 12)
        {
            lblappok1.Visible = true;
            lblappok2.Visible = true;
            lblappok3.Visible = true;
            lblappokabout.Visible = false;
        }
        else if (noidx >= 6 && noidx != 8 && noidx != 12 && appabout != null)
        {
            lblappok1.Visible = true;
            lblappok2.Visible = true;
            lblappok3.Visible = true;
            lblappokabout.Visible = true;
        }


        ViewState["newsys"] = newsys;
        ViewState["oldsys"] = oldsys;
        ViewState["comment"] = comment;

        if (newsys.Length >= 200) //50
        {
            lblnewsys.Text = _dtchr.BindData_U0Document[0].new_sys.Substring(0, 200);
            lblnewmore.Visible = true;

            ViewState["newsys_more"] = lblnewsys.Text;
        }
        else
        {
            lblnewsys.Text = _dtchr.BindData_U0Document[0].new_sys;
            lblnewmore.Visible = false;
        }

        if (oldsys.Length >= 200)
        {
            lbloldsys.Text = _dtchr.BindData_U0Document[0].old_sys.Substring(0, 200);
            lbloldmore.Visible = true;
            ViewState["oldsys_more"] = lbloldsys.Text;
        }
        else
        {
            lbloldsys.Text = _dtchr.BindData_U0Document[0].old_sys;
            lbloldmore.Visible = false;
        }


        if (comment.Length >= 200)
        {
            lblcomment.Text = _dtchr.BindData_U0Document[0].comment.Substring(0, 200);
            lblcommentmore.Visible = true;
            ViewState["comment_more"] = lblcomment.Text;
        }
        else
        {
            lblcomment.Text = _dtchr.BindData_U0Document[0].comment;
            lblcommentmore.Visible = false;
        }


    }

    protected void Select_SetDefault()
    {
        dept = ViewState["rtCode_rdepidx"].ToString();
        deptemp = dept.Split(',');
        foreach (string rtdept in deptemp)
        {

            if (rtdept != String.Empty)
            {
                ViewState["rtdept"] = rtdept;


                if (int.Parse(ViewState["rtCode_empidx"].ToString()) == int.Parse(ViewState["EmpIDX"].ToString()) && int.Parse(ViewState["EmpIDX"].ToString()) != 174)
                {
                    //กรณีโชว์เฉพาะคนสร้าง
                    SelectApproveEmpIDXCreate();
                    // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('1');", true);
                }
                else if (int.Parse(ViewState["rtCode_empidx"].ToString()) == 0 && int.Parse(ViewState["rtdept"].ToString()) == int.Parse(ViewState["rdept_idx"].ToString()) &&
                    int.Parse(ViewState["EmpIDX"].ToString()) != 174 && int.Parse(ViewState["EmpIDX"].ToString()) != 1423)
                {
                    // กรณีแสดงเฉพาะคนลาออกต้องการคน Approve แทน และไม่มีเอกสารของตัวเอง
                    SelectApproveSameEmpIDXCreate();
                    // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('2');", true);


                }

                else
                {
                    if (int.Parse(ViewState["EmpIDX"].ToString()) == 174 || int.Parse(ViewState["rtCode_empidx"].ToString()) == 174 || int.Parse(ViewState["EmpIDX"].ToString()) == 1423)
                    {
                        SelectApproveExaminer();
                    }
                    else if (int.Parse(ViewState["EmpIDX"].ToString()) == 3639 || int.Parse(ViewState["EmpIDX"].ToString()) == 3640 || int.Parse(ViewState["EmpIDX"].ToString()) == 3641 || int.Parse(ViewState["EmpIDX"].ToString()) == 3631
                        || ViewState["rtCode_Assign"].ToString() == "1" && int.Parse(ViewState["EmpIDX"].ToString()) == int.Parse(ViewState["rtCode_EmpAssign"].ToString()))
                    {
                        SelectApproveCEO();
                    }
                }
            }
            else
            {
                if (int.Parse(ViewState["rtCode_empidx"].ToString()) == int.Parse(ViewState["EmpIDX"].ToString()) && int.Parse(ViewState["EmpIDX"].ToString()) != 174)
                {
                    //กรณีโชว์เฉพาะคนสร้าง
                    SelectApproveEmpIDXCreate();
                    // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('1');", true);
                }
                else if (int.Parse(ViewState["EmpIDX"].ToString()) == 174 || int.Parse(ViewState["rtCode_empidx"].ToString()) == 174 || int.Parse(ViewState["EmpIDX"].ToString()) == 1423)
                {
                    SelectApproveExaminer();
                }
                else if (int.Parse(ViewState["EmpIDX"].ToString()) == 3639 || int.Parse(ViewState["EmpIDX"].ToString()) == 3640 || int.Parse(ViewState["EmpIDX"].ToString()) == 3641 || int.Parse(ViewState["EmpIDX"].ToString()) == 3631 || ViewState["rtCode_Assign"].ToString() == "1" && int.Parse(ViewState["EmpIDX"].ToString()) == int.Parse(ViewState["rtCode_EmpAssign"].ToString()))
                {
                    SelectApproveCEO();

                }
                else
                {
                    SelectApproveSameEmpIDXCreate();
                }
            }
        }


    }
    #endregion

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Default
    protected void SetDefault()
    {
        var dsEmp = new DataSet();
        dsEmp.Tables.Add("TempDevice");

        dsEmp.Tables[0].Columns.Add("FileName", typeof(string));
        dsEmp.Tables[0].Columns.Add("Download", typeof(string));

        ViewState["vsTempDevicw"] = dsEmp;
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }
    #endregion

    #region Databound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvApprove":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvApprove.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lblsysidx = ((Label)e.Row.FindControl("lblsysidx"));
                        //var images = ((Image)e.Row.FindControl("images"));
                        var lblnew_sys = ((Label)e.Row.FindControl("lblnew_sys"));
                        var hfM0NodeIDXMD = ((HiddenField)e.Row.FindControl("hfM0NodeIDXMD"));
                        var hfM0ActoreIDXMD = ((HiddenField)e.Row.FindControl("hfM0ActoreIDXMD"));
                        var ltlnode = ((Literal)e.Row.FindControl("ltlnode"));
                        string new_sys = lblnew_sys.Text;



                        if (hfM0NodeIDXMD.Value == "8" && hfM0ActoreIDXMD.Value == "6")
                        {
                            ltlnode.Text = "<span style='color:red'> เรียน ผู้อนุมัติที่เกี่ยวข้อง กรุณาตรวจสอบผลกระทบที่ได้รับจากการแก้ไข และพิจารณาผลการอนุมัติรายการ </span>";

                        }
                        else
                        {
                            ltlnode.Text = "";
                        }


                        if (new_sys.Length >= 200) //100
                        {
                            lblnew_sys.Text = new_sys.Substring(0, 200);

                        }
                        else
                        {
                            lblnew_sys.Text = new_sys;
                        }



                    }
                }

                break;

            case "gvFile":
                //e.Row.Attributes.Add("style", "cursor:help;");
                //if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Alternate)
                //{
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    Literal ltFileName11 = (Literal)e.Row.FindControl("ltFileName11");
                    string LinkHost11 = "http://172.16.11.5/taokaenoi.co.th/WebSystem/ITService/UploadFileCHR/";

                    filename = ViewState["Filename"].ToString();
                    filename_ = filename.Split(',');
                    foreach (string name in filename_)
                    {
                        if (name != String.Empty)
                        {
                            ViewState["name"] = name;
                            btnDL11.NavigateUrl = LinkHost11 + ViewState["DocCode"].ToString() + "/" + ltFileName11.Text;
                            ViewState["name"] = null;

                            //e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='orange'");
                            //e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#ffe6ff'");
                            //e.Row.BackColor = System.Drawing.Color.Pink;
                            break;

                        }



                    }

                    //}

                }

                break;


        }
    }

    #endregion

    #region GvPageIndexChang
    // GridView Show
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvApprove":

                GvApprove.PageIndex = e.NewPageIndex;
                //GvApprove.DataBind();
                //text.Text = ViewState["rtCode_empidx"].ToString();

                //if (int.Parse(ViewState["rtCode_empidx"].ToString()) == int.Parse(ViewState["EmpIDX"].ToString()) && int.Parse(ViewState["EmpIDX"].ToString()) == 174)
                //{
                //    SelectApproveEmpIDXCreate();
                //}
                //else if (int.Parse(ViewState["rtCode_empidx"].ToString()) == 0 && int.Parse(ViewState["rtdept"].ToString()) == int.Parse(ViewState["RDeptIDX"].ToString()) &&
                //            int.Parse(ViewState["EmpIDX"].ToString()) != 174)
                //{
                //    // กรณีแสดงเฉพาะคนลาออกต้องการคน Approve แทน และไม่มีเอกสารของตัวเอง
                //    SelectApproveSameEmpIDXCreate();

                //}
                //else
                //{

                //    if (int.Parse(ViewState["EmpIDX"].ToString()) == 174 || int.Parse(ViewState["rtCode_empidx"].ToString()) == 174 || int.Parse(ViewState["EmpIDX"].ToString()) == 1423)
                //    {
                //        SelectApproveExaminer();
                //    }
                //    else if (int.Parse(ViewState["EmpIDX"].ToString()) == 3639 || int.Parse(ViewState["EmpIDX"].ToString()) == 3640 || int.Parse(ViewState["EmpIDX"].ToString()) == 3641 || int.Parse(ViewState["EmpIDX"].ToString()) == 3631 ||
                //        ViewState["rtCode_Assign"].ToString() == "1" && int.Parse(ViewState["EmpIDX"].ToString()) == int.Parse(ViewState["rtCode_EmpAssign"].ToString()))
                //    {

                //        SelectApproveCEO();

                //    }
                //    else
                //    {
                //        // SelectMasterApproveList();
                //    }
                //}

                //if (ViewState["rtdept"].ToString() != String.Empty)
                //{
                if (int.Parse(ViewState["rtCode_empidx"].ToString()) == int.Parse(ViewState["EmpIDX"].ToString()) && int.Parse(ViewState["EmpIDX"].ToString()) != 174)
                {
                    //กรณีโชว์เฉพาะคนสร้าง
                    SelectApproveEmpIDXCreate();
                    // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('1');", true);
                }
                else if (int.Parse(ViewState["rtCode_empidx"].ToString()) == 0 &&
                    int.Parse(ViewState["EmpIDX"].ToString()) != 174 && int.Parse(ViewState["EmpIDX"].ToString()) != 1423)
                {
                    // กรณีแสดงเฉพาะคนลาออกต้องการคน Approve แทน และไม่มีเอกสารของตัวเอง
                    SelectApproveSameEmpIDXCreate();
                    // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('2');", true);

                }
                else
                {
                    if (int.Parse(ViewState["EmpIDX"].ToString()) == 174 || int.Parse(ViewState["rtCode_empidx"].ToString()) == 174 || int.Parse(ViewState["EmpIDX"].ToString()) == 1423)
                    {
                        SelectApproveExaminer();
                    }
                    else if (int.Parse(ViewState["EmpIDX"].ToString()) == 3639 || int.Parse(ViewState["EmpIDX"].ToString()) == 3640 || int.Parse(ViewState["EmpIDX"].ToString()) == 3641 || int.Parse(ViewState["EmpIDX"].ToString()) == 3631
                        || ViewState["rtCode_Assign"].ToString() == "1" && int.Parse(ViewState["EmpIDX"].ToString()) == int.Parse(ViewState["rtCode_EmpAssign"].ToString()))
                    {
                        SelectApproveCEO();
                    }
                    else
                    {
                        SelectApproveSameEmpIDXCreate();
                    }
                }
                //}
                //else
                //{
                //    if (int.Parse(ViewState["rtCode_empidx"].ToString()) == int.Parse(ViewState["EmpIDX"].ToString()) && int.Parse(ViewState["EmpIDX"].ToString()) != 174)
                //    {
                //        //กรณีโชว์เฉพาะคนสร้าง
                //        SelectApproveEmpIDXCreate();
                //        // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('1');", true);
                //    }
                //    else if (int.Parse(ViewState["EmpIDX"].ToString()) == 174 || int.Parse(ViewState["rtCode_empidx"].ToString()) == 174 || int.Parse(ViewState["EmpIDX"].ToString()) == 1423)
                //    {
                //        SelectApproveExaminer();
                //    }
                //    else if (int.Parse(ViewState["EmpIDX"].ToString()) == 3639 || int.Parse(ViewState["EmpIDX"].ToString()) == 3640 || int.Parse(ViewState["EmpIDX"].ToString()) == 3641 || int.Parse(ViewState["EmpIDX"].ToString()) == 3631 || ViewState["rtCode_Assign"].ToString() == "1" && int.Parse(ViewState["EmpIDX"].ToString()) == int.Parse(ViewState["rtCode_EmpAssign"].ToString()))
                //    {
                //        SelectApproveCEO();

                //    }
                //    else
                //    {
                //        SelectApproveSameEmpIDXCreate();
                //    }
                //}



                //if (int.Parse(ViewState["rtCode_empidx"].ToString()) == int.Parse(ViewState["EmpIDX"].ToString()) && int.Parse(ViewState["EmpIDX"].ToString()) == 174)
                //{
                //    SelectApproveEmpIDXCreate();
                //}
                //else if (int.Parse(ViewState["rtCode_empidx"].ToString()) == 0 && int.Parse(ViewState["rtdept"].ToString()) == int.Parse(ViewState["rdept_idx"].ToString()) &&
                //            int.Parse(ViewState["EmpIDX"].ToString()) != 174)
                //{
                //    // กรณีแสดงเฉพาะคนลาออกต้องการคน Approve แทน และไม่มีเอกสารของตัวเอง
                //    SelectApproveSameEmpIDXCreate();

                //}
                //else
                //{
                //    //CheckBox chkRow = (CheckBox)GvApprove.FindControl("chkRow");


                //    if (int.Parse(ViewState["EmpIDX"].ToString()) == 174 || int.Parse(ViewState["rtCode_empidx"].ToString()) == 174 || int.Parse(ViewState["EmpIDX"].ToString()) == 1423)
                //    {
                //        SelectApproveExaminer();
                //    }
                //    else if (int.Parse(ViewState["EmpIDX"].ToString()) == 3639 || int.Parse(ViewState["EmpIDX"].ToString()) == 3640 ||
                //    int.Parse(ViewState["EmpIDX"].ToString()) == 3641 || int.Parse(ViewState["EmpIDX"].ToString()) == 3631 ||
                //    ViewState["rtCode_Assign"].ToString() == "1" && int.Parse(ViewState["EmpIDX"].ToString()) == int.Parse(ViewState["rtCode_EmpAssign"].ToString()))
                //    {

                //        SelectApproveCEO();

                //    }
                //    else
                //    {
                //        // SelectMasterApproveList();
                //    }
                //}

                break;

        }
    }




    #endregion

    #region SetFormView
    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        Control div_personal = (Control)FvDetailUser.FindControl("div_personal");
        Control div_p1 = (Control)FvDetailUser.FindControl("div_p1");

        LinkButton BoxButtonSearchShow = (LinkButton)FvDetailUser.FindControl("BoxButtonSearchShow");
        LinkButton BoxButtonSearchHide = (LinkButton)FvDetailUser.FindControl("BoxButtonSearchHide");
        Button btnHidden = (Button)GvApprove.FindControl("btnHidden");
        Label lblu0idx = (Label)GvApprove.FindControl("lblu0idx");
        HiddenField hfM0NodeIDXMD = (HiddenField)FvDetailUser.FindControl("hfM0NodeIDXMD");
        HiddenField hfM0ActoreIDXMD = (HiddenField)FvDetailUser.FindControl("hfM0ActoreIDXMD");

        Label lbloldsys = (Label)FvDetailUser.FindControl("lbloldsys");
        LinkButton lbloldhide = (LinkButton)FvDetailUser.FindControl("lbloldhide");
        LinkButton lbloldmore = (LinkButton)FvDetailUser.FindControl("lbloldmore");

        Label lblnewsys = (Label)FvDetailUser.FindControl("lblnewsys");
        LinkButton lblnewmore = (LinkButton)FvDetailUser.FindControl("lblnewmore");
        LinkButton lblnewhide = (LinkButton)FvDetailUser.FindControl("lblnewhide");

        Label lblcomment = (Label)FvDetailUser.FindControl("lblcomment");
        LinkButton lblcommentmore = (LinkButton)FvDetailUser.FindControl("lblcommentmore");
        LinkButton lblcommenthide = (LinkButton)FvDetailUser.FindControl("lblcommenthide");
        HyperLink btnDL11 = (HyperLink)GvApprove.FindControl("btnDL11");

        Control div_comment1 = (Control)FvDetailUser.FindControl("div_comment1");
        LinkButton lbtcomment1show = (LinkButton)FvDetailUser.FindControl("lbtcomment1show");
        LinkButton lbtcomment1hide = (LinkButton)FvDetailUser.FindControl("lbtcomment1hide");

        Control div_comment2 = (Control)FvDetailUser.FindControl("div_comment2");
        LinkButton lbtcomment2show = (LinkButton)FvDetailUser.FindControl("lbtcomment2show");
        LinkButton lbtcomment2hide = (LinkButton)FvDetailUser.FindControl("lbtcomment2hide");

        Control div_comment3 = (Control)FvDetailUser.FindControl("div_comment3");
        LinkButton lbtcomment3show = (LinkButton)FvDetailUser.FindControl("lbtcomment3show");
        LinkButton lbtcomment3hide = (LinkButton)FvDetailUser.FindControl("lbtcomment3hide");

        Control div_commentabout = (Control)FvDetailUser.FindControl("div_commentabout");
        LinkButton lbtcommentaboutshow = (LinkButton)FvDetailUser.FindControl("lbtcommentaboutshow");
        LinkButton lbtcommentabouthide = (LinkButton)FvDetailUser.FindControl("lbtcommentabouthide");

        _dtchr = new data_chr();
        _dtchr.BindData_U0Document = new BindData[1];
        BindData updateapprove = new BindData();

        switch (cmdName)
        {

            case "btnhide":
                string arg_detail = "0";// = new string();
                arg_detail = e.CommandArgument.ToString();
                int uoidx = int.Parse(arg_detail);
                ViewState["u0idx"] = uoidx;

                MvMaster.SetActiveView(ViewDetail);
                Select_U0IDX();

                GridView gvFile = (GridView)FvDetailUser.FindControl("gvFile");


                string pathFilesNew = "http://mas.taokaenoi.co.th/uploadfiles/CHR/";
                string pathFilesOld = "http://www.taokaenoi.co.th/WebSystem/ITService/UploadFileCHR/"; //"http://172.16.11.44/taokaenoi.co.th/WebSystem/ITService/UploadFileCHR/";// UploadFileCHR/";// 

                //string pathall = pathFilesNew +ViewState["DocCode"].ToString() + "/";
                Uri urlNew = new Uri(pathFilesNew + ViewState["DocCode"].ToString() + "/"); //+ ViewState["DocCode"].ToString() + "0.pdf"
                                                                                            // string[] parts = urlNew.LocalPath.Split('/');
                HttpWebResponse responseNew = null;
                HttpWebRequest requestNew = (HttpWebRequest)WebRequest.Create(urlNew);
                requestNew.Method = "HEAD";
                requestNew.ContentLength = 0;


                try
                {
                    responseNew = (HttpWebResponse)requestNew.GetResponse();
                    if (responseNew != null)
                    {
                        // ลิงก์ที่ 1 มีไฟล์
                        responseNew.Close();
                    }
                }
                catch (Exception)
                {
                    // ลิงก์ที่ 1 ไม่มีไฟล์
                    for (int i = 0; i <= 20; i++)
                    {
                        Uri urlOld = new Uri(pathFilesOld + ViewState["DocCode"].ToString() + "/" + ViewState["DocCode"].ToString() + i + ".pdf");
                        HttpWebResponse responseOld = null;
                        HttpWebRequest requestOld = (HttpWebRequest)WebRequest.Create(urlOld);
                        requestOld.Method = "HEAD";
                        requestOld.ContentLength = 0;


                        try
                        {
                            responseOld = (HttpWebResponse)requestOld.GetResponse();


                            if (responseOld != null)
                            {
                                // ลิงก์ที่ 2 มีไฟล์

                                ViewState["responseOld"] = responseOld;
                                var dsDevice = (DataSet)ViewState["vsTempDevicw"];

                                var drDevice = dsDevice.Tables[0].NewRow();
                                drDevice["FileName"] = ViewState["DocCode"].ToString() + i + ".pdf";
                                dsDevice.Tables[0].Rows.Add(drDevice);
                                ViewState["vsTempDevicw"] = dsDevice;
                                ViewState["Filename"] += drDevice["FileName"] + ",";

                                gvFile.DataSource = dsDevice.Tables[0];
                                gvFile.DataBind();

                            }
                        }
                        catch (Exception ex)
                        {
                            // ลิงก์ที่ 2 ไม่มีไฟล์
                            // text.Text = ex.ToString();//"File not found.";
                        }
                    }
                }


                break;

            case "btnback":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "BtnHideSETBoxAllSearchShow":

                //  div_personal.Visible = true;
                BoxButtonSearchShow.Visible = false;
                BoxButtonSearchHide.Visible = true;
                div_p1.Visible = true;

                break;

            case "BtnHideSETBoxAllSearchHide":
                //  div_personal.Visible = false;
                BoxButtonSearchShow.Visible = true;
                BoxButtonSearchHide.Visible = false;
                div_p1.Visible = false;

                break;


            case "btnapprove":
                int IDX = 0;
                int UNIDX = 0;
                int ACIDX = 0;
                int Approve = 0;

                string[] arg_approve_y = new string[4];
                arg_approve_y = e.CommandArgument.ToString().Split(';');
                IDX = int.Parse(arg_approve_y[0]);
                UNIDX = int.Parse(arg_approve_y[1]);
                ACIDX = int.Parse(arg_approve_y[2]);
                Approve = int.Parse(arg_approve_y[3]);

                // text.Text = Convert.ToString(IDX) + "," + Convert.ToString(UNIDX) + "," + Convert.ToString(ACIDX) + "," + Convert.ToString(Approve);

                updateapprove.noidx_add = UNIDX;
                updateapprove.acidx_add = ACIDX;
                updateapprove.approve_status_add = Approve;
                updateapprove.u0idx_add = IDX;
                updateapprove.Add_IDX_add = int.Parse(ViewState["EmpIDX"].ToString());
                updateapprove.comment = "-";
                _dtchr.BindData_U0Document[0] = updateapprove;

                // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));



                if (UNIDX == 2 && ACIDX == 2 || UNIDX == 3 && ACIDX == 3 || UNIDX == 4 && ACIDX == 4
                    || UNIDX == 5 && ACIDX == 5 || UNIDX == 8 && ACIDX == 6)
                {
                    _dtchr = callServicePostCHR(urlUpdate_Approver, _dtchr);
                    ViewState["rtu1idx"] = _dtchr.ReturnU1IDX;

                    if (Approve == 2)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal1();", true);
                    }
                    else
                    {
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }

                }
                else if (UNIDX == 6 && ACIDX == 7 || UNIDX == 7 && ACIDX == 1 || UNIDX == 17 && ACIDX == 7
                    || UNIDX == 18 && ACIDX == 1)
                {
                    _dtchr = callServicePostCHR(urlUpdate_ApproveExam_User, _dtchr);
                    ViewState["rtu1idx"] = _dtchr.ReturnU1IDX;

                    if (Approve == 2)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal1();", true);
                    }
                    else
                    {
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                }

                break;


            case "btnapprove_detail":
                string arg_detail_update = "0";// = new string();
                arg_detail_update = e.CommandArgument.ToString();
                int approve = int.Parse(arg_detail_update);



                updateapprove.noidx_add = int.Parse(hfM0NodeIDXMD.Value);
                updateapprove.acidx_add = int.Parse(hfM0ActoreIDXMD.Value);
                updateapprove.approve_status_add = approve;
                updateapprove.u0idx_add = int.Parse(ViewState["u0idx"].ToString());
                updateapprove.Add_IDX_add = int.Parse(ViewState["EmpIDX"].ToString());
                updateapprove.comment_add = "-";
                _dtchr.BindData_U0Document[0] = updateapprove;

                // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));



                if (int.Parse(hfM0NodeIDXMD.Value) == 2 && int.Parse(hfM0ActoreIDXMD.Value) == 2 || int.Parse(hfM0NodeIDXMD.Value) == 3 && int.Parse(hfM0ActoreIDXMD.Value) == 3
                    || int.Parse(hfM0NodeIDXMD.Value) == 4 && int.Parse(hfM0ActoreIDXMD.Value) == 4
                    || int.Parse(hfM0NodeIDXMD.Value) == 5 && int.Parse(hfM0ActoreIDXMD.Value) == 5 || int.Parse(hfM0NodeIDXMD.Value) == 8 && int.Parse(hfM0ActoreIDXMD.Value) == 6)
                {
                    _dtchr = callServicePostCHR(urlUpdate_Approver, _dtchr);
                    ViewState["rtu1idx"] = _dtchr.ReturnU1IDX;

                    if (approve == 2)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    }
                    else
                    {
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }




                }
                else if (int.Parse(hfM0NodeIDXMD.Value) == 6 && int.Parse(hfM0ActoreIDXMD.Value) == 7 || int.Parse(hfM0NodeIDXMD.Value) == 7 && int.Parse(hfM0ActoreIDXMD.Value) == 1 || int.Parse(hfM0NodeIDXMD.Value) == 17 && int.Parse(hfM0ActoreIDXMD.Value) == 7 || int.Parse(hfM0NodeIDXMD.Value) == 18 && int.Parse(hfM0ActoreIDXMD.Value) == 1)
                {
                    _dtchr = callServicePostCHR(urlUpdate_ApproveExam_User, _dtchr);
                    ViewState["rtu1idx"] = _dtchr.ReturnU1IDX;

                    if (approve == 2)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    }
                    else
                    {
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                }



                break;

            case "btncomment":

                updateapprove.UIDX = int.Parse(ViewState["rtu1idx"].ToString());

                if (txtremark_approve.Text != "")
                {
                    updateapprove.comment_add = txtremark_approve.Text;
                }
                else if (txtremark_approve1.Text != "")
                {
                    updateapprove.comment_add = txtremark_approve1.Text;
                }
                else
                {
                    updateapprove.comment_add = "-";
                }


                _dtchr.BindData_U0Document[0] = updateapprove;
                _dtchr = callServicePostCHR(urlComment_Reject, _dtchr);


                //MvMaster.SetActiveView(ViewIndex);
                //Select_SetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);


                break;

            case "closecomment":
                // ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "CloseModal();", true);
                //MvMaster.SetActiveView(ViewIndex);
                //Select_SetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "lbloldmore":


                lbloldsys.Text = ViewState["oldsys"].ToString();
                lbloldmore.Visible = false;
                lbloldhide.Visible = true;

                break;

            case "lbloldhide":


                lbloldsys.Text = ViewState["oldsys_more"].ToString();
                lbloldmore.Visible = true;
                lbloldhide.Visible = false;

                break;


            case "lblnewmore":

                lblnewsys.Text = ViewState["newsys"].ToString();
                lblnewmore.Visible = false;
                lblnewhide.Visible = true;
                break;




            case "lblnewhide":

                lblnewsys.Text = ViewState["newsys_more"].ToString();
                lblnewmore.Visible = true;
                lblnewhide.Visible = false;

                break;

            case "lblcommentmore":


                lblcomment.Text = ViewState["comment"].ToString();
                lblcommentmore.Visible = false;
                lblcommenthide.Visible = true;

                break;


            case "lblcommenthide":

                lblcomment.Text = ViewState["comment_more"].ToString();
                lblcommentmore.Visible = true;
                lblcommenthide.Visible = false;

                break;

            case "lbtcomment1show":
                div_comment1.Visible = true;
                lbtcomment1show.Visible = false;
                lbtcomment1hide.Visible = true;

                break;
            case "lbtcomment1hide":
                div_comment1.Visible = false;
                lbtcomment1hide.Visible = false;
                lbtcomment1show.Visible = true;
                break;

            case "lbtcomment2show":
                div_comment2.Visible = true;
                lbtcomment2show.Visible = false;
                lbtcomment2hide.Visible = true;

                break;
            case "lbtcomment2hide":
                div_comment2.Visible = false;
                lbtcomment2hide.Visible = false;
                lbtcomment2show.Visible = true;

                break;

            case "lbtcomment3show":
                div_comment3.Visible = true;
                lbtcomment3show.Visible = false;
                lbtcomment3hide.Visible = true;

                break;
            case "lbtcomment3hide":
                div_comment3.Visible = false;
                lbtcomment3hide.Visible = false;
                lbtcomment3show.Visible = true;

                break;

            case "lbtcommentaboutshow":
                div_commentabout.Visible = true;
                lbtcommentaboutshow.Visible = false;
                lbtcommentabouthide.Visible = true;

                break;
            case "lbtcommentabouthide":
                div_commentabout.Visible = false;
                lbtcommentabouthide.Visible = false;
                lbtcommentaboutshow.Visible = true;

                break;

        }
    }
    #endregion
}