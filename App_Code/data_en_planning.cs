﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_en_planning")]
public class data_en_planning
{
    [XmlElement("return_code")]
    public string return_code { get; set; }

    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("BoxEN_TypeItemList")]
    public TypeItem_Detail[] BoxEN_TypeItemList { get; set; }

    [XmlElement("BoxEN_StandardList")]
    public Standard_Detail[] BoxEN_StandardList { get; set; }

    [XmlElement("BoxEN_MethodList")]
    public Method_Detail[] BoxEN_MethodList { get; set; }

    [XmlElement("BoxEN_ToolingList")]
    public Tooling_Detail[] BoxEN_ToolingList { get; set; }

    [XmlElement("BoxEN_TimeList")]
    public Timing_Detail[] BoxEN_TimeList { get; set; }

    [XmlElement("BoxEN_TypeCodeList")]
    public TypeCode_Detail[] BoxEN_TypeCodeList { get; set; }

    [XmlElement("BoxEN_GroupCodeList")]
    public GroupCode_Detail[] BoxEN_GroupCodeList { get; set; }

    [XmlElement("BoxEN_TypeMachineList")]
    public TypeMachine_Detail[] BoxEN_TypeMachineList { get; set; }

    [XmlElement("BoxEN_ContentList")]
    public Content_Detail[] BoxEN_ContentList { get; set; }

    [XmlElement("BoxEN_m1formList")]
    public m1form_Detail[] BoxEN_m1formList { get; set; }

    [XmlElement("BoxEN_m2formList")]
    public m2form_Detail[] BoxEN_m2formList { get; set; }

    [XmlElement("BoxEN_m3formList")]
    public m3form_Detail[] BoxEN_m3formList { get; set; }

    [XmlElement("BoxEN_u0formList")]
    public u0form_Detail[] BoxEN_u0formList { get; set; }

    [XmlElement("BoxEN_u0docform")]
    public u0docform_Detail[] BoxEN_u0docform { get; set; }

    [XmlElement("BoxEN_u2docform")]
    public u2docform_Detail[] BoxEN_u2docform { get; set; }


    [XmlElement("en_report_action")]
    public en_report[] en_report_action { get; set; }

    [XmlElement("en_lookup_action")]
    public en_lookup[] en_lookup_action { get; set; }
}

#region Master Data M0
[Serializable]
public class TypeItem_Detail
{
    [XmlElement("m0tyidx")]
    public int m0tyidx { get; set; }

    [XmlElement("type_name_th")]
    public string type_name_th { get; set; }

    [XmlElement("type_name_en")]
    public string type_name_en { get; set; }

    [XmlElement("type_status")]
    public int type_status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("UEmpIDX")]
    public int UEmpIDX { get; set; }

    [XmlElement("TypeStatusDetail")]
    public string TypeStatusDetail { get; set; }

    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }

    [XmlElement("NameEN")]
    public string NameEN { get; set; }

    [XmlElement("m0stidx")]
    public int m0stidx { get; set; }

    [XmlElement("GCIDX")]
    public int GCIDX { get; set; }

    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }

    [XmlElement("NameTypecode")]
    public string NameTypecode { get; set; }

    [XmlElement("NameGroupcode")]
    public string NameGroupcode { get; set; }


}

[Serializable]
public class Standard_Detail
{
    [XmlElement("m0stidx")]
    public int m0stidx { get; set; }

    [XmlElement("m0tyidx")]
    public int m0tyidx { get; set; }

    [XmlElement("m0coidx")]
    public int m0coidx { get; set; }

    [XmlElement("standard_name_th")]
    public string standard_name_th { get; set; }

    [XmlElement("standard_name_en")]
    public string standard_name_en { get; set; }

    [XmlElement("standard_status")]
    public int standard_status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("UEmpIDX")]
    public int UEmpIDX { get; set; }

    [XmlElement("StandardStatusDetail")]
    public string StandardStatusDetail { get; set; }

    [XmlElement("type_name_th")]
    public string type_name_th { get; set; }

    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }

    [XmlElement("NameEN")]
    public string NameEN { get; set; }

    [XmlElement("GCIDX")]
    public int GCIDX { get; set; }

    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }

    [XmlElement("NameTypecode")]
    public string NameTypecode { get; set; }

    [XmlElement("NameGroupcode")]
    public string NameGroupcode { get; set; }

    [XmlElement("contentth")]
    public string contentth { get; set; }

}


[Serializable]
public class Method_Detail
{
    [XmlElement("m0meidx")]
    public int m0meidx { get; set; }

    [XmlElement("method_name_th")]
    public string method_name_th { get; set; }

    [XmlElement("method_name_en")]
    public string method_name_en { get; set; }

    [XmlElement("method_status")]
    public int method_status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("UEmpIDX")]
    public int UEmpIDX { get; set; }

    [XmlElement("MethodStatusDetail")]
    public string MethodStatusDetail { get; set; }

    [XmlElement("m0stidx")]
    public int m0stidx { get; set; }

    [XmlElement("m0tyidx")]
    public int m0tyidx { get; set; }

    [XmlElement("m0coidx")]
    public int m0coidx { get; set; }

    [XmlElement("standard_name_th")]
    public string standard_name_th { get; set; }

    [XmlElement("type_name_th")]
    public string type_name_th { get; set; }

    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }

    [XmlElement("NameEN")]
    public string NameEN { get; set; }

    [XmlElement("GCIDX")]
    public int GCIDX { get; set; }

    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }

    [XmlElement("NameTypecode")]
    public string NameTypecode { get; set; }

    [XmlElement("NameGroupcode")]
    public string NameGroupcode { get; set; }

    [XmlElement("contentth")]
    public string contentth { get; set; }

}

[Serializable]
public class TypeCode_Detail
{
    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }

    [XmlElement("NameTypecode")]
    public string NameTypecode { get; set; }

    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }

    [XmlElement("TypeCode")]
    public string TypeCode { get; set; }



}

[Serializable]
public class GroupCode_Detail
{
    [XmlElement("GCIDX")]
    public int GCIDX { get; set; }

    [XmlElement("NameTypecode")]
    public string NameTypecode { get; set; }

    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }

    [XmlElement("GroupCode")]
    public int GroupCode { get; set; }


}


[Serializable]
public class Tooling_Detail
{
    [XmlElement("m0toidx")]
    public int m0toidx { get; set; }

    [XmlElement("tooling_name_th")]
    public string tooling_name_th { get; set; }

    [XmlElement("tooling_name_en")]
    public string tooling_name_en { get; set; }

    [XmlElement("tooling_status")]
    public int tooling_status { get; set; }

    [XmlElement("mat_number")]
    public int mat_number { get; set; }


    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("UEmpIDX")]
    public int UEmpIDX { get; set; }

    [XmlElement("ToolingStatusDetail")]
    public string ToolingStatusDetail { get; set; }

    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }

    [XmlElement("NameEN")]
    public string NameEN { get; set; }

    [XmlElement("GCIDX")]
    public int GCIDX { get; set; }

    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }

    [XmlElement("NameTypecode")]
    public string NameTypecode { get; set; }

    [XmlElement("NameGroupcode")]
    public string NameGroupcode { get; set; }

    [XmlElement("choose_type")]
    public int choose_type { get; set; }

}

[Serializable]
public class Timing_Detail
{
    [XmlElement("m0tidx")]
    public int m0tidx { get; set; }

    [XmlElement("time_desc")]
    public string time_desc { get; set; }

    [XmlElement("time_short")]
    public string time_short { get; set; }

    [XmlElement("qty")]
    public int qty { get; set; }

    [XmlElement("unit")]
    public string unit { get; set; }

    [XmlElement("count_doing")]
    public int count_doing { get; set; }

    [XmlElement("unit_doing")]
    public string unit_doing { get; set; }

    [XmlElement("count_director")]
    public int count_director { get; set; }

    [XmlElement("unit_director")]
    public string unit_director { get; set; }

    [XmlElement("time_status")]
    public int time_status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("TimingStatusDetail")]
    public string TimingStatusDetail { get; set; }

    [XmlElement("count_coo")]
    public int count_coo { get; set; }

    [XmlElement("unit_coo")]
    public string unit_coo { get; set; }
    [XmlElement("alertdoing")]
    public string alertdoing { get; set; }

    [XmlElement("alertdirector")]
    public string alertdirector { get; set; }
    [XmlElement("alertcoo")]
    public string alertcoo { get; set; }

}

[Serializable]
public class TypeMachine_Detail
{
    [XmlElement("TypeIDX")]
    public int TypeIDX { get; set; }

    [XmlElement("TypeName")]
    public string TypeName { get; set; }



}


[Serializable]
public class Content_Detail
{
    [XmlElement("m0coidx")]
    public int m0coidx { get; set; }

    [XmlElement("contentth")]
    public string contentth { get; set; }

    [XmlElement("contenten")]
    public string contenten { get; set; }

    [XmlElement("content_status")]
    public int content_status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("ContentStatusDetail")]
    public string ContentStatusDetail { get; set; }

    [XmlElement("m0tyidx")]
    public int m0tyidx { get; set; }

    [XmlElement("type_name_th")]
    public string type_name_th { get; set; }

    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }

    [XmlElement("NameEN")]
    public string NameEN { get; set; }

    [XmlElement("m0stidx")]
    public int m0stidx { get; set; }

    [XmlElement("GCIDX")]
    public int GCIDX { get; set; }

    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }

    [XmlElement("NameTypecode")]
    public string NameTypecode { get; set; }

    [XmlElement("NameGroupcode")]
    public string NameGroupcode { get; set; }


}

#endregion


#region Master Data Form Result
[Serializable]
public class m1form_Detail
{
    [XmlElement("m1idx")]
    public int m1idx { get; set; }

    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }

    [XmlElement("m1status")]
    public int m1status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("GCIDX")]
    public int GCIDX { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

}

[Serializable]
public class m2form_Detail
{
    [XmlElement("m2idx")]
    public int m2idx { get; set; }

    [XmlElement("m1idx")]
    public int m1idx { get; set; }

    [XmlElement("m2status")]
    public int m2status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("m0tyidx")]
    public int m0tyidx { get; set; }

    [XmlElement("m0coidx")]
    public int m0coidx { get; set; }

    [XmlElement("m0stidx")]
    public int m0stidx { get; set; }

    [XmlElement("m0meidx")]
    public int m0meidx { get; set; }

    [XmlElement("time")]
    public string time { get; set; }

    [XmlElement("TCIDX2")]
    public int TCIDX2 { get; set; }

    [XmlElement("GCIDX2")]
    public int GCIDX2 { get; set; }

    [XmlElement("m0toidx_comma")]
    public string m0toidx_comma { get; set; }


}

[Serializable]
public class m3form_Detail
{
    [XmlElement("m3idx")]
    public int m3idx { get; set; }

    [XmlElement("m2idx")]
    public int m2idx { get; set; }

    [XmlElement("m3status")]
    public int m3status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("m0toidx")]
    public int m0toidx { get; set; }

    [XmlElement("m0toidx_comma")]
    public string m0toidx_comma { get; set; }

    [XmlElement("TCIDX3")]
    public int TCIDX3 { get; set; }

    [XmlElement("GCIDX3")]
    public int GCIDX3 { get; set; }

    [XmlElement("m0toidx3")]
    public int m0toidx3 { get; set; }



}


[Serializable]
public class u0form_Detail
{
    [XmlElement("m1idx")]
    public int m1idx { get; set; }

    [XmlElement("m2idx")]
    public int m2idx { get; set; }

    [XmlElement("m3idx")]
    public int m3idx { get; set; }

    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("GCIDX")]
    public int GCIDX { get; set; }

    [XmlElement("NameGroupcode")]
    public string NameGroupcode { get; set; }

    [XmlElement("NameTypecode")]
    public string NameTypecode { get; set; }

    [XmlElement("NameEN")]
    public string NameEN { get; set; }

    [XmlElement("datecerate")]
    public string datecerate { get; set; }

    [XmlElement("type_name_th")]
    public string type_name_th { get; set; }

    [XmlElement("contentth")]
    public string contentth { get; set; }

    [XmlElement("method_name_th")]
    public string method_name_th { get; set; }

    [XmlElement("standard_name_th")]
    public string standard_name_th { get; set; }

    [XmlElement("tooling_name_th")]
    public string tooling_name_th { get; set; }

    [XmlElement("m0coidx")]
    public int m0coidx { get; set; }

    [XmlElement("m0tyidx")]
    public int m0tyidx { get; set; }

    [XmlElement("m0stidx")]
    public int m0stidx { get; set; }

    [XmlElement("m0meidx")]
    public int m0meidx { get; set; }

    [XmlElement("m0toidx")]
    public int m0toidx { get; set; }

    [XmlElement("time")]
    public string time { get; set; }

    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }

    [XmlElement("m0toidx_comma")]
    public string m0toidx_comma { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("result_comment")]
    public string result_comment { get; set; }

}
#endregion

#region PMMS System
[Serializable]
public class u0docform_Detail
{
    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }

    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }

    [XmlElement("GCIDX")]
    public int GCIDX { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }   

    [XmlElement("m0tidx")]
    public int m0tidx { get; set; }

    [XmlElement("customplan")]
    public string customplan { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("m1idx")]
    public int m1idx { get; set; }

    [XmlElement("MCCode")]
    public string MCCode { get; set; }

    [XmlElement("countplan")]
    public int countplan { get; set; }

    [XmlElement("dateplanstart")]
    public string dateplanstart { get; set; }

    [XmlElement("BuildingIDX")]
    public int BuildingIDX { get; set; }

    [XmlElement("RoomIDX")]
    public int RoomIDX { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("RdeptIDX")]
    public int RdeptIDX { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("MCIDX")]
    public int MCIDX { get; set; }

    [XmlElement("MCCode_comma")]
    public string MCCode_comma { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }

    [XmlElement("code")]
    public string code { get; set; }

    [XmlElement("contentth")]
    public string contentth { get; set; }

    [XmlElement("method_name_th")]
    public string method_name_th { get; set; }

    [XmlElement("standard_name_th")]
    public string standard_name_th { get; set; }

    [XmlElement("tooling_name_th")]
    public string tooling_name_th { get; set; }

    [XmlElement("m0coidx")]
    public int m0coidx { get; set; }

    [XmlElement("m0tyidx")]
    public int m0tyidx { get; set; }

    [XmlElement("m0stidx")]
    public int m0stidx { get; set; }

    [XmlElement("m0meidx")]
    public int m0meidx { get; set; }

    [XmlElement("m0toidx")]
    public int m0toidx { get; set; }

    [XmlElement("time")]
    public string time { get; set; }

    [XmlElement("u1idx_planning")]
    public int u1idx_planning { get; set; }

    [XmlElement("u0idx")]
    public int u0idx { get; set; }


    [XmlElement("datestart")]
    public string datestart { get; set; }

    [XmlElement("RPIDX")]
    public int RPIDX { get; set; }

    [XmlElement("m2idx")]
    public int m2idx { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("stidx")]
    public int stidx { get; set; }

    [XmlElement("TypeNameTH")]
    public string TypeNameTH { get; set; }

    [XmlElement("MCNameTH")]
    public string MCNameTH { get; set; }

    [XmlElement("BuildingName")]
    public string BuildingName { get; set; }

    [XmlElement("Roomname")]
    public string Roomname { get; set; }

    [XmlElement("time_desc")]
    public string time_desc { get; set; }

    [XmlElement("DateManage")]
    public string DateManage { get; set; }

    [XmlElement("NameGroupCode")]
    public string NameGroupCode { get; set; }

    [XmlElement("NameTypecode")]
    public string NameTypecode { get; set; }

    [XmlElement("yearplan")]
    public string yearplan { get; set; }

    [XmlElement("Year")]
    public int Year { get; set; }

    [XmlElement("RECode")]
    public string RECode { get; set; }

    [XmlElement("dateplanend")]
    public string dateplanend { get; set; }

    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }

    [XmlElement("MobileNo")]
    public string MobileNo { get; set; }

    [XmlElement("Email")]
    public string Email { get; set; }

    [XmlElement("EmpCode")]
    public string EmpCode { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("nodeidx")]
    public int nodeidx { get; set; }

    [XmlElement("actoridx")]
    public int actoridx { get; set; }

    [XmlElement("STAppIDX")]
    public int STAppIDX { get; set; }

    [XmlElement("MCIDX_Comma")]
    public string MCIDX_Comma { get; set; }



}

[Serializable]
public class u2docform_Detail
{
    [XmlElement("m2idx")]
    public int m2idx { get; set; }

    [XmlElement("reult_value")]
    public int reult_value { get; set; }

    [XmlElement("result_comment")]
    public string result_comment { get; set; }

}

// start teppanop
#region EN Report 
[Serializable]
public class en_report
{
    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }
    [XmlElement("idx")]
    public int idx { get; set; }
    [XmlElement("idx1")]
    public int idx1 { get; set; }

    [XmlElement("zId")]
    public int zId { get; set; }
    [XmlElement("zName")]
    public string zName { get; set; }
    [XmlElement("zId_G")]
    public int zId_G { get; set; }
    [XmlElement("zName_G")]
    public string zName_G { get; set; }

    [XmlElement("zmonth")]
    public int zmonth { get; set; }
    [XmlElement("zyear")]
    public int zyear { get; set; }

    [XmlElement("fil_Search")]
    public string fil_Search { get; set; }
    [XmlElement("filter_keyword")]
    public string filter_keyword { get; set; }

    [XmlElement("mcidx")]
    public int mcidx { get; set; }
    [XmlElement("MCCode")]
    public string MCCode { get; set; }
    [XmlElement("MCNameTH")]
    public string MCNameTH { get; set; }
    [XmlElement("zMCCode")]
    public string zMCCode { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }
    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }
    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }
    [XmlElement("BuildingIDX")]
    public int BuildingIDX { get; set; }
    [XmlElement("MCIDX")]
    public int MCIDX { get; set; }
    [XmlElement("zday")]
    public int zday { get; set; }
    [XmlElement("zweek")]
    public int zweek { get; set; }
    [XmlElement("countplan")]
    public int countplan { get; set; }
    [XmlElement("zCount")]
    public int zCount { get; set; }
}
[Serializable]
public class en_lookup
{
    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }
    [XmlElement("idx")]
    public int idx { get; set; }
    [XmlElement("idx1")]
    public int idx1 { get; set; }

    [XmlElement("zId")]
    public int zId { get; set; }
    [XmlElement("zName")]
    public string zName { get; set; }
    [XmlElement("zId_G")]
    public int zId_G { get; set; }
    [XmlElement("zName_G")]
    public string zName_G { get; set; }

    [XmlElement("zmonth")]
    public int zmonth { get; set; }
    [XmlElement("zyear")]
    public int zyear { get; set; }

    [XmlElement("fil_Search")]
    public string fil_Search { get; set; }
    [XmlElement("filter_keyword")]
    public string filter_keyword { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }
    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }
    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }
    [XmlElement("BuildingIDX")]
    public int BuildingIDX { get; set; }



}

#endregion
// end teppanop
#endregion





