﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_overtime_shiftrotate.aspx.cs" Inherits="websystem_hr_hr_overtime_shiftrotate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>


    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">

                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docCreate"> สร้างรายการ</asp:LinkButton>
                        </li>

                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbDetailAdmin" runat="server" CommandName="cmdDetailAdmin" OnCommand="navCommand" CommandArgument="docDetailAdmin"> รายการสำหรับ Admin</asp:LinkButton>
                        </li>

                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbDetail" runat="server" CommandName="cmdDetail" OnCommand="navCommand" CommandArgument="docDetail"> รายการ OT กะหมุนเวียน</asp:LinkButton>
                        </li>

                        <li id="li4" runat="server">
                            <asp:LinkButton ID="lbWaitApproveAdminEdit" runat="server" CommandName="cmdApproveOTShiftRotate" OnCommand="btnCommand" CommandArgument="11"> รายการที่รอแก้ไข</asp:LinkButton>
                        </li>

                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbWaitApproveHeadUser" runat="server" CommandName="cmdApproveOTShiftRotate" OnCommand="btnCommand" CommandArgument="12"> รายการที่รออนุมัติ</asp:LinkButton>
                        </li>

                        <li id="li5" runat="server">
                            <asp:LinkButton ID="lbWaitApproveHR" runat="server" CommandName="cmdApproveOTShiftRotate" OnCommand="btnCommand" CommandArgument="13"> ส่วนของ HR</asp:LinkButton>
                        </li>


                        <%--<li id="li2" runat="server" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <asp:Label ID="lbApprove" Font-Bold="true" runat="server"> รายการรออนุมัติ</asp:Label>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li id="liWaitApproveHeadUser" runat="server">

                                    <asp:LinkButton ID="lbWaitApproveHeadUser" runat="server" CommandName="cmdApproveOTShiftRotate" OnCommand="btnCommand" CommandArgument="12"> หัวหน้างาน</asp:LinkButton>
                                </li>

                                <li role="separator" class="divider"></li>

                                <li id="liWaitApproveHR" runat="server">

                                    <asp:LinkButton ID="lbWaitApproveHR" runat="server" CommandName="cmdApproveOTShiftRotate" OnCommand="btnCommand" CommandArgument="13"> เจ้าหน้าที่ฝ่ายทรัพยากรบุคคล</asp:LinkButton>
                                </li>

                                <li role="separator" class="divider"></li>
                                <li id="liWaitApproveAdminEdit" runat="server">

                                    <asp:LinkButton ID="lbWaitApproveAdminEdit" runat="server" CommandName="cmdApproveOTShiftRotate" OnCommand="btnCommand" CommandArgument="11"> Admin แก้ไขรายการ</asp:LinkButton>
                                </li>
                            </ul>

                        </li>--%>
                    </ul>

                    <ul class="nav navbar-nav navbar-right" runat="server">

                        <li id="_divFlowLiToDocument" runat="server">
                            <%--<asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                                NavigateUrl="https://docs.google.com/document/d/1qjlYvqPlYoq0uldvKZOVs7BQAl4emwGql_SSOipUNP8/edit?usp=sharing" Target="_blank" Text="คู่มือการใช้งาน" />--%>

                            <asp:HyperLink ID="hplFlowOvertime" NavigateUrl="https://drive.google.com/file/d/1SE2ymo8eZvcPjeOtBM1cEvwV_efcNoM6/view?usp=sharing" Target="_blank" runat="server" CommandName="cmdFlowOvertime" OnCommand="btnCommand"><i class="fas fa-book"></i> Flow การทำงาน</asp:HyperLink>
                        </li>

                        <li id="_divMenuLiToDocument" runat="server" visible="false">
                            <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                                NavigateUrl="https://docs.google.com/document/d/14BxmnEdxu18lEsSf9y_jrq8JtRTY7QXZ3orMzNtCqGE/edit?usp=sharing" Target="_blank" Text="คู่มือการใช้งาน" />
                        </li>
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->

            </div>
        </nav>
    </div>
    <!--tab menu-->

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug1" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View DetailAdmin-->
        <asp:View ID="docDetailAdmin" runat="server">
            <div class="col-md-12">

                <%-- <div style="overflow-y: scroll; overflow-x: hidden; height: 200px; width: 600px;">--%>

                <asp:GridView ID="GvDetailCreateToAdmin" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    ShowHeaderWhenEmpty="True"
                    AllowPaging="True"
                    PageSize="10"
                    BorderStyle="None"
                    CellSpacing="2">
                    <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <%# (Container.DataItemIndex + 1) %>
                                <asp:Label ID="lbl_u0_doc_idx_admin" runat="server" Visible="false" Text='<%# Eval("u0_doc_idx") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดผู้สร้าง" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="left">
                            <ItemTemplate>
                                <p>
                                    <b>รหัสพนักงาน :</b>

                                    <asp:Label ID="lbl_emp_code_admin" runat="server" Text='<%# Eval("emp_code") %>' />
                                </p>
                                <p>
                                    <b>ชื่อ-สกุลผู้สร้าง :</b>
                                    <asp:Label ID="lbl_cemp_idx_admin" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                    <asp:Label ID="lbl_emp_name_th_admin" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                </p>
                                <p>
                                    <b>องค์กร :</b>
                                    <asp:Label ID="lbl_org_name_th_admin" runat="server" Text='<%# Eval("org_name_th") %>' />
                                </p>
                                <p>
                                    <b>ฝ่าย :</b>
                                    <asp:Label ID="lbl_dept_name_th_admin" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                </p>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <p>
                                    <asp:Label ID="lbl_create_date_admin" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="lbl_time_create_date_admin" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label>
                                </p>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                            <ItemTemplate>

                                <asp:LinkButton ID="btnViewAdminShiftRotate" CssClass="btn btn-sm btn-info" target="" runat="server" CommandName="cmdViewAdminShiftRotate" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u0_doc_idx")%>' data-toggle="tooltip" title="ดูข้อมูล"><i class="far fa-file-alt"> ดูข้อมูล</i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

                <%--</div>--%>


                <asp:UpdatePanel ID="Panel_ViewDetailAdminShiftRotate" runat="server">
                    <ContentTemplate>

                        <div class="form-group">
                            <asp:LinkButton ID="btnBackToDetailAdmin" CssClass="btn btn-default" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                CommandName="cmdBackToDetailAdmin" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>
                        </div>

                        <asp:GridView ID="GvViewDetailCreateToAdmin" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            AllowPaging="True" PageSize="31"
                            ShowFooter="false">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">ไม่พบข้อมูล</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="ลำดับ" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <%# (Container.DataItemIndex + 1) %>

                                        <%-- <asp:CheckBox ID="chk_otday" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดพนักงาน" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <p>
                                            <b>รหัสพนักงาน :</b>

                                            <asp:Label ID="lbl_emp_code_viewadmin" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                            <asp:Label ID="lbl_emp_idx_viewadmin" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                            <asp:Label ID="lbl_STIDX_viewadmin" runat="server" Visible="false" Text='<%# Eval("STIDX") %>'></asp:Label>
                                            <asp:Label ID="lbl_m0_parttime_idx_viewadmin" runat="server" Visible="false" Text='<%# Eval("m0_parttime_idx") %>'></asp:Label>
                                            <%--<asp:Label ID="lbl_emp_type_idx_viewadmin" Visible="false" runat="server" Text='<%# Eval("emp_type_idx") %>'></asp:Label>--%>
                                        </p>
                                        <p>
                                            <b>ชื่อ-สกุล :</b>
                                            <asp:Label ID="lbl_emp_name_th_viewadmin" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                        </p>
                                        <p>
                                            <b>ตำแหน่ง :</b>

                                            <asp:Label ID="lbl_pos_name_th_viewadmin" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="กะการทำงาน" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <p>
                                            <b>scan-in :</b>
                                            <asp:Label ID="lbl_datetime_start_viewadmin" runat="server" Text='<%# Eval("datetime_start") %>'></asp:Label>
                                        </p>
                                        <p>
                                            <b>scan-out :</b>
                                            <asp:Label ID="lbl_datetime_end_viewadmin" runat="server" Text='<%# Eval("datetime_end") %>'></asp:Label>
                                        </p>
                                        <p>
                                            <b>กะ :</b>
                                            <asp:Label ID="lbl_parttime_name_th_viewadmin" runat="server" Text='<%# Eval("parttime_name_th") %>'></asp:Label>

                                            <asp:Label ID="lbl_parttime_start_time_viewadmin" Visible="false" runat="server" Text='<%# Eval("parttime_start_time") %>'></asp:Label>
                                            <asp:Label ID="lbl_parttime_end_time_viewadmin" Visible="false" runat="server" Text='<%# Eval("parttime_end_time") %>'></asp:Label>

                                            <%--    <span class="text-danger">
                                                <asp:Label ID="lbl_holiday_name" runat="server" Text='<%# Eval("holiday_name") %>'></asp:Label></span>--%>
                                            <%--<asp:Label ID="lbl_holiday_idx" Visible="false" runat="server" Text='<%# Eval("holiday_idx") %>'></asp:Label>--%>
                                            <asp:Label ID="lbl_day_off_viewadmin" Visible="false" runat="server" Text='<%# Eval("day_off") %>'></asp:Label>
                                            <asp:Label ID="lbl_date_condition_viewadmin" Visible="false" runat="server" Text='<%# Eval("date_condition") %>'></asp:Label>
                                            <%-- <asp:Label ID="lbl_time_shiftstart" Visible="false" runat="server"></asp:Label>
                                                <asp:Label ID="lbl_time_shiftend" Visible="false" runat="server"></asp:Label>
                                                <asp:Label ID="lbl_parttime_break_start_time" Visible="false" runat="server"></asp:Label>
                                                <asp:Label ID="lbl_parttime_break_end_time" Visible="false" runat="server"></asp:Label>--%>
                                        </p>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ot ก่อน" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_ot_before_viewadmin" Text='<%# Eval("ot_before") %>' runat="server"></asp:Label>
                                        <%--   <asp:DropDownList ID="ddl_timebefore_set" runat="server" CssClass="form-control" Visible="false">
                                            </asp:DropDownList>

                                            <asp:DropDownList ID="ddl_timebefore" runat="server" CssClass="form-control">
                                            </asp:DropDownList>--%>
                                        <asp:Label ID="lbl_hour_otbefore_viewadmin" Text='<%# Eval("hour_ot_before") %>' runat="server"></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชั่วโมงทำงานวันหยุด" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_ot_holiday_viewadmin" Text='<%# Eval("ot_holiday") %>' runat="server"></asp:Label>
                                        <%--<asp:DropDownList ID="ddl_timeholiday_set" runat="server" CssClass="form-control" Visible="false">
                                                            </asp:DropDownList>--%>

                                        <%-- <asp:DropDownList ID="ddl_timeholiday" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>--%>
                                        <asp:Label ID="lbl_hour_otholiday_viewadmin" runat="server" Text='<%# Eval("hour_ot_holiday") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ot หลัง" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <asp:Label ID="lbl_ot_after_viewadmin" runat="server" Text='<%# Eval("ot_after") %>'></asp:Label>
                                        <%--<asp:DropDownList ID="ddl_timeafter_set" runat="server" CssClass="form-control" Visible="false">
                                            </asp:DropDownList>

                                            <asp:DropDownList ID="ddl_timeafter" runat="server" CssClass="form-control">
                                            </asp:DropDownList>--%>
                                        <asp:Label ID="lbl_hour_otafter_viewadmin" runat="server" Text='<%# Eval("hour_ot_after") %>'></asp:Label>


                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <p>
                                            <b>ความเห็น(admin) :</b>
                                            <%--<asp:Label ID="lbl_remark_admin_viewadmin" Style="word-wrap:initial" Text='<%# Eval("remark_admin") %>' runat="server"></asp:Label>--%>
                                            <asp:TextBox ID="txt_remark_admin_viewadmin" Enabled="false" Text='<%# Eval("remark_admin") %>' runat="server" CssClass="form-control" Style="overflow: auto" Rows="2" TextMode="MultiLine" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>
                                        </p>

                                        <p>
                                            <b>ความเห็น(หัวหน้า) :</b>
                                            <%--<asp:Label ID="lbl_comment_head_viewadmin" Text='<%# Eval("comment_head") %>' runat="server"></asp:Label>--%>
                                            <asp:TextBox ID="txt_comment_head_viewadmin" Enabled="false" Text='<%# Eval("comment_head") %>' runat="server" CssClass="form-control" Style="overflow: auto" Rows="2" TextMode="MultiLine" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>
                                        </p>

                                        <p>
                                            <b>ความเห็น(HR) :</b>
                                            <%--<asp:Label ID="lbl_comment_hr_viewadmin"  Text='<%# Eval("comment_hr") %>' runat="server"></asp:Label>--%>
                                            <asp:TextBox ID="txt_comment_hr_viewadmin" Enabled="false" Text='<%# Eval("comment_hr") %>' runat="server" CssClass="form-control" Style="overflow: auto" Rows="2" TextMode="MultiLine" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>
                                        </p>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะ" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_staidx_detail" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                        <asp:Label ID="lbl_status_name_detail" runat="server" Text='<%# Eval("status_name") %>' />
                                        <asp:Label ID="lbl_m0_node_idx_detail" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_node_name_detail" runat="server" Text='<%# Eval("node_name") %>' />
                                        <br />
                                        โดย<br />
                                        <asp:Label ID="lbl_m0_actor_idx_detail" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_actor_name_detail" runat="server" Text='<%# Eval("actor_name") %>' />

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </asp:View>
        <!--View DetailAdmin-->

        <!--View Detail-->
        <asp:View ID="docDetail" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="_PanelSearchDetailUser" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหารายการ</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">scan-in</label>
                                        <div class="col-sm-4">
                                            <div class="input-group date">
                                                <asp:TextBox ID="txt_scanin_search" runat="server" placeholder="scan-in ..." CssClass="form-control datetimepicker-from cursor-pointer" />

                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                            </div>
                                        </div>

                                        <label class="col-sm-2 control-label">สถานะรายการ</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                            </asp:DropDownList>

                                        </div>

                                        <%--<label class="col-sm-2 control-label">scan-out</label>
                                        <div class="col-sm-4">
                                            <div class="input-group date">
                                                <asp:TextBox ID="txt_scanout_search" runat="server" placeholder="scan-out ..." CssClass="form-control datetimepicker-to cursor-pointer" />
                                                <span class="input-group-addon show-to-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">

                                            <asp:LinkButton ID="btnUserSearch" runat="server" CssClass="btn btn-primary" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="UserSearch" CommandName="cmdUserSearch" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                                            <asp:LinkButton ID="btnUserSearchRefresh" CssClass="btn btn-default" runat="server" data-original-title="ล้างค่า" data-toggle="tooltip" CommandName="cmdUserSearchRefresh" OnCommand="btnCommand"><span class="glyphicon glyphicon-refresh"></span> ล้างค่า</asp:LinkButton>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>


                <asp:GridView ID="GvDetailOTShiftRotate" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    AllowPaging="True" PageSize="10"
                    ShowFooter="false">
                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <%# (Container.DataItemIndex + 1) %>
                                <asp:Label ID="lbl_u0_doc_idx_detail" Visible="false" runat="server" Text='<%# Eval("u0_doc_idx") %>'></asp:Label>
                                <asp:Label ID="lbl_u1_doc_idx_detail" Visible="false" runat="server" Text='<%# Eval("u1_doc_idx") %>'></asp:Label>

                                <%-- <asp:CheckBox ID="chk_otday" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดพนักงาน" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <p>
                                    <b>รหัสพนักงาน :</b>

                                    <asp:Label ID="lbl_emp_code_detail" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    <asp:Label ID="lbl_emp_idx_detail" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                    <asp:Label ID="lbl_STIDX_detail" runat="server" Visible="false" Text='<%# Eval("STIDX") %>'></asp:Label>
                                    <asp:Label ID="lbl_m0_parttime_idx_detail" runat="server" Visible="false" Text='<%# Eval("m0_parttime_idx") %>'></asp:Label>

                                    <asp:Label ID="lbl_datetime_otstart_detail" runat="server" Visible="false" Text='<%# Eval("datetime_otstart") %>'></asp:Label>
                                    <%--<asp:Label ID="lbl_emp_type_idx_viewadmin" Visible="false" runat="server" Text='<%# Eval("emp_type_idx") %>'></asp:Label>--%>
                                </p>
                                <p>
                                    <b>ชื่อ-สกุล :</b>
                                    <asp:Label ID="lbl_emp_name_th_detail" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                </p>
                                <p>
                                    <b>ตำแหน่ง :</b>

                                    <asp:Label ID="lbl_pos_name_th_detail" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                </p>

                                <p>
                                    <b>กะ :</b>
                                    <asp:Label ID="lbl_parttime_name_th_detail" runat="server" Text='<%# Eval("parttime_name_th") %>'></asp:Label>

                                    <asp:Label ID="lbl_parttime_start_time_detail" Visible="false" runat="server" Text='<%# Eval("parttime_start_time") %>'></asp:Label>
                                    <asp:Label ID="lbl_parttime_end_time_detail" Visible="false" runat="server" Text='<%# Eval("parttime_end_time") %>'></asp:Label>

                                    <asp:Label ID="lbl_detailholiday_detail" runat="server"><span class="text-danger">*(HH)</span></asp:Label>
                                    <asp:Label ID="lbl_detaildayoff_detail" runat="server"><span class="text-danger">*(DH)</span></asp:Label>


                                    <asp:Label ID="lbl_day_off_detail" Visible="false" runat="server" Text='<%# Eval("day_off") %>'></asp:Label>
                                    <asp:Label ID="lbl_date_condition_detail" Visible="false" runat="server" Text='<%# Eval("date_condition") %>'></asp:Label>
                                    <asp:Label ID="lbl_holiday_idx_detail" Visible="false" runat="server" Text='<%# Eval("holiday_idx") %>'></asp:Label>

                                </p>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="scan-in" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="lbl_datetime_start_detail" runat="server" Text='<%# Eval("datetime_start") %>'></asp:Label>
                                <%-- <p>
                                    <b>scan-in :</b>
                                    <asp:Label ID="lbl_datetime_start_detail" runat="server" Text='<%# Eval("datetime_start") %>'></asp:Label>
                                </p>
                                <p>
                                    <b>scan-out :</b>
                                    <asp:Label ID="lbl_datetime_end_detail" runat="server" Text='<%# Eval("datetime_end") %>'></asp:Label>
                                </p>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="scan-out" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="lbl_datetime_end_detail" runat="server" Text='<%# Eval("datetime_end") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <%--  <asp:TemplateField HeaderText="ot ก่อน" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="lbl_ot_before_detail" Text='<%# Eval("ot_before") %>' runat="server"></asp:Label>
                                
                                <asp:Label ID="lbl_hour_otbefore_detail" Text='<%# Eval("hour_ot_before") %>' runat="server"></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ot หลัง" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>

                                <asp:Label ID="lbl_ot_after_detail" runat="server" Text='<%# Eval("ot_after") %>'></asp:Label>
                               
                                <asp:Label ID="lbl_hour_otafter_detail" runat="server" Text='<%# Eval("hour_ot_after") %>'></asp:Label>


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ot วันหยุด" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="lbl_ot_holiday_detail" Text='<%# Eval("ot_holiday") %>' runat="server"></asp:Label>
                              
                                <asp:Label ID="lbl_hour_otholiday_detail" runat="server" Text='<%# Eval("hour_ot_holiday") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>--%>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>

                                <asp:Label ID="lbl_staidx_detail" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                <asp:Label ID="lbl_status_name_detail" runat="server" Text='<%# Eval("status_name") %>' />
                                <asp:Label ID="lbl_m0_node_idx_detail" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                <asp:Label ID="lbl_node_name_detail" runat="server" Text='<%# Eval("node_name") %>' />
                                <br />
                                โดย<br />
                                <asp:Label ID="lbl_m0_actor_idx_detail" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                <asp:Label ID="lbl_actor_name_detail" runat="server" Text='<%# Eval("actor_name") %>' />


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ดำเนินการ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnViewUserShiftRotate" CssClass="btn btn-sm btn-info" target="" runat="server" CommandName="cmdViewUserShiftRotate" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u0_doc_idx") + ";" + Eval("u1_doc_idx")%>' data-toggle="tooltip" title="รายละเอียด"><i class="far fa-file"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvEmpCreateDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfu1_doc_idx" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <asp:HiddenField ID="hfu0_doc_idx" runat="server" Value='<%# Eval("u0_doc_idx") %>' />

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดรายการ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ผู้อนุมัติลำดับที่ 1</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbemp_approve1" runat="server" CssClass="form-control" Text='<%# Eval("emp_approve1") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ผู้อนุมัติลำดับที่ 2</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbemp_approve2" runat="server" CssClass="form-control" Text='<%# Eval("emp_approve2") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">scan-in</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbdatetime_start" runat="server" CssClass="form-control" Text='<%# Eval("datetime_start") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">scan-out</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbdatetime_end" runat="server" CssClass="form-control" Text='<%# Eval("datetime_end") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">กะ</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbparttime_name_th" runat="server" CssClass="form-control" Text='<%# Eval("parttime_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">break-time</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbparttime_break" runat="server" CssClass="form-control" Text='<%# Eval("parttime_break_start_time") + " " + "-" + " " +  Eval("parttime_break_end_time") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ot ก่อน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbot_before" runat="server" CssClass="form-control" Text='<%# Eval("ot_before") + " " +  Eval("hour_ot_before") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ot หลัง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbot_after" runat="server" CssClass="form-control" Text='<%# Eval("ot_after") + " " +  Eval("hour_ot_after")%>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ot วันหยุด</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbot_holiday" runat="server" CssClass="form-control" Text='<%# Eval("ot_holiday") + " " +  Eval("hour_ot_holiday") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-6 control-label"></label>
                                        <%--  <div class="col-sm-4">
                                            <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("ot_after") + " " +  Eval("hour_ot_after")%>' Enabled="false" />
                                        </div>--%>
                                    </div>

                                    <hr />

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment(ผู้สร้าง)</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbremark_admin" TextMode="MultiLine" Rows="2" Style="overflow: auto" runat="server" CssClass="form-control" Text='<%# Eval("remark_admin")%>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Comment(หัวหน้างาน)</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbcomment_head" TextMode="MultiLine" Rows="2" Style="overflow: auto" runat="server" CssClass="form-control" Text='<%# Eval("comment_head")%>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment(เจ้าหน้าที่ฝ่ายบุคคล)</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbcomment_hr" TextMode="MultiLine" Rows="2" Style="overflow: auto" runat="server" CssClass="form-control" Text='<%# Eval("comment_hr")%>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-6 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->

                <!-- Log Detail OT Shifttime Rotate -->
                <div class="panel panel-info" id="div_LogViewDetailOTShiftRotate" runat="server" visible="false">
                    <div class="panel-heading">ประวัติการดำเนินการ</div>
                    <table class="table table-striped f-s-12 table-empshift-responsive">
                        <asp:Repeater ID="rptLogViewDetailOTShiftRotate" runat="server">
                            <HeaderTemplate>
                                <tr>
                                    <th>วัน / เวลา</th>
                                    <th>ผู้ดำเนินการ</th>
                                    <th>ดำเนินการ</th>
                                    <%--<th>ผลการดำเนินการ</th>--%>
                                    <th>เหตุผล</th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                    <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_artor") %> )</td>
                                    <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                                    <td data-th="เหตุผล"><%# Eval("comment") %></td>
                                    <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div class="m-t-10 m-b-10"></div>
                </div>
                <!-- Log Detail OT Shifttime Rotate -->

            </div>
        </asp:View>
        <!--View Detail-->

        <!--View Create-->
        <asp:View ID="docCreate" runat="server">
            <div class="col-md-12">

                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->

                <!-- Panel Admin CreateOT -->
                <asp:UpdatePanel ID="PanelAdminCreateOT" runat="server">
                    <ContentTemplate>

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">สร้างรายการ OT (กะหมุนเวียน)</h3>
                            </div>

                            <div class="panel-body">

                                <!-- Search Date -->
                                <div class="col-md-10 col-md-offset-1">


                                    <div class="col-md-6">
                                        <div class="form-group">

                                            <label>
                                                เลือกวันที่สร้างรายการ <font color="red"> **</font>
                                            </label>
                                            <div class="input-group date">

                                                <asp:TextBox ID="txtDateStart_create" runat="server" placeholder="เลือกวันที่..." CssClass="form-control datetimepicker-fromsearch cursor-pointer" value="" />

                                                <span class="input-group-addon show-fromsearch-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>



                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ประเภทพนักงาน</label>

                                            <asp:DropDownList ID="ddlemptypeCreate" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">--- เลือกประเภทพนักงาน ---</asp:ListItem>
                                                <asp:ListItem Value="1">รายวัน</asp:ListItem>
                                                <asp:ListItem Value="2">รายเดือน</asp:ListItem>

                                            </asp:DropDownList>


                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                องค์กร <font color="red"> **</font>
                                            </label>
                                            <asp:DropDownList ID="ddlorgCreate" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />

                                            <asp:RequiredFieldValidator ID="Required_ddlorgCreate"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlorgCreate" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกองค์กร"
                                                ValidationGroup="SearchCreate" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_ddlorgCreate" Width="160" PopupPosition="BottomLeft" />


                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ฝ่าย</label>
                                            <asp:DropDownList ID="ddlrdeptCreate" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                แผนก
                                            </label>
                                            <asp:DropDownList ID="ddlrsecCreate" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                ตำแหน่ง
                                            </label>
                                            <asp:DropDownList ID="ddlrposCreate" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                รหัสพนักงาน
                                            </label>
                                            <asp:TextBox ID="txt_empcodeCreate" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" placeholder="กรอกรหัสพนักงาน Ex.(58000106,58000107)..." CssClass="form-control" />


                                            <%--  <asp:RequiredFieldValidator ID="req_txt_empcodeCreate"
                                                ValidationGroup="addContactToList" runat="server"
                                                Display="None"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtEmpVisitorIDCard"
                                                Font-Size="13px" ForeColor="Red"
                                                ErrorMessage="กรุณากรอกเลขบัตรประชาชน" />
                                            
                                            <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredTxtEmpVisitorIDCard" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="requiredTxtEmpVisitorIDCard" Width="160" PopupPosition="BottomRight" />--%>

                                            <%--<asp:RegularExpressionValidator ID="reg_txt_empcodeCreate" runat="server"
                                                ValidationGroup="SearchCreate"
                                                Display="None"
                                                SetFocusOnError="true"
                                                ControlToValidate="txt_empcodeCreate"
                                                Font-Size="13px" ForeColor="Red"
                                                ValidationExpression="^[0-9]{8}$"
                                                ErrorMessage="กรอกรหัสพนักงานไม่ถูกต้อง" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRegExTxtEmpVisitorIDCard" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="reg_txt_empcodeCreate" Width="160" PopupPosition="BottomRight" />--%>
                                        </div>
                                    </div>


                                    <asp:UpdatePanel ID="_Panel_Employee" runat="server" Visible="false">
                                        <ContentTemplate>
                                            <div class="col-md-12">
                                                <div style="overflow-y: scroll; width: 100%; height: 300px">
                                                    <div class="form-group">
                                                        <label>
                                                            พนักงาน 
                                                        </label>
                                                        <asp:CheckBoxList ID="chkempCreate" runat="server" CellPadding="5" CellSpacing="5" RepeatColumns="4" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" Style="overflow: auto;" />

                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <%--<label>&nbsp;</label>--%>
                                            <div class="clearfix"></div>
                                            <asp:LinkButton ID="btnSearchDate" runat="server" CssClass="btn btn-primary" Text="ค้นหา" data-original-title="ค้นหา" data-toggle="tooltip" OnCommand="btnCommand" ValidationGroup="SearchCreate" CommandName="cmdSearchDate"> <span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>
                                            <asp:LinkButton ID="btnClearSearch" runat="server" CssClass="btn btn-default" data-original-title="ล้างค่า" data-toggle="tooltip" Text="ล้างค่า" OnCommand="btnCommand" CommandName="cmdClearSearch"> <span class="glyphicon glyphicon-refresh"></span> ล้างค่า</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                                <!-- Search Date -->

                                <!-- Detail OT Create -->
                                <asp:UpdatePanel ID="PanelDetailOTCreate" runat="server">
                                    <ContentTemplate>
                                        <div class="col-md-12">
                                            <div id="divscroll_GvCreateShiftRotate" style="overflow-x: auto; width: 100%" runat="server">
                                                <asp:GridView ID="GvCreateShiftRotate" runat="server"
                                                    AutoGenerateColumns="False"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    OnRowDataBound="gvRowDataBound"
                                                    ShowHeaderWhenEmpty="False"
                                                    AllowPaging="False"
                                                    PageSize="2"
                                                    ShowFooter="False">
                                                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="ลำดับ" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">

                                                            <HeaderTemplate>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <asp:UpdatePanel ID="_PanelChkCreateAll" runat="server">
                                                                            <ContentTemplate>
                                                                                <small>
                                                                                    <asp:CheckBox ID="chkallCreate" Enabled="false" runat="server" Text=" เลือก" Font-Size="Small"
                                                                                        OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>
                                                                                </small>

                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </div>
                                                                </div>
                                                            </HeaderTemplate>

                                                            <ItemTemplate>

                                                                <asp:CheckBox ID="chk_otday" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="รายละเอียดพนักงาน" ItemStyle-Width="22%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <p>
                                                                    <b>รหัสพนักงาน :</b>
                                                                    <asp:Label ID="lbl_org_idx" Visible="false" runat="server" Text='<%# Eval("org_idx") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_emp_code_otday" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_emp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_STIDX" runat="server" Visible="false" Text='<%# Eval("STIDX") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_m0_parttime_idx" runat="server" Visible="false" Text='<%# Eval("m0_parttime_idx") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_emp_type_idx" Visible="false" runat="server" Text='<%# Eval("emp_type_idx") %>'></asp:Label>

                                                                </p>
                                                                <p>
                                                                    <b>ชื่อ-สกุล :</b>
                                                                    <asp:Label ID="lbl_emp_name_th_otday" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                                </p>
                                                                <p>
                                                                    <b>ตำแหน่ง :</b>

                                                                    <asp:Label ID="lbl_pos_name_th_otday" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                                </p>
                                                                <p>
                                                                    <b>กะ :</b>
                                                                    <asp:Label ID="lbl_parttime_name_th" runat="server" Text='<%# Eval("parttime_name_th") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_parttime_start_time" Visible="false" runat="server" Text='<%# Eval("parttime_start_time") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_parttime_end_time" Visible="false" runat="server" Text='<%# Eval("parttime_end_time") %>'></asp:Label>

                                                                    <asp:Label ID="lbl_detailholiday" runat="server"><span class="text-danger">*(HH)</span></asp:Label>
                                                                    <asp:Label ID="lbl_detaildayoff" runat="server"><span class="text-danger">*(DH)</span></asp:Label>
                                                                </p>
                                                                <p>
                                                                    <asp:Label ID="lbl_holiday_idx" Visible="false" runat="server" Text='<%# Eval("holiday_idx") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_day_off" Visible="false" runat="server" Text='<%# Eval("day_off") %>'></asp:Label>

                                                                    <asp:Label ID="lbl_date_condition" Visible="false" runat="server" Text='<%# Eval("date_condition") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_time_shiftstart" Visible="false" runat="server"></asp:Label>
                                                                    <asp:Label ID="lbl_time_shiftend" Visible="false" runat="server"></asp:Label>
                                                                    <asp:Label ID="lbl_parttime_break_start_time" Visible="false" runat="server" Text='<%# Eval("parttime_break_start_time") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_parttime_break_end_time" Visible="false" runat="server" Text='<%# Eval("parttime_break_end_time") %>'></asp:Label>
                                                                </p>


                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="เวลาเริ่มทำ" ItemStyle-Width="15%" HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="Update_Timestart" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:DropDownList ID="ddl_timestart" runat="server" Width="160px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>

                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="เวลาเลิกทำ" ItemStyle-Width="15%" HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="Update_Timeend" runat="server">
                                                                    <ContentTemplate>

                                                                        <asp:DropDownList ID="ddl_timeend" runat="server" Width="160px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                        </asp:DropDownList>

                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>

                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="กะการทำงาน" ItemStyle-HorizontalAlign="left" Visible="false" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <p>
                                                                    <b>scan-in :</b>
                                                                    <asp:Label ID="lbl_scan_in" runat="server"></asp:Label>
                                                                </p>
                                                                <p>
                                                                    <b>scan-out :</b>
                                                                    <asp:Label ID="lbl_scan_out" runat="server"></asp:Label>
                                                                </p>
                                                                <p>

                                                                    <span class="text-danger">
                                                                        <asp:Label ID="lbl_holiday_name" runat="server" Text='<%# Eval("holiday_name") %>'></asp:Label></span>

                                                                </p>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ot ก่อน" ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_ot_before" Visible="false" runat="server"></asp:Label>
                                                                <asp:DropDownList ID="ddl_timebefore_set" runat="server" CssClass="form-control" Visible="false">
                                                                </asp:DropDownList>

                                                                <asp:DropDownList ID="ddl_timebefore" Width="100px" runat="server" CssClass="form-control">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lbl_hour_otbefore" runat="server"></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ชั่วโมงทำงานวันหยุด" ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_ot_holiday" Visible="false" runat="server"></asp:Label>
                                                                <%--<asp:DropDownList ID="ddl_timeholiday_set" runat="server" CssClass="form-control" Visible="false">
                                                            </asp:DropDownList>--%>

                                                                <asp:DropDownList ID="ddl_timeholiday" runat="server" Width="100px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lbl_hour_otholiday" runat="server"></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ot หลัง" ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lbl_ot_after" Visible="false" runat="server"></asp:Label>
                                                                <asp:DropDownList ID="ddl_timeafter_set" runat="server" CssClass="form-control" Visible="false">
                                                                </asp:DropDownList>

                                                                <asp:DropDownList ID="ddl_timeafter" Width="100px" runat="server" CssClass="form-control">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lbl_hour_otafter" runat="server"></asp:Label>


                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-Width="10%" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>

                                                                <asp:TextBox ID="txt_remark_shift_otholiday" runat="server" CssClass="form-control" Style="overflow: auto" Rows="2" TextMode="MultiLine" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>

                                            <div id="div_SaveOTRote" runat="server">
                                                <%-- <label>&nbsp;</label>
                                                <div class="clearfix"></div>--%>
                                                <div class="form-group pull-right">
                                                    <asp:LinkButton ID="btnSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdSave" ValidationGroup="SaveOTRotate"> <i class="fas fa-save"></i> บันทึก</asp:LinkButton>

                                                    <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdCancel"> <i class="fas fa-times"></i> ยกเลิก</asp:LinkButton>
                                                </div>
                                            </div>

                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <!-- Detail OT Create -->

                            </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Panel Admin CreateOT -->

            </div>
        </asp:View>
        <!--View Create-->

        <!--View Approve-->
        <asp:View ID="docApprove" runat="server">
            <div class="col-md-12">


                <asp:UpdatePanel ID="_PanelSearchWaitApprove" runat="server">
                    <ContentTemplate>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหารายการ</h3>
                            </div>

                            <div class="panel-body">

                                <!-- Search Date -->
                                <div class="col-md-10 col-md-offset-1">


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                เลือกวันที่ขอโอที <font color="red"> **</font>
                                            </label>
                                            <div class="input-group date">

                                                <asp:TextBox ID="txt_dateStart_approve" runat="server" placeholder="เลือกวันที่..." CssClass="form-control datetimepicker-fromsearchapprove cursor-pointer" value="" />

                                                <span class="input-group-addon show-from-onclicksearchapprove">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                                <asp:RequiredFieldValidator ID="Req_txt_dateStart_approve"
                                                    runat="server" ControlToValidate="txt_dateStart_approve" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกวันที่"
                                                    ValidationGroup="SearchWaitApprove" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txt_dateStart_approve" Width="160" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                ถึงวันที่ <font color="red"> **</font>
                                            </label>
                                            <div class="input-group date">

                                                <asp:TextBox ID="txt_dateEnd_approve" runat="server" placeholder="เลือกวันที่..." CssClass="form-control datetimepicker-tosearchapprove cursor-pointer" value="" />

                                                <span class="input-group-addon show-to-onclicksearchapprove">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                                <asp:RequiredFieldValidator ID="Req_txt_dateEnd_approve"
                                                    runat="server" ControlToValidate="txt_dateEnd_approve" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกวันที่"
                                                    ValidationGroup="SearchWaitApprove" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txt_dateEnd_approve" Width="160" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ประเภทพนักงาน</label>

                                            <asp:DropDownList ID="ddlemptypeApprove" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">--- เลือกประเภทพนักงาน ---</asp:ListItem>
                                                <asp:ListItem Value="1">รายวัน</asp:ListItem>
                                                <asp:ListItem Value="2">รายเดือน</asp:ListItem>

                                            </asp:DropDownList>


                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>สังกัด</label>

                                            <asp:DropDownList ID="ddl_Affiliation" runat="server" CssClass="form-control">
                                                <%--<asp:ListItem Value="0">--- เลือกสังกัด ---</asp:ListItem>--%>
                                            </asp:DropDownList>


                                        </div>
                                    </div>

                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                องค์กร <font color="red"> **</font>
                                            </label>
                                            <asp:DropDownList ID="ddlorgApprove" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />

                                            <asp:RequiredFieldValidator ID="Req_ddlorgApprove"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlorgApprove" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกองค์กร"
                                                ValidationGroup="SearchWaitApprove" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlorgApprove" Width="160" PopupPosition="BottomLeft" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ฝ่าย</label>
                                            <asp:DropDownList ID="ddlrdeptApprove" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                แผนก
                                            </label>
                                            <asp:DropDownList ID="ddlrsecApprove" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>
                                                ตำแหน่ง
                                            </label>
                                            <asp:DropDownList ID="ddlrposApprove" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">

                                            <label>
                                                รหัสพนักงาน <%--<font color="red"> ** Ex.(58000106,58000107)</font>--%>
                                            </label>
                                            
                                            <asp:TextBox ID="txt_empcodeApprove" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" placeholder="กรอกรหัสพนักงาน Ex.(58000106,58000107)..." CssClass="form-control" />


                                        </div>
                                    </div>




                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <%--<label>&nbsp;</label>--%>
                                            <div class="clearfix"></div>
                                            <asp:LinkButton ID="btnSearchWaitApprove" runat="server" CssClass="btn btn-primary" Text="ค้นหา" data-original-title="ค้นหา" data-toggle="tooltip" OnCommand="btnCommand" ValidationGroup="SearchWaitApprove" CommandName="cmdSearchWaitApprove"> <span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>
                                            <asp:LinkButton ID="btnClearSearchWaitApprove" runat="server" CssClass="btn btn-default" data-original-title="ล้างค่า" data-toggle="tooltip" Text="ล้างค่า" OnCommand="btnCommand" CommandName="cmdClearSearchWaitApprove"> <span class="glyphicon glyphicon-refresh"></span> ล้างค่า</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                                <!-- Search Date -->

                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>


                <asp:UpdatePanel ID="_PanelbtnApprove" runat="server">
                    <ContentTemplate>
                        <%--<label>&nbsp;</label>
                        <div class="clearfix"></div>--%>
                        <div class="form-group pull-left">

                            <asp:Repeater ID="rptBindbtnApprove" OnItemDataBound="rptOnRowDataBound" runat="server">
                                <ItemTemplate>
                                    <asp:Label ID="lbcheck_coler_approve" runat="server" Visible="false" Text='<%# Eval("decision_idx") %>'></asp:Label>
                                    <asp:LinkButton ID="btnApprove" CssClass="btn btn-primary" runat="server" data-original-title='<%# Eval("decision_name") %>' data-toggle="tooltip" CommandArgument='<%# Eval("decision_idx")%>' OnCommand="btnCommand" Text='<%# Eval("decision_name") %>' CommandName="cmdSaveApprove"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:Repeater>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>





                <asp:UpdatePanel ID="_PanelbtnAdmin_Edit" runat="server">
                    <ContentTemplate>

                        <div class="form-group pull-left">
                            <asp:LinkButton ID="btnSaveAdminEdit" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึกการเปลี่ยนแปลง" OnCommand="btnCommand" CommandArgument="17" CommandName="cmdSaveAdminEdit"></asp:LinkButton>

                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>



                <!-- Detail OT WaitApprove -->
                <asp:UpdatePanel ID="_PanelWaitApprove" runat="server">
                    <ContentTemplate>
                        <div id="div_scrollGvWaitApprove" style="overflow-x: auto; width: 100%" runat="server">
                            <asp:GridView ID="GvWaitApprove" runat="server"
                                AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound"
                                AllowPaging="False" PageSize="2"
                                ShowFooter="False">
                                <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">

                                        <HeaderTemplate>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:UpdatePanel ID="_PanelApproveAll" runat="server">
                                                        <ContentTemplate>
                                                            <small>
                                                                <asp:CheckBox ID="chkallApprove" runat="server" Text=" เลือก" Font-Size="Small" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>
                                                            </small>

                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>

                                        </HeaderTemplate>

                                        <ItemTemplate>

                                            <asp:CheckBox ID="chk_ot_approve" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>
                                            <asp:Label ID="lbl_u1_doc_idx_approve" Visible="false" runat="server" Text='<%# Eval("u1_doc_idx") %>'></asp:Label>
                                            <asp:Label ID="lbl_u0_doc_idx_approve" Visible="false" runat="server" Text='<%# Eval("u0_doc_idx") %>'></asp:Label>

                                            <asp:Label ID="lbl_m0_node_idx_approve" Visible="false" runat="server" Text='<%# Eval("m0_node_idx") %>'></asp:Label>
                                            <asp:Label ID="lbl_m0_actor_idx_approve" Visible="false" runat="server" Text='<%# Eval("m0_actor_idx") %>'></asp:Label>
                                            <asp:Label ID="lbl_staidx_approve" Visible="false" runat="server" Text='<%# Eval("staidx") %>'></asp:Label>

                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียดพนักงาน" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <p>
                                                <b>รหัสพนักงาน :</b>

                                                <asp:Label ID="lbl_emp_code_approve" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                <asp:Label ID="lbl_emp_idx_approve" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_STIDX_approve" runat="server" Visible="false" Text='<%# Eval("STIDX") %>'></asp:Label>
                                                <asp:Label ID="lbl_m0_parttime_idx_approve" runat="server" Visible="false" Text='<%# Eval("m0_parttime_idx") %>'></asp:Label>
                                                <%-- <asp:Label ID="lbl_emp_type_idx_approve" Visible="false" runat="server" Text='<%# Eval("emp_type_idx") %>'></asp:Label>--%>
                                            </p>
                                            <p>
                                                <b>ชื่อ-สกุล :</b>
                                                <asp:Label ID="lbl_emp_name_th_approve" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <b>ตำแหน่ง :</b>

                                                <asp:Label ID="lbl_pos_name_th_approve" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <b>กะ :</b>
                                                <asp:Label ID="lbl_parttime_name_th_approve" runat="server" Text='<%# Eval("parttime_name_th") %>'></asp:Label>
                                                <asp:Label ID="lbl_parttime_start_time_approve" Visible="false" runat="server" Text='<%# Eval("parttime_start_time") %>'></asp:Label>
                                                <asp:Label ID="lbl_parttime_end_time_approve" Visible="false" runat="server" Text='<%# Eval("parttime_end_time") %>'></asp:Label>

                                                <asp:Label ID="lbl_holiday_name_approve" runat="server"><span class="text-danger">*(HH)</span></asp:Label>
                                                <asp:Label ID="lbl_detaildayoff_approve" runat="server"><span class="text-danger">*(DH)</span></asp:Label>

                                                <%--<span class="text-danger">
                                                <asp:Label ID="lbl_holiday_name_approve" runat="server" Text='<%# Eval("holiday_name") %>'></asp:Label></span>--%>
                                            </p>

                                            <p>
                                                <asp:Label ID="lbl_holiday_idx_approve" Visible="false" runat="server" Text='<%# Eval("holiday_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_day_off_approve" Visible="false" runat="server" Text='<%# Eval("day_off") %>'></asp:Label>
                                                <asp:Label ID="lbl_date_condition_approve" Visible="false" runat="server" Text='<%# Eval("date_condition") %>'></asp:Label>

                                                <asp:Label ID="lbl_time_shiftstart_approve" Visible="false" runat="server"></asp:Label>
                                                <asp:Label ID="lbl_time_shiftend_approve" Visible="false" runat="server"></asp:Label>
                                                <asp:Label ID="lbl_parttime_break_start_time_approve" Visible="false" Text='<%# Eval("parttime_break_start_time") %>' runat="server"></asp:Label>
                                                <asp:Label ID="lbl_parttime_break_end_time_approve" Visible="false" Text='<%# Eval("parttime_break_end_time") %>' runat="server"></asp:Label>
                                            </p>


                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เวลาเริ่มทำ" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="Update_Timestart_approve" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="lbl_datetime_otstart_approve" Visible="false" runat="server" Text='<%# Eval("datetime_otstart") %>'></asp:Label>
                                                    <asp:Label ID="lbl_ddl_timestart_approve" Text='<%# Eval("datetime_start") %>' runat="server"></asp:Label>
                                                    <asp:DropDownList ID="ddl_timestart_approve" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เวลาเลิกทำ" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="Update_Timeend_approve" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="lbl_ddl_timeend_approve" Text='<%# Eval("datetime_end") %>' runat="server"></asp:Label>
                                                    <asp:DropDownList ID="ddl_timeend_approve" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="กะการทำงาน" Visible="false" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <p>
                                                <b>scan-in :</b>
                                                <asp:Label ID="lbl_scan_in_approve" Text='<%# Eval("datetime_start") %>' runat="server"></asp:Label>
                                            </p>
                                            <p>
                                                <b>scan-out :</b>
                                                <asp:Label ID="lbl_scan_out_approve" Text='<%# Eval("datetime_end") %>' runat="server"></asp:Label>
                                            </p>


                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ot ก่อน" ItemStyle-Width="10%" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ot_before_approve_max" Text='<%# Eval("ot_before_max") %>' Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_ot_before_approve" Text='<%# Eval("ot_before") %>' Visible="false" runat="server"></asp:Label>


                                            <asp:DropDownList ID="ddl_timebefore_approve" Width="100px" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:Label ID="lbl_hour_otbefore_approve" Text='<%# Eval("hour_ot_before") %>' runat="server"></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชั่วโมงทำงานวันหยุด" ItemStyle-Width="10%" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ot_holiday_max" Text='<%# Eval("ot_holiday_max") %>' Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_ot_holiday_approve" Visible="false" Text='<%# Eval("ot_holiday") %>' runat="server"></asp:Label>


                                            <asp:DropDownList ID="ddl_timeholiday_approve" Width="100px" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:Label ID="lbl_hour_otholiday_approve" Text='<%# Eval("hour_ot_holiday") %>' runat="server"></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ot หลัง" ItemStyle-Width="10%" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ot_after_max" Text='<%# Eval("ot_after_max") %>' Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_ot_after_approve" Visible="false" Text='<%# Eval("ot_after") %>' runat="server"></asp:Label>

                                            <asp:DropDownList ID="ddl_timeafter_approve" Width="100px" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:Label ID="lbl_hour_otafter_approve" Text='<%# Eval("hour_ot_after") %>' runat="server"></asp:Label>


                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <p>
                                                <b>ความเห็น(ผู้สร้าง) :</b>
                                                <asp:TextBox ID="txt_remark_admin_edit" Text='<%# Eval("remark_admin") %>' runat="server" CssClass="form-control" Style="overflow: auto" Rows="2" TextMode="MultiLine" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>

                                            </p>

                                            <p>
                                                <b>ความเห็น(หัวหน้า) :</b>
                                                <asp:TextBox ID="txt_comment_head_approve" Text='<%# Eval("comment_head") %>' runat="server" CssClass="form-control" Style="overflow: auto" Rows="2" TextMode="MultiLine" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>
                                            </p>

                                            <p>
                                                <b>ความเห็น(HR) :</b>
                                                <asp:TextBox ID="txt_comment_hr_check" Text='<%# Eval("comment_hr") %>' runat="server" CssClass="form-control" Style="overflow: auto" Rows="2" TextMode="MultiLine" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Detail OT WaitApprove -->

                <div id="div_scrollGvWaitAdminEditDoc" style="overflow-x: auto; width: 100%" runat="server">
                    <asp:GridView ID="GvWaitAdminEditDoc" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        ShowHeaderWhenEmpty="False"
                        AllowPaging="False"
                        PageSize="2"
                        ShowFooter="False">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="ลำดับ" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">

                                <HeaderTemplate>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:UpdatePanel ID="_PanelChkCreateAll_edit" runat="server">
                                                <ContentTemplate>
                                                    <small>
                                                        <asp:CheckBox ID="chkall_edit" runat="server" Text=" เลือก" Font-Size="Small" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>
                                                    </small>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </HeaderTemplate>

                                <ItemTemplate>

                                    <asp:CheckBox ID="chk_edit" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>

                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียดพนักงาน" ItemStyle-Width="22%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <p>
                                        <b>รหัสพนักงาน :</b>
                                        <asp:Label ID="lbl_u1_doc_idx_edit" Visible="false" runat="server" Text='<%# Eval("u1_doc_idx") %>'></asp:Label>
                                        <asp:Label ID="lbl_u0_doc_idx_edit" Visible="false" runat="server" Text='<%# Eval("u0_doc_idx") %>'></asp:Label>


                                        <asp:Label ID="lbl_m0_node_idx_edit" Visible="false" runat="server" Text='<%# Eval("m0_node_idx") %>'></asp:Label>
                                        <asp:Label ID="lbl_m0_actor_idx_edit" Visible="false" runat="server" Text='<%# Eval("m0_actor_idx") %>'></asp:Label>
                                        <asp:Label ID="lbl_staidx_edit" Visible="false" runat="server" Text='<%# Eval("staidx") %>'></asp:Label>


                                        <asp:Label ID="lbl_emp_code_otday_edit" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                        <asp:Label ID="lbl_emp_idx_edit" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                        <asp:Label ID="lbl_STIDX_edit" runat="server" Visible="false" Text='<%# Eval("STIDX") %>'></asp:Label>
                                        <asp:Label ID="lbl_m0_parttime_idx_edit" runat="server" Visible="false" Text='<%# Eval("m0_parttime_idx") %>'></asp:Label>
                                        <asp:Label ID="lbl_emp_type_idx_edit" Visible="false" runat="server" Text='<%# Eval("emp_type_idx") %>'></asp:Label>
                                        <asp:Label ID="lbl_datetime_otstart_edit" Visible="false" runat="server" Text='<%# Eval("datetime_otstart") %>'></asp:Label>
                                    </p>
                                    <p>
                                        <b>ชื่อ-สกุล :</b>
                                        <asp:Label ID="lbl_emp_name_th_otday_edit" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                    </p>
                                    <p>
                                        <b>ตำแหน่ง :</b>

                                        <asp:Label ID="lbl_pos_name_th_otday_edit" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                    </p>
                                    <p>
                                        <b>กะ :</b>
                                        <asp:Label ID="lbl_parttime_name_th_edit" runat="server" Text='<%# Eval("parttime_name_th") %>'></asp:Label>
                                        <asp:Label ID="lbl_parttime_start_time_edit" Visible="false" runat="server" Text='<%# Eval("parttime_start_time") %>'></asp:Label>
                                        <asp:Label ID="lbl_parttime_end_time_edit" Visible="false" runat="server" Text='<%# Eval("parttime_end_time") %>'></asp:Label>

                                        <asp:Label ID="lbl_detailholiday_edit" runat="server"><span class="text-danger">*(HH)</span></asp:Label>
                                        <asp:Label ID="lbl_detaildayoff_edit" runat="server"><span class="text-danger">*(DH)</span></asp:Label>
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_holiday_idx_edit" Visible="false" runat="server" Text='<%# Eval("holiday_idx") %>'></asp:Label>
                                        <asp:Label ID="lbl_day_off_edit" Visible="false" runat="server" Text='<%# Eval("day_off") %>'></asp:Label>

                                        <asp:Label ID="lbl_date_condition_edit" Visible="false" runat="server" Text='<%# Eval("day_condition") %>'></asp:Label>
                                        <asp:Label ID="lbl_time_shiftstart_edit" Visible="false" runat="server"></asp:Label>
                                        <asp:Label ID="lbl_time_shiftend_edit" Visible="false" runat="server"></asp:Label>
                                        <asp:Label ID="lbl_parttime_break_start_time_edit" Visible="false" runat="server" Text='<%# Eval("parttime_break_start_time") %>'></asp:Label>
                                        <asp:Label ID="lbl_parttime_break_end_time_edit" Visible="false" runat="server" Text='<%# Eval("parttime_break_end_time") %>'></asp:Label>
                                    </p>


                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เวลาเริ่มทำ" ItemStyle-Width="15%" HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:UpdatePanel ID="Update_Timestart" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lbl_datetime_start_edit" Visible="false" runat="server" Text='<%# Eval("datetime_start") %>'></asp:Label>
                                            <asp:DropDownList ID="ddl_timestart_edit" runat="server" Width="160px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เวลาเลิกทำ" ItemStyle-Width="15%" HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:UpdatePanel ID="Update_Timeend" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lbl_datetime_end_edit" Visible="false" runat="server" Text='<%# Eval("datetime_end") %>'></asp:Label>
                                            <asp:DropDownList ID="ddl_timeend_edit" runat="server" Width="160px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ot ก่อน" ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_ot_before_edit" Visible="false" runat="server"></asp:Label>

                                    <asp:Label ID="lbl_ot_before_valueedit" Visible="false" runat="server" Text='<%# Eval("ot_before") %>'></asp:Label>
                                    <asp:DropDownList ID="ddl_timebefore_edit" Width="100px" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    <asp:Label ID="lbl_hour_otbefore_edit" runat="server"></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชั่วโมงทำงานวันหยุด" ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_ot_holiday_edit" Visible="false" runat="server"></asp:Label>

                                    <asp:Label ID="lbl_ot_holiday_valueedit" Visible="false" runat="server" Text='<%# Eval("ot_holiday") %>'></asp:Label>
                                    <asp:DropDownList ID="ddl_timeholiday_edit" Width="100px" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:Label ID="lbl_hour_otholiday_edit" runat="server"></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ot หลัง" ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>

                                    <asp:Label ID="lbl_ot_after_edit" Visible="false" runat="server"></asp:Label>

                                    <asp:Label ID="lbl_ot_after_valueedit" Visible="false" runat="server" Text='<%# Eval("ot_after") %>'></asp:Label>
                                    <asp:DropDownList ID="ddl_timeafter_edit" runat="server" Width="100px" CssClass="form-control">
                                    </asp:DropDownList>
                                    <asp:Label ID="lbl_hour_otafter_edit" runat="server"></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <p>
                                        <b>ความเห็น(ผู้สร้าง) :</b>
                                        <asp:TextBox ID="txt_remark_admin_edit" Text='<%# Eval("remark_admin") %>' runat="server" CssClass="form-control" Style="overflow: auto" Rows="2" TextMode="MultiLine" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>

                                    </p>

                                    <p>
                                        <b>ความเห็น(หัวหน้า) :</b>
                                        <asp:TextBox ID="txt_comment_head_approve" Enabled="false" Text='<%# Eval("comment_head") %>' runat="server" CssClass="form-control" Style="overflow: auto" Rows="2" TextMode="MultiLine" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>
                                    </p>

                                    <p>
                                        <b>ความเห็น(HR) :</b>
                                        <asp:TextBox ID="txt_comment_hr_check" Enabled="false" Text='<%# Eval("comment_hr") %>' runat="server" CssClass="form-control" Style="overflow: auto" Rows="2" TextMode="MultiLine" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>

            </div>

        </asp:View>
        <!--View Approve-->


    </asp:MultiView>
    <!--multiview-->

    <script type="text/javascript">

        $(function () {
            var string = $(".datetimepicker-fromsearch").val();
            //alert(string);
            if (string == '') {
                // alert(1);
                $('.datetimepicker-fromsearch').datetimepicker({

                    minDate: moment().add(-60, 'day'),
                    maxDate: moment().add(-1, 'day'),
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true,

                });

            }
            else {
                //alert(2);
                $('.datetimepicker-fromsearch').datetimepicker({

                    minDate: moment().add(-60, 'day'),
                    maxDate: moment().add(-1, 'day'),
                    format: 'DD/MM/YYYY', //$('.datetimepicker-fromsearch').data("DateTimePicker").date(e.date.format("DD/MM/YYYY")),//'DD/MM/YYYY',
                    //ignoreReadonly: true,

                }).val(string);
            }

            $('.show-fromsearch-onclick').click(function (e) {

                //alert("3");
                $('.datetimepicker-fromsearch').datetimepicker.show;
            });
        });



        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                var string = $(".datetimepicker-fromsearch").val();
                //alert(string);
                if (string == '') {
                    // alert(1);
                    $('.datetimepicker-fromsearch').datetimepicker({

                        minDate: moment().add(-60, 'day'),
                        maxDate: moment().add(-1, 'day'),
                        format: 'DD/MM/YYYY',
                        ignoreReadonly: true,

                    });

                }
                else {
                    //alert(2);
                    $('.datetimepicker-fromsearch').datetimepicker({

                        minDate: moment().add(-60, 'day'),
                        maxDate: moment().add(-1, 'day'),
                        format: 'DD/MM/YYYY', //$('.datetimepicker-fromsearch').data("DateTimePicker").date(e.date.format("DD/MM/YYYY")),//'DD/MM/YYYY',
                        //ignoreReadonly: true,

                    }).val(string);
                }

                $('.show-fromsearch-onclick').click(function (e) {

                    //alert("3");
                    $('.datetimepicker-fromsearch').datetimepicker.show;
                });
            });
        });
    </script>

    <script type="text/javascript">

        $(function () {
            $('.datetimepicker-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            //$('.datetimepicker-from').on('dp.change', function (e) {
            //    var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
            //    if (e.date > dateTo) {
            //        $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
            //    }
            //});

            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show;
            });
            //$('.datetimepicker-to').datetimepicker({
            //    format: 'DD/MM/YYYY',

            //});
            //$('.show-to-onclick').click(function () {
            //    $('.datetimepicker-to').data("DateTimePicker").show();
            //});
            //$('.datetimepicker-to').on('dp.change', function (e) {
            //    var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
            //    if (e.date < dateFrom) {
            //        $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
            //    }

            //});
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',

            });

            //$('.datetimepicker-from').on('dp.change', function (e) {
            //    var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
            //    if (e.date > dateTo) {
            //        $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
            //    }

            //});

            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            //$('.datetimepicker-to').datetimepicker({
            //    format: 'DD/MM/YYYY',
            //});
            //$('.show-to-onclick').click(function () {
            //    $('.datetimepicker-to').data("DateTimePicker").show();
            //});
            //$('.datetimepicker-to').on('dp.change', function (e) {
            //    var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
            //    if (e.date < dateFrom) {
            //        $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
            //    }

            //});

        });
    </script>

    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            //var dateToday = new Date();
            //currentTime = new Date();
            $('.datetimepicker-fromsearchapprove').datetimepicker({

                format: 'DD/MM/YYYY',
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(60, 'day'),
                //ignoreReadonly: true

            });
            $('.datetimepicker-fromsearchapprove').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-tosearchapprove').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-tosearchapprove').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });
            $('.show-from-onclicksearchapprove').click(function () {
                $('.datetimepicker-fromsearchapprove').data("DateTimePicker").show();
            });
            $('.datetimepicker-tosearchapprove').datetimepicker({
                format: 'DD/MM/YYYY',
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(60, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclicksearchapprove').click(function () {
                $('.datetimepicker-tosearchapprove').data("DateTimePicker").show();
            });
            $('.datetimepicker-tosearchapprove').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-fromsearchapprove').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-fromsearchapprove').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }

            });


            /* END Filter Datetimepicker Permission Permanent */
        });
    </script>


</asp:Content>

