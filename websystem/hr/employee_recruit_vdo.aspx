﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage_recurit.master" AutoEventWireup="true" CodeFile="employee_recruit_vdo.aspx.cs" Inherits="websystem_employee_recruit_vdo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <style type="text/css">
        :focus {
            outline: unset;
        }

        .upload_input {
            position: relative;
            display: flex;
            height: 40px;
            border: 1px solid #808080;
            border-radius: 3px;
            cursor: pointer;
            padding: 14px;
            height: 50px;
            padding-left: 100px;
            width: 720px;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            max-width: calc(100vw - 35px);
            margin: 0 auto;
        }

            .upload_input::before {
                font: normal normal normal 14px/1 FontAwesome;
                display: block;
                content: "\f0ed    Upload";
                background: #4696e5;
                border-radius: 3px;
                padding: 7px;
                position: absolute;
                position: absolute;
                top: 50%;
                color: #fff;
                left: 7px;
                transform: translate(0%,-50%);
                font-size: 15px;
            }

            .upload_input:hover::before {
                background: #0078ee;
            }

        nav {
            margin: -15px -15px 0;
            margin-bottom: 0 !important;
        }

        .navbar.navbar-default {
        }

        .navbar-header {
            display: inline-block;
        }

        .navbar-brand {
            width: 100%;
            height: 100%;
            padding: 7px 15px;
        }

            .navbar-brand img {
                height: 60px;
                width: auto;
            }

        .video-responesive {
            width: 720px;
            height: auto;
            max-width: 100%;
            border-radius: 15px;
        }

        video.video-responesive:focus {
            /*outline: unset !important;*/
        }

        .backTomain {
            display: inline-block;
            float: right;
            padding-top: 15px;
        }

            .backTomain a {
                /*border:1px  solid transparent;*/
                background-color: transparent;
            }

        .break_line {
            flex-basis: 100%;
            height: 0;
        }

        .txt_condi {
            width: 720px;
            max-width: calc(100vw - 35px);
            margin: 10px auto;
            text-align: left;
            font-size:12px;
            color:#f65454;
            padding-left:7px;
        }
    </style>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <img alt="Brand" src="https://www.taokaenoi.co.th/images/logo-th.png" />
                </a>

            </div>
            <div class="backTomain" runat="server" id="b_backTomain">
                <asp:LinkButton ID="backTomain"
                    CssClass="btn btn-default"
                    runat="server"
                    CommandName="backTomain"
                    OnCommand="btnCommand">
                                BackToHome
                </asp:LinkButton>
            </div>
        </div>

    </nav>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:MultiView runat="server" ID="MvMaster" ActiveViewIndex="0">
        <asp:View runat="server" ID="viewIndex">


            <div class="container" style="min-height: 80vh;">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div class="h2">อัปโหลด วิดีโอ สำหรับแนะนำตัวของ คุณ <span runat="server" id="title_name"></span></div>

                    </div>
                </div>
                <div class="row">
                    <asp:FormView runat="server" ID="FvuploadVDO" DefaultMode="Insert" Style="width: 100%;">
                        <InsertItemTemplate>
                            <asp:UpdatePanel ID="upActor1Node1Files11" runat="server">
                                <ContentTemplate>

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                                            <asp:Label ID="txtupload" runat="server" CssClass="upload_input" AssociatedControlID="upload" AutoPostBack="true" Enabled="true" Text="">
                                                <div runat="server" id="txtnamevdo">Browse File ...</div>


                                            </asp:Label>
                                            <div class="txt_condi">*อัปโหลดไฟล์ ไม่เกิน 15 MB และ Import ได้เฉพาะไฟล์ MP4 หรือ M4V (นามสกุลไฟล์ .mp4,.m4v) เท่านั้น</div>
                                            <asp:FileUpload ID="upload" runat="server" AutoPostBack="true" Enabled="true" Style="display: none;" onchange="FileUploadPostBack();" accept="video/mp4" />
                                            <asp:CustomValidator
                                                ID="cusv_upload"
                                                runat="server"
                                                ErrorMessage="อัปโหลดไฟล์ ไม่เกิน 15 MB."
                                                ValidationGroup="vali_importfile"
                                                Style="color: red; margin: 0 auto;"></asp:CustomValidator>
                                            <asp:RegularExpressionValidator
                                                runat="server"
                                                ID="rvaupload"
                                                ControlToValidate="upload"
                                                ErrorMessage="Import ได้เฉพาะไฟล์ MP4 หรือ M4V (นามสกุลไฟล์ .mp4,.m4v) เท่านั้น"
                                                ValidationExpression="^.*\.([m|M][p|P][4]||[m|M][4][v|V])$" ValidationGroup="vali_importfile"
                                                Style="color: red; margin: 0 auto;" />
                                            <asp:LinkButton ID="btn_upload" runat="server" CommandName="btn_upload" OnCommand="btnCommand" Style="display: none;"></asp:LinkButton>

                                        </div>
                                    </div>
                                    <div class="form-group" runat="server" id="box_video_preview" visible="false">
                                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                            <video class="video-responesive" controls="controls">
                                                <source runat="server" id="vdo_src" type="video/mp4" />

                                                Your browser does not support the video tag.
                                            </video>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                            <asp:LinkButton class="btn btn-default" ID="btn_save" runat="server" CommandName="btn_save" Visible="false" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ บันทึกแล้วไม่สามารถแก้ไขได้?')">บันทึก</asp:LinkButton>
                                        </div>
                                    </div>

                                </ContentTemplate>

                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btn_upload" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

        </asp:View>
        <asp:View runat="server" ID="viewerror">
            <div class="col-xs-12 text-center">
                <div class="h1 text-danger" style="min-height: 75vh;">Page Not Found</div>
            </div>
        </asp:View>
        <asp:View runat="server" ID="viewreadonly">
            <div class="container" style="min-height: 80vh;">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div class="h2">วิดีโอแนะนำตัวของ คุณ <span runat="server" id="title_readonly"></span></div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <video class="video-responesive" controls="controls">
                            <source runat="server" id="vdo_readonly" type="video/mp4" />

                            Your browser does not support the video tag.
                        </video>
                    </div>
                </div>
            </div>
        </asp:View>

    </asp:MultiView>
    <div class="footer">
        <div class=" text-right h6">
            © Copyright 2013 Taokaenoi Food & Marketing PCL.
        </div>
    </div>
</asp:Content>
