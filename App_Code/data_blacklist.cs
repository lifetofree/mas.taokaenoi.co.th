﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_blacklist
/// </summary>
/// 
[Serializable]
[XmlRoot("data_blacklist")]
public class data_blacklist
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("blacklist_m0_list")]
    public blacklist_m0_detail[] blacklist_m0_list { get; set; }

    [XmlElement("blacklist_u0_list")]
    public blacklist_u0_detail[] blacklist_u0_list { get; set; }

    [XmlElement("blacklist_u1_list")]
    public blacklist_u1_detail[] blacklist_u1_list { get; set; }

    [XmlElement("blacklist_log_list")]
    public blacklist_log_detail[] blacklist_log_list { get; set; }

    [XmlElement("blacklist_log_listview")]
    public blacklist_log_view[] blacklist_log_listview { get; set; }

    [XmlElement("blacklist_m0")]
    public Mblacklist[] blacklist_m0 { get; set; }


}

[Serializable]
public class Mblacklist
{
    [XmlElement("Blacklist_idx")]
    public int Blacklist_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("Blacklist_detail")]
    public string Blacklist_detail { get; set; }

    [XmlElement("Blacklist_status")]
    public string Blacklist_status { get; set; }

}

[Serializable]
public class blacklist_log_view
{
    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement("PrefixnameTH")]
    public string PrefixnameTH { get; set; }

    [XmlElement("FirstnameTH")]
    public string FirstnameTH { get; set; }

    [XmlElement("LastnameTH")]
    public string LastnameTH { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("time_update")]
    public string time_update { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("Blacklist_detail")]
    public string Blacklist_detail { get; set; }

    [XmlElement("log_status")]
    public string log_status { get; set; }

}

[Serializable]
public class blacklist_log_detail
{

    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement("u1_idx")]
    public int u1_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    //[XmlElement("time_update")]
    //public string time_update { get; set; }

    //[XmlElement("create_date")]
    //public string create_date { get; set; }

    //[XmlElement("time_create")]
    //public string time_create { get; set; }

    //[XmlElement("emp_name_th_update")]
    //public string emp_name_th_update { get; set; }

}

[Serializable]
public class blacklist_u1_detail
{
    [XmlElement("U1_IDX")]
    public int U1_IDX { get; set; }

    [XmlElement("U0_IDX")]
    public int U0_IDX { get; set; }

    [XmlElement("BlacklistIDX")]
    public int BlacklistIDX { get; set; }

    [XmlElement("Note")]
    public string Note { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("Blacklist_detail")]
    public string Blacklist_detail { get; set; }

    [XmlElement("M0_blacklistidx")]
    public int M0_blacklistidx { get; set; }

    [XmlElement("M0_Blacklistdetail")]
    public string M0_Blacklistdetail { get; set; }

    [XmlElement("U1_BlacklistIDX")]
    public int U1_BlacklistIDX { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("U1_note")]
    public string U1_note { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("U1_IDX1")]
    public int U1_IDX1 { get; set; }

}

[Serializable]
public class blacklist_m0_detail
{
    [XmlElement("Blacklist_idx")]
    public int Blacklist_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("Blacklist_detail")]
    public string Blacklist_detail { get; set; }

    [XmlElement("Blacklist_status")]
    public int Blacklist_status { get; set; }

    [XmlElement("Cemp_IDX")]
    public int Cemp_IDX { get; set; }

    [XmlElement("Create_date")]
    public string Create_date { get; set; }

    [XmlElement("Update_date")]
    public string Update_date { get; set; }

    [XmlElement("M0_IDX")]
    public int M0_IDX { get; set; }
}

[Serializable]
public class blacklist_u0_detail
{
    [XmlElement("U0_idx")]
    public int U0_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("Idcard")]
    public string Idcard { get; set; }

    [XmlElement("Passpost")]
    public string Passpost { get; set; }

    [XmlElement("PrefixIDXTH")]
    public int PrefixIDXTH { get; set; }

    [XmlElement("FirstnameTH")]
    public string FirstnameTH { get; set; }

    [XmlElement("LastnameTH")]
    public string LastnameTH { get; set; }

    [XmlElement("PrefixIDXEN")]
    public int PrefixIDXEN { get; set; }

    [XmlElement("FirstnameEN")]
    public string FirstnameEN { get; set; }

    [XmlElement("LastnameEN")]
    public string LastnameEN { get; set; }

    [XmlElement("NatIDX")]
    public int NatIDX { get; set; }

    [XmlElement("RaceIDX")]
    public int RaceIDX { get; set; }

    [XmlElement("U0_note")]
    public string U0_note { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("U0_status")]
    public int U0_status { get; set; }

    [XmlElement("PrefixnameTH")]
    public string PrefixnameTH { get; set; }

    [XmlElement("PrefixnameEN")]
    public string PrefixnameEN { get; set; }

    [XmlElement("RaceName")]
    public string RaceName { get; set; }

    [XmlElement("NatName")]
    public string NatName { get; set; }

    [XmlElement("u1_note")]
    public string u1_note { get; set; }

    [XmlElement("Blacklist_detail")]
    public string Blacklist_detail { get; set; }

    [XmlElement("u1BlacklistIDX")]
    public int u1BlacklistIDX { get; set; }
}



