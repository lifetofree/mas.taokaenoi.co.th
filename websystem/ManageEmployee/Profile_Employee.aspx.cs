﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;

public partial class websystem_ManageEmployee_Profile_Employee : System.Web.UI.Page
{
    #region initial 

    function_tool _funcTool = new function_tool();
    service_execute _service_exec = new service_execute();
    data_employee _data_employee = new data_employee();

    string Conn_show_profile_employee = "Conn_manage_employee";
    string _local_xml = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlCheckLogin = _serviceUrl + ConfigurationManager.AppSettings["urlCheckLogin"];

    string _localJson = "";
    string _initPass = "25d55ad283aa400af464c76d713c07ad";
    int _tempInt = 0;

    #endregion

    #region Page_Init
    private void Page_Init(object sender, System.EventArgs e)
    {
        if (Request.Form["emp_idx"] != null)
        {
            bool _result = Int32.TryParse(Request.Form["emp_idx"], out _tempInt);
            if (_result && _tempInt > 0)
            {
                Session["emp_idx"] = Request.Form["emp_idx"];
                // go to page
                string url = Request.QueryString["url"];
                if (url == null)
                {
                    Response.Redirect(ResolveUrl("~/"));
                }
                else
                {
                    Response.Redirect(ResolveUrl("~/" + url));
                }
            }
        }
    }

     #endregion

    #region page load

    protected void Page_Load(object sender, EventArgs e)
    {
        ViewState["emp_idx"] = int.Parse(Session["emp_idx"].ToString());

        if (!this.IsPostBack)
       {
            //Session.Clear();
            select_social_security();
            divShowError.Visible = false;

        }
           
            select_personal_employee();
            show_profile_picture();
            select_personal_employee_Thai();
       

       
           

    }

    #endregion

    #region perrsonal employee
    protected void select_personal_employee()
    {
        var gvpicture = (GridView)repeater_contact.FindControl("gvpicture");
        Label Lb_emp_code = (Label)repeater_contact.FindControl("Lb_emp_code");

        data_employee data_emps = new data_employee();
        employee_detail show_details = new employee_detail();
        data_emps.employee_list = new employee_detail[1];
        show_details.emp_idx = int.Parse(ViewState["emp_idx"].ToString());
        data_emps.employee_list[0] = show_details;

        _local_xml = _service_exec.actionExec(Conn_show_profile_employee, "data_employee", "Manage_Employee", data_emps, 238);
        data_emps = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        ViewState["emp_code"] = data_emps.employee_list[0].emp_code;
      

        repeater_personal.DataSource = data_emps.employee_list;
        repeater_personal.DataBind();

        repeater_contact.DataSource = data_emps.employee_list;
        repeater_contact.DataBind();

        repeater_employee.DataSource = data_emps.employee_list;
        repeater_employee.DataBind();

        repeater_approve.DataSource = data_emps.employee_list;
        repeater_approve.DataBind();

        ViewState["approve_1"] = data_emps.employee_list[0].emp_approve1;
        ViewState["approve_2"] = data_emps.employee_list[0].emp_approve2;
      

        //var Label_emp_approve1 = (Label)repeater_approve.FindControl("Label_emp_approve1");

        //if (ViewState["approve_1"].ToString() != "" 
        //    && ViewState["approve_1"].ToString() != null)
        //{

        //    Label_emp_approve1.Text = ViewState["approve_1"].ToString();
        //}
        //else
        //{
        //    ViewState["aaaaa"] = "ท่านยังไม่มีข้อมูลผู้อนุมัติ";
        //    Label_emp_approve1.Text = ViewState["aaaaa"].ToString();
        //}

    }

    #endregion

    #region social_security
    protected void select_social_security()
    {
        data_employee data_emp = new data_employee();
        EmployeeTHlist show_details_Thai = new EmployeeTHlist();
        data_emp.BoxEmployeeTHlist = new EmployeeTHlist[1];
        show_details_Thai.emp_idx_th = int.Parse(ViewState["emp_idx"].ToString());
        data_emp.BoxEmployeeTHlist[0] = show_details_Thai;

        _local_xml = _service_exec.actionExec(Conn_show_profile_employee, "data_employee", "Manage_Employee", data_emp, 241);
        data_emp = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["returnCode_security_social"] = data_emp.return_code;

        if (data_emp.return_code != "0")
        {
            ViewState["returnCode_security"] = data_emp.BoxEmployeeTHlist[0].SocHosIDX;
        }
        else
        {
            ViewState["returnCode_security"] = 0;

        }

        value_social_security.DataSource = data_emp.BoxEmployeeTHlist;
        value_social_security.DataBind();
       
    }
    #endregion

    #region personal employee Thai
    protected void select_personal_employee_Thai()
    {
        data_employee data_emp = new data_employee();
        EmployeeTHlist show_details_Thai = new EmployeeTHlist();
        data_emp.BoxEmployeeTHlist = new EmployeeTHlist[1];
        show_details_Thai.emp_idx_th = int.Parse(ViewState["emp_idx"].ToString());
        data_emp.BoxEmployeeTHlist[0] = show_details_Thai;

        _local_xml = _service_exec.actionExec(Conn_show_profile_employee, "data_employee", "Manage_Employee", data_emp, 241);
        data_emp = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["returnCode_empsTH"] = data_emp.return_code;

         var lable_socIDX = (TextBox)value_social_security.FindControl("lable_socIDX");

       if ((ViewState["returnCode_security"].ToString() == "0"))
        {
            showalert_social_security.Visible = true;
        }
        else if (data_emp.return_code == "1")
        {
            repeater_social_security.DataSource = data_emp.BoxEmployeeTHlist;
            repeater_social_security.DataBind();
            showalert_social_security.Visible = false;
        }
    }
    #endregion

    #region profile picture
    protected void show_profile_picture()
    {
        data_employee edit_employee_data = new data_employee();
        employee_detail Editdataemployee = new employee_detail();
        edit_employee_data.employee_list = new employee_detail[1];

        Editdataemployee.emp_idx = int.Parse(ViewState["emp_idx"].ToString());
        edit_employee_data.employee_list[0] = Editdataemployee;

        _local_xml = _service_exec.actionExec(Conn_show_profile_employee, "data_employee", "Manage_Employee", edit_employee_data, 212);
        edit_employee_data = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        gvpicture.DataSource = edit_employee_data.employee_list;
        gvpicture.DataBind();

    }

    #endregion

    #region sarary
    protected void select_sarary_employee()
    {

        data_employee data_sarary = new data_employee();
        employee_detail show_details = new employee_detail();
        data_sarary.employee_list = new employee_detail[1];
        show_details.emp_idx = int.Parse(ViewState["emp_idx"].ToString());
        data_sarary.employee_list[0] = show_details;

        _local_xml = _service_exec.actionExec(Conn_show_profile_employee, "data_employee", "Manage_Employee", data_sarary, 239);
        data_sarary = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["retrun_sarary"] = data_sarary.return_code;

        panel_login.Visible = false;
        if (data_sarary.return_code != "0")
        {
            repeater_sarary.DataSource = data_sarary.employee_list;
            repeater_sarary.DataBind();
            repeater_sarary_empty.Visible = false;
            repeater_sarary.Visible = true;
        }
        else
        {
          repeater_sarary_empty.Visible = true;
          repeater_sarary.Visible = false;

        }

        ViewState["emp_idx"] = null;


    }
    #endregion

    #region btnCommand 

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "hiddenViewSarary":
                repeater_sarary.Visible = false;
                repeater_sarary_empty.Visible = false;
                break;

            case "change_password":
                Response.Redirect(ResolveUrl("~/changepassword"));
                break;

            case "view_sarary":
                //  tbEmpCode.Focus();
                //Session["emp_idx"] = null;
                panel_login.Visible = true;
                
                //  Response.Redirect(ResolveUrl("~/warningLoginAgian"));
                break;

            case "cmdLogin":
                string _empCode = tbEmpCode.Text.Trim();
                string _empPass = _funcTool.getMd5Sum(tbEmpPass.Text.Trim());
                if (_empCode != String.Empty && _empPass != String.Empty)
                {
                    // set data
                    _data_employee.employee_list = new employee_detail[1];
                    employee_detail _empDetail = new employee_detail();
                    _empDetail.emp_code = _empCode;
                    _empDetail.emp_password = _empPass;
                    _data_employee.employee_list[0] = _empDetail;

                    // convert to json
                    _localJson = _funcTool.convertObjectToJson(_data_employee);

                    // call services
                    _localJson = _funcTool.callServiceGet(_urlCheckLogin + _localJson);

                    // convert json to object
                    _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

                    // check return_code
                    if (int.Parse(_data_employee.return_code) == 0)
                    {
                        // create session
                        Session["emp_idx"] = _data_employee.employee_list[0].emp_idx;

                        // check initial password or not
                        if (_empPass != _initPass)
                        {
                            // go to page
                            string url = Request.QueryString["url"];
                            if (url == null)
                            {
                                select_sarary_employee();
                                tbEmpCode.Text = String.Empty;
                               // repeater_sarary.Visible = true;
                            }
                            else
                            {
                                Response.Redirect(ResolveUrl(url));

                            }
                           // panel_login.Visible = false;
                        }
                        else
                        {
                            Session["reset_type"] = 1;
                            Response.Redirect(ResolveUrl("~/changepassword"));
                        }
                    }
                    else
                    {
                        divShowError.Visible = !divShowError.Visible;
                        litErrorCode.Text = _data_employee.return_code.ToString();
                        //litDebug.Text = "error : " + _dataEmployee.return_code.ToString() + " - " + _dataEmployee.return_msg;
                    }
                }

                break;
        }
    }

    #endregion

    #region gridview

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvNameTEST = (GridView)sender;

        switch (GvNameTEST.ID)
        {

            case "gvpicture":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Image ImagProfile = (Image)e.Row.Cells[0].FindControl("ImagProfile");
                    Panel ImagProfile_default = (Panel)e.Row.Cells[0].FindControl("ImagProfile_default");
                    Repeater Repeater_pictureProfile = (Repeater)e.Row.Cells[0].FindControl("Repeater_pictureProfile");
                    TextBox txtemp_code = (TextBox)e.Row.Cells[0].FindControl("txtemp_code");

                    ViewState["emp_code_path_picture"] = txtemp_code.Text;

                    string getPath_picture = ConfigurationSettings.AppSettings["upload_profile_picture"];

                    if (Directory.Exists(Server.MapPath(getPath_picture + ViewState["emp_code_path_picture"].ToString())))
                    {

                        data_employee edit_employee_data = new data_employee();
                        employee_detail Editdataemployee = new employee_detail();
                        edit_employee_data.employee_list = new employee_detail[1];

                        Editdataemployee.emp_idx = (int)ViewState["emp_idx"];
                        edit_employee_data.employee_list[0] = Editdataemployee;

                        _local_xml = _service_exec.actionExec(Conn_show_profile_employee, "data_employee", "Manage_Employee", edit_employee_data, 212);
                        edit_employee_data = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

                        Repeater_pictureProfile.DataSource = edit_employee_data.employee_list;
                        Repeater_pictureProfile.DataBind();

                        ImagProfile_default.Visible = false;

                    }
                    else
                    {
                        Repeater_pictureProfile.Visible = false;
                        ImagProfile_default.Visible = true;

                    }
                }

                break;
        }
    }

    #endregion

    #region path pricture

    protected string retrunpatProfile(string empC0de)
    {
        string getPath = ConfigurationSettings.AppSettings["upload_profile_picture"];
        string path = getPath + empC0de.ToString() + "/" + empC0de.ToString() + ".jpg";
        return path;

    }

    #endregion
}