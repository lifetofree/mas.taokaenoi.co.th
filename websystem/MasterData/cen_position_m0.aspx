﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="cen_position_m0.aspx.cs" Inherits="websystem_MasterData_cen_position_m0" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litdebug" runat="server"></asp:Literal>
    <asp:MultiView ID="mvPosition" runat="server" ActiveViewIndex="0">
        <asp:View ID="vMaster" runat="server">


            <asp:FormView ID="FormViewS" runat="server" Visible="false" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">ค้นหา</h3>
                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">

                                <div class="form-horizontal" role="form">

                                    <div class="panel-heading">


                                        <div class="form-group">
                                            <asp:Label ID="LabelDropDownOrg_s" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="DropDownOrg_s" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>

                                            <%--<div class="col-sm-3">
                                                <%--<asp:RequiredFieldValidator InitialValue="" ID="Req_ID252" runat="server" ControlToValidate="DropDownOrg_s" ErrorMessage="กรุณาเลือกองค์กร" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>--%>
                                        
                                            <asp:Label ID="LabelDropDownWg_s" runat="server" Text="กลุ่มงาน" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="DropDownWg_s" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <%--<div class="col-sm-3">
                                                <%--<asp:RequiredFieldValidator InitialValue="" ID="Req_ID252" runat="server" ControlToValidate="DropDownOrg_s" ErrorMessage="กรุณาเลือกองค์กร" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>--%>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="LabelDropDownLw_s" runat="server" Text="สายงาน" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="DropDownLw_s" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                           <%-- <div class="col-sm-3">
                                                <%--<asp:RequiredFieldValidator InitialValue="" ID="Req_ID252" runat="server" ControlToValidate="DropDownOrg_s" ErrorMessage="กรุณาเลือกองค์กร" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>--%>
                                        
                                            <asp:Label ID="LabelDropDownDept_s" runat="server" Text="ฝ่าย" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="DropDownDept_s" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <%--<div class="col-sm-3">
                                                <%--<asp:RequiredFieldValidator InitialValue="" ID="Req_ID252" runat="server" ControlToValidate="DropDownOrg_s" ErrorMessage="กรุณาเลือกองค์กร" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>--%>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="LabelDropDownSec_s" runat="server" Text="แผนก" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="DropDownSec_s" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                            <%--<div class="col-sm-3">
                                                <%--<asp:RequiredFieldValidator InitialValue="" ID="Req_ID252" runat="server" ControlToValidate="DropDownOrg_s" ErrorMessage="กรุณาเลือกองค์กร" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>--%>
                                        
                                            <asp:Label ID="LabelDropDownGrade_s" runat="server" Text="Job Grade" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="DropDownGrade_s" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                           <%-- <div class="col-sm-3">
                                                <%--<asp:RequiredFieldValidator InitialValue="" ID="Req_ID252" runat="server" ControlToValidate="DropDownOrg_s" ErrorMessage="กรุณาเลือกองค์กร" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>--%>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="lbs_pos" runat="server" CssClass="col-sm-2 control-label">ชื่อตำแหน่ง</asp:Label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tb_pos_s" runat="server" CssClass="form-control" placeholder="กรอกชื่อตำแหน่ง">

                                                </asp:TextBox>

                                            </div>
                                            <%--<div class="col-sm-3">
                                            </div>--%>
                                        
                                            <asp:Label ID="LabelDropDownstatus" runat="server" Text="สถานะ" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="DropDownstatus_s" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="" Text="---เลือกสถานะ---" />
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                            <%--<div class="col-sm-3">
                                                <%--<asp:RequiredFieldValidator InitialValue="" ID="Req_ID252" runat="server" ControlToValidate="DropDownOrg_s" ErrorMessage="กรุณาเลือกองค์กร" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>--%>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <asp:LinkButton ID="lbsearch" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearch" CommandArgument="Search">
                                                <i class="fas fa-search" aria-hidden="true"></i>&nbsp;Search</asp:LinkButton>

                                                <asp:LinkButton ID="lbsearch_cancel" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="cmdReset" CausesValidation="false">
                                                    <i class="fas fa-redo" aria-hidden="true"></i>&nbsp;Reset
                                                
                                                </asp:LinkButton>
                                            </div>
                                            <%--<label class="col-sm-3 control-label"></label>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>


            <div class="form-group">
                <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdCreate"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูล</asp:LinkButton>
            </div>

            <asp:FormView ID="FvInsertEdit" runat="server" Visible="false" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มตำแหน่ง</h3>

                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">

                                <div class="form-horizontal" role="form">

                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server" Text="องค์กร" CssClass="col-sm-3 control-label">
                                                องค์กร<span class="text-danger"> *</span>
                                            </asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="DropDownOrg" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownOrg" runat="server" ControlToValidate="DropDownOrg" ErrorMessage="*กรุณาเลือกองค์กร" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownOrg" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownOrg" Width="220" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label5" runat="server" Text="กลุ่มงาน" CssClass="col-sm-3 control-label">
                                                กลุ่มงาน<span class="text-danger"> *</span>
                                            </asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="DropDownWg" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownWg" runat="server" ControlToValidate="DropDownWg" ErrorMessage="*กรุณาเลือกกลุ่มงาน" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownWg" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownWg" Width="220" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label2" runat="server" Text="สายงาน" CssClass="col-sm-3 control-label">
                                                สายงาน<span class="text-danger"> *</span>
                                            </asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="DropDownLw" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownLw" runat="server" ControlToValidate="DropDownLw" ErrorMessage="*กรุณาเลือกสายงาน" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownLw" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownLw" Width="220" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label6" runat="server" Text="ฝ่าย" CssClass="col-sm-3 control-label">
                                                ฝ่าย<span class="text-danger"> *</span>
                                            </asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="DropDownDept" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownDept" runat="server" ControlToValidate="DropDownDept" ErrorMessage="*กรุณาเลือกฝ่าย" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownDept" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownDept" Width="220" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label9" runat="server" Text="แผนก" CssClass="col-sm-3 control-label">
                                                แผนก<span class="text-danger"> *</span>
                                            </asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="DropDownSec" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownSec" runat="server" ControlToValidate="DropDownSec" ErrorMessage="*กรุณาเลือกแผนก" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                 <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownSec" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownSec" Width="220" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label10" runat="server" CssClass="col-sm-3 control-label">
                                                Job Grade<span class="text-danger"> *</span>
                                            </asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="DropDownGrade" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownGrade" runat="server" ControlToValidate="DropDownGrade" ErrorMessage="*กรุณาเลือก Job Grade" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownGrade" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownGrade" Width="220" />

                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <asp:Label ID="Label3" runat="server" CssClass="col-sm-3 control-label">Man Power<span class="text-danger"> *</span></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="tbmanpower" runat="server" CssClass="form-control" MaxLength="250" Text="0" placeholder="กรอก Man Power">

                                                </asp:TextBox>
                                            </div>
                                            <div class="col-sm-3">

                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidatortbmanpower" runat="server" ControlToValidate="tbmanpower" ErrorMessage="กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" SetFocusOnError="true">
                                                </asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressiontbmanpower" runat="server"
                                                    ControlToValidate="tbmanpower"
                                                    ErrorMessage="*กรุณาใส่ข้อมูลเป็นตัวเลขให้ถูกต้อง"
                                                    ForeColor="Red"
                                                    Display="None"
                                                    ValidationExpression="^[0-9]{0,4}$"></asp:RegularExpressionValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttbmanpowerreq" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatortbmanpower" Width="220" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttbmanpowerre" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressiontbmanpower" Width="220" />

                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <asp:Label ID="lbnameth" runat="server" CssClass="col-sm-3 control-label">ชื่อตำแหน่ง (ภาษาไทย)<span class="text-danger"> *</span></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="tb_posnameth" runat="server" CssClass="form-control" MaxLength="250" placeholder="กรอกชื่อตำแหน่ง (ภาษาไทย)">

                                                </asp:TextBox>
                                            </div>
                                            <div class="col-sm-3">

                                                <asp:RequiredFieldValidator ID="RequiredFieldtb_posnameth" runat="server" ControlToValidate="tb_posnameth" ErrorMessage="*กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" SetFocusOnError="true" Display="None">
                                                </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttb_posnameth" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtb_posnameth" Width="220" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbnameen" runat="server" CssClass="col-sm-3 control-label">ชื่อตำแหน่ง (ภาษาอังกฤษ)<span class="text-danger"> *</span></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="tb_posnameen" runat="server" CssClass="form-control" MaxLength="250" placeholder="กรอกชื่อตำแหน่ง (ภาษาอังกฤษ)">

                                                </asp:TextBox>
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:RequiredFieldValidator ID="RequiredFieldtb_posnameen" runat="server" ControlToValidate="tb_posnameen" ErrorMessage="*กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" SetFocusOnError="true" Display="None">
                                                </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttb_posnameen" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtb_posnameen" Width="220" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0">
                                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</asp:LinkButton>

                                                <asp:LinkButton ID="Lbtn_cancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancel
                                                </asp:LinkButton>
                                            </div>
                                            <label class="col-sm-3 control-label"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>

                <EditItemTemplate>
                    <%--Edit--%>

                    <div class="panel panel-info">
                        <div class="panel-heading">

                            <h3 class="panel-title">แก้ไข</h3>
                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="labeledit1" runat="server" class="col-sm-3 control-label">
                                            องค์กร<span class="text-danger"> *</span>
                                        </asp:Label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="DropDownOrg" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>

                                        </div>
                                        <div class="col-sm-3">
                                            <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownOrg" runat="server" ControlToValidate="DropDownOrg" ErrorMessage="*กรุณาเลือกองค์กร" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownOrg" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownOrg" Width="220" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="labeledit2" runat="server" class="col-sm-3 control-label">
                                            กลุ่มงาน<span class="text-danger"> *</span>
                                        </asp:Label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="DropDownWg" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>

                                        </div>
                                        <div class="col-sm-3">
                                            <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownWg" runat="server" ControlToValidate="DropDownWg" ErrorMessage="*กรุณาเลือกกลุ่มงาน" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownWg" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownWg" Width="220" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="labeledit6" runat="server" class="col-sm-3 control-label">
                                            สายงาน<span class="text-danger"> *</span>
                                        </asp:Label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="DropDownLw" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>

                                        </div>
                                        <div class="col-sm-3">
                                            <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownLw" runat="server" ControlToValidate="DropDownLw" ErrorMessage="*กรุณาเลือกสายงาน" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownLw" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownLw" Width="220" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="label3" runat="server" class="col-sm-3 control-label">
                                            ฝ่าย<span class="text-danger"> *</span>
                                        </asp:Label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="DropDownDept" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>

                                        </div>
                                        <div class="col-sm-3">
                                            <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownDept" runat="server" ControlToValidate="DropDownDept" ErrorMessage="*กรุณาเลือกฝ่าย" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownDept" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownDept" Width="220" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="label7" runat="server" class="col-sm-3 control-label">
                                            แผนก<span class="text-danger"> *</span>
                                        </asp:Label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="DropDownSec" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>

                                        </div>
                                        <div class="col-sm-3">
                                            <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownSec" runat="server" ControlToValidate="DropDownSec" ErrorMessage="*กรุณาเลือกแผนก" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownSec" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownSec" Width="220" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="label8" runat="server" class="col-sm-3 control-label">
                                            Job Grade<span class="text-danger"> *</span>
                                        </asp:Label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="DropDownGrade" CssClass="form-control" runat="server">
                                            </asp:DropDownList>

                                        </div>
                                        <div class="col-sm-3">
                                            <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownGrade" runat="server" ControlToValidate="DropDownGrade" ErrorMessage="*กรุณาเลือก Job Grade" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownGrade" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownGrade" Width="220" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="label11" runat="server" class="col-sm-3 control-label">Man Power<span class="text-danger"> *</span></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbmanpower" runat="server" CssClass="form-control" Text='<%# Eval("man_power") %>' placeholder="กรอก Man Power" MaxLength="250"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-3">
                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="tbmanpower" ErrorMessage="กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" SetFocusOnError="true">
                                            </asp:RequiredFieldValidator>--%>
                                            <asp:RegularExpressionValidator ID="RegularExpressiontbmanpower" runat="server"
                                                ControlToValidate="tbmanpower"
                                                ErrorMessage="*กรุณาใส่ข้อมูลให้ถูกต้อง"
                                                ForeColor="Red"
                                                Display="None"
                                                ValidationExpression="^[0-9]{0,250}$"></asp:RegularExpressionValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttbmanpower" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressiontbmanpower" Width="220" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="labeledit3" runat="server" class="col-sm-3 control-label">ชื่อตำแหน่ง (ภาษาไทย)<span class="text-danger"> *</span></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tb_posnameth" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' placeholder="กรอกชื่อตำแหน่ง (ภาษาไทย)" MaxLength="250"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:RequiredFieldValidator ID="RequiredFieldtb_posnameth" runat="server" ControlToValidate="tb_posnameth" ErrorMessage="*กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" SetFocusOnError="true" Display="None">
                                            </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttb_posnameth" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtb_posnameth" Width="220" />
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                            ControlToValidate="tbNameTH"
                                            ValidationExpression="^[a-zA-Z0-9'@&#.,)(\s]{1,250}$"></asp:RegularExpressionValidator>--%>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="labeledit4" runat="server" class="col-sm-3 control-label">ชื่อตำแหน่ง (ภาษาอังกฤษ)<span class="text-danger"> *</span></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tb_posnameen" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_en") %>' placeholder="กรอกชื่อตำแหน่ง (ภาษาอังกฤษ)" MaxLength="250"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:RequiredFieldValidator ID="RequiredFieldtb_posnameen" runat="server" ControlToValidate="tb_posnameen" ErrorMessage="*กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" SetFocusOnError="true" Display="None">
                                            </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttb_posnameen" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtb_posnameen" Width="220" />
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                            ControlToValidate="tbNameEN"
                                            ValidationExpression="^[a-zA-Z0-9'@&#.,)(\s]{1,250}$"></asp:RegularExpressionValidator>--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="labeledit5" runat="server" class="col-sm-3 control-label" Text="สถานะ"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server" Text='<%# Eval("pos_status") %>'>
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-6">
                                            <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="editSave" CommandArgument='<%# Eval("pos_idx") %>'>
                                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</asp:LinkButton>
                                            <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0" CausesValidation="false">
                                                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancel</asp:LinkButton>
                                        </div>
                                        <label class="col-sm-3 control-label"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </EditItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvPosition" runat="server" AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="info"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="Master_PageIndexChanging">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                </EmptyDataTemplate>
                <Columns>

                    <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                                <asp:Label ID="gvtextid" runat="server" Visible="false" Text='<%# Eval("pos_idx") %>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>



                    <asp:TemplateField HeaderText="องค์กร" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <asp:Label ID="Labelgv1" runat="server" Text='<%# Eval("org_name_th") %>'>

                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="กลุ่มงาน" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <asp:Label ID="Labelgv2" runat="server" Text='<%# Eval("wg_name_th") %>'>

                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สายงาน" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <asp:Label ID="Labelgv6" runat="server" Text='<%# Eval("lw_name_th") %>'>

                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>



                    <asp:TemplateField HeaderText="ฝ่าย" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <asp:Label ID="Labelgv7" runat="server" Text='<%# Eval("dept_name_th") %>'>

                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="แผนก" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <asp:Label ID="Labelgv10" runat="server" Text='<%# Eval("sec_name_th") %>'>

                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Job Grade" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                            <asp:Label ID="Labelgv9" runat="server" Text='<%# Eval("jobgrade_idx") %>'>

                            </asp:Label>
                                </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Man Power" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                            <asp:Label ID="Labelgv8" runat="server" Text='<%# Eval("man_power") %>'>

                            </asp:Label>
                                </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อตำแหน่ง (ภาษาไทย)" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <asp:Label ID="Labelgv5" runat="server" Text='<%# Eval("pos_name_th") %>'>

                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อตำแหน่ง (ภาษาอังกฤษ)" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <asp:Label ID="Labelgv4" runat="server" Text='<%# Eval("pos_name_en") %>'>

                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>




                    <asp:TemplateField HeaderText="สถานะ" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                                <asp:Label ID="Label4" runat="server" Text='<%# convertStatus((int)Eval("pos_status")) %>'
                                    CssClass='<%# convertCss((int)Eval("pos_status")) %>'>
                                </asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                                <asp:LinkButton ID="lbEdit" CssClass="btn btn-info" runat="server" CommandName="cmdEdit" data-toggle="tooltip" OnCommand="btnCommand" title="Edit" CommandArgument='<%#Eval("pos_idx") %>'><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                <asp:LinkButton ID="lbDelete" CssClass="btn btn-danger" runat="server" CommandName="cmdDelete" data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบตำแหน่งนี้ใช่หรือไม่ ?')" CommandArgument='<%#Eval("pos_idx") %>' title="Delete"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>



                </Columns>
            </asp:GridView>
        </asp:View>
    </asp:MultiView>
</asp:Content>

