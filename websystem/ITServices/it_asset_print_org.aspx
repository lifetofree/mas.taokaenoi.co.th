﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="it_asset_print_org.aspx.cs" Inherits="websystem_ITServices_it_asset_print_org" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <%-- <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />--%>
    <link href="../../Content/bootstrap.css" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />
    <style type="text/css">
        /* Set the body text family and font size both for screen and print*/
        body {
            width: 1000px;
            margin: 10px;
            font-family: 'Times New Roman';
            font-size: 18px;
        }

        /* Set the title text font size and weight both for screen and print*/
        .title {
            font-size: 24px;
            font-weight: bold;
        }

        @media screen {
            /* Set a background color only when the HTML page is displayed on screen*/
            body {
                background-color: aliceblue;
            }

            /* Use blue to write the text on screen*/
            p {
                color: darkblue;
            }
        }

        @media print {
            /* Hide images when priting*/
            img {
                display: none;
            }

            /* Use black to write the text on screen*/
            p {
                color: black;
            }
        }


        @media screen,print {
            /* Set the paragraph text family and font size both for screen and print*/
            p {
                font-family: 'Times New Roman';
                font-size: 20px;
            }
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <div class="form-group">

            <div class="col-md-12" runat="server" visible="false">
                <blockquote class="danger" style="font-size: small; background-color: lavender;">
                    <p><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp; รายการขอซื้อ</p>

                </blockquote>
            </div>
            <div class="col-md-12">

                <asp:GridView ID="gvlist_org" runat="server"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                    GridLines="None"
                    HeaderStyle-CssClass="info" AllowPaging="true"
                    AutoGenerateColumns="false"
                    HeaderStyle-Height="30px">
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูลรายการขอซื้อ</div>
                    </EmptyDataTemplate>

                    <Columns>

                        <asp:TemplateField HeaderText="สถานที่" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>

                                    <asp:Label ID="lb_org_name_th" runat="server" CssClass="col-sm-12"
                                        Text='<%# Eval("org_name_th") %>'></asp:Label>

                                    <asp:Label ID="lb_u0_docket_idx" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("u0_docket_idx") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lb_org_idx_its" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("org_idx_its") %>'
                                        Visible="false"></asp:Label>

                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อไฟล์" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>

                                    <asp:Label ID="lb_file_name" runat="server" CssClass="col-sm-12"
                                        Text='<%# Eval("file_name") %>'></asp:Label>

                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>

                </asp:GridView>
                 <asp:Button ID="Button1" runat="server" OnClick="btnExportPDF1_Click"
                Text="PDF" />
            </div>
        </div>


    </form>
</body>
</html>
