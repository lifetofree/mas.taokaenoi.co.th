<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="fingerprint_manage.aspx.cs" Inherits="websystem_fingerscan_fingerprint_manage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <div class="row">
        <!--multiview-->
        <asp:MultiView ID="mvSystem" runat="server">
            <asp:View ID="viewCreate" runat="server">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">จัดการลายนิ้วมือ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <asp:FormView ID="fvDetail" runat="server" Width="100%" DefaultMode="Insert">
                                    <InsertItemTemplate>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">รหัสพนักงาน
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static">
                                                    61005686
                                                </p>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">ชื่อ
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static">
                                                นภาภรณ์ วิเชียรสรรค์
                                                </p>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Index 1
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <input type="text" class="form-control">
                                                    <asp:LinkButton ID="lbItemIndex1" runat="server" CssClass="input-group-addon"><i class="far fa-edit"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Index 2
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <input type="text" class="form-control">
                                                    <asp:LinkButton ID="lbItemIndex2" runat="server" CssClass="input-group-addon"><i class="far fa-edit"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Index 3 :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <input type="text" class="form-control">
                                                    <asp:LinkButton ID="lbItemIndex3" runat="server" CssClass="input-group-addon"><i class="far fa-edit"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Index 4 :</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <input type="text" class="form-control">
                                                    <asp:LinkButton ID="lbItemIndex4" runat="server" CssClass="input-group-addon"><i class="far fa-edit"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0">
                                                    <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                            </div>
                                        </div>
                                    </InsertItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="viewList" runat="server">
                <div class="col-md-12">
                    <div class="panel panel-default" ID="div_heading" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title"><asp:Literal ID="litHeadingTitle" runat="server" Text="ค้นหาข้อมูลพนักงาน"></asp:Literal></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <asp:FormView ID="fvEmpSearch" runat="server" Width="100%" DefaultMode="Insert">
                                    <InsertItemTemplate>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน" />
                                        </div>
                                        <label class="col-md-2 control-label">ชื่อ(TH/EN)</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" placeholder="ชื่อ(TH/EN)" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">องค์กร</label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlOrg" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="--- องค์กร ---" Value="-1" />
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-md-2 control-label">ฝ่าย</label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="--- ฝ่าย ---" Value="-1" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">แผนก</label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlSec" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="--- แผนก ---" Value="-1" />
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-md-2 control-label">Cost Center</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbCostCenter" runat="server" CssClass="form-control" placeholder="Cost Center" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">ประเภทพนักงาน</label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlEmpType" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="--- ประเภทพนักงาน ---" Value="-1" />
                                                <asp:ListItem Text="รายวัน" Value="1" />
                                                <asp:ListItem Text="รายเดือน" Value="2" />
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-md-2 control-label">สถานะพนักงาน</label>
                                        <label class="col-md-4 control-label-static">
                                            <asp:DropDownList ID="ddlEmpStatus" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="พนักงาน" Value="1" />
                                                <asp:ListItem Text="พ้นสภาพพนักงาน" Value="0" />
                                            </asp:DropDownList>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <label class="col-md-10 control-label-static">
                                            <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearch" CommandArgument="0">
                                                <i class="fas fa-search" aria-hidden="true"></i>&nbsp;ค้นหา</asp:LinkButton>
                                            <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                                <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                            <asp:LinkButton ID="lbGetTemplate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdGetTemplate" CommandArgument="0">
                                                <i class="fas fa-fingerprint" aria-hidden="true"></i>&nbsp;ดึงลายนิ้วมือเข้าระบบ</asp:LinkButton>
                                        </label>
                                        <!-- <label class="col-md-6 control-label"></label> -->
                                    </div>
                                    </InsertItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </div>
                    <asp:GridView ID="gvEmployeeList" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive" OnRowDataBound="gvRowDataBound" OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10"
                    DataKeyNames="emp_idx">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbEmpSelected" runat="server" AutoPostBack="true" OnCheckedChanged="cbCheckedChanged" Text='<%# Eval("emp_idx") + "|" + Eval("emp_code") + "|" + Eval("emp_firstname_th") + " " + Eval("emp_lastname_th") + " (" + Eval("emp_nickname_th") + ") " %>' CssClass="hiddenText" Style="color: transparent;"></asp:CheckBox>
                                    <asp:HiddenField ID="hfEmpIdx" runat="server" Value='<%# Eval("emp_idx")%>'></asp:HiddenField>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รหัสพนักงาน">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อ">
                                <ItemTemplate>
                                    <asp:Label ID="lblFirstnameTH" runat="server" Text='<%# Eval("emp_firstname_th") %>'></asp:Label>
                                    <asp:Label ID="lblLastnameTH" runat="server" Text='<%# Eval("emp_lastname_th") %>'></asp:Label>
                                    (<asp:Label ID="lblNicknameTH" runat="server" Text='<%# Eval("emp_nickname_th") %>'></asp:Label>)
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="องค์กร">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrganizationTH" runat="server" Text='<%# Eval("org_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ฝ่าย">
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartmentTH" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="แผนก">
                                <ItemTemplate>
                                    <asp:Label ID="lblSectionTH" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ตำแหน่ง">
                                <ItemTemplate>
                                    <asp:Label ID="lblPositionTH" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cost Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblCostCenter" runat="server" Text='<%# Eval("costcenter_no") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ">
                                <ItemTemplate>
                                    <asp:Label ID="lbEmpStatus" runat="server" Text='<%# getStatusIcon((Int32)Eval("emp_status")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="การจัดการ" Visible="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbFingerPrint" runat="server" CssClass="btn btn-xs btn-success" OnCommand="btnCommand" CommandName="cmdFingerPrint" CommandArgument='<%# Eval("emp_idx") %>'><i class="fas fa-fingerprint" aria-hidden="true"></i>&nbsp;จัดการลายนิ้วมือ</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <table id="table1" runat="server" class="table table-striped table-bordered table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse:collapse; display: none;">
                        <tbody>
                            <tr class="info" style="font-size:Small;height:40px;">
                                <th scope="col">#</th>
                                <th scope="col">รหัสพนักงาน</th>
                                <th scope="col">ชื่อ</th>
                                <th scope="col">ฝ่าย</th>
                                <th scope="col">แผนก</th>
                                <th scope="col">ตำแหน่ง</th>
                                <th scope="col">สถานะ</th>
                                <th scope="col">การจัดการ</th>
                            </tr>
                            <tr style="font-size:Small;">
                                <td><input type="checkbox" /></td>
                                <td>61005686</td>
                                <td>นภาภรณ์ วิเชียรสรรค์</td>
                                <td>ฝ่ายทรัพยากรบุคคลและธุรการ - โรงงานโรจนะ</td>
                                <td>ส่วนงานธุรการทั่วไป - โรงงานโรจนะ</td>
                                <td>เจ้าหน้าที่ธุรการ</td>
                                <td><i class="fas fa-check-circle text-success"></i></td>
                                <td>
                                    <asp:LinkButton ID="lbFingerPrint" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdFingerPrint" CommandArgument="0">
                                        <i class="fas fa-fingerprint" aria-hidden="true"></i>&nbsp;จัดการลายนิ้วมือ</asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size:Small;">
                                <td><input type="checkbox" /></td>
                                <td>61005526</td>
                                <td>สุพัตรา หาเรือนศรี</td>
                                <td>ฝ่ายทรัพยากรบุคคลและธุรการ - โรงงานโรจนะ</td>
                                <td>ส่วนงานธุรการทั่วไป - โรงงานโรจนะ</td>
                                <td>เจ้าหน้าที่ธุรการ</td>
                                <td><i class="fas fa-check-circle text-success"></i></td>
                                <td>
                                    <a id="ContentMain_gvList_lbEdit_0" class="btn btn-success" href="#"><i class="fas fa-fingerprint" aria-hidden="true"></i>&nbsp;จัดการลายนิ้วมือ</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="row" style="margin-bottom: 20px;" ID="div_Action" runat="server">
                        <asp:LinkButton ID="lbTransfer" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdTransfer" CommandArgument="0">
                            <i class="far fa-share-square" aria-hidden="true"></i>&nbsp;โอนลายนิ้วมือ</asp:LinkButton>
                        <asp:LinkButton ID="lbDelete" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdDelete" CommandArgument="0">
                            <i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบลายนิ้วมือ</asp:LinkButton>
                    </div>

                    <div class="row" ID="div_transfer" runat="server">
                        <div class="panel panel-primary" ID="div_heading2" runat="server">
                            <div class="panel-heading">
                                <h5 class="panel-title"><asp:Literal ID="litHeadingTitle2" runat="server" Text="โอนลายนิ้วมือ"></asp:Literal></h5>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        รายชื่อพนักงาน
                                        <asp:GridView ID="gvEmployeeSelected" runat="server" AutoGenerateColumns="False"
                                        CssClass="table table-striped table-bordered table-hover table-responsive" OnRowDataBound="gvRowDataBound" OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10"
                                        DataKeyNames="emp_idx">
                                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                            <RowStyle Font-Size="Small" />
                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">ไม่พบข้อมูล</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="รหัสพนักงาน">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hfEmpIdx" runat="server" Value='<%# Eval("emp_idx")%>'></asp:HiddenField>
                                                        <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ชื่อ">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpNameTH" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="การจัดการ">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbDelete" runat="server" CssClass="btn btn-xs btn-danger" OnCommand="btnCommand" CommandName="cmdActionDelete" CommandArgument='<%# Eval("emp_idx") %>'><i class="far fa-trash-alt" aria-hidden="true"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <table id="table2" runat="server" class="table table-striped table-bordered table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse:collapse; display: none;">
                                            <tbody>
                                                <tr class="info" style="font-size:Small;height:40px;">
                                                    <th scope="col">รหัสพนักงาน</th>
                                                    <th scope="col">ชื่อ</th>
                                                    <th scope="col">การจัดการ</th>
                                                </tr>
                                                <tr style="font-size:Small;">
                                                    <td>61005686</td>
                                                    <td>นภาภรณ์ วิเชียรสรรค์</td>
                                                    <td>
                                                        <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        รายการเครื่องสแกน
                                        <div class="row" style="margin-top: 5px; margin-bottom: 5px;">
                                            <div class="col-md-3 clearpm">
                                                <asp:DropDownList ID="ddlZone" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="--- Zone ---" Value="-1" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-3 clearpm">
                                                <asp:DropDownList ID="ddlPlace" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="--- สถานที่ตั้ง ---" Value="-1" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-6 clearpm">
                                                <div class="input-group">
                                                    <asp:DropDownList ID="ddlLoc" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="--- ตำแหน่งที่ตั้ง ---" Value="-1" />
                                                    </asp:DropDownList>
                                                    <asp:LinkButton ID="lbMachineSearch" runat="server" CssClass="input-group-addon" OnCommand="btnCommand" CommandName="cmdMachineSearch"><i class="fas fa-search"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lbMachineReset" runat="server" CssClass="input-group-addon" OnCommand="btnCommand" CommandName="cmdMachineReset"><i class="fas fa-sync-alt"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:GridView ID="gvMachineList" runat="server" AutoGenerateColumns="False"
                                        CssClass="table table-striped table-bordered table-hover table-responsive" OnRowDataBound="gvRowDataBound" OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10"
                                        DataKeyNames="m0_idx">
                                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                            <RowStyle Font-Size="Small" />
                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">ไม่พบข้อมูล</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="#">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbMachineSelected" runat="server" AutoPostBack="true" OnCheckedChanged="cbCheckedChanged" Text='<%# Eval("m0_idx") + "|" + Eval("ip_host") %>' CssClass="hiddenText" Style="color: transparent;"></asp:CheckBox>
                                                        <asp:HiddenField ID="hfM0Idx" runat="server" Value='<%# Eval("m0_idx")%>'></asp:HiddenField>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="IP / Host name">
                                                    <ItemTemplate>
                                                        <% if(Session["emp_idx"].ToString() == "172" || Session["emp_idx"].ToString() == "24047") { %>
                                                        <asp:Label ID="lblIpHost" runat="server" Text='<%# Eval("ip_host") %>'></asp:Label>
                                                        <% } else { %>
                                                            -
                                                        <% } %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Machine Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMachineName" runat="server" Text='<%# Eval("machine_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Zone">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblZoneName" runat="server" Text='<%# Eval("zone_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="สถานที่ตั้ง">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPlaceName" runat="server" Text='<%# Eval("place_name") + " - " + Eval("loc_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <table id="table3" runat="server" class="table table-striped table-bordered table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse:collapse; display: none;">
                                            <tbody>
                                                <tr class="info" style="font-size:Small;height:40px;">
                                                    <th scope="col">#</th>
                                                    <th scope="col">IP / Host name</th>
                                                    <th scope="col">Zone</th>
                                                    <th scope="col">สถานที่ตั้ง</th>
                                                </tr>
                                                <tr style="font-size:Small;">
                                                    <td><input type="checkbox" /></td>
                                                    <td>192.168.3.1</td>
                                                    <td scope="col">NPW-A</td>
                                                    <td>NPW(นพวงศ์) - หน้าประตูทางเข้า</td>
                                                </tr>
                                                <tr style="font-size:Small;">
                                                    <td><input type="checkbox" /></td>
                                                    <td>192.168.3.2</td>
                                                    <td scope="col">NPW-A</td>
                                                    <td>NPW(นพวงศ์) - หน้าประตูทางเข้า</td>
                                                </tr>
                                                <tr style="font-size:Small;">
                                                    <td><input type="checkbox" /></td>
                                                    <td>192.168.3.3</td>
                                                    <td scope="col">NPW-A</td>
                                                    <td>NPW(นพวงศ์) - หน้าประตูทางเข้า</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <asp:LinkButton ID="lbTransferItem" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdTransferItem" CommandArgument="0" Visible="false">
                                        <i class="far fa-share-square" aria-hidden="true"></i>&nbsp;ยืนยันการโอนลายนิ้วมือ</asp:LinkButton>
                                    <asp:LinkButton ID="lbDeleteItem" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdDeleteItem" CommandArgument="0" Visible="false">
                                        <i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ยืนยันการลบลายนิ้วมือ</asp:LinkButton>
                                    <asp:LinkButton ID="lbResetItem" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdResetItem" CommandArgument="0">
                                        <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="viewTransfer" runat="server">
                <div class="col-md-12 form-group">
                    <asp:LinkButton ID="lbTransferBack" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdBackFromTransfer" CommandArgument="0">
                        <i class="fas fa-chevron-left" aria-hidden="true"></i>&nbsp;Back</asp:LinkButton>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-success" ID="div_heading_transfer" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title"><asp:Literal ID="litHeadingTitleTransfer" runat="server" Text="ดึงลายนิ้วมือเข้าระบบ"></asp:Literal></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-md-2 col-md-offset-3 control-label">สถานที่</label>
                                    <div class="col-md-4">
                                        <asp:DropDownList ID="ddlMachineList" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="--- เครื่อง ---" Value="0" />
                                            <asp:ListItem Text="สำนักงานเมืองทอง" Value="10.1.101.13" />
                                            <asp:ListItem Text="โรงงานโรจนะ" Value="10.3.3.30" />
                                            <asp:ListItem Text="โรงงานนพวงศ์" Value="192.168.110.14" />
                                            <asp:ListItem Text="ตึกโทบิ" Value="192.168.211.9" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-md-offset-3 control-label"></label>
                                    <div class="col-md-4">
                                        <asp:LinkButton ID="lbGetTransferTemplate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdGetTransferTemplate" CommandArgument="0">
                                            <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;ดึงลายนิ้วมือเข้าระบบ</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-md-offset-3 control-label">Result</label>
                                    <div class="col-md-4">
                                        <p class="form-control-static">
                                            <asp:Literal ID="litTransferResult" runat="server" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>