﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_m0_course_costcenter.aspx.cs" Inherits="websystem_elearning_master_el_el_m0_course_costcenter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFile").MultiFile();
            }
        })
    </script>

    <asp:Literal ID="text" runat="server"></asp:Literal>

    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp;Course Cost Center (งบประมานอบรมแต่ละคอร์สเซ็นเตอร์) </strong></h3>
                </div>

                <div class="panel-body">

                    <div class="form-group">
                        <asp:LinkButton ID="btnaddholder" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Add Cost Center" runat="server" CommandName="CmdAddHolder" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                    </div>


                    <%------------------------ Div ADD  ------------------------%>

                    <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Master Course Cost Center</strong></h4>
                            <div class="form-horizontal" role="form">
                                <%--<div class="panel panel-default">--%>
                                <div class="panel-heading">



                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <asp:Label ID="Label141" runat="server" Text="ประเภทการสร้าง" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddlchoose_addcost" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกประเภทการสร้าง" />
                                                        <asp:ListItem Value="1" Text="สร้างเอง" />
                                                        <asp:ListItem Value="2" Text="Import File" />
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator38" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlchoose_addcost" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกประเภทการสร้าง"
                                                        ValidationExpression="กรุณาเลือกประเภทการสร้าง" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender57" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator38" Width="160" />

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlchoose_addcost" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <div class="col-md-12" id="div3" runat="server" style="color: transparent;">
                                        <asp:FileUpload ID="FileUpload3" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="xls" />
                                    </div>
                                    <div id="div_addmanual" runat="server" visible="false">


                                        <div class="form-group">
                                            <asp:Label ID="Label3" runat="server" Text="Cost Center" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-5">
                                                <asp:DropDownList ID="ddlcost_center" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                            <asp:RequiredFieldValidator ID="Requi3idator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlcost_center" Font-Size="11"
                                                ErrorMessage="select cost center ...."
                                                ValidationExpression="select cost cente ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requi3idator1" Width="160" />
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label26" runat="server" Text="จำนวนเงิน" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-5">
                                                <asp:TextBox ID="txtcourse_nub" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                            </div>
                                            <asp:RegularExpressionValidator ID="R_course_nub" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtcourse_nub" ValidationExpression="^[0-9'.\s]{1,10}$" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="VS_Height2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_course_nub" Width="160" />

                                            <asp:RequiredFieldValidator ID="Reqtor1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtcourse_nub" Font-Size="11"
                                                ErrorMessage="Plese course salary" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validatoender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtor1" Width="160" />
                                        </div>

                                        <div class="form-group" id="divdetail" runat="server" visible="false">
                                            <asp:Label ID="Label1" runat="server" Text="รายละเอียด" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-5">
                                                <asp:TextBox ID="txtcourse_detail" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                            </div>
                                            <asp:RequiredFieldValidator ID="Reator2" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtcourse_detail" Font-Size="11"
                                                ErrorMessage="Plese course_detail" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reator2" Width="160" />
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label4" runat="server" Text="ปีที่ของบประมาณ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-5">
                                                <asp:DropDownList ID="ddlyear" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <%--<div class="form-group">
                                                <asp:Label ID="Label25" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddl_status" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1">Online.....</asp:ListItem>
                                                        <asp:ListItem Value="0">Offline.....</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>--%>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="lbladd" ValidationGroup="SaveAdd" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>


                                    <div id="div_addimport" runat="server" visible="false">

                                        <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:Label ID="Label181" class="col-sm-3 control-label text_right" runat="server" />
                                                    <div class="col-sm-6">
                                                        <u>
                                                            <asp:HyperLink ID="HyperLink1" runat="server" ForeColor="red"
                                                                NavigateUrl="https://docs.google.com/spreadsheets/d/13Q42yXVimrJa_OWMWYi-Mev71jgCNyVYkEWLMdlJavs/edit?usp=sharing" Target="_blank"
                                                                CommandName="_divImportMat"
                                                                OnCommand="btnCommand" Text="Template สำหรับ Import กรุณาอย่าเปลี่ยน Format!!" /></u>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnAdddata_import" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>

                                                <div class="form-group">
                                                    <asp:Label ID="Label142" class="col-sm-3 control-label text_right" runat="server" Text="Upload File : " />
                                                    <div class="col-sm-6">
                                                        <%-- <asp:FileUpload ID="UploadFile" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi max-1" accept="xls" />--%>

                                                        <asp:FileUpload ID="UploadFile" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi max-1" accept="xls" />


                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator39"
                                                            runat="server" ControlToValidate="UploadFile" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="กรุณาอัพโหลดไฟล์"
                                                            ValidationGroup="Save" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender58" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator39" Width="200" PopupPosition="BottomLeft" />

                                                        <small>
                                                            <p class="help-block"><font color="red">**Attach File name xls</font></p>
                                                        </small>
                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnAdddata_import" />
                                            </Triggers>
                                        </asp:UpdatePanel>


                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="btnAdddata_import" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdImportMaster" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton32" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <hr />
                            <%--  </div>--%>
                        </div>
                    </asp:Panel>


                    <div class="form-group" id="div_search" runat="server">
                        <asp:Label ID="Label121" runat="server" Text="Cost Center" CssClass="col-sm-2 control-label text_right"></asp:Label>
                        <div class="col-sm-3 ">
                            <asp:DropDownList ID="ddl_cost_center_search" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary" runat="server" Text="Search" OnCommand="btnCommand" CommandName="CmdSearch" data-toggle="tooltip" title="Search"><i class="fa fa-search"></i></asp:LinkButton>
                        <asp:LinkButton ID="btnrefresh" CssClass="btn btn-default" runat="server" Text="Refresh" OnCommand="btnCommand" CommandName="CmdRefresh" data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i></asp:LinkButton>

                    </div>


                    <asp:GridView ID="GvMaster" runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        HeaderStyle-CssClass="primary"
                        HeaderStyle-Height="40px"
                        AllowPaging="true"
                        DataKeyNames="CCIDX"
                        PageSize="10"
                        OnRowDataBound="Master_RowDataBound"
                        OnRowEditing="Master_RowEditing"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowUpdating="Master_RowUpdating"
                        OnRowCancelingEdit="Master_RowCancelingEdit">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="#">

                                <ItemTemplate>
                                    <asp:Label ID="lblCCIDX" runat="server" Visible="false" Text='<%# Eval("CCIDX") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </ItemTemplate>

                                <EditItemTemplate>
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="txtCCIDX_edit" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("CCIDX")%>' />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="Cost Center" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <asp:Label ID="lblcost_center_edit" runat="server" Visible="false" Text='<%# Eval("cost_idx")%>'></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddlcost_center_edit" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="Requor1" ValidationGroup="SaveEdit" runat="server" Display="None"
                                                    ControlToValidate="ddlcost_center_edit" Font-Size="11"
                                                    ErrorMessage="select cost center ...."
                                                    ValidationExpression="select cost cente ...." InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requor1" Width="160" />
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="งบประมาณ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:TextBox ID="txtcourse_nub_edit" runat="server" CssClass="form-control" Text='<%# Eval("course_nub")%>' />
                                                </div>
                                                <asp:RegularExpressionValidator ID="R_course_nub_edit" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtcourse_nub_edit" ValidationExpression="^[0-9'.\s]{1,3}$" ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="VS_Height2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_course_nub_edit" Width="160" />

                                                <asp:RequiredFieldValidator ID="Reqtor1_edit" ValidationGroup="SaveEdit" runat="server" Display="None"
                                                    ControlToValidate="txtcourse_nub_edit" Font-Size="11"
                                                    ErrorMessage="Plese course salary" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validatoer2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtor1_edit" Width="160" />
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label2" runat="server" Text="รายละเอียด" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:TextBox ID="txtcourse_detail_edit" runat="server" CssClass="form-control" Text='<%# Eval("course_detail")%>' />
                                                </div>
                                                <asp:RequiredFieldValidator ID="Reatdor2" ValidationGroup="SaveEdit" runat="server" Display="None"
                                                    ControlToValidate="txtcourse_detail_edit" Font-Size="11"
                                                    ErrorMessage="Plese course_detail" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValiutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reatdor2" Width="160" />
                                            </div>

                                            <%--<div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />

                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("course_status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>--%>


                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="SaveEdit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                </EditItemTemplate>

                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="ปีที่ของบ" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblcost_year" runat="server" Text='<%# Eval("year_cost") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Cost Center" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblcost_name" runat="server" Text='<%# Eval("cost_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="งบประมาณทั้งหมด" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblcourse_nub" Visible="false" runat="server" Text='<%# Eval("course_nub") %>'></asp:Label>
                                    <asp:Label ID="lbprice" runat="server"></asp:Label>
                                    บาท
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="งบประมาณคงเหลือ" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblcost_balance" Visible="false" runat="server" Text='<%# Eval("cost_balance") %>'></asp:Label>
                                    <asp:Label ID="lbprice_balance" runat="server"></asp:Label>
                                    บาท
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Edit" Visible="false" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                    <asp:LinkButton ID="Delete" Visible="false" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("CCIDX") %>'><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                    <asp:LinkButton ID="btnviewdetail" CssClass="btn btn-warning btn-sm" runat="server" OnCommand="btnCommand" ToolTip="View" CommandName="CmdDetail" CommandArgument='<%#  Eval("CCIDX") %>'><i class="	fa fa-book"></i></asp:LinkButton>

                                </ItemTemplate>

                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewDetail" runat="server">

            <div class="col-lg-12">
                <div class="panel panel-danger">
                    <div class="panel-heading" style="background-color: #FFD133; color: black;">
                        <h3 class="panel-title"><i class="fa fa-money"></i><strong>&nbsp; ประวัติงบประมาณ</strong></h3>
                    </div>

                    <div class="panel-body">



                        <div style="overflow-x: auto; width: 100%">
                            <asp:GridView ID="GvHistory" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="default"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="m1costidx"
                                OnRowDataBound="Master_RowDataBound"
                                PageSize="10">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#" HeaderStyle-Width="5%">

                                        <ItemTemplate>
                                            <asp:Label ID="lblm1costidx" runat="server" Visible="false" Text='<%# Eval("m1costidx") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>


                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="วันที่สร้างรายการ"  HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="40%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("CreateDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="จำนวนงบประมาณ"  HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="40%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbcost_price" Visible="false" runat="server" Text='<%# Eval("cost_price") %>'></asp:Label>
                                            <asp:Label ID="lbprice" runat="server"></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-2 col-sm-offset-10">
                                <asp:LinkButton ID="btnback" CssClass="btn btn-danger btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="BtnBack" data-toggle="tooltip" title="Close"><i class="glyphicon glyphicon-home"></i> กลับ</asp:LinkButton>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </asp:View>

    </asp:MultiView>

    <%--   </ContentTemplate>
    </asp:UpdatePanel>--%>



    <%--     <script src='<%=ResolveUrl("~/Scripts/jquery.MultiFileNew.js")%>'></script>

    <script>
        $(function () {
            $('.UploadFile').MultiFile({

            });
            alert("1");
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.UploadFile').MultiFile({

                });
                alert("2");
            });
    </script>--%>
</asp:Content>

