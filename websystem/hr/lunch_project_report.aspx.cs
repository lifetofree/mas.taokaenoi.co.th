using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;
using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;

public partial class websystem_hr_lunch_project_report : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_lunch _data_lunch = new data_lunch();
    static double price = 2.50;
    int countSumAmount = 0;

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlLpGetReport = _serviceUrl + ConfigurationManager.AppSettings["urlLpGetReport"];

    static string[,] _plant_list = { { "2", "นพวงศ์" }, { "14", "โรจนะ" } };
    static string[,] _res_npw = { { "0", "ทั้งหมด" }, { "1", "ร้านที่ 1" }, { "2", "ร้านที่ 2" }, { "3", "ร้านที่ 3" } };
    static string[,] _res_rjn = { { "0", "ทั้งหมด" }, { "1", "ร้านที่ 1" }, { "2", "ร้านที่ 2" } };
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!(Session["emp_idx"] != null && (
        Session["emp_idx"].ToString() != "172"
        || Session["emp_idx"].ToString() != "24047"
        || Session["emp_idx"].ToString() != "26237" //61005861
        || Session["emp_idx"].ToString() != "26069" //61005686
        || Session["emp_idx"].ToString() != "24750" //61004988
        || Session["emp_idx"].ToString() != "3834" //59002008
        || Session["emp_idx"].ToString() != "2497" //58000156
        || Session["emp_idx"].ToString() != "1448" //58000036
        || Session["emp_idx"].ToString() != "24854" //61005092
        )))
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            Response.Redirect(ResolveUrl("~/"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
        }
    }

    #region event command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdReset":
                initPage();
                break;
            case "cmdSearch":
                clearViewState();
                search_meal_detail _search_detail = new search_meal_detail();
                _data_lunch.search_meal_list = new search_meal_detail[1];
                _search_detail.s_res_idx = _funcTool.convertToInt(ddlResIdx.SelectedValue).ToString();
                _search_detail.s_start_date = tbStart.Text.Trim();
                _search_detail.s_end_date = tbEnd.Text.Trim();
                _search_detail.s_report_type = _funcTool.convertToInt(ddlReportType.SelectedValue).ToString();
                _search_detail.s_plant_idx = _funcTool.convertToInt(ddlPlantIdx.SelectedValue).ToString();
                _data_lunch.search_meal_list[0] = _search_detail;

                _data_lunch = getReportList(_data_lunch);

                // hide all grid
                gvReportList.Visible = false;
                gvReportList2.Visible = false;
                gvReportList3.Visible = false;

                switch (_funcTool.convertToInt(ddlReportType.SelectedValue))
                {
                    case 1:
                        gvReportList.Visible = true;
                        setGridData(gvReportList, _data_lunch.lunch_meal_list);
                        break;
                    case 2:
                        gvReportList2.Visible = true;
                        setGridData(gvReportList2, _data_lunch.lunch_meal_list);
                        // setGraphColumnReport(litReport, _data_lunch.lunch_meal_list);
                        break;
                    case 3:
                        gvReportList3.Visible = true;
                        setGridData(gvReportList3, _data_lunch.lunch_meal_list);
                        break;
                }

                if (_data_lunch.lunch_meal_list != null)
                {
                    lbExport.Visible = true;
                    ViewState["report_list"] = _data_lunch;
                }
                else
                {
                    lbExport.Visible = false;
                }
                break;
            case "cmdExport":
                int _count_row = 0;

                if (ViewState["report_list"] != null)
                {
                    data_lunch report_data = (data_lunch)ViewState["report_list"];
                    switch (_funcTool.convertToInt(ddlReportType.SelectedValue))
                    {
                        case 1:
                            DataTable tableLunch = new DataTable();
                            tableLunch.Columns.Add("#", typeof(String));
                            tableLunch.Columns.Add("รหัสพนักงาน", typeof(String));
                            tableLunch.Columns.Add("ชื่อ - นามสกุล", typeof(String));
                            tableLunch.Columns.Add("ฝ่าย", typeof(String));
                            tableLunch.Columns.Add("แผนก", typeof(String));
                            tableLunch.Columns.Add("ร้าน", typeof(String));
                            tableLunch.Columns.Add("เวลาที่บันทึก", typeof(String));

                            foreach (var temp_row in report_data.lunch_meal_list)
                            {
                                DataRow add_row = tableLunch.NewRow();

                                add_row[0] = (_count_row + 1).ToString();
                                add_row[1] = temp_row.emp_code.ToString();
                                add_row[2] = temp_row.emp_name_th.ToString();
                                add_row[3] = temp_row.dept_name_th.ToString();
                                add_row[4] = temp_row.sec_name_th.ToString();
                                add_row[5] = "ร้านที่ " + temp_row.res_idx.ToString();
                                add_row[6] = temp_row.create_date.ToString();

                                tableLunch.Rows.InsertAt(add_row, _count_row++);
                            }

                            DataRow sum_row = tableLunch.NewRow();
                            sum_row[5] = "รวม(บาท)";
                            sum_row[6] = (_count_row * price).ToString("#,##0.00");
                            tableLunch.Rows.InsertAt(sum_row, _count_row + 1);

                            WriteExcelWithNPOI(tableLunch, "xls", "report-lunch");
                            break;
                        case 2:
                            DataTable tableLunch2 = new DataTable();
                            tableLunch2.Columns.Add("ปี-เดือน", typeof(String));
                            tableLunch2.Columns.Add("ร้าน", typeof(String));
                            tableLunch2.Columns.Add("จำนวน", typeof(String));
                            tableLunch2.Columns.Add("รวมเป็นเงิน(บาท)", typeof(String));

                            foreach (var temp_row in report_data.lunch_meal_list)
                            {
                                DataRow add_row2 = tableLunch2.NewRow();

                                add_row2[0] = temp_row.create_date.ToString();
                                add_row2[1] = "ร้านที่ " + temp_row.res_idx.ToString();
                                add_row2[2] = temp_row.count_amount.ToString();
                                add_row2[3] = (temp_row.count_amount * price).ToString();

                                tableLunch2.Rows.InsertAt(add_row2, _count_row++);
                                countSumAmount += temp_row.count_amount;
                            }

                            DataRow sum_row2 = tableLunch2.NewRow();
                            sum_row2[1] = "รวม";
                            sum_row2[2] = countSumAmount.ToString();
                            sum_row2[3] = (countSumAmount * price).ToString("#,##0.00");
                            tableLunch2.Rows.InsertAt(sum_row2, _count_row + 1);

                            WriteExcelWithNPOI(tableLunch2, "xls", "report-lunch");
                            break;
                        case 3:
                            DataTable tableLunch3 = new DataTable();
                            tableLunch3.Columns.Add("ปี", typeof(String));
                            tableLunch3.Columns.Add("ร้าน", typeof(String));
                            tableLunch3.Columns.Add("จำนวน", typeof(String));
                            tableLunch3.Columns.Add("รวมเป็นเงิน(บาท)", typeof(String));

                            foreach (var temp_row in report_data.lunch_meal_list)
                            {
                                DataRow add_row3 = tableLunch3.NewRow();

                                add_row3[0] = temp_row.create_date.ToString();
                                add_row3[1] = "ร้านที่ " + temp_row.res_idx.ToString();
                                add_row3[2] = temp_row.count_amount.ToString();
                                add_row3[3] = (temp_row.count_amount * price).ToString();

                                tableLunch3.Rows.InsertAt(add_row3, _count_row++);
                                countSumAmount += temp_row.count_amount;
                            }

                            DataRow sum_row3 = tableLunch3.NewRow();
                            sum_row3[1] = "รวม";
                            sum_row3[2] = countSumAmount.ToString();
                            sum_row3[3] = (countSumAmount * price).ToString("#,##0.00");
                            tableLunch3.Rows.InsertAt(sum_row3, _count_row + 1);

                            WriteExcelWithNPOI(tableLunch3, "xls", "report-lunch");
                            break;
                    }
                }
                break;
        }
    }
    #endregion event command

    #region RowDatabound
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvReportList":
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    int count = gvReportList.Rows.Count;
                    Label lblSum = (Label)e.Row.FindControl("lblSum");
                    lblSum.Text = (count * price).ToString("#,##0.00");
                }
                break;
            case "gvReportList2":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int count = _funcTool.convertToInt(((Label)e.Row.FindControl("lblCountAmount")).Text);
                    Label lblSum = (Label)e.Row.FindControl("lblSum");
                    lblSum.Text = (count * price).ToString("#,##0.00");

                    countSumAmount = countSumAmount + count;
                }
                else if (e.Row.RowType == DataControlRowType.Footer)
                {
                    Label lblSumAmount = (Label)e.Row.FindControl("lblSumAmount");
                    Label lblSumTotal = (Label)e.Row.FindControl("lblSumTotal");
                    lblSumAmount.Text = countSumAmount.ToString();
                    lblSumTotal.Text = (countSumAmount * price).ToString("#,##0.00");
                }
                break;
            case "gvReportList3":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int count = _funcTool.convertToInt(((Label)e.Row.FindControl("lblCountAmount")).Text);
                    Label lblSum = (Label)e.Row.FindControl("lblSum");
                    lblSum.Text = (count * price).ToString("#,##0.00");

                    countSumAmount = countSumAmount + count;
                }
                else if (e.Row.RowType == DataControlRowType.Footer)
                {
                    Label lblSumAmount = (Label)e.Row.FindControl("lblSumAmount");
                    Label lblSumTotal = (Label)e.Row.FindControl("lblSumTotal");
                    lblSumAmount.Text = countSumAmount.ToString();
                    lblSumTotal.Text = (countSumAmount * price).ToString("#,##0.00");
                }
                break;
        }
    }
    #endregion RowDatabound

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveView("viewList", 0);

        setDdlPlantIdx();
        setDdlResIdx(_funcTool.convertToInt(ddlPlantIdx.SelectedValue));
        ddlPlantIdx.SelectedIndex = 0;
        ddlResIdx.SelectedIndex = 0;
        ddlReportType.SelectedIndex = 0;
        tbStart.Text = String.Empty;
        tbEnd.Text = String.Empty;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        ViewState["report_list"] = null;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {
            case "ddlPlantIdx":
                setDdlResIdx(_funcTool.convertToInt(ddlPlantIdx.SelectedValue));
                break;
        }
    }

    protected void setDdlPlantIdx()
    {
        int rows = _plant_list.GetUpperBound(0);
        int columns = _plant_list.GetUpperBound(1);
        ddlPlantIdx.Items.Clear();

        for (int currentRow = 0; currentRow <= rows; currentRow++)
        {
            ListItem li = new ListItem();
            for (int currentColumn = 0; currentColumn <= columns; currentColumn++)
            {
                if (currentColumn == 0)
                {
                    li.Value = _plant_list[currentRow, currentColumn];
                }
                else
                {
                    li.Text = _plant_list[currentRow, currentColumn];
                }
            }
            ddlPlantIdx.Items.Add(li);
        }
    }

    protected void setDdlResIdx(int plantIdx)
    {
        int rows; int columns;
        switch (plantIdx)
        {
            case 2:
                rows = _res_npw.GetUpperBound(0);
                columns = _res_npw.GetUpperBound(1);
                ddlResIdx.Items.Clear();

                for (int currentRow = 0; currentRow <= rows; currentRow++)
                {
                    ListItem li = new ListItem();
                    for (int currentColumn = 0; currentColumn <= columns; currentColumn++)
                    {
                        if (currentColumn == 0)
                        {
                            li.Value = _res_npw[currentRow, currentColumn];
                        }
                        else
                        {
                            li.Text = _res_npw[currentRow, currentColumn];
                        }
                    }
                    ddlResIdx.Items.Add(li);
                }
                break;
            case 14:
                rows = _res_rjn.GetUpperBound(0);
                columns = _res_rjn.GetUpperBound(1);
                ddlResIdx.Items.Clear();

                for (int currentRow = 0; currentRow <= rows; currentRow++)
                {
                    ListItem li = new ListItem();
                    for (int currentColumn = 0; currentColumn <= columns; currentColumn++)
                    {
                        if (currentColumn == 0)
                        {
                            li.Value = _res_rjn[currentRow, currentColumn];
                        }
                        else
                        {
                            li.Text = _res_rjn[currentRow, currentColumn];
                        }
                    }
                    ddlResIdx.Items.Add(li);
                }
                break;
        }
    }

    protected void setActiveView(string activeTab, int masterType)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        switch (activeTab)
        {
            case "viewList":
                // setFormData(fvSearchReport, FormViewMode.Insert, null);
                lbExport.Visible = false;

                setGridData(gvReportList, null);
                setGridData(gvReportList2, null);
                setGridData(gvReportList3, null);

                // hide all grid
                gvReportList.Visible = false;
                gvReportList2.Visible = false;
                gvReportList3.Visible = false;
                litReport.Visible = false;
                break;
        }
    }

    protected void setGraphColumnReport(Literal litName, lunch_meal_detail[] lunch_data)
    {
        // set color for graph
        string[] c_color = new string[10] { "#7cb5ec", "#aa4643", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1" };

        int i;
        string[] v_type = new string[0];
        switch (_funcTool.convertToInt(ddlPlantIdx.SelectedValue))
        {
            case 2:
                break;
            case 14:
                // prepare type list
                v_type = new string[_res_rjn.GetLength(0) - 1];
                // check number of type. not over than 10(c_color)
                if (v_type.Length > c_color.Length + 1) { return; }
                for (int a = 0; a < v_type.Length; a++)
                {
                    v_type[a] = _res_rjn[a+1, 0];
                }
                break;
        }

        // prepare date list
        var select_date = (from x in lunch_data
                            .GroupBy(x => x.create_date)
                           select x);
        int count_date = select_date.Count();
        string[] v_date = new string[count_date];

        i = 0;
        foreach (var item in select_date.ToList())
        {
            try { v_date[i] = item.ElementAt(0).create_date; }
            catch { v_date[i] = "0"; };
            i++;
        }

        // prepare data list
        object[,] v_zcount = new object[v_type.Length, v_date.Length];
        for (int j = 0; j < v_type.Length; j++)
        {
            for (int k = 0; k < v_date.Length; k++)
            {
                var select_value = (from x in lunch_data
                                    .Where(x => x.create_date == v_date[k] &&
                                    x.res_idx == _funcTool.convertToInt(v_type[j]))
                                    select x).ToList();

                try { v_zcount[j, k] = select_value.ElementAt(0).count_amount; }
                catch { v_zcount[j, k] = "0"; };
            }
        }

        // create graph
        Highcharts chart = new Highcharts("chart");

        Series[] s_list = new Series[v_type.Length];
        for (int m = 0; m < v_type.Length; m++)
        {
            object[] v_display = new object[v_date.Length];
            for (int n = 0; n < v_date.Length; n++)
            {
                v_display[n] = v_zcount[m, n];
            }

            s_list[m] = new Series
            {
                Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                Name = v_type[m],
                Data = new Data(v_display),
                Color = System.Drawing.ColorTranslator.FromHtml(c_color[m])
            };
        }

        chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Count" } } });
        chart.SetXAxis(new XAxis { Categories = v_date });
        chart.SetPlotOptions(new PlotOptions { Series = new PlotOptionsSeries { DataLabels = new PlotOptionsSeriesDataLabels { Enabled = true } } });
        chart.SetSeries(
            s_list
        );
        litName.Text = chart.ToHtmlString();
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected data_lunch getReportList(data_lunch _data_lunch)
    {
        return callServicePostLunch(_urlLpGetReport, _data_lunch);
    }

    protected data_lunch callServicePostLunch(string _cmdUrl, data_lunch _data_lunch)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_lunch);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_lunch = (data_lunch)_funcTool.convertJsonToObject(typeof(data_lunch), _local_json);

        return _data_lunch;
    }

    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }
    #endregion reuse
}