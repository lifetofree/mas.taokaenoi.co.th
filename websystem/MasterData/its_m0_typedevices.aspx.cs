﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_its_m0_typedevices : System.Web.UI.Page
{
    #region Connect

    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();
    data_itasset _dtitseet = new data_itasset();

    string _localJson = String.Empty;


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlSelectm0type = _serviceUrl + ConfigurationManager.AppSettings["urlSelectm0type"];
    static string _urlInsert_TypeDevices = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_TypeDevices"];
    static string _urlSelectm1type = _serviceUrl + ConfigurationManager.AppSettings["urlSelectm1type"];
    static string _urlDeleteTypeDevices = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteTypeDevices"];
   
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();
            Select_TypeDevices(GvMaster);
            mergeCell(GvMaster);
        }

    }
    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void select_type(DropDownList ddlName)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxdevices_reference = new devices_reference[1];
        devices_reference device = new devices_reference();

        _dtitseet.boxdevices_reference[0] = device;

        _dtitseet = callServicePostITAsset(_urlSelectm0type, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxdevices_reference, "name_gen", "m0_naidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรุ่นอุปกรณ์...", "0"));

    }

    protected void Select_TypeDevices(GridView Gvname)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxdevices_reference = new devices_reference[1];
        devices_reference select = new devices_reference();

        _dtitseet.boxdevices_reference[0] = select;

        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelectm1type, _dtitseet);
        setGridData(Gvname, _dtitseet.boxdevices_reference);
    }

    protected void Insert_Type(int m0_naidx, int cempidx, string name, int status, int m0_tyidx)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxdevices_reference = new devices_reference[1];
        devices_reference insert = new devices_reference();

        insert.m0_naidx = m0_naidx;
        insert.CEmpIDX = cempidx;
        insert.type_name = name;
        insert.status = status;
        insert.m0_tyidx = m0_tyidx;

        _dtitseet.boxdevices_reference[0] = insert;
      //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlInsert_TypeDevices, _dtitseet);
        ViewState["rtcode"] = _dtitseet.ReturnCode.ToString();
    }

    protected void Delete_Type(int m0_tyidx, int cempidx)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxdevices_reference = new devices_reference[1];
        devices_reference insert = new devices_reference();

        insert.CEmpIDX = cempidx;
        insert.m0_tyidx = m0_tyidx;

        _dtitseet.boxdevices_reference[0] = insert;
      //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlDeleteTypeDevices, _dtitseet);
    }


    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='fa fa-check-circle'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='fa fa-times-circle'></i></span>";
        }
    }

    protected void SetDefaultAdd()
    {
        select_type(ddlbrand);
        ddlbrand.SelectedValue = "0";
        txttype.Text = String.Empty;
        ddStatusadd.SelectedValue = "1";
    }
    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_itasset callServicePostITAsset(string _cmdUrl, data_itasset _dtitseet)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtitseet);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtitseet = (data_itasset)_funcTool.convertJsonToObject(typeof(data_itasset), _localJson);
        return _dtitseet;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }
    #endregion

    #region Gridview


    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        var ddlm0naidx = ((DropDownList)e.Row.FindControl("ddlm0naidx"));
                        var txtm0_naidx = ((TextBox)e.Row.FindControl("txtm0_naidx"));


                        select_type(ddlm0naidx);
                        ddlm0naidx.SelectedValue = txtm0_naidx.Text;
                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                Select_TypeDevices(GvMaster);

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                Select_TypeDevices(GvMaster);

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                Select_TypeDevices(GvMaster);
                mergeCell(GvMaster);
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int m0_tyidx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtm0_naidx = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtm0_naidx");
                var txttype_name = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txttype_name");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                Insert_Type(0, int.Parse(ViewState["EmpIDX"].ToString()), txttype_name.Text, int.Parse(StatusUpdate.SelectedValue),  m0_tyidx);
                Select_TypeDevices(GvMaster);
                mergeCell(GvMaster);
                break;
        }
    }

    #endregion

    #region MergeCell
    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvMaster":
                for (int rowIndex = GvMaster.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvMaster.Rows[rowIndex];
                    GridViewRow previousRow = GvMaster.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[1].FindControl("lbname")).Text == ((Label)previousRow.Cells[1].FindControl("lbname")).Text)
                    {
                        if (previousRow.Cells[1].RowSpan < 2)
                        {
                            currentRow.Cells[1].RowSpan = 2;
                        }
                        else
                        {
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;
                        }
                        previousRow.Cells[1].Visible = false;
                    }
               
                }
                break;
        }
    }
    #endregion

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "btnAdd":
                Insert_Type(int.Parse(ddlbrand.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()), txttype.Text, int.Parse(ddStatusadd.SelectedValue),  0);
                if (ViewState["rtcode"].ToString() != "1")
                {
                    Panel_Add.Visible = false;
                    btnshow.Visible = true;
                    Select_TypeDevices(GvMaster);
                    mergeCell(GvMaster);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                }
                break;
            case "CmdDel":
                Delete_Type(int.Parse(cmdArg), int.Parse(ViewState["EmpIDX"].ToString()));
                Select_TypeDevices(GvMaster);
                mergeCell(GvMaster);
                break;
        }



    }
    #endregion

}