﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class websystem_MasterData_qa_cims_m0_lab : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_qa_cims _data_qa_cims = new data_qa_cims();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];


    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- master data lab --//
    static string _urlCimsGetCal_type = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetCal_type"];
    static string _urlCimsGetPlace = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetPlace"];
    static string _urlCimsSetLab = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetLab"];
    static string _urlCimsGetLab = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetLab"];
    static string _urlCimsDeleteLab = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDeleteLab"];

    //-- master data lab M1 --//
    static string _urlCimsSetLabM1 = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetLabM1"];
    static string _urlCimsGetLabM1 = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetLabM1"];
    static string _urlCimsDeleteLabM1 = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDeleteLabM1"];


    //-- master data equipment name --//
    static string _urlCimsGetEquipment_name = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetEquipment_name"];




    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            initPage();
            //MvMaster_lab.SetActiveView(view_Genaral);
            //Select_lab();
        }

    }
    #endregion

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region initPage
    protected void initPage()
    {

        clearSession();
        clearViewState();
        setActiveView("view_Genaral", 0);
        //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);
        Select_lab();
    }
    #endregion

    #region Select M0 lab
    protected void Select_lab()
    {
        data_qa_cims lab_bSE = new data_qa_cims();
        qa_cims_m0_lab_detail lab_sSE = new qa_cims_m0_lab_detail();

        lab_bSE.qa_cims_m0_lab_list = new qa_cims_m0_lab_detail[1];
        lab_bSE.qa_cims_m0_lab_list[0] = lab_sSE;

        lab_bSE = callServicePostMasterQACIMS(_urlCimsGetLab, lab_bSE);
        setGridData(Gv_select_lab, lab_bSE.qa_cims_m0_lab_list);
        //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(lab_bSE));
    }
    #endregion

    #region Select M1 lab
    protected void Select_labM1()
    {


        data_qa_cims lab_bSE = new data_qa_cims();
        qa_cims_m1_lab_detail lab_sSE = new qa_cims_m1_lab_detail();

        lab_bSE.qa_cims_m1_lab_list = new qa_cims_m1_lab_detail[1];
        lab_bSE.qa_cims_m1_lab_list[0] = lab_sSE;
        lab_sSE.m0_lab_idx = int.Parse(ViewState["m0_lab_idx"].ToString());


        lab_bSE = callServicePostMasterQACIMS(_urlCimsGetLabM1, lab_bSE);
        setGridData(Gv_selectLabM1, lab_bSE.qa_cims_m1_lab_list);

        //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(lab_bSE));
    }
    #endregion

    #region dropdown list

    protected void getM0CaltypeList(DropDownList ddlName, string _cal_type_idx)
    {
        data_qa_cims cal_type_bSE = new data_qa_cims();
        qa_cims_m0_cal_type_detail cal_type_sSE = new qa_cims_m0_cal_type_detail();
        cal_type_bSE.qa_cims_m0_cal_type_list = new qa_cims_m0_cal_type_detail[1];

        cal_type_bSE.qa_cims_m0_cal_type_list[0] = cal_type_sSE;
        cal_type_bSE = callServicePostMasterQACIMS(_urlCimsGetCal_type, cal_type_bSE);

        setDdlData(ddlName, cal_type_bSE.qa_cims_m0_cal_type_list, "cal_type_name", "cal_type_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทแลปทดสอบ ---", "0"));
        ddlName.SelectedValue = _cal_type_idx;

    }


    protected void getM0placeList(DropDownList ddlName, string _place_idx)
    {
        data_qa_cims place_bSE = new data_qa_cims();
        qa_cims_m0_place_detail place_sSE = new qa_cims_m0_place_detail();
        place_bSE.qa_cims_m0_place_list = new qa_cims_m0_place_detail[1];

        //place_sSE.condition = 1;

        place_bSE.qa_cims_m0_place_list[0] = place_sSE;
        place_bSE = callServicePostMasterQACIMS(_urlCimsGetPlace, place_bSE);

        setDdlData(ddlName, place_bSE.qa_cims_m0_place_list, "place_name", "place_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานที่ตรวจ ---", "0"));
        ddlName.SelectedValue = _place_idx;

    }

    protected void getM1equipment_nameList(DropDownList ddlName, string _equipment_idx)
    {
        data_qa_cims equipmentName_B = new data_qa_cims();
        qa_cims_m0_equipment_name_detail equipmentName_S = new qa_cims_m0_equipment_name_detail();
        equipmentName_B.qa_cims_m0_equipment_name_list = new qa_cims_m0_equipment_name_detail[1];
        //equipmentName_B.condition = 1;

        equipmentName_B.qa_cims_m0_equipment_name_list[0] = equipmentName_S;
        equipmentName_B = callServicePostMasterQACIMS(_urlCimsGetEquipment_name, equipmentName_B);

        setDdlData(ddlName, equipmentName_B.qa_cims_m0_equipment_name_list, "equipment_name", "equipment_idx");
        ddlName.Items.Insert(0, new ListItem("--- ชื่ออุปกรณ์ ---", "0"));
        ddlName.SelectedValue = _equipment_idx;

    }

    #endregion

    #region getM1Set_labName
    protected void getM1Set_labName()
    {
        setFormData(fvform_Insert_Root, FormViewMode.Insert, null);
        TextBox tbSetLabList = (TextBox)fvform_Insert_Root.FindControl("tbSetLabList");

        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_lab_list = new qa_cims_m0_lab_detail[1];

        qa_cims_m0_lab_detail _m0SubSet = new qa_cims_m0_lab_detail();
        _dataqacims.qa_cims_m0_lab_list[0] = _m0SubSet;
        // test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataqa));
        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetLab, _dataqacims);
        var _linqSet = (from data_qa_cims in _dataqacims.qa_cims_m0_lab_list
                        where data_qa_cims.m0_lab_idx == int.Parse(ViewState["m0_lab_idx"].ToString())
                        select new
                        {
                            data_qa_cims.lab_name
                        }).ToList();
        foreach (var item in _linqSet)
        {
            tbSetLabList.Text = item.lab_name;

        }
    }

    #endregion

    #region setDdlData
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    #region clear
    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    #endregion

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    #endregion

    #region callService
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    //protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    //{
    //    // convert to json
    //    _localJson = _funcTool.convertObjectToJson(_dataEmployee);
    //    //litDebug.Text = _localJson;

    //    // call services
    //    _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

    //    // convert json to object
    //    _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

    //    return _dataEmployee;
    //}
    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {

            case "cmdAddlab":
                switch (cmdArg)
                {
                    case "0":
                        Gv_select_lab.EditIndex = -1;
                        // btn_addlab.Visible = false;
                        setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                        FormView fvDocDetail_insert_qa = (FormView)view_Genaral.FindControl("Fv_Insert_Result");
                        getM0CaltypeList((DropDownList)fvDocDetail_insert_qa.FindControl("DDcal_type"), "0");
                        getM0placeList((DropDownList)fvDocDetail_insert_qa.FindControl("DD_place"), "0");
                        Fv_Insert_Result.Visible = true;
                        Select_lab();
                        break;
                    case "1":
                        Gv_selectLabM1.EditIndex = -1;
                        btnAdd.Visible = false;
                        setFormData(fvform_Insert_Root, FormViewMode.Insert, null);
                        getM1Set_labName();
                        FormView fvDocDetail_insert_Root_qa = (FormView)view_Genaral.FindControl("fvform_Insert_Root");
                        getM1equipment_nameList((DropDownList)fvDocDetail_insert_Root_qa.FindControl("DD_EquipmentName"), "0");
                        fvform_Insert_Root.Visible = true;
                        Select_labM1();
                        break;
                }
                //btn_addlab.Visible = false;
                //setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                //Fv_Insert_Result.Visible = true;

                break;
            case "cmdCancel":
                switch (cmdArg)
                {
                    case "0":
                        Gv_select_lab.EditIndex = -1;
                        btn_addlab.Visible = true;
                        setActiveView("view_Genaral", 0);
                        Fv_Insert_Result.Visible = false;
                        setFormData(Fv_Insert_Result, FormViewMode.Insert, null);

                        SETFOCUS.Focus();
                        break;
                    case "1":
                        Gv_selectLabM1.EditIndex = -1;
                        btnAdd.Visible = true;
                        setActiveView("view_CreateRoot", 0);
                        fvform_Insert_Root.Visible = false;
                        setFormData(fvform_Insert_Root, FormViewMode.Insert, null);

                        SETFOCUS.Focus();
                        break;
                }

                break;

            case "cmdback":
                //ViewState["m0_lab_idx"] = 0;
                btn_addlab.Visible = true;
                setActiveView("view_Genaral", 0);
                setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                SETFOCUS.Focus();
                break;


            case "Lbtn_cancel_lab":
                btn_addlab.Visible = true;
                setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);
                Fv_Insert_Result.Visible = false;
                break;

            case "cmdSave":
                switch (cmdArg)
                {
                    case "0":
                        //Insert_lab();
                        //FormView fvDocDetail_insert_qa = (FormView)view_Genaral.FindControl("Fv_Insert_Result");
                        TextBox tex_name_lab = (TextBox)Fv_Insert_Result.FindControl("txtlab_names");
                        DropDownList dropD_cal_type = (DropDownList)Fv_Insert_Result.FindControl("DDcal_type");
                        DropDownList dropD_place = (DropDownList)Fv_Insert_Result.FindControl("DD_place");
                        DropDownList dropD_status_lab = (DropDownList)Fv_Insert_Result.FindControl("DDlab_status");


                        data_qa_cims cal_bIN = new data_qa_cims();
                        qa_cims_m0_lab_detail cal_sIN = new qa_cims_m0_lab_detail();
                        cal_bIN.qa_cims_m0_lab_list = new qa_cims_m0_lab_detail[1];

                        cal_sIN.lab_name = tex_name_lab.Text;
                        cal_sIN.cal_type_idx = int.Parse(dropD_cal_type.SelectedValue);
                        cal_sIN.place_idx = int.Parse(dropD_place.SelectedValue);
                        cal_sIN.lab_status = int.Parse(dropD_status_lab.SelectedValue);
                        cal_sIN.cemp_idx = _emp_idx;
                        cal_bIN.qa_cims_m0_lab_list[0] = cal_sIN;
                        //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(lab_bIN));
                        cal_bIN = callServicePostMasterQACIMS(_urlCimsSetLab, cal_bIN);
                        if (cal_bIN.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }
                        else
                        {
                            tex_name_lab.Text = String.Empty;
                            //tex_code_place.Text = String.Empty;
                        }

                        setActiveView("view_Genaral", 0);
                        //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);
                        //Fv_Insert_Result.Visible = true;
                        btn_addlab.Visible = true;
                        Select_lab();

                        SETFOCUS.Focus();
                        break;

                    case "1":
                        TextBox tbRootLab = (TextBox)fvform_Insert_Root.FindControl("tbSetLabList");
                        DropDownList ddEquipmentName = (DropDownList)fvform_Insert_Root.FindControl("DD_EquipmentName");
                        DropDownList ddlRootSetStatus = (DropDownList)fvform_Insert_Root.FindControl("ddlRootSetStatus");
                        RadioButtonList rdBtn_certificate = (RadioButtonList)fvform_Insert_Root.FindControl("rb_cer");



                        data_qa_cims labM1_bIN = new data_qa_cims();
                        qa_cims_m1_lab_detail labM1_sIN = new qa_cims_m1_lab_detail();
                        labM1_bIN.qa_cims_m1_lab_list = new qa_cims_m1_lab_detail[1];

                        labM1_sIN.m0_lab_idx = int.Parse(ViewState["m0_lab_idx"].ToString());
                        labM1_sIN.equipment_idx = int.Parse(ddEquipmentName.SelectedValue);
                        labM1_sIN.status = ddlRootSetStatus.SelectedValue;
                        labM1_sIN.certificate = int.Parse(rdBtn_certificate.SelectedValue);


                        labM1_bIN.qa_cims_m1_lab_list[0] = labM1_sIN;
                        //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(labM1_bIN));
                        labM1_bIN = callServicePostMasterQACIMS(_urlCimsSetLabM1, labM1_bIN);

                        if (labM1_bIN.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }

                        setActiveView("view_CreateRoot", 0);
                        setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);
                        //fvform_Insert_Root.Visible = true;
                        btnAdd.Visible = true;

                        Select_labM1();
                        SETFOCUS.Focus();
                        break;


                }
                break;
            case "btnTodelete_lab":

                int a = int.Parse(cmdArg);
                data_qa_cims lab_Bde = new data_qa_cims();
                qa_cims_m0_lab_detail lab_Sde = new qa_cims_m0_lab_detail();

                lab_Bde.qa_cims_m0_lab_list = new qa_cims_m0_lab_detail[1];

                lab_Sde.m0_lab_idx = int.Parse(a.ToString());

                lab_Bde.qa_cims_m0_lab_list[0] = lab_Sde;
                //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(lab_Bde));
                lab_Bde = callServicePostMasterQACIMS(_urlCimsDeleteLab, lab_Bde);

                if (lab_Bde.return_code == 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);

                }

                else

                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);


                }
                Fv_Insert_Result.Visible = false;
                btn_addlab.Visible = true;
                Select_lab();
                SETFOCUS.Focus();

                break;

            case "btnTodelete_labM1":
                int m1 = int.Parse(cmdArg);

                data_qa_cims labM1_Bde1 = new data_qa_cims();
                qa_cims_m1_lab_detail labM1_Sde = new qa_cims_m1_lab_detail();

                labM1_Bde1.qa_cims_m1_lab_list = new qa_cims_m1_lab_detail[1];

                labM1_Sde.m1_lab_idx = int.Parse(m1.ToString());

                labM1_Bde1.qa_cims_m1_lab_list[0] = labM1_Sde;
                //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(labM1_Bde1));
                labM1_Bde1 = callServicePostMasterQACIMS(_urlCimsDeleteLabM1, labM1_Bde1);

                //if (labM1_Bde.return_code == 0)
                //{

                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);

                //}

                //else

                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);


                //}
                fvform_Insert_Root.Visible = false;
                btnAdd.Visible = true;
                Select_labM1();
                SETFOCUS.Focus();
                break;


            case "cmdview_exa":
                Gv_selectLabM1.EditIndex = -1;
                btnAdd.Visible = true;
                int m0_lab_idx = int.Parse(cmdArg);

                ViewState["m0_lab_idx"] = m0_lab_idx;

                //test_lab.Text = ViewState["m0_lab_idx"].ToString();
                setActiveView("view_CreateRoot", m0_lab_idx);
                setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);
                Select_labM1();
                SETFOCUS.Focus();
                break;
        }

    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gv_select_lab":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblab_status = (Label)e.Row.Cells[4].FindControl("lblab_status");
                    Label lab_statusOpen = (Label)e.Row.Cells[4].FindControl("lab_statusOpen");
                    Label lab_statusClose = (Label)e.Row.Cells[4].FindControl("lab_statusClose");

                    ViewState["_lblab_status"] = lblab_status.Text;


                    if (ViewState["_lblab_status"].ToString() == "1")
                    {
                        lab_statusOpen.Visible = true;
                    }
                    else if (ViewState["_lblab_status"].ToString() == "0")
                    {
                        lab_statusClose.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";


                    //FormView fvDocDetail_insert_qa = (FormView)view_Genaral.FindControl("Fv_Insert_Result");

                    TextBox txt_caltype = (TextBox)e.Row.Cells[0].FindControl("txt_IDcaltype");
                    getM0CaltypeList((DropDownList)e.Row.Cells[0].FindControl("DD_caltype"), txt_caltype.Text);
                    TextBox txt_place = (TextBox)e.Row.Cells[0].FindControl("txt_IDlabplace");
                    getM0placeList((DropDownList)e.Row.Cells[0].FindControl("DD_labplace"), txt_place.Text);

                    btn_addlab.Visible = true;
                    Fv_Insert_Result.Visible = false;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);
                }
                break;

            case "Gv_selectLabM1":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbtestDtail_status = (Label)e.Row.Cells[3].FindControl("lbtestDtail_status");
                    Label testDtail_statusOpen = (Label)e.Row.Cells[3].FindControl("testDtail_statusOpen");
                    Label testDtail_statusClose = (Label)e.Row.Cells[3].FindControl("testDtail_statusClose");



                    ViewState["_lbtestDetail_status"] = lbtestDtail_status.Text;


                    if (ViewState["_lbtestDetail_status"].ToString() == "1")
                    {
                        testDtail_statusOpen.Visible = true;
                    }
                    else if (ViewState["_lbtestDetail_status"].ToString() == "0")
                    {
                        testDtail_statusClose.Visible = true;
                    }

                }


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lb_certificate = (Label)e.Row.Cells[4].FindControl("lb_certificate");
                    Label Lbhas_certificate = (Label)e.Row.Cells[4].FindControl("Lbhas_certificate");
                    Label Lbnohas_certificate = (Label)e.Row.Cells[4].FindControl("Lbnohas_certificate");

                    //Label m0_lab_idx = (Label)e.Row.Cells[1].FindControl("m0_lab_idx");
                    // ViewState["m0_lab_idx"] = int.Parse(m0_lab_idx.Text);


                    ViewState["_lb_certificate"] = lb_certificate.Text;

                    if (ViewState["_lb_certificate"].ToString() == "1")
                    {
                        Lbhas_certificate.Visible = true;
                    }
                    else if (ViewState["_lb_certificate"].ToString() == "0")
                    {
                        Lbnohas_certificate.Visible = true;
                    }

                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    //FormView fvDocDetail_insert_qa = (FormView)view_Genaral.FindControl("Fv_Insert_Result");

                    TextBox txt_equipmentname = (TextBox)e.Row.Cells[0].FindControl("txt_equipmentname");
                    getM1equipment_nameList((DropDownList)e.Row.Cells[0].FindControl("DD_equipmentname"), txt_equipmentname.Text);

                    TextBox text_Certificate = (TextBox)e.Row.Cells[0].FindControl("text_Certificate");
                    RadioButtonList Ra_Certificate = (RadioButtonList)e.Row.Cells[0].FindControl("Ra_Certificate");
                    Ra_Certificate.SelectedValue = text_Certificate.Text;


                    btnAdd.Visible = true;
                    fvform_Insert_Root.Visible = false;
                    //setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);
                }

                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_lab":
                Gv_select_lab.EditIndex = e.NewEditIndex;
                Select_lab();
                break;
            case "Gv_selectLabM1":
                Gv_selectLabM1.EditIndex = e.NewEditIndex;
                Select_labM1();
                break;
        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_lab":

                var id_lab = (TextBox)Gv_select_lab.Rows[e.RowIndex].FindControl("ID_lab");
                var name_lab = (TextBox)Gv_select_lab.Rows[e.RowIndex].FindControl("Name_lab");
                var name_caltype = (DropDownList)Gv_select_lab.Rows[e.RowIndex].FindControl("DD_caltype");
                var place_lab = (DropDownList)Gv_select_lab.Rows[e.RowIndex].FindControl("DD_labplace");
                var status_lab = (DropDownList)Gv_select_lab.Rows[e.RowIndex].FindControl("ddEdit_lab");


                Gv_select_lab.EditIndex = -1;


                data_qa_cims cal_bUP = new data_qa_cims();
                qa_cims_m0_lab_detail cal_sUP = new qa_cims_m0_lab_detail();
                cal_bUP.qa_cims_m0_lab_list = new qa_cims_m0_lab_detail[1];

                cal_sUP.m0_lab_idx = int.Parse(id_lab.Text);
                cal_sUP.cal_type_idx = int.Parse(name_caltype.SelectedValue);
                cal_sUP.place_idx = int.Parse(place_lab.SelectedValue);
                cal_sUP.lab_status = int.Parse(status_lab.SelectedValue);
                cal_sUP.lab_name = name_lab.Text;

                cal_bUP.qa_cims_m0_lab_list[0] = cal_sUP;
                //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(lab_bUP));
                cal_bUP = callServicePostMasterQACIMS(_urlCimsSetLab, cal_bUP);

                Select_lab();

                if (cal_bUP.return_code == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);
                }
                else

                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);
                }
                break;


            case "Gv_selectLabM1":
                var id_labM1 = (TextBox)Gv_selectLabM1.Rows[e.RowIndex].FindControl("ID_labM1");
                var id_labM0 = (TextBox)Gv_select_lab.Rows[e.RowIndex].FindControl("txt_IDM0Edit");
                // var labnameM0 = (TextBox)Gv_select_lab.Rows[e.RowIndex].FindControl("Name_lab1");
                var name_equipmentname = (DropDownList)Gv_selectLabM1.Rows[e.RowIndex].FindControl("DD_equipmentname");
                var status_labM1 = (DropDownList)Gv_selectLabM1.Rows[e.RowIndex].FindControl("ddEdit_statusLab");
                var Ra_Certificate = (RadioButtonList)Gv_selectLabM1.Rows[e.RowIndex].FindControl("Ra_Certificate");

                Gv_selectLabM1.EditIndex = -1;

                data_qa_cims labM1_bUP = new data_qa_cims();
                qa_cims_m1_lab_detail labM1_sUP = new qa_cims_m1_lab_detail();
                labM1_bUP.qa_cims_m1_lab_list = new qa_cims_m1_lab_detail[1];

                labM1_sUP.m1_lab_idx = int.Parse(id_labM1.Text);
                labM1_sUP.equipment_idx = int.Parse(name_equipmentname.SelectedValue);
                labM1_sUP.status = status_labM1.SelectedValue;
                labM1_sUP.certificate = int.Parse(Ra_Certificate.SelectedValue);
                labM1_sUP.m0_lab_idx = int.Parse(ViewState["m0_lab_idx"].ToString());
                // labM1_sUP.lab_name = labnameM0.Text;


                labM1_bUP.qa_cims_m1_lab_list[0] = labM1_sUP;
                //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(labM1_bUP));
                labM1_bUP = callServicePostMasterQACIMS(_urlCimsSetLabM1, labM1_bUP);

                Select_labM1();
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_lab":
                Gv_select_lab.EditIndex = -1;
                Select_lab();
                btn_addlab.Visible = true;
                break;
            case "Gv_selectLabM1":
                Gv_selectLabM1.EditIndex = -1;
                Select_labM1();
                btnAdd.Visible = true;
                break;
        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_lab":
                Gv_select_lab.PageIndex = e.NewPageIndex;
                Gv_select_lab.DataBind();
                Select_lab();
                break;
            case "Gv_selectLabM1":
                Gv_selectLabM1.PageIndex = e.NewPageIndex;
                Gv_selectLabM1.DataBind();
                Select_labM1();
                break;
        }
    }
    #endregion

    #region setActiveView

    protected void setActiveView(string activeTab, int uidx)
    {
        MvMaster_lab.SetActiveView((View)MvMaster_lab.FindControl(activeTab));
        Fv_Insert_Result.Visible = false;
        //fvform_Insert_Root.Visible = false;

        switch (activeTab)
        {
            case "view_Genaral":

                break;

            case "view_CreateRoot":


                // TextBox tbSetLabList = (TextBox)fvform_Insert_Root.FindControl("tbSetLabList");

                // //data_qa _dataqa = new data_qa();
                // //_dataqa.qa_m0_lab_list = new qa_m0_lab[1];

                // //qa_m0_lab _m0SubSet = new qa_m0_lab();
                // //_dataqa.qa_m0_lab_list[0] = _m0SubSet;
                // //// test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataqa));
                // //_dataqa = callServicePostMasterQA(_urlQaget_lab, _dataqa);
                // //var _linqSet = (from data_qa in _dataqa.qa_m0_lab_list
                // //                where data_qa.m0_lab_idx == uidx
                // //                select new
                // //                {
                // //                    data_qa.lab_name
                // //                }).ToList();
                // //foreach (var item in _linqSet)
                // //{
                // //    tbSetLabList.Text = item.lab_name;

                // //}

                //// TextBox ID_labM0 = (TextBox)fvform_Insert_Root.FindControl("ID_labM0");

                // data_qa lab_bSE = new data_qa();
                // qa_m1_lab lab_sSE = new qa_m1_lab();

                // lab_bSE.qa_m1_lab_list = new qa_m1_lab[1];
                // lab_sSE.m0_lab_idx = uidx;
                // lab_bSE.qa_m1_lab_list[0] = lab_sSE;
                //lab_bSE = callServicePostMasterQA(_urlQaget_labM1, lab_bSE);
                // setGridData(Gv_selectLabM1, lab_bSE.qa_m1_lab_list);
                // test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(lab_bSE));


                getM1Set_labName();
                Select_labM1();
                break;
        }
    }

    #endregion
}