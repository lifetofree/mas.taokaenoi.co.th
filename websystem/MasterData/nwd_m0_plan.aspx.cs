﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_nwd_m0_plan : System.Web.UI.Page
{

    #region initial function/data

    function_tool _funcTool = new function_tool();
    data_networkdevices _dtnetde = new data_networkdevices();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlSelectLocation = _serviceUrl + ConfigurationManager.AppSettings["urlSelectLocation"];
    static string urlSelectFloor = _serviceUrl + ConfigurationManager.AppSettings["urlSelectFloor"];
    static string urlGetm0Room = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0Room"];
    //  static string urlSelectPosition = _serviceUrl + ConfigurationManager.AppSettings["urlSelectPosition"];
    static string urlInsertImage = _serviceUrl + ConfigurationManager.AppSettings["urlInsertImage"];
    static string urlSelectImage = _serviceUrl + ConfigurationManager.AppSettings["urlSelectImage"];
    static string urlDeleteImage = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteImage"];

    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;

    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            SelectList();
            mergeCell();
        }

        linkBtnTrigger(btnToInsert);
        linkBtnTrigger(lbsave);
    }
    #endregion

    #region CallService
    protected data_networkdevices callServiceNetwork(string _cmdUrl, data_networkdevices _dtnetde)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dtnetde);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _dtnetde = (data_networkdevices)_funcTool.convertJsonToObject(typeof(data_networkdevices), _localJson);

        return _dtnetde;
    }

    #endregion

    #region Directories_File URL

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr = dt.NewRow();
        DataColumn dc = new DataColumn("FileName", typeof(string));
        dc = new DataColumn("Download", typeof(string));
        dt.Columns.Add("FileName");
        dt.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        foreach (FileInfo file in files)
        {
            if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
            {
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt.Rows.Add(file.Name);
                dt.Rows[i][1] = f[0];
                i++;
            }
        }

        GridView GvMaster = (GridView)ViewIndex.FindControl("GvMaster");
        // Panel gvFileLo1 = (Panel)FormViewDetails_CMP.FindControl("gvFileLo1");

        if (dt.Rows.Count > 0)
        {

            ds.Tables.Add(dt);
            //gvFileLo1.Visible = true;
            GvMaster.DataSource = ds.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            GvMaster.DataBind();
            ds.Dispose();
        }
        else
        {

            GvMaster.DataSource = null;
            GvMaster.DataBind();

        }

    }

    #endregion

    #region SQL
    protected void SelectLocation(DropDownList ddlName)
    {
        data_networkdevices _dtnetde = new data_networkdevices();
        _dtnetde.m0image_list = new m0image_detail[1];
        m0image_detail image = new m0image_detail();

        //  image.CHIDX = 0;

        _dtnetde.m0image_list[0] = image;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnetde = callServiceNetwork(urlSelectLocation, _dtnetde);

        setDdlData(ddlName, _dtnetde.m0image_list, "place_name", "place_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานที่ตั้งอุปกรณ์...", "0"));
    }

    protected void SelectFloorList(DropDownList ddlName)
    {

        data_networkdevices _dtnetde = new data_networkdevices();
        _dtnetde.m0floor_list = new m0floor_detail[1];

        m0floor_detail Floor_add = new m0floor_detail();

        Floor_add.FLIDX = 0;

        _dtnetde.m0floor_list[0] = Floor_add;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnetde = callServiceNetwork(urlSelectFloor, _dtnetde);

        setDdlData(ddlName, _dtnetde.m0floor_list, "Floor_name", "FLIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกชั้นที่ตั้งอุปกรณ์...", "0"));
    }

    protected void SelectBuilding(DropDownList ddlName)
    {
        data_networkdevices _data_networkdevicesindex = new data_networkdevices();
        _data_networkdevicesindex.m0room_list = new m0room_detail[1];

        m0room_detail _m0room_detailindex = new m0room_detail();

        _m0room_detailindex.room_idx = 0;

        _data_networkdevicesindex.m0room_list[0] = _m0room_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_networkdevicesindex));
        _data_networkdevicesindex = callServiceNetwork(urlGetm0Room, _data_networkdevicesindex);

        setDdlData(ddlName, _data_networkdevicesindex.m0room_list, "room_name", "room_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกอาคารที่ตั้งอุปกรณ์...", "0"));
    }

    protected void SelectList()
    {

        data_networkdevices _dtnetde = new data_networkdevices();
        _dtnetde.m0image_list = new m0image_detail[1];
        m0image_detail position_add = new m0image_detail();

        // position_add.REIDX = 0;

        _dtnetde.m0image_list[0] = position_add;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnetde = callServiceNetwork(urlSelectImage, _dtnetde);

        setGridData(GvMaster, _dtnetde.m0image_list);
    }


    #endregion

    #region SetFunction

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        ddlName.Items.Clear();
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected void ClearDefault()
    {
        DropDownList ddlStatus = (DropDownList)ViewInsert.FindControl("ddlStatus");
        //DropDownList ddlchamber = (DropDownList)ViewInsert.FindControl("ddlchamber");
        //DropDownList ddlfloor = (DropDownList)ViewInsert.FindControl("ddlfloor");
        //DropDownList ddlregis = (DropDownList)ViewInsert.FindControl("ddlregis");

        ddlStatus.Items.Clear();
        ddlStatus.AppendDataBoundItems = true;
        ddlStatus.Items.Add(new ListItem("Online", "1"));
        ddlStatus.Items.Add(new ListItem("Offline", "0"));

        SelectLocation(ddlLoc);
        SelectBuilding(ddlBuild);
        SelectFloorList(ddlfloor);
        //SelectRegisNumber(ddlregis);
    }

    #endregion

    #region GridView

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                SelectList();

                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":



                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                else if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[4].FindControl("btnDL11");  //Cells[3] เลือกเซลล์ว่าช่องที่ต้องการแสดงอยู่คอลัมน์ไหน
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[4].FindControl("hidFile11");

                    // TextBox1.Text = hidFile11.Value;
                    // Display the company name in italics.
                    //string LinkHost11 = string.Format("http://{0}", Request.Url.Host);


                    string getPath = ConfigurationSettings.AppSettings["pathfile_networklayout"];
                    if (Directory.Exists(Server.MapPath(getPath + hidFile11.Value)))
                    {
                        btnDL11.Visible = true;
                        btnDL11.NavigateUrl = getPath + hidFile11.Value + "/" + hidFile11.Value + ".jpg";//LinkHost11 + MapURL(hidFile11.Value);
                    }
                    else
                    {
                        btnDL11.Visible = false;

                    }

                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                        var lblLocIDX = (Label)e.Row.FindControl("lblLocIDX");
                        var ddlLocUpdate = (DropDownList)e.Row.FindControl("ddlLocUpdate");
                        var lblBUIDX = (Label)e.Row.FindControl("lblBUIDX");
                        var ddlBuildUpdate = (DropDownList)e.Row.FindControl("ddlBuildUpdate");
                        var lblFLIDX = (Label)e.Row.FindControl("lblFLIDX");
                        var ddlFloorUpdate = (DropDownList)e.Row.FindControl("ddlFloorUpdate");

                        SelectLocation(ddlLocUpdate);
                        ddlLocUpdate.SelectedValue = lblLocIDX.Text;
                        SelectBuilding(ddlBuildUpdate);
                        ddlBuildUpdate.SelectedValue = lblBUIDX.Text;
                        SelectFloorList(ddlFloorUpdate);
                        ddlFloorUpdate.SelectedValue = lblFLIDX.Text;

                    }
                }


                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectList();

                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int imgidx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlLocUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlLocUpdate");
                var ddlBuildUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlBuildUpdate");
                var ddlFloorUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlFloorUpdate");
                var ddlStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlStatusUpdate");
                var txtimage_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtimage_update");

                GvMaster.EditIndex = -1;

                _dtnetde.m0image_list = new m0image_detail[1];
                m0image_detail _m0image_update = new m0image_detail();

                _m0image_update.imgidx = imgidx;
                _m0image_update.LocIDX = int.Parse(ddlLocUpdate.SelectedValue);
                _m0image_update.BUIDX = int.Parse(ddlBuildUpdate.SelectedValue);
                _m0image_update.FLIDX = int.Parse(ddlFloorUpdate.SelectedValue);
                _m0image_update.img_name = txtimage_update.Text;
                _m0image_update.img_status = int.Parse(ddlStatusUpdate.SelectedValue);
                _m0image_update.CEmpIDX = emp_idx;

                _dtnetde.m0image_list[0] = _m0image_update;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));
                _dtnetde = callServiceNetwork(urlInsertImage, _dtnetde);

                if (_dtnetde.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    SelectList();


                }
                else
                {
                    setError(_dtnetde.return_code.ToString() + " - " + _dtnetde.return_msg);
                }


                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectList();
                break;
        }
    }

    #endregion

    #region Trigger
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }
    #endregion

    #region MergeCell

    protected void mergeCell()
    {
        for (int rowIndex = GvMaster.Rows.Count - 2; rowIndex >= 0; rowIndex--)
        {
            GridViewRow currentRow = GvMaster.Rows[rowIndex];
            GridViewRow previousRow = GvMaster.Rows[rowIndex + 1];

            if (((Label)currentRow.Cells[1].FindControl("Locname")).Text == ((Label)previousRow.Cells[1].FindControl("Locname")).Text)
            {
                if (previousRow.Cells[1].RowSpan < 2)
                {
                    currentRow.Cells[1].RowSpan = 2;
                }
                else
                {
                    currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;
                }
                previousRow.Cells[1].Visible = false;
            }

            if (((Label)currentRow.Cells[2].FindControl("Buildname")).Text == ((Label)previousRow.Cells[2].FindControl("Buildname")).Text)
            {
                if (previousRow.Cells[2].RowSpan < 2)
                {
                    currentRow.Cells[2].RowSpan = 2;
                }
                else
                {
                    currentRow.Cells[2].RowSpan = previousRow.Cells[2].RowSpan + 1;
                }
                previousRow.Cells[2].Visible = false;
            }

            if (((Label)currentRow.Cells[1].FindControl("Locname")).Text == ((Label)previousRow.Cells[1].FindControl("Locname")).Text &&
                ((Label)currentRow.Cells[2].FindControl("Buildname")).Text == ((Label)previousRow.Cells[2].FindControl("Buildname")).Text)
            {
                if (previousRow.Cells[1].RowSpan < 2 && previousRow.Cells[2].RowSpan < 2)
                {
                    currentRow.Cells[4].RowSpan = 2;
                }
                else
                {
                    currentRow.Cells[4].RowSpan = previousRow.Cells[4].RowSpan + 1;
                }
                previousRow.Cells[4].Visible = false;
            }
        }
    }

    #endregion


    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        int _cemp_idx;

        m0image_detail _image = new m0image_detail();

        switch (cmdName)
        {
            case "btnToInsert":

                MvMaster.SetActiveView(ViewInsert);
                ClearDefault();


                break;

            case "btnInsert":
                string namefile = txtimage.Text;
                // DropDownList ddlStatus = ((DropDownList)ViewInsert.FindControl("ddlStatus"));
                _cemp_idx = emp_idx;

                _dtnetde.m0image_list = new m0image_detail[1];
                // _r0posit.CHIDX = int.Parse(ddlchamber.SelectedValue);
                _image.FLIDX = int.Parse(ddlfloor.SelectedValue);
                _image.LocIDX = int.Parse(ddlLoc.SelectedValue);
                _image.BUIDX = int.Parse(ddlBuild.SelectedValue);
                _image.img_name = txtimage.Text;
                _image.img_status = int.Parse(ddlStatus.SelectedValue);
                _image.CEmpIDX = _cemp_idx;

                _dtnetde.m0image_list[0] = _image;

                _dtnetde = callServiceNetwork(urlInsertImage, _dtnetde);

                HttpFileCollection hfcit1 = Request.Files;
                UpdatePanel5.Update();

                //litDebug.Text = hfcit1.Count.ToString();

                hfcit1 = Request.Files;
                if (UploadImages.HasFile)
                {
                    try
                    {
                        if (hfcit1.Count >= 1)
                        {
                            for (int ii = 0; ii < hfcit1.Count; ii++)
                            {
                                HttpPostedFile hpfLo = hfcit1[ii];
                                if (hpfLo.ContentLength > 1)
                                {
                                    string getPath = ConfigurationSettings.AppSettings["pathfile_networklayout"];
                                    string RECode1 = namefile.ToString();
                                    string fileName1 = RECode1;
                                    string filePath1 = Server.MapPath(getPath + RECode1);
                                    if (!Directory.Exists(filePath1))
                                    {
                                        Directory.CreateDirectory(filePath1);
                                    }
                                    string extension = Path.GetExtension(hpfLo.FileName);

                                    hpfLo.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + RECode1 + extension); // + extension); //
                                }
                            }
                        }

                    }
                    catch
                    {

                    }
                }
                else
                {
                    //lblmessage.Text = sb.ToString();
                }



                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnetde));
                if (_dtnetde.return_code == 0)
                {
                    //ClearDefault();

                    MvMaster.SetActiveView(ViewIndex);
                    SelectList();
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);

                }
                else
                {
                    setError(_dtnetde.return_code.ToString() + " - " + _dtnetde.return_msg);
                }

                break;


            case "btnCancel":

                ClearDefault();
                SelectList();
                MvMaster.SetActiveView(ViewIndex);
                mergeCell();

                break;

            case "btnDelete":

                int imgidx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _dtnetde.m0image_list = new m0image_detail[1];
                _image.imgidx = imgidx;
                _image.CEmpIDX = _cemp_idx;

                _dtnetde.m0image_list[0] = _image;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                _dtnetde = callServiceNetwork(urlDeleteImage, _dtnetde);


                if (_dtnetde.return_code == 0)
                {

                    SelectList();

                }
                else
                {
                    setError(_dtnetde.return_code.ToString() + " - " + _dtnetde.return_msg);
                }

                break;
        }
    }
    #endregion


}