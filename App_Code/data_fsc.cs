using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_fsc")]
public class data_fsc
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("fsc_random_list")]
    public fsc_random_detail[] fsc_random_list { get; set; }
    [XmlElement("search_fsc_list")]
    public search_fsc_detail[] search_fsc_list { get; set; }
}

[Serializable]
public class fsc_random_detail
{
    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    // employee name profile.
    [XmlElement("prefix_th")]
    public string prefix_th { get; set; }
    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("emp_firstname_en")]
    public string emp_firstname_en { get; set; }
    [XmlElement("emp_lastname_en")]
    public string emp_lastname_en { get; set; }
    [XmlElement("emp_firstname_th")]
    public string emp_firstname_th { get; set; }
    [XmlElement("emp_lastname_th")]
    public string emp_lastname_th { get; set; }

    [XmlElement("emp_nickname_en")]
    public string emp_nickname_en { get; set; }
    [XmlElement("emp_nickname_th")]
    public string emp_nickname_th { get; set; }
    // employee name profile

    // employee organization profile
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("plant_idx")]
    public int plant_idx { get; set; }
    [XmlElement("plant_name")]
    public string plant_name { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }
    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }
    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }
    [XmlElement("pos_name_en")]
    public string pos_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    // employee organization profile

    [XmlElement("survey_status")]
    public int survey_status { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class search_fsc_detail
{
    [XmlElement("s_emp_type_idx")]
    public string s_emp_type_idx { get; set; }
    [XmlElement("s_plant_idx")]
    public string s_plant_idx { get; set; }
    [XmlElement("s_nat_idx")]
    public string s_nat_idx { get; set; }
    [XmlElement("s_rdept_idx")]
    public string s_rdept_idx { get; set; }
}