﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Drawing;

using System.Net.Mail;

using System.Configuration;
using System.IO;

using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

public partial class websystem_ITServices_ITRepair : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    service_mail servicemail = new service_mail();

    function_tool _funcTool = new function_tool();
    private string ODSP_Reletion = "ODSP_Reletion";

    data_employee _dtEmployee = new data_employee();
    dataODSP_Relation dtODSP = new dataODSP_Relation();
    DataReservation datareservation = new DataReservation();
    DataSupportIT _dtsupport = new DataSupportIT();


    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _ret_val = "";
    string _mail_subject = "";
    string _mail_body = "";
    string emailsap = "sap@taokaenoi.co.th";//"kantida3620@gmail.com,mongkonl.d@taokaenoi.co.th";//
    string emailpos = "nawee.t@taokaenoi.co.th,mis_tknl@taokaenoi.co.th";
    string emailit = "mis@taokaenoi.co.th,senior_support@taokaenoi.co.th";
    string emailgm = "basis@taokaenoi.co.th,mis@taokaenoi.co.th";
    //  string emailres = "siriwan.ph@taokaenoi.com";
    string emailres = "siriwan.ph@taokaenoi.co.th";//"yotsapat.k@taokaenoiland.co.th";
    string emailresen = "callcenter@drtobi.co.th";
    // string email = "kantida3620@gmail.com" + "," + "webmaster@taokaenoi.co.th";
    string linkquest = "http://mas.taokaenoi.co.th/itrepair/"; //"http://localhost/mas.taokaenoi.co.th/itrepair/";//"~/itrepair/";//"http://mas.taokaenoi.co.th/itrepair/";
    string deptidx = "20";
    string posidxres = "13";

    public string checkfile;
    string _localJson = "";
    string sum;
    private string check = "1";

    string subsap_comment = "[Comment][MIS/แจ้งซ่อม : SAP] - ";
    string subbi_comment = "[Comment][MIS/แจ้งซ่อม : BI] - ";
    string subbp_comment = "[Comment][MIS/แจ้งซ่อม : BP] - ";
    string subject_comment = String.Empty;
    string url_pos = "http://mas.taokaenoi.co.th/reportitrepair";
    string url_fb = "http://www.taokaenoi.co.th/MAS/ReportFB";
    //string url = "http://172.16.11.5/taokaenoi.co.th/MAS/ReportSap";
    //string url_it = "http://172.16.11.5/taokaenoi.co.th/MAS/ReportIT";


    #region linkcode

    static string keyitrepair = ConfigurationManager.AppSettings["keyitrepair"];

    #endregion

    #region URL

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];


    static string urlSelect_List = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_List"];
    static string urlSelect_List_Search = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_List_Search"];
    static string urlSelect_SYSTEMList = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_SYSTEMList"];
    static string urlSelect_StatusSAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_StatusSAP"];

    static string urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetPlace"];
    static string urlSelect_Priority = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Priority"];
    static string urlSelect_SAPList = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_SAPList"];
    static string urlSelect_SAPALLList = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_SAPALLList"];
    static string urlSelect_SAPStatuWaiating = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_SAPStatuWaiating"];
    static string urlSelect_SAPStatuDoing = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_SAPStatuDoing"];
    static string urlSelect_SAPStatuWaiatingUserComment = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_SAPStatuWaiatingUserComment"];
    static string urlGetOrganization = _serviceUrl + ConfigurationManager.AppSettings["urlOrganization"];

    static string urlSelect_DetailList = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_DetailList"];
    static string urlSelect_DetailCloseJobList = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_DetailCloseJobList"];
    static string urlSelect_GvComment_List = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_GvComment_List"];
    static string urlSelect_ddlLV1SAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV1"];
    static string urlSelect_ddlLV2SAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV2"];
    static string urlSelect_ddlLV3SAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV3"];
    static string urlSelect_ddlLV4SAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV4"];
    static string urlSelect_ddlLV5SAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV5"];


    static string urlSelect_ddlLV1IT = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV1IT"];
    static string urlSelect_ddlLV2IT = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV2IT"];
    static string urlSelect_ddlLV3IT = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV3IT"];
    static string urlSelect_ddlLV4IT = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV4IT"];
    static string urlSelect_ITStatuWaiating = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ITStatuWaiating"];
    static string urlSelect_ITStatuDoing = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ITStatuDoing"];
    static string urlSelectShow_ITStatus = _serviceUrl + ConfigurationManager.AppSettings["urlSelectShow_ITStatus"];
    static string urlSelectChooseOther = _serviceUrl + ConfigurationManager.AppSettings["urlSelectChooseOther"];
    static string urlSelect_Remote = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Remote"];



    static string urlInsertItrepair = _serviceUrl + ConfigurationManager.AppSettings["urlInsertItrepair"];
    static string urlInsertCommentIT = _serviceUrl + ConfigurationManager.AppSettings["urlInsertCommentIT"];
    static string urlInsertCommentSAP = _serviceUrl + ConfigurationManager.AppSettings["urlInsertCommentSAP"];
    static string urlInsertCommentGM = _serviceUrl + ConfigurationManager.AppSettings["urlInsertCommentGM"];

    static string urlInsertWorkTime = _serviceUrl + ConfigurationManager.AppSettings["urlInsertWorkTime"];
    static string urlInsertInsertSAPList = _serviceUrl + ConfigurationManager.AppSettings["urlInsertInsertSAPList"];
    static string urlInsertITList = _serviceUrl + ConfigurationManager.AppSettings["urlInsertITList"];
    static string urlInsertGMList = _serviceUrl + ConfigurationManager.AppSettings["urlInsertGMList"];
    static string urlInsertPOSList = _serviceUrl + ConfigurationManager.AppSettings["urlInsertPOSList"];


    static string urlUpdateUploadFile = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateUploadFile"];
    static string urlUpdate_GvComment = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_GvComment"];
    static string urlUpdate_GvCommentIT = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_GvCommentIT"];
    static string urlUpdate_GvCommentGM = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_GvCommentGM"];


    static string urlUpdate_ChangeSystem = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_ChangeSystem"];
    static string urlUpdate_SapGetJob = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_SapGetJob"];
    static string urlUpdate_ITGetJob = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_ITGetJob"];
    static string urlUpdate_GMGetJob = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_GMGetJob"];
    static string urlUpdate_POSGetJob = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_POSGetJob"];


    static string urlSap_CloseJob = _serviceUrl + ConfigurationManager.AppSettings["urlSap_CloseJob"];
    static string urlIT_CloseJob = _serviceUrl + ConfigurationManager.AppSettings["urlIT_CloseJob"];
    static string urlGM_CloseJob = _serviceUrl + ConfigurationManager.AppSettings["urlGM_CloseJob"];
    static string urlPOS_CloseJob = _serviceUrl + ConfigurationManager.AppSettings["urlPOS_CloseJob"];

    static string urlSelect_GvQuestionIT_List = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_GvQuestionIT_List"];
    static string urlInsert_GvQuestionIT_List = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_GvQuestionIT_List"];
    static string urlInsert_GvQuestionIT1_List = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_GvQuestionIT1_List"];

    static string urlUpdate_GvCommentPOS = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_GvCommentPOS"];
    static string urlInsertCommentPOS = _serviceUrl + ConfigurationManager.AppSettings["urlInsertCommentPOS"];

    static string urlSelectCaseLV1POSU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV1POSU0"];
    static string urlSelectCaseLV2POSU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV2POSU0"];
    static string urlSelectCaseLV3POSU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV3POSU0"];
    static string urlSelectCaseLV4POSU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV4POSU0"];

    static string urlSelect_CloseJobList = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_CloseJobList"];
    static string urlSelect_Holder = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Holder"];

    static string urlSelect_WaitUserSAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_WaitUserSAP"];
    static string urlSelect_WaitUserIT = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_WaitUserIT"];
    static string urlSelect_WaitUserGM = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_WaitUserGM"];
    static string urlSelect_WaitUserPOS = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_WaitUserPOS"];


    static string urlSelect_BIWaiting = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_BIWaiting"];
    static string urlSelect_BIDoing = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_BIDoing"];
    static string urlSelect_BIWaitingUser = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_BIWaitingUser"];
    static string urlSelect_BIList = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_BIList"];

    static string urlSelect_BPWaiting = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_BPWaiting"];
    static string urlSelect_BPDoing = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_BPDoing"];
    static string urlSelect_BPWaitingUser = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_BPWaitingUser"];
    static string urlSelect_BPList = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_BPList"];

    static string urlSelect_WaitGM = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_WaitGM"];
    static string urlSelect_DoingGM = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_DoingGM"];

    static string urlSelect_WaitPOS = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_WaitPOS"];
    static string urlSelect_DoingPOS = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_DoingPOS"];
    static string urlSelect_StatusGM = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_StatusGM"];
    static string urlSelect_StatusPOS = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_StatusPOS"];
    static string urlSelectm0StatuswaituserrecheckPOS = _serviceUrl + ConfigurationManager.AppSettings["urlSelectm0StatuswaituserrecheckPOS"];


    static string urlUpdateStatusComment = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateStatusComment"];
    static string urlUpdate_SapCloseJob_Comment = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_SapCloseJob_Comment"];
    static string urlSelectMasterDevicesRes = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterDevicesRes"];
    static string urlSelectMasterEN_RES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterEN_RES"];
    static string urlUpdateRESGetJob = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateRESGetJob"];

    static string urlSelectCaseLV1RES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterResCaseLV1_status1"];
    static string urlSelectCaseLV2RES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterResCaseLV2_status1"];
    static string urlSelectCaseLV3RES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterResCaseLV3_status1"];
    static string urlSelectCaseLV4RES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterResCaseLV4_status1"];
    static string urlInsertCommentRES = _serviceUrl + ConfigurationManager.AppSettings["urlInsertCommentRES"];
    static string urlUpdateCommentRES = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateCommentRES"];

    static string urlSelectStatusWaitRES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectStatusWaitRES"];
    static string urlSelectStatusDoingRES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectStatusDoingRES"];
    static string urlSelectStatusWaitUserRES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectStatusWaitUserRES"];
    static string urlSelectStatusRES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectStatusRES"];



    // PHON
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    // PHON


    #endregion

    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {


            ViewState["EmpIDX"] =  int.Parse(Session["emp_idx"].ToString());//
            select_empIdx_present();
            getOrganizationList(ddlSearchOrg);
            Select_List();
            Select_SYSTEMList();

            Select_SAPStatuWaiating();
            Select_SAPStatuDoing();
            Select_SAPStatuWaitUser();
            Select_SAPStatuWaiatingComment();
            Select_BIStatuWaiating();
            Select_BIStatuDoing();
            Select_BIStatuWaitUser();

            Select_BPStatuWaiating();
            Select_BPStatuDoing();
            Select_BPStatuWaitUser();

            Select_ITStatuWaiating();
            Select_ITStatuDoing();
            Select_ITStatuWaitUser();

            Select_GMStatuWaiating();
            Select_GMStatuDoing();
            Select_GMStatuWaitUser();

            Select_POSStatuWaiating();
            Select_POSStatuDoing();
            Select_POSStatuWaitUser();
            Select_waituserrecheckPOS();

            ViewState["Status_SAP"] = "0";
            ViewState["rdept_idx"] = ViewState["rdept_idx"].ToString();
            ViewState["MS1IDX"] = "0";
            ViewState["MS2IDX"] = "0";
            ViewState["MS3IDX"] = "0";
            ViewState["MS4IDX"] = "0";
            ViewState["MS5IDX"] = "0";

            ViewState["CIT1IDX"] = "0";
            ViewState["CIT2IDX"] = "0";
            ViewState["CIT3IDX"] = "0";
            ViewState["CIT4IDX"] = "0";

            ViewState["POS1IDX"] = "0";
            ViewState["POS2IDX"] = "0";
            ViewState["POS3IDX"] = "0";
            ViewState["POS4IDX"] = "0";

            ViewState["RES1IDX"] = "0";
            ViewState["RES2IDX"] = "0";
            ViewState["RES3IDX"] = "0";
            ViewState["RES4IDX"] = "0";

            ViewState["Check_paging"] = "0";
            ViewState["rtcode_mail"] = "0";
            Set_Defult_Index();


            if (ViewState["rdept_idx"].ToString() != "20" && ViewState["rdept_idx"].ToString() != "21" && ViewState["Pos_idx"].ToString() != "13")
            {
                lbReportPOS.Visible = false;
                lbReportQuestIT.Visible = false;
                panel_status.Visible = false;
                div_res.Visible = false;
            }
            else
            {
                if (ViewState["Pos_idx"].ToString() == "13")
                {
                    lbReportPOS.Visible = true;
                    lbReportQuestIT.Visible = false;
                    panel_status.Visible = false;
                    div_res.Visible = true;

                    Select_RESStatuWaiating();
                    Select_REStatuDoing();
                    Select_REStatuWaitUser();

                }
                else if (ViewState["Pos_idx"].ToString() != "13" && ViewState["link"].ToString() == "0")
                {
                    lbReportPOS.Visible = true;
                    lbReportQuestIT.Visible = true;
                    panel_status.Visible = true;
                    div_res.Visible = false;
                }

            }

        }

        linkBtnTrigger(lbindex);
        linkBtnTrigger(lbalterrepair);
        linkBtnTrigger(lbReportPOS);
        linkBtnTrigger(btnAddquest);


        linkBtnTrigger(lbReportQuestIT);
        ImangeBtnTrigger(imgbi);
        ImangeBtnTrigger(imgbp);
        ImangeBtnTrigger(imgsap);
        ImangeBtnTrigger(imgit);
        ImangeBtnTrigger(imggoogle);
        ImangeBtnTrigger(imgpos);
        ImangeBtnTrigger(imgres);


        // Set_Defult_Index();





    }
    #endregion

    #region Link for Email
    protected string Questionrepair(int urqidxquest, int nodequest, int nodestate, int sysidx)
    {
        string urlendcry = linkquest + _funcTool.getEncryptRC4(urqidxquest.ToString() + "|" + nodequest.ToString() + "|" + nodestate.ToString() + "|" + sysidx.ToString(), keyitrepair);

        return urlendcry;
    } // สำหรับลิ้งแจ้ง email
    #endregion

    #region CallService

    protected DataSupportIT callServiceITRepair(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        //text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);

        return _dtsupport;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected DataSupportIT callServicePostITRepair(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);




        return _dtsupport;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    #endregion

    #region Select
    protected void getOrganizationList(DropDownList ddlName)
    {
        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmp(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกฝ่าย....", "0"));
    }

    protected void Select_HolderIT()
    {
        var ddlholder = ((DropDownList)FvInsertSAP.FindControl("ddlholder"));
        var ddlselectother = ((DropDownList)FvInsertSAP.FindControl("ddlselectother"));
        var ddlselect_choose = ((DropDownList)FvInsertSAP.FindControl("ddlselect_choose"));


        ddlholder.Items.Clear();
        ddlholder.AppendDataBoundItems = true;
        ddlholder.Items.Add(new ListItem("เลือกเครื่องถือครอง....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();

        if (ddlselect_choose.SelectedValue == "1")
        {
            closejobit.EmpIDX_add = int.Parse(ViewState["EmpIDX"].ToString());
        }
        else
        {
            closejobit.EmpIDX_add = int.Parse(ddlselectother.SelectedValue);
        }

        _dtsupport.BoxUserRequest[0] = closejobit;


        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));


        _dtsupport = callServicePostITRepair(urlSelect_Holder, _dtsupport);

        var rtCode = _dtsupport.ReturnCode;

        if (rtCode.ToString() == "0")
        {
            ddlholder.DataSource = _dtsupport.BoxUserRequest;
            ddlholder.DataTextField = "u0_code";
            ddlholder.DataValueField = "DeviceIDX";
            ddlholder.DataBind();

            //   ddlholder.SelectedValue = ViewState["DeviceIDX"].ToString();
        }
        else
        {
            ddlholder.AppendDataBoundItems = true;
            ddlholder.Items.Clear();
            ddlholder.Items.Add(new ListItem("เลือกเครื่องถือครอง ....", "0"));
        }

    }

    protected void Select_ddlLV1IT()
    {
        var ddlITLV1 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV1"));

        ddlITLV1.Items.Clear();
        ddlITLV1.AppendDataBoundItems = true;
        ddlITLV1.Items.Add(new ListItem("เลือกเคสแจ้งซ่อม....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();



        _dtsupport.BoxUserRequest[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV1IT, _dtsupport);

        ddlITLV1.DataSource = _dtsupport.BoxUserRequest;
        ddlITLV1.DataTextField = "Name_Code1";
        ddlITLV1.DataValueField = "CIT1IDX";
        ddlITLV1.DataBind();

        ddlITLV1.SelectedValue = ViewState["CIT1IDX"].ToString();


    }

    protected void Select_ddlLV2IT()
    {
        var ddlITLV1 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV1"));
        var ddlITLV2 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV2"));


        ddlITLV2.Items.Clear();
        ddlITLV2.AppendDataBoundItems = true;
        ddlITLV2.Items.Add(new ListItem("เลือกอาการแจ้งซ่อม....", "0"));

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();

        closejobit.CIT1IDX = int.Parse(ddlITLV1.SelectedValue);

        _dtsupport.BoxUserRequest[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV2IT, _dtsupport);

        ddlITLV2.DataSource = _dtsupport.BoxUserRequest;
        ddlITLV2.DataTextField = "Name_Code2";
        ddlITLV2.DataValueField = "CIT2IDX";
        ddlITLV2.DataBind();

        ddlITLV2.SelectedValue = ViewState["CIT2IDX"].ToString();

    }

    protected void Select_ddlLV2IT_Edit()
    {
        var ddlITLV1 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV1"));
        var ddlITLV2 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV2"));


        ddlITLV2.Items.Clear();
        ddlITLV2.AppendDataBoundItems = true;
        ddlITLV2.Items.Add(new ListItem("เลือกอาการแจ้งซ่อม....", "0"));

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();

        closejobit.CIT1IDX = int.Parse(ddlITLV1.SelectedValue);

        _dtsupport.BoxUserRequest[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV2IT, _dtsupport);

        ddlITLV2.DataSource = _dtsupport.BoxUserRequest;
        ddlITLV2.DataTextField = "Name_Code2";
        ddlITLV2.DataValueField = "CIT2IDX";
        ddlITLV2.DataBind();

    }


    protected void Select_ddlLV3IT()
    {
        var ddlITLV2 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV2"));
        var ddlITLV3 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV3"));

        ddlITLV3.Items.Clear();
        ddlITLV3.AppendDataBoundItems = true;
        ddlITLV3.Items.Add(new ListItem("เลือกสาเหตุแจ้งซ่อม....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();

        closejobit.CIT2IDX = int.Parse(ddlITLV2.SelectedValue);

        _dtsupport.BoxUserRequest[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV3IT, _dtsupport);

        ddlITLV3.DataSource = _dtsupport.BoxUserRequest;
        ddlITLV3.DataTextField = "Name_Code3";
        ddlITLV3.DataValueField = "CIT3IDX";
        ddlITLV3.DataBind();

        ddlITLV3.SelectedValue = ViewState["CIT3IDX"].ToString();


    }

    protected void Select_ddlLV3IT_edit()
    {
        var ddlITLV2 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV2"));
        var ddlITLV3 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV3"));

        ddlITLV3.Items.Clear();
        ddlITLV3.AppendDataBoundItems = true;
        ddlITLV3.Items.Add(new ListItem("เลือกสาเหตุแจ้งซ่อม....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();

        closejobit.CIT2IDX = int.Parse(ddlITLV2.SelectedValue);

        _dtsupport.BoxUserRequest[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV3IT, _dtsupport);

        ddlITLV3.DataSource = _dtsupport.BoxUserRequest;
        ddlITLV3.DataTextField = "Name_Code3";
        ddlITLV3.DataValueField = "CIT3IDX";
        ddlITLV3.DataBind();



    }

    protected void Select_ddlLV4IT()
    {
        var ddlITLV3 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV3"));
        var ddlITLV4 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV4"));

        ddlITLV4.Items.Clear();
        ddlITLV4.AppendDataBoundItems = true;
        ddlITLV4.Items.Add(new ListItem("เลือกแก้ไขแจ้งซ่อม....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();

        closejobit.CIT3IDX = int.Parse(ddlITLV3.SelectedValue);

        _dtsupport.BoxUserRequest[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV4IT, _dtsupport);

        ddlITLV4.DataSource = _dtsupport.BoxUserRequest;
        ddlITLV4.DataTextField = "Name_Code4";
        ddlITLV4.DataValueField = "CIT4IDX";
        ddlITLV4.DataBind();

        ddlITLV4.SelectedValue = ViewState["CIT4IDX"].ToString();

    }

    protected void Select_ddlLV4IT_edit()
    {
        var ddlITLV3 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV3"));
        var ddlITLV4 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV4"));

        ddlITLV4.Items.Clear();
        ddlITLV4.AppendDataBoundItems = true;
        ddlITLV4.Items.Add(new ListItem("เลือกแก้ไขแจ้งซ่อม....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();

        closejobit.CIT3IDX = int.Parse(ddlITLV3.SelectedValue);

        _dtsupport.BoxUserRequest[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV4IT, _dtsupport);

        ddlITLV4.DataSource = _dtsupport.BoxUserRequest;
        ddlITLV4.DataTextField = "Name_Code4";
        ddlITLV4.DataValueField = "CIT4IDX";
        ddlITLV4.DataBind();

    }
    protected void Select_Remote()
    {
        var ddlprogram = ((DropDownList)FvInsertSAP.FindControl("ddlprogram"));

        ddlprogram.Items.Clear();
        ddlprogram.AppendDataBoundItems = true;
        ddlprogram.Items.Add(new ListItem("กรุณาเลือกโปรแกรมรีโมท....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();


        _dtsupport.BoxUserRequest[0] = closejobit;

        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));
        //_localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text = _localJson;


        _dtsupport = callServicePostITRepair(urlSelect_Remote, _dtsupport);

        ddlprogram.DataSource = _dtsupport.BoxUserRequest;
        ddlprogram.DataTextField = "RemoteName";
        ddlprogram.DataValueField = "RemoteIDX";
        ddlprogram.DataBind();


    }

    protected void SetDevicesRes(DropDownList ddlName)
    {
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList dtsupport = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = dtsupport;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));
        _dtsupport = callServicePostITRepair(urlSelectMasterDevicesRes, _dtsupport);
        setDdlData(ddlName, _dtsupport.BoxUserRequest, "devices_name", "res_devicesidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกอุปกรณ์", "0"));

    }

    protected void SetEnRES(DropDownList ddlName)
    {
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList dtsupport = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = dtsupport;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));
        _dtsupport = callServicePostITRepair(urlSelectMasterEN_RES, _dtsupport);
        setDdlData(ddlName, _dtsupport.BoxUserRequest, "en_name", "enidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกช่าง", "0"));

    }

    protected void SetDDLCaseLV1_edit()
    {
        var ddlRESLV1 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV1"));

        _dtsupport.BoxRESList = new RESList[1];
        RESList dtsupport = new RESList();

        _dtsupport.BoxRESList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV1RES, _dtsupport);
        setDdlData(ddlRESLV1, _dtsupport.BoxRESList, "RES1_Name", "RES1IDX");
        ddlRESLV1.Items.Insert(0, new ListItem("กรุณาเลือกหัวข้อแจ้ง", "0"));
        ddlRESLV1.SelectedValue = ViewState["RES1IDX"].ToString();
    }

    protected void SetDDLCaseLV2_edit()
    {
        var ddlRESLV1 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV1"));
        var ddlRESLV2 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV2"));

        _dtsupport.BoxRESList = new RESList[1];
        RESList dtsupport = new RESList();

        dtsupport.RES1IDX = int.Parse(ddlRESLV1.SelectedValue);

        _dtsupport.BoxRESList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV2RES, _dtsupport);
        setDdlData(ddlRESLV2, _dtsupport.BoxRESList, "RES2_Name", "RES2IDX");
        ddlRESLV2.Items.Insert(0, new ListItem("กรุณาเลือกรายการซ่อม", "0"));
        ddlRESLV2.SelectedValue = ViewState["RES2IDX"].ToString();

    }

    protected void SetDDLCaseLV3_edit()
    {
        var ddlRESLV2 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV2"));
        var ddlRESLV3 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV3"));

        _dtsupport.BoxRESList = new RESList[1];
        RESList dtsupport = new RESList();

        dtsupport.RES2IDX = int.Parse(ddlRESLV2.SelectedValue);

        _dtsupport.BoxRESList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV3RES, _dtsupport);
        setDdlData(ddlRESLV3, _dtsupport.BoxRESList, "RES3_Name", "RES3IDX");
        ddlRESLV3.Items.Insert(0, new ListItem("กรุณาเลือกวิธีแก้ไข", "0"));
        ddlRESLV3.SelectedValue = ViewState["RES3IDX"].ToString();

    }

    protected void SetDDLCaseLV4_edit()
    {
        var ddlRESLV3 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV3"));
        var ddlRESLV4 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV4"));

        _dtsupport.BoxRESList = new RESList[1];
        RESList dtsupport = new RESList();

        dtsupport.RES3IDX = int.Parse(ddlRESLV3.SelectedValue); ;

        _dtsupport.BoxRESList[0] = dtsupport;
        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        _dtsupport = callServicePostITRepair(urlSelectCaseLV4RES, _dtsupport);
        setDdlData(ddlRESLV4, _dtsupport.BoxRESList, "RES4_Name", "RES4IDX");
        ddlRESLV4.Items.Insert(0, new ListItem("กรุณาเลือกคำแนะนำ", "0"));
        ddlRESLV4.SelectedValue = ViewState["RES4IDX"].ToString();

    }

    protected void SetDDLCaseLV1()
    {
        var ddlRESLV1 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV1"));

        _dtsupport.BoxRESList = new RESList[1];
        RESList dtsupport = new RESList();

        _dtsupport.BoxRESList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV1RES, _dtsupport);
        setDdlData(ddlRESLV1, _dtsupport.BoxRESList, "RES1_Name", "RES1IDX");
        ddlRESLV1.Items.Insert(0, new ListItem("กรุณาเลือกหัวข้อแจ้ง", "0"));
    }

    protected void SetDDLCaseLV2()
    {
        var ddlRESLV1 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV1"));
        var ddlRESLV2 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV2"));

        _dtsupport.BoxRESList = new RESList[1];
        RESList dtsupport = new RESList();

        dtsupport.RES1IDX = int.Parse(ddlRESLV1.SelectedValue);

        _dtsupport.BoxRESList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV2RES, _dtsupport);
        setDdlData(ddlRESLV2, _dtsupport.BoxRESList, "RES2_Name", "RES2IDX");
        ddlRESLV2.Items.Insert(0, new ListItem("กรุณาเลือกรายการซ่อม", "0"));

    }

    protected void SetDDLCaseLV3()
    {
        var ddlRESLV2 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV2"));
        var ddlRESLV3 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV3"));

        _dtsupport.BoxRESList = new RESList[1];
        RESList dtsupport = new RESList();

        dtsupport.RES2IDX = int.Parse(ddlRESLV2.SelectedValue);

        _dtsupport.BoxRESList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV3RES, _dtsupport);
        setDdlData(ddlRESLV3, _dtsupport.BoxRESList, "RES3_Name", "RES3IDX");
        ddlRESLV3.Items.Insert(0, new ListItem("กรุณาเลือกวิธีแก้ไข", "0"));

    }

    protected void SetDDLCaseLV4()
    {
        var ddlRESLV3 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV3"));
        var ddlRESLV4 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV4"));

        _dtsupport.BoxRESList = new RESList[1];
        RESList dtsupport = new RESList();

        dtsupport.RES3IDX = int.Parse(ddlRESLV3.SelectedValue); ;

        _dtsupport.BoxRESList[0] = dtsupport;
        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        _dtsupport = callServicePostITRepair(urlSelectCaseLV4RES, _dtsupport);
        setDdlData(ddlRESLV4, _dtsupport.BoxRESList, "RES4_Name", "RES4IDX");
        ddlRESLV4.Items.Insert(0, new ListItem("กรุณาเลือกคำแนะนำ", "0"));

    }

    #region Status

    protected void Select_ITStatus()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        search.StaIDXComma = ViewState["Status_IT"].ToString();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelectShow_ITStatus, _dtsupport);
        setGridData(GvViewITRepair, _dtsupport.BoxUserRequest);


    }

    protected void Select_GMStatus()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        search.StaIDX = int.Parse(ViewState["Status_GM"].ToString());

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_StatusGM, _dtsupport);
        setGridData(GvViewITRepair, _dtsupport.BoxUserRequest);


    }

    protected void Select_POSStatus()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        search.StaIDX = int.Parse(ViewState["Status_POS"].ToString());

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_StatusPOS, _dtsupport);
        setGridData(GvViewITRepair, _dtsupport.BoxUserRequest);


    }

    protected void Select_RESList()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();
        search.StaIDX = int.Parse(ViewState["Status_RES"].ToString());

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelectStatusRES, _dtsupport);

        setGridData(GvViewITRepair, _dtsupport.BoxUserRequest);

    }


    protected void Select_SAPList()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();
        search.StaIDXComma = ViewState["Status_SAP"].ToString();

        _dtsupport.BoxUserRequest[0] = search;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        _dtsupport = callServicePostITRepair(urlSelect_SAPList, _dtsupport);

        setGridData(GvViewITRepair, _dtsupport.BoxUserRequest);

    }

    protected void Select_SAPALLList()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_SAPALLList, _dtsupport);

        setGridData(GvViewITRepair, _dtsupport.BoxUserRequest);

    }

    protected void Select_SAPStatuWaiating()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_SAPStatuWaiating, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        lblstatewaitsap1.Text = "SAP " + ViewState["ReturnCode"].ToString();

    }
    protected void Select_SAPStatuWaiatingComment()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_SAPStatuWaiatingUserComment, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        lblstatewaituserrechecksap.Text = "SAP " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_SAPStatuDoing()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_SAPStatuDoing, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        lblstatewaitsap2.Text = "SAP " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_SAPStatuWaitUser()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_WaitUserSAP, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        lblstatedwaitusersap3.Text = "SAP " + ViewState["ReturnCode"].ToString();

    }


    protected void Select_BIStatuWaiating()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_BIWaiting, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        lblstatewaitbi.Text = "BI " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_BIStatuDoing()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_BIDoing, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        lblstatedoingbi.Text = "BI " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_BIStatuWaitUser()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_BIWaitingUser, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        lblstatewaituserbi.Text = "BI " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_BIList()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();
        search.StaIDX = int.Parse(ViewState["Status_BI"].ToString());

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_BIList, _dtsupport);

        setGridData(GvViewITRepair, _dtsupport.BoxUserRequest);

    }

    protected void Select_BPStatuWaiating()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_BPWaiting, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        lblstatewaitbp.Text = "BP " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_BPStatuDoing()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_BPDoing, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        lblstatedoingbp.Text = "BP " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_BPStatuWaitUser()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_BPWaitingUser, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        lblstatewaituserbp.Text = "BP " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_BPList()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();
        search.StaIDX = int.Parse(ViewState["Status_BP"].ToString());

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_BPList, _dtsupport);

        setGridData(GvViewITRepair, _dtsupport.BoxUserRequest);

    }

    protected void Select_ITStatuWaiating()
    {
        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;


        _dtsupport = callServicePostITRepair(urlSelect_ITStatuWaiating, _dtsupport);


        ViewState["ReturnCode_it"] = _dtsupport.ReturnCode;

        lblstatewaitit.Text = "IT " + ViewState["ReturnCode_it"].ToString();

    }

    protected void Select_ITStatuDoing()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_ITStatuDoing, _dtsupport);


        ViewState["ReturnCode_doingit"] = _dtsupport.ReturnCode;
        // ViewState["ReturnIDX_doingit"] = _dtsupport.ReturnIDX;



        lblstatedoingit.Text = "IT " + ViewState["ReturnCode_doingit"].ToString();

    }

    protected void Select_ITStatuWaitUser()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_WaitUserIT, _dtsupport);


        ViewState["ReturnCode_doingit"] = _dtsupport.ReturnCode;
        // ViewState["ReturnIDX_doingit"] = _dtsupport.ReturnIDX;



        lblstatewaituserit.Text = "IT " + ViewState["ReturnCode_doingit"].ToString();

    }

    protected void Select_GMStatuWaiating()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_WaitGM, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;

        lblstatewaitgm.Text = "GM " + ViewState["ReturnCode"].ToString();


    }

    protected void Select_GMStatuDoing()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_DoingGM, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        // ViewState["ReturnIDX_doingit"] = _dtsupport.ReturnIDX;



        lblstatedoinggm.Text = "GM " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_GMStatuWaitUser()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_WaitUserGM, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;

        lblstatewaitusergm.Text = "GM " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_POSStatuWaiating()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_WaitPOS, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;

        lblstatewaitpos.Text = "POS " + ViewState["ReturnCode"].ToString();


    }

    protected void Select_waituserrecheckPOS()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        _dtsupport = callServicePostITRepair(urlSelectm0StatuswaituserrecheckPOS, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;

        lblstatewaituserrecheckpos.Text = "POS " + ViewState["ReturnCode"].ToString();
        //text.Text = ViewState["ReturnCode"].ToString();

    }


    protected void Select_POSStatuDoing()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_DoingPOS, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        // ViewState["ReturnIDX_doingit"] = _dtsupport.ReturnIDX;



        lblstatedoingpos.Text = "POS " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_POSStatuWaitUser()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_WaitUserPOS, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;

        lblstatewaituserpos.Text = "POS " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_RESStatuWaiating()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelectStatusWaitRES, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        lblstatewaitres.Text = "RES " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_REStatuDoing()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelectStatusDoingRES, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        lblstatedoingres.Text = "RES " + ViewState["ReturnCode"].ToString();

    }

    protected void Select_REStatuWaitUser()
    {

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList search = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = search;

        _dtsupport = callServicePostITRepair(urlSelectStatusWaitUserRES, _dtsupport);


        ViewState["ReturnCode"] = _dtsupport.ReturnCode;
        lblstatewaituserres.Text = "RES " + ViewState["ReturnCode"].ToString();

    }



    #endregion

    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void Select_CloseJobList()
    {
        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejob = new UserRequestList();

        closejob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());

        _dtsupport.BoxUserRequest[0] = closejob;

        _dtsupport = callServicePostITRepair(urlSelect_CloseJobList, _dtsupport);

        ViewState["MS1IDX"] = _dtsupport.BoxUserRequest[0].MS1IDX;
        ViewState["MS2IDX"] = _dtsupport.BoxUserRequest[0].MS2IDX;
        ViewState["MS3IDX"] = _dtsupport.BoxUserRequest[0].MS3IDX;
        ViewState["MS4IDX"] = _dtsupport.BoxUserRequest[0].MS4IDX;
        ViewState["MS5IDX"] = _dtsupport.BoxUserRequest[0].MS5IDX;

        ViewState["CIT1IDX"] = _dtsupport.BoxUserRequest[0].CIT1IDX;
        ViewState["CIT2IDX"] = _dtsupport.BoxUserRequest[0].CIT2IDX;
        ViewState["CIT3IDX"] = _dtsupport.BoxUserRequest[0].CIT3IDX;
        ViewState["CIT4IDX"] = _dtsupport.BoxUserRequest[0].CIT4IDX;
        ViewState["DeviceIDX"] = _dtsupport.BoxUserRequest[0].DeviceIDX;

        ViewState["POS1IDX"] = _dtsupport.BoxUserRequest[0].POS1IDX;
        ViewState["POS2IDX"] = _dtsupport.BoxUserRequest[0].POS2IDX;
        ViewState["POS3IDX"] = _dtsupport.BoxUserRequest[0].POS3IDX;
        ViewState["POS4IDX"] = _dtsupport.BoxUserRequest[0].POS4IDX;

        ViewState["RES1IDX"] = _dtsupport.BoxUserRequest[0].RES1IDX;
        ViewState["RES2IDX"] = _dtsupport.BoxUserRequest[0].RES2IDX;
        ViewState["RES3IDX"] = _dtsupport.BoxUserRequest[0].RES3IDX;
        ViewState["RES4IDX"] = _dtsupport.BoxUserRequest[0].RES4IDX;

    }



    protected void Select_List()
    {

        if (ViewState["BoxSearch"] == null)
        {
            _dtsupport = new DataSupportIT();

            _dtsupport.BoxUserRequest = new UserRequestList[1];
            UserRequestList search = new UserRequestList();
            search.RDeptID = int.Parse(ViewState["rdept_idx"].ToString());
            _dtsupport.BoxUserRequest[0] = search;
            _dtsupport = callServicePostITRepair(urlSelect_List, _dtsupport);
            setGridData(GvViewITRepair, _dtsupport.BoxUserRequest);
        }
        else
        {
            //_localJson = _funcTool.convertObjectToJson(_dtsupport);
            //text.Text = _localJson;

            //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["BoxSearch"]));
            _dtsupport = callServicePostITRepair(urlSelect_List_Search, (DataSupportIT)ViewState["BoxSearch"]);
            setGridData(GvViewITRepair, _dtsupport.BoxUserRequest);
        }
    }

    protected void Select_SYSTEMList()
    {

        ddSystemSearch.Items.Clear();
        ddSystemSearch.AppendDataBoundItems = true;
        ddSystemSearch.Items.Add(new ListItem("กรุณาเลือกระบบ....", "0"));

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxSystem = new SystemtList[1];
        SystemtList search = new SystemtList();

        _dtsupport.BoxSystem[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_SYSTEMList, _dtsupport);

        ddSystemSearch.DataSource = _dtsupport.BoxSystem;
        ddSystemSearch.DataTextField = "SysNameTH";
        ddSystemSearch.DataValueField = "SysIDX";
        ddSystemSearch.DataBind();


    }

    protected void Select_Location()
    {
        var ddllocation = ((DropDownList)FvInsertSAP.FindControl("ddllocation"));

        ddllocation.Items.Clear();
        ddllocation.AppendDataBoundItems = true;
        ddllocation.Items.Add(new ListItem("กรุณาเลือกสถานที่แจ้งซ่อม....", "0"));

        _dtEmployee = new data_employee();

        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail search = new employee_detail();

        search.org_idx = int.Parse(ViewState["Org_idx"].ToString());

        _dtEmployee.employee_list[0] = search;

        _dtEmployee = callServicePostEmp(urlSelect_Location, _dtEmployee);

        ddllocation.DataSource = _dtEmployee.employee_list;
        ddllocation.DataTextField = "LocName";
        ddllocation.DataValueField = "r0idx";
        ddllocation.DataBind();


    }

    protected void Select_Priority()
    {
        var ddlpriority = ((DropDownList)FvInsertSAP.FindControl("ddlpriority"));

        ddlpriority.Items.Clear();
        ddlpriority.AppendDataBoundItems = true;
        ddlpriority.Items.Add(new ListItem("กรุณาเลือกลำดับความสำคัญ....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxStatusPriority = new PrioritySAP[1];
        PrioritySAP search = new PrioritySAP();


        _dtsupport.BoxStatusPriority[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_Priority, _dtsupport);

        ddlpriority.DataSource = _dtsupport.BoxStatusPriority;
        ddlpriority.DataTextField = "Priority_name";
        ddlpriority.DataValueField = "PIDX";
        ddlpriority.DataBind();

        ddlpriority.SelectedValue = "3";


    }

    protected void Select_EditPriority()
    {
        var LbPIDX = ((Label)FvDetailRepair.FindControl("LbPIDX"));
        var ddl_editpriority = ((DropDownList)getjob_sap.FindControl("ddl_editpriority"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxStatusPriority = new PrioritySAP[1];
        PrioritySAP search = new PrioritySAP();


        _dtsupport.BoxStatusPriority[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_Priority, _dtsupport);

        ddl_editpriority.DataSource = _dtsupport.BoxStatusPriority;
        ddl_editpriority.DataTextField = "Priority_name";
        ddl_editpriority.DataValueField = "PIDX";
        ddl_editpriority.DataBind();

        ddl_editpriority.SelectedValue = LbPIDX.Text;

    }

    protected void Select_EditPriority_Close()
    {
        var LbPIDX = ((Label)FvDetailRepair.FindControl("LbPIDX"));
        var ddl_editpriority_close = ((DropDownList)FvCloseJob.FindControl("ddl_editpriority_close"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxStatusPriority = new PrioritySAP[1];
        PrioritySAP search = new PrioritySAP();


        _dtsupport.BoxStatusPriority[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_Priority, _dtsupport);

        ddl_editpriority_close.DataSource = _dtsupport.BoxStatusPriority;
        ddl_editpriority_close.DataTextField = "Priority_name";
        ddl_editpriority_close.DataValueField = "PIDX";
        ddl_editpriority_close.DataBind();

        ddl_editpriority_close.SelectedValue = LbPIDX.Text;


    }

    protected void Select_EditPriority_RESClose()
    {
        var LbPIDX = ((Label)FvDetailRepair.FindControl("LbPIDX"));
        var ddl_editpriority_resclose = ((DropDownList)FvCloseJobRES.FindControl("ddl_editpriority_resclose"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxStatusPriority = new PrioritySAP[1];
        PrioritySAP search = new PrioritySAP();


        _dtsupport.BoxStatusPriority[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_Priority, _dtsupport);

        ddl_editpriority_resclose.DataSource = _dtsupport.BoxStatusPriority;
        ddl_editpriority_resclose.DataTextField = "Priority_name";
        ddl_editpriority_resclose.DataValueField = "PIDX";
        ddl_editpriority_resclose.DataBind();

        ddl_editpriority_resclose.SelectedValue = LbPIDX.Text;


    }

    protected void Select_DetailList()
    {
        var lblacceptadmin = ((Label)ViewIndex.FindControl("lblacceptadmin"));

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList detail = new UserRequestList();

        detail.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());

        _dtsupport.BoxUserRequest[0] = detail;

        _dtsupport = callServicePostITRepair(urlSelect_DetailList, _dtsupport);

        setFormViewData(FvDetailUserRepair, _dtsupport.BoxUserRequest);
        setFormViewData(FvDetailRepair, _dtsupport.BoxUserRequest);
        ViewState["Returncode_closejobsap"] = _dtsupport.ReturnCode;

        lblacceptadmin.Text = _dtsupport.BoxUserRequest[0].AdminName;

        ViewState["DocCode"] = _dtsupport.BoxUserRequest[0].DocCode;
        ViewState["CEmpIDX_Create"] = _dtsupport.BoxUserRequest[0].EmpIDX;

        ViewState["StaIDX_Close"] = _dtsupport.BoxUserRequest[0].StaIDX;
    }

    protected void Select_DetailCloseJobList()
    {
        var lblacceptadmin = ((Label)FvCloseJob.FindControl("lblacceptadmin"));

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList detail = new UserRequestList();

        detail.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());

        _dtsupport.BoxUserRequest[0] = detail;

        _dtsupport = callServicePostITRepair(urlSelect_DetailCloseJobList, _dtsupport);


        setFormViewData(FvViewClostJob, _dtsupport.BoxUserRequest);

    }

    protected void Select_DetailCloseJobListIT()
    {
        var lblacceptadmin = ((Label)FvCloseJob.FindControl("lblacceptadmin"));

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList detail = new UserRequestList();

        detail.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());

        _dtsupport.BoxUserRequest[0] = detail;

        _dtsupport = callServicePostITRepair(urlSelect_DetailCloseJobList, _dtsupport);


        setFormViewData(FvVIewCloseJobIT, _dtsupport.BoxUserRequest);

    }

    protected void Select_DetailCloseJobListGM()
    {
        var lblacceptadmin = ((Label)FvCloseJob.FindControl("lblacceptadmin"));

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList detail = new UserRequestList();

        detail.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());

        _dtsupport.BoxUserRequest[0] = detail;

        _dtsupport = callServicePostITRepair(urlSelect_DetailCloseJobList, _dtsupport);


        setFormViewData(FvVIewCloseJobGM, _dtsupport.BoxUserRequest);

    }

    protected void Select_DetailCloseJobListPOS()
    {
        var lblacceptadmin = ((Label)FvCloseJob.FindControl("lblacceptadmin"));

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList detail = new UserRequestList();

        detail.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());

        _dtsupport.BoxUserRequest[0] = detail;

        _dtsupport = callServicePostITRepair(urlSelect_DetailCloseJobList, _dtsupport);


        setFormViewData(FvViewClosePOS, _dtsupport.BoxUserRequest);

    }

    protected void Select_DetailCloseJobListRES()
    {
        var lblacceptadmin = ((Label)FvCloseJob.FindControl("lblacceptadmin"));

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList detail = new UserRequestList();

        detail.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());

        _dtsupport.BoxUserRequest[0] = detail;

        _dtsupport = callServicePostITRepair(urlSelect_DetailCloseJobList, _dtsupport);


        setFormViewData(FvViewCloseRES, _dtsupport.BoxUserRequest);

    }

    protected void Select_GvComment_List()
    {
        GridView GvComment = (GridView)ViewIndex.FindControl("GvComment");

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList commentsap = new UserRequestList();
        commentsap.SysIDX_add = int.Parse(ViewState["SysIDX"].ToString());
        commentsap.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());

        _dtsupport.BoxUserRequest[0] = commentsap;

        _dtsupport = callServicePostITRepair(urlSelect_GvComment_List, _dtsupport);


        setGridData(GvComment, _dtsupport.BoxUserRequest);

    }

    protected void Select_ddlLV1()
    {
        var ddlLV1 = ((DropDownList)FvCloseJob.FindControl("ddlLV1"));

        ddlLV1.Items.Clear();
        ddlLV1.AppendDataBoundItems = true;
        ddlLV1.Items.Add(new ListItem("Select Module ....", "0"));

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList ddllv1 = new UserRequestList();

        ddllv1.SysIDX_add = int.Parse(ViewState["SysIDX"].ToString());

        _dtsupport.BoxUserRequest[0] = ddllv1;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV1SAP, _dtsupport);


        ddlLV1.DataSource = _dtsupport.BoxUserRequest;
        ddlLV1.DataTextField = "Name_Code1";
        ddlLV1.DataValueField = "MS1IDX";
        ddlLV1.DataBind();
        ddlLV1.SelectedValue = ViewState["MS1IDX"].ToString();
    }

    protected void Select_ddlLV2()
    {
        var ddlLV1 = ((DropDownList)FvCloseJob.FindControl("ddlLV1"));
        var ddlLV2 = ((DropDownList)FvCloseJob.FindControl("ddlLV2"));

        ddlLV2.Items.Clear();
        ddlLV2.AppendDataBoundItems = true;
        ddlLV2.Items.Add(new ListItem("Select Topic ....", "0"));

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList ddllv1 = new UserRequestList();

        ddllv1.SysIDX_add = int.Parse(ViewState["SysIDX"].ToString());
        ddllv1.MS1IDX = int.Parse(ddlLV1.SelectedValue);

        _dtsupport.BoxUserRequest[0] = ddllv1;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV2SAP, _dtsupport);


        ddlLV2.DataSource = _dtsupport.BoxUserRequest;
        ddlLV2.DataTextField = "Name_Code2";
        ddlLV2.DataValueField = "MS2IDX";
        ddlLV2.DataBind();
        ddlLV2.SelectedValue = ViewState["MS2IDX"].ToString();


    }

    protected void Select_ddlLV3()
    {
        var ddlLv3 = ((DropDownList)FvCloseJob.FindControl("ddlLv3"));

        ddlLv3.Items.Clear();
        ddlLv3.AppendDataBoundItems = true;
        ddlLv3.Items.Add(new ListItem("Select Cause ........", "0"));


        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList ddllv1 = new UserRequestList();

        ddllv1.SysIDX_add = int.Parse(ViewState["SysIDX"].ToString());
        _dtsupport.BoxUserRequest[0] = ddllv1;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV3SAP, _dtsupport);


        ddlLv3.DataSource = _dtsupport.BoxUserRequest;
        ddlLv3.DataTextField = "Name_Code3";
        ddlLv3.DataValueField = "MS3IDX";
        ddlLv3.DataBind();
        ddlLv3.SelectedValue = ViewState["MS3IDX"].ToString();

    }

    protected void Select_ddlLV4()
    {
        var ddlLV4 = ((DropDownList)FvCloseJob.FindControl("ddlLV4"));

        ddlLV4.Items.Clear();
        ddlLV4.AppendDataBoundItems = true;
        ddlLV4.Items.Add(new ListItem("Select Consist ........", "0"));

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList ddllv1 = new UserRequestList();

        ddllv1.SysIDX_add = int.Parse(ViewState["SysIDX"].ToString());
        _dtsupport.BoxUserRequest[0] = ddllv1;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV4SAP, _dtsupport);

        ddlLV4.DataSource = _dtsupport.BoxUserRequest;
        ddlLV4.DataTextField = "Name_Code4";
        ddlLV4.DataValueField = "MS4IDX";
        ddlLV4.DataBind();

        ddlLV4.SelectedValue = ViewState["MS4IDX"].ToString();
    }

    protected void Select_ddlLV5()
    {
        var ddlLV5 = ((DropDownList)FvCloseJob.FindControl("ddlLV5"));

        ddlLV5.Items.Clear();
        ddlLV5.AppendDataBoundItems = true;
        ddlLV5.Items.Add(new ListItem("Select Adjust ........", "0"));

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList ddllv1 = new UserRequestList();
        ddllv1.SysIDX_add = int.Parse(ViewState["SysIDX"].ToString());
        _dtsupport.BoxUserRequest[0] = ddllv1;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV5SAP, _dtsupport);

        ddlLV5.DataSource = _dtsupport.BoxUserRequest;
        ddlLV5.DataTextField = "Name_Code5";
        ddlLV5.DataValueField = "MS5IDX";
        ddlLV5.DataBind();
        ddlLV5.SelectedValue = ViewState["MS5IDX"].ToString();

    }

    protected void Select_GvQuestionIT_List()
    {
        GridView GvQuestionIT = (GridView)ViewQuestion.FindControl("GvQuestionIT");


        _dtsupport = new DataSupportIT();
        _dtsupport.BoxFeedBack = new FeedBackList[1];
        FeedBackList feedback = new FeedBackList();

        _dtsupport.BoxFeedBack[0] = feedback;

        //string _localJson1 = _funcTool.convertObjectToJson(_dtsupport);

        //text.Text = _localJson1;
        _dtsupport = callServicePostITRepair(urlSelect_GvQuestionIT_List, _dtsupport);

        setGridData(GvQuestionIT, _dtsupport.BoxFeedBack);

        var rtuid = _dtsupport.ReturnUIDX;
        var rtmfb = _dtsupport.ReturnMFB;
        var rtmfb2 = _dtsupport.ReturnMFB2;

        ViewState["UIDX"] = rtuid;
        ViewState["MFBIDX"] = rtmfb;
        ViewState["MFBIDX2"] = rtmfb2;

    }

    protected void Select_ddlPOSLV1()
    {
        FormView FvCloseJobPOS = (FormView)ViewIndex.FindControl("FvCloseJobPOS");
        DropDownList ddlPOSLV1 = (DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV1");

        ddlPOSLV1.Items.Clear();
        ddlPOSLV1.AppendDataBoundItems = true;
        ddlPOSLV1.Items.Add(new ListItem("เลือกเคสแจ้งซ่อม....", "0"));

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxPOSList = new POSList[1];
        POSList casepos1 = new POSList();

        _dtsupport.BoxPOSList[0] = casepos1;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV1POSU0, _dtsupport);

        ddlPOSLV1.DataSource = _dtsupport.BoxPOSList;
        ddlPOSLV1.DataTextField = "Name_Code1";
        ddlPOSLV1.DataValueField = "POS1IDX";
        ddlPOSLV1.DataBind();

        ddlPOSLV1.SelectedValue = ViewState["POS1IDX"].ToString();
    }

    protected void Select_ddlPOSLV2()
    {
        FormView FvCloseJobPOS = (FormView)ViewIndex.FindControl("FvCloseJobPOS");
        DropDownList ddlPOSLV1 = (DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV1");
        DropDownList ddlPOSLV2 = (DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV2");

        ddlPOSLV2.Items.Clear();
        ddlPOSLV2.AppendDataBoundItems = true;
        ddlPOSLV2.Items.Add(new ListItem("เลือกอาการแจ้งซ่อม....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxPOSList = new POSList[1];
        POSList casepos1 = new POSList();


        casepos1.POS1IDX = int.Parse(ddlPOSLV1.SelectedValue);
        _dtsupport.BoxPOSList[0] = casepos1;


        _dtsupport = callServicePostITRepair(urlSelectCaseLV2POSU0, _dtsupport);

        ddlPOSLV2.DataSource = _dtsupport.BoxPOSList;
        ddlPOSLV2.DataTextField = "Name_Code2";
        ddlPOSLV2.DataValueField = "POS2IDX";
        ddlPOSLV2.DataBind();

        ddlPOSLV2.SelectedValue = ViewState["POS2IDX"].ToString();
    }

    protected void Select_ddlPOSLV3()
    {
        FormView FvCloseJobPOS = (FormView)ViewIndex.FindControl("FvCloseJobPOS");
        DropDownList ddlPOSLV1 = (DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV1");
        DropDownList ddlPOSLV3 = (DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV3");

        ddlPOSLV3.Items.Clear();
        ddlPOSLV3.AppendDataBoundItems = true;
        ddlPOSLV3.Items.Add(new ListItem("เลือกแก้ไขแจ้งซ่อม....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxPOSList = new POSList[1];
        POSList casepos1 = new POSList();


        casepos1.POS1IDX = int.Parse(ddlPOSLV1.SelectedValue);
        _dtsupport.BoxPOSList[0] = casepos1;


        _dtsupport = callServicePostITRepair(urlSelectCaseLV3POSU0, _dtsupport);

        ddlPOSLV3.DataSource = _dtsupport.BoxPOSList;
        ddlPOSLV3.DataTextField = "Name_Code3";
        ddlPOSLV3.DataValueField = "POS3IDX";
        ddlPOSLV3.DataBind();

        ddlPOSLV3.SelectedValue = ViewState["POS3IDX"].ToString();
    }

    protected void Select_ddlPOSLV4()
    {
        FormView FvCloseJobPOS = (FormView)ViewIndex.FindControl("FvCloseJobPOS");
        DropDownList ddlPOSLV1 = (DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV1");
        DropDownList ddlPOSLV4 = (DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV4");

        ddlPOSLV4.Items.Clear();
        ddlPOSLV4.AppendDataBoundItems = true;
        ddlPOSLV4.Items.Add(new ListItem("เลือกสาเหตุแจ้งซ่อม....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxPOSList = new POSList[1];
        POSList casepos1 = new POSList();


        casepos1.POS1IDX = int.Parse(ddlPOSLV1.SelectedValue);
        _dtsupport.BoxPOSList[0] = casepos1;


        _dtsupport = callServicePostITRepair(urlSelectCaseLV4POSU0, _dtsupport);

        ddlPOSLV4.DataSource = _dtsupport.BoxPOSList;
        ddlPOSLV4.DataTextField = "Name_Code4";
        ddlPOSLV4.DataValueField = "POS4IDX";
        ddlPOSLV4.DataBind();

        ddlPOSLV4.SelectedValue = ViewState["POS4IDX"].ToString();
    }

    protected DataSupportIT Select_mail(DataSupportIT dtsupport_mail)
    {

        dtsupport_mail.BoxUserRequest = new UserRequestList[1];
        UserRequestList detail = new UserRequestList();

        detail.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());

        dtsupport_mail.BoxUserRequest[0] = detail;

        dtsupport_mail = callServicePostITRepair(urlSelect_DetailList, dtsupport_mail);


        return dtsupport_mail;


    }


    #endregion

    #region Insert

    protected void Insert_Comment()
    {
        var Lbldetailuser = ((Label)FvDetailRepair.FindControl("Lbldetailuser"));
        var DocCodeSap = ((Label)FvDetailRepair.FindControl("DocCodeSap"));
        var lblemail = ((Label)FvDetailUserRepair.FindControl("lblemail"));
        var LbLocIDX = ((Label)FvDetailRepair.FindControl("LbLocIDX"));

        // var txtcomment = ((TextBox)FvInsertComment.FindControl("txtcomment"));
        // string email = lblemail.Text + "," + "sap@taokaenoi.co.th";
        string email = "";
        string urlendcry = Questionrepair(int.Parse(ViewState["URQID_Detail"].ToString()), int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["node_state"].ToString()), int.Parse(ViewState["SysIDX"].ToString()));
        DocCodeSap.Text = DocCodeSap.Text.Replace('\r', ' ').Replace('\n', ' ');


        if (ViewState["SysIDX"].ToString() == "1")
        {
            email = lblemail.Text + "," + emailgm;
        }
        else if (ViewState["SysIDX"].ToString() == "2" || ViewState["SysIDX"].ToString() == "22" || ViewState["SysIDX"].ToString() == "23")
        {
            if (LbLocIDX.Text == "31" || LbLocIDX.Text == "6" || LbLocIDX.Text == "12" || LbLocIDX.Text == "22" || LbLocIDX.Text == "23")
            {
                email = lblemail.Text + "," + emailsap + "," + emailpos;
            }
            else
            {
                email = lblemail.Text + "," + emailsap;
            }
        }
        else if (ViewState["SysIDX"].ToString() == "3")
        {
            email = lblemail.Text + "," + emailit;
        }
        else if (ViewState["SysIDX"].ToString() == "20")
        {
            email = lblemail.Text + "," + emailpos;
        }
        else if (ViewState["SysIDX"].ToString() == "28")
        {
            email = lblemail.Text + "," + emailres;
        }
        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList commentsap = new UserRequestList();

        commentsap.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        commentsap.EmpIDX_add = int.Parse(ViewState["EmpIDX"].ToString());
        commentsap.CommentAuto = txtcomment.Text;
        commentsap.CStatus = 1;

        _dtsupport.BoxUserRequest[0] = commentsap;


        try
        {

            if (ViewState["SysIDX"].ToString() == "2")
            {
                _dtsupport = callServicePostITRepair(urlInsertCommentSAP, _dtsupport);

                _mail_subject = "[Comment][MIS/แจ้งซ่อม : SAP] - " + DocCodeSap.Text + " - ท่านมีข้อความเพิ่มเติม";
                _mail_body = servicemail.ITRepairCommentBody(_dtsupport.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailsap, _mail_subject, _mail_body);
            }
            else if (ViewState["SysIDX"].ToString() == "22")
            {
                _dtsupport = callServicePostITRepair(urlInsertCommentSAP, _dtsupport);

                _mail_subject = "[Comment][MIS/แจ้งซ่อม : BI] - " + DocCodeSap.Text + " - ท่านมีข้อความเพิ่มเติม";
                _mail_body = servicemail.ITRepairCommentBIBody(_dtsupport.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailsap, _mail_subject, _mail_body);
            }
            else if (ViewState["SysIDX"].ToString() == "23")
            {
                _dtsupport = callServicePostITRepair(urlInsertCommentSAP, _dtsupport);

                _mail_subject = "[Comment][MIS/แจ้งซ่อม : BP] - " + DocCodeSap.Text + " - ท่านมีข้อความเพิ่มเติม";
                _mail_body = servicemail.ITRepairCommentBPBody(_dtsupport.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailsap, _mail_subject, _mail_body);
            }
            else if (ViewState["SysIDX"].ToString() == "3")
            {
                _dtsupport = callServicePostITRepair(urlInsertCommentIT, _dtsupport);

                _mail_subject = "[MIS/แจ้งซ่อม : IT] - " + DocCodeSap.Text;
                _mail_body = servicemail.ITRepairCommentITBody(_dtsupport.BoxUserRequest[0]);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailit, _mail_subject, _mail_body);

            }
            else if (ViewState["SysIDX"].ToString() == "20")
            {
                _dtsupport = callServicePostITRepair(urlInsertCommentPOS, _dtsupport);
                _mail_subject = "[MIS/แจ้งซ่อม : POS] - " + DocCodeSap.Text;
                _mail_body = servicemail.ITRepairCommentITBody(_dtsupport.BoxUserRequest[0]);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailpos, _mail_subject, _mail_body);

            }
            else if (ViewState["SysIDX"].ToString() == "1")
            {
                _dtsupport = callServicePostITRepair(urlInsertCommentGM, _dtsupport);

                _mail_subject = "[MIS/แจ้งซ่อม : GM] - " + DocCodeSap.Text;
                _mail_body = servicemail.ITRepairCommentITBody(_dtsupport.BoxUserRequest[0]);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailgm, _mail_subject, _mail_body);

            }
            else if (ViewState["SysIDX"].ToString() == "28")
            {
                _dtsupport = callServicePostITRepair(urlInsertCommentRES, _dtsupport);

                _mail_subject = "[MIS/แจ้งซ่อม : RES] - " + DocCodeSap.Text;
                _mail_body = servicemail.ITRepairCommentITBody(_dtsupport.BoxUserRequest[0]);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailgm, _mail_subject, _mail_body);

            }
            ViewState["Return_Comment"] = _dtsupport.ReturnCode;
        }
        catch
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }

    protected void Insert_Repair()
    {
        Label lbliso = (Label)FvInsertSAP.FindControl("lbliso");
        TextBox txtteladd = (TextBox)FvInsertSAP.FindControl("txtteladd");
        TextBox txtemail = (TextBox)FvDetailUser.FindControl("txtemail");
        DropDownList ddllocation = (DropDownList)FvInsertSAP.FindControl("ddllocation");
        String placeRepairSap = GetIPAddress();
        //  CheckBox SAPCheckRemote = (CheckBox)FvInsertSAP.FindControl("SAPCheckRemote");
        TextBox txtremote = (TextBox)FvInsertSAP.FindControl("txtremote");
        TextBox txtremotepass = (TextBox)FvInsertSAP.FindControl("txtremotepass");
        TextBox txtremark = (TextBox)FvInsertSAP.FindControl("txtremark");
        DropDownList ddlprogram = (DropDownList)FvInsertSAP.FindControl("ddlprogram");
        TextBox txtempcode = (TextBox)FvDetailUser.FindControl("txtempcode");
        DropDownList ddlpriority = (DropDownList)FvInsertSAP.FindControl("ddlpriority");
        TextBox txtrequesname = (TextBox)FvDetailUser.FindControl("txtrequesname");
        TextBox txtrequesdept = (TextBox)FvDetailUser.FindControl("txtrequesdept");
        FileUpload UploadImagesSAP = (FileUpload)FvInsertSAP.FindControl("UploadImagesSAP");
        UpdatePanel UpdatePanel2 = (UpdatePanel)FvInsertSAP.FindControl("UpdatePanel2");
        DropDownList ddlselect_choose = (DropDownList)FvInsertSAP.FindControl("ddlselect_choose");
        DropDownList ddlselectother = (DropDownList)FvInsertSAP.FindControl("ddlselectother");
        DropDownList ddldevicesres = (DropDownList)FvInsertSAP.FindControl("ddldevicesres");

        //string email = txtemail.Text + "," + "sap@taokaenoi.co.th";

        if (ViewState["Add_System"].ToString() == "2" || ViewState["Add_System"].ToString() == "22" || ViewState["Add_System"].ToString() == "23")
        {
            if (ddllocation.SelectedValue == "31" || ddllocation.SelectedValue == "6" || ddllocation.SelectedValue == "12" || ddllocation.SelectedValue == "22" || ddllocation.SelectedValue == "23")
            {
                ViewState["Email_Insert"] = txtemail.Text + "," + emailsap + "," + emailpos;
            }
            else
            {
                ViewState["Email_Insert"] = txtemail.Text + "," + emailsap;
            }

        }
        else if (ViewState["Add_System"].ToString() == "3")
        {
            ViewState["Email_Insert"] = txtemail.Text + "," + emailit;
        }
        else if (ViewState["Add_System"].ToString() == "1")

        {
            ViewState["Email_Insert"] = txtemail.Text + "," + emailgm;
        }
        else if (ViewState["Add_System"].ToString() == "20")

        {
            ViewState["Email_Insert"] = txtemail.Text + "," + emailpos;
        }
        else if (ViewState["Add_System"].ToString() == "28")

        {
            ViewState["Email_Insert"] = txtemail.Text + "," + emailres;
        }

        // string To = ViewState["Email_Insert"].ToString().Trim(new Char[] { ' ', 'ำ', 'ะ', 'ั', 'ํ', 'ี', '๊', '่', '๋', '้', '็', 'ฺ', '์', 'ิ', 'ื' });
        string name = ViewState["Email_Insert"].ToString().Split('@')[1].Split('.')[0];



        HttpFileCollection hfcit2 = Request.Files;
        UpdatePanel2.Update();


        ViewState["NameUser"] = txtrequesname.Text;
        ViewState["DeptUser"] = txtrequesdept.Text;
        ViewState["TelUser"] = txtteladd.Text;
        ViewState["DetailUser"] = txtremark.Text;
        ViewState["EmailUser"] = txtemail.Text;


        if (name == "taokaenoi" || name == "taokaenoiland" || name == "gmail" || name == "drtobi" || name == "genc")
        //if (txtemail.Text != "" && txtemail.Text != "-" && (name == "taokaenoi" || name == "taokaenoiland" || name == "gmail" || name == "drtobi" || name == "genc"))
        {



            _dtsupport = new DataSupportIT();

            _dtsupport.BoxUserRequest = new UserRequestList[1];
            UserRequestList insert = new UserRequestList();


            insert.FileUser = 0;

            insert.EmpIDX_add = int.Parse(ViewState["EmpIDX"].ToString());
            insert.RSecID = int.Parse(ViewState["Sec_idx"].ToString());
            insert.RPosIDX_J = int.Parse(ViewState["Pos_idx"].ToString());
            insert.RDeptID = int.Parse(ViewState["rdept_idx"].ToString());
            insert.LocIDX = int.Parse(ddllocation.SelectedValue);
            insert.IPAddress = placeRepairSap;
            insert.OrgIDX = int.Parse(ViewState["Org_idx"].ToString());
            insert.SysIDX_add = int.Parse(ViewState["Add_System"].ToString());
            insert.ISO_User = lbliso.Text;

            if (ViewState["Add_System"].ToString() == "2" || ViewState["Add_System"].ToString() == "22" ||
                ViewState["Add_System"].ToString() == "23" || ViewState["Add_System"].ToString() == "28")
            {
                insert.PIDX_Add = int.Parse(ddlpriority.SelectedValue);
            }
            else
            {
                insert.PIDX_Add = 3;
            }

            if (txtteladd.Text != "")
            {
                insert.TelETC = txtteladd.Text;
            }
            else
            {
                insert.TelETC = "-";
            }
            if (txtemail.Text != "")
            {
                insert.EmailETC = txtemail.Text;
            }
            else
            {
                insert.EmailETC = "-";
            }

            if (ddlselect_choose.SelectedValue == "1")
            {
                insert.NCEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
            }
            else
            {
                insert.NCEmpIDX = int.Parse(ddlselectother.SelectedValue);
            }

            insert.CostIDX = int.Parse(ViewState["CostIDX"].ToString());

            if (txtremote.Text != "" && txtremotepass.Text != "")
            {
                insert.CheckRemote = 1;
                insert.RemoteIDX = int.Parse(ddlprogram.SelectedValue);
                insert.UserIDRemote = txtremote.Text;
                insert.PasswordRemote = txtremotepass.Text;
            }
            else
            {
                insert.CheckRemote = 0;
                insert.RemoteIDX = 0;
                insert.UserIDRemote = "-";
                insert.PasswordRemote = "-";

            }


            insert.DeviceIDX = int.Parse(ddldevicesres.SelectedValue);
            insert.AdminIDX = 0;
            insert.AdminDoingIDX = 0;
            insert.CommentAMDoing = "ยังไม่ปิดงาน";
            insert.FileAMDoing = 0;
            insert.CCAIDX = 0;
            insert.StaIDX = 1; //กำหนดสถานะชั่วคราว
                               // insert.detailUser = txtremark.Text;

            if (txtremark.Text == "")
            {
                insert.detailUser = "-";

            }
            else
            {
                insert.detailUser = txtremark.Text;
            }

            _dtsupport.BoxUserRequest[0] = insert;

            //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

            _dtsupport = callServicePostITRepair(urlInsertItrepair, _dtsupport);

            // var rtcode = _dtsupport.ReturnCode;
            ViewState["URQID_Detail"] = Int32.Parse(_dtsupport.ReturnCode);
            // text.Text =

            ViewState["DOCSAP"] = _dtsupport.ReturnMsg;


            hfcit2 = Request.Files;
            if (hfcit2.Count > 0)
            {
                for (int ii = 0; ii < hfcit2.Count; ii++)
                {
                    HttpPostedFile hpfLo = hfcit2[ii];
                    if (hpfLo.ContentLength > 1)
                    {
                        string getPath = ConfigurationSettings.AppSettings["PathFile_SAP"];
                        string RECode1 = ViewState["DOCSAP"].ToString();
                        string fileName1 = RECode1 + ii;
                        string filePath1 = Server.MapPath(getPath + RECode1);
                        if (!Directory.Exists(filePath1))
                        {
                            Directory.CreateDirectory(filePath1);
                        }
                        string extension = Path.GetExtension(hpfLo.FileName);

                        hpfLo.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                    }
                }
                //text.Text = "hasfile";
            }
            else

            {
                //text.Text = "don'thasfile";
            }



            _dtsupport = new DataSupportIT();

            _dtsupport.BoxUserRequest = new UserRequestList[1];
            UserRequestList updatefile = new UserRequestList();


            updatefile.FileUser = 1;
            updatefile.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());

            _dtsupport.BoxUserRequest[0] = updatefile;
            // _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", _dtsupport, 303);

            _dtsupport = callServicePostITRepair(urlUpdateUploadFile, _dtsupport);


            _dtsupport = new DataSupportIT();

            _dtsupport.BoxUserRequest = new UserRequestList[1];
            UserRequestList worklist = new UserRequestList();

            worklist.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
            worklist.CCAIDX = 0;
            worklist.SysIDX_add = int.Parse(ViewState["Add_System"].ToString());
            worklist.LocIDX = int.Parse(ddllocation.SelectedValue);

            _dtsupport.BoxUserRequest[0] = worklist;
            _dtsupport = callServicePostITRepair(urlInsertWorkTime, _dtsupport);

        }

        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีอีเมล์กรุณาติดต่อ HR สามารถใช้ได้เฉพาะโดเมน @taokaenoi.co.th / @taokaenoiland.com / @drtobi.co.th / @genc.co.th / @gmail.com');", true);
        }
    }

    protected void Insert_GvQuestionIT_List()
    {
        int[] subSet = new int[30]; //ค่าประเมิน

        GridView GvQuestionIT = (GridView)ViewQuestion.FindControl("GvQuestionIT");
        TextBox txtother = (TextBox)ViewQuestion.FindControl("txtother");
        //  LinkButton lbQuestion1 = (LinkButton)GvViewITRepair.FindControl("URQIDX");

        //code percen
        for (int i = 0; i <= GvQuestionIT.Rows.Count - 1; i++)
        {
            RadioButtonList rdbChoice = (RadioButtonList)GvQuestionIT.Rows[i].FindControl("rdbChoice");
            if (rdbChoice.SelectedValue != "0" && rdbChoice.SelectedValue != null && rdbChoice.SelectedValue != "")
            {
                string rd = rdbChoice.SelectedValue;

                subSet[i] = int.Parse(rd);
            }
        }

        //ชุดแบบสอบถาม
        int[] numSet = new int[] { 1 }; //เลขชุดข้อมูล
        int[] valueSet = new int[] { GvQuestionIT.Rows.Count }; //จำนวนข้อย่อยของแต่ละชุด
        //int[] subSet = new int[] { 1, 1, 1, 1, 1, 1, 1, 1 }; //ค่าประเมิน

        double[] subSet2 = new double[30];//รับค่าข้อย่อย
        double[] subSet3 = new double[30];//รัปค่าเปอร์เซ็นข้อย่อย
                                          //คะแนน
        double[] fullScore = new double[30];
        double[] score = new double[30];
        //แยกจำนวนข้อของแต่ละชุด
        int pointSet = 0;
        //หาเปอร์เซ็น
        double setPercen = 0.0;
        setPercen += 100 / numSet.Length;

        Double setPercen2 = 0.0;
        //ผลรวมทั้งหมด
        double summary = 0.0;
        //จำช่อง array จะได้ไม่วนค่าซ้ำ
        int arrayPoint = 0;

        for (int num = 0; num < numSet.Length; num++)
        {
            //เปอร์เซ็นที่ 2
            setPercen2 = setPercen / valueSet[num];
            //รับเลขจำนวนข้อย่อย
            pointSet = valueSet[num];
            for (int num2 = 0; num2 < pointSet; num2++)
            {
                //คะแนนเต็ม
                fullScore[num] += 4;
                //รวมคะแนนข้อย่อย
                score[num] += subSet[arrayPoint];
                //เปอร์เซ็นข้อย่อย
                subSet2[arrayPoint] = subSet[arrayPoint];
                subSet3[arrayPoint] = (subSet2[arrayPoint] / pointSet) * setPercen2;
                //บวกเพิ่มครั้งละ1
                arrayPoint += 1;
            }
        }

        //หาเปอร์เซ็น
        for (int num3 = 0; num3 < numSet.Length; num3++)
        {
            summary += (score[num3] / fullScore[num3]) * setPercen;
        }
        int sumInt = Convert.ToInt32(summary);
        //แสดงค่า
        for (int num4 = 0; num4 < numSet.Length; num4++)
        {
            pointSet = valueSet[num4];
            for (int num5 = 0; num5 < pointSet; num5++)
            {
                //Response.Write(subSet3[num5] + "%"+"<br />");                
            }

        }

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxFeedBack = new FeedBackList[1];
        FeedBackList insertfb = new FeedBackList();



        insertfb.UIDX = int.Parse(ViewState["UIDX"].ToString());
        insertfb.URQIDX_Q = int.Parse(ViewState["URQID_Detail"].ToString());


        if (txtother.Text != "")
        {
            insertfb.Other = txtother.Text;
        }
        else
        {
            insertfb.Other = "-";
        }

        insertfb.SumPoint = sumInt;//Convert.ToInt32(summary);
        insertfb.WFIDX = 1;

        _dtsupport.BoxFeedBack[0] = insertfb;



        _dtsupport = callServicePostITRepair(urlInsert_GvQuestionIT_List, _dtsupport);


        ViewState["ReturnCode_fb"] = _dtsupport.ReturnCode;

        if (int.Parse(ViewState["ReturnCode_fb"].ToString()) == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('แบบสอบถามนี้ถูกประเมินไปแล้ว');", true);


            //Response.Redirect("http://172.16.11.5/taokaenoi.co.th/MAS/ITRepair");
        }

        else
        {
            Insert_GvQuestionIT1_List(numSet, valueSet, pointSet, subSet3, summary);

            Response.Redirect("http://mas.taokaenoi.co.th/itrepair");

            // Page.Response.Redirect(Page.Request.Url.ToString(), true);

            //Response.Redirect("http://www.taokaenoi.co.th/MAS/ITRepair");
        }
    }

    protected void Insert_GvQuestionIT1_List(int[] numSet, int[] valueSet, int pointSet, double[] subSet3, double summary)
    {

        int[] subSet = new int[30]; //ค่าประเมิน

        GridView GvQuestionIT = (GridView)ViewQuestion.FindControl("GvQuestionIT");

        for (int i = 0; i <= GvQuestionIT.Rows.Count - 1; i++)
        {
            RadioButtonList rdbChoice = (RadioButtonList)GvQuestionIT.Rows[i].FindControl("rdbChoice");
            string rd = rdbChoice.SelectedValue;


            _dtsupport = new DataSupportIT();
            _dtsupport.BoxFeedBack = new FeedBackList[1];
            FeedBackList insertfb = new FeedBackList();



            insertfb.MFBIDX = int.Parse(ViewState["MFBIDX"].ToString());
            insertfb.PO_MFBIDX2 = Convert.ToInt32(subSet3[i]);
            insertfb.POIDX = int.Parse(rd);

            //string _localJson1 = _funcTool.convertObjectToJson(_dtsupport);
            //text.Text = _localJson1;

            _dtsupport.BoxFeedBack[0] = insertfb;
            _dtsupport = callServicePostITRepair(urlInsert_GvQuestionIT1_List, _dtsupport);


        }
    }

    protected void Insert_File()
    {
        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList updatefile_comment = new UserRequestList();


        updatefile_comment.FileUser = 1;
        updatefile_comment.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());


        _dtsupport.BoxUserRequest[0] = updatefile_comment;


        _dtsupport = callServicePostITRepair(urlUpdateUploadFile, _dtsupport);


    }



    #endregion

    #region Update

    protected void Update_GvComment()
    {

        GridView GvComment = (GridView)ViewIndex.FindControl("GvComment");
        var Lbldetailuser = ((Label)FvDetailRepair.FindControl("Lbldetailuser"));
        var DocCodeSap = ((Label)FvDetailRepair.FindControl("DocCodeSap"));
        var lblemail = ((Label)FvDetailUserRepair.FindControl("lblemail"));
        string email = "";
        string urlendcry = Questionrepair(int.Parse(ViewState["URQID_Detail"].ToString()), int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["node_state"].ToString()), int.Parse(ViewState["SysIDX"].ToString()));


        if (ViewState["SysIDX"].ToString() == "1")
        {
            email = lblemail.Text + "," + emailgm;
        }
        else if (ViewState["SysIDX"].ToString() == "2" || ViewState["SysIDX"].ToString() == "22" || ViewState["SysIDX"].ToString() == "23")
        {
            email = lblemail.Text + "," + emailsap;
        }
        else if (ViewState["SysIDX"].ToString() == "3")
        {
            email = lblemail.Text + "," + emailit;
        }
        else if (ViewState["SysIDX"].ToString() == "20")
        {
            email = lblemail.Text + "," + emailpos;
        }

        //  string email = "sap@taokaenoi.co.th";
        //string email = "kantida3620@gmail.com";

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList commentsap = new UserRequestList();


        commentsap.CMIDX = int.Parse(ViewState["CMIDX_Update1"].ToString());
        commentsap.CommentAuto = ViewState["Comment_Update1"].ToString();
        commentsap.EmpIDX_add = int.Parse(ViewState["EmpIDX_Update1"].ToString());

        _dtsupport.BoxUserRequest[0] = commentsap;

        //   text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dtsupport));
        DocCodeSap.Text = DocCodeSap.Text.Replace('\r', ' ').Replace('\n', ' ');

        try
        {
            string sub = String.Empty;

            if (ViewState["SysIDX"].ToString() == "2" || ViewState["SysIDX"].ToString() == "22" || ViewState["SysIDX"].ToString() == "23")
            {
                _dtsupport = callServicePostITRepair(urlUpdate_GvComment, _dtsupport);

                if (ViewState["SysIDX"].ToString() == "2")
                {
                    _mail_subject = subsap_comment + DocCodeSap.Text + " - ท่านมีข้อความเพิ่มเติม";
                    _mail_body = servicemail.ITRepairCommentBody(_dtsupport.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                }
                else if (ViewState["SysIDX"].ToString() == "22")
                {
                    _mail_subject = subbi_comment + DocCodeSap.Text + " - ท่านมีข้อความเพิ่มเติม";
                    _mail_body = servicemail.ITRepairCommentBIBody(_dtsupport.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                }
                else if (ViewState["SysIDX"].ToString() == "23")
                {
                    _mail_subject = subbp_comment + DocCodeSap.Text + " - ท่านมีข้อความเพิ่มเติม";
                    _mail_body = servicemail.ITRepairCommentBPBody(_dtsupport.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                }



                servicemail.SendHtmlFormattedEmailFull(email, "", emailsap, _mail_subject, _mail_body);
            }
            else if (ViewState["SysIDX"].ToString() == "3")
            {
                _dtsupport = callServicePostITRepair(urlUpdate_GvCommentIT, _dtsupport);

                _mail_subject = "[MIS/แจ้งซ่อม : IT] - " + DocCodeSap.Text;
                _mail_body = servicemail.ITRepairCommentITBody(_dtsupport.BoxUserRequest[0]);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailit, _mail_subject, _mail_body);
            }
            else if (ViewState["SysIDX"].ToString() == "20")
            {
                _dtsupport = callServicePostITRepair(urlUpdate_GvCommentPOS, _dtsupport);
                _mail_subject = "[MIS/แจ้งซ่อม : POS] - " + DocCodeSap.Text;
                _mail_body = servicemail.ITRepairCommentITBody(_dtsupport.BoxUserRequest[0]);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailpos, _mail_subject, _mail_body);
            }
            else if (ViewState["SysIDX"].ToString() == "1")
            {
                _dtsupport = callServicePostITRepair(urlUpdate_GvCommentGM, _dtsupport);

                _mail_subject = "[MIS/แจ้งซ่อม : GM] - " + DocCodeSap.Text;
                _mail_body = servicemail.ITRepairCommentITBody(_dtsupport.BoxUserRequest[0]);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailgm, _mail_subject, _mail_body);
            }
            else if (ViewState["SysIDX"].ToString() == "28")
            {
                _dtsupport = callServicePostITRepair(urlUpdateCommentRES, _dtsupport);

                _mail_subject = "[MIS/แจ้งซ่อม : RES] - " + DocCodeSap.Text;
                _mail_body = servicemail.ITRepairCommentITBody(_dtsupport.BoxUserRequest[0]);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailres, _mail_subject, _mail_body);
            }
        }
        catch
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }


    }


    protected void Update_ChangeSystem()
    {
        var Lbldetailuser = ((Label)FvDetailRepair.FindControl("Lbldetailuser"));
        var DocCodeSap = ((Label)FvDetailRepair.FindControl("DocCodeSap"));
        // var lblsystem = ((Label)FvDetailRepair.FindControl("lblsystem"));
        var lblemail = ((Label)FvDetailUserRepair.FindControl("lblemail"));
        var LbOrgIDX = ((Label)FvDetailUserRepair.FindControl("LbOrgIDX"));
        string email = "";

        if (ViewState["SysIDX"].ToString() == "1")
        {
            email = lblemail.Text + "," + emailgm;
        }
        else if (ViewState["SysIDX"].ToString() == "2" || ViewState["SysIDX"].ToString() == "22" || ViewState["SysIDX"].ToString() == "23")
        {
            email = lblemail.Text + "," + emailsap;
        }
        else if (ViewState["SysIDX"].ToString() == "3")
        {
            email = lblemail.Text + "," + emailit;
        }
        else if (ViewState["SysIDX"].ToString() == "20")
        {
            email = lblemail.Text + "," + emailpos;
        }



        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList changesys = new UserRequestList();


        changesys.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        changesys.AdminIDX = int.Parse(ViewState["EmpIDX"].ToString());
        changesys.AdminDoingIDX = int.Parse(ViewState["EmpIDX"].ToString());
        changesys.StaIDX = 0;
        changesys.SysIDX_add = int.Parse(ddl_changesys.SelectedValue);
        changesys.OrgIDX = int.Parse(LbOrgIDX.Text);
        changesys.CommentAMDoing = "โอนย้ายไประบบ " + ddl_changesys.SelectedItem.Text;
        changesys.acidx = int.Parse(ViewState["m0_actor"].ToString());


        _dtsupport.BoxUserRequest[0] = changesys;

        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        _dtsupport = callServicePostITRepair(urlUpdate_ChangeSystem, _dtsupport);
        var Doccode = _dtsupport.ReturnMsg;
        var URQIDX = _dtsupport.ReturnCode;

        string urlendcry = Questionrepair(int.Parse(URQIDX), 1, 1, int.Parse(ddl_changesys.SelectedValue));

        Lbldetailuser.Text = Lbldetailuser.Text.Replace('\r', ' ').Replace('\n', ' ');

        try
        {
            DataSupportIT dtsupport_mail = new DataSupportIT();

            dtsupport_mail = Select_mail(dtsupport_mail);


            if (ddl_changesys.SelectedValue == "2" || ddl_changesys.SelectedValue == "22" || ddl_changesys.SelectedValue == "23")
            {
                if (ddl_changesys.SelectedValue == "2")
                {
                    _mail_subject = "[Initial][MIS/แจ้งซ่อม : SAP] - " + Doccode.ToString() + " - " + Lbldetailuser.Text;
                }
                else if (ddl_changesys.SelectedValue == "22")
                {
                    _mail_subject = "[Initial][MIS/แจ้งซ่อม : BI] - " + Doccode.ToString() + " - " + Lbldetailuser.Text;
                }
                else if (ddl_changesys.SelectedValue == "23")
                {
                    _mail_subject = "[Initial][MIS/แจ้งซ่อม : BP] - " + Doccode.ToString() + " - " + Lbldetailuser.Text;
                }

                _mail_body = servicemail.ITRepairCreateBody(dtsupport_mail.BoxUserRequest[0]);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailsap, _mail_subject, _mail_body);
            }
            else if (ddl_changesys.SelectedValue == "1")
            {
                _mail_subject = "[MIS/แจ้งซ่อม : GoogleApps] - " + Doccode.ToString() + " - " + Lbldetailuser.Text;
                _mail_body = servicemail.ITRepairITCreateBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailgm, _mail_subject, _mail_body); //ViewState["Email_Insert"].ToString()

            }
            else if (ddl_changesys.SelectedValue == "3")
            {
                _mail_subject = "[MIS/แจ้งซ่อม : IT] - " + Doccode.ToString() + " - " + Lbldetailuser.Text;
                _mail_body = servicemail.ITRepairITCreateBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailit, _mail_subject, _mail_body);
            }
            else if (ddl_changesys.SelectedValue == "20")
            {
                _mail_subject = "[MIS/แจ้งซ่อม : POS] - " + Doccode.ToString() + " - " + Lbldetailuser.Text;
                _mail_body = servicemail.ITRepairITCreateBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailpos, _mail_subject, _mail_body); //ViewState["Email_Insert"].ToString()
            }
        }

        catch
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }


    }

    protected void Update_SapGetJob()
    {
        var ddl_editpriority = ((DropDownList)getjob_sap.FindControl("ddl_editpriority"));
        var Lbldetailuser = ((Label)FvDetailRepair.FindControl("Lbldetailuser"));
        var DocCodeSap = ((Label)FvDetailRepair.FindControl("DocCodeSap"));
        var lblemail = ((Label)FvDetailUserRepair.FindControl("lblemail"));
        var LbLocIDX = ((Label)FvDetailRepair.FindControl("LbLocIDX"));
        //  ViewState["rtcode_mail"] = null;
        string email = String.Empty;
        string urlendcry = Questionrepair(int.Parse(ViewState["URQID_Detail"].ToString()), int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["node_state"].ToString()), int.Parse(ViewState["SysIDX"].ToString()));

        if (LbLocIDX.Text == "31" || LbLocIDX.Text == "6" || LbLocIDX.Text == "12" || LbLocIDX.Text == "22" || LbLocIDX.Text == "23")
        {
            email = lblemail.Text + "," + emailsap + "," + emailpos;// "kantida3620@gmail.com";//
        }
        else
        {
            email = lblemail.Text + "," + emailsap;
        }

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList sapgetjob = new UserRequestList();


        sapgetjob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        sapgetjob.AdminIDX = int.Parse(ViewState["EmpIDX"].ToString());
        sapgetjob.PIDX_Add = int.Parse(ddl_editpriority.SelectedValue);
        sapgetjob.unidx = int.Parse(ViewState["m0_node"].ToString());
        sapgetjob.acidx = int.Parse(ViewState["m0_actor"].ToString());

        if (int.Parse(ViewState["m0_node"].ToString()) == 2 && int.Parse(ViewState["m0_actor"].ToString()) == 1)
        {
            sapgetjob.StaIDX = int.Parse(ddl_approve.SelectedValue);
        }

        _dtsupport.BoxUserRequest[0] = sapgetjob;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));


        if (ViewState["m0_node"].ToString() == "3" && ViewState["m0_actor"].ToString() == "2" || ViewState["m0_node"].ToString() == "13" && ViewState["m0_actor"].ToString() == "1")
        {
            SAP_CloseJob();
        }
        else
        {
            _dtsupport = callServicePostITRepair(urlUpdate_SapGetJob, _dtsupport);
            ViewState["rtcode_mail"] = _dtsupport.ReturnCode;

        }


        Lbldetailuser.Text = Lbldetailuser.Text.Replace('\r', ' ').Replace('\n', ' ');

        if (ViewState["rtcode_mail"].ToString() != "1")
        {
            try
            {
                string sub = String.Empty;
                DataSupportIT dtsupport_mail = new DataSupportIT();

                dtsupport_mail = Select_mail(dtsupport_mail);


                if (ViewState["SysIDX"].ToString() == "2")
                {
                    sub = "[MIS/แจ้งซ่อม : SAP] - ";
                }
                else if (ViewState["SysIDX"].ToString() == "22")
                {
                    sub = "[MIS/แจ้งซ่อม : BI] - ";
                }
                else if (ViewState["SysIDX"].ToString() == "23")
                {
                    sub = "[MIS/แจ้งซ่อม : BP] - ";
                }

                _mail_subject = sub + DocCodeSap.Text + " - " + Lbldetailuser.Text;

                if (ViewState["m0_node"].ToString() == "6" && ViewState["m0_actor"].ToString() == "2")
                {
                    if (ViewState["SysIDX"].ToString() == "2")
                    {
                        _mail_body = servicemail.ITRepairSapAcceptBody(dtsupport_mail.BoxUserRequest[0]);
                    }
                    else if (ViewState["SysIDX"].ToString() == "22")
                    {
                        _mail_body = servicemail.ITRepairBIAcceptBody(dtsupport_mail.BoxUserRequest[0]);
                    }
                    else if (ViewState["SysIDX"].ToString() == "23")
                    {
                        _mail_body = servicemail.ITRepairBPAcceptBody(dtsupport_mail.BoxUserRequest[0]);
                    }

                    servicemail.SendHtmlFormattedEmailFull(email, "", emailsap, _mail_subject + "- เจ้าหน้าที่รับงานแล้วครับ", _mail_body);

                }
                else if (ViewState["m0_node"].ToString() == "3" && ViewState["m0_actor"].ToString() == "2")
                {

                    if (ViewState["SysIDX"].ToString() == "2")
                    {
                        _mail_body = servicemail.ITRepairSendUserBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                    }
                    else if (ViewState["SysIDX"].ToString() == "22")
                    {
                        _mail_body = servicemail.ITRepairSendUserBIBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                    }
                    else if (ViewState["SysIDX"].ToString() == "23")
                    {
                        _mail_body = servicemail.ITRepairSendUserBPBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                    }

                    servicemail.SendHtmlFormattedEmailFull(email, "", emailsap, _mail_subject + "- รบกวนตรวจสอบ", _mail_body);

                }
                else if (ViewState["m0_node"].ToString() == "2" && ViewState["m0_actor"].ToString() == "1" && int.Parse(ddl_approve.SelectedValue) == 4)
                {
                    _mail_body = servicemail.ITRepairUserAcceptBody(dtsupport_mail.BoxUserRequest[0]);
                    servicemail.SendHtmlFormattedEmailFull(email, "", emailsap, _mail_subject + "- ปิดงาน", _mail_body);

                }
                else if (ViewState["m0_node"].ToString() == "2" && ViewState["m0_actor"].ToString() == "1" && int.Parse(ddl_approve.SelectedValue) == 5)
                {
                    _mail_body = servicemail.ITRepairUsernotAcceptBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                    servicemail.SendHtmlFormattedEmailFull(email, "", emailsap, _mail_subject + "- ไม่อนุมัติการแก้ไข", _mail_body);

                }
            }
            catch
            {
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขั้นตอนนี้ได้ถูกดำเนินการแล้ว ไม่สามารถดำเนินการซ้ำได้');", true);
        }


    }


    protected void UpdateSAPCloseJob_Comment()
    {
        DropDownList ddlLV1 = ((DropDownList)FvCloseJob.FindControl("ddlLV1"));
        DropDownList ddlLV2 = ((DropDownList)FvCloseJob.FindControl("ddlLV2"));
        DropDownList ddlLV3 = ((DropDownList)FvCloseJob.FindControl("ddlLV3"));
        DropDownList ddlLV4 = ((DropDownList)FvCloseJob.FindControl("ddlLV4"));
        DropDownList ddlLV5 = ((DropDownList)FvCloseJob.FindControl("ddlLV5"));
        DropDownList ddl_editpriority_close = ((DropDownList)FvCloseJob.FindControl("ddl_editpriority_close"));
        TextBox txtmanhours = ((TextBox)FvCloseJob.FindControl("txtmanhours"));
        TextBox txtlink = ((TextBox)FvCloseJob.FindControl("txtlink"));
        TextBox txtsapmsg = ((TextBox)FvCloseJob.FindControl("txtsapmsg"));
        TextBox CommentAdminSap = ((TextBox)FvCloseJob.FindControl("CommentAdminSap"));
        Label DocCodeSap = ((Label)FvDetailRepair.FindControl("DocCodeSap"));
        Label AdminGetJobSapIDX = ((Label)FvDetailRepair.FindControl("AdminGetJobSapIDX"));
        DropDownList SAPModule = ((DropDownList)FvCloseJob.FindControl("SAPModule"));


        DataSupportIT _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList sapclosejob = new UserRequestList();

        sapclosejob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        sapclosejob.MS1IDX = int.Parse(ddlLV1.SelectedValue);
        sapclosejob.MS2IDX = int.Parse(ddlLV2.SelectedValue);
        sapclosejob.MS3IDX = int.Parse(ddlLV3.SelectedValue);
        sapclosejob.MS4IDX = int.Parse(ddlLV4.SelectedValue);
        sapclosejob.MS5IDX = int.Parse(ddlLV5.SelectedValue);
        sapclosejob.PIDX_Add = int.Parse(ddl_editpriority_close.SelectedValue);
        sapclosejob.ManHours = int.Parse(txtmanhours.Text);
        sapclosejob.Link = txtlink.Text;
        sapclosejob.SapMsg = txtsapmsg.Text;
        sapclosejob.AdminDoingIDX = int.Parse(ViewState["EmpIDX"].ToString());
        sapclosejob.CommentAMDoing = CommentAdminSap.Text;
        sapclosejob.CCAIDX = 0;// int.Parse(SAPModule.SelectedValue);
        sapclosejob.RecieveIDX = 0;

        _dtsupport.BoxUserRequest[0] = sapclosejob;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));
        _dtsupport = callServicePostITRepair(urlUpdate_SapCloseJob_Comment, _dtsupport);

    }

    protected void Update_SapGetJob_Comment()
    {

        DataSupportIT _dtsupport_comment = new DataSupportIT();
        _dtsupport_comment.BoxUserRequest = new UserRequestList[1];
        UserRequestList sapgetjob = new UserRequestList();


        sapgetjob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        sapgetjob.unidx = int.Parse(ViewState["m0_node"].ToString());
        sapgetjob.acidx = int.Parse(ViewState["m0_actor"].ToString());
        sapgetjob.StaIDX = int.Parse(ddlstatus_comment.SelectedValue);
        sapgetjob.AdminIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport_comment.BoxUserRequest[0] = sapgetjob;
        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport_comment));

        _dtsupport_comment = callServicePostITRepair(urlUpdateStatusComment, _dtsupport_comment);

        //_dtsupport.BoxUserRequest = new UserRequestList[1];
        //UserRequestList sapgetjob = new UserRequestList();


        //sapgetjob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        //sapgetjob.unidx = int.Parse(ViewState["m0_node"].ToString());
        //sapgetjob.acidx = int.Parse(ViewState["m0_actor"].ToString());
        //sapgetjob.StaIDX = int.Parse(ddlstatus_comment.SelectedValue);
        //sapgetjob.AdminIDX = int.Parse(ViewState["EmpIDX"].ToString());

        //_dtsupport.BoxUserRequest[0] = sapgetjob;

        //_dtsupport = callServicePostITRepair(urlUpdateStatusComment, _dtsupport);

    }

    protected void Update_ITGetJob()
    {
        var Lbldetailuser = ((Label)FvDetailRepair.FindControl("Lbldetailuser"));
        var DocCodeSap = ((Label)FvDetailRepair.FindControl("DocCodeSap"));
        var lblemail = ((Label)FvDetailUserRepair.FindControl("lblemail"));

        string urlendcry;
        // ViewState["rtcode_mail"] = null;
        string email = lblemail.Text + "," + emailit; //"kantida3620@gmail.com";//

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList itgetjob = new UserRequestList();

        itgetjob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        itgetjob.AdminIDX = int.Parse(ViewState["EmpIDX"].ToString());
        itgetjob.unidx = int.Parse(ViewState["m0_node"].ToString());
        itgetjob.acidx = int.Parse(ViewState["m0_actor"].ToString());

        if (int.Parse(ViewState["m0_node"].ToString()) == 2 && int.Parse(ViewState["m0_actor"].ToString()) == 1)
        {
            if (ddl_approve.SelectedValue == "5")
            {
                itgetjob.StaIDX = 24;// int.Parse(ddl_approve.SelectedValue);
            }
            else
            {
                itgetjob.StaIDX = 23;
            }
        }
        else if (int.Parse(ViewState["m0_node"].ToString()) == 4 && int.Parse(ViewState["m0_actor"].ToString()) == 3 && ddl_chooseit.SelectedValue != "22")
        {
            itgetjob.StaIDX = int.Parse(ddl_chooseit.SelectedValue);

        }


        _dtsupport.BoxUserRequest[0] = itgetjob;




        if (ViewState["m0_node"].ToString() == "4" && ddl_chooseit.SelectedValue == "22")
        {
            IT_CloseJob();
        }
        else
        {
            //  text.Text = text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));
            _dtsupport = callServicePostITRepair(urlUpdate_ITGetJob, _dtsupport);

            ViewState["rtcode_mail"] = _dtsupport.ReturnCode;


        }

        Lbldetailuser.Text = Lbldetailuser.Text.Replace('\r', ' ').Replace('\n', ' ');
        if (ViewState["rtcode_mail"].ToString() != "1")
        {
            try
            {
                DataSupportIT dtsupport_mail = new DataSupportIT();

                dtsupport_mail = Select_mail(dtsupport_mail);

                if (int.Parse(ViewState["m0_node"].ToString()) == 2 && int.Parse(ViewState["m0_actor"].ToString()) == 1 && ddl_approve.SelectedValue == "4")
                {
                    urlendcry = Questionrepair(int.Parse(ViewState["URQID_Detail"].ToString()), 9, int.Parse(ViewState["node_state"].ToString()), int.Parse(ViewState["SysIDX"].ToString()));


                    _mail_subject = "[MIS/แจ้งซ่อม : IT] - " + DocCodeSap.Text + " - " + Lbldetailuser.Text;
                    _mail_body = servicemail.ITRepairITfeedbackBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                    servicemail.SendHtmlFormattedEmailFull(email, "", emailit, _mail_subject, _mail_body);


                }
                else if (int.Parse(ViewState["m0_node"].ToString()) == 2 && int.Parse(ViewState["m0_actor"].ToString()) == 1 && ddl_approve.SelectedValue == "5")
                {
                    urlendcry = Questionrepair(int.Parse(ViewState["URQID_Detail"].ToString()), 9, int.Parse(ViewState["node_state"].ToString()), int.Parse(ViewState["SysIDX"].ToString()));


                    _mail_subject = "[MIS/แจ้งซ่อม : IT] - " + DocCodeSap.Text + " - " + Lbldetailuser.Text;
                    _mail_body = servicemail.ITRepairUsernotAcceptBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                    servicemail.SendHtmlFormattedEmailFull(email, "", emailit, _mail_subject, _mail_body);


                }
                else
                {
                    urlendcry = Questionrepair(int.Parse(ViewState["URQID_Detail"].ToString()), int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["node_state"].ToString()), int.Parse(ViewState["SysIDX"].ToString()));

                    _mail_subject = "[MIS/แจ้งซ่อม : IT] - " + DocCodeSap.Text + " - " + Lbldetailuser.Text;
                    _mail_body = servicemail.ITRepairITSendBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                    servicemail.SendHtmlFormattedEmailFull(email, "", emailit, _mail_subject, _mail_body);
                }

            }
            catch
            {
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขั้นตอนนี้ได้ถูกดำเนินการแล้ว ไม่สามารถดำเนินการซ้ำได้');", true);
        }

    }

    protected void Update_GMGetJob()
    {
        var Lbldetailuser = ((Label)FvDetailRepair.FindControl("Lbldetailuser"));
        var DocCodeSap = ((Label)FvDetailRepair.FindControl("DocCodeSap"));
        var lblemail = ((Label)FvDetailUserRepair.FindControl("lblemail"));
        string email = lblemail.Text + "," + emailgm; //"kantida3620@gmail.com";//
        string urlendcry = Questionrepair(int.Parse(ViewState["URQID_Detail"].ToString()), int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["node_state"].ToString()), int.Parse(ViewState["SysIDX"].ToString()));
        // ViewState["rtcode_mail"] = null;


        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList itgetjob = new UserRequestList();

        itgetjob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        itgetjob.AdminIDX = int.Parse(ViewState["EmpIDX"].ToString());
        itgetjob.unidx = int.Parse(ViewState["m0_node"].ToString());
        itgetjob.acidx = int.Parse(ViewState["m0_actor"].ToString());

        if (int.Parse(ViewState["m0_node"].ToString()) == 2 && int.Parse(ViewState["m0_actor"].ToString()) == 1)
        {
            if (ddl_approve.SelectedValue == "5")
            {
                itgetjob.StaIDX = 12;// int.Parse(ddl_approve.SelectedValue);
            }
            else
            {
                itgetjob.StaIDX = 11;
            }
        }


        _dtsupport.BoxUserRequest[0] = itgetjob;

        if (ViewState["m0_node"].ToString() == "5")
        {
            GM_CloseJob();
        }
        else
        {
            _dtsupport = callServicePostITRepair(urlUpdate_GMGetJob, _dtsupport);
            ViewState["rtcode_mail"] = _dtsupport.ReturnCode;
        }

        Lbldetailuser.Text = Lbldetailuser.Text.Replace('\r', ' ').Replace('\n', ' ');
        if (ViewState["rtcode_mail"].ToString() != "1")
        {
            try
            {
                DataSupportIT dtsupport_mail = new DataSupportIT();

                dtsupport_mail = Select_mail(dtsupport_mail);

                _mail_subject = "[MIS/แจ้งซ่อม : GM] - " + DocCodeSap.Text + " - " + Lbldetailuser.Text;
                _mail_body = servicemail.ITRepairITSendBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                servicemail.SendHtmlFormattedEmailFull(email, "", emailgm, _mail_subject, _mail_body);
            }
            catch
            {
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขั้นตอนนี้ได้ถูกดำเนินการแล้ว ไม่สามารถดำเนินการซ้ำได้');", true);
        }

    }

    protected void Update_POSGetJob()
    {
        var Lbldetailuser = ((Label)FvDetailRepair.FindControl("Lbldetailuser"));
        var DocCodeSap = ((Label)FvDetailRepair.FindControl("DocCodeSap"));
        var lblemail = ((Label)FvDetailUserRepair.FindControl("lblemail"));
        var LbLocIDX = ((Label)FvDetailRepair.FindControl("LbLocIDX"));
        string email = lblemail.Text + "," + emailpos; //"kantida3620@gmail.com";//
                                                       // ViewState["rtcode_mail"] = null;



        string urlendcry = Questionrepair(int.Parse(ViewState["URQID_Detail"].ToString()), int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["node_state"].ToString()), int.Parse(ViewState["SysIDX"].ToString()));


        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList posgetjob = new UserRequestList();

        posgetjob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        posgetjob.AdminIDX = int.Parse(ViewState["EmpIDX"].ToString());
        posgetjob.unidx = int.Parse(ViewState["m0_node"].ToString());
        posgetjob.acidx = int.Parse(ViewState["m0_actor"].ToString());
        posgetjob.LocIDX = int.Parse(LbLocIDX.Text);

        if (int.Parse(ViewState["m0_node"].ToString()) == 2 && int.Parse(ViewState["m0_actor"].ToString()) == 1)
        {
            if (ddl_approve.SelectedValue == "5")
            {
                posgetjob.StaIDX = 30;// int.Parse(ddl_approve.SelectedValue);
            }
            else
            {
                posgetjob.StaIDX = 29;
            }
        }



        _dtsupport.BoxUserRequest[0] = posgetjob;

        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));


        ////string _localJson1 = _funcTool.convertObjectToJson(_dtsupport);
        ////text.Text = _localJson1;



        if (ViewState["m0_node"].ToString() == "12" && ViewState["m0_actor"].ToString() == "5")
        {
            POS_CloseJob();
        }
        else
        {
            _dtsupport = callServicePostITRepair(urlUpdate_POSGetJob, _dtsupport);
            ViewState["rtcode_mail"] = _dtsupport.ReturnCode;
        }

        Lbldetailuser.Text = Lbldetailuser.Text.Replace('\r', ' ').Replace('\n', ' ');
        if (ViewState["rtcode_mail"].ToString() != "1")
        {
            try
            {
                DataSupportIT dtsupport_mail = new DataSupportIT();

                dtsupport_mail = Select_mail(dtsupport_mail);

                _mail_subject = "[MIS/แจ้งซ่อม : POS] - " + DocCodeSap.Text + " - " + Lbldetailuser.Text;

                if (ViewState["m0_node"].ToString() == "12" && ViewState["m0_actor"].ToString() == "5" && ddl_chooseit.SelectedValue == "28")
                {
                    _mail_body = servicemail.ITRepairPOSSendBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                    servicemail.SendHtmlFormattedEmailFull(email, "", emailpos, _mail_subject, _mail_body);

                }
                else if (ViewState["m0_node"].ToString() == "12" && ViewState["m0_actor"].ToString() == "5" && ddl_chooseit.SelectedValue == "60")
                {
                    _mail_body = servicemail.ITRepairITSendBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                    servicemail.SendHtmlFormattedEmailFull(email, "", emailpos, _mail_subject, _mail_body);

                }
                else if (ViewState["m0_node"].ToString() == "2" && ViewState["m0_actor"].ToString() == "1" && ddl_approve.SelectedValue != "5")
                {
                    _mail_body = servicemail.ITRepairSendPOSBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                    servicemail.SendHtmlFormattedEmailFull(email, "", emailpos, _mail_subject, _mail_body);


                }
            }
            catch
            {
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขั้นตอนนี้ได้ถูกดำเนินการแล้ว ไม่สามารถดำเนินการซ้ำได้');", true);

        }

    }

    protected void SAP_CloseJob()
    {
        int FileAdmin1;
        DropDownList ddlLV1 = ((DropDownList)FvCloseJob.FindControl("ddlLV1"));
        DropDownList ddlLV2 = ((DropDownList)FvCloseJob.FindControl("ddlLV2"));
        DropDownList ddlLV3 = ((DropDownList)FvCloseJob.FindControl("ddlLV3"));
        DropDownList ddlLV4 = ((DropDownList)FvCloseJob.FindControl("ddlLV4"));
        DropDownList ddlLV5 = ((DropDownList)FvCloseJob.FindControl("ddlLV5"));
        DropDownList ddl_editpriority_close = ((DropDownList)FvCloseJob.FindControl("ddl_editpriority_close"));
        TextBox txtmanhours = ((TextBox)FvCloseJob.FindControl("txtmanhours"));
        TextBox txtlink = ((TextBox)FvCloseJob.FindControl("txtlink"));
        TextBox txtsapmsg = ((TextBox)FvCloseJob.FindControl("txtsapmsg"));
        TextBox CommentAdminSap = ((TextBox)FvCloseJob.FindControl("CommentAdminSap"));
        Label DocCodeSap = ((Label)FvDetailRepair.FindControl("DocCodeSap"));
        Label AdminGetJobSapIDX = ((Label)FvDetailRepair.FindControl("AdminGetJobSapIDX"));
        DropDownList SAPModule = ((DropDownList)FvCloseJob.FindControl("SAPModule"));
        // ViewState["rtcode_mail"] = null;

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList sapclosejob = new UserRequestList();

        sapclosejob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        sapclosejob.MS1IDX = int.Parse(ddlLV1.SelectedValue);
        sapclosejob.MS2IDX = int.Parse(ddlLV2.SelectedValue);
        sapclosejob.MS3IDX = int.Parse(ddlLV3.SelectedValue);
        sapclosejob.MS4IDX = int.Parse(ddlLV4.SelectedValue);
        sapclosejob.MS5IDX = int.Parse(ddlLV5.SelectedValue);
        sapclosejob.PIDX_Add = int.Parse(ddl_editpriority_close.SelectedValue);
        sapclosejob.ManHours = int.Parse(txtmanhours.Text);
        sapclosejob.Link = txtlink.Text;
        sapclosejob.SapMsg = txtsapmsg.Text;
        sapclosejob.AdminDoingIDX = int.Parse(ViewState["EmpIDX"].ToString());
        sapclosejob.CommentAMDoing = CommentAdminSap.Text;
        sapclosejob.CCAIDX = 0;// int.Parse(SAPModule.SelectedValue);
        sapclosejob.RecieveIDX = 0;
        sapclosejob.EmpIDX_add = int.Parse(AdminGetJobSapIDX.Text);
        sapclosejob.unidx = int.Parse(ViewState["m0_node"].ToString());
        sapclosejob.acidx = int.Parse(ViewState["m0_actor"].ToString());


        HttpFileCollection hfcSap2 = Request.Files;

        if (hfcSap2.Count > 1)
        {
            FileAdmin1 = 1;
            sapclosejob.FileAMDoing = FileAdmin1;

        }
        else
        {
            FileAdmin1 = 0;
            sapclosejob.FileAMDoing = FileAdmin1;

        }



        _dtsupport.BoxUserRequest[0] = sapclosejob;
        _dtsupport = callServicePostITRepair(urlUpdate_SapGetJob, _dtsupport);
        ViewState["rtcode_mail"] = _dtsupport.ReturnCode;
        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        //  _dtsupport = callServicePostITRepair(urlSap_CloseJob, _dtsupport);
    }

    protected void IT_CloseJob()
    {
        var Lbldetailuser = ((Label)FvDetailRepair.FindControl("Lbldetailuser"));
        var DocCodeSap = ((Label)FvDetailRepair.FindControl("DocCodeSap"));
        var lblemail = ((Label)FvDetailUserRepair.FindControl("lblemail"));

        DropDownList ddlITLV1 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV1"));
        DropDownList ddlITLV2 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV2"));
        DropDownList ddlITLV3 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV3"));
        DropDownList ddlITLV4 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV4"));
        // DropDownList ddlholder = ((DropDownList)FvCloseJobIT.FindControl("ddlholder"));

        TextBox txtlinkit = ((TextBox)FvCloseJobIT.FindControl("txtlink"));
        TextBox commentitclosejob = ((TextBox)FvCloseJobIT.FindControl("commentitclosejob"));
        Label AdminGetJobSapIDX1 = ((Label)FvDetailRepair.FindControl("AdminGetJobSapIDX"));

        //ViewState["rtcode_mail"] = null;
        string email = lblemail.Text + "," + emailit;

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList itclosejob = new UserRequestList();




        itclosejob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        itclosejob.CIT1IDX = int.Parse(ddlITLV1.SelectedValue);
        itclosejob.CIT2IDX = int.Parse(ddlITLV2.SelectedValue);
        itclosejob.CIT3IDX = int.Parse(ddlITLV3.SelectedValue);
        itclosejob.CIT4IDX = int.Parse(ddlITLV4.SelectedValue);
        //itclosejob.DeviceIDX = int.Parse(ddlholder.SelectedValue);
        itclosejob.Link_IT = txtlinkit.Text;
        itclosejob.AdminDoingIDX = int.Parse(ViewState["EmpIDX"].ToString());
        itclosejob.CommentAMDoing = commentitclosejob.Text;
        itclosejob.CCAIDX = 0;// int.Parse(SAPModule.SelectedValue);
        itclosejob.RecieveIDX = 0;
        itclosejob.EmpIDX_add = int.Parse(AdminGetJobSapIDX1.Text);
        itclosejob.unidx = int.Parse(ViewState["m0_node"].ToString());
        itclosejob.acidx = int.Parse(ViewState["m0_actor"].ToString());
        itclosejob.StaIDX = int.Parse(ddl_chooseit.SelectedValue);


        _dtsupport.BoxUserRequest[0] = itclosejob;

        // _dtsupport = callServicePostITRepair(urlIT_CloseJob, _dtsupport);
        _dtsupport = callServicePostITRepair(urlUpdate_ITGetJob, _dtsupport);
        ViewState["rtcode_mail"] = _dtsupport.ReturnCode;

    }

    protected void GM_CloseJob()
    {
        TextBox commentgmclosejob = ((TextBox)FvCloseJobGM.FindControl("commentgmclosejob"));
        // ViewState["rtcode_mail"] = null;

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList gmclosejob = new UserRequestList();


        gmclosejob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        gmclosejob.AdminDoingIDX = int.Parse(ViewState["EmpIDX"].ToString());
        gmclosejob.CommentAMDoing = commentgmclosejob.Text;
        gmclosejob.unidx = int.Parse(ViewState["m0_node"].ToString());
        gmclosejob.acidx = int.Parse(ViewState["m0_actor"].ToString());



        _dtsupport.BoxUserRequest[0] = gmclosejob;

        // _dtsupport = callServicePostITRepair(urlGM_CloseJob, _dtsupport);
        _dtsupport = callServicePostITRepair(urlUpdate_GMGetJob, _dtsupport);
        ViewState["rtcode_mail"] = _dtsupport.ReturnCode;

    }

    protected void POS_CloseJob()
    {
        var Lbldetailuser = ((Label)FvDetailRepair.FindControl("Lbldetailuser"));
        var DocCodeSap = ((Label)FvDetailRepair.FindControl("DocCodeSap"));
        var lblemail = ((Label)FvDetailUserRepair.FindControl("lblemail"));

        // int FileAdmin11;
        DropDownList ddlPOSLV1 = ((DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV1"));
        DropDownList ddlPOSLV2 = ((DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV2"));
        DropDownList ddlPOSLV3 = ((DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV3"));
        DropDownList ddlPOSLV4 = ((DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV4"));
        TextBox commentposclosejob = ((TextBox)FvCloseJobPOS.FindControl("commentposclosejob"));
        Label AdminGetJobSapIDX1 = ((Label)FvDetailRepair.FindControl("AdminGetJobSapIDX"));
        //  ViewState["rtcode_mail"] = null;


        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList posclosejob = new UserRequestList();


        string email = lblemail.Text + "," + emailpos;

        posclosejob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        posclosejob.POS1IDX = int.Parse(ddlPOSLV1.SelectedValue);
        posclosejob.POS2IDX = int.Parse(ddlPOSLV2.SelectedValue);
        posclosejob.POS3IDX = int.Parse(ddlPOSLV3.SelectedValue);
        posclosejob.POS4IDX = int.Parse(ddlPOSLV4.SelectedValue);
        // itclosejob.Link_IT = txtlinkit.Text;
        posclosejob.AdminDoingIDX = int.Parse(ViewState["EmpIDX"].ToString());
        posclosejob.CommentAMDoing = commentposclosejob.Text;
        posclosejob.CCAIDX = 0;// int.Parse(SAPModule.SelectedValue);
        posclosejob.RecieveIDX = 0;
        posclosejob.EmpIDX_add = int.Parse(AdminGetJobSapIDX1.Text);
        posclosejob.unidx = int.Parse(ViewState["m0_node"].ToString());
        posclosejob.acidx = int.Parse(ViewState["m0_actor"].ToString());
        posclosejob.StaIDX = int.Parse(ddl_chooseit.SelectedValue);

        //if (int.Parse(ViewState["m0_node"].ToString()) == 12 && int.Parse(ViewState["m0_actor"].ToString()) == 5) //
        //{
        //    posclosejob.StaIDX = int.Parse(ddl_chooseit.SelectedValue);

        //}



        _dtsupport.BoxUserRequest[0] = posclosejob;

        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        // _dtsupport = callServicePostITRepair(urlPOS_CloseJob, _dtsupport);

        _dtsupport = callServicePostITRepair(urlUpdate_POSGetJob, _dtsupport);
        ViewState["rtcode_mail"] = _dtsupport.ReturnCode;


    }

    protected void Update_REGetJob()
    {
        var Lbldetailuser = ((Label)FvDetailRepair.FindControl("Lbldetailuser"));
        var DocCodeSap = ((Label)FvDetailRepair.FindControl("DocCodeSap"));
        var lblemail = ((Label)FvDetailUserRepair.FindControl("lblemail"));

        string urlendcry;
        // ViewState["rtcode_mail"] = null;
        string email = lblemail.Text + "," + emailres; //"kantida3620@gmail.com";//

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList resgetjob = new UserRequestList();

        resgetjob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        resgetjob.AdminIDX = int.Parse(ViewState["EmpIDX"].ToString());
        resgetjob.PIDX_Add = int.Parse(ddl_editpriority.SelectedValue);
        resgetjob.unidx = int.Parse(ViewState["m0_node"].ToString());
        resgetjob.acidx = int.Parse(ViewState["m0_actor"].ToString());

        if (int.Parse(ViewState["m0_node"].ToString()) == 2 && int.Parse(ViewState["m0_actor"].ToString()) == 1)
        {
            if (ddl_approve.SelectedValue == "5")
            {
                resgetjob.StaIDX = 52;// int.Parse(ddl_approve.SelectedValue);
            }
            else
            {
                resgetjob.StaIDX = 51;
            }
        }
        else if (int.Parse(ViewState["m0_node"].ToString()) == 15 && int.Parse(ViewState["m0_actor"].ToString()) == 6 && ddl_chooseit.SelectedValue != "50")
        {
            resgetjob.StaIDX = int.Parse(ddl_chooseit.SelectedValue);

        }
        else if (int.Parse(ViewState["m0_node"].ToString()) == 14)
        {
            resgetjob.enidx = int.Parse(ddlen.SelectedValue);

            if (int.Parse(ddlen.SelectedValue) == 1)
            {
                email += "," + emailresen;
            }

        }

        _dtsupport.BoxUserRequest[0] = resgetjob;




        if (ViewState["m0_node"].ToString() == "15" && ddl_chooseit.SelectedValue == "50")
        {
            RES_CloseJob();
        }
        else
        {

            _dtsupport = callServicePostITRepair(urlUpdateRESGetJob, _dtsupport);
            ViewState["rtcode_mail"] = _dtsupport.ReturnCode;

        }

        Lbldetailuser.Text = Lbldetailuser.Text.Replace('\r', ' ').Replace('\n', ' ');
        if (ViewState["rtcode_mail"].ToString() != "1")
        {
            try
            {
                DataSupportIT dtsupport_mail = new DataSupportIT();

                dtsupport_mail = Select_mail(dtsupport_mail);

                if (int.Parse(ViewState["m0_node"].ToString()) == 2 && int.Parse(ViewState["m0_actor"].ToString()) == 1 && ddl_approve.SelectedValue == "4")
                {
                    urlendcry = Questionrepair(int.Parse(ViewState["URQID_Detail"].ToString()), 9, int.Parse(ViewState["node_state"].ToString()), int.Parse(ViewState["SysIDX"].ToString()));


                    _mail_subject = "[MIS/แจ้งซ่อม : RES] - " + DocCodeSap.Text + " - " + Lbldetailuser.Text;
                    _mail_body = servicemail.ITRepairUserAcceptBody(dtsupport_mail.BoxUserRequest[0]);
                    servicemail.SendHtmlFormattedEmailFull(email, "", emailres, _mail_subject, _mail_body);


                }
                else if (int.Parse(ViewState["m0_node"].ToString()) == 2 && int.Parse(ViewState["m0_actor"].ToString()) == 1 && ddl_approve.SelectedValue == "5")
                {
                    urlendcry = Questionrepair(int.Parse(ViewState["URQID_Detail"].ToString()), 9, int.Parse(ViewState["node_state"].ToString()), int.Parse(ViewState["SysIDX"].ToString()));


                    _mail_subject = "[MIS/แจ้งซ่อม : RES] - " + DocCodeSap.Text + " - " + Lbldetailuser.Text;
                    _mail_body = servicemail.ITRepairUsernotAcceptBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                    servicemail.SendHtmlFormattedEmailFull(email, "", emailres, _mail_subject, _mail_body);


                }
                else
                {
                    urlendcry = Questionrepair(int.Parse(ViewState["URQID_Detail"].ToString()), int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["node_state"].ToString()), int.Parse(ViewState["SysIDX"].ToString()));

                    _mail_subject = "[MIS/แจ้งซ่อม : RES] - " + DocCodeSap.Text + " - " + Lbldetailuser.Text;
                    _mail_body = servicemail.ITRepairITSendBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                    servicemail.SendHtmlFormattedEmailFull(email, "", emailres, _mail_subject, _mail_body);
                }

            }
            catch
            {
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขั้นตอนนี้ได้ถูกดำเนินการแล้ว ไม่สามารถดำเนินการซ้ำได้');", true);
        }

    }

    protected void RES_CloseJob()
    {
        var Lbldetailuser = ((Label)FvDetailRepair.FindControl("Lbldetailuser"));
        var DocCodeSap = ((Label)FvDetailRepair.FindControl("DocCodeSap"));
        var lblemail = ((Label)FvDetailUserRepair.FindControl("lblemail"));
        DropDownList ddlRESLV1 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV1"));
        DropDownList ddlRESLV2 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV2"));
        DropDownList ddlRESLV3 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV3"));
        DropDownList ddlRESLV4 = ((DropDownList)FvCloseJobRES.FindControl("ddlRESLV4"));
        TextBox txtprices = ((TextBox)FvCloseJobRES.FindControl("txtprices"));
        DropDownList ddl_editpriority_resclose = ((DropDownList)FvCloseJobRES.FindControl("ddl_editpriority_resclose"));
        Label AdminGetJobSapIDX1 = ((Label)FvDetailRepair.FindControl("AdminGetJobSapIDX"));
        TextBox commentresclosejob = ((TextBox)FvCloseJobRES.FindControl("commentresclosejob"));

        //  ViewState["rtcode_mail"] = null;


        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList resgetjob = new UserRequestList();


        string email = lblemail.Text + "," + emailpos;

        resgetjob.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());
        resgetjob.RES1IDX = int.Parse(ddlRESLV1.SelectedValue);
        resgetjob.RES2IDX = int.Parse(ddlRESLV2.SelectedValue);
        resgetjob.RES3IDX = int.Parse(ddlRESLV3.SelectedValue);
        resgetjob.RES4IDX = int.Parse(ddlRESLV4.SelectedValue);
        resgetjob.price = txtprices.Text;
        resgetjob.PIDX_Add = int.Parse(ddl_editpriority_resclose.SelectedValue);
        resgetjob.AdminDoingIDX = int.Parse(ViewState["EmpIDX"].ToString());
        resgetjob.CommentAMDoing = commentresclosejob.Text;
        resgetjob.EmpIDX_add = int.Parse(AdminGetJobSapIDX1.Text);
        resgetjob.unidx = int.Parse(ViewState["m0_node"].ToString());
        resgetjob.acidx = int.Parse(ViewState["m0_actor"].ToString());
        resgetjob.StaIDX = int.Parse(ddl_chooseit.SelectedValue);



        _dtsupport.BoxUserRequest[0] = resgetjob;

        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        // _dtsupport = callServicePostITRepair(urlPOS_CloseJob, _dtsupport);

        _dtsupport = callServicePostITRepair(urlUpdateRESGetJob, _dtsupport);
        ViewState["rtcode_mail"] = _dtsupport.ReturnCode;


    }




    #endregion

    #region Ip Address

    protected String GetIPAddress()
    {

        IPHostEntry host;
        string localIP = "?";
        host = Dns.GetHostEntry(Dns.GetHostName());

        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily.ToString() == "InterNetwork")
            {
                localIP = ip.ToString();

            }
        }

        return Convert.ToString(localIP);
    }

    #endregion

    #region GridView

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvViewITRepair":

                GvViewITRepair.PageIndex = e.NewPageIndex;
                GvViewITRepair.DataBind();

                if (ViewState["Check_paging"].ToString() == "1")
                {
                    Select_GMStatus();
                }
                else if (ViewState["Check_paging"].ToString() == "2")
                {
                    Select_SAPList();
                }
                else if (ViewState["Check_paging"].ToString() == "3")
                {
                    Select_ITStatus();
                }
                else if (ViewState["Check_paging"].ToString() == "20")
                {
                    Select_POSStatus();
                }
                else if (ViewState["Check_paging"].ToString() == "28")
                {
                    Select_RESList();
                }
                else
                {
                    Select_List();
                }

                linkBtnTrigger(btn_saveadduser);


                //if (int.Parse(ViewState["Status_SAP"].ToString()) == 1 || int.Parse(ViewState["Status_SAP"].ToString()) == 2 
                //    || int.Parse(ViewState["Status_SAP"].ToString()) == 3)
                //{
                //    Select_SAPList();
                //}
                //else if (int.Parse(ViewState["Status_SAP"].ToString()) == 3)
                //{
                //    Select_SAPALLList();
                //}

                //else
                //{
                //    Select_List();
                //}

                break;



        }
    }

    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvViewITRepair":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }


                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    //if (e.Row.RowState == DataControlRowState.Alternate || e.Row.RowState == DataControlRowState.Normal)
                    //{
                    //    var lbVeiw1 = (LinkButton)e.Row.FindControl("lbVeiw1");

                    //    linkBtnTrigger(lbVeiw1);

                    //}
                    if (GvViewITRepair.EditIndex != e.Row.RowIndex) //to overlook header row
                    {

                        var txtIDSta = ((Label)e.Row.FindControl("txtIDSta"));
                        var ltCCAstatus = ((Label)e.Row.FindControl("ltCCAstatus"));
                        var BoxDate = ((Panel)e.Row.FindControl("BoxDate"));
                        var lbb1 = ((Label)e.Row.FindControl("lbb1"));
                        var lbdaterecive = ((Literal)e.Row.FindControl("lbdaterecive"));
                        var lbtimerecive = ((Literal)e.Row.FindControl("lbtimerecive"));
                        var lblnode_state = ((Literal)e.Row.FindControl("lblnode_state"));
                        //CommentSap
                        var txtsys = (Literal)e.Row.FindControl("ltSys");
                        var lblsta = (Label)e.Row.FindControl("txtIDSta");
                        var StatusComment = (Label)e.Row.FindControl("StatusComment");
                        var lbStatusComment = (Label)e.Row.FindControl("lbStatusComment");
                        var StatusClosejob = (Label)e.Row.FindControl("StatusClosejob");
                        //  var StatusClosejobIT = (Label)e.Row.FindControl("StatusClosejobIT");
                        var btnquest = (LinkButton)e.Row.FindControl("lbQuestion1");
                        var lblanssta = (Label)e.Row.FindControl("lblitfeedsta");
                        var lblAdminDoingIDX = (Label)e.Row.FindControl("lblAdminDoingIDX");
                        var ITRepairEmpIDX = ((Label)e.Row.FindControl("ITRepairEmpIDX"));
                        int emp = int.Parse(ITRepairEmpIDX.Text);
                        var lbview1 = (LinkButton)e.Row.FindControl("lbVeiw1");

                        linkBtnTrigger(lbview1);
                        linkBtnTrigger(btn_saveadduser);

                        if (txtsys.Text == "3" && int.Parse(lblsta.Text) == 33 && lblanssta.Text != "1" && lblnode_state.Text != "11")
                        {
                            //btnquest.Visible = true;
                            if (emp == int.Parse(ViewState["EmpIDX"].ToString()))
                            {
                                //txt.Text = "1111";// Convert.ToString(emp);
                                btnquest.Visible = true;
                            }
                            else
                            {
                                btnquest.Visible = false;
                            }


                        }
                        else
                        {
                            btnquest.Visible = false;
                        }

                        ViewState["stateanswer"] = lblanssta.Text;





                        if (txtsys.Text == "2" && lbStatusComment.Text == "1")
                        {
                            StatusComment.Visible = true;
                        }
                        else
                        {
                            StatusComment.Visible = false;
                        }


                        if (int.Parse(txtIDSta.Text) == 0 || int.Parse(txtIDSta.Text) == 32 ||
                            int.Parse(txtIDSta.Text) == 33 || int.Parse(txtIDSta.Text) == 34 || int.Parse(txtIDSta.Text) == 54)  // เสร็จสมบูรณ์
                        {
                            ltCCAstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                            ltCCAstatus.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = true;
                            lbtimerecive.Visible = true;
                            BoxDate.Visible = true;

                            if (lblnode_state.Text == "99")
                            {
                                StatusClosejob.Visible = false;

                            }
                            else if (lblAdminDoingIDX.Text == "0")
                            {
                                StatusClosejob.Visible = true;
                                StatusClosejob.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF8C1A");
                                StatusClosejob.Style["font-weight"] = "bold";
                            }



                        }
                        else if (int.Parse(txtIDSta.Text) == 1 || int.Parse(txtIDSta.Text) == 7 ||
                            int.Parse(txtIDSta.Text) == 20 || int.Parse(txtIDSta.Text) == 26 || int.Parse(txtIDSta.Text) == 48)  // ยังไม่รับงาน
                        {
                            ltCCAstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                            ltCCAstatus.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = false;
                            lbtimerecive.Visible = false;
                            BoxDate.Visible = false;
                            StatusClosejob.Visible = false;
                        }
                        else if (int.Parse(txtIDSta.Text) == 43 || int.Parse(txtIDSta.Text) == 44 || int.Parse(txtIDSta.Text) == 57 || int.Parse(txtIDSta.Text) == 55
                            || int.Parse(txtIDSta.Text) == 45 || int.Parse(txtIDSta.Text) == 46 || int.Parse(txtIDSta.Text) == 47 || int.Parse(txtIDSta.Text) == 56
                            || int.Parse(txtIDSta.Text) == 60)  // ส่งซัพ
                        {
                            ltCCAstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff6600");
                            ltCCAstatus.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = false;
                            lbtimerecive.Visible = false;
                            BoxDate.Visible = false;
                            StatusClosejob.Visible = false;
                        }
                        else if (int.Parse(txtIDSta.Text) == 2 || int.Parse(txtIDSta.Text) == 8 || int.Parse(txtIDSta.Text) == 49
                          || int.Parse(txtIDSta.Text) == 21 || int.Parse(txtIDSta.Text) == 27) //  กำลังดำเนินการ
                        {
                            ltCCAstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                            ltCCAstatus.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = false;
                            lbtimerecive.Visible = false;
                            BoxDate.Visible = false;
                            StatusClosejob.Visible = false;
                        }
                        else if (int.Parse(txtIDSta.Text) == 3 || int.Parse(txtIDSta.Text) == 9 || int.Parse(txtIDSta.Text) == 50
                          || int.Parse(txtIDSta.Text) == 22 || int.Parse(txtIDSta.Text) == 28) //  กำลังตรวจสอบ
                        {
                            ltCCAstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0000FF");
                            ltCCAstatus.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = false;
                            lbtimerecive.Visible = false;
                            BoxDate.Visible = false;
                            StatusClosejob.Visible = false;
                        }
                    }

                    else
                    {
                        var lbview11 = (LinkButton)e.Row.FindControl("lbVeiw1");

                        linkBtnTrigger(lbview11);


                    }


                }


                break;

            case "gvFileSAP":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;

            case "GvComment":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                GridView GvComment = (GridView)ViewIndex.FindControl("GvComment");
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    if (GvComment.EditIndex != e.Row.RowIndex) //to overlook header row  int.Parse(ViewState["EmpIDX"].ToString()) ViewState["EmpIDX"].ToString();  Session["EmpIDX"]
                    {

                        var btnedit = ((LinkButton)e.Row.FindControl("Edit"));
                        var lblIDXCMSAP = ((Label)e.Row.FindControl("lblIDXCMSAP"));
                        int emp = int.Parse(lblIDXCMSAP.Text);

                        if (emp == int.Parse(ViewState["EmpIDX"].ToString()))
                        {
                            //txt.Text = "1111";// Convert.ToString(emp);
                            btnedit.Visible = true;
                        }
                        else
                        {
                            //txt.Text = "2222";
                            btnedit.Visible = false;
                        }
                    }

                }
                break;

            case "GvQuestionIT":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }

                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    if (GvQuestionIT.EditIndex != e.Row.RowIndex)
                //    {
                //        RadioButtonList rdbChoice = (RadioButtonList)e.Row.FindControl("rdbChoice");
                //        RadioBtnTrigger(rdbChoice);
                //    }
                //}

                break;






        }

    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvComment":

                GridView GvComment = (GridView)ViewIndex.FindControl("GvComment");
                GvComment.EditIndex = -1;
                Select_GvComment_List();

                break;
        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvComment":
                GridView GvComment = (GridView)ViewIndex.FindControl("GvComment");
                GvComment.EditIndex = e.NewEditIndex;
                Select_GvComment_List();

                break;


        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvComment":

                GridView GvComment = (GridView)ViewIndex.FindControl("GvComment");


                int CMIDX = Convert.ToInt32(GvComment.DataKeys[e.RowIndex].Values[0].ToString());
                var txtNameTypecodeEdit = (TextBox)GvComment.Rows[e.RowIndex].FindControl("txtNameTypecodeEdit");
                var txtEmpIDX = (TextBox)GvComment.Rows[e.RowIndex].FindControl("txtEmpIDX");

                var txtEmpIDXUpdate = (TextBox)GvComment.Rows[e.RowIndex].FindControl("txtEmpIDXUpdate");

                GvComment.EditIndex = -1;

                ViewState["CMIDX_Update1"] = CMIDX;
                ViewState["Comment_Update1"] = txtNameTypecodeEdit.Text;
                ViewState["EmpIDX_Update1"] = txtEmpIDX.Text; ;

                //ViewState["btnedit"] = btnedit;
                //ViewState["EmpIDX_Update1"] = txtEmpIDX.Text;
                //ViewState["TS2Status_Update"] = StatusUpdate.SelectedValue; int.Parse(ViewState["EmpIDX"].ToString());

                Update_GvComment();
                Select_GvComment_List();

                break;
        }
    }

    #endregion

    #endregion

    #region Set_Defult_Index

    protected void Set_Defult_Index()
    {
        lbindex.BackColor = System.Drawing.Color.LightGray;
        lbalterrepair.BackColor = System.Drawing.Color.Transparent;

        SETBoxAllSearch.Visible = false;
        BoxButtonSearchShow.Visible = true;
        BoxButtonSearchHide.Visible = false;

        if (Page.RouteData.Values["link"] != null)
        {
            string _link = Page.RouteData.Values["link"].ToString().ToLower();
            ViewState["link"] = _link;

            string[] urldecry = (_funcTool.getDecryptRC4(_link, keyitrepair)).Split('|');

            if (ViewState["link"].ToString() != "0")
            {
                ViewState["URQID_Detail"] = urldecry[0];
                ViewState["m0_node"] = urldecry[1];
                ViewState["node_state"] = urldecry[2];
                ViewState["SysIDX"] = urldecry[3];

                Select_DetailList();

                if (urldecry[3] == "2" || urldecry[3] == "22" || urldecry[3] == "23")
                {

                    ddlstatus_comment.AppendDataBoundItems = true;
                    ddlstatus_comment.Items.Clear();
                    if (ViewState["rdept_idx"].ToString() == "20")
                    {
                        ddlstatus_comment.Items.Add(new ListItem("รอ User Comment ตอบกลับ", "47"));
                    }
                    else
                    {
                        ddlstatus_comment.Items.Add(new ListItem("กำลังดำเนินการ", "2"));
                        divstatus_comment.Visible = false;

                    }

                }

                panel_status.Visible = false;
                div_res.Visible = false;
            }

            if (_link != "0" && urldecry[1] != "9")
            {
                gridviewindex.Visible = false;
                BoxSearch.Visible = false;
                div_showdata.Visible = true;
                panel_status.Visible = false;
                div_res.Visible = false;

                Select_DetailList();
                // Select_DetailCloseJobList();
                Select_GvComment_List();
                setFormData();
                Select_EditPriority();
                Select_EditPriority_Close();

            }
            else if (_link != "0" && urldecry[1] == "9" && urldecry[3] == "3")
            {
                // text.Text = "เข้า show แบบสอบถาม";

                if (!IsPostBack)
                {
                    if (ViewState["CEmpIDX_Create"].ToString() == ViewState["EmpIDX"].ToString())
                    {
                        MvMaster.SetActiveView(ViewQuestion);
                        Select_GvQuestionIT_List();
                    }
                    else
                    {
                        gridviewindex.Visible = false;
                        BoxSearch.Visible = false;
                        div_showdata.Visible = true;
                        panel_status.Visible = false;
                        div_res.Visible = false;

                        Select_DetailList();
                        Select_DetailCloseJobList();
                        Select_GvComment_List();
                        setFormData();
                    }
                }

                panel_status.Visible = false;
                div_res.Visible = false;
            }
            else if (_link != "0" && urldecry[1] == "9" && urldecry[3] != "3")
            {
                gridviewindex.Visible = false;
                BoxSearch.Visible = false;
                div_showdata.Visible = true;
                panel_status.Visible = false;
                div_res.Visible = false;

                Select_DetailList();
                Select_DetailCloseJobList();
                Select_GvComment_List();
                setFormData();
            }

            try
            {

                string getPathLotus = ConfigurationSettings.AppSettings["PathFile_SAP"];

                string filePathLotus = Server.MapPath(getPathLotus + ViewState["DocCode"].ToString());
                DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                SearchDirectories(myDirLotus, ViewState["DocCode"].ToString());

                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + myDirLotus + "');", true);



            }
            catch
            {

            }
        }
        else
        {
            ViewState["link"] = "0";
        }
    }

    protected void Set_Defult_TextSearch()
    {
        ddlTypeSearchDate.AppendDataBoundItems = true;
        ddlTypeSearchDate.Items.Clear();
        ddlTypeSearchDate.Items.Insert(0, new ListItem("เลือกประเภทการค้นหา ........", "0"));
        ddlTypeSearchDate.Items.Insert(1, new ListItem("วันที่รับงาน", "1"));
        ddlTypeSearchDate.Items.Insert(2, new ListItem("วันที่ปิดงาน", "2"));

        ddlSearchDate.AppendDataBoundItems = true;
        ddlSearchDate.Items.Clear();
        ddlSearchDate.Items.Insert(0, new ListItem("เลือกเงื่อนไข ....", "0"));
        ddlSearchDate.Items.Insert(1, new ListItem("มากกว่า >", "1"));
        ddlSearchDate.Items.Insert(2, new ListItem("น้อยกว่า <", "2"));
        ddlSearchDate.Items.Insert(3, new ListItem("ระหว่าง <>", "3"));


        AddEndDate.Enabled = false;
        AddStartdate.Text = string.Empty;
        AddEndDate.Text = string.Empty;
        txtSearchName.Text = string.Empty;
        txtSearchNameEN.Text = string.Empty;
        txtSearchSurname.Text = string.Empty;
        txtSearchSurnameEN.Text = string.Empty;

    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    #region Trigger
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }


    protected void ImangeBtnTrigger(ImageButton ImgBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = ImgBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);

    }

    protected void linkBtnAsynTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger asynctrigger1 = new AsyncPostBackTrigger();
        asynctrigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(asynctrigger1);
    }
    //protected void RadioBtnTrigger(RadioButtonList rdoBtnID)
    //{
    //    UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
    //    UpdatePanelControlTrigger trigger1 = new AsyncPostBackTrigger();
    //    trigger1.ControlID = rdoBtnID.UniqueID;
    //    updatePanel.Triggers.Add(trigger1);
    //}


    #endregion

    #region Directories_File URL


    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }



    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            GridView gvFileSAP = (GridView)FvDetailRepair.FindControl("gvFileSAP");
            // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

            if (dt1.Rows.Count > 0)
            {

                ds1.Tables.Add(dt1);
                // gvFileLo1.Visible = true;
                gvFileSAP.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                gvFileSAP.DataBind();
                ds1.Dispose();
            }
            else
            {

                gvFileSAP.DataSource = null;
                gvFileSAP.DataBind();

            }
        }
        catch
        {
            //ViewState["CheckFile"] = "0";
            checkfile = "11";
        }
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {
            case "ddlTypeSearchDate":

                if (int.Parse(ddlTypeSearchDate.SelectedValue) != 00)
                {
                    BoxSearch.Visible = true;
                }
                else
                {
                    AddStartdate.Text = string.Empty;
                    AddEndDate.Text = string.Empty;

                    ddlSearchDate.AppendDataBoundItems = true;
                    ddlSearchDate.Items.Clear();
                    ddlSearchDate.Items.Add(new ListItem("เลือกเงื่อนไข ....", "00"));
                    ddlSearchDate.Items.Add(new ListItem("มากกว่า >", "1"));
                    ddlSearchDate.Items.Add(new ListItem("น้อยกว่า <", "2"));
                    ddlSearchDate.Items.Add(new ListItem("ระหว่าง <>", "3"));
                }

                break;

            case "ddlSearchDate":

                if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                {
                    AddEndDate.Enabled = true;
                }
                else
                {
                    AddEndDate.Enabled = false;
                    AddEndDate.Text = string.Empty;
                }

                break;
            case "ddlSearchOrg":

                getDepartmentList(ddlSearchDep, int.Parse(ddlSearchOrg.SelectedValue));

                break;

            case "ddlselect_choose":
                DropDownList ddlselect_choose = (DropDownList)FvInsertSAP.FindControl("ddlselect_choose");
                Control div_choose = (Control)FvInsertSAP.FindControl("div_choose");
                DropDownList ddlselectother = (DropDownList)FvInsertSAP.FindControl("ddlselectother");
                if (ddlselect_choose.SelectedValue == "1")
                {
                    div_choose.Visible = false;
                    Select_HolderIT();


                }
                else
                {
                    div_choose.Visible = true;

                    ddlselectother.Items.Clear();
                    ddlselectother.AppendDataBoundItems = true;
                    ddlselectother.Items.Add(new ListItem("กรุณาเลือกชื่อผู้แจ้ง....", "0"));

                    _dtsupport = new DataSupportIT();
                    _dtsupport.BoxUserRequest = new UserRequestList[1];
                    UserRequestList selectchoose = new UserRequestList();


                    selectchoose.RSecID = int.Parse(ViewState["Sec_idx"].ToString());

                    _dtsupport.BoxUserRequest[0] = selectchoose;
                    //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));
                    ////_localJson = _funcTool.convertObjectToJson(_dtsupport);
                    ////text.Text = _localJson;

                    _dtsupport = callServicePostITRepair(urlSelectChooseOther, _dtsupport);



                    ddlselectother.DataSource = _dtsupport.BoxUserRequest;
                    ddlselectother.DataTextField = "FullNameTH";
                    ddlselectother.DataValueField = "EmpIDX";
                    ddlselectother.DataBind();

                }

                break;

            case "ddSystemSearch":

                if (ddSystemSearch.SelectedValue == "0")
                {


                    ddSystemSearch.Items.Clear();
                    ddSystemSearch.AppendDataBoundItems = true;
                    ddSystemSearch.Items.Add(new ListItem("กรุณาเลือกระบบ....", "0"));

                    _dtsupport = new DataSupportIT();

                    _dtsupport.BoxSystem = new SystemtList[1];
                    SystemtList search = new SystemtList();

                    _dtsupport.BoxSystem[0] = search;

                    _dtsupport = callServicePostITRepair(urlSelect_SYSTEMList, _dtsupport);

                    ddSystemSearch.DataSource = _dtsupport.BoxSystem;
                    ddSystemSearch.DataTextField = "SysNameTH";
                    ddSystemSearch.DataValueField = "SysIDX";
                    ddSystemSearch.DataBind();

                    ddlSearchStatus.Items.Clear();
                    ddlSearchStatus.Items.Insert(0, new ListItem("กรุณาเลือกสถานะดำเนินการ....", "88"));
                    ddlSearchStatus.SelectedValue = "0";
                }
                else
                {

                    ddlSearchStatus.Items.Clear();
                    ddlSearchStatus.AppendDataBoundItems = true;
                    ddlSearchStatus.Items.Add(new ListItem("กรุณาเลือกสถานะดำเนินการ....", "88"));

                    _dtsupport = new DataSupportIT();
                    _dtsupport.BoxSystem = new SystemtList[1];
                    SystemtList search = new SystemtList();

                    search.SysIDX = int.Parse(ddSystemSearch.SelectedValue);

                    _dtsupport.BoxSystem[0] = search;

                    _dtsupport = callServicePostITRepair(urlSelect_StatusSAP, _dtsupport);


                    ddlSearchStatus.DataSource = _dtsupport.BoxSystem;
                    ddlSearchStatus.DataTextField = "status_name";
                    ddlSearchStatus.DataValueField = "stidx";
                    ddlSearchStatus.DataBind();
                }


                break;

            case "ddlLV1":
                DropDownList ddlLV1 = (DropDownList)FvCloseJob.FindControl("ddlLV1");
                DropDownList ddlLV2 = (DropDownList)FvCloseJob.FindControl("ddlLV2");
                Label lblMS2IDX = (Label)FvCloseJob.FindControl("lblMS2IDX");



                if (ddlLV1.SelectedValue == "0")
                {
                    ddlLV1.Items.Clear();
                    ddlLV1.AppendDataBoundItems = true;
                    ddlLV1.Items.Add(new ListItem("Select Module ....", "0"));

                    _dtsupport = new DataSupportIT();
                    _dtsupport.BoxUserRequest = new UserRequestList[1];
                    UserRequestList ddllv1 = new UserRequestList();

                    _dtsupport.BoxUserRequest[0] = ddllv1;

                    _dtsupport = callServicePostITRepair(urlSelect_ddlLV1SAP, _dtsupport);


                    ddlLV1.DataSource = _dtsupport.BoxUserRequest;
                    ddlLV1.DataTextField = "Name_Code1";
                    ddlLV1.DataValueField = "MS1IDX";
                    ddlLV1.DataBind();

                    ddlLV2.Items.Clear();
                    ddlLV2.Items.Insert(0, new ListItem("Select Topic....", "0"));
                    ddlLV2.SelectedValue = "0";


                }
                else
                {

                    ddlLV2.Items.Clear();
                    ddlLV2.AppendDataBoundItems = true;
                    ddlLV2.Items.Add(new ListItem("Select Topic ....", "0"));

                    //// ddlLV2.SelectedValue = ViewState["MS2IDX"].ToString();

                    _dtsupport = new DataSupportIT();
                    _dtsupport.BoxUserRequest = new UserRequestList[1];
                    UserRequestList ddllv2 = new UserRequestList();

                    ddllv2.MS1IDX = int.Parse(ddlLV1.SelectedValue);
                    _dtsupport.BoxUserRequest[0] = ddllv2;

                    _dtsupport = callServicePostITRepair(urlSelect_ddlLV2SAP, _dtsupport);


                    ddlLV2.DataSource = _dtsupport.BoxUserRequest;
                    ddlLV2.DataTextField = "Name_Code2";
                    ddlLV2.DataValueField = "MS2IDX";
                    ddlLV2.DataBind();

                    //  ddlLV2.SelectedValue = lblMS2IDX.Text;// ViewState["MS2IDX"].ToString();

                }


                break;

            case "ddlPOSLV1":
                FormView FvCloseJobPOS = (FormView)ViewIndex.FindControl("FvCloseJobPOS");
                DropDownList ddlPOSLV1 = (DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV1");
                DropDownList ddlPOSLV2 = (DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV2");
                DropDownList ddlPOSLV3 = (DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV3");
                DropDownList ddlPOSLV4 = (DropDownList)FvCloseJobPOS.FindControl("ddlPOSLV4");

                if (ddlPOSLV1.SelectedValue == "0")
                {

                    ddlPOSLV1.Items.Clear();
                    ddlPOSLV1.AppendDataBoundItems = true;
                    ddlPOSLV1.Items.Add(new ListItem("เลือกเคสแจ้งซ่อม....", "0"));


                    _dtsupport = new DataSupportIT();

                    _dtsupport.BoxPOSList = new POSList[1];
                    POSList casepos1 = new POSList();

                    _dtsupport.BoxPOSList[0] = casepos1;


                    _dtsupport = callServicePostITRepair(urlSelectCaseLV1POSU0, _dtsupport);

                    ddlPOSLV1.DataSource = _dtsupport.BoxPOSList;
                    ddlPOSLV1.DataTextField = "Name_Code1";
                    ddlPOSLV1.DataValueField = "POS1IDX";
                    ddlPOSLV1.DataBind();


                    ddlPOSLV2.Items.Clear();
                    ddlPOSLV2.AppendDataBoundItems = true;
                    ddlPOSLV2.Items.Add(new ListItem("เลือกอาการแจ้งซ่อม....", "0"));

                    ddlPOSLV3.Items.Clear();
                    ddlPOSLV3.AppendDataBoundItems = true;
                    ddlPOSLV3.Items.Add(new ListItem("เลือกแก้ไขแจ้งซ่อม....", "0"));

                    ddlPOSLV4.Items.Clear();
                    ddlPOSLV4.AppendDataBoundItems = true;
                    ddlPOSLV4.Items.Add(new ListItem("เลือกสาเหตุแจ้งซ่อม....", "0"));

                }
                else
                {
                    ddlPOSLV2.Items.Clear();
                    ddlPOSLV2.AppendDataBoundItems = true;
                    ddlPOSLV2.Items.Add(new ListItem("เลือกอาการแจ้งซ่อม....", "0"));


                    _dtsupport = new DataSupportIT();

                    _dtsupport.BoxPOSList = new POSList[1];
                    POSList casepos1 = new POSList();

                    casepos1.POS1IDX = int.Parse(ddlPOSLV1.SelectedValue);
                    _dtsupport.BoxPOSList[0] = casepos1;


                    _dtsupport = callServicePostITRepair(urlSelectCaseLV2POSU0, _dtsupport);

                    ddlPOSLV2.DataSource = _dtsupport.BoxPOSList;
                    ddlPOSLV2.DataTextField = "Name_Code2";
                    ddlPOSLV2.DataValueField = "POS2IDX";
                    ddlPOSLV2.DataBind();



                    ddlPOSLV3.Items.Clear();
                    ddlPOSLV3.AppendDataBoundItems = true;
                    ddlPOSLV3.Items.Add(new ListItem("เลือกแก้ไขแจ้งซ่อม....", "0"));

                    _dtsupport = new DataSupportIT();

                    _dtsupport.BoxPOSList = new POSList[1];
                    POSList casepos3 = new POSList();

                    casepos3.POS1IDX = int.Parse(ddlPOSLV1.SelectedValue);
                    _dtsupport.BoxPOSList[0] = casepos3;


                    _dtsupport = callServicePostITRepair(urlSelectCaseLV3POSU0, _dtsupport);

                    ddlPOSLV3.DataSource = _dtsupport.BoxPOSList;
                    ddlPOSLV3.DataTextField = "Name_Code3";
                    ddlPOSLV3.DataValueField = "POS3IDX";
                    ddlPOSLV3.DataBind();


                    ddlPOSLV4.Items.Clear();
                    ddlPOSLV4.AppendDataBoundItems = true;
                    ddlPOSLV4.Items.Add(new ListItem("เลือกสาเหตุแจ้งซ่อม....", "0"));


                    _dtsupport = new DataSupportIT();

                    _dtsupport.BoxPOSList = new POSList[1];
                    POSList casepos4 = new POSList();

                    casepos4.POS1IDX = int.Parse(ddlPOSLV1.SelectedValue);
                    _dtsupport.BoxPOSList[0] = casepos4;


                    _dtsupport = callServicePostITRepair(urlSelectCaseLV4POSU0, _dtsupport);

                    ddlPOSLV4.DataSource = _dtsupport.BoxPOSList;
                    ddlPOSLV4.DataTextField = "Name_Code4";
                    ddlPOSLV4.DataValueField = "POS4IDX";
                    ddlPOSLV4.DataBind();



                }

                break;

            case "ddl_chooseit":

                FormView FvCloseJobPOS1 = (FormView)ViewIndex.FindControl("FvCloseJobPOS");
                Label lblsysidx = (Label)FvDetailRepair.FindControl("lblsysidx");

                if (ddl_chooseit.SelectedValue == "22" && ViewState["SysIDX"].ToString() == "3")
                {
                    btnadminfinishgetjobsap.Visible = false;
                    btncancelgetjobsap.Visible = false;
                    AdminAcceptCloseJob.Visible = true;
                    FvCloseJob.Visible = false;
                    FvCloseJobIT.Visible = true;
                    FvCloseJobGM.Visible = false;
                    FvCloseJobPOS1.Visible = false;
                    FvCloseJobRES.Visible = false;

                    //  Select_HolderIT();
                }
                else if (ddl_chooseit.SelectedValue == "50" && ViewState["SysIDX"].ToString() == "28")
                {
                    btnadminfinishgetjobsap.Visible = false;
                    btncancelgetjobsap.Visible = false;
                    AdminAcceptCloseJob.Visible = true;
                    FvCloseJob.Visible = false;
                    FvCloseJobIT.Visible = false;
                    FvCloseJobGM.Visible = false;
                    FvCloseJobPOS1.Visible = false;
                    FvCloseJobRES.Visible = true;
                    linkBtnAsynTrigger(btnclosejob);

                }
                else if (ddl_chooseit.SelectedValue == "28" && ViewState["SysIDX"].ToString() == "20")
                {
                    btnadminfinishgetjobsap.Visible = false;
                    btncancelgetjobsap.Visible = false;
                    AdminAcceptCloseJob.Visible = true;
                    FvCloseJob.Visible = false;
                    FvCloseJobIT.Visible = false;
                    FvCloseJobGM.Visible = false;
                    FvCloseJobPOS1.Visible = true;
                    FvCloseJobRES.Visible = false;
                    btnadmingetjobsap.Visible = false;
                }
                else
                {
                    btnadminfinishgetjobsap.Visible = true;
                    btncancelgetjobsap.Visible = true;
                    AdminAcceptCloseJob.Visible = false;

                }
                break;

            case "ddlselectother":

                Select_HolderIT();
                break;

            case "ddlRESLV1":

                SetDDLCaseLV2();

                break;

            case "ddlRESLV2":

                SetDDLCaseLV3();

                break;
            case "ddlRESLV3":

                SetDDLCaseLV4();

                break;

            case "ddlITLV1":

                Select_ddlLV2IT_Edit();

                break;
            case "ddlITLV2":
                Select_ddlLV3IT_edit();
                break;
            case "ddlITLV3":
                Select_ddlLV4IT_edit();

                break;

                //case "ddlITLV1":
                //    DropDownList ddlITLV1 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV1"));
                //    DropDownList ddlITLV2 = ((DropDownList)FvCloseJobIT.FindControl("ddlITLV2"));
                //    Select_ddlLV2IT();

                //    break;
                //case "ddlITLV2":
                //    Select_ddlLV3IT();
                //    break;
                //case "ddlITLV3":
                //    Select_ddlLV4IT();
                //    break;

        }
    }
    #endregion

    #region FormView

    #region SetFormView
    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewAdd.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();



                }
                break;

            case "FvInesrtSAP":
                FormView FvInesrtSAP = (FormView)ViewAdd.FindControl("FvInesrtSAP");

                if (FvInesrtSAP.CurrentMode == FormViewMode.Insert)
                {
                    Select_Location();
                    Select_Priority();

                }
                break;

            case "FvCloseJob":

                FormView FvCloseJob = (FormView)ViewIndex.FindControl("FvCloseJob");
                Label lblsapsmg = (Label)FvCloseJob.FindControl("lblsapsmg");
                TextBox txtsapmsg = (TextBox)FvCloseJob.FindControl("txtsapmsg");
                if (FvCloseJob.CurrentMode == FormViewMode.Insert)
                {
                    Select_ddlLV1();
                    Select_ddlLV2();
                    Select_ddlLV3();
                    Select_ddlLV4();
                    Select_ddlLV5();

                    if (ViewState["SysIDX"].ToString() == "2")
                    {
                        lblsapsmg.Text = "Sap Msg : ";
                        txtsapmsg.Attributes["placeholder"] = "Sap Msg ......";


                    }
                    else if (ViewState["SysIDX"].ToString() == "22")
                    {
                        lblsapsmg.Text = "BI Msg : ";
                        txtsapmsg.Attributes["placeholder"] = "BI Msg ......";

                    }
                    else if (ViewState["SysIDX"].ToString() == "23")
                    {
                        lblsapsmg.Text = "BP Msg : ";
                        txtsapmsg.Attributes["placeholder"] = "BP Msg ......";
                    }

                }

                break;

            case "FvCloseJobIT":
                FormView FvCloseJobIT = (FormView)ViewIndex.FindControl("FvCloseJobIT");
                DropDownList ddlITLV2 = (DropDownList)FvCloseJobIT.FindControl("ddlITLV2");

                if (FvCloseJobIT.CurrentMode == FormViewMode.Insert)
                {
                    Select_ddlLV1IT();
                    Select_ddlLV2IT();
                    Select_ddlLV3IT();
                    Select_ddlLV4IT();

                }

                break;

            case "FvCloseJobRES":
                FormView FvCloseJobRES = (FormView)ViewIndex.FindControl("FvCloseJobRES");

                if (FvCloseJobRES.CurrentMode == FormViewMode.Insert)
                {
                    SetDDLCaseLV1_edit();
                    SetDDLCaseLV2_edit();
                    SetDDLCaseLV3_edit();
                    SetDDLCaseLV4_edit();
                    Select_EditPriority_RESClose();
                    //ddlRESLV1.SelectedValue = ViewState["RES1IDX"].ToString();
                    //ddlRESLV2.SelectedValue = ViewState["RES2IDX"].ToString();
                    //ddlRESLV3.SelectedValue = ViewState["RES3IDX"].ToString();
                    //ddlRESLV4.SelectedValue = ViewState["RES4IDX"].ToString();

                }


                break;


            case "FvCloseJobPOS":
                FormView FvCloseJobPOS = (FormView)ViewIndex.FindControl("FvCloseJobPOS");

                if (FvCloseJobPOS.CurrentMode == FormViewMode.Insert)
                {
                    Select_ddlPOSLV1();
                    Select_ddlPOSLV2();
                    Select_ddlPOSLV3();
                    Select_ddlPOSLV4();

                }

                break;

        }
    }

    #endregion

    #endregion


    public void setddlstatus()
    {
        if (ViewState["SysIDX"].ToString() == "3" && ViewState["m0_status"].ToString() != "43" && ViewState["m0_status"].ToString() != "44" && ViewState["m0_status"].ToString() != "45"
            && ViewState["m0_status"].ToString() != "46")
        {
            ddl_chooseit.AppendDataBoundItems = true;
            ddl_chooseit.Items.Clear();
            ddl_chooseit.Items.Add(new ListItem("เลือกสถานะดำเนินการ", "0"));
            ddl_chooseit.Items.Add(new ListItem("รออะไหล่", "43"));
            ddl_chooseit.Items.Add(new ListItem("ส่งเครม", "44"));
            ddl_chooseit.Items.Add(new ListItem("ติดต่อซัพภายนอก", "45"));
            ddl_chooseit.Items.Add(new ListItem("รอ User สะดวกให้ดำเนินการ", "46"));
            ddl_chooseit.Items.Add(new ListItem("ดำเนินการเรียบร้อยแล้ว", "22"));


        }
        else
        {
            ddl_chooseit.AppendDataBoundItems = true;
            ddl_chooseit.Items.Clear();
            ddl_chooseit.Items.Add(new ListItem("ดำเนินการต่อ", "25"));
        }
        if (ViewState["SysIDX"].ToString() == "28")
        {
            ddl_chooseit.AppendDataBoundItems = true;
            ddl_chooseit.Items.Clear();
            ddl_chooseit.Items.Add(new ListItem("เลือกสถานะดำเนินการ", "0"));
            ddl_chooseit.Items.Add(new ListItem("รออะไหล่", "55"));
            ddl_chooseit.Items.Add(new ListItem("ส่งเครม", "56"));
            ddl_chooseit.Items.Add(new ListItem("ติดต่อซัพภายนอก", "57"));
            ddl_chooseit.Items.Add(new ListItem("ดำเนินการเรียบร้อยแล้ว", "50"));

        }
        if (ViewState["SysIDX"].ToString() == "20")
        {
            if (ViewState["StaIDX_Close"].ToString() != "60")
            {
                ddl_chooseit.AppendDataBoundItems = true;
                ddl_chooseit.Items.Clear();
                ddl_chooseit.Items.Add(new ListItem("เลือกสถานะดำเนินการ", "0"));
                ddl_chooseit.Items.Add(new ListItem("ดำเนินการเสร็จสิ้น", "28"));
                ddl_chooseit.Items.Add(new ListItem("รอผู้ให้บริการภายนอกตรวจสอบ", "60"));
            }
            else
            {
                ddl_chooseit.AppendDataBoundItems = true;
                ddl_chooseit.Items.Clear();
                ddl_chooseit.Items.Add(new ListItem("ดำเนินการต่อ", "27"));
            }

        }
    }

    #region bind Node

    protected void setFormData()
    {

        HiddenField hfM0NodeIDX = (HiddenField)FvDetailRepair.FindControl("hfM0NodeIDX");
        ViewState["m0_node"] = hfM0NodeIDX.Value;

        int m0_node = int.Parse(ViewState["m0_node"].ToString());


        switch (m0_node)
        {
            case 1: // สร้าง/แก้ไข

                setFormDataActor();

                break;


            case 2: // พิจารณาผลโดยผู้สร้าง

                setFormDataActor();

                break;


            case 3: // ดำเนินการโดยเจ้าหน้าที่SAP
            case 4: // ดำเนินการโดยเจ้าหน้าที่ IT
            case 5: // ดำเนินการโดยเจ้าหน้าที่ Google Apps
            case 6: // พิจารณาผลโดยเจ้าหน้าที่ SAP
            case 7: // พิจารณาผลโดยเจ้าหน้าที่ IT
            case 8: // พิจารณาผลโดยเจ้าหน้าที่ IT
            case 14: //พิจารณาผลโดยเจ้าหน้าที่สาขา
            case 15: //ดำเนินการโดยเจ้าหน้าที่สาขา

                setFormDataActor();

                break;


            case 9: // จบการดำเนินการ


                setFormDataActor();

                break;

            case 10: // พิจารณาโดยเจ้าหน้าที่ POS
                     //case 11: // โอนย้ายระบบ
            case 12: // ดำเนินการโดยเจ้าหน้าที่ POS
                setFormDataActor();

                break;
            case 13: // รอ User Comment กลับ
                setFormDataActor();

                break;
        }

    }

    #endregion

    #region bind Actor

    protected void setFormDataActor()
    {

        HiddenField hfM0ActoreIDX = (HiddenField)FvDetailRepair.FindControl("hfM0ActoreIDX");
        HiddenField hfM0StatusIDX = (HiddenField)FvDetailRepair.FindControl("hfM0StatusIDX");
        Label LbEmpIDX = (Label)FvDetailUserRepair.FindControl("LbEmpIDX");
        FormView FvViewClostJobIT = (FormView)ViewIndex.FindControl("FvViewClostJobIT");
        FormView FvCloseJobPOS = (FormView)ViewIndex.FindControl("FvCloseJobPOS");


        ViewState["m0_actor"] = hfM0ActoreIDX.Value;
        ViewState["m0_status"] = hfM0StatusIDX.Value;
        int m0_actor = int.Parse(hfM0ActoreIDX.Value);

        //text.Text = ViewState["m0_node"].ToString() + "," + ViewState["m0_actor"].ToString() + "," + ViewState["Returncode_closejobsap"].ToString() + "," + ViewState["SysIDX"].ToString();

        switch (m0_actor)
        {

            case 1: // ผู้สร้าง
                if (ViewState["m0_node"].ToString() == "2" && LbEmpIDX.Text == ViewState["EmpIDX"].ToString()) // Node SAP Get Job
                {
                    Panel_user_approve.Visible = true;
                    FvViewClostJob.Visible = true;


                    if (ViewState["SysIDX"].ToString() == "3" || ViewState["SysIDX"].ToString() == "1")
                    {
                        ddl_approve.AppendDataBoundItems = true;
                        ddl_approve.Items.Clear();
                        ddl_approve.Items.Add(new ListItem("กรุณาเลือกสถานะรายการ", "0"));
                        ddl_approve.Items.Add(new ListItem("สามารถใช้งานได้ตามปกติ", "4"));
                        ddl_approve.Items.Add(new ListItem("ยังพบอาการตามที่แจ้ง", "5"));

                    }
                    else
                    {
                        ddl_approve.AppendDataBoundItems = true;
                        ddl_approve.Items.Clear();
                        ddl_approve.Items.Add(new ListItem("กรุณาเลือกสถานะรายการ", "0"));
                        ddl_approve.Items.Add(new ListItem("อนุมัติ", "4"));
                        ddl_approve.Items.Add(new ListItem("ไม่อนุมัติ", "5"));

                    }
                }

                else if (ViewState["m0_node"].ToString() == "13")
                {
                    if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                    {
                        AdminAcceptCloseJob.Visible = true;
                        FvCloseJob.Visible = true;
                        FvCloseJobIT.Visible = false;
                        FvCloseJobGM.Visible = false;
                        FvCloseJobPOS.Visible = false;
                        FvCloseJobRES.Visible = false;

                        FvCloseJob.ChangeMode(FormViewMode.Insert);
                        FvCloseJob.DataBind();
                    }
                    else
                    {
                        getjob_sap.Visible = false;
                        AdminAcceptCloseJob.Visible = false;
                        Panel_ViewCloseJob.Visible = false;
                    }
                }
                else
                {
                    getjob_sap.Visible = false;
                    div_choosesys.Visible = false;
                    Panel_user_approve.Visible = false;
                }

                if (ViewState["m0_node"].ToString() == "9" && ViewState["SysIDX"].ToString() == "2" || ViewState["m0_node"].ToString() == "2" && ViewState["SysIDX"].ToString() == "2" ||
                    ViewState["m0_node"].ToString() == "9" && ViewState["SysIDX"].ToString() == "22" || ViewState["m0_node"].ToString() == "2" && ViewState["SysIDX"].ToString() == "22" ||
                    ViewState["m0_node"].ToString() == "9" && ViewState["SysIDX"].ToString() == "23" || ViewState["m0_node"].ToString() == "2" && ViewState["SysIDX"].ToString() == "23")
                {
                    AdminAcceptCloseJob.Visible = false;
                    Panel_ViewCloseJob.Visible = true;
                    FvViewClostJob.Visible = true;
                    Select_DetailCloseJobList();
                }
                else if (ViewState["m0_node"].ToString() == "9" && ViewState["SysIDX"].ToString() == "3" || ViewState["m0_node"].ToString() == "2" && ViewState["SysIDX"].ToString() == "3")
                {

                    AdminAcceptCloseJob.Visible = false;
                    Panel_ViewCloseJob.Visible = true;
                    FvVIewCloseJobIT.Visible = true;
                    FvViewClostJob.Visible = false;
                    Select_DetailCloseJobListIT();


                }

                else if (ViewState["m0_node"].ToString() == "9" && ViewState["SysIDX"].ToString() == "1" || ViewState["m0_node"].ToString() == "2" && ViewState["SysIDX"].ToString() == "1")
                {

                    AdminAcceptCloseJob.Visible = false;
                    Panel_ViewCloseJob.Visible = true;
                    FvViewClostJob.Visible = false;
                    FvVIewCloseJobGM.Visible = true;
                    Select_DetailCloseJobListGM();

                }
                else if (ViewState["m0_node"].ToString() == "9" && ViewState["SysIDX"].ToString() == "20" || ViewState["m0_node"].ToString() == "2" && ViewState["SysIDX"].ToString() == "20")
                {

                    AdminAcceptCloseJob.Visible = false;
                    Panel_ViewCloseJob.Visible = true;
                    FvViewClostJob.Visible = false;
                    FvViewClosePOS.Visible = true;
                    Select_DetailCloseJobListPOS();
                }
                else if (ViewState["m0_node"].ToString() == "9" && ViewState["SysIDX"].ToString() == "28" || ViewState["m0_node"].ToString() == "2" && ViewState["SysIDX"].ToString() == "28")
                {

                    AdminAcceptCloseJob.Visible = false;
                    Panel_ViewCloseJob.Visible = true;
                    FvViewClostJob.Visible = false;
                    FvViewClosePOS.Visible = false;
                    FvViewCloseRES.Visible = true;
                    Select_DetailCloseJobListRES();
                }

                break;

            case 2: // เจ้าหน้าที่ SAP

                if (ViewState["m0_node"].ToString() == "6") // Node SAP Get Job
                {
                    if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                    {
                        getjob_sap.Visible = true;
                        btnadminfinishgetjobsap.Visible = false;
                        div_choosesys.Visible = true;
                        divchangesys.Visible = true;
                        divchooseen.Visible = false;



                    }
                    else
                    {
                        getjob_sap.Visible = false;
                        div_choosesys.Visible = false;
                    }
                }
                else if (ViewState["m0_node"].ToString() == "3") // Node SAP Doing
                {
                    if (ViewState["Returncode_closejobsap"].ToString() != "2" && ViewState["SysIDX"].ToString() == "2" ||
                        ViewState["Returncode_closejobsap"].ToString() != "2" && ViewState["SysIDX"].ToString() == "22" ||
                        ViewState["Returncode_closejobsap"].ToString() != "2" && ViewState["SysIDX"].ToString() == "23")
                    {

                        if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                        {
                            AdminAcceptCloseJob.Visible = true;
                            FvCloseJobIT.Visible = false;
                            FvCloseJobGM.Visible = false;
                            FvCloseJobPOS.Visible = false;
                            FvCloseJobRES.Visible = false;


                        }
                        else
                        {
                            getjob_sap.Visible = false;
                            AdminAcceptCloseJob.Visible = false;
                            Panel_ViewCloseJob.Visible = false;
                        }
                    }
                    else if (ViewState["Returncode_closejobsap"].ToString() == "2" && ViewState["SysIDX"].ToString() == "2" ||
                        ViewState["Returncode_closejobsap"].ToString() == "2" && ViewState["SysIDX"].ToString() == "22" ||
                        ViewState["Returncode_closejobsap"].ToString() == "2" && ViewState["SysIDX"].ToString() == "23")
                    {
                        if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                        {
                            AdminAcceptCloseJob.Visible = true;
                            FvCloseJob.Visible = true;
                            FvCloseJobGM.Visible = false;
                            FvCloseJobIT.Visible = false;
                            FvCloseJobPOS.Visible = false;
                            FvCloseJobRES.Visible = false;

                        }
                        else
                        {
                            AdminAcceptCloseJob.Visible = false;
                            Panel_ViewCloseJob.Visible = true;
                            FvViewClostJob.Visible = true;
                            FvCloseJobIT.Visible = false;
                            FvCloseJobGM.Visible = false;
                            FvCloseJobPOS.Visible = false;
                            FvCloseJobRES.Visible = false;

                            Select_DetailCloseJobList();

                        }

                    }

                    div_choosesys.Visible = false;
                }

                break;

            case 3: //เจ้าหน้าที่ IT

                if (ViewState["m0_node"].ToString() == "7") // Node IT Get Job
                {

                    if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                    {
                        getjob_sap.Visible = true;
                        divpriority.Visible = false;
                        div_choosesys.Visible = true;
                        divchangesys.Visible = true;
                        divchooseen.Visible = false;

                    }
                    else
                    {

                        getjob_sap.Visible = false;
                    }
                }
                else if (ViewState["m0_node"].ToString() == "4") // Node Doing Job Complete
                {
                    if (ViewState["Returncode_closejobsap"].ToString() != "2" && ViewState["SysIDX"].ToString() == "3")
                    {
                        if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                        {
                            getjob_sap.Visible = true;
                            div_choosesys.Visible = false;
                            btnadminfinishgetjobsap.Visible = true;
                            btnadmingetjobsap.Visible = false;
                            div_statusitfinish.Visible = true;
                            setddlstatus();
                            divpriority.Visible = false;

                        }
                        else
                        {
                            getjob_sap.Visible = false;
                            divpriority.Visible = false;
                            div_choosesys.Visible = false;
                            AdminAcceptCloseJob.Visible = false;
                            Panel_ViewCloseJob.Visible = false;

                        }
                    }

                    else if (ViewState["Returncode_closejobsap"].ToString() == "2" && ViewState["SysIDX"].ToString() == "3")
                    {
                        if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                        {
                            getjob_sap.Visible = true;
                            div_choosesys.Visible = false;
                            btnadminfinishgetjobsap.Visible = true;
                            btnadmingetjobsap.Visible = false;
                            div_statusitfinish.Visible = true;
                            setddlstatus();
                            divpriority.Visible = false;


                        }
                        else
                        {
                            AdminAcceptCloseJob.Visible = false;
                            Panel_ViewCloseJob.Visible = true;
                            FvViewClostJob.Visible = false;
                            FvVIewCloseJobIT.Visible = true;
                            FvVIewCloseJobGM.Visible = false;
                            FvCloseJobRES.Visible = false;
                            FvViewClosePOS.Visible = false;
                            Select_DetailCloseJobListIT();

                        }
                    }
                }


                break;
            case 4: // เจ้าหน้าที่ Google Apps
                if (ViewState["m0_node"].ToString() == "8") // Node IT Get Job
                {
                    if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                    {
                        getjob_sap.Visible = true;
                        divpriority.Visible = false;
                        div_choosesys.Visible = true;
                        divchangesys.Visible = true;
                        divchooseen.Visible = false;

                    }
                    else
                    {

                        getjob_sap.Visible = false;
                    }
                }
                else if (ViewState["m0_node"].ToString() == "5") // Node Doing Job Complete
                {
                    if (ViewState["Returncode_closejobsap"].ToString() != "2" && ViewState["SysIDX"].ToString() == "1")
                    {
                        if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                        {

                            divpriority.Visible = false;
                            AdminAcceptCloseJob.Visible = true;
                            FvCloseJob.Visible = false;
                            FvCloseJobIT.Visible = false;
                            FvCloseJobGM.Visible = true;
                            FvCloseJobPOS.Visible = false;
                            FvCloseJobRES.Visible = false;

                        }
                        else
                        {

                            getjob_sap.Visible = false;
                            divpriority.Visible = false;
                            div_choosesys.Visible = false;
                            AdminAcceptCloseJob.Visible = false;
                            Panel_ViewCloseJob.Visible = false;
                        }
                    }

                    else if (ViewState["Returncode_closejobsap"].ToString() == "2" && ViewState["SysIDX"].ToString() == "1")
                    {
                        if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                        {
                            AdminAcceptCloseJob.Visible = true;
                            FvCloseJob.Visible = false;
                            FvCloseJobIT.Visible = false;
                            FvCloseJobGM.Visible = true;
                            FvCloseJobPOS.Visible = false;
                            FvCloseJobRES.Visible = false;

                        }
                        else
                        {
                            AdminAcceptCloseJob.Visible = false;
                            Panel_ViewCloseJob.Visible = false;

                        }
                    }

                }

                break;

            case 5: // เจ้าหน้าที่ POS

                if (ViewState["m0_node"].ToString() == "10") // Node SAP Get Job
                {
                    if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                    {
                        getjob_sap.Visible = true;
                        divpriority.Visible = false;
                        div_choosesys.Visible = true;
                        divchangesys.Visible = true;
                        divchooseen.Visible = false;


                    }
                    else
                    {

                        getjob_sap.Visible = false;
                    }
                }
                else if (ViewState["m0_node"].ToString() == "12") // Node Doing Job Complete
                {
                    if (ViewState["Returncode_closejobsap"].ToString() != "2" && ViewState["SysIDX"].ToString() == "20")
                    {
                        if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                        {
                            getjob_sap.Visible = true;
                            div_statusitfinish.Visible = true;
                            divpriority.Visible = false;
                            setddlstatus();
                            btnadmingetjobsap.Visible = false;

                        }
                        else
                        {
                            getjob_sap.Visible = true;
                            div_choosesys.Visible = false;
                            btnadminfinishgetjobsap.Visible = true;
                            btnadmingetjobsap.Visible = false;
                            div_statusitfinish.Visible = true;
                            setddlstatus();
                            divpriority.Visible = false;
                        }
                    }

                    else if (ViewState["Returncode_closejobsap"].ToString() == "2" && ViewState["SysIDX"].ToString() == "20")
                    {
                        if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                        {

                            getjob_sap.Visible = true;
                            div_choosesys.Visible = false;
                            btnadminfinishgetjobsap.Visible = true;
                            btnadmingetjobsap.Visible = false;
                            div_statusitfinish.Visible = true;
                            setddlstatus();
                            divpriority.Visible = false;
                        }
                        else
                        {
                            AdminAcceptCloseJob.Visible = false;
                            Panel_ViewCloseJob.Visible = true;

                            FvViewClostJob.Visible = false;
                            FvVIewCloseJobIT.Visible = false;
                            FvVIewCloseJobGM.Visible = false;
                            FvViewClosePOS.Visible = true;
                            FvCloseJobRES.Visible = false;
                            btnadmingetjobsap.Visible = false;
                            Select_DetailCloseJobListPOS();
                        }
                    }


                }

                break;

            case 6: // เจ้าหน้าที่สาขา
                if (ViewState["m0_node"].ToString() == "14") // Node RES Get Job
                {
                    if (ViewState["EmpIDX"].ToString() == "1374" || ViewState["Pos_idx"].ToString() == "13")
                    {
                        getjob_sap.Visible = true;
                        divpriority.Visible = true;
                        div_choosesys.Visible = true;
                        divchangesys.Visible = false;
                        divchooseen.Visible = true;
                    }
                    else
                    {
                        getjob_sap.Visible = false;
                        div_choosesys.Visible = false;
                    }

                }
                else if (ViewState["m0_node"].ToString() == "15") // Node RES Close Job
                {
                    if (ViewState["EmpIDX"].ToString() == "1374" || ViewState["Pos_idx"].ToString() == "13")
                    {
                        getjob_sap.Visible = true;
                        div_choosesys.Visible = false;
                        btnadminfinishgetjobsap.Visible = true;
                        btnadmingetjobsap.Visible = false;
                        div_statusitfinish.Visible = true;
                        setddlstatus();
                        divpriority.Visible = false;
                        FvCloseJobRES.Visible = true;

                    }

                }
                break;

        }

    }

    #endregion

    #region RadioCheckedChange
    protected void Group1_CheckedChanged(Object sender, EventArgs e)
    {
        var ddName = (CheckBox)sender;

        switch (ddName.ID)
        {
            case "rdoaccept":
                if (ViewState["SysIDX"].ToString() == "2" || ViewState["SysIDX"].ToString() == "22" || ViewState["SysIDX"].ToString() == "23")
                {
                    divselectsys.Visible = false;
                    divpriority.Visible = true;
                }
                else
                {
                    divselectsys.Visible = false;
                    divpriority.Visible = false;
                }
                break;


            case "rdochange":
                Label lblorgidx = (Label)FvDetailRepair.FindControl("lblorgidx");

                divselectsys.Visible = true;
                divpriority.Visible = false;

                if (lblorgidx.Text == "3" || lblorgidx.Text == "1")
                {
                    ddl_changesys.AppendDataBoundItems = true;
                    ddl_changesys.Items.Add(new ListItem("POS", "20"));

                }


                break;



        }
    }
    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "btnIndex":
                lbindex.BackColor = System.Drawing.Color.LightGray;
                lbalterrepair.BackColor = System.Drawing.Color.Transparent;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;


            case "btnalterrepair":

                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbalterrepair.BackColor = System.Drawing.Color.LightGray;

                MvMaster.SetActiveView(ViewAdd);

                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();

                FvInsertSAP.ChangeMode(FormViewMode.Insert);
                FvInsertSAP.DataBind();

                Select_Priority();
                Select_Location();
                Select_Remote();
                Select_HolderIT();

                if (ViewState["Org_idx"].ToString() == "3")
                {
                    imgpos.Enabled = true;
                    imgres.Enabled = true;
                }
                else if (ViewState["Org_idx"].ToString() == "1")
                {
                    imgpos.Enabled = true;
                    imgres.Enabled = false;
                }
                else
                {
                    imgpos.Enabled = false;
                    imgres.Enabled = false;

                }

                break;


            case "btnReportPOS":

                string emp_pos = ViewState["EmpIDX"].ToString();
                string rdept_pos = ViewState["rdept_idx"].ToString();

                HttpResponse response_pos = HttpContext.Current.Response;
                response_pos.Clear();

                StringBuilder s_pos = new StringBuilder();
                s_pos.Append("<html>");
                s_pos.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
                s_pos.AppendFormat("<form name='form' action='{0}' method='post'>", url_pos);
                s_pos.Append("<input type='hidden' name='emp_idx' value=" + emp_pos + " />");
                s_pos.Append("<input type='hidden' name='rdept_idx' value=" + rdept_pos + " />");

                s_pos.Append("</form></body></html>");
                response_pos.Write(s_pos.ToString());
                response_pos.End();


                break;


            case "lbReportQuestIT":
                string emp_fb = ViewState["EmpIDX"].ToString();
                string rdept_fb = ViewState["rdept_idx"].ToString();


                HttpResponse response_fb = HttpContext.Current.Response;
                response_fb.Clear();

                StringBuilder s_fb = new StringBuilder();
                s_fb.Append("<html>");
                s_fb.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
                s_fb.AppendFormat("<form name='form' action='{0}' method='post'>", url_fb);
                s_fb.Append("<input type='hidden' name='emp_idx' value=" + emp_fb + " />");
                s_fb.Append("<input type='hidden' name='rdept_idx' value=" + rdept_fb + " />");

                s_fb.Append("</form></body></html>");
                response_fb.Write(s_fb.ToString());
                response_fb.End();
                break;

            case "btnsystem":
                string cmdArg_sys = e.CommandArgument.ToString();
                Control insersap = (Control)FvInsertSAP.FindControl("insersap");
                Control div_usersap = (Control)FvInsertSAP.FindControl("div_usersap");
                Control div_deviceres = (Control)FvInsertSAP.FindControl("div_deviceres");
                Panel BoxRemoteSAP = (Panel)FvInsertSAP.FindControl("BoxRemoteSAP");
                Control selectother = (Control)FvInsertSAP.FindControl("selectother");
                Control div_holder = (Control)FvInsertSAP.FindControl("div_holder");
                Label lbltcode = (Label)FvInsertSAP.FindControl("lbltcode");
                DropDownList ddldevicesres = (DropDownList)FvInsertSAP.FindControl("ddldevicesres");

                ViewState["Add_System"] = cmdArg_sys;



                switch (cmdArg_sys)
                {
                    case "1":


                        Panel_repair.Visible = true;
                        insersap.Visible = false;
                        namesystem.Text = "Google Apps";
                        imggoogle.ImageUrl = "~/masterpage/images/itservice/btn_google_active.png";
                        imgsap.ImageUrl = "~/masterpage/images/itservice/btn_sap.png";
                        imgit.ImageUrl = "~/masterpage/images/itservice/btn_it.png";
                        imgpos.ImageUrl = "~/masterpage/images/itservice/btn_pos.png";
                        imgbp.ImageUrl = "~/masterpage/images/itservice/btn_bp.png";
                        imgbi.ImageUrl = "~/masterpage/images/itservice/btn_bi.png";
                        imgres.ImageUrl = "~/masterpage/images/itservice/btn_res.png";

                        selectother.Visible = true;

                        BoxRemoteSAP.Visible = false;

                        break;
                    case "2":

                        Panel_repair.Visible = true;
                        insersap.Visible = true;
                        div_deviceres.Visible = false;
                        namesystem.Text = "SAP";
                        imgsap.ImageUrl = "~/masterpage/images/itservice/btn_sap_active.png";
                        imggoogle.ImageUrl = "~/masterpage/images/itservice/btn_google.png";
                        imgit.ImageUrl = "~/masterpage/images/itservice/btn_it.png";
                        imgpos.ImageUrl = "~/masterpage/images/itservice/btn_pos.png";
                        imgbi.ImageUrl = "~/masterpage/images/itservice/btn_bi.png";
                        imgbp.ImageUrl = "~/masterpage/images/itservice/btn_bp.png";
                        imgres.ImageUrl = "~/masterpage/images/itservice/btn_res.png";

                        selectother.Visible = false;
                        BoxRemoteSAP.Visible = true;
                        div_holder.Visible = false;
                        lbltcode.Text = "T-Code/หน้าจอที่ใช้งาน :";


                        break;

                    case "3":
                        Panel_repair.Visible = true;
                        insersap.Visible = false;
                        namesystem.Text = "IT Support";
                        imgit.ImageUrl = "~/masterpage/images/itservice/btn_it_active.png";
                        imgsap.ImageUrl = "~/masterpage/images/itservice/btn_sap.png";
                        imggoogle.ImageUrl = "~/masterpage/images/itservice/btn_google.png";
                        imgpos.ImageUrl = "~/masterpage/images/itservice/btn_pos.png";
                        imgbi.ImageUrl = "~/masterpage/images/itservice/btn_bi.png";
                        imgbp.ImageUrl = "~/masterpage/images/itservice/btn_bp.png";
                        imgres.ImageUrl = "~/masterpage/images/itservice/btn_res.png";

                        selectother.Visible = true;

                        BoxRemoteSAP.Visible = true;
                        div_holder.Visible = true;


                        break;

                    case "20":
                        Panel_repair.Visible = true;
                        insersap.Visible = false;
                        namesystem.Text = "POS";
                        imgpos.ImageUrl = "~/masterpage/images/itservice/btn_pos_active.png";
                        imgit.ImageUrl = "~/masterpage/images/itservice/btn_it.png";
                        imgsap.ImageUrl = "~/masterpage/images/itservice/btn_sap.png";
                        imggoogle.ImageUrl = "~/masterpage/images/itservice/btn_google.png";
                        imgbi.ImageUrl = "~/masterpage/images/itservice/btn_bi.png";
                        imgbp.ImageUrl = "~/masterpage/images/itservice/btn_bp.png";
                        imgres.ImageUrl = "~/masterpage/images/itservice/btn_res.png";

                        selectother.Visible = true;

                        BoxRemoteSAP.Visible = true;
                        div_holder.Visible = false;
                        break;

                    case "22":
                        Panel_repair.Visible = true;
                        insersap.Visible = true;
                        div_deviceres.Visible = false;
                        namesystem.Text = "BI";
                        selectother.Visible = false;
                        BoxRemoteSAP.Visible = true;
                        div_holder.Visible = false;
                        lbltcode.Text = "Application/Report :";
                        imgbi.ImageUrl = "~/masterpage/images/itservice/btn_bi_active.png";
                        imgbp.ImageUrl = "~/masterpage/images/itservice/btn_bp.png";
                        imgpos.ImageUrl = "~/masterpage/images/itservice/btn_pos.png";
                        imgit.ImageUrl = "~/masterpage/images/itservice/btn_it.png";
                        imgsap.ImageUrl = "~/masterpage/images/itservice/btn_sap.png";
                        imggoogle.ImageUrl = "~/masterpage/images/itservice/btn_google.png";
                        imgres.ImageUrl = "~/masterpage/images/itservice/btn_res.png";

                        break;

                    case "23":
                        Panel_repair.Visible = true;
                        insersap.Visible = true;
                        div_deviceres.Visible = false;
                        namesystem.Text = "BP";
                        selectother.Visible = false;
                        BoxRemoteSAP.Visible = true;
                        div_holder.Visible = false;
                        lbltcode.Text = "Application/Report :";
                        imgbi.ImageUrl = "~/masterpage/images/itservice/btn_bi.png";
                        imgbp.ImageUrl = "~/masterpage/images/itservice/btn_bp_active.png";
                        imgpos.ImageUrl = "~/masterpage/images/itservice/btn_pos.png";
                        imgit.ImageUrl = "~/masterpage/images/itservice/btn_it.png";
                        imgsap.ImageUrl = "~/masterpage/images/itservice/btn_sap.png";
                        imggoogle.ImageUrl = "~/masterpage/images/itservice/btn_google.png";
                        imgres.ImageUrl = "~/masterpage/images/itservice/btn_res.png";

                        break;

                    case "28":
                        Panel_repair.Visible = true;
                        insersap.Visible = true;
                        div_deviceres.Visible = true;
                        div_usersap.Visible = false;
                        namesystem.Text = "อุปกรณ์สาขา";
                        imgsap.ImageUrl = "~/masterpage/images/itservice/btn_sap.png";
                        imggoogle.ImageUrl = "~/masterpage/images/itservice/btn_google.png";
                        imgit.ImageUrl = "~/masterpage/images/itservice/btn_it.png";
                        imgpos.ImageUrl = "~/masterpage/images/itservice/btn_pos.png";
                        imgbi.ImageUrl = "~/masterpage/images/itservice/btn_bi.png";
                        imgbp.ImageUrl = "~/masterpage/images/itservice/btn_bp.png";
                        imgres.ImageUrl = "~/masterpage/images/itservice/btn_res_active.png";
                        selectother.Visible = false;
                        BoxRemoteSAP.Visible = false;
                        SetDevicesRes(ddldevicesres);

                        break;

                }

                break;

            case "btnsearch":

                _dtsupport.BoxUserRequest = new UserRequestList[1];
                UserRequestList search = new UserRequestList();

                search.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
                search.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
                search.DategetJob = AddStartdate.Text;
                search.DatecloseJob = AddEndDate.Text;
                search.SysIDX_add = int.Parse(ddSystemSearch.SelectedValue);
                search.StaIDX = int.Parse(ddlSearchStatus.SelectedValue);
                search.OrgIDX = int.Parse(ddlSearchOrg.SelectedValue);
                search.RDeptID = int.Parse(ddlSearchDep.SelectedValue);
                search.EmpCode = txtAdminEmpIDX.Text;
                search.DocCode = txtDocCode.Text;
                _dtsupport.BoxUserRequest[0] = search;

                ViewState["BoxSearch"] = _dtsupport;

                //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["BoxSearch"]));

                Select_List();

                break;

            case "btnbistatus":


                string[] arg1bi = new string[0];
                arg1bi = e.CommandArgument.ToString().Split(';');
                int statusidxbi = int.Parse(arg1bi[0]);

                ViewState["Status_BI"] = statusidxbi;
                ViewState["Check_paging"] = "22";

                Select_BIList();

                break;

            case "btnbpstatus":


                string[] arg1bp = new string[0];
                arg1bp = e.CommandArgument.ToString().Split(';');
                int statusidxbp = int.Parse(arg1bp[0]);

                ViewState["Status_BP"] = statusidxbp;
                ViewState["Check_paging"] = "23";

                Select_BPList();

                break;

            case "btnsapstatus":


                string[] arg1 = new string[0];
                arg1 = e.CommandArgument.ToString().Split(';');
                string statusidx = arg1[0];

                ViewState["Status_SAP"] = statusidx;
                ViewState["Check_paging"] = "2";

                Select_SAPList();

                break;



            case "btnitstatus":

                string[] arg1it = new string[0];
                arg1it = e.CommandArgument.ToString().Split(';');
                string statusidxit = arg1it[0];
                ViewState["Check_paging"] = "3";
                ViewState["Status_IT"] = statusidxit;
                // text.Text = ViewState["Status_IT"].ToString();

                Select_ITStatus();

                break;

            case "btngmstatus":

                string[] arg1gm = new string[0];
                arg1gm = e.CommandArgument.ToString().Split(';');
                int statusidxgm = int.Parse(arg1gm[0]);
                ViewState["Check_paging"] = "1";
                ViewState["Status_GM"] = statusidxgm;

                Select_GMStatus();

                break;

            case "btnposstatus":

                string[] arg1pos = new string[0];
                arg1pos = e.CommandArgument.ToString().Split(';');
                int statusidxpos = int.Parse(arg1pos[0]);
                ViewState["Check_paging"] = "20";
                ViewState["Status_POS"] = statusidxpos;

                Select_POSStatus();

                break;

            case "btnresstatus":


                string[] arg1res = new string[0];
                arg1res = e.CommandArgument.ToString().Split(';');
                int statusidxres = int.Parse(arg1res[0]);

                ViewState["Status_RES"] = statusidxres;
                ViewState["Check_paging"] = "28";

                Select_RESList();

                break;


            case "BtnBack":
                //  Page.Response.Redirect(Page.Request.Url.ToString(), true);
                Response.Redirect("http://mas.taokaenoi.co.th/itrepair");

                break;



            case "CmdInsert":

                TextBox txtuser = (TextBox)FvInsertSAP.FindControl("txtuser");
                TextBox txttcode = (TextBox)FvInsertSAP.FindControl("txttcode");
                TextBox txtemail = (TextBox)FvDetailUser.FindControl("txtemail");
                DropDownList ddlprogram = (DropDownList)FvInsertSAP.FindControl("ddlprogram");
                TextBox txtremote = (TextBox)FvInsertSAP.FindControl("txtremote");
                TextBox txtremotepass = (TextBox)FvInsertSAP.FindControl("txtremotepass");
                DropDownList ddlholder = (DropDownList)FvInsertSAP.FindControl("ddlholder");



                if (ddlprogram.SelectedValue != "0" && (ViewState["Add_System"].ToString() == "3" || ViewState["Add_System"].ToString() == "2"
                    || ViewState["Add_System"].ToString() == "22" || ViewState["Add_System"].ToString() == "23") && txtremote.Text == "" ||
                    ddlprogram.SelectedValue != "0" && (ViewState["Add_System"].ToString() == "3" || ViewState["Add_System"].ToString() == "2"
                    || ViewState["Add_System"].ToString() == "22" || ViewState["Add_System"].ToString() == "23") && txtremotepass.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาใส่ YourID และ Password ให้ครบ');", true);

                }
                else
                {
                    Insert_Repair();

                    string urlendcry = Questionrepair(int.Parse(ViewState["URQID_Detail"].ToString()), 1, 1, int.Parse(ViewState["Add_System"].ToString()));
                    string name = ViewState["Email_Insert"].ToString().Split('@')[1].Split('.')[0];

                    if (name == "taokaenoi" || name == "taokaenoiland" || name == "gmail" || name == "drtobi" || name == "genc")
                    // if (txtemail.Text != "" && txtemail.Text != "-" && (name == "taokaenoi" || name == "taokaenoiland" || name == "gmail" || name == "drtobi" || name == "genc"))
                    {

                        switch (ViewState["Add_System"].ToString())
                        {

                            case "1":
                                Panel_repair.Visible = true;

                                _dtsupport = new DataSupportIT();

                                _dtsupport.BoxUserRequest = new UserRequestList[1];
                                UserRequestList gmlist = new UserRequestList();

                                gmlist.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());// URQIDXSap;
                                gmlist.DeviceIDX = 0;
                                gmlist.DeviceETC = "-";
                                gmlist.EmailIDX = 0;
                                gmlist.EmailETC = txtemail.Text;

                                _dtsupport.BoxUserRequest[0] = gmlist;


                                _dtsupport = callServicePostITRepair(urlInsertGMList, _dtsupport);


                                break;
                            case "2":
                            case "22":
                            case "23":

                                _dtsupport = new DataSupportIT();

                                _dtsupport.BoxUserRequest = new UserRequestList[1];
                                UserRequestList saplist = new UserRequestList();



                                saplist.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());// URQIDXSap;
                                saplist.TransactionCode = txttcode.Text;
                                saplist.UserLogonName = txtuser.Text;

                                _dtsupport.BoxUserRequest[0] = saplist;


                                _dtsupport = callServicePostITRepair(urlInsertInsertSAPList, _dtsupport);


                                break;

                            case "3":
                                Panel_repair.Visible = true;

                                _dtsupport = new DataSupportIT();

                                _dtsupport.BoxUserRequest = new UserRequestList[1];
                                UserRequestList itlist = new UserRequestList();

                                itlist.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());// URQIDXSap;
                                itlist.DeviceIDX = int.Parse(ddlholder.SelectedValue);
                                itlist.DeviceETC = "-";

                                _dtsupport.BoxUserRequest[0] = itlist;


                                _dtsupport = callServicePostITRepair(urlInsertITList, _dtsupport);


                                break;

                            case "20":
                                Panel_repair.Visible = true;

                                _dtsupport = new DataSupportIT();

                                _dtsupport.BoxUserRequest = new UserRequestList[1];
                                UserRequestList poslist = new UserRequestList();

                                poslist.URQIDX = int.Parse(ViewState["URQID_Detail"].ToString());// URQIDXSap;

                                _dtsupport.BoxUserRequest[0] = poslist;


                                _dtsupport = callServicePostITRepair(urlInsertPOSList, _dtsupport);

                                break;


                        }

                        try
                        {
                            string sub = String.Empty;
                            DataSupportIT dtsupport_mail = new DataSupportIT();

                            dtsupport_mail = Select_mail(dtsupport_mail);


                            ViewState["DetailUser"] = ViewState["DetailUser"].ToString().Replace('\r', ' ').Replace('\n', ' ');

                            if (ViewState["Add_System"].ToString() == "2" || ViewState["Add_System"].ToString() == "22" || ViewState["Add_System"].ToString() == "23")
                            {
                                if (ViewState["Add_System"].ToString() == "2")
                                {
                                    sub = "[Initial][MIS/แจ้งซ่อม : SAP] - ";
                                }
                                else if (ViewState["Add_System"].ToString() == "22")
                                {
                                    sub = "[Initial][MIS/แจ้งซ่อม : BI] - ";
                                }
                                else if (ViewState["Add_System"].ToString() == "23")
                                {
                                    sub = "[Initial][MIS/แจ้งซ่อม : BP] - ";
                                }

                                _mail_subject = sub + ViewState["DOCSAP"].ToString() + " - " + ViewState["DetailUser"].ToString();
                                _mail_subject = _mail_subject.Replace('\r', ' ').Replace('\n', ' ');
                                _mail_body = servicemail.ITRepairCreateBody(dtsupport_mail.BoxUserRequest[0]);
                                servicemail.SendHtmlFormattedEmailFull(ViewState["Email_Insert"].ToString(), "", emailsap, _mail_subject, _mail_body);
                            }
                            else if (ViewState["Add_System"].ToString() == "1")
                            {
                                _mail_subject = "[MIS/แจ้งซ่อม : GoogleApps] - " + ViewState["DOCSAP"].ToString() + " - " + ViewState["DetailUser"].ToString();
                                _mail_body = servicemail.ITRepairITCreateBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                                servicemail.SendHtmlFormattedEmailFull(ViewState["Email_Insert"].ToString(), "", emailgm, _mail_subject, _mail_body); //ViewState["Email_Insert"].ToString()

                            }
                            else if (ViewState["Add_System"].ToString() == "3")
                            {
                                _mail_subject = "[MIS/แจ้งซ่อม : IT] - " + ViewState["DOCSAP"].ToString() + " - " + ViewState["DetailUser"].ToString();
                                _mail_body = servicemail.ITRepairITCreateBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                                servicemail.SendHtmlFormattedEmailFull(ViewState["Email_Insert"].ToString(), "", emailit, _mail_subject, _mail_body);
                            }
                            else if (ViewState["Add_System"].ToString() == "20")
                            {
                                _mail_subject = "[MIS/แจ้งซ่อม : POS] - " + ViewState["DOCSAP"].ToString() + " - " + ViewState["DetailUser"].ToString();
                                _mail_body = servicemail.ITRepairITCreateBody(dtsupport_mail.BoxUserRequest[0], ViewState["URQID_Detail"].ToString(), urlendcry);
                                servicemail.SendHtmlFormattedEmailFull(ViewState["Email_Insert"].ToString(), "", emailpos, _mail_subject, _mail_body); //ViewState["Email_Insert"].ToString()
                            }
                            else if (ViewState["Add_System"].ToString() == "28")
                            {
                                _mail_subject = "[MIS/แจ้งซ่อม : RES] - " + ViewState["DOCSAP"].ToString() + " - " + ViewState["DetailUser"].ToString();
                                _mail_body = servicemail.ITRepairCreateRESBody(dtsupport_mail.BoxUserRequest[0]);
                                servicemail.SendHtmlFormattedEmailFull(ViewState["Email_Insert"].ToString(), "", emailres, _mail_subject, _mail_body); //ViewState["Email_Insert"].ToString()
                            }
                        }
                        catch
                        {


                            Page.Response.Redirect(Page.Request.Url.ToString(), true);
                        }


                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีอีเมล์กรุณาติดต่อ HR สามารถใช้ได้เฉพาะโดเมน @taokaenoi.co.th / @taokaenoiland.com / @drtobi.co.th / @genc.co.th / @gmail.com');", true);
                    }


                }

                break;



            case "ViewITRepair":


                string[] arg_detail = new string[7];
                arg_detail = e.CommandArgument.ToString().Split(';');
                int IDX = int.Parse(arg_detail[0]);
                int SysIDX = int.Parse(arg_detail[1]);
                int StaIDX = int.Parse(arg_detail[2]);
                int EmpIDX = int.Parse(arg_detail[3]);
                int NCEmpIDX = int.Parse(arg_detail[4]);
                string Doccode = arg_detail[5];
                int node_state = int.Parse(arg_detail[6]);

                ViewState["URQID_Detail"] = IDX;
                ViewState["DocCode"] = Doccode;
                ViewState["SysIDX"] = SysIDX;
                ViewState["EmpIDX_Create"] = EmpIDX;
                ViewState["NCEmpIDX"] = NCEmpIDX;
                ViewState["node_state"] = node_state;

                gridviewindex.Visible = false;
                BoxSearch.Visible = false;
                div_showdata.Visible = true;
                panel_status.Visible = false;
                div_res.Visible = false;

                Select_DetailList();

                Select_GvComment_List();
                Select_CloseJobList();
                setFormData();

                Control div_tranandtcode = (Control)FvDetailRepair.FindControl("div_tranandtcode");
                Control divRemote = (Control)FvDetailRepair.FindControl("divRemote");
                Control divpriority = (Control)getjob_sap.FindControl("divpriority");
                Control prioritysap = (Control)FvDetailRepair.FindControl("prioritysap");
                Control divstatus_comment = (Control)Div_Comment.FindControl("divstatus_comment");
                Control divdevices_res = (Control)FvDetailRepair.FindControl("divdevices_res");
                Control id_holderit = (Control)FvDetailRepair.FindControl("id_holderit");

                Label lbltcode1 = (Label)FvDetailRepair.FindControl("lbltcode");

                if (ViewState["SysIDX"].ToString() == "2" || ViewState["SysIDX"].ToString() == "22" || ViewState["SysIDX"].ToString() == "23")
                {
                    Select_EditPriority();
                    Select_EditPriority_Close();
                    div_tranandtcode.Visible = true;
                    divpriority.Visible = true;
                    divRemote.Visible = true;
                    id_holderit.Visible = false;
                    prioritysap.Visible = true;
                    divdevices_res.Visible = false;

                    if (ViewState["SysIDX"].ToString() == "22" || ViewState["SysIDX"].ToString() == "23")
                    {
                        lbltcode1.Text = "Application/Report : ";
                    }
                    else
                    {
                        lbltcode1.Text = "T-Code/หน้าจอใช้งาน : ";
                        divstatus_comment.Visible = true;

                        ddlstatus_comment.AppendDataBoundItems = true;
                        ddlstatus_comment.Items.Clear();
                        if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                        {
                            ddlstatus_comment.Items.Add(new ListItem("รอ User Comment ตอบกลับ", "47"));
                        }
                        else
                        {
                            ddlstatus_comment.Items.Add(new ListItem("กำลังดำเนินการ", "2"));
                            divstatus_comment.Visible = false;

                        }

                    }

                }
                else if (ViewState["SysIDX"].ToString() == "1")
                {
                    divRemote.Visible = false;
                    prioritysap.Visible = false;
                    divstatus_comment.Visible = false;
                    divdevices_res.Visible = false;


                }
                else if (ViewState["SysIDX"].ToString() == "28")
                {
                    divRemote.Visible = false;
                    prioritysap.Visible = true;
                    div_tranandtcode.Visible = false;
                    divstatus_comment.Visible = false;
                    divdevices_res.Visible = true;
                    Select_EditPriority();
                    Select_EditPriority_Close();
                    SetEnRES(ddlen);
                }
                else
                {
                    div_tranandtcode.Visible = false;
                    divRemote.Visible = true;
                    id_holderit.Visible = true;
                    prioritysap.Visible = false;
                    divstatus_comment.Visible = false;
                    divdevices_res.Visible = false;

                }



                Control divothername = (Control)FvDetailRepair.FindControl("divothername");

                if (ViewState["EmpIDX_Create"].ToString() == ViewState["NCEmpIDX"].ToString())
                {
                    divothername.Visible = false;
                }
                else

                {
                    divothername.Visible = true;
                }


                if (ViewState["EmpIDX_Create"].ToString() == ViewState["EmpIDX"].ToString() || (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21") ||
                    ViewState["SysIDX"].ToString() == "28" && ViewState["Pos_idx"].ToString() == "13")
                {
                    Div_Comment.Visible = true;
                }
                else
                {
                    Div_Comment.Visible = false;
                }



                try
                {

                    string getPathLotus = ConfigurationSettings.AppSettings["PathFile_SAP"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["DocCode"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus, ViewState["DocCode"].ToString());

                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + myDirLotus + "');", true);



                }
                catch
                {

                }

                break;

            case "ITRepairQuestion":

                MvMaster.SetActiveView(ViewQuestion);
                Select_GvQuestionIT_List();


                break;



            case "CmdSaveComment":

                FileUpload UploadImagesComment = (FileUpload)ViewIndex.FindControl("UploadImagesComment");
                string nameupload;

                if (txtcomment.Text == "" || txtcomment.Text == null)
                {

                }
                else if (txtcomment.Text != "" && check == "1")
                {
                    if (ViewState["SysIDX"].ToString() == "2")
                    {
                        getjob_sap.Visible = false;
                        Update_SapGetJob_Comment();
                        getjob_sap.Visible = false;
                        AdminAcceptCloseJob.Visible = false;

                    }

                    Insert_Comment();
                    txtcomment.Text = String.Empty;
                }

                if (ViewState["rdept_idx"].ToString() == deptidx && ViewState["SysIDX"].ToString() != "28" ||
                    ViewState["Pos_idx"].ToString() == "13" && ViewState["SysIDX"].ToString() == "28")
                {
                    nameupload = "Admin-";
                }
                else
                {
                    nameupload = "User-";
                }
                string getPathSAP_comment = ConfigurationSettings.AppSettings["PathFile_SAP"];
                string filePathSAP = Server.MapPath(getPathSAP_comment + ViewState["DocCode"].ToString());
                DirectoryInfo dir = new DirectoryInfo(filePathSAP);
                HttpFileCollection hfc = Request.Files;

                SearchDirectories(dir, ViewState["DocCode"].ToString());




                if (checkfile != "11")
                {
                    hfc = Request.Files;
                    int ocount = dir.GetFiles().Length; //นับจำนวน file ใน folder

                    //upload files
                    if (hfc.Count > 0)
                    {
                        for (int i = ocount; i < (hfc.Count + ocount); i++)
                        {
                            HttpPostedFile hpfLo_comment = hfc[i - ocount];
                            if (hpfLo_comment.ContentLength > 1)
                            {
                                string getPath = ConfigurationSettings.AppSettings["PathFile_SAP"];
                                string RECode1 = ViewState["DocCode"].ToString();
                                string fileName1 = nameupload + RECode1 + i;
                                string filePath1 = Server.MapPath(getPath + RECode1);
                                if (!Directory.Exists(filePath1))
                                {
                                    Directory.CreateDirectory(filePath1);
                                }
                                string extension = Path.GetExtension(hpfLo_comment.FileName);

                                hpfLo_comment.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                            }
                        }
                        Insert_File();
                    }
                }
                else
                {
                    hfc = Request.Files;
                    if (hfc.Count > 0)
                    {
                        for (int ii = 0; ii < hfc.Count; ii++)
                        {
                            HttpPostedFile hpfLo = hfc[ii];
                            if (hpfLo.ContentLength > 1)
                            {
                                string getPath = ConfigurationSettings.AppSettings["PathFile_SAP"];
                                string RECode1 = ViewState["DocCode"].ToString();
                                string fileName1 = nameupload + RECode1 + ii;
                                string filePath1 = Server.MapPath(getPath + RECode1);
                                if (!Directory.Exists(filePath1))
                                {
                                    Directory.CreateDirectory(filePath1);
                                }
                                string extension = Path.GetExtension(hpfLo.FileName);

                                hpfLo.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                            }
                        }
                        //text.Text = "hasfile";
                        Insert_File();
                    }
                }

                SearchDirectories(dir, ViewState["DocCode"].ToString());

                Select_GvComment_List();




                break;



            case "CmdSapGetJob":

                Label Lbldetailuser = (Label)FvDetailRepair.FindControl("Lbldetailuser");
                Label DocCodeSap_update = (Label)FvDetailRepair.FindControl("DocCodeSap");
                Label lblemail = (Label)FvDetailUserRepair.FindControl("lblemail");



                if (rdoaccept.Checked)
                {
                    switch (ViewState["SysIDX"].ToString())
                    {

                        case "1":

                            Update_GMGetJob();


                            break;

                        case "2":
                        case "22":
                        case "23":
                            if (ViewState["m0_actor"].ToString() == "1" && ViewState["m0_status"].ToString() == "47" && ViewState["m0_node"].ToString() == "13")
                            {
                                UpdateSAPCloseJob_Comment();
                            }
                            else
                            {
                                Update_SapGetJob();
                            }
                            break;

                        case "3":

                            Update_ITGetJob();


                            break;

                        case "20":

                            Update_POSGetJob();

                            break;

                        case "28":
                            Update_REGetJob();

                            if (ViewState["rdept_idx"].ToString() == posidxres)
                            {
                                nameupload = "Admin-";
                            }
                            else
                            {
                                nameupload = "User-";
                            }
                            string getPathSAP_closejob = ConfigurationSettings.AppSettings["PathFile_SAP"];
                            string filePathSAP_closejob = Server.MapPath(getPathSAP_closejob + ViewState["DocCode"].ToString());
                            DirectoryInfo dir_closejob = new DirectoryInfo(filePathSAP_closejob);
                            HttpFileCollection hfc_closejob = Request.Files;

                            SearchDirectories(dir_closejob, ViewState["DocCode"].ToString());




                            if (checkfile != "11")
                            {
                                hfc = Request.Files;
                                int ocount = dir_closejob.GetFiles().Length; //นับจำนวน file ใน folder

                                //upload files
                                if (hfc.Count > 0)
                                {
                                    for (int i = ocount; i < (hfc.Count + ocount); i++)
                                    {
                                        HttpPostedFile hpfLo_comment = hfc[i - ocount];
                                        if (hpfLo_comment.ContentLength > 1)
                                        {
                                            string getPath = ConfigurationSettings.AppSettings["PathFile_SAP"];
                                            string RECode1 = ViewState["DocCode"].ToString();
                                            string fileName1 = nameupload + RECode1 + i;
                                            string filePath1 = Server.MapPath(getPath + RECode1);
                                            if (!Directory.Exists(filePath1))
                                            {
                                                Directory.CreateDirectory(filePath1);
                                            }
                                            string extension = Path.GetExtension(hpfLo_comment.FileName);

                                            hpfLo_comment.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                                        }
                                    }
                                    Insert_File();
                                }
                            }
                            else
                            {
                                hfc = Request.Files;
                                if (hfc.Count > 0)
                                {
                                    for (int ii = 0; ii < hfc.Count; ii++)
                                    {
                                        HttpPostedFile hpfLo = hfc[ii];
                                        if (hpfLo.ContentLength > 1)
                                        {
                                            string getPath = ConfigurationSettings.AppSettings["PathFile_SAP"];
                                            string RECode1 = ViewState["DocCode"].ToString();
                                            string fileName1 = nameupload + RECode1 + ii;
                                            string filePath1 = Server.MapPath(getPath + RECode1);
                                            if (!Directory.Exists(filePath1))
                                            {
                                                Directory.CreateDirectory(filePath1);
                                            }
                                            string extension = Path.GetExtension(hpfLo.FileName);

                                            hpfLo.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                                        }
                                    }
                                    //text.Text = "hasfile";
                                    Insert_File();
                                }
                            }

                            SearchDirectories(dir_closejob, ViewState["DocCode"].ToString());
                            break;
                    }
                }
                else if (rdochange.Checked)
                {
                    Update_ChangeSystem();
                }

                if (ViewState["rtcode_mail"].ToString() != "1")
                {

                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขั้นตอนนี้ได้ถูกดำเนินการแล้ว ไม่สามารถดำเนินการซ้ำได้');", true);

                }




                break;

            case "CmdCloseJob":

                switch (ViewState["SysIDX"].ToString())
                {

                    case "1":

                        GM_CloseJob();
                        break;

                    case "2":

                        SAP_CloseJob();
                        break;

                    case "3":

                        IT_CloseJob();

                        break;

                    case "20":
                        POS_CloseJob();

                        break;

                    case "28":
                        RES_CloseJob();
                        break;
                }


                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;


            case "CmdInsertQuestion":

                Insert_GvQuestionIT_List();


                break;

            case "BtnHideSETBoxAllSearchShow":

                SETBoxAllSearch.Visible = true;
                BoxButtonSearchShow.Visible = false;
                BoxButtonSearchHide.Visible = true;
                break;

            case "BtnHideSETBoxAllSearchHide":

                SETBoxAllSearch.Visible = false;
                BoxButtonSearchShow.Visible = true;
                BoxButtonSearchHide.Visible = false;

                break;
        }

    }


    #endregion

    //protected void Link_Click(object sender, EventArgs e)
    //{
    //    string Url = "itrepair";
    //    Response.Write("<script language='javascript'>window.open('" + Url + "','_blank','');");
    //    Response.Write("</script>");
    //}
}