﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ecom_m0_channel.aspx.cs" Inherits="websystem_MasterData_ecom_m0_channel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litdebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvSystem" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">

        


            <div class="form-group">
                <asp:LinkButton ID="btn_addplace" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่ม Channel" data-toggle="tooltip" title="เพิ่ม Channel" runat="server" CommandName="cmdAddUnit" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่ม Channel</asp:LinkButton>

                <div class="clearfix"></div>
            </div>
            <asp:FormView ID="FvInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class=" panel panel-info">
                        <div class=" panel-heading">
                            <h3 class="panel-title">เพิ่ม Channel
                            </h3>
                        </div>
                        <div class=" panel-body">

                            <div class=" form-horizontal" style="padding: 20px 30px;">
                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="ชื่อ Channel"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txtchannel_name" runat="server" CssClass="form-control" placeholder="ตัวอย่าง เช่น Shoppe lazada JD เป็นต้น" Enabled="true" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ControlToValidate="txtchannel_name" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกรายละเอียดโปรโมชั่น" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="220" />
                                    </div>
                                </div>

                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="Customer No"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txtcustomer_no" runat="server" CssClass="form-control" placeholder="Customer No ..." Enabled="true" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"
                                            ControlToValidate="txtcustomer_no" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอก Customer No" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="220" />
                                    </div>
                                </div>

                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="Order Type"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txtorder_type" runat="server" CssClass="form-control" placeholder="Order Type ..." Enabled="true" Text="1201" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                            ControlToValidate="txtorder_type" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอก Order Type" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                    </div>
                                </div>

                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="Sale Org."></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txtsale_org" runat="server" CssClass="form-control" placeholder="Sale Org. ..." Enabled="true" Text="1000" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                            ControlToValidate="txtsale_org" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอก Sale Org." ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="220" />
                                    </div>
                                </div>



                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="Channel"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txtchannel" runat="server" CssClass="form-control" placeholder="Channel ..." Enabled="true" Text="14" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                            ControlToValidate="txtchannel" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอก Channel" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="220" />
                                    </div>
                                </div>

                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="Division"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txtdivision" runat="server" CssClass="form-control" placeholder="Division ..." Enabled="true" Text="00" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                                            ControlToValidate="txtdivision" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอก Division" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="220" />
                                    </div>
                                </div>

                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="Sales Office"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txtsales_office" runat="server" CssClass="form-control" placeholder="Sales Office ..." Enabled="true" Text="1100" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                                            ControlToValidate="txtsales_office" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอก Sales Office" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="220" />
                                    </div>
                                </div>


                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="Sales Group"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txtsales_group" runat="server" CssClass="form-control" placeholder="Sales Group ..." Enabled="true" Text="007" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                                            ControlToValidate="txtsales_group" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอก Sales Group" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="220" />
                                    </div>
                                </div>

                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="สถานะ"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <asp:DropDownList ID="ddPlace" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-offset-3 col-sm-9">
                                        <asp:LinkButton runat="server" OnCommand="btnCommand" CommandName="CmdSave" CssClass="btn btn-success" data-toggle="tooltip" title="บันทึก" Text="Save" ValidationGroup="Save"></asp:LinkButton>
                                        <asp:LinkButton runat="server" OnCommand="btnCommand" CommandName="CmdCancel" CssClass="btn btn-danger" data-toggle="tooltip" title="ยกเลิก" Text="Cancel"></asp:LinkButton>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <div id="div_searchmaster_page" runat="server">

                <div class=" panel panel-info">
                    <div class=" panel-heading">
                        <h3 class="panel-title">ค้นหา
                        </h3>
                    </div>
                    <div class=" panel-body">


                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <asp:Label ID="Label130" runat="server" Text="ชื่อ Channel" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtmaster_search" runat="server" CssClass="form-control" PlaceHolder="........" />

                                </div>
                                <asp:Label ID="Label131" runat="server" Text="สถานะ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="Src_status" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="999" Text="ALL" />
                                        <asp:ListItem Value="1" Text="Online" />
                                        <asp:ListItem Value="0" Text="Offline" />
                                    </asp:DropDownList>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <asp:LinkButton ID="LinkButton25" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSearchMaster_src" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i>&nbsp;ค้นหา</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton26" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Supplier" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <asp:GridView ID="Gv_select_place" runat="server" Visible="true" AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                OnRowEditing="Master_RowEditing"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                PageSize="20"
                AutoPostBack="FALSE">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                </EmptyDataTemplate>


                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="place_idx" runat="server" Visible="false" Text='<%# Eval("channel_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_channel_idx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("channel_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class=" form-horizontal" style="padding: 20px 30px;">
                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-3  control-label">
                                                <asp:Label runat="server" Text="ชื่อ Channel"></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6 ">
                                                <asp:TextBox ID="Name_channel_name" runat="server" CssClass="form-control" placeholder="ตัวอย่าง เช่น Shoppe lazada JD เป็นต้น" Enabled="true" Text='<%# Eval("channel_name") %>' />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                    ControlToValidate="Name_channel_name" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกรายละเอียดโปรโมชั่น" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="220" />
                                            </div>
                                        </div>

                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-3  control-label">
                                                <asp:Label runat="server" Text="Customer No"></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6 ">
                                                <asp:TextBox ID="Name_customer_no" runat="server" CssClass="form-control" placeholder="Customer No ..." Enabled="true" Text='<%# Eval("m0_customer_no") %>' />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"
                                                    ControlToValidate="Name_customer_no" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก Customer No" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="220" />
                                            </div>
                                        </div>

                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-3  control-label">
                                                <asp:Label runat="server" Text="Order Type"></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6 ">
                                                <asp:TextBox ID="Name_order_type" runat="server" CssClass="form-control" placeholder="Order Type ..." Enabled="true" Text='<%# Eval("m0_order_type") %>' />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                    ControlToValidate="Name_order_type" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก Order Type" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                            </div>
                                        </div>

                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-3  control-label">
                                                <asp:Label runat="server" Text="Sale Org."></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6 ">
                                                <asp:TextBox ID="Name_sale_org" runat="server" CssClass="form-control" placeholder="Sale Org. ..." Enabled="true" Text='<%# Eval("m0_sale_org") %>' />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                                    ControlToValidate="Name_sale_org" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก Sale Org." ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="220" />
                                            </div>
                                        </div>



                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-3  control-label">
                                                <asp:Label runat="server" Text="Channel"></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6 ">
                                                <asp:TextBox ID="Name_channel" runat="server" CssClass="form-control" placeholder="Channel ..." Enabled="true" Text='<%# Eval("m0_channel") %>' />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                                    ControlToValidate="Name_channel" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก Channel" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="220" />
                                            </div>
                                        </div>

                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-3  control-label">
                                                <asp:Label runat="server" Text="Division"></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6 ">
                                                <asp:TextBox ID="Name_division" runat="server" CssClass="form-control" placeholder="Division ..." Enabled="true" Text='<%# Eval("m0_division") %>' />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                                                    ControlToValidate="Name_division" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก Division" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="220" />
                                            </div>
                                        </div>

                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-3  control-label">
                                                <asp:Label runat="server" Text="Sales Office"></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6 ">
                                                <asp:TextBox ID="Name_sales_office" runat="server" CssClass="form-control" placeholder="Sales Office ..." Enabled="true" Text='<%# Eval("m0_sales_office") %>' />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                                                    ControlToValidate="Name_sales_office" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก Sales Office" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="220" />
                                            </div>
                                        </div>


                                        <div class=" form-group">
                                            <div class="col-xs-12 col-sm-3  control-label">
                                                <asp:Label runat="server" Text="Sales Group"></asp:Label>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6 ">
                                                <asp:TextBox ID="Name_sales_group" runat="server" CssClass="form-control" placeholder="Sales Group ..." Enabled="true" Text='<%# Eval("m0_sales_group") %>' />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                                                    ControlToValidate="Name_sales_group" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก Sales Group" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="220" />
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="lbupdates_place" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddEdit_place" Text='<%# Eval("channel_status") %>'
                                                    CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                    <asp:ListItem Value="0">Offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-9">
                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editpname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" CommandName="Update"></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อ Channel" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("channel_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Customer No" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("m0_customer_no") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Order Type" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("m0_order_type") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Sale Org" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("m0_sale_org") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Channel" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("m0_channel") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Division" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("m0_division") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Sales Office" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("m0_sales_office") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Sales Group" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("m0_sales_group") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>




                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <div style="padding: 5px 10px 0px; text-align: left;">
                                    <asp:Label ID="lbpplace_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("channel_status") %>'></asp:Label>
                                    <asp:Label ID="place_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style=" color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="place_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style=" color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style=" text-align: center;">
                                <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="btnTodelete"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("channel_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>

    </asp:MultiView>


</asp:Content>
