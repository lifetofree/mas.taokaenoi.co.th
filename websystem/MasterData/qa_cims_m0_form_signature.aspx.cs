﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_cims_m0_form_signature : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();
    data_employee _dataEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    static string _urlCimsGetFormCalddlName = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetFormCalddlName"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //setActiveView("docManageFormCalM0", 0, "");
        }
    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        //default
        ////btnAddFormCal.Visible = true;

        switch (cmdName)
        {
            case "cmdAddSignature":
                setFormData(fvInsertSignature, FormViewMode.Insert, null);
                btnAddSignature.Visible = false;

                getFormcalList((DropDownList)fvInsertSignature.FindControl("ddl_formcalculate"));



                ////gvFormList.EditIndex = -1;
                //getFormList();
                break;

            case "cmdAddTableResult":
                ////setFormData(fvFormTableResult, FormViewMode.Insert, null);
                ////btnAddTableResult.Visible = false;
                ////var txtFormName = (TextBox)fvFormTableResult.FindControl("txtFormName");
                ////txtFormName.Text = name.Text;

                break;

            case "cmdCancel":
                //setActiveView("docManageFormCalM0", 0, name.Text);
                break;

            case "cmdCancelTable":
                //setActiveView("docManageFormCalM1", int.Parse(ViewState["vs_m0_formcal_idx"].ToString()), name.Text);
                break;

            case "cmdView":

                string[] arg1 = new string[2];
                arg1 = e.CommandArgument.ToString().Split(',');
                int _view_formcal_idx = int.Parse((arg1[0].ToString()));
                string _name = (arg1[1]);

                ViewState["vs_m0_formcal_idx"] = _view_formcal_idx.ToString();

                setActiveView("docManageFormCalM1", int.Parse(ViewState["vs_m0_formcal_idx"].ToString()), _name);
                //name.Text = _name.ToString();
                break;

            case "cmdBack":
                setActiveView("docManageFormCalM0", 0, "");
                break;

            case "cmdDelete":
                int _m0_formcal_idx = int.Parse(cmdArg);

                data_qa_cims _data_cims_form_del = new data_qa_cims();
                _data_cims_form_del.qa_cims_m0_form_calculate_list = new qa_cims_m0_form_calculate_details[1];
                qa_cims_m0_form_calculate_details _del_formcal = new qa_cims_m0_form_calculate_details();

                _del_formcal.m0_formcal_idx = (_m0_formcal_idx);
                _del_formcal.m0_form_status = 9;

                _data_cims_form_del.qa_cims_m0_form_calculate_list[0] = _del_formcal;

                //_data_cims_form_del = callServicePostMasterQACIMS(_urlCimsSetFormCalName, _data_cims_form_del);

                if (_data_cims_form_del.return_code == 0)
                {
                    getFormList();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินรายการไม่สำเร็จ กรุณาลองใหม่อีกครั้ง');", true);
                }

                break;

            case "cmdSave":

                var ddl_formcalculate = (DropDownList)fvInsertSignature.FindControl("ddl_formcalculate");
                var txtsignature_name = (TextBox)fvInsertSignature.FindControl("txtsignature_name");
                var txtposition_signature = (TextBox)fvInsertSignature.FindControl("txtposition_signature");
                var txtdatetime_signature = (TextBox)fvInsertSignature.FindControl("txtdatetime_signature");
                var txtselect_detail = (TextBox)fvInsertSignature.FindControl("txtselect_detail");
                var ddlformcal_status = (DropDownList)fvInsertSignature.FindControl("ddlformcal_status");
               
                data_qa_cims _data_cims_form = new data_qa_cims();
                _data_cims_form.qa_cims_m0_form_calculate_list = new qa_cims_m0_form_calculate_details[1];
                qa_cims_m0_form_calculate_details _formcal = new qa_cims_m0_form_calculate_details();

                _formcal.cemp_idx = _emp_idx;
                _formcal.m0_formcal_idx = int.Parse(ddl_formcalculate.SelectedValue);
                _formcal.m2_signature_name = txtsignature_name.Text;
                _formcal.m2_position_signature = txtposition_signature.Text;
                _formcal.m2_datetime_signature = txtdatetime_signature.Text;
                _formcal.m2_select_detail = txtselect_detail.Text;
                _formcal.m2_formcal_status = int.Parse(ddlformcal_status.SelectedValue);
 
                _data_cims_form.qa_cims_m0_form_calculate_list[0] = _formcal;

                //_data_cims_form = callServicePostMasterQACIMS(_urlCimsSetFormSignature, _data_cims_form);

                btnAddSignature.Visible = false;

                //if (_data_cims_form.return_code == 0)
                //{
                //    getFormList();
                //    setFormData(fvFormName, FormViewMode.Insert, null);
                //}
                //else
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                //}

                break;

            case "cmdSaveTableResult":

                //var txt_form_table_name = (TextBox)fvFormTableResult.FindControl("txt_form_table_name");
                //var txt_table_decription = (TextBox)fvFormTableResult.FindControl("txt_table_decription");
                //var ddl_status_table_form_name = (DropDownList)fvFormTableResult.FindControl("ddl_status_form_table_name");

                //data_qa_cims _data_cims_form_table = new data_qa_cims();
                //_data_cims_form_table.qa_cims_m1_form_calculate_list = new qa_cims_m1_form_calculate_details[1];
                //qa_cims_m1_form_calculate_details _formcal_table = new qa_cims_m1_form_calculate_details();

                //_formcal_table.cemp_idx = _emp_idx;
                //_formcal_table.table_result_name = txt_form_table_name.Text;
                //_formcal_table.table_decription = txt_table_decription.Text;
                //_formcal_table.status = int.Parse(ddl_status_table_form_name.SelectedValue);
                //_formcal_table.m0_formcal_idx = int.Parse(ViewState["vs_m0_formcal_idx"].ToString());

                //_data_cims_form_table.qa_cims_m1_form_calculate_list[0] = _formcal_table;

                ////litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cims_form_table));

                //_data_cims_form_table = callServicePostMasterQACIMS(_urlCimsSetFormTableResult, _data_cims_form_table);

                //if (_data_cims_form_table.return_code == 0)
                //{
                //    getFormTableList(int.Parse(ViewState["vs_m0_formcal_idx"].ToString()), "");
                //    txt_table_decription.Text = "";
                //    txt_form_table_name.Text = "";
                //}
                //else
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                //}

                break;

            case "cmdDeleteTable":

                int _m1_formcal_table_idx = int.Parse(cmdArg);

                //data_qa_cims _data_cims_form_table_del = new data_qa_cims();
                //_data_cims_form_table_del.qa_cims_m1_form_calculate_list = new qa_cims_m1_form_calculate_details[1];
                //qa_cims_m1_form_calculate_details _del_formcal_form = new qa_cims_m1_form_calculate_details();

                //_del_formcal_form.m1_formcal_idx = (_m1_formcal_table_idx);
                //_del_formcal_form.status = 9;

                //_data_cims_form_table_del.qa_cims_m1_form_calculate_list[0] = _del_formcal_form;

                //_data_cims_form_table_del = callServicePostMasterQACIMS(_urlCimsSetFormTableResult, _data_cims_form_table_del);

                //if (_data_cims_form_table_del.return_code == 0)
                //{
                //    getFormTableList(int.Parse(ViewState["vs_m0_formcal_idx"].ToString()), "");
                //}
                //else
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินรายการไม่สำเร็จ กรุณาลองใหม่อีกครั้ง');", true);
                //}

                break;
        }
    }

    #endregion

    #region setgriddata

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void gvRowDeleted(object sender, GridViewDeleteEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvTableResultList":

                //GridView _DelgvTableResultList = (GridView)fvFormTableResult.FindControl("gvTableResultList");
                //var _pnbtnSave = (Panel)fvFormTableResult.FindControl("pnbtnSave");

                //var DeleteFormList = (DataSet)ViewState["vsDataTableFormList"];
                //var drFormList = DeleteFormList.Tables[0].Rows;

                //drFormList.RemoveAt(e.RowIndex);

                //ViewState["vsDataTableFormList"] = DeleteFormList;
                //_DelgvTableResultList.EditIndex = -1;

                //setGridData(_DelgvTableResultList, ViewState["vsDataTableFormList"]);

                //if (_DelgvTableResultList.Rows.Count == 0)
                //{
                //    _pnbtnSave.Visible = false;
                //}
                //else
                //{
                //    _pnbtnSave.Visible = true;
                //}
                break;
        }
    }

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            //case "gvFormList":
            //    gvFormList.PageIndex = e.NewPageIndex;
            //    getFormList();
            //    break;

            //case "gvTableResult":
            //    gvFormList.PageIndex = e.NewPageIndex;
            //    getFormTableList(int.Parse(ViewState["vs_m0_formcal_idx"].ToString()), "");
            //    break;
        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvFormList":

                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    Label lbformcal_status = (Label)e.Row.Cells[3].FindControl("lbformcal_status");
                //    Label lbformcal_statusOnline = (Label)e.Row.Cells[3].FindControl("lbformcal_statusOnline");
                //    Label lbformcal_statusOffline = (Label)e.Row.Cells[3].FindControl("lbformcal_statusOffline");
                //    Label lbformcal_name_note_detail = (Label)e.Row.Cells[2].FindControl("lbformcal_name_note_detail");
                //    Label lbformcal_name_note_detail_show = (Label)e.Row.Cells[2].FindControl("lbformcal_name_note_detail_show");

                //    lbformcal_name_note_detail_show.Text = HttpUtility.HtmlDecode(lbformcal_name_note_detail.Text.ToString());

                //    ViewState["_formcal_status"] = lbformcal_status.Text;

                //    if (ViewState["_formcal_status"].ToString() == "1")
                //    {
                //        lbformcal_statusOnline.Visible = true;
                //    }
                //    else if (ViewState["_formcal_status"].ToString() == "0")
                //    {
                //        lbformcal_statusOffline.Visible = true;
                //    }
                //}

                //if (e.Row.RowState.ToString().Contains("Edit"))
                //{
                //    GridView editGrid = sender as GridView;
                //    int colSpan = editGrid.Columns.Count;
                //    for (int i = 1; i < colSpan; i++)
                //    {
                //        e.Row.Cells[i].Visible = false;
                //        e.Row.Cells[i].Controls.Clear();
                //    }

                //    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                //    e.Row.Cells[0].CssClass = "";

                //    Label notedetails = (Label)e.Row.Cells[2].FindControl("notedetails");
                //    TextBox txt_update_note_detail = (TextBox)e.Row.Cells[0].FindControl("txt_update_note_detail");

                //    txt_update_note_detail.Text = HttpUtility.HtmlDecode(notedetails.Text.ToString());

                //    setFormData(fvFormName, FormViewMode.ReadOnly, null);
                //}
                break;

            case "gvTableResult":

                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    Label lbformcal_table_status = (Label)e.Row.Cells[3].FindControl("lbformcal_table_status");
                //    Label lbformcal_table_statusOnline = (Label)e.Row.Cells[3].FindControl("lbformcal_table_statusOnline");
                //    Label lbformcal_table_statusOffline = (Label)e.Row.Cells[3].FindControl("lbformcal_table_statusOffline");

                //    ViewState["_formcal_table_status"] = lbformcal_table_status.Text;

                //    if (ViewState["_formcal_table_status"].ToString() == "1")
                //    {
                //        lbformcal_table_statusOnline.Visible = true;
                //    }
                //    else if (ViewState["_formcal_table_status"].ToString() == "0")
                //    {
                //        lbformcal_table_statusOffline.Visible = true;
                //    }
                //}

                //if (e.Row.RowState.ToString().Contains("Edit"))
                //{
                //    GridView editGrid = sender as GridView;
                //    int colSpan = editGrid.Columns.Count;
                //    for (int i = 1; i < colSpan; i++)
                //    {
                //        e.Row.Cells[i].Visible = false;
                //        e.Row.Cells[i].Controls.Clear();
                //    }

                //    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                //    e.Row.Cells[0].CssClass = "";

                //    setFormData(fvFormTableResult, FormViewMode.ReadOnly, null);
                //    TextBox txt_formcal_for_update_name = (TextBox)e.Row.Cells[0].FindControl("txt_formcal_for_update_name");
                //    txt_formcal_for_update_name.Text = name.Text;
                //}
                break;
        }
    }

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvFormList":
                //var _update_m0_formcal_idx = (TextBox)gvFormList.Rows[e.RowIndex].FindControl("txt_m0_formcal_idx");
                //var _update_m0_formcal_name = (TextBox)gvFormList.Rows[e.RowIndex].FindControl("txt_update_formcal_name");
                //var _update_update_note_detail = (TextBox)gvFormList.Rows[e.RowIndex].FindControl("txt_update_note_detail");
                //var _update_m0_formcal_status = (DropDownList)gvFormList.Rows[e.RowIndex].FindControl("ddlUpdateStatus");

                //gvFormList.EditIndex = -1;

                //data_qa_cims _data_update_formcal = new data_qa_cims();
                //_data_update_formcal.qa_cims_m0_form_calculate_list = new qa_cims_m0_form_calculate_details[1];
                //qa_cims_m0_form_calculate_details _update_form_calculate = new qa_cims_m0_form_calculate_details();

                //_update_form_calculate.m0_formcal_idx = int.Parse(_update_m0_formcal_idx.Text);
                //_update_form_calculate.m0_formcal_name = _update_m0_formcal_name.Text;
                //_update_form_calculate.note_detail = _update_update_note_detail.Text;
                //_update_form_calculate.m0_form_status = int.Parse(_update_m0_formcal_status.SelectedValue);

                //_data_update_formcal.qa_cims_m0_form_calculate_list[0] = _update_form_calculate;

                //_data_update_formcal = callServicePostMasterQACIMS(_urlCimsSetFormCalName, _data_update_formcal);

                //if (_data_update_formcal.return_code == 1)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                //    gvFormList.EditIndex = -1;
                //    getFormList();
                //}
                //else
                //{
                //    getFormList();
                //    setFocus.Focus();
                //}

                break;

            case "gvTableResult":

                //var _update_m1_formcal_table_idx = (TextBox)gvTableResult.Rows[e.RowIndex].FindControl("txt_m1_formcal_table_idx");
                //var _update_m1_formcal_table_name = (TextBox)gvTableResult.Rows[e.RowIndex].FindControl("txt_update_formcal_table_name");
                //var _update_m1_formcal_table_status = (DropDownList)gvTableResult.Rows[e.RowIndex].FindControl("ddlUpdateTableStatus");
                //var _update_m1_formcal_table_decriptionedit = (TextBox)gvTableResult.Rows[e.RowIndex].FindControl("txt_table_decriptionedit");

                //gvTableResult.EditIndex = -1;

                //data_qa_cims _data_update_formcal_table = new data_qa_cims();
                //_data_update_formcal_table.qa_cims_m1_form_calculate_list = new qa_cims_m1_form_calculate_details[1];
                //qa_cims_m1_form_calculate_details _update_form_calculate_table = new qa_cims_m1_form_calculate_details();

                //_update_form_calculate_table.m1_formcal_idx = int.Parse(_update_m1_formcal_table_idx.Text);
                //_update_form_calculate_table.table_result_name = _update_m1_formcal_table_name.Text;
                //_update_form_calculate_table.status = int.Parse(_update_m1_formcal_table_status.SelectedValue);
                //_update_form_calculate_table.table_decription = _update_m1_formcal_table_decriptionedit.Text;

                //_data_update_formcal_table.qa_cims_m1_form_calculate_list[0] = _update_form_calculate_table;

                //_data_update_formcal_table = callServicePostMasterQACIMS(_urlCimsSetFormTableResult, _data_update_formcal_table);

                //if (_data_update_formcal_table.return_code == 1)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                //    getFormTableList(int.Parse(ViewState["vs_m0_formcal_idx"].ToString()), "");
                //}
                //else
                //{
                //    getFormTableList(int.Parse(ViewState["vs_m0_formcal_idx"].ToString()), "");
                //    setFocus.Focus();
                //}
                break;
        }
    }

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvFormList":
                // getFormList();
                ///gvFormList.EditIndex = -1;
                getFormList();
                break;

            case "gvTableResult":
                ////gvTableResult.EditIndex = -1;
                getFormTableList(int.Parse(ViewState["vs_m0_formcal_idx"].ToString()), "");
                ///setFocus.Focus();
                break;
        }
    }

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvFormList":
                //gvFormList.EditIndex = e.NewEditIndex;
                getFormList();
                break;

            case "gvTableResult":
                //gvTableResult.EditIndex = e.NewEditIndex;
                getFormTableList(int.Parse(ViewState["vs_m0_formcal_idx"].ToString()), "");
                break;
        }
    }

    #endregion setgriddata

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    #endregion setformdata

    #region getdata
    protected void getFormcalList(DropDownList ddlName)
    {
        data_qa_cims _data_cims_form_list = new data_qa_cims();
        _data_cims_form_list.qa_cims_m0_form_calculate_list = new qa_cims_m0_form_calculate_details[1];
        qa_cims_m0_form_calculate_details _formlist = new qa_cims_m0_form_calculate_details();

        _data_cims_form_list.qa_cims_m0_form_calculate_list[0] = _formlist;

        _data_cims_form_list = callServicePostMasterQACIMS(_urlCimsGetFormCalddlName, _data_cims_form_list);

        setDdlData(ddlName, _data_cims_form_list.qa_cims_m0_form_calculate_list, "m0_formcal_name", "m0_formcal_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฟอร์ม ---", "-1"));
        //setGridData(gvFormList, _data_cims_form_list.qa_cims_m0_form_calculate_list);
    }


    protected void getFormList()
    {
        data_qa_cims _data_cims_form_list = new data_qa_cims();
        _data_cims_form_list.qa_cims_m0_form_calculate_list = new qa_cims_m0_form_calculate_details[1];
        qa_cims_m0_form_calculate_details _formlist = new qa_cims_m0_form_calculate_details();

        _data_cims_form_list.qa_cims_m0_form_calculate_list[0] = _formlist;

        //_data_cims_form_list = callServicePostMasterQACIMS(_urlCimsGetFormCalName, _data_cims_form_list);

        //setGridData(gvFormList, _data_cims_form_list.qa_cims_m0_form_calculate_list);
    }

    protected void getFormTableList(int _m0_formcal_idx, string name)
    {
        data_qa_cims _data_cims_form_table_list = new data_qa_cims();
        _data_cims_form_table_list.qa_cims_m1_form_calculate_list = new qa_cims_m1_form_calculate_details[1];
        qa_cims_m1_form_calculate_details _formtablelist = new qa_cims_m1_form_calculate_details();
        _formtablelist.m0_formcal_idx = int.Parse(_m0_formcal_idx.ToString());
        _data_cims_form_table_list.qa_cims_m1_form_calculate_list[0] = _formtablelist;

        //_data_cims_form_table_list = callServicePostMasterQACIMS(_urlCimsGetFormTableResult, _data_cims_form_table_list);

        //setGridData(gvTableResult, _data_cims_form_table_list.qa_cims_m1_form_calculate_list);
    }
    #endregion getdata

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);

        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    #endregion callService Functions

    #region setActiveView

    protected void setActiveView(string activeTab, int idx, string name)
    {
        //mvMultiview.SetActiveView((View)mvMultiview.FindControl(activeTab));

        //setFormData(fvFormName, FormViewMode.ReadOnly, null);
        //setFormData(fvFormTableResult, FormViewMode.ReadOnly, null);


        switch (activeTab)
        {
            case "docManageFormCalM0":
                getFormList();
                break;

            case "docManageFormCalM1":
                getFormTableList(idx, "");
                //showNameForm.Text = "ฟอร์ม : " + name.ToString();
                //btnAddTableResult.Visible = true;
                break;

        }
    }

    #endregion setActiveView
}