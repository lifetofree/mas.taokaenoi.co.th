﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;

using Point = DotNet.Highcharts.Options.Point;
using System.Drawing;
using ClosedXML.Excel;
using DocumentFormat.OpenXml;


public partial class websystem_hr_hr_report_time_payroll : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    private string ConCL = "conn_centralized";

    //dataODSP_Relation dtODSP = new dataODSP_Relation();
    data_employee _dtEmployee = new data_employee();

    //function_scan _funcScan = new function_scan();
    function_db _funcdb = new function_db();
    data_employee _dataEmployee = new data_employee();
    data_report_iam _dataReport_Iam = new data_report_iam();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlGetEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee"];
    static string urlGetEmployeelist = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployeeList"];
    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    static string _urlGetHospitalOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetHospitalOld"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];
    static string _urlGetList = _serviceUrl + ConfigurationManager.AppSettings["urlGetList"];
    static string _urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetLocation"];


    static string imgPath = ConfigurationSettings.AppSettings["path_flie_employee"];

    int emp_idx = 0;
    int defaultInt = 0;
    decimal tot_actual_all_time = 0;
    decimal tot_actual_time_in = 0;
    decimal tot_actual_time_rate = 0;
    decimal tot_actual_time_out = 0;
    decimal tot_actual_time_leave = 0;

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";
    int _tempInt = 0;
    string temp = "";

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ViewState["RPos_idx_login"] = 0;
        ViewState["Org_idx_login"] = 0;

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + Session["emp_idx"].ToString());
        ViewState["RPos_idx_login"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Org_idx_login"] = _dtEmployee.employee_list[0].org_idx;

        if (
            (int.Parse(ViewState["RPos_idx_login"].ToString()) == 1017 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 1018 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 1019 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5916 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5901 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5899 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5900 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5902 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5884 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5903 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5904 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5905 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5906 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5907 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5909 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5872 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5908 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5873 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5874 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5875 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5876 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5885 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5886 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5888 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5989 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 3543 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 377 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 378 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 867 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5947 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5945 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7135 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7086 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 8150 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7108 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7062 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5891 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5878 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 8153 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5964 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 9182 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 9199 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 4699 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 198
            )
            || int.Parse(ViewState["Org_idx_login"].ToString()) != 1
            )
        {

        }
        else
        {
            _funcTool.showAlert(this, "คุณไม่มีสิทธิ์ใช้งาน");
            Response.Redirect("http://mas.taokaenoi.co.th/");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            select_empIdx_present();
            //Select_Employee_index();
            Menu_Color(4);
            MvMaster.SetActiveView(ViewReport);

        }

        linkBtnTrigger(btnsearch_report);
    }

    #region INSERT&SELECT&UPDATE

    #region SELECT

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void select_empIdx_present()
    {
        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["jobgrade_level"] = _dtEmployee.employee_list[0].jobgrade_level;
        ViewState["return_code"] = 0;
        ViewState["Gv_emp_idx"] = 0;
        ViewState["Box_dataEmployee"] = null;
        ViewState["Box_dataEmployee_Report"] = null;

        ViewState["Box_dataEmployee_Report_0"] = null;
        ViewState["Box_dataEmployee_Report_1"] = null;
        ViewState["Box_dataEmployee_Report_2"] = null;
        ViewState["Box_dataEmployee_Report_3"] = null;
        ViewState["Box_dataEmployee_Report_4"] = null;
        ViewState["Box_dataEmployee_Report_5"] = null;

        ViewState["txtempcode_s"] = "";
        ViewState["txtempname_s"] = "";
        ViewState["txtdatestart"] = "";
        ViewState["ddlorg_rp"] = "";
        ViewState["ddldep_rp"] = "";
        ViewState["ddlsec_rp"] = "";
        ViewState["ddlemptype_s"] = "";
        ViewState["ddllocation"] = "";
        ViewState["empshif_idx"] = 1;

        ViewState["txtempcode_s"] = "";
        ViewState["txtempname_s"] = "";
        ViewState["ddlmonth"] = 0;
        ViewState["ddllocation"] = 0;
        ViewState["ddlorg_rp"] = 0;
        ViewState["ddldep_rp"] = 0;
        ViewState["ddlsec_rp"] = 0;
        ViewState["ddlemptype_s"] = 0;

        ViewState["vsExport_org"] = null;
        DataSet dsOrg = new DataSet();
        dsOrg.Tables.Add("Export_org");
        dsOrg.Tables[0].Columns.Add("org_idx", typeof(int));
        dsOrg.Tables[0].Columns.Add("org_name_th", typeof(String));
        ViewState["vsExport_org"] = dsOrg;

        var dssAdd_edit = (DataSet)ViewState["vsExport_org"];
        var drshiftAdd_edit1 = dssAdd_edit.Tables[0].NewRow();
        drshiftAdd_edit1["org_idx"] = 22;
        drshiftAdd_edit1["org_name_th"] = "ABCD";

        dssAdd_edit.Tables[0].Rows.Add(drshiftAdd_edit1);
        ViewState["vsExport_dept"] = dssAdd_edit;
        //GvEmployee_Gv1.DataSource = ViewState["vsExport_org"];
        //GvEmployee_Gv1.DataBind();


        ViewState["vsExport_dept"] = null;
        DataSet dsDepadd = new DataSet();
        dsDepadd.Tables.Add("Export_dept");
        dsDepadd.Tables[0].Columns.Add("rdept_idx", typeof(int));
        dsDepadd.Tables[0].Columns.Add("dept_name_th", typeof(String));
        ViewState["vsExport_dept"] = dsDepadd;


        var dsshiftAdd_edit = (DataSet)ViewState["vsExport_dept"];
        var drshiftAdd_edit = dsshiftAdd_edit.Tables[0].NewRow();
        drshiftAdd_edit["rdept_idx"] = 1;
        drshiftAdd_edit["dept_name_th"] = "aaaaa";

        dsshiftAdd_edit.Tables[0].Rows.Add(drshiftAdd_edit);
        ViewState["vsExport_dept"] = dsshiftAdd_edit;
        //GvEmployee_Gv2.DataSource = ViewState["vsExport_dept"];
        //GvEmployee_Gv2.DataBind();

        /*ViewState["vsExport_org"] = null;
        var dsPosadd = new DataSet();
        dsPosadd.Tables.Add("Export_org");

        dsPosadd.Tables[0].Columns.Add("org_idx", typeof(int));
        dsPosadd.Tables[0].Columns.Add("org_name_th", typeof(String));
        ViewState["vsExport_org"] = dsPosadd;*/
    }

    protected void Select_Employee_report_search()
    {
        FormView Fv_Search_Emp_Report = (FormView)ViewReport.FindControl("Fv_Search_Emp_Report");
        TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_s");
        TextBox txtempname_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempname_s");
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
        //TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddllocation = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation");
        //DropDownList ddlparttime = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlparttime");

        ViewState["txtempcode_s"] = txtempcode_s.Text;
        ViewState["txtempname_s"] = txtempname_s.Text;
        ViewState["txtdatestart"] = txtdatestart.Text;
        ViewState["ddlorg_rp"] = int.Parse(ddlorg_rp.SelectedValue);
        ViewState["ddldep_rp"] = int.Parse(ddldep_rp.SelectedValue);
        ViewState["ddlsec_rp"] = int.Parse(ddlsec_rp.SelectedValue);
        ViewState["ddlemptype_s"] = int.Parse(ddlemptype_s.SelectedValue);
        ViewState["ddllocation"] = int.Parse(ddllocation.SelectedValue);
        //ViewState["empshif_idx"] = int.Parse(ddlparttime.SelectedValue);

        _dataEmployee.employee_list_report_time = new employee_detail_time[1];
        employee_detail_time _dataEmployee_ = new employee_detail_time();

        _dataEmployee_.emp_code = txtempcode_s.Text;
        _dataEmployee_.emp_name_th = txtempname_s.Text;

        _dataEmployee.employee_list_report_time[0] = _dataEmployee_;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        //_dataEmployee = callServiceReport_Iam(_urlGetList, _dataEmployee);
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        //adf.Text = _dataEmployee.return_code;

        if (_dataEmployee.return_code != "1")
        {
            ViewState["Box_dataEmployee_Report"] = _dataEmployee.employee_list_report_time;
            //setGridData(GvEmployee_Report, _dataEmployee.employee_list_report_time);
        }
        else
        {
            //setGridData(GvEmployee_Report, null);
        }

    }

    protected void Select_late_1()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 1;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        ViewState["GvEmployee_Gv1"] = _dataReport_Iam.u0_report_iam_action;

        try{
            u0_report_iam[] _item_scanin = (u0_report_iam[])ViewState["GvEmployee_Gv1"];
            if (ViewState["GvEmployee_Gv1"] != null)
            {
                var _linq_scanin_emp_code = (from data in _item_scanin
                                             select new
                                             {
                                                 data.emp_code,
                                                 data.pay_date,
                                                 data.money_type,
                                                 data.money,
                                                 data.unit,
                                                 data.capacity,
                                                 data.etc
                                             }
                               ).ToList().Distinct();

                setGridData(GvEmployee_Gv1, _linq_scanin_emp_code.ToList());
            }
        }
        catch (Exception)
        {
            throw;
        }

        //setGridData(GvEmployee_Gv1, _dataReport_Iam.u0_report_iam_action);

    }

    protected void Select_return_2()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 2;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        /*ViewState["GvEmployee_Gv2"] = _dataReport_Iam.u0_report_iam_action;

        u0_report_iam[] _item_scanin = (u0_report_iam[])ViewState["GvEmployee_Gv2"];
        var _linq_scanin_emp_code = (from data in _item_scanin
                                     select new
                                     {
                                         data.emp_code,
                                         data.pay_date,
                                         data.money_type,
                                         data.money,
                                         data.unit,
                                         data.capacity,
                                         data.etc
                                     }
                           ).ToList().Distinct();

        setGridData(GvEmployee_Gv2, _linq_scanin_emp_code.ToList());*/
        //setGridData(GvEmployee_Gv2, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_sick_file_3()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 3;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv3, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_sick_non_file_4()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 4;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv4, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_holiday_5()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 5;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataReport_Iam));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv5, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_birthday_6()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 6;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        //ViewState["birthday_6"] = _dataReport_Iam.u0_report_iam_action;
        setGridData(GvEmployee_Gv6, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_probation_7()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 7;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv7, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_takecare_8()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 8;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv8, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_absence_9()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 9;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv9, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_absence_return_10()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 10;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        
        ViewState["GvEmployee_Gv10"] = _dataReport_Iam.u0_report_iam_action;

        u0_report_iam[] _item_scanin = (u0_report_iam[])ViewState["GvEmployee_Gv10"];
        var _linq_scanin_emp_code = (from data in _item_scanin
                                     select new
                                     {
                                         data.emp_code,
                                         data.pay_date,
                                         data.money_type,
                                         data.money,
                                         data.unit,
                                         data.capacity,
                                         data.etc
                                     }
                           ).ToList().Distinct();

        setGridData(GvEmployee_Gv10, _linq_scanin_emp_code.ToList());
        //setGridData(GvEmployee_Gv10, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_absence_late_11()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 11;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv11, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_absen_all_12()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 12;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv12, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_probation_absence_13()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 13;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv13, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_activity_over_14()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 14;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv14, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_maternity_before_15()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 15;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv15, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_maternity_after_16()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 16;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv16, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_etc_17()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 17;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv17, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_time_18()
    {
        _dataReport_Iam.u0_report_iam_action = new u0_report_iam[1];
        u0_report_iam _dataReportIam = new u0_report_iam();

        _dataReportIam.emp_code = ViewState["txtempcode_s"].ToString();
        _dataReportIam.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataReportIam.month = int.Parse(ViewState["ddlmonth"].ToString());
        _dataReportIam.location_idx = int.Parse(ViewState["ddllocation"].ToString());
        _dataReportIam.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataReportIam.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataReportIam.resec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataReportIam.emptype_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        _dataReportIam.type_action = 18;

        _dataReport_Iam.u0_report_iam_action[0] = _dataReportIam;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataReport_Iam = callServiceReport_Iam(_urlGetList, _dataReport_Iam);
        setGridData(GvEmployee_Gv18, _dataReport_Iam.u0_report_iam_action);
    }

    protected void Select_Coscenter(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือก CostCenter...", "0"));

        _dataEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        _dataEmployee.CostCenterDetail[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetCostcenterOld, _dataEmployee);

        ddlName.DataSource = _dataEmployee.CostCenterDetail;
        ddlName.DataTextField = "costcenter_name";
        ddlName.DataValueField = "CostIDX";
        ddlName.DataBind();
    }

    protected void select_org(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlGetOrganizationList, _dataEmployee);
       
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));

        ViewState["vsExport_org"] = _dataEmployee.organization_list;
        //GvEmployee_Gv1.DataSource = ViewState["vsExport_org"];
        //GvEmployee_Gv1.DataBind();
    }

    protected void select_dep(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServiceEmployee(_urlGetDepartmentList, _dataEmployee);     
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกฝ่าย....", "0"));

        ViewState["vsExport_dept"] = _dataEmployee.department_list;
        //GvEmployee_Gv2.DataSource = ViewState["vsExport_dept"];
        //GvEmployee_Gv2.DataBind();
    }

    protected void select_sec(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServiceEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกหน่วยงาน....", "0"));
    }

    protected void Select_Location(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("ประจำสำนักงาน....", "0"));

        _dtEmployee = new data_employee();

        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail search = new employee_detail();

        search.org_idx = int.Parse(ViewState["Org_idx"].ToString());

        _dtEmployee.employee_list[0] = search;

        _dtEmployee = callServiceEmployee(_urlSelect_Location, _dtEmployee);

        ddlName.DataSource = _dtEmployee.employee_list;
        ddlName.DataTextField = "LocName";
        ddlName.DataValueField = "LocIDX";
        ddlName.DataBind();
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected string formatDateTimeCheck(string dateIN)
    {
        //return DateTime.ParseExact(dateIN, "dd/MM/yyyy", null).Month.ToString();
        return DateTime.ParseExact(dateIN, "MM", null).Month.ToString();
    }

    public DataSet getDataSetExportToExcel()
    {
        DataSet ds = new DataSet();
        DataTable dt_gv1 = new DataTable();
        dt_gv1 = GridView_Gv_1();
        DataTable dt_gv2 = new DataTable();
        dt_gv2 = GridView_Gv_2();
        DataTable dt_gv3 = new DataTable();
        dt_gv3 = GridView_Gv_3();
        DataTable dt_gv4 = new DataTable();
        dt_gv4 = GridView_Gv_4();
        DataTable dt_gv5 = new DataTable();
        dt_gv5 = GridView_Gv_5();
        DataTable dt_gv6 = new DataTable();
        dt_gv6 = GridView_Gv_6();
        DataTable dt_gv7 = new DataTable();
        dt_gv7 = GridView_Gv_7();
        DataTable dt_gv8 = new DataTable();
        dt_gv8 = GridView_Gv_8();
        DataTable dt_gv9 = new DataTable();
        dt_gv9 = GridView_Gv_9();
        DataTable dt_gv10 = new DataTable();
        dt_gv10 = GridView_Gv_10();
        DataTable dt_gv11 = new DataTable();
        dt_gv11 = GridView_Gv_11();
        DataTable dt_gv12 = new DataTable();
        dt_gv12 = GridView_Gv_12();
        DataTable dt_gv13 = new DataTable();
        dt_gv13 = GridView_Gv_13();
        DataTable dt_gv14 = new DataTable();
        dt_gv14 = GridView_Gv_14();
        DataTable dt_gv15 = new DataTable();
        dt_gv15 = GridView_Gv_15();
        DataTable dt_gv16 = new DataTable();
        dt_gv16 = GridView_Gv_16();
        DataTable dt_gv17 = new DataTable();
        dt_gv17 = GridView_Gv_17();
        DataTable dt_gv18 = new DataTable();
        dt_gv18 = GridView_Gv_18();

        ds.Tables.Add(dt_gv1);
        ds.Tables.Add(dt_gv2);
        ds.Tables.Add(dt_gv3);
        ds.Tables.Add(dt_gv4);
        ds.Tables.Add(dt_gv5);
        ds.Tables.Add(dt_gv6);
        ds.Tables.Add(dt_gv7);
        ds.Tables.Add(dt_gv8);
        ds.Tables.Add(dt_gv9);
        ds.Tables.Add(dt_gv10);
        ds.Tables.Add(dt_gv11);
        ds.Tables.Add(dt_gv12);
        ds.Tables.Add(dt_gv13);
        ds.Tables.Add(dt_gv14);
        ds.Tables.Add(dt_gv15);
        ds.Tables.Add(dt_gv16);
        ds.Tables.Add(dt_gv17);
        ds.Tables.Add(dt_gv18);
        return ds;
    }

    private DataTable GridView_Gv_1()
    {
        try
        {
            DataTable dtTable = new DataTable("มาสาย");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv1.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_1");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_1");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_1");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_1");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_1");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_1");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_1");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_1");

                    if (lblnum.Text != "0")
                    {
                        dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());
                    }               
                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_2()
    {
        try
        {
            DataTable dtTable = new DataTable("กลับก่อน");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv2.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_2");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_2");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_2");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_2");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_2");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_2");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_2");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_2");

                    if (lblnum.Text != "0")
                    {
                        dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());
                    }
                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_3()
    {
        try
        {
            DataTable dtTable = new DataTable("ป่วยมีใบรับรองแพทย์");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv3.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_3");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_3");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_3");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_3");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_3");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_3");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_3");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_3");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_4()
    {
        try
        {
            DataTable dtTable = new DataTable("ป่วยไม่มีใบรับรองแพทย์");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv4.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_4");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_4");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_4");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_4");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_4");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_4");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_4");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_4");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_5()
    {
        try
        {
            DataTable dtTable = new DataTable("พักผ่อนประจำปี");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv5.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_5");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_5");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_5");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_5");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_5");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_5");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_5");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_5");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_6()
    {
        try
        {
            DataTable dtTable = new DataTable("ลาวันเกิด");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv6.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_6");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_6");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_6");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_6");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_6");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_6");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_6");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_6");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_7()
    {
        try
        {
            DataTable dtTable = new DataTable("ลากิจ(ผ่านทดลองงาน)");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv7.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_7");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_7");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_7");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_7");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_7");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_7");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_7");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_7");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_8()
    {
        try
        {
            DataTable dtTable = new DataTable("ลาดูแลภรรยาคลอดบุตร(3 วัน)");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv8.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_8");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_8");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_8");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_8");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_8");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_8");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_8");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_8");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_9()
    {
        try
        {
            DataTable dtTable = new DataTable("หักขาดงาน");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv9.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_9");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_9");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_9");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_9");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_9");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_9");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_9");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_9");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_10()
    {
        try
        {
            DataTable dtTable = new DataTable("หักขาดงาน(สาย-กลับก่อน)");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv10.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_10");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_10");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_10");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_10");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_10");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_10");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_10");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_10");

                    if (lblnum.Text != "0")
                    {
                        dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());
                    }

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_11()
    {
        try
        {
            DataTable dtTable = new DataTable("ไม่ตอกบัตรเข้า(หักขาดงาน 1 วัน)");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv11.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_11");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_11");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_11");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_11");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_11");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_11");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_11");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_11");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_12()
    {
        try
        {
            DataTable dtTable = new DataTable("ไม่ตอกบัตรออก(หักขาดงาน 1 วัน)");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv12.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_12");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_12");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_12");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_12");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_12");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_12");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_12");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_12");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_13()
    {
        try
        {
            DataTable dtTable = new DataTable("หักลากิจทดลองงาน");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv13.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_13");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_13");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_13");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_13");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_13");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_13");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_13");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_13");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_14()
    {
        try
        {
            DataTable dtTable = new DataTable("หักลากิจ(เกิน 7 วัน)");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv14.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_14");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_14");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_14");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_14");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_14");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_14");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_14");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_14");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_15()
    {
        try
        {
            DataTable dtTable = new DataTable("ลาคลอด1-45");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv15.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_15");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_15");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_15");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_15");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_15");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_15");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_15");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_15");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_16()
    {
        try
        {
            DataTable dtTable = new DataTable("หักลาคลอด46-98");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv16.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_16");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_16");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_16");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_16");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_16");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_16");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_16");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_16");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_17()
    {
        try
        {
            DataTable dtTable = new DataTable("หักอื่นๆ");
            dtTable.Columns.AddRange(new DataColumn[8] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทเงินหัก", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string)),
                                                    new DataColumn("หมายเหตุ", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv17.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_17");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_17");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_17");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_17");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_17");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_17");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_17");
                    Label lbletc = (Label)row.Cells[7].FindControl("lbletc_17");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString(),
                        lbletc.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private DataTable GridView_Gv_18()
    {
        try
        {
            DataTable dtTable = new DataTable("ข้อมูลเงินได้ที่มาจากระบบเวลา");
            dtTable.Columns.AddRange(new DataColumn[7] { new DataColumn("รหัสพนักงาน", typeof(int)),
                                                    new DataColumn("วันที่จ่ายเงิน", typeof(string)),
                                                    new DataColumn("ประเภทข้อมูลเวลา", typeof(string)),
                                                    new DataColumn("จำนวนเงิน", typeof(string)),
                                                    new DataColumn("จำนวน", typeof(string)),
                                                    new DataColumn("หน่วย", typeof(string)),
                                                    new DataColumn("อัตรา", typeof(string))});

            foreach (GridViewRow row in GvEmployee_Gv18.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_code = (Label)row.Cells[0].FindControl("lblemp_code_18");
                    Label lblpay_date = (Label)row.Cells[1].FindControl("lblpay_date_18");
                    Label lblmoney_type = (Label)row.Cells[2].FindControl("lblmoney_type_18");
                    Label lblmoney = (Label)row.Cells[3].FindControl("lblmoney_18");
                    Label lblnum = (Label)row.Cells[4].FindControl("lblnum_18");
                    Label lblunit = (Label)row.Cells[5].FindControl("lblunit_18");
                    Label lblcapacity = (Label)row.Cells[6].FindControl("lblcapacity_18");

                    dtTable.Rows.Add(int.Parse(lblemp_code.Text.ToString()),
                        lblpay_date.Text.ToString(),
                        lblmoney_type.Text.ToString(),
                        lblmoney.Text.ToString(),
                        lblnum.Text.ToString(),
                        lblunit.Text.ToString(),
                        lblcapacity.Text.ToString());

                }
            }

            return dtTable;
        }
        catch (Exception)
        {
            throw;
        }
    }

    #endregion


    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "Fv_Search_Emp_Report":
                FormView Fv_Search_Emp_Report = (FormView)ViewReport.FindControl("Fv_Search_Emp_Report");
                DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
                DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
                DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
                DropDownList ddllocation = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation");
                //DropDownList ddlparttime = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlparttime");

                if (Fv_Search_Emp_Report.CurrentMode == FormViewMode.Insert)
                {
                    select_org(ddlorg_rp);
                    Select_Location(ddllocation);
                }
                break;
        }
    }

    #endregion

    #region Switch-Munu Color
    protected void Menu_Color(int choice)
    {
        switch (choice)
        {
            case 4: //Report
                //lbindex.BackColor = System.Drawing.Color.Transparent;
                //lbadd.BackColor = System.Drawing.Color.Transparent;
                //lbsetapprove.BackColor = System.Drawing.Color.Transparent;
                lblreport.BackColor = System.Drawing.Color.LightGray;
                Fv_Search_Emp_Report.ChangeMode(FormViewMode.Insert);
                Fv_Search_Emp_Report.DataBind();
                break;
            case 2:

                break;
        }
    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        string check_emp_code = "";

        switch (GvName.ID)
        {
            case "GvEmployee":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                }
                break;
            case "GvEmployee_Report":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    try
                    {

                        Label lbltime_all_sum = (Label)e.Row.FindControl("lbltime_all_sum");
                        Label lbltime_in = (Label)e.Row.FindControl("lbltime_in");
                        Label lbltime_out = (Label)e.Row.FindControl("lbltime_out");
                        Label lbltime_in_rate = (Label)e.Row.FindControl("lbltime_in_rate");
                        Label lbltime_leave = (Label)e.Row.FindControl("lbltime_leave");


                        decimal value = Convert.ToDecimal(lbltime_all_sum.Text);
                        int n = Convert.ToInt32(value);
                        decimal value1 = Convert.ToDecimal(lbltime_in.Text);
                        int n1 = Convert.ToInt32(value1);
                        decimal value2 = Convert.ToDecimal(lbltime_out.Text);
                        int n2 = Convert.ToInt32(value2);
                        decimal value3 = Convert.ToDecimal(lbltime_in_rate.Text);
                        int n3 = Convert.ToInt32(value3);
                        decimal value4 = Convert.ToDecimal(lbltime_leave.Text);
                        int n4 = Convert.ToInt32(value4);

                        tot_actual_all_time += Convert.ToDecimal(value);
                        tot_actual_time_in += Convert.ToDecimal(value1);
                        tot_actual_time_out += Convert.ToDecimal(value2);
                        tot_actual_time_rate += Convert.ToDecimal(value3);
                        tot_actual_time_leave += Convert.ToDecimal(value4);
                    }
                    catch
                    {

                    }




                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    try
                    {
                        Label lit_time_all_sum = (Label)e.Row.FindControl("lit_time_all_sum");
                        Label lit_time_in = (Label)e.Row.FindControl("lit_time_in");
                        Label lit_time_out = (Label)e.Row.FindControl("lit_time_out");
                        Label lit_time_in_rate = (Label)e.Row.FindControl("lit_time_in_rate");
                        Label lit_time_leave = (Label)e.Row.FindControl("lit_time_leave");

                        lit_time_all_sum.Text = String.Format("{0:N2}", tot_actual_all_time);
                        lit_time_in.Text = String.Format("{0:N2}", tot_actual_time_in);
                        lit_time_out.Text = String.Format("{0:N2}", tot_actual_time_out);
                        lit_time_in_rate.Text = String.Format("{0:N2}", tot_actual_time_rate);
                        lit_time_leave.Text = String.Format("{0:N2}", tot_actual_time_leave);
                    }
                    catch
                    {

                    }

                }
                break;
            case "GvEmployee_Gv1":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblnum_1 = (Label)e.Row.FindControl("lblnum_1");
                    Label lblemp_code_1 = (Label)e.Row.FindControl("lblemp_code_1");
                    GridView GvEmployee_Gv1 = (GridView)ViewReport.FindControl("GvEmployee_Gv1");
                    //Label Label5 = (Label)e.Row.FindControl("Label5");
                    //Label Label6 = (Label)e.Row.FindControl("Label6");
                    DropDownList ddltemp = (DropDownList)e.Row.FindControl("ddltemp");

                    if (GvEmployee_Gv1.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        #region Scan IN
                        // Scan IN
                        u0_report_iam[] _item_scanin = (u0_report_iam[])ViewState["GvEmployee_Gv1"];
                        var _linq_scanin = (from data in _item_scanin
                                            where
                                            data.emp_code == lblemp_code_1.Text
                                            && data.emp_code != "" 
                                            select new
                                            {
                                                data.late_idx,
                                                data.emp_code,
                                                data.pay_date,
                                                data.money_type,
                                                data.money,
                                                data.num,
                                                data.unit,
                                                data.capacity,
                                                data.etc
                                                /*data.status,
                                                data.month,
                                                data.emp_time*/
                                            }
                                           ).ToList();
                        if (_linq_scanin.Count() > 0)
                        {
                            setDdlData(ddltemp, _linq_scanin.ToList(), "num", "num");
                            int maxValue = ddltemp.Items.Cast<ListItem>().Select(item => int.Parse(item.Value)).Min();
                            ddltemp.SelectedValue = maxValue.ToString();
                            //Label5.Text = ddltemp.SelectedItem.ToString();
                            var _linq_scanin_hour = (from data in _item_scanin
                                                     where
                                                     data.emp_code == lblemp_code_1.Text
                                                     && data.emp_code != ""
                                                     && data.num == int.Parse(ddltemp.SelectedValue)
                                                     select new
                                                     {
                                                         data.late_idx,
                                                         data.emp_code,
                                                         data.pay_date,
                                                         data.money_type,
                                                         data.money,
                                                         data.num,
                                                         data.unit,
                                                         data.capacity,
                                                         data.etc,
                                                         data.status,
                                                         data.month
                                                     }
                                           ).ToList();

                            lblnum_1.Text = _linq_scanin_hour[0].num.ToString();
                            //getHourToOT(ddl_timebefore, Convert.ToDouble(lbl_ot_before.Text));
                            //lbl_time_shiftstart.Text = _linq_scanin_hour[0].time_shiftstart.ToString();
                            //lbl_parttime_break_start_time.Text = _linq_scanin_hour[0].parttime_break_start_time.ToString();
                        }
                        #endregion Scan IN
                        if (e.Row.RowType == DataControlRowType.DataRow)
                        {
                            if (lblnum_1.Text == "0")
                            {
                                e.Row.Visible = false;
                            }
                        }
                    }

                    check_emp_code = lblemp_code_1.Text.ToString();                   
                }
                break;
            case "GvEmployee_Gv2":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblnum_2 = (Label)e.Row.FindControl("lblnum_2");
                    Label lblemp_code_2 = (Label)e.Row.FindControl("lblemp_code_2");
                    GridView GvEmployee_Gv2 = (GridView)ViewReport.FindControl("GvEmployee_Gv2");
                    //Label Label5 = (Label)e.Row.FindControl("Label5");
                    //Label Label6 = (Label)e.Row.FindControl("Label6");
                    DropDownList ddltemp_2 = (DropDownList)e.Row.FindControl("ddltemp_2");

                    if (GvEmployee_Gv2.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        #region Scan IN
                        // Scan IN
                        u0_report_iam[] _item_scanin = (u0_report_iam[])ViewState["GvEmployee_Gv2"];
                        var _linq_scanin = (from data in _item_scanin
                                            where
                                            data.emp_code == lblemp_code_2.Text
                                            && data.emp_code != ""
                                            select new
                                            {
                                                data.late_idx,
                                                data.emp_code,
                                                data.pay_date,
                                                data.money_type,
                                                data.money,
                                                data.num,
                                                data.unit,
                                                data.capacity,
                                                data.etc
                                            }
                                           ).ToList();
                        if (_linq_scanin.Count() > 0)
                        {
                            setDdlData(ddltemp_2, _linq_scanin.ToList(), "num", "num");
                            int maxValue = ddltemp_2.Items.Cast<ListItem>().Select(item => int.Parse(item.Value)).Min();
                            ddltemp_2.SelectedValue = maxValue.ToString();
                            //Label5.Text = ddltemp.SelectedItem.ToString();
                            var _linq_scanin_hour = (from data in _item_scanin
                                                     where
                                                     data.emp_code == lblemp_code_2.Text
                                                     && data.emp_code != ""
                                                     && data.num == int.Parse(ddltemp_2.SelectedValue)
                                                     select new
                                                     {
                                                         data.late_idx,
                                                         data.emp_code,
                                                         data.pay_date,
                                                         data.money_type,
                                                         data.money,
                                                         data.num,
                                                         data.unit,
                                                         data.capacity,
                                                         data.etc,
                                                         data.status,
                                                         data.month
                                                     }
                                           ).ToList();

                            lblnum_2.Text = _linq_scanin_hour[0].num.ToString();
                        }
                        #endregion Scan IN
                        if (e.Row.RowType == DataControlRowType.DataRow)
                        {
                            if (lblnum_2.Text == "0")
                            {
                                e.Row.Visible = false;
                            }
                        }
                    }

                    check_emp_code = lblemp_code_2.Text.ToString();
                    
                }
                break;
            case "GvEmployee_Gv10":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblnum_10 = (Label)e.Row.FindControl("lblnum_10");
                    Label lblemp_code_10 = (Label)e.Row.FindControl("lblemp_code_10");
                    GridView GvEmployee_Gv10 = (GridView)ViewReport.FindControl("GvEmployee_Gv10");
                    DropDownList ddltemp_10 = (DropDownList)e.Row.FindControl("ddltemp_10");

                    if (GvEmployee_Gv10.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        #region Scan IN
                        // Scan IN
                        u0_report_iam[] _item_scanin = (u0_report_iam[])ViewState["GvEmployee_Gv10"];
                        var _linq_scanin = (from data in _item_scanin
                                            where
                                            data.emp_code == lblemp_code_10.Text
                                            && data.emp_code != ""
                                            select new
                                            {
                                                data.late_idx,
                                                data.emp_code,
                                                data.pay_date,
                                                data.money_type,
                                                data.money,
                                                data.num,
                                                data.unit,
                                                data.capacity,
                                                data.etc
                                            }
                                           ).ToList();
                        if (_linq_scanin.Count() > 0)
                        {
                            setDdlData(ddltemp_10, _linq_scanin.ToList(), "num", "num");
                            int maxValue = ddltemp_10.Items.Cast<ListItem>().Select(item => int.Parse(item.Value)).Min();
                            ddltemp_10.SelectedValue = maxValue.ToString();
                            //Label5.Text = ddltemp.SelectedItem.ToString();
                            var _linq_scanin_hour = (from data in _item_scanin
                                                     where
                                                     data.emp_code == lblemp_code_10.Text
                                                     && data.emp_code != ""
                                                     //&& data.num == int.Parse(ddltemp_10.SelectedValue)
                                                     select new
                                                     {
                                                         data.late_idx,
                                                         data.emp_code,
                                                         data.pay_date,
                                                         data.money_type,
                                                         data.money,
                                                         data.num,
                                                         data.unit,
                                                         data.capacity,
                                                         data.etc,
                                                         data.status,
                                                         data.month
                                                     }
                                           ).ToList();

                            lblnum_10.Text = _linq_scanin_hour[0].num.ToString();
                        }
                        #endregion Scan IN
                        if (e.Row.RowType == DataControlRowType.DataRow)
                        {
                            if (lblnum_10.Text == "0")
                            {
                                e.Row.Visible = false;
                            }
                        }
                    }

                    check_emp_code = lblemp_code_10.Text.ToString();

                }
                break;
            case "GvEmployee_Gv3":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //Label lblnum_3 = (Label)e.Row.FindControl("lblnum_3");

                    //lblnum_3.Text = lblnum_3.Text.ToString("N2"); //String.Format("{0:N2}", lblnum_3);
                }
                break;
        }
    }
    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvEmployee_Gv1":
                //GvEmployee_Report.PageIndex = e.NewPageIndex;
                //GvEmployee_Report.DataBind();
                //Select_Employee_report_search();
                break;
        }
    }

    #endregion

    #region GvRowEditing
    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "dd":

                break;

        }

    }
    #endregion

    #region GvRowUpdating
    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "dd":

                break;

        }

    }

    #endregion

    #region GvRowCancelingEdit
    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "dd":

                break;
        }
    }

    #endregion

    #region GvRowDeleting
    protected void gvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "dd":

                break;
        }

    }
    #endregion

    #endregion

    #region ChkSelectedIndexChanged
    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (CheckBox)sender;

        switch (ddName.ID)
        {
            case "ckreference":

                break;

        }
    }
    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        Panel BoxSearch_Date = (Panel)Fv_Search_Emp_Report.FindControl("BoxSearch_Date");



        switch (ddName.ID)
        {
            case "ddlOrganizationEdit_Select":
                var ddNameOrg = (DropDownList)sender;
                var row = (GridViewRow)ddNameOrg.NamingContainer;

                var ddlOrg = (DropDownList)row.FindControl("ddlOrganizationEdit_Select");
                var ddlDep = (DropDownList)row.FindControl("ddlDepartmentEdit_Select");
                var ddlSec = (DropDownList)row.FindControl("ddlSectionEdit_Select");
                var ddlPos = (DropDownList)row.FindControl("ddlPositionEdit_Select");

                select_dep(ddlDep, int.Parse(ddlOrg.SelectedValue));
                break;
            case "ddlDepartmentEdit_Select":
                var ddNameOrg_1 = (DropDownList)sender;
                var row_1 = (GridViewRow)ddNameOrg_1.NamingContainer;

                var ddlOrg_1 = (DropDownList)row_1.FindControl("ddlOrganizationEdit_Select");
                var ddlDep_1 = (DropDownList)row_1.FindControl("ddlDepartmentEdit_Select");
                var ddlSec_1 = (DropDownList)row_1.FindControl("ddlSectionEdit_Select");
                var ddlPos_1 = (DropDownList)row_1.FindControl("ddlPositionEdit_Select");

                select_sec(ddlSec_1, int.Parse(ddlOrg_1.SelectedValue), int.Parse(ddlDep_1.SelectedValue));
                break;

            case "ddlorg_rp":
                select_dep(ddldep_rp, int.Parse(ddlorg_rp.SelectedValue));
                break;
            case "ddldep_rp":
                select_sec(ddlsec_rp, int.Parse(ddlorg_rp.SelectedValue), int.Parse(ddldep_rp.SelectedValue));
                break;

            case "ddldatetype":
                if (ddldatetype.SelectedValue == "1" || ddldatetype.SelectedValue == "2" || ddldatetype.SelectedValue == "3" || ddldatetype.SelectedValue == "4")
                {
                    BoxSearch_Date.Visible = true;
                }
                else
                {
                    BoxSearch_Date.Visible = false;
                }
                break;
            case "ddllocation":
                LinkButton btnsearch_report = (LinkButton)Fv_Search_Emp_Report.FindControl("btnsearch_report");
                DropDownList ddllocation = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation");

                /*if (ddllocation.SelectedValue == "0")
                {
                    btnsearch_report.Visible = false;
                }
                else
                {
                    btnsearch_report.Visible = true;
                }*/

                break;
        }
    }

    #endregion

    #region callService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_report_iam callServiceReport_Iam(string _cmdUrl, data_report_iam _dataReport_Iam)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataReport_Iam);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataReport_Iam = (data_report_iam)_funcTool.convertJsonToObject(typeof(data_report_iam), _localJson);


        return _dataReport_Iam;
    }

    #endregion

    #region custom data format and display
    protected string formatDate(string dateIN)
    {
        return DateTime.Parse(dateIN).ToString("dd/MM/yyyy");
    }
    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "btnreport":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewReport);
                break;

            case "btn_search_report":

                TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_s");
                TextBox txtempname_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempname_s");
                //TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
                //TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");

                DropDownList ddlmonth = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlmonth");
                DropDownList ddllocation = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation");
                DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
                DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
                DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
                DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");

                if (int.Parse(ddlmonth.SelectedValue) == 8 || int.Parse(ddlmonth.SelectedValue) == 9 || int.Parse(ddlmonth.SelectedValue) == 10)
                {
                    ViewState["txtempcode_s"] = txtempcode_s.Text;
                    ViewState["txtempname_s"] = txtempname_s.Text;
                    ViewState["ddlmonth"] = int.Parse(ddlmonth.SelectedValue);
                    ViewState["ddllocation"] = int.Parse(ddllocation.SelectedValue);
                    ViewState["ddlorg_rp"] = int.Parse(ddlorg_rp.SelectedValue);
                    ViewState["ddldep_rp"] = int.Parse(ddldep_rp.SelectedValue);
                    ViewState["ddlsec_rp"] = int.Parse(ddlsec_rp.SelectedValue);
                    ViewState["ddlemptype_s"] = int.Parse(ddlemptype_s.SelectedValue);

                    Select_late_1();
                    Select_return_2();
                    Select_sick_file_3();
                    Select_sick_non_file_4();
                    Select_holiday_5();
                    Select_birthday_6();
                    Select_probation_7();
                    Select_takecare_8();
                    Select_absence_9();
                    Select_absence_return_10();
                    Select_absence_late_11();
                    Select_absen_all_12();
                    Select_probation_absence_13();
                    Select_activity_over_14();
                    Select_maternity_before_15();
                    Select_maternity_after_16();
                    Select_etc_17();
                    Select_time_18();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "แจ้งเตือน", "alert('" + "ไม่มีข้อมูล" + "')", true);
                }
                

                break;
            case "Cmdsearch_clear":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnreport_back":
                box_index.Visible = true;
                break;

            case "btnExport":
                try{
                    switch (int.Parse(cmdArg))
                    {
                        case 1: //Time_all_sum
                                //GvEmployee_Gv1.AllowPaging = false;
                                //GvEmployee_Gv1.DataSource = ViewState["vsExport_org"];
                                //GvEmployee_Gv1.DataBind();

                            DataSet ds = getDataSetExportToExcel();
                            using (XLWorkbook wb = new XLWorkbook())
                            {
                                /*foreach (DataTable dt in ds.Tables)
                                {
                                    //Add DataTable as Worksheet.
                                    wb.Worksheets.Add(dt);
                                }*/

                                wb.Worksheets.Add(ds);
                                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                wb.Style.Font.Bold = true;
                                Response.Clear();
                                Response.Buffer = true;
                                Response.Charset = "utf-8";
                                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                Response.AddHeader("content-disposition", "attachment;filename= Report_Iam.xls");
                                using (MemoryStream MyMemoryStream = new MemoryStream())
                                {
                                    wb.SaveAs(MyMemoryStream);
                                    MyMemoryStream.WriteTo(Response.OutputStream);
                                    Response.Flush();
                                    Response.End();
                                }

                                /*Response.Clear();
                                Response.Buffer = true;
                                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_Time.xls");
                                // Response.Charset = ""; set character 
                                Response.Charset = "utf-8";
                                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                                Response.ContentType = "application/vnd.ms-excel";

                                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                                //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                                StringWriter sw = new StringWriter();
                                HtmlTextWriter hw = new HtmlTextWriter(sw);

                                for (int i = 0; i < GvEmployee_Gv1.Rows.Count; i++)
                                {
                                    //Apply text style to each Row
                                    GvEmployee_Gv1.Rows[i].Attributes.Add("class", "number2");
                                }
                                GvEmployee_Gv1.RenderControl(hw);
                                //Amount is displayed in number format with 2 decimals
                                Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                                Response.Output.Write(sw.ToString());
                                Response.Flush();
                                Response.End();*/
                            }
                            break;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                
                break;
        }
    }
    #endregion
}