﻿<%@ WebHandler Language="C#" Class="JsonResponse" %>

using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.SessionState;


public class JsonResponse : IHttpHandler, IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/json";

        DateTime start = Convert.ToDateTime(context.Request.QueryString["start"]);
        //try { start = Convert.ToDateTime(context.Request.QueryString["start"]); } catch (Exception e) { start = DateTime.Now; }
        DateTime end = Convert.ToDateTime(context.Request.QueryString["end"]);
        //try { end = Convert.ToDateTime(context.Request.QueryString["end"]); } catch (Exception e) { end = DateTime.Now; }
        String place_idx_value = context.Request.QueryString["place_idx"];


        int place_idx = int.Parse(context.Session["PlaceSearch_Room"].ToString());//3;//int.Parse(place_idx_value);//3;//Convert.ToDateTime(context.Request.QueryString["end"]);
        int m0_room_idx = int.Parse(context.Session["RoomIDXSearch_Room"].ToString());//0;

        List<int> idList = new List<int>();
        List<ImproperCalendarEvent> tasksList = new List<ImproperCalendarEvent>();

        //Generate JSON serializable events
        foreach (CalendarEvent cevent in EventDAO.getEvents(start, end, place_idx, m0_room_idx))
        {
            tasksList.Add(new ImproperCalendarEvent {
                id = cevent.id,
                title = cevent.title,
                start = String.Format("{0:s}", cevent.start),
                end = String.Format("{0:s}", cevent.end),

                description = cevent.description,
                place_name = cevent.place_name,
                room_name_th = cevent.room_name_th,
                topic_booking = cevent.topic_booking,
                allDay = cevent.allDay,
                emp_name_th = cevent.emp_name_th,
            });
            idList.Add(cevent.id);
        }

        context.Session["idList"] = idList;

        //Serialize events to string
        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        string sJSON = oSerializer.Serialize(tasksList);

        //Write JSON to response object
        context.Response.Write(sJSON);
    }

    public bool IsReusable {
        get { return false; }
    }
}