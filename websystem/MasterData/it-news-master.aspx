﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="it-news-master.aspx.cs" Inherits="websystem_MasterData_it_news_master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">


    <asp:Literal ID="xmlCheck" runat="server"></asp:Literal>
    <!-- ----------- START TAB MANU BAR --------------- -->
    <asp:HyperLink ID="SETFOCUS_ONTOP" runat="server"></asp:HyperLink>
    <div id="divMenubar" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav ulActiveMenu" runat="server">
                     <li id="insert_type_newslist" class="header-menu collapse-left menu-default dropdown" runat="server">
                        <asp:LinkButton ID="listMenu2" runat="server"
                            CommandName="viewmanu2" Visible="false"
                            OnCommand="btncommand" Text="ประเภทข่าวไอที" />
                    </li>
                    <li id="IndexList" runat="server">
                        <asp:LinkButton ID="listMenu0" runat="server"
                            CommandName="viewmanu1" Visible="false"
                            OnCommand="btncommand" Text="ข้อมูลทั่วไป" />
                    </li>
                    <li id="insert_title_newslist" runat="server">
                        <asp:LinkButton ID="listMenu3" runat="server"
                            CommandName="viewmanu3" 
                            OnCommand="btncommand" Text="หัวข้อข่าวไอที" />
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <!-- ----------- END TAB MANU BAR --------------- -->

    <asp:MultiView ID="mvMultiview" runat="server" ActiveViewIndex="0">


        <!-- ----------- START VIEW GENERAL INFORMATION --------------- -->

        <asp:View ID="viewHomepage" runat="server">
            <div class="form-group">
                <div class="col-sm-12">
                    <asp:GridView ID="gvNewsITlist" runat="server" AutoGenerateColumns="false" DataKeyNames="u0_news_idx"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="10"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                            FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูลข่าวไอที</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <div style="text-align: center; padding-top: 5px;">
                                        <asp:Label ID="_lbu0newsidx" runat="server" Visible="false" Text='<%# Eval("u0_news_idx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ประเภทข่าวไอที" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px;">
                                            <asp:Label ID="lbNametypeitnews" runat="server"
                                                CssClass="col-sm-12" Text='<%# Eval("name_type_news") %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หัวข้อข่าวไอที" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px;">
                                            <asp:Label ID="lbtitleitnews" runat="server"
                                                CssClass="col-sm-12" Text='<%# Eval("title_it_news") %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รายละเอียดของข่าว" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="40%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px;">
                                            <asp:Label ID="lbdetailsnews" runat="server"
                                                CssClass="col-sm-12" Text='<%# Eval("details_news") %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px;">
                                            <asp:Label ID="lbstatusnews" Visible="false" runat="server"
                                                CssClass="col-sm-12" Text='<%# Eval("status_news") %>'></asp:Label>
                                            <asp:Label ID="statusnewsoffline" runat="server" Visible="false"
                                                data-toggle="tooltip" data-placement="top" title="offline"
                                                CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                            </asp:Label>
                                            <asp:Label ID="statusnewsonline" runat="server" Visible="false"
                                                data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                            </asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </asp:View>

        <!-- ----------- END VIEW GENERAL INFORMATION --------------- -->


        <!-- ----------- START VIEW INSERT TYPE NEWS IT --------------- -->

        <asp:View ID="viewInsert_TYpeNewsIT" runat="server">
            <div class="col-sm-12" runat="server" id="content_managetypeNews">
                <div class="form-group">
                    <asp:LinkButton ID="btnShowinsertType" CssClass="btn btn-md btn-primary" runat="server" CommandArgument="0"
                        CommandName="cmdShowBoxTypeNews" data-original-title="เพิ่มประเภทข่าวไอที" data-toggle="tooltip" OnCommand="btncommand" Text="เพิ่มประเภทข่าว"></asp:LinkButton>
                    <asp:LinkButton ID="btnBack" CssClass="btn btn-md btn-default" runat="server"
                        CommandName="btncancel" Visible="false" data-original-title="ย้อนกลับ" data-toggle="tooltip" OnCommand="btncommand" Text="ย้อนกลับ"></asp:LinkButton>
                </div>
                <div class="form-group">
                    <asp:Panel ID="panel_Insert_TYpeNewsIT" runat="server" Visible="false">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-plus-sign"></i>&nbsp;
                            <b>INSERT TYPE NEWS</b> (เพิ่มประเภทข่าวไอที)
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-6">
                                            <label><small>ชื่อประเภทข่าว  ( NAME NEWS TYPE )</small></label>

                                            <asp:TextBox ID="txtNameTYpeNews" placeholder="กรุณากรอกชื่อประเภทข่าวไอที..."
                                                runat="server" CssClass="form-control fa-align-left" OnTextChanged="txtChange"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredtxtNameTYpeNews" runat="server"
                                                ControlToValidate="txtNameTYpeNews" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อประเภทข่าวไอที" ValidationGroup="SaveTypeNews" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtNameTYpeNews" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtxtNameTYpeNews" Width="250" />
                                             <asp:Label ID="lbltxtShowTypeNews" runat="server" Font-Size="9"></asp:Label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label><small>สถานะประเภทข่าว ( STATUS )</small></label>

                                            <asp:DropDownList ID="ddlStatusTypeNews"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-9">

                                            <asp:LinkButton CssClass="btn btn-success" ID="btnsave" ValidationGroup="SaveTypeNews" data-toggle="tooltip" title="บันทึก" runat="server"
                                                CommandName="save_type_news" OnCommand="btncommand"> บันทึก</asp:LinkButton>&nbsp;
                                            <asp:LinkButton CssClass="btn btn-danger" ID="btncancel" data-toggle="tooltip" title="ยกเลิก" runat="server"
                                                CommandName="btnback" CommandArgument="0" OnCommand="btncommand"> ยกเลิก</asp:LinkButton>&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="form-group">

                    <asp:GridView ID="gvtypeNews" runat="server" AutoGenerateColumns="false" DataKeyNames="m0_news_idx"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="10"
                        OnPageIndexChanging="gvPageIndexChanging" OnRowEditing="gvRowEditing" OnRowUpdating="gvRowUpdating"
                        OnRowCancelingEdit="gvRowCancelingEdit" OnRowDataBound="gvRowDataBound">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                            FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูลประเภทข่าวไอที</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <div style="text-align: center;">
                                        <asp:Label ID="lbm0newsidx" runat="server" Visible="false" Text='<%# Eval("m0_news_idx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </div>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="_update_m0_news_idx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("m0_news_idx")%>' />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อประเภทข่าวไอที" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtupdate_typeNews" runat="server" OnTextChanged="txtChange" AutoPostBack="true" CssClass="form-control" Text='<%# Eval("type_it_news")%>' />
                                                     <asp:Label ID="lbltxtShowtypeNewsUpdate" runat="server" Font-Size="9"></asp:Label>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="lbupdatestatus_type_news" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddlstatus_type_news" Text='<%# Eval("status_type_news") %>'
                                                        CssClass="form-control fa-align-left" runat="server">
                                                        <asp:ListItem Value="1">online</asp:ListItem>
                                                        <asp:ListItem Value="0">offline</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div> <div class="col-sm-2"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3"></div>
                                                <div class="col-sm-9">
                                                    <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ประเภทข่าวไอที" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px;">
                                            <asp:Label ID="lbtype_it_news" runat="server"
                                                CssClass="col-sm-12" Text='<%# Eval("type_it_news") %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        
                                            <asp:Label ID="lbstatus_type_news" Visible="false" runat="server"
                                                CssClass="col-sm-12" Text='<%# Eval("status_type_news") %>'></asp:Label>
                                            <asp:Label ID="status_offline" runat="server" Visible="false"
                                                data-toggle="tooltip" data-placement="top" title="offline"
                                                CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                            </asp:Label>
                                            <asp:Label ID="status_online" runat="server" Visible="false"
                                                data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                            </asp:Label>
                                     
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>

                                    <asp:LinkButton ID="Edit" CssClass="btn btn-sm btn-primary" runat="server" CommandName="Edit"
                                        data-toggle="tooltip" OnCommand="btncommand" title="แก้ไขประเภทข่าวไอที">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btndelettypeNews" CssClass="btn btn-sm btn-danger" runat="server" CommandName="deletetypeNews"
                                        data-toggle="tooltip" OnCommand="btncommand" OnClientClick="return confirm('คุณต้องการลบประเภทข่าวไอทีนี้ใช่หรือไม่ ?')"
                                        CommandArgument='<%#Eval("m0_news_idx") %>' title="ลบประเภทข่าวไอที">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>
            </div>
        </asp:View>

        <!-- ----------- END VIEW INSERT TYPE NEWS IT --------------- -->



        <!-- ----------- START VIEW INSERT NEWS IT --------------- -->

        <asp:View ID="viewInsert_NewsIT" runat="server">
            <div class="col-sm-12" runat="server" id="content_manageNews">
                <div class="form-group">
                    <asp:LinkButton ID="btnShowBoxNews" CssClass="btn btn-primary" runat="server" CommandArgument="1"
                        CommandName="cmdShowBoxTypeNews" data-original-title="เพิ่มหัวข้อข่าวไอที" data-toggle="tooltip" OnCommand="btncommand" Text="เพิ่มหัวข้อข่าวไอที"></asp:LinkButton>
                    <asp:LinkButton ID="btncancelNews" CssClass="btn btn-default" runat="server"
                        CommandName="btncancel" Visible="false" data-original-title="ย้อนกลับ" data-toggle="tooltip" OnCommand="btncommand" Text="ย้อนกลับ"></asp:LinkButton>
                </div>
                <div class="form-group">
                    <asp:Panel ID="panelInsertNews" runat="server" Visible="false">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-plus-sign"></i>&nbsp;
                            <b>INSERT NAME NEWS</b> (เพิ่มหัวข้อข่าวไอที)
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-7">
                                            <label><small>เลือกประเภทของข่าว ( SELECT TYPE NEWS )</small></label>

                                            <asp:DropDownList ID="ddlNametypeNews"
                                                runat="server" CssClass="form-control fa-align-left"
                                                Enabled="true">
                                            </asp:DropDownList>

                                              <asp:RequiredFieldValidator ID="RequireddlNametypeNews" runat="server" InitialValue="0"
                                                ControlToValidate="ddlNametypeNews" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทข่าวไอที" ValidationGroup="SaveNews" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlNametypeNews" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequireddlNametypeNews" Width="250" />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-4">
                                            <label><small>ชื่อข่าว  ( NAME NEWS TYPE )</small></label>

                                            <asp:TextBox ID="txttitleNews" placeholder="กรุณากรอกชื่อข่าวไอที..."
                                                runat="server" CssClass="form-control fa-align-left"
                                                Enabled="true">
                                            </asp:TextBox>
                                              <asp:RequiredFieldValidator ID="RequiredtxttitleNews" runat="server"
                                                ControlToValidate="txttitleNews" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อข่าวไอที" ValidationGroup="SaveNews" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxttitleNews" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtxttitleNews" Width="250" />
                                        </div>
                                        <div class="col-sm-3">
                                            <label><small>สถานะข่าว ( STATUS )</small></label>

                                            <asp:DropDownList ID="ddlStatusNews"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-7">
                                            <label><small>รายละเอียดของข่าว  ( DETAILS NEWS )</small></label>

                                            <asp:TextBox ID="txtdetailsNews" placeholder="กรุณากรอกรายละเอียดของข่าวไอที..."
                                                runat="server" CssClass="form-control fa-align-left"
                                                TextMode="MultiLine" MaxLength="3" Enabled="true">
                                            </asp:TextBox>
                                               <asp:RequiredFieldValidator ID="RequiredtxtdetailsNews" runat="server"
                                                ControlToValidate="txtdetailsNews" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกรายละเอียดของข่าวไอที" ValidationGroup="SaveNews" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatotxtdetailsNews" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtxtdetailsNews" Width="250" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-7">
                                            <div class="panel panel-success">
                                                <div class="panel-body">
                                                    <label><small>อัพโหลดไฟล์ภาพ  (upload picture)</small></label>

                                                    <asp:FileUpload ID="uploadpicture" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                        CssClass="control-label" accept="jpg|png" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-7">
                                            <div class="panel panel-success">
                                                <div class="panel-body">

                                                    <asp:CheckBox ID="chk_sameAddress_present" runat="server"
                                                        />&nbsp; ** ต้องการแจ้งข่าวสารผ่าน e-mail
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--  OnCheckedChanged="chkCheckedChanged"--%>

                                    <asp:UpdatePanel ID="updatebtnsave" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-9">

                                                    <asp:LinkButton CssClass="btn btn-success" ID="btnsavenews" data-toggle="tooltip" title="บันทึก" runat="server"
                                                        CommandName="save_news" OnCommand="btncommand" ValidationGroup="SaveNews"> บันทึก</asp:LinkButton>&nbsp;
                                            <asp:LinkButton CssClass="btn btn-danger" ID="btnCancelsave" data-toggle="tooltip" title="ยกเลิก" runat="server"
                                                CommandName="btnback" CommandArgument="1" OnCommand="btncommand"> ยกเลิก</asp:LinkButton>&nbsp;
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnsavenews" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>

                <div class="form-group">
                    <asp:GridView ID="gvNewsIT" runat="server" AutoGenerateColumns="false" DataKeyNames="u0_news_idx"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        HeaderStyle-CssClass="info" HeaderStyle-Height="30px" Font-Size="11" AllowPaging="true" PageSize="10"
                        OnPageIndexChanging="gvPageIndexChanging" OnRowEditing="gvRowEditing" OnRowUpdating="gvRowUpdating"
                        OnRowCancelingEdit="gvRowCancelingEdit" OnRowDataBound="gvRowDataBound">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                            FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูลข่าวไอที</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="left" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <div style="text-align: center">
                                        <asp:Label ID="lbu0newsidx" runat="server" Visible="false" Text='<%# Eval("u0_news_idx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </div>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="_update_u0_news_idx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("u0_news_idx")%>' />
                                                </div>
                                                 <div class="col-sm-10"></div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="lbupdate_m0_news_idx" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อประเภทข่าวไอที" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtupdate_m0_news_idx" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("m0_news_idx")%>' />
                                                    <asp:DropDownList ID="ddlupdate_m0_news" CssClass="form-control fa-align-left" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                 <div class="col-sm-2"></div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="lbupdate_titlenews" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อหัวข้อข่าวไอที" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtupdate_titlenews" runat="server" CssClass="form-control" Text='<%# Eval("title_it_news")%>' />
                                                </div>
                                                 <div class="col-sm-2"></div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="lbupdate_details_news" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดของข่าว" />
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtupdate_details_news" Rows="10" TextMode="MultiLine" runat="server" CssClass="form-control" Text='<%# Eval("details_news")%>' />
                                                </div> 
                                                <div class="col-sm-2"></div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="lbupdatestatus_news" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddlupdate_status_news" Text='<%# Eval("status_news") %>'
                                                        CssClass="form-control fa-align-left" runat="server">
                                                        <asp:ListItem Value="1">online</asp:ListItem>
                                                        <asp:ListItem Value="0">offline</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div> 
                                                <div class="col-sm-2"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3"></div>
                                                <div class="col-sm-9">
                                                    <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ประเภทข่าวไอที" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                     
                                            <asp:Label ID="lbNametype_it_news" runat="server"
                                                CssClass="col-sm-12" Text='<%# Eval("name_type_news") %>'></asp:Label>
                                     
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หัวข้อข่าวไอที" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                     
                                            <asp:Label ID="lbtitle_it_news" runat="server"
                                                CssClass="col-sm-12" Text='<%# Eval("title_it_news") %>'></asp:Label>
                                      
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รายละเอียดของข่าว" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="40%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                       
                                            <asp:Label ID="lbdetails_news" runat="server"
                                                CssClass="col-sm-12" Text='<%# Eval("details_news") %>'></asp:Label>
                                      
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                      
                                            <asp:Label ID="lbstatus_news" Visible="false" runat="server"
                                                CssClass="col-sm-12" Text='<%# Eval("status_news") %>'></asp:Label>
                                            <asp:Label ID="status_news_offline" runat="server" Visible="false"
                                                data-toggle="tooltip" data-placement="top" title="offline"
                                                CssClass="col-sm-12">
                                                <div style="color: red;">
                                                    <span><b>offline</b></span>
                                            </asp:Label>
                                            <asp:Label ID="status_news_online" runat="server" Visible="false"
                                                data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="color: green;">
                                                <span><b>online</b></span>
                                            </asp:Label>
                                       
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                  
                                        <asp:LinkButton ID="Edit" CssClass="btn btn-sm btn-primary" runat="server" CommandName="Edit"
                                            data-toggle="tooltip" OnCommand="btncommand" title="แก้ไขข่าวไอที">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                        <asp:LinkButton ID="btndeletNews" CssClass="btn btn btn-sm btn-danger" runat="server" CommandName="deleteNews"
                                            data-toggle="tooltip" OnCommand="btncommand" OnClientClick="return confirm('คุณต้องการลบข่าวไอทีนี้ใช่หรือไม่ ?')"
                                            CommandArgument='<%#Eval("u0_news_idx") %>' title="ลบข่าวไอที">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                  
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>
            </div>


        </asp:View>

        <!-- ----------- END VIEW INSERT NEWS IT --------------- -->

    </asp:MultiView>

</asp:Content>

