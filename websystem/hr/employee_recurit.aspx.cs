﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class websystem_hr_employee_recurit : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    private string ConCL = "conn_centralized";

    //dataODSP_Relation dtODSP = new dataODSP_Relation();
    data_employee _dtEmployee = new data_employee();

    //function_scan _funcScan = new function_scan();
    function_db _funcdb = new function_db();
    data_employee _dataEmployee = new data_employee();
    data_rcm_question _dtrcm = new data_rcm_question();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlGetEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee"];
    static string urlGetEmployeelist = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployeeList"];
    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    static string _urlGetHospitalOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetHospitalOld"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];

    static string _urlGetCountryList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountryList"];
    static string _urlGetProvincetList = _serviceUrl + ConfigurationManager.AppSettings["urlGetProvincetList"];
    static string _urlGetAmphurList = _serviceUrl + ConfigurationManager.AppSettings["urlGetAmphurList"];
    static string _urlGetDistList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDistList"];
    static string _urlGetApproveLonList = _serviceUrl + ConfigurationManager.AppSettings["urlGetApproveLonList"];
    static string _UrlSetEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["UrlSetEmployeeList"];
    static string _urlSetODSPList = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList"];
    static string _urlSetODSPList_Update = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList_Update"];
    static string _urlSetODSPList_Delete = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList_Delete"];
    static string _urlGetChildList = _serviceUrl + ConfigurationManager.AppSettings["urlGetChildList"];
    static string _urlGetPriorList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPriorList"];
    static string _urlGetEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetEducationList"];
    static string _urlGetTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlGetTrainList"];
    static string _urlSetUpdateChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateChildList"];
    static string _urlSetUpdatePriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdatePriorList"];
    static string _urlSetUpdateEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateEducationList"];
    static string _urlSetUpdateTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateTrainList"];
    static string _urlSetDeleteChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteChildList"];
    static string _urlSetDeletePriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeletePriorList"];
    static string _urlSetDeleteEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteEducationList"];
    static string _urlSetDeleteTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteTrainList"];
    static string _urlSetDeleteEmployeeListList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteEmployeeList"];
    static string _urlSetResetpass_employeeListList = _serviceUrl + ConfigurationManager.AppSettings["urlSetResetpass_employeeList"];
    static string _urlurlSetInsertEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsertEmployeeList"];
    static string _urlSetUpdateDetail_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateDetail_employeeList"];
    static string _urlSetApprove_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetApprove_employeeList"];
    static string _urlSetupdateapprove_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetupdateapprove_employeeList"];
    static string _urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetPlace"];
    static string _urlGetNationalityList = _serviceUrl + ConfigurationManager.AppSettings["urlGetNationalityList"];
    static string _urlGetReferList = _serviceUrl + ConfigurationManager.AppSettings["urlGetReferList"];
    static string _urlSetUpdateReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateReferList"];
    static string _urlSetDeleteReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteReferList"];
    static string _urlGetEmployeeList_Recurit = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee_RecuritList"];
    static string _urlGetEmployee_Register = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee_Register"];
    static string _urlGetselect_group_positionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetselect_group_positionList"];
    static string _urlGetselect_ma_positionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetselect_ma_positionList"];
    static string _urlSetupdate_employee_registerList = _serviceUrl + ConfigurationManager.AppSettings["urlSetupdate_employee_registerList"];
    static string _urlSetupdateapprove_RecuritList = _serviceUrl + ConfigurationManager.AppSettings["urlset_updateapprove_RecurittList"];
    static string _urlSetupdaterequest_RecurittList = _serviceUrl + ConfigurationManager.AppSettings["urlset_updaterequest_RecurittList"];
    static string _urlSetupdatetransfer_RecurittList = _serviceUrl + ConfigurationManager.AppSettings["urlset_updatetransfer_RecurittList"];

    static string _urlGetChildRegisList = _serviceUrl + ConfigurationManager.AppSettings["urlGetChildRegisList"];
    static string _urlGetPriorRegisList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPriorRegisList"];
    static string _urlGetEducationRegisList = _serviceUrl + ConfigurationManager.AppSettings["urlGetEducationRegisList"];
    static string _urlGetTrainRegisList = _serviceUrl + ConfigurationManager.AppSettings["urlGetTrainRegisList"];
    static string _urlGetReferRegisList = _serviceUrl + ConfigurationManager.AppSettings["urlGetReferRegisList"];
    static string _urlSetUpdateRegisReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateRegisReferList"];
    static string _urlSetUpdateRegisChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateRegisChildList"];
    static string _urlSetUpdateRegisPriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateRegisPriorList"];
    static string _urlSetUpdateRegisEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateRegisEducationList"];
    static string _urlSetUpdateRegisTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateRegisTrainList"];
    static string _urlSetDeleteRegisReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteRegisReferList"];
    static string _urlSetDeleteRegisChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteRegisChildList"];
    static string _urlSetDeleteRegisPriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteRegisPriorList"];
    static string _urlSetDeleteRegisEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteRegisEducationList"];
    static string _urlSetDeleteRegisTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteRegisTrainList"];

    static string _urlSelect_ScoreRecruit = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ScoreRecruit"];


    static string imgPath = ConfigurationSettings.AppSettings["path_flie_employee"];

    int emp_idx = 0;
    int defaultInt = 0;

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";
    int _tempInt = 0;
    string temp = "";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //ViewState["EmpIDX"] =    Session["emp_idx"].ToString();
             ViewState["EmpIDX"] = 173;
    litDebug.Text = Session["emp_idx"].ToString();
            select_empIdx_present();
            Set_Permission();

            //Select_Employee_Approve();
            //nav_approve.Text = "รายการอนุมัติ <span class='badge progress-bar-danger' >" + ViewState["list_approve"].ToString() + "</span>";

            if (Request.Form["identity_card"] != null)
            {
                ViewState["identity_card_m0toidx"] = Request.Form["identity_card"].ToString();
                ViewState["CempIDX_m0toidx"] = Request.Form["CempIDX"].ToString();
                SetDefaultApprove();
                boxviewApprove.Focus();
            }
            else
            {
                ViewState["CempIDX_m0toidx"] = "0";
            }
        }

        linkBtnTrigger(lbladdscore);
    }

    #region reuse
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void SetDefaultApprove()
    {
        MvMaster.SetActiveView(ViewEdit);
        FvEdit_Detail.ChangeMode(FormViewMode.Edit);
        FvTemplate_print.ChangeMode(FormViewMode.ReadOnly);
        FvViewApprove.ChangeMode(FormViewMode.Insert);

        Select_Fv_Detail(FvEdit_Detail, ViewState["identity_card_m0toidx"].ToString());
        Select_Fv_Detail(FvTemplate_print, ViewState["identity_card_m0toidx"].ToString());

        Label lblu0_unidx_edit = (Label)FvEdit_Detail.FindControl("lblu0_unidx");
        Label lblu0_acidx_edit = (Label)FvEdit_Detail.FindControl("lblu0_acidx");
        Label lblu0_doc_decision_edit = (Label)FvEdit_Detail.FindControl("lblu0_doc_decision");
        Label lblSRIDX = (Label)FvEdit_Detail.FindControl("lblSRIDX");

        if ((int.Parse(lblu0_unidx_edit.Text) == 9 && int.Parse(lblu0_acidx_edit.Text) == 3) && int.Parse(lblu0_doc_decision_edit.Text) == 2) // Approve Secess
        {
            lbnrequest_register.Visible = false;
            lbntransfer_register.Visible = true;
        }
        else if ((int.Parse(lblu0_unidx_edit.Text) == 2 && int.Parse(lblu0_acidx_edit.Text) == 1) || (int.Parse(lblu0_unidx_edit.Text) == 3 && int.Parse(lblu0_acidx_edit.Text) == 3)) // รอดำเนินการโดย HR จากผู้สมัคร
        {
            lbnrequest_register.Visible = false;
            lbntransfer_register.Visible = false;
        }
        else
        {
            lbnrequest_register.Visible = true;
            lbntransfer_register.Visible = false;
        }

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    #region SELECT

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void select_empIdx_present()
    {
        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["jobgrade_level"] = _dtEmployee.employee_list[0].jobgrade_level;
        ViewState["return_code"] = 0;
        ViewState["Gv_emp_idx"] = 0;
        ViewState["Box_dataEmployee"] = null;
        ViewState["Box_dataEmployee_Report"] = null;
        ViewState["list_approve"] = 0;
        Fv_Search_Emp_Index.ChangeMode(FormViewMode.Insert);
        Fv_Search_Emp_Index.DataBind();
    }

    protected void Set_Permission()
    {
        if (ViewState["Sec_idx"].ToString() == "28811" || ViewState["Sec_idx"].ToString() == "436" || ViewState["Sec_idx"].ToString() == "21" || ViewState["EmpIDX"].ToString() == "173" || ViewState["EmpIDX"].ToString() == "24047" || ViewState["EmpIDX"].ToString() == "23069" || ViewState["EmpIDX"].ToString() == "172") //Admin
        {
           //// litDebug.Text = "1";
            lbindex.Visible = true;
            lbscreen.Visible = true;
            lbrequest.Visible = true;
            lbapprove.Visible = true;
            lblreport.Visible = true;

            Select_Employee_index();
            Menu_Color(1);
            MvMaster.SetActiveView(ViewIndex);
        }
        else
        {
            ////litDebug.Text = "2";
            lbindex.Visible = false;
            lbscreen.Visible = true;
            lbrequest.Visible = true;
            lbapprove.Visible = true;
            lblreport.Visible = false;

            Menu_Color(2);
            MvMaster.SetActiveView(ViewScreen);
            Set_Defult_Screen();
        }
    }

    protected void Set_Defult_Screen()
    {
        Fv_Search_screen_Index.ChangeMode(FormViewMode.Insert);
        Fv_Search_screen_Index.DataBind();

        CheckBoxList YrChkBox = (CheckBoxList)Fv_Search_screen_Index.FindControl("YrChkBox");
        ArrayList TypeStatus = new ArrayList() { "ประจำ", "Part-time", "Freelance", "ฝึกงาน" };

        YrChkBox.Items.Clear();
        YrChkBox.AppendDataBoundItems = true;

        YrChkBox.DataSource = TypeStatus;
        YrChkBox.DataBind();

        CheckBoxList Chkdriver = (CheckBoxList)Fv_Search_screen_Index.FindControl("Chkdriver");
        ArrayList Typedriver = new ArrayList() { "รถยนต์", "รถจักรยานยนต์", "รถบรรทุก", "รถกระบะ", "รถฟอร์คลิฟท์" };

        Chkdriver.Items.Clear();
        Chkdriver.AppendDataBoundItems = true;

        Chkdriver.DataSource = Typedriver;
        Chkdriver.DataBind();
    }

    protected void Select_Employee_index()
    {
        DropDownList ddlstartdate_in_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlstartdate_in_s");
        DropDownList ddltypecategory_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddltypecategory_s");

        //DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlemptype_s");
        TextBox txtidcard_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtidcard_s");
        TextBox txtname_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtname_s");
        DropDownList ddlpositionfocus_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlpositionfocus_s");
        DropDownList ddlposition_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlposition_s");
        //TextBox txtsalary_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtsalary_s");
        DropDownList ddlstatus_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlstatus_s");

        _dataEmployee = new data_employee();
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.EmpINID = int.Parse(ddlstartdate_in_s.SelectedValue); //

        if (txtname_s.Text != "")
        {
            _dataEmployee_.emp_name_th = txtname_s.Text; //
        }
        else
        {
            _dataEmployee_.emp_name_th = "";
        }

        _dataEmployee_.identity_card = txtidcard_s.Text;
        _dataEmployee_.JobTypeIDX = int.Parse(ddltypecategory_s.SelectedValue);
        _dataEmployee_.PosGrop1 = int.Parse(ddlpositionfocus_s.SelectedValue);
        _dataEmployee_.PIDX = int.Parse(ddlposition_s.SelectedValue);
        _dataEmployee_.emp_status = int.Parse(ddlstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 1; //Index   

        _dataEmployee.employee_list[0] = _dataEmployee_;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dataEmployee));

        //_dataEmployee = callServiceEmployee(_urlGetEmployeeList_Recurit, _dataEmployee);
        ViewState["Box_dataEmployee_index_"] = _dataEmployee.employee_list;
        setGridData(GvEmployee, ViewState["Box_dataEmployee_index_"]);

        employee_detail[] _templist_cheack = (employee_detail[])ViewState["Box_dataEmployee_index_"];
        var _linqCheck = (from dataMat in _templist_cheack

                          select new
                          {
                              dataMat
                          }).ToList();

        lblsum_list_index.Text = "จำนวน : " + _linqCheck.Count().ToString() + " รายการ / ทั้งหมด : " + GvEmployee.PageCount.ToString() + " หน้า";
    }

    protected void Select_Employee_Approve()
    {
        /*DropDownList ddlstartdate_in_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlstartdate_in_s");
        DropDownList ddltypecategory_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddltypecategory_s");

        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlemptype_s");
        TextBox txtidcard_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtidcard_s");
        TextBox txtname_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtname_s");
        DropDownList ddlpositionfocus_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlpositionfocus_s");
        DropDownList ddlposition_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlposition_s");
        TextBox txtsalary_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtsalary_s");
        DropDownList ddlstatus_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlstatus_s");*/

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        /*_dataEmployee_.EmpINID = int.Parse(ddlstartdate_in_s.SelectedValue); //

        if (txtname_s.Text != "")
        {
            _dataEmployee_.emp_name_th = txtname_s.Text; //
        }
        else
        {
            _dataEmployee_.emp_name_th = "";
        }

        _dataEmployee_.identity_card = txtidcard_s.Text;
        _dataEmployee_.JobTypeIDX = int.Parse(ddltypecategory_s.SelectedValue);
        _dataEmployee_.PosGrop1 = int.Parse(ddlpositionfocus_s.SelectedValue);
        _dataEmployee_.PIDX = int.Parse(ddlposition_s.SelectedValue);*/
        _dataEmployee_.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
        _dataEmployee_.emp_status = 1; // int.Parse(ddlstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 6; //Index   

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(_urlGetEmployeeList_Recurit, _dataEmployee);
        ViewState["Box_dataEmployee_Approve_"] = _dataEmployee.employee_list;
        setGridData(GvApprove, _dataEmployee.employee_list);

        if (ViewState["Box_dataEmployee_Approve_"] != null)
        {
            employee_detail[] _templist_cheack = (employee_detail[])ViewState["Box_dataEmployee_Approve_"];
            var _linqCheck = (from dataMat in _templist_cheack

                              select new
                              {
                                  dataMat
                              }).ToList();
            try
            {
                sum_approve.Text = "จำนวน : " + _linqCheck.Count().ToString() + " รายการ / ทั้งหมด : " + GvApprove.PageCount.ToString() + " หน้า";
                ViewState["list_approve"] = _linqCheck.Count().ToString();
            }
            catch (Exception ex)
            {
                sum_approve.Text = "จำนวน : " + "0" + " รายการ / ทั้งหมด : " + "0" + " หน้า";
            }
        }
    }

    protected void Select_Score(GridView gvName, int empidx)
    {
        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_topicquest = new m0_topicquest[1];
        m0_topicquest topic = new m0_topicquest();

        topic.CEmpIDX = empidx;
        _dtrcm.Boxm0_topicquest[0] = topic;
        
        _dtrcm = callServicePostRCM(_urlSelect_ScoreRecruit, _dtrcm);
        setGridData(gvName, _dtrcm.Boxm0_topicquest);
    }


    protected void Select_Employee_index_search()
    {
        DropDownList ddlstartdate_in_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlstartdate_in_s");
        DropDownList ddltypecategory_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddltypecategory_s");

        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlemptype_s");
        TextBox txtidcard_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtidcard_s");
        TextBox txtname_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtname_s");
        DropDownList ddlpositionfocus_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlpositionfocus_s");
        DropDownList ddlposition_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlposition_s");
        TextBox txtsalary_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtsalary_s");
        DropDownList ddlstatus_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlstatus_s");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.EmpINID = int.Parse(ddlstartdate_in_s.SelectedValue); //

        if (txtname_s.Text != "")
        {
            _dataEmployee_.emp_name_th = txtname_s.Text; //
        }
        else
        {
            _dataEmployee_.emp_name_th = "";
        }

        _dataEmployee_.identity_card = txtidcard_s.Text;
        _dataEmployee_.JobTypeIDX = int.Parse(ddltypecategory_s.SelectedValue);
        _dataEmployee_.PosGrop1 = int.Parse(ddlpositionfocus_s.SelectedValue);
        _dataEmployee_.PIDX = int.Parse(ddlposition_s.SelectedValue);
        _dataEmployee_.emp_status = int.Parse(ddlstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 2; //Index   

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlGetEmployeeList_Recurit, _dataEmployee);
        ViewState["Box_dataEmployee_index"] = _dataEmployee.employee_list;

        if (ViewState["Box_dataEmployee_index"] != null)
        {
            setGridData(GvEmployee, _dataEmployee.employee_list);
        }
        else
        {
            setGridData(GvEmployee, null);
        }
    }

    protected void Select_Employee_screen_search()
    {
        DropDownList ddlgroup_screen = (DropDownList)Fv_Search_screen_Index.FindControl("ddlgroup_screen");
        DropDownList ddlposition_screen = (DropDownList)Fv_Search_screen_Index.FindControl("ddlposition_screen");
        DropDownList ddlexperiences_screen = (DropDownList)Fv_Search_screen_Index.FindControl("ddlexperiences_screen");
        DropDownList ddlsalary_screen = (DropDownList)Fv_Search_screen_Index.FindControl("ddlsalary_screen");
        DropDownList ddlstartdate_in = (DropDownList)Fv_Search_screen_Index.FindControl("ddlstartdate_in");

        TextBox txtgroup_screen = (TextBox)Fv_Search_screen_Index.FindControl("txtgroup_screen");
        TextBox txt_name_th = (TextBox)Fv_Search_screen_Index.FindControl("txt_name_th");
        TextBox txt_surname_th = (TextBox)Fv_Search_screen_Index.FindControl("txt_surname_th");
        DropDownList ddlage_screen = (DropDownList)Fv_Search_screen_Index.FindControl("ddlage_screen");
        DropDownList ddlsex_screen = (DropDownList)Fv_Search_screen_Index.FindControl("ddlsex_screen");
        DropDownList ddleducation_screen = (DropDownList)Fv_Search_screen_Index.FindControl("ddleducation_screen");
        TextBox txtbranch_screen = (TextBox)Fv_Search_screen_Index.FindControl("txtbranch_screen");
        TextBox txtunivercity_screen = (TextBox)Fv_Search_screen_Index.FindControl("txtunivercity_screen");

        CheckBoxList Chkdriver = (CheckBoxList)Fv_Search_screen_Index.FindControl("Chkdriver");
        CheckBoxList YrChkBox = (CheckBoxList)Fv_Search_screen_Index.FindControl("YrChkBox");


        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.PosGrop1 = int.Parse(ddlgroup_screen.SelectedValue);
        _dataEmployee_.PIDX = int.Parse(ddlposition_screen.SelectedValue);
        _dataEmployee_.rpos_idx = int.Parse(ddlexperiences_screen.SelectedValue);
        _dataEmployee_.salary = int.Parse(ddlsalary_screen.SelectedValue);
        _dataEmployee_.EmpINID = int.Parse(ddlstartdate_in.SelectedValue);

        _dataEmployee_.PosGroupNameTH = txtgroup_screen.Text;
        _dataEmployee_.emp_name_th = txt_name_th.Text;
        _dataEmployee_.emp_lastname_th = txt_surname_th.Text;
        _dataEmployee_.brh_idx = int.Parse(ddlage_screen.SelectedValue);
        _dataEmployee_.sex_idx = int.Parse(ddlsex_screen.SelectedValue);
        _dataEmployee_.EDUIDX = int.Parse(ddleducation_screen.SelectedValue);
        _dataEmployee_.btype_name_en = txtbranch_screen.Text;
        _dataEmployee_.btype_name_th = txtunivercity_screen.Text;

        List<String> CkDrirList = new List<string>();
        foreach (ListItem item in Chkdriver.Items)
        {
            if (item.Selected)
            {
                CkDrirList.Add(item.Value);
            }
        }
        String ckcars = String.Join(",", CkDrirList.ToArray());
        _dataEmployee_.ckcars_search = ckcars.ToString();

        List<String> YrStrList = new List<string>();
        foreach (ListItem item in YrChkBox.Items)
        {
            if (item.Selected)
            {
                YrStrList.Add(item.Value);
            }
        }
        String Ckjobstype = String.Join(",", YrStrList.ToArray());
        _dataEmployee_.ckjobs_search = Ckjobstype.ToString();


        _dataEmployee_.emp_status = 1;//int.Parse(ddlstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 3; //Index   

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //Label99.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlGetEmployeeList_Recurit, _dataEmployee);
        ViewState["Box_dataEmployee_screen"] = _dataEmployee.employee_list;

        if (ViewState["Box_dataEmployee_screen"] != null)
        {
            setGridData(GvEmployee_Screen, _dataEmployee.employee_list);
        }
        else
        {
            setGridData(GvEmployee_Screen, null);
        }
    }

    protected void Select_Employee_request_search()
    {
        /*DropDownList ddlstartdate_in_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlstartdate_in_s");
        DropDownList ddltypecategory_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddltypecategory_s");

        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlemptype_s");
        TextBox txtidcard_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtidcard_s");
        TextBox txtname_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtname_s");
        DropDownList ddlpositionfocus_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlpositionfocus_s");
        DropDownList ddlposition_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlposition_s");
        TextBox txtsalary_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtsalary_s");
        DropDownList ddlstatus_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlstatus_s");*/

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        /*_dataEmployee_.EmpINID = int.Parse(ddlstartdate_in_s.SelectedValue); //

        if (txtname_s.Text != "")
        {
            _dataEmployee_.emp_name_th = txtname_s.Text; //
        }
        else
        {
            _dataEmployee_.emp_name_th = "";
        }

        _dataEmployee_.identity_card = txtidcard_s.Text;
        _dataEmployee_.JobTypeIDX = int.Parse(ddltypecategory_s.SelectedValue);
        _dataEmployee_.PosGrop1 = int.Parse(ddlpositionfocus_s.SelectedValue);
        _dataEmployee_.PIDX = int.Parse(ddlposition_s.SelectedValue);*/
        _dataEmployee_.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
        _dataEmployee_.emp_status = 1; // int.Parse(ddlstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 7; //Index   

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlGetEmployeeList_Recurit, _dataEmployee);
        ViewState["Box_dataEmployee_request"] = _dataEmployee.employee_list;
        setGridData(GvRequest, _dataEmployee.employee_list);
    }

    protected void Select_Employee_report_search()
    {
        //TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
        //TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");

        DropDownList ddlstartdate_in_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlstartdate_in_rp");
        DropDownList ddltypecategory_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddltypecategory_rp");
        DropDownList ddlpositionfocus_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpositionfocus_rp");
        DropDownList ddlgroup_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlgroup_rp");
        DropDownList ddljobgrade = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.PosGrop1 = int.Parse(ddlgroup_rp.SelectedValue);
        _dataEmployee_.PIDX = int.Parse(ddlpositionfocus_rp.SelectedValue);
        //_dataEmployee_.rpos_idx = int.Parse(ddlexperiences_screen.SelectedValue);
        //_dataEmployee_.salary = int.Parse(ddlsalary_screen.SelectedValue);
        _dataEmployee_.EmpINID = int.Parse(ddlstartdate_in_rp.SelectedValue);

        _dataEmployee_.emp_type_idx = int.Parse(ddltypecategory_rp.SelectedValue);
        _dataEmployee_.emp_status = 1; //int.Parse(ddlempstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 9; //Report   

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlGetEmployeeList_Recurit, _dataEmployee);
        ViewState["Box_dataEmployee_Report"] = _dataEmployee.employee_list;

        if (ViewState["Box_dataEmployee_Report"] != null)
        {
            setGridData(GvEmployee_Report, _dataEmployee.employee_list);
        }
        else
        {
            setGridData(GvEmployee_Report, null);
        }

    }

    protected void Select_FvEdit_Detail(int Empidx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_idx = Empidx;
        _dataEmployee_.type_select_emp = 4;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        setFormData(FvEdit_Detail, _dataEmployee.employee_list);
    }

    protected void Select_Fv_Detail(FormView FvName, String identity_card)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_code = "";
        _dataEmployee_.type_select_emp = 4;
        _dataEmployee_.identity_card = identity_card;


        _dataEmployee.employee_list[0] = _dataEmployee_;
        //Label3.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlGetEmployee_Register, _dataEmployee);
        setFormData(FvName, _dataEmployee.employee_list);
    }

    protected void Select_RptLog_Detail(int Empidx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_idx = Empidx;
        _dataEmployee_.type_select_emp = 5;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(_urlGetEmployeeList_Recurit, _dataEmployee);
        //fsa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        setRptData(rptLog, _dataEmployee.employee_list);
    }

    protected void Select_Coscenter(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือก CostCenter...", "0"));

        _dataEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        _dataEmployee.CostCenterDetail[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetCostcenterOld, _dataEmployee);

        ddlName.DataSource = _dataEmployee.CostCenterDetail;
        ddlName.DataTextField = "costcenter_name";
        ddlName.DataValueField = "CostIDX";
        ddlName.DataBind();
    }

    protected void Select_Location(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("ประจำสำนักงาน....", "0"));

        _dtEmployee = new data_employee();

        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail search = new employee_detail();

        search.org_idx = int.Parse(ViewState["Org_idx"].ToString());

        _dtEmployee.employee_list[0] = search;

        _dtEmployee = callServiceEmployee(_urlSelect_Location, _dtEmployee);

        ddlName.DataSource = _dtEmployee.employee_list;
        ddlName.DataTextField = "LocName";
        ddlName.DataValueField = "r0idx";
        ddlName.DataBind();
    }

    protected void Select_hospital(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกโรงพยาบาล...", "0"));

        _dataEmployee.hospital_list = new Hospital[1];
        Hospital dtemployee = new Hospital();

        _dataEmployee.hospital_list[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetHospitalOld, _dataEmployee);

        ddlName.DataSource = _dataEmployee.hospital_list;
        ddlName.DataTextField = "Name";
        ddlName.DataValueField = "HosIDX";
        ddlName.DataBind();
    }

    protected void select_org(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected void select_dep(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServiceEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกฝ่าย....", "0"));
    }

    protected void select_sec(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServiceEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกหน่วยงาน....", "0"));
    }

    protected void select_pos(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dataEmployee.position_list = new position_details[1];
        position_details _posList = new position_details();
        _posList.org_idx = _org_idx;
        _posList.rdept_idx = _rdept_idx;
        _posList.rsec_idx = _rsec_idx;
        _dataEmployee.position_list[0] = _posList;

        _dataEmployee = callServiceEmployee(_urlGetPositionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกตำแหน่ง....", "0"));
    }

    protected void select_empname(DropDownList ddlName, int _rsec_idx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _orgList = new employee_detail();
        _orgList.rsec_idx = _rsec_idx;
        _orgList.type_select_emp = 8; //Index
        _dataEmployee.employee_list[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlGetEmployeeList_Recurit, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.employee_list, "emp_name_th", "emp_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกชื่อผู้อนุมัติ....", "0"));
    }

    protected void select_country(DropDownList ddlName)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        //_addresslist.org_idx = 0;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetCountryList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "country_name", "country_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกประเทศ....", "0"));
    }

    protected void select_nationality(DropDownList ddlName)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();

        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetNationalityList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "NatName", "NatIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกสัญชาติ....", "0"));
    }

    protected void select_province(DropDownList ddlName)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        //_addresslist.org_idx = 0;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetProvincetList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "prov_name", "prov_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกจังหวัด....", "0"));
    }

    protected void select_amphur(DropDownList ddlName, int prov_idx)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        _addresslist.prov_idx = prov_idx;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetAmphurList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "amp_name", "amp_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกอำเภอ....", "0"));
    }

    protected void select_dist(DropDownList ddlName, int prov_idx, int amphur_idx)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        _addresslist.prov_idx = prov_idx;
        _addresslist.amp_idx = amphur_idx;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetDistList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "dist_name", "dist_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกตำบล....", "0"));
    }

    protected void select_approve_add(DropDownList ddlName, int org_idx, int dep_idx, int sec_idx, int pos_idx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_idx = 0;
        _dataEmployee_.org_idx = org_idx;
        _dataEmployee_.rdept_idx = dep_idx;
        _dataEmployee_.rsec_idx = sec_idx;
        _dataEmployee_.rpos_idx = pos_idx;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(_urlGetApproveLonList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.employee_list, "emp_approve1", "emp_idx_approve1");
        ddlName.Items.Insert(0, new ListItem("เลือกผู้อนุมัติ ....", "0"));
    }

    protected void Select_ODSP(int emp_idx)
    {
        /*var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");

        _dataEmployee.ODSPRelation_list = new ODSP_List[1];
        ODSP_List _dataEmployee_ = new ODSP_List();

        _dataEmployee_.emp_idx = emp_idx;

        _dataEmployee.ODSPRelation_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(_urlSetODSPList, _dataEmployee);
        setGridData(GvPosAdd_View, _dataEmployee.ODSPRelation_list);
        ViewState["BoxGvPosAdd_View"] = _dataEmployee.ODSPRelation_list;*/
    }

    protected void Select_Refer_view(int emp_idx)
    {
        data_employee _dataEmployee_c = new data_employee();
        GridView GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");

        _dataEmployee_c.BoxReferent_list = new Referent_List[1];
        Referent_List _dataEmployee_1 = new Referent_List();

        _dataEmployee_1.EmpIDX = emp_idx;

        _dataEmployee_c.BoxReferent_list[0] = _dataEmployee_1;
        _dataEmployee_c = callServiceEmployee(_urlGetReferRegisList, _dataEmployee_c);
        setGridData(GvReference_View, _dataEmployee_c.BoxReferent_list);
        ViewState["BoxGvRefer_view"] = _dataEmployee_c.BoxReferent_list;
    }

    protected void Select_Child_view(int emp_idx)
    {
        data_employee _dataEmployee_c = new data_employee();
        GridView GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");

        _dataEmployee_c.BoxChild_list = new Child_List[1];
        Child_List _dataEmployee_1 = new Child_List();

        _dataEmployee_1.EmpIDX = emp_idx;

        _dataEmployee_c.BoxChild_list[0] = _dataEmployee_1;
        _dataEmployee_c = callServiceEmployee(_urlGetChildRegisList, _dataEmployee_c);
        setGridData(GvChildAdd_View, _dataEmployee_c.BoxChild_list);
        ViewState["BoxGvChild_view"] = _dataEmployee_c.BoxChild_list;
    }

    protected void Select_Prior_view(int emp_idx)
    {
        data_employee _dataEmployee_p = new data_employee();
        GridView GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");

        _dataEmployee_p.BoxPrior_list = new Prior_List[1];
        Prior_List _dataEmployee_2 = new Prior_List();

        _dataEmployee_2.EmpIDX = emp_idx;

        _dataEmployee_p.BoxPrior_list[0] = _dataEmployee_2;
        _dataEmployee_p = callServiceEmployee(_urlGetPriorRegisList, _dataEmployee_p);
        setGridData(GvPri_View, _dataEmployee_p.BoxPrior_list);
        ViewState["BoxGvPrior_view"] = _dataEmployee_p.BoxPrior_list;
    }

    protected void Select_Education_view(int emp_idx)
    {
        data_employee _dataEmployee_e = new data_employee();
        GridView GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");

        _dataEmployee_e.BoxEducation_list = new Education_List[1];
        Education_List _dataEmployee_ = new Education_List();

        _dataEmployee_.EmpIDX = emp_idx;

        _dataEmployee_e.BoxEducation_list[0] = _dataEmployee_;
        _dataEmployee_e = callServiceEmployee(_urlGetEducationRegisList, _dataEmployee_e);
        setGridData(GvEducation_View, _dataEmployee_e.BoxEducation_list);
        ViewState["BoxGvEducation_view"] = _dataEmployee_e.BoxEducation_list;
    }

    protected void Select_Train_view(int emp_idx)
    {
        data_employee _dataEmployee_t = new data_employee();
        GridView GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");

        _dataEmployee_t.BoxTrain_list = new Train_List[1];
        Train_List _dataEmployee_ = new Train_List();

        _dataEmployee_.EmpIDX = emp_idx;

        _dataEmployee_t.BoxTrain_list[0] = _dataEmployee_;
        _dataEmployee_t = callServiceEmployee(_urlGetTrainRegisList, _dataEmployee_t);
        setGridData(GvTrain_View, _dataEmployee_t.BoxTrain_list);
        ViewState["BoxGvTrain_view"] = _dataEmployee_t.BoxTrain_list;
    }

    protected void SelectGroupPositionList(DropDownList ddlName)
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetselect_group_positionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.BoxManage_Position_list, "PosGroupNameTH", "PosGroupIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกประเภทกลุ่มงาน....", "0"));
    }

    protected void SelectPositionList(DropDownList ddlName)
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetselect_ma_positionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.BoxManage_Position_list, "P_Name", "PIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกตำแหน่งงาน....", "0"));
    }

    protected string formatDateTimeCheck(string dateIN)
    {
        //return DateTime.ParseExact(dateIN, "dd/MM/yyyy", null).Month.ToString();
        return DateTime.ParseExact(dateIN, "MM", null).Month.ToString();
    }

    public void SearchDirectories(DirectoryInfo dir, String target, GridView Gv)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

            if (dt1.Rows.Count > 0)
            {

                ds1.Tables.Add(dt1);
                // gvFileLo1.Visible = true;
                Gv.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                Gv.DataBind();
                ds1.Dispose();
                ViewState["RowCountGvFile"] = dt1.Rows.Count;
            }
            else
            {

                Gv.DataSource = null;
                Gv.DataBind();

            }
        }
        catch
        {
            //ViewState["CheckFile"] = "0";
            //checkfile = "11";
        }
    }

    protected void Select_Child_print(int emp_idx)
    {
        GridView GvChildAdd_print = (GridView)FvTemplate_print.FindControl("GvChildAdd_print");
        data_employee _dataEmployee_c = new data_employee();

        _dataEmployee_c.BoxChild_list = new Child_List[1];
        Child_List _dataEmployee_1 = new Child_List();

        _dataEmployee_1.EmpIDX = emp_idx;

        _dataEmployee_c.BoxChild_list[0] = _dataEmployee_1;
        _dataEmployee_c = callServiceEmployee(_urlGetChildRegisList, _dataEmployee_c);
        setGridData(GvChildAdd_print, _dataEmployee_c.BoxChild_list);
    }

    protected void Select_Education_print(int emp_idx)
    {
        data_employee _dataEmployee_e = new data_employee();
        GridView GvEducation_print = (GridView)FvTemplate_print.FindControl("GvEducation_print");

        _dataEmployee_e.BoxEducation_list = new Education_List[1];
        Education_List _dataEmployee_ = new Education_List();

        _dataEmployee_.EmpIDX = emp_idx;

        _dataEmployee_e.BoxEducation_list[0] = _dataEmployee_;
        _dataEmployee_e = callServiceEmployee(_urlGetEducationRegisList, _dataEmployee_e);
        setGridData(GvEducation_print, _dataEmployee_e.BoxEducation_list);
        //ViewState["BoxGvEducation_view"] = _dataEmployee_e.BoxEducation_list;
    }

    protected void Select_Prior_print(int emp_idx)
    {
        data_employee _dataEmployee_p = new data_employee();
        GridView GvPri_print = (GridView)FvTemplate_print.FindControl("GvPri_print");

        _dataEmployee_p.BoxPrior_list = new Prior_List[1];
        Prior_List _dataEmployee_2 = new Prior_List();

        _dataEmployee_2.EmpIDX = emp_idx;

        _dataEmployee_p.BoxPrior_list[0] = _dataEmployee_2;
        _dataEmployee_p = callServiceEmployee(_urlGetPriorRegisList, _dataEmployee_p);
        setGridData(GvPri_print, _dataEmployee_p.BoxPrior_list);
        //ViewState["BoxGvPrior_view"] = _dataEmployee_p.BoxPrior_list;
    }

    protected void Select_Refer_print(int emp_idx)
    {
        data_employee _dataEmployee_c = new data_employee();
        GridView GvReference_print = (GridView)FvTemplate_print.FindControl("GvReference_print");

        _dataEmployee_c.BoxReferent_list = new Referent_List[1];
        Referent_List _dataEmployee_1 = new Referent_List();

        _dataEmployee_1.EmpIDX = emp_idx;

        _dataEmployee_c.BoxReferent_list[0] = _dataEmployee_1;
        _dataEmployee_c = callServiceEmployee(_urlGetReferRegisList, _dataEmployee_c);
        setGridData(GvReference_print, _dataEmployee_c.BoxReferent_list);
    }

    protected void Select_Train_print(int emp_idx)
    {
        data_employee _dataEmployee_t = new data_employee();
        GridView GvTrain_print = (GridView)FvTemplate_print.FindControl("GvTrain_print");

        _dataEmployee_t.BoxTrain_list = new Train_List[1];
        Train_List _dataEmployee_ = new Train_List();

        _dataEmployee_.EmpIDX = emp_idx;

        _dataEmployee_t.BoxTrain_list[0] = _dataEmployee_;
        _dataEmployee_t = callServiceEmployee(_urlGetTrainRegisList, _dataEmployee_t);
        setGridData(GvTrain_print, _dataEmployee_t.BoxTrain_list);
    }

    protected void select_Group_Position(DropDownList ddlName)
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List _PosList = new Manage_Position_List();

        _dataEmployee.BoxManage_Position_list[0] = _PosList;

        _dataEmployee = callServiceEmployee(_urlGetselect_group_positionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.BoxManage_Position_list, "PosGroupNameTH", "PosGroupIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกกลุ่มตำแหน่ง....", "0"));
    }

    protected void Select_MaPosition(DropDownList ddlName)
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetselect_ma_positionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.BoxManage_Position_list, "P_Name", "PIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกตำแหน่ง....", "0"));
        //text.Text = _dataEmployee.ToString();
    }

    #endregion

    #region INSERT / UPDATEEDUIDX

    protected void Update_Employee()
    {
        var lbl_emp_idx = (Label)FvEdit_Detail.FindControl("lbl_emp_idx");
        var ddlstartdate_in = (DropDownList)FvEdit_Detail.FindControl("ddlstartdate_in");
        var txtsalary = (TextBox)FvEdit_Detail.FindControl("txtsalary");
        var ddltypecategory = (DropDownList)FvEdit_Detail.FindControl("ddltypecategory");
        var ddlpositionfocus = (DropDownList)FvEdit_Detail.FindControl("ddlpositionfocus");
        var ddlposition1 = (DropDownList)FvEdit_Detail.FindControl("ddlposition1");
        var ddlposition2 = (DropDownList)FvEdit_Detail.FindControl("ddlposition2");
        var ddlposition3 = (DropDownList)FvEdit_Detail.FindControl("ddlposition3");
        var txtdescription = (TextBox)FvEdit_Detail.FindControl("txtdescription");

        var ddl_prefix_th = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_th_edit");
        var txt_name_th = (TextBox)FvEdit_Detail.FindControl("txt_name_th");
        var txt_surname_th = (TextBox)FvEdit_Detail.FindControl("txt_surname_th");
        var ddl_prefix_en = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_en_edit");
        var txt_name_en = (TextBox)FvEdit_Detail.FindControl("txt_name_en");
        var txt_surname_en = (TextBox)FvEdit_Detail.FindControl("txt_surname_en");
        var txt_nickname_th = (TextBox)FvEdit_Detail.FindControl("txt_nickname_th");
        var txt_nickname_en = (TextBox)FvEdit_Detail.FindControl("txt_nickname_en");
        //var ddlcostcenter = (DropDownList)FvEdit_Detail.FindControl("ddlcostcenter");
        var txtbirth = (TextBox)FvEdit_Detail.FindControl("txtbirth");
        var ddl_sex = (DropDownList)FvEdit_Detail.FindControl("ddl_sex");
        var ddl_status = (DropDownList)FvEdit_Detail.FindControl("ddl_status");
        var ddlnation = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
        var ddlrace = (DropDownList)FvEdit_Detail.FindControl("ddlrace");
        var ddlreligion = (DropDownList)FvEdit_Detail.FindControl("ddlreligion");
        var ddlmilitary = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary_edit");
        var txtidcard = (TextBox)FvEdit_Detail.FindControl("txtidcard");
        var txtissued_at = (TextBox)FvEdit_Detail.FindControl("txtissued_at");
        var txtexpcard = (TextBox)FvEdit_Detail.FindControl("txtexpcard");

        //var ddllocation = (DropDownList)FvEdit_Detail.FindControl("ddllocation");

        var ddldvcar = (DropDownList)FvEdit_Detail.FindControl("ddldvcar");
        var ddldvmt = (DropDownList)FvEdit_Detail.FindControl("ddldvmt");
        var ddldvfork = (DropDownList)FvEdit_Detail.FindControl("ddldvfork");
        var ddldvtruck = (DropDownList)FvEdit_Detail.FindControl("ddldvtruck");

        var txtlicens_car = (TextBox)FvEdit_Detail.FindControl("txtlicen_car");
        var txtlicens_moto = (TextBox)FvEdit_Detail.FindControl("txtlicens_moto");
        var txtlicens_fork = (TextBox)FvEdit_Detail.FindControl("txtlicens_fork");
        var txtlicens_truck = (TextBox)FvEdit_Detail.FindControl("txtlicens_truck");

        //var dllbank = (DropDownList)FvEdit_Detail.FindControl("dllbank");
        var txtaccountname = (TextBox)FvEdit_Detail.FindControl("txtaccountname");
        var txtaccountnumber = (TextBox)FvEdit_Detail.FindControl("txtaccountnumber");
        var txtemercon = (TextBox)FvEdit_Detail.FindControl("txtemercon");
        var txtrelation = (TextBox)FvEdit_Detail.FindControl("txtrelation");
        var txttelemer = (TextBox)FvEdit_Detail.FindControl("txttelemer");

        var txtfathername = (TextBox)FvEdit_Detail.FindControl("txtfathername_edit");
        var txttelfather = (TextBox)FvEdit_Detail.FindControl("txttelfather_edit");
        var txtlicenfather = (TextBox)FvEdit_Detail.FindControl("txtlicenfather_edit");
        var txtmothername = (TextBox)FvEdit_Detail.FindControl("txtmothername_edit");
        var txttelmother = (TextBox)FvEdit_Detail.FindControl("txttelmother_edit");
        var txtlicenmother = (TextBox)FvEdit_Detail.FindControl("txtlicenmother_edit");
        var txtwifename = (TextBox)FvEdit_Detail.FindControl("txtwifename_edit");
        var txttelwife = (TextBox)FvEdit_Detail.FindControl("txttelwife_edit");
        var txtlicenwifename = (TextBox)FvEdit_Detail.FindControl("txtlicenwife_edit");

        var txtchildname = (TextBox)FvEdit_Detail.FindControl("txtchildname");
        var ddlchildnumber = (DropDownList)FvEdit_Detail.FindControl("ddlchildnumber");
        var GvChildAdd = (GridView)FvEdit_Detail.FindControl("GvChildAdd");

        var txtorgold = (TextBox)FvEdit_Detail.FindControl("txtorgold_edit");
        var txtposold = (TextBox)FvEdit_Detail.FindControl("txtposold_edit");
        var txtstartdate_ex = (TextBox)FvEdit_Detail.FindControl("txtstartdate_ex");
        var txtresigndate_ex = (TextBox)FvEdit_Detail.FindControl("txtresigndate_ex");
        var txtprisalary_edit = (TextBox)FvEdit_Detail.FindControl("txtprisalary_edit");
        var txtpridescription_edit = (TextBox)FvEdit_Detail.FindControl("txtpridescription_edit");
        var GvPri = (GridView)FvEdit_Detail.FindControl("GvPri");

        var ddleducationback = (DropDownList)FvEdit_Detail.FindControl("ddleducationback");
        var txtschoolname = (TextBox)FvEdit_Detail.FindControl("txtschoolname");
        var txtstarteducation = (TextBox)FvEdit_Detail.FindControl("txtstarteducation");
        var txtendeducation = (TextBox)FvEdit_Detail.FindControl("txtendeducation");
        var txtstudy = (TextBox)FvEdit_Detail.FindControl("txtstudy");
        var GvEducation = (GridView)FvEdit_Detail.FindControl("GvEducation");

        var txttraincourses = (TextBox)FvEdit_Detail.FindControl("txttraincourses");
        var txttraindate = (TextBox)FvEdit_Detail.FindControl("txttraindate");
        var txttrainassessment = (TextBox)FvEdit_Detail.FindControl("txttrainassessment");
        var GvTrain = (GridView)FvEdit_Detail.FindControl("GvTrain");

        //------------------------------- Page 3

        var txtaddress_present = (TextBox)FvEdit_Detail.FindControl("txtaddress_present");
        var ddlcountry_present = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_present_edit");
        var ddlprovince_present = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_present_edit");
        var ddlamphoe_present = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_present_edit");
        var ddldistrict_present = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_present_edit");
        var txttelmobile_present = (TextBox)FvEdit_Detail.FindControl("txttelmobile_present");
        var txttelhome_present = (TextBox)FvEdit_Detail.FindControl("txttelhome_present");
        var txtzipcode_present = (TextBox)FvEdit_Detail.FindControl("txtzipcode_present");
        var txtemail_present = (TextBox)FvEdit_Detail.FindControl("txtemail_present");

        var txtaddress_permanentaddress = (TextBox)FvEdit_Detail.FindControl("txtaddress_permanentaddress");
        var ddlcountry_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_permanentaddress_edit");
        var ddlprovince_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_permanentaddress_edit");
        var ddlamphoe_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_permanentaddress_edit");
        var ddldistrict_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_permanentaddress_edit");
        var txttelmobile_permanentaddress = (TextBox)FvEdit_Detail.FindControl("txttelmobile_permanentaddress");
        var txttelhome_permanentaddress = (TextBox)FvEdit_Detail.FindControl("txttelhome_permanentaddress");

        //------------------------------- Page 4

        var ddlhospital = (DropDownList)FvEdit_Detail.FindControl("ddlhospital");
        var txtexaminationdate = (TextBox)FvEdit_Detail.FindControl("txtexaminationdate");
        var txtsecurityid = (TextBox)FvEdit_Detail.FindControl("txtsecurityid");
        var txtexpsecurity = (TextBox)FvEdit_Detail.FindControl("txtexpsecurity");
        var txtheight = (TextBox)FvEdit_Detail.FindControl("txtheight");
        var txtweight = (TextBox)FvEdit_Detail.FindControl("txtweight");
        var ddlblood = (DropDownList)FvEdit_Detail.FindControl("ddlblood");
        var txtscar = (TextBox)FvEdit_Detail.FindControl("txtscar");
        var ddlmedicalcertificate = (DropDownList)FvEdit_Detail.FindControl("ddlmedicalcertificate");
        var ddlresultlab = (DropDownList)FvEdit_Detail.FindControl("ddlresultlab");

        //------------------------------- Page 5

        var ddlapprove1 = (DropDownList)FvEdit_Detail.FindControl("ddlapprove1");
        var ddlapprove2 = (DropDownList)FvEdit_Detail.FindControl("ddlapprove2");

        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        int e = 0;
        int f = 0;

        var _dataEmployee_ = new employee_detail[1];
        _dataEmployee_[0] = new employee_detail();

        _dataEmployee_[0].emp_idx = int.Parse(lbl_emp_idx.Text);

        _dataEmployee_[0].EmpINID = int.Parse(ddlstartdate_in.SelectedValue);
        _dataEmployee_[0].salary = int.Parse(txtsalary.Text);
        _dataEmployee_[0].JobTypeIDX = int.Parse(ddltypecategory.SelectedValue);
        _dataEmployee_[0].PIDX = int.Parse(ddlpositionfocus.SelectedValue);
        _dataEmployee_[0].PosGrop1 = int.Parse(ddlposition1.SelectedValue);
        _dataEmployee_[0].PosGrop2 = int.Parse(ddlposition2.SelectedValue);
        _dataEmployee_[0].PosGrop3 = int.Parse(ddlposition3.SelectedValue);
        _dataEmployee_[0].DetailProfile = txtdescription.Text;

        _dataEmployee_[0].prefix_idx = int.Parse(ddl_prefix_th.SelectedValue);

        if (txt_name_th.Text != "") { _dataEmployee_[0].emp_firstname_th = txt_name_th.Text; } else { _dataEmployee_[0].emp_firstname_th = "-"; }
        if (txt_surname_th.Text != "") { _dataEmployee_[0].emp_lastname_th = txt_surname_th.Text; } else { _dataEmployee_[0].emp_lastname_th = "-"; }

        _dataEmployee_[0].emp_firstname_en = txt_name_en.Text;
        _dataEmployee_[0].emp_lastname_en = txt_surname_en.Text;
        _dataEmployee_[0].emp_nickname_th = txt_nickname_th.Text;
        _dataEmployee_[0].emp_nickname_en = txt_nickname_en.Text;
        _dataEmployee_[0].costcenter_idx = 0;//int.Parse(ddlcostcenter.SelectedValue);
        if (txtbirth.Text != "") { _dataEmployee_[0].emp_birthday = txtbirth.Text; } else { _dataEmployee_[0].emp_birthday = "01/01/1900"; }

        //_dataEmployee_[0].LocIDX = int.Parse(ddllocation.SelectedValue); //No Base

        _dataEmployee_[0].sex_idx = int.Parse(ddl_sex.SelectedValue);
        _dataEmployee_[0].married_status = int.Parse(ddl_status.SelectedValue); // สถานะแต่งงาน
        _dataEmployee_[0].nat_idx = int.Parse(ddlnation.SelectedValue);
        _dataEmployee_[0].race_idx = int.Parse(ddlrace.SelectedValue);
        _dataEmployee_[0].rel_idx = int.Parse(ddlreligion.SelectedValue);
        _dataEmployee_[0].mil_idx = int.Parse(ddlmilitary.SelectedValue); // สถานะทางทหาร
        _dataEmployee_[0].identity_card = txtidcard.Text; // รหัสบัตรประชน
        _dataEmployee_[0].issued_at = txtissued_at.Text; // สถานที่ออกบัตร
        if (txtexpcard.Text != "") { _dataEmployee_[0].idate_expired = txtexpcard.Text; } else { _dataEmployee_[0].idate_expired = "01/01/1900"; } // วันที่หมดอายุ

        _dataEmployee_[0].dvcar_idx = int.Parse(ddldvcar.SelectedValue); // สถานะขับขี่รถยนต์ - New Table
        _dataEmployee_[0].dvmt_idx = int.Parse(ddldvmt.SelectedValue); // สถานะขับขี่รถมอเตอร์ไซ - New Table
        _dataEmployee_[0].dvfork_idx = int.Parse(ddldvfork.SelectedValue); //  No Base
        _dataEmployee_[0].dvtruck_idx = int.Parse(ddldvtruck.SelectedValue); //  No Base
        _dataEmployee_[0].dvlicen_car = txtlicens_car.Text; //  No Base
        _dataEmployee_[0].dvlicen_moto = txtlicens_moto.Text; //  No Base
        _dataEmployee_[0].dvlicen_fork = txtlicens_fork.Text; //  No Base
        _dataEmployee_[0].dvlicen_truck = txtlicens_truck.Text; //  No Base

        _dataEmployee_[0].emer_name = txtemercon.Text; // กรณีฉุกเฉินติดต่อ
        _dataEmployee_[0].emer_relation = txtrelation.Text; // ความสัมพันธ์
        _dataEmployee_[0].emer_tel = txttelemer.Text; // เบอร์โทร

        _dataEmployee_[0].fathername = txtfathername.Text; // ชื่อบิดา
        _dataEmployee_[0].telfather = txttelfather.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_father = txtlicenfather.Text; //  No Base
        _dataEmployee_[0].mothername = txtmothername.Text; // ชื่อมารดา
        _dataEmployee_[0].telmother = txttelmother.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_mother = txtlicenmother.Text; //  No Base
        _dataEmployee_[0].wifename = txtwifename.Text; // ชื่อภรรยา
        _dataEmployee_[0].telwife = txttelwife.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_wife = txtlicenwifename.Text; //  No Base

        _dataEmployee_[0].emp_address = txtaddress_present.Text; // ******************* ที่อยู่ปัจจุบัน
        _dataEmployee_[0].country_idx = int.Parse(ddlcountry_present.SelectedValue);
        _dataEmployee_[0].prov_idx = int.Parse(ddlprovince_present.SelectedValue);
        _dataEmployee_[0].amp_idx = int.Parse(ddlamphoe_present.SelectedValue);
        _dataEmployee_[0].dist_idx = int.Parse(ddldistrict_present.SelectedValue);
        _dataEmployee_[0].emp_mobile_no = txttelmobile_present.Text;  // ******
        _dataEmployee_[0].emp_phone_no = txttelhome_present.Text;   // ******
        _dataEmployee_[0].post_code = txtzipcode_present.Text;
        _dataEmployee_[0].emp_email = txtemail_present.Text;

        _dataEmployee_[0].emp_address_permanent = txtaddress_permanentaddress.Text;  // ******************* ที่อยู่ตามทะเบียนบ้าน
        _dataEmployee_[0].country_idx_permanent = int.Parse(ddlcountry_permanentaddress.SelectedValue);
        _dataEmployee_[0].prov_idx_permanent = int.Parse(ddlprovince_permanentaddress.SelectedValue);
        _dataEmployee_[0].amp_idx_permanent = int.Parse(ddlamphoe_permanentaddress.SelectedValue);
        _dataEmployee_[0].dist_idx_permanent = int.Parse(ddldistrict_permanentaddress.SelectedValue);
        _dataEmployee_[0].PhoneNumber_permanent = txttelmobile_permanentaddress.Text;  // ******

        _dataEmployee_[0].soc_hos_idx = int.Parse(ddlhospital.SelectedValue);  // โรงพยาบาล
        if (txtexaminationdate.Text != "") { _dataEmployee_[0].examinationdate = txtexaminationdate.Text; } else { _dataEmployee_[0].examinationdate = "01/01/1900"; } // วันที่ตรวจสุขภาพ
        _dataEmployee_[0].soc_no = txtsecurityid.Text;  // รหัสบัตรประกันสังคม       
        if (txtexpsecurity.Text != "") { _dataEmployee_[0].soc_expired_date = txtexpsecurity.Text; } else { _dataEmployee_[0].soc_expired_date = "01/01/1900"; } // วันหมดอายุประกันสังคม

        if (txtheight.Text == "") { _dataEmployee_[0].height_s = 0; } else { _dataEmployee_[0].height_s = int.Parse(txtheight.Text); } // ส่วนสูง
        if (txtweight.Text == "") { _dataEmployee_[0].weight_s = 0; } else { _dataEmployee_[0].weight_s = int.Parse(txtweight.Text); } // น้ำหนัก  

        _dataEmployee_[0].btype_idx = int.Parse(ddlblood.SelectedValue);  // กรุ๊ปเลือด
        _dataEmployee_[0].Scar = txtscar.Text;  // แผลเป้น
        _dataEmployee_[0].medicalcertificate_id = int.Parse(ddlmedicalcertificate.SelectedValue); //ใบรับรอง
        _dataEmployee_[0].resultlab_id = int.Parse(ddlresultlab.SelectedValue); // ผลตรวจ LAB

        _dtEmployee.employee_list = _dataEmployee_;

        if (ViewState["vsTemp_Refer_edit"] != null)
        {
            var dsrefer = (DataSet)ViewState["vsTemp_Refer_edit"];
            var Addrefer = new Referent_List[dsrefer.Tables[0].Rows.Count];
            foreach (DataRow dr in dsrefer.Tables[0].Rows)
            {
                Addrefer[f] = new Referent_List();
                Addrefer[f].ref_fullname = dr["ref_fullname"].ToString();
                Addrefer[f].ref_position = dr["ref_position"].ToString();
                Addrefer[f].ref_relation = dr["ref_relation"].ToString();
                Addrefer[f].ref_tel = dr["ref_tel"].ToString();

                f++;
            }

            if (dsrefer.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxReferent_list = Addrefer;
            }
        }


        if (ViewState["vsTemp_Child_edit"] != null)
        {
            var dschild = (DataSet)ViewState["vsTemp_Child_edit"];
            var AddChild = new Child_List[dschild.Tables[0].Rows.Count];
            foreach (DataRow dr in dschild.Tables[0].Rows)
            {
                AddChild[a] = new Child_List();
                AddChild[a].Child_name = dr["Child_name"].ToString(); //ชื่อบัตร
                AddChild[a].Child_num = dr["Child_num"].ToString(); //จำนวนบุตร

                a++;
            }

            if (dschild.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxChild_list = AddChild;
            }
        }


        if (ViewState["vsTemp_Prior_edit"] != null)
        {
            var dsPrior = (DataSet)ViewState["vsTemp_Prior_edit"];
            var AddPrior = new Prior_List[dsPrior.Tables[0].Rows.Count];
            foreach (DataRow dr in dsPrior.Tables[0].Rows)
            {
                AddPrior[b] = new Prior_List();
                AddPrior[b].Pri_Nameorg = dr["Pri_Nameorg"].ToString();
                AddPrior[b].Pri_pos = dr["Pri_pos"].ToString();
                AddPrior[b].Pri_startdate = dr["Pri_startdate"].ToString();
                AddPrior[b].Pri_resigndate = dr["Pri_resigndate"].ToString();
                AddPrior[b].Pri_description = dr["Pri_description"].ToString();
                AddPrior[b].Pri_salary = dr["Pri_salary"].ToString();

                b++;
            }

            if (dsPrior.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxPrior_list = AddPrior;
            }
        }


        if (ViewState["vsTemp_Education_edit"] != null)
        {
            var dsEdu = (DataSet)ViewState["vsTemp_Education_edit"];
            var AddEdu = new Education_List[dsEdu.Tables[0].Rows.Count];
            foreach (DataRow dr in dsEdu.Tables[0].Rows)
            {
                AddEdu[c] = new Education_List();
                AddEdu[c].Edu_qualification_ID = dr["Edu_qualification_ID"].ToString();
                AddEdu[c].Edu_qualification = dr["Edu_qualification"].ToString();
                AddEdu[c].Edu_name = dr["Edu_name"].ToString();
                AddEdu[c].Edu_branch = dr["Edu_branch"].ToString();
                AddEdu[c].Edu_start = dr["Edu_start"].ToString();
                AddEdu[c].Edu_end = dr["Edu_end"].ToString();

                c++;
            }

            if (dsEdu.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxEducation_list = AddEdu;
            }
        }


        if (ViewState["vsTemp_Train_edit"] != null)
        {
            var dsTrain = (DataSet)ViewState["vsTemp_Train_edit"];
            var AddTrain = new Train_List[dsTrain.Tables[0].Rows.Count];
            foreach (DataRow dr in dsTrain.Tables[0].Rows)
            {
                AddTrain[d] = new Train_List();
                AddTrain[d].Train_courses = dr["Train_courses"].ToString();
                AddTrain[d].Train_date = dr["Train_date"].ToString();
                AddTrain[d].Train_assessment = dr["Train_assessment"].ToString();

                d++;
            }

            if (dsTrain.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxTrain_list = AddTrain;
            }
        }


        try
        {
            //Label2.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
            _dtEmployee = callServiceEmployee_Check(_urlSetupdate_employee_registerList, _dtEmployee);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
        catch (FileNotFoundException ex)
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }

    protected void Update_Approve()
    {
        var ddl_view_Approve = (DropDownList)FvViewApprove.FindControl("ddl_view_Approve");
        var tbDocRemark = (TextBox)FvViewApprove.FindControl("tbDocRemark");
        var txt_startdate = (TextBox)FvViewApprove.FindControl("txt_startdate");
        var txt_enddate = (TextBox)FvViewApprove.FindControl("txt_enddate");
        var u0_unidx = (Label)FvEdit_Detail.FindControl("lblu0_unidx");
        var u0_acidx = (Label)FvEdit_Detail.FindControl("lblu0_acidx");
        var u0_doc_decision = (Label)FvEdit_Detail.FindControl("lblu0_doc_decision");
        var ddlempidx_s = (DropDownList)FvViewApprove.FindControl("ddlempidx_s");
        var lblSRIDX = (Label)FvEdit_Detail.FindControl("lblSRIDX");

        var _dataEmployee_ = new employee_detail[1];
        _dataEmployee_[0] = new employee_detail();

        _dataEmployee_[0].emp_idx = int.Parse(ViewState["fv_emp_idx"].ToString());
        _dataEmployee_[0].comment_approve = tbDocRemark.Text;
        _dataEmployee_[0].u0_unidx = int.Parse(u0_unidx.Text);
        _dataEmployee_[0].u0_acidx = int.Parse(u0_acidx.Text);
        _dataEmployee_[0].Approve_status = int.Parse(ddl_view_Approve.SelectedValue);
        _dataEmployee_[0].sridx = int.Parse(lblSRIDX.Text);
        _dataEmployee_[0].HCEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        if (int.Parse(u0_unidx.Text) == 2 && int.Parse(u0_acidx.Text) == 1) //กรณีเจ้าหน้าที่ HR อนุมัติ
        {
            if (int.Parse(ddl_view_Approve.SelectedValue) == 2) //Approve
            {
                _dataEmployee_[0].u0_doc_decision = 1;
            }
            else
            {
                _dataEmployee_[0].u0_doc_decision = int.Parse(ddl_view_Approve.SelectedValue);
            }
        }
        else //กรณีเจ้าหน้าที่สัมภาษณ์อนุมัติ
        {
            _dataEmployee_[0].u0_doc_decision = int.Parse(ddl_view_Approve.SelectedValue);
        }



        if (int.Parse(u0_acidx.Text) == 1 && int.Parse(lblSRIDX.Text) == 1 && int.Parse(u0_unidx.Text) == 2) //เจ้าหน้าที่ HR / สมัครใหม่
        {
            if (int.Parse(ddlempidx_s.SelectedValue) != 0)
            {
                _dataEmployee_[0].emp_idx_approve1 = int.Parse(ddlempidx_s.SelectedValue);
            }
        }
        else if (int.Parse(u0_acidx.Text) == 1 && int.Parse(lblSRIDX.Text) == 2 && int.Parse(u0_unidx.Text) == 3) //เจ้าหน้าที่ HR / Request / เพิ่มเงื่อนไขตอน Approve
        {
            if (int.Parse(ddlempidx_s.SelectedValue) != 0)
            {
                _dataEmployee_[0].emp_idx_approve1 = int.Parse(ddlempidx_s.SelectedValue);
            }
        }
        else if (int.Parse(u0_acidx.Text) == 3 && int.Parse(lblSRIDX.Text) == 2 && int.Parse(u0_unidx.Text) == 2) //เจ้าหน้าที่สัมภาษณ์ / Request
        {
            if (int.Parse(ddlempidx_s.SelectedValue) != 0)
            {
                _dataEmployee_[0].emp_idx_approve1 = int.Parse(ViewState["EmpIDX"].ToString());
            }
        }

        if (txt_startdate.Text != "")
        {
            _dataEmployee_[0].startdate_search = txt_startdate.Text;
        }

        if (txt_enddate.Text != "")
        {
            _dataEmployee_[0].enddate_search = txt_enddate.Text;
        }

        _dtEmployee.employee_list = _dataEmployee_;
        Label3.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
        _dtEmployee = callServiceEmployee(_urlSetupdateapprove_RecuritList, _dtEmployee);
    }

    protected void Update_Request()
    {
        var ddl_view_Approve = (DropDownList)FvViewApprove.FindControl("ddl_view_Approve");
        var tbDocRemark = (TextBox)FvViewApprove.FindControl("tbDocRemark");
        var u0_unidx = (Label)FvEdit_Detail.FindControl("lblu0_unidx");
        var u0_acidx = (Label)FvEdit_Detail.FindControl("lblu0_acidx");
        var u0_doc_decision = (Label)FvEdit_Detail.FindControl("lblu0_doc_decision");
        var lblSRIDX = (Label)FvEdit_Detail.FindControl("lblSRIDX");

        var _dataEmployee_ = new employee_detail[1];
        _dataEmployee_[0] = new employee_detail();

        _dataEmployee_[0].emp_idx = int.Parse(ViewState["fv_emp_idx"].ToString());
        _dataEmployee_[0].HCEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dataEmployee_[0].comment_approve = "Request";
        _dataEmployee_[0].u0_unidx = int.Parse(u0_unidx.Text);
        _dataEmployee_[0].u0_acidx = int.Parse(u0_acidx.Text);
        _dataEmployee_[0].Approve_status = int.Parse(ddl_view_Approve.SelectedValue);
        _dataEmployee_[0].sridx = int.Parse(lblSRIDX.Text);

        if (int.Parse(ddl_view_Approve.SelectedValue) == 2) //Approve
        {
            _dataEmployee_[0].u0_doc_decision = 1;
        }
        else
        {
            _dataEmployee_[0].u0_doc_decision = int.Parse(ddl_view_Approve.SelectedValue);
        }

        _dtEmployee.employee_list = _dataEmployee_;
        //Label3.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
        _dtEmployee = callServiceEmployee(_urlSetupdaterequest_RecurittList, _dtEmployee);
    }

    protected void Update_Tranfer()
    {
        var txt_startdate = (TextBox)FvTranfer.FindControl("txt_startdate");

        var _dataEmployee_ = new employee_detail[1];
        _dataEmployee_[0] = new employee_detail();

        _dataEmployee_[0].emp_idx = int.Parse(ViewState["fv_emp_idx"].ToString());
        _dataEmployee_[0].startdate_search = txt_startdate.Text;

        _dtEmployee.employee_list = _dataEmployee_;
        //ffs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
        _dtEmployee = callServiceEmployee(_urlSetupdatetransfer_RecurittList, _dtEmployee);
    }
    #endregion

    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "Fv_Search_Emp_Index":

                DropDownList ddlpositionfocus_index = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlpositionfocus_s");
                DropDownList ddlgroup_index = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlposition_s");

                if (Fv_Search_Emp_Index.CurrentMode == FormViewMode.Insert)
                {
                    select_Group_Position(ddlgroup_index);
                    Select_MaPosition(ddlpositionfocus_index);
                }
                break;

            case "Fv_Search_screen_Index":
                DropDownList ddlgroup_screen = (DropDownList)Fv_Search_screen_Index.FindControl("ddlgroup_screen");
                DropDownList ddlposition_screen = (DropDownList)Fv_Search_screen_Index.FindControl("ddlposition_screen");

                if (Fv_Search_screen_Index.CurrentMode == FormViewMode.Insert)
                {
                    select_Group_Position(ddlgroup_screen);
                    Select_MaPosition(ddlposition_screen);
                }

                break;

            case "FvEdit_Detail":

                Label lbl_emp_idx = (Label)FvEdit_Detail.FindControl("lbl_emp_idx");

                if (ViewState["CempIDX_m0toidx"].ToString() == null || ViewState["CempIDX_m0toidx"].ToString() == "0")
                {
                    ViewState["fv_emp_idx"] = lbl_emp_idx.Text;
                }
                else
                {
                    ViewState["fv_emp_idx"] = ViewState["CempIDX_m0toidx"].ToString();
                }

                Label lbl_cost_idx = (Label)FvEdit_Detail.FindControl("lbl_cost_idx");
                DropDownList ddlmilitary_edit = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary_edit");
                Label lbl_mil_idx = (Label)FvEdit_Detail.FindControl("lbl_mil_idx");

                Label lbl_probation_status = (Label)FvEdit_Detail.FindControl("lbl_probation_status");
                Label lbl_emp_status = (Label)FvEdit_Detail.FindControl("lbl_emp_status");
                Label lblemp_in_idx = (Label)FvEdit_Detail.FindControl("lblemp_in_idx");
                Label lbljob_type_idx = (Label)FvEdit_Detail.FindControl("lbljob_type_idx");
                Label lblPosGroupIDX_1 = (Label)FvEdit_Detail.FindControl("lblPosGroupIDX_1");
                Label lblPosGroupIDX_2 = (Label)FvEdit_Detail.FindControl("lblPosGroupIDX_2");
                Label lblPosGroupIDX_3 = (Label)FvEdit_Detail.FindControl("lblPosGroupIDX_3");

                DropDownList ddlstartdate_in = (DropDownList)FvEdit_Detail.FindControl("ddlstartdate_in");
                DropDownList ddltypecategory = (DropDownList)FvEdit_Detail.FindControl("ddltypecategory");
                DropDownList ddlposition1 = (DropDownList)FvEdit_Detail.FindControl("ddlposition1");
                DropDownList ddlposition2 = (DropDownList)FvEdit_Detail.FindControl("ddlposition2");
                DropDownList ddlposition3 = (DropDownList)FvEdit_Detail.FindControl("ddlposition3");

                //ddl ข้อมูล Dropdown fix
                DropDownList ddl_prefix_th = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_th_edit");
                DropDownList ddl_prefix_en = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_en_edit");
                DropDownList ddl_sex = (DropDownList)FvEdit_Detail.FindControl("ddl_sex");
                DropDownList ddl_status = (DropDownList)FvEdit_Detail.FindControl("ddl_status");
                DropDownList ddlrace = (DropDownList)FvEdit_Detail.FindControl("ddlrace");
                DropDownList ddlreligion = (DropDownList)FvEdit_Detail.FindControl("ddlreligion");
                DropDownList ddlmilitary = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary");
                DropDownList ddldvcar = (DropDownList)FvEdit_Detail.FindControl("ddldvcar");
                DropDownList ddldvmt = (DropDownList)FvEdit_Detail.FindControl("ddldvmt");
                DropDownList ddldvfork = (DropDownList)FvEdit_Detail.FindControl("ddldvfork");
                DropDownList ddldvtruck = (DropDownList)FvEdit_Detail.FindControl("ddldvtruck");
                DropDownList ddldvcarstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvcarstatus");
                DropDownList ddldvmtstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvmtstatus");

                DropDownList ddlblood = (DropDownList)FvEdit_Detail.FindControl("ddlblood");
                DropDownList ddlmedicalcertificate = (DropDownList)FvEdit_Detail.FindControl("ddlmedicalcertificate");
                DropDownList ddlresultlab = (DropDownList)FvEdit_Detail.FindControl("ddlresultlab");

                Label lbl_prefix_idx = (Label)FvEdit_Detail.FindControl("lbl_prefix_idx");
                Label lbl_sex_idx = (Label)FvEdit_Detail.FindControl("lbl_sex_idx");
                Label lbl_married_status_idx = (Label)FvEdit_Detail.FindControl("lbl_married_status_idx");
                Label lbl_race_idx = (Label)FvEdit_Detail.FindControl("lbl_race_idx");
                Label lbl_rel_idx = (Label)FvEdit_Detail.FindControl("lbl_rel_idx");
                Label lbl_dvcar_idx = (Label)FvEdit_Detail.FindControl("lbl_dvcar_idx");
                Label lbl_dvmt_idx = (Label)FvEdit_Detail.FindControl("lbl_dvmt_idx");
                Label lbl_dvfork_idx = (Label)FvEdit_Detail.FindControl("lbl_dvfork_idx");
                Label lbl_dvtruck_idx = (Label)FvEdit_Detail.FindControl("lbl_dvtruck_idx");
                Label lbl_dvcar_status = (Label)FvEdit_Detail.FindControl("lbl_dvcar_status");
                Label lbl_dvmoto_status = (Label)FvEdit_Detail.FindControl("lbl_dvmoto_status");
                //Label lbl_bank_idx = (Label)FvEdit_Detail.FindControl("lbl_bank_idx");
                Label lbl_BRHIDX = (Label)FvEdit_Detail.FindControl("lbl_BRHIDX");
                Label lbl_medical_id = (Label)FvEdit_Detail.FindControl("lbl_medical_id");
                Label lbl_resultlab_id = (Label)FvEdit_Detail.FindControl("lbl_resultlab_id");

                //ที่อยู่ปัจจุบัน
                DropDownList ddlcountry_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_present_edit");
                DropDownList ddlprovince_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_present_edit");
                DropDownList ddlamphoe_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_present_edit");
                DropDownList ddldistrict_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_present_edit");
                Label lbl_country_edit = (Label)FvEdit_Detail.FindControl("lbl_country_edit");
                Label lbl_prov_edit = (Label)FvEdit_Detail.FindControl("lbl_prov_edit");
                Label lbl_amp_edit = (Label)FvEdit_Detail.FindControl("lbl_amp_edit");
                Label lbl_dist_edit = (Label)FvEdit_Detail.FindControl("lbl_dist_edit");

                //ที่อยู่ตามทะเบียนบ้าน
                DropDownList ddlcountry_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_permanentaddress_edit");
                DropDownList ddlprovince_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_permanentaddress_edit");
                DropDownList ddlamphoe_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_permanentaddress_edit");
                DropDownList ddldistrict_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_permanentaddress_edit");
                Label lbl_ddlcountry_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddlcountry_permanent_edit");
                Label lbl_ddlprovince_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddlprovince_permanent_edit");
                Label lbl_ddlamphoe_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddlamphoe_permanent_edit");
                Label lbl_ddldistrict_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddldistrict_permanent_edit");

                DropDownList ddlhospital_edit = (DropDownList)FvEdit_Detail.FindControl("ddlhospital");
                Label lbl_hos_edit = (Label)FvEdit_Detail.FindControl("lbl_hos_edit");
                Label lbl_location_idx = (Label)FvEdit_Detail.FindControl("lbl_location_idx");
                DropDownList ddlnation_edit = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
                Label lbl_nation_idx = (Label)FvEdit_Detail.FindControl("lbl_nation_idx");

                Image img_profile_edit = (Image)FvEdit_Detail.FindControl("img_profile_edit");
                GridView gvFileEmp_edit = (GridView)FvEdit_Detail.FindControl("gvFileEmp_edit");
                Label lblidentity_card_edit = (Label)FvEdit_Detail.FindControl("lblidentity_card_edit");
                Panel ImagProfile_default_edit = (Panel)FvEdit_Detail.FindControl("ImagProfile_default_edit");
                DropDownList ddlposition_1_edit = (DropDownList)FvEdit_Detail.FindControl("ddlposition1");
                DropDownList ddlposition_2_edit = (DropDownList)FvEdit_Detail.FindControl("ddlposition2");
                DropDownList ddlposition_3_edit = (DropDownList)FvEdit_Detail.FindControl("ddlposition3");
                DropDownList ddlpositionfocus_edit = (DropDownList)FvEdit_Detail.FindControl("ddlpositionfocus");
                Label lblPIDX_edit = (Label)FvEdit_Detail.FindControl("lblPIDX_edit");

                Label lblu0_unidx_edit = (Label)FvEdit_Detail.FindControl("lblu0_unidx");
                Label lblu0_acidx_edit = (Label)FvEdit_Detail.FindControl("lblu0_acidx");
                Label lblu0_doc_decision_edit = (Label)FvEdit_Detail.FindControl("lblu0_doc_decision");
                Panel boxviewApprove = (Panel)ViewEdit.FindControl("boxviewApprove");
                Panel pnnameapprove = (Panel)FvViewApprove.FindControl("pnnameapprove");
                Label lblSRIDX = (Label)FvEdit_Detail.FindControl("lblSRIDX");
                Label lblemp_idx_approve = (Label)FvEdit_Detail.FindControl("lblemp_idx_approve");

                //FormView FvTemplate_print_ = (FormView)FvEdit_Detail.FindControl("FvTemplate_print");

                if (FvEdit_Detail.CurrentMode == FormViewMode.Edit)
                {
                    ddlstartdate_in.SelectedValue = lblemp_in_idx.Text;
                    ddltypecategory.SelectedValue = lbljob_type_idx.Text;

                    if (lblPosGroupIDX_1.Text != "0")
                    {
                        ddlposition1.SelectedValue = lblPosGroupIDX_1.Text;
                    }

                    if (lblPosGroupIDX_2.Text != "0")
                    {
                        ddlposition2.SelectedValue = lblPosGroupIDX_2.Text;
                    }

                    if (lblPosGroupIDX_3.Text != "0")
                    {
                        ddlposition3.SelectedValue = lblPosGroupIDX_3.Text;
                    }

                    ddl_prefix_th.SelectedValue = lbl_prefix_idx.Text;
                    ddl_prefix_en.SelectedValue = lbl_prefix_idx.Text;
                    ddl_sex.SelectedValue = lbl_sex_idx.Text;
                    ddl_status.SelectedValue = lbl_married_status_idx.Text;
                    ddlrace.SelectedValue = lbl_race_idx.Text;
                    ddlreligion.SelectedValue = lbl_rel_idx.Text;
                    ddldvcar.SelectedValue = lbl_dvcar_idx.Text;
                    ddldvmt.SelectedValue = lbl_dvmt_idx.Text;
                    ddldvfork.SelectedValue = lbl_dvfork_idx.Text;
                    ddldvtruck.SelectedValue = lbl_dvtruck_idx.Text;
                    ddlblood.SelectedValue = lbl_BRHIDX.Text;
                    ddlmedicalcertificate.SelectedValue = lbl_medical_id.Text;
                    ddlresultlab.SelectedValue = lbl_resultlab_id.Text;
                    ddlmilitary_edit.SelectedValue = lbl_mil_idx.Text;

                    ddldvcarstatus.SelectedValue = lbl_dvcar_status.Text;
                    ddldvmtstatus.SelectedValue = lbl_dvmoto_status.Text;

                    Select_hospital(ddlhospital_edit);
                    ddlhospital_edit.SelectedValue = lbl_hos_edit.Text;

                    select_country(ddlcountry_present_edit);
                    ddlcountry_present_edit.SelectedValue = lbl_country_edit.Text;
                    select_province(ddlprovince_present_edit);
                    ddlprovince_present_edit.SelectedValue = lbl_prov_edit.Text;
                    select_amphur(ddlamphoe_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue));
                    ddlamphoe_present_edit.SelectedValue = lbl_amp_edit.Text;
                    select_dist(ddldistrict_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue), int.Parse(ddlamphoe_present_edit.SelectedValue));
                    ddldistrict_present_edit.SelectedValue = lbl_dist_edit.Text;

                    select_country(ddlcountry_permanentaddress_edit);
                    ddlcountry_permanentaddress_edit.SelectedValue = lbl_ddlcountry_permanent_edit.Text;
                    select_province(ddlprovince_permanentaddress_edit);
                    ddlprovince_permanentaddress_edit.SelectedValue = lbl_ddlprovince_permanent_edit.Text;
                    select_amphur(ddlamphoe_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue));
                    ddlamphoe_permanentaddress_edit.SelectedValue = lbl_ddlamphoe_permanent_edit.Text;
                    select_dist(ddldistrict_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue), int.Parse(ddlamphoe_permanentaddress_edit.SelectedValue));
                    ddldistrict_permanentaddress_edit.SelectedValue = lbl_ddldistrict_permanent_edit.Text;

                    Select_Refer_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Child_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Prior_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Education_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Train_view(int.Parse(ViewState["fv_emp_idx"].ToString()));

                    select_nationality(ddlnation_edit);
                    ddlnation_edit.SelectedValue = lbl_nation_idx.Text;
                    img_profile_edit.Visible = false;
                    SelectGroupPositionList(ddlposition_1_edit);
                    SelectGroupPositionList(ddlposition_2_edit);
                    SelectGroupPositionList(ddlposition_3_edit);
                    SelectPositionList(ddlpositionfocus_edit);
                    ddlpositionfocus_edit.SelectedValue = lblPIDX_edit.Text;
                    Select_RptLog_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));

                    //FvTemplate_print.ChangeMode(FormViewMode.Edit);
                    //Select_FvTemplate_print(int.Parse(ViewState["fv_emp_idx"].ToString()));

                    try
                    {
                        showimage_upload(lblidentity_card_edit.Text, img_profile_edit);
                        ImagProfile_default_edit.Visible = false;

                        string getPathLotus = ConfigurationSettings.AppSettings["path_flie_emp_register"];
                        string filePathLotus = Server.MapPath(getPathLotus + lblidentity_card_edit.Text);
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
                        SearchDirectories(myDirLotus, lblidentity_card_edit.Text, gvFileEmp_edit);
                        //litDebug.Text = filePathLotus.ToString();
                    }
                    catch
                    {

                    }

                    if ((int.Parse(lblu0_unidx_edit.Text) == 9 && int.Parse(lblu0_acidx_edit.Text) == 3)) //Process Sucess
                    {
                        boxviewApprove.Visible = false;
                    }
                    else if ((int.Parse(lblu0_unidx_edit.Text) == 3 && int.Parse(lblu0_acidx_edit.Text) == 3 && int.Parse(lblu0_doc_decision_edit.Text) == 1)) //กรณี Process เจ้าหน้าที่สัมภาษณ์ / เมื่อ HR เข้ามาจะไม่เห็นช่อง Approve
                    {
                        if (int.Parse(lblemp_idx_approve.Text) == int.Parse(ViewState["EmpIDX"].ToString()))
                        {
                            boxviewApprove.Visible = true;
                        }
                        else
                        {
                            boxviewApprove.Visible = false;
                        }
                    }
                    else
                    {
                        if (int.Parse(lblu0_acidx_edit.Text) == 1 && int.Parse(lblSRIDX.Text) == 1)
                        {
                            pnnameapprove.Visible = true;
                        }

                        boxviewApprove.Visible = true;
                    }
                }
                break;

            case "Fv_Search_Emp_Report":
                FormView Fv_Search_Emp_Report = (FormView)ViewReport.FindControl("Fv_Search_Emp_Report");
                DropDownList ddlpositionfocus_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpositionfocus_rp");
                DropDownList ddlgroup_report = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlgroup_rp");

                if (Fv_Search_Emp_Report.CurrentMode == FormViewMode.Insert)
                {
                    select_Group_Position(ddlgroup_report);
                    Select_MaPosition(ddlpositionfocus_report);
                }
                break;

            case "FvTemplate_print":
                Label lbl_SexNameTH_hr = (Label)FvTemplate_print.FindControl("lbl_SexNameTH_hr");
                CheckBoxList check_SexNameTH = (CheckBoxList)FvTemplate_print.FindControl("check_SexNameTH");
                Label lblJName = (Label)FvTemplate_print.FindControl("lblJName");
                CheckBoxList Ckl_jobcate = (CheckBoxList)FvTemplate_print.FindControl("Ckl_jobcate");
                Label lblmil_idx = (Label)FvTemplate_print.FindControl("lblmil_idx");
                CheckBoxList ckl_military = (CheckBoxList)FvTemplate_print.FindControl("ckl_military");
                Label lblmarried_status = (Label)FvTemplate_print.FindControl("lblmarried_status");
                CheckBoxList ckl_mrried = (CheckBoxList)FvTemplate_print.FindControl("ckl_mrried");
                Label lbldvcar_idx = (Label)FvTemplate_print.FindControl("lbldvcar_idx");
                CheckBoxList ckl_dvcar = (CheckBoxList)FvTemplate_print.FindControl("ckl_dvcar");
                Label lbldvmt_idx = (Label)FvTemplate_print.FindControl("lbldvmt_idx");
                CheckBoxList ckl_dvmt = (CheckBoxList)FvTemplate_print.FindControl("ckl_dvmt");
                Label lbldvfork_idx = (Label)FvTemplate_print.FindControl("lbldvfork_idx");
                CheckBoxList ckl_fvfork = (CheckBoxList)FvTemplate_print.FindControl("ckl_fvfork");
                Label lbldvtruck_idx = (Label)FvTemplate_print.FindControl("lbldvtruck_idx");
                CheckBoxList ckl_dvtruck = (CheckBoxList)FvTemplate_print.FindControl("ckl_dvtruck");
                Label lbldvcarstatus_idx = (Label)FvTemplate_print.FindControl("lbldvcarstatus_idx");
                CheckBoxList ckl_carstatus = (CheckBoxList)FvTemplate_print.FindControl("ckl_carstatus");
                Label lbldvmtstatus_idx = (Label)FvTemplate_print.FindControl("lbldvmtstatus_idx");
                CheckBoxList ckl_mtstatus = (CheckBoxList)FvTemplate_print.FindControl("ckl_mtstatus");
                Label lblBTypeIDX = (Label)FvTemplate_print.FindControl("lblBTypeIDX");
                Label lbl_BType = (Label)FvTemplate_print.FindControl("lbl_BType");

                if (FvTemplate_print.CurrentMode == FormViewMode.ReadOnly)
                {
                    Select_Child_print(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Education_print(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Prior_print(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Refer_print(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Train_print(int.Parse(ViewState["fv_emp_idx"].ToString()));

                    if (lbl_SexNameTH_hr.Text == "ชาย")
                    {
                        check_SexNameTH.SelectedValue = "ชาย";
                    }
                    else if (lbl_SexNameTH_hr.Text == "หญิง")
                    {
                        check_SexNameTH.SelectedValue = "หญิง";
                    }

                    if (lblJName.Text == "ประจำ")
                    {
                        Ckl_jobcate.SelectedValue = "ประจำ";
                    }
                    else if (lblJName.Text == "part-time")
                    {
                        Ckl_jobcate.SelectedValue = "part-time";
                    }
                    else if (lblJName.Text == "freelance")
                    {
                        Ckl_jobcate.SelectedValue = "freelance";
                    }
                    else if (lblJName.Text == "ฝึกงาน")
                    {
                        Ckl_jobcate.SelectedValue = "ฝึกงาน";
                    }

                    ckl_military.SelectedValue = lblmil_idx.Text;

                    if (lblmarried_status.Text == "1")
                    {
                        ckl_mrried.SelectedValue = "1";
                    }
                    else if (lblmarried_status.Text == "2")
                    {
                        ckl_mrried.SelectedValue = "2";
                    }
                    else if (lblmarried_status.Text == "3")
                    {
                        ckl_mrried.SelectedValue = "3";
                    }

                    if (lbldvcar_idx.Text == "1")
                    {
                        ckl_dvcar.SelectedValue = "1";
                    }
                    else if (lbldvcar_idx.Text == "2")
                    {
                        ckl_dvcar.SelectedValue = "2";
                    }

                    if (lbldvmt_idx.Text == "1")
                    {
                        ckl_dvmt.SelectedValue = "1";
                    }
                    else if (lbldvmt_idx.Text == "2")
                    {
                        ckl_dvmt.SelectedValue = "2";
                    }

                    if (lbldvfork_idx.Text == "1")
                    {
                        ckl_fvfork.SelectedValue = "1";
                    }
                    else if (lbldvfork_idx.Text == "2")
                    {
                        ckl_fvfork.SelectedValue = "2";
                    }

                    if (lbldvtruck_idx.Text == "1")
                    {
                        ckl_dvtruck.SelectedValue = "1";
                    }
                    else if (lbldvtruck_idx.Text == "2")
                    {
                        ckl_dvtruck.SelectedValue = "2";
                    }

                    if (lbldvcarstatus_idx.Text == "1")
                    {
                        ckl_carstatus.SelectedValue = "1";
                    }
                    else if (lbldvcarstatus_idx.Text == "2")
                    {
                        ckl_carstatus.SelectedValue = "2";
                    }

                    if (lbldvmtstatus_idx.Text == "1")
                    {
                        ckl_mtstatus.SelectedValue = "1";
                    }
                    else if (lbldvmtstatus_idx.Text == "2")
                    {
                        ckl_mtstatus.SelectedValue = "2";
                    }

                    if (lblBTypeIDX.Text == "1")
                    {
                        lbl_BType.Text = "เอ (A)";
                    }
                    else if (lblBTypeIDX.Text == "2")
                    {
                        lbl_BType.Text = "บี (B)";
                    }
                    else if (lblBTypeIDX.Text == "3")
                    {
                        lbl_BType.Text = "โอ (O)";
                    }
                    else if (lblBTypeIDX.Text == "4")
                    {
                        lbl_BType.Text = "เอบี (AB)";
                    }
                    else
                    {
                        lbl_BType.Text = "-";
                    }
                }
                break;

            case "FvViewApprove":
                if (FvViewApprove.CurrentMode == FormViewMode.Insert)
                {
                    DropDownList ddlorg_s = (DropDownList)FvViewApprove.FindControl("ddlorg_s");
                    select_org(ddlorg_s);

                }
                break;
        }
    }

    #endregion

    #region Switch-Munu Color
    protected void Menu_Color(int choice)
    {
        switch (choice)
        {
            case 1: //Index
                lbindex.BackColor = System.Drawing.Color.LightGray;
                lbscreen.BackColor = System.Drawing.Color.Transparent;
                lbrequest.BackColor = System.Drawing.Color.Transparent;
                lbapprove.BackColor = System.Drawing.Color.Transparent;
                lblreport.BackColor = System.Drawing.Color.Transparent;
                GvEmployee.Visible = true;
                Fv_Search_Emp_Index.ChangeMode(FormViewMode.Insert);
                Fv_Search_Emp_Index.DataBind();
                break;
            case 2: //Screen
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbscreen.BackColor = System.Drawing.Color.LightGray;
                lbrequest.BackColor = System.Drawing.Color.Transparent;
                lbapprove.BackColor = System.Drawing.Color.Transparent;
                lblreport.BackColor = System.Drawing.Color.Transparent;
                GvEmployee.Visible = true;
                Fv_Search_screen_Index.ChangeMode(FormViewMode.Insert);
                Fv_Search_screen_Index.DataBind();

                break;
            case 3: //request
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbscreen.BackColor = System.Drawing.Color.Transparent;
                lbrequest.BackColor = System.Drawing.Color.LightGray;
                lbapprove.BackColor = System.Drawing.Color.Transparent;
                lblreport.BackColor = System.Drawing.Color.Transparent;
                break;
            case 4: //approve
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbscreen.BackColor = System.Drawing.Color.Transparent;
                lbrequest.BackColor = System.Drawing.Color.Transparent;
                lbapprove.BackColor = System.Drawing.Color.LightGray;
                lblreport.BackColor = System.Drawing.Color.Transparent;
                break;
            case 5: //Report
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbscreen.BackColor = System.Drawing.Color.Transparent;
                lbrequest.BackColor = System.Drawing.Color.Transparent;
                lbapprove.BackColor = System.Drawing.Color.Transparent;
                lblreport.BackColor = System.Drawing.Color.LightGray;
                Fv_Search_Emp_Report.ChangeMode(FormViewMode.Insert);
                Fv_Search_Emp_Report.DataBind();
                break;
        }
    }


    protected void Menu_Color_Edit(int choice)
    {
        //Panel BoxEditEmployee = (Panel)ViewEdit.FindControl("BoxEditEmployee");
        Panel BoxGen_Edit = (Panel)FvEdit_Detail.FindControl("BoxGen_Edit");
        //Panel BoxAddress = (Panel)FvEdit_Detail.FindControl("BoxAddress");
        //Panel BoxHeal = (Panel)FvEdit_Detail.FindControl("BoxHeal");
        //Panel BoxApprove = (Panel)FvEdit_Detail.FindControl("BoxApprove");

        switch (choice)
        {
            case 1: //Gen
                //btngen_edit.ImageUrl = "~/masterpage/images/hr/color_1.png";
                //btnpos_edit.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                //btnaddress_edit.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                //btnheal_edit.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                //btmapprove_edit.ImageUrl = "~/masterpage/images/hr/gray_5.png";

                BoxGen_Edit.Visible = true;
                //BoxPos.Visible = false;
                //BoxAddress.Visible = false;
                //BoxHeal.Visible = false;
                //BoxApprove.Visible = false;
                //BoxEditEmployee.Visible = true;

                break;
            case 3: //Address
                //btngen_edit.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                //btnpos_edit.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                //btnaddress_edit.ImageUrl = "~/masterpage/images/hr/color_3.png";
                //btnheal_edit.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                //btmapprove_edit.ImageUrl = "~/masterpage/images/hr/gray_5.png";

                //BoxGen.Visible = false;
                //BoxPos.Visible = false;
                //BoxAddress.Visible = true;
                //BoxHeal.Visible = false;
                //BoxApprove.Visible = false;

                break;
            case 4: //Heal
                //btngen_edit.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                //btnpos_edit.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                //btnaddress_edit.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                //btnheal_edit.ImageUrl = "~/masterpage/images/hr/color_4.png";
                //btmapprove_edit.ImageUrl = "~/masterpage/images/hr/gray_5.png";

                //BoxGen.Visible = false;
                //BoxPos.Visible = false;
                //BoxAddress.Visible = false;
                //BoxHeal.Visible = true;
                //BoxApprove.Visible = false;
                break;
        }
    }
    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setRptData(Repeater RpName, Object obj)
    {
        RpName.DataSource = obj;
        RpName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvEmployee":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvEmployee = (GridView)ViewIndex.FindControl("GvEmployee");
                    if (GvEmployee.EditIndex != e.Row.RowIndex)
                    {
                        var img_profile = ((Image)e.Row.FindControl("img_profile"));
                        var ImagProfile_default = ((Panel)e.Row.FindControl("ImagProfile_default"));
                        var lblidentitycard = ((Label)e.Row.FindControl("lblidentitycard"));
                        var btnedit_emp = ((LinkButton)e.Row.FindControl("btnedit_emp"));
                        if (ViewState["EmpIDX"].ToString() == "173")
                        {
                            btnedit_emp.Visible = true;
                        }
                        else
                        {
                            btnedit_emp.Visible = false;
                        }
                        //btnedit_emp.Visible = false;
                        try
                        {
                            showimage_upload(lblidentitycard.Text, img_profile);
                            ImagProfile_default.Visible = false; //No
                            img_profile.Visible = true; // Yes
                        }
                        catch
                        {
                            ImagProfile_default.Visible = true; //No
                            img_profile.Visible = false; // Yes
                        }
                    }

                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        if (GvEmployee.EditIndex != e.Row.RowIndex) //to overlook header row
                        {
                            var lblu0_unidx = ((Label)e.Row.Cells[0].FindControl("lblu0_unidx"));
                            var lblu0_acidx = ((Label)e.Row.Cells[0].FindControl("lblu0_acidx"));
                            var lblu0_doc_decision = ((Label)e.Row.Cells[0].FindControl("lblu0_doc_decision"));
                            var lblstatus = ((Label)e.Row.Cells[6].FindControl("lblstatus"));
                            var lblemp_idx = ((Label)e.Row.Cells[0].FindControl("lblemp_idx"));
                       


                            if (int.Parse(lblu0_unidx.Text) == 2 && int.Parse(lblu0_acidx.Text) == 1 && int.Parse(lblu0_doc_decision.Text) == 1) //ดำเนินการ พิจารณาผล โดย เจ้าหน้าที่ HR
                            {
                                lblstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#3300FF"); //น้ำเงิน
                            }
                            else if (int.Parse(lblu0_unidx.Text) == 3 && int.Parse(lblu0_acidx.Text) == 3 && int.Parse(lblu0_doc_decision.Text) == 1) //ดำเนินการ พิจารณาผล โดย เจ้าหน้าที่สัมภาษณ์
                            {
                                lblstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#3300FF"); //น้ำเงิน
                            }
                            else if (int.Parse(lblu0_unidx.Text) == 9 && int.Parse(lblu0_acidx.Text) == 3 && int.Parse(lblu0_doc_decision.Text) == 2) //อนุมัติ จบการดำเนินการ โดย เจ้าหน้าที่สัมภาษณ์
                            {
                                lblstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#32CD32"); //เขียว
                            }
                            else //ไม่อนุมัติ จบการดำเนินการ โดย เจ้าหน้าที่ HR
                            {
                                lblstatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF8C00"); //ส้ม
                            }




                            var GvTopic = (GridView)e.Row.Cells[5].FindControl("GvTopic");
                            Select_Score(GvTopic, int.Parse(lblemp_idx.Text));

                        }

                    }

                    /*var btnViewFileCar = (HyperLink)e.Row.FindControl("btnViewFileCar");
                    //var btnViewFileRoom_ = (Image)e.Row.FindControl("btnViewFileRoom");

                    string filePath_View = ConfigurationManager.AppSettings["path_flie_cardetail"];
                    string directoryName = lbl_m0_car_idx_detail.Text;//lbl_place_name_file.Text.Trim() + lbl_room_name_en_view_file.Text + lbl_m0_room_idx_file.Text;

                    if (Directory.Exists(Server.MapPath(filePath_View + directoryName)))
                    {
                        string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View + directoryName));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string path in filesPath)
                        {
                            string getfiles = "";
                            getfiles = Path.GetFileName(path);
                            btnViewFileCar.NavigateUrl = filePath_View + directoryName + "/" + getfiles;
                            //btnViewFileRoom_.ImageUrl = filePath_View + directoryName + "/" + getfiles;

                            HtmlImage img = new HtmlImage();
                            img.Src = filePath_View + directoryName + "/" + getfiles;
                            img.Height = 100;
                            btnViewFileCar.Controls.Add(img);



                        }
                        btnViewFileCar.Visible = true;
                    }
                    else
                    {
                        btnViewFileCar.Visible = false;
                        //btnViewFileRoom_.Text = "-";
                    }*/
                }
                break;

            case "GvTopic":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbldetermine = (Label)e.Row.Cells[2].FindControl("lbldetermine");
                    Label lblnode_decision = (Label)e.Row.Cells[3].FindControl("lblnode_decision");
                    Label lblstatusinterview = (Label)e.Row.Cells[3].FindControl("lblstatusinterview");
                    Label lblm0_tqidx = (Label)e.Row.Cells[3].FindControl("lblm0_tqidx");
                    //GridView GvTopic = (GridView)GvEmployee.FindControl("GvTopic");


                    if (lbldetermine.Text == "อยู่ในระหว่างตรวจสอบ")
                    {
                        lbldetermine.ForeColor = System.Drawing.ColorTranslator.FromHtml("#6600ff");
                        lbldetermine.Style["font-weight"] = "bold";
                    }
                    else if (lbldetermine.Text == "ผ่านแบบทดสอบ")
                    {
                        lbldetermine.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00cc99");
                        lbldetermine.Style["font-weight"] = "bold";
                    }
                    else if (lbldetermine.Text == "ไม่ผ่านแบบทดสอบ")
                    {
                        lbldetermine.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8000");
                        lbldetermine.Style["font-weight"] = "bold";
                    }

                    if (lblnode_decision.Text == "1")
                    {
                        lblstatusinterview.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                        lblstatusinterview.Style["font-weight"] = "bold";
                    }
                    else if (lblnode_decision.Text == "3")
                    {
                        lblstatusinterview.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0099ff");
                        lblstatusinterview.Style["font-weight"] = "bold";
                    }
                    else if (lblnode_decision.Text == "4")
                    {
                        lblstatusinterview.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0066");
                        lblstatusinterview.Style["font-weight"] = "bold";
                    }

                    if(lblm0_tqidx.Text == "2")
                    {
                        lblstatusinterview.Visible = true;
                    }
                    else
                    {
                        lblstatusinterview.Visible = false;
                    }

                    //mergeCell(GvTopic);
                }
                break;

            case "GvEmployee_Screen":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvEmployee = (GridView)ViewScreen.FindControl("GvEmployee_Screen");
                    if (GvEmployee.EditIndex != e.Row.RowIndex)
                    {
                        var img_profile = ((Image)e.Row.FindControl("img_profile"));
                        var ImagProfile_default = ((Panel)e.Row.FindControl("ImagProfile_default"));
                        var lblidentitycard = ((Label)e.Row.FindControl("lblidentitycard"));


                        try
                        {
                            showimage_upload(lblidentitycard.Text, img_profile);
                            ImagProfile_default.Visible = false; //No
                            img_profile.Visible = true; // Yes
                        }
                        catch
                        {
                            ImagProfile_default.Visible = true; //No
                            img_profile.Visible = false; // Yes
                        }
                    }
                }
                break;
            case "GvRequest":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvEmployee = (GridView)ViewRequest.FindControl("GvRequest");
                    if (GvEmployee.EditIndex != e.Row.RowIndex)
                    {
                        var img_profile = ((Image)e.Row.FindControl("img_profile"));
                        var ImagProfile_default = ((Panel)e.Row.FindControl("ImagProfile_default"));
                        var lblidentitycard = ((Label)e.Row.FindControl("lblidentitycard"));


                        try
                        {
                            showimage_upload(lblidentitycard.Text, img_profile);
                            ImagProfile_default.Visible = false; //No
                            img_profile.Visible = true; // Yes
                        }
                        catch
                        {
                            ImagProfile_default.Visible = true; //No
                            img_profile.Visible = false; // Yes
                        }
                    }
                }
                break;
            case "GvApprove":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvApprove = (GridView)ViewApprove.FindControl("GvApprove");
                    if (GvApprove.EditIndex != e.Row.RowIndex)
                    {
                        var img_profile = ((Image)e.Row.FindControl("img_profile"));
                        var ImagProfile_default = ((Panel)e.Row.FindControl("ImagProfile_default"));
                        var lblidentitycard = ((Label)e.Row.FindControl("lblidentitycard"));
                        var GvTopic_approve = (GridView)e.Row.Cells[5].FindControl("GvTopic_approve");
                        var lblemp_idx1 = ((Label)e.Row.Cells[0].FindControl("lblemp_idx1"));

                        try
                        {
                            showimage_upload(lblidentitycard.Text, img_profile);
                            ImagProfile_default.Visible = false; //No
                            img_profile.Visible = true; // Yes
                        }
                        catch
                        {
                            ImagProfile_default.Visible = true; //No
                            img_profile.Visible = false; // Yes
                        }

                        Select_Score(GvTopic_approve, int.Parse(lblemp_idx1.Text));


                    }
                }
                break;
            case "GvEmployee_Report":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvEmployee = (GridView)ViewReport.FindControl("GvEmployee_Report");
                    if (GvEmployee.EditIndex != e.Row.RowIndex)
                    {
                        var img_profile = ((Image)e.Row.FindControl("img_profile"));
                        var ImagProfile_default = ((Panel)e.Row.FindControl("ImagProfile_default"));
                        var lblidentitycard = ((Label)e.Row.FindControl("lblidentitycard"));


                        try
                        {
                            showimage_upload(lblidentitycard.Text, img_profile);
                            ImagProfile_default.Visible = false; //No
                            img_profile.Visible = true; // Yes
                        }
                        catch
                        {
                            ImagProfile_default.Visible = true; //No
                            img_profile.Visible = false; // Yes
                        }
                    }
                }
                break;
            case "GvPosAdd_View":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    var txtEditorgIDX_Select = (TextBox)e.Row.FindControl("txtEditorgIDX_Select");
                    var txtEditDepIDX_Select = (TextBox)e.Row.FindControl("txtEditDepIDX_Select");
                    var txtEditSecIDX_Select = (TextBox)e.Row.FindControl("txtEditSecIDX_Select");
                    var txtEditPosIDX_Select = (TextBox)e.Row.FindControl("txtEditPosIDX_Select");

                    var ddlOrganizationEdit_Select = (DropDownList)e.Row.FindControl("ddlOrganizationEdit_Select");
                    var ddlDepartmentEdit_Select = (DropDownList)e.Row.FindControl("ddlDepartmentEdit_Select");
                    var ddlSectionEdit_Select = (DropDownList)e.Row.FindControl("ddlSectionEdit_Select");
                    var ddlPositionEdit_Select = (DropDownList)e.Row.FindControl("ddlPositionEdit_Select");

                    select_org(ddlOrganizationEdit_Select);
                    ddlOrganizationEdit_Select.SelectedValue = txtEditorgIDX_Select.Text;
                    select_dep(ddlDepartmentEdit_Select, int.Parse(txtEditorgIDX_Select.Text));
                    ddlDepartmentEdit_Select.SelectedValue = txtEditDepIDX_Select.Text;
                    select_sec(ddlSectionEdit_Select, int.Parse(txtEditorgIDX_Select.Text), int.Parse(txtEditDepIDX_Select.Text));
                    ddlSectionEdit_Select.SelectedValue = txtEditSecIDX_Select.Text;
                    select_pos(ddlPositionEdit_Select, int.Parse(txtEditorgIDX_Select.Text), int.Parse(txtEditDepIDX_Select.Text), int.Parse(txtEditSecIDX_Select.Text));
                    ddlPositionEdit_Select.SelectedValue = txtEditPosIDX_Select.Text;
                }
                break;

            case "GvReference_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                break;

            case "GvChildAdd_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvChildAdd_View.EditIndex == e.Row.RowIndex)
                    {
                        var lbl_Child_num = (Label)e.Row.FindControl("lbl_Child_num");
                        var ddlchildnumber = (DropDownList)e.Row.FindControl("ddlchildnumber");
                        ddlchildnumber.SelectedValue = lbl_Child_num.Text;
                    }
                }
                break;
            case "GvPri_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }
                break;
            case "GvEducation_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }
                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvEducation_View.EditIndex == e.Row.RowIndex)
                    {
                        var lbl_Edu_qualification_ID = (Label)e.Row.FindControl("lbl_Edu_qualification_ID");
                        var ddleducationback = (DropDownList)e.Row.FindControl("ddleducationback");
                        ddleducationback.SelectedValue = lbl_Edu_qualification_ID.Text;
                    }
                }
                break;
            case "GvTrain_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }
                break;
            case "gvFileEmp_edit":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    //ddd4.Text = hidFile.Value;
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    //e.Row.Cells[1].Text = a + MapURL(e.Row.Cells[1].Text);
                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;

        }
    }

    /*
    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvTopic":
                GridView GvTopic = (GridView)GvEmployee.FindControl("GvTopic");

                for (int rowIndex = GvTopic.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvTopic.Rows[rowIndex];
                    GridViewRow previousRow = GvTopic.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[1].FindControl("lbldatedoing")).Text == ((Label)previousRow.Cells[1].FindControl("lbldatedoing")).Text)
                    {
                        if (previousRow.Cells[3].RowSpan < 2)
                        {
                            currentRow.Cells[3].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[3].RowSpan = previousRow.Cells[3].RowSpan + 1;

                        }
                        previousRow.Cells[3].Visible = false;
                    }
                }
                break;
        }
    }
    */
    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvEmployee":
                GvEmployee.PageIndex = e.NewPageIndex;
                GvEmployee.DataBind();
                Select_Employee_index_search();
                break;
            case "GvEmployee_Report":
                GvEmployee_Report.PageIndex = e.NewPageIndex;
                GvEmployee_Report.DataBind();
                Select_Employee_report_search();
                break;
            case "GvPosAdd_View":
                var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");
                GvPosAdd_View.PageIndex = e.NewPageIndex;
                GvPosAdd_View.DataSource = ViewState["BoxGvPosAdd_View"];
                GvPosAdd_View.DataBind();
                break;
            case "GvReference_View":
                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");
                GvReference_View.PageIndex = e.NewPageIndex;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;
            case "GvChildAdd_View":
                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");
                GvChildAdd_View.PageIndex = e.NewPageIndex;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;
            case "GvPri_View":
                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");
                GvPri_View.PageIndex = e.NewPageIndex;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;
            case "GvEducation_View":
                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");
                GvEducation_View.PageIndex = e.NewPageIndex;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;
            case "GvTrain_View":
                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");
                GvTrain_View.PageIndex = e.NewPageIndex;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;
        }
    }

    #endregion

    #region GvRowEditing
    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvPosAdd_View":
                var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");
                GvPosAdd_View.EditIndex = e.NewEditIndex;
                GvPosAdd_View.DataSource = ViewState["BoxGvPosAdd_View"];
                GvPosAdd_View.DataBind();
                break;

            case "GvChildAdd_View":
                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");
                GvChildAdd_View.EditIndex = e.NewEditIndex;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;

            case "GvReference_View":
                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");
                GvReference_View.EditIndex = e.NewEditIndex;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;

            case "GvPri_View":
                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");
                GvPri_View.EditIndex = e.NewEditIndex;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;

            case "GvEducation_View":
                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");
                GvEducation_View.EditIndex = e.NewEditIndex;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;

            case "GvTrain_View":
                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");
                GvTrain_View.EditIndex = e.NewEditIndex;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;
        }

    }
    #endregion

    #region GvRowUpdating
    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {

            case "GvPosAdd_View":

                var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");

                int EODSPIDX1 = Convert.ToInt32(GvPosAdd_View.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlOrganizationEdit_Select = (DropDownList)GvPosAdd_View.Rows[e.RowIndex].FindControl("ddlOrganizationEdit_Select");
                var ddlDepartmentEdit_Select = (DropDownList)GvPosAdd_View.Rows[e.RowIndex].FindControl("ddlDepartmentEdit_Select");
                var ddlSectionEdit_Select = (DropDownList)GvPosAdd_View.Rows[e.RowIndex].FindControl("ddlSectionEdit_Select");
                var ddlPositionEdit_Select = (DropDownList)GvPosAdd_View.Rows[e.RowIndex].FindControl("ddlPositionEdit_Select");
                var ddStatusPosition = (DropDownList)GvPosAdd_View.Rows[e.RowIndex].FindControl("ddStatusPosition");
                Label lbl_emp_idx = (Label)FvEdit_Detail.FindControl("lbl_emp_idx");

                var txtEdito = (TextBox)GvPosAdd_View.Rows[e.RowIndex].FindControl("txtEdito");

                _dataEmployee.ODSPRelation_list = new ODSP_List[1];
                ODSP_List _dataEmployee_ = new ODSP_List();

                _dataEmployee_.EODSPIDX = EODSPIDX1;
                _dataEmployee_.emp_idx = int.Parse(lbl_emp_idx.Text);
                _dataEmployee_.RPosIDX_S = int.Parse(ddlPositionEdit_Select.SelectedValue);
                _dataEmployee_.EStaOut = int.Parse(ddStatusPosition.SelectedValue);
                _dataEmployee_.OrgIDX_S = int.Parse(ddlOrganizationEdit_Select.SelectedValue);
                _dataEmployee_.RDeptIDX_S = int.Parse(ddlDepartmentEdit_Select.SelectedValue);
                _dataEmployee_.RSecIDX_S = int.Parse(ddlSectionEdit_Select.SelectedValue);
                _dataEmployee_.RPosIDX_S = int.Parse(ddlPositionEdit_Select.SelectedValue);

                _dataEmployee.ODSPRelation_list[0] = _dataEmployee_;
                _dataEmployee = callServiceEmployee(_urlSetODSPList_Update, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(2);
                txtfocus.Focus();
                GvPosAdd_View.EditIndex = -1;
                GvPosAdd_View.DataSource = ViewState["BoxGvPosAdd_View"];
                GvPosAdd_View.DataBind();
                break;

            case "GvReference_View":

                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");
                int ReIDX = Convert.ToInt32(GvReference_View.DataKeys[e.RowIndex].Values[0].ToString());
                var txtRef_Name_edit = (TextBox)GvReference_View.Rows[e.RowIndex].FindControl("txtRef_Name_edit");
                var txtRef_pos_edit = (TextBox)GvReference_View.Rows[e.RowIndex].FindControl("txtRef_pos_edit");
                var txtRef_relation_edit = (TextBox)GvReference_View.Rows[e.RowIndex].FindControl("txtRef_relation_edit");
                var txtRef_tel_edit = (TextBox)GvReference_View.Rows[e.RowIndex].FindControl("txtRef_tel_edit");

                _dataEmployee.BoxReferent_list = new Referent_List[1];
                Referent_List _dataEmployee_ref = new Referent_List();

                _dataEmployee_ref.ReIDX = ReIDX;
                _dataEmployee_ref.ref_fullname = txtRef_Name_edit.Text;
                _dataEmployee_ref.ref_position = txtRef_pos_edit.Text;
                _dataEmployee_ref.ref_relation = txtRef_relation_edit.Text;
                _dataEmployee_ref.ref_tel = txtRef_tel_edit.Text;

                _dataEmployee.BoxReferent_list[0] = _dataEmployee_ref;
                _dataEmployee = callServiceEmployee(_urlSetUpdateRegisReferList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvReference_View.EditIndex = -1;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;

            case "GvChildAdd_View":

                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");
                int CHIDX = Convert.ToInt32(GvChildAdd_View.DataKeys[e.RowIndex].Values[0].ToString());
                var txtChild_name_edit = (TextBox)GvChildAdd_View.Rows[e.RowIndex].FindControl("txtChild_name_edit");
                var ddlchildnumber = (DropDownList)GvChildAdd_View.Rows[e.RowIndex].FindControl("ddlchildnumber");

                _dataEmployee.BoxChild_list = new Child_List[1];
                Child_List _dataEmployee_1 = new Child_List();

                _dataEmployee_1.CHIDX = CHIDX;
                _dataEmployee_1.Child_name = txtChild_name_edit.Text;
                _dataEmployee_1.Child_num = ddlchildnumber.SelectedValue.ToString();

                _dataEmployee.BoxChild_list[0] = _dataEmployee_1;
                _dataEmployee = callServiceEmployee(_urlSetUpdateRegisChildList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvChildAdd_View.EditIndex = -1;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;

            case "GvPri_View":

                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");
                int ExperIDX = Convert.ToInt32(GvPri_View.DataKeys[e.RowIndex].Values[0].ToString());
                var txtPri_Nameorg_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txtPri_Nameorg_edit");
                var txtPri_pos_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txtPri_pos_edit");
                var txt_Pri_start_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txt_Pri_start_edit");
                var txt_Pri_resign_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txt_Pri_resign_edit");

                _dataEmployee.BoxPrior_list = new Prior_List[1];
                Prior_List _dataEmployee_2 = new Prior_List();

                _dataEmployee_2.ExperIDX = ExperIDX;
                _dataEmployee_2.Pri_Nameorg = txtPri_Nameorg_edit.Text;
                _dataEmployee_2.Pri_pos = txtPri_pos_edit.Text;
                _dataEmployee_2.Pri_startdate = txt_Pri_start_edit.Text;
                _dataEmployee_2.Pri_resigndate = txt_Pri_resign_edit.Text;

                _dataEmployee.BoxPrior_list[0] = _dataEmployee_2;
                _dataEmployee = callServiceEmployee(_urlSetUpdateRegisPriorList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvPri_View.EditIndex = -1;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;

            case "GvEducation_View":

                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");
                int EDUIDX = Convert.ToInt32(GvEducation_View.DataKeys[e.RowIndex].Values[0].ToString());
                var ddleducationback = (DropDownList)GvEducation_View.Rows[e.RowIndex].FindControl("ddleducationback");
                var txtEdu_name_edit = (TextBox)GvEducation_View.Rows[e.RowIndex].FindControl("txtEdu_name_edit");
                var txtEdu_branch_edit = (TextBox)GvEducation_View.Rows[e.RowIndex].FindControl("txtEdu_branch_edit");
                var txtEdu_start_edit = (TextBox)GvEducation_View.Rows[e.RowIndex].FindControl("txtEdu_start_edit");
                var txtEdu_end_edit = (TextBox)GvEducation_View.Rows[e.RowIndex].FindControl("txtEdu_end_edit");

                _dataEmployee.BoxEducation_list = new Education_List[1];
                Education_List _dataEmployee_3 = new Education_List();

                _dataEmployee_3.EDUIDX = EDUIDX;
                _dataEmployee_3.Edu_qualification_ID = ddleducationback.SelectedValue;
                _dataEmployee_3.Edu_name = txtEdu_name_edit.Text;
                _dataEmployee_3.Edu_branch = txtEdu_branch_edit.Text;
                _dataEmployee_3.Edu_start = txtEdu_start_edit.Text;
                _dataEmployee_3.Edu_end = txtEdu_end_edit.Text;

                _dataEmployee.BoxEducation_list[0] = _dataEmployee_3;
                _dataEmployee = callServiceEmployee(_urlSetUpdateRegisEducationList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvEducation_View.EditIndex = -1;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;

            case "GvTrain_View":

                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");
                int TNIDX = Convert.ToInt32(GvTrain_View.DataKeys[e.RowIndex].Values[0].ToString());
                var txtTrain_courses_edit = (TextBox)GvTrain_View.Rows[e.RowIndex].FindControl("txtTrain_courses_edit");
                var txtTrain_date_edit = (TextBox)GvTrain_View.Rows[e.RowIndex].FindControl("txtTrain_date_edit");
                var txtTrain_assessment_edit = (TextBox)GvTrain_View.Rows[e.RowIndex].FindControl("txtTrain_assessment_edit");

                _dataEmployee.BoxTrain_list = new Train_List[1];
                Train_List _dataEmployee_4 = new Train_List();

                _dataEmployee_4.TNIDX = TNIDX;
                _dataEmployee_4.Train_courses = txtTrain_courses_edit.Text;
                _dataEmployee_4.Train_date = txtTrain_date_edit.Text;
                _dataEmployee_4.Train_assessment = txtTrain_assessment_edit.Text;

                _dataEmployee.BoxTrain_list[0] = _dataEmployee_4;
                _dataEmployee = callServiceEmployee(_urlSetUpdateRegisTrainList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvTrain_View.EditIndex = -1;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;

        }

    }

    #endregion

    #region GvRowCancelingEdit
    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "GvPosAdd_View":
                var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");
                GvPosAdd_View.EditIndex = -1;
                GvPosAdd_View.DataSource = ViewState["BoxGvPosAdd_View"];
                GvPosAdd_View.DataBind();
                break;
            case "GvReference_View":
                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");
                GvReference_View.EditIndex = -1;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;
            case "GvChildAdd_View":
                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");
                GvChildAdd_View.EditIndex = -1;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;
            case "GvPri_View":
                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");
                GvPri_View.EditIndex = -1;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;
            case "GvEducation_View":
                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");
                GvEducation_View.EditIndex = -1;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;
            case "GvTrain_View":
                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");
                GvTrain_View.EditIndex = -1;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;


        }
    }

    #endregion

    #region GvRowDeleting
    protected void gvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            /*case "GvReference":

                var GvReference = (GridView)FvInsertEmp.FindControl("GvReference");
                var dsvsDelete = (DataSet)ViewState["vsTemp_Refer"];
                var drRefer = dsvsDelete.Tables[0].Rows;

                drRefer.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Refer"] = dsvsDelete;
                GvReference.EditIndex = -1;
                GvReference.DataSource = ViewState["vsTemp_Refer"];
                GvReference.DataBind();

                break;*/

            case "GvReference_Edit":

                var GvReference_Edit = (GridView)FvEdit_Detail.FindControl("GvReference_Edit");
                var dsvsBu_ = (DataSet)ViewState["vsTemp_Refer_edit"];
                var drDr1_ = dsvsBu_.Tables[0].Rows;

                drDr1_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Refer_edit"] = dsvsBu_;
                GvReference_Edit.EditIndex = -1;
                GvReference_Edit.DataSource = ViewState["vsTemp_Refer_edit"];
                GvReference_Edit.DataBind();

                break;

            /*case "GvChildAdd":

                var GvChildAdd = (GridView)FvInsertEmp.FindControl("GvChildAdd");
                var dsvsBuyequipmentDelete = (DataSet)ViewState["vsTemp_Child"];
                var drDriving = dsvsBuyequipmentDelete.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Child"] = dsvsBuyequipmentDelete;
                GvChildAdd.EditIndex = -1;
                GvChildAdd.DataSource = ViewState["vsTemp_Child"];
                GvChildAdd.DataBind();

                break;*/

            case "GvChildAdd_Edit":

                var GvChildAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvChildAdd_Edit");
                var dsvsBuyequipmentDelete_ = (DataSet)ViewState["vsTemp_Child_edit"];
                var drDriving_ = dsvsBuyequipmentDelete_.Tables[0].Rows;

                drDriving_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Child_edit"] = dsvsBuyequipmentDelete_;
                GvChildAdd_Edit.EditIndex = -1;
                GvChildAdd_Edit.DataSource = ViewState["vsTemp_Child_edit"];
                GvChildAdd_Edit.DataBind();

                break;

            /*case "GvPri":

                var GvPri = (GridView)FvInsertEmp.FindControl("GvPri");
                var dsvsBuyequipmentDelete1 = (DataSet)ViewState["vsTemp_Prior"];
                var drDriving1 = dsvsBuyequipmentDelete1.Tables[0].Rows;

                drDriving1.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Prior"] = dsvsBuyequipmentDelete1;
                GvPri.EditIndex = -1;
                GvPri.DataSource = ViewState["vsTemp_Prior"];
                GvPri.DataBind();

                break;*/

            case "GvPri_Edit":

                var GvPri_Edit = (GridView)FvEdit_Detail.FindControl("GvPri_Edit");
                var dsvsBuyequipmentDelete1_ = (DataSet)ViewState["vsTemp_Prior_edit"];
                var drDriving1_ = dsvsBuyequipmentDelete1_.Tables[0].Rows;

                drDriving1_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Prior_edit"] = dsvsBuyequipmentDelete1_;
                GvPri_Edit.EditIndex = -1;
                GvPri_Edit.DataSource = ViewState["vsTemp_Prior_edit"];
                GvPri_Edit.DataBind();

                break;

            /*case "GvEducation":

                var GvEducation = (GridView)FvInsertEmp.FindControl("GvEducation");
                var dsvsBuyequipmentDelete2 = (DataSet)ViewState["vsTemp_Education"];
                var drDriving2 = dsvsBuyequipmentDelete2.Tables[0].Rows;

                drDriving2.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Education"] = dsvsBuyequipmentDelete2;
                GvEducation.EditIndex = -1;
                GvEducation.DataSource = ViewState["vsTemp_Education"];
                GvEducation.DataBind();

                break;*/

            case "GvEducation_Edit":

                var GvEducation_Edit = (GridView)FvEdit_Detail.FindControl("GvEducation_Edit");
                var dsvsBuyequipmentDelete2_ = (DataSet)ViewState["vsTemp_Education_edit"];
                var drDriving2_ = dsvsBuyequipmentDelete2_.Tables[0].Rows;

                drDriving2_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Education_edit"] = dsvsBuyequipmentDelete2_;
                GvEducation_Edit.EditIndex = -1;
                GvEducation_Edit.DataSource = ViewState["vsTemp_Education_edit"];
                GvEducation_Edit.DataBind();

                break;

            /*case "GvTrain":

                var GvTrain = (GridView)FvInsertEmp.FindControl("GvTrain");
                var dsvsBuyequipmentDelete3 = (DataSet)ViewState["vsTemp_Train"];
                var drDriving3 = dsvsBuyequipmentDelete3.Tables[0].Rows;

                drDriving3.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Train"] = dsvsBuyequipmentDelete3;
                GvTrain.EditIndex = -1;
                GvTrain.DataSource = ViewState["vsTemp_Train"];
                GvTrain.DataBind();

                break;*/

            case "GvTrain_Edit":

                var GvTrain_Edit = (GridView)FvEdit_Detail.FindControl("GvTrain_Edit");
                var dsvsBuyequipmentDelete3_ = (DataSet)ViewState["vsTemp_Train_edit"];
                var drDriving3_ = dsvsBuyequipmentDelete3_.Tables[0].Rows;

                drDriving3_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Train_edit"] = dsvsBuyequipmentDelete3_;
                GvTrain_Edit.EditIndex = -1;
                GvTrain_Edit.DataSource = ViewState["vsTemp_Train_edit"];
                GvTrain_Edit.DataBind();

                break;

            /*case "GvPosAdd":

                var GvPosAdd = (GridView)FvInsertEmp.FindControl("GvPosAdd");
                var Delete3 = (DataSet)ViewState["vsTemp_PosAdd"];
                var driving3 = Delete3.Tables[0].Rows;

                driving3.RemoveAt(e.RowIndex);

                ViewState["vsTemp_PosAdd"] = Delete3;
                GvPosAdd.EditIndex = -1;
                GvPosAdd.DataSource = ViewState["vsTemp_PosAdd"];
                GvPosAdd.DataBind();

                break;*/

            case "GvPosAdd_Edit":

                var GvPosAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvPosAdd_Edit");
                var Delete4 = (DataSet)ViewState["vsTemp_PosAdd_edit"];
                var driving4 = Delete4.Tables[0].Rows;

                driving4.RemoveAt(e.RowIndex);

                ViewState["vsTemp_PosAdd_edit"] = Delete4;
                GvPosAdd_Edit.EditIndex = -1;
                GvPosAdd_Edit.DataSource = ViewState["vsTemp_PosAdd_edit"];
                GvPosAdd_Edit.DataBind();

                break;
        }

    }
    #endregion

    #endregion

    #region ChkSelectedIndexChanged
    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (CheckBox)sender;

        switch (ddName.ID)
        {

            case "ckreference_edit":

                var ckreference_edit = (CheckBox)FvEdit_Detail.FindControl("ckreference_edit");
                var GvReference_Edit = (GridView)FvEdit_Detail.FindControl("GvReference_Edit");
                var BoxReference_edit = (Panel)FvEdit_Detail.FindControl("BoxReference");

                if (ckreference_edit.Checked)
                {
                    ViewState["vsTemp_Refer_edit"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer_edit"] = dsRefer;
                    GvReference_Edit.DataSource = dsRefer.Tables[0];
                    GvReference_Edit.DataBind();

                    BoxReference_edit.Visible = true;
                }
                else
                {
                    ViewState["vsTemp_Refer_edit"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer_edit"] = dsRefer;
                    GvReference_Edit.DataSource = dsRefer.Tables[0];
                    GvReference_Edit.DataBind();

                    BoxReference_edit.Visible = false;
                    GvReference_Edit.DataSource = null;
                    GvReference_Edit.DataBind();
                }
                break;

            case "chkchild_edit":

                var chkchild_edit = (CheckBox)FvEdit_Detail.FindControl("chkchild_edit");
                var GvChildAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvChildAdd_Edit");
                var BoxChild_edit = (Panel)FvEdit_Detail.FindControl("BoxChild");

                if (chkchild_edit.Checked)
                {
                    ViewState["vsTemp_Child_edit"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Equipment_");

                    dsEquipment.Tables[0].Columns.Add("Child_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Child_num", typeof(String));

                    ViewState["vsTemp_Child_edit"] = dsEquipment;
                    GvChildAdd_Edit.DataSource = dsEquipment.Tables[0];
                    GvChildAdd_Edit.DataBind();

                    BoxChild_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Child_edit"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Equipment_");

                    dsEquipment.Tables[0].Columns.Add("Child_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Child_num", typeof(String));

                    ViewState["vsTemp_Child_edit"] = dsEquipment;
                    GvChildAdd_Edit.DataSource = dsEquipment.Tables[0];
                    GvChildAdd_Edit.DataBind();

                    BoxChild_edit.Visible = false;
                    GvChildAdd_Edit.DataSource = null;
                    GvChildAdd_Edit.DataBind();
                }
                break;

            /*case "chkorg_edit":

                var chkorg_edit = (CheckBox)FvEdit_Detail.FindControl("chkorg_edit");
                var GvPri_Edit = (GridView)FvEdit_Detail.FindControl("GvPri_Edit");
                var BoxOrgOld_edit = (Panel)FvEdit_Detail.FindControl("BoxOrgOld");


                if (chkorg_edit.Checked)
                {
                    ViewState["vsTemp_Prior_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("OrgOld_edit");

                    dsEquipment_.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    //dsEquipment_.Tables[0].Columns.Add("Pri_worktime", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_resigndate", typeof(String));


                    ViewState["vsTemp_Prior_edit"] = dsEquipment_;
                    GvPri_Edit.DataSource = dsEquipment_.Tables[0];
                    GvPri_Edit.DataBind();

                    BoxOrgOld_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Prior_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("OrgOld_edit");

                    dsEquipment_.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    //dsEquipment_.Tables[0].Columns.Add("Pri_worktime", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_resigndate", typeof(String));

                    ViewState["vsTemp_Prior_edit"] = dsEquipment_;
                    GvPri_Edit.DataSource = dsEquipment_.Tables[0];
                    GvPri_Edit.DataBind();

                    BoxOrgOld_edit.Visible = false;
                    GvPri_Edit.DataSource = null;
                    GvPri_Edit.DataBind();
                }
                break;*/

            case "ckeducation_edit":

                var ckeducation_edit = (CheckBox)FvEdit_Detail.FindControl("ckeducation_edit");
                var GvEducation_Edit = (GridView)FvEdit_Detail.FindControl("GvEducation_Edit");
                var BoxEducation_edit = (Panel)FvEdit_Detail.FindControl("BoxEducation");

                if (ckeducation_edit.Checked)
                {
                    ViewState["vsTemp_Education_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Edu_edit");

                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education_edit"] = dsEquipment_;
                    GvEducation_Edit.DataSource = dsEquipment_.Tables[0];
                    GvEducation_Edit.DataBind();

                    BoxEducation_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Education_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Edu_edit");

                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education_edit"] = dsEquipment_;
                    GvEducation_Edit.DataSource = dsEquipment_.Tables[0];
                    GvEducation_Edit.DataBind();

                    BoxEducation_edit.Visible = false;
                    GvEducation_Edit.DataSource = null;
                    GvEducation_Edit.DataBind();
                }


                break;

            case "cktraining_edit":

                var cktraining_edit = (CheckBox)FvEdit_Detail.FindControl("cktraining_edit");
                var GvTrain_Edit = (GridView)FvEdit_Detail.FindControl("GvTrain_Edit");
                var BoxTrain_edit = (Panel)FvEdit_Detail.FindControl("BoxTrain");


                if (cktraining_edit.Checked)
                {
                    ViewState["vsTemp_Train_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Train_edit");

                    dsEquipment_.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_assessment", typeof(String));

                    ViewState["vsTemp_Train_edit"] = dsEquipment_;
                    GvTrain_Edit.DataSource = dsEquipment_.Tables[0];
                    GvTrain_Edit.DataBind();

                    BoxTrain_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Train_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Train_edit");

                    dsEquipment_.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_assessment", typeof(String));

                    ViewState["vsTemp_Train_edit"] = dsEquipment_;
                    GvTrain_Edit.DataSource = dsEquipment_.Tables[0];
                    GvTrain_Edit.DataBind();

                    BoxTrain_edit.Visible = false;
                    GvTrain_Edit.DataSource = null;
                    GvTrain_Edit.DataBind();
                }


                break;

            /*case "chkposition_edit":

                var chkposition_edit = (CheckBox)FvEdit_Detail.FindControl("chkposition_edit");
                var GvPosAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvPosAdd_Edit");
                var BoxPosition_edit = (Panel)FvEdit_Detail.FindControl("BoxPosition_edit");

                if (chkposition_edit.Checked)
                {
                    ViewState["vsTemp_PosAdd_edit"] = null;

                    var dsPosadd = new DataSet();
                    dsPosadd.Tables.Add("PosAdd_edit");

                    dsPosadd.Tables[0].Columns.Add("OrgIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RDeptIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("DeptNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RSecIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("SecNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RPosIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("PosNameTH_S", typeof(String));

                    ViewState["vsTemp_PosAdd_edit"] = dsPosadd;
                    GvPosAdd_Edit.DataSource = dsPosadd.Tables[0];
                    GvPosAdd_Edit.DataBind();

                    BoxPosition_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_PosAdd_edit"] = null;

                    var dsPosadd = new DataSet();
                    dsPosadd.Tables.Add("PosAdd_edit");

                    dsPosadd.Tables[0].Columns.Add("OrgIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RDeptIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("DeptNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RSecIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("SecNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RPosIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("PosNameTH_S", typeof(String));

                    ViewState["vsTemp_PosAdd_edit"] = dsPosadd;
                    GvPosAdd_Edit.DataSource = dsPosadd.Tables[0];
                    GvPosAdd_Edit.DataBind();

                    BoxPosition_edit.Visible = false;
                    GvPosAdd_Edit.DataSource = null;
                    GvPosAdd_Edit.DataBind();
                }
                break;*/

            case "chkAddress_edit":
                var chkAddress_edit = (CheckBox)FvEdit_Detail.FindControl("chkAddress_edit");
                var BoxAddress_edit = (Panel)FvEdit_Detail.FindControl("BoxAddress_edit");
                var BoxAddress = (Panel)FvEdit_Detail.FindControl("BoxAddress");

                var txtaddress_present_edit = (TextBox)BoxAddress.FindControl("txtaddress_present");
                var ddlcountry_present_edit = (DropDownList)BoxAddress.FindControl("ddlcountry_present_edit");
                var ddlprovince_present_edit = (DropDownList)BoxAddress.FindControl("ddlprovince_present_edit");
                var ddlamphoe_present_edit = (DropDownList)BoxAddress.FindControl("ddlamphoe_present_edit");
                var ddldistrict_present_edit = (DropDownList)BoxAddress.FindControl("ddldistrict_present_edit");
                var txttelmobile_present_edit = (TextBox)BoxAddress.FindControl("txttelmobile_present");
                var txttelhome_present_edit = (TextBox)BoxAddress.FindControl("txttelhome_present");
                var txtzipcode_present_edit = (TextBox)BoxAddress.FindControl("txtzipcode_present");
                var txtemail_present_edit = (TextBox)BoxAddress.FindControl("txtemail_present");

                var txtaddress_permanentaddress_edit = (TextBox)BoxAddress_edit.FindControl("txtaddress_permanentaddress");
                var ddlcountry_permanentaddress_edit = (DropDownList)BoxAddress_edit.FindControl("ddlcountry_permanentaddress_edit");
                var ddlprovince_permanentaddress_edit = (DropDownList)BoxAddress_edit.FindControl("ddlprovince_permanentaddress_edit");
                var ddlamphoe_permanentaddress_edit = (DropDownList)BoxAddress_edit.FindControl("ddlamphoe_permanentaddress_edit");
                var ddldistrict_permanentaddress_edit = (DropDownList)BoxAddress_edit.FindControl("ddldistrict_permanentaddress_edit");
                var txttelmobile_permanentaddress_edit = (TextBox)BoxAddress_edit.FindControl("txttelmobile_permanentaddress");


                if (chkAddress_edit.Checked)
                {
                    BoxAddress_edit.Visible = false;
                    txtaddress_permanentaddress_edit.Text = txtaddress_present_edit.Text;
                    select_country(ddlcountry_permanentaddress_edit);
                    ddlcountry_permanentaddress_edit.SelectedValue = ddlcountry_present_edit.SelectedValue;
                    select_province(ddlprovince_permanentaddress_edit);
                    ddlprovince_permanentaddress_edit.SelectedValue = ddlprovince_present_edit.SelectedValue;
                    select_amphur(ddlamphoe_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue));
                    ddlamphoe_permanentaddress_edit.SelectedValue = ddlamphoe_present_edit.SelectedValue;
                    select_dist(ddldistrict_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue), int.Parse(ddlamphoe_permanentaddress_edit.SelectedValue));
                    ddldistrict_permanentaddress_edit.SelectedValue = ddldistrict_present_edit.SelectedValue;
                    txttelmobile_permanentaddress_edit.Text = txttelmobile_present_edit.Text;

                    txtaddress_permanentaddress_edit.Enabled = false;
                    ddlcountry_permanentaddress_edit.Enabled = false;
                    ddlprovince_permanentaddress_edit.Enabled = false;
                    ddlamphoe_permanentaddress_edit.Enabled = false;
                    ddldistrict_permanentaddress_edit.Enabled = false;
                    txttelmobile_permanentaddress_edit.Enabled = false;
                }
                else
                {
                    BoxAddress_edit.Visible = true;
                    select_country(ddlcountry_permanentaddress_edit);
                    select_province(ddlprovince_permanentaddress_edit);

                    txtaddress_permanentaddress_edit.Text = null;
                    txtaddress_permanentaddress_edit.Enabled = true;
                    ddlcountry_permanentaddress_edit.Enabled = true;
                    ddlprovince_permanentaddress_edit.Enabled = true;
                    ddlamphoe_permanentaddress_edit.Enabled = true;
                    ddlamphoe_permanentaddress_edit.SelectedValue = null;
                    ddldistrict_permanentaddress_edit.Enabled = true;
                    ddldistrict_permanentaddress_edit.SelectedValue = null;
                    txttelmobile_permanentaddress_edit.Text = null;
                    txttelmobile_permanentaddress_edit.Enabled = true;
                }
                break;

            case "chkAll":

                bool b = (sender as CheckBox).Checked;
                CheckBoxList YrChkBox = (CheckBoxList)Fv_Search_screen_Index.FindControl("YrChkBox");

                for (int i = 0; i < YrChkBox.Items.Count; i++)
                {
                    YrChkBox.Items[i].Selected = b;
                }

                break;
            case "ChkdriverAll":

                bool c = (sender as CheckBox).Checked;
                CheckBoxList Chkdriver = (CheckBoxList)Fv_Search_screen_Index.FindControl("Chkdriver");

                for (int i = 0; i < Chkdriver.Items.Count; i++)
                {
                    Chkdriver.Items[i].Selected = c;
                }

                break;
        }
    }
    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        /*DropDownList ddlorg = (DropDownList)FvInsertEmp.FindControl("ddlorg");
        DropDownList ddldep = (DropDownList)FvInsertEmp.FindControl("ddldep");
        DropDownList ddlsec = (DropDownList)FvInsertEmp.FindControl("ddlsec");
        DropDownList ddlpos = (DropDownList)FvInsertEmp.FindControl("ddlpos");

        DropDownList ddlorg_add = (DropDownList)FvInsertEmp.FindControl("ddlorg_add");
        DropDownList ddldep_add = (DropDownList)FvInsertEmp.FindControl("ddldep_add");
        DropDownList ddlsec_add = (DropDownList)FvInsertEmp.FindControl("ddlsec_add");
        DropDownList ddlpos_add = (DropDownList)FvInsertEmp.FindControl("ddlpos_add");

        DropDownList ddlnation = (DropDownList)FvInsertEmp.FindControl("ddlnation");
        TextBox txtidcard = (TextBox)FvInsertEmp.FindControl("txtidcard");
        TextBox txtsecurityid = (TextBox)FvInsertEmp.FindControl("txtsecurityid");*/

        //DropDownList ddlorg_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_edit");
        //DropDownList ddldep_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddldep_add_edit");
        //DropDownList ddlsec_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlsec_add_edit");
        //DropDownList ddlpos_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlpos_add_edit");

        /*DropDownList ddlcountry_present = (DropDownList)FvInsertEmp.FindControl("ddlcountry_present");
        DropDownList ddlprovince_present = (DropDownList)FvInsertEmp.FindControl("ddlprovince_present");
        DropDownList ddlamphoe_present = (DropDownList)FvInsertEmp.FindControl("ddlamphoe_present");
        DropDownList ddldistrict_present = (DropDownList)FvInsertEmp.FindControl("ddldistrict_present");

        DropDownList ddlprovince_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlprovince_permanentaddress");
        DropDownList ddlamphoe_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlamphoe_permanentaddress");
        DropDownList ddldistrict_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddldistrict_permanentaddress");*/

        DropDownList ddlprovince_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_permanentaddress_edit");
        DropDownList ddlamphoe_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_permanentaddress_edit");
        DropDownList ddldistrict_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_permanentaddress_edit");

        //DropDownList ddlapprove1 = (DropDownList)FvInsertEmp.FindControl("ddlapprove1");
        //DropDownList ddlapprove2 = (DropDownList)FvInsertEmp.FindControl("ddlapprove2");

        DropDownList ddlorg_s = (DropDownList)FvViewApprove.FindControl("ddlorg_s");
        DropDownList ddldep_s = (DropDownList)FvViewApprove.FindControl("ddldep_s");
        DropDownList ddlsec_s = (DropDownList)FvViewApprove.FindControl("ddlsec_s");
        DropDownList ddlempidx_s = (DropDownList)FvViewApprove.FindControl("ddlempidx_s");

        DropDownList ddlnation_edit = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
        TextBox txtidcard_edit = (TextBox)FvEdit_Detail.FindControl("txtidcard");
        TextBox txtsecurityid_edit = (TextBox)FvEdit_Detail.FindControl("txtsecurityid");

        DropDownList ddlprovince_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_present_edit");
        DropDownList ddlamphoe_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_present_edit");
        DropDownList ddldistrict_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_present_edit");
        Label lbl_prov_edit = (Label)FvEdit_Detail.FindControl("lbl_prov_edit");
        Label lbl_amp_edit = (Label)FvEdit_Detail.FindControl("lbl_amp_edit");
        Label lbl_dist_edit = (Label)FvEdit_Detail.FindControl("lbl_dist_edit");

        //DropDownList ddl_prefix_th = (DropDownList)FvInsertEmp.FindControl("ddl_prefix_th");
        //DropDownList ddl_prefix_en = (DropDownList)FvInsertEmp.FindControl("ddl_prefix_en");
        //DropDownList ddlmilitary = (DropDownList)FvInsertEmp.FindControl("ddlmilitary");

        DropDownList ddl_prefix_th_edit = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_th_edit");
        DropDownList ddl_prefix_en_edit = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_en_edit");
        DropDownList ddlmilitary_edit = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary_edit");

        //DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        //DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        //DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        //DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        Panel BoxSearch_Date = (Panel)Fv_Search_Emp_Report.FindControl("BoxSearch_Date");

        switch (ddName.ID)
        {
            case "ddldatetype":
                if (ddldatetype.SelectedValue == "1" || ddldatetype.SelectedValue == "2")
                {
                    BoxSearch_Date.Visible = true;
                }
                else
                {
                    BoxSearch_Date.Visible = false;
                }
                break;
            case "ddlprovince_present_edit":
                select_amphur(ddlamphoe_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue));
                break;
            case "ddlamphoe_present_edit":
                select_dist(ddldistrict_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue), int.Parse(ddlamphoe_present_edit.SelectedValue));
                break;

            case "ddlprovince_permanentaddress_edit":
                select_amphur(ddlamphoe_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue));
                break;
            case "ddlamphoe_permanentaddress_edit":
                select_dist(ddldistrict_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue), int.Parse(ddlamphoe_permanentaddress_edit.SelectedValue));
                break;
            case "ddl_prefix_th_edit":
                if (ddl_prefix_th_edit.SelectedValue == "1") //นาย
                {
                    ddl_prefix_en_edit.SelectedValue = "1";
                    ddlmilitary_edit.SelectedValue = "1";
                }
                else if (ddl_prefix_th_edit.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_en_edit.SelectedValue = "2";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else if (ddl_prefix_th_edit.SelectedValue == "3") //นาง
                {
                    ddl_prefix_en_edit.SelectedValue = "3";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else
                {
                    ddlmilitary_edit.SelectedValue = "1";
                }
                break;
            case "ddl_prefix_en_edit":
                if (ddl_prefix_en_edit.SelectedValue == "1") //นาย
                {
                    ddl_prefix_th_edit.SelectedValue = "1";
                    ddlmilitary_edit.SelectedValue = "1";
                }
                else if (ddl_prefix_en_edit.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_th_edit.SelectedValue = "2";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else if (ddl_prefix_en_edit.SelectedValue == "3") //นาง
                {
                    ddl_prefix_th_edit.SelectedValue = "3";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                break;
            case "ddlnation_edit":
                if (ddlnation_edit.SelectedValue == "177")
                {
                    txtsecurityid_edit.Text = txtidcard_edit.Text;
                }
                else
                {
                    ddlmilitary_edit.SelectedValue = "3";
                }
                break;
            case "ddlorg_s":
                select_dep(ddldep_s, int.Parse(ddlorg_s.SelectedValue));
                break;
            case "ddldep_s":
                select_sec(ddlsec_s, int.Parse(ddlorg_s.SelectedValue), int.Parse(ddldep_s.SelectedValue));
                break;
            case "ddlsec_s":
                select_empname(ddlempidx_s, int.Parse(ddlsec_s.SelectedValue));
                break;
        }
    }

    #endregion

    #region callService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceEmployee_Check(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;
        //Label2.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        ViewState["return_code"] = _dataEmployee.return_code;
        ViewState["return_sucess"] = _dataEmployee.return_msg;

        return _dataEmployee;
    }

    protected data_rcm_question callServicePostRCM(string _cmdUrl, data_rcm_question _dtrcm)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtrcm);
        // txt.Text = _cmdUrl + _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson;

        //// convert json to object
        _dtrcm = (data_rcm_question)_funcTool.convertJsonToObject(typeof(data_rcm_question), _localJson);


        return _dtrcm;
    }


    #endregion

    #region custom data format and display
    protected string formatDate(string dateIN)
    {
        return DateTime.Parse(dateIN).ToString("dd/MM/yyyy");
    }
    #endregion

    #region Directories_File URL

    protected void showimage_upload(string x, Image y)
    {

        string getPath = ConfigurationSettings.AppSettings["path_flie_emp_register"];
        string pat = getPath + x.ToString() + "/" + x.ToString() + ".jpg";
        y.Visible = true;
        y.ImageUrl = pat; //Page.ResolveUrl("~/ImageFiles/") + fi.Name;
        //Label3.Text = pat;
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "btnIndex":
                Select_Employee_index();
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewIndex);
                break;
            case "btnscreen":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewScreen);
                Set_Defult_Screen();

                break;
            case "btnrequest":
                MvMaster.SetActiveView(ViewRequest);
                Menu_Color(int.Parse(cmdArg));
                Select_Employee_request_search();
                break;
            case "btnapprove":
                MvMaster.SetActiveView(ViewApprove);
                Menu_Color(int.Parse(cmdArg));
                Select_Employee_Approve();

                break;

            case "btnaddscore":
                string url = "http://mas.taokaenoi.co.th/rcm-question";
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();

                StringBuilder s = new StringBuilder();
                s.Append("<html>");
                s.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
                s.AppendFormat("<form name='form' action='{0}' method='post'>", url);

                s.Append("</form></body></html>");
                response.Write(s.ToString());
                response.End();
                break;

            case "btnreport":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewReport);
                break;
            case "btnedit_emp":
                MvMaster.SetActiveView(ViewEdit);
                FvEdit_Detail.ChangeMode(FormViewMode.Edit);
                FvTemplate_print.ChangeMode(FormViewMode.ReadOnly);
                FvViewApprove.ChangeMode(FormViewMode.Insert);
                Select_Fv_Detail(FvEdit_Detail, cmdArg.ToString());
                Select_Fv_Detail(FvTemplate_print, cmdArg.ToString());
                DropDownList ddlorg_s = (DropDownList)FvViewApprove.FindControl("ddlorg_s");
                Label lblu0_unidx_edit = (Label)FvEdit_Detail.FindControl("lblu0_unidx");
                Label lblu0_acidx_edit = (Label)FvEdit_Detail.FindControl("lblu0_acidx");
                Label lblu0_doc_decision_edit = (Label)FvEdit_Detail.FindControl("lblu0_doc_decision");
                Label lblSRIDX = (Label)FvEdit_Detail.FindControl("lblSRIDX");
                select_org(ddlorg_s);

                if ((int.Parse(lblu0_unidx_edit.Text) == 9 && int.Parse(lblu0_acidx_edit.Text) == 3) && int.Parse(lblu0_doc_decision_edit.Text) == 2) // Approve Secess
                {
                    lbnrequest_register.Visible = false;
                    lbntransfer_register.Visible = true;
                }
                else if ((int.Parse(lblu0_unidx_edit.Text) == 2 && int.Parse(lblu0_acidx_edit.Text) == 1) || (int.Parse(lblu0_unidx_edit.Text) == 3 && int.Parse(lblu0_acidx_edit.Text) == 3)) // รอดำเนินการโดย HR จากผู้สมัคร
                {
                    lbnrequest_register.Visible = false;
                    lbntransfer_register.Visible = false;
                }
                else
                {
                    lbnrequest_register.Visible = true;
                    lbntransfer_register.Visible = false;
                }
                break;
            case "btn_Approve":
                Update_Approve();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btn_search_index":
                Select_Employee_index_search();
                //if (ViewState["Box_dataEmployee_index"] != null)
                //{
                //    employee_detail[] _templist_cheack = (employee_detail[])ViewState["Box_dataEmployee_index"];
                //    var _linqCheck = (from dataMat in _templist_cheack

                //                      select new
                //                      {
                //                          dataMat
                //                      }).ToList();
                //    try
                //    {
                //        lblsum_list_index.Text = "จำนวน : " + _linqCheck.Count().ToString() + " รายการ / ทั้งหมด : " + GvEmployee.PageCount.ToString() + " หน้า";
                //    }
                //    catch (Exception ex)
                //    {
                //        lblsum_list_index.Text = "จำนวน : " + "0" + " รายการ / ทั้งหมด : " + "0" + " หน้า";
                //    }
                //}
                break;
            case "btn_search_screen":
                Select_Employee_screen_search();
                if (ViewState["Box_dataEmployee_screen"] != null)
                {
                    employee_detail[] _templist_cheack_ = (employee_detail[])ViewState["Box_dataEmployee_screen"];
                    var _linqCheck_ = (from dataMat in _templist_cheack_

                                       select new
                                       {
                                           dataMat
                                       }).ToList();
                    try
                    {
                        lblsum_list_screen_index.Text = "จำนวน : " + _linqCheck_.Count().ToString() + " รายการ / ทั้งหมด : " + GvEmployee_Screen.PageCount.ToString() + " หน้า";
                    }
                    catch (Exception ex)
                    {
                        lblsum_list_screen_index.Text = "จำนวน : " + "0" + " รายการ / ทั้งหมด : " + "0" + " หน้า";
                    }
                }
                break;
            case "btn_search_report":
                Select_Employee_report_search();
                if (ViewState["Box_dataEmployee_Report"] != null)
                {
                    employee_detail[] _templist_cheackMat = (employee_detail[])ViewState["Box_dataEmployee_Report"];
                    var _linqCheckMat = (from dataMat in _templist_cheackMat

                                         select new
                                         {
                                             dataMat
                                         }).ToList();
                    try
                    {
                        lblsum_list.Text = "จำนวน : " + _linqCheckMat.Count().ToString() + " รายการ / ทั้งหมด : " + GvEmployee_Report.PageCount.ToString() + " หน้า";
                    }
                    catch (Exception ex)
                    {
                        lblsum_list.Text = "จำนวน : " + "0" + " รายการ / ทั้งหมด : " + "0" + " หน้า";
                    }

                }

                break;
            case "Cmdsearch_clear":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btngeneral_edit":
                Menu_Color_Edit(int.Parse(cmdArg));
                break;
            case "btnaddress_edit":
                Menu_Color_Edit(int.Parse(cmdArg));
                break;
            case "btnhealth_edit":

                Menu_Color_Edit(int.Parse(cmdArg));
                break;

            case "btnSave": // Save Data ----------------------------
                /*var txt_startdate = (TextBox)FvInsertEmp.FindControl("txt_startdate");
                var ddlemptype = (DropDownList)FvInsertEmp.FindControl("ddlemptype");
                var txt_name_th = (TextBox)FvInsertEmp.FindControl("txt_name_th");
                var txt_surname_th = (TextBox)FvInsertEmp.FindControl("txt_surname_th");
                var txt_name_en = (TextBox)FvInsertEmp.FindControl("txt_name_en");
                var txt_surname_en = (TextBox)FvInsertEmp.FindControl("txt_surname_en");

                var ddlorg = (DropDownList)FvInsertEmp.FindControl("ddlorg");
                var ddldep = (DropDownList)FvInsertEmp.FindControl("ddldep");
                var ddlsec = (DropDownList)FvInsertEmp.FindControl("ddlsec");
                var ddlpos = (DropDownList)FvInsertEmp.FindControl("ddlpos");

                if (txt_startdate.Text != "" && ddlemptype.SelectedValue != "0" && txt_name_th.Text != "" && txt_surname_th.Text != "" & txt_name_en.Text != "" && txt_surname_en.Text != ""
                    && ddlorg.SelectedValue != "0" && ddldep.SelectedValue != "0" && ddlsec.SelectedValue != "0" && ddlpos.SelectedValue != "0")
                {
                    //Insert_Employee();
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    //Menu_Color(1);
                    //MvMaster.SetActiveView(ViewIndex);
                    //txtfocus.Focus();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "UploadError", "alert('" + "คุณกรอกข้อมูลไม่ครบถ้วน" + "')", true);
                }*/
                break;
            case "btnUpdate":
                //Update_Employee();
                break;
            case "btnExport":
                //Select_Employee_Export();

                GvExport.AllowPaging = false;
                GvExport.DataSource = ViewState["Box_dataEmployee_Report"];
                GvExport.DataBind();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                for (int i = 0; i < GvExport.Rows.Count; i++)
                {
                    //Apply text style to each Row
                    GvExport.Rows[i].Attributes.Add("class", "number2");
                }
                GvExport.RenderControl(hw);

                //Amount is displayed in number format with 2 decimals
                Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();

                break;


            case "AddPosAdd_Edit":

                var GvPosAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvPosAdd_Edit");
                var ddlorg_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_edit");
                var ddldep_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddldep_add_edit");
                var ddlsec_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlsec_add_edit");
                var ddlpos_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlpos_add_edit");


                var dsPosAdd_edit = (DataSet)ViewState["vsTemp_PosAdd_edit"];

                var drPosAdd_edit = dsPosAdd_edit.Tables[0].NewRow();
                drPosAdd_edit["OrgIDX_S"] = int.Parse(ddlorg_add_edit.SelectedValue);
                drPosAdd_edit["OrgNameTH_S"] = ddlorg_add_edit.SelectedItem.Text;
                drPosAdd_edit["RDeptIDX_S"] = int.Parse(ddldep_add_edit.SelectedValue);
                drPosAdd_edit["DeptNameTH_S"] = ddldep_add_edit.SelectedItem.Text;
                drPosAdd_edit["RSecIDX_S"] = int.Parse(ddlsec_add_edit.SelectedValue);
                drPosAdd_edit["SecNameTH_S"] = ddlsec_add_edit.SelectedItem.Text;
                drPosAdd_edit["RPosIDX_S"] = int.Parse(ddlpos_add_edit.SelectedValue);
                drPosAdd_edit["PosNameTH_S"] = ddlpos_add_edit.SelectedItem.Text;

                dsPosAdd_edit.Tables[0].Rows.Add(drPosAdd_edit);

                ViewState["vsTemp_PosAdd_edit"] = dsPosAdd_edit;

                GvPosAdd_Edit.DataSource = dsPosAdd_edit.Tables[0];
                GvPosAdd_Edit.DataBind();

                ddlorg_add_edit.SelectedValue = null;
                ddldep_add_edit.SelectedValue = null;
                ddlsec_add_edit.SelectedValue = null;
                ddlpos_add_edit.SelectedValue = null;

                break;

            case "AddChild_Edit":

                var GvChildAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvChildAdd_Edit");
                var txtchildname_edit = (TextBox)FvEdit_Detail.FindControl("txtchildname_edit");
                var ddlchildnumber_edit = (DropDownList)FvEdit_Detail.FindControl("ddlchildnumber_edit");

                var dsEquiment_ = (DataSet)ViewState["vsTemp_Child_edit"];

                var drEquiment_ = dsEquiment_.Tables[0].NewRow();
                drEquiment_["Child_name"] = txtchildname_edit.Text;
                drEquiment_["Child_num"] = ddlchildnumber_edit.SelectedValue;

                dsEquiment_.Tables[0].Rows.Add(drEquiment_);

                ViewState["vsTemp_Child_edit"] = dsEquiment_;

                GvChildAdd_Edit.DataSource = dsEquiment_.Tables[0];
                GvChildAdd_Edit.DataBind();

                txtchildname_edit.Text = null;
                ddlchildnumber_edit.SelectedValue = null;

                break;
            case "AddReference_Edit":
                var GvReferenceEdit = (GridView)FvEdit_Detail.FindControl("GvReference_Edit");
                var txtfullname_edit = (TextBox)FvEdit_Detail.FindControl("txtfullname_add");
                var txtposition_edit = (TextBox)FvEdit_Detail.FindControl("txtposition_add");
                var txtrelation_edit = (TextBox)FvEdit_Detail.FindControl("txtrelation_add");
                var txttel_edit = (TextBox)FvEdit_Detail.FindControl("txttel_add");

                var dsRefer_edit = (DataSet)ViewState["vsTemp_Refer_edit"];

                var drRefer_edit = dsRefer_edit.Tables[0].NewRow();
                drRefer_edit["ref_fullname"] = txtfullname_edit.Text;
                drRefer_edit["ref_position"] = txtposition_edit.Text;
                drRefer_edit["ref_relation"] = txtrelation_edit.Text;
                drRefer_edit["ref_tel"] = txttel_edit.Text;

                dsRefer_edit.Tables[0].Rows.Add(drRefer_edit);

                ViewState["vsTemp_Refer_edit"] = dsRefer_edit;
                GvReferenceEdit.DataSource = dsRefer_edit.Tables[0];
                GvReferenceEdit.DataBind();

                txtfullname_edit.Text = null;
                txtposition_edit.Text = null;
                txtrelation_edit.Text = null;
                txttel_edit.Text = null;

                break;

            case "AddPrior_Edit":

                var GvPri_Edit = (GridView)FvEdit_Detail.FindControl("GvPri_Edit");
                var txtorgold_edit = (TextBox)FvEdit_Detail.FindControl("txtorgold_edit");
                var txtposold_edit = (TextBox)FvEdit_Detail.FindControl("txtposold_edit");
                //var txtworktimeold_edit = (TextBox)FvEdit_Detail.FindControl("txtworktimeold_edit");
                var txtstartdate_edit = (TextBox)FvEdit_Detail.FindControl("txtstartdate_edit");
                var txtresigndate_edit = (TextBox)FvEdit_Detail.FindControl("txtresigndate_edit");

                var dsOrg_edit = (DataSet)ViewState["vsTemp_Prior_edit"];

                var drOrg_edit = dsOrg_edit.Tables[0].NewRow();
                drOrg_edit["Pri_Nameorg"] = txtorgold_edit.Text;
                drOrg_edit["Pri_pos"] = txtposold_edit.Text;
                //drOrg_edit["Pri_worktime"] = txtworktimeold_edit.Text;
                drOrg_edit["Pri_startdate"] = txtstartdate_edit.Text;
                drOrg_edit["Pri_resigndate"] = txtresigndate_edit.Text;

                dsOrg_edit.Tables[0].Rows.Add(drOrg_edit);

                ViewState["vsTemp_Prior_edit"] = dsOrg_edit;

                GvPri_Edit.DataSource = dsOrg_edit.Tables[0];
                GvPri_Edit.DataBind();

                txtorgold_edit.Text = null;
                txtposold_edit.Text = null;
                txtstartdate_edit.Text = null;
                txtresigndate_edit.Text = null;

                break;

            case "AddEducation_Edit":

                var GvEducation_Edit = (GridView)FvEdit_Detail.FindControl("GvEducation_Edit");
                var ddleducationback_edit = (DropDownList)FvEdit_Detail.FindControl("ddleducationback_edit");
                var txtschoolname_edit = (TextBox)FvEdit_Detail.FindControl("txtschoolname_edit");
                var txtstarteducation_edit = (TextBox)FvEdit_Detail.FindControl("txtstarteducation_edit");
                var txtendeducation_edit = (TextBox)FvEdit_Detail.FindControl("txtendeducation_edit");
                var txtstudy_edit = (TextBox)FvEdit_Detail.FindControl("txtstudy_edit");

                var dsEdu_edit = (DataSet)ViewState["vsTemp_Education_edit"];

                var drEdu_edit = dsEdu_edit.Tables[0].NewRow();
                drEdu_edit["Edu_qualification_ID"] = ddleducationback_edit.SelectedValue;
                drEdu_edit["Edu_qualification"] = ddleducationback_edit.SelectedItem.Text;
                drEdu_edit["Edu_name"] = txtschoolname_edit.Text;
                drEdu_edit["Edu_branch"] = txtstudy_edit.Text;
                drEdu_edit["Edu_start"] = txtstarteducation_edit.Text;
                drEdu_edit["Edu_end"] = txtendeducation_edit.Text;

                dsEdu_edit.Tables[0].Rows.Add(drEdu_edit);

                ViewState["vsTemp_Education_edit"] = dsEdu_edit;

                GvEducation_Edit.DataSource = dsEdu_edit.Tables[0];
                GvEducation_Edit.DataBind();

                ddleducationback_edit.SelectedValue = null;
                txtschoolname_edit.Text = null;
                txtstarteducation_edit.Text = null;
                txtendeducation_edit.Text = null;
                txtstudy_edit.Text = null;

                break;

            case "Addtraining_Edit":

                var GvTrain_Edit = (GridView)FvEdit_Detail.FindControl("GvTrain_Edit");
                var txttraincourses_edit = (TextBox)FvEdit_Detail.FindControl("txttraincourses_edit");
                var txttraindate_edit = (TextBox)FvEdit_Detail.FindControl("txttraindate_edit");
                var txttrainassessment_edit = (TextBox)FvEdit_Detail.FindControl("txttrainassessment_edit");

                var dsTrain_edit = (DataSet)ViewState["vsTemp_Train_edit"];

                var drTrain_edit = dsTrain_edit.Tables[0].NewRow();
                drTrain_edit["Train_courses"] = txttraincourses_edit.Text;
                drTrain_edit["Train_date"] = txttraindate_edit.Text;
                drTrain_edit["Train_assessment"] = txttrainassessment_edit.Text;

                dsTrain_edit.Tables[0].Rows.Add(drTrain_edit);

                ViewState["vsTemp_Train_edit"] = dsTrain_edit;

                GvTrain_Edit.DataSource = dsTrain_edit.Tables[0];
                GvTrain_Edit.DataBind();

                txttraincourses_edit.Text = null;
                txttraindate_edit.Text = null;
                txttrainassessment_edit.Text = null;

                break;

            case "DeleteGvPos":

                string[] arg1 = new string[9];
                arg1 = e.CommandArgument.ToString().Split(';');
                int EODSPIDX = int.Parse(arg1[0]);
                int OrgIDX = int.Parse(arg1[1]);
                int RDeptIDX = int.Parse(arg1[3]);
                int RSecIDX = int.Parse(arg1[5]);
                int RPosIDX = int.Parse(arg1[8]);

                var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");

                _dataEmployee.ODSPRelation_list = new ODSP_List[1];
                ODSP_List _dataEmployee_ = new ODSP_List();

                _dataEmployee_.EODSPIDX = EODSPIDX;
                _dataEmployee_.EmpIDXUser = int.Parse(ViewState["EmpIDX"].ToString());

                _dataEmployee_.OrgIDX_S = OrgIDX;
                _dataEmployee_.RDeptIDX_S = RDeptIDX;
                _dataEmployee_.RSecIDX_S = RSecIDX;
                _dataEmployee_.RPosIDX_S = RPosIDX;

                _dataEmployee.ODSPRelation_list[0] = _dataEmployee_;
                _dataEmployee = callServiceEmployee_Check(_urlSetODSPList_Delete, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(2);
                txtfocus.Focus();

                if (int.Parse(ViewState["return_code_check"].ToString()) == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('*** ไม่สามารถลบตำแหน่งหลักได้ กรุณาเปลี่ยนสถานะก่อน')", true);
                }

                GvPosAdd_View.DataSource = ViewState["BoxGvPosAdd_View"];
                GvPosAdd_View.DataBind();
                break;

            case "DeleteGvRefer_View":
                string cmdArg_ref = e.CommandArgument.ToString();
                int ReIDX = int.Parse(cmdArg_ref);

                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");

                _dataEmployee.BoxReferent_list = new Referent_List[1];
                Referent_List _dataEmployee_ref = new Referent_List();

                _dataEmployee_ref.ReIDX = ReIDX;

                _dataEmployee.BoxReferent_list[0] = _dataEmployee_ref;
                _dataEmployee = callServiceEmployee(_urlSetDeleteRegisReferList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvReference_View.EditIndex = -1;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;

            case "DeleteGvChildAdd_View":
                string cmdArg2 = e.CommandArgument.ToString();
                int CHIDX = int.Parse(cmdArg2);

                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");

                _dataEmployee.BoxChild_list = new Child_List[1];
                Child_List _dataEmployee_1 = new Child_List();

                _dataEmployee_1.CHIDX = CHIDX;

                _dataEmployee.BoxChild_list[0] = _dataEmployee_1;
                _dataEmployee = callServiceEmployee(_urlSetDeleteRegisChildList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvChildAdd_View.EditIndex = -1;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;

            case "DeleteGvPri_View":
                string cmdArg3 = e.CommandArgument.ToString();
                int ExperIDX = int.Parse(cmdArg3);

                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");

                _dataEmployee.BoxPrior_list = new Prior_List[1];
                Prior_List _dataEmployee_2 = new Prior_List();

                _dataEmployee_2.ExperIDX = ExperIDX;

                _dataEmployee.BoxPrior_list[0] = _dataEmployee_2;
                _dataEmployee = callServiceEmployee(_urlSetDeleteRegisPriorList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvPri_View.EditIndex = -1;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;

            case "DeleteGvEducation_View":
                string cmdArg4 = e.CommandArgument.ToString();
                int EDUIDX = int.Parse(cmdArg4);

                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");

                _dataEmployee.BoxEducation_list = new Education_List[1];
                Education_List _dataEmployee_3 = new Education_List();

                _dataEmployee_3.EDUIDX = EDUIDX;

                _dataEmployee.BoxEducation_list[0] = _dataEmployee_3;
                _dataEmployee = callServiceEmployee(_urlSetDeleteRegisEducationList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvEducation_View.EditIndex = -1;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;

            case "DeleteGvTrain_View":
                string cmdArg5 = e.CommandArgument.ToString();
                int TNIDX = int.Parse(cmdArg5);

                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");

                _dataEmployee.BoxTrain_list = new Train_List[1];
                Train_List _dataEmployee_4 = new Train_List();

                _dataEmployee_4.TNIDX = TNIDX;

                _dataEmployee.BoxTrain_list[0] = _dataEmployee_4;
                _dataEmployee = callServiceEmployee(_urlSetDeleteRegisTrainList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                Menu_Color_Edit(1);
                txtfocus.Focus();
                GvTrain_View.EditIndex = -1;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;

            case "btnrequest_employee":
                boxviewApprove.Visible = true;
                FvViewApprove.ChangeMode(FormViewMode.Insert);
                Panel pnnameapprove = (Panel)FvViewApprove.FindControl("pnnameapprove");
                LinkButton btn_Approve_request = (LinkButton)FvViewApprove.FindControl("btn_Approve_request");
                LinkButton btnApprove = (LinkButton)FvViewApprove.FindControl("btnApprove");
                Panel BoxddlApprove = (Panel)FvViewApprove.FindControl("BoxddlApprove");
                pnnameapprove.Visible = true;
                btn_Approve_request.Visible = true;
                btnApprove.Visible = false;
                BoxddlApprove.Visible = false;
                break;
            case "btntransfer_employee":
                FvTranfer.ChangeMode(FormViewMode.Insert);
                box_tranfer.Visible = true;
                break;

            case "btn_Approve_request":
                Update_Request();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btn_Approve_tranfer":
                Update_Tranfer();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }
    #endregion

}