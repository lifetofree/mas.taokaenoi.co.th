﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_emps_parttime : System.Web.UI.Page
{
   #region Init
   data_emps dataEmps = new data_emps();
   service_execute servExec = new service_execute();
   function_tool _funcTool = new function_tool();
   string centralizedConn = "conn_centralized";
   string masConn = "conn_mas";
   string empsParttimeService = "empsParttimeService";
   string empsRewardService = "empsRewardService";
   string empsodspeService = "empsodspeService";
   string _local_xml = null;
   #endregion Init

   #region Constant
   public static class Constants
   {
      #region for region Action
      public const int CREATE = 10;
      public const int UPDATE = 11;
      public const int SELECT_ALL = 20;
      public const int SELECT_WHERE = 21;
      public const int BAN = 30;
      public const int UNBAN = 31;
      public const int NUMBER_NULL = -1;
      #endregion for region Action
   }
   #endregion Constant

   #region Page Load
   protected void Page_Load(object sender, EventArgs e)
   {
      if (!IsPostBack)
      {
         getEmpLogin(int.Parse(Session["emp_idx"].ToString()));
         actionIndex();
      }
      injectJS();
   }
   #endregion Page Load

   #region Action
   protected void actionIndex()
   {
      parttime objParttime = new parttime();
      dataEmps.emps_parttime_action = new parttime[1];
      dataEmps.emps_parttime_action[0] = objParttime;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsParttimeService, dataEmps, Constants.SELECT_ALL);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      setGridData(GvMaster, dataEmps.emps_parttime_action);
   }

   protected void actionCreate()
   {
      var ddlShiftWorkReward = (DropDownList)fvCRUD.FindControl("ddlShiftWorkReward");
      var txtShiftWorkCode = (TextBox)fvCRUD.FindControl("txtShiftWorkCode");
      var txtShiftWorkNameTH = (TextBox)fvCRUD.FindControl("txtShiftWorkNameTH");
      var txtShiftWorkNameENG = (TextBox)fvCRUD.FindControl("txtShiftWorkNameENG");
      var txtShiftWorkNote = (TextBox)fvCRUD.FindControl("txtShiftWorkNote");
      var txtShiftWorkBgColor = (TextBox)fvCRUD.FindControl("txtShiftWorkBgColor");
      var ddlShiftWorkStartTime = (DropDownList)fvCRUD.FindControl("ddlShiftWorkStartTime");
      var txtShiftWorkStartTime = (TextBox)fvCRUD.FindControl("txtShiftWorkStartTime");
      var ddlShiftWorkEndTime = (DropDownList)fvCRUD.FindControl("ddlShiftWorkEndTime");
      var txtShiftWorkEndTime = (TextBox)fvCRUD.FindControl("txtShiftWorkEndTime");
      var ddlShiftWorkBetweenStartTime = (DropDownList)fvCRUD.FindControl("ddlShiftWorkBetweenStartTime");
      var txtShiftWorkBetweenStartTime = (TextBox)fvCRUD.FindControl("txtShiftWorkBetweenStartTime");
      var ddlShiftWorkBetweenEndTime = (DropDownList)fvCRUD.FindControl("ddlShiftWorkBetweenEndTime");
      var txtShiftWorkBetweenEndTime = (TextBox)fvCRUD.FindControl("txtShiftWorkBetweenEndTime");
      var ddlShiftWorkBreakStartTime = (DropDownList)fvCRUD.FindControl("ddlShiftWorkBreakStartTime");
      var txtShiftWorkBreakStartTime = (TextBox)fvCRUD.FindControl("txtShiftWorkBreakStartTime");
      var ddlShiftWorkBreakEndTime = (DropDownList)fvCRUD.FindControl("ddlShiftWorkBreakEndTime");
      var txtShiftWorkBreakEndTime = (TextBox)fvCRUD.FindControl("txtShiftWorkBreakEndTime");
      var chkShiftWorkAllowBreak = (CheckBox)fvCRUD.FindControl("chkShiftWorkAllowBreak");
      var chkShiftWorkTimeStand = (CheckBox)fvCRUD.FindControl("chkShiftWorkTimeStand");
      var txtShiftWorkAmountScanCard = (TextBox)fvCRUD.FindControl("txtShiftWorkAmountScanCard");
      var txtShiftWorkAmountWorkHours = (TextBox)fvCRUD.FindControl("txtShiftWorkAmountWorkHours");
      var txtShiftWorkAmountBreakHours = (TextBox)fvCRUD.FindControl("txtShiftWorkAmountBreakHours");
      var ddlParttimeStatus = (DropDownList)fvCRUD.FindControl("ddlParttimeStatus");

      parttime objParttime = new parttime();
      dataEmps.emps_parttime_action = new parttime[1];
      objParttime.m0_reward_idx_ref = int.Parse(ddlShiftWorkReward.SelectedValue);
      objParttime.parttime_code = txtShiftWorkCode.Text;
      objParttime.parttime_name_th = txtShiftWorkNameTH.Text;
      objParttime.parttime_name_eng = txtShiftWorkNameENG.Text;
      objParttime.parttime_note = txtShiftWorkNote.Text;
      objParttime.parttime_bg_color = txtShiftWorkBgColor.Text;
      objParttime.parttime_start_date = int.Parse(ddlShiftWorkStartTime.SelectedValue);
      objParttime.parttime_start_time = txtShiftWorkStartTime.Text + ":00";
      objParttime.parttime_end_date = int.Parse(ddlShiftWorkEndTime.SelectedValue);
      objParttime.parttime_end_time = txtShiftWorkEndTime.Text + ":00";
      objParttime.parttime_btw_start_date = int.Parse(ddlShiftWorkBetweenStartTime.SelectedValue);
      objParttime.parttime_btw_start_time = txtShiftWorkBetweenStartTime.Text + ":00";
      objParttime.parttime_btw_end_date = int.Parse(ddlShiftWorkBetweenEndTime.SelectedValue);
      objParttime.parttime_btw_end_time = txtShiftWorkBetweenEndTime.Text + ":00";
      if (chkShiftWorkAllowBreak.Checked == true)
      {
         objParttime.parttime_break_start_date = int.Parse(ddlShiftWorkBreakStartTime.SelectedValue);
         objParttime.parttime_break_start_time = txtShiftWorkBreakStartTime.Text + ":00";
         objParttime.parttime_break_end_date = int.Parse(ddlShiftWorkBreakEndTime.SelectedValue);
         objParttime.parttime_break_end_time = txtShiftWorkBreakEndTime.Text + ":00";
      }
      objParttime.parttime_amount_scan_card = int.Parse(txtShiftWorkAmountScanCard.Text);
      if (chkShiftWorkTimeStand.Checked == true)
      {
         objParttime.parttime_time_stand = 1;
      }
      objParttime.parttime_amount_work_hours = int.Parse(txtShiftWorkAmountWorkHours.Text);
      objParttime.parttime_amount_break_hours = int.Parse(txtShiftWorkAmountBreakHours.Text);
      objParttime.parttime_status = int.Parse(ddlParttimeStatus.SelectedValue);
      objParttime.parttime_created_by = int.Parse(ViewState["EmpIDX"].ToString());
      objParttime.parttime_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
      dataEmps.emps_parttime_action[0] = objParttime;
      servExec.actionExec(masConn, "data_emps", empsParttimeService, dataEmps, Constants.CREATE);
   }

   protected void actionUpdate(int id)
   {
      var ddlShiftWorkRewardUpdate = (DropDownList)fvCRUD.FindControl("ddlShiftWorkRewardUpdate");
      var txtShiftWorkCodeUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkCodeUpdate");
      var txtShiftWorkNameTHUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkNameTHUpdate");
      var txtShiftWorkNameENGUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkNameENGUpdate");
      var txtShiftWorkNoteUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkNoteUpdate");
      var txtShiftWorkBgColorUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkBgColorUpdate");
      var ddlShiftWorkStartTimeUpdate = (DropDownList)fvCRUD.FindControl("ddlShiftWorkStartTimeUpdate");
      var txtShiftWorkStartTimeUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkStartTimeUpdate");
      var ddlShiftWorkEndTimeUpdate = (DropDownList)fvCRUD.FindControl("ddlShiftWorkEndTimeUpdate");
      var txtShiftWorkEndTimeUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkEndTimeUpdate");
      var ddlShiftWorkBetweenStartTimeUpdate = (DropDownList)fvCRUD.FindControl("ddlShiftWorkBetweenStartTimeUpdate");
      var txtShiftWorkBetweenStartTimeUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkBetweenStartTimeUpdate");
      var ddlShiftWorkBetweenEndTimeUpdate = (DropDownList)fvCRUD.FindControl("ddlShiftWorkBetweenEndTimeUpdate");
      var txtShiftWorkBetweenEndTimeUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkBetweenEndTimeUpdate");
      var ddlShiftWorkBreakStartTimeUpdate = (DropDownList)fvCRUD.FindControl("ddlShiftWorkBreakStartTimeUpdate");
      var txtShiftWorkBreakStartTimeUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkBreakStartTimeUpdate");
      var ddlShiftWorkBreakEndTimeUpdate = (DropDownList)fvCRUD.FindControl("ddlShiftWorkBreakEndTimeUpdate");
      var txtShiftWorkBreakEndTimeUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkBreakEndTimeUpdate");
      var chkShiftWorkAllowBreakUpdate = (CheckBox)fvCRUD.FindControl("chkShiftWorkAllowBreakUpdate");
      var chkShiftWorkTimeStandUpdate = (CheckBox)fvCRUD.FindControl("chkShiftWorkTimeStandUpdate");
      var txtShiftWorkAmountScanCardUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkAmountScanCardUpdate");
      var txtShiftWorkAmountWorkHoursUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkAmountWorkHoursUpdate");
      var txtShiftWorkAmountBreakHoursUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkAmountBreakHoursUpdate");
      var ddlParttimeStatusUpdate = (DropDownList)fvCRUD.FindControl("ddlParttimeStatusUpdate");

      parttime objParttime = new parttime();
      dataEmps.emps_parttime_action = new parttime[1];
      objParttime.m0_parttime_idx = id;
      objParttime.m0_reward_idx_ref = int.Parse(ddlShiftWorkRewardUpdate.SelectedValue);
      objParttime.parttime_code = txtShiftWorkCodeUpdate.Text;
      objParttime.parttime_name_th = txtShiftWorkNameTHUpdate.Text;
      objParttime.parttime_name_eng = txtShiftWorkNameENGUpdate.Text;
      objParttime.parttime_note = txtShiftWorkNoteUpdate.Text;
      objParttime.parttime_bg_color = txtShiftWorkBgColorUpdate.Text;
      objParttime.parttime_start_date = int.Parse(ddlShiftWorkStartTimeUpdate.SelectedValue);
      objParttime.parttime_start_time = txtShiftWorkStartTimeUpdate.Text + ":00";
      objParttime.parttime_end_date = int.Parse(ddlShiftWorkEndTimeUpdate.SelectedValue);
      objParttime.parttime_end_time = txtShiftWorkEndTimeUpdate.Text + ":00";
      objParttime.parttime_btw_start_date = int.Parse(ddlShiftWorkBetweenStartTimeUpdate.SelectedValue);
      objParttime.parttime_btw_start_time = txtShiftWorkBetweenStartTimeUpdate.Text + ":00";
      objParttime.parttime_btw_end_date = int.Parse(ddlShiftWorkBetweenEndTimeUpdate.SelectedValue);
      objParttime.parttime_btw_end_time = txtShiftWorkBetweenEndTimeUpdate.Text + ":00";
      if (chkShiftWorkAllowBreakUpdate.Checked == true)
      {
         objParttime.parttime_break_start_date = int.Parse(ddlShiftWorkBreakStartTimeUpdate.SelectedValue);
         objParttime.parttime_break_start_time = txtShiftWorkBreakStartTimeUpdate.Text + ":00";
         objParttime.parttime_break_end_date = int.Parse(ddlShiftWorkBreakEndTimeUpdate.SelectedValue);
         objParttime.parttime_break_end_time = txtShiftWorkBreakEndTimeUpdate.Text + ":00";
      }
      else
      {
         objParttime.parttime_break_start_date = 0;
         objParttime.parttime_break_start_time = null;
         objParttime.parttime_break_end_date = 0;
         objParttime.parttime_break_end_time = null;
      }
      if (chkShiftWorkTimeStandUpdate.Checked == true)
      {
         objParttime.parttime_time_stand = 1;
      }
      else
      {
         objParttime.parttime_time_stand = 0;
      }
      objParttime.parttime_amount_scan_card = int.Parse(txtShiftWorkAmountScanCardUpdate.Text);
      objParttime.parttime_amount_work_hours = int.Parse(txtShiftWorkAmountWorkHoursUpdate.Text);
      objParttime.parttime_amount_break_hours = int.Parse(txtShiftWorkAmountBreakHoursUpdate.Text);
      objParttime.parttime_status = int.Parse(ddlParttimeStatusUpdate.SelectedValue);
      objParttime.parttime_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
      dataEmps.emps_parttime_action[0] = objParttime;
      servExec.actionExec(masConn, "data_emps", empsParttimeService, dataEmps, Constants.UPDATE);
   }

   protected void actionRead(int id)
   {
      parttime objParttime = new parttime();
      dataEmps.emps_parttime_action = new parttime[1];
      objParttime.m0_parttime_idx = id;
      dataEmps.emps_parttime_action[0] = objParttime;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsParttimeService, dataEmps, Constants.SELECT_WHERE);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      setFormData(fvCRUD, dataEmps.emps_parttime_action);
   }

   protected void actionBan(int id)
   {
      parttime objParttime = new parttime();
      dataEmps.emps_parttime_action = new parttime[1];
      objParttime.m0_parttime_idx = id;
      objParttime.parttime_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
      dataEmps.emps_parttime_action[0] = objParttime;
      servExec.actionExec(masConn, "data_emps", empsParttimeService, dataEmps, Constants.BAN);
   }

   protected void actionUnban(int id)
   {
      parttime objParttime = new parttime();
      dataEmps.emps_parttime_action = new parttime[1];
      objParttime.m0_parttime_idx = id;
      objParttime.parttime_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
      dataEmps.emps_parttime_action[0] = objParttime;
      servExec.actionExec(masConn, "data_emps", empsParttimeService, dataEmps, Constants.UNBAN);
   }
   #endregion Action

   #region btnCommand
   protected void btnCommand(object sender, CommandEventArgs e)
   {
      string cmdName = e.CommandName.ToString();
      string cmdArg = e.CommandArgument.ToString();
      switch (cmdName)
      {
         case "btnToInsert":
            GvMaster.Visible = false;
            btnToInsert.Visible = false;
            fvCRUD.ChangeMode(FormViewMode.Insert);
            getDDLShiftWorkReward(Constants.NUMBER_NULL);
            injectJS();
            break;

         case "btnView":
            actionRead(int.Parse(cmdArg));
            GvMaster.Visible = false;
            btnToInsert.Visible = false;
            break;

         case "btnToUpdate":
            GvMaster.Visible = false;
            btnToInsert.Visible = false;
            actionRead(int.Parse(cmdArg));
            fvCRUD.ChangeMode(FormViewMode.Edit);
            setFormData(fvCRUD, dataEmps.emps_parttime_action);
            getDDLShiftWorkReward(int.Parse(cmdArg));
            break;

         case "btnInsertShiftWork":
            actionCreate();
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnUpdateShiftWork":
            actionUpdate(int.Parse(cmdArg));
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnBan":
            actionBan(int.Parse(cmdArg));
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnUnBan":
            actionUnban(int.Parse(cmdArg));
            break;

         case "btnCancel":
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;
      }
   }
   #endregion btnCommand

   #region Get data from DB
   protected void getEmpLogin(int empId)
   {
      odspe objODSPE = new odspe();
      dataEmps.emps_odspe_action = new odspe[1];
      objODSPE.emp_idx = empId;
      dataEmps.emps_odspe_action[0] = objODSPE;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      ViewState["EmpIDX"] = empId;
   }

   protected void getDDLShiftWorkReward(int id)
   {
      rewards objReward = new rewards();
      dataEmps.emps_reward_action = new rewards[1];
      dataEmps.emps_reward_action[0] = objReward;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsRewardService, dataEmps, Constants.SELECT_ALL);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      if (id != Constants.NUMBER_NULL)
      {
         var lblShiftWorkRewardUpdate = (Label)fvCRUD.FindControl("lblShiftWorkRewardUpdate");
         var ddlShiftWorkRewardUpdate = (DropDownList)fvCRUD.FindControl("ddlShiftWorkRewardUpdate");
         ddlShiftWorkRewardUpdate.AppendDataBoundItems = true;
         ddlShiftWorkRewardUpdate.Items.Add(new ListItem("-- เลือกผลตอบแทน --", String.Empty));
         ddlShiftWorkRewardUpdate.DataSource = dataEmps.emps_reward_action;
         ddlShiftWorkRewardUpdate.DataTextField = "reward_name";
         ddlShiftWorkRewardUpdate.DataValueField = "m0_reward_idx";
         ddlShiftWorkRewardUpdate.DataBind();
         ddlShiftWorkRewardUpdate.SelectedValue = lblShiftWorkRewardUpdate.Text;
      }
      else
      {
         DropDownList ddlShiftWorkReward = (DropDownList)fvCRUD.FindControl("ddlShiftWorkReward");
         ddlShiftWorkReward.AppendDataBoundItems = true;
         ddlShiftWorkReward.Items.Add(new ListItem("-- เลือกผลตอบแทน --", String.Empty));
         ddlShiftWorkReward.DataSource = dataEmps.emps_reward_action;
         ddlShiftWorkReward.DataTextField = "reward_name";
         ddlShiftWorkReward.DataValueField = "m0_reward_idx";
         ddlShiftWorkReward.DataBind();
      }
   }
   #endregion Get data from DB

   #region Custom Functions
   protected string getStatus(int status)
   {
      if (status == 1)
      {
         return "<span class='status-online' data-toggle='tooltip' title='ONLINE'><i class='fa fa-check-circle fa-lg'></i></span>";
      }
      else
      {
         return "<span class='status-offline' data-toggle='tooltip' title='OFFLINE'><i class='fa fa-times-circle fa-lg'></i></span>";
      }
   }

   protected string getWhenDate(int date)
   {
      if (date == 1)
      {
         return "เมื่อวาน";
      }
      else if (date == 2)
      {
         return "วันนี้";
      }
      else if (date == 3)
      {
         return "พรุ่งนี้";
      }
      return null;
   }

   protected string getColorParttime(string hexColor)
   {
      return "<span style='display: block; width: 40px;height: 20px; border: 1px solid #ccc; background-color: " + hexColor + "'></span>";
   }

   protected void FvDetail_DataBound(object sender, EventArgs e)
   {
      var FvName = (FormView)sender;
      switch (FvName.ID)
      {
         case "fvCRUD":
            if (FvName.CurrentMode == FormViewMode.ReadOnly)
            {
               Label lblNotFound = (Label)fvCRUD.FindControl("lblNotFound");
               Label lblNotFoundForCheck = (Label)fvCRUD.FindControl("lblNotFoundForCheck");
               Control divData = fvCRUD.FindControl("divData");
               if (lblNotFoundForCheck.Text == "0" || lblNotFoundForCheck.Text == null)
               {
                  lblNotFound.Visible = true;
                  divData.Visible = false;
               }
               else
               {
                  lblNotFound.Visible = false;
                  divData.Visible = true;
               }
            }
            else if (FvName.CurrentMode == FormViewMode.Edit)
            {
               Label statusTimeStand = (Label)fvCRUD.FindControl("statusTimeStand");
               CheckBox chkShiftWorkTimeStandUpdate = (CheckBox)fvCRUD.FindControl("chkShiftWorkTimeStandUpdate");
               if (statusTimeStand.Text == "1")
               {
                  chkShiftWorkTimeStandUpdate.Checked = true;
               }
               Label lblBreakDate = (Label)fvCRUD.FindControl("lblBreakDate");
               CheckBox chkShiftWorkAllowBreakUpdate = (CheckBox)fvCRUD.FindControl("chkShiftWorkAllowBreakUpdate");
               DropDownList ddlShiftWorkBreakStartTimeUpdate = (DropDownList)fvCRUD.FindControl("ddlShiftWorkBreakStartTimeUpdate");
               TextBox txtShiftWorkBreakStartTimeUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkBreakStartTimeUpdate");
               DropDownList ddlShiftWorkBreakEndTimeUpdate = (DropDownList)fvCRUD.FindControl("ddlShiftWorkBreakEndTimeUpdate");
               TextBox txtShiftWorkBreakEndTimeUpdate = (TextBox)fvCRUD.FindControl("txtShiftWorkBreakEndTimeUpdate");
               if (lblBreakDate.Text == "0")
               {
                  chkShiftWorkAllowBreakUpdate.Checked = false;
                  ddlShiftWorkBreakStartTimeUpdate.Attributes.Add("disabled", "disabled");
                  txtShiftWorkBreakStartTimeUpdate.Attributes.Add("disabled", "disabled");
                  ddlShiftWorkBreakEndTimeUpdate.Attributes.Add("disabled", "disabled");
                  txtShiftWorkBreakEndTimeUpdate.Attributes.Add("disabled", "disabled");
               }
               else
               {
                  chkShiftWorkAllowBreakUpdate.Checked = true;
               }

               Label parttimeStatusForCheck = (Label)fvCRUD.FindControl("parttimeStatusForCheck");
               DropDownList ddlParttimeStatusUpdate = (DropDownList)fvCRUD.FindControl("ddlParttimeStatusUpdate");
               ddlParttimeStatusUpdate.AppendDataBoundItems = true;
               ddlParttimeStatusUpdate.DataSource = dataEmps.emps_reward_action;
               ddlParttimeStatusUpdate.DataTextField = "reward_name";
               ddlParttimeStatusUpdate.DataValueField = "m0_reward_idx";
               ddlParttimeStatusUpdate.DataBind();
               ddlParttimeStatusUpdate.SelectedValue = parttimeStatusForCheck.Text;
            }
            break;
      }
   }

   protected void setGridData(GridView gvName, Object obj)
   {
      gvName.DataSource = obj;
      gvName.DataBind();
   }

   protected void setFormData(FormView fvName, Object obj)
   {
      fvName.DataSource = obj;
      fvName.DataBind();
   }

   protected void setRepeaterData(Repeater rpName, Object obj)
   {
      rpName.DataSource = obj;
      rpName.DataBind();
   }
   #endregion Custom Functions

   #region Inject JS
   protected void injectJS()
   {
      string jsTextStart = String.Empty;
      string jsText = String.Empty;
      string jsTextEnd = String.Empty;
      jsTextStart +=
         "var prm = Sys.WebForms.PageRequestManager.getInstance();" +
         "prm.add_endRequest(function() {";
      /* START Create Form */
      jsText +=
         "$(function () {" + 
            "$('.colorpicker').colorpicker({align: 'left'});" +
         "});";
      jsText +=
         "$(function () {" +
            "$('.txtShiftWorkStartTime').clockpicker({" +
               "autoclose: true});" +
            "$('.txtShiftWorkEndTime').clockpicker({" +
               "autoclose: true});" +
         "});";
      jsText +=
         "$(function () {" +
            "$('.txtShiftWorkBreakStartTime').clockpicker({" +
               "autoclose: true});" +
            "$('.txtShiftWorkBreakEndTime').clockpicker({" +
               "autoclose: true});" +
         "});";
      jsText +=
         "$(function () {" +
            "$('.txtShiftWorkBetweenStartTime').clockpicker({" +
               "autoclose: true});" +
            "$('.txtShiftWorkBetweenEndTime').clockpicker({" +
               "autoclose: true});" +
         "});";
      jsText +=
         "$(function () {" +
            "$('.chkShiftWorkAllowBreak input').click(function () {" +
               "$('.ddlShiftWorkBreakStartTime').attr('disabled', !this.checked);" +
               "$('.txtShiftWorkBreakStartTime').attr('disabled', !this.checked);" +
               "$('.ddlShiftWorkBreakEndTime').attr('disabled', !this.checked);" +
               "$('.txtShiftWorkBreakEndTime').attr('disabled', !this.checked);" +
            "});" +
         "});";
      /* END Create Form */
      /* START Update Form */
      jsText +=
         "$('.txtShiftWorkStartTimeUpdate').clockpicker({" +
            "autoclose: true});" +
         "$('.txtShiftWorkEndTimeUpdate').clockpicker({" +
            "autoclose: true});";
      jsText +=
         "$('.txtShiftWorkBreakStartTimeUpdate').clockpicker({" +
            "autoclose: true});" +
         "$('.txtShiftWorkBreakEndTimeUpdate').clockpicker({" +
            "autoclose: true});";
      jsText +=
         "$('.txtShiftWorkBetweenStartTimeUpdate').clockpicker({" +
            "autoclose: true});" +
         "$('.txtShiftWorkBetweenEndTimeUpdate').clockpicker({" +
            "autoclose: true});";
      jsText +=
         "$('.chkShiftWorkAllowBreakUpdate input').click(function () {" +
            "$('.ddlShiftWorkBreakStartTimeUpdate').attr('disabled', !this.checked);" +
            "$('.txtShiftWorkBreakStartTimeUpdate').attr('disabled', !this.checked);" +
            "$('.ddlShiftWorkBreakEndTimeUpdate').attr('disabled', !this.checked);" +
            "$('.txtShiftWorkBreakEndTimeUpdate').attr('disabled', !this.checked);" +
         "});";
      /* END Update Form */
      jsTextEnd +=
         "});";
      string jsCall = jsTextStart + jsText + jsTextEnd;
      Page.ClientScript.RegisterStartupScript(this.GetType(), "", jsCall, true);
   }
   #endregion Inject JS
}
