﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_roombooking
/// </summary>
/// 
[Serializable]
[XmlRoot("data_ecom_promotion")]
public class data_ecom_promotion
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    // --- master data ---//
    [XmlElement("ecom_m0_promotion_list")]
    public ecom_m0_promotion[] ecom_m0_promotion_list { get; set; }

}

[Serializable]
public class ecom_m0_promotion
{
    [XmlElement("pro_idx")]
    public int pro_idx { get; set; }

    [XmlElement("PromotionDesc")]
    public string PromotionDesc { get; set; }

    [XmlElement("PromotionID")]
    public string PromotionID { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("emp_idx_update")]
    public int emp_idx_update { get; set; }

    [XmlElement("pro_status")]
    public int pro_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("type_promotion")]
    public int type_promotion { get; set; }

    [XmlElement("amount")]
    public float amount { get; set; }

    [XmlElement("ZAP_code")]
    public string ZAP_code { get; set; }

    [XmlElement("sap_and_desc")]
    public string sap_and_desc { get; set; }
    
}


