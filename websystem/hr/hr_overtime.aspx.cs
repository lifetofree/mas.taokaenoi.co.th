﻿using AjaxControlToolkit;

using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_overtime : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();
    data_overtime _data_overtime = new data_overtime();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];

    static string _urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetPlace"];

    //-- employee --//

    //-- over time --//
    static string _urlGetDateOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlGetDateOTMonth"];
    static string _urlGetTimeScanOTMont = _serviceUrl + ConfigurationManager.AppSettings["urlGetTimeScanOTMont"];
    static string _urlGetDetailOT = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailOT"];
    static string _urlGetDetailOTView = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailOTView"];
    static string _urlGetDetailOTViewLog = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailOTViewLog"];
    static string _urlGetOTMontView = _serviceUrl + ConfigurationManager.AppSettings["urlGetOTMontView"];
    static string _urlGetDetailWaitApproveOT = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailWaitApproveOT"];
    static string _urlGetCountWaitApproveOT = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountWaitApproveOT"];
    static string _urlGetDecisionWaitApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetDecisionWaitApprove"];
    static string _urlGetDateScanOTMont = _serviceUrl + ConfigurationManager.AppSettings["urlGetDateScanOTMont"];
    static string _urlGetTimeStartScanOTMont = _serviceUrl + ConfigurationManager.AppSettings["urlGetTimeStartScanOTMont"];

    static string _urlSetCreateOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlSetCreateOTMonth"];
    static string _urlSetHeadUserApproveOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlSetHeadUserApproveOTMonth"];
    static string _urlSetUserEditOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlSetUserEditOTMonth"];
    static string _urlGetHeadUserSetOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlGetHeadUserSetOTMonth"];
    static string _urlGetEditViewScanOTMont = _serviceUrl + ConfigurationManager.AppSettings["urlGetEditViewScanOTMont"];
    static string _urlSetGroupOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlSetGroupOTDay"];
    static string _urlGetDetailGroupOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailGroupOTDay"];
    static string _urlGetViewDetailGroupOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailGroupOTDay"];
    static string _urlSetDelGroupOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelGroupOTDay"];
    static string _urlGetTypeOTGroupOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetTypeOTGroupOTDay"];
    static string _urlSetCreateGroupOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlSetCreateGroupOTDay"];
    static string _urlSetOTMontHeadUser = _serviceUrl + ConfigurationManager.AppSettings["urlSetOTMontHeadUser"];
    static string _urlGetTypeDayWorkOT = _serviceUrl + ConfigurationManager.AppSettings["urlGetTypeDayWorkOT"];
    static string _urlGetTypeOTImport = _serviceUrl + ConfigurationManager.AppSettings["urlGetTypeOTImport"];
    static string _urlSetImportEmployeeOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlSetImportEmployeeOTDay"];
    static string _urlGetDetailEmployeeImportOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailEmployeeImportOTDay"];
    static string _urlGetViewDetailEmployeeImportOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailEmployeeImportOTDay"];
    static string _urlGetLogViewDetailEmployeeImportOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogViewDetailEmployeeImportOTDay"];
    static string _urlGetSearchReportDetailEmpOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchReportDetailEmpOTDay"];
    static string _urlGetReportOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlGetReportOTMonth"];
    static string _urlGetSearchOTMonthIndex = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchOTMonthIndex"];

    static string _urlGetDetailOTDayAdmin = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailOTDayAdmin"];
    static string _urlGetTimeStartOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetTimeStartOTDay"];
    static string _urlGetPermissionAdminCreate = _serviceUrl + ConfigurationManager.AppSettings["urlGetPermissionAdminCreate"];
    static string _urlGetDetailAdminCreateOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailAdminCreateOTDay"];
    static string _urlGetViewDetailAdminCreateOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailAdminCreateOTDay"];
    static string _urlGetViewDetailGvAdminOT = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailGvAdminOT"];
    static string _urlGetDetailToEmployeeOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailToEmployeeOTDay"];
    static string _urlGetViewDetailToEmployeeOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailToEmployeeOTDay"];
    static string _urlGetLogDetailEmployeeOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogDetailEmployeeOTDay"];
    static string _urlGetWaitApproveHeadOTDayAdminCreate = _serviceUrl + ConfigurationManager.AppSettings["urlGetWaitApproveHeadOTDayAdminCreate"];
    static string _urlSetApproveOTAdminCreate = _serviceUrl + ConfigurationManager.AppSettings["urlSetApproveOTAdminCreate"];
    static string _urlGetCountWaitApproveAdminCreate = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountWaitApproveAdminCreate"];
    static string _urlSetCreateOTByAdmin = _serviceUrl + ConfigurationManager.AppSettings["urlSetCreateOTByAdmin"];
    static string _urlGetDocStatusOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlGetDocStatusOTMonth"];
    static string _urlGetSearchWaitApproveOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchWaitApproveOTMonth"];


    static string _path_file_otmonth = ConfigurationManager.AppSettings["path_file_otmonth"];
    //-- over time --//


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;

    string _otx1 = "x1";
    string _otx2 = "x1.5";
    string _otx3 = "x3";


    int decision_hr_addusecar = 6; //hr add use car 

    decimal tot_actual_otdayimport26 = 0;
    decimal tot_actual_otdayimport27 = 0;
    decimal tot_actual_otdayimport28 = 0;
    decimal tot_actual_otdayimport29 = 0;
    decimal tot_actual_otdayimport30 = 0;
    decimal tot_actual_otdayimport31 = 0;
    decimal tot_actual_otdayimport1 = 0;
    decimal tot_actual_otdayimport2 = 0;
    decimal tot_actual_otdayimport3 = 0;
    decimal tot_actual_otdayimport4 = 0;
    decimal tot_actual_otdayimport5 = 0;
    decimal tot_actual_otdayimport6 = 0;
    decimal tot_actual_otdayimport7 = 0;
    decimal tot_actual_otdayimport8 = 0;
    decimal tot_actual_otdayimport9 = 0;
    decimal tot_actual_otdayimport10 = 0;
    decimal tot_actual_otdayimport11 = 0;
    decimal tot_actual_otdayimport12 = 0;
    decimal tot_actual_otdayimport13 = 0;
    decimal tot_actual_otdayimport14 = 0;
    decimal tot_actual_otdayimport15 = 0;
    decimal tot_actual_otdayimport16 = 0;
    decimal tot_actual_otdayimport17 = 0;
    decimal tot_actual_otdayimport18 = 0;
    decimal tot_actual_otdayimport19 = 0;
    decimal tot_actual_otdayimport20 = 0;
    decimal tot_actual_otdayimport21 = 0;
    decimal tot_actual_otdayimport22 = 0;
    decimal tot_actual_otdayimport23 = 0;
    decimal tot_actual_otdayimport24 = 0;
    decimal tot_actual_otdayimport25 = 0;

    //total in row
    decimal tot_actual_26 = 0;
    decimal tot_actual_27 = 0;
    decimal tot_actual_28 = 0;
    decimal tot_actual_29 = 0;
    decimal tot_actual_30 = 0;
    decimal tot_actual_31 = 0;
    decimal tot_actual_1 = 0;
    decimal tot_actual_2 = 0;
    decimal tot_actual_3 = 0;
    decimal tot_actual_4 = 0;
    decimal tot_actual_5 = 0;
    decimal tot_actual_6 = 0;
    decimal tot_actual_7 = 0;
    decimal tot_actual_8 = 0;
    decimal tot_actual_9 = 0;
    decimal tot_actual_10 = 0;
    decimal tot_actual_11 = 0;
    decimal tot_actual_12 = 0;
    decimal tot_actual_13 = 0;
    decimal tot_actual_14 = 0;
    decimal tot_actual_15 = 0;
    decimal tot_actual_16 = 0;
    decimal tot_actual_17 = 0;
    decimal tot_actual_18 = 0;
    decimal tot_actual_19 = 0;
    decimal tot_actual_20 = 0;
    decimal tot_actual_21 = 0;
    decimal tot_actual_22 = 0;
    decimal tot_actual_23 = 0;
    decimal tot_actual_24 = 0;
    decimal tot_actual_25 = 0;
    decimal tot_actual_sumot = 0;
    //total in row

    decimal tot_actual_otmonth = 0;
    decimal tot_actual_otday = 0;
    decimal tot_actualoutplan = 0;

    decimal tot_actual_otbefore = 0;
    decimal tot_actual_otafter = 0;
    decimal tot_actual_otholiday = 0;

    decimal totactual_otbefore = 0;
    decimal totactual_otafter = 0;
    decimal totactual_otholiday = 0;

    int returnResult = 0;
    int condition_ = 0;
    int _set_statusFilter = 0;
    string _sumhour = "";
    string _total_floor = "";
    string _total_time = "";
    //set rpos hr tab approve
    string set_rpos_idx_hr = "5903,5906";

    //ViewState["vs_ViewWaitApproveRoomBooking"]

    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;
        ViewState["emp_type_permission"] = _dataEmployee.employee_list[0].emp_type_idx;
        ViewState["time_idx_permission"] = _dataEmployee.employee_list[0].ACIDX; // shift type


        Session["DetailCarUseIDXSearch"] = 0;
        Session["DetailtypecarSearch"] = 0;
        Session["ddlm0car_idxSearch"] = 0;

        ViewState["_value_timescan_late_add"] = 0;

        //////Set tab Head set OT Month
        ////if (int.Parse(Session["emp_idx"].ToString()) == 1413)
        ////{
        ////    li5.Visible = true;
        ////}
        ////else
        ////{
        ////    li5.Visible = false;
        ////}
        //////Set tab Head set OT Month


        //////Set tab Head set OT Month
        //////if (int.Parse(ViewState["joblevel_permission"].ToString()) > 7)
        ////if (int.Parse(Session["emp_idx"].ToString()) == 1413)
        ////{
        ////    li2.Visible = true;
        ////}
        ////else
        ////{
        ////    li2.Visible = false;
        ////}
        //////Set tab Head set OT Month

        //////Set tab wait approve with head user and HR
        ////string[] setTabReportHR = set_rpos_idx_hr.Split(',');
        ////for (int i = 0; i < setTabReportHR.Length; i++)
        ////{
        ////    if (setTabReportHR[i] == ViewState["rpos_permission"].ToString() || int.Parse(Session["emp_idx"].ToString()) == 1413)
        ////    {
        ////        li6.Visible = true;
        ////        li7.Visible = true;
        ////        li8.Visible = true;

        ////        break;
        ////    }
        ////    else
        ////    {
        ////        li6.Visible = false;
        ////        li7.Visible = false;
        ////        li8.Visible = false;

        ////    }

        ////}
        //////Set Permission Tab Report with HR


        ////if (int.Parse(Session["emp_idx"].ToString()) == 1413)
        ////{
        ////    lbDetailMonth.Visible = true;
        ////    liDetailOTDay.Visible = true;
        ////}
        ////else if(int.Parse(ViewState["emp_type_permission"].ToString()) == 2) // ot month
        ////{
        ////    lbDetailMonth.Visible = true;
        ////    liDetailOTDay.Visible = false;
        ////}
        ////else if (int.Parse(ViewState["emp_type_permission"].ToString()) == 1) // ot day
        ////{
        ////    lbDetailMonth.Visible = false;
        ////    liDetailOTDay.Visible = true;

        ////}



    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            getCountWaitDetailApprove();
            getCountWaitDetailApproveOTAdminCreate();

            initPage();
            initPageLoad();

            linkBtnTrigger(lbAddGroupOTDay);
            linkBtnTrigger(lbImportOTDay);
            linkBtnTrigger(lbReportOTDay);
            linkBtnTrigger(lbDetailImportOTDay);
            linkBtnTrigger(lbCreateMonth);
            ////linkBtnTrigger(btnImportExcelOTDay);

            

        }
        linkBtnTrigger(lbAddGroupOTDay);
        linkBtnTrigger(lbImportOTDay);
        linkBtnTrigger(lbReportOTDay);
        linkBtnTrigger(lbDetailImportOTDay);
        linkBtnTrigger(lbCreateMonth);
        ////linkBtnTrigger(btnImportExcelOTDay);

    }

    #region set/get bind data

    protected void getDetailBackIndex()
    {
        Update_BackToDetail.Visible = false;
        setFormData(FvDetailOTMont, FormViewMode.ReadOnly, null);
        GvViewDetailOTMont.Visible = false;
        setGridData(GvViewDetailOTMont, null);
        div_Addfileotmonth.Visible = false;

        GvViewEditDetailOTMont.Visible = false;
        setGridData(GvViewEditDetailOTMont, null);

        div_GvViewEditDetailOTMont.Visible = false;
        Update_PanelSaveEditDocMonth.Visible = false;


        div_GvViewDetailOTMont.Visible = false;
        GvFileMenoOTMonth.Visible = false;
        setGridData(GvFileMenoOTMonth, null);
        div_LogViewDetailMonth.Visible = false;


        _PanelSearchDetail.Visible = true;

        data_overtime data_u0_searchotmonth_ = new data_overtime();
        ovt_search_otmonth_detail u0_searchotmonth_ = new ovt_search_otmonth_detail();
        data_u0_searchotmonth_.ovt_search_otmonth_list = new ovt_search_otmonth_detail[1];

        u0_searchotmonth_.cemp_idx = _emp_idx;
        u0_searchotmonth_.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        u0_searchotmonth_.emp_type = 2;
        u0_searchotmonth_.emp_idx = int.Parse(ddlSearchemp.SelectedValue);
        u0_searchotmonth_.month_idx = int.Parse(ddlSearchMonth.SelectedValue);
        u0_searchotmonth_.year_idx = int.Parse(ddlSearchYear.SelectedValue);
        u0_searchotmonth_.org_idx = int.Parse(ddlSearchorg.SelectedValue);
        u0_searchotmonth_.rdept_idx = int.Parse(ddlSearchrdept.SelectedValue);
        u0_searchotmonth_.rsec_idx = int.Parse(ddlSearchrsec.SelectedValue);
        u0_searchotmonth_.emp_code = txtSearchEmpcode.Text;
        u0_searchotmonth_.costcenter = txtSearchCostcenter.Text;
        u0_searchotmonth_.staidx = int.Parse(ddlStatusDoc.SelectedValue);


        data_u0_searchotmonth_.ovt_search_otmonth_list[0] = u0_searchotmonth_;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_searchotmonth));
        data_u0_searchotmonth_ = callServicePostOvertime(_urlGetSearchOTMonthIndex, data_u0_searchotmonth_);
        //
        if (data_u0_searchotmonth_.return_code == 0)
        {
            GvDetailOTMont.Visible = true;
            ViewState["Vs_DetailOT"] = data_u0_searchotmonth_.ovt_search_otmonth_list;
            setGridData(GvDetailOTMont, ViewState["Vs_DetailOT"]);
        }
        else
        {
            GvDetailOTMont.Visible = true;
            ViewState["Vs_DetailOT"] = null;
            setGridData(GvDetailOTMont, ViewState["Vs_DetailOT"]);
        }
    }

    protected void getHourToOT(DropDownList ddlNane, double _hour)
    {
        ddlNane.AppendDataBoundItems = true;
        ddlNane.Items.Clear();

        if (Convert.ToDouble(_hour) >= 0.5)
        {
            ddlNane.Visible = true;
            double _timescan = Convert.ToDouble(_hour);
            for (double i = 0.0; i <= _timescan; i += 0.5)
            {
                ddlNane.Items.Add((i).ToString());
            }

            ddlNane.Items.FindByValue(Convert.ToString(_timescan)).Selected = true;

        }
        else
        {
            ddlNane.Visible = false;
            ddlNane.SelectedValue = "0";
        }
    }

    protected void getTimeStartOTDay(string _datesearch)
    {
        data_overtime data_timestart_otday_detail = new data_overtime();
        ovt_timestart_otday_detail timestart_otday_detail = new ovt_timestart_otday_detail();
        data_timestart_otday_detail.ovt_timestart_otday_list = new ovt_timestart_otday_detail[1];
        timestart_otday_detail.condition = 0;
        timestart_otday_detail.datesearch = _datesearch.ToString();
        //timestart_otday_detail.d = 0;

        data_timestart_otday_detail.ovt_timestart_otday_list[0] = timestart_otday_detail;

        data_timestart_otday_detail = callServicePostOvertime(_urlGetTimeStartOTDay, data_timestart_otday_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_timestart_otday_detail));
        if (data_timestart_otday_detail.return_code == 0)
        {
            ViewState["Vs_TimeStartOTDay"] = data_timestart_otday_detail.ovt_timestart_otday_list;
        }
        else
        {
            ViewState["Vs_TimeStartOTDay"] = null;
        }


    }

    protected void getTimeEndOTDay(string _datesearch)
    {
        data_overtime data_timeend_otday_detail = new data_overtime();
        ovt_timestart_otday_detail timeend_otday_detail = new ovt_timestart_otday_detail();
        data_timeend_otday_detail.ovt_timestart_otday_list = new ovt_timestart_otday_detail[1];
        timeend_otday_detail.condition = 1;
        timeend_otday_detail.datesearch = _datesearch.ToString();

        data_timeend_otday_detail.ovt_timestart_otday_list[0] = timeend_otday_detail;

        data_timeend_otday_detail = callServicePostOvertime(_urlGetTimeStartOTDay, data_timeend_otday_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_timeend_otday_detail));
        if (data_timeend_otday_detail.return_code == 0)
        {
            ViewState["Vs_TimeEndOTDay"] = data_timeend_otday_detail.ovt_timestart_otday_list;
        }
        else
        {
            ViewState["Vs_TimeEndOTDay"] = null;
        }


    }

    protected void getTimeStartOTDay_Scan(string _datesearch)
    {
        data_overtime data_timestart_otdayscan_detail = new data_overtime();
        ovt_timestart_otday_detail timestart_otdayscan_detail = new ovt_timestart_otday_detail();
        data_timestart_otdayscan_detail.ovt_timestart_otday_list = new ovt_timestart_otday_detail[1];
        timestart_otdayscan_detail.condition = 2;
        timestart_otdayscan_detail.datesearch = _datesearch.ToString();

        data_timestart_otdayscan_detail.ovt_timestart_otday_list[0] = timestart_otdayscan_detail;

        data_timestart_otdayscan_detail = callServicePostOvertime(_urlGetTimeStartOTDay, data_timestart_otdayscan_detail);

        if (data_timestart_otdayscan_detail.return_code == 0)
        {
            ViewState["Vs_TimeStartOTDay_Scan"] = data_timestart_otdayscan_detail.ovt_timestart_otday_list;
        }
        else
        {
            ViewState["Vs_TimeStartOTDay_Scan"] = null;
        }


    }

    protected void getTimeEndOTDay_Scan(string _datesearch)
    {
        data_overtime data_timeend_otdayscan_detail = new data_overtime();
        ovt_timestart_otday_detail timeend_otdayscan_detail = new ovt_timestart_otday_detail();
        data_timeend_otdayscan_detail.ovt_timestart_otday_list = new ovt_timestart_otday_detail[1];
        timeend_otdayscan_detail.condition = 3;
        timeend_otdayscan_detail.datesearch = _datesearch.ToString();

        data_timeend_otdayscan_detail.ovt_timestart_otday_list[0] = timeend_otdayscan_detail;

        data_timeend_otdayscan_detail = callServicePostOvertime(_urlGetTimeStartOTDay, data_timeend_otdayscan_detail);

        if (data_timeend_otdayscan_detail.return_code == 0)
        {
            ViewState["Vs_TimeEndOTDay_Scan"] = data_timeend_otdayscan_detail.ovt_timestart_otday_list;
        }
        else
        {
            ViewState["Vs_TimeEndOTDay_Scan"] = null;
        }



    }

    protected void getDetailOTDayAdmin(GridView gvName)
    {
        data_overtime data_otday_detail = new data_overtime();
        ovt_otday_detail otday_detail = new ovt_otday_detail();
        data_otday_detail.ovt_otday_list = new ovt_otday_detail[1];

        data_otday_detail.ovt_otday_list[0] = otday_detail;
        data_otday_detail = callServicePostOvertime(_urlGetDetailOTDayAdmin, data_otday_detail);

        if (data_otday_detail.return_code == 0)
        {
            ViewState["Vs_ViewDetailCreateOTDay"] = data_otday_detail.ovt_otday_list;

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_ViewDetailCreateOTDay"]);
            div_btnAdminCreateOTDay.Visible = true;
        }
        else
        {
            ViewState["Vs_ViewDetailCreateOTDay"] = null;
            setGridData(gvName, ViewState["Vs_ViewDetailCreateOTDay"]);
            div_btnAdminCreateOTDay.Visible = false;
        }

    }


    protected void getLocation(DropDownList ddlName, int _locidx)
    {

        data_employee _dtEmployee = new data_employee();
        employee_detail search = new employee_detail();
        _dtEmployee.employee_list = new employee_detail[1];

        search.org_idx = 1;

        _dtEmployee.employee_list[0] = search;
        _dtEmployee = callServiceEmployee(_urlSelect_Location, _dtEmployee);

        setDdlData(ddlName, _dtEmployee.employee_list, "LocName", "LocIDX");
        ddlName.Items.Insert(0, new ListItem("--- เลือกโรงงาน---", "0"));
        ddlName.SelectedValue = _locidx.ToString();

    }

    protected void getTypeOTDay(DropDownList ddlName, int type_idx)
    {

        data_overtime data_m0type_detail = new data_overtime();
        ovt_m0_type_detail m0type_detail = new ovt_m0_type_detail();
        data_m0type_detail.ovt_m0_type_list = new ovt_m0_type_detail[1];

        data_m0type_detail.ovt_m0_type_list[0] = m0type_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0type_detail = callServicePostOvertime(_urlGetTypeOTGroupOTDay, data_m0type_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0type_detail));

        setDdlData(ddlName, data_m0type_detail.ovt_m0_type_list, "type_name", "type_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทการขอ OT รายวัน ---", "0"));
    }

    protected void getViewDetailGroupOTDay(GridView gvName, int m0_idx)
    {

        data_overtime data_viewm0group_detail = new data_overtime();
        ovt_m1_group_detail m0group_viewdetail = new ovt_m1_group_detail();
        data_viewm0group_detail.ovt_m1_group_list = new ovt_m1_group_detail[1];
        m0group_viewdetail.m0_group_idx = m0_idx;
        data_viewm0group_detail.ovt_m1_group_list[0] = m0group_viewdetail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_employee_otmont_detail));

        data_viewm0group_detail = callServicePostOvertime(_urlGetViewDetailGroupOTDay, data_viewm0group_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_employee_otmont_detail));

        ViewState["Vs_ViewDetailGroupOTDay"] = data_viewm0group_detail.ovt_m1_group_list;

        gvName.Visible = true;
        setGridData(gvName, ViewState["Vs_ViewDetailGroupOTDay"]);
    }

    protected void getDetailGroupOTDay()
    {

        data_overtime data_m0group_detail = new data_overtime();
        ovt_m0_group_detail m0group_detail = new ovt_m0_group_detail();
        data_m0group_detail.ovt_m0_group_list = new ovt_m0_group_detail[1];

        data_m0group_detail.ovt_m0_group_list[0] = m0group_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_employee_otmont_detail));

        data_m0group_detail = callServicePostOvertime(_urlGetDetailGroupOTDay, data_m0group_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_employee_otmont_detail));

        ViewState["Vs_DetailGroupOTDay"] = data_m0group_detail.ovt_m0_group_list;

        GvDetailGroupOTDay.Visible = true;
        setGridData(GvDetailGroupOTDay, ViewState["Vs_DetailGroupOTDay"]);
    }

    protected void getGroupOTDay(DropDownList ddlName, int _m0group_idx)
    {
        data_overtime data_m0group_detail = new data_overtime();
        ovt_m0_group_detail m0group_detail = new ovt_m0_group_detail();
        data_m0group_detail.ovt_m0_group_list = new ovt_m0_group_detail[1];

        data_m0group_detail.ovt_m0_group_list[0] = m0group_detail;

        data_m0group_detail = callServicePostOvertime(_urlGetDetailGroupOTDay, data_m0group_detail);
        setDdlData(ddlName, data_m0group_detail.ovt_m0_group_list, "group_name", "m0_group_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกกลุ่ม ---", "0"));
    }

    protected void getTypeDayWork(DropDownList ddlName)
    {
        data_overtime data_m0_type_daywork_detail = new data_overtime();
        ovt_m0_type_daywork_detail m0_type_daywork_detail = new ovt_m0_type_daywork_detail();
        data_m0_type_daywork_detail.ovt_m0_type_daywork_list = new ovt_m0_type_daywork_detail[1];

        data_m0_type_daywork_detail.ovt_m0_type_daywork_list[0] = m0_type_daywork_detail;

        data_m0_type_daywork_detail = callServicePostOvertime(_urlGetTypeDayWorkOT, data_m0_type_daywork_detail);
        setDdlData(ddlName, data_m0_type_daywork_detail.ovt_m0_type_daywork_list, "type_daywork_name", "type_daywork_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกวันทำงาน ---", "0"));
    }

    protected void getTypeOT(DropDownList ddlName)
    {
        data_overtime data_m0_type_ot_detail = new data_overtime();
        ovt_m0_typeot_detail m0_type_ot_detail = new ovt_m0_typeot_detail();
        data_m0_type_ot_detail.ovt_m0_typeot_list = new ovt_m0_typeot_detail[1];

        data_m0_type_ot_detail.ovt_m0_typeot_list[0] = m0_type_ot_detail;

        data_m0_type_ot_detail = callServicePostOvertime(_urlGetTypeOTImport, data_m0_type_ot_detail);
        setDdlData(ddlName, data_m0_type_ot_detail.ovt_m0_typeot_list, "type_ot_name", "type_ot_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภท OT ---", "0"));
    }

    protected void getEmployeeSetOTMonth()
    {

        data_overtime data_employee_otmont_detail = new data_overtime();
        ovt_employeeset_otmonth_detail employeeset_setotmonth = new ovt_employeeset_otmonth_detail();
        data_employee_otmont_detail.ovt_employeeset_otmonth_list = new ovt_employeeset_otmonth_detail[1];

        employeeset_setotmonth.cemp_idx = _emp_idx;
        employeeset_setotmonth.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        employeeset_setotmonth.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        employeeset_setotmonth.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        employeeset_setotmonth.joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_employee_otmont_detail.ovt_employeeset_otmonth_list[0] = employeeset_setotmonth;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_employee_otmont_detail));

        data_employee_otmont_detail = callServicePostOvertime(_urlGetHeadUserSetOTMonth, data_employee_otmont_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_employee_otmont_detail));

        ViewState["Vs_EmployeeSetOTMonth"] = data_employee_otmont_detail.ovt_employeeset_otmonth_list;

        GvEmployeeSetOTMonth.Visible = true;
        setGridData(GvEmployeeSetOTMonth, ViewState["Vs_EmployeeSetOTMonth"]);
    }

    protected void getStatusDocList(DropDownList ddlName)
    {
        data_overtime data_docStatus = new data_overtime();
        ovt_m0_status_detail m0_docstatus = new ovt_m0_status_detail();
        data_docStatus.ovt_m0_status_list = new ovt_m0_status_detail[1];

        data_docStatus.ovt_m0_status_list[0] = m0_docstatus;

        data_docStatus = callServicePostOvertime(_urlGetDocStatusOTMonth, data_docStatus);
        setDdlData(ddlName, data_docStatus.ovt_m0_status_list, "status_desc", "staidx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานะรายการ ---", "0"));
    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));
    }

    protected void getEmpList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _empList = new employee_detail();
        _empList.org_idx = _org_idx;
        _empList.rdept_idx = _rdept_idx;
        _empList.rsec_idx = _rsec_idx;
        _empList.emp_status = 1;
        _dataEmployee.employee_list[0] = _empList;


        _dataEmployee = callServicePostEmployee(urlGetAll, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.employee_list, "emp_name_th", "emp_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกพนักงาน ---", "0"));
    }

    protected void getddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }
        ddlName.Items.Insert(0, new ListItem("--- เลือกปี ---", "0"));
        ////ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    protected void getDetailOT(GridView gvName, int _emp_type)
    {

        data_overtime data_u0_document_detail = new data_overtime();
        ovt_u0_document_detail u0_document_detail = new ovt_u0_document_detail();
        data_u0_document_detail.ovt_u0_document_list = new ovt_u0_document_detail[1];
        u0_document_detail.cemp_idx = _emp_idx;
        u0_document_detail.emp_type = _emp_type;
        u0_document_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0_document_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u0_document_detail.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        u0_document_detail.joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u0_document_detail.ovt_u0_document_list[0] = u0_document_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_u0_document_detail));

        data_u0_document_detail = callServicePostOvertime(_urlGetDetailOT, data_u0_document_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_detail));

        ViewState["Vs_DetailOT"] = data_u0_document_detail.ovt_u0_document_list;

        gvName.Visible = true;
        setGridData(gvName, ViewState["Vs_DetailOT"]);
    }

    protected void getDetailEmployeeImportOTDay(GridView gvName, int _emp_type)
    {

        data_overtime data_u0_importday_detail = new data_overtime();
        ovt_u0_document_day_detail u0_importday_detail = new ovt_u0_document_day_detail();
        data_u0_importday_detail.ovt_u0_document_day_list = new ovt_u0_document_day_detail[1];
        u0_importday_detail.cemp_idx = _emp_idx;
        u0_importday_detail.emp_type = _emp_type;
        u0_importday_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0_importday_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u0_importday_detail.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        u0_importday_detail.joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u0_importday_detail.ovt_u0_document_day_list[0] = u0_importday_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_importday_detail));
        data_u0_importday_detail = callServicePostOvertime(_urlGetDetailEmployeeImportOTDay, data_u0_importday_detail);
        //
        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_detail));

        ViewState["Vs_DetailOTImportEmployeeDay"] = data_u0_importday_detail.ovt_u0_document_day_list;
        ViewState["Vs_DetailOTImportEmployeeDayFilter"] = data_u0_importday_detail.ovt_u0_document_day_list;
        gvName.Visible = true;
        setGridData(gvName, ViewState["Vs_DetailOTImportEmployeeDay"]);
    }

    protected void getWaitApproveDetailOT(GridView gvName, int _emp_type)
    {

        data_overtime data_u0_document_detail = new data_overtime();
        ovt_u0_document_detail u0_document_detail = new ovt_u0_document_detail();
        data_u0_document_detail.ovt_u0_document_list = new ovt_u0_document_detail[1];
        u0_document_detail.cemp_idx = _emp_idx;
        u0_document_detail.emp_type = _emp_type;
        u0_document_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0_document_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u0_document_detail.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        u0_document_detail.joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u0_document_detail.ovt_u0_document_list[0] = u0_document_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_detail));

        data_u0_document_detail = callServicePostOvertime(_urlGetDetailWaitApproveOT, data_u0_document_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_detail));

        ViewState["Vs_WaitApproveDetailOTMonth"] = data_u0_document_detail.ovt_u0_document_list;

        gvName.Visible = true;
        setGridData(gvName, ViewState["Vs_WaitApproveDetailOTMonth"]);
    }

    protected void getBackWaitApproveDetailOT()
    {

        Update_BackToWaitApprove.Visible = false;
        setFormData(FvDetailOTMontWaitapprove, FormViewMode.ReadOnly, null);
        setFormData(FvHeadUserApprove, FormViewMode.ReadOnly, null);
        GvViewWaitApproveOTMont.Visible = false;
        setGridData(GvViewWaitApproveOTMont, null);
        div_GvViewWaitApproveOTMont.Visible = false;

        GvFileMenoWaitOTMonth.Visible = false;
        setGridData(GvFileMenoWaitOTMonth, null);
        setFormData(FvHrApproveOTMonth, FormViewMode.ReadOnly, null);
        _div_LogViewDetailWaitMonth.Visible = false;

        _PanelSearchWaitApprove.Visible = true;

        data_overtime data_u0_searchwaitotmonth_approve = new data_overtime();
        ovt_search_otmonth_detail u0_searchwaitotmonth_approve = new ovt_search_otmonth_detail();
        data_u0_searchwaitotmonth_approve.ovt_search_otmonth_list = new ovt_search_otmonth_detail[1];

        u0_searchwaitotmonth_approve.cemp_idx = _emp_idx;
        u0_searchwaitotmonth_approve.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        u0_searchwaitotmonth_approve.emp_type = 2;
        u0_searchwaitotmonth_approve.emp_idx = int.Parse(ddlSearch_emp_WaitApprove.SelectedValue);
        u0_searchwaitotmonth_approve.month_idx = int.Parse(ddlSearchMonth_WaitApprove.SelectedValue);
        u0_searchwaitotmonth_approve.year_idx = int.Parse(ddlSearchYear_WaitApprove.SelectedValue);
        u0_searchwaitotmonth_approve.org_idx = int.Parse(ddlSearchorg_WaitApprove.SelectedValue);
        u0_searchwaitotmonth_approve.rdept_idx = int.Parse(ddlSearch_rdept_WaitApprove.SelectedValue);
        u0_searchwaitotmonth_approve.rsec_idx = int.Parse(ddlSearch_rsec_WaitApprove.SelectedValue);
        u0_searchwaitotmonth_approve.emp_code = txt_SearchEmpCode_WaitApprove.Text;
        u0_searchwaitotmonth_approve.costcenter = txt_SearchCost_WaitApprove.Text;
        //u0_searchwaitotmonth.staidx = int.Parse(ddlStatusDoc.SelectedValue);


        data_u0_searchwaitotmonth_approve.ovt_search_otmonth_list[0] = u0_searchwaitotmonth_approve;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_searchwaitotmonth));
        data_u0_searchwaitotmonth_approve = callServicePostOvertime(_urlGetSearchWaitApproveOTMonth, data_u0_searchwaitotmonth_approve);
        //
        if (data_u0_searchwaitotmonth_approve.return_code == 0)
        {
            GvWaitDetailOTMonth.Visible = true;
            ViewState["Vs_WaitApproveDetailOTMonth"] = data_u0_searchwaitotmonth_approve.ovt_search_otmonth_list;
            setGridData(GvWaitDetailOTMonth, ViewState["Vs_WaitApproveDetailOTMonth"]);
        }
        else
        {
            GvWaitDetailOTMonth.Visible = true;
            ViewState["Vs_WaitApproveDetailOTMonth"] = null;
            setGridData(GvWaitDetailOTMonth, ViewState["Vs_WaitApproveDetailOTMonth"]);
        }
    }

    protected void getWaitApproveDetailOTDayAdminCreate(GridView gvName, int m0_node_idx, int condition)
    {

        data_overtime data_u1doc_otday_detail = new data_overtime();
        ovt_u1doc_otday_detail u1doc_otday_detail = new ovt_u1doc_otday_detail();
        data_u1doc_otday_detail.ovt_u1doc_otday_list = new ovt_u1doc_otday_detail[1];

        u1doc_otday_detail.emp_idx = _emp_idx;
        u1doc_otday_detail.m0_node_idx = m0_node_idx;
        u1doc_otday_detail.condition = condition;
        u1doc_otday_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u1doc_otday_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u1doc_otday_detail.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        u1doc_otday_detail.joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u1doc_otday_detail.ovt_u1doc_otday_list[0] = u1doc_otday_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1doc_otday_detail));
        data_u1doc_otday_detail = callServicePostOvertime(_urlGetWaitApproveHeadOTDayAdminCreate, data_u1doc_otday_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1doc_otday_detail));

        if (data_u1doc_otday_detail.return_code == 0)
        {
            ViewState["Vs_WaitApproveDetailOTDayAdminCreate"] = data_u1doc_otday_detail.ovt_u1doc_otday_list;

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_WaitApproveDetailOTDayAdminCreate"]);
        }
        else
        {
            ViewState["Vs_WaitApproveDetailOTDayAdminCreate"] = null;

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_WaitApproveDetailOTDayAdminCreate"]);
        }

    }

    protected void getViewOTMonth(FormView fvName, int _u0_doc_idx)
    {

        data_overtime data_u0_document_viewot = new data_overtime();
        ovt_u0_document_detail u0_document_view = new ovt_u0_document_detail();
        data_u0_document_viewot.ovt_u0_document_list = new ovt_u0_document_detail[1];
        u0_document_view.u0_doc_idx = _u0_doc_idx;

        data_u0_document_viewot.ovt_u0_document_list[0] = u0_document_view;

        data_u0_document_viewot = callServicePostOvertime(_urlGetOTMontView, data_u0_document_viewot);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_viewot));  

        ViewState["Vs_ViewOTMonth"] = data_u0_document_viewot.ovt_u0_document_list;
        fvName.Visible = true;
        setFormData(fvName, FormViewMode.ReadOnly, ViewState["Vs_ViewOTMonth"]);
    }

    protected void getViewDetailImportDay(FormView fvName, int _u0_doc_idx, int _condition)
    {

        data_overtime data_u0_doc_viewimportotday = new data_overtime();
        ovt_u0_document_day_detail u0_document_viewimportotday = new ovt_u0_document_day_detail();
        data_u0_doc_viewimportotday.ovt_u0_document_day_list = new ovt_u0_document_day_detail[1];

        u0_document_viewimportotday.u0_doc_idx = _u0_doc_idx;
        u0_document_viewimportotday.condition = _condition;
        u0_document_viewimportotday.cemp_idx = _emp_idx;
        u0_document_viewimportotday.emp_type = 1;
        u0_document_viewimportotday.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0_document_viewimportotday.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u0_document_viewimportotday.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        u0_document_viewimportotday.joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u0_doc_viewimportotday.ovt_u0_document_day_list[0] = u0_document_viewimportotday;

        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_doc_viewimportotday));
        data_u0_doc_viewimportotday = callServicePostOvertime(_urlGetDetailEmployeeImportOTDay, data_u0_doc_viewimportotday);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_doc_viewimportotday));

        if (data_u0_doc_viewimportotday.return_code == 0)
        {
            ViewState["Vs_FvViewDetailEmployeeImportDay"] = data_u0_doc_viewimportotday.ovt_u0_document_day_list;
            fvName.Visible = true;
            setFormData(fvName, FormViewMode.ReadOnly, ViewState["Vs_FvViewDetailEmployeeImportDay"]);

            if (fvName == FvViewDetailEmployeeImportDay)
            {
                Label lbl_day_all_sum_viewimportvalue = (Label)fvName.FindControl("lbl_day_all_sum_viewimportvalue");
                Label lbl_day_all_sum_viewimport = (Label)fvName.FindControl("lbl_day_all_sum_viewimport");

                if (lbl_day_all_sum_viewimportvalue.Text == "" || lbl_day_all_sum_viewimportvalue.Text != null)
                {
                    lbl_day_all_sum_viewimport.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_day_all_sum_viewimportvalue.Text)) + " " + "ชั่วโมง";
                    //litDebug.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_day_all_sum_viewimportvalue.Text));
                }
            }
            else if (fvName == FvReportViewDetailEmployeeImportDay)
            {
                Label lbl_day_all_sum_viewimportvaluereport = (Label)fvName.FindControl("lbl_day_all_sum_viewimportvaluereport");
                Label lbl_day_all_sum_viewimportreport = (Label)fvName.FindControl("lbl_day_all_sum_viewimportreport");

                if (lbl_day_all_sum_viewimportvaluereport.Text == "" || lbl_day_all_sum_viewimportvaluereport.Text != null)
                {
                    lbl_day_all_sum_viewimportreport.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_day_all_sum_viewimportvaluereport.Text)) + " " + "ชั่วโมง";
                }
            }
            else
            {

            }

        }
    }

    protected void getViewDetailOTMonth(GridView gvName, int _u0_doc_idx, int _condition)
    {

        data_overtime data_u0_document_view = new data_overtime();
        ovt_u1_document_detail u0_document_view = new ovt_u1_document_detail();
        data_u0_document_view.ovt_u1_document_list = new ovt_u1_document_detail[1];
        u0_document_view.condition = _condition;
        u0_document_view.u0_doc_idx = _u0_doc_idx;

        data_u0_document_view.ovt_u1_document_list[0] = u0_document_view;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_view));
        data_u0_document_view = callServicePostOvertime(_urlGetDetailOTView, data_u0_document_view);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_view));

        ViewState["Vs_ViewDetailOTMonth"] = data_u0_document_view.ovt_u1_document_list;
        gvName.Visible = true;
        setGridData(gvName, ViewState["Vs_ViewDetailOTMonth"]);
        div_GvViewDetailOTMont.Visible = true;
    }

    protected void getFileViewDetailOTMonth(GridView gvName, int _u0_doc_idx, int _condition)
    {

        ////string getPathFileMemo = ConfigurationSettings.AppSettings["path_file_otmonth"];
        ////string idx_waittemp = "58000106-" + _u0_doc_idx.ToString();

        ////data_overtime data_u0_document_view = new data_overtime();
        ////ovt_u1_document_detail u0_document_view = new ovt_u1_document_detail();
        ////data_u0_document_view.ovt_u1_document_list = new ovt_u1_document_detail[1];
        ////u0_document_view.condition = _condition;
        ////u0_document_view.u0_doc_idx = _u0_doc_idx;

        ////data_u0_document_view.ovt_u1_document_list[0] = u0_document_view;
        //////test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));

        ////////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_view));
        ////data_u0_document_view = callServicePostOvertime(_urlGetDetailOTView, data_u0_document_view);
        //////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_view));

        ////ViewState["Vs_ViewDetailOTMonth"] = data_u0_document_view.ovt_u1_document_list;
        ////gvName.Visible = true;
        ////setGridData(gvName, ViewState["Vs_ViewDetailOTMonth"]);


        ////GetPathFile(idx_waittemp.ToString(), getPathFileMemo, gvName);

    }

    protected void GetPathFile(string _u0_doc_idx, string path, GridView gvName)
    {
        try
        {
            string filePathLotus = Server.MapPath(path + _u0_doc_idx);
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
            SearchDirectories(myDirLotus, _u0_doc_idx, gvName);
            gvName.Visible = true;

            ViewState["Vs_CheckFileMemo"] = "yes";

        }
        catch (Exception ex)
        {
            gvName.Visible = false;
            ViewState["Vs_CheckFileMemo"] = "no";
            //txt.Text = ex.ToString();
        }
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target, GridView gvName)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");


        FileInfo[] files = dir.GetFiles();
        int i = 0;
        foreach (FileInfo file in files)
        {
            //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
            //{
            string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
            dt1.Rows.Add(file.Name);
            dt1.Rows[i][1] = f[0];
            i++;
            //}
        }


        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gvName.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvName.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvName.DataSource = null;
            gvName.DataBind();

        }


    }

    protected void getViewDetailOTDay(GridView gvName, int _u0_doc_idx, int _condition)
    {

        data_overtime data_u0_document_viewotday = new data_overtime();
        ovt_u1_document_detail u0_document_viewotday = new ovt_u1_document_detail();
        data_u0_document_viewotday.ovt_u1_document_list = new ovt_u1_document_detail[1];
        u0_document_viewotday.u0_doc_idx = _u0_doc_idx;

        data_u0_document_viewotday.ovt_u1_document_list[0] = u0_document_viewotday;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));

        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_viewotday));
        ////data_u0_document_view = callServicePostOvertime(_urlGetDetailOTView, data_u0_document_view);
        //////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_view));

        ////ViewState["Vs_ViewDetailOTMonth"] = data_u0_document_view.ovt_u1_document_list;
        ////gvName.Visible = true;
        ////setGridData(gvName, ViewState["Vs_ViewDetailOTMonth"]);



    }

    protected void getViewDetailEditOTMonth(GridView gvName, int _u0_doc_idx, int _condition)
    {

        data_overtime data_u0_document_edit = new data_overtime();
        ovt_u1_document_detail u0_document_edit = new ovt_u1_document_detail();
        data_u0_document_edit.ovt_u1_document_list = new ovt_u1_document_detail[1];
        u0_document_edit.u0_doc_idx = _u0_doc_idx;
        u0_document_edit.cemp_idx = _emp_idx;

        data_u0_document_edit.ovt_u1_document_list[0] = u0_document_edit;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_edit));
        data_u0_document_edit = callServicePostOvertime(_urlGetEditViewScanOTMont, data_u0_document_edit);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_edit));
        ViewState["Vs_ViewDetailEditOTMonth"] = data_u0_document_edit.ovt_u1_document_list;
        gvName.Visible = true;
        setGridData(gvName, ViewState["Vs_ViewDetailEditOTMonth"]);
        div_GvViewEditDetailOTMont.Visible = true;


    }

    protected void getViewDetailEmployeeImportDay(GridView gvName, int _u0_doc_idx, int _condition)
    {

        data_overtime data_u0_document_viewimport = new data_overtime();
        ovt_u1_document_day_detail u0_document_viewimport = new ovt_u1_document_day_detail();
        data_u0_document_viewimport.ovt_u1_document_day_list = new ovt_u1_document_day_detail[1];
        u0_document_viewimport.condition = _condition;
        u0_document_viewimport.u0_doc_idx = _u0_doc_idx;

        data_u0_document_viewimport.ovt_u1_document_day_list[0] = u0_document_viewimport;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));

        data_u0_document_viewimport = callServicePostOvertime(_urlGetViewDetailEmployeeImportOTDay, data_u0_document_viewimport);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_viewimport));
        //////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_view));

        if (data_u0_document_viewimport.return_code == 0)
        {
            ViewState["Vs_ViewDetailEmployeeImportDay"] = data_u0_document_viewimport.ovt_u1_document_day_list;

            if (gvName == GvReportEmployeeImportDay)
            {
                divReportGvViewDetailEmployeeImportDay.Visible = true;
            }
            else
            {
                divGvViewDetailEmployeeImportDay.Visible = true;
            }

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_ViewDetailEmployeeImportDay"]);
        }


    }


    protected void getDetailOTDayToEmployee(FormView fvName, int _u0_doc_idx)
    {

        data_overtime data_u0doc_otday_view = new data_overtime();
        ovt_u0doc_otday_detail u0doc_otday_detail_view = new ovt_u0doc_otday_detail();
        data_u0doc_otday_view.ovt_u0doc_otday_list = new ovt_u0doc_otday_detail[1];


        u0doc_otday_detail_view.u0_docday_idx = _u0_doc_idx;

        data_u0doc_otday_view.ovt_u0doc_otday_list[0] = u0doc_otday_detail_view;

        //data_search_month = callServicePostOvertime(_urlGetDateScanOTMont, data_search_month);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth));
        data_u0doc_otday_view = callServicePostOvertime(_urlGetViewDetailAdminCreateOTDay, data_u0doc_otday_view);

        //ViewState["Vs_ViewDetailAdminOTDay"] = data_u0doc_otday_view.ovt_u0doc_otday_list;
        setFormData(fvName, FormViewMode.ReadOnly, data_u0doc_otday_view.ovt_u0doc_otday_list);



    }


    protected void getLogDetailOTDayAdminCreate(Repeater rptName, int _u0_doc_idx, int _u1_doc_idx)
    {

        data_overtime data_u2doc_otday_log = new data_overtime();
        ovt_u2doc_otday_detail u2doc_otday_log = new ovt_u2doc_otday_detail();
        data_u2doc_otday_log.ovt_u2doc_otday_list = new ovt_u2doc_otday_detail[1];

        u2doc_otday_log.u0_docday_idx = _u0_doc_idx;
        u2doc_otday_log.u1_docday_idx = _u1_doc_idx;

        data_u2doc_otday_log.ovt_u2doc_otday_list[0] = u2doc_otday_log;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_otmonth_log));
        data_u2doc_otday_log = callServicePostOvertime(_urlGetLogDetailEmployeeOTDay, data_u2doc_otday_log);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_otmonth_log));

        ViewState["vs_LogDetailOTDayAdminCreate"] = data_u2doc_otday_log.ovt_u2doc_otday_list;
        setRepeaterData(rptName, ViewState["vs_LogDetailOTDayAdminCreate"]);


    }

    protected void getLogDetailOTMonth(Repeater rptName, int _u0_doc_idx)
    {

        data_overtime data_u0_otmonth_log = new data_overtime();
        ovt_u2_document_detail u0_logdetailmonth = new ovt_u2_document_detail();
        data_u0_otmonth_log.ovt_u2_document_list = new ovt_u2_document_detail[1];
        u0_logdetailmonth.u0_doc_idx = _u0_doc_idx;

        data_u0_otmonth_log.ovt_u2_document_list[0] = u0_logdetailmonth;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_otmonth_log));
        data_u0_otmonth_log = callServicePostOvertime(_urlGetDetailOTViewLog, data_u0_otmonth_log);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_otmonth_log));

        ViewState["vs_LogDetailOTMonth"] = data_u0_otmonth_log.ovt_u2_document_list;
        setRepeaterData(rptName, ViewState["vs_LogDetailOTMonth"]);
    }

    protected void getLogDetailImportOTDay(Repeater rptName, int _u0_doc_idx)
    {

        data_overtime data_u0_logdetailimport = new data_overtime();
        ovt_u2_document_day_detail u0_logdetailimport = new ovt_u2_document_day_detail();
        data_u0_logdetailimport.ovt_u2_document_day_list = new ovt_u2_document_day_detail[1];

        u0_logdetailimport.u0_doc_idx = _u0_doc_idx;

        data_u0_logdetailimport.ovt_u2_document_day_list[0] = u0_logdetailimport;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));

        data_u0_logdetailimport = callServicePostOvertime(_urlGetLogViewDetailEmployeeImportOTDay, data_u0_logdetailimport);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_otmonth_log));
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_logdetailimport));
        ViewState["vs_LogDetailImportOTDay"] = data_u0_logdetailimport.ovt_u2_document_day_list;
        setRepeaterData(rptName, ViewState["vs_LogDetailImportOTDay"]);
    }

    protected void getCountWaitDetailApprove()
    {

        data_overtime data_u0_document_count = new data_overtime();
        ovt_u0_document_detail u0_document_count = new ovt_u0_document_detail();
        data_u0_document_count.ovt_u0_document_list = new ovt_u0_document_detail[1];

        u0_document_count.cemp_idx = _emp_idx;
        u0_document_count.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0_document_count.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u0_document_count.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        u0_document_count.joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u0_document_count.ovt_u0_document_list[0] = u0_document_count;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_count));

        data_u0_document_count = callServicePostOvertime(_urlGetCountWaitApproveOT, data_u0_document_count);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_count));

        if (data_u0_document_count.return_code == 0)
        {
            //litDebug.Text = "1";
            ViewState["vs_CountWaitApprove"] = data_u0_document_count.ovt_u0_document_list[0].count_waitapprove; // count all wait approve
            ViewState["vs_CountWaitApprove_Monthly"] = data_u0_document_count.ovt_u0_document_list[0].count_waitapprove_monthly; // count all wait approve
            ViewState["vs_CountWaitApprove_Daily"] = data_u0_document_count.ovt_u0_document_list[0].count_waitapprove_daily; // count all wait approve

            ////lbl_waitapprove.Text = " รายการรออนุมัติ OT <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
            lbWaitApproveMonth.Text = " รายการรออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_Monthly"]) + "</span>";
            ////lbWaitApproveDay.Text = " รายวัน <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_Daily"]) + "</span>";
            ////setActiveTab("docWaitApprove", 0, 0, 0, 0, 2, 0, 0);
            
            
            ////if (int.Parse(ViewState["vs_CountWaitApprove_Monthly"].ToString()) > 0)//ot month
            ////{
            ////    setActiveTab("docWaitApprove", 0, 0, 0, 0, 2, 0, 0);
            ////}
            ////else if (int.Parse(ViewState["vs_CountWaitApprove_Daily"].ToString()) > 0)//ot day
            ////{
            ////    setActiveTab("docWaitApprove", 0, 0, 0, 0, 1, 0, 0);
            ////    //  (string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx)
            ////}
            //else
            //{
            //    setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);
            //}


        }
        else
        {
            //litDebug.Text = "2";
            ViewState["vs_CountWaitApprove"] = 0;
            ViewState["vs_CountWaitApprove_Monthly"] = 0;
            ViewState["vs_CountWaitApprove_Daily"] = 0;

            ////lbl_waitapprove.Text = " รายการรออนุมัติ OT <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
            lbWaitApproveMonth.Text = " รายการรออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_Monthly"]) + "</span>";
            ////lbWaitApproveDay.Text = " รายวัน <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_Daily"]) + "</span>";

            setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);

            //lbl_waitapprove.Text = "รายการรออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
            //setActiveTab("docDetailCar", 0, 0, 0, 0, 0, 0);


        }

    }

    protected void getCountWaitDetailApproveOTAdminCreate()
    {

        data_overtime data_u1doc_otday_count = new data_overtime();
        ovt_u1doc_otday_detail u1doc_otday_count = new ovt_u1doc_otday_detail();
        data_u1doc_otday_count.ovt_u1doc_otday_list = new ovt_u1doc_otday_detail[1];

        u1doc_otday_count.emp_idx = _emp_idx;
        u1doc_otday_count.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u1doc_otday_count.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u1doc_otday_count.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        u1doc_otday_count.joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u1doc_otday_count.ovt_u1doc_otday_list[0] = u1doc_otday_count;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1doc_otday_count));

        data_u1doc_otday_count = callServicePostOvertime(_urlGetCountWaitApproveAdminCreate, data_u1doc_otday_count);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_count));

        if (data_u1doc_otday_count.return_code == 0)
        {
            //litDebug.Text = "1";
            ViewState["vs_CountWaitApprove_"] = data_u1doc_otday_count.ovt_u1doc_otday_list[0].count_waitapprove; // count all wait approve
            ViewState["vs_CountWaitApprove_Head"] = data_u1doc_otday_count.ovt_u1doc_otday_list[0].count_waitapprove_head; // count all wait approve
            ViewState["vs_CountWaitApprove_Hr"] = data_u1doc_otday_count.ovt_u1doc_otday_list[0].count_waitapprove_hr; // count all wait approve
            ViewState["vs_CountWaitApprove_AdminEdit"] = data_u1doc_otday_count.ovt_u1doc_otday_list[0].count_waitapprove_adminedit; // count all wait approve

            lbl_waitapprove_.Text = " รายการรออนุมัติ OT กะหมุนเวียน <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_"]) + "</span>";
            lbWaitApproveHeadAdminCreate.Text = " หัวหน้างาน <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_Head"]) + "</span>";
            lbWaitApproveHRAdminCreate.Text = " เจ้าหน้าที่ฝ่ายทรัพยากรบุคคล <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_Hr"]) + "</span>";
            lbWaitApproveAdminEdit.Text = " Admin แก้ไขรายการ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_AdminEdit"]) + "</span>";

            ////if (int.Parse(ViewState["vs_CountWaitApprove_Monthly"].ToString()) > 0)//ot month
            ////{
            ////    setActiveTab("docWaitApprove", 0, 0, 0, 0, 2, 0, 0);
            ////}
            ////else if (int.Parse(ViewState["vs_CountWaitApprove_Daily"].ToString()) > 0)//ot day
            ////{
            ////    setActiveTab("docWaitApprove", 0, 0, 0, 0, 1, 0, 0);
            ////    //  (string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx)
            ////}
            ////else
            ////{
            ////    setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);
            ////}


        }
        else
        {
            //litDebug.Text = "2";
            ViewState["vs_CountWaitApprove_"] = 0;
            ViewState["vs_CountWaitApprove_Head"] = 0;
            ViewState["vs_CountWaitApprove_Hr"] = 0;
            ViewState["vs_CountWaitApprove_AdminEdit"] = 0;

            lbl_waitapprove_.Text = " รายการรออนุมัติ OT กะหมุนเวียน <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_"]) + "</span>";
            lbWaitApproveHeadAdminCreate.Text = " หัวหน้างาน <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_Head"]) + "</span>";
            lbWaitApproveHRAdminCreate.Text = " เจ้าหน้าที่ฝ่ายทรัพยากรบุคคล <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_Hr"]) + "</span>";
            lbWaitApproveAdminEdit.Text = " Admin แก้ไขรายการ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_AdminEdit"]) + "</span>";
            ////setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);
            //lbl_waitapprove.Text = "รายการรออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
            //setActiveTab("docDetailCar", 0, 0, 0, 0, 0, 0);


        }

    }

    protected void getDecisionApprove(DropDownList ddlName, int _m0_node_idx, int _decision_idx)
    {

        data_overtime data_u0_document_node = new data_overtime();
        ovt_m0_node_decision_detail u0_document_node = new ovt_m0_node_decision_detail();
        data_u0_document_node.ovt_m0_node_decision_list = new ovt_m0_node_decision_detail[1];
        u0_document_node.noidx = _m0_node_idx;

        data_u0_document_node.ovt_m0_node_decision_list[0] = u0_document_node;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b)); 
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_node));
        data_u0_document_node = callServicePostOvertime(_urlGetDecisionWaitApprove, data_u0_document_node);


        setDdlData(ddlName, data_u0_document_node.ovt_m0_node_decision_list, "decision_name", "decision_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานะ ---", "0"));
        ddlName.SelectedValue = _decision_idx.ToString();
    }

    protected void getbtnDecisionApprove(Repeater rptName, int _m0_node_idx, int _decision_idx)
    {

        data_overtime data_u0_document_node = new data_overtime();
        ovt_m0_node_decision_detail u0_document_node = new ovt_m0_node_decision_detail();
        data_u0_document_node.ovt_m0_node_decision_list = new ovt_m0_node_decision_detail[1];
        u0_document_node.noidx = _m0_node_idx;

        data_u0_document_node.ovt_m0_node_decision_list[0] = u0_document_node;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b)); 
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_node));
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_node));
        data_u0_document_node = callServicePostOvertime(_urlGetDecisionWaitApprove, data_u0_document_node);

        setRepeaterData(rptName, data_u0_document_node.ovt_m0_node_decision_list);

        //setDdlData(ddlName, data_u0_document_node.ovt_m0_node_decision_list, "decision_name", "decision_idx");
        //ddlName.Items.Insert(0, new ListItem("--- เลือกสถานะ ---", "0"));
        //ddlName.SelectedValue = _decision_idx.ToString();
    }

    protected void getTimeStartList(DropDownList ddlName, int _emp_idx, string _tbEmpCode, string _date_start)
    {

        data_overtime data_u0_timestart_otmonth = new data_overtime();
        ovt_timestart_otmonth_detail u0_timestart_otmonth = new ovt_timestart_otmonth_detail();
        data_u0_timestart_otmonth.ovt_timestart_otmonth_list = new ovt_timestart_otmonth_detail[1];
        u0_timestart_otmonth.emp_idx = _emp_idx;
        u0_timestart_otmonth.emp_code = _tbEmpCode.ToString();
        u0_timestart_otmonth.date_start = _date_start;

        data_u0_timestart_otmonth.ovt_timestart_otmonth_list[0] = u0_timestart_otmonth;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));  ddl_timestart
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timestart_otmonth));
        data_u0_timestart_otmonth = callServicePostOvertime(_urlGetTimeStartScanOTMont, data_u0_timestart_otmonth);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timestart_otmonth));
        setDdlData(ddlName, data_u0_timestart_otmonth.ovt_timestart_otmonth_list, "time_start", "month_idx");
        //ddlName.Items.Insert(0, new ListItem("--- เลือกเวลาเริ่มทำ ---", "0"));
        //setDdlData(ddlName, data_u0_timestart_otmonth.ovt_timestart_otmonth_list, "time_start", "month_idx");


        ////TextBox txt_job = (TextBox)GvCreateOTMonth.FindControl("txt_job");

        ////if (ddlName.SelectedValue != "0")
        ////{
        ////    litDebug.Text += "1";

        ////    getTimeSumhour();
        ////    //_sumhour = "1";
        ////    //txt_job.Text = "1";


        ////}
        ////else
        ////{
        ////    litDebug.Text += "2";
        ////    //txt_job.Text = "2";
        ////}

    }

    protected void getEditTimeStartList(DropDownList ddlName, int _emp_idx, string _tbEmpCode, string _date_start, int _u1idx, int _month_idx_start)
    {

        data_overtime data_u0_timestart_otmonth = new data_overtime();
        ovt_timestart_otmonth_detail u0_timestart_otmonth = new ovt_timestart_otmonth_detail();
        data_u0_timestart_otmonth.ovt_timestart_otmonth_list = new ovt_timestart_otmonth_detail[1];
        u0_timestart_otmonth.condition = 2;
        u0_timestart_otmonth.u0_doc1_idx = _u1idx;
        u0_timestart_otmonth.emp_idx = _emp_idx;
        u0_timestart_otmonth.emp_code = _tbEmpCode.ToString();
        u0_timestart_otmonth.date_start = _date_start;

        data_u0_timestart_otmonth.ovt_timestart_otmonth_list[0] = u0_timestart_otmonth;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timestart_otmonth));
        data_u0_timestart_otmonth = callServicePostOvertime(_urlGetTimeStartScanOTMont, data_u0_timestart_otmonth);
        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timestart_otmonth));

        setDdlData(ddlName, data_u0_timestart_otmonth.ovt_timestart_otmonth_list, "time_start", "month_idx");
        ddlName.SelectedValue = _month_idx_start.ToString();
        ////if (data_u0_timestart_otmonth.ovt_timestart_otmonth_list[0].u0_doc1_idx == _u1idx)
        ////{
        ////    ddlName.SelectedValue = data_u0_timestart_otmonth.ovt_timestart_otmonth_list[0].time_start.ToString();

        ////    //litDebug.Text += data_u0_timestart_otmonth.ovt_timestart_otmonth_list[0].time_start.ToString();
        ////}
        ////else
        ////{
        ////    ddlName.SelectedValue = "";
        ////}




    }

    protected void getTimeEndList(DropDownList ddlName, int _emp_idx, string _tbEmpCode, string _date_start)
    {

        data_overtime data_u0_timeend_otmonth = new data_overtime();
        ovt_timestart_otmonth_detail u0_timeend_otmonth = new ovt_timestart_otmonth_detail();
        data_u0_timeend_otmonth.ovt_timestart_otmonth_list = new ovt_timestart_otmonth_detail[1];

        u0_timeend_otmonth.condition = 1;
        u0_timeend_otmonth.emp_idx = _emp_idx;
        u0_timeend_otmonth.emp_code = _tbEmpCode.ToString();
        u0_timeend_otmonth.date_start = _date_start;

        data_u0_timeend_otmonth.ovt_timestart_otmonth_list[0] = u0_timeend_otmonth;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b)); 
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timeend_otmonth));

        data_u0_timeend_otmonth = callServicePostOvertime(_urlGetTimeStartScanOTMont, data_u0_timeend_otmonth);
        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timeend_otmonth));
        setDdlData(ddlName, data_u0_timeend_otmonth.ovt_timestart_otmonth_list, "time_end", "month_idx");
        //ddlName.Items.Insert(0, new ListItem("--- เลือกเวลาเลิกทำ ---", "0"));
    }

    protected void getEditTimeEndList(DropDownList ddlName, int _emp_idx, string _tbEmpCode, string _date_start, int _u1idx, int _month_idx_end)
    {

        data_overtime data_u0_timeend_otmonth = new data_overtime();
        ovt_timestart_otmonth_detail u0_timeend_otmonth = new ovt_timestart_otmonth_detail();
        data_u0_timeend_otmonth.ovt_timestart_otmonth_list = new ovt_timestart_otmonth_detail[1];

        u0_timeend_otmonth.condition = 3;
        u0_timeend_otmonth.u0_doc1_idx = _u1idx;
        u0_timeend_otmonth.emp_idx = _emp_idx;
        u0_timeend_otmonth.emp_code = _tbEmpCode.ToString();
        u0_timeend_otmonth.date_start = _date_start;

        data_u0_timeend_otmonth.ovt_timestart_otmonth_list[0] = u0_timeend_otmonth;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b)); 
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timeend_otmonth));

        data_u0_timeend_otmonth = callServicePostOvertime(_urlGetTimeStartScanOTMont, data_u0_timeend_otmonth);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timeend_otmonth));
        setDdlData(ddlName, data_u0_timeend_otmonth.ovt_timestart_otmonth_list, "time_end", "month_idx");
        ddlName.SelectedValue = _month_idx_end.ToString();
    }

    /*
    protected void Select_Employee_report_search()
    {
        TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_s");
        TextBox txtempname_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempname_s");
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
        TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlempstatus_s");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        DropDownList ddljobgrade_start = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_start");
        DropDownList ddljobgrade_end = (DropDownList)Fv_Search_Emp_Report.FindControl("ddljobgrade_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlAffiliation_s");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        if (txtempname_s.Text != "")
        {
            _dataEmployee_.emp_name_th = txtempname_s.Text;
        }
        else
        {
            _dataEmployee_.emp_name_th = "";
        }

        if (txtempcode_s.Text != "")
        {
            _dataEmployee_.emp_code = txtempcode_s.Text;
        }
        else
        {
            _dataEmployee_.emp_code = "";
        }

        if (txtdatestart.Text != "")
        {
            _dataEmployee_.startdate_search = txtdatestart.Text;
        }
        else
        {
            _dataEmployee_.startdate_search = "";
        }

        if (txtdateend.Text != "")
        {
            _dataEmployee_.enddate_search = txtdateend.Text;
        }
        else
        {
            _dataEmployee_.enddate_search = "";
        }

        _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
        _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
        _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
        _dataEmployee_.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
        _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
        _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 2; //Index     
        _dataEmployee_.datetype_search = int.Parse(ddldatetype.SelectedValue);
        _dataEmployee_.Search_jobgrade1 = int.Parse(ddljobgrade_start.SelectedValue);
        _dataEmployee_.Search_jobgrade2 = int.Parse(ddljobgrade_end.SelectedValue);
        _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        //_dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        //ViewState["Box_dataEmployee_Report"] = _dataEmployee.employee_list;
        //setGridData(GvEmployee_Report, _dataEmployee.employee_list);
    }

    protected void Select_Employee_report_search_day()
    {
        TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Report_day.FindControl("txtempcode_s");
        TextBox txtempname_s = (TextBox)Fv_Search_Emp_Report_day.FindControl("txtempname_s");
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report_day.FindControl("txtdatestart");
        TextBox txtdateend = (TextBox)Fv_Search_Emp_Report_day.FindControl("txtdateend");

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlpos_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlemptype_s");
        DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlempstatus_s");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddldatetype");
        DropDownList ddljobgrade_start = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddljobgrade_start");
        DropDownList ddljobgrade_end = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddljobgrade_end");
        DropDownList ddlAffiliation_s = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlAffiliation_s");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        if (txtempname_s.Text != "")
        {
            _dataEmployee_.emp_name_th = txtempname_s.Text;
        }
        else
        {
            _dataEmployee_.emp_name_th = "";
        }

        if (txtempcode_s.Text != "")
        {
            _dataEmployee_.emp_code = txtempcode_s.Text;
        }
        else
        {
            _dataEmployee_.emp_code = "";
        }

        if (txtdatestart.Text != "")
        {
            _dataEmployee_.startdate_search = txtdatestart.Text;
        }
        else
        {
            _dataEmployee_.startdate_search = "";
        }

        if (txtdateend.Text != "")
        {
            _dataEmployee_.enddate_search = txtdateend.Text;
        }
        else
        {
            _dataEmployee_.enddate_search = "";
        }

        _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
        _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
        _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
        _dataEmployee_.rpos_idx = int.Parse(ddlpos_rp.SelectedValue);
        _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
        _dataEmployee_.emp_status = int.Parse(ddlempstatus_s.SelectedValue);
        _dataEmployee_.type_select_emp = 2; //Index     
        _dataEmployee_.datetype_search = int.Parse(ddldatetype.SelectedValue);
        _dataEmployee_.Search_jobgrade1 = int.Parse(ddljobgrade_start.SelectedValue);
        _dataEmployee_.Search_jobgrade2 = int.Parse(ddljobgrade_end.SelectedValue);
        _dataEmployee_.ACIDX = int.Parse(ddlAffiliation_s.SelectedValue);

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        //_dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        //ViewState["Box_dataEmployee_Report"] = _dataEmployee.employee_list;
        //setGridData(GvEmployee_Report, _dataEmployee.employee_list);
    }
    */
    #endregion set/get bind data  

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0, 0);

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            case "cmdDetail":

                setActiveTab("docDetail", 0, 0, 0, 0, int.Parse(cmdArg), 0, 0);

                break;
            case "cmdCancel":
                setActiveTab("docDetail", 0, 0, 0, 0, int.Parse(cmdArg), 0, 0);
                break;
            case "cmdBackToDetail":


                getDetailBackIndex();
                ////setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);

                break;
            case "cmdResetSearchDetailOTMonth":
                setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);
                break;
            case "cmdBackToDetailOTDay":
                setActiveTab("docDetail", 0, 0, 0, 0, 1, 0, 0);
                break;
            case "cmdCreate":

                setActiveTab("docCreate", 0, 0, 0, 0, int.Parse(cmdArg), 0, 0);


                break;
            case "cmdWaitApprove":
                setActiveTab("docWaitApprove", 0, 0, 0, 0, int.Parse(cmdArg), 0, 0);
                break;
            case "cmdReport":
                setActiveTab("docReport", 0, 0, 0, 0, int.Parse(cmdArg), 0, 0);
                Fv_Search_Emp_Report.ChangeMode(FormViewMode.Insert);
                Fv_Search_Emp_Report.DataBind();
                break;
            case "cmdAddGroupOTDay":
                linkBtnTrigger(btnImport);
                setActiveTab("docAddGroupOTDay", 0, 0, 0, 0, int.Parse(cmdArg), 0, 0);

                break;
            case "cmdViewDetailGroupOTDay":
                setActiveTab("docAddGroupOTDay", int.Parse(cmdArg), 0, 0, 0, 2, 0, 0);
                break;

            case "CmdDeleteDetailGroupOTDay":

                //getViewDetailGroupOTDay(int.Parse(cmdArg));

                data_overtime data_m0group_del = new data_overtime();
                ovt_m0_group_detail m0group_del = new ovt_m0_group_detail();
                data_m0group_del.ovt_m0_group_list = new ovt_m0_group_detail[1];

                m0group_del.cemp_idx = _emp_idx;
                m0group_del.m0_group_idx = int.Parse(cmdArg);

                data_m0group_del.ovt_m0_group_list[0] = m0group_del;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_employee_otmont_detail));

                data_m0group_del = callServicePostOvertime(_urlSetDelGroupOTDay, data_m0group_del);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_employee_otmont_detail));

                setActiveTab("docAddGroupOTDay", 0, 0, 0, 0, 0, 0, 0);
                break;
            case "cmdBackToDetailGroupOTDay":
                setActiveTab("docAddGroupOTDay", 0, 0, 0, 0, 0, 0, 0);
                break;
            case "cmdBackToDetailReportImportOTDay":

                setActiveTab("docReportOTDay", 0, 0, 0, 0, 0, 0, 0);
                break;

            case "cmdImportExcel":

                data_overtime data_m0_import = new data_overtime();

                if (UploadFileExcel.HasFile)
                {
                    //litDebug.Text = "1";
                    string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                    string FileName = Path.GetFileName(UploadFileExcel.PostedFile.FileName);
                    string extension = Path.GetExtension(UploadFileExcel.PostedFile.FileName);
                    string newFileName = datetimeNow + extension.ToLower();
                    string folderPath = ConfigurationManager.AppSettings["path_otgroup_day_import"];
                    string filePath = Server.MapPath(folderPath + newFileName);
                    if (extension.ToLower() == ".xls" || extension.ToLower() == ".xlsx")
                    {
                        UploadFileExcel.SaveAs(filePath);

                        string conStr = String.Empty;
                        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        conStr = String.Format(conStr, filePath, "Yes");
                        OleDbConnection connExcel = new OleDbConnection(conStr);
                        OleDbCommand cmdExcel = new OleDbCommand();
                        OleDbDataAdapter oda = new OleDbDataAdapter();
                        DataTable dt = new DataTable();

                        cmdExcel.Connection = connExcel;
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                        oda.SelectCommand = cmdExcel;
                        oda.Fill(dt);
                        connExcel.Close();

                        // m1 group detail
                        var count_alert_u1 = dt.Rows.Count;

                        var m1_group_import = new ovt_m1_group_detail[dt.Rows.Count];
                        //m1_group_import[count_] = new ovt_u1_document_detail();
                        for (var i = 0; i <= dt.Rows.Count - 1;)
                        {
                            //if (dt.Rows[i][0].ToString().Trim() != String.Empty || dt.Rows[i][1].ToString().Trim() != String.Empty)
                            //{
                            m1_group_import[i] = new ovt_m1_group_detail();

                            if (dt.Rows[i][0].ToString().Trim() != String.Empty)
                            {
                                m1_group_import[i].emp_code = dt.Rows[i][0].ToString().Trim();
                            }
                            else
                            {
                                m1_group_import[i].emp_code = "-";
                            }

                            if (dt.Rows[i][1].ToString().Trim() != String.Empty)
                            {
                                m1_group_import[i].emp_name = dt.Rows[i][1].ToString().Trim();
                            }
                            else
                            {
                                m1_group_import[i].emp_name = "-";
                            }

                            if (dt.Rows[i][2].ToString().Trim() != String.Empty)
                            {
                                m1_group_import[i].dept_name_th = dt.Rows[i][2].ToString().Trim();
                            }
                            else
                            {
                                m1_group_import[i].dept_name_th = "-";
                            }

                            if (dt.Rows[i][3].ToString().Trim() != String.Empty)
                            {
                                m1_group_import[i].sec_name_th = dt.Rows[i][3].ToString().Trim();
                            }
                            else
                            {
                                m1_group_import[i].sec_name_th = "-";
                            }

                            i++;
                            //}
                        }

                        ViewState["VsDetailExcelOTDay"] = m1_group_import;
                        ////data_m0_import.ovt_m1_group_list = m1_group_import;
                        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_import));

                        if (dt.Rows.Count != 0)
                        {
                            //litDebug.Text = "555";
                            Div_GvDetailExcelImport.Visible = true;
                            ////Save_Excel.Visible = true;
                            setGridData(GvDetailExcelImport, ViewState["VsDetailExcelOTDay"]);

                            GvDetailExcelImport.Focus();
                        }
                        else
                        {

                            _funcTool.showAlert(this, "ไม่มีข้อมูล!!");
                            Div_GvDetailExcelImport.Visible = false;
                            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert(ไม่มีข้อมูล!!);"), true);
                            //GvExcel_Import.Visible = true;
                            //setGridData(GvExcel_Show, null);

                            //_funcTool.showAlert(this, "ไม่มีข้อมูล");

                            txt_addnamegroup.Focus();
                            break;
                        }

                        ////actionImport(filePath, extension, "Yes", FileName);
                        ////File.Delete(filePath);

                        ////if (ddlPlace_excelchk.SelectedValue != "-1")
                        ////{
                        ////    setActiveTab("docDetail", 0, 0, 0, "0");
                        ////}
                        ////else
                        ////{
                        ////    _funcTool.showAlert(this, "กรุณาเลือกสถานที่ตรวจสอบก่อนทำการบันทึก!!!");
                        ////}

                    }
                    else
                    {

                        _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");

                    }


                }
                else
                {
                    _funcTool.showAlert(this, "--- ไม่มีข้อมูล ---");
                }

                break;
            case "cmdImportExcelOTDay":
                linkBtnTrigger(btnImportExcelOTDay);

                data_overtime data_m0_importotday = new data_overtime();

                if (UploadFileExcelOTDay.HasFile)
                {
                    //litDebug.Text = "1";

                    string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                    string FileName = Path.GetFileName(UploadFileExcelOTDay.PostedFile.FileName);
                    string extension = Path.GetExtension(UploadFileExcelOTDay.PostedFile.FileName);
                    string newFileName = datetimeNow + extension.ToLower();
                    string folderPath = ConfigurationManager.AppSettings["path_employee_otday_import"];
                    string filePath = Server.MapPath(folderPath + newFileName);
                    if (extension.ToLower() == ".xls" || extension.ToLower() == ".xlsx")
                    {
                        UploadFileExcelOTDay.SaveAs(filePath);

                        string conStr = String.Empty;
                        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        conStr = String.Format(conStr, filePath, "Yes");
                        OleDbConnection connExcel = new OleDbConnection(conStr);
                        OleDbCommand cmdExcel = new OleDbCommand();
                        OleDbDataAdapter oda = new OleDbDataAdapter();
                        DataTable dt = new DataTable();

                        cmdExcel.Connection = connExcel;
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                        oda.SelectCommand = cmdExcel;
                        oda.Fill(dt);
                        connExcel.Close();

                        // m1 group detail
                        var count_alert_u1 = dt.Rows.Count;

                        //litDebug.Text = count_alert_u1.ToString();

                        var u1_document_day_import = new ovt_u1_document_day_detail[dt.Rows.Count];
                        for (var i = 0; i <= dt.Rows.Count - 1;)
                        {
                            //if (dt.Rows[i][0].ToString().Trim() != String.Empty || dt.Rows[i][1].ToString().Trim() != String.Empty)
                            //{
                            u1_document_day_import[i] = new ovt_u1_document_day_detail();

                            //0
                            if (dt.Rows[i][0].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].emp_code = dt.Rows[i][0].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].emp_code = "-";
                            }

                            //
                            if (dt.Rows[i][1].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].emp_code = dt.Rows[i][1].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].emp_code = "-";
                            }

                            //
                            if (dt.Rows[i][2].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].emp_name_th = dt.Rows[i][2].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].emp_name_th = "-";
                            }

                            //
                            if (dt.Rows[i][3].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].pos_name_th = dt.Rows[i][3].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].pos_name_th = "-";
                            }

                            //
                            if (dt.Rows[i][4].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_26 = dt.Rows[i][4].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_26 = "-";
                            }

                            //
                            if (dt.Rows[i][5].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_27 = dt.Rows[i][5].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_27 = "-";
                            }

                            //
                            if (dt.Rows[i][6].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_28 = dt.Rows[i][6].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_28 = "-";
                            }

                            //
                            if (dt.Rows[i][7].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_29 = dt.Rows[i][7].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_29 = "-";
                            }

                            //
                            if (dt.Rows[i][8].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_30 = dt.Rows[i][8].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_30 = "-";
                            }

                            //
                            if (dt.Rows[i][9].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_31 = dt.Rows[i][9].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_31 = "-";
                            }

                            //
                            if (dt.Rows[i][10].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_1 = dt.Rows[i][10].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_1 = "-";
                            }

                            //
                            if (dt.Rows[i][11].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_2 = dt.Rows[i][11].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_2 = "-";
                            }

                            //
                            if (dt.Rows[i][12].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_3 = dt.Rows[i][12].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_3 = "-";
                            }

                            //
                            if (dt.Rows[i][13].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_4 = dt.Rows[i][13].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_4 = "-";
                            }

                            //
                            if (dt.Rows[i][14].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_5 = dt.Rows[i][14].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_5 = "-";
                            }

                            //
                            if (dt.Rows[i][15].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_6 = dt.Rows[i][15].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_6 = "-";
                            }

                            //
                            if (dt.Rows[i][16].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_7 = dt.Rows[i][16].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_7 = "-";
                            }

                            //
                            if (dt.Rows[i][17].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_8 = dt.Rows[i][17].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_8 = "-";
                            }

                            //
                            if (dt.Rows[i][18].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_9 = dt.Rows[i][18].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_9 = "-";
                            }

                            //
                            if (dt.Rows[i][19].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_10 = dt.Rows[i][19].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_10 = "-";
                            }

                            //
                            if (dt.Rows[i][20].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_11 = dt.Rows[i][20].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_11 = "-";
                            }

                            //
                            if (dt.Rows[i][21].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_12 = dt.Rows[i][21].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_12 = "-";
                            }

                            //
                            if (dt.Rows[i][22].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_13 = dt.Rows[i][22].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_13 = "-";
                            }

                            //
                            if (dt.Rows[i][23].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_14 = dt.Rows[i][23].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_14 = "-";
                            }

                            //
                            if (dt.Rows[i][24].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_15 = dt.Rows[i][24].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_15 = "-";
                            }

                            //
                            if (dt.Rows[i][25].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_16 = dt.Rows[i][25].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_16 = "-";
                            }

                            //
                            if (dt.Rows[i][26].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_17 = dt.Rows[i][26].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_17 = "-";
                            }

                            //
                            if (dt.Rows[i][27].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_18 = dt.Rows[i][27].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_18 = "-";
                            }

                            //
                            if (dt.Rows[i][28].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_19 = dt.Rows[i][28].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_19 = "-";
                            }

                            //
                            if (dt.Rows[i][29].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_20 = dt.Rows[i][29].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_20 = "-";
                            }

                            //
                            if (dt.Rows[i][30].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_21 = dt.Rows[i][30].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_21 = "-";
                            }

                            //
                            if (dt.Rows[i][31].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_22 = dt.Rows[i][31].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_22 = "-";
                            }

                            //
                            if (dt.Rows[i][32].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_23 = dt.Rows[i][32].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_23 = "-";
                            }

                            //
                            if (dt.Rows[i][33].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_24 = dt.Rows[i][33].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_24 = "-";
                            }

                            //
                            if (dt.Rows[i][34].ToString().Trim() != String.Empty)
                            {
                                u1_document_day_import[i].ot_day_25 = dt.Rows[i][34].ToString().Trim();
                            }
                            else
                            {
                                u1_document_day_import[i].ot_day_25 = "-";
                            }

                            i++;
                            //}
                        }

                        ViewState["Vs_DetailFileExcelEmployeeDay"] = u1_document_day_import;
                        //data_m0_importotday.ovt_u1_document_day_list = u1_document_day_import;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_importotday));

                        if (dt.Rows.Count != 0)
                        {
                            //litDebug.Text = "555";
                            Div_GvDetailImportOTDay.Visible = true;
                            ////Save_Excel.Visible = true;
                            GvDetailImportOTDay.Visible = true;
                            setGridData(GvDetailImportOTDay, ViewState["Vs_DetailFileExcelEmployeeDay"]);

                            GvDetailImportOTDay.Focus();
                        }
                        else
                        {
                            ////litDebug.Text = "666";
                            _funcTool.showAlert(this, "ไม่มีข้อมูล!!");
                            Div_GvDetailImportOTDay.Visible = false;
                            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert(ไม่มีข้อมูล!!);"), true);
                            //GvExcel_Import.Visible = true;
                            //setGridData(GvExcel_Show, null);

                            //_funcTool.showAlert(this, "ไม่มีข้อมูล");

                            ////txt_addnamegroup.Focus();
                            break;
                        }


                    }
                    else
                    {

                        _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");

                    }


                }
                else
                {
                    //litDebug.Text = "2";
                    _funcTool.showAlert(this, "--- ไม่มีข้อมูล ---");
                    break;
                }

                break;

            case "cmdSaveImportExcel":

                data_overtime data_m0_importgroup_ot = new data_overtime();

                //insert m0
                ovt_m0_group_detail m0_importgroup_ot_detail = new ovt_m0_group_detail();
                data_m0_importgroup_ot.ovt_m0_group_list = new ovt_m0_group_detail[1];

                m0_importgroup_ot_detail.cemp_idx = _emp_idx;
                m0_importgroup_ot_detail.group_name = txt_addnamegroup.Text;


                //insert m1
                var _m1_importgroup_ot_detail = new ovt_m1_group_detail[GvDetailExcelImport.Rows.Count];
                int sumcheck_m1 = 0;
                int count_m1 = 0;
                //int sum_total_price = 0;

                foreach (GridViewRow gvrow in GvDetailExcelImport.Rows)
                {

                    data_m0_importgroup_ot.ovt_m1_group_list = new ovt_m1_group_detail[1];

                    //CheckBox chk_date_ot = (CheckBox)gv_row.FindControl("chk_date_ot");
                    Label lbl_emp_code_excel = (Label)gvrow.FindControl("lbl_emp_code_excel");
                    Label lbl_emp_name_excel = (Label)gvrow.FindControl("lbl_emp_name_excel");


                    //if (chk_date_ot.Checked == true)
                    //{

                    _m1_importgroup_ot_detail[count_m1] = new ovt_m1_group_detail();
                    _m1_importgroup_ot_detail[count_m1].cemp_idx = _emp_idx;
                    _m1_importgroup_ot_detail[count_m1].emp_code = lbl_emp_code_excel.Text;
                    _m1_importgroup_ot_detail[count_m1].emp_name = lbl_emp_name_excel.Text;


                    sumcheck_m1 = sumcheck_m1 + 1;
                    count_m1++;

                    //}

                }

                data_m0_importgroup_ot.ovt_m0_group_list[0] = m0_importgroup_ot_detail;
                data_m0_importgroup_ot.ovt_m1_group_list = _m1_importgroup_ot_detail;
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_importgroup_ot));

                data_m0_importgroup_ot = callServicePostOvertime(_urlSetGroupOTDay, data_m0_importgroup_ot);
                //litDebug.Text = String.Format("{0:N2}", lit_total_hoursmonth.Text);

                setActiveTab("docAddGroupOTDay", 0, 0, 0, 0, 0, 0, 0);

                break;
            case "cmdSaveImportExcelOTDay":

                data_overtime data_m0_import_otday = new data_overtime();

                //insert m0
                ovt_u0_document_day_detail m0_import_otday_detail = new ovt_u0_document_day_detail();
                data_m0_import_otday.ovt_u0_document_day_list = new ovt_u0_document_day_detail[1];

                m0_import_otday_detail.cemp_idx = _emp_idx;
                m0_import_otday_detail.u0_loc_idx = int.Parse(ddlplace.SelectedValue);
                m0_import_otday_detail.u0_org_idx = int.Parse(ddlorg.SelectedValue);
                m0_import_otday_detail.u0_dept_idx = int.Parse(ddlrdept.SelectedValue);
                m0_import_otday_detail.u0_sec_idx = int.Parse(ddlrsec.SelectedValue);
                m0_import_otday_detail.type_daywork_idx = int.Parse(ddl_type_daywork.SelectedValue);
                m0_import_otday_detail.type_ot_idx = int.Parse(ddl_typeot.SelectedValue);
                m0_import_otday_detail.dateot_from = txt_dateot_dayform.Text;
                m0_import_otday_detail.dateot_to = txt_dateot_dayto.Text;
                m0_import_otday_detail.time_form = txt_time_dayform.Text;
                m0_import_otday_detail.time_to = txt_time_dayto.Text;
                m0_import_otday_detail.m0_actor_idx = 4;
                m0_import_otday_detail.m0_node_idx = 10;
                m0_import_otday_detail.decision = 16;

                //insert m1
                var _m1_import_otday_detail = new ovt_u1_document_day_detail[GvDetailImportOTDay.Rows.Count];
                int sum_otday = 0;
                int count_otday = 0;
                //int sum_total_price = 0;

                foreach (GridViewRow gvrow in GvDetailImportOTDay.Rows)
                {

                    data_m0_import_otday.ovt_u1_document_day_list = new ovt_u1_document_day_detail[1];

                    //CheckBox chk_date_ot = (CheckBox)gv_row.FindControl("chk_date_ot");
                    Label lbl_emp_code_excel_otday = (Label)gvrow.FindControl("lbl_emp_code_excel_otday");
                    Label lbl_ot_day_26_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_26_excel_otday");
                    Label lbl_ot_day_27_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_27_excel_otday");
                    Label lbl_ot_day_28_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_28_excel_otday");
                    Label lbl_ot_day_29_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_29_excel_otday");
                    Label lbl_ot_day_30_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_30_excel_otday");
                    Label lbl_ot_day_31_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_31_excel_otday");
                    Label lbl_ot_day_1_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_1_excel_otday");
                    Label lbl_ot_day_2_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_2_excel_otday");
                    Label lbl_ot_day_3_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_3_excel_otday");
                    Label lbl_ot_day_4_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_4_excel_otday");
                    Label lbl_ot_day_5_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_5_excel_otday");
                    Label lbl_ot_day_6_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_6_excel_otday");
                    Label lbl_ot_day_7_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_7_excel_otday");
                    Label lbl_ot_day_8_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_8_excel_otday");
                    Label lbl_ot_day_9_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_9_excel_otday");
                    Label lbl_ot_day_10_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_10_excel_otday");
                    Label lbl_ot_day_11_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_11_excel_otday");
                    Label lbl_ot_day_12_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_12_excel_otday");
                    Label lbl_ot_day_13_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_13_excel_otday");
                    Label lbl_ot_day_14_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_14_excel_otday");
                    Label lbl_ot_day_15_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_15_excel_otday");
                    Label lbl_ot_day_16_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_16_excel_otday");
                    Label lbl_ot_day_17_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_17_excel_otday");
                    Label lbl_ot_day_18_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_18_excel_otday");
                    Label lbl_ot_day_19_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_19_excel_otday");
                    Label lbl_ot_day_20_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_20_excel_otday");
                    Label lbl_ot_day_21_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_21_excel_otday");
                    Label lbl_ot_day_22_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_22_excel_otday");
                    Label lbl_ot_day_23_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_23_excel_otday");
                    Label lbl_ot_day_24_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_24_excel_otday");
                    Label lbl_ot_day_25_excel_otday = (Label)gvrow.FindControl("lbl_ot_day_25_excel_otday");


                    //if (chk_date_ot.Checked == true)
                    //{

                    _m1_import_otday_detail[count_otday] = new ovt_u1_document_day_detail();
                    _m1_import_otday_detail[count_otday].cemp_idx = _emp_idx;
                    _m1_import_otday_detail[count_otday].emp_code = lbl_emp_code_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_26 = lbl_ot_day_26_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_27 = lbl_ot_day_27_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_28 = lbl_ot_day_28_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_29 = lbl_ot_day_29_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_30 = lbl_ot_day_30_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_31 = lbl_ot_day_31_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_1 = lbl_ot_day_1_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_2 = lbl_ot_day_2_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_3 = lbl_ot_day_3_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_4 = lbl_ot_day_4_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_5 = lbl_ot_day_5_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_6 = lbl_ot_day_6_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_7 = lbl_ot_day_7_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_8 = lbl_ot_day_8_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_9 = lbl_ot_day_9_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_10 = lbl_ot_day_10_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_11 = lbl_ot_day_11_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_12 = lbl_ot_day_12_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_13 = lbl_ot_day_13_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_14 = lbl_ot_day_14_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_15 = lbl_ot_day_15_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_16 = lbl_ot_day_16_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_17 = lbl_ot_day_17_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_18 = lbl_ot_day_18_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_19 = lbl_ot_day_19_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_20 = lbl_ot_day_20_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_21 = lbl_ot_day_21_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_22 = lbl_ot_day_22_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_23 = lbl_ot_day_23_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_24 = lbl_ot_day_24_excel_otday.Text;
                    _m1_import_otday_detail[count_otday].ot_day_25 = lbl_ot_day_25_excel_otday.Text;


                    sumcheck_m1 = sum_otday + 1;
                    count_otday++;

                    //}

                }

                data_m0_import_otday.ovt_u0_document_day_list[0] = m0_import_otday_detail;
                data_m0_import_otday.ovt_u1_document_day_list = _m1_import_otday_detail;
                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_import_otday));
                data_m0_import_otday = callServicePostOvertime(_urlSetImportEmployeeOTDay, data_m0_import_otday);
                if (data_m0_import_otday.return_code == 0)
                {
                    setActiveTab("docDetailImportOTDay", 0, 0, 0, 0, 0, 0, 0);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- มีข้อมูลอยู่ในระบบแล้ว ---');", true);
                    break;
                }
                break;


            case "cmdSearchMonth":


                if (ViewState["time_idx_permission"].ToString() == "1" || ViewState["emp_type_permission"].ToString() == "2")
                {
                    TextBox tbEmpCode = (TextBox)fvEmpDetail.FindControl("tbEmpCode");

                    IFormatProvider culture_search = new CultureInfo("en-US", true);
                    //string Month_current = DateTime.Now.ToString("yyyy MM", culture_search); //Date Time Today Incheck

                    //string Month_current = DateTime.Now.ToString("MM", culture_search);
                    string prevMonth = DateTime.Now.AddMonths(-1).ToString("MM");
                    string varDay = DateTime.Now.AddDays(-1).ToString("DD");


                    ////IFormatProvider culture = new CultureInfo("en-US", true);
                    DateTime DateToday_Set = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", culture_search);

                    ////litDebug.Text = DateToday_Set.ToString();
                    //IFormatProvider culture = new CultureInfo("en-US", true);
                    DateTime DateToday = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    //path_file = "~/masterpage/images/holiday-header/" + "room-booking" + ".png";

                    //litDebug.Text = DateToday.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);//DateToday.ToString();


                    string input = DateToday.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    string[] result = input.Split(new string[] { "/" }, StringSplitOptions.None);


                    string set_day = "01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31";
                    string set_day_fileout = "01,02,03,04,05,06,07,08,09,10,11,12";
                    //string set_day_fileout = "01,02,03,04";
                    //string[] _value_chk;
                    string[] setTabReportHR = set_day.Split(',');
                    string[] setTab_FileOut = set_day_fileout.Split(',');
                    //_value_chk = set_day.ToArray();
                    if (Array.IndexOf(setTabReportHR, result[0].ToString()) > -1)
                    {
                        //litDebug.Text = "ok";

                        data_overtime data_search_month = new data_overtime();
                        ovt_u0_month_detail search_month_detail = new ovt_u0_month_detail();
                        data_search_month.ovt_u0_month_list = new ovt_u0_month_detail[1];

                        search_month_detail.month_idx = int.Parse(ddl_month_search.SelectedValue);
                        search_month_detail.emp_idx = _emp_idx;
                        search_month_detail.emp_code = tbEmpCode.Text;

                        data_search_month.ovt_u0_month_list[0] = search_month_detail;

                        //litDebug.Text = ddl_month_search.SelectedValue.ToString();
                        //check month current in ot month
                        if (prevMonth != ddl_month_search.SelectedValue)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่สามารถทำรายการขอ OT ได้ ---');", true);
                            _PanelOTMonth.Visible = false;
                            div_Addfileotmonth.Visible = false;
                            break;
                        }
                        else
                        {
                            linkBtnTrigger(btnSaveOTMonth);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_month));
                            data_search_month = callServicePostOvertime(_urlGetDateScanOTMont, data_search_month);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_month));
                            if (data_search_month.return_code == 0)
                            {

                                //datetime start ddl
                                data_overtime data_u0_timestart_otmonth = new data_overtime();
                                ovt_timestart_otmonth_detail u0_timestart_otmonth = new ovt_timestart_otmonth_detail();
                                data_u0_timestart_otmonth.ovt_timestart_otmonth_list = new ovt_timestart_otmonth_detail[1];

                                u0_timestart_otmonth.month_idx = int.Parse(ddl_month_search.SelectedValue);
                                u0_timestart_otmonth.emp_idx = _emp_idx;
                                u0_timestart_otmonth.emp_code = tbEmpCode.Text;
                                ////u0_timestart_otmonth.date_start = _date_start;

                                data_u0_timestart_otmonth.ovt_timestart_otmonth_list[0] = u0_timestart_otmonth;
                                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));  ddl_timestart
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timestart_otmonth));
                                data_u0_timestart_otmonth = callServicePostOvertime(_urlGetTimeStartScanOTMont, data_u0_timestart_otmonth);
                                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timestart_otmonth));

                                if (data_u0_timestart_otmonth.return_code == 0)
                                {
                                    ViewState["vs_TimeStart"] = data_u0_timestart_otmonth.ovt_timestart_otmonth_list;
                                }


                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timestart_otmonth));
                                ////setDdlData(ddlName, data_u0_timestart_otmonth.ovt_timestart_otmonth_list, "time_start", "month_idx");

                                //datetime start ddl

                                //datetime end ddl
                                data_overtime data_u0_timeend_otmonth = new data_overtime();
                                ovt_timestart_otmonth_detail u0_timeend_otmonth = new ovt_timestart_otmonth_detail();
                                data_u0_timeend_otmonth.ovt_timestart_otmonth_list = new ovt_timestart_otmonth_detail[1];

                                u0_timeend_otmonth.condition = 1;
                                u0_timeend_otmonth.month_idx = int.Parse(ddl_month_search.SelectedValue);
                                u0_timeend_otmonth.emp_idx = _emp_idx;
                                u0_timeend_otmonth.emp_code = tbEmpCode.Text;
                                //u0_timeend_otmonth.date_start = _date_start;

                                data_u0_timeend_otmonth.ovt_timestart_otmonth_list[0] = u0_timeend_otmonth;
                                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b)); 
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timeend_otmonth));

                                data_u0_timeend_otmonth = callServicePostOvertime(_urlGetTimeStartScanOTMont, data_u0_timeend_otmonth);
                                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timeend_otmonth));

                                if (data_u0_timeend_otmonth.return_code == 0)
                                {
                                    ViewState["vs_TimeEnd"] = data_u0_timeend_otmonth.ovt_timestart_otmonth_list;
                                }

                                ////setDdlData(ddlName, data_u0_timeend_otmonth.ovt_timestart_otmonth_list, "time_end", "month_idx");


                                //datetime end ddl

                                //litDebug.Text = "2";
                                //int count = data_search_month.ovt_u0_month_list.;
                                //litDebug.Text = data_search_month.ovt_u0_month_list.Length + " คน";
                                if (Array.IndexOf(setTab_FileOut, result[0].ToString()) > -1)
                                {
                                    _PanelOTMonth.Visible = true;
                                    div_Addfileotmonth.Visible = false;
                                    ViewState["Vs_DateOTOfMonth"] = data_search_month.ovt_u0_month_list;
                                    setGridData(GvCreateOTMonth, ViewState["Vs_DateOTOfMonth"]);
                                }
                                else
                                {

                                    _PanelOTMonth.Visible = true;
                                    div_Addfileotmonth.Visible = true;
                                    ViewState["Vs_DateOTOfMonth"] = data_search_month.ovt_u0_month_list;
                                    setGridData(GvCreateOTMonth, ViewState["Vs_DateOTOfMonth"]);
                                }
                            }
                            else
                            {
                                //litDebug.Text = "1";
                                _PanelOTMonth.Visible = false;
                                div_Addfileotmonth.Visible = false;
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่สามารถทำรายการขอ OT ได้ ---');", true);

                                break;

                            }
                        }

                    }
                    else
                    {

                        //litDebug.Text = "no ok";
                        ////ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่สามารถทำรายการขอ OT ได้ ---');", true);
                        ////break;

                    }

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่สามารถสร้างรายการ OT ได้ กรุณาตรวจสอบกะการทำงาน ---');", true);
                    break;
                }


                break;
            case "cmdSearchGroupOT":

                getViewDetailGroupOTDay(GvGroupCreateOTDay, int.Parse(ddlgroup_otday.SelectedValue.ToString()));
                UpdatePanel_CreateOTDay.Visible = true;
                getbtnDecisionApprove(rptBindbtnSaveOTDay, 7, 0);
                //getTypeOTDay(ddl_type_otday, 0);
                break;

            case "cmdSaveOTMonth":


                TextBox tbEmpCode_ = (TextBox)fvEmpDetail.FindControl("tbEmpCode");
                DropDownList ddl_month_search_insert = (DropDownList)_PanelCreateMonth.FindControl("ddl_month_search");

                //Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");

                FileUpload UploadFileMemo = (FileUpload)_PanelOTMonth.FindControl("UploadFileMemo");

                //lit_total_hoursmonth.Text = String.Format("{0:N2}", tot_actual_otmonth);

                data_overtime data_insert_otmonth = new data_overtime();

                //insert u0 document ot month
                ovt_u0_document_detail u0_insert_otmonth = new ovt_u0_document_detail();
                data_insert_otmonth.ovt_u0_document_list = new ovt_u0_document_detail[1];

                u0_insert_otmonth.emp_type = 2; //ot month
                u0_insert_otmonth.cemp_idx = _emp_idx;
                u0_insert_otmonth.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                u0_insert_otmonth.m0_actor_idx = 1;
                u0_insert_otmonth.m0_node_idx = 1;
                u0_insert_otmonth.decision = 0;
                u0_insert_otmonth.month_ot = int.Parse(ddl_month_search_insert.SelectedValue);
                //u0_insert_otmonth.sumhours = lit_total_hoursmonth.Text;

                data_insert_otmonth.ovt_u0_document_list[0] = u0_insert_otmonth;
                //insert u0 document ot month

                //insert u1 document ot month
                var _u1_doc_otmonth = new ovt_u1_document_detail[GvCreateOTMonth.Rows.Count];
                int sumcheck = 0;
                int count_ = 0;
                //int sum_total_price = 0;

                foreach (GridViewRow gv_row in GvCreateOTMonth.Rows)
                {

                    data_insert_otmonth.ovt_u1_document_list = new ovt_u1_document_detail[1];

                    CheckBox chk_date_ot = (CheckBox)gv_row.FindControl("chk_date_ot");
                    Label lbl_date_start = (Label)gv_row.FindControl("lbl_date_start");
                    TextBox txt_job = (TextBox)gv_row.FindControl("txt_job");
                    DropDownList ddl_timestart = (DropDownList)gv_row.FindControl("ddl_timestart");
                    DropDownList ddl_timeend = (DropDownList)gv_row.FindControl("ddl_timeend");

                    TextBox txt_sum_hour = (TextBox)gv_row.FindControl("txt_sum_hour");
                    TextBox txt_head_set = (TextBox)gv_row.FindControl("txt_head_set");
                    TextBox txt_remark = (TextBox)gv_row.FindControl("txt_remark");

                    ////Label txtot_before = (Label)gv_row.FindControl("txtot_before");
                    ////Label txtot_after = (Label)gv_row.FindControl("txtot_after");
                    ////Label txtot_holiday = (Label)gv_row.FindControl("txtot_holiday");

                    Label lblot_before = (Label)gv_row.FindControl("lblot_before");
                    Label lblot_after = (Label)gv_row.FindControl("lblot_after");
                    Label lblot_holiday = (Label)gv_row.FindControl("lblot_holiday");

                    DropDownList ddl_timebefore = (DropDownList)gv_row.FindControl("ddl_timebefore");
                    DropDownList ddl_timeafter = (DropDownList)gv_row.FindControl("ddl_timeafter");
                    DropDownList ddl_timeholiday = (DropDownList)gv_row.FindControl("ddl_timeholiday");


                    //Label lb_sum_counttotal = (Label)gv_row.FindControl("lb_sum_counttotal");

                    if (chk_date_ot.Checked == true)
                    {

                        _u1_doc_otmonth[count_] = new ovt_u1_document_detail();
                        _u1_doc_otmonth[count_].emp_code = tbEmpCode_.Text;
                        //_u1_doc_otmonth[count_].dateot_from = lbl_date_start.Text;
                        _u1_doc_otmonth[count_].dateot_from = ddl_timestart.SelectedItem.Text;
                        _u1_doc_otmonth[count_].dateot_to = ddl_timeend.SelectedItem.Text;

                        if (txt_job.Text == "")
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('--- กรุณากรอกข้อมูลงานที่ปฏิบัติ ---');"), true);
                            break;
                        }
                        else
                        {
                            _u1_doc_otmonth[count_].detail_job = txt_job.Text;
                        }


                        _u1_doc_otmonth[count_].time_form = ddl_timestart.SelectedItem.Text;
                        _u1_doc_otmonth[count_].time_to = ddl_timeend.SelectedItem.Text;
                        _u1_doc_otmonth[count_].sum_hours = txt_sum_hour.Text;

                        if (ddl_timebefore.SelectedValue != "")
                        {
                            _u1_doc_otmonth[count_].ot_before = ddl_timebefore.SelectedValue.ToString();
                        }
                        else
                        {
                            _u1_doc_otmonth[count_].ot_before = "0.00";
                        }
                        _u1_doc_otmonth[count_].hours_ot_before = lblot_before.Text;


                        if (ddl_timeafter.SelectedValue != "")
                        {
                            _u1_doc_otmonth[count_].ot_after = ddl_timeafter.SelectedValue.ToString();
                        }
                        else
                        {
                            _u1_doc_otmonth[count_].ot_after = "0.00";
                        }
                        _u1_doc_otmonth[count_].hours_ot_after = lblot_after.Text;


                        if (ddl_timeholiday.SelectedValue != "")
                        {
                            _u1_doc_otmonth[count_].ot_holiday = ddl_timeholiday.SelectedValue.ToString();
                        }
                        else
                        {
                            _u1_doc_otmonth[count_].ot_holiday = "0.00";
                        }
                        _u1_doc_otmonth[count_].hours_ot_holiday = lblot_holiday.Text;

                        _u1_doc_otmonth[count_].month_idx_start = int.Parse(ddl_timestart.SelectedValue);
                        _u1_doc_otmonth[count_].month_idx_end = int.Parse(ddl_timeend.SelectedValue);


                        if (txt_head_set.Text == "")
                        {
                            _u1_doc_otmonth[count_].settime = "0"; // เดี๋ยวมาแก้นะ
                        }
                        else
                        {
                            _u1_doc_otmonth[count_].settime = txt_head_set.Text; // เดี๋ยวมาแก้นะ
                        }

                        if (txt_remark.Text == "")
                        {
                            _u1_doc_otmonth[count_].comment = "-"; // เดี๋ยวมาแก้นะ
                        }
                        else
                        {
                            _u1_doc_otmonth[count_].comment = txt_remark.Text; // เดี๋ยวมาแก้นะ
                        }

                        sumcheck = sumcheck + 1;
                        count_++;

                    }

                }
                //insert u1 document ot month

                if (sumcheck == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('--- กรุณากรอกข้อมูล ก่อนทำการบันทึก ---');"), true);
                    break;
                }
                else
                {
                    data_insert_otmonth.ovt_u0_document_list[0] = u0_insert_otmonth;
                    data_insert_otmonth.ovt_u1_document_list = _u1_doc_otmonth;

                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_insert_otmonth));


                    if (div_Addfileotmonth.Visible == true)
                    {


                        if (UploadFileMemo.HasFile)
                        {



                            data_insert_otmonth = callServicePostOvertime(_urlSetCreateOTMonth, data_insert_otmonth);
                            //litDebug.Text = "22";
                            string filepath = Server.MapPath(_path_file_otmonth);

                            //litDebug.Text = filepath.ToString();
                            HttpFileCollection uploadedFiles = Request.Files;

                            for (int i = 0; i < uploadedFiles.Count; i++)
                            {
                                HttpPostedFile userPostedFileMemo = uploadedFiles[i];

                                try
                                {
                                    if (userPostedFileMemo.ContentLength > 0)
                                    {
                                        ////string filePath12 = Constants.KEY_GET_FROM_PERM_PERM + "-" + dataADOnlineU0.return_code.ToString();
                                        string filePath2 = data_insert_otmonth.ovt_u0_document_list[0].u0_doc_idx.ToString();
                                        string filePath1 = Server.MapPath(_path_file_otmonth + filePath2);

                                        string _filepathExtension = Path.GetExtension(userPostedFileMemo.FileName);

                                        if (!Directory.Exists(filePath1))
                                        {
                                            Directory.CreateDirectory(filePath1);
                                        }


                                        //litDebug.Text += "<u>File #" + (i + 1) + "</u><br>";
                                        //litDebug.Text += "File Content Type: " + userPostedFile.ContentType + "<br>"; data_insert_otmonth.ovt_u0_document_list[0].u0_doc_idx.ToString()
                                        //litDebug.Text += "File Size: " + userPostedFile.ContentLength + "kb<br>";
                                        //litDebug.Text += "File Name: " + userPostedFile.FileName + "<br>";

                                        //litDebug.Text += (filepath + filePath12 + "\\" + "1209" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        userPostedFileMemo.SaveAs(filepath + filePath2 + "\\" + data_insert_otmonth.ovt_u0_document_list[0].u0_doc_idx.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        //litDebug.Text += "Location where saved: " + filepath + "\\" + Path.GetFileName(userPostedFile.FileName) + "<p>";
                                    }
                                }
                                catch (Exception Ex)
                                {
                                    //litDebug.Text += "Error: <br>" + Ex.Message;
                                }
                            }

                            setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);


                        }
                        else
                        {




                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('-- กรุณาแนบไฟล์ ก่อนทำการบันทึก --');", true);
                            break;
                        }

                    }
                    else
                    {
                        //litDebug.Text = "23";
                        data_insert_otmonth = callServicePostOvertime(_urlSetCreateOTMonth, data_insert_otmonth);
                        setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);

                    }

                }
                ////setActiveTab("docDetail", 0, 0, 0, 0, 2, 0);

                break;


            case "cmdAdminCreateOTDay":

                data_overtime data_insert_otday_admin = new data_overtime();

                //insert u0 document ot day by admin
                ovt_u0doc_otday_detail u0_insert_otday_admin = new ovt_u0doc_otday_detail();
                data_insert_otday_admin.ovt_u0doc_otday_list = new ovt_u0doc_otday_detail[1];

                u0_insert_otday_admin.cemp_idx = _emp_idx;
                u0_insert_otday_admin.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                u0_insert_otday_admin.m0_actor_idx = 4;
                u0_insert_otday_admin.m0_node_idx = 11;
                u0_insert_otday_admin.decision = 17;

                data_insert_otday_admin.ovt_u0doc_otday_list[0] = u0_insert_otday_admin;
                //insert u0 document ot day by admin

                //insert u1 document ot month
                var _u1_docday_idx = new ovt_u1doc_otday_detail[GvDetailCreateOTDay.Rows.Count];
                int sum_check = 0;
                int count_check = 0;
                //int sum_total_price = 0;

                foreach (GridViewRow gv_row in GvDetailCreateOTDay.Rows)
                {

                    data_insert_otday_admin.ovt_u1doc_otday_list = new ovt_u1doc_otday_detail[1];

                    CheckBox chk_otday = (CheckBox)gv_row.FindControl("chk_otday");
                    Label lbl_shift_date = (Label)gv_row.FindControl("lbl_shift_date");
                    Label lbl_STIDX_otday = (Label)gv_row.FindControl("lbl_STIDX_otday");
                    Label lbl_emp_idx_otday = (Label)gv_row.FindControl("lbl_emp_idx_otday");

                    Label lbl_shift_timestart = (Label)gv_row.FindControl("lbl_shift_timestart");
                    Label lbl_shift_timestart_uidx = (Label)gv_row.FindControl("lbl_shift_timestart_uidx");

                    Label lbl_shift_timeend = (Label)gv_row.FindControl("lbl_shift_timeend");
                    Label lbl_shift_timeend_uidx = (Label)gv_row.FindControl("lbl_shift_timeend_uidx");

                    Label lbl_shift_timestart_otscan = (Label)gv_row.FindControl("lbl_shift_timestart_otscan");
                    Label lbl_shift_timestart_uidx_otscan = (Label)gv_row.FindControl("lbl_shift_timestart_uidx_otscan");

                    Label lbl_shift_timeend_otscan = (Label)gv_row.FindControl("lbl_shift_timeend_otscan");
                    Label lbl_shift_timeend_uidx_otscan = (Label)gv_row.FindControl("lbl_shift_timeend_uidx_otscan");

                    Label lbl_shift_otbefore = (Label)gv_row.FindControl("lbl_shift_otbefore");
                    Label lbl_shift_otafter = (Label)gv_row.FindControl("lbl_shift_otafter");
                    Label lbl_shift_otholiday = (Label)gv_row.FindControl("lbl_shift_otholiday");

                    Label lbl_hours_otbefore = (Label)gv_row.FindControl("lbl_hours_otbefore");
                    Label lbl_hours_otafter = (Label)gv_row.FindControl("lbl_hours_otafter");
                    Label lbl_hours_otholiday = (Label)gv_row.FindControl("lbl_hours_otholiday");
                    TextBox txt_remarkadmin_shift_otholiday = (TextBox)gv_row.FindControl("txt_remark_shift_otholiday");

                    DropDownList ddl_shifttimestart = (DropDownList)gv_row.FindControl("ddl_shifttimestart");
                    DropDownList ddl_shifttimeend = (DropDownList)gv_row.FindControl("ddl_shifttimeend");
                    DropDownList ddl_shifttimestart_ot = (DropDownList)gv_row.FindControl("ddl_shifttimestart_ot");
                    DropDownList ddl_shifttimeend_ot = (DropDownList)gv_row.FindControl("ddl_shifttimeend_ot");

                    if (chk_otday.Checked == true)
                    {

                        _u1_docday_idx[count_check] = new ovt_u1doc_otday_detail();
                        _u1_docday_idx[count_check].cemp_idx = _emp_idx;
                        //_u1_docday_idx[count_check].rsec_idx = _emp_idx;
                        //_u1_docday_idx[count_check].m0_actor_idx = 4;
                        //_u1_docday_idx[count_check].m0_node_idx = 11;
                        //_u1_docday_idx[count_check].decision = 17;
                        _u1_docday_idx[count_check].emp_idx = int.Parse(lbl_emp_idx_otday.Text);
                        _u1_docday_idx[count_check].STIDX = int.Parse(lbl_STIDX_otday.Text);

                        //if (lbl_shift_timestart.Text != "")
                        if (ddl_shifttimestart.SelectedValue != "")
                        {
                            //_u1_docday_idx[count_check].datetime_start = lbl_shift_timestart.Text;
                            //_u1_docday_idx[count_check].datetime_start_uidx = int.Parse(lbl_shift_timestart_uidx.Text);
                            _u1_docday_idx[count_check].datetime_start = ddl_shifttimestart.SelectedItem.Text;
                            _u1_docday_idx[count_check].datetime_start_uidx = int.Parse(ddl_shifttimestart.SelectedValue);


                        }

                        //if (lbl_shift_timeend.Text != "")
                        if (ddl_shifttimeend.SelectedValue != "")
                        {
                            //_u1_docday_idx[count_check].datetime_end = lbl_shift_timeend.Text;
                            //_u1_docday_idx[count_check].datetime_end_uidx = int.Parse(lbl_shift_timeend_uidx.Text);
                            _u1_docday_idx[count_check].datetime_end = ddl_shifttimeend.SelectedItem.Text;
                            _u1_docday_idx[count_check].datetime_end_uidx = int.Parse(ddl_shifttimeend.SelectedValue);
                        }

                        //if (lbl_shift_timestart_otscan.Text != "")
                        if (ddl_shifttimestart_ot.SelectedValue != "")
                        {
                            //_u1_docday_idx[count_check].datetime_scanot_in = lbl_shift_timestart_otscan.Text;
                            //_u1_docday_idx[count_check].datetime_scanot_in_idx = int.Parse(lbl_shift_timestart_uidx_otscan.Text);
                            _u1_docday_idx[count_check].datetime_scanot_in = ddl_shifttimestart_ot.SelectedItem.Text;
                            _u1_docday_idx[count_check].datetime_scanot_in_idx = int.Parse(ddl_shifttimestart_ot.SelectedValue);
                        }

                        //if (lbl_shift_timeend_otscan.Text != "")
                        if (ddl_shifttimeend_ot.SelectedValue != "")
                        {
                            _u1_docday_idx[count_check].datetime_scanot_out = ddl_shifttimeend_ot.SelectedItem.Text;
                            _u1_docday_idx[count_check].datetime_scanot_out_idx = int.Parse(ddl_shifttimeend_ot.SelectedValue);
                        }

                        _u1_docday_idx[count_check].ot_before = lbl_shift_otbefore.Text;
                        _u1_docday_idx[count_check].hours_otbefore = lbl_hours_otbefore.Text;
                        _u1_docday_idx[count_check].ot_after = lbl_shift_otafter.Text;
                        _u1_docday_idx[count_check].hours_otafter = lbl_hours_otafter.Text;
                        _u1_docday_idx[count_check].ot_holiday = lbl_shift_otholiday.Text;
                        _u1_docday_idx[count_check].hours_otholiday = lbl_hours_otholiday.Text;

                        if (txt_remarkadmin_shift_otholiday.Text != "")
                        {
                            _u1_docday_idx[count_check].comment_admin = txt_remarkadmin_shift_otholiday.Text;
                        }
                        else
                        {
                            _u1_docday_idx[count_check].comment_admin = "-";
                        }

                        sum_check = sum_check + 1;
                        count_check++;

                    }

                }
                //insert u1 document ot month

                if (sum_check == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('--- กรุณาเลือกข้อมูล ก่อนทำการบันทึก ---');"), true);
                    break;
                }
                else
                {
                    data_insert_otday_admin.ovt_u0doc_otday_list[0] = u0_insert_otday_admin;
                    data_insert_otday_admin.ovt_u1doc_otday_list = _u1_docday_idx;
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_insert_otday_admin));
                    data_insert_otday_admin = callServicePostOvertime(_urlSetCreateOTByAdmin, data_insert_otday_admin);

                    //setActiveTab("docDetailAdminOTDay", 0, 0, 0, 0, 0, 0, 0);

                    getCountWaitDetailApproveOTAdminCreate();
                    setActiveTab("docDetailAdminOTDay", 0, 0, 0, 0, 1, 0, 0);

                }

                //litDebug.Text = sum_check.ToString();

                break;
            case "cmdBackToDetailAdmin":

                setActiveTab("docDetailAdminOTDay", 0, 0, 0, 0, 1, 0, 0);

                break;
            case "cmdBackToDetailEmployee":

                setActiveTab("docDetailAdminOTDay", 0, 0, 0, 0, 2, 0, 0);

                break;


            case "cmdSaveOTDay"://create ot day

                string[] arg_saveotday = new string[1];
                arg_saveotday = e.CommandArgument.ToString().Split(';');
                int _decision_idx_saveotday = int.Parse(arg_saveotday[0]);
                string _decision_name_saveotday = arg_saveotday[1];

                data_overtime data_insert_otday = new data_overtime();

                //insert u0 document ot day
                ovt_u0_document_detail u0_insert_otday = new ovt_u0_document_detail();
                data_insert_otday.ovt_u0_document_list = new ovt_u0_document_detail[1];

                u0_insert_otday.emp_type = 1; //ot day
                u0_insert_otday.cemp_idx = _emp_idx;
                u0_insert_otday.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                u0_insert_otday.m0_actor_idx = 3;
                u0_insert_otday.m0_node_idx = 7;
                u0_insert_otday.decision = _decision_idx_saveotday;
                u0_insert_otday.m0_group_idx = int.Parse(ddlgroup_otday.SelectedValue);


                data_insert_otday.ovt_u0_document_list[0] = u0_insert_otday;
                //insert u0 document ot day


                //insert u1 document ot day
                var _u1_doc_otday = new ovt_u1_document_detail[GvGroupCreateOTDay.Rows.Count];
                int sum_otday1 = 0;
                int count_otday1 = 0;
                //int sum_total_price = 0;

                IFormatProvider culture_otday = new CultureInfo("en-US", true);
                //DateTime DateToday_Set = DateTime.ParseExact(DateTime.Now.ToString("HH:mm"), "HH:mm", culture); //Date Time Today Incheck
                DateTime time_form_otday = DateTime.ParseExact(txt_timestart_otday.Text, "HH:mm", culture_otday);
                DateTime time_to_otday = DateTime.ParseExact(txt_timeend_otday.Text, "HH:mm", culture_otday);


                var result_time_otday = time_to_otday.Subtract(time_form_otday).TotalMinutes; // result time chrck > 30 min because cancel in room
                var hours_otday = (time_to_otday - time_form_otday).TotalHours;
                //litDebug.Text = hours.ToString();

                tot_actual_otday = Convert.ToDecimal(hours_otday.ToString());

                foreach (GridViewRow gv_row in GvGroupCreateOTDay.Rows)
                {

                    data_insert_otday.ovt_u1_document_list = new ovt_u1_document_detail[1];

                    CheckBox chk_otday = (CheckBox)gv_row.FindControl("chk_otday");
                    Label lbl_emp_code_createotday = (Label)gv_row.FindControl("lbl_emp_code_createotday");
                    //DropDownList ddl_timestart = (DropDownList)gv_row.FindControl("ddl_timestart");
                    //DropDownList ddl_timeend = (DropDownList)gv_row.FindControl("ddl_timeend");
                    //TextBox txt_timestart_job = (TextBox)gv_row.FindControl("txt_timestart_job");
                    //TextBox txt_timeend_job = (TextBox)gv_row.FindControl("txt_timeend_job");
                    //TextBox txt_sum_hour = (TextBox)gv_row.FindControl("txt_sum_hour");
                    //TextBox txt_head_set = (TextBox)gv_row.FindControl("txt_head_set");
                    //TextBox txt_remark = (TextBox)gv_row.FindControl("txt_remark");

                    //Label lb_sum_counttotal = (Label)gv_row.FindControl("lb_sum_counttotal");

                    if (chk_otday.Checked == true)
                    {

                        _u1_doc_otday[sum_otday1] = new ovt_u1_document_detail();
                        _u1_doc_otday[sum_otday1].emp_code = lbl_emp_code_createotday.Text;
                        _u1_doc_otday[sum_otday1].dateot_from = txt_datestart_otday.Text;
                        _u1_doc_otday[sum_otday1].dateot_to = txt_dateend_otday.Text;
                        _u1_doc_otday[sum_otday1].time_form = txt_timestart_otday.Text;
                        _u1_doc_otday[sum_otday1].time_to = txt_timeend_otday.Text;
                        _u1_doc_otday[sum_otday1].sum_hours = tot_actual_otday.ToString();
                        _u1_doc_otday[sum_otday1].settime = "0";
                        _u1_doc_otday[sum_otday1].comment = "-";

                        sum_otday1 = sum_otday1 + 1;
                        count_otday1++;

                    }



                }
                //insert u1 document ot month

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_insert_otday));
                data_insert_otday.ovt_u0_document_list[0] = u0_insert_otday;
                data_insert_otday.ovt_u1_document_list = _u1_doc_otday;
                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_insert_otday));

                data_insert_otday = callServicePostOvertime(_urlSetCreateGroupOTDay, data_insert_otday);
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('จำนวนรายชื่อที่กำหนด OT = {0} คน');", count_headset.ToString()), true);
                setActiveTab("docDetail", 0, 0, 0, 0, 1, 0, 0);

                break;

            case "cmdHeadSaveSetOTMonth":

                TextBox txt_set_datestart = (TextBox)_PanelSetOTMonth.FindControl("txt_set_datestart");
                TextBox txt_set_dateend = (TextBox)_PanelSetOTMonth.FindControl("txt_set_dateend");
                TextBox txt_timestart_form = (TextBox)_PanelSetOTMonth.FindControl("txt_timestart_form");
                TextBox txt_timeend_to = (TextBox)_PanelSetOTMonth.FindControl("txt_timeend_to");


                IFormatProvider culture = new CultureInfo("en-US", true);
                //DateTime DateToday_Set = DateTime.ParseExact(DateTime.Now.ToString("HH:mm"), "HH:mm", culture); //Date Time Today Incheck
                DateTime time_form = DateTime.ParseExact(txt_timestart_form.Text, "HH:mm", culture);

                DateTime time_to = DateTime.ParseExact(txt_timeend_to.Text, "HH:mm", culture);


                var result_time = time_to.Subtract(time_form).TotalMinutes; // result time chrck > 30 min because cancel in room
                var hours = (time_to - time_form).TotalHours;
                //litDebug.Text = hours.ToString();

                tot_actual_otmonth = Convert.ToDecimal(hours.ToString());
                // double n_hours = Convert.ToDouble(value);

                //litDebug.Text = String.Format("{0:N2}", tot_actual_otmonth); // Convert.ToString(tot_actual);

                //litDebug.Text = n_hours.ToString();

                //head set ot month
                data_overtime data_set_otmonth = new data_overtime();

                //insert u0 document ot month
                ovt_u0_document_detail u0_headset_otmonth = new ovt_u0_document_detail();
                data_set_otmonth.ovt_u0_document_list = new ovt_u0_document_detail[1];

                u0_headset_otmonth.emp_type = 2; //ot month
                u0_headset_otmonth.cemp_idx = _emp_idx;
                u0_headset_otmonth.m0_actor_idx = 2;
                u0_headset_otmonth.m0_node_idx = 8;
                u0_headset_otmonth.decision = 15;
                //u0_headset_otmonth.month_ot = int.Parse(ddl_month_search_insert.SelectedValue);
                //u0_headset_otmonth.sumhours = lit_total_hoursmonth.Text;

                data_set_otmonth.ovt_u0_document_list[0] = u0_headset_otmonth;
                //insert u0 document ot month

                //insert u1 document ot month
                var _u1_doc_setotmonth = new ovt_u1_document_detail[GvEmployeeSetOTMonth.Rows.Count];

                int count_headset = 0;
                //int sum_total_price = 0;

                foreach (GridViewRow gv_row in GvEmployeeSetOTMonth.Rows)
                {

                    data_set_otmonth.ovt_u1_document_list = new ovt_u1_document_detail[1];

                    CheckBox check_employee = (CheckBox)gv_row.FindControl("check_employee");
                    Label lbl_empcode_setotmonth = (Label)gv_row.FindControl("lbl_empcode_setotmonth");


                    //Label lb_sum_counttotal = (Label)gv_row.FindControl("lb_sum_counttotal");

                    if (check_employee.Checked == true)
                    {

                        _u1_doc_setotmonth[count_headset] = new ovt_u1_document_detail();
                        _u1_doc_setotmonth[count_headset].emp_code = lbl_empcode_setotmonth.Text;
                        _u1_doc_setotmonth[count_headset].dateot_from = txt_set_datestart.Text;
                        _u1_doc_setotmonth[count_headset].dateot_to = txt_set_dateend.Text;
                        _u1_doc_setotmonth[count_headset].detail_job = "-";
                        _u1_doc_setotmonth[count_headset].time_form = txt_timestart_form.Text;
                        _u1_doc_setotmonth[count_headset].time_to = txt_timeend_to.Text;
                        _u1_doc_setotmonth[count_headset].sum_hours = tot_actual_otmonth.ToString();
                        _u1_doc_setotmonth[count_headset].settime = "0"; // เดี๋ยวมาแก้นะ
                        _u1_doc_setotmonth[count_headset].comment = "-"; // เดี๋ยวมาแก้นะ

                        count_headset++;
                    }



                }
                //insert u1 document ot month

                data_set_otmonth.ovt_u0_document_list[0] = u0_headset_otmonth;
                data_set_otmonth.ovt_u1_document_list = _u1_doc_setotmonth;
                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_set_otmonth));


                data_set_otmonth = callServicePostOvertime(_urlSetOTMontHeadUser, data_set_otmonth);

                if (data_set_otmonth.return_code == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('จำนวนรายชื่อที่กำหนด OT = {0} คน');", count_headset.ToString()), true);
                    setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- มีข้อมูลอยู่ในระบบแล้ว ---');", true);
                    break;
                }

                ////ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('จำนวนรายชื่อที่กำหนด OT = {0} คน');", count_headset.ToString()), true);
                ////setActiveTab("docDetail", 0, 0, 0, 0, 2, 0);


                break;

            case "cmdViewDetail":

                linkBtnTrigger(btnViewDetail);
                string[] arg2 = new string[4];
                arg2 = e.CommandArgument.ToString().Split(';');
                int _u0_document_idx_view = int.Parse(arg2[0]);
                int _m0_node_idx_view = int.Parse(arg2[1]);
                int _m0_actor_idx_view = int.Parse(arg2[2]);
                int _staidx_view = int.Parse(arg2[3]);
                int _cemp_idx_view = int.Parse(arg2[4]);

                //litDebug.Text = _u0_document_idx_view.ToString() + "|" + _m0_node_idx_view.ToString() + "|" + _m0_actor_idx_view.ToString() + "|" + _staidx_view.ToString() + "|" + _cemp_idx_view.ToString() + "|";

                //setActiveTab("docDetail", 0, 0, 0, 0, 2, 0);
                setActiveTab("docDetail", _u0_document_idx_view, _m0_node_idx_view, _staidx_view, _m0_actor_idx_view, 2, _cemp_idx_view, 0);
                //linkBtnTrigger(btnViewDetail);
                break;
            case "cmdViewDetailOTDay":

                //linkBtnTrigger(btnViewDetail);
                string[] arg3 = new string[4];
                arg3 = e.CommandArgument.ToString().Split(';');
                int _u0_document_idx_viewotday = int.Parse(arg3[0]);
                int _m0_node_idx_viewotday = int.Parse(arg3[1]);
                int _m0_actor_idx_viewotday = int.Parse(arg3[2]);
                int _staidx_viewotday = int.Parse(arg3[3]);
                int _cemp_idx_viewotday = int.Parse(arg3[4]);


                setActiveTab("docDetail", _u0_document_idx_viewotday, _m0_node_idx_viewotday, _staidx_viewotday, _m0_actor_idx_viewotday, 1, _cemp_idx_viewotday, 0);
                //linkBtnTrigger(btnViewDetail);
                break;
            case "cmdBackDetailImportOTDay":

                setActiveTab("docDetailImportOTDay", 0, 0, 0, 0, 0, 0, 0);
                //linkBtnTrigger(btnViewDetail);
                break;
            case "cmdViewEmployeeImportOTDay":

                //linkBtnTrigger(btnViewDetail);
                string[] arg4 = new string[4];
                arg4 = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_viewimport = int.Parse(arg4[0]);
                int _m0_node_idx_viewimport = int.Parse(arg4[1]);
                int _m0_actor_idx_viewimport = int.Parse(arg4[2]);
                int _staidx_viewimport = int.Parse(arg4[3]);
                int _cemp_idx_viewimport = int.Parse(arg4[4]);


                setActiveTab("docDetailImportOTDay", _u0_doc_idx_viewimport, _m0_node_idx_viewimport, _staidx_viewimport, _m0_actor_idx_viewimport, 1, _cemp_idx_viewimport, 0);
                //linkBtnTrigger(btnViewDetail);
                break;
            case "cmdViewDetailWaitApprove":

                linkBtnTrigger(btnViewDetailWaitApprove);
                string[] arg_approve = new string[4];
                arg_approve = e.CommandArgument.ToString().Split(';');
                int _u0_docidx_approve = int.Parse(arg_approve[0]);
                int _m0_node_idx_approve = int.Parse(arg_approve[1]);
                int _m0_actor_idx_approve = int.Parse(arg_approve[2]);
                int _staidx_approve = int.Parse(arg_approve[3]);
                int _cemp_idx_approve = int.Parse(arg_approve[4]);
                //int _car_use_idx_view = int.Parse(arg2[5]);

                //ViewState["Vs_cemp_idx_view"] = _cemp_idx_view;

                //setActiveTab("docDetail", 0, 0, 0, 0, 2, 0);
                setActiveTab("docWaitApprove", _u0_docidx_approve, _m0_node_idx_approve, _staidx_approve, _m0_actor_idx_approve, 2, 0, 0);
                //linkBtnTrigger(btnViewDetail);
                break;
            case "cmdViewDetailWaitApproveOTDay":

                //linkBtnTrigger(btnViewDetailWaitApprove);
                string[] arg_approve_day = new string[4];
                arg_approve_day = e.CommandArgument.ToString().Split(';');
                int _u0_docidx_approve_day = int.Parse(arg_approve_day[0]);
                int _m0_node_idx_approve_day = int.Parse(arg_approve_day[1]);
                int _m0_actor_idx_approve_day = int.Parse(arg_approve_day[2]);
                int _staidx_approve_day = int.Parse(arg_approve_day[3]);
                int _cemp_idx_approve_day = int.Parse(arg_approve_day[4]);
                //int _car_use_idx_view = int.Parse(arg2[5]);

                //ViewState["Vs_cemp_idx_view"] = _cemp_idx_view;

                //setActiveTab("docDetail", 0, 0, 0, 0, 2, 0);
                setActiveTab("docWaitApprove", _u0_docidx_approve_day, _m0_node_idx_approve_day, _staidx_approve_day, _m0_actor_idx_approve_day, 1, 0, 0);
                //linkBtnTrigger(btnViewDetail);
                break;
            case "cmdBackToWaitApprove":
                //setActiveTab("docWaitApprove", 0, 0, 0, 0, int.Parse(cmdArg), 0, 0);



                getBackWaitApproveDetailOT();


                break;
            case "cmdResetSearchWaitApproveOTMonth":
                setActiveTab("docWaitApprove", 0, 0, 0, 0, 2, 0, 0);

                break;
            
            case "cmdSaveApprove": //head user approve ot month

                string[] arg_headapprove = new string[1];
                arg_headapprove = e.CommandArgument.ToString().Split(';');
                int _decision_idx_headapprove = int.Parse(arg_headapprove[0]);
                string _decision_name_headapprove = arg_headapprove[1];

                HiddenField hfu0_document_idx = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfu0_document_idx");
                HiddenField hfM0NodeIDX = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfM0NodeIDX");
                HiddenField hfM0ActoreIDX = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfM0ActoreIDX");
                HiddenField hfM0StatusIDX = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfM0StatusIDX");

                DropDownList ddlApproveStatus = (DropDownList)FvHeadUserApprove.FindControl("ddlApproveStatus");
                TextBox txt_comment_approve = (TextBox)FvHeadUserApprove.FindControl("txt_comment_approve");


                data_overtime data_u0_document_headapprove = new data_overtime();
                ovt_u0_document_detail u0_document_headapprove = new ovt_u0_document_detail();
                data_u0_document_headapprove.ovt_u0_document_list = new ovt_u0_document_detail[1];

                u0_document_headapprove.u0_doc_idx = int.Parse(hfu0_document_idx.Value);
                u0_document_headapprove.m0_node_idx = int.Parse(hfM0NodeIDX.Value);
                u0_document_headapprove.m0_actor_idx = int.Parse(hfM0ActoreIDX.Value);
                u0_document_headapprove.staidx = int.Parse(hfM0StatusIDX.Value);
                ////u0_document_headapprove.decision = int.Parse(ddlApproveStatus.SelectedValue);
                u0_document_headapprove.decision = _decision_idx_headapprove;

                if (txt_comment_approve.Text == "")
                {
                    u0_document_headapprove.comment = "-";
                }
                else
                {
                    u0_document_headapprove.comment = txt_comment_approve.Text;
                }

                u0_document_headapprove.cemp_idx = _emp_idx;

                data_u0_document_headapprove.ovt_u0_document_list[0] = u0_document_headapprove;

                //update u1 document ot month
                var _u1doc_otmonth_approve = new ovt_u1_document_detail[GvViewWaitApproveOTMont.Rows.Count];
                int sumcheck_approve = 0;
                int count_approve = 0;

                foreach (GridViewRow gv_row in GvViewWaitApproveOTMont.Rows)
                {

                    data_u0_document_headapprove.ovt_u1_document_list = new ovt_u1_document_detail[1];
                    CheckBox chkstatus_otbefore = (CheckBox)gv_row.FindControl("chkstatus_otbefore");
                    Label lbl_u0_doc1_idx_approve = (Label)gv_row.FindControl("lbl_u0_doc1_idx_approve");

                    DropDownList ddl_timebefore_approve = (DropDownList)gv_row.FindControl("ddl_timebefore_approve");
                    DropDownList ddl_timeafter_approve = (DropDownList)gv_row.FindControl("ddl_timeafter_approve");
                    DropDownList ddl_timeholiday_approve = (DropDownList)gv_row.FindControl("ddl_timeholiday_approve");

                    Label lblot_before_approve = (Label)gv_row.FindControl("lblot_before_approve");
                    Label lblot_after_approve = (Label)gv_row.FindControl("lblot_after_approve");
                    Label lblot_holiday_approve = (Label)gv_row.FindControl("lblot_holiday_approve");

                    //Label lb_sum_counttotal = (Label)gv_row.FindControl("lb_sum_counttotal");

                    if (chkstatus_otbefore.Checked == true)
                    {

                        _u1doc_otmonth_approve[count_approve] = new ovt_u1_document_detail();
                        //_u1doc_otmonth_approve[count_approve].emp_code = tbEmpCode_.Text;
                        _u1doc_otmonth_approve[count_approve].u1_doc_idx = int.Parse(lbl_u0_doc1_idx_approve.Text);
                        _u1doc_otmonth_approve[count_approve].ot_before_status = 1;

                        if (ddl_timebefore_approve.SelectedValue != "")
                        {
                            _u1doc_otmonth_approve[count_approve].ot_before = ddl_timebefore_approve.SelectedValue.ToString();
                        }
                        else
                        {
                            _u1doc_otmonth_approve[count_approve].ot_before = "0.00";
                        }
                        _u1doc_otmonth_approve[count_approve].hours_ot_before = lblot_before_approve.Text;


                        if (ddl_timeafter_approve.SelectedValue != "")
                        {
                            _u1doc_otmonth_approve[count_approve].ot_after = ddl_timeafter_approve.SelectedValue.ToString();
                        }
                        else
                        {
                            _u1doc_otmonth_approve[count_approve].ot_after = "0.00";
                        }
                        _u1doc_otmonth_approve[count_approve].hours_ot_after = lblot_after_approve.Text;


                        if (ddl_timeholiday_approve.SelectedValue != "")
                        {
                            _u1doc_otmonth_approve[count_approve].ot_holiday = ddl_timeholiday_approve.SelectedValue.ToString();
                        }
                        else
                        {
                            _u1doc_otmonth_approve[count_approve].ot_holiday = "0.00";
                        }
                        _u1doc_otmonth_approve[count_approve].hours_ot_holiday = lblot_holiday_approve.Text;

                        sumcheck_approve = sumcheck_approve + 1;
                        count_approve++;

                    }
                    else
                    {
                        _u1doc_otmonth_approve[count_approve] = new ovt_u1_document_detail();
                        //_u1doc_otmonth_approve[count_approve].emp_code = tbEmpCode_.Text;
                        //_u1_doc_otmonth[count_].dateot_from = lbl_date_start.Text;
                        _u1doc_otmonth_approve[count_approve].u1_doc_idx = int.Parse(lbl_u0_doc1_idx_approve.Text);
                        _u1doc_otmonth_approve[count_approve].ot_before_status = 2;

                        if (ddl_timebefore_approve.SelectedValue != "")
                        {
                            _u1doc_otmonth_approve[count_approve].ot_before = ddl_timebefore_approve.SelectedValue.ToString();
                        }
                        else
                        {
                            _u1doc_otmonth_approve[count_approve].ot_before = "0.00";
                        }
                        _u1doc_otmonth_approve[count_approve].hours_ot_before = lblot_before_approve.Text;


                        if (ddl_timeafter_approve.SelectedValue != "")
                        {
                            _u1doc_otmonth_approve[count_approve].ot_after = ddl_timeafter_approve.SelectedValue.ToString();
                        }
                        else
                        {
                            _u1doc_otmonth_approve[count_approve].ot_after = "0.00";
                        }
                        _u1doc_otmonth_approve[count_approve].hours_ot_after = lblot_after_approve.Text;


                        if (ddl_timeholiday_approve.SelectedValue != "")
                        {
                            _u1doc_otmonth_approve[count_approve].ot_holiday = ddl_timeholiday_approve.SelectedValue.ToString();
                        }
                        else
                        {
                            _u1doc_otmonth_approve[count_approve].ot_holiday = "0.00";
                        }
                        _u1doc_otmonth_approve[count_approve].hours_ot_holiday = lblot_holiday_approve.Text;

                        sumcheck_approve = sumcheck_approve + 1;
                        count_approve++;
                    }

                }
                //update u1 document ot month
                data_u0_document_headapprove.ovt_u1_document_list = _u1doc_otmonth_approve;
                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_headapprove));
                data_u0_document_headapprove = callServicePostOvertime(_urlSetHeadUserApproveOTMonth, data_u0_document_headapprove);

                getCountWaitDetailApprove();
                getBackWaitApproveDetailOT();
                

                //setActiveTab("docWaitApprove", 0, 0, 0, 0, 2, 0, 0);

                break;
            case "cmdHrSaveApprove": //hr approve ot month

                string[] arg_hrapprove = new string[1];
                arg_hrapprove = e.CommandArgument.ToString().Split(';');
                int _decision_idx_hrapprove = int.Parse(arg_hrapprove[0]);
                string _decision_name_hrapprove = arg_hrapprove[1];

                HiddenField hfu0_document_idx_hr = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfu0_document_idx");
                HiddenField hfM0NodeIDX_hr = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfM0NodeIDX");
                HiddenField hfM0ActoreIDX_hr = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfM0ActoreIDX");
                HiddenField hfM0StatusIDX_hr = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfM0StatusIDX");

                DropDownList ddlApproveStatus_hr = (DropDownList)FvHrApproveOTMonth.FindControl("ddlHrApproveStatus");
                TextBox txt_comment_approve_hr = (TextBox)FvHrApproveOTMonth.FindControl("txt_comment_hrapprove");


                data_overtime data_u0_document_hrapprove = new data_overtime();
                ovt_u0_document_detail u0_document_hrapprove = new ovt_u0_document_detail();
                data_u0_document_hrapprove.ovt_u0_document_list = new ovt_u0_document_detail[1];

                u0_document_hrapprove.u0_doc_idx = int.Parse(hfu0_document_idx_hr.Value);
                u0_document_hrapprove.m0_node_idx = int.Parse(hfM0NodeIDX_hr.Value);
                u0_document_hrapprove.m0_actor_idx = int.Parse(hfM0ActoreIDX_hr.Value);
                u0_document_hrapprove.staidx = int.Parse(hfM0StatusIDX_hr.Value);
                u0_document_hrapprove.decision = _decision_idx_hrapprove;
                //u0_document_hrapprove.decision = int.Parse(ddlApproveStatus_hr.SelectedValue);
                u0_document_hrapprove.comment = txt_comment_approve_hr.Text;
                u0_document_hrapprove.cemp_idx = _emp_idx;

                //insert u1 document ot month
                var _u1_doc_otmonth_hrapprove = new ovt_u1_document_detail[GvViewWaitApproveOTMont.Rows.Count];

                int count_hrapprove = 0;
                //int sum_total_price = 0;

                foreach (GridViewRow gv_row_hrapprove in GvViewWaitApproveOTMont.Rows)
                {

                    data_u0_document_hrapprove.ovt_u1_document_list = new ovt_u1_document_detail[1];


                    Label lbl_u0_doc1_idx_approve = (Label)gv_row_hrapprove.FindControl("lbl_u0_doc1_idx_approve");
                    TextBox lbl_comment_approve = (TextBox)gv_row_hrapprove.FindControl("lbl_comment_approve");
                    TextBox lbl_comment_hr_approve = (TextBox)gv_row_hrapprove.FindControl("lbl_comment_hr_approve");

                    _u1_doc_otmonth_hrapprove[count_hrapprove] = new ovt_u1_document_detail();
                    _u1_doc_otmonth_hrapprove[count_hrapprove].u1_doc_idx = int.Parse(lbl_u0_doc1_idx_approve.Text);
                    _u1_doc_otmonth_hrapprove[count_hrapprove].cemp_idx = _emp_idx;

                    if (lbl_comment_approve.Text != "")
                    {
                        _u1_doc_otmonth_hrapprove[count_hrapprove].comment_hr = lbl_comment_hr_approve.Text;
                    }
                    else
                    {
                        _u1_doc_otmonth_hrapprove[count_hrapprove].comment_hr = "-";
                    }


                    count_hrapprove++;

                }
                //insert u1 document ot month
                data_u0_document_hrapprove.ovt_u0_document_list[0] = u0_document_hrapprove;
                data_u0_document_hrapprove.ovt_u1_document_list = _u1_doc_otmonth_hrapprove;
                //
                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_hrapprove));
                data_u0_document_headapprove = callServicePostOvertime(_urlSetHeadUserApproveOTMonth, data_u0_document_hrapprove);

                getCountWaitDetailApprove();
                getBackWaitApproveDetailOT();
                //setActiveTab("docWaitApprove", 0, 0, 0, 0, 2, 0, 0);

                break;
            case "cmdSaveEditOTMonth":


                Label lit_total_hoursmonth_edit = (Label)GvViewEditDetailOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursmonth_edit");

                HiddenField hfu0_document_idx_edit = (HiddenField)FvDetailOTMont.FindControl("hfu0_document_idx");
                HiddenField hfM0NodeIDX_edit = (HiddenField)FvDetailOTMont.FindControl("hfM0NodeIDX");
                HiddenField hfM0ActoreIDX_edit = (HiddenField)FvDetailOTMont.FindControl("hfM0ActoreIDX");
                HiddenField hfM0StatusIDX_edit = (HiddenField)FvDetailOTMont.FindControl("hfM0StatusIDX");
                Label lbl_emp_code_view = (Label)FvDetailOTMont.FindControl("lbl_emp_code_view");
                //lit_total_hoursmonth.Text = String.Format("{0:N2}", tot_actual_otmonth);

                data_overtime data_edit_otmonth = new data_overtime();

                //edit u0 document ot month
                ovt_u0_document_detail u0_edit_otmonth = new ovt_u0_document_detail();
                data_edit_otmonth.ovt_u0_document_list = new ovt_u0_document_detail[1];

                u0_edit_otmonth.u0_doc_idx = int.Parse(hfu0_document_idx_edit.Value);
                u0_edit_otmonth.emp_type = 2; //ot month
                u0_edit_otmonth.cemp_idx = _emp_idx;
                u0_edit_otmonth.m0_actor_idx = int.Parse(hfM0ActoreIDX_edit.Value);
                u0_edit_otmonth.m0_node_idx = int.Parse(hfM0NodeIDX_edit.Value);
                u0_edit_otmonth.decision = 0;
                u0_edit_otmonth.sumhours = lit_total_hoursmonth_edit.Text;

                data_edit_otmonth.ovt_u0_document_list[0] = u0_edit_otmonth;
                //insert u0 document ot month

                //insert u1 document ot month
                var _u1_doc_otmonth_edit = new ovt_u1_document_detail[GvViewEditDetailOTMont.Rows.Count];

                int count_edit = 0;

                int count_checkbox = 0;
                int count_checktext = 0;
                //int sum_total_price = 0;

                foreach (GridViewRow gv_rowedit in GvViewEditDetailOTMont.Rows)
                {

                    data_edit_otmonth.ovt_u1_document_list = new ovt_u1_document_detail[1];

                    CheckBox chk_date_ot_edit = (CheckBox)gv_rowedit.FindControl("chk_date_ot_edit");
                    Label lbl_u1_doc_idx_edit = (Label)gv_rowedit.FindControl("lbl_u1_doc_idx_edit");
                    Label lbl_date_start_edit = (Label)gv_rowedit.FindControl("lbl_date_start_edit");
                    DropDownList ddl_timestart_edit = (DropDownList)gv_rowedit.FindControl("ddl_timestart_edit");
                    DropDownList ddl_timeend_edit = (DropDownList)gv_rowedit.FindControl("ddl_timeend_edit");
                    TextBox txt_job_edit = (TextBox)gv_rowedit.FindControl("txt_job_edit");
                    //TextBox txt_timestart_job = (TextBox)gv_rowedit.FindControl("txt_timestart_job");
                    //TextBox txt_timeend_job = (TextBox)gv_rowedit.FindControl("txt_timeend_job");
                    TextBox txt_sum_hour_edit = (TextBox)gv_rowedit.FindControl("txt_sum_hour_edit");
                    //TextBox txt_head_set = (TextBox)gv_rowedit.FindControl("txt_head_set");
                    TextBox txt_remark_edit = (TextBox)gv_rowedit.FindControl("txt_remark_edit");
                    TextBox txt_head_set_edit = (TextBox)gv_rowedit.FindControl("txt_head_set_edit");

                    Label txtot_before_edit = (Label)gv_rowedit.FindControl("txtot_before_edit");
                    Label lblot_before_edit = (Label)gv_rowedit.FindControl("lblot_before_edit");
                    Label txtot_after_edit = (Label)gv_rowedit.FindControl("txtot_after_edit");
                    Label lblot_after_edit = (Label)gv_rowedit.FindControl("lblot_after_edit");
                    Label txtot_holiday_edit = (Label)gv_rowedit.FindControl("txtot_holiday_edit");
                    Label lblot_holiday_edit = (Label)gv_rowedit.FindControl("lblot_holiday_edit");

                    DropDownList ddl_timebefore_edit = (DropDownList)gv_rowedit.FindControl("ddl_timebefore_edit");
                    DropDownList ddl_timeafter_edit = (DropDownList)gv_rowedit.FindControl("ddl_timeafter_edit");
                    DropDownList ddl_timeholiday_edit = (DropDownList)gv_rowedit.FindControl("ddl_timeholiday_edit");


                    //Label lb_sum_counttotal = (Label)gv_row.FindControl("lb_sum_counttotal");

                    if (chk_date_ot_edit.Checked == true)
                    {

                        _u1_doc_otmonth_edit[count_edit] = new ovt_u1_document_detail();
                        _u1_doc_otmonth_edit[count_edit].u1_doc_idx = int.Parse(lbl_u1_doc_idx_edit.Text);
                        _u1_doc_otmonth_edit[count_edit].cemp_idx = _emp_idx;
                        _u1_doc_otmonth_edit[count_edit].emp_code = lbl_emp_code_view.Text;
                        _u1_doc_otmonth_edit[count_edit].dateot_from = ddl_timestart_edit.SelectedItem.Text;
                        _u1_doc_otmonth_edit[count_edit].dateot_to = ddl_timeend_edit.SelectedItem.Text;

                        if (txt_job_edit.Text != "")
                        {
                            _u1_doc_otmonth_edit[count_edit].detail_job = txt_job_edit.Text;

                            count_checktext++;

                        }
                        //else
                        //{
                        //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณากรอกข้อมูลให้ครบถ้วน ก่อนบันทึกรายการ ---');", true);
                        //    break;
                        //}

                        if (ddl_timestart_edit.SelectedItem.Text != "")
                        {
                            _u1_doc_otmonth_edit[count_edit].time_form = ddl_timestart_edit.SelectedItem.Text;
                        }

                        if (ddl_timestart_edit.SelectedItem.Text != "")
                        {
                            _u1_doc_otmonth_edit[count_edit].time_to = ddl_timeend_edit.SelectedItem.Text;
                        }

                        //_u1_doc_otmonth_edit[count_edit].time_form = ddl_timestart_edit.SelectedItem.Text;
                        //_u1_doc_otmonth_edit[count_edit].time_to = ddl_timeend_edit.SelectedItem.Text;
                        _u1_doc_otmonth_edit[count_edit].sum_hours = txt_sum_hour_edit.Text;
                        _u1_doc_otmonth_edit[count_edit].settime = txt_head_set_edit.Text;
                        //_u1_doc_otmonth_edit[count_edit].settime = 0; // เดี๋ยวมาแก้นะ
                        _u1_doc_otmonth_edit[count_edit].comment = txt_remark_edit.Text;
                        _u1_doc_otmonth_edit[count_edit].u1_ovt_status = 1;
                        _u1_doc_otmonth_edit[count_edit].month_idx_start = int.Parse(ddl_timestart_edit.SelectedValue);
                        _u1_doc_otmonth_edit[count_edit].month_idx_end = int.Parse(ddl_timeend_edit.SelectedValue);

                        if (ddl_timebefore_edit.SelectedValue != "")
                        {
                            _u1_doc_otmonth_edit[count_edit].ot_before = ddl_timebefore_edit.SelectedValue.ToString();
                            _u1_doc_otmonth_edit[count_edit].hours_ot_before = lblot_before_edit.Text;
                        }
                        else
                        {
                            _u1_doc_otmonth_edit[count_edit].ot_before = "0.00";
                            _u1_doc_otmonth_edit[count_edit].hours_ot_before = lblot_before_edit.Text;
                        }

                        if (ddl_timeafter_edit.SelectedValue != "")
                        {
                            _u1_doc_otmonth_edit[count_edit].ot_after = ddl_timeafter_edit.SelectedValue.ToString();
                            _u1_doc_otmonth_edit[count_edit].hours_ot_after = lblot_after_edit.Text;
                        }
                        else
                        {
                            _u1_doc_otmonth_edit[count_edit].ot_after = "0.00";
                            _u1_doc_otmonth_edit[count_edit].hours_ot_after = lblot_after_edit.Text;
                        }

                        if (ddl_timeholiday_edit.SelectedValue != "")
                        {
                            _u1_doc_otmonth_edit[count_edit].ot_holiday = ddl_timeholiday_edit.SelectedValue.ToString();
                            _u1_doc_otmonth_edit[count_edit].hours_ot_holiday = lblot_holiday_edit.Text;
                        }
                        else
                        {
                            _u1_doc_otmonth_edit[count_edit].ot_holiday = "0.00";
                            _u1_doc_otmonth_edit[count_edit].hours_ot_holiday = lblot_holiday_edit.Text;
                        }

                        count_checkbox++;
                    }
                    //else if(chk_date_ot_edit.Checked == false && (ddl_timestart_edit.SelectedItem.Text != "" || ddl_timestart_edit.SelectedItem.Text != null) 
                    //    && (ddl_timeend_edit.SelectedItem.Text != "" || ddl_timeend_edit.SelectedItem.Text != null))
                    else //if (ddl_timestart_edit.SelectedValue != "" && ddl_timeend_edit.SelectedValue != "")
                    {
                        _u1_doc_otmonth_edit[count_edit] = new ovt_u1_document_detail();
                        _u1_doc_otmonth_edit[count_edit].u1_doc_idx = int.Parse(lbl_u1_doc_idx_edit.Text);
                        _u1_doc_otmonth_edit[count_edit].cemp_idx = _emp_idx;
                        _u1_doc_otmonth_edit[count_edit].emp_code = lbl_emp_code_view.Text;


                        _u1_doc_otmonth_edit[count_edit].detail_job = txt_job_edit.Text;

                        if (ddl_timestart_edit.SelectedValue != "")
                        {
                            _u1_doc_otmonth_edit[count_edit].dateot_from = ddl_timestart_edit.SelectedItem.Text;
                            _u1_doc_otmonth_edit[count_edit].time_form = ddl_timestart_edit.SelectedItem.Text;
                        }


                        //litDebug.Text = ddl_timeend_edit.SelectedValue.ToString();

                        if (ddl_timeend_edit.SelectedValue != "")
                        {
                            _u1_doc_otmonth_edit[count_edit].dateot_to = ddl_timeend_edit.SelectedItem.Text;
                            _u1_doc_otmonth_edit[count_edit].time_to = ddl_timeend_edit.SelectedItem.Text;
                        }


                        //_u1_doc_otmonth_edit[count_edit].sum_hours = txt_sum_hour_edit.Text;
                        //_u1_doc_otmonth_edit[count_edit].settime = txt_head_set_edit.Text;
                        //_u1_doc_otmonth_edit[count_edit].settime = 0; // เดี๋ยวมาแก้นะ
                        _u1_doc_otmonth_edit[count_edit].comment = txt_remark_edit.Text;
                        _u1_doc_otmonth_edit[count_edit].u1_ovt_status = 0;

                        if (ddl_timestart_edit.SelectedValue != "")
                        {
                            _u1_doc_otmonth_edit[count_edit].month_idx_start = int.Parse(ddl_timestart_edit.SelectedValue);
                        }

                        if (ddl_timeend_edit.SelectedValue != "")
                        {
                            _u1_doc_otmonth_edit[count_edit].month_idx_end = int.Parse(ddl_timeend_edit.SelectedValue);
                        }

                        if (ddl_timeafter_edit.SelectedValue != "")
                        {
                            _u1_doc_otmonth_edit[count_edit].ot_after = ddl_timeafter_edit.SelectedValue.ToString();
                            _u1_doc_otmonth_edit[count_edit].hours_ot_after = lblot_after_edit.Text;
                        }
                        else
                        {
                            _u1_doc_otmonth_edit[count_edit].ot_after = "0.00";
                            _u1_doc_otmonth_edit[count_edit].hours_ot_after = lblot_after_edit.Text;
                        }

                        if (ddl_timeholiday_edit.SelectedValue != "")
                        {
                            _u1_doc_otmonth_edit[count_edit].ot_holiday = ddl_timeholiday_edit.SelectedValue.ToString();
                            _u1_doc_otmonth_edit[count_edit].hours_ot_holiday = lblot_holiday_edit.Text;
                        }
                        else
                        {
                            _u1_doc_otmonth_edit[count_edit].ot_holiday = "0.00";
                            _u1_doc_otmonth_edit[count_edit].hours_ot_holiday = lblot_holiday_edit.Text;
                        }

                        //_u1_doc_otmonth_edit[count_edit].ot_before = txtot_before_edit.Text;
                        //_u1_doc_otmonth_edit[count_edit].hours_ot_before = lblot_before_edit.Text;

                        //_u1_doc_otmonth_edit[count_edit].ot_after = txtot_after_edit.Text;
                        //_u1_doc_otmonth_edit[count_edit].hours_ot_after = lblot_after_edit.Text;

                        //_u1_doc_otmonth_edit[count_edit].ot_holiday = txtot_holiday_edit.Text;
                        //_u1_doc_otmonth_edit[count_edit].hours_ot_holiday = lblot_holiday_edit.Text;
                    }

                    count_edit++;

                }
                //insert u1 document ot month

                data_edit_otmonth.ovt_u0_document_list[0] = u0_edit_otmonth;
                data_edit_otmonth.ovt_u1_document_list = _u1_doc_otmonth_edit;

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_edit_otmonth));

                if (count_checkbox == count_checktext)
                {
                    data_edit_otmonth = callServicePostOvertime(_urlSetUserEditOTMonth, data_edit_otmonth);
                    FileUpload UploadFileMemoEdit = (FileUpload)Update_PanelSaveEditDocMonth.FindControl("UploadFileMemoEdit");
                    if (UploadFileMemoEdit.HasFile)
                    {
                        //litDebug.Text = "1";
                        string filepath = Server.MapPath(_path_file_otmonth);

                        //litDebug.Text = filepath.ToString();
                        HttpFileCollection uploadedFiles = Request.Files;

                        for (int i = 0; i < uploadedFiles.Count; i++)
                        {
                            HttpPostedFile userPostedFileMemo = uploadedFiles[i];

                            try
                            {
                                if (userPostedFileMemo.ContentLength > 0)
                                {
                                    ////string filePath12 = Constants.KEY_GET_FROM_PERM_PERM + "-" + dataADOnlineU0.return_code.ToString();
                                    string filePath2_editold = hfu0_document_idx_edit.Value.ToString();
                                    string filePath1_edit = Server.MapPath(_path_file_otmonth + filePath2_editold);

                                    string _filepathExtension = Path.GetExtension(userPostedFileMemo.FileName);

                                    if (!Directory.Exists(filePath1_edit))
                                    {
                                        //litDebug.Text = "99999";
                                        string filePath_new = Server.MapPath(_path_file_otmonth + filePath2_editold);

                                        if (!Directory.Exists(filePath_new))
                                        {
                                            Directory.CreateDirectory(filePath_new);
                                        }
                                        //string extension = Path.GetExtension(UploadFileCarEdit.FileName);

                                        //UploadFileCarEdit.SaveAs(Server.MapPath(getPathfile_edit + directoryName_edit_new) + "\\" + directoryName_edit_new + extension);
                                        userPostedFileMemo.SaveAs(filepath + filePath2_editold + "\\" + filePath2_editold.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                    }
                                    else
                                    {
                                        string path_old = string.Empty;
                                        List<string> files = new List<string>();
                                        DirectoryInfo dirListRoom = new DirectoryInfo(filePath1_edit);
                                        foreach (FileInfo file in dirListRoom.GetFiles())
                                        {
                                            path_old = ResolveUrl(filePath1_edit + "\\" + file.Name);
                                        }

                                        //litDebug.Text = path_old.ToString();

                                        File.Delete(path_old);
                                        userPostedFileMemo.SaveAs(filepath + filePath2_editold + "\\" + filePath2_editold.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        ////string extension = Path.GetExtension(UploadFileCarEdit.FileName);
                                        ////UploadFileCarEdit.SaveAs(Server.MapPath(getPathfile_edit + directoryName_edit_old) + "\\" + directoryName_edit_old + extension);

                                    }

                                }
                            }
                            catch (Exception Ex)
                            {
                                //litDebug.Text += "Error: <br>" + Ex.Message;
                            }
                        }


                    }
                    //else
                    //{
                    //    //litDebug.Text = "2";
                    //}

                    //litDebug.Text = String.Format("{0:N2}", lit_total_hoursmonth.Text);

                    //setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);
                    getDetailBackIndex();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณากรอกข้อมูลให้ครบถ้วน ก่อนบันทึกรายการ ---');", true);
                    break;
                }
                break;

            case "cmdSaveApproveOTDay": //head user approve ot day

                string[] arg_headapprove_otday = new string[1];
                arg_headapprove_otday = e.CommandArgument.ToString().Split(';');
                int _decision_idx_headapproveotday = int.Parse(arg_headapprove_otday[0]);
                string _decision_name_headapproveotday = arg_headapprove_otday[1];

                HiddenField hfu0_document_idx_otday = (HiddenField)FvDetailOTDayWaitapprove.FindControl("hfu0_document_idx");
                HiddenField hfM0NodeIDX_otday = (HiddenField)FvDetailOTDayWaitapprove.FindControl("hfM0NodeIDX");
                HiddenField hfM0ActoreIDX_otday = (HiddenField)FvDetailOTDayWaitapprove.FindControl("hfM0ActoreIDX");
                HiddenField hfM0StatusIDX_otday = (HiddenField)FvDetailOTDayWaitapprove.FindControl("hfM0StatusIDX");

                TextBox txt_comment_approve_otday = (TextBox)FvHeadUserApproveOTDay.FindControl("txt_comment_approve_otday");

                data_overtime data_u0_document_headapprove_otday = new data_overtime();
                ovt_u0_document_detail u0_document_headapprove_otday = new ovt_u0_document_detail();
                data_u0_document_headapprove_otday.ovt_u0_document_list = new ovt_u0_document_detail[1];

                u0_document_headapprove_otday.u0_doc_idx = int.Parse(hfu0_document_idx_otday.Value);
                u0_document_headapprove_otday.m0_node_idx = int.Parse(hfM0NodeIDX_otday.Value);
                u0_document_headapprove_otday.m0_actor_idx = int.Parse(hfM0ActoreIDX_otday.Value);
                u0_document_headapprove_otday.staidx = int.Parse(hfM0StatusIDX_otday.Value);
                ////u0_document_headapprove.decision = int.Parse(ddlApproveStatus.SelectedValue);
                u0_document_headapprove_otday.decision = _decision_idx_headapproveotday;

                if (txt_comment_approve_otday.Text == "")
                {
                    u0_document_headapprove_otday.comment = "-";
                }
                else
                {
                    u0_document_headapprove_otday.comment = txt_comment_approve_otday.Text;
                }

                u0_document_headapprove_otday.cemp_idx = _emp_idx;

                data_u0_document_headapprove_otday.ovt_u0_document_list[0] = u0_document_headapprove_otday;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_headapprove_otday));
                data_u0_document_headapprove_otday = callServicePostOvertime(_urlSetHeadUserApproveOTMonth, data_u0_document_headapprove_otday);

                getCountWaitDetailApprove();


                break;
            case "cmdHrSaveApproveOTDay": //hr approve ot day

                string[] arg_hrapprove_otday = new string[1];
                arg_hrapprove_otday = e.CommandArgument.ToString().Split(';');
                int _decision_idx_hrapprove_otday = int.Parse(arg_hrapprove_otday[0]);
                string _decision_name_hrapprove_otday = arg_hrapprove_otday[1];

                HiddenField hfu0_document_idx_hrotday = (HiddenField)FvDetailOTDayWaitapprove.FindControl("hfu0_document_idx");
                HiddenField hfM0NodeIDX_hrotday = (HiddenField)FvDetailOTDayWaitapprove.FindControl("hfM0NodeIDX");
                HiddenField hfM0ActoreIDX_hrotday = (HiddenField)FvDetailOTDayWaitapprove.FindControl("hfM0ActoreIDX");
                HiddenField hfM0StatusIDX_hrotday = (HiddenField)FvDetailOTDayWaitapprove.FindControl("hfM0StatusIDX");


                TextBox txt_comment_hrapprove_otday = (TextBox)FvHrApproveOTDay.FindControl("txt_comment_hrapprove_otday");


                data_overtime data_u0_document_hrapprove_otday = new data_overtime();
                ovt_u0_document_detail u0_document_hrapprove_otday = new ovt_u0_document_detail();
                data_u0_document_hrapprove_otday.ovt_u0_document_list = new ovt_u0_document_detail[1];

                u0_document_hrapprove_otday.u0_doc_idx = int.Parse(hfu0_document_idx_hrotday.Value);
                u0_document_hrapprove_otday.m0_node_idx = int.Parse(hfM0NodeIDX_hrotday.Value);
                u0_document_hrapprove_otday.m0_actor_idx = int.Parse(hfM0ActoreIDX_hrotday.Value);
                u0_document_hrapprove_otday.staidx = int.Parse(hfM0StatusIDX_hrotday.Value);
                u0_document_hrapprove_otday.decision = _decision_idx_hrapprove_otday;
                //u0_document_hrapprove.decision = int.Parse(ddlApproveStatus_hr.SelectedValue);

                u0_document_hrapprove_otday.cemp_idx = _emp_idx;

                if (txt_comment_hrapprove_otday.Text == "")
                {
                    u0_document_hrapprove_otday.comment = "-";
                }
                else
                {
                    u0_document_hrapprove_otday.comment = txt_comment_hrapprove_otday.Text;
                }

                data_u0_document_hrapprove_otday.ovt_u0_document_list[0] = u0_document_hrapprove_otday;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_hrapprove_otday));
                //////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_hrapprove));
                data_u0_document_hrapprove_otday = callServicePostOvertime(_urlSetHeadUserApproveOTMonth, data_u0_document_hrapprove_otday);

                getCountWaitDetailApprove();


                break;
            case "cmdSearchReportDay":


                data_overtime data_search_reportday = new data_overtime();
                ovt_u0_document_day_detail search_reportday_detail = new ovt_u0_document_day_detail();
                data_search_reportday.ovt_u0_document_day_list = new ovt_u0_document_day_detail[1];
                search_reportday_detail.cemp_idx = _emp_idx;
                search_reportday_detail.u0_loc_idx = int.Parse(ddlplace_report.SelectedValue);
                search_reportday_detail.u0_org_idx = int.Parse(ddlorg_report.SelectedValue);
                search_reportday_detail.u0_dept_idx = int.Parse(ddlrdept_report.SelectedValue);
                search_reportday_detail.u0_sec_idx = int.Parse(ddlrsec_report.SelectedValue);
                search_reportday_detail.type_daywork_idx = int.Parse(ddl_type_daywork_report.SelectedValue);
                search_reportday_detail.type_ot_idx = int.Parse(ddl_typeot_report.SelectedValue);
                search_reportday_detail.dateot_from = txt_dateot_dayform_report.Text + " " + txt_time_dayform_report.Text;
                search_reportday_detail.dateot_to = txt_dateot_dayto_report.Text + " " + txt_time_dayto_report.Text;
                search_reportday_detail.time_form = txt_time_dayform_report.Text;
                search_reportday_detail.time_to = txt_time_dayto_report.Text;

                data_search_reportday.ovt_u0_document_day_list[0] = search_reportday_detail;


                //data_search_month = callServicePostOvertime(_urlGetDateScanOTMont, data_search_month);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportday));
                data_search_reportday = callServicePostOvertime(_urlGetSearchReportDetailEmpOTDay, data_search_reportday);

                if (data_search_reportday.return_code == 0)
                {
                    ViewState["Vs_SearchReportDetailEmployeeDay"] = data_search_reportday.ovt_u0_document_day_list;

                    GvSearchReportOTDay.Visible = true;
                    setGridData(GvSearchReportOTDay, ViewState["Vs_SearchReportDetailEmployeeDay"]);
                }
                else
                {
                    ViewState["Vs_SearchReportDetailEmployeeDay"] = null;

                    GvSearchReportOTDay.Visible = true;
                    setGridData(GvSearchReportOTDay, ViewState["Vs_SearchReportDetailEmployeeDay"]);
                }

                break;

            case "cmdSearchReportEmployeeImportOTDay":

                //linkBtnTrigger(btnViewDetail);
                string[] arg5 = new string[4];
                arg5 = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_reportday = int.Parse(arg5[0]);
                int _m0_node_idx_reportday = int.Parse(arg5[1]);
                int _m0_actor_idx_reportday = int.Parse(arg5[2]);
                int _staidx_reportday = int.Parse(arg5[3]);
                int _cemp_idx_reportday = int.Parse(arg5[4]);


                setActiveTab("docReportOTDay", _u0_doc_idx_reportday, _m0_node_idx_reportday, _staidx_reportday, _m0_actor_idx_reportday, 1, _cemp_idx_reportday, 0);
                break;

            case "cmdSearchReportMonth":
                //setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx)
                setActiveTab("docReportOTMonth", 0, 0, 0, 0, int.Parse(cmdArg), 0, 0);
                break;
            case "cmdResetSearchReport":
                //setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx)
                setActiveTab("docReportOTMonth", 0, 0, 0, 0, 0, 0, 0);
                break;

            case "cmdExportExcelReportMonth":
                int count_export = 0;

                ovt_report_otmonth_detail[] _templist_ReportOTMonth = (ovt_report_otmonth_detail[])ViewState["Vs_ReportOTMonth"];

                DataTable tableReportOtMonth = new DataTable();
                tableReportOtMonth.Columns.Add("รหัสพนักงาน", typeof(String));
                tableReportOtMonth.Columns.Add("วันที่จ่ายเงิน", typeof(String));
                tableReportOtMonth.Columns.Add("ประเภทเงินได้", typeof(String));
                tableReportOtMonth.Columns.Add("จำนวนเงิน", typeof(String));
                tableReportOtMonth.Columns.Add("จำนวน", typeof(String));
                tableReportOtMonth.Columns.Add("หน่วย", typeof(String));
                tableReportOtMonth.Columns.Add("อัตรา", typeof(String));
                tableReportOtMonth.Columns.Add("หมายเหตุ", typeof(String));


                var linqRoomDetailCheck = (from _dt_ReportExcelOtMonth in _templist_ReportOTMonth
                                           select _dt_ReportExcelOtMonth
                                           ).ToList();

                foreach (var row_report in linqRoomDetailCheck)
                {
                    DataRow addRowExcel = tableReportOtMonth.NewRow();

                    IFormatProvider culture_ = new CultureInfo("en-US", true);
                    var d_payment = new DateTime();
                    d_payment = DateTime.ParseExact((row_report.date_payment.ToString()), "dd/MM/yyyy", culture_);

                    addRowExcel[0] = row_report.emp_code.ToString();

                    if (d_payment.ToString("dd") == "31")
                    {

                        addRowExcel[1] = d_payment.AddDays(-1).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        addRowExcel[1] = d_payment.ToString("dd/MM/yyyy");
                    }


                    addRowExcel[2] = row_report.condition_otmonth.ToString();
                    addRowExcel[3] = "";
                    addRowExcel[4] = row_report.ot_total.ToString();
                    addRowExcel[5] = "ชั่วโมง";
                    addRowExcel[6] = "";
                    addRowExcel[7] = row_report.remark_otmonth.ToString();

                    tableReportOtMonth.Rows.InsertAt(addRowExcel, count_export++);
                }
                //WriteExcelWithNPOI(DataTable dt, String extension, String fileName, int _set_value)
                WriteExcelWithNPOI(tableReportOtMonth, "xls", "report-otmonth");


                break;

            case "cmdSearchIndexOTMonth":

                ////div_searchDetailOTMonth.Visible = false;
                Update_BackToDetailSearchIndexOTMonth.Visible = true;

                _PanelSearchDetail.Visible = true;
                getddlYear(ddlSearchYear);
                ddlSearchMonth.ClearSelection();
                ddlSearchemp.ClearSelection();

                txtSearchCostcenter.Text = string.Empty;
                txtSearchEmpcode.Text = string.Empty;

                getOrganizationList(ddlSearchorg);
                getDepartmentList(ddlSearchrdept, int.Parse(ddlSearchorg.SelectedValue));
                getSectionList(ddlSearchrsec, int.Parse(ddlSearchorg.SelectedValue), int.Parse(ddlSearchrdept.SelectedValue));
                getStatusDocList(ddlStatusDoc);

                break;

            case "cmdSearchWaitApproveOTMonth":

                div_SearchWaitApproveOTMonth.Visible = false;
                _PanelCancelSearchWaitApproveOTMonth.Visible = true;

                _PanelSearchWaitApprove.Visible = true;
                getddlYear(ddlSearchYear_WaitApprove);
                ddlSearchMonth_WaitApprove.ClearSelection();
                ddlSearch_emp_WaitApprove.ClearSelection();
                txt_SearchCost_WaitApprove.Text = string.Empty;
                txt_SearchEmpCode_WaitApprove.Text = string.Empty;

                getOrganizationList(ddlSearchorg_WaitApprove);
                getDepartmentList(ddlSearch_rdept_WaitApprove, int.Parse(ddlSearchorg_WaitApprove.SelectedValue));
                getSectionList(ddlSearch_rsec_WaitApprove, int.Parse(ddlSearchorg_WaitApprove.SelectedValue), int.Parse(ddlSearch_rdept_WaitApprove.SelectedValue));
                //getStatusDocList(ddlStatusDoc);

                break;

            case "cmdCancelSearchWaitApproveOTMonth":
                setActiveTab("docWaitApprove", 0, 0, 0, 0, 2, 0, 0);

                break;

            case "cmdCancelSearchIndexOTMonth":
                setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);
                break;

            case "cmdSearchDetailOTMonth":


                data_overtime data_u0_searchotmonth = new data_overtime();
                ovt_search_otmonth_detail u0_searchotmonth = new ovt_search_otmonth_detail();
                data_u0_searchotmonth.ovt_search_otmonth_list = new ovt_search_otmonth_detail[1];

                u0_searchotmonth.cemp_idx = _emp_idx;
                u0_searchotmonth.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                u0_searchotmonth.emp_type = 2;
                u0_searchotmonth.emp_idx = int.Parse(ddlSearchemp.SelectedValue);
                u0_searchotmonth.month_idx = int.Parse(ddlSearchMonth.SelectedValue);
                u0_searchotmonth.year_idx = int.Parse(ddlSearchYear.SelectedValue);
                u0_searchotmonth.org_idx = int.Parse(ddlSearchorg.SelectedValue);
                u0_searchotmonth.rdept_idx = int.Parse(ddlSearchrdept.SelectedValue);
                u0_searchotmonth.rsec_idx = int.Parse(ddlSearchrsec.SelectedValue);
                u0_searchotmonth.emp_code = txtSearchEmpcode.Text;
                u0_searchotmonth.costcenter = txtSearchCostcenter.Text;
                u0_searchotmonth.staidx = int.Parse(ddlStatusDoc.SelectedValue);


                data_u0_searchotmonth.ovt_search_otmonth_list[0] = u0_searchotmonth;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_searchotmonth));
                data_u0_searchotmonth = callServicePostOvertime(_urlGetSearchOTMonthIndex, data_u0_searchotmonth);
                //
                if (data_u0_searchotmonth.return_code == 0)
                {
                    ViewState["Vs_DetailOT"] = data_u0_searchotmonth.ovt_search_otmonth_list;
                    setGridData(GvDetailOTMont, ViewState["Vs_DetailOT"]);
                }
                else
                {
                    ViewState["Vs_DetailOT"] = null;
                    setGridData(GvDetailOTMont, ViewState["Vs_DetailOT"]);
                }

                break;
            case "cmdSearchWaitApprove":


                data_overtime data_u0_searchwaitotmonth = new data_overtime();
                ovt_search_otmonth_detail u0_searchwaitotmonth = new ovt_search_otmonth_detail();
                data_u0_searchwaitotmonth.ovt_search_otmonth_list = new ovt_search_otmonth_detail[1];

                u0_searchwaitotmonth.cemp_idx = _emp_idx;
                u0_searchwaitotmonth.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                u0_searchwaitotmonth.emp_type = 2;
                u0_searchwaitotmonth.emp_idx = int.Parse(ddlSearch_emp_WaitApprove.SelectedValue);
                u0_searchwaitotmonth.month_idx = int.Parse(ddlSearchMonth_WaitApprove.SelectedValue);
                u0_searchwaitotmonth.year_idx = int.Parse(ddlSearchYear_WaitApprove.SelectedValue);
                u0_searchwaitotmonth.org_idx = int.Parse(ddlSearchorg_WaitApprove.SelectedValue);
                u0_searchwaitotmonth.rdept_idx = int.Parse(ddlSearch_rdept_WaitApprove.SelectedValue);
                u0_searchwaitotmonth.rsec_idx = int.Parse(ddlSearch_rsec_WaitApprove.SelectedValue);
                u0_searchwaitotmonth.emp_code = txt_SearchEmpCode_WaitApprove.Text;
                u0_searchwaitotmonth.costcenter = txt_SearchCost_WaitApprove.Text;
                //u0_searchwaitotmonth.staidx = int.Parse(ddlStatusDoc.SelectedValue);


                data_u0_searchwaitotmonth.ovt_search_otmonth_list[0] = u0_searchwaitotmonth;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_searchwaitotmonth));
                data_u0_searchwaitotmonth = callServicePostOvertime(_urlGetSearchWaitApproveOTMonth, data_u0_searchwaitotmonth);
                //
                if (data_u0_searchwaitotmonth.return_code == 0)
                {
                    ViewState["Vs_WaitApproveDetailOTMonth"] = data_u0_searchwaitotmonth.ovt_search_otmonth_list;
                    setGridData(GvWaitDetailOTMonth, ViewState["Vs_WaitApproveDetailOTMonth"]);
                }
                else
                {
                    ViewState["Vs_WaitApproveDetailOTMonth"] = null;
                    setGridData(GvWaitDetailOTMonth, ViewState["Vs_WaitApproveDetailOTMonth"]);
                }

                break;
            case "cmdViewAdminOTDay":
                setActiveTab("docDetailAdminOTDay", int.Parse(cmdArg.ToString()), 0, 0, 0, 1, 0, 1);
                break;

            case "cmdViewEmployeeDetailOTDay":

                string[] arg11 = new string[1];
                arg11 = e.CommandArgument.ToString().Split(';');
                int _u0_docday_idx = int.Parse(arg11[0]);
                int _u1_docday_idx_ = int.Parse(arg11[1]);

                setActiveTab("docDetailAdminOTDay", _u0_docday_idx, 0, 1, 0, 2, 0, _u1_docday_idx_);
                //setActiveTab("docDetailEmployeeOTDay", _u0_docday_idx, 0, 1, 0, 2, 0, _u1_docday_idx_); 
                break;

            case "cmdApproveOTDayADminCreate":

                setActiveTab("docApproveOTDayADminCreate", 0, int.Parse(cmdArg.ToString()), 0, 0, 0, 0, 0);

                break;

            case "cmdSaveApproveOTDayAdminCreate":

                string[] arg_headapprove_ot = new string[1];
                arg_headapprove_ot = e.CommandArgument.ToString().Split(';');
                int _decision_idx_headapprove_ot = int.Parse(arg_headapprove_ot[0]);
                //string _decision_name_headapprove_ot = arg_headapprove_ot[1];

                data_overtime data_ovt_u1doc_otday = new data_overtime();

                //update u1 document ot month
                var _u1doc_otday_approve = new ovt_u1doc_otday_detail[GvWaitApproveOTDayAdminCrate.Rows.Count];
                int sum_approve_ = 0;
                int count_approve_ = 0;
                string u0_sentmail = "";

                foreach (GridViewRow gv_row in GvWaitApproveOTDayAdminCrate.Rows)
                {

                    data_ovt_u1doc_otday.ovt_u1doc_otday_list = new ovt_u1doc_otday_detail[1];

                    CheckBox chkheadapprove_otday = (CheckBox)gv_row.FindControl("chkheadapprove_otday");
                    Label lbl_u0_docday_idx_waitapprove = (Label)gv_row.FindControl("lbl_u0_docday_idx_waitapprove");
                    Label lbl_u1_docday_idx_waitapprove = (Label)gv_row.FindControl("lbl_u1_docday_idx_waitapprove");

                    Label lbl_staidx_detail_otday_waitapprove = (Label)gv_row.FindControl("lbl_staidx_detail_otday_waitapprove");
                    Label lbl_m0_node_idx_detail_otday_waitapprove = (Label)gv_row.FindControl("lbl_m0_node_idx_detail_otday_waitapprove");
                    Label lbl_m0_actor_idx_detail_otday_waitapprove = (Label)gv_row.FindControl("lbl_m0_actor_idx_detail_otday_waitapprove");
                    TextBox txt_comment_waitapprove = (TextBox)gv_row.FindControl("txt_comment_waitapprove");
                    //TextBox txt_remarkhead_shift_waitapprove = (TextBox)gv_row.FindControl("txt_remarkhead_shift_waitapprove");
                    //TextBox txt_remarkhr_shift_waitapprove = (TextBox)gv_row.FindControl("txt_remarkhr_shift_waitapprove");
                    //TextBox txt_remarkadmin_shift_waitapprove = (TextBox)gv_row.FindControl("txt_remarkadmin_shift_waitapprove");

                    //Label lb_sum_counttotal = (Label)gv_row.FindControl("lb_sum_counttotal");

                    if (chkheadapprove_otday.Checked == true)
                    {

                        _u1doc_otday_approve[count_approve_] = new ovt_u1doc_otday_detail();
                        //_u1doc_otmonth_approve[count_approve].emp_code = tbEmpCode_.Text;
                        _u1doc_otday_approve[count_approve_].u0_docday_idx = int.Parse(lbl_u0_docday_idx_waitapprove.Text);
                        _u1doc_otday_approve[count_approve_].u1_docday_idx = int.Parse(lbl_u1_docday_idx_waitapprove.Text);
                        _u1doc_otday_approve[count_approve_].m0_node_idx = int.Parse(lbl_m0_node_idx_detail_otday_waitapprove.Text);
                        _u1doc_otday_approve[count_approve_].m0_actor_idx = int.Parse(lbl_m0_actor_idx_detail_otday_waitapprove.Text);
                        _u1doc_otday_approve[count_approve_].staidx = int.Parse(lbl_staidx_detail_otday_waitapprove.Text);
                        _u1doc_otday_approve[count_approve_].decision = _decision_idx_headapprove_ot;
                        _u1doc_otday_approve[count_approve_].cemp_idx = _emp_idx;
                        _u1doc_otday_approve[count_approve_].comment = txt_comment_waitapprove.Text;


                        //_u1doc_otday_approve[count_approve_].comment_head = txt_remarkhead_shift_waitapprove.Text;
                        //_u1doc_otday_approve[count_approve_].comment_hr = txt_remarkhr_shift_waitapprove.Text;
                        //_u1doc_otday_approve[count_approve_].comment_admin = txt_remarkadmin_shift_waitapprove.Text;


                        sum_approve_ = sum_approve_ + 1;
                        count_approve_++;

                    }


                }
                //update u1 document ot month

                if (sum_approve_ == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาเลือกรายการ ก่อนทำการบันทึก ---');", true);
                    break;
                }
                else
                {
                    data_ovt_u1doc_otday.ovt_u1doc_otday_list = _u1doc_otday_approve;
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ovt_u1doc_otday));

                    data_ovt_u1doc_otday = callServicePostOvertime(_urlSetApproveOTAdminCreate, data_ovt_u1doc_otday);

                    ////getCountWaitDetailApprove();
                }

                //litDebug.Text = ViewState["Vs_check_tab_noidx"].ToString();
                getCountWaitDetailApproveOTAdminCreate();
                setActiveTab("docApproveOTDayADminCreate", 0, int.Parse(ViewState["Vs_check_tab_noidx"].ToString()), 0, 0, 0, 0, 0);

                break;
            case "cmdDetailAdminCreateOTDay":
                setActiveTab("docDetailAdminOTDay", 0, 0, 0, 0, int.Parse(cmdArg.ToString()), 0, 0);
                break;
            case "cmdDetailAdminCreateEmployeee":
                setActiveTab("docDetailAdminOTDay", 0, 0, 0, 0, int.Parse(cmdArg.ToString()), 0, 0);
                break;

            case "cmdSearchDayCreateOT":

                data_overtime data_search_date = new data_overtime();
                ovt_otday_detail u0_searchdate = new ovt_otday_detail();
                data_search_date.ovt_otday_list = new ovt_otday_detail[1];

                u0_searchdate.cemp_idx = _emp_idx;
                u0_searchdate.search_date = txtDateStart_create.Text;

                data_search_date.ovt_otday_list[0] = u0_searchdate;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_date));
                data_search_date = callServicePostOvertime(_urlGetDetailOTDayAdmin, data_search_date);

                //txtDateStart_create.Text = "TOEI";

                //txtSearchDateStart_createHidden.Value.ToString() = txtDateStart_create.Text;
                getTimeStartOTDay(txtDateStart_create.Text);
                getTimeEndOTDay(txtDateStart_create.Text);

                if (data_search_date.return_code == 0 && ViewState["Vs_TimeStartOTDay"] != null && ViewState["Vs_TimeEndOTDay"] != null)
                {

                    getTimeStartOTDay_Scan(txtDateStart_create.Text);
                    getTimeEndOTDay_Scan(txtDateStart_create.Text);

                    ViewState["Vs_ViewDetailCreateOTDay"] = data_search_date.ovt_otday_list;
                    _PanelDetailCreateOTDay.Visible = true;
                    GvDetailCreateOTDay.Visible = true;
                    setGridData(GvDetailCreateOTDay, ViewState["Vs_ViewDetailCreateOTDay"]);

                }
                else
                {

                    ViewState["Vs_ViewDetailCreateOTDay"] = null;
                    ////getTimeStartOTDay(txtDateStart_create.Text);
                    ////getTimeEndOTDay(txtDateStart_create.Text);

                    getTimeStartOTDay_Scan(txtDateStart_create.Text);
                    getTimeEndOTDay_Scan(txtDateStart_create.Text);

                    _PanelDetailCreateOTDay.Visible = false;
                    GvDetailCreateOTDay.Visible = false;
                    setGridData(GvDetailCreateOTDay, ViewState["Vs_ViewDetailCreateOTDay"]);

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                    //txtDateStart_create.Text = string.Empty;
                    break;


                }

                break;

        }

    }
    //endbtn

    #endregion event command

    #region Changed
    protected void onSelectedCheckChanged(object sender, EventArgs e)
    {
        var cbName = (CheckBox)sender;

        //var chkName = (CheckBoxList)sender;

        switch (cbName.ID)
        {


            case "chkheadapprove_allotday":
                int chack_all = 0;
                if (chkheadapprove_allotday.Checked)
                {
                    foreach (GridViewRow row in GvWaitApproveOTDayAdminCrate.Rows)
                    {
                        CheckBox chkheadapprove_otday = (CheckBox)row.FindControl("chkheadapprove_otday");

                        chkheadapprove_otday.Checked = true;
                        chack_all++;
                    }
                }
                else
                {
                    foreach (GridViewRow row in GvWaitApproveOTDayAdminCrate.Rows)
                    {
                        CheckBox chkheadapprove_otday = (CheckBox)row.FindControl("chkheadapprove_otday");

                        chkheadapprove_otday.Checked = false;
                        chack_all++;
                    }
                }

                break;

            case "chk_allotbefore":
                int count_all = 0;
                if (chk_allotbefore.Checked)
                {
                    foreach (GridViewRow row in GvViewWaitApproveOTMont.Rows)
                    {
                        CheckBox chkstatus_otbefore = (CheckBox)row.FindControl("chkstatus_otbefore");
                        chkstatus_otbefore.Checked = true;
                        //Label txtot_before_approve = (Label)row.FindControl("txtot_before_approve");
                        //Label txtot_after_approve = (Label)row.FindControl("txtot_after_approve");
                        //Label txtot_holiday_approve = (Label)row.FindControl("txtot_holiday_approve");

                        DropDownList ddl_timebefore_approve = (DropDownList)row.FindControl("ddl_timebefore_approve");
                        DropDownList ddl_timeafter_approve = (DropDownList)row.FindControl("ddl_timeafter_approve");
                        DropDownList ddl_timeholiday_approve = (DropDownList)row.FindControl("ddl_timeholiday_approve");


                        if (ddl_timebefore_approve.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timebefore_approve.SelectedValue) >= 0 && chkstatus_otbefore.Checked == true)
                            {
                                totactual_otbefore += Convert.ToDecimal(ddl_timebefore_approve.SelectedValue);
                                Label lit_total_hoursot_before_approve = (Label)GvViewWaitApproveOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_approve");
                                lit_total_hoursot_before_approve.Text = String.Format("{0:N2}", totactual_otbefore);
                            }
                        }

                        ////tot_actual_otmonth += Convert.ToDecimal(value);

                        ////Label lit_total_hoursmonth = (Label)GvViewEditDetailOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursmonth_edit");

                        ////lit_total_hoursmonth.Text = String.Format("{0:N2}", tot_actual_otmonth);
                        // GvCreateOTMonth.FooterRow.Cells[5] = FontBoldWeight.Bold;

                        count_all++;

                    }
                }
                else
                {
                    foreach (GridViewRow row in GvViewWaitApproveOTMont.Rows)
                    {
                        CheckBox chkstatus_otbefore = (CheckBox)row.FindControl("chkstatus_otbefore");
                        chkstatus_otbefore.Checked = false;
                        //Label txtot_before_approve = (Label)row.FindControl("txtot_before_approve");
                        //Label txtot_after_approve = (Label)row.FindControl("txtot_after_approve");
                        //Label txtot_holiday_approve = (Label)row.FindControl("txtot_holiday_approve");


                        Label lit_total_hoursot_before_approve = (Label)GvViewWaitApproveOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_approve");
                        lit_total_hoursot_before_approve.Text = String.Format("{0:N2}", "0.00");

                    }
                }


                break;

            case "chk_date_ot":

                //litDebug.Text = "5";
                int count_ = 0;
                linkBtnTrigger(btnSaveOTMonth);
                foreach (GridViewRow row in GvCreateOTMonth.Rows)
                {
                    CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");
                    UpdatePanel Update_Timestart = (UpdatePanel)row.FindControl("Update_Timestart");
                    UpdatePanel Update_Timeend = (UpdatePanel)row.FindControl("Update_Timeend");
                    TextBox txt_job = (TextBox)row.FindControl("txt_job");
                    TextBox txt_timestart_job = (TextBox)row.FindControl("txt_timestart_job");
                    TextBox txt_timeend_job = (TextBox)row.FindControl("txt_timeend_job");
                    TextBox txt_sum_hour = (TextBox)row.FindControl("txt_sum_hour");
                    TextBox txt_head_set = (TextBox)row.FindControl("txt_head_set");
                    TextBox txt_remark = (TextBox)row.FindControl("txt_remark");

                    Label txtot_before = (Label)row.FindControl("txtot_before");
                    Label lblot_before = (Label)row.FindControl("lblot_before");

                    Label txtot_after = (Label)row.FindControl("txtot_after");
                    Label lblot_after = (Label)row.FindControl("lblot_after");

                    Label txtot_holiday = (Label)row.FindControl("txtot_holiday");
                    Label lblot_holiday = (Label)row.FindControl("lblot_holiday");

                    Panel color_otbefore = (Panel)row.FindControl("color_otbefore");
                    Panel color_otafter = (Panel)row.FindControl("color_otafter");
                    Panel color_otholiday = (Panel)row.FindControl("color_otholiday");


                    DropDownList ddl_timebefore = (DropDownList)row.FindControl("ddl_timebefore");
                    DropDownList ddl_timeafter = (DropDownList)row.FindControl("ddl_timeafter");
                    DropDownList ddl_timeholiday = (DropDownList)row.FindControl("ddl_timeholiday");

                    ////decimal totactual_otbefore = 0;
                    ////decimal totactual_otafter = 0;
                    ////decimal totactual_otholiday = 0;

                    ////decimal value = Convert.ToDecimal(txt_sum_hour.Text);

                    if (chk_date_ot.Checked)
                    {

                        //litDebug.Text += "1";

                        linkBtnTrigger(btnSaveOTMonth);

                        txt_job.Visible = true;
                        txt_head_set.Visible = true;
                        txt_remark.Visible = true;
                        txt_sum_hour.Visible = true;

                        row.BackColor = System.Drawing.Color.Bisque;

                        ////color_otbefore.BackColor = System.Drawing.Color.Orange;
                        ////color_otafter.BackColor = System.Drawing.Color.Orange;
                        ////color_otholiday.BackColor = System.Drawing.Color.Orange;

                        //tot_actual_otmonth += Convert.ToDecimal(value);
                        //Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                        //lit_total_hoursmonth.Text = String.Format("{0:N2}", tot_actual_otmonth);

                        if (ddl_timebefore.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timebefore.SelectedValue) >= 0.5)
                            {
                                totactual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                                Label lit_total_hoursot_before = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");
                                lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                            }
                        }

                        if (ddl_timeafter.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeafter.SelectedValue) >= 0.5)
                            {
                                totactual_otafter += Convert.ToDecimal(ddl_timeafter.SelectedValue);
                                Label lit_total_hoursot_after = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");
                                lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                            }
                        }

                        if (ddl_timeholiday.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeholiday.SelectedValue) >= 0.5)
                            {
                                totactual_otholiday += Convert.ToDecimal(ddl_timeholiday.SelectedValue);
                                Label lit_total_hoursot_holiday = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");
                                lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);
                            }
                        }

                        // GvCreateOTMonth.FooterRow.Cells[5] = FontBoldWeight.Bold;

                        count_++;
                    }
                    else
                    {



                        ////litDebug.Text += "2";
                        txt_job.Text = string.Empty;
                        txt_head_set.Text = string.Empty;
                        txt_remark.Text = string.Empty;


                        txt_job.Visible = false;
                        txt_head_set.Visible = false;
                        txt_remark.Visible = false;
                        txt_sum_hour.Visible = false;

                        ////color_otbefore.BackColor = System.Drawing.Color.Transparent;
                        ////color_otafter.BackColor = System.Drawing.Color.Transparent;
                        ////color_otholiday.BackColor = System.Drawing.Color.Transparent;

                        row.BackColor = System.Drawing.Color.Transparent;

                    }


                    //not check ot month
                    if (count_ == 0)
                    {
                        ////litDebug.Text += "3";
                        ////Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                        ////lit_total_hoursmonth.Text = String.Format("{0:N2}", "0.00");

                        Label lit_total_hoursot_before = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");
                        lit_total_hoursot_before.Text = String.Format("{0:N2}", "0.00");

                        Label lit_total_hoursot_after = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");
                        lit_total_hoursot_after.Text = String.Format("{0:N2}", "0.00");

                        Label lit_total_hoursot_holiday = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");
                        lit_total_hoursot_holiday.Text = String.Format("{0:N2}", "0.00");


                    }
                    //litDebug.Text = count_.ToString();

                }
                break;
            case "chk_date_ot_edit":

                //litDebug.Text = "5";
                int count_edit = 0;

                int count_edit_holiday = 0;
                linkBtnTrigger(btnSaveEditOTMonth);
                foreach (GridViewRow row in GvViewEditDetailOTMont.Rows)
                {
                    CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot_edit");


                    TextBox txt_job = (TextBox)row.FindControl("txt_job_edit");
                    //TextBox txt_timestart_job = (TextBox)row.FindControl("txt_timestart_job");
                    //TextBox txt_timeend_job = (TextBox)row.FindControl("txt_timeend_job");
                    //TextBox txt_sum_hour = (TextBox)row.FindControl("txt_sum_hour");
                    TextBox txt_head_set = (TextBox)row.FindControl("txt_head_set_edit");
                    TextBox txt_remark = (TextBox)row.FindControl("txt_remark_edit");

                    Label txtot_before = (Label)row.FindControl("txtot_before_edit");
                    Label lblot_before = (Label)row.FindControl("lblot_before_edit");

                    Label txtot_after = (Label)row.FindControl("txtot_after_edit");
                    Label lblot_after = (Label)row.FindControl("lblot_after_edit");

                    Label txtot_holiday = (Label)row.FindControl("txtot_holiday_edit");
                    Label lblot_holiday = (Label)row.FindControl("lblot_holiday_edit");

                    //Panel color_otbefore = (Panel)row.FindControl("color_otbefore");
                    //Panel color_otafter = (Panel)row.FindControl("color_otafter");
                    //Panel color_otholiday = (Panel)row.FindControl("color_otholiday");


                    DropDownList ddl_timebefore = (DropDownList)row.FindControl("ddl_timebefore_edit");
                    DropDownList ddl_timeafter = (DropDownList)row.FindControl("ddl_timeafter_edit");
                    DropDownList ddl_timeholiday = (DropDownList)row.FindControl("ddl_timeholiday_edit");

                    ////decimal totactual_otbefore = 0;
                    ////decimal totactual_otafter = 0;
                    ////decimal totactual_otholiday = 0;

                    ////decimal value = Convert.ToDecimal(txt_sum_hour.Text);

                    if (chk_date_ot.Checked)
                    {

                        //litDebug.Text += "1";

                        linkBtnTrigger(btnSaveOTMonth);

                        txt_job.Visible = true;
                        txt_head_set.Visible = true;
                        txt_remark.Visible = true;
                        //txt_sum_hour.Visible = true;

                        row.BackColor = System.Drawing.Color.Bisque;

                        ////color_otbefore.BackColor = System.Drawing.Color.Orange;
                        ////color_otafter.BackColor = System.Drawing.Color.Orange;
                        ////color_otholiday.BackColor = System.Drawing.Color.Orange;

                        //tot_actual_otmonth += Convert.ToDecimal(value);
                        //Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                        //lit_total_hoursmonth.Text = String.Format("{0:N2}", tot_actual_otmonth);

                        if (ddl_timebefore.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timebefore.SelectedValue) >= 0)
                            {
                                totactual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                                Label lit_total_hoursot_before = (Label)GvViewEditDetailOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_edit");
                                lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);

                                //litDebug.Text = String.Format("{0:N2}", totactual_otbefore);
                            }
                        }

                        if (ddl_timeafter.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeafter.SelectedValue) >= 0.5)
                            {
                                totactual_otafter += Convert.ToDecimal(ddl_timeafter.SelectedValue);
                                Label lit_total_hoursot_after = (Label)GvViewEditDetailOTMont.FooterRow.Cells[6].FindControl("lit_total_hoursot_after_edit");
                                lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                            }
                        }

                        if (ddl_timeholiday.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeholiday.SelectedValue) >= 0.5)
                            {
                                totactual_otholiday += Convert.ToDecimal(ddl_timeholiday.SelectedValue);
                                Label lit_total_hoursot_holiday = (Label)GvViewEditDetailOTMont.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday_edit");
                                lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);
                            }

                            count_edit_holiday++;
                        }

                        // GvCreateOTMonth.FooterRow.Cells[5] = FontBoldWeight.Bold;
                        count_edit++;

                    }
                    else
                    {


                        ////litDebug.Text += "2";
                        txt_job.Text = string.Empty;
                        txt_head_set.Text = string.Empty;
                        txt_remark.Text = string.Empty;


                        txt_job.Visible = false;
                        txt_head_set.Visible = false;
                        txt_remark.Visible = false;
                        //txt_sum_hour.Visible = false;

                        ////color_otbefore.BackColor = System.Drawing.Color.Transparent;
                        ////color_otafter.BackColor = System.Drawing.Color.Transparent;
                        ////color_otholiday.BackColor = System.Drawing.Color.Transparent;

                        row.BackColor = System.Drawing.Color.Transparent;

                    }


                    //not check ot month
                    if (count_edit == 0)
                    {
                        //litDebug.Text += "3";
                        ////Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                        ////lit_total_hoursmonth.Text = String.Format("{0:N2}", "0.00");

                        Label lit_total_hoursot_before = (Label)GvViewEditDetailOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_edit");
                        lit_total_hoursot_before.Text = String.Format("{0:N2}", "0.00");

                        Label lit_total_hoursot_after = (Label)GvViewEditDetailOTMont.FooterRow.Cells[6].FindControl("lit_total_hoursot_after_edit");
                        lit_total_hoursot_after.Text = String.Format("{0:N2}", "0.00");





                    }

                    if (count_edit_holiday == 0)
                    {
                        Label lit_total_hoursot_holiday = (Label)GvViewEditDetailOTMont.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday_edit");
                        lit_total_hoursot_holiday.Text = String.Format("{0:N2}", "0.00");
                    }
                    //litDebug1.Text = count_edit.ToString();

                }
                break;

            case "chkstatus_otbefore":
                int count_status_otbefore = 0;

                HiddenField hfM0NodeIDX = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfM0NodeIDX");
                HiddenField hfM0ActoreIDX = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfM0ActoreIDX");


                foreach (GridViewRow row in GvViewWaitApproveOTMont.Rows)
                {
                    CheckBox chkstatus_otbefore = (CheckBox)row.FindControl("chkstatus_otbefore");
                    //Label txtot_before_approve = (Label)row.FindControl("txtot_before_approve");
                    //Label txtot_after_approve = (Label)row.FindControl("txtot_after_approve");
                    //Label txtot_holiday_approve = (Label)row.FindControl("txtot_holiday_approve");

                    DropDownList ddl_timebefore_approve = (DropDownList)row.FindControl("ddl_timebefore_approve");
                    DropDownList ddl_timeafter_approve = (DropDownList)row.FindControl("ddl_timeafter_approve");
                    DropDownList ddl_timeholiday_approve = (DropDownList)row.FindControl("ddl_timeholiday_approve");


                    if (chkstatus_otbefore.Checked)
                    {

                        if (ddl_timebefore_approve.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timebefore_approve.SelectedValue) >= 0)
                            {
                                totactual_otbefore += Convert.ToDecimal(ddl_timebefore_approve.SelectedValue);
                                Label lit_total_hoursot_before_approve = (Label)GvViewWaitApproveOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_approve");
                                lit_total_hoursot_before_approve.Text = String.Format("{0:N2}", totactual_otbefore);
                            }
                        }

                        count_status_otbefore++;
                    }



                }

                if (count_status_otbefore == 0)
                {

                    Label lit_total_hoursot_before_approve = (Label)GvViewWaitApproveOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_approve");
                    lit_total_hoursot_before_approve.Text = String.Format("{0:N2}", "0.00");
                }

                break;


        }
    }


    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {

            case "ddl_timebefore_approve":

                int count_beforeapprove = 0;
                foreach (GridViewRow row in GvViewWaitApproveOTMont.Rows)
                {
                    //CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");

                    DropDownList ddl_timebefore_approve = (DropDownList)row.FindControl("ddl_timebefore_approve");
                    DropDownList ddl_timeafter_approve = (DropDownList)row.FindControl("ddl_timeafter_approve");
                    DropDownList ddl_timeholiday_approve = (DropDownList)row.FindControl("ddl_timeholiday_approve");

                    CheckBox chkstatus_otbefore = (CheckBox)row.FindControl("chkstatus_otbefore");


                    if (ddl_timebefore_approve.SelectedValue != "")
                    {
                        if (Convert.ToDouble(ddl_timebefore_approve.SelectedValue) >= 0)
                        {
                            totactual_otbefore += Convert.ToDecimal(ddl_timebefore_approve.SelectedValue);
                            Label lit_total_hoursot_before = (Label)GvViewWaitApproveOTMont.FooterRow.Cells[4].FindControl("lit_total_hoursot_before_approve");
                            lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                        }


                    }

                    //if (ddl_timeafter_approve.SelectedValue != "")
                    //{
                    //    if (Convert.ToDouble(ddl_timeafter_approve.SelectedValue) >= 0)
                    //    {
                    //        totactual_otafter += Convert.ToDecimal(ddl_timeafter_approve.SelectedValue);
                    //        Label lit_total_hoursot_after = (Label)GvViewWaitApproveOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_after_approve");
                    //        lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                    //    }
                    //}

                    //if (ddl_timeholiday_approve.SelectedValue != "")
                    //{
                    //    if (Convert.ToDouble(ddl_timeholiday_approve.SelectedValue) >= 0)
                    //    {
                    //        totactual_otholiday += Convert.ToDecimal(ddl_timeholiday_approve.SelectedValue);
                    //        Label lit_total_hoursot_holiday = (Label)GvViewWaitApproveOTMont.FooterRow.Cells[6].FindControl("lit_total_hoursot_holiday_approve");
                    //        lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);
                    //    }
                    //}

                    // GvCreateOTMonth.FooterRow.Cells[5] = FontBoldWeight.Bold;

                    count_beforeapprove++;
                }

                break;

            case "ddl_timeafter_approve":

                int count_afterapprove = 0;
                foreach (GridViewRow row in GvViewWaitApproveOTMont.Rows)
                {
                    //CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");

                    DropDownList ddl_timebefore_approve = (DropDownList)row.FindControl("ddl_timebefore_approve");
                    DropDownList ddl_timeafter_approve = (DropDownList)row.FindControl("ddl_timeafter_approve");
                    DropDownList ddl_timeholiday_approve = (DropDownList)row.FindControl("ddl_timeholiday_approve");


                    //if (ddl_timebefore_approve.SelectedValue != "")
                    //{
                    //    if (Convert.ToDouble(ddl_timebefore_approve.SelectedValue) >= 0)
                    //    {
                    //        totactual_otbefore += Convert.ToDecimal(ddl_timebefore_approve.SelectedValue);
                    //        Label lit_total_hoursot_before = (Label)GvViewWaitApproveOTMont.FooterRow.Cells[4].FindControl("lit_total_hoursot_before_approve");
                    //        lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                    //    }
                    //}

                    if (ddl_timeafter_approve.SelectedValue != "")
                    {
                        if (Convert.ToDouble(ddl_timeafter_approve.SelectedValue) >= 0)
                        {
                            totactual_otafter += Convert.ToDecimal(ddl_timeafter_approve.SelectedValue);
                            Label lit_total_hoursot_after = (Label)GvViewWaitApproveOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_after_approve");
                            lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                        }
                    }

                    //if (ddl_timeholiday_approve.SelectedValue != "")
                    //{
                    //    if (Convert.ToDouble(ddl_timeholiday_approve.SelectedValue) >= 0)
                    //    {
                    //        totactual_otholiday += Convert.ToDecimal(ddl_timeholiday_approve.SelectedValue);
                    //        Label lit_total_hoursot_holiday = (Label)GvViewWaitApproveOTMont.FooterRow.Cells[6].FindControl("lit_total_hoursot_holiday_approve");
                    //        lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);
                    //    }
                    //}

                    // GvCreateOTMonth.FooterRow.Cells[5] = FontBoldWeight.Bold;

                    count_afterapprove++;
                }
                break;

            case "ddl_timeholiday_approve":

                int count_holidayapprove = 0;
                foreach (GridViewRow row in GvViewWaitApproveOTMont.Rows)
                {
                    //CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");

                    DropDownList ddl_timebefore_approve = (DropDownList)row.FindControl("ddl_timebefore_approve");
                    DropDownList ddl_timeafter_approve = (DropDownList)row.FindControl("ddl_timeafter_approve");
                    DropDownList ddl_timeholiday_approve = (DropDownList)row.FindControl("ddl_timeholiday_approve");


                    //if (ddl_timebefore_approve.SelectedValue != "")
                    //{
                    //    if (Convert.ToDouble(ddl_timebefore_approve.SelectedValue) >= 0)
                    //    {
                    //        totactual_otbefore += Convert.ToDecimal(ddl_timebefore_approve.SelectedValue);
                    //        Label lit_total_hoursot_before = (Label)GvViewWaitApproveOTMont.FooterRow.Cells[4].FindControl("lit_total_hoursot_before_approve");
                    //        lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                    //    }
                    //}

                    //if (ddl_timeafter_approve.SelectedValue != "")
                    //{
                    //    if (Convert.ToDouble(ddl_timeafter_approve.SelectedValue) >= 0)
                    //    {
                    //        totactual_otafter += Convert.ToDecimal(ddl_timeafter_approve.SelectedValue);
                    //        Label lit_total_hoursot_after = (Label)GvViewWaitApproveOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_after_approve");
                    //        lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                    //    }
                    //}

                    if (ddl_timeholiday_approve.SelectedValue != "")
                    {
                        if (Convert.ToDouble(ddl_timeholiday_approve.SelectedValue) >= 0)
                        {
                            totactual_otholiday += Convert.ToDecimal(ddl_timeholiday_approve.SelectedValue);
                            Label lit_total_hoursot_holiday = (Label)GvViewWaitApproveOTMont.FooterRow.Cells[6].FindControl("lit_total_hoursot_holiday_approve");
                            lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);
                        }
                    }

                    // GvCreateOTMonth.FooterRow.Cells[5] = FontBoldWeight.Bold;

                    count_holidayapprove++;
                }

                break;

            case "ddl_timebefore":

                int count_before = 0;
                foreach (GridViewRow row in GvCreateOTMonth.Rows)
                {
                    CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");

                    DropDownList ddl_timebefore = (DropDownList)row.FindControl("ddl_timebefore");
                    DropDownList ddl_timeafter = (DropDownList)row.FindControl("ddl_timeafter");
                    DropDownList ddl_timeholiday = (DropDownList)row.FindControl("ddl_timeholiday");

                    if (chk_date_ot.Checked)
                    {
                        if (ddl_timebefore.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timebefore.SelectedValue) >= 0)
                            {
                                totactual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                                Label lit_total_hoursot_before = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");
                                lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                            }
                        }

                        //if (ddl_timeafter.SelectedValue != "")
                        //{
                        //    if (Convert.ToDouble(ddl_timeafter.SelectedValue) >= 0.5)
                        //    {
                        //        totactual_otafter += Convert.ToDecimal(ddl_timeafter.SelectedValue);
                        //        Label lit_total_hoursot_after = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");
                        //        lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                        //    }
                        //}

                        //if (ddl_timeholiday.SelectedValue != "")
                        //{
                        //    if (Convert.ToDouble(ddl_timeholiday.SelectedValue) >= 0.5)
                        //    {
                        //        totactual_otholiday += Convert.ToDecimal(ddl_timeholiday.SelectedValue);
                        //        Label lit_total_hoursot_holiday = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");
                        //        lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);
                        //    }
                        //}

                        // GvCreateOTMonth.FooterRow.Cells[5] = FontBoldWeight.Bold;

                        count_before++;
                    }

                }

                //not check ot month
                if (count_before == 0)
                {
                    ////litDebug.Text += "3";
                    ////Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                    ////lit_total_hoursmonth.Text = String.Format("{0:N2}", "0.00");

                    Label lit_total_hoursot_before = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");
                    lit_total_hoursot_before.Text = String.Format("{0:N2}", "0.00");

                    //Label lit_total_hoursot_after = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");
                    //lit_total_hoursot_after.Text = String.Format("{0:N2}", "0.00");

                    //Label lit_total_hoursot_holiday = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");
                    //lit_total_hoursot_holiday.Text = String.Format("{0:N2}", "0.00");


                }

                break;

            case "ddl_timeafter":

                int count_after = 0;
                foreach (GridViewRow row in GvCreateOTMonth.Rows)
                {
                    CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");

                    DropDownList ddl_timebefore = (DropDownList)row.FindControl("ddl_timebefore");
                    DropDownList ddl_timeafter = (DropDownList)row.FindControl("ddl_timeafter");
                    DropDownList ddl_timeholiday = (DropDownList)row.FindControl("ddl_timeholiday");

                    if (chk_date_ot.Checked)
                    {
                        //if (ddl_timebefore.SelectedValue != "")
                        //{
                        //    if (Convert.ToDouble(ddl_timebefore.SelectedValue) >= 0.5)
                        //    {
                        //        totactual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                        //        Label lit_total_hoursot_before = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");
                        //        lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                        //    }
                        //}

                        if (ddl_timeafter.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeafter.SelectedValue) >= 0)
                            {
                                totactual_otafter += Convert.ToDecimal(ddl_timeafter.SelectedValue);
                                Label lit_total_hoursot_after = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");
                                lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                            }
                        }

                        //if (ddl_timeholiday.SelectedValue != "")
                        //{
                        //    if (Convert.ToDouble(ddl_timeholiday.SelectedValue) >= 0.5)
                        //    {
                        //        totactual_otholiday += Convert.ToDecimal(ddl_timeholiday.SelectedValue);
                        //        Label lit_total_hoursot_holiday = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");
                        //        lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);
                        //    }
                        //}

                        // GvCreateOTMonth.FooterRow.Cells[5] = FontBoldWeight.Bold;

                        count_after++;
                    }

                }


                //not check ot month
                if (count_after == 0)
                {
                    ////litDebug.Text += "3";
                    ////Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                    ////lit_total_hoursmonth.Text = String.Format("{0:N2}", "0.00");

                    //Label lit_total_hoursot_before = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");
                    //lit_total_hoursot_before.Text = String.Format("{0:N2}", "0.00");

                    Label lit_total_hoursot_after = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");
                    lit_total_hoursot_after.Text = String.Format("{0:N2}", "0.00");

                    //Label lit_total_hoursot_holiday = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");
                    //lit_total_hoursot_holiday.Text = String.Format("{0:N2}", "0.00");


                }


                break;
            case "ddl_timeholiday":

                int count_holiday = 0;
                foreach (GridViewRow row in GvCreateOTMonth.Rows)
                {
                    CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");

                    DropDownList ddl_timebefore = (DropDownList)row.FindControl("ddl_timebefore");
                    DropDownList ddl_timeafter = (DropDownList)row.FindControl("ddl_timeafter");
                    DropDownList ddl_timeholiday = (DropDownList)row.FindControl("ddl_timeholiday");

                    if (chk_date_ot.Checked)
                    {
                        //if (ddl_timebefore.SelectedValue != "")
                        //{
                        //    if (Convert.ToDouble(ddl_timebefore.SelectedValue) >= 0.5)
                        //    {
                        //        totactual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                        //        Label lit_total_hoursot_before = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");
                        //        lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                        //    }
                        //}

                        //if (ddl_timeafter.SelectedValue != "")
                        //{
                        //    if (Convert.ToDouble(ddl_timeafter.SelectedValue) >= 0.5)
                        //    {
                        //        totactual_otafter += Convert.ToDecimal(ddl_timeafter.SelectedValue);
                        //        Label lit_total_hoursot_after = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");
                        //        lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                        //    }
                        //}

                        if (ddl_timeholiday.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeholiday.SelectedValue) >= 0)
                            {
                                totactual_otholiday += Convert.ToDecimal(ddl_timeholiday.SelectedValue);
                                Label lit_total_hoursot_holiday = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");
                                lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);
                            }
                        }

                        // GvCreateOTMonth.FooterRow.Cells[5] = FontBoldWeight.Bold;

                        count_holiday++;
                    }

                }


                //not check ot month
                if (count_holiday == 0)
                {
                    ////litDebug.Text += "3";
                    ////Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                    ////lit_total_hoursmonth.Text = String.Format("{0:N2}", "0.00");

                    //Label lit_total_hoursot_before = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");
                    //lit_total_hoursot_before.Text = String.Format("{0:N2}", "0.00");

                    //Label lit_total_hoursot_after = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");
                    //lit_total_hoursot_after.Text = String.Format("{0:N2}", "0.00");

                    Label lit_total_hoursot_holiday = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");
                    lit_total_hoursot_holiday.Text = String.Format("{0:N2}", "0.00");


                }

                break;
            case "ddl_timebefore_edit":

                int count_beforeedit = 0;
                foreach (GridViewRow row in GvViewEditDetailOTMont.Rows)
                {
                    CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot_edit");

                    DropDownList ddl_timebefore = (DropDownList)row.FindControl("ddl_timebefore_edit");
                    DropDownList ddl_timeafter = (DropDownList)row.FindControl("ddl_timeafter_edit");
                    DropDownList ddl_timeholiday = (DropDownList)row.FindControl("ddl_timeholiday_edit");

                    if (chk_date_ot.Checked)
                    {
                        if (ddl_timebefore.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timebefore.SelectedValue) >= 0)
                            {
                                totactual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                                Label lit_total_hoursot_before = (Label)GvViewEditDetailOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_edit");
                                lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                            }
                        }

                        //if (ddl_timeafter.SelectedValue != "")
                        //{
                        //    if (Convert.ToDouble(ddl_timeafter.SelectedValue) >= 0.5)
                        //    {
                        //        totactual_otafter += Convert.ToDecimal(ddl_timeafter.SelectedValue);
                        //        Label lit_total_hoursot_after = (Label)GvViewEditDetailOTMont.FooterRow.Cells[6].FindControl("lit_total_hoursot_after_edit");
                        //        lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                        //    }
                        //}

                        //if (ddl_timeholiday.SelectedValue != "")
                        //{
                        //    if (Convert.ToDouble(ddl_timeholiday.SelectedValue) >= 0.5)
                        //    {
                        //        totactual_otholiday += Convert.ToDecimal(ddl_timeholiday.SelectedValue);
                        //        Label lit_total_hoursot_holiday = (Label)GvViewEditDetailOTMont.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday_edit");
                        //        lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);
                        //    }
                        //}

                        // GvCreateOTMonth.FooterRow.Cells[5] = FontBoldWeight.Bold;

                        count_beforeedit++;
                    }

                }


                //not check ot month
                if (count_beforeedit == 0)
                {
                    ////litDebug.Text += "3";
                    ////Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                    ////lit_total_hoursmonth.Text = String.Format("{0:N2}", "0.00");

                    Label lit_total_hoursot_before = (Label)GvViewEditDetailOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_edit");
                    lit_total_hoursot_before.Text = String.Format("{0:N2}", "0.00");

                    //Label lit_total_hoursot_after = (Label)GvViewEditDetailOTMont.FooterRow.Cells[6].FindControl("lit_total_hoursot_after_edit");
                    //lit_total_hoursot_after.Text = String.Format("{0:N2}", "0.00");

                    //Label lit_total_hoursot_holiday = (Label)GvViewEditDetailOTMont.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday_edit");
                    //lit_total_hoursot_holiday.Text = String.Format("{0:N2}", "0.00");


                }


                break;
            case "ddl_timeafter_edit":

                int count_afteredit = 0;
                foreach (GridViewRow row in GvViewEditDetailOTMont.Rows)
                {
                    CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot_edit");

                    DropDownList ddl_timebefore = (DropDownList)row.FindControl("ddl_timebefore_edit");
                    DropDownList ddl_timeafter = (DropDownList)row.FindControl("ddl_timeafter_edit");
                    DropDownList ddl_timeholiday = (DropDownList)row.FindControl("ddl_timeholiday_edit");

                    if (chk_date_ot.Checked)
                    {
                        //if (ddl_timebefore.SelectedValue != "")
                        //{
                        //    if (Convert.ToDouble(ddl_timebefore.SelectedValue) >= 0.5)
                        //    {
                        //        totactual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                        //        Label lit_total_hoursot_before = (Label)GvViewEditDetailOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_edit");
                        //        lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                        //    }
                        //}

                        if (ddl_timeafter.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeafter.SelectedValue) >= 0)
                            {
                                totactual_otafter += Convert.ToDecimal(ddl_timeafter.SelectedValue);
                                Label lit_total_hoursot_after = (Label)GvViewEditDetailOTMont.FooterRow.Cells[6].FindControl("lit_total_hoursot_after_edit");
                                lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                            }
                        }

                        //if (ddl_timeholiday.SelectedValue != "")
                        //{
                        //    if (Convert.ToDouble(ddl_timeholiday.SelectedValue) >= 0.5)
                        //    {
                        //        totactual_otholiday += Convert.ToDecimal(ddl_timeholiday.SelectedValue);
                        //        Label lit_total_hoursot_holiday = (Label)GvViewEditDetailOTMont.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday_edit");
                        //        lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);
                        //    }
                        //}

                        // GvCreateOTMonth.FooterRow.Cells[5] = FontBoldWeight.Bold;

                        count_afteredit++;
                    }

                }


                //not check ot month
                if (count_afteredit == 0)
                {
                    ////litDebug.Text += "3";
                    ////Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                    ////lit_total_hoursmonth.Text = String.Format("{0:N2}", "0.00");

                    //Label lit_total_hoursot_before = (Label)GvViewEditDetailOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_edit");
                    //lit_total_hoursot_before.Text = String.Format("{0:N2}", "0.00");

                    Label lit_total_hoursot_after = (Label)GvViewEditDetailOTMont.FooterRow.Cells[6].FindControl("lit_total_hoursot_after_edit");
                    lit_total_hoursot_after.Text = String.Format("{0:N2}", "0.00");

                    //Label lit_total_hoursot_holiday = (Label)GvViewEditDetailOTMont.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday_edit");
                    //lit_total_hoursot_holiday.Text = String.Format("{0:N2}", "0.00");


                }


                break;
            case "ddl_timeholiday_edit":

                int count_holidayedit = 0;
                foreach (GridViewRow row in GvViewEditDetailOTMont.Rows)
                {
                    CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot_edit");

                    DropDownList ddl_timebefore = (DropDownList)row.FindControl("ddl_timebefore_edit");
                    DropDownList ddl_timeafter = (DropDownList)row.FindControl("ddl_timeafter_edit");
                    DropDownList ddl_timeholiday = (DropDownList)row.FindControl("ddl_timeholiday_edit");

                    if (chk_date_ot.Checked)
                    {
                        //if (ddl_timebefore.SelectedValue != "")
                        //{
                        //    if (Convert.ToDouble(ddl_timebefore.SelectedValue) >= 0.5)
                        //    {
                        //        totactual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                        //        Label lit_total_hoursot_before = (Label)GvViewEditDetailOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_edit");
                        //        lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                        //    }
                        //}

                        //if (ddl_timeafter.SelectedValue != "")
                        //{
                        //    if (Convert.ToDouble(ddl_timeafter.SelectedValue) >= 0.5)
                        //    {
                        //        totactual_otafter += Convert.ToDecimal(ddl_timeafter.SelectedValue);
                        //        Label lit_total_hoursot_after = (Label)GvViewEditDetailOTMont.FooterRow.Cells[6].FindControl("lit_total_hoursot_after_edit");
                        //        lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                        //    }
                        //}

                        if (ddl_timeholiday.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeholiday.SelectedValue) >= 0)
                            {
                                totactual_otholiday += Convert.ToDecimal(ddl_timeholiday.SelectedValue);
                                Label lit_total_hoursot_holiday = (Label)GvViewEditDetailOTMont.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday_edit");
                                lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);
                            }
                        }

                        // GvCreateOTMonth.FooterRow.Cells[5] = FontBoldWeight.Bold;

                        count_holidayedit++;
                    }

                }


                //not check ot month
                if (count_holidayedit == 0)
                {
                    ////litDebug.Text += "3";
                    ////Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                    ////lit_total_hoursmonth.Text = String.Format("{0:N2}", "0.00");

                    //Label lit_total_hoursot_before = (Label)GvViewEditDetailOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_edit");
                    //lit_total_hoursot_before.Text = String.Format("{0:N2}", "0.00");

                    //Label lit_total_hoursot_after = (Label)GvViewEditDetailOTMont.FooterRow.Cells[6].FindControl("lit_total_hoursot_after_edit");
                    //lit_total_hoursot_after.Text = String.Format("{0:N2}", "0.00");

                    Label lit_total_hoursot_holiday = (Label)GvViewEditDetailOTMont.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday_edit");
                    lit_total_hoursot_holiday.Text = String.Format("{0:N2}", "0.00");


                }


                break;

            case "ddlPlacesFilter":

                ////Panel_searchDetailIndex.Visible = false;
                if (ddlPlacesFilter.SelectedValue != "0")
                {

                    data_overtime data_u0_importday_detail = new data_overtime();
                    ovt_u0_document_day_detail[] _item_FilterStatus = (ovt_u0_document_day_detail[])ViewState["Vs_DetailOTImportEmployeeDayFilter"];

                    var _linq_FilterStatusIndex = (from data in _item_FilterStatus
                                                   where
                                                   data.u0_loc_idx == int.Parse(ddlPlacesFilter.SelectedValue)

                                                   select data
                                            ).ToList();

                    _set_statusFilter = int.Parse(ddlPlacesFilter.SelectedValue);

                    //

                    ViewState["Vs_DetailOTImportEmployeeDay"] = _linq_FilterStatusIndex;
                    setGridData(GvDetailEmployeeImportOTDay, ViewState["Vs_DetailOTImportEmployeeDay"]);
                    //ViewState["Va_DetailCarbooking"] = null;

                }
                else
                {

                    getDetailEmployeeImportOTDay(GvDetailEmployeeImportOTDay, 1);

                    ////Panel_searchDetailIndex.Visible = true;
                    //////ViewState["Va_DetailCarbooking_linq"] = null;
                    ////getDetailCarBooking();

                    //setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                }

                break;
            case "ddl_shifttimestart":

                foreach (GridViewRow row in GvDetailCreateOTDay.Rows)
                {

                    Label lbl_shift_date = (Label)row.Cells[1].FindControl("lbl_shift_date");
                    Label lbl_holiday_idx_otday = (Label)row.Cells[1].FindControl("lbl_holiday_idx_otday");
                    Label lbl_parttime_start_time = (Label)row.Cells[1].FindControl("lbl_parttime_start_time");
                    Label lbl_parttime_end_time = (Label)row.Cells[1].FindControl("lbl_parttime_end_time");
                    Label lbl_parttime_break_start_time = (Label)row.Cells[1].FindControl("lbl_parttime_break_start_time");

                    Label lbl_parttime_day_off = (Label)row.Cells[1].FindControl("lbl_parttime_day_off");
                    Label lbl_date_condition = (Label)row.Cells[1].FindControl("lbl_date_condition");
                    Label lbl_emp_type_idx = (Label)row.Cells[1].FindControl("lbl_emp_type_idx");


                    DropDownList ddl_shifttimestart = (DropDownList)row.Cells[3].FindControl("ddl_shifttimestart");
                    DropDownList ddl_shifttimeend = (DropDownList)row.Cells[4].FindControl("ddl_shifttimeend");

                    Label lbl_shift_otholiday = (Label)row.Cells[9].FindControl("lbl_shift_otholiday");
                    Label lbl_hours_otholiday = (Label)row.Cells[9].FindControl("lbl_hours_otholiday");

                    Label lbl_shift_otafter = (Label)row.Cells[8].FindControl("lbl_shift_otafter");
                    Label lbl_hours_otafter = (Label)row.Cells[8].FindControl("lbl_hours_otafter");

                    Label lbl_shift_otbefore = (Label)row.Cells[9].FindControl("lbl_shift_otbefore");
                    Label lbl_hours_otbefore = (Label)row.Cells[9].FindControl("lbl_hours_otbefore");

                    Label lbl_shift_timestart_otscan = (Label)row.Cells[9].FindControl("lbl_shift_timestart_otscan");
                    Label lbl_shift_timeend_otscan = (Label)row.Cells[9].FindControl("lbl_shift_timeend_otscan");

                    DropDownList ddl_shifttimestart_ot = (DropDownList)row.Cells[5].FindControl("ddl_shifttimestart_ot");
                    DropDownList ddl_shifttimeend_ot = (DropDownList)row.Cells[6].FindControl("ddl_shifttimeend_ot");



                    //day off employee
                    //string day_work = lbl_parttime_workdays.Text;

                    string day_off = lbl_parttime_day_off.Text;
                    string day_condition = lbl_date_condition.Text;
                    string[] set_work = day_off.Split(',');
                    //string day_workemployee = "";
                    string day_offemployee = "";

                    if (Array.IndexOf(set_work, day_condition.ToString()) > -1)
                    {
                        day_offemployee = "1"; //yes day off
                                               //litDebug.Text += day_offemployee.ToString() + "|";
                                               //litDebug.Text += "1" + "|";
                    }
                    else
                    {
                        day_offemployee = "2"; // no day off
                                               //litDebug.Text += day_offemployee.ToString() + "|";
                    }
                    //day off employee

                    IFormatProvider culture = new CultureInfo("en-US", true);
                    DateTime DateTime_Start = new DateTime();
                    DateTime DateTime_End = new DateTime();
                    DateTime DateTime_Break = new DateTime();
                    DateTime DateTime_StartDay = new DateTime();
                    DateTime DateTime_EndDay = new DateTime();
                    DateTime dstart_del = new DateTime();
                    //DateTime dstart_del = new DateTime();
                    decimal matFloor_;


                    var shifttime_start = new DateTime();
                    var shifttime_end = new DateTime();
                    var d_start = new DateTime();
                    var d_end = new DateTime();

                    shifttime_start = DateTime.ParseExact((lbl_parttime_start_time.Text), "HH:mm", culture);
                    shifttime_end = DateTime.ParseExact((lbl_parttime_end_time.Text), "HH:mm", culture);
                    d_start = DateTime.ParseExact((lbl_shift_date.Text), "dd/MM/yyyy", culture);
                    d_end = DateTime.ParseExact((lbl_shift_date.Text), "dd/MM/yyyy", culture);

                    if (shifttime_start > shifttime_end)
                    {
                        dstart_del = d_start.AddDays(-1);
                    }
                    else
                    {
                        dstart_del = DateTime.ParseExact((lbl_shift_date.Text), "dd/MM/yyyy", culture);
                    }

                    DateTime_Start = DateTime.ParseExact(ddl_shifttimestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture);
                    DateTime_End = DateTime.ParseExact(ddl_shifttimeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture);
                    DateTime_Break = DateTime.ParseExact((dstart_del.ToString("dd/MM/yyyy") + " " + lbl_parttime_break_start_time.Text), "dd/MM/yyyy HH:mm", culture);

                    DateTime_StartDay = DateTime.ParseExact((dstart_del.ToString("dd/MM/yyyy") + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture);
                    DateTime_EndDay = DateTime.ParseExact((lbl_shift_date.Text + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture);


                    //litDebug1.Text = DateTime_Start.ToString("dd/MM/yyyy HH:mm") + "|" + DateTime_End.ToString("dd/MM/yyyy HH:mm") + "|" + DateTime_Break.ToString("dd/MM/yyyy HH:mm");

                    //var result_time_holiday = new DateTime();
                    if (DateTime_Start < DateTime_StartDay && DateTime_End < DateTime_Break)
                    {

                        var result_time_holiday1_ = DateTime_End.Subtract(DateTime_StartDay).TotalHours;
                        matFloor_ = (Convert.ToDecimal(result_time_holiday1_.ToString()));//(Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()))).ToString();

                        //litDebug.Text += "1" + "|" + matFloor_.ToString();
                    }
                    else if (DateTime_Start < DateTime_StartDay && (DateTime_End > DateTime_Break && DateTime_End < DateTime_EndDay)) //(DateTime_StartBefore_ > DateTime_Break_ && DateTime_Out_ < DateTime_EndHoliday_)
                    {
                        //litDebug.Text += "2" + "|";
                        var result_time_holiday1_ = DateTime_End.Subtract(DateTime_StartDay).TotalHours;
                        matFloor_ = decimal.Subtract((Convert.ToDecimal(result_time_holiday1_.ToString())), (Convert.ToDecimal("1.00")));//Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()) - Convert.ToDecimal("1.00")))).ToString();

                        //decimal t;
                        //t = decimal.Subtract((Convert.ToDecimal(result_time_holiday1_.ToString())), (Convert.ToDecimal("1.00")));//(Convert.ToDecimal(result_time_holiday1_.ToString()) - Convert.ToDecimal("1.00")))).ToString();
                        //// Convert.ToDecimal(Convert.ToDecimal((Math.Floor(Convert.ToDecimal(matFloor_.ToString()) - Convert.ToDecimal("1.00"))).ToString()));
                        //litDebug.Text += "2" + "|" + matFloor_.ToString() + "|" + Convert.ToDecimal(Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()) - Convert.ToDecimal("1.00"))).ToString())) + "|" + result_time_holiday1_.ToString() + "|Toei" + t.ToString();
                    }
                    else if (DateTime_Start < DateTime_StartDay && (DateTime_End > DateTime_Break && DateTime_End > DateTime_EndDay)) //(DateTime_StartBefore_ > DateTime_Break_ && DateTime_Out_ < DateTime_EndHoliday_)
                    {
                        //litDebug.Text += "3" + "|";
                        var result_time_holiday1_ = DateTime_EndDay.Subtract(DateTime_StartDay).TotalHours;
                        matFloor_ = decimal.Subtract((Convert.ToDecimal(result_time_holiday1_.ToString())), (Convert.ToDecimal("1.00")));//(Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()) - Convert.ToDecimal("1.00"))).ToString();
                    }
                    else if (DateTime_Start > DateTime_StartDay && (DateTime_End < DateTime_Break))
                    {
                        //litDebug.Text += "4" + "|";
                        var result_time_holiday1_ = DateTime_End.Subtract(DateTime_Start).TotalHours;
                        matFloor_ = (Convert.ToDecimal(result_time_holiday1_.ToString()));//(Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()))).ToString();
                                                                                          //litDebug.Text += matFloor_.ToString() + "|";
                    }
                    else if ((DateTime_Start > DateTime_StartDay) && (DateTime_End > DateTime_Break && DateTime_End < DateTime_EndDay))
                    {
                        //litDebug.Text += "5" + "|";
                        var result_time_holiday1_ = DateTime_End.Subtract(DateTime_Start).TotalHours;
                        matFloor_ = decimal.Subtract((Convert.ToDecimal(result_time_holiday1_.ToString())), (Convert.ToDecimal("1.00")));//(Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()) - Convert.ToDecimal("1.00"))).ToString();
                    }
                    else if ((DateTime_Start > DateTime_StartDay && DateTime_Start > DateTime_Break) && (DateTime_End > DateTime_Break && DateTime_End > DateTime_EndDay))
                    {
                        //litDebug.Text += "6" + "|";
                        var result_time_holiday1_ = DateTime_EndDay.Subtract(DateTime_Start).TotalHours;
                        matFloor_ = (Convert.ToDecimal(result_time_holiday1_.ToString()));//(Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()))).ToString();
                    }
                    else if ((DateTime_Start > DateTime_StartDay && DateTime_Start < DateTime_Break) && (DateTime_End > DateTime_Break && DateTime_End > DateTime_EndDay))
                    {
                        //litDebug.Text += "7" + "|";
                        var result_time_holiday1_ = DateTime_EndDay.Subtract(DateTime_Start).TotalHours;
                        matFloor_ = decimal.Subtract((Convert.ToDecimal(result_time_holiday1_.ToString())), (Convert.ToDecimal("1.00")));//(Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()) - Convert.ToDecimal("1.00"))).ToString();
                    }
                    else
                    {
                        //litDebug.Text += "8" + "|";
                        matFloor_ = (Convert.ToDecimal("0.00")); //"0.00";
                    }

                    //holiday or day off
                    if (lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1")
                    {
                        string input_decimal_holiday = String.Format("{0:N2}", Convert.ToDecimal(matFloor_.ToString()));
                        //litDebug.Text += input_decimal_holiday + "|";

                        var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                        if (regex.IsMatch(input_decimal_holiday))
                        {
                            var decimal_holiday = regex.Match(input_decimal_holiday).Value;
                            //litDebug1.Text = decimal_places.ToString();
                            decimal value_holiday = Convert.ToDecimal(decimal_holiday.ToString());
                            int n_holiday = Convert.ToInt32(value_holiday);
                            // litDebug1.Text += value.ToString() + ",";

                            if (n_holiday >= 50)
                            {

                                //litDebug1.Text = "1";
                                //chk_date_ot.Checked = true;
                                lbl_shift_otholiday.Text = (Math.Floor(Convert.ToDecimal(matFloor_.ToString())) + ".50").ToString();
                                if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                {
                                    lbl_hours_otholiday.Text = "x1";
                                }
                                else
                                {
                                    lbl_hours_otholiday.Text = "x2";
                                }



                            }
                            else
                            {
                                //litDebug1.Text = "33";
                                lbl_shift_otholiday.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(matFloor_.ToString()))))));
                                if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                {
                                    lbl_hours_otholiday.Text = "x1";
                                }
                                else
                                {
                                    lbl_hours_otholiday.Text = "x2";
                                }

                            }
                        }
                    }

                    //date scan tranlof time end
                    DateTime DateTime_Start_ot = new DateTime();
                    DateTime DateTime_End_ot = new DateTime();

                    if (ddl_shifttimestart_ot.SelectedValue != "")
                    {
                        //DateTime_Start = DateTime.ParseExact(lbl_shift_timestart_otscan.Text, "dd/MM/yyyy HH:mm", culture_);
                        DateTime_Start = DateTime.ParseExact(ddl_shifttimestart_ot.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture);
                        //litDebug.Text += DateTime_Start.ToString("dd/MM/yyyy HH:mm:ss") + "|";

                    }

                    //if (lbl_shift_timeend_otscan.Text != "")
                    if (ddl_shifttimeend_ot.SelectedValue != "")
                    {
                        //DateTime_End = DateTime.ParseExact(lbl_shift_timeend_otscan.Text, "dd/MM/yyyy HH:mm", culture_);
                        DateTime_End = DateTime.ParseExact(ddl_shifttimeend_ot.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture);
                    }
                    //date scan tranlof time end

                    ////if (lbl_shift_timestart_otscan.Text != "")
                    ////{
                    ////    DateTime_Start = DateTime.ParseExact(lbl_shift_timestart_otscan.Text, "dd/MM/yyyy HH:mm", culture);
                    ////    //litDebug.Text += DateTime_Start.ToString("dd/MM/yyyy HH:mm:ss") + "|";

                    ////}

                    ////if (lbl_shift_timeend_otscan.Text != "")
                    ////{
                    ////    DateTime_End = DateTime.ParseExact(lbl_shift_timeend_otscan.Text, "dd/MM/yyyy HH:mm", culture);
                    ////}

                    //ot after
                    string time_finish = "";
                    DateTime Datestarting_otafter = new DateTime();
                    DateTime time_finish_end = new DateTime();
                    //DateTime new_time_start_ot = new DateTime();

                    time_finish = lbl_parttime_end_time.Text;
                    time_finish_end = DateTime.ParseExact(time_finish, "HH:mm", CultureInfo.InvariantCulture);
                    time_finish_end = time_finish_end.AddMinutes(30);
                    Datestarting_otafter = DateTime.ParseExact((lbl_shift_date.Text + " " + time_finish_end.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture);


                    var result_time = DateTime_End.Subtract(Datestarting_otafter).TotalHours;
                    double result_time_ = Convert.ToDouble(result_time);
                    // ot after
                    if (ddl_shifttimeend_ot.SelectedValue != "")
                    {
                        if (result_time_ > 0)
                        {

                            string input_decimal_number = String.Format("{0:N2}", Convert.ToDecimal(result_time.ToString()));
                            //string input_decimal_otafter = String.Format("{0:N2}", Convert.ToDecimal(result_time.ToString()));
                            var regex_ = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                            if (regex_.IsMatch(input_decimal_number))
                            {
                                var decimal_places = regex_.Match(input_decimal_number).Value;
                                decimal value = Convert.ToDecimal(decimal_places.ToString());
                                int n = Convert.ToInt32(value);

                                if (n >= 50)
                                {

                                    if ((lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1"))// && valueholiday_check.ToString() == "1")//is holiday and no day work
                                    {

                                        //litDebug.Text = "1";

                                        if (Convert.ToDouble(lbl_shift_otholiday.Text) >= 8)
                                        {
                                            lbl_shift_otafter.Text = ((Math.Floor(Convert.ToDecimal(result_time.ToString())) + ".50").ToString());
                                            lbl_hours_otafter.Text = "x3";
                                        }
                                        else
                                        {
                                            lbl_shift_otafter.Text = "0.00";
                                            lbl_hours_otafter.Text = "x0";
                                        }

                                    }
                                    else
                                    {
                                        lbl_shift_otafter.Text = ((Math.Floor(Convert.ToDecimal(result_time.ToString())) + ".50").ToString());
                                        lbl_hours_otafter.Text = "x1.5";
                                    }


                                }
                                else
                                {

                                    if ((lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1"))// && valueholiday_check.ToString() == "1")//is holiday and no day work
                                    {
                                        //litDebug.Text = valueholiday_check.ToString();

                                        if (Convert.ToDouble(lbl_shift_otholiday.Text) >= 8)
                                        {

                                            lbl_shift_otafter.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_time.ToString()))).ToString())));
                                            lbl_hours_otafter.Text = "x3";
                                        }
                                        else
                                        {

                                            lbl_shift_otafter.Text = "0.00";
                                            lbl_hours_otafter.Text = "x0";
                                        }

                                    }
                                    else
                                    {


                                        lbl_shift_otafter.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_time.ToString()))).ToString())));
                                        lbl_hours_otafter.Text = "x1.5";
                                    }


                                }
                            }
                        }
                        else
                        {

                            lbl_shift_otafter.Text = "0.00";
                            lbl_shift_otafter.Visible = false;
                            lbl_hours_otafter.Visible = false;


                        }
                    }
                    else
                    {
                        lbl_shift_otafter.Text = "0.00";
                    }


                    //ot before
                    if (ddl_shifttimestart_ot.SelectedValue != "")
                    {

                        string time_workstart = "";
                        DateTime Datestart_otbefore = new DateTime();
                        DateTime time_work_start = new DateTime();
                        //DateTime new_time_start_ot = new DateTime();

                        time_workstart = lbl_parttime_start_time.Text;
                        time_work_start = DateTime.ParseExact(time_workstart, "HH:mm", CultureInfo.InvariantCulture);

                        if (shifttime_start > shifttime_end)
                        {

                            //litDebug.Text = dstart_del.ToString("dd/MM/yyyy") + "|" + lbl_shift_date.Text;
                            //Datestart_otbefore = DateTime.ParseExact((dstart_del.ToString() + " " + time_work_start.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture_);
                            //Datestart_otbefore = DateTime.ParseExact((DateTime.ParseExact((dstart_del.ToString()), "dd/MM/yyyy", culture_) + " " + time_work_start.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture_);
                            Datestart_otbefore = DateTime.ParseExact((dstart_del.ToString("dd/MM/yyyy") + " " + time_work_start.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture);


                        }
                        else
                        {
                            Datestart_otbefore = DateTime.ParseExact((lbl_shift_date.Text + " " + time_work_start.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture);

                        }


                        //

                        var result_timebefore = Datestart_otbefore.Subtract(DateTime_Start).TotalHours;
                        double result_time_before = Convert.ToDouble(result_timebefore);
                        //litDebug.Text = Datestart_otbefore.ToString("dd/MM/yyyy HH:mm") + "|" + DateTime_Start.ToString("dd/MM/yyyy HH:mm") + "|" + result_time_before.ToString();
                        //ot before
                        if (result_time_before > 0)
                        {

                            string input_decimal_before = String.Format("{0:N2}", Convert.ToDecimal(result_timebefore.ToString()));
                            //string input_decimal_otafter = String.Format("{0:N2}", Convert.ToDecimal(result_time.ToString()));
                            var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                            if (regex.IsMatch(input_decimal_before))
                            {
                                var decimal_before = regex.Match(input_decimal_before).Value;
                                //litDebug1.Text = decimal_places.ToString();
                                decimal value_before = Convert.ToDecimal(decimal_before.ToString());
                                int n_before = Convert.ToInt32(value_before);
                                // litDebug1.Text += value.ToString() + ",";

                                if (n_before >= 50)
                                {
                                    //chk_date_ot.Checked = true;
                                    if ((lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1"))// && valueholiday_check.ToString() == "1") //is holiday and no day work
                                    {

                                        if (Convert.ToDouble(lbl_shift_otholiday.Text) >= 8)
                                        {
                                            lbl_shift_otbefore.Text = ((Math.Floor(Convert.ToDecimal(result_timebefore.ToString())) + ".50").ToString());
                                            lbl_hours_otbefore.Text = "x3";
                                        }
                                        else
                                        {
                                            lbl_shift_otbefore.Text = "0.00";
                                            lbl_hours_otbefore.Text = "x0";
                                        }

                                    }
                                    else
                                    {
                                        lbl_shift_otbefore.Text = ((Math.Floor(Convert.ToDecimal(result_timebefore.ToString())) + ".50").ToString());
                                        lbl_hours_otbefore.Text = "x1.5";
                                    }



                                }
                                else
                                {

                                    if ((lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1"))// && valueholiday_check.ToString() == "1") //is holiday and no day work
                                    {

                                        if (Convert.ToDouble(lbl_shift_otholiday.Text) >= 8)
                                        {
                                            lbl_shift_otbefore.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_timebefore.ToString()))).ToString())));
                                            lbl_hours_otbefore.Text = "x3";
                                        }
                                        else
                                        {
                                            lbl_shift_otbefore.Text = "0.00";
                                            lbl_hours_otbefore.Text = "x1";
                                        }

                                    }
                                    else
                                    {
                                        lbl_shift_otbefore.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_timebefore.ToString()))).ToString())));
                                        lbl_hours_otbefore.Text = "x1.5";
                                    }



                                }
                            }
                            else
                            {
                                lbl_shift_otbefore.Text = "0.00";
                            }
                        }
                        else
                        {

                            lbl_shift_otbefore.Text = "0.00";
                            //txtot_before.Visible = false;
                            //txtot_before.Text = "0.00";
                        }

                    }
                    else
                    {
                        lbl_shift_otbefore.Text = "0.00";
                    }


                    //
                    if (lbl_shift_otbefore.Text == "0.00")
                    {
                        lbl_shift_otbefore.Visible = false;
                        lbl_hours_otbefore.Visible = false;
                    }
                    else
                    {
                        lbl_shift_otbefore.Visible = true;
                        lbl_hours_otbefore.Visible = true;
                    }


                    if (lbl_shift_otafter.Text == "0.00")
                    {
                        lbl_shift_otafter.Visible = false;
                        lbl_hours_otafter.Visible = false;
                    }
                    else
                    {
                        lbl_shift_otafter.Visible = true;
                        lbl_hours_otafter.Visible = true;
                    }


                    if (ddl_shifttimestart_ot.SelectedValue == "")
                    {
                        ddl_shifttimestart_ot.Visible = false;
                    }
                    else
                    {
                        ddl_shifttimestart_ot.Visible = true;
                    }

                    if (ddl_shifttimeend_ot.SelectedValue == "")
                    {
                        ddl_shifttimeend_ot.Visible = false;
                    }
                    else
                    {
                        ddl_shifttimeend_ot.Visible = true;
                    }


                }
                break;

            case "ddl_shifttimeend":

                foreach (GridViewRow row in GvDetailCreateOTDay.Rows)
                {

                    Label lbl_shift_date = (Label)row.Cells[1].FindControl("lbl_shift_date");
                    Label lbl_holiday_idx_otday = (Label)row.Cells[1].FindControl("lbl_holiday_idx_otday");
                    Label lbl_parttime_start_time = (Label)row.Cells[1].FindControl("lbl_parttime_start_time");
                    Label lbl_parttime_end_time = (Label)row.Cells[1].FindControl("lbl_parttime_end_time");
                    Label lbl_parttime_break_start_time = (Label)row.Cells[1].FindControl("lbl_parttime_break_start_time");

                    Label lbl_parttime_day_off = (Label)row.Cells[1].FindControl("lbl_parttime_day_off");
                    Label lbl_date_condition = (Label)row.Cells[1].FindControl("lbl_date_condition");
                    Label lbl_emp_type_idx = (Label)row.Cells[1].FindControl("lbl_emp_type_idx");

                    DropDownList ddl_shifttimestart = (DropDownList)row.Cells[3].FindControl("ddl_shifttimestart");
                    DropDownList ddl_shifttimeend = (DropDownList)row.Cells[4].FindControl("ddl_shifttimeend");

                    Label lbl_shift_otholiday = (Label)row.Cells[9].FindControl("lbl_shift_otholiday");
                    Label lbl_hours_otholiday = (Label)row.Cells[9].FindControl("lbl_hours_otholiday");

                    Label lbl_shift_otafter = (Label)row.Cells[8].FindControl("lbl_shift_otafter");
                    Label lbl_hours_otafter = (Label)row.Cells[8].FindControl("lbl_hours_otafter");

                    Label lbl_shift_otbefore = (Label)row.Cells[9].FindControl("lbl_shift_otbefore");
                    Label lbl_hours_otbefore = (Label)row.Cells[9].FindControl("lbl_hours_otbefore");

                    Label lbl_shift_timestart_otscan = (Label)row.Cells[9].FindControl("lbl_shift_timestart_otscan");
                    Label lbl_shift_timeend_otscan = (Label)row.Cells[9].FindControl("lbl_shift_timeend_otscan");

                    DropDownList ddl_shifttimestart_ot = (DropDownList)row.Cells[5].FindControl("ddl_shifttimestart_ot");
                    DropDownList ddl_shifttimeend_ot = (DropDownList)row.Cells[6].FindControl("ddl_shifttimeend_ot");



                    //day off employee
                    //string day_work = lbl_parttime_workdays.Text;

                    string day_off = lbl_parttime_day_off.Text;
                    string day_condition = lbl_date_condition.Text;
                    string[] set_work = day_off.Split(',');
                    //string day_workemployee = "";
                    string day_offemployee = "";

                    if (Array.IndexOf(set_work, day_condition.ToString()) > -1)
                    {
                        day_offemployee = "1"; //yes day off
                                               //litDebug.Text += day_offemployee.ToString() + "|";
                                               //litDebug.Text += "1" + "|";
                    }
                    else
                    {
                        day_offemployee = "2"; // no day off
                                               //litDebug.Text += day_offemployee.ToString() + "|";
                    }
                    //day off employee

                    IFormatProvider culture = new CultureInfo("en-US", true);
                    DateTime DateTime_Start = new DateTime();
                    DateTime DateTime_End = new DateTime();
                    DateTime DateTime_Break = new DateTime();
                    DateTime DateTime_StartDay = new DateTime();
                    DateTime DateTime_EndDay = new DateTime();
                    DateTime dstart_del = new DateTime();
                    //DateTime dstart_del = new DateTime();
                    decimal matFloor_;


                    var shifttime_start = new DateTime();
                    var shifttime_end = new DateTime();
                    var d_start = new DateTime();
                    var d_end = new DateTime();

                    shifttime_start = DateTime.ParseExact((lbl_parttime_start_time.Text), "HH:mm", culture);
                    shifttime_end = DateTime.ParseExact((lbl_parttime_end_time.Text), "HH:mm", culture);
                    d_start = DateTime.ParseExact((lbl_shift_date.Text), "dd/MM/yyyy", culture);
                    d_end = DateTime.ParseExact((lbl_shift_date.Text), "dd/MM/yyyy", culture);

                    if (shifttime_start > shifttime_end)
                    {
                        dstart_del = d_start.AddDays(-1);
                    }
                    else
                    {
                        dstart_del = DateTime.ParseExact((lbl_shift_date.Text), "dd/MM/yyyy", culture);
                    }

                    DateTime_Start = DateTime.ParseExact(ddl_shifttimestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture);
                    DateTime_End = DateTime.ParseExact(ddl_shifttimeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture);
                    DateTime_Break = DateTime.ParseExact((dstart_del.ToString("dd/MM/yyyy") + " " + lbl_parttime_break_start_time.Text), "dd/MM/yyyy HH:mm", culture);

                    DateTime_StartDay = DateTime.ParseExact((dstart_del.ToString("dd/MM/yyyy") + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture);
                    DateTime_EndDay = DateTime.ParseExact((lbl_shift_date.Text + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture);


                    //litDebug1.Text = DateTime_Start.ToString("dd/MM/yyyy HH:mm") + "|" + DateTime_End.ToString("dd/MM/yyyy HH:mm") + "|" + DateTime_Break.ToString("dd/MM/yyyy HH:mm");

                    //var result_time_holiday = new DateTime();
                    if (DateTime_Start < DateTime_StartDay && DateTime_End < DateTime_Break)
                    {

                        var result_time_holiday1_ = DateTime_End.Subtract(DateTime_StartDay).TotalHours;
                        matFloor_ = (Convert.ToDecimal(result_time_holiday1_.ToString()));//(Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()))).ToString();

                        //litDebug.Text += "1" + "|" + matFloor_.ToString();
                    }
                    else if (DateTime_Start < DateTime_StartDay && (DateTime_End > DateTime_Break && DateTime_End < DateTime_EndDay)) //(DateTime_StartBefore_ > DateTime_Break_ && DateTime_Out_ < DateTime_EndHoliday_)
                    {
                        //litDebug.Text += "2" + "|";
                        var result_time_holiday1_ = DateTime_End.Subtract(DateTime_StartDay).TotalHours;
                        matFloor_ = decimal.Subtract((Convert.ToDecimal(result_time_holiday1_.ToString())), (Convert.ToDecimal("1.00")));//Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()) - Convert.ToDecimal("1.00")))).ToString();

                        //decimal t;
                        //t = decimal.Subtract((Convert.ToDecimal(result_time_holiday1_.ToString())), (Convert.ToDecimal("1.00")));//(Convert.ToDecimal(result_time_holiday1_.ToString()) - Convert.ToDecimal("1.00")))).ToString();
                        //// Convert.ToDecimal(Convert.ToDecimal((Math.Floor(Convert.ToDecimal(matFloor_.ToString()) - Convert.ToDecimal("1.00"))).ToString()));
                        //litDebug.Text += "2" + "|" + matFloor_.ToString() + "|" + Convert.ToDecimal(Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()) - Convert.ToDecimal("1.00"))).ToString())) + "|" + result_time_holiday1_.ToString() + "|Toei" + t.ToString();
                    }
                    else if (DateTime_Start < DateTime_StartDay && (DateTime_End > DateTime_Break && DateTime_End > DateTime_EndDay)) //(DateTime_StartBefore_ > DateTime_Break_ && DateTime_Out_ < DateTime_EndHoliday_)
                    {
                        //litDebug.Text += "3" + "|";
                        var result_time_holiday1_ = DateTime_EndDay.Subtract(DateTime_StartDay).TotalHours;
                        matFloor_ = decimal.Subtract((Convert.ToDecimal(result_time_holiday1_.ToString())), (Convert.ToDecimal("1.00")));//(Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()) - Convert.ToDecimal("1.00"))).ToString();
                    }
                    else if (DateTime_Start > DateTime_StartDay && (DateTime_End < DateTime_Break))
                    {
                        //litDebug.Text += "4" + "|";
                        var result_time_holiday1_ = DateTime_End.Subtract(DateTime_Start).TotalHours;
                        matFloor_ = (Convert.ToDecimal(result_time_holiday1_.ToString()));//(Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()))).ToString();
                                                                                          //litDebug.Text += matFloor_.ToString() + "|";
                    }
                    else if ((DateTime_Start > DateTime_StartDay) && (DateTime_End > DateTime_Break && DateTime_End < DateTime_EndDay))
                    {
                        //litDebug.Text += "5" + "|";
                        var result_time_holiday1_ = DateTime_End.Subtract(DateTime_Start).TotalHours;
                        matFloor_ = decimal.Subtract((Convert.ToDecimal(result_time_holiday1_.ToString())), (Convert.ToDecimal("1.00")));//(Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()) - Convert.ToDecimal("1.00"))).ToString();
                    }
                    else if ((DateTime_Start > DateTime_StartDay && DateTime_Start > DateTime_Break) && (DateTime_End > DateTime_Break && DateTime_End > DateTime_EndDay))
                    {
                        //litDebug.Text += "6" + "|";
                        var result_time_holiday1_ = DateTime_EndDay.Subtract(DateTime_Start).TotalHours;
                        matFloor_ = (Convert.ToDecimal(result_time_holiday1_.ToString()));//(Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()))).ToString();
                    }
                    else if ((DateTime_Start > DateTime_StartDay && DateTime_Start < DateTime_Break) && (DateTime_End > DateTime_Break && DateTime_End > DateTime_EndDay))
                    {
                        //litDebug.Text += "7" + "|";
                        var result_time_holiday1_ = DateTime_EndDay.Subtract(DateTime_Start).TotalHours;
                        matFloor_ = decimal.Subtract((Convert.ToDecimal(result_time_holiday1_.ToString())), (Convert.ToDecimal("1.00")));//(Math.Floor(Convert.ToDecimal(result_time_holiday1_.ToString()) - Convert.ToDecimal("1.00"))).ToString();
                    }
                    else
                    {
                        //litDebug.Text += "8" + "|";
                        matFloor_ = (Convert.ToDecimal("0.00")); //"0.00";
                    }

                    //holiday or day off
                    if (lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1")
                    {
                        string input_decimal_holiday = String.Format("{0:N2}", Convert.ToDecimal(matFloor_.ToString()));
                        //litDebug.Text += input_decimal_holiday + "|";

                        var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                        if (regex.IsMatch(input_decimal_holiday))
                        {
                            var decimal_holiday = regex.Match(input_decimal_holiday).Value;
                            //litDebug1.Text = decimal_places.ToString();
                            decimal value_holiday = Convert.ToDecimal(decimal_holiday.ToString());
                            int n_holiday = Convert.ToInt32(value_holiday);
                            // litDebug1.Text += value.ToString() + ",";

                            if (n_holiday >= 50)
                            {

                                //litDebug1.Text = "1";
                                //chk_date_ot.Checked = true;
                                lbl_shift_otholiday.Text = (Math.Floor(Convert.ToDecimal(matFloor_.ToString())) + ".50").ToString();
                                if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                {
                                    lbl_hours_otholiday.Text = "x1";
                                }
                                else
                                {
                                    lbl_hours_otholiday.Text = "x2";
                                }


                            }
                            else
                            {
                                //litDebug1.Text = "33";
                                lbl_shift_otholiday.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(matFloor_.ToString()))))));
                                if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                {
                                    lbl_hours_otholiday.Text = "x1";
                                }
                                else
                                {
                                    lbl_hours_otholiday.Text = "x2";
                                }

                            }
                        }
                    }


                    ////if ((Convert.ToDecimal(matFloor_)) >= (Convert.ToDecimal("8.00")))
                    ////{

                    //date scan tranlof time end
                    DateTime DateTime_Start_ot = new DateTime();
                    DateTime DateTime_End_ot = new DateTime();

                    if (ddl_shifttimestart_ot.SelectedValue != "")
                    {
                        //DateTime_Start = DateTime.ParseExact(lbl_shift_timestart_otscan.Text, "dd/MM/yyyy HH:mm", culture_);
                        DateTime_Start = DateTime.ParseExact(ddl_shifttimestart_ot.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture);
                        //litDebug.Text += DateTime_Start.ToString("dd/MM/yyyy HH:mm:ss") + "|";

                    }

                    //if (lbl_shift_timeend_otscan.Text != "")
                    if (ddl_shifttimeend_ot.SelectedValue != "")
                    {
                        //DateTime_End = DateTime.ParseExact(lbl_shift_timeend_otscan.Text, "dd/MM/yyyy HH:mm", culture_);
                        DateTime_End = DateTime.ParseExact(ddl_shifttimeend_ot.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture);
                    }

                    ////if (lbl_shift_timestart_otscan.Text != "")
                    ////{
                    ////    DateTime_Start = DateTime.ParseExact(lbl_shift_timestart_otscan.Text, "dd/MM/yyyy HH:mm", culture);
                    ////    //litDebug.Text += DateTime_Start.ToString("dd/MM/yyyy HH:mm:ss") + "|";

                    ////}

                    ////if (lbl_shift_timeend_otscan.Text != "")
                    ////{
                    ////    DateTime_End = DateTime.ParseExact(lbl_shift_timeend_otscan.Text, "dd/MM/yyyy HH:mm", culture);
                    ////}

                    //ot after
                    string time_finish = "";
                    DateTime Datestarting_otafter = new DateTime();
                    DateTime time_finish_end = new DateTime();
                    //DateTime new_time_start_ot = new DateTime();

                    time_finish = lbl_parttime_end_time.Text;
                    time_finish_end = DateTime.ParseExact(time_finish, "HH:mm", CultureInfo.InvariantCulture);
                    time_finish_end = time_finish_end.AddMinutes(30);
                    Datestarting_otafter = DateTime.ParseExact((lbl_shift_date.Text + " " + time_finish_end.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture);


                    var result_time = DateTime_End.Subtract(Datestarting_otafter).TotalHours;
                    double result_time_ = Convert.ToDouble(result_time);
                    // ot after
                    if (ddl_shifttimeend_ot.SelectedValue != "")
                    {
                        if (result_time_ > 0)
                        {

                            string input_decimal_number = String.Format("{0:N2}", Convert.ToDecimal(result_time.ToString()));
                            //string input_decimal_otafter = String.Format("{0:N2}", Convert.ToDecimal(result_time.ToString()));
                            var regex_ = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                            if (regex_.IsMatch(input_decimal_number))
                            {
                                var decimal_places = regex_.Match(input_decimal_number).Value;
                                decimal value = Convert.ToDecimal(decimal_places.ToString());
                                int n = Convert.ToInt32(value);

                                if (n >= 50)
                                {

                                    if ((lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1"))// && valueholiday_check.ToString() == "1")//is holiday and no day work
                                    {

                                        //litDebug.Text = "1";

                                        if (Convert.ToDouble(lbl_shift_otholiday.Text) >= 8)
                                        {
                                            lbl_shift_otafter.Text = ((Math.Floor(Convert.ToDecimal(result_time.ToString())) + ".50").ToString());
                                            lbl_hours_otafter.Text = "x3";
                                        }
                                        else
                                        {
                                            lbl_shift_otafter.Text = "0.00";
                                            lbl_hours_otafter.Text = "x0";
                                        }

                                    }
                                    else
                                    {
                                        lbl_shift_otafter.Text = ((Math.Floor(Convert.ToDecimal(result_time.ToString())) + ".50").ToString());
                                        lbl_hours_otafter.Text = "x1.5";
                                    }


                                }
                                else
                                {

                                    if ((lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1"))// && valueholiday_check.ToString() == "1")//is holiday and no day work
                                    {
                                        //litDebug.Text = valueholiday_check.ToString();

                                        if (Convert.ToDouble(lbl_shift_otholiday.Text) >= 8)
                                        {

                                            lbl_shift_otafter.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_time.ToString()))).ToString())));
                                            lbl_hours_otafter.Text = "x3";
                                        }
                                        else
                                        {

                                            lbl_shift_otafter.Text = "0.00";
                                            lbl_hours_otafter.Text = "x0";
                                        }

                                    }
                                    else
                                    {


                                        lbl_shift_otafter.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_time.ToString()))).ToString())));
                                        lbl_hours_otafter.Text = "x1.5";
                                    }


                                }
                            }
                        }
                        else
                        {

                            lbl_shift_otafter.Text = "0.00";
                            lbl_shift_otafter.Visible = false;
                            lbl_hours_otafter.Visible = false;


                        }
                    }
                    else
                    {
                        lbl_shift_otafter.Text = "0.00";
                    }


                    //ot before
                    if (ddl_shifttimestart_ot.SelectedValue != "")
                    {

                        string time_workstart = "";
                        DateTime Datestart_otbefore = new DateTime();
                        DateTime time_work_start = new DateTime();
                        //DateTime new_time_start_ot = new DateTime();

                        time_workstart = lbl_parttime_start_time.Text;
                        time_work_start = DateTime.ParseExact(time_workstart, "HH:mm", CultureInfo.InvariantCulture);

                        if (shifttime_start > shifttime_end)
                        {

                            //litDebug.Text = dstart_del.ToString("dd/MM/yyyy") + "|" + lbl_shift_date.Text;
                            //Datestart_otbefore = DateTime.ParseExact((dstart_del.ToString() + " " + time_work_start.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture_);
                            //Datestart_otbefore = DateTime.ParseExact((DateTime.ParseExact((dstart_del.ToString()), "dd/MM/yyyy", culture_) + " " + time_work_start.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture_);
                            Datestart_otbefore = DateTime.ParseExact((dstart_del.ToString("dd/MM/yyyy") + " " + time_work_start.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture);


                        }
                        else
                        {
                            Datestart_otbefore = DateTime.ParseExact((lbl_shift_date.Text + " " + time_work_start.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture);

                        }


                        //

                        var result_timebefore = Datestart_otbefore.Subtract(DateTime_Start).TotalHours;
                        double result_time_before = Convert.ToDouble(result_timebefore);
                        //litDebug.Text = Datestart_otbefore.ToString("dd/MM/yyyy HH:mm") + "|" + DateTime_Start.ToString("dd/MM/yyyy HH:mm") + "|" + result_time_before.ToString();
                        //ot before
                        if (result_time_before > 0)
                        {

                            string input_decimal_before = String.Format("{0:N2}", Convert.ToDecimal(result_timebefore.ToString()));
                            //string input_decimal_otafter = String.Format("{0:N2}", Convert.ToDecimal(result_time.ToString()));
                            var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                            if (regex.IsMatch(input_decimal_before))
                            {
                                var decimal_before = regex.Match(input_decimal_before).Value;
                                //litDebug1.Text = decimal_places.ToString();
                                decimal value_before = Convert.ToDecimal(decimal_before.ToString());
                                int n_before = Convert.ToInt32(value_before);
                                // litDebug1.Text += value.ToString() + ",";

                                if (n_before >= 50)
                                {
                                    //chk_date_ot.Checked = true;
                                    if ((lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1"))// && valueholiday_check.ToString() == "1") //is holiday and no day work
                                    {

                                        if (Convert.ToDouble(lbl_shift_otholiday.Text) >= 8)
                                        {
                                            lbl_shift_otbefore.Text = ((Math.Floor(Convert.ToDecimal(result_timebefore.ToString())) + ".50").ToString());
                                            lbl_hours_otbefore.Text = "x3";
                                        }
                                        else
                                        {
                                            lbl_shift_otbefore.Text = "0.00";
                                            lbl_hours_otbefore.Text = "x0";
                                        }

                                    }
                                    else
                                    {
                                        lbl_shift_otbefore.Text = ((Math.Floor(Convert.ToDecimal(result_timebefore.ToString())) + ".50").ToString());
                                        lbl_hours_otbefore.Text = "x1.5";
                                    }



                                }
                                else
                                {

                                    if ((lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1"))// && valueholiday_check.ToString() == "1") //is holiday and no day work
                                    {

                                        if (Convert.ToDouble(lbl_shift_otholiday.Text) >= 8)
                                        {
                                            lbl_shift_otbefore.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_timebefore.ToString()))).ToString())));
                                            lbl_hours_otbefore.Text = "x3";
                                        }
                                        else
                                        {
                                            lbl_shift_otbefore.Text = "0.00";
                                            lbl_hours_otbefore.Text = "x1";
                                        }

                                    }
                                    else
                                    {
                                        lbl_shift_otbefore.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_timebefore.ToString()))).ToString())));
                                        lbl_hours_otbefore.Text = "x1.5";
                                    }



                                }
                            }
                            else
                            {
                                lbl_shift_otbefore.Text = "0.00";
                            }
                        }
                        else
                        {

                            lbl_shift_otbefore.Text = "0.00";
                            //txtot_before.Visible = false;
                            //txtot_before.Text = "0.00";
                        }

                    }
                    else
                    {
                        lbl_shift_otbefore.Text = "0.00";
                    }


                    //
                    if (lbl_shift_otbefore.Text == "0.00")
                    {
                        lbl_shift_otbefore.Visible = false;
                        lbl_hours_otbefore.Visible = false;
                    }
                    else
                    {
                        lbl_shift_otbefore.Visible = true;
                        lbl_hours_otbefore.Visible = true;
                    }


                    if (lbl_shift_otafter.Text == "0.00")
                    {
                        lbl_shift_otafter.Visible = false;
                        lbl_hours_otafter.Visible = false;
                    }
                    else
                    {
                        lbl_shift_otafter.Visible = true;
                        lbl_hours_otafter.Visible = true;
                    }

                    if (ddl_shifttimestart_ot.SelectedValue == "")
                    {
                        ddl_shifttimestart_ot.Visible = false;
                    }
                    else
                    {
                        ddl_shifttimestart_ot.Visible = true;
                    }

                    if (ddl_shifttimeend_ot.SelectedValue == "")
                    {
                        ddl_shifttimeend_ot.Visible = false;
                    }
                    else
                    {
                        ddl_shifttimeend_ot.Visible = true;
                    }



                }

                break;

            case "ddl_timestart":

                int count_ = 0;
                foreach (GridViewRow row in GvCreateOTMonth.Rows)
                {
                    CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");

                    Label lbl_date_start = (Label)row.FindControl("lbl_date_start");
                    Label lbl_holiday_idx_check = (Label)row.FindControl("lbl_holiday_idx_check");
                    Label lbl_work_start = (Label)row.FindControl("lbl_work_start");
                    Label lbl_work_finish = (Label)row.FindControl("lbl_work_finish");
                    Label lbl_break_start = (Label)row.FindControl("lbl_break_start");
                    Label lbl_break_end = (Label)row.FindControl("lbl_break_end");

                    Label lbl_day_off = (Label)row.FindControl("lbl_day_off");
                    Label lbl_date_condition = (Label)row.FindControl("lbl_date_condition");

                    TextBox txt_sum_hour = (TextBox)row.FindControl("txt_sum_hour");
                    ////TextBox txt_sum_hour = (TextBox)e.Row.FindControl("txt_sum_hour");
                    TextBox txt_job = (TextBox)row.FindControl("txt_job");
                    TextBox txt_remark = (TextBox)row.FindControl("txt_remark");
                    TextBox txt_head_set = (TextBox)row.FindControl("txt_head_set");

                    Label txtot_before = (Label)row.FindControl("txtot_before");
                    Label txtot_after = (Label)row.FindControl("txtot_after");
                    Label txtot_holiday = (Label)row.FindControl("txtot_holiday");
                    Label lblot_before = (Label)row.FindControl("lblot_before");
                    Label lblot_after = (Label)row.FindControl("lblot_after");
                    Label lblot_holiday = (Label)row.FindControl("lblot_holiday");
                    Label lbl_detailholiday = (Label)row.FindControl("lbl_detailholiday");
                    Label lbl_detaildayoff = (Label)row.FindControl("lbl_detaildayoff");
                    // bind time start //

                    DropDownList ddl_timestart = (DropDownList)row.FindControl("ddl_timestart");
                    DropDownList ddl_timeend = (DropDownList)row.FindControl("ddl_timeend");

                    DropDownList ddl_timebefore = (DropDownList)row.FindControl("ddl_timebefore");
                    DropDownList ddl_timeafter = (DropDownList)row.FindControl("ddl_timeafter");
                    DropDownList ddl_timeholiday = (DropDownList)row.FindControl("ddl_timeholiday");



                    ddl_timebefore.Visible = false;
                    lblot_before.Visible = false;
                    ddl_timeholiday.Visible = false;
                    lblot_holiday.Visible = false;
                    ddl_timeafter.Visible = false;
                    lblot_after.Visible = false;


                    var d_start = new DateTime();
                    var d_end = new DateTime();
                    var d_finish = new DateTime();
                    var d_workstart = new DateTime();

                    double value_total_ot;
                    double value_total_otresult;

                    double value_total_ot_holiday;
                    double value_total_ot_before;
                    double value_total_ot_diff;

                    IFormatProvider culture_ = new CultureInfo("en-US", true);

                    d_finish = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);
                    d_workstart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_start.Text), "dd/MM/yyyy HH:mm", culture_);

                    d_start = DateTime.ParseExact((lbl_date_start.Text + " " + "00:00"), "dd/MM/yyyy HH:mm", culture_);
                    d_end = DateTime.ParseExact((lbl_date_start.Text + " " + "05:59"), "dd/MM/yyyy HH:mm", culture_);

                    double value_otbefore;
                    double value_otafter;
                    DateTime d_finish_addhour_after = new DateTime();
                    string day_offemployee = "";
                    //litDebug.Text += lbl_date_start.Text + "|";


                    if (row.RowType == DataControlRowType.DataRow)
                    {

                        //day off employee
                        string day_off = lbl_day_off.Text;
                        string day_condition = lbl_date_condition.Text;
                        string[] set_test = day_off.Split(',');


                        if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                        {
                            day_offemployee = "1"; //yes day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                                                   //litDebug.Text += "1" + "|";
                        }
                        else
                        {
                            day_offemployee = "2"; // no day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                        }
                        //day off employee

                        IFormatProvider culture = new CultureInfo("en-US", true);

                        //litDebug.Text += lbl_break_start.Text;

                        //if ((ddl_timeend.SelectedValue != "" && ddl_timestart.SelectedValue != "") && (ddl_timeend.SelectedItem.ToString() != ddl_timestart.SelectedItem.ToString()))
                        if ((ddl_timeend.SelectedValue != "" && ddl_timestart.SelectedValue != ""))
                        {



                            DateTime DateTime_ScanIn = DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ScanOut = DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);


                            DateTime DateTime_ShiftStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_work_start.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ShiftEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_work_finish.Text, "dd/MM/yyyy HH:mm", culture_);

                            DateTime DateTime_BreakStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_start.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_BreakEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_end.Text, "dd/MM/yyyy HH:mm", culture_);

                            double value;
                            double _value_diffbreaktime;
                            double _value_timescan_late;
                            double _value_timescan_late_add;

                            double _value_otafter_holiday;

                            DateTime d_finish_addhour = new DateTime();
                            DateTime d_start_addhour = new DateTime();

                            DateTime d_finish_addhour_holiday = new DateTime();
                            double _value_diff_scanout;
                            /*
                            
                            litDebug1.Text += DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";
                            DateTime DateTime_BreakStart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);

                            */

                            //ot before
                            value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                            txtot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                            getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));

                            //if (d_workstart.Subtract(DateTime_ScanIn).TotalMinutes >= 30)
                            //{
                            //    getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));
                            //}
                            //else
                            //{
                            //    getHourToOT(ddl_timebefore, 0);
                            //}
                            // getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));

                            //ot after
                            if (DateTime_ScanOut > DateTime_ScanIn)
                            {

                                d_finish_addhour_after = d_finish.AddMinutes(30);

                                //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                txtot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                //litDebug.Text += value_otafter.ToString() + "|";

                                //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));
                            }



                            //ot holiday
                            if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1")
                            {
                                lbl_detailholiday.Visible = true;
                                //if ((DateTime_ScanIn < DateTime_ShiftStart || DateTime_ScanIn > DateTime_ShiftStart) && DateTime_ScanOut >= DateTime_ShiftEnd)
                                if ((DateTime_ScanIn < DateTime_ShiftStart || DateTime_ScanIn > DateTime_ShiftStart))
                                {
                                    //litDebug.Text += (Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString())/30) * 0.50).ToString()) + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|"  + DateTime_ShiftEnd.ToString("dd/MM/yyyy HH:mm");
                                    ////decimal value = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                    ////litDebug1.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                    //litDebug.Text += _value_diffbreaktime.ToString() + "|";


                                    //ot before
                                    value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                    txtot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                    getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));

                                    //if (d_workstart.Subtract(DateTime_ScanIn).TotalMinutes >= 30)
                                    //{
                                    //    getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));
                                    //}
                                    //else
                                    //{
                                    //    getHourToOT(ddl_timebefore, 0);
                                    //}
                                    // getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));

                                    //ot after
                                    if (DateTime_ScanOut > DateTime_ScanIn)
                                    {

                                        d_finish_addhour_after = d_finish.AddMinutes(30);

                                        //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                        //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                        value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                        txtot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                                                                          //litDebug.Text += value_otafter.ToString() + "|";

                                        //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                        getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));
                                    }



                                    _value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                    _value_diff_scanout = DateTime_ScanOut.Subtract(DateTime_ShiftEnd).TotalMinutes;

                                    //litDebug1.Text = _value_diff_scanout.ToString() + "|";

                                    if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn < DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                        if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                        {

                                            d_finish_addhour_holiday = DateTime_ShiftEnd.AddMinutes(30);

                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                            txtot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));


                                        }
                                    }
                                    else if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn > DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {

                                        //check hour .xx
                                        string input_decimal_number_be = (Math.Floor((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60))).ToString(); //((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString();
                                        string input_decimal_number_af = ((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString("#.##");//"1.50";

                                        string decimal_places = String.Empty;


                                        ViewState["_value_timescan_late_add"] = "0";

                                        var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                                        if (regex.IsMatch(input_decimal_number_af))
                                        {
                                            decimal_places = regex.Match(input_decimal_number_af).Value;
                                            if (int.Parse(decimal_places) > 50)
                                            {

                                                _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + 1);//Convert.ToDouble((input_decimal_number_be) + 1);
                                                ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                            }
                                            else if (int.Parse(decimal_places) < 50)
                                            {

                                                _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + .50);//Convert.ToDouble((input_decimal_number_be + .50) );

                                                ViewState["_value_timescan_late_add"] = _value_timescan_late_add;


                                                d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                            }
                                            else
                                            {
                                                _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be))); //Convert.ToDouble((input_decimal_number_be));
                                                ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));


                                            }

                                        }

                                        // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                        if (_value_diff_scanout >= 60)
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }
                                        else
                                        {

                                            //value = Convert.ToDouble(((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1) );

                                            value = Convert.ToDouble(((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1) - Convert.ToDouble(ViewState["_value_timescan_late_add"].ToString()));

                                        }

                                        //litDebug1.Text += value.ToString();
                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                        if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                        {
                                            //litDebug.Text = d_finish_addhour_holiday.ToString();
                                            //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                            //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                            //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                            d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                            txtot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));

                                        }
                                        else
                                        {
                                            getHourToOT(ddl_timeafter, Convert.ToDouble("0"));
                                        }

                                        /*

                                        //litDebug.Text = DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                        _value_timescan_late = Convert.ToDouble((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;
                                        _value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();

                                        //litDebug.Text = _value_timescan_late_add.ToString();

                                        d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                        if (_value_diff_scanout >= 60)
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }
                                        else
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }

                                        //litDebug1.Text += value.ToString();
                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                        if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                        {
                                            //litDebug.Text = d_finish_addhour_holiday.ToString();
                                            //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                            //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                            //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                            d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                            txtot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));

                                        }
                                        else
                                        {
                                            getHourToOT(ddl_timeafter, Convert.ToDouble("0"));
                                        }

                                        */

                                    }
                                    else if (DateTime_ScanOut < DateTime_ShiftEnd && (DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60))
                                    {
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));
                                    }
                                    else //if (DateTime_ScanOut > DateTime_BreakEnd && DateTime_ScanOut < DateTime_ShiftEnd)
                                    {
                                        //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);

                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));
                                    }

                                }
                                else if (DateTime_ScanIn < d_workstart && DateTime_ScanOut < d_finish)
                                {
                                    //total ot
                                    value_total_ot = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 60));
                                    value_total_otresult = (Math.Floor(value_total_ot * 2) / 2);
                                    //total ot


                                    //litDebug.Text = value_total_otresult.ToString();
                                    if (value_total_otresult >= 9.5) //set -1 breaktime
                                    {

                                        value_total_ot_diff = value_total_otresult - 1;


                                        value_total_ot_holiday = 8;
                                        value_total_ot_before = value_total_ot_diff - value_total_ot_holiday;

                                        //litDebug.Text = value_total_ot_before.ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                        getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));

                                    }
                                    else if (value_total_otresult == 9)
                                    {
                                        value_total_ot_diff = value_total_otresult - 1;
                                        value_total_ot_holiday = 8;
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                    }
                                    else if (value_total_otresult >= 4 && value_total_otresult <= 5)
                                    {
                                        value_total_ot_before = 4;
                                        getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));
                                    }
                                    else if (value_total_otresult > 5 && value_total_otresult <= 8)
                                    {
                                        value_total_ot_diff = value_total_otresult - 1;

                                        value_total_ot_before = value_total_ot_diff;
                                        getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));
                                    }
                                }
                            }



                            //set ot x1, x1.5, x2, x3
                            //set ot before
                            if (ddl_timebefore.SelectedValue != "" && DateTime_ScanOut > DateTime_ScanIn)
                            {

                                lblot_before.Visible = true;
                                ddl_timebefore.Visible = true;
                                if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                {
                                    if (ddl_timeholiday.SelectedValue == "8")
                                    {
                                        lblot_before.Text = "x3";
                                    }
                                    else
                                    {
                                        lblot_before.Text = "x1";
                                    }
                                    //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                    //{
                                    //    lbl_hour_otbefore.Text = "x1";
                                    //}
                                    //else
                                    //{
                                    //    lbl_hour_otbefore.Text = "x2";
                                    //}
                                }
                                else
                                {
                                    lblot_before.Text = "x1.5";
                                }
                            }


                            //set ot after
                            if (ddl_timeafter.SelectedValue != "" && DateTime_ScanOut > DateTime_ScanIn)
                            {
                                lblot_after.Visible = true;
                                ddl_timeafter.Visible = true;
                                if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                {
                                    if (ddl_timeholiday.SelectedValue == "8")
                                    {
                                        lblot_after.Text = "x3";
                                    }
                                    else
                                    {
                                        lblot_after.Text = "x1";
                                    }
                                    //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                    //{
                                    //    lbl_hour_otbefore.Text = "x1";
                                    //}
                                    //else
                                    //{
                                    //    lbl_hour_otbefore.Text = "x2";
                                    //}
                                }
                                else
                                {
                                    lblot_after.Text = "x1.5";
                                }
                            }


                            //set ot holiday
                            if (ddl_timeholiday.SelectedValue != "" && DateTime_ScanOut > DateTime_ScanIn)
                            {

                                lblot_holiday.Visible = true;
                                ddl_timeholiday.Visible = true;
                                if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                {
                                    lblot_holiday.Text = "x1";

                                }

                            }

                            if (lbl_holiday_idx_check.Text != "0")
                            {
                                lbl_detailholiday.Visible = true;
                                lbl_detaildayoff.Visible = false;
                            }
                            else if (day_offemployee.ToString() == "1")
                            {
                                lbl_detaildayoff.Visible = true;
                                lbl_detailholiday.Visible = false;
                            }


                            //litDebug.Text += value_otbefore.ToString() + "|";
                        }
                        else
                        {


                            ddl_timebefore.Visible = false;
                            lblot_before.Visible = false;
                            ddl_timeholiday.Visible = false;
                            lblot_holiday.Visible = false;
                            ddl_timeafter.Visible = false;
                            lblot_after.Visible = false;

                        }



                    }

                    count_++;


                    Label lit_total_hoursot_before = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");
                    Label lit_total_hoursot_after = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");
                    Label lit_total_hoursot_holiday = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");
                    if (chk_date_ot.Checked)
                    {

                        if (ddl_timebefore.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timebefore.SelectedValue) >= 0.5)
                            {
                                totactual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                                //Label lit_total_hoursot_before_ = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");

                            }
                        }

                        if (ddl_timeafter.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeafter.SelectedValue) >= 0.5)
                            {
                                totactual_otafter += Convert.ToDecimal(ddl_timeafter.SelectedValue);
                                //Label lit_total_hoursot_after_ = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");

                            }
                        }

                        if (ddl_timeholiday.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeholiday.SelectedValue) >= 0.5)
                            {
                                totactual_otholiday += Convert.ToDecimal(ddl_timeholiday.SelectedValue);
                                //Label lit_total_hoursot_holiday_ = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");

                            }
                        }

                        lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                        lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                        lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);

                        //tot_actual_otbefore += Convert.ToDecimal(txtot_before.Text);
                        //tot_actual_otafter += Convert.ToDecimal(txtot_after.Text);
                        //tot_actual_otholiday += Convert.ToDecimal(txtot_holiday.Text);

                        ////Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                        ////lit_total_hoursmonth.Text = String.Format("{0:N2}", tot_actual_otmonth);

                        //lit_total_hoursot_before.Text = String.Format("{0:N2}", tot_actual_otbefore);
                        //lit_total_hoursot_after.Text = String.Format("{0:N2}", tot_actual_otafter);
                        //lit_total_hoursot_holiday.Text = String.Format("{0:N2}", tot_actual_otholiday);
                    }


                    //else
                    //{
                    //    tot_actual_otbefore += Convert.ToDecimal("0.00");
                    //    tot_actual_otafter += Convert.ToDecimal("0.00");
                    //    tot_actual_otholiday += Convert.ToDecimal("0.00");

                    //    //Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                    //    //lit_total_hoursmonth.Text = String.Format("{0:N2}", tot_actual_otmonth);

                    //    lit_total_hoursot_before.Text = String.Format("{0:N2}", tot_actual_otbefore);
                    //    lit_total_hoursot_after.Text = String.Format("{0:N2}", tot_actual_otafter);
                    //    lit_total_hoursot_holiday.Text = String.Format("{0:N2}", tot_actual_otholiday);
                    //}


                }
                break;


            case "ddl_timeend":

                int count_end = 0;
                foreach (GridViewRow row in GvCreateOTMonth.Rows)
                {
                    CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");

                    Label lbl_date_start = (Label)row.FindControl("lbl_date_start");
                    Label lbl_holiday_idx_check = (Label)row.FindControl("lbl_holiday_idx_check");
                    Label lbl_work_start = (Label)row.FindControl("lbl_work_start");
                    Label lbl_work_finish = (Label)row.FindControl("lbl_work_finish");
                    Label lbl_break_start = (Label)row.FindControl("lbl_break_start");
                    Label lbl_break_end = (Label)row.FindControl("lbl_break_end");

                    Label lbl_day_off = (Label)row.FindControl("lbl_day_off");
                    Label lbl_date_condition = (Label)row.FindControl("lbl_date_condition");

                    TextBox txt_sum_hour = (TextBox)row.FindControl("txt_sum_hour");
                    ////TextBox txt_sum_hour = (TextBox)e.Row.FindControl("txt_sum_hour");
                    TextBox txt_job = (TextBox)row.FindControl("txt_job");
                    TextBox txt_remark = (TextBox)row.FindControl("txt_remark");
                    TextBox txt_head_set = (TextBox)row.FindControl("txt_head_set");

                    Label txtot_before = (Label)row.FindControl("txtot_before");
                    Label txtot_after = (Label)row.FindControl("txtot_after");
                    Label txtot_holiday = (Label)row.FindControl("txtot_holiday");
                    Label lblot_before = (Label)row.FindControl("lblot_before");
                    Label lblot_after = (Label)row.FindControl("lblot_after");
                    Label lblot_holiday = (Label)row.FindControl("lblot_holiday");
                    Label lbl_detailholiday = (Label)row.FindControl("lbl_detailholiday");
                    Label lbl_detaildayoff = (Label)row.FindControl("lbl_detaildayoff");
                    // bind time start //

                    DropDownList ddl_timestart = (DropDownList)row.FindControl("ddl_timestart");
                    DropDownList ddl_timeend = (DropDownList)row.FindControl("ddl_timeend");

                    DropDownList ddl_timebefore = (DropDownList)row.FindControl("ddl_timebefore");
                    DropDownList ddl_timeafter = (DropDownList)row.FindControl("ddl_timeafter");
                    DropDownList ddl_timeholiday = (DropDownList)row.FindControl("ddl_timeholiday");

                    var d_start = new DateTime();
                    var d_end = new DateTime();
                    var d_finish = new DateTime();
                    var d_workstart = new DateTime();

                    double value_total_ot;
                    double value_total_otresult;

                    double value_total_ot_holiday;
                    double value_total_ot_before;
                    double value_total_ot_diff;

                    IFormatProvider culture_ = new CultureInfo("en-US", true);

                    d_finish = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);
                    d_workstart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_start.Text), "dd/MM/yyyy HH:mm", culture_);

                    d_start = DateTime.ParseExact((lbl_date_start.Text + " " + "00:00"), "dd/MM/yyyy HH:mm", culture_);
                    d_end = DateTime.ParseExact((lbl_date_start.Text + " " + "05:59"), "dd/MM/yyyy HH:mm", culture_);

                    double value_otbefore;
                    double value_otafter;
                    DateTime d_finish_addhour_after = new DateTime();
                    string day_offemployee = "";
                    //litDebug.Text += lbl_date_start.Text + "|";

                    ddl_timebefore.Visible = false;
                    lblot_before.Visible = false;
                    ddl_timeholiday.Visible = false;
                    lblot_holiday.Visible = false;
                    ddl_timeafter.Visible = false;
                    lblot_after.Visible = false;

                    if (row.RowType == DataControlRowType.DataRow)
                    {


                        //day off employee
                        string day_off = lbl_day_off.Text;
                        string day_condition = lbl_date_condition.Text;
                        string[] set_test = day_off.Split(',');


                        if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                        {
                            day_offemployee = "1"; //yes day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                                                   //litDebug.Text += "1" + "|";
                        }
                        else
                        {
                            day_offemployee = "2"; // no day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                        }
                        //day off employee

                        IFormatProvider culture = new CultureInfo("en-US", true);

                        //litDebug.Text += lbl_break_start_.Text;

                        if (ddl_timeend.SelectedValue != "" && ddl_timestart.SelectedValue != "")
                        {


                            DateTime DateTime_ScanIn = DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ScanOut = DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);


                            DateTime DateTime_ShiftStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_work_start.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ShiftEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_work_finish.Text, "dd/MM/yyyy HH:mm", culture_);

                            DateTime DateTime_BreakStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_start.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_BreakEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_end.Text, "dd/MM/yyyy HH:mm", culture_);

                            if (DateTime_ScanOut > DateTime_ScanIn)
                            {

                                //litDebug.Text = DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";

                                double value;
                                double _value_diffbreaktime;
                                double _value_timescan_late;
                                double _value_timescan_late_add;

                                double _value_otafter_holiday;

                                DateTime d_finish_addhour = new DateTime();
                                DateTime d_start_addhour = new DateTime();

                                DateTime d_finish_addhour_holiday = new DateTime();
                                double _value_diff_scanout;
                                /*

                                litDebug1.Text += DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";
                                DateTime DateTime_BreakStart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);

                                */

                                //ot before
                                value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                txtot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                ddl_timebefore.Visible = true;
                                lblot_before.Visible = true;

                                getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));

                                //ot after
                                if (DateTime_ScanOut > DateTime_ScanIn)
                                {
                                    ddl_timeafter.Visible = true;
                                    lblot_after.Visible = true;

                                    d_finish_addhour_after = d_finish.AddMinutes(30);

                                    //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                    //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                    value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                    txtot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                                                                      //litDebug.Text += value_otafter.ToString() + "|";

                                    //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                    getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));
                                }
                                //else
                                //{
                                //    ddl_timeafter.Visible = false;
                                //}


                                //ot holiday
                                if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1")
                                {

                                    //litDebug.Text += "1";
                                    ddl_timeholiday.Visible = true;
                                    lblot_holiday.Visible = true;

                                    //lbl_detailholiday.Visible = true;

                                    //if ((DateTime_ScanIn < DateTime_ShiftStart || DateTime_ScanIn > DateTime_ShiftStart) && DateTime_ScanOut >= DateTime_ShiftEnd)
                                    if ((DateTime_ScanIn <= DateTime_ShiftStart || DateTime_ScanIn >= DateTime_ShiftStart))
                                    {
                                        //litDebug.Text += (Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString())/30) * 0.50).ToString()) + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|"  + DateTime_ShiftEnd.ToString("dd/MM/yyyy HH:mm");
                                        ////decimal value = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                        ////litDebug1.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        //litDebug.Text += _value_diffbreaktime.ToString() + "|";

                                        //ot before
                                        value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                        txtot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));

                                        //if (d_workstart.Subtract(DateTime_ScanIn).TotalMinutes >= 30)
                                        //{
                                        //    getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));
                                        //}
                                        //else
                                        //{
                                        //    getHourToOT(ddl_timebefore, 0);
                                        //}


                                        //getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));


                                        //ot after
                                        if (DateTime_ScanOut > DateTime_ScanIn)
                                        {

                                            d_finish_addhour_after = d_finish.AddMinutes(30);

                                            //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                            //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                            value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                            txtot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                                                                              //litDebug.Text += value_otafter.ToString() + "|";

                                            //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                            getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));
                                        }
                                        //else
                                        //{
                                        //    ddl_timeafter.Visible = false;
                                        //}



                                        _value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                        _value_diff_scanout = DateTime_ScanOut.Subtract(DateTime_ShiftEnd).TotalMinutes;

                                        //litDebug1.Text = _value_diff_scanout.ToString() + "|";

                                        if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn <= DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                        {

                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                            txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                            if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                            {

                                                d_finish_addhour_holiday = DateTime_ShiftEnd.AddMinutes(30);

                                                _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                                txtot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));


                                            }
                                        }
                                        else if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn >= DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                        {

                                            //check hour .xx
                                            string input_decimal_number_be = (Math.Floor((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60))).ToString(); //((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString();
                                            string input_decimal_number_af = ((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString("#.##");//"1.50";

                                            string decimal_places = String.Empty;


                                            ViewState["_value_timescan_late_add"] = "0";

                                            var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                                            if (regex.IsMatch(input_decimal_number_af))
                                            {
                                                decimal_places = regex.Match(input_decimal_number_af).Value;
                                                if (int.Parse(decimal_places) > 50)
                                                {

                                                    _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + 1);//Convert.ToDouble((input_decimal_number_be) + 1);
                                                    ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                    d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                    d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                                }
                                                else if (int.Parse(decimal_places) < 50)
                                                {

                                                    _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + .50);//Convert.ToDouble((input_decimal_number_be + .50) );

                                                    ViewState["_value_timescan_late_add"] = _value_timescan_late_add;


                                                    d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                    d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                                }
                                                else
                                                {
                                                    _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be))); //Convert.ToDouble((input_decimal_number_be));
                                                    ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                    d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                    d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));


                                                }

                                            }

                                            // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                            if (_value_diff_scanout >= 60)
                                            {
                                                value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                            }
                                            else
                                            {

                                                //value = Convert.ToDouble(((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1) );

                                                value = Convert.ToDouble(((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1) - Convert.ToDouble(ViewState["_value_timescan_late_add"].ToString()));

                                            }

                                            //litDebug1.Text += value.ToString();
                                            txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                            // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                            if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                            {
                                                //litDebug.Text = d_finish_addhour_holiday.ToString();
                                                //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                                //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                                //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                                d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                                _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                                txtot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));

                                            }
                                            else
                                            {
                                                getHourToOT(ddl_timeafter, Convert.ToDouble("0"));
                                            }


                                            /*
                                            //litDebug1.Text += DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm") + "1|";

                                            _value_timescan_late = Convert.ToDouble((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;
                                            _value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();

                                            //litDebug.Text = _value_timescan_late_add.ToString();

                                            d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                            d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                            // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                            if (_value_diff_scanout >= 60)
                                            {
                                                value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                            }
                                            else
                                            {
                                                value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                            }

                                            //litDebug1.Text += value.ToString();
                                            txtot_holiday.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                            // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                            if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                            {
                                                //litDebug.Text = d_finish_addhour_holiday.ToString();
                                                //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                                //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                                //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                                d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                                _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                                txtot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));

                                            }
                                            else
                                            {
                                                getHourToOT(ddl_timeafter, Convert.ToDouble("0"));
                                            }

                                            */

                                        }
                                        else if (DateTime_ScanOut < DateTime_ShiftEnd && (DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60))
                                        {

                                            //litDebug.Text += "333";

                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                            txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));
                                        }
                                        else //if (DateTime_ScanOut > DateTime_BreakEnd && DateTime_ScanOut < DateTime_ShiftEnd)
                                        {
                                            //litDebug.Text += "444";

                                            //litDebug1.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                            if (DateTime_ScanOut > DateTime_ScanIn)
                                            {
                                                //litDebug1.Text += "2" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");
                                                value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);

                                                txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));
                                            }
                                            else
                                            {
                                                //litDebug1.Text += "99" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");
                                                getHourToOT(ddl_timeafter, Convert.ToDouble("0"));
                                                getHourToOT(ddl_timeholiday, Convert.ToDouble("0"));
                                            }

                                        }

                                    }
                                    else if (DateTime_ScanIn < d_workstart && DateTime_ScanOut < d_finish)
                                    {
                                        //litDebug1.Text = "have case new";

                                        //total ot
                                        value_total_ot = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 60));
                                        value_total_otresult = (Math.Floor(value_total_ot * 2) / 2);
                                        //total ot

                                        if (value_total_otresult >= 9.5) //set -1 breaktime
                                        {

                                            value_total_ot_diff = value_total_otresult - 1;


                                            value_total_ot_holiday = 8;
                                            value_total_ot_before = value_total_ot_diff - value_total_ot_holiday;

                                            //litDebug.Text = value_total_ot_before.ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                            getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));

                                        }
                                        else if (value_total_otresult == 9)
                                        {
                                            value_total_ot_diff = value_total_otresult - 1;
                                            value_total_ot_holiday = 8;
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                        }
                                        else if (value_total_otresult >= 4 && value_total_otresult <= 5)
                                        {
                                            value_total_ot_before = 4;
                                            getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));
                                        }
                                        else if (value_total_otresult > 5 && value_total_otresult <= 8)
                                        {
                                            value_total_ot_diff = value_total_otresult - 1;

                                            value_total_ot_before = value_total_ot_diff;
                                            getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));
                                        }
                                    }
                                }


                                //set ot x1, x1.5, x2, x3
                                //set ot before

                                if (ddl_timebefore.SelectedValue != "" && DateTime_ScanOut > DateTime_ScanIn)
                                {

                                    lblot_before.Visible = true;
                                    ddl_timebefore.Visible = true;
                                    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                    {
                                        if (ddl_timeholiday.SelectedValue == "8")
                                        {
                                            lblot_before.Text = "x3";
                                        }
                                        else
                                        {
                                            lblot_before.Text = "x1";
                                        }
                                        //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                        //{
                                        //    lbl_hour_otbefore.Text = "x1";
                                        //}
                                        //else
                                        //{
                                        //    lbl_hour_otbefore.Text = "x2";
                                        //}
                                    }
                                    else
                                    {
                                        lblot_before.Text = "x1.5";
                                    }
                                }

                                //set ot after
                                if (ddl_timeafter.SelectedValue != "" && DateTime_ScanOut > DateTime_ScanIn)
                                {
                                    lblot_after.Visible = true;
                                    ddl_timeafter.Visible = true;
                                    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                    {
                                        if (ddl_timeholiday.SelectedValue == "8")
                                        {
                                            lblot_after.Text = "x3";
                                        }
                                        else
                                        {
                                            lblot_after.Text = "x1";
                                        }
                                        //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                        //{
                                        //    lbl_hour_otbefore.Text = "x1";
                                        //}
                                        //else
                                        //{
                                        //    lbl_hour_otbefore.Text = "x2";
                                        //}
                                    }
                                    else
                                    {
                                        lblot_after.Text = "x1.5";
                                    }
                                }


                                //set ot holiday
                                if (ddl_timeholiday.SelectedValue != "" && DateTime_ScanOut > DateTime_ScanIn)
                                {

                                    lblot_holiday.Visible = true;
                                    ddl_timeholiday.Visible = true;
                                    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                    {
                                        lblot_holiday.Text = "x1";

                                    }

                                }


                                if (lbl_holiday_idx_check.Text != "0")
                                {
                                    lbl_detailholiday.Visible = true;
                                    lbl_detaildayoff.Visible = false;
                                }
                                else if (day_offemployee.ToString() == "1")
                                {
                                    lbl_detaildayoff.Visible = true;
                                    lbl_detailholiday.Visible = false;
                                }


                            }
                            else
                            {
                                ddl_timebefore.Visible = false;
                                lblot_before.Visible = false;
                                ddl_timeholiday.Visible = false;
                                lblot_holiday.Visible = false;
                                ddl_timeafter.Visible = false;
                                lblot_after.Visible = false;
                            }



                        }
                        //else
                        //{
                        //    ddl_timebefore.Visible = false;
                        //    lblot_before.Visible = false;
                        //    ddl_timeholiday.Visible = false;
                        //    lblot_holiday.Visible = false;
                        //    ddl_timeafter.Visible = false;
                        //    lblot_after.Visible = false;
                        //}


                    }

                    count_end++;


                    Label lit_total_hoursot_before = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");
                    Label lit_total_hoursot_after = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");
                    Label lit_total_hoursot_holiday = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");
                    if (chk_date_ot.Checked)
                    {


                        if (ddl_timebefore.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timebefore.SelectedValue) >= 0.5)
                            {
                                totactual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                                //Label lit_total_hoursot_before_ = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");

                            }
                        }

                        if (ddl_timeafter.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeafter.SelectedValue) >= 0.5)
                            {
                                totactual_otafter += Convert.ToDecimal(ddl_timeafter.SelectedValue);
                                //Label lit_total_hoursot_after_ = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");

                            }
                        }

                        if (ddl_timeholiday.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeholiday.SelectedValue) >= 0.5)
                            {

                                //litDebug.Text += "1" + "|";
                                totactual_otholiday += Convert.ToDecimal(ddl_timeholiday.SelectedValue);
                                //Label lit_total_hoursot_holiday_ = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");

                            }
                        }

                        lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                        lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                        lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);


                    }




                    //set ot x1, x1.5, x2, x3
                    //set ot before

                    //if (ddl_timebefore.SelectedValue != "")
                    //{

                    //    lblot_before.Visible = true;
                    //    ddl_timebefore.Visible = true;
                    //    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                    //    {
                    //        if (ddl_timeholiday.SelectedValue == "8")
                    //        {
                    //            lblot_before.Text = "x3";
                    //        }
                    //        else
                    //        {
                    //            lblot_before.Text = "x1";
                    //        }
                    //        //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                    //        //{
                    //        //    lbl_hour_otbefore.Text = "x1";
                    //        //}
                    //        //else
                    //        //{
                    //        //    lbl_hour_otbefore.Text = "x2";
                    //        //}
                    //    }
                    //    else
                    //    {
                    //        lblot_before.Text = "x1.5";
                    //    }
                    //}
                    //else
                    //{
                    //    lblot_before.Visible = false;
                    //    ddl_timebefore.Visible = false;
                    //}

                    ////set ot after
                    //if (ddl_timeafter.SelectedValue != "")
                    //{
                    //    lblot_after.Visible = true;
                    //    ddl_timeafter.Visible = true;
                    //    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                    //    {
                    //        if (ddl_timeholiday.SelectedValue == "8")
                    //        {
                    //            lblot_after.Text = "x3";
                    //        }
                    //        else
                    //        {
                    //            lblot_after.Text = "x1";
                    //        }
                    //        //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                    //        //{
                    //        //    lbl_hour_otbefore.Text = "x1";
                    //        //}
                    //        //else
                    //        //{
                    //        //    lbl_hour_otbefore.Text = "x2";
                    //        //}
                    //    }
                    //    else
                    //    {
                    //        lblot_after.Text = "x1.5";
                    //    }
                    //}
                    //else
                    //{
                    //    lblot_after.Visible = false;
                    //    ddl_timeafter.Visible = false;
                    //}

                    ////set ot holiday
                    //if (ddl_timeholiday.SelectedValue != "")
                    //{

                    //    lblot_holiday.Visible = true;
                    //    ddl_timeholiday.Visible = true;
                    //    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                    //    {
                    //        lblot_holiday.Text = "x1";

                    //    }

                    //}
                    //else
                    //{
                    //    lblot_holiday.Visible = false;
                    //    ddl_timeholiday.Visible = false;
                    //}


                }

                break;

            case "ddl_timestart_edit":

                int count_edit = 0;
                foreach (GridViewRow row in GvViewEditDetailOTMont.Rows)
                {
                    CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot_edit");

                    Label lbl_date_start = (Label)row.FindControl("lbl_date_start_edit");
                    Label lbl_holiday_idx_check = (Label)row.FindControl("lbl_holiday_idx_check_edit");
                    Label lbl_work_start = (Label)row.FindControl("lbl_work_start_edit");
                    Label lbl_work_finish = (Label)row.FindControl("lbl_work_finish_edit");
                    Label lbl_break_start = (Label)row.FindControl("lbl_break_start_edit");
                    Label lbl_break_end = (Label)row.FindControl("lbl_break_finish_edit");

                    Label lbl_day_off = (Label)row.FindControl("lbl_day_off_edit");
                    Label lbl_date_condition = (Label)row.FindControl("lbl_date_condition_edit");

                    //TextBox txt_sum_hour = (TextBox)row.FindControl("txt_sum_hour");
                    ////TextBox txt_sum_hour = (TextBox)e.Row.FindControl("txt_sum_hour");
                    TextBox txt_job = (TextBox)row.FindControl("txt_job_edit");
                    TextBox txt_remark = (TextBox)row.FindControl("txt_remark_edit");
                    //TextBox txt_head_set = (TextBox)row.FindControl("txt_head_set");

                    Label txtot_before = (Label)row.FindControl("txtot_before_edit");
                    Label txtot_after = (Label)row.FindControl("txtot_after_edit");
                    Label txtot_holiday = (Label)row.FindControl("txtot_holiday_edit");
                    Label lblot_before = (Label)row.FindControl("lblot_before_edit");
                    Label lblot_after = (Label)row.FindControl("lblot_after_edit");
                    Label lblot_holiday = (Label)row.FindControl("lblot_holiday_edit");
                    Label lbl_detailholiday = (Label)row.FindControl("lbl_detailholiday_edit");
                    // bind time start //

                    DropDownList ddl_timestart = (DropDownList)row.FindControl("ddl_timestart_edit");
                    DropDownList ddl_timeend = (DropDownList)row.FindControl("ddl_timeend_edit");

                    DropDownList ddl_timebefore = (DropDownList)row.FindControl("ddl_timebefore_edit");
                    DropDownList ddl_timeafter = (DropDownList)row.FindControl("ddl_timeafter_edit");
                    DropDownList ddl_timeholiday = (DropDownList)row.FindControl("ddl_timeholiday_edit");


                    var d_start = new DateTime();
                    var d_end = new DateTime();
                    var d_finish = new DateTime();
                    var d_workstart = new DateTime();

                    double value_total_ot;
                    double value_total_otresult;

                    double value_total_ot_holiday;
                    double value_total_ot_before;
                    double value_total_ot_diff;


                    IFormatProvider culture_ = new CultureInfo("en-US", true);

                    d_finish = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);
                    d_workstart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_start.Text), "dd/MM/yyyy HH:mm", culture_);

                    d_start = DateTime.ParseExact((lbl_date_start.Text + " " + "00:00"), "dd/MM/yyyy HH:mm", culture_);
                    d_end = DateTime.ParseExact((lbl_date_start.Text + " " + "05:59"), "dd/MM/yyyy HH:mm", culture_);

                    double value_otbefore;
                    double value_otafter;
                    DateTime d_finish_addhour_after = new DateTime();
                    string day_offemployee = "";
                    //litDebug.Text += lbl_date_start.Text + "|";


                    if (row.RowType == DataControlRowType.DataRow)
                    {

                        //day off employee
                        string day_off = lbl_day_off.Text;
                        string day_condition = lbl_date_condition.Text;
                        string[] set_test = day_off.Split(',');


                        if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                        {
                            day_offemployee = "1"; //yes day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                                                   //litDebug.Text += "1" + "|";
                        }
                        else
                        {
                            day_offemployee = "2"; // no day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                        }
                        //day off employee

                        IFormatProvider culture = new CultureInfo("en-US", true);

                        //litDebug.Text += lbl_break_start.Text;

                        if (ddl_timeend.SelectedValue != "" && ddl_timestart.SelectedValue != "")
                        {


                            DateTime DateTime_ScanIn = DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ScanOut = DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);


                            DateTime DateTime_ShiftStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_work_start.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ShiftEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_work_finish.Text, "dd/MM/yyyy HH:mm", culture_);

                            DateTime DateTime_BreakStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_start.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_BreakEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_end.Text, "dd/MM/yyyy HH:mm", culture_);

                            double value;
                            double _value_diffbreaktime;
                            decimal _value_timescan_late;
                            double _value_timescan_late_add;

                            double _value_otafter_holiday;

                            DateTime d_finish_addhour = new DateTime();
                            DateTime d_start_addhour = new DateTime();

                            DateTime d_finish_addhour_holiday = new DateTime();
                            double _value_diff_scanout;
                            /*
                            
                            litDebug1.Text += DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";
                            DateTime DateTime_BreakStart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);

                            */

                            //ot before
                            value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                            if (d_workstart.Subtract(DateTime_ScanIn).TotalMinutes >= 30)
                            {

                                txtot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));
                            }
                            else
                            {
                                getHourToOT(ddl_timebefore, 0);
                            }





                            //ot after
                            if (DateTime_ScanOut > DateTime_ScanIn)
                            {

                                d_finish_addhour_after = d_finish.AddMinutes(30);

                                //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                txtot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                //litDebug.Text += value_otafter.ToString() + "|";

                                //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));
                            }
                            else
                            {
                                ddl_timeafter.Visible = false;
                            }


                            //ot holiday
                            if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1")
                            {
                                //lbl_detailholiday.Visible = true;
                                //if ((DateTime_ScanIn <= DateTime_ShiftStart || DateTime_ScanIn >= DateTime_ShiftStart) && DateTime_ScanOut >= DateTime_ShiftEnd)
                                if ((DateTime_ScanIn <= DateTime_ShiftStart || DateTime_ScanIn >= DateTime_ShiftStart))
                                {
                                    //litDebug.Text += (Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString())/30) * 0.50).ToString()) + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|"  + DateTime_ShiftEnd.ToString("dd/MM/yyyy HH:mm");
                                    ////decimal value = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                    ////litDebug1.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                    //litDebug.Text += _value_diffbreaktime.ToString() + "|";

                                    //ot before
                                    value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                    if (d_workstart.Subtract(DateTime_ScanIn).TotalMinutes >= 30)
                                    {

                                        txtot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));
                                    }
                                    else
                                    {
                                        getHourToOT(ddl_timebefore, 0);
                                    }


                                    //ot after
                                    if (DateTime_ScanOut > DateTime_ScanIn)
                                    {

                                        d_finish_addhour_after = d_finish.AddMinutes(30);

                                        //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                        //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                        value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                        txtot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                                                                          //litDebug.Text += value_otafter.ToString() + "|";

                                        //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                        getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));
                                    }
                                    else
                                    {
                                        ddl_timeafter.Visible = false;
                                    }

                                    _value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                    _value_diff_scanout = DateTime_ScanOut.Subtract(DateTime_ShiftEnd).TotalMinutes;

                                    //litDebug1.Text = _value_diff_scanout.ToString() + "|";

                                    if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn < DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {



                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                        if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                        {

                                            d_finish_addhour_holiday = DateTime_ShiftEnd.AddMinutes(30);

                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                            txtot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));


                                        }
                                    }
                                    else if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn > DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {



                                        //check hour .xx
                                        string input_decimal_number_be = (Math.Floor((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60))).ToString(); //((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString();
                                        string input_decimal_number_af = ((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString("#.##");//"1.50";

                                        string decimal_places = String.Empty;


                                        ViewState["_value_timescan_late_add"] = "0";

                                        var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                                        if (regex.IsMatch(input_decimal_number_af))
                                        {
                                            decimal_places = regex.Match(input_decimal_number_af).Value;
                                            if (int.Parse(decimal_places) > 50)
                                            {

                                                _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + 1);//Convert.ToDouble((input_decimal_number_be) + 1);
                                                ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                            }
                                            else if (int.Parse(decimal_places) < 50)
                                            {

                                                _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + .50);//Convert.ToDouble((input_decimal_number_be + .50) );

                                                ViewState["_value_timescan_late_add"] = _value_timescan_late_add;


                                                d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                            }
                                            else
                                            {
                                                _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be))); //Convert.ToDouble((input_decimal_number_be));
                                                ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));


                                            }

                                        }

                                        if (_value_diff_scanout >= 60)
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }
                                        else
                                        {

                                            //value = Convert.ToDouble(((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1) );

                                            value = Convert.ToDouble(((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1) - Convert.ToDouble(ViewState["_value_timescan_late_add"].ToString()));

                                        }

                                        //litDebug1.Text += value.ToString();
                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                        if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                        {
                                            //litDebug.Text = d_finish_addhour_holiday.ToString();
                                            //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                            //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                            //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                            d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                            txtot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));

                                        }
                                        else
                                        {
                                            getHourToOT(ddl_timeafter, Convert.ToDouble("0"));
                                        }


                                        /*

                                        //litDebug.Text = DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");
                                        

                                        _value_timescan_late = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;
                                        _value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();

                                        //litDebug.Text = _value_timescan_late_add.ToString();

                                        d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                        if (_value_diff_scanout >= 60)
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }
                                        else
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }

                                        //litDebug1.Text += value.ToString();
                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                        if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                        {
                                            
                                            d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                            txtot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));

                                        }
                                        else
                                        {
                                            getHourToOT(ddl_timeafter, Convert.ToDouble("0"));
                                        }

                                        */

                                    }
                                    else if (DateTime_ScanOut < DateTime_ShiftEnd && DateTime_ScanIn >= DateTime_BreakStart)
                                    {
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));
                                    }
                                    else //if (DateTime_ScanOut > DateTime_BreakEnd && DateTime_ScanOut < DateTime_ShiftEnd)
                                    {
                                        //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);

                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));
                                    }

                                }
                                else if (DateTime_ScanIn < d_workstart && DateTime_ScanOut < d_finish)
                                {
                                    //total ot
                                    value_total_ot = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 60));
                                    value_total_otresult = (Math.Floor(value_total_ot * 2) / 2);
                                    //total ot

                                    if (value_total_otresult >= 9.5) //set -1 breaktime
                                    {

                                        value_total_ot_diff = value_total_otresult - 1;


                                        value_total_ot_holiday = 8;
                                        value_total_ot_before = value_total_ot_diff - value_total_ot_holiday;

                                        //litDebug.Text = value_total_ot_before.ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                        getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));

                                    }
                                    else if (value_total_otresult == 9)
                                    {
                                        value_total_ot_diff = value_total_otresult - 1;
                                        value_total_ot_holiday = 8;
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                    }
                                    else if (value_total_otresult >= 4 && value_total_otresult <= 5)
                                    {
                                        value_total_ot_before = 4;
                                        getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));
                                    }
                                    else if (value_total_otresult > 5 && value_total_otresult <= 8)
                                    {
                                        value_total_ot_diff = value_total_otresult - 1;

                                        value_total_ot_before = value_total_ot_diff;
                                        getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));
                                    }
                                }



                            }
                            else
                            {
                                lbl_detailholiday.Visible = false;
                                ddl_timeholiday.Visible = false;
                            }


                            //litDebug.Text += value_otbefore.ToString() + "|";
                            if (DateTime_ScanIn < DateTime_ScanOut)
                            {

                                ddl_timebefore.Visible = true;
                                lblot_before.Visible = true;
                                ddl_timeholiday.Visible = true;
                                lblot_holiday.Visible = true;
                                ddl_timeafter.Visible = true;
                                lblot_after.Visible = true;

                                //set ot x1, x1.5, x2, x3
                                //set ot before
                                if (ddl_timebefore.SelectedValue != "")
                                {

                                    lblot_before.Visible = true;
                                    ddl_timebefore.Visible = true;
                                    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                    {
                                        if (ddl_timeholiday.SelectedValue == "8")
                                        {
                                            lblot_before.Text = "x3";
                                        }
                                        else
                                        {
                                            lblot_before.Text = "x1";
                                        }
                                        //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                        //{
                                        //    lbl_hour_otbefore.Text = "x1";
                                        //}
                                        //else
                                        //{
                                        //    lbl_hour_otbefore.Text = "x2";
                                        //}
                                    }
                                    else
                                    {
                                        lblot_before.Text = "x1.5";
                                    }
                                }
                                else
                                {
                                    lblot_before.Visible = false;
                                    ddl_timebefore.Visible = false;
                                }

                                //set ot after
                                if (ddl_timeafter.SelectedValue != "")
                                {
                                    lblot_after.Visible = true;
                                    ddl_timeafter.Visible = true;
                                    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                    {
                                        if (ddl_timeholiday.SelectedValue == "8")
                                        {
                                            lblot_after.Text = "x3";
                                        }
                                        else
                                        {
                                            lblot_after.Text = "x1";
                                        }
                                        //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                        //{
                                        //    lbl_hour_otbefore.Text = "x1";
                                        //}
                                        //else
                                        //{
                                        //    lbl_hour_otbefore.Text = "x2";
                                        //}
                                    }
                                    else
                                    {
                                        lblot_after.Text = "x1.5";
                                    }
                                }
                                else
                                {
                                    lblot_after.Visible = false;
                                    ddl_timeafter.Visible = false;
                                }

                                //set ot holiday
                                if (ddl_timeholiday.SelectedValue != "")
                                {

                                    lblot_holiday.Visible = true;
                                    ddl_timeholiday.Visible = true;
                                    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                    {
                                        lblot_holiday.Text = "x1";

                                    }

                                }
                                else
                                {
                                    lblot_holiday.Visible = false;
                                    ddl_timeholiday.Visible = false;
                                }
                            }
                            else
                            {
                                ddl_timebefore.Visible = false;
                                lblot_before.Visible = false;
                                ddl_timeholiday.Visible = false;
                                lblot_holiday.Visible = false;
                                ddl_timeafter.Visible = false;
                                lblot_after.Visible = false;
                            }

                        }
                        else
                        {
                            ddl_timebefore.Visible = false;
                            lblot_before.Visible = false;
                            ddl_timeholiday.Visible = false;
                            lblot_holiday.Visible = false;
                            ddl_timeafter.Visible = false;
                            lblot_after.Visible = false;
                        }



                    }

                    count_edit++;



                    Label lit_total_hoursot_before = (Label)GvViewEditDetailOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_edit");
                    Label lit_total_hoursot_after = (Label)GvViewEditDetailOTMont.FooterRow.Cells[6].FindControl("lit_total_hoursot_after_edit");
                    Label lit_total_hoursot_holiday = (Label)GvViewEditDetailOTMont.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday_edit");
                    if (chk_date_ot.Checked)
                    {

                        if (ddl_timebefore.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timebefore.SelectedValue) >= 0.5)
                            {
                                totactual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                                //Label lit_total_hoursot_before_ = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");

                            }
                        }

                        if (ddl_timeafter.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeafter.SelectedValue) >= 0.5)
                            {
                                totactual_otafter += Convert.ToDecimal(ddl_timeafter.SelectedValue);
                                //Label lit_total_hoursot_after_ = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");

                            }
                        }

                        if (ddl_timeholiday.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeholiday.SelectedValue) >= 0.5)
                            {
                                totactual_otholiday += Convert.ToDecimal(ddl_timeholiday.SelectedValue);
                                //Label lit_total_hoursot_holiday_ = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");

                            }
                        }

                        lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                        lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                        lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);

                        //tot_actual_otbefore += Convert.ToDecimal(txtot_before.Text);
                        //tot_actual_otafter += Convert.ToDecimal(txtot_after.Text);
                        //tot_actual_otholiday += Convert.ToDecimal(txtot_holiday.Text);

                        ////Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                        ////lit_total_hoursmonth.Text = String.Format("{0:N2}", tot_actual_otmonth);

                        //lit_total_hoursot_before.Text = String.Format("{0:N2}", tot_actual_otbefore);
                        //lit_total_hoursot_after.Text = String.Format("{0:N2}", tot_actual_otafter);
                        //lit_total_hoursot_holiday.Text = String.Format("{0:N2}", tot_actual_otholiday);
                    }

                    ////set ot x1, x1.5, x2, x3
                    ////set ot before
                    //if (ddl_timebefore.SelectedValue != "")
                    //{

                    //    lblot_before.Visible = true;
                    //    ddl_timebefore.Visible = true;
                    //    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                    //    {
                    //        if (ddl_timeholiday.SelectedValue == "8")
                    //        {
                    //            lblot_before.Text = "x3";
                    //        }
                    //        else
                    //        {
                    //            lblot_before.Text = "x1";
                    //        }
                    //        //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                    //        //{
                    //        //    lbl_hour_otbefore.Text = "x1";
                    //        //}
                    //        //else
                    //        //{
                    //        //    lbl_hour_otbefore.Text = "x2";
                    //        //}
                    //    }
                    //    else
                    //    {
                    //        lblot_before.Text = "x1.5";
                    //    }
                    //}
                    //else
                    //{
                    //    lblot_before.Visible = false;
                    //    ddl_timebefore.Visible = false;
                    //}

                    ////set ot after
                    //if (ddl_timeafter.SelectedValue != "")
                    //{
                    //    lblot_after.Visible = true;
                    //    ddl_timeafter.Visible = true;
                    //    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                    //    {
                    //        if (ddl_timeholiday.SelectedValue == "8")
                    //        {
                    //            lblot_after.Text = "x3";
                    //        }
                    //        else
                    //        {
                    //            lblot_after.Text = "x1";
                    //        }
                    //        //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                    //        //{
                    //        //    lbl_hour_otbefore.Text = "x1";
                    //        //}
                    //        //else
                    //        //{
                    //        //    lbl_hour_otbefore.Text = "x2";
                    //        //}
                    //    }
                    //    else
                    //    {
                    //        lblot_after.Text = "x1.5";
                    //    }
                    //}
                    //else
                    //{
                    //    lblot_after.Visible = false;
                    //    ddl_timeafter.Visible = false;
                    //}

                    ////set ot holiday
                    //if (ddl_timeholiday.SelectedValue != "")
                    //{

                    //    lblot_holiday.Visible = true;
                    //    ddl_timeholiday.Visible = true;
                    //    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                    //    {
                    //        lblot_holiday.Text = "x1";

                    //    }

                    //}
                    //else
                    //{
                    //    lblot_holiday.Visible = false;
                    //    ddl_timeholiday.Visible = false;
                    //}


                }
                break;

            case "ddl_timeend_edit":

                int count_editend = 0;
                foreach (GridViewRow row in GvViewEditDetailOTMont.Rows)
                {
                    CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot_edit");

                    Label lbl_date_start = (Label)row.FindControl("lbl_date_start_edit");
                    Label lbl_holiday_idx_check = (Label)row.FindControl("lbl_holiday_idx_check_edit");
                    Label lbl_work_start = (Label)row.FindControl("lbl_work_start_edit");
                    Label lbl_work_finish = (Label)row.FindControl("lbl_work_finish_edit");
                    Label lbl_break_start = (Label)row.FindControl("lbl_break_start_edit");
                    Label lbl_break_end = (Label)row.FindControl("lbl_break_finish_edit");

                    Label lbl_day_off = (Label)row.FindControl("lbl_day_off_edit");
                    Label lbl_date_condition = (Label)row.FindControl("lbl_date_condition_edit");

                    //TextBox txt_sum_hour = (TextBox)row.FindControl("txt_sum_hour");
                    ////TextBox txt_sum_hour = (TextBox)e.Row.FindControl("txt_sum_hour");
                    TextBox txt_job = (TextBox)row.FindControl("txt_job_edit");
                    TextBox txt_remark = (TextBox)row.FindControl("txt_remark_edit");
                    //TextBox txt_head_set = (TextBox)row.FindControl("txt_head_set");

                    Label txtot_before = (Label)row.FindControl("txtot_before_edit");
                    Label txtot_after = (Label)row.FindControl("txtot_after_edit");
                    Label txtot_holiday = (Label)row.FindControl("txtot_holiday_edit");
                    Label lblot_before = (Label)row.FindControl("lblot_before_edit");
                    Label lblot_after = (Label)row.FindControl("lblot_after_edit");
                    Label lblot_holiday = (Label)row.FindControl("lblot_holiday_edit");
                    Label lbl_detailholiday = (Label)row.FindControl("lbl_detailholiday_edit");
                    // bind time start //

                    DropDownList ddl_timestart = (DropDownList)row.FindControl("ddl_timestart_edit");
                    DropDownList ddl_timeend = (DropDownList)row.FindControl("ddl_timeend_edit");

                    DropDownList ddl_timebefore = (DropDownList)row.FindControl("ddl_timebefore_edit");
                    DropDownList ddl_timeafter = (DropDownList)row.FindControl("ddl_timeafter_edit");
                    DropDownList ddl_timeholiday = (DropDownList)row.FindControl("ddl_timeholiday_edit");


                    var d_start = new DateTime();
                    var d_end = new DateTime();
                    var d_finish = new DateTime();
                    var d_workstart = new DateTime();

                    double value_total_ot;
                    double value_total_otresult;

                    double value_total_ot_holiday;
                    double value_total_ot_before;
                    double value_total_ot_diff;


                    IFormatProvider culture_ = new CultureInfo("en-US", true);

                    d_finish = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);
                    d_workstart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_start.Text), "dd/MM/yyyy HH:mm", culture_);

                    d_start = DateTime.ParseExact((lbl_date_start.Text + " " + "00:00"), "dd/MM/yyyy HH:mm", culture_);
                    d_end = DateTime.ParseExact((lbl_date_start.Text + " " + "05:59"), "dd/MM/yyyy HH:mm", culture_);

                    double value_otbefore;
                    double value_otafter;
                    DateTime d_finish_addhour_after = new DateTime();
                    string day_offemployee = "";
                    //litDebug.Text += lbl_date_start.Text + "|";

                    if (row.RowType == DataControlRowType.DataRow)
                    {

                        //day off employee
                        string day_off = lbl_day_off.Text;
                        string day_condition = lbl_date_condition.Text;
                        string[] set_test = day_off.Split(',');


                        if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                        {
                            day_offemployee = "1"; //yes day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                                                   //litDebug.Text += "1" + "|";
                        }
                        else
                        {
                            day_offemployee = "2"; // no day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                        }
                        //day off employee

                        IFormatProvider culture = new CultureInfo("en-US", true);

                        //litDebug.Text += lbl_break_start.Text;

                        if (ddl_timeend.SelectedValue != "" && ddl_timestart.SelectedValue != "")
                        {

                            DateTime DateTime_ScanIn = DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ScanOut = DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);


                            DateTime DateTime_ShiftStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_work_start.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ShiftEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_work_finish.Text, "dd/MM/yyyy HH:mm", culture_);

                            DateTime DateTime_BreakStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_start.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_BreakEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_end.Text, "dd/MM/yyyy HH:mm", culture_);

                            double value;
                            double _value_diffbreaktime;
                            decimal _value_timescan_late;
                            double _value_timescan_late_add;

                            double _value_otafter_holiday;

                            DateTime d_finish_addhour = new DateTime();
                            DateTime d_start_addhour = new DateTime();

                            DateTime d_finish_addhour_holiday = new DateTime();
                            double _value_diff_scanout;
                            /*
                            
                            litDebug1.Text += DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";
                            DateTime DateTime_BreakStart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);

                            */

                            //ot before
                            value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                            if (d_workstart.Subtract(DateTime_ScanIn).TotalMinutes >= 30)
                            {
                                txtot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));
                            }
                            else
                            {
                                getHourToOT(ddl_timebefore, 0);
                                //ddl_timebefore.Visible = false;
                            }

                            //ot after
                            if (DateTime_ScanOut > DateTime_ScanIn)
                            {

                                d_finish_addhour_after = d_finish.AddMinutes(30);

                                //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                txtot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                //litDebug.Text += value_otafter.ToString() + "|";

                                //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));
                            }
                            else
                            {
                                ddl_timeafter.Visible = false;
                            }


                            //ot holiday
                            if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1")
                            {
                                //lbl_detailholiday.Visible = true;
                                ////if ((DateTime_ScanIn < DateTime_ShiftStart || DateTime_ScanIn > DateTime_ShiftStart) && DateTime_ScanOut >= DateTime_ShiftEnd)
                                if ((DateTime_ScanIn < DateTime_ShiftStart || DateTime_ScanIn > DateTime_ShiftStart))
                                {
                                    //litDebug.Text += (Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString())/30) * 0.50).ToString()) + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|"  + DateTime_ShiftEnd.ToString("dd/MM/yyyy HH:mm");
                                    ////decimal value = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                    ////litDebug1.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                    //litDebug.Text += _value_diffbreaktime.ToString() + "|";

                                    //ot before
                                    value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                    //litDebug.Text += d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString() + "||" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm").ToString();

                                    if (d_workstart.Subtract(DateTime_ScanIn).TotalMinutes >= 30)
                                    {
                                        txtot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));
                                    }
                                    else
                                    {
                                        getHourToOT(ddl_timebefore, 0);
                                        //ddl_timebefore.Visible = false;

                                        //litDebug.Text = "2";
                                    }


                                    //ot after
                                    if (DateTime_ScanOut > DateTime_ScanIn)
                                    {

                                        d_finish_addhour_after = d_finish.AddMinutes(30);

                                        //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                        //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                        value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                        txtot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                                                                          //litDebug.Text += value_otafter.ToString() + "|";

                                        //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                        getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));
                                    }
                                    else
                                    {
                                        ddl_timeafter.Visible = false;
                                    }

                                    _value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                    _value_diff_scanout = DateTime_ScanOut.Subtract(DateTime_ShiftEnd).TotalMinutes;

                                    //litDebug1.Text = _value_diff_scanout.ToString() + "|";

                                    if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn < DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {



                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                        if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                        {

                                            d_finish_addhour_holiday = DateTime_ShiftEnd.AddMinutes(30);

                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                            txtot_after.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));


                                        }
                                    }
                                    else if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn > DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {

                                        //check hour .xx
                                        string input_decimal_number_be = (Math.Floor((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60))).ToString(); //((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString();
                                        string input_decimal_number_af = ((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString("#.##");//"1.50";

                                        string decimal_places = String.Empty;


                                        ViewState["_value_timescan_late_add"] = "0";

                                        var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                                        if (regex.IsMatch(input_decimal_number_af))
                                        {
                                            decimal_places = regex.Match(input_decimal_number_af).Value;
                                            if (int.Parse(decimal_places) > 50)
                                            {

                                                _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + 1);//Convert.ToDouble((input_decimal_number_be) + 1);
                                                ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                            }
                                            else if (int.Parse(decimal_places) < 50)
                                            {

                                                _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + .50);//Convert.ToDouble((input_decimal_number_be + .50) );

                                                ViewState["_value_timescan_late_add"] = _value_timescan_late_add;


                                                d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                            }
                                            else
                                            {
                                                _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be))); //Convert.ToDouble((input_decimal_number_be));
                                                ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));


                                            }

                                        }

                                        if (_value_diff_scanout >= 60)
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }
                                        else
                                        {

                                            //value = Convert.ToDouble(((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1) );

                                            value = Convert.ToDouble(((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1) - Convert.ToDouble(ViewState["_value_timescan_late_add"].ToString()));

                                        }

                                        //litDebug1.Text += value.ToString();
                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                        if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                        {
                                            //litDebug.Text = d_finish_addhour_holiday.ToString();
                                            //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                            //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                            //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                            d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                            txtot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));

                                        }
                                        else
                                        {
                                            getHourToOT(ddl_timeafter, Convert.ToDouble("0"));
                                        }

                                        /*
                                        //litDebug.Text = DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                        _value_timescan_late = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;
                                        _value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();

                                        //litDebug.Text = _value_timescan_late_add.ToString();

                                        d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                        if (_value_diff_scanout >= 60)
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }
                                        else
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }

                                        //litDebug1.Text += value.ToString();
                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                        if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                        {
                                            //litDebug.Text = d_finish_addhour_holiday.ToString();
                                            //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                            //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                            //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                            d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                            _value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                            txtot_after.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));

                                        }
                                        else
                                        {
                                            getHourToOT(ddl_timeafter, Convert.ToDouble("0"));
                                        }

                                        */

                                    }
                                    else if (DateTime_ScanOut < DateTime_ShiftEnd && DateTime_ScanIn >= DateTime_BreakStart)
                                    {
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));
                                    }
                                    else //if (DateTime_ScanOut > DateTime_BreakEnd && DateTime_ScanOut < DateTime_ShiftEnd)
                                    {
                                        //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                        ////value = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);

                                        ////txtot_holiday.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        ////getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                        if (DateTime_ScanOut > DateTime_ScanIn)
                                        {
                                            //litDebug1.Text += "2" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);

                                            txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));
                                        }
                                        else
                                        {
                                            //litDebug1.Text += "99" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");
                                            getHourToOT(ddl_timeafter, Convert.ToDouble("0"));
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble("0"));
                                        }


                                    }

                                }
                                else if (DateTime_ScanIn < d_workstart && DateTime_ScanOut < d_finish)
                                {
                                    //litDebug1.Text = "have case new";

                                    //total ot
                                    value_total_ot = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 60));
                                    value_total_otresult = (Math.Floor(value_total_ot * 2) / 2);
                                    //total ot

                                    if (value_total_otresult >= 9.5) //set -1 breaktime
                                    {

                                        value_total_ot_diff = value_total_otresult - 1;


                                        value_total_ot_holiday = 8;
                                        value_total_ot_before = value_total_ot_diff - value_total_ot_holiday;

                                        //litDebug.Text = value_total_ot_before.ToString();
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                        getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));

                                    }
                                    else if (value_total_otresult == 9)
                                    {
                                        value_total_ot_diff = value_total_otresult - 1;
                                        value_total_ot_holiday = 8;
                                        getHourToOT(ddl_timeholiday, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                    }
                                    else if (value_total_otresult >= 4 && value_total_otresult <= 5)
                                    {
                                        value_total_ot_before = 4;
                                        getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));
                                    }
                                    else if (value_total_otresult > 5 && value_total_otresult <= 8)
                                    {
                                        value_total_ot_diff = value_total_otresult - 1;

                                        value_total_ot_before = value_total_ot_diff;
                                        getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));
                                    }
                                }

                            }
                            else
                            {
                                lbl_detailholiday.Visible = false;
                                ddl_timeholiday.Visible = false;
                            }


                            //litDebug.Text += value_otbefore.ToString() + "|";
                            if (DateTime_ScanOut > DateTime_ScanIn)
                            {

                                //ddl_timebefore.Visible = true;
                                //lblot_before.Visible = true;
                                //ddl_timeholiday.Visible = true;
                                //lblot_holiday.Visible = true;
                                //ddl_timeafter.Visible = true;
                                //lblot_after.Visible = true;


                                //set ot x1, x1.5, x2, x3
                                //set ot before
                                if (ddl_timebefore.SelectedValue != "")
                                {

                                    lblot_before.Visible = true;
                                    ddl_timebefore.Visible = true;
                                    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                    {
                                        if (ddl_timeholiday.SelectedValue == "8")
                                        {
                                            lblot_before.Text = "x3";
                                        }
                                        else
                                        {
                                            lblot_before.Text = "x1";
                                        }
                                        //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                        //{
                                        //    lbl_hour_otbefore.Text = "x1";
                                        //}
                                        //else
                                        //{
                                        //    lbl_hour_otbefore.Text = "x2";
                                        //}
                                    }
                                    else
                                    {
                                        lblot_before.Text = "x1.5";
                                    }
                                }
                                else
                                {
                                    lblot_before.Visible = false;
                                    ddl_timebefore.Visible = false;
                                }

                                //set ot after
                                if (ddl_timeafter.SelectedValue != "")
                                {
                                    lblot_after.Visible = true;
                                    ddl_timeafter.Visible = true;
                                    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                    {
                                        if (ddl_timeholiday.SelectedValue == "8")
                                        {
                                            lblot_after.Text = "x3";
                                        }
                                        else
                                        {
                                            lblot_after.Text = "x1";
                                        }
                                        //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                        //{
                                        //    lbl_hour_otbefore.Text = "x1";
                                        //}
                                        //else
                                        //{
                                        //    lbl_hour_otbefore.Text = "x2";
                                        //}
                                    }
                                    else
                                    {
                                        lblot_after.Text = "x1.5";
                                    }
                                }
                                else
                                {
                                    lblot_after.Visible = false;
                                    ddl_timeafter.Visible = false;
                                }

                                //set ot holiday
                                if (ddl_timeholiday.SelectedValue != "")
                                {

                                    lblot_holiday.Visible = true;
                                    ddl_timeholiday.Visible = true;
                                    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                    {
                                        lblot_holiday.Text = "x1";

                                    }

                                }
                                else
                                {
                                    lblot_holiday.Visible = false;
                                    ddl_timeholiday.Visible = false;
                                }
                            }
                            else
                            {
                                ddl_timebefore.Visible = false;
                                lblot_before.Visible = false;
                                ddl_timeholiday.Visible = false;
                                lblot_holiday.Visible = false;
                                ddl_timeafter.Visible = false;
                                lblot_after.Visible = false;
                            }

                        }
                        else
                        {
                            ddl_timebefore.Visible = false;
                            lblot_before.Visible = false;
                            ddl_timeholiday.Visible = false;
                            lblot_holiday.Visible = false;
                            ddl_timeafter.Visible = false;
                            lblot_after.Visible = false;
                        }


                    }

                    count_editend++;

                    Label lit_total_hoursot_before = (Label)GvViewEditDetailOTMont.FooterRow.Cells[5].FindControl("lit_total_hoursot_before_edit");
                    Label lit_total_hoursot_after = (Label)GvViewEditDetailOTMont.FooterRow.Cells[6].FindControl("lit_total_hoursot_after_edit");
                    Label lit_total_hoursot_holiday = (Label)GvViewEditDetailOTMont.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday_edit");

                    if (chk_date_ot.Checked)
                    {

                        if (ddl_timebefore.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timebefore.SelectedValue) >= 0.5)
                            {
                                totactual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                                //Label lit_total_hoursot_before_ = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");

                            }
                        }

                        if (ddl_timeafter.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeafter.SelectedValue) >= 0.5)
                            {
                                totactual_otafter += Convert.ToDecimal(ddl_timeafter.SelectedValue);
                                //Label lit_total_hoursot_after_ = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");

                            }
                        }

                        if (ddl_timeholiday.SelectedValue != "")
                        {
                            if (Convert.ToDouble(ddl_timeholiday.SelectedValue) >= 0.5)
                            {
                                totactual_otholiday += Convert.ToDecimal(ddl_timeholiday.SelectedValue);
                                //Label lit_total_hoursot_holiday_ = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");

                            }
                        }

                        lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                        lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                        lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);

                        //tot_actual_otbefore += Convert.ToDecimal(txtot_before.Text);
                        //tot_actual_otafter += Convert.ToDecimal(txtot_after.Text);
                        //tot_actual_otholiday += Convert.ToDecimal(txtot_holiday.Text);

                        ////Label lit_total_hoursmonth = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursmonth");
                        ////lit_total_hoursmonth.Text = String.Format("{0:N2}", tot_actual_otmonth);

                        //lit_total_hoursot_before.Text = String.Format("{0:N2}", tot_actual_otbefore);
                        //lit_total_hoursot_after.Text = String.Format("{0:N2}", tot_actual_otafter);
                        //lit_total_hoursot_holiday.Text = String.Format("{0:N2}", tot_actual_otholiday);
                    }

                    ////set ot x1, x1.5, x2, x3
                    ////set ot before
                    //if (ddl_timebefore.SelectedValue != "")
                    //{

                    //    lblot_before.Visible = true;
                    //    ddl_timebefore.Visible = true;
                    //    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                    //    {
                    //        if (ddl_timeholiday.SelectedValue == "8")
                    //        {
                    //            lblot_before.Text = "x3";
                    //        }
                    //        else
                    //        {
                    //            lblot_before.Text = "x1";
                    //        }
                    //        //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                    //        //{
                    //        //    lbl_hour_otbefore.Text = "x1";
                    //        //}
                    //        //else
                    //        //{
                    //        //    lbl_hour_otbefore.Text = "x2";
                    //        //}
                    //    }
                    //    else
                    //    {
                    //        lblot_before.Text = "x1.5";
                    //    }
                    //}
                    //else
                    //{
                    //    lblot_before.Visible = false;
                    //    ddl_timebefore.Visible = false;
                    //}

                    ////set ot after
                    //if (ddl_timeafter.SelectedValue != "")
                    //{
                    //    lblot_after.Visible = true;
                    //    ddl_timeafter.Visible = true;
                    //    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                    //    {
                    //        if (ddl_timeholiday.SelectedValue == "8")
                    //        {
                    //            lblot_after.Text = "x3";
                    //        }
                    //        else
                    //        {
                    //            lblot_after.Text = "x1";
                    //        }
                    //        //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                    //        //{
                    //        //    lbl_hour_otbefore.Text = "x1";
                    //        //}
                    //        //else
                    //        //{
                    //        //    lbl_hour_otbefore.Text = "x2";
                    //        //}
                    //    }
                    //    else
                    //    {
                    //        lblot_after.Text = "x1.5";
                    //    }
                    //}
                    //else
                    //{
                    //    lblot_after.Visible = false;
                    //    ddl_timeafter.Visible = false;
                    //}

                    ////set ot holiday
                    //if (ddl_timeholiday.SelectedValue != "")
                    //{

                    //    lblot_holiday.Visible = true;
                    //    ddl_timeholiday.Visible = true;
                    //    if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                    //    {
                    //        lblot_holiday.Text = "x1";

                    //    }

                    //}
                    //else
                    //{
                    //    lblot_holiday.Visible = false;
                    //    ddl_timeholiday.Visible = false;
                    //}


                }
                break;


            case "ddlorg":

                getDepartmentList(ddlrdept, int.Parse(ddlorg.SelectedValue));

                ddlrsec.Items.Clear();
                ddlrsec.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

                break;
            case "ddlrdept":

                getSectionList(ddlrsec, int.Parse(ddlorg.SelectedItem.Value), int.Parse(ddlrdept.SelectedItem.Value));
                break;
            case "ddlorgMonthReport":

                getDepartmentList(ddlrdeptMonthReport, int.Parse(ddlorgMonthReport.SelectedValue));

                ddlrsecMonthReport.Items.Clear();
                ddlrsecMonthReport.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

                ddlempMonthReport.Items.Clear();
                ddlempMonthReport.Items.Insert(0, new ListItem("-- เลือกพนักงาน --", "0"));

                break;
            case "ddlrdeptMonthReport":

                getSectionList(ddlrsecMonthReport, int.Parse(ddlorgMonthReport.SelectedItem.Value), int.Parse(ddlrdeptMonthReport.SelectedItem.Value));


                ddlempMonthReport.Items.Clear();
                ddlempMonthReport.Items.Insert(0, new ListItem("-- เลือกพนักงาน --", "0"));

                break;
            case "ddlrsecMonthReport":
                getEmpList(ddlempMonthReport, int.Parse(ddlorgMonthReport.SelectedValue), int.Parse(ddlrdeptMonthReport.SelectedValue), int.Parse(ddlrsecMonthReport.SelectedValue));
                //getSectionList(ddlrsec_report, int.Parse(ddlorg_report.SelectedItem.Value), int.Parse(ddlrdept_report.SelectedItem.Value));
                break;

            case "ddlSearchorg":

                getDepartmentList(ddlSearchrdept, int.Parse(ddlSearchorg.SelectedValue));

                ddlSearchrsec.Items.Clear();
                ddlSearchrsec.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

                ddlSearchemp.Items.Clear();
                ddlSearchemp.Items.Insert(0, new ListItem("-- เลือกพนักงาน --", "0"));

                break;
            case "ddlSearchrdept":

                getSectionList(ddlSearchrsec, int.Parse(ddlSearchorg.SelectedItem.Value), int.Parse(ddlSearchrdept.SelectedItem.Value));


                ddlSearchemp.Items.Clear();
                ddlSearchemp.Items.Insert(0, new ListItem("-- เลือกพนักงาน --", "0"));

                break;
            case "ddlSearchrsec":
                getEmpList(ddlSearchemp, int.Parse(ddlSearchorg.SelectedValue), int.Parse(ddlSearchrdept.SelectedValue), int.Parse(ddlSearchrsec.SelectedValue));
                //getSectionList(ddlrsec_report, int.Parse(ddlorg_report.SelectedItem.Value), int.Parse(ddlrdept_report.SelectedItem.Value));
                break;
            case "ddlSearchorg_WaitApprove":

                getDepartmentList(ddlSearch_rdept_WaitApprove, int.Parse(ddlSearchorg_WaitApprove.SelectedValue));

                ddlSearch_rsec_WaitApprove.Items.Clear();
                ddlSearch_rsec_WaitApprove.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

                ddlSearch_emp_WaitApprove.Items.Clear();
                ddlSearch_emp_WaitApprove.Items.Insert(0, new ListItem("-- เลือกพนักงาน --", "0"));

                break;
            case "ddlSearch_rdept_WaitApprove":

                getSectionList(ddlSearch_rsec_WaitApprove, int.Parse(ddlSearchorg_WaitApprove.SelectedItem.Value), int.Parse(ddlSearch_rdept_WaitApprove.SelectedItem.Value));


                ddlSearch_emp_WaitApprove.Items.Clear();
                ddlSearch_emp_WaitApprove.Items.Insert(0, new ListItem("-- เลือกพนักงาน --", "0"));

                break;
            case "ddlSearch_rsec_WaitApprove":
                getEmpList(ddlSearch_emp_WaitApprove, int.Parse(ddlSearchorg_WaitApprove.SelectedValue), int.Parse(ddlSearch_rdept_WaitApprove.SelectedValue), int.Parse(ddlSearch_rsec_WaitApprove.SelectedValue));
                //getSectionList(ddlrsec_report, int.Parse(ddlorg_report.SelectedItem.Value), int.Parse(ddlrdept_report.SelectedItem.Value));
                break;

            case "ddlorg_report":

                getDepartmentList(ddlrdept_report, int.Parse(ddlorg_report.SelectedValue));

                ddlrsec_report.Items.Clear();
                ddlrsec_report.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

                break;
            case "ddlrdept_report":

                getSectionList(ddlrsec_report, int.Parse(ddlorg_report.SelectedItem.Value), int.Parse(ddlrdept_report.SelectedItem.Value));
                break;

        }
    }

    protected void TextBoxChanged(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;

        switch (txtName.ID)
        {

            ////case "txt_timeend_job":

            ////    //linkBtnTrigger(btnSaveDetailMA);

            ////    foreach (GridViewRow row in GvCreateOTMonth.Rows)
            ////    {
            ////        CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");
            ////        UpdatePanel Update_Timestart = (UpdatePanel)row.FindControl("Update_Timestart");
            ////        UpdatePanel Update_Timeend = (UpdatePanel)row.FindControl("Update_Timeend");
            ////        TextBox txt_job = (TextBox)row.FindControl("txt_job");
            ////        TextBox txt_timestart_job = (TextBox)row.FindControl("txt_timestart_job");
            ////        TextBox txt_timeend_job = (TextBox)row.FindControl("txt_timeend_job");
            ////        TextBox txt_sum_hour = (TextBox)row.FindControl("txt_sum_hour");
            ////        TextBox txt_head_set = (TextBox)row.FindControl("txt_head_set");
            ////        TextBox txt_remark = (TextBox)row.FindControl("txt_remark");
            ////        decimal total_hour = 0;


            ////        //TimeSpan diff = secondDate - firstDate;
            ////        //double hours = diff.TotalHours;

            ////        litDebug.Text = "3333";

            ////        if (chk_date_ot.Checked)
            ////        {
            ////            if (txt_timestart_job.Text != "" && txt_timeend_job.Text != "")
            ////            {
            ////                //linkBtnTrigger(btnSaveDetailMA);
            ////                //litDebug.Text = Convert.ToInt32(txt_Quantity_txt.Text).ToString();
            ////                total_hour = Convert.ToDecimal(txt_timestart_job.Text) * Convert.ToDecimal(txt_timeend_job.Text);
            ////                //litDebug.Text = Convert.ToDecimal(total_price).ToString();
            ////                txt_sum_hour.Text = Convert.ToDecimal(total_hour).ToString();

            ////            }
            ////            else
            ////            {
            ////                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรอกราคา/หน่วย ---');", true);

            ////                break;
            ////            }
            ////        }


            ////    }

            ////    break;




        }

    }

    #endregion Changed

    #region gridview
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "GvDetailToEmployeeCreateAdmin":
                setGridData(GvDetailToEmployeeCreateAdmin, ViewState["Vs_GvDetailToEmployeeCreateAdmin"]);

                break;
            case "GvWaitDetailOTMonth":
                setGridData(GvWaitDetailOTMonth, ViewState["Vs_WaitApproveDetailOTMonth"]);

                break;

            case "GvDetailAdminOTDay":
                setGridData(GvDetailAdminOTDay, ViewState["Vs_DetailAdminOTDay"]);

                break;

            case "GvCreateOTMonth":
                setGridData(GvCreateOTMonth, ViewState["Vs_DateOTOfMonth"]);

                break;
            case "GvDetailOTMont":

                setGridData(GvDetailOTMont, ViewState["Vs_DetailOT"]);
                //setOntop.Focus();

                break;
            case "GvViewDetailHeadSetOTMont":
                setGridData(GvViewDetailHeadSetOTMont, ViewState["Vs_ViewDetailOTMonth"]);
                //setOntop.Focus();

                break;
            case "GvEmployeeSetOTMonth":
                ////ovt_employeeset_otmonth_detail[] _tempList = (ovt_employeeset_otmonth_detail[])ViewState["Vs_EmployeeSetOTMonth"];
                ////setGridData(GvEmployeeSetOTMonth, _tempList);

                break;
            case "GvEmployee_Report":
                GvEmployee_Report.PageIndex = e.NewPageIndex;
                GvEmployee_Report.DataBind();
                //Select_Employee_report_search(); 
                break;
            case "GvDetailEmployeeImportOTDay":

                setGridData(GvDetailEmployeeImportOTDay, ViewState["Vs_DetailOTImportEmployeeDay"]);
                GvDetailEmployeeImportOTDay.Focus();

                break;
            case "GvSearchReportOTDay":

                setGridData(GvSearchReportOTDay, ViewState["Vs_SearchReportDetailEmployeeDay"]);
                GvSearchReportOTDay.Focus();

                break;
            case "GvViewDetailEmployeeImportDay":
                setGridData(GvViewDetailEmployeeImportDay, ViewState["Vs_ViewDetailEmployeeImportDay"]);

                break;

        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvViewDetailAdminOTDay":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lbl_shift_timestart_waitapprove = (Label)e.Row.FindControl("lbl_shift_timestart_view");
                    Label lbl_shift_timeend_waitapprove = (Label)e.Row.FindControl("lbl_shift_timeend_view");
                    Label lbl_shift_timestart_otscan_waitapprove = (Label)e.Row.FindControl("lbl_shift_timestart_otscan_view");
                    Label lbl_shift_timeend_otscan_waitapprove = (Label)e.Row.FindControl("lbl_shift_timeend_otscan_view");

                    Label lbl_parttime_day_off_detail_view = (Label)e.Row.FindControl("lbl_parttime_day_off_detail_view");
                    //Label lbl_parttime_workdays_waitapprove = (Label)e.Row.FindControl("lbl_parttime_workdays_detail_view");
                    Label lbl_date_condition_waitapprove = (Label)e.Row.FindControl("lbl_date_condition_detail_view");
                    Label lbl_holiday_idx_waitapprove = (Label)e.Row.FindControl("lbl_holiday_idx_detail_view");

                    Label lbl_shift_otbefore_waitapprove = (Label)e.Row.FindControl("lbl_shift_otbefore_view");
                    Label lbl_shift_otafter_waitapprove = (Label)e.Row.FindControl("lbl_shift_otafter_view");
                    Label lbl_shift_otholiday_waitapprove = (Label)e.Row.FindControl("lbl_shift_otholiday_view");

                    // set date defualt //
                    if (lbl_shift_timestart_waitapprove.Text == "01/01/1900 00:00")
                    {
                        lbl_shift_timestart_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_timestart_waitapprove.Visible = true;
                    }

                    if (lbl_shift_timeend_waitapprove.Text == "01/01/1900 00:00")
                    {
                        lbl_shift_timeend_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_timeend_waitapprove.Visible = true;
                    }

                    if (lbl_shift_timestart_otscan_waitapprove.Text == "01/01/1900 00:00")
                    {
                        lbl_shift_timestart_otscan_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_timestart_otscan_waitapprove.Visible = true;
                    }

                    if (lbl_shift_timeend_otscan_waitapprove.Text == "01/01/1900 00:00")
                    {
                        lbl_shift_timeend_otscan_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_timeend_otscan_waitapprove.Visible = true;
                    }
                    // set date defualt //

                    //day work
                    string day_off = lbl_parttime_day_off_detail_view.Text;
                    string day_condition_ = lbl_date_condition_waitapprove.Text;
                    string[] set_test_ = day_off.Split(',');
                    string day_offmployee_ = "";

                    if (Array.IndexOf(set_test_, day_condition_.ToString()) > -1)
                    {

                        day_offmployee_ = "1"; //yes day off
                                               //litDebug.Text += day_offemployee.ToString() + "|";
                                               //litDebug.Text += "1" + "|";

                        //litDebug.Text = day_workemployee_.ToString();
                    }
                    else
                    {
                        day_offmployee_ = "2"; // no day work
                                               //litDebug.Text += day_offemployee.ToString() + "|";

                        //litDebug.Text = day_workemployee_.ToString();
                    }
                    //day work

                    //set tab hours
                    if (lbl_shift_otbefore_waitapprove.Text == "0.00")
                    {
                        lbl_shift_otbefore_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_otbefore_waitapprove.Visible = true;
                    }

                    //
                    if (lbl_shift_otafter_waitapprove.Text == "0.00")
                    {
                        lbl_shift_otafter_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_otafter_waitapprove.Visible = true;
                    }

                    //
                    if (lbl_shift_otholiday_waitapprove.Text == "0.00")
                    {
                        lbl_shift_otholiday_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_otholiday_waitapprove.Visible = true;
                    }

                    //set tab hours


                }

                break;


            case "GvDetailOTDayToEmployee":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lbl_shift_timestart_waitapprove = (Label)e.Row.FindControl("lbl_shift_timestart_detail_toemp");
                    Label lbl_shift_timeend_waitapprove = (Label)e.Row.FindControl("lbl_shift_timeend_detail_toemp");
                    Label lbl_shift_timestart_otscan_waitapprove = (Label)e.Row.FindControl("lbl_shift_timestart_otscan_detail_toemp");
                    Label lbl_shift_timeend_otscan_waitapprove = (Label)e.Row.FindControl("lbl_shift_timeend_otscan_detail_toemp");
                    Label lbl_parttime_day_off_detail_toemp = (Label)e.Row.FindControl("lbl_parttime_day_off_detail_toemp");
                    //Label lbl_parttime_workdays_waitapprove = (Label)e.Row.FindControl("lbl_parttime_workdays_detail_toemp");
                    Label lbl_date_condition_waitapprove = (Label)e.Row.FindControl("lbl_date_condition_detail_toemp");
                    Label lbl_holiday_idx_waitapprove = (Label)e.Row.FindControl("lbl_holiday_idx_detail_toemp");

                    Label lbl_shift_otbefore_waitapprove = (Label)e.Row.FindControl("lbl_shift_otbefore_detail_toemp");
                    Label lbl_shift_otafter_waitapprove = (Label)e.Row.FindControl("lbl_shift_otafter_detail_toemp");
                    Label lbl_shift_otholiday_waitapprove = (Label)e.Row.FindControl("lbl_shift_otholiday_detail_toemp");

                    // set date defualt //
                    if (lbl_shift_timestart_waitapprove.Text == "01/01/1900 00:00")
                    {
                        lbl_shift_timestart_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_timestart_waitapprove.Visible = true;
                    }

                    if (lbl_shift_timeend_waitapprove.Text == "01/01/1900 00:00")
                    {
                        lbl_shift_timeend_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_timeend_waitapprove.Visible = true;
                    }

                    if (lbl_shift_timestart_otscan_waitapprove.Text == "01/01/1900 00:00")
                    {
                        lbl_shift_timestart_otscan_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_timestart_otscan_waitapprove.Visible = true;
                    }

                    if (lbl_shift_timeend_otscan_waitapprove.Text == "01/01/1900 00:00")
                    {
                        lbl_shift_timeend_otscan_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_timeend_otscan_waitapprove.Visible = true;
                    }
                    // set date defualt //

                    //day work
                    string day_off = lbl_parttime_day_off_detail_toemp.Text;
                    string day_condition_ = lbl_date_condition_waitapprove.Text;
                    string[] set_test_ = day_off.Split(',');
                    string day_offemployee_ = "";

                    if (Array.IndexOf(set_test_, day_condition_.ToString()) > -1)
                    {

                        day_offemployee_ = "1"; //yes day off
                                                //litDebug.Text += day_offemployee.ToString() + "|";
                                                //litDebug.Text += "1" + "|";

                        //litDebug.Text = day_workemployee_.ToString();
                    }
                    else
                    {
                        day_offemployee_ = "2"; // no day off
                                                //litDebug.Text += day_offemployee.ToString() + "|";

                        //litDebug.Text = day_workemployee_.ToString();
                    }
                    //day work

                    //set tab hours
                    if (lbl_shift_otbefore_waitapprove.Text == "0.00")
                    {
                        lbl_shift_otbefore_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_otbefore_waitapprove.Visible = true;
                    }

                    //
                    if (lbl_shift_otafter_waitapprove.Text == "0.00")
                    {
                        lbl_shift_otafter_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_otafter_waitapprove.Visible = true;
                    }

                    //
                    if (lbl_shift_otholiday_waitapprove.Text == "0.00")
                    {
                        lbl_shift_otholiday_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_otholiday_waitapprove.Visible = true;
                    }

                    //set tab hours


                }

                break;

            case "GvWaitApproveOTDayAdminCrate":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lbl_shift_timestart_waitapprove = (Label)e.Row.FindControl("lbl_shift_timestart_waitapprove");
                    Label lbl_shift_timeend_waitapprove = (Label)e.Row.FindControl("lbl_shift_timeend_waitapprove");
                    Label lbl_shift_timestart_otscan_waitapprove = (Label)e.Row.FindControl("lbl_shift_timestart_otscan_waitapprove");
                    Label lbl_shift_timeend_otscan_waitapprove = (Label)e.Row.FindControl("lbl_shift_timeend_otscan_waitapprove");
                    Label lbl_parttime_day_off_waitapprove = (Label)e.Row.FindControl("lbl_parttime_day_off_waitapprove");
                    //Label lbl_parttime_workdays_waitapprove = (Label)e.Row.FindControl("lbl_parttime_workdays_waitapprove");
                    Label lbl_date_condition_waitapprove = (Label)e.Row.FindControl("lbl_date_condition_waitapprove");
                    Label lbl_holiday_idx_waitapprove = (Label)e.Row.FindControl("lbl_holiday_idx_waitapprove");

                    Label lbl_shift_otbefore_waitapprove = (Label)e.Row.FindControl("lbl_shift_otbefore_waitapprove");
                    Label lbl_shift_otafter_waitapprove = (Label)e.Row.FindControl("lbl_shift_otafter_waitapprove");
                    Label lbl_shift_otholiday_waitapprove = (Label)e.Row.FindControl("lbl_shift_otholiday_waitapprove");

                    Label lbl_staidx_detail_otday_waitapprove = (Label)e.Row.FindControl("lbl_staidx_detail_otday_waitapprove");
                    Label lbl_m0_actor_idx_detail_otday_waitapprove = (Label)e.Row.FindControl("lbl_m0_actor_idx_detail_otday_waitapprove");

                    //TextBox txt_remarkhead_shift_waitapprove = (TextBox)e.Row.FindControl("txt_remarkhead_shift_waitapprove");
                    //TextBox txt_remarkhr_shift_waitapprove = (TextBox)e.Row.FindControl("txt_remarkhr_shift_waitapprove");
                    //TextBox txt_remarkadmin_shift_waitapprove = (TextBox)e.Row.FindControl("txt_remarkadmin_shift_waitapprove");


                    // set date defualt //
                    if (lbl_shift_timestart_waitapprove.Text == "01/01/1900 00:00")
                    {
                        lbl_shift_timestart_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_timestart_waitapprove.Visible = true;
                    }

                    if (lbl_shift_timeend_waitapprove.Text == "01/01/1900 00:00")
                    {
                        lbl_shift_timeend_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_timeend_waitapprove.Visible = true;
                    }

                    if (lbl_shift_timestart_otscan_waitapprove.Text == "01/01/1900 00:00")
                    {
                        lbl_shift_timestart_otscan_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_timestart_otscan_waitapprove.Visible = true;
                    }

                    if (lbl_shift_timeend_otscan_waitapprove.Text == "01/01/1900 00:00")
                    {
                        lbl_shift_timeend_otscan_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_timeend_otscan_waitapprove.Visible = true;
                    }
                    // set date defualt //

                    //day work
                    string day_off = lbl_parttime_day_off_waitapprove.Text;
                    string day_condition_ = lbl_date_condition_waitapprove.Text;
                    string[] set_test_ = day_off.Split(',');
                    string day_offemployee_ = "";

                    if (Array.IndexOf(set_test_, day_condition_.ToString()) > -1)
                    {

                        day_offemployee_ = "1"; //yes day work
                                                //litDebug.Text += day_offemployee.ToString() + "|";
                                                //litDebug.Text += "1" + "|";

                        //litDebug.Text = day_workemployee_.ToString();
                    }
                    else
                    {
                        day_offemployee_ = "2"; // no day work
                                                //litDebug.Text += day_offemployee.ToString() + "|";

                        //litDebug.Text = day_workemployee_.ToString();
                    }
                    //day work

                    //set tab hours
                    if (lbl_shift_otbefore_waitapprove.Text == "0.00")
                    {
                        lbl_shift_otbefore_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_otbefore_waitapprove.Visible = true;
                    }

                    //
                    if (lbl_shift_otafter_waitapprove.Text == "0.00")
                    {
                        lbl_shift_otafter_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_otafter_waitapprove.Visible = true;
                    }

                    //
                    if (lbl_shift_otholiday_waitapprove.Text == "0.00")
                    {
                        lbl_shift_otholiday_waitapprove.Visible = false;
                    }
                    else
                    {
                        lbl_shift_otholiday_waitapprove.Visible = true;
                    }

                    //set tab hours





                }
                break;

            case "GvDetailGroupOTDay":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lbl_m0_group_status_detail = (Label)e.Row.FindControl("lbl_m0_group_status_detail");
                    Label m0_group_statusOnline = (Label)e.Row.FindControl("m0_group_statusOnline");
                    Label m0_group_statusOffline = (Label)e.Row.FindControl("m0_group_statusOffline");

                    ViewState["vs_group_status"] = lbl_m0_group_status_detail.Text;


                    if (ViewState["vs_group_status"].ToString() == "1")
                    {
                        m0_group_statusOnline.Visible = true;
                    }
                    else if (ViewState["vs_group_status"].ToString() == "0")
                    {
                        m0_group_statusOffline.Visible = true;
                    }
                }

                break;

            case "GvCreateOTMonth":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    if (ViewState["vs_TimeStart"].ToString() != null && ViewState["vs_TimeEnd"].ToString() != null)
                    {
                        ovt_u0_month_detail[] _item_TotalOTMonth = (ovt_u0_month_detail[])ViewState["Vs_DateOTOfMonth"];

                        TextBox tbEmpCode = (TextBox)fvEmpDetail.FindControl("tbEmpCode");


                        CheckBox chk_date_ot = (CheckBox)e.Row.FindControl("chk_date_ot");

                        Label lbl_date_start = (Label)e.Row.FindControl("lbl_date_start");
                        Label lbl_holiday_idx_check = (Label)e.Row.FindControl("lbl_holiday_idx_check");
                        Label lbl_work_start = (Label)e.Row.FindControl("lbl_work_start");
                        Label lbl_work_finish = (Label)e.Row.FindControl("lbl_work_finish");
                        Label lbl_break_start = (Label)e.Row.FindControl("lbl_break_start");
                        Label lbl_break_end = (Label)e.Row.FindControl("lbl_break_end");

                        Label lbl_day_off = (Label)e.Row.FindControl("lbl_day_off");
                        Label lbl_date_condition = (Label)e.Row.FindControl("lbl_date_condition");

                        TextBox txt_sum_hour = (TextBox)e.Row.FindControl("txt_sum_hour");
                        ////TextBox txt_sum_hour = (TextBox)e.Row.FindControl("txt_sum_hour");
                        TextBox txt_job = (TextBox)e.Row.FindControl("txt_job");
                        TextBox txt_remark = (TextBox)e.Row.FindControl("txt_remark");
                        TextBox txt_head_set = (TextBox)e.Row.FindControl("txt_head_set");

                        Label txtot_before = (Label)e.Row.FindControl("txtot_before");
                        Label txtot_after = (Label)e.Row.FindControl("txtot_after");
                        Label txtot_holiday = (Label)e.Row.FindControl("txtot_holiday");
                        Label lblot_before = (Label)e.Row.FindControl("lblot_before");
                        Label lblot_after = (Label)e.Row.FindControl("lblot_after");
                        Label lblot_holiday = (Label)e.Row.FindControl("lblot_holiday");
                        Label lbl_detailholiday = (Label)e.Row.FindControl("lbl_detailholiday");
                        Label lbl_detaildayoff = (Label)e.Row.FindControl("lbl_detaildayoff");
                        // bind time start //

                        DropDownList ddl_timestart = (DropDownList)e.Row.FindControl("ddl_timestart");
                        DropDownList ddl_timeend = (DropDownList)e.Row.FindControl("ddl_timeend");

                        DropDownList ddl_timebefore = (DropDownList)e.Row.FindControl("ddl_timebefore");
                        DropDownList ddl_timeafter = (DropDownList)e.Row.FindControl("ddl_timeafter");
                        DropDownList ddl_timeholiday = (DropDownList)e.Row.FindControl("ddl_timeholiday");

                        var d_start = new DateTime();
                        var d_end = new DateTime();
                        var d_finish = new DateTime();
                        var d_workstart = new DateTime();

                        double value_total_ot;
                        double value_total_otresult;

                        double value_total_ot_holiday;
                        double value_total_ot_before;
                        double value_total_ot_diff;

                        IFormatProvider culture_ = new CultureInfo("en-US", true);

                        d_finish = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);
                        d_workstart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_start.Text), "dd/MM/yyyy HH:mm", culture_);

                        d_start = DateTime.ParseExact((lbl_date_start.Text + " " + "00:00"), "dd/MM/yyyy HH:mm", culture_);
                        d_end = DateTime.ParseExact((lbl_date_start.Text + " " + "05:59"), "dd/MM/yyyy HH:mm", culture_);

                        double value_otbefore;
                        double value_otafter;
                        DateTime d_finish_addhour_after = new DateTime();
                        string day_offemployee = "";
                        //litDebug.Text += lbl_date_start.Text + "|";

                        //lbl_detailholiday.Visible = false;
                        //lbl_detaildayoff.Visible = false;



                        if (lbl_date_start.Text != "")
                        {

                            //day off employee
                            string day_off = lbl_day_off.Text;
                            string day_condition = lbl_date_condition.Text;
                            string[] set_test = day_off.Split(',');


                            if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                            {
                                day_offemployee = "1"; //yes day off
                                                       //litDebug.Text += day_offemployee.ToString() + "|";
                                                       //litDebug.Text += "1" + "|";
                            }
                            else
                            {
                                day_offemployee = "2"; // no day off
                                                       //litDebug.Text += day_offemployee.ToString() + "|";
                            }
                            //day off employee

                            ovt_timestart_otmonth_detail[] _templist_TimeStart = (ovt_timestart_otmonth_detail[])ViewState["vs_TimeStart"];

                            var _linqTimeStart = (from dt in _templist_TimeStart

                                                  where
                                                  ////dt.date_start != "" && dt.time_start != ""
                                                  ////&& dt.date_start == lbl_date_start.Text && (DateTime.ParseExact((dt.time_start), "dd/MM/yyyy HH:mm", culture_) < d_finish)

                                                  dt.date_start != "" && dt.time_start != ""
                                                  && (dt.date_start == lbl_date_start.Text)
                                                  && ((DateTime.ParseExact((dt.time_start), "dd/MM/yyyy HH:mm", culture_) < d_finish)
                                                  || (
                                                   (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1")
                                                   && (DateTime.ParseExact((dt.time_start), "dd/MM/yyyy HH:mm", culture_) > d_finish)
                                                   && dt.date_start == lbl_date_start.Text && dt.date_start != "" && dt.time_start != "")
                                                   )


                                                  //&& (
                                                  //   (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1")
                                                  //   || ((DateTime.ParseExact((dt.time_start), "dd/MM/yyyy HH:mm", culture_) < d_finish))
                                                  // )

                                                  select new
                                                  {
                                                      dt.time_start,
                                                      dt.month_idx
                                                  }).Distinct().ToList();

                            setDdlData(ddl_timestart, _linqTimeStart.ToList(), "time_start", "month_idx");
                            // bind time start //

                            //value_otbefore = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);



                            // bind time end //
                            DateTime dstart_add = d_start.AddDays(1);
                            DateTime dend_add = d_end.AddDays(1);

                            //litDebug.Text += dend_add.ToString("dd/MM/yyyy HH:mm") + "|";

                            ovt_timestart_otmonth_detail[] _templist_TimeEnd = (ovt_timestart_otmonth_detail[])ViewState["vs_TimeEnd"];

                            var _linqTimeEnd = (from dt in _templist_TimeEnd
                                                    //join dt1 in _templist_TimeStart on dt.time_end equals dt1.time_start into ps
                                                where
                                                ////dt.date_end == lbl_date_start.Text 
                                                ////&&

                                                (dt.time_end != "" && dt.date_end != "" && dt.month_idx != 0
                                                &&
                                                    (
                                                        (DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) >= d_finish && dt.date_end == lbl_date_start.Text)

                                                        ||
                                                        (DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) >= d_finish && DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_).Subtract(d_finish).TotalHours <= 8)

                                                    )
                                                //|| (DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) >= d_finish && dt.date_end == lbl_date_start.Text)
                                                //&& (DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) >= d_finish)
                                                || (dt.time_end != "" && dt.date_end != "" && dt.month_idx != 0
                                                   && (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1")
                                                   && ((DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) >= d_finish && dt.date_end == lbl_date_start.Text)
                                                        || DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) < d_finish && dt.date_end == lbl_date_start.Text)
                                                   )

                                                || (
                                                        dt.time_end != "" && dt.date_end != "" && dt.month_idx != 0 &&
                                                        DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) > dstart_add && DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) < dend_add

                                                    )

                                                )
                                                select new
                                                {
                                                    dt.time_end,
                                                    dt.month_idx
                                                }).Distinct().ToList();


                            setDdlData(ddl_timeend, _linqTimeEnd.ToList(), "time_end", "month_idx");



                            //litDebug.Text += ddl_timeend.Items.Count.ToString() + "|" + lbl_date_start.Text;
                            // bind time end //


                        }



                        //set gv row detail show data
                        if (ddl_timestart.SelectedValue != "" && ddl_timeend.SelectedValue != "")
                        {

                            if ((ddl_timeend.SelectedItem.ToString() != ddl_timestart.SelectedItem.ToString()) || (ddl_timeend.Items.Count.ToString() != ddl_timestart.Items.Count.ToString()))
                            {
                                e.Row.Visible = true;

                                DateTime DateTime_ScanIn = DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);
                                DateTime DateTime_ScanOut = DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);


                                DateTime DateTime_ShiftStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_work_start.Text, "dd/MM/yyyy HH:mm", culture_);
                                DateTime DateTime_ShiftEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_work_finish.Text, "dd/MM/yyyy HH:mm", culture_);

                                DateTime DateTime_BreakStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_start.Text, "dd/MM/yyyy HH:mm", culture_);
                                DateTime DateTime_BreakEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_end.Text, "dd/MM/yyyy HH:mm", culture_);

                                double value;
                                double _value_diffbreaktime;
                                decimal _value_timescan_late;
                                double _value_timescan_late_add;

                                double _value_otafter_holiday;

                                DateTime d_finish_addhour = new DateTime();
                                DateTime d_start_addhour = new DateTime();

                                DateTime d_finish_addhour_holiday = new DateTime();
                                double _value_diff_scanout;


                                double _value_test;
                                double _value_test1;
                                double _value_test2;

                                /*

                                litDebug1.Text += DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";
                                DateTime DateTime_BreakStart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);

                                */

                                //ot before
                                value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                //litDebug.Text += d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString() + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm");

                                txtot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));

                                ////if (d_workstart.Subtract(DateTime_ScanIn).TotalMinutes >= 30)
                                ////{
                                ////    getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));
                                ////}
                                ////else
                                ////{
                                ////    getHourToOT(ddl_timebefore, 0);
                                ////}



                                //ot after
                                if (DateTime_ScanOut > DateTime_ScanIn)
                                {

                                    d_finish_addhour_after = d_finish.AddMinutes(30);

                                    //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                    //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                    //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);

                                    value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);

                                    _value_test = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);

                                    //litDebug.Text += DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString() + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");//Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);

                                    //var time = TimeSpan.FromMinutes(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes);

                                    txtot_after.Text = (Math.Floor(_value_test * 2) / 2).ToString();//(Math.Round((value_otafter * 2)/2, MidpointRounding.AwayFromZero)).ToString();
                                                                                                    //string.Format("{0:00}:{1:00}", (int)value_otafter.TotalHours, value_otafter.Minutes);

                                    ////litDebug.Text += value_otafter.ToString() + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                    //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                    getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));

                                    //double x = 10.2349;
                                    ////double rounded = Math.Floor(_value_test * 2) / 2;

                                    ////litDebug.Text += rounded.ToString() + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                    ////double val = 4.1;

                                    //////Console.WriteLine("Inside Loop:\n");

                                    //////'for loop', it execute the next 
                                    ////// output for 8 times 
                                    ////for (int i = 0; i <= 8; i++)
                                    ////{

                                    ////    // '{0}' specify the variable 'val' and  
                                    ////    // '{1}' specify the rounded value 
                                    ////    litDebug.Text += value_otafter.ToString() + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");
                                    ////    Console.WriteLine("{0} = {1}", val, Math.Round(val,
                                    ////                       MidpointRounding.AwayFromZero));

                                    ////    // increment 'val' by '0.1'                 
                                    ////    val += 0.1;
                                    ////}

                                    //double before1 = 2.65;
                                    //_value_test = Math.Round(Convert.ToDouble(value_otafter*2/2), 2, MidpointRounding.AwayFromZero);

                                    //litDebug.Text += _value_test + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");//_value_test.ToString();

                                    ////litDebug1.Text += Math.Round(Convert.ToDouble(Math.Round(Convert.ToDouble(_value_test), 0, MidpointRounding.AwayFromZero)), 2, MidpointRounding.AwayFromZero) + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");//_value_test.ToString();

                                }
                                else
                                {
                                    ddl_timeafter.Visible = false;
                                }


                                //ot holiday
                                if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1")
                                {
                                    //litDebug.Text += "1"; 
                                    //litDebug.Text += "11" + "|" + lbl_holiday_idx_check.Text + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy");
                                    //litDebug.Text += DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm") + "|" + DateTime_ShiftEnd.ToString("dd/MM/yyyy HH:mm") + "|";

                                    if (lbl_holiday_idx_check.Text != "0")
                                    {
                                        lbl_detailholiday.Visible = true;
                                        lbl_detaildayoff.Visible = false;
                                    }
                                    else if (day_offemployee.ToString() == "1")
                                    {
                                        lbl_detaildayoff.Visible = true;
                                        lbl_detailholiday.Visible = false;
                                    }

                                    //if ((DateTime_ScanIn <= DateTime_ShiftStart || DateTime_ScanIn >= DateTime_ShiftStart) && DateTime_ScanOut >= DateTime_ShiftEnd)
                                    if ((DateTime_ScanIn <= DateTime_ShiftStart || DateTime_ScanIn >= DateTime_ShiftStart))
                                    {
                                        //litDebug.Text += (Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString())/30) * 0.50).ToString()) + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|"  + DateTime_ShiftEnd.ToString("dd/MM/yyyy HH:mm");
                                        ////decimal value = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                        ////litDebug1.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        //litDebug.Text += _value_diffbreaktime.ToString() + "|";


                                        //ot before
                                        value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        //litDebug.Text += d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString() + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm");

                                        txtot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        getHourToOT(ddl_timebefore, Convert.ToDouble(txtot_before.Text));

                                        //ot after
                                        if (DateTime_ScanOut > DateTime_ScanIn)
                                        {

                                            d_finish_addhour_after = d_finish.AddMinutes(30);

                                            //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                            value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                            _value_test = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                            txtot_after.Text = (Math.Floor(_value_test * 2) / 2).ToString();//(Math.Round((value_otafter * 2)/2, MidpointRounding.AwayFromZero)).ToString();
                                                                                                            //string.Format("{0:00}:{1:00}", (int)value_otafter.TotalHours, value_otafter.Minutes);

                                            ////litDebug.Text += value_otafter.ToString() + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                            //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                            getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));

                                        }
                                        else
                                        {
                                            ddl_timeafter.Visible = false;
                                        }


                                        _value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                        _value_diff_scanout = DateTime_ScanOut.Subtract(DateTime_ShiftEnd).TotalMinutes;

                                        //litDebug1.Text = _value_diff_scanout.ToString() + "|" + _value_diffbreaktime.ToString();

                                        if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn <= DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                        {
                                            //litDebug.Text += "6";

                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                            txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                            if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                            {

                                                d_finish_addhour_holiday = DateTime_ShiftEnd.AddMinutes(30);

                                                _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                                txtot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));


                                            }
                                        }
                                        else if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn >= DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                        {

                                            //check hour .xx
                                            string input_decimal_number_be = (Math.Floor((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60))).ToString(); //((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString();
                                            string input_decimal_number_af = ((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString("#.##");//"1.50";

                                            string decimal_places = String.Empty;


                                            ViewState["_value_timescan_late_add"] = "0";

                                            var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                                            if (regex.IsMatch(input_decimal_number_af))
                                            {
                                                decimal_places = regex.Match(input_decimal_number_af).Value;
                                                if (int.Parse(decimal_places) > 50)
                                                {

                                                    _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + 1);//Convert.ToDouble((input_decimal_number_be) + 1);
                                                    ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                    d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                    d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                                }
                                                else if (int.Parse(decimal_places) < 50)
                                                {

                                                    _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + .50);//Convert.ToDouble((input_decimal_number_be + .50) );

                                                    ViewState["_value_timescan_late_add"] = _value_timescan_late_add;


                                                    d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                    d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                                }
                                                else
                                                {
                                                    _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be))); //Convert.ToDouble((input_decimal_number_be));
                                                    ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                    d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                    d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));


                                                }

                                            }

                                            // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                            if (_value_diff_scanout >= 60)
                                            {
                                                value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                            }
                                            else
                                            {

                                                //value = Convert.ToDouble(((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1) );

                                                value = Convert.ToDouble(((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1) - Convert.ToDouble(ViewState["_value_timescan_late_add"].ToString()));

                                            }

                                            //litDebug1.Text += value.ToString();
                                            txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                            // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                            if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                            {
                                                //litDebug.Text = d_finish_addhour_holiday.ToString();
                                                //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                                //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                                //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                                d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                                _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                                txtot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));

                                            }
                                            else
                                            {
                                                getHourToOT(ddl_timeafter, Convert.ToDouble("0"));
                                            }



                                            /*
                                            //litDebug.Text += "4";
                                            //litDebug.Text = DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                            _value_timescan_late = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;
                                            _value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();

                                            //litDebug.Text = _value_timescan_late_add.ToString();

                                            d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                            d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                            // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                            if (_value_diff_scanout >= 60)
                                            {
                                                value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                            }
                                            else
                                            {
                                                value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                            }

                                            //litDebug1.Text += value.ToString();
                                            txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();// (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                            // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));

                                            if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                            {
                                                //litDebug.Text = d_finish_addhour_holiday.ToString();
                                                //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                                //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                                //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                                d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                                _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                                txtot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeafter, Convert.ToDouble(txtot_after.Text));

                                            }
                                            else
                                            {
                                                getHourToOT(ddl_timeafter, Convert.ToDouble("0"));
                                            }

                                            */

                                        }
                                        else if (DateTime_ScanOut < DateTime_ShiftEnd && (DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60))//&& (DateTime_ScanIn >= DateTime_BreakStart))
                                        {

                                            //litDebug.Text += "3";

                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                            txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));
                                        }
                                        //else if (DateTime_ScanOut > DateTime_ShiftEnd)
                                        //{
                                        //    //litDebug.Text += "5";
                                        //}
                                        else //if (DateTime_ScanOut > DateTime_BreakEnd && DateTime_ScanOut < DateTime_ShiftEnd)
                                        {
                                            //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);

                                            txtot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(txtot_holiday.Text));
                                        }

                                    }
                                    else if (DateTime_ScanIn < d_workstart && DateTime_ScanOut < d_finish)
                                    {
                                        //litDebug1.Text = "have case new";

                                        //total ot
                                        value_total_ot = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 60));
                                        value_total_otresult = (Math.Floor(value_total_ot * 2) / 2);
                                        //total ot

                                        if (value_total_otresult >= 9.5) //set -1 breaktime
                                        {

                                            value_total_ot_diff = value_total_otresult - 1;


                                            value_total_ot_holiday = 8;
                                            value_total_ot_before = value_total_ot_diff - value_total_ot_holiday;

                                            //litDebug.Text = value_total_ot_before.ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                            getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));

                                        }
                                        else if (value_total_otresult == 9)
                                        {
                                            value_total_ot_diff = value_total_otresult - 1;
                                            value_total_ot_holiday = 8;
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                        }
                                        else if (value_total_otresult >= 4 && value_total_otresult <= 5)
                                        {
                                            value_total_ot_before = 4;
                                            getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));
                                        }
                                        else if (value_total_otresult > 5 && value_total_otresult <= 8)
                                        {
                                            value_total_ot_diff = value_total_otresult - 1;

                                            value_total_ot_before = value_total_ot_diff;
                                            getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()));
                                        }
                                    }
                                }
                                else
                                {
                                    //litDebug.Text += "2" + "|" + lbl_holiday_idx_check.Text + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy");
                                    //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                    lbl_detailholiday.Visible = false;
                                    lbl_detaildayoff.Visible = false;
                                    ddl_timeholiday.Visible = false;
                                }


                                //litDebug.Text += value_otbefore.ToString() + "|";
                            }
                            //else
                            //{

                            //    //litDebug1.Text += ddl_timeend.SelectedItem.ToString() + "|" + ddl_timestart.SelectedItem.ToString() + "|";
                            //    e.Row.Visible = false;
                            //}


                        }
                        else
                        {
                            //litDebug1.Text += ddl_timeend.SelectedItem.ToString() + "|";
                            e.Row.Visible = false;
                        }
                        //set gv row detail show data






                        if (chk_date_ot.Checked)
                        {


                            if (ddl_timebefore.SelectedValue != "")
                            {
                                if (Convert.ToDouble(ddl_timebefore.SelectedValue) >= 0.5)
                                {
                                    tot_actual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                                    //Label lit_total_hoursot_before = (Label)GvCreateOTMonth.FooterRow.Cells[5].FindControl("lit_total_hoursot_before");
                                    //lit_total_hoursot_before.Text = String.Format("{0:N2}", totactual_otbefore);
                                }
                            }

                            if (ddl_timeafter.SelectedValue != "")
                            {
                                if (Convert.ToDouble(ddl_timeafter.SelectedValue) >= 0.5)
                                {
                                    tot_actual_otafter += Convert.ToDecimal(ddl_timeafter.SelectedValue);
                                    //Label lit_total_hoursot_after = (Label)GvCreateOTMonth.FooterRow.Cells[6].FindControl("lit_total_hoursot_after");
                                    //lit_total_hoursot_after.Text = String.Format("{0:N2}", totactual_otafter);
                                }
                            }

                            if (ddl_timeholiday.SelectedValue != "")
                            {
                                if (Convert.ToDouble(ddl_timeholiday.SelectedValue) >= 0.5)
                                {
                                    tot_actual_otholiday += Convert.ToDecimal(ddl_timeholiday.SelectedValue);
                                    //Label lit_total_hoursot_holiday = (Label)GvCreateOTMonth.FooterRow.Cells[7].FindControl("lit_total_hoursot_holiday");
                                    //lit_total_hoursot_holiday.Text = String.Format("{0:N2}", totactual_otholiday);
                                }
                            }


                        }
                        else
                        {
                            //tot_actual_otmonth += Convert.ToDecimal("0.00");
                            tot_actual_otbefore += Convert.ToDecimal("0.00");
                            tot_actual_otafter += Convert.ToDecimal("0.00");
                            tot_actual_otholiday += Convert.ToDecimal("0.00");
                        }

                        //set ot x1, x1.5, x2, x3
                        //set ot before
                        if (ddl_timebefore.SelectedValue != "")
                        {

                            lblot_before.Visible = true;
                            ddl_timebefore.Visible = true;
                            if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                            {
                                if (ddl_timeholiday.SelectedValue == "8")
                                {
                                    lblot_before.Text = "x3";
                                }
                                else
                                {
                                    lblot_before.Text = "x1";
                                }
                                //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                //{
                                //    lbl_hour_otbefore.Text = "x1";
                                //}
                                //else
                                //{
                                //    lbl_hour_otbefore.Text = "x2";
                                //}
                            }
                            else
                            {
                                lblot_before.Text = "x1.5";
                            }
                        }
                        else
                        {
                            lblot_before.Visible = false;
                            ddl_timebefore.Visible = false;
                        }

                        //set ot after
                        if (ddl_timeafter.SelectedValue != "")
                        {

                            lblot_after.Visible = true;
                            ddl_timeafter.Visible = true;
                            if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                            {
                                if (ddl_timeholiday.SelectedValue == "8")
                                {
                                    lblot_after.Text = "x3";
                                }
                                else
                                {
                                    lblot_after.Text = "x1";
                                }
                                //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                //{
                                //    lbl_hour_otbefore.Text = "x1";
                                //}
                                //else
                                //{
                                //    lbl_hour_otbefore.Text = "x2";
                                //}
                            }
                            else
                            {
                                lblot_after.Text = "x1.5";
                            }
                        }
                        else
                        {
                            lblot_after.Visible = false;
                            ddl_timeafter.Visible = false;
                        }

                        //set ot holiday
                        if (ddl_timeholiday.SelectedValue != "")
                        {

                            lblot_holiday.Visible = true;
                            ddl_timeholiday.Visible = true;
                            if (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                            {

                                lblot_holiday.Text = "x1";

                                //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                //{
                                //    lblot_holiday.Text = "x1";
                                //}
                                //else
                                //{
                                //    lblot_holiday.Text = "x2";
                                //}
                            }



                        }
                        else
                        {
                            lblot_holiday.Visible = false;
                            ddl_timeholiday.Visible = false;
                        }

                        //litDebug.Text += day_offemployee.ToString() + "|";
                        if (lbl_holiday_idx_check.Text != "0")
                        {
                            lbl_detailholiday.Visible = true;
                            lbl_detaildayoff.Visible = false;
                        }
                        else if (day_offemployee.ToString() == "1")
                        {
                            lbl_detaildayoff.Visible = true;
                            lbl_detailholiday.Visible = false;
                        }



                    }

                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    ////Label lit_total_hoursmonth = (Label)e.Row.FindControl("lit_total_hoursmonth");
                    ////lit_total_hoursmonth.Text = String.Format("{0:N2}", tot_actual_otmonth); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_before = (Label)e.Row.FindControl("lit_total_hoursot_before");
                    lit_total_hoursot_before.Text = String.Format("{0:N2}", tot_actual_otbefore); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_after = (Label)e.Row.FindControl("lit_total_hoursot_after");
                    lit_total_hoursot_after.Text = String.Format("{0:N2}", tot_actual_otafter); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_holiday = (Label)e.Row.FindControl("lit_total_hoursot_holiday");
                    lit_total_hoursot_holiday.Text = String.Format("{0:N2}", tot_actual_otholiday); // Convert.ToString(tot_actual);

                }

                break;
            case "GvDetailCreateOTDay":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    CheckBox chk_otday = (CheckBox)e.Row.FindControl("chk_otday");
                    Label lbl_shift_date = (Label)e.Row.FindControl("lbl_shift_date");
                    Label lbl_emp_code_otday = (Label)e.Row.FindControl("lbl_emp_code_otday");
                    Label lbl_parttime_start_time = (Label)e.Row.FindControl("lbl_parttime_start_time");
                    Label lbl_parttime_end_time = (Label)e.Row.FindControl("lbl_parttime_end_time");
                    Label lbl_holiday_idx_otday = (Label)e.Row.FindControl("lbl_holiday_idx_otday");
                    Label lbl_parttime_day_off = (Label)e.Row.FindControl("lbl_parttime_day_off");
                    Label lbl_parttime_workdays = (Label)e.Row.FindControl("lbl_parttime_workdays");
                    Label lbl_date_condition = (Label)e.Row.FindControl("lbl_date_condition");
                    Label lbl_parttime_break_start_time = (Label)e.Row.FindControl("lbl_parttime_break_start_time");
                    Label lbl_emp_type_idx = (Label)e.Row.FindControl("lbl_emp_type_idx");

                    //bind time
                    ////Label lbl_shift_timestart = (Label)e.Row.FindControl("lbl_shift_timestart");
                    ////Label lbl_shift_timestart_uidx = (Label)e.Row.FindControl("lbl_shift_timestart_uidx");

                    ////Label lbl_shift_timeend = (Label)e.Row.FindControl("lbl_shift_timeend");
                    ////Label lbl_shift_timeend_uidx = (Label)e.Row.FindControl("lbl_shift_timeend_uidx");

                    ////Label lbl_shift_timestart_otscan = (Label)e.Row.FindControl("lbl_shift_timestart_otscan");
                    ////Label lbl_shift_timestart_uidx_otscan = (Label)e.Row.FindControl("lbl_shift_timestart_uidx_otscan");

                    Label lbl_shift_timeend_otscan = (Label)e.Row.FindControl("lbl_shift_timeend_otscan");
                    Label lbl_shift_timeend_uidx_otscan = (Label)e.Row.FindControl("lbl_shift_timeend_uidx_otscan");

                    Label lbl_shift_otbefore = (Label)e.Row.FindControl("lbl_shift_otbefore");
                    Label lbl_hours_otbefore = (Label)e.Row.FindControl("lbl_hours_otbefore");
                    Label lbl_shift_otafter = (Label)e.Row.FindControl("lbl_shift_otafter");
                    Label lbl_hours_otafter = (Label)e.Row.FindControl("lbl_hours_otafter");
                    Label lbl_shift_otholiday = (Label)e.Row.FindControl("lbl_shift_otholiday");
                    Label lbl_hours_otholiday = (Label)e.Row.FindControl("lbl_hours_otholiday");

                    DropDownList ddl_shifttimestart = (DropDownList)e.Row.FindControl("ddl_shifttimestart");
                    DropDownList ddl_shifttimeend = (DropDownList)e.Row.FindControl("ddl_shifttimeend");
                    DropDownList ddl_shifttimestart_ot = (DropDownList)e.Row.FindControl("ddl_shifttimestart_ot");
                    DropDownList ddl_shifttimeend_ot = (DropDownList)e.Row.FindControl("ddl_shifttimeend_ot");


                    IFormatProvider culture_ = new CultureInfo("en-US", true);

                    var d_start = new DateTime();
                    var d_end = new DateTime();

                    var shifttime_start = new DateTime();
                    var shifttime_end = new DateTime();
                    //var d_finish = new DateTime();


                    //d_finish = DateTime.ParseExact((lbl_shift_date.Text + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm:ss", culture_);

                    d_start = DateTime.ParseExact((lbl_shift_date.Text), "dd/MM/yyyy", culture_);
                    d_end = DateTime.ParseExact((lbl_shift_date.Text), "dd/MM/yyyy", culture_);
                    //d_end = DateTime.ParseExact((lbl_date_start.Text + " " + "05:59:59"), "dd/MM/yyyy HH:mm:ss", culture_);

                    DateTime dstart_del = d_start.AddDays(-1);
                    DateTime dend_del = d_start.AddDays(+1);

                    //check type shifttime

                    shifttime_start = DateTime.ParseExact((lbl_parttime_start_time.Text), "HH:mm", culture_);
                    shifttime_end = DateTime.ParseExact((lbl_parttime_end_time.Text), "HH:mm", culture_);

                    ovt_timestart_otday_detail[] _templist_TimeStartOTDay = (ovt_timestart_otday_detail[])ViewState["Vs_TimeStartOTDay"];
                    ovt_timestart_otday_detail[] _templist_TimeEndOTDay = (ovt_timestart_otday_detail[])ViewState["Vs_TimeEndOTDay"];

                    ovt_timestart_otday_detail[] _templist_TimeStartOTDay_scan = (ovt_timestart_otday_detail[])ViewState["Vs_TimeStartOTDay_Scan"];
                    ovt_timestart_otday_detail[] _templist_TimeEndOTDay_scan = (ovt_timestart_otday_detail[])ViewState["Vs_TimeEndOTDay_Scan"];

                    //litDebug.Text += shifttime_start.ToString("HH:mm") + "|" + shifttime_end.ToString("HH:mm");

                    if (ViewState["Vs_TimeStartOTDay"] != null)
                    {
                        if (shifttime_start > shifttime_end) //ข้ามวัน
                        {
                            //start
                            var _linqTimeStartOTDay = (from dt in _templist_TimeStartOTDay
                                                       where
                                                       dt.emp_code == lbl_emp_code_otday.Text && dt.time_start != ""
                                                       && (DateTime.ParseExact((dt.datetime_scan.ToString()), "dd/MM/yyyy", culture_) == dstart_del)

                                                       select new
                                                       {
                                                           dt.time_start,
                                                           dt.datetime_scan,
                                                           dt.uidx,
                                                           dt.emp_code,
                                                           dt.parttime_start_time,
                                                           dt.parttime_end_time
                                                       }).ToList();

                            setDdlData(ddl_shifttimestart, _linqTimeStartOTDay.ToList(), "time_start", "uidx");


                        }
                        else
                        {
                            //start
                            var _linqTimeStartOTDay = (from dt in _templist_TimeStartOTDay
                                                       where
                                                       dt.emp_code == lbl_emp_code_otday.Text && dt.time_start != ""
                                                       && (dt.datetime_scan.ToString() == lbl_shift_date.Text)
                                                       select new
                                                       {
                                                           dt.time_start,
                                                           dt.datetime_scan,
                                                           dt.uidx,
                                                           dt.emp_code,
                                                           dt.parttime_start_time,
                                                           dt.parttime_end_time
                                                       }).ToList();


                            setDdlData(ddl_shifttimestart, _linqTimeStartOTDay.ToList(), "time_start", "uidx");



                        }
                    }

                    //check type shifttime

                    // bind time start //


                    // bind time end //
                    if (ViewState["Vs_TimeEndOTDay"] != null)
                    {
                        var _linqTimeEndOTDay = (from dt in _templist_TimeEndOTDay
                                                 where
                                                 dt.emp_code == lbl_emp_code_otday.Text && dt.time_end != ""
                                                 && (dt.datetime_scan.ToString() == lbl_shift_date.Text)
                                                 || ((DateTime.ParseExact((dt.datetime_scan.ToString()), "dd/MM/yyyy", culture_) == dend_del) && dt.emp_code == lbl_emp_code_otday.Text)


                                                 select new
                                                 {
                                                     dt.time_end,
                                                     dt.datetime_scan,
                                                     dt.uidx,
                                                     dt.emp_code,
                                                     dt.parttime_start_time,
                                                     dt.parttime_end_time
                                                 }).ToList();

                        setDdlData(ddl_shifttimeend, _linqTimeEndOTDay.ToList(), "time_end", "uidx");


                    }
                    // bind time otstart //


                    //ข้ามวัน
                    if (ViewState["Vs_TimeStartOTDay_Scan"] != null)
                    {
                        if (shifttime_start > shifttime_end) //ข้ามวัน
                        {
                            var _linqTimeStartOTDay_scan = (from dt in _templist_TimeStartOTDay_scan
                                                            where
                                                            dt.emp_code == lbl_emp_code_otday.Text
                                                            && (DateTime.ParseExact((dt.datetime_scan.ToString()), "dd/MM/yyyy", culture_) == dstart_del)
                                                            select new
                                                            {
                                                                dt.time_otstart,
                                                                dt.datetime_scan,
                                                                dt.uidx,
                                                                dt.emp_code,
                                                                dt.parttime_start_time,
                                                                dt.parttime_end_time

                                                            }).ToList();
                            setDdlData(ddl_shifttimestart_ot, _linqTimeStartOTDay_scan.ToList(), "time_otstart", "uidx");

                        }
                        else
                        {
                            var _linqTimeStartOTDay_scan = (from dt in _templist_TimeStartOTDay_scan
                                                            where
                                                            dt.emp_code == lbl_emp_code_otday.Text
                                                            && (dt.datetime_scan.ToString() == lbl_shift_date.Text)
                                                            select new
                                                            {
                                                                dt.time_otstart,
                                                                dt.datetime_scan,
                                                                dt.uidx,
                                                                dt.emp_code,
                                                                dt.parttime_start_time,
                                                                dt.parttime_end_time

                                                            }).ToList();
                            setDdlData(ddl_shifttimestart_ot, _linqTimeStartOTDay_scan.ToList(), "time_otstart", "uidx");

                        }
                    }


                    // bind time otend
                    if (ViewState["Vs_TimeEndOTDay_Scan"] != null)
                    {
                        var _linqTimeEndOTDay_scan = (from dt in _templist_TimeEndOTDay_scan
                                                      where
                                                      dt.emp_code == lbl_emp_code_otday.Text && dt.time_otend != "" && dt.datetime_scan != ""
                                                      && (dt.datetime_scan.ToString() == lbl_shift_date.Text)
                                                       || ((DateTime.ParseExact((dt.datetime_scan.ToString()), "dd/MM/yyyy", culture_) == dend_del) && dt.emp_code == lbl_emp_code_otday.Text)
                                                      select new
                                                      {
                                                          dt.time_otend,
                                                          dt.datetime_scan,
                                                          dt.uidx,
                                                          dt.emp_code
                                                      }).ToList();


                        setDdlData(ddl_shifttimeend_ot, _linqTimeEndOTDay_scan.ToList(), "time_otend", "uidx");

                    }

                    //time finish and break time 30 minute -ot before


                    //date scan tranlof time end
                    DateTime DateTime_Start = new DateTime();
                    DateTime DateTime_End = new DateTime();

                    if (ddl_shifttimestart_ot.SelectedValue != "")
                    {
                        //DateTime_Start = DateTime.ParseExact(lbl_shift_timestart_otscan.Text, "dd/MM/yyyy HH:mm", culture_);
                        DateTime_Start = DateTime.ParseExact(ddl_shifttimestart_ot.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);
                        //litDebug.Text += DateTime_Start.ToString("dd/MM/yyyy HH:mm:ss") + "|";

                    }

                    //if (lbl_shift_timeend_otscan.Text != "")
                    if (ddl_shifttimeend_ot.SelectedValue != "")
                    {
                        //DateTime_End = DateTime.ParseExact(lbl_shift_timeend_otscan.Text, "dd/MM/yyyy HH:mm", culture_);
                        DateTime_End = DateTime.ParseExact(ddl_shifttimeend_ot.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);
                    }
                    //date scan tranlof time end

                    //day off employee
                    //string day_work = lbl_parttime_workdays.Text;

                    string day_off = lbl_parttime_day_off.Text;
                    string day_condition = lbl_date_condition.Text;
                    string[] set_work = day_off.Split(',');
                    //string day_workemployee = "";
                    string day_offemployee = "";

                    if (Array.IndexOf(set_work, day_condition.ToString()) > -1)
                    {
                        day_offemployee = "1"; //yes day off
                                               //litDebug.Text += day_offemployee.ToString() + "|";
                                               //litDebug.Text += "1" + "|";
                    }
                    else
                    {
                        day_offemployee = "2"; // no day off
                                               //litDebug.Text += day_offemployee.ToString() + "|";
                    }
                    //day off employee


                    //if (lbl_shift_timestart.Text != "" && lbl_shift_timeend.Text != "")
                    if (ddl_shifttimestart.SelectedValue != "" && ddl_shifttimeend.SelectedValue != "")
                    {


                        //litDebug.Text += day_workemployee.ToString() + "|";
                        // cal ot 
                        //litDebug.Text = DateTime_EndHoliday.ToString("dd/MM/yyyy HH:mm:ss");
                        DateTime datetime_check = new DateTime();

                        datetime_check = DateTime.ParseExact((ddl_shifttimestart.SelectedItem.ToString()), "dd/MM/yyyy HH:mm", culture_);
                        //litDebug.Text += lbl_shift_date.Text + "|" + datetime_check.ToString("dd/MM/yyyy");

                        string valueholiday_check = "0";

                        if (d_start.ToString("dd/MM/yyyy") == datetime_check.ToString("dd/MM/yyyy"))
                        {
                            valueholiday_check = "1";
                        }
                        else
                        {
                            valueholiday_check = "2";
                        }


                        //ot holiday and  day work
                        if ((lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1"))//-- && valueholiday_check.ToString() == "1")
                        {

                            DateTime tt = DateTime.ParseExact(ddl_shifttimestart.SelectedItem.ToString(), "dd/MM/yyyy HH:mm", culture_);
                            //DateTime DateTime_StartHoliday = DateTime.ParseExact((lbl_shift_date.Text + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm:ss", culture_);

                            DateTime DateTime_StartHoliday = DateTime.ParseExact((tt.ToString("dd/MM/yyyy") + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_EndHoliday = DateTime.ParseExact((lbl_shift_date.Text + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture_);

                            DateTime DateTime_Out = DateTime.ParseExact(ddl_shifttimeend.SelectedItem.ToString(), "dd/MM/yyyy HH:mm", culture_);
                            //DateTime DateTime_Break = DateTime.ParseExact((lbl_shift_date.Text + " " + lbl_parttime_break_start_time.Text), "dd/MM/yyyy HH:mm:ss", culture_);

                            DateTime DateTime_Break = DateTime.ParseExact((tt.ToString("dd/MM/yyyy") + " " + lbl_parttime_break_start_time.Text), "dd/MM/yyyy HH:mm", culture_);

                            //DateTime DateTime_Break = DateTime.ParseExact((lbl_date_start.Text + " " + "12:00:00"), "dd/MM/yyyy HH:mm:ss", culture);

                            //lbl_break_start
                            string input_decimal_holiday1 = "";
                            double resulttime_holiday1;
                            string matFloor = "";

                            //lbl_shift_timestart.Text

                            //var result_time_holiday = new DateTime();
                            if (DateTime_Out < DateTime_Break && DateTime_Out < DateTime_EndHoliday)
                            {
                                //litDebug.Text += "1" + "|";
                                var result_time_holiday1 = DateTime_Out.Subtract(DateTime_StartHoliday).TotalHours;
                                matFloor = (Math.Floor(Convert.ToDecimal(result_time_holiday1.ToString()))).ToString();
                            }
                            else if (DateTime_Out > DateTime_Break && DateTime_Out < DateTime_EndHoliday)
                            {
                                //litDebug.Text += "2" + "|";
                                var result_time_holiday1 = DateTime_Out.Subtract(DateTime_StartHoliday).TotalHours;
                                matFloor = (Math.Floor(Convert.ToDecimal(result_time_holiday1.ToString()) - Convert.ToDecimal("1.00"))).ToString();
                            }
                            else // -1
                            {
                                //litDebug.Text += "3" + "|";
                                //litDebug.Text += DateTime_Out.ToString() + "|" + DateTime_Break.ToString() + "|" + DateTime_EndHoliday.ToString() + "|" + DateTime_StartHoliday.ToString();

                                var result_time_holiday1 = DateTime_EndHoliday.Subtract(DateTime_StartHoliday).TotalHours;
                                matFloor = (Math.Floor(Convert.ToDecimal(result_time_holiday1.ToString()) - Convert.ToDecimal("1.00"))).ToString();

                            }

                            string input_decimal_holiday = String.Format("{0:N2}", Convert.ToDecimal(matFloor.ToString()));

                            var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                            if (regex.IsMatch(input_decimal_holiday))
                            {
                                var decimal_holiday = regex.Match(input_decimal_holiday).Value;
                                //litDebug1.Text = decimal_places.ToString();
                                decimal value_holiday = Convert.ToDecimal(decimal_holiday.ToString());
                                int n_holiday = Convert.ToInt32(value_holiday);
                                // litDebug1.Text += value.ToString() + ",";

                                if (n_holiday >= 50)
                                {
                                    lbl_shift_otholiday.Text = (Math.Floor(Convert.ToDecimal(matFloor.ToString())) + ".50").ToString();
                                    if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                    {
                                        lbl_hours_otholiday.Text = "x1";
                                    }
                                    else
                                    {
                                        lbl_hours_otholiday.Text = "x2";
                                    }


                                }
                                else
                                {

                                    lbl_shift_otholiday.Text = (String.Format("{0:N2}", Convert.ToDecimal(matFloor.ToString())));
                                    if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                    {
                                        lbl_hours_otholiday.Text = "x1";
                                    }
                                    else
                                    {
                                        lbl_hours_otholiday.Text = "x2";
                                    }
                                    ////lbl_hours_otholiday.Text = "x1";

                                }
                            }

                        }
                        else
                        {
                            lbl_shift_otholiday.Text = "0.00";
                        }

                        // ot after
                        string time_finish = "";
                        DateTime Datestarting_otafter = new DateTime();
                        DateTime time_finish_end = new DateTime();
                        //DateTime new_time_start_ot = new DateTime();

                        time_finish = lbl_parttime_end_time.Text;
                        time_finish_end = DateTime.ParseExact(time_finish, "HH:mm", CultureInfo.InvariantCulture);
                        time_finish_end = time_finish_end.AddMinutes(30);
                        Datestarting_otafter = DateTime.ParseExact((lbl_shift_date.Text + " " + time_finish_end.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture_);


                        var result_time = DateTime_End.Subtract(Datestarting_otafter).TotalHours;
                        double result_time_ = Convert.ToDouble(result_time);

                        //if (lbl_shift_timeend_otscan.Text != "")
                        if (ddl_shifttimeend_ot.SelectedValue != "")
                        {

                            if (result_time_ > 0)
                            {

                                string input_decimal_number = String.Format("{0:N2}", Convert.ToDecimal(result_time.ToString()));
                                //string input_decimal_otafter = String.Format("{0:N2}", Convert.ToDecimal(result_time.ToString()));
                                var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                                if (regex.IsMatch(input_decimal_number))
                                {
                                    var decimal_places = regex.Match(input_decimal_number).Value;
                                    decimal value = Convert.ToDecimal(decimal_places.ToString());
                                    int n = Convert.ToInt32(value);

                                    if (n >= 50)
                                    {

                                        if ((lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1"))// && valueholiday_check.ToString() == "1")//is holiday and no day work
                                        {

                                            //litDebug.Text = "1";
                                            lbl_shift_otafter.Text = ((Math.Floor(Convert.ToDecimal(result_time.ToString())) + ".50").ToString());
                                            if (Convert.ToDouble(lbl_shift_otholiday.Text) >= 8)
                                            {
                                                lbl_hours_otafter.Text = "x3";
                                            }
                                            else
                                            {
                                                lbl_hours_otafter.Text = "x1";
                                            }

                                        }
                                        else
                                        {
                                            lbl_shift_otafter.Text = ((Math.Floor(Convert.ToDecimal(result_time.ToString())) + ".50").ToString());
                                            lbl_hours_otafter.Text = "x1.5";
                                        }


                                    }
                                    else
                                    {

                                        if ((lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1"))// && valueholiday_check.ToString() == "1")//is holiday and no day work
                                        {
                                            //litDebug.Text = valueholiday_check.ToString();
                                            lbl_shift_otafter.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_time.ToString()))).ToString())));
                                            if (Convert.ToDouble(lbl_shift_otholiday.Text) >= 8)
                                            {
                                                lbl_hours_otafter.Text = "x3";
                                            }
                                            else
                                            {
                                                lbl_hours_otafter.Text = "x1";
                                            }

                                        }
                                        else
                                        {


                                            lbl_shift_otafter.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_time.ToString()))).ToString())));
                                            lbl_hours_otafter.Text = "x1.5";
                                        }


                                    }
                                }
                            }
                            else
                            {

                                lbl_shift_otafter.Text = "0.00";

                            }
                        }
                        else
                        {

                            lbl_shift_otafter.Text = "0.00";
                        }
                        // ot after

                        //ot before
                        if (ddl_shifttimestart_ot.SelectedValue != "")
                        {

                            string time_workstart = "";
                            DateTime Datestart_otbefore = new DateTime();
                            DateTime time_work_start = new DateTime();
                            //DateTime new_time_start_ot = new DateTime();

                            time_workstart = lbl_parttime_start_time.Text;
                            time_work_start = DateTime.ParseExact(time_workstart, "HH:mm", CultureInfo.InvariantCulture);

                            if (shifttime_start > shifttime_end)
                            {

                                //litDebug.Text = dstart_del.ToString("dd/MM/yyyy") + "|" + lbl_shift_date.Text;
                                //Datestart_otbefore = DateTime.ParseExact((dstart_del.ToString() + " " + time_work_start.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture_);
                                //Datestart_otbefore = DateTime.ParseExact((DateTime.ParseExact((dstart_del.ToString()), "dd/MM/yyyy", culture_) + " " + time_work_start.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture_);
                                Datestart_otbefore = DateTime.ParseExact((dstart_del.ToString("dd/MM/yyyy") + " " + time_work_start.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture_);


                            }
                            else
                            {
                                Datestart_otbefore = DateTime.ParseExact((lbl_shift_date.Text + " " + time_work_start.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture_);

                            }


                            //litDebug.Text = Datestart_otbefore.ToString("dd/MM/yyyy HH:mm:ss");

                            var result_timebefore = Datestart_otbefore.Subtract(DateTime_Start).TotalHours;
                            double result_time_before = Convert.ToDouble(result_timebefore);

                            //ot before
                            if (result_time_before > 0)
                            {

                                string input_decimal_before = String.Format("{0:N2}", Convert.ToDecimal(result_timebefore.ToString()));
                                //string input_decimal_otafter = String.Format("{0:N2}", Convert.ToDecimal(result_time.ToString()));
                                var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                                if (regex.IsMatch(input_decimal_before))
                                {
                                    var decimal_before = regex.Match(input_decimal_before).Value;
                                    //litDebug1.Text = decimal_places.ToString();
                                    decimal value_before = Convert.ToDecimal(decimal_before.ToString());
                                    int n_before = Convert.ToInt32(value_before);
                                    // litDebug1.Text += value.ToString() + ",";

                                    if (n_before >= 50)
                                    {
                                        //chk_date_ot.Checked = true;
                                        if ((lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1"))// && valueholiday_check.ToString() == "1") //is holiday and no day work
                                        {
                                            lbl_shift_otbefore.Text = ((Math.Floor(Convert.ToDecimal(result_timebefore.ToString())) + ".50").ToString());
                                            if (Convert.ToDouble(lbl_shift_otholiday.Text) >= 8)
                                            {
                                                lbl_hours_otbefore.Text = "x3";
                                            }
                                            else
                                            {
                                                lbl_hours_otbefore.Text = "x1";
                                            }

                                        }
                                        else
                                        {
                                            lbl_shift_otbefore.Text = ((Math.Floor(Convert.ToDecimal(result_timebefore.ToString())) + ".50").ToString());
                                            lbl_hours_otbefore.Text = "x1.5";
                                        }


                                        ////decimal value1_before = Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_timebefore.ToString())) + ".50").ToString());
                                        //////decimal value2 = Convert.ToDecimal(txtot_after.Text);
                                        ////double n1_before = Convert.ToDouble(value1_before);
                                        ////if (n1_before >= 0.50)
                                        ////{
                                        ////    //litDebug1.Text += "9";
                                        ////    txtot_before.Visible = true;
                                        ////    lblot_before.Visible = true;

                                        ////}
                                        ////else
                                        ////{

                                        ////    txtot_before.Visible = true;
                                        ////    lblot_before.Visible = true;

                                        ////}

                                    }
                                    else
                                    {

                                        if ((lbl_holiday_idx_otday.Text != "0" || day_offemployee.ToString() == "1"))// && valueholiday_check.ToString() == "1") //is holiday and no day work
                                        {
                                            lbl_shift_otbefore.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_timebefore.ToString()))).ToString())));
                                            if (Convert.ToDouble(lbl_shift_otholiday.Text) >= 8)
                                            {
                                                lbl_hours_otbefore.Text = "x3";
                                            }
                                            else
                                            {
                                                lbl_hours_otbefore.Text = "x1";
                                            }

                                        }
                                        else
                                        {
                                            lbl_shift_otbefore.Text = (String.Format("{0:N2}", Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_timebefore.ToString()))).ToString())));
                                            lbl_hours_otbefore.Text = "x1.5";
                                        }


                                        ////decimal value1_before = Convert.ToDecimal(Convert.ToDecimal((Math.Floor(Convert.ToDecimal(result_timebefore.ToString()))).ToString()));
                                        //////decimal value2 = Convert.ToDecimal(txtot_after.Text);
                                        ////double n1_before = Convert.ToDouble(value1_before);
                                        ////if (n1_before >= 0.50)
                                        ////{
                                        ////    //litDebug1.Text += "7";
                                        ////    txtot_before.Visible = false;
                                        ////    lblot_before.Visible = false;

                                        ////}
                                        ////else
                                        ////{
                                        ////    //litDebug1.Text += "8";
                                        ////    txtot_before.Visible = false;
                                        ////    lblot_before.Visible = false;


                                        ////}
                                    }
                                }
                                else
                                {
                                    lbl_shift_otbefore.Text = "0.00";
                                }
                            }
                            else
                            {

                                lbl_shift_otbefore.Text = "0.00";
                                //txtot_before.Visible = false;
                                //txtot_before.Text = "0.00";
                            }

                        }
                        else
                        {
                            lbl_shift_otbefore.Text = "0.00";
                        }

                        //check unable checkbok
                        if (lbl_shift_otbefore.Text == "0.00" && lbl_shift_otafter.Text == "0.00" && lbl_shift_otholiday.Text == "0.00")
                        {
                            chk_otday.Enabled = false;
                        }
                        else
                        {
                            chk_otday.Enabled = true;
                        }
                        //check unable checkbok


                        //set unable ot
                        if (lbl_shift_otbefore.Text == "0.00")
                        {
                            lbl_shift_otbefore.Visible = false;
                        }
                        else
                        {
                            lbl_shift_otbefore.Visible = true;
                        }

                        if (lbl_shift_otafter.Text == "0.00")
                        {
                            lbl_shift_otafter.Visible = false;
                        }
                        else
                        {
                            lbl_shift_otafter.Visible = true;
                        }

                        if (lbl_shift_otholiday.Text == "0.00")
                        {
                            lbl_shift_otholiday.Visible = false;
                        }
                        else
                        {
                            lbl_shift_otholiday.Visible = true;
                        }

                        if (ddl_shifttimestart_ot.SelectedValue == "")
                        {
                            ddl_shifttimestart_ot.Visible = false;
                        }
                        else
                        {
                            ddl_shifttimestart_ot.Visible = true;
                        }

                        if (ddl_shifttimeend_ot.SelectedValue == "")
                        {
                            ddl_shifttimeend_ot.Visible = false;
                        }
                        else
                        {
                            ddl_shifttimeend_ot.Visible = true;
                        }

                    }
                    else
                    {
                        chk_otday.Enabled = false;
                    }


                }
                break;
            case "GvFileMenoOTMonth":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11_permwait = (HyperLink)e.Row.FindControl("btnDL11_permwait");
                    HiddenField hidFile11_permwait = (HiddenField)e.Row.FindControl("hidFile11_permwait");
                    string LinkHost11_permwait = string.Format("http://{0}", Request.Url.Host);

                    string path = HttpContext.Current.Request.Url.AbsolutePath;

                    //string input2 = "/taokaenoi.co.th";
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    //litDebug.Text = path;
                    //litDebug1.Text = LinkHost11_permwait.ToString();
                    //litDebug.Text = result_path[0].ToString() + "|" + result_path[1].ToString();

                    //if (result_path[0].ToString() == "")
                    if (LinkHost11_permwait.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11_permwait.NavigateUrl = LinkHost11_permwait + "/" + result_path[1].ToString() + MapURL(hidFile11_permwait.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11_permwait.NavigateUrl = LinkHost11_permwait + MapURL(hidFile11_permwait.Value);//path + MapURL(hidFile11_permwait.Value);
                    }

                }

                break;
            case "GvFileMenoWaitOTMonth":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11_permwait = (HyperLink)e.Row.FindControl("btnDL11_permwait");
                    HiddenField hidFile11_permwait = (HiddenField)e.Row.FindControl("hidFile11_permwait");
                    string LinkHost11_permwait = string.Format("http://{0}", Request.Url.Host);

                    string path = HttpContext.Current.Request.Url.AbsolutePath;

                    //string input2 = "/taokaenoi.co.th";
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    //litDebug.Text = result_path[0].ToString() + "|" + result_path[1].ToString();
                    //if (result_path[0].ToString() == "")
                    if (LinkHost11_permwait.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11_permwait.NavigateUrl = LinkHost11_permwait + "/" + result_path[1].ToString() + MapURL(hidFile11_permwait.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11_permwait.NavigateUrl = LinkHost11_permwait + MapURL(hidFile11_permwait.Value);//path + MapURL(hidFile11_permwait.Value);
                    }

                }

                break;

            case "GvViewDetailOTMont":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label lbl_sum_hours_view = (Label)e.Row.FindControl("lbl_sum_hours_view");
                    Label txtot_before_view = (Label)e.Row.FindControl("txtot_before_view");
                    Label lblot_before_view = (Label)e.Row.FindControl("lblot_before_view");
                    Label txtot_after_view = (Label)e.Row.FindControl("txtot_after_view");
                    Label lblot_after_view = (Label)e.Row.FindControl("lblot_after_view");
                    Label txtot_holiday_view = (Label)e.Row.FindControl("txtot_holiday_view");
                    Label lblot_holiday_view = (Label)e.Row.FindControl("lblot_holiday_view");

                    Label lbl_ot_before_status_view = (Label)e.Row.FindControl("lbl_ot_before_status_view");
                    CheckBox chk_ot_before_view = (CheckBox)e.Row.FindControl("chk_ot_before_view");


                    HiddenField hfM0NodeIDX = (HiddenField)FvDetailOTMont.FindControl("hfM0NodeIDX");
                    HiddenField hfM0ActoreIDX = (HiddenField)FvDetailOTMont.FindControl("hfM0ActoreIDX");
                    HiddenField hfM0StatusIDX = (HiddenField)FvDetailOTMont.FindControl("hfM0StatusIDX");

                    Label lbl_holiday_idx_view = (Label)e.Row.FindControl("lbl_holiday_idx_view");
                    Label lbl_day_off_view = (Label)e.Row.FindControl("lbl_day_off_view");
                    Label lbl_date_condition_view = (Label)e.Row.FindControl("lbl_date_condition_view");
                    Label lbl_detailholiday_view = (Label)e.Row.FindControl("lbl_detailholiday_view");
                    Label lbl_detaildayoff_view = (Label)e.Row.FindControl("lbl_detaildayoff_view");

                    Label lbl_comment_hr_view = (Label)e.Row.FindControl("lbl_comment_hr_view");

                    chk_ot_before_view.Visible = false;
                    lbl_detailholiday_view.Visible = false;
                    lbl_detaildayoff_view.Visible = false;


                    decimal value = Convert.ToDecimal(lbl_sum_hours_view.Text);
                    int n = Convert.ToInt32(value);
                    tot_actual_otmonth += Convert.ToDecimal(value);

                    decimal value_before = Convert.ToDecimal(txtot_before_view.Text);
                    int n_before = Convert.ToInt32(value);
                    if (lbl_ot_before_status_view.Text == "1")
                    {
                        tot_actual_otbefore += Convert.ToDecimal(value_before);
                    }

                    if (txtot_before_view.Text == "0.00")
                    {
                        txtot_before_view.Visible = false;
                        lblot_before_view.Visible = false;
                        chk_ot_before_view.Visible = false;

                    }
                    else
                    {

                        //litDebug.Text += "2";

                        txtot_before_view.Visible = true;
                        lblot_before_view.Visible = true;
                        //chk_ot_before_view.Visible = true;
                        if (lbl_ot_before_status_view.Text == "1" && txtot_before_view.Text != "0.00")
                        {
                            chk_ot_before_view.Checked = true;
                        }
                        else if (lbl_ot_before_status_view.Text == "2" && txtot_before_view.Text != "0.00")
                        {
                            chk_ot_before_view.Checked = false;
                        }
                        ////else if(lbl_ot_before_status_view.Text == "2")
                        ////{
                        ////    chk_ot_before_view.Checked = false;

                        ////}

                    }

                    //
                    decimal value_after = Convert.ToDecimal(txtot_after_view.Text);
                    int n_after = Convert.ToInt32(value);
                    tot_actual_otafter += Convert.ToDecimal(value_after);
                    if (txtot_after_view.Text == "0.00")
                    {
                        txtot_after_view.Visible = false;
                        lblot_after_view.Visible = false;
                    }
                    else
                    {
                        txtot_after_view.Visible = true;
                        lblot_after_view.Visible = true;
                    }

                    //
                    decimal value_holiday = Convert.ToDecimal(txtot_holiday_view.Text);
                    int n_holiday = Convert.ToInt32(value);
                    tot_actual_otholiday += Convert.ToDecimal(value_holiday);
                    if (txtot_holiday_view.Text == "0.00")
                    {
                        txtot_holiday_view.Visible = false;
                        lblot_holiday_view.Visible = false;
                    }
                    else
                    {
                        txtot_holiday_view.Visible = true;
                        lblot_holiday_view.Visible = true;
                    }

                    //day off employee
                    string day_off = lbl_day_off_view.Text;
                    string day_condition = lbl_date_condition_view.Text;
                    string[] set_test = day_off.Split(',');
                    string day_offemployee = "";

                    if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                    {
                        day_offemployee = "1"; //yes day off
                                               //litDebug.Text += day_offemployee.ToString() + "|";
                                               //litDebug.Text += "1" + "|";
                    }
                    else
                    {
                        day_offemployee = "2"; // no day off
                                               //litDebug.Text += day_offemployee.ToString() + "|";
                    }

                    //day off employee
                    if (lbl_holiday_idx_view.Text != "0" || day_offemployee.ToString() == "1")
                    {
                        if (lbl_holiday_idx_view.Text != "0")
                        {
                            lbl_detailholiday_view.Visible = true;
                        }
                        else if (day_offemployee.ToString() == "1")
                        {
                            lbl_detaildayoff_view.Visible = true;
                        }

                    }
                    else
                    {
                        lbl_detailholiday_view.Visible = false;
                        lbl_detaildayoff_view.Visible = false;
                    }
                    //

                    switch (int.Parse(hfM0NodeIDX.Value))
                    {

                        case 1:
                            switch (int.Parse(hfM0StatusIDX.Value))
                            {
                                case 7:
                                    if ((lbl_ot_before_status_view.Text == "1" && txtot_before_view.Text != "0.00") || (lbl_ot_before_status_view.Text == "2" && txtot_before_view.Text != "0.00"))
                                    {
                                        chk_ot_before_view.Visible = true;
                                    }
                                    break;
                            }

                            break;

                        case 2:
                        case 3:

                            if ((lbl_ot_before_status_view.Text == "1" && txtot_before_view.Text != "0.00") || (lbl_ot_before_status_view.Text == "2" && txtot_before_view.Text != "0.00"))
                            {
                                chk_ot_before_view.Visible = true;
                            }
                            ////else
                            ////{
                            ////    chk_ot_before_view.Visible = false;
                            ////}


                            break;

                    }

                    //set row color hr comment
                    if (lbl_comment_hr_view.Text != "" && lbl_comment_hr_view.Text != "-")
                    {
                        e.Row.BackColor = System.Drawing.Color.Yellow;
                    }
                    //set row color hr comment


                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    Label lit_total_hoursmonth_view = (Label)e.Row.FindControl("lit_total_hoursmonth_view");
                    lit_total_hoursmonth_view.Text = String.Format("{0:N2}", tot_actual_otmonth); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_before_view = (Label)e.Row.FindControl("lit_total_hoursot_before_view");
                    lit_total_hoursot_before_view.Text = String.Format("{0:N2}", tot_actual_otbefore); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_after_view = (Label)e.Row.FindControl("lit_total_hoursot_after_view");
                    lit_total_hoursot_after_view.Text = String.Format("{0:N2}", tot_actual_otafter); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_holiday_view = (Label)e.Row.FindControl("lit_total_hoursot_holiday_view");
                    lit_total_hoursot_holiday_view.Text = String.Format("{0:N2}", tot_actual_otholiday); // Convert.ToString(tot_actual);

                }

                break;
            case "GvViewWaitApproveOTMont":

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    //// HiddenField hfM0NodeIDX = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfM0NodeIDX");
                    //// HiddenField hfM0ActoreIDX = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfM0ActoreIDX");
                    //// CheckBox chk_allotbefore = (CheckBox)e.Row.FindControl("chk_allotbefore");

                    ////// CheckBox chk_allotbefore = (CheckBox)GvCreateShiftRotate.HeaderRow.Cells[0].FindControl("chk_allotbefore");
                    //// if (hfM0NodeIDX.Value == "2" && hfM0ActoreIDX.Value == "2")
                    //// {

                    ////     chk_allotbefore.Enabled = true;

                    //// }
                    //// else
                    //// {
                    ////     chk_allotbefore.Enabled = false;
                    //// }
                }

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    HiddenField hfM0NodeIDX = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfM0NodeIDX");
                    HiddenField hfM0ActoreIDX = (HiddenField)FvDetailOTMontWaitapprove.FindControl("hfM0ActoreIDX");

                    Label lbl_sum_hours_approve = (Label)e.Row.FindControl("lbl_sum_hours_approve");
                    TextBox lbl_comment_hr_approve = (TextBox)e.Row.FindControl("lbl_comment_hr_approve");


                    Label txtot_before_approve = (Label)e.Row.FindControl("txtot_before_approve");
                    Label lblot_before_approve = (Label)e.Row.FindControl("lblot_before_approve");
                    Label txtot_after_approve = (Label)e.Row.FindControl("txtot_after_approve");
                    Label lblot_after_approve = (Label)e.Row.FindControl("lblot_after_approve");
                    Label txtot_holiday_approve = (Label)e.Row.FindControl("txtot_holiday_approve");
                    Label lblot_holiday_approve = (Label)e.Row.FindControl("lblot_holiday_approve");

                    Label lbl_ot_before_status_approve = (Label)e.Row.FindControl("lbl_ot_before_status_approve");
                    CheckBox chkstatus_otbefore = (CheckBox)e.Row.FindControl("chkstatus_otbefore");

                    CheckBox chk_allotbefore = (CheckBox)GvViewWaitApproveOTMont.HeaderRow.Cells[4].FindControl("chk_allotbefore");

                    Label lbl_detailholiday_approve = (Label)e.Row.FindControl("lbl_detailholiday_approve");
                    Label lbl_detaildayoff_approve = (Label)e.Row.FindControl("lbl_detaildayoff_approve");

                    Label lbl_holiday_idx_approve = (Label)e.Row.FindControl("lbl_holiday_idx_approve");
                    Label lbl_day_off_approve = (Label)e.Row.FindControl("lbl_day_off_approve");
                    Label lbl_date_condition_approve = (Label)e.Row.FindControl("lbl_date_condition_approve");

                    DropDownList ddl_timebefore_approve = (DropDownList)e.Row.FindControl("ddl_timebefore_approve");
                    DropDownList ddl_timeafter_approve = (DropDownList)e.Row.FindControl("ddl_timeafter_approve");
                    DropDownList ddl_timeholiday_approve = (DropDownList)e.Row.FindControl("ddl_timeholiday_approve");

                    chkstatus_otbefore.Checked = false;

                    //decimal value = Convert.ToDecimal(lbl_sum_hours_approve.Text);
                    //int n = Convert.ToInt32(value);
                    //tot_actual_otmonth += Convert.ToDecimal(value);

                    //totactual_otbefore += Convert.ToDecimal(ddl_timebefore.SelectedValue);
                    //decimal value_before = Convert.ToDecimal(txtot_before_approve.Text);

                    if (txtot_before_approve.Text == "0.00")
                    {
                        txtot_before_approve.Visible = false;
                        lblot_before_approve.Visible = false;
                        chkstatus_otbefore.Visible = false;
                        chkstatus_otbefore.Checked = false;
                    }
                    else
                    {
                        //txtot_before_approve.Visible = true;
                        lblot_before_approve.Visible = true;

                        chkstatus_otbefore.Visible = true;
                        chk_allotbefore.Enabled = true;

                        if ((hfM0NodeIDX.Value == "2" && hfM0ActoreIDX.Value == "2"))
                        {

                            //if (lbl_ot_before_status_approve.Text == "1" || lbl_ot_before_status_approve.Text == "0")
                            if (lbl_ot_before_status_approve.Text == "1")
                            {
                                chkstatus_otbefore.Checked = true;

                            }
                            //else
                            //{
                            //    chkstatus_otbefore.Checked = false;
                            //}

                        }
                        ////else
                        ////{
                        ////    chkstatus_otbefore.Checked = false;
                        ////}

                    }

                    if (txtot_after_approve.Text == "0.00")
                    {
                        txtot_after_approve.Visible = false;
                        lblot_after_approve.Visible = false;
                    }
                    else
                    {
                        //txtot_after_approve.Visible = true;
                        lblot_after_approve.Visible = true;
                    }

                    //litDebug1.Text += ddl_timeholiday_approve.SelectedValue.ToString();
                    if (txtot_holiday_approve.Text == "0.00")
                    {
                        txtot_holiday_approve.Visible = false;
                        lblot_holiday_approve.Visible = false;
                    }
                    else
                    {
                        //txtot_holiday_approve.Visible = true;
                        lblot_holiday_approve.Visible = true;
                    }

                    //chek comment hr
                    if (hfM0NodeIDX.Value == "3" && hfM0ActoreIDX.Value == "3")
                    {
                        lbl_comment_hr_approve.Enabled = true;
                        chkstatus_otbefore.Enabled = false;

                        if (lbl_ot_before_status_approve.Text == "1")
                        {
                            chkstatus_otbefore.Checked = true;


                        }
                        //chkstatus_otbefore.Enabled = false;
                    }
                    else
                    {
                        lbl_comment_hr_approve.Enabled = false;
                        chkstatus_otbefore.Enabled = true;
                    }

                    //day off employee
                    string day_off = lbl_day_off_approve.Text;
                    string day_condition = lbl_date_condition_approve.Text;
                    string[] set_test = day_off.Split(',');
                    string day_offemployee = "";

                    if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                    {
                        day_offemployee = "1"; //yes day off
                                               //litDebug.Text += day_offemployee.ToString() + "|";
                                               //litDebug.Text += "1" + "|";
                    }
                    else
                    {
                        day_offemployee = "2"; // no day off
                                               //litDebug.Text += day_offemployee.ToString() + "|";
                    }
                    //day off employee

                    //set ddl hour before
                    if (txtot_before_approve.Text != "")
                    {

                        if (hfM0NodeIDX.Value == "3" && hfM0ActoreIDX.Value == "3")
                        {

                            ddl_timebefore_approve.Enabled = false;
                            getHourToOT(ddl_timebefore_approve, Convert.ToDouble(txtot_before_approve.Text));

                        }
                        else
                        {

                            ddl_timebefore_approve.Enabled = true;
                            getHourToOT(ddl_timebefore_approve, Convert.ToDouble(txtot_before_approve.Text));
                        }
                    }
                    else
                    {
                        getHourToOT(ddl_timebefore_approve, Convert.ToDouble("0"));

                    }

                    //set ddl hour after
                    if (txtot_after_approve.Text != "")
                    {
                        if (hfM0NodeIDX.Value == "3" && hfM0ActoreIDX.Value == "3")
                        {
                            ddl_timeafter_approve.Enabled = false;
                            getHourToOT(ddl_timeafter_approve, Convert.ToDouble(txtot_after_approve.Text));
                        }
                        else
                        {
                            ddl_timeafter_approve.Enabled = true;
                            getHourToOT(ddl_timeafter_approve, Convert.ToDouble(txtot_after_approve.Text));
                        }

                    }
                    else
                    {
                        getHourToOT(ddl_timeafter_approve, Convert.ToDouble("0"));
                    }

                    //set ddl hour holiday
                    if (txtot_holiday_approve.Text != "")
                    {
                        if (hfM0NodeIDX.Value == "3" && hfM0ActoreIDX.Value == "3")
                        {
                            ddl_timeholiday_approve.Enabled = false;
                            getHourToOT(ddl_timeholiday_approve, Convert.ToDouble(txtot_holiday_approve.Text));

                        }
                        else
                        {
                            ddl_timeholiday_approve.Enabled = true;
                            getHourToOT(ddl_timeholiday_approve, Convert.ToDouble(txtot_holiday_approve.Text));
                        }
                    }
                    else
                    {
                        getHourToOT(ddl_timeholiday_approve, Convert.ToDouble("0"));
                    }


                    if (lbl_holiday_idx_approve.Text != "0" || day_offemployee.ToString() == "1")
                    {
                        if (lbl_holiday_idx_approve.Text != "0")
                        {
                            lbl_detailholiday_approve.Visible = true;
                            lbl_detaildayoff_approve.Visible = false;
                        }
                        else if (day_offemployee.ToString() == "1")
                        {
                            lbl_detaildayoff_approve.Visible = true;
                            lbl_detailholiday_approve.Visible = false;
                        }

                    }
                    else
                    {
                        lbl_detailholiday_approve.Visible = false;
                        lbl_detaildayoff_approve.Visible = false;
                    }
                    //

                    //sum chek ot all
                    if (ddl_timebefore_approve.SelectedValue != "")
                    {
                        decimal value_before = Convert.ToDecimal(ddl_timebefore_approve.SelectedValue);
                        //int n_before = Convert.ToInt32(value);

                        if (hfM0NodeIDX.Value == "2" && hfM0ActoreIDX.Value == "2")
                        {
                            //if (lbl_ot_before_status_approve.Text == "1" || lbl_ot_before_status_approve.Text == "0")
                            if (lbl_ot_before_status_approve.Text == "1")
                            {
                                tot_actual_otbefore += Convert.ToDecimal(value_before);
                            }
                        }
                        else
                        {
                            if (lbl_ot_before_status_approve.Text == "1")
                            {
                                tot_actual_otbefore += Convert.ToDecimal(value_before);
                            }
                        }
                    }

                    if (ddl_timeafter_approve.SelectedValue != "")
                    {
                        //decimal value_after = Convert.ToDecimal(txtot_after_approve.Text);
                        decimal value_after = Convert.ToDecimal(ddl_timeafter_approve.SelectedValue);
                        //int n_after = Convert.ToInt32(value);
                        tot_actual_otafter += Convert.ToDecimal(value_after);

                    }

                    if (ddl_timeholiday_approve.SelectedValue != "")
                    {
                        //decimal value_holiday = Convert.ToDecimal(txtot_holiday_approve.Text);
                        decimal value_holiday = Convert.ToDecimal(ddl_timeholiday_approve.SelectedValue);
                        //int n_holiday = Convert.ToInt32(value);
                        tot_actual_otholiday += Convert.ToDecimal(value_holiday);

                        //litDebug.Text = tot_actual_otholiday.ToString();
                    }

                    //sum chek ot all

                    //set color comment hr
                    if (lbl_comment_hr_approve.Text != "" && lbl_comment_hr_approve.Text != "-")
                    {
                        e.Row.BackColor = System.Drawing.Color.Yellow;
                    }
                    //set color comment hr

                    //set check all ot before
                    if (hfM0NodeIDX.Value == "3" && hfM0ActoreIDX.Value == "3")
                    {
                        chk_allotbefore.Enabled = false;

                    }
                    else
                    {
                        chk_allotbefore.Enabled = true;
                    }
                }


                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    //Label lit_total_hoursmonth_approve = (Label)e.Row.FindControl("lit_total_hoursmonth_approve");
                    //lit_total_hoursmonth_approve.Text = String.Format("{0:N2}", tot_actual_otmonth); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_before_approve = (Label)e.Row.FindControl("lit_total_hoursot_before_approve");
                    lit_total_hoursot_before_approve.Text = String.Format("{0:N2}", tot_actual_otbefore); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_after_approve = (Label)e.Row.FindControl("lit_total_hoursot_after_approve");
                    lit_total_hoursot_after_approve.Text = String.Format("{0:N2}", tot_actual_otafter); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_holiday_approve = (Label)e.Row.FindControl("lit_total_hoursot_holiday_approve");
                    lit_total_hoursot_holiday_approve.Text = String.Format("{0:N2}", tot_actual_otholiday); // Convert.ToString(tot_actual);

                }

                break;
            case "GvViewEditDetailOTMont":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {



                    int a = 0;
                    int b = 0;

                    if (ViewState["vs_TimeStartEdit"].ToString() != null && ViewState["vs_TimeEndEdit"].ToString() != null)
                    {
                        ovt_u1_document_detail[] _item_TotalOTMonth_edit = (ovt_u1_document_detail[])ViewState["Vs_ViewDetailEditOTMonth"];

                        Label lbl_emp_code_view = (Label)FvDetailOTMont.FindControl("lbl_emp_code_view");


                        CheckBox chk_date_ot_edit = (CheckBox)e.Row.FindControl("chk_date_ot_edit");
                        Label lbl_u1_doc_idx_edit = (Label)e.Row.FindControl("lbl_u1_doc_idx_edit");
                        Label lbl_date_start_edit = (Label)e.Row.FindControl("lbl_date_start_edit");

                        Label lbl_holiday_idx_check_edit = (Label)e.Row.FindControl("lbl_holiday_idx_check_edit");
                        Label lbl_work_start_edit = (Label)e.Row.FindControl("lbl_work_start_edit");
                        Label lbl_work_finish_edit = (Label)e.Row.FindControl("lbl_work_finish_edit");
                        Label lbl_break_start_edit = (Label)e.Row.FindControl("lbl_break_start_edit");
                        Label lbl_break_finish_edit = (Label)e.Row.FindControl("lbl_break_finish_edit");

                        Label lbl_day_off_edit = (Label)e.Row.FindControl("lbl_day_off_edit");
                        Label lbl_date_condition_edit = (Label)e.Row.FindControl("lbl_date_condition_edit");
                        Label lbl_detailholiday_edit = (Label)e.Row.FindControl("lbl_detailholiday_edit");
                        Label lbl_detaildayoff_edit = (Label)e.Row.FindControl("lbl_detaildayoff_edit");

                        TextBox txt_sum_hour_edit = (TextBox)e.Row.FindControl("txt_sum_hour_edit");
                        ////TextBox txt_sum_hour = (TextBox)e.Row.FindControl("txt_sum_hour");
                        TextBox txt_job_edit = (TextBox)e.Row.FindControl("txt_job_edit");
                        TextBox txt_remark_edit = (TextBox)e.Row.FindControl("txt_remark_edit");

                        Label txtot_before_edit = (Label)e.Row.FindControl("txtot_before_edit");
                        Label lblot_before_edit = (Label)e.Row.FindControl("lblot_before_edit");
                        Label txtot_after_edit = (Label)e.Row.FindControl("txtot_after_edit");
                        Label lblot_after_edit = (Label)e.Row.FindControl("lblot_after_edit");
                        Label txtot_holiday_edit = (Label)e.Row.FindControl("txtot_holiday_edit");
                        Label lblot_holiday_edit = (Label)e.Row.FindControl("lblot_holiday_edit");

                        Label lbl_month_idx_start_edit = (Label)e.Row.FindControl("lbl_month_idx_start_edit");
                        Label lbl_month_idx_end_edit = (Label)e.Row.FindControl("lbl_month_idx_end_edit");

                        Label lbl_ot_before_status_edit = (Label)e.Row.FindControl("lbl_ot_before_status_edit");
                        CheckBox chk_ot_before_edit = (CheckBox)e.Row.FindControl("chk_ot_before_edit");

                        //////bind time start
                        DropDownList ddl_timestart_edit = (DropDownList)e.Row.FindControl("ddl_timestart_edit");
                        DropDownList ddl_timeend_edit = (DropDownList)e.Row.FindControl("ddl_timeend_edit");

                        DropDownList ddl_timebefore_edit = (DropDownList)e.Row.FindControl("ddl_timebefore_edit");
                        DropDownList ddl_timeafter_edit = (DropDownList)e.Row.FindControl("ddl_timeafter_edit");
                        DropDownList ddl_timeholiday_edit = (DropDownList)e.Row.FindControl("ddl_timeholiday_edit");

                        TextBox txt_comment_hr_edit = (TextBox)e.Row.FindControl("txt_comment_hr_edit");


                        var d_start = new DateTime();
                        var d_end = new DateTime();
                        var d_finish = new DateTime();
                        var d_workstart = new DateTime();

                        double value_total_ot;
                        double value_total_otresult;

                        double value_total_ot_holiday;
                        double value_total_ot_before;
                        double value_total_ot_diff;


                        IFormatProvider culture_ = new CultureInfo("en-US", true);

                        d_finish = DateTime.ParseExact((lbl_date_start_edit.Text + " " + lbl_work_finish_edit.Text), "dd/MM/yyyy HH:mm", culture_);
                        d_workstart = DateTime.ParseExact((lbl_date_start_edit.Text + " " + lbl_work_start_edit.Text), "dd/MM/yyyy HH:mm", culture_);

                        d_start = DateTime.ParseExact((lbl_date_start_edit.Text + " " + "00:00"), "dd/MM/yyyy HH:mm", culture_);
                        d_end = DateTime.ParseExact((lbl_date_start_edit.Text + " " + "05:59"), "dd/MM/yyyy HH:mm", culture_);

                        double value_otbefore;
                        double value_otafter;
                        DateTime d_finish_addhour_after = new DateTime();
                        string day_offemployee = "";


                        //day off employee
                        string day_off = lbl_day_off_edit.Text;
                        string day_condition = lbl_date_condition_edit.Text;
                        string[] set_test = day_off.Split(',');
                        //string day_offemployee = "";

                        if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                        {
                            day_offemployee = "1"; //yes day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                                                   //litDebug.Text += "1" + "|";
                        }
                        else
                        {
                            day_offemployee = "2"; // no day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                        }
                        //day off employee

                        ovt_timestart_otmonth_detail[] _templist_TimeStartEdit = (ovt_timestart_otmonth_detail[])ViewState["vs_TimeStartEdit"];

                        var _linqTimeStartEdit = (from dt in _templist_TimeStartEdit
                                                  where

                                                  dt.date_start != "" && dt.time_start != ""
                                                  && (dt.date_start == lbl_date_start_edit.Text)
                                                  && ((DateTime.ParseExact((dt.time_start), "dd/MM/yyyy HH:mm", culture_) < d_finish)
                                                  || (
                                                   (lbl_date_start_edit.Text != "0" || day_offemployee.ToString() == "1")
                                                   && (DateTime.ParseExact((dt.time_start), "dd/MM/yyyy HH:mm", culture_) > d_finish)
                                                   && dt.date_start == lbl_date_start_edit.Text && dt.date_start != "" && dt.time_start != "")
                                                   )

                                                  select new
                                                  {
                                                      dt.time_start,
                                                      dt.month_idx
                                                  }).ToList();

                        setDdlData(ddl_timestart_edit, _linqTimeStartEdit.ToList(), "time_start", "month_idx");


                        //a = _linqTimeStartEdit.Count();
                        ddl_timestart_edit.SelectedValue = lbl_month_idx_start_edit.Text;


                        // bind time end //




                        DateTime dstart_add = d_start.AddDays(1);
                        DateTime dend_add = d_end.AddDays(1);






                        ovt_timestart_otmonth_detail[] _templist_TimeEnd = (ovt_timestart_otmonth_detail[])ViewState["vs_TimeEndEdit"];

                        var _linqTimeEnd = (from dt in _templist_TimeEnd
                                            where
                                            (dt.time_end != "" && dt.date_end != ""
                                            && (DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) >= d_finish && dt.date_end == lbl_date_start_edit.Text)
                                                ||
                                                (
                                                   (lbl_holiday_idx_check_edit.Text != "0" || day_offemployee.ToString() == "1")
                                                   && ((DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) >= d_finish && dt.date_end == lbl_date_start_edit.Text)
                                                        || DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) < d_finish && dt.date_end == lbl_date_start_edit.Text)
                                                )

                                                ||
                                                        (DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) >= d_finish
                                                        && DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_).Subtract(d_finish).TotalHours <= 8 && dt.time_end != "")

                                                || (DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) > dstart_add && DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) < dend_add)

                                            )

                                            select new
                                            {
                                                dt.time_end,
                                                dt.month_idx
                                            }).ToList();



                        //b = _linqTimeEnd.Count();

                        setDdlData(ddl_timeend_edit, _linqTimeEnd.ToList(), "time_end", "month_idx");
                        ddl_timeend_edit.SelectedValue = lbl_month_idx_end_edit.Text;


                        if (ddl_timestart_edit.SelectedValue != "" && ddl_timeend_edit.SelectedValue != "")
                        {

                            DateTime DateTime_ScanIn = DateTime.ParseExact(ddl_timestart_edit.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ScanOut = DateTime.ParseExact(ddl_timeend_edit.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);


                            DateTime DateTime_ShiftStart = DateTime.ParseExact(lbl_date_start_edit.Text + " " + lbl_work_start_edit.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ShiftEnd = DateTime.ParseExact(lbl_date_start_edit.Text + " " + lbl_work_finish_edit.Text, "dd/MM/yyyy HH:mm", culture_);

                            DateTime DateTime_BreakStart = DateTime.ParseExact(lbl_date_start_edit.Text + " " + lbl_break_start_edit.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_BreakEnd = DateTime.ParseExact(lbl_date_start_edit.Text + " " + lbl_break_finish_edit.Text, "dd/MM/yyyy HH:mm", culture_);

                            double value;
                            double _value_diffbreaktime;
                            decimal _value_timescan_late;
                            double _value_timescan_late_add;

                            double _value_otafter_holiday;

                            DateTime d_finish_addhour = new DateTime();
                            DateTime d_start_addhour = new DateTime();

                            DateTime d_finish_addhour_holiday = new DateTime();
                            double _value_diff_scanout;

                            if (lbl_u1_doc_idx_edit.Text == "0")
                            {
                                chk_date_ot_edit.Checked = false;
                                txt_job_edit.Visible = false;
                                txt_remark_edit.Visible = false;
                                chk_ot_before_edit.Visible = false;

                                if (ddl_timestart_edit.SelectedValue != "" && ddl_timeend_edit.SelectedValue != "")
                                {
                                    ddl_timestart_edit.Visible = true;
                                    ddl_timeend_edit.Visible = true;
                                    chk_date_ot_edit.Enabled = true;


                                    /*

                                    //ot after
                                    if (lbl_work_finish_edit.Text != "")
                                    {
                                        time_finish = lbl_work_finish_edit.Text;
                                        Closetime_end = DateTime.ParseExact(time_finish, "HH:mm", CultureInfo.InvariantCulture);
                                        new_Closetime_end = Closetime_end.AddMinutes(30);

                                        Datestarting = DateTime.ParseExact((lbl_date_start_edit.Text + " " + new_Closetime_end.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture);

                                    }


                                    //ot before
                                    if (lbl_work_start_edit.Text != "")
                                    {
                                        time_start = lbl_work_start_edit.Text;
                                        Closetime_start = DateTime.ParseExact(time_start, "HH:mm", CultureInfo.InvariantCulture);
                                        //new_Closetime_start = Closetime_start.AddMinutes(30);

                                        Datestart_otbefore = DateTime.ParseExact((lbl_date_start_edit.Text + " " + Closetime_start.ToString("HH:mm")), "dd/MM/yyyy HH:mm", culture);

                                    }
                                    */

                                    //ot before
                                    value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                    //if ((Math.Floor(value_otbefore * 2) / 2) > 0)
                                    if (d_workstart.Subtract(DateTime_ScanIn).TotalMinutes >= 30)
                                    {
                                        txtot_before_edit.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        getHourToOT(ddl_timebefore_edit, Convert.ToDouble(txtot_before_edit.Text));
                                    }
                                    else
                                    {
                                        txtot_before_edit.Text = "0";//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        getHourToOT(ddl_timebefore_edit, Convert.ToDouble(txtot_before_edit.Text));
                                    }




                                    //ot after
                                    if (DateTime_ScanOut > DateTime_ScanIn)
                                    {

                                        d_finish_addhour_after = d_finish.AddMinutes(30);

                                        //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                        //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                        value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                        txtot_after_edit.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        //litDebug.Text += value_otafter.ToString() + "|";

                                        //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                        getHourToOT(ddl_timeafter_edit, Convert.ToDouble(txtot_after_edit.Text));
                                    }
                                    else
                                    {
                                        ddl_timeafter_edit.Visible = false;
                                    }


                                    //ot holiday
                                    if (lbl_holiday_idx_check_edit.Text != "0" || day_offemployee.ToString() == "1")
                                    {


                                        //if (lbl_holiday_idx_check_edit.Text != "0")
                                        //{
                                        //    lbl_detailholiday_edit.Visible = true;
                                        //    lbl_detaildayoff_edit.Visible = false;
                                        //}
                                        //else if (day_offemployee.ToString() == "1")
                                        //{
                                        //    lbl_detaildayoff_edit.Visible = true;
                                        //    lbl_detailholiday_edit.Visible = false;
                                        //}

                                        if (DateTime_ScanIn <= DateTime_ShiftStart || DateTime_ScanIn >= DateTime_ShiftStart)
                                        {
                                            //litDebug.Text += (Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString())/30) * 0.50).ToString()) + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|"  + DateTime_ShiftEnd.ToString("dd/MM/yyyy HH:mm");
                                            ////decimal value = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                            ////litDebug1.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            //litDebug.Text += _value_diffbreaktime.ToString() + "|";

                                            _value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                            _value_diff_scanout = DateTime_ScanOut.Subtract(DateTime_ShiftEnd).TotalMinutes;

                                            //litDebug1.Text = _value_diff_scanout.ToString() + "|";

                                            if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn <= DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                            {
                                                //litDebug.Text += (Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString())/30) * 0.50).ToString()) + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|"  + DateTime_ShiftEnd.ToString("dd/MM/yyyy HH:mm");

                                                //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";// + DateTime_ShiftEnd.ToString("dd/MM/yyyy HH:mm");
                                                value = Convert.ToDouble((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                                txtot_holiday_edit.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(txtot_holiday_edit.Text));

                                                if (ddl_timeholiday_edit.SelectedValue == "8")//check ot holiday have 8 hour
                                                {

                                                    d_finish_addhour_holiday = DateTime_ShiftEnd.AddMinutes(30);

                                                    _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                                    txtot_after_edit.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                    getHourToOT(ddl_timeafter_edit, Convert.ToDouble(txtot_after_edit.Text));


                                                }
                                            }
                                            else if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn > DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                            {


                                                //check hour .xx
                                                string input_decimal_number_be = (Math.Floor((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60))).ToString(); //((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString();
                                                string input_decimal_number_af = ((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString("#.##");//"1.50";

                                                string decimal_places = String.Empty;


                                                //ViewState["_value_timescan_late_add"] = "0";

                                                var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                                                if (regex.IsMatch(input_decimal_number_af))
                                                {
                                                    decimal_places = regex.Match(input_decimal_number_af).Value;
                                                    if (int.Parse(decimal_places) > 50)
                                                    {

                                                        _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + 1);//Convert.ToDouble((input_decimal_number_be) + 1);
                                                        ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                        d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                        d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                                    }
                                                    else if (int.Parse(decimal_places) < 50)
                                                    {

                                                        _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + .50);//Convert.ToDouble((input_decimal_number_be + .50) );

                                                        ViewState["_value_timescan_late_add"] = _value_timescan_late_add;


                                                        d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                        d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                                    }
                                                    else
                                                    {
                                                        _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be))); //Convert.ToDouble((input_decimal_number_be));
                                                        ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                        d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                        d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));


                                                    }

                                                }


                                                ////litDebug.Text += ViewState["_value_timescan_late_add"].ToString();
                                                //////_value_timescan_late = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;

                                                ////////litDebug.Text += _value_timescan_late.ToString() + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");


                                                ////////litDebug.Text += (Math.Floor(_value_timescan_late * 2) / 2).ToString();

                                                //////_value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();


                                                // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                                if (_value_diff_scanout >= 60)
                                                {
                                                    value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                                }
                                                else
                                                {

                                                    //value = Convert.ToDouble(((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1) );

                                                    value = Convert.ToDouble(((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1) - Convert.ToDouble(ViewState["_value_timescan_late_add"].ToString()));

                                                }

                                                //litDebug1.Text += value.ToString();
                                                txtot_holiday_edit.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                                // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(txtot_holiday_edit.Text));

                                                if (ddl_timeholiday_edit.SelectedValue == "8")//check ot holiday have 8 hour
                                                {
                                                    //litDebug.Text = d_finish_addhour_holiday.ToString();
                                                    //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                                    //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                                    //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                                    d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                                    _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                                    txtot_after_edit.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                    getHourToOT(ddl_timeafter_edit, Convert.ToDouble(txtot_after_edit.Text));

                                                }
                                                else
                                                {
                                                    getHourToOT(ddl_timeafter_edit, Convert.ToDouble("0"));
                                                }

                                                /*

                                                //litDebug.Text = DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                                _value_timescan_late = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;
                                                _value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();

                                                //litDebug.Text = _value_timescan_late_add.ToString();

                                                d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                                if (_value_diff_scanout >= 60)
                                                {
                                                    value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                                }
                                                else
                                                {
                                                    value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                                }

                                                //litDebug1.Text += value.ToString();
                                                txtot_holiday_edit.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                                // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(txtot_holiday_edit.Text));

                                                if (ddl_timeholiday_edit.SelectedValue == "8")//check ot holiday have 8 hour
                                                {
                                                    //litDebug.Text = d_finish_addhour_holiday.ToString();
                                                    //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                                    //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                                    //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                                    d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                                    _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                                    txtot_after_edit.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                    getHourToOT(ddl_timeafter_edit, Convert.ToDouble(txtot_after_edit.Text));

                                                }
                                                else
                                                {
                                                    getHourToOT(ddl_timeafter_edit, Convert.ToDouble("0"));
                                                }

                                                */

                                            }
                                            else if (DateTime_ScanOut < DateTime_ShiftEnd && DateTime_ScanIn >= DateTime_BreakStart)
                                            {
                                                value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                                txtot_holiday_edit.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(txtot_holiday_edit.Text));
                                            }
                                            else //if (DateTime_ScanOut > DateTime_BreakEnd && DateTime_ScanOut < DateTime_ShiftEnd)
                                            {
                                                //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                                value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);

                                                txtot_holiday_edit.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(txtot_holiday_edit.Text));
                                            }

                                        }
                                        else if (DateTime_ScanIn < d_workstart && DateTime_ScanOut < d_finish)
                                        {
                                            //litDebug1.Text = "have case new";

                                            //total ot
                                            value_total_ot = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 60));
                                            value_total_otresult = (Math.Floor(value_total_ot * 2) / 2);
                                            //total ot

                                            if (value_total_otresult >= 9.5) //set -1 breaktime
                                            {

                                                value_total_ot_diff = value_total_otresult - 1;


                                                value_total_ot_holiday = 8;
                                                value_total_ot_before = value_total_ot_diff - value_total_ot_holiday;

                                                //litDebug.Text = value_total_ot_before.ToString();
                                                getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                                getHourToOT(ddl_timebefore_edit, Convert.ToDouble(value_total_ot_before.ToString()));

                                            }
                                            else if (value_total_otresult == 9)
                                            {
                                                value_total_ot_diff = value_total_otresult - 1;
                                                value_total_ot_holiday = 8;
                                                getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                            }
                                            else if (value_total_otresult >= 4 && value_total_otresult <= 5)
                                            {
                                                value_total_ot_before = 4;
                                                getHourToOT(ddl_timebefore_edit, Convert.ToDouble(value_total_ot_before.ToString()));
                                            }
                                            else if (value_total_otresult > 5 && value_total_otresult <= 8)
                                            {
                                                value_total_ot_diff = value_total_otresult - 1;

                                                value_total_ot_before = value_total_ot_diff;
                                                getHourToOT(ddl_timebefore_edit, Convert.ToDouble(value_total_ot_before.ToString()));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        lblot_holiday_edit.Visible = false;
                                        lbl_detaildayoff_edit.Visible = false;
                                        ddl_timeholiday_edit.Visible = false;
                                    }

                                }
                                else
                                {
                                    ddl_timestart_edit.Visible = false;
                                    ddl_timeend_edit.Visible = false;
                                    chk_date_ot_edit.Enabled = false;
                                }
                            }
                            else
                            {
                                chk_date_ot_edit.Checked = true;

                                if (txt_comment_hr_edit.Text != "" && txt_comment_hr_edit.Text != "-")
                                {
                                    e.Row.BackColor = System.Drawing.Color.Yellow;
                                }
                                else
                                {
                                    e.Row.BackColor = System.Drawing.Color.Bisque;

                                }


                                txt_job_edit.Visible = true;
                                txt_remark_edit.Visible = true;

                                tot_actual_otmonth += Convert.ToDecimal(txt_sum_hour_edit.Text);

                                //if (lbl_ot_before_status_edit.Text == "1")
                                //{
                                //    //litDebug.Text += "2";
                                //    tot_actual_otbefore += Convert.ToDecimal(txtot_before_edit.Text);
                                //}

                                if (DateTime_ScanIn < d_workstart)
                                {
                                    //ot before
                                    value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                    //litDebug.Text = value_otbefore.ToString();
                                    //litDebug.Text += d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString() + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm");


                                    if (d_workstart.Subtract(DateTime_ScanIn).TotalMinutes >= 30)
                                    {
                                        //litDebug1.Text += "1";
                                        txtot_before_edit.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();
                                        getHourToOT(ddl_timebefore_edit, Convert.ToDouble(txtot_before_edit.Text));
                                        //ddl_timebefore_edit.Visible = true;

                                    }
                                    else
                                    {

                                        //litDebug1.Text += "3" + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm");

                                        getHourToOT(ddl_timebefore_edit, 0);
                                        //ddl_timebefore_edit.Visible = false;
                                    }

                                    ////if ((Math.Floor(value_otbefore * 2) / 2) > 0)
                                    ////{
                                    ////    //(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                    ////    txtot_before_edit.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();
                                    ////    getHourToOT(ddl_timebefore_edit, Convert.ToDouble(txtot_before_edit.Text));
                                    ////}
                                    ////else
                                    ////{
                                    ////    txtot_before_edit.Text = "0";//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                    ////    getHourToOT(ddl_timebefore_edit, Convert.ToDouble(txtot_before_edit.Text));
                                    ////}

                                }
                                else
                                {
                                    ddl_timebefore_edit.Visible = false;
                                }



                                //ot after
                                if (DateTime_ScanOut > DateTime_ScanIn)
                                {

                                    d_finish_addhour_after = d_finish.AddMinutes(30);

                                    //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                    //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                    value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                    txtot_after_edit.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                    //litDebug.Text += value_otafter.ToString() + "|";

                                    //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                    getHourToOT(ddl_timeafter_edit, Convert.ToDouble(txtot_after_edit.Text));
                                }
                                else
                                {
                                    ddl_timeafter_edit.Visible = false;
                                }


                                //ot holiday
                                if (lbl_holiday_idx_check_edit.Text != "0" || day_offemployee.ToString() == "1")
                                {
                                    ////if (lbl_holiday_idx_check_edit.Text != "0")
                                    ////{
                                    ////    lbl_detailholiday_edit.Visible = true;
                                    ////    lbl_detaildayoff_edit.Visible = false;
                                    ////}
                                    ////else if (day_offemployee.ToString() == "1")
                                    ////{
                                    ////    lbl_detaildayoff_edit.Visible = true;
                                    ////    lbl_detailholiday_edit.Visible = false;
                                    ////}

                                    //if ((DateTime_ScanIn <= DateTime_ShiftStart || DateTime_ScanIn >= DateTime_ShiftStart) && DateTime_ScanOut >= DateTime_ShiftEnd)
                                    if ((DateTime_ScanIn <= DateTime_ShiftStart || DateTime_ScanIn >= DateTime_ShiftStart))
                                    {
                                        //litDebug.Text += (Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString())/30) * 0.50).ToString()) + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|"  + DateTime_ShiftEnd.ToString("dd/MM/yyyy HH:mm");
                                        ////decimal value = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                        ////litDebug1.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        //litDebug.Text += _value_diffbreaktime.ToString() + "|";

                                        //ot before
                                        value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        txtot_before_edit.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        getHourToOT(ddl_timebefore_edit, Convert.ToDouble(txtot_before_edit.Text));

                                        ////if (d_workstart.Subtract(DateTime_ScanIn).TotalMinutes >= 30)
                                        ////{
                                        ////    litDebug1.Text += "1";
                                        ////    txtot_before_edit.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();
                                        ////    getHourToOT(ddl_timebefore_edit, Convert.ToDouble(txtot_before_edit.Text));
                                        ////    //ddl_timebefore_edit.Visible = true;

                                        ////}
                                        ////else
                                        ////{

                                        ////    litDebug1.Text += "3" + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm");

                                        ////    getHourToOT(ddl_timebefore_edit, 0);
                                        ////    //ddl_timebefore_edit.Visible = false;
                                        ////}


                                        //ot after
                                        if (DateTime_ScanOut > DateTime_ScanIn)
                                        {

                                            d_finish_addhour_after = d_finish.AddMinutes(30);

                                            //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                            //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                            value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                            txtot_after_edit.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                                                                                   //litDebug.Text += value_otafter.ToString() + "|";

                                            //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                            getHourToOT(ddl_timeafter_edit, Convert.ToDouble(txtot_after_edit.Text));
                                        }
                                        else
                                        {
                                            ddl_timeafter_edit.Visible = false;
                                        }



                                        _value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                        _value_diff_scanout = DateTime_ScanOut.Subtract(DateTime_ShiftEnd).TotalMinutes;

                                        //litDebug1.Text += _value_diff_scanout.ToString() + "|";

                                        if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn <= DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                        {

                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                            txtot_holiday_edit.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(txtot_holiday_edit.Text));

                                            if (ddl_timeholiday_edit.SelectedValue == "8")//check ot holiday have 8 hour
                                            {

                                                d_finish_addhour_holiday = DateTime_ShiftEnd.AddMinutes(30);

                                                _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                                txtot_after_edit.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeafter_edit, Convert.ToDouble(txtot_after_edit.Text));


                                            }
                                        }
                                        else if (DateTime_ScanOut >= DateTime_ShiftEnd && DateTime_ScanIn > DateTime_ShiftStart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                        {

                                            //litDebug.Text += DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm") + "|";

                                            //litDebug1.Text += ((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60 )).ToString() + " | ";

                                            //litDebug1.Text += ((Convert.ToDouble(167) / 60)).ToString();

                                            //check hour .xx
                                            string input_decimal_number_be = (Math.Floor((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60))).ToString(); //((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString();
                                            string input_decimal_number_af = ((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 60)).ToString("#.##");//"1.50";

                                            string decimal_places = String.Empty;


                                            //ViewState["_value_timescan_late_add"] = "0";

                                            var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
                                            if (regex.IsMatch(input_decimal_number_af))
                                            {
                                                decimal_places = regex.Match(input_decimal_number_af).Value;
                                                if (int.Parse(decimal_places) > 50)
                                                {

                                                    _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + 1);//Convert.ToDouble((input_decimal_number_be) + 1);
                                                    ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                    d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                    d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                                }
                                                else if (int.Parse(decimal_places) < 50)
                                                {

                                                    _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be)) + .50);//Convert.ToDouble((input_decimal_number_be + .50) );

                                                    ViewState["_value_timescan_late_add"] = _value_timescan_late_add;


                                                    d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                    d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));

                                                }
                                                else
                                                {
                                                    _value_timescan_late_add = (Math.Floor(Convert.ToDouble(input_decimal_number_be))); //Convert.ToDouble((input_decimal_number_be));
                                                    ViewState["_value_timescan_late_add"] = _value_timescan_late_add;

                                                    d_start_addhour = DateTime_ShiftStart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                                    d_finish_addhour = DateTime_ShiftEnd.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));


                                                }

                                            }


                                            ////litDebug.Text += ViewState["_value_timescan_late_add"].ToString();
                                            //////_value_timescan_late = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;

                                            ////////litDebug.Text += _value_timescan_late.ToString() + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");


                                            ////////litDebug.Text += (Math.Floor(_value_timescan_late * 2) / 2).ToString();

                                            //////_value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();


                                            // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                            if (_value_diff_scanout >= 60)
                                            {
                                                value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                            }
                                            else
                                            {

                                                //value = Convert.ToDouble(((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1) );

                                                value = Convert.ToDouble(((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50 - 1) - Convert.ToDouble(ViewState["_value_timescan_late_add"].ToString()));

                                            }

                                            //litDebug1.Text += value.ToString();
                                            txtot_holiday_edit.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                            // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(txtot_holiday_edit.Text));

                                            if (ddl_timeholiday_edit.SelectedValue == "8")//check ot holiday have 8 hour
                                            {
                                                //litDebug.Text = d_finish_addhour_holiday.ToString();
                                                //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                                //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                                //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                                d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                                _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                                txtot_after_edit.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeafter_edit, Convert.ToDouble(txtot_after_edit.Text));

                                            }
                                            else
                                            {
                                                getHourToOT(ddl_timeafter_edit, Convert.ToDouble("0"));
                                            }

                                        }
                                        else //if (DateTime_ScanOut > DateTime_BreakEnd && DateTime_ScanOut < DateTime_ShiftEnd)
                                        {
                                            //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ShiftStart).TotalMinutes.ToString()) / 30) * 0.50);

                                            txtot_holiday_edit.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(txtot_holiday_edit.Text));
                                        }

                                    }
                                    else if (DateTime_ScanIn < d_workstart && DateTime_ScanOut < d_finish)
                                    {
                                        //litDebug1.Text = "have case new";

                                        //total ot
                                        value_total_ot = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 60));
                                        value_total_otresult = (Math.Floor(value_total_ot * 2) / 2);
                                        //total ot

                                        if (value_total_otresult >= 9.5) //set -1 breaktime
                                        {

                                            value_total_ot_diff = value_total_otresult - 1;


                                            value_total_ot_holiday = 8;
                                            value_total_ot_before = value_total_ot_diff - value_total_ot_holiday;

                                            //litDebug.Text = value_total_ot_before.ToString();
                                            getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                            getHourToOT(ddl_timebefore_edit, Convert.ToDouble(value_total_ot_before.ToString()));

                                        }
                                        else if (value_total_otresult == 9)
                                        {
                                            value_total_ot_diff = value_total_otresult - 1;
                                            value_total_ot_holiday = 8;
                                            getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(value_total_ot_holiday.ToString()));
                                        }
                                        else if (value_total_otresult >= 4 && value_total_otresult <= 5)
                                        {
                                            value_total_ot_before = 4;
                                            getHourToOT(ddl_timebefore_edit, Convert.ToDouble(value_total_ot_before.ToString()));
                                        }
                                        else if (value_total_otresult > 5 && value_total_otresult <= 8)
                                        {
                                            value_total_ot_diff = value_total_otresult - 1;

                                            value_total_ot_before = value_total_ot_diff;
                                            getHourToOT(ddl_timebefore_edit, Convert.ToDouble(value_total_ot_before.ToString()));
                                        }
                                    }
                                }
                                else
                                {
                                    lblot_holiday_edit.Visible = false;
                                    lbl_detaildayoff_edit.Visible = false;
                                    ddl_timeholiday_edit.Visible = false;
                                }

                                //sum total ot all type
                                if (Convert.ToDecimal(txtot_before_edit.Text) >= 0)
                                {
                                    tot_actual_otbefore += Convert.ToDecimal(txtot_before_edit.Text);
                                }


                                if (Convert.ToDecimal(txtot_after_edit.Text) >= 0)
                                {
                                    tot_actual_otafter += Convert.ToDecimal(txtot_after_edit.Text);
                                }

                                if (Convert.ToDecimal(txtot_holiday_edit.Text) > 0)
                                {
                                    tot_actual_otholiday += Convert.ToDecimal(txtot_holiday_edit.Text);
                                }



                            }


                            ////if (lbl_holiday_idx_check_edit.Text != "0" || day_offemployee.ToString() == "1")
                            ////{
                            ////    lbl_detailholiday_edit.Visible = true;
                            ////}
                            ////else
                            ////{
                            ////    lbl_detailholiday_edit.Visible = false;
                            ////}

                            //set gv row detail show data
                            if (ddl_timestart_edit.SelectedValue != "" && ddl_timeend_edit.SelectedValue != "")
                            {
                                e.Row.Visible = true;
                            }
                            else
                            {
                                //litDebug.Text += ddl_timestart.SelectedItem.ToString() + "|";
                                e.Row.Visible = false;
                            }
                            //set gv row detail show data

                            //set ot x1, x1.5, x2, x3
                            //set ot before
                            if (ddl_timebefore_edit.SelectedValue != "")
                            {

                                lblot_before_edit.Visible = true;
                                ddl_timebefore_edit.Visible = true;
                                if (lbl_holiday_idx_check_edit.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                {
                                    if (ddl_timeholiday_edit.SelectedValue == "8")
                                    {
                                        lblot_before_edit.Text = "x3";
                                    }
                                    else
                                    {
                                        lblot_before_edit.Text = "x1";
                                    }
                                    //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                    //{
                                    //    lbl_hour_otbefore.Text = "x1";
                                    //}
                                    //else
                                    //{
                                    //    lbl_hour_otbefore.Text = "x2";
                                    //}
                                }
                                else
                                {
                                    lblot_before_edit.Text = "x1.5";
                                }
                            }
                            else
                            {
                                lblot_before_edit.Visible = false;
                                ddl_timebefore_edit.Visible = false;
                            }

                            //set ot after
                            if (ddl_timeafter_edit.SelectedValue != "")
                            {

                                lblot_after_edit.Visible = true;
                                ddl_timeafter_edit.Visible = true;
                                if (lbl_holiday_idx_check_edit.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                {
                                    if (ddl_timeholiday_edit.SelectedValue == "8")
                                    {
                                        lblot_after_edit.Text = "x3";
                                    }
                                    else
                                    {
                                        lblot_after_edit.Text = "x1";
                                    }
                                    //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                    //{
                                    //    lbl_hour_otbefore.Text = "x1";
                                    //}
                                    //else
                                    //{
                                    //    lbl_hour_otbefore.Text = "x2";
                                    //}
                                }
                                else
                                {
                                    lblot_after_edit.Text = "x1.5";
                                }
                            }
                            else
                            {
                                lblot_after_edit.Visible = false;
                                ddl_timeafter_edit.Visible = false;
                            }




                            //set ot holiday
                            if (ddl_timeholiday_edit.SelectedValue != "")
                            {

                                lblot_holiday_edit.Visible = true;
                                ddl_timeholiday_edit.Visible = true;
                                if (ddl_timeafter_edit.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                                {

                                    lblot_holiday_edit.Text = "x1";

                                    //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                    //{
                                    //    lblot_holiday.Text = "x1";
                                    //}
                                    //else
                                    //{
                                    //    lblot_holiday.Text = "x2";
                                    //}
                                }

                            }
                            else
                            {
                                lblot_holiday_edit.Visible = false;
                                ddl_timeholiday_edit.Visible = false;
                            }


                            //litDebug.Text += day_offemployee.ToString() + "|";
                            if (lbl_holiday_idx_check_edit.Text != "0" || day_offemployee.ToString() == "1")
                            {
                                if (lbl_holiday_idx_check_edit.Text != "0")
                                {
                                    lbl_detailholiday_edit.Visible = true;
                                    lbl_detaildayoff_edit.Visible = false;
                                }
                                else if (day_offemployee.ToString() == "1")
                                {
                                    lbl_detaildayoff_edit.Visible = true;
                                    lbl_detailholiday_edit.Visible = false;
                                }
                            }
                            else
                            {
                                lbl_detailholiday_edit.Visible = false;
                                lbl_detaildayoff_edit.Visible = false;
                            }



                        }
                        else
                        {
                            //litDebug1.Text += ddl_timeend.SelectedItem.ToString() + "|";
                            e.Row.Visible = false;
                        }

                    }

                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    Label lit_total_hoursmonth_edit = (Label)e.Row.FindControl("lit_total_hoursmonth_edit");
                    lit_total_hoursmonth_edit.Text = String.Format("{0:N2}", tot_actual_otmonth); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_before_edit = (Label)e.Row.FindControl("lit_total_hoursot_before_edit");
                    lit_total_hoursot_before_edit.Text = String.Format("{0:N2}", tot_actual_otbefore); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_after_edit = (Label)e.Row.FindControl("lit_total_hoursot_after_edit");
                    lit_total_hoursot_after_edit.Text = String.Format("{0:N2}", tot_actual_otafter); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_holiday_edit = (Label)e.Row.FindControl("lit_total_hoursot_holiday_edit");
                    lit_total_hoursot_holiday_edit.Text = String.Format("{0:N2}", tot_actual_otholiday); // Convert.ToString(tot_actual);


                }
                break;
            case "GvDetailImportOTDay":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {


                    Label lbl_ot_day_26_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_26_excel_otday");
                    Label lbl_ot_day_27_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_27_excel_otday");
                    Label lbl_ot_day_28_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_28_excel_otday");
                    Label lbl_ot_day_29_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_29_excel_otday");
                    Label lbl_ot_day_30_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_30_excel_otday");
                    Label lbl_ot_day_31_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_31_excel_otday");
                    Label lbl_ot_day_1_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_1_excel_otday");
                    Label lbl_ot_day_2_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_2_excel_otday");
                    Label lbl_ot_day_3_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_3_excel_otday");
                    Label lbl_ot_day_4_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_4_excel_otday");
                    Label lbl_ot_day_5_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_5_excel_otday");
                    Label lbl_ot_day_6_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_6_excel_otday");
                    Label lbl_ot_day_7_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_7_excel_otday");
                    Label lbl_ot_day_8_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_8_excel_otday");
                    Label lbl_ot_day_9_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_9_excel_otday");
                    Label lbl_ot_day_10_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_10_excel_otday");
                    Label lbl_ot_day_11_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_11_excel_otday");
                    Label lbl_ot_day_12_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_12_excel_otday");
                    Label lbl_ot_day_13_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_13_excel_otday");
                    Label lbl_ot_day_14_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_14_excel_otday");
                    Label lbl_ot_day_15_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_15_excel_otday");
                    Label lbl_ot_day_16_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_16_excel_otday");
                    Label lbl_ot_day_17_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_17_excel_otday");
                    Label lbl_ot_day_18_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_18_excel_otday");
                    Label lbl_ot_day_19_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_19_excel_otday");
                    Label lbl_ot_day_20_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_20_excel_otday");
                    Label lbl_ot_day_21_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_21_excel_otday");
                    Label lbl_ot_day_22_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_22_excel_otday");
                    Label lbl_ot_day_23_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_23_excel_otday");
                    Label lbl_ot_day_24_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_24_excel_otday");
                    Label lbl_ot_day_25_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_25_excel_otday");

                    if (lbl_ot_day_26_excel_otday.Text != "" && lbl_ot_day_26_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport26 += Convert.ToDecimal(lbl_ot_day_26_excel_otday.Text);
                    }

                    if (lbl_ot_day_27_excel_otday.Text != "" && lbl_ot_day_27_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport27 += Convert.ToDecimal(lbl_ot_day_27_excel_otday.Text);
                    }

                    if (lbl_ot_day_28_excel_otday.Text != "" && lbl_ot_day_28_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport28 += Convert.ToDecimal(lbl_ot_day_28_excel_otday.Text);
                    }

                    if (lbl_ot_day_29_excel_otday.Text != "" && lbl_ot_day_29_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport29 += Convert.ToDecimal(lbl_ot_day_29_excel_otday.Text);
                    }

                    if (lbl_ot_day_30_excel_otday.Text != "" && lbl_ot_day_30_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport30 += Convert.ToDecimal(lbl_ot_day_30_excel_otday.Text);
                    }

                    if (lbl_ot_day_31_excel_otday.Text != "" && lbl_ot_day_31_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport31 += Convert.ToDecimal(lbl_ot_day_31_excel_otday.Text);
                    }

                    if (lbl_ot_day_1_excel_otday.Text != "" && lbl_ot_day_1_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport1 += Convert.ToDecimal(lbl_ot_day_1_excel_otday.Text);
                    }

                    if (lbl_ot_day_2_excel_otday.Text != "" && lbl_ot_day_2_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport2 += Convert.ToDecimal(lbl_ot_day_2_excel_otday.Text);
                    }

                    if (lbl_ot_day_3_excel_otday.Text != "" && lbl_ot_day_3_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport3 += Convert.ToDecimal(lbl_ot_day_3_excel_otday.Text);
                    }

                    if (lbl_ot_day_4_excel_otday.Text != "" && lbl_ot_day_4_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport4 += Convert.ToDecimal(lbl_ot_day_4_excel_otday.Text);
                    }

                    if (lbl_ot_day_5_excel_otday.Text != "" && lbl_ot_day_5_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport5 += Convert.ToDecimal(lbl_ot_day_5_excel_otday.Text);
                    }

                    if (lbl_ot_day_6_excel_otday.Text != "" && lbl_ot_day_6_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport6 += Convert.ToDecimal(lbl_ot_day_6_excel_otday.Text);
                    }

                    if (lbl_ot_day_7_excel_otday.Text != "" && lbl_ot_day_7_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport7 += Convert.ToDecimal(lbl_ot_day_7_excel_otday.Text);
                    }

                    if (lbl_ot_day_8_excel_otday.Text != "" && lbl_ot_day_8_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport8 += Convert.ToDecimal(lbl_ot_day_8_excel_otday.Text);
                    }

                    if (lbl_ot_day_9_excel_otday.Text != "" && lbl_ot_day_9_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport9 += Convert.ToDecimal(lbl_ot_day_9_excel_otday.Text);
                    }

                    if (lbl_ot_day_10_excel_otday.Text != "" && lbl_ot_day_10_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport10 += Convert.ToDecimal(lbl_ot_day_10_excel_otday.Text);
                    }

                    if (lbl_ot_day_11_excel_otday.Text != "" && lbl_ot_day_11_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport11 += Convert.ToDecimal(lbl_ot_day_11_excel_otday.Text);
                    }

                    if (lbl_ot_day_12_excel_otday.Text != "" && lbl_ot_day_12_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport12 += Convert.ToDecimal(lbl_ot_day_12_excel_otday.Text);
                    }

                    if (lbl_ot_day_13_excel_otday.Text != "" && lbl_ot_day_13_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport13 += Convert.ToDecimal(lbl_ot_day_13_excel_otday.Text);
                    }

                    if (lbl_ot_day_14_excel_otday.Text != "" && lbl_ot_day_14_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport14 += Convert.ToDecimal(lbl_ot_day_14_excel_otday.Text);
                    }

                    if (lbl_ot_day_15_excel_otday.Text != "" && lbl_ot_day_15_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport15 += Convert.ToDecimal(lbl_ot_day_15_excel_otday.Text);
                    }

                    if (lbl_ot_day_16_excel_otday.Text != "" && lbl_ot_day_16_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport16 += Convert.ToDecimal(lbl_ot_day_16_excel_otday.Text);
                    }

                    if (lbl_ot_day_17_excel_otday.Text != "" && lbl_ot_day_17_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport17 += Convert.ToDecimal(lbl_ot_day_17_excel_otday.Text);
                    }

                    if (lbl_ot_day_18_excel_otday.Text != "" && lbl_ot_day_18_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport18 += Convert.ToDecimal(lbl_ot_day_18_excel_otday.Text);
                    }

                    if (lbl_ot_day_19_excel_otday.Text != "" && lbl_ot_day_19_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport19 += Convert.ToDecimal(lbl_ot_day_19_excel_otday.Text);
                    }

                    if (lbl_ot_day_20_excel_otday.Text != "" && lbl_ot_day_20_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport20 += Convert.ToDecimal(lbl_ot_day_20_excel_otday.Text);
                    }

                    if (lbl_ot_day_21_excel_otday.Text != "" && lbl_ot_day_21_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport21 += Convert.ToDecimal(lbl_ot_day_21_excel_otday.Text);
                    }

                    if (lbl_ot_day_22_excel_otday.Text != "" && lbl_ot_day_22_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport22 += Convert.ToDecimal(lbl_ot_day_22_excel_otday.Text);
                    }

                    if (lbl_ot_day_23_excel_otday.Text != "" && lbl_ot_day_23_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport23 += Convert.ToDecimal(lbl_ot_day_23_excel_otday.Text);
                    }

                    if (lbl_ot_day_24_excel_otday.Text != "" && lbl_ot_day_24_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport24 += Convert.ToDecimal(lbl_ot_day_24_excel_otday.Text);
                    }

                    if (lbl_ot_day_25_excel_otday.Text != "" && lbl_ot_day_25_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport25 += Convert.ToDecimal(lbl_ot_day_25_excel_otday.Text);
                    }


                    //litDebug1.Text = tot_actual_otmonth.ToString();

                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    Label lit_total_ot_day_26_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_26_excel_otday");
                    Label lit_total_ot_day_27_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_27_excel_otday");
                    Label lit_total_ot_day_28_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_28_excel_otday");
                    Label lit_total_ot_day_29_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_29_excel_otday");
                    Label lit_total_ot_day_30_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_30_excel_otday");
                    Label lit_total_ot_day_31_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_31_excel_otday");
                    Label lit_total_ot_day_1_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_1_excel_otday");
                    Label lit_total_ot_day_2_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_2_excel_otday");
                    Label lit_total_ot_day_3_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_3_excel_otday");
                    Label lit_total_ot_day_4_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_4_excel_otday");
                    Label lit_total_ot_day_5_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_5_excel_otday");
                    Label lit_total_ot_day_6_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_6_excel_otday");
                    Label lit_total_ot_day_7_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_7_excel_otday");
                    Label lit_total_ot_day_8_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_8_excel_otday");
                    Label lit_total_ot_day_9_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_9_excel_otday");
                    Label lit_total_ot_day_10_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_10_excel_otday");
                    Label lit_total_ot_day_11_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_11_excel_otday");
                    Label lit_total_ot_day_12_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_12_excel_otday");
                    Label lit_total_ot_day_13_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_13_excel_otday");
                    Label lit_total_ot_day_14_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_14_excel_otday");
                    Label lit_total_ot_day_15_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_15_excel_otday");
                    Label lit_total_ot_day_16_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_16_excel_otday");
                    Label lit_total_ot_day_17_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_17_excel_otday");
                    Label lit_total_ot_day_18_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_18_excel_otday");
                    Label lit_total_ot_day_19_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_19_excel_otday");
                    Label lit_total_ot_day_20_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_20_excel_otday");
                    Label lit_total_ot_day_21_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_21_excel_otday");
                    Label lit_total_ot_day_22_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_22_excel_otday");
                    Label lit_total_ot_day_23_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_23_excel_otday");
                    Label lit_total_ot_day_24_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_24_excel_otday");
                    Label lit_total_ot_day_25_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_25_excel_otday");

                    lit_total_ot_day_26_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport26); // Convert.ToString(tot_actual);
                    lit_total_ot_day_27_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport27); // Convert.ToString(tot_actual);
                    lit_total_ot_day_28_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport28); // Convert.ToString(tot_actual);
                    lit_total_ot_day_29_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport29); // Convert.ToString(tot_actual);
                    lit_total_ot_day_30_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport30); // Convert.ToString(tot_actual);
                    lit_total_ot_day_31_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport31); // Convert.ToString(tot_actual);
                    lit_total_ot_day_1_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport1); // Convert.ToString(tot_actual);
                    lit_total_ot_day_2_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport2); // Convert.ToString(tot_actual);
                    lit_total_ot_day_3_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport3); // Convert.ToString(tot_actual);
                    lit_total_ot_day_4_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport4); // Convert.ToString(tot_actual);
                    lit_total_ot_day_5_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport5); // Convert.ToString(tot_actual);
                    lit_total_ot_day_6_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport6); // Convert.ToString(tot_actual);
                    lit_total_ot_day_7_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport7); // Convert.ToString(tot_actual);
                    lit_total_ot_day_8_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport8); // Convert.ToString(tot_actual);
                    lit_total_ot_day_9_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport9); // Convert.ToString(tot_actual);
                    lit_total_ot_day_10_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport10); // Convert.ToString(tot_actual);
                    lit_total_ot_day_11_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport11); // Convert.ToString(tot_actual);
                    lit_total_ot_day_12_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport12); // Convert.ToString(tot_actual);
                    lit_total_ot_day_13_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport13); // Convert.ToString(tot_actual);
                    lit_total_ot_day_14_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport14); // Convert.ToString(tot_actual);
                    lit_total_ot_day_15_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport15); // Convert.ToString(tot_actual);
                    lit_total_ot_day_16_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport16); // Convert.ToString(tot_actual);
                    lit_total_ot_day_17_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport17); // Convert.ToString(tot_actual);
                    lit_total_ot_day_18_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport18); // Convert.ToString(tot_actual);
                    lit_total_ot_day_19_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport19); // Convert.ToString(tot_actual);
                    lit_total_ot_day_20_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport20); // Convert.ToString(tot_actual);
                    lit_total_ot_day_21_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport21); // Convert.ToString(tot_actual);
                    lit_total_ot_day_22_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport22); // Convert.ToString(tot_actual);
                    lit_total_ot_day_23_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport23); // Convert.ToString(tot_actual);
                    lit_total_ot_day_24_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport24); // Convert.ToString(tot_actual);
                    lit_total_ot_day_25_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport25); // Convert.ToString(tot_actual);

                }
                break;
            case "GvViewDetailEmployeeImportDay":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {


                    Label lbl_ot_day_26_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_26_excel_otday");
                    Label lbl_ot_day_27_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_27_excel_otday");
                    Label lbl_ot_day_28_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_28_excel_otday");
                    Label lbl_ot_day_29_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_29_excel_otday");
                    Label lbl_ot_day_30_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_30_excel_otday");
                    Label lbl_ot_day_31_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_31_excel_otday");
                    Label lbl_ot_day_1_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_1_excel_otday");
                    Label lbl_ot_day_2_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_2_excel_otday");
                    Label lbl_ot_day_3_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_3_excel_otday");
                    Label lbl_ot_day_4_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_4_excel_otday");
                    Label lbl_ot_day_5_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_5_excel_otday");
                    Label lbl_ot_day_6_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_6_excel_otday");
                    Label lbl_ot_day_7_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_7_excel_otday");
                    Label lbl_ot_day_8_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_8_excel_otday");
                    Label lbl_ot_day_9_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_9_excel_otday");
                    Label lbl_ot_day_10_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_10_excel_otday");
                    Label lbl_ot_day_11_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_11_excel_otday");
                    Label lbl_ot_day_12_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_12_excel_otday");
                    Label lbl_ot_day_13_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_13_excel_otday");
                    Label lbl_ot_day_14_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_14_excel_otday");
                    Label lbl_ot_day_15_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_15_excel_otday");
                    Label lbl_ot_day_16_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_16_excel_otday");
                    Label lbl_ot_day_17_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_17_excel_otday");
                    Label lbl_ot_day_18_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_18_excel_otday");
                    Label lbl_ot_day_19_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_19_excel_otday");
                    Label lbl_ot_day_20_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_20_excel_otday");
                    Label lbl_ot_day_21_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_21_excel_otday");
                    Label lbl_ot_day_22_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_22_excel_otday");
                    Label lbl_ot_day_23_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_23_excel_otday");
                    Label lbl_ot_day_24_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_24_excel_otday");
                    Label lbl_ot_day_25_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_25_excel_otday");
                    Label lbl_totalhours_ = (Label)e.Row.FindControl("lbl_totalhours_");
                    Label lbl_totalhours_sumtotal = (Label)e.Row.FindControl("lbl_totalhours_sumtotal");

                    if (lbl_ot_day_26_excel_otday.Text != "" && lbl_ot_day_26_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport26 += Convert.ToDecimal(lbl_ot_day_26_excel_otday.Text);
                        tot_actual_26 = Convert.ToDecimal(lbl_ot_day_26_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_26 = 0;
                    }

                    if (lbl_ot_day_27_excel_otday.Text != "" && lbl_ot_day_27_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport27 += Convert.ToDecimal(lbl_ot_day_27_excel_otday.Text);
                        tot_actual_27 = Convert.ToDecimal(lbl_ot_day_27_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_27 = 0;
                    }

                    if (lbl_ot_day_28_excel_otday.Text != "" && lbl_ot_day_28_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport28 += Convert.ToDecimal(lbl_ot_day_28_excel_otday.Text);
                        tot_actual_28 = Convert.ToDecimal(lbl_ot_day_28_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_28 = 0;
                    }

                    if (lbl_ot_day_29_excel_otday.Text != "" && lbl_ot_day_29_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport29 += Convert.ToDecimal(lbl_ot_day_29_excel_otday.Text);
                        tot_actual_29 = Convert.ToDecimal(lbl_ot_day_29_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_29 = 0;
                    }

                    if (lbl_ot_day_30_excel_otday.Text != "" && lbl_ot_day_30_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport30 += Convert.ToDecimal(lbl_ot_day_30_excel_otday.Text);
                        tot_actual_30 = Convert.ToDecimal(lbl_ot_day_30_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_30 = 0;
                    }

                    if (lbl_ot_day_31_excel_otday.Text != "" && lbl_ot_day_31_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport31 += Convert.ToDecimal(lbl_ot_day_31_excel_otday.Text);
                        tot_actual_31 = Convert.ToDecimal(lbl_ot_day_31_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_31 = 0;
                    }

                    if (lbl_ot_day_1_excel_otday.Text != "" && lbl_ot_day_1_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport1 += Convert.ToDecimal(lbl_ot_day_1_excel_otday.Text);
                        tot_actual_1 = Convert.ToDecimal(lbl_ot_day_1_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_1 = 0;
                    }

                    if (lbl_ot_day_2_excel_otday.Text != "" && lbl_ot_day_2_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport2 += Convert.ToDecimal(lbl_ot_day_2_excel_otday.Text);
                        tot_actual_2 = Convert.ToDecimal(lbl_ot_day_2_excel_otday.Text);

                    }
                    else
                    {
                        tot_actual_2 = 0;
                    }

                    if (lbl_ot_day_3_excel_otday.Text != "" && lbl_ot_day_3_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport3 += Convert.ToDecimal(lbl_ot_day_3_excel_otday.Text);
                        tot_actual_3 = Convert.ToDecimal(lbl_ot_day_3_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_3 = 0;
                    }

                    if (lbl_ot_day_4_excel_otday.Text != "" && lbl_ot_day_4_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport4 += Convert.ToDecimal(lbl_ot_day_4_excel_otday.Text);
                        tot_actual_4 = Convert.ToDecimal(lbl_ot_day_4_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_4 = 0;
                    }

                    if (lbl_ot_day_5_excel_otday.Text != "" && lbl_ot_day_5_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport5 += Convert.ToDecimal(lbl_ot_day_5_excel_otday.Text);
                        tot_actual_5 = Convert.ToDecimal(lbl_ot_day_5_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_5 = 0;
                    }

                    if (lbl_ot_day_6_excel_otday.Text != "" && lbl_ot_day_6_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport6 += Convert.ToDecimal(lbl_ot_day_6_excel_otday.Text);
                        tot_actual_6 = Convert.ToDecimal(lbl_ot_day_6_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_6 = 0;
                    }

                    if (lbl_ot_day_7_excel_otday.Text != "" && lbl_ot_day_7_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport7 += Convert.ToDecimal(lbl_ot_day_7_excel_otday.Text);
                        tot_actual_7 = Convert.ToDecimal(lbl_ot_day_7_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_7 = 0;
                    }

                    if (lbl_ot_day_8_excel_otday.Text != "" && lbl_ot_day_8_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport8 += Convert.ToDecimal(lbl_ot_day_8_excel_otday.Text);
                        tot_actual_8 = Convert.ToDecimal(lbl_ot_day_8_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_8 = 0;
                    }

                    if (lbl_ot_day_9_excel_otday.Text != "" && lbl_ot_day_9_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport9 += Convert.ToDecimal(lbl_ot_day_9_excel_otday.Text);
                        tot_actual_9 = Convert.ToDecimal(lbl_ot_day_9_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_9 = 0;
                    }

                    if (lbl_ot_day_10_excel_otday.Text != "" && lbl_ot_day_10_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport10 += Convert.ToDecimal(lbl_ot_day_10_excel_otday.Text);
                        tot_actual_10 = Convert.ToDecimal(lbl_ot_day_10_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_10 = 0;
                    }

                    if (lbl_ot_day_11_excel_otday.Text != "" && lbl_ot_day_11_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport11 += Convert.ToDecimal(lbl_ot_day_11_excel_otday.Text);
                        tot_actual_11 = Convert.ToDecimal(lbl_ot_day_11_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_11 = 0;
                    }

                    if (lbl_ot_day_12_excel_otday.Text != "" && lbl_ot_day_12_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport12 += Convert.ToDecimal(lbl_ot_day_12_excel_otday.Text);
                        tot_actual_12 = Convert.ToDecimal(lbl_ot_day_12_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_12 = 0;
                    }


                    if (lbl_ot_day_13_excel_otday.Text != "" && lbl_ot_day_13_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport13 += Convert.ToDecimal(lbl_ot_day_13_excel_otday.Text);
                        tot_actual_13 = Convert.ToDecimal(lbl_ot_day_13_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_13 = 0;
                    }

                    if (lbl_ot_day_14_excel_otday.Text != "" && lbl_ot_day_14_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport14 += Convert.ToDecimal(lbl_ot_day_14_excel_otday.Text);
                        tot_actual_14 = Convert.ToDecimal(lbl_ot_day_14_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_14 = 0;
                    }

                    if (lbl_ot_day_15_excel_otday.Text != "" && lbl_ot_day_15_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport15 += Convert.ToDecimal(lbl_ot_day_15_excel_otday.Text);
                        tot_actual_15 = Convert.ToDecimal(lbl_ot_day_15_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_15 = 0;
                    }

                    if (lbl_ot_day_16_excel_otday.Text != "" && lbl_ot_day_16_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport16 += Convert.ToDecimal(lbl_ot_day_16_excel_otday.Text);
                        tot_actual_16 = Convert.ToDecimal(lbl_ot_day_16_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_16 = 0;
                    }

                    if (lbl_ot_day_17_excel_otday.Text != "" && lbl_ot_day_17_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport17 += Convert.ToDecimal(lbl_ot_day_17_excel_otday.Text);
                        tot_actual_17 = Convert.ToDecimal(lbl_ot_day_17_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_17 = 0;
                    }

                    if (lbl_ot_day_18_excel_otday.Text != "" && lbl_ot_day_18_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport18 += Convert.ToDecimal(lbl_ot_day_18_excel_otday.Text);
                        tot_actual_18 = Convert.ToDecimal(lbl_ot_day_18_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_18 = 0;
                    }

                    if (lbl_ot_day_19_excel_otday.Text != "" && lbl_ot_day_19_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport19 += Convert.ToDecimal(lbl_ot_day_19_excel_otday.Text);
                        tot_actual_19 = Convert.ToDecimal(lbl_ot_day_19_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_19 = 0;
                    }

                    if (lbl_ot_day_20_excel_otday.Text != "" && lbl_ot_day_20_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport20 += Convert.ToDecimal(lbl_ot_day_20_excel_otday.Text);
                        tot_actual_20 = Convert.ToDecimal(lbl_ot_day_20_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_20 = 0;
                    }

                    if (lbl_ot_day_21_excel_otday.Text != "" && lbl_ot_day_21_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport21 += Convert.ToDecimal(lbl_ot_day_21_excel_otday.Text);
                        tot_actual_21 = Convert.ToDecimal(lbl_ot_day_21_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_21 = 0;
                    }

                    if (lbl_ot_day_22_excel_otday.Text != "" && lbl_ot_day_22_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport22 += Convert.ToDecimal(lbl_ot_day_22_excel_otday.Text);
                        tot_actual_22 = Convert.ToDecimal(lbl_ot_day_22_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_22 = 0;
                    }

                    if (lbl_ot_day_23_excel_otday.Text != "" && lbl_ot_day_23_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport23 += Convert.ToDecimal(lbl_ot_day_23_excel_otday.Text);
                        tot_actual_23 = Convert.ToDecimal(lbl_ot_day_23_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_23 = 0;
                    }

                    if (lbl_ot_day_24_excel_otday.Text != "" && lbl_ot_day_24_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport24 += Convert.ToDecimal(lbl_ot_day_24_excel_otday.Text);
                        tot_actual_24 = Convert.ToDecimal(lbl_ot_day_24_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_24 = 0;
                    }

                    if (lbl_ot_day_25_excel_otday.Text != "" && lbl_ot_day_25_excel_otday.Text != "-")
                    {

                        tot_actual_otdayimport25 += Convert.ToDecimal(lbl_ot_day_25_excel_otday.Text);
                        tot_actual_25 = Convert.ToDecimal(lbl_ot_day_25_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_25 = 0;
                    }

                    lbl_totalhours_.Text = (tot_actual_26 + tot_actual_27 + tot_actual_28 + tot_actual_29 + tot_actual_30 + tot_actual_31 + tot_actual_1 + tot_actual_2 + tot_actual_3
                        + tot_actual_4 + tot_actual_5 + tot_actual_6 + tot_actual_7 + tot_actual_8 + tot_actual_9 + tot_actual_10 + tot_actual_11 + tot_actual_12 + tot_actual_13 + tot_actual_14 + tot_actual_15
                        + tot_actual_16 + tot_actual_17 + tot_actual_18 + tot_actual_19 + tot_actual_20 + tot_actual_21 + tot_actual_22 + tot_actual_23 + tot_actual_24 + tot_actual_25).ToString();

                    tot_actual_sumot += (tot_actual_26 + tot_actual_27 + tot_actual_28 + tot_actual_29 + tot_actual_30 + tot_actual_31 + tot_actual_1 + tot_actual_2 + tot_actual_3
                        + tot_actual_4 + tot_actual_5 + tot_actual_6 + tot_actual_7 + tot_actual_8 + tot_actual_9 + tot_actual_10 + tot_actual_11 + tot_actual_12 + tot_actual_13 + tot_actual_14 + tot_actual_15
                        + tot_actual_16 + tot_actual_17 + tot_actual_18 + tot_actual_19 + tot_actual_20 + tot_actual_21 + tot_actual_22 + tot_actual_23 + tot_actual_24 + tot_actual_25);
                    //decimal tot_actual_sumot = 0;
                    //litDebug1.Text = tot_actual_otmonth.ToString(); 

                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    Label lit_total_ot_day_26_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_26_excel_otday");
                    Label lit_total_ot_day_27_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_27_excel_otday");
                    Label lit_total_ot_day_28_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_28_excel_otday");
                    Label lit_total_ot_day_29_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_29_excel_otday");
                    Label lit_total_ot_day_30_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_30_excel_otday");
                    Label lit_total_ot_day_31_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_31_excel_otday");
                    Label lit_total_ot_day_1_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_1_excel_otday");
                    Label lit_total_ot_day_2_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_2_excel_otday");
                    Label lit_total_ot_day_3_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_3_excel_otday");
                    Label lit_total_ot_day_4_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_4_excel_otday");
                    Label lit_total_ot_day_5_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_5_excel_otday");
                    Label lit_total_ot_day_6_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_6_excel_otday");
                    Label lit_total_ot_day_7_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_7_excel_otday");
                    Label lit_total_ot_day_8_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_8_excel_otday");
                    Label lit_total_ot_day_9_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_9_excel_otday");
                    Label lit_total_ot_day_10_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_10_excel_otday");
                    Label lit_total_ot_day_11_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_11_excel_otday");
                    Label lit_total_ot_day_12_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_12_excel_otday");
                    Label lit_total_ot_day_13_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_13_excel_otday");
                    Label lit_total_ot_day_14_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_14_excel_otday");
                    Label lit_total_ot_day_15_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_15_excel_otday");
                    Label lit_total_ot_day_16_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_16_excel_otday");
                    Label lit_total_ot_day_17_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_17_excel_otday");
                    Label lit_total_ot_day_18_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_18_excel_otday");
                    Label lit_total_ot_day_19_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_19_excel_otday");
                    Label lit_total_ot_day_20_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_20_excel_otday");
                    Label lit_total_ot_day_21_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_21_excel_otday");
                    Label lit_total_ot_day_22_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_22_excel_otday");
                    Label lit_total_ot_day_23_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_23_excel_otday");
                    Label lit_total_ot_day_24_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_24_excel_otday");
                    Label lit_total_ot_day_25_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_25_excel_otday");
                    Label lit_totalhours_sumtotal = (Label)e.Row.FindControl("lit_totalhours_sumtotal");

                    lit_total_ot_day_26_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport26); // Convert.ToString(tot_actual);
                    lit_total_ot_day_27_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport27); // Convert.ToString(tot_actual);
                    lit_total_ot_day_28_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport28); // Convert.ToString(tot_actual);
                    lit_total_ot_day_29_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport29); // Convert.ToString(tot_actual);
                    lit_total_ot_day_30_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport30); // Convert.ToString(tot_actual);
                    lit_total_ot_day_31_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport31); // Convert.ToString(tot_actual);
                    lit_total_ot_day_1_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport1); // Convert.ToString(tot_actual);
                    lit_total_ot_day_2_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport2); // Convert.ToString(tot_actual);
                    lit_total_ot_day_3_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport3); // Convert.ToString(tot_actual);
                    lit_total_ot_day_4_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport4); // Convert.ToString(tot_actual);
                    lit_total_ot_day_5_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport5); // Convert.ToString(tot_actual);
                    lit_total_ot_day_6_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport6); // Convert.ToString(tot_actual);
                    lit_total_ot_day_7_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport7); // Convert.ToString(tot_actual);
                    lit_total_ot_day_8_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport8); // Convert.ToString(tot_actual);
                    lit_total_ot_day_9_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport9); // Convert.ToString(tot_actual);
                    lit_total_ot_day_10_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport10); // Convert.ToString(tot_actual);
                    lit_total_ot_day_11_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport11); // Convert.ToString(tot_actual);
                    lit_total_ot_day_12_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport12); // Convert.ToString(tot_actual);
                    lit_total_ot_day_13_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport13); // Convert.ToString(tot_actual);
                    lit_total_ot_day_14_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport14); // Convert.ToString(tot_actual);
                    lit_total_ot_day_15_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport15); // Convert.ToString(tot_actual);
                    lit_total_ot_day_16_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport16); // Convert.ToString(tot_actual);
                    lit_total_ot_day_17_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport17); // Convert.ToString(tot_actual);
                    lit_total_ot_day_18_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport18); // Convert.ToString(tot_actual);
                    lit_total_ot_day_19_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport19); // Convert.ToString(tot_actual);
                    lit_total_ot_day_20_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport20); // Convert.ToString(tot_actual);
                    lit_total_ot_day_21_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport21); // Convert.ToString(tot_actual);
                    lit_total_ot_day_22_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport22); // Convert.ToString(tot_actual);
                    lit_total_ot_day_23_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport23); // Convert.ToString(tot_actual);
                    lit_total_ot_day_24_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport24); // Convert.ToString(tot_actual);
                    lit_total_ot_day_25_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport25); // Convert.ToString(tot_actual);
                    lit_totalhours_sumtotal.Text = String.Format("{0:N2}", tot_actual_sumot); // Convert.ToString(tot_actual);

                }
                break;
            case "GvReportEmployeeImportDay":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {


                    Label lbl_ot_day_26_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_26_excel_otday");
                    Label lbl_ot_day_27_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_27_excel_otday");
                    Label lbl_ot_day_28_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_28_excel_otday");
                    Label lbl_ot_day_29_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_29_excel_otday");
                    Label lbl_ot_day_30_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_30_excel_otday");
                    Label lbl_ot_day_31_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_31_excel_otday");
                    Label lbl_ot_day_1_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_1_excel_otday");
                    Label lbl_ot_day_2_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_2_excel_otday");
                    Label lbl_ot_day_3_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_3_excel_otday");
                    Label lbl_ot_day_4_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_4_excel_otday");
                    Label lbl_ot_day_5_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_5_excel_otday");
                    Label lbl_ot_day_6_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_6_excel_otday");
                    Label lbl_ot_day_7_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_7_excel_otday");
                    Label lbl_ot_day_8_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_8_excel_otday");
                    Label lbl_ot_day_9_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_9_excel_otday");
                    Label lbl_ot_day_10_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_10_excel_otday");
                    Label lbl_ot_day_11_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_11_excel_otday");
                    Label lbl_ot_day_12_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_12_excel_otday");
                    Label lbl_ot_day_13_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_13_excel_otday");
                    Label lbl_ot_day_14_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_14_excel_otday");
                    Label lbl_ot_day_15_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_15_excel_otday");
                    Label lbl_ot_day_16_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_16_excel_otday");
                    Label lbl_ot_day_17_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_17_excel_otday");
                    Label lbl_ot_day_18_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_18_excel_otday");
                    Label lbl_ot_day_19_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_19_excel_otday");
                    Label lbl_ot_day_20_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_20_excel_otday");
                    Label lbl_ot_day_21_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_21_excel_otday");
                    Label lbl_ot_day_22_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_22_excel_otday");
                    Label lbl_ot_day_23_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_23_excel_otday");
                    Label lbl_ot_day_24_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_24_excel_otday");
                    Label lbl_ot_day_25_excel_otday = (Label)e.Row.FindControl("lbl_ot_day_25_excel_otday");
                    Label lbl_totalhours_ = (Label)e.Row.FindControl("lbl_totalhours_");
                    Label lbl_totalhours_sumtotal = (Label)e.Row.FindControl("lbl_totalhours_sumtotal");


                    if (lbl_ot_day_26_excel_otday.Text != "" && lbl_ot_day_26_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport26 += Convert.ToDecimal(lbl_ot_day_26_excel_otday.Text);
                        tot_actual_26 = Convert.ToDecimal(lbl_ot_day_26_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_26 = 0;
                    }

                    if (lbl_ot_day_27_excel_otday.Text != "" && lbl_ot_day_27_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport27 += Convert.ToDecimal(lbl_ot_day_27_excel_otday.Text);
                        tot_actual_27 = Convert.ToDecimal(lbl_ot_day_27_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_27 = 0;
                    }

                    if (lbl_ot_day_28_excel_otday.Text != "" && lbl_ot_day_28_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport28 += Convert.ToDecimal(lbl_ot_day_28_excel_otday.Text);
                        tot_actual_28 = Convert.ToDecimal(lbl_ot_day_28_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_28 = 0;
                    }

                    if (lbl_ot_day_29_excel_otday.Text != "" && lbl_ot_day_29_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport29 += Convert.ToDecimal(lbl_ot_day_29_excel_otday.Text);
                        tot_actual_29 = Convert.ToDecimal(lbl_ot_day_29_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_29 = 0;
                    }

                    if (lbl_ot_day_30_excel_otday.Text != "" && lbl_ot_day_30_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport30 += Convert.ToDecimal(lbl_ot_day_30_excel_otday.Text);
                        tot_actual_30 = Convert.ToDecimal(lbl_ot_day_30_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_30 = 0;
                    }

                    if (lbl_ot_day_31_excel_otday.Text != "" && lbl_ot_day_31_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport31 += Convert.ToDecimal(lbl_ot_day_31_excel_otday.Text);
                        tot_actual_31 = Convert.ToDecimal(lbl_ot_day_31_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_31 = 0;
                    }

                    if (lbl_ot_day_1_excel_otday.Text != "" && lbl_ot_day_1_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport1 += Convert.ToDecimal(lbl_ot_day_1_excel_otday.Text);
                        tot_actual_1 = Convert.ToDecimal(lbl_ot_day_1_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_1 = 0;
                    }

                    if (lbl_ot_day_2_excel_otday.Text != "" && lbl_ot_day_2_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport2 += Convert.ToDecimal(lbl_ot_day_2_excel_otday.Text);
                        tot_actual_2 = Convert.ToDecimal(lbl_ot_day_2_excel_otday.Text);

                    }
                    else
                    {
                        tot_actual_2 = 0;
                    }

                    if (lbl_ot_day_3_excel_otday.Text != "" && lbl_ot_day_3_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport3 += Convert.ToDecimal(lbl_ot_day_3_excel_otday.Text);
                        tot_actual_3 = Convert.ToDecimal(lbl_ot_day_3_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_3 = 0;
                    }

                    if (lbl_ot_day_4_excel_otday.Text != "" && lbl_ot_day_4_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport4 += Convert.ToDecimal(lbl_ot_day_4_excel_otday.Text);
                        tot_actual_4 = Convert.ToDecimal(lbl_ot_day_4_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_4 = 0;
                    }

                    if (lbl_ot_day_5_excel_otday.Text != "" && lbl_ot_day_5_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport5 += Convert.ToDecimal(lbl_ot_day_5_excel_otday.Text);
                        tot_actual_5 = Convert.ToDecimal(lbl_ot_day_5_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_5 = 0;
                    }

                    if (lbl_ot_day_6_excel_otday.Text != "" && lbl_ot_day_6_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport6 += Convert.ToDecimal(lbl_ot_day_6_excel_otday.Text);
                        tot_actual_6 = Convert.ToDecimal(lbl_ot_day_6_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_6 = 0;
                    }

                    if (lbl_ot_day_7_excel_otday.Text != "" && lbl_ot_day_7_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport7 += Convert.ToDecimal(lbl_ot_day_7_excel_otday.Text);
                        tot_actual_7 = Convert.ToDecimal(lbl_ot_day_7_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_7 = 0;
                    }

                    if (lbl_ot_day_8_excel_otday.Text != "" && lbl_ot_day_8_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport8 += Convert.ToDecimal(lbl_ot_day_8_excel_otday.Text);
                        tot_actual_8 = Convert.ToDecimal(lbl_ot_day_8_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_8 = 0;
                    }

                    if (lbl_ot_day_9_excel_otday.Text != "" && lbl_ot_day_9_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport9 += Convert.ToDecimal(lbl_ot_day_9_excel_otday.Text);
                        tot_actual_9 = Convert.ToDecimal(lbl_ot_day_9_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_9 = 0;
                    }

                    if (lbl_ot_day_10_excel_otday.Text != "" && lbl_ot_day_10_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport10 += Convert.ToDecimal(lbl_ot_day_10_excel_otday.Text);
                        tot_actual_10 = Convert.ToDecimal(lbl_ot_day_10_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_10 = 0;
                    }

                    if (lbl_ot_day_11_excel_otday.Text != "" && lbl_ot_day_11_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport11 += Convert.ToDecimal(lbl_ot_day_11_excel_otday.Text);
                        tot_actual_11 = Convert.ToDecimal(lbl_ot_day_11_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_11 = 0;
                    }

                    if (lbl_ot_day_12_excel_otday.Text != "" && lbl_ot_day_12_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport12 += Convert.ToDecimal(lbl_ot_day_12_excel_otday.Text);
                        tot_actual_12 = Convert.ToDecimal(lbl_ot_day_12_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_12 = 0;
                    }


                    if (lbl_ot_day_13_excel_otday.Text != "" && lbl_ot_day_13_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport13 += Convert.ToDecimal(lbl_ot_day_13_excel_otday.Text);
                        tot_actual_13 = Convert.ToDecimal(lbl_ot_day_13_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_13 = 0;
                    }

                    if (lbl_ot_day_14_excel_otday.Text != "" && lbl_ot_day_14_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport14 += Convert.ToDecimal(lbl_ot_day_14_excel_otday.Text);
                        tot_actual_14 = Convert.ToDecimal(lbl_ot_day_14_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_14 = 0;
                    }

                    if (lbl_ot_day_15_excel_otday.Text != "" && lbl_ot_day_15_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport15 += Convert.ToDecimal(lbl_ot_day_15_excel_otday.Text);
                        tot_actual_15 = Convert.ToDecimal(lbl_ot_day_15_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_15 = 0;
                    }

                    if (lbl_ot_day_16_excel_otday.Text != "" && lbl_ot_day_16_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport16 += Convert.ToDecimal(lbl_ot_day_16_excel_otday.Text);
                        tot_actual_16 = Convert.ToDecimal(lbl_ot_day_16_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_16 = 0;
                    }

                    if (lbl_ot_day_17_excel_otday.Text != "" && lbl_ot_day_17_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport17 += Convert.ToDecimal(lbl_ot_day_17_excel_otday.Text);
                        tot_actual_17 = Convert.ToDecimal(lbl_ot_day_17_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_17 = 0;
                    }

                    if (lbl_ot_day_18_excel_otday.Text != "" && lbl_ot_day_18_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport18 += Convert.ToDecimal(lbl_ot_day_18_excel_otday.Text);
                        tot_actual_18 = Convert.ToDecimal(lbl_ot_day_18_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_18 = 0;
                    }

                    if (lbl_ot_day_19_excel_otday.Text != "" && lbl_ot_day_19_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport19 += Convert.ToDecimal(lbl_ot_day_19_excel_otday.Text);
                        tot_actual_19 = Convert.ToDecimal(lbl_ot_day_19_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_19 = 0;
                    }

                    if (lbl_ot_day_20_excel_otday.Text != "" && lbl_ot_day_20_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport20 += Convert.ToDecimal(lbl_ot_day_20_excel_otday.Text);
                        tot_actual_20 = Convert.ToDecimal(lbl_ot_day_20_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_20 = 0;
                    }

                    if (lbl_ot_day_21_excel_otday.Text != "" && lbl_ot_day_21_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport21 += Convert.ToDecimal(lbl_ot_day_21_excel_otday.Text);
                        tot_actual_21 = Convert.ToDecimal(lbl_ot_day_21_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_21 = 0;
                    }

                    if (lbl_ot_day_22_excel_otday.Text != "" && lbl_ot_day_22_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport22 += Convert.ToDecimal(lbl_ot_day_22_excel_otday.Text);
                        tot_actual_22 = Convert.ToDecimal(lbl_ot_day_22_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_22 = 0;
                    }

                    if (lbl_ot_day_23_excel_otday.Text != "" && lbl_ot_day_23_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport23 += Convert.ToDecimal(lbl_ot_day_23_excel_otday.Text);
                        tot_actual_23 = Convert.ToDecimal(lbl_ot_day_23_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_23 = 0;
                    }

                    if (lbl_ot_day_24_excel_otday.Text != "" && lbl_ot_day_24_excel_otday.Text != "-")
                    {
                        tot_actual_otdayimport24 += Convert.ToDecimal(lbl_ot_day_24_excel_otday.Text);
                        tot_actual_24 = Convert.ToDecimal(lbl_ot_day_24_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_24 = 0;
                    }

                    if (lbl_ot_day_25_excel_otday.Text != "" && lbl_ot_day_25_excel_otday.Text != "-")
                    {

                        tot_actual_otdayimport25 += Convert.ToDecimal(lbl_ot_day_25_excel_otday.Text);
                        tot_actual_25 = Convert.ToDecimal(lbl_ot_day_25_excel_otday.Text);
                    }
                    else
                    {
                        tot_actual_25 = 0;
                    }

                    lbl_totalhours_.Text = (tot_actual_26 + tot_actual_27 + tot_actual_28 + tot_actual_29 + tot_actual_30 + tot_actual_31 + tot_actual_1 + tot_actual_2 + tot_actual_3
                      + tot_actual_4 + tot_actual_5 + tot_actual_6 + tot_actual_7 + tot_actual_8 + tot_actual_9 + tot_actual_10 + tot_actual_11 + tot_actual_12 + tot_actual_13 + tot_actual_14 + tot_actual_15
                      + tot_actual_16 + tot_actual_17 + tot_actual_18 + tot_actual_19 + tot_actual_20 + tot_actual_21 + tot_actual_22 + tot_actual_23 + tot_actual_24 + tot_actual_25).ToString();

                    tot_actual_sumot += (tot_actual_26 + tot_actual_27 + tot_actual_28 + tot_actual_29 + tot_actual_30 + tot_actual_31 + tot_actual_1 + tot_actual_2 + tot_actual_3
                        + tot_actual_4 + tot_actual_5 + tot_actual_6 + tot_actual_7 + tot_actual_8 + tot_actual_9 + tot_actual_10 + tot_actual_11 + tot_actual_12 + tot_actual_13 + tot_actual_14 + tot_actual_15
                        + tot_actual_16 + tot_actual_17 + tot_actual_18 + tot_actual_19 + tot_actual_20 + tot_actual_21 + tot_actual_22 + tot_actual_23 + tot_actual_24 + tot_actual_25);


                    //litDebug1.Text = tot_actual_otmonth.ToString();

                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    Label lit_total_ot_day_26_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_26_excel_otday");
                    Label lit_total_ot_day_27_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_27_excel_otday");
                    Label lit_total_ot_day_28_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_28_excel_otday");
                    Label lit_total_ot_day_29_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_29_excel_otday");
                    Label lit_total_ot_day_30_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_30_excel_otday");
                    Label lit_total_ot_day_31_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_31_excel_otday");
                    Label lit_total_ot_day_1_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_1_excel_otday");
                    Label lit_total_ot_day_2_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_2_excel_otday");
                    Label lit_total_ot_day_3_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_3_excel_otday");
                    Label lit_total_ot_day_4_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_4_excel_otday");
                    Label lit_total_ot_day_5_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_5_excel_otday");
                    Label lit_total_ot_day_6_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_6_excel_otday");
                    Label lit_total_ot_day_7_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_7_excel_otday");
                    Label lit_total_ot_day_8_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_8_excel_otday");
                    Label lit_total_ot_day_9_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_9_excel_otday");
                    Label lit_total_ot_day_10_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_10_excel_otday");
                    Label lit_total_ot_day_11_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_11_excel_otday");
                    Label lit_total_ot_day_12_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_12_excel_otday");
                    Label lit_total_ot_day_13_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_13_excel_otday");
                    Label lit_total_ot_day_14_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_14_excel_otday");
                    Label lit_total_ot_day_15_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_15_excel_otday");
                    Label lit_total_ot_day_16_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_16_excel_otday");
                    Label lit_total_ot_day_17_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_17_excel_otday");
                    Label lit_total_ot_day_18_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_18_excel_otday");
                    Label lit_total_ot_day_19_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_19_excel_otday");
                    Label lit_total_ot_day_20_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_20_excel_otday");
                    Label lit_total_ot_day_21_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_21_excel_otday");
                    Label lit_total_ot_day_22_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_22_excel_otday");
                    Label lit_total_ot_day_23_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_23_excel_otday");
                    Label lit_total_ot_day_24_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_24_excel_otday");
                    Label lit_total_ot_day_25_excel_otday = (Label)e.Row.FindControl("lit_total_ot_day_25_excel_otday");
                    Label lit_totalhours_sumtotal = (Label)e.Row.FindControl("lit_totalhours_sumtotal");

                    lit_total_ot_day_26_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport26); // Convert.ToString(tot_actual);
                    lit_total_ot_day_27_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport27); // Convert.ToString(tot_actual);
                    lit_total_ot_day_28_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport28); // Convert.ToString(tot_actual);
                    lit_total_ot_day_29_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport29); // Convert.ToString(tot_actual);
                    lit_total_ot_day_30_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport30); // Convert.ToString(tot_actual);
                    lit_total_ot_day_31_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport31); // Convert.ToString(tot_actual);
                    lit_total_ot_day_1_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport1); // Convert.ToString(tot_actual);
                    lit_total_ot_day_2_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport2); // Convert.ToString(tot_actual);
                    lit_total_ot_day_3_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport3); // Convert.ToString(tot_actual);
                    lit_total_ot_day_4_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport4); // Convert.ToString(tot_actual);
                    lit_total_ot_day_5_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport5); // Convert.ToString(tot_actual);
                    lit_total_ot_day_6_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport6); // Convert.ToString(tot_actual);
                    lit_total_ot_day_7_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport7); // Convert.ToString(tot_actual);
                    lit_total_ot_day_8_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport8); // Convert.ToString(tot_actual);
                    lit_total_ot_day_9_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport9); // Convert.ToString(tot_actual);
                    lit_total_ot_day_10_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport10); // Convert.ToString(tot_actual);
                    lit_total_ot_day_11_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport11); // Convert.ToString(tot_actual);
                    lit_total_ot_day_12_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport12); // Convert.ToString(tot_actual);
                    lit_total_ot_day_13_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport13); // Convert.ToString(tot_actual);
                    lit_total_ot_day_14_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport14); // Convert.ToString(tot_actual);
                    lit_total_ot_day_15_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport15); // Convert.ToString(tot_actual);
                    lit_total_ot_day_16_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport16); // Convert.ToString(tot_actual);
                    lit_total_ot_day_17_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport17); // Convert.ToString(tot_actual);
                    lit_total_ot_day_18_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport18); // Convert.ToString(tot_actual);
                    lit_total_ot_day_19_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport19); // Convert.ToString(tot_actual);
                    lit_total_ot_day_20_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport20); // Convert.ToString(tot_actual);
                    lit_total_ot_day_21_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport21); // Convert.ToString(tot_actual);
                    lit_total_ot_day_22_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport22); // Convert.ToString(tot_actual);
                    lit_total_ot_day_23_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport23); // Convert.ToString(tot_actual);
                    lit_total_ot_day_24_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport24); // Convert.ToString(tot_actual);
                    lit_total_ot_day_25_excel_otday.Text = String.Format("{0:N2}", tot_actual_otdayimport25); // Convert.ToString(tot_actual);
                    lit_totalhours_sumtotal.Text = String.Format("{0:N2}", tot_actual_sumot); // Convert.ToString(tot_actual);

                }
                break;
            case "GvDetailEmployeeImportOTDay":
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    UpdatePanel UpdatePanel_Header = (UpdatePanel)e.Row.FindControl("UpdatePanel_Header");
                    DropDownList ddlPlacesFilter = (DropDownList)e.Row.FindControl("ddlPlacesFilter");


                    getLocation(ddlPlacesFilter, _set_statusFilter);
                    //getStatusDocument(ddlStatusFilter, _set_statusFilter);

                    if (ViewState["Vs_SetHeader_Filter"].ToString() != "0")
                    {
                        UpdatePanel_Header.Visible = false;
                    }
                    else
                    {
                        UpdatePanel_Header.Visible = true;
                    }

                }
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label lbl_day_all_sum_detailimport_value1 = (Label)e.Row.FindControl("lbl_day_all_sum_detailimport_value");
                    Label lbl_day_all_sum_detailimport1 = (Label)e.Row.FindControl("lbl_day_all_sum_detailimport");
                    if (lbl_day_all_sum_detailimport_value1.Text != "" || lbl_day_all_sum_detailimport_value1.Text != null)
                    {
                        lbl_day_all_sum_detailimport1.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_day_all_sum_detailimport_value1.Text));
                        //litDebug.Text = lbl_day_all_sum_detailimport_value1.Text;
                        //litDebug.Text = "1";
                    }

                }
                break;
            case "GvSearchReportOTDay":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label lbl_day_all_sum_detailimportvalue = (Label)e.Row.FindControl("lbl_day_all_sum_detailimportvalue");
                    Label lbl_day_all_sum_detailimport = (Label)e.Row.FindControl("lbl_day_all_sum_detailimport");
                    if (lbl_day_all_sum_detailimportvalue.Text != "" || lbl_day_all_sum_detailimportvalue.Text != null)
                    {
                        //litDebug.Text = lbl_day_all_sum_detailimportvalue.Text;
                        //litDebug.Text = "12";
                        lbl_day_all_sum_detailimport.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_day_all_sum_detailimportvalue.Text));
                    }

                }

                break;
            case "GvEmployeeSetOTMonth":

                ////if (e.Row.RowType == DataControlRowType.DataRow)
                ////{
                ////    CheckBox check_employee = (CheckBox)e.Row.FindControl("check_employee");
                ////    HiddenField hfSelected = (HiddenField)e.Row.FindControl("hfSelected");
                ////    //test

                ////    if (chkAllEmployee.Checked)
                ////    {
                ////        litDebug.Text = "49";
                ////        check_employee.Checked = true;
                ////        ////if (ViewState["Vs_EmployeeSetOTMonth"] != null)
                ////        ////{
                ////        ////    ovt_employeeset_otmonth_detail[] _tempList = (ovt_employeeset_otmonth_detail[])ViewState["Vs_EmployeeSetOTMonth"];

                ////        ////    //int _checked = int.Parse(chk.Text);
                ////        ////    //_tempList[_checked].selected = chk.Checked;
                ////        ////    int _checked = int.Parse(check_employee.Text);
                ////        ////    _tempList[_checked].selected = check_employee.Checked;
                ////        ////    ////check_employee.Checked = true;
                ////        ////    //chk.Enabled = false;
                ////        ////    litDebug.Text = "49";

                ////        ////}
                ////        ////else
                ////        ////{
                ////        ////    litDebug.Text = "39";
                ////        ////}


                ////    }
                ////    else if(bool.Parse(hfSelected.Value) == true)
                ////    {
                ////        litDebug.Text = "222";
                ////        check_employee.Checked = true;
                ////        //litDebug.Text = "9";
                ////    }
                ////    else
                ////    {
                ////        if(check_employee.Checked == true)
                ////        {
                ////            check_employee.Checked = true;
                ////            litDebug.Text = "21";
                ////        }
                ////        //else
                ////        //{
                ////        //    check_employee.Checked = false;
                ////        //    litDebug.Text = "22";
                ////        //}
                ////        //chkAllEmployee.Checked = true;
                ////        //litDebug.Text = "21";
                ////    }

                ////}


                break;
            case "GvEmployee_Report":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                }

                break;

        }
    }

    protected void getTimeSumhour()
    {
        int s_excel = 0;
        foreach (GridViewRow row in GvCreateOTMonth.Rows)
        {
            //CheckBox chk_detailTopic_ma_txt = (CheckBox)row.Cells[0].FindControl("chk_detailTopic_ma");
            DropDownList ddl_timestart = (DropDownList)row.FindControl("ddl_timestart");
            DropDownList ddl_timeend = (DropDownList)row.FindControl("ddl_timeend");
            TextBox txt_job = (TextBox)row.FindControl("txt_job");
            //decimal total_price = 0;

            //if (ddl_timestart.SelectedValue != "0" && (ddl_timeend.SelectedValue != "0" || ddl_timeend.SelectedValue != ""))
            if (ddl_timestart.SelectedValue != "0")
            {
                txt_job.Visible = true;
                //litDebug.Text += "7";
                //break;
                //if (txt_Quantity_txt.Text != "" && txt_price_uit_txt.Text != "")
                //{
                //    linkBtnTrigger(btnSaveDetailMA);
                //    //litDebug.Text = Convert.ToInt32(txt_Quantity_txt.Text).ToString();
                //    total_price = Convert.ToDecimal(txt_Quantity_txt.Text) * Convert.ToDecimal(txt_price_uit_txt.Text);
                //    //litDebug.Text = Convert.ToDecimal(total_price).ToString();
                //    txt_price_total_txt.Text = Convert.ToDecimal(total_price).ToString();

                //}
                //else
                //{
                //    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรอกราคา/หน่วย ---');", true);

                //    break;
                //}
            }
            //else
            //{
            //    litDebug1.Text = "8";
            //    txt_job.Visible = false;
            //}
            s_excel++;

        }
    }


    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvViewDetailReport":
                //for (int rowIndex = GvViewDetailReport.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                //{
                //    GridViewRow currentRow = GvViewDetailReport.Rows[rowIndex];
                //    GridViewRow previousRow = GvViewDetailReport.Rows[rowIndex + 1];

                //    if (((Label)currentRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text == ((Label)previousRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text)
                //    {
                //        if (previousRow.Cells[0].RowSpan < 2)
                //        {
                //            currentRow.Cells[0].RowSpan = 2;

                //        }
                //        else
                //        {
                //            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                //        }
                //        previousRow.Cells[0].Visible = false;
                //    }


                //    if (((Label)currentRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text == ((Label)previousRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text)
                //    {
                //        if (previousRow.Cells[1].RowSpan < 2)
                //        {
                //            currentRow.Cells[1].RowSpan = 2;

                //        }
                //        else
                //        {
                //            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                //        }
                //        previousRow.Cells[1].Visible = false;
                //    }


                //}
                break;
        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;

            switch (cmdName)
            {
                case "cmdRemoveCar":
                    //GridView gvAddCarBooking = (GridView)FvHrApprove.FindControl("gvAddCarBooking");
                    //GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    //int rowIndex = rowSelect.RowIndex;
                    //DataSet dsContacts = (DataSet)ViewState["vsCarDetailList"];
                    //dsContacts.Tables["dsCarDetailTable"].Rows[rowIndex].Delete();
                    //dsContacts.AcceptChanges();
                    //setGridData(gvAddCarBooking, dsContacts.Tables["dsCarDetailTable"]);
                    //if (dsContacts.Tables["dsCarDetailTable"].Rows.Count < 1)
                    //{
                    //    gvAddCarBooking.Visible = false;
                    //}
                    break;

            }
        }
    }

    #endregion gridview

    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {
                case "FvWaitApproveDetail":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {

                    }

                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {


                    }
                    break;
                case "Fv_Search_Emp_Report":
                    FormView Fv_Search_Emp_Report = (FormView)docReport.FindControl("Fv_Search_Emp_Report");
                    DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
                    DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
                    DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
                    DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");

                    /*if (Fv_Search_Emp_Report.CurrentMode == FormViewMode.Insert)
                    {
                        select_org(ddlorg_rp);
                    }*/
                    break;
                case "Fv_Search_Emp_Report_day":
                    FormView Fv_Search_Emp_Report_day = (FormView)docReport.FindControl("Fv_Search_Emp_Report_day");
                    DropDownList ddlorg_rp_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlorg_rp");
                    DropDownList ddldep_rp_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddldep_rp");
                    DropDownList ddlsec_rp_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlsec_rp");
                    DropDownList ddlpos_rp_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlpos_rp");

                    if (Fv_Search_Emp_Report_day.CurrentMode == FormViewMode.Insert)
                    {
                        getOrganizationList(ddlorg_rp_day);
                    }
                    break;
                case "FvViewDetailEmployeeImportDay":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {
                        ////if(ViewState["Vs_FvViewDetailEmployeeImportDay"] != null)
                        ////{
                        ////    Label lbl_day_all_sum_viewimportvalue = (Label)FvViewDetailEmployeeImportDay.FindControl("lbl_day_all_sum_viewimportvalue");
                        ////    Label lbl_day_all_sum_viewimport = (Label)FvViewDetailEmployeeImportDay.FindControl("lbl_day_all_sum_viewimport");

                        ////    if (lbl_day_all_sum_viewimportvalue.Text == "" || lbl_day_all_sum_viewimportvalue.Text != null)
                        ////    {
                        ////        lbl_day_all_sum_viewimport.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_day_all_sum_viewimportvalue.Text));
                        ////    }
                        ////}


                    }


                    break;
            }
        }
    }
    #endregion Formview Databind

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {

        if (int.Parse(ViewState["vs_CountWaitApprove_Monthly"].ToString()) > 0)
        {

            setActiveTab("docWaitApprove", 0, 0, 0, 0, 2, 0, 0);
        }
        else
        {
            setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);
        }

        ////litDebug.Text = ViewState["time_idx_permission"].ToString();
        //if (ViewState["time_idx_permission"].ToString() == "1") //shift คงที่
        //{
        //    setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);
        //}
        //else if (ViewState["time_idx_permission"].ToString() == "2")
        //{

        //    setActiveTab("docDetailAdminOTDay", 0, 0, 0, 0, 2, 0, 0);
        //    //setActiveTab("docDetail", 0, 0, 0, 0, 1, 0, 0);
        //}
        //else
        //{
        //    setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);
        //}

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_overtime callServicePostOvertime(string _cmdUrl, data_overtime _data_overtime)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_overtime);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_overtime = (data_overtime)_funcTool.convertJsonToObject(typeof(data_overtime), _localJson);

        return _data_overtime;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx, int u1idx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
        setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);

        //set tab detail
        ////liDetailOTDay.Attributes.Add("class", "");
        ////liDetailMonth.Attributes.Add("class", "");

        _PanelDetailDay.Visible = false;
        _PanelDetailMonth.Visible = false;

        Update_BackToDetailOTDay.Visible = false;
        GvDetailOTDay.Visible = false;
        setGridData(GvDetailOTDay, null);

        GvDetailOTMont.Visible = false;
        setGridData(GvDetailOTMont, null);

        GvViewDetailOTDay.Visible = false;
        setGridData(GvViewDetailOTDay, null);

        Update_BackToDetail.Visible = false;
        div_LogViewDetailDay.Visible = false;
        div_LogViewDetailMonth.Visible = false;
        _divheaderday.Visible = false;
        _divheadermonth.Visible = false;
        div_searchDetailOTMonth.Visible = false;
        _PanelSearchDetail.Visible = false;
        Update_BackToDetailSearchIndexOTMonth.Visible = false;

        GvViewDetailOTMont.Visible = false;
        setGridData(GvViewDetailOTMont, null);
        div_GvViewDetailOTMont.Visible = false;


        GvFileMenoOTMonth.Visible = false;
        setGridData(GvFileMenoOTMonth, null);

        GvFileMenoWaitOTMonth.Visible = false;
        setGridData(GvFileMenoWaitOTMonth, null);

        setFormData(FvDetailOTDay, FormViewMode.ReadOnly, null);
        setFormData(FvDetailOTMont, FormViewMode.ReadOnly, null);

        GvViewEditDetailOTMont.Visible = false;
        setGridData(GvViewEditDetailOTMont, null);
        div_GvViewEditDetailOTMont.Visible = false;


        Update_PanelSaveEditDocMonth.Visible = false;
        div_EditFileMemo.Visible = false;

        //set tab create
        //liCreateOTDay.Attributes.Add("class", "");
        //liCreateOTMonth.Attributes.Add("class", "");

        _PanelCreateDay.Visible = false;
        _PanelCreateMonth.Visible = false;
        _PanelOTMonth.Visible = false;

        //tab ot day admin create
        _PanelCreateOTDay.Visible = false;
        _PanelDetailCreateOTDay.Visible = false;
        _PanelPermissionCreateOTShift2.Visible = false;

        ddl_month_search.ClearSelection();
        setGridData(GvCreateOTMonth, null);

        UpdatePanel_CreateOTDay.Visible = false;
        setGridData(GvGroupCreateOTDay, null);

        div_Addfileotmonth.Visible = false;


        //tab head set ot month
        setFormData(fvEmpDetailSetOTMonth, FormViewMode.ReadOnly, null);
        _PanelSetOTMonth.Visible = false;
        GvEmployeeSetOTMonth.Visible = false;
        setGridData(GvEmployeeSetOTMonth, null);
        GvViewDetailHeadSetOTMont.Visible = false;
        setGridData(GvViewDetailHeadSetOTMont, null);


        //tab wait approve ot
        _PanelWaitApproveDay.Visible = false;
        _PanelWaitApproveMonth.Visible = false;

        //lbWaitDetailApproveDay.Attributes.Add("class", "");
        //lbWaitDetailApproveMonth.Attributes.Add("class", "");

        GvWaitDetailOT.Visible = false;
        setGridData(GvWaitDetailOT, null);

        GvWaitDetailOTMonth.Visible = false;
        setGridData(GvWaitDetailOTMonth, null);
        _div_headerwaitapprove.Visible = false;
        _div_headerwaitapprove_otday.Visible = false;

        div_SearchWaitApproveOTMonth.Visible = false;
        _PanelCancelSearchWaitApproveOTMonth.Visible = false;
        _PanelSearchWaitApprove.Visible = false;

        setFormData(FvDetailOTDayWaitapprove, FormViewMode.ReadOnly, null);

        setFormData(FvDetailOTMontWaitapprove, FormViewMode.ReadOnly, null);
        setFormData(FvHeadUserApprove, FormViewMode.ReadOnly, null);

        setFormData(FvHeadUserApproveOTDay, FormViewMode.ReadOnly, null);

        Update_BackToWaitApproveOTDay.Visible = false;
        Update_BackToWaitApprove.Visible = false;

        GvViewWaitApproveOTMont.Visible = false;
        setGridData(GvViewWaitApproveOTMont, null);
        div_GvViewWaitApproveOTMont.Visible = false;

        _div_LogViewDetailWaitDay.Visible = false;
        _div_LogViewDetailWaitMonth.Visible = false;

        setFormData(FvHrApproveOTDay, FormViewMode.ReadOnly, null);
        setFormData(FvHrApproveOTMonth, FormViewMode.ReadOnly, null);
        //setRepeaterData(rptBindbtnApprove, null);

        //tab report
        _PanelReportDay.Visible = false;
        _PanelReportMonth.Visible = false;

        lbReportDay.Attributes.Add("class", "");
        lbReportMonth.Attributes.Add("class", "");

        //tab add group ot day
        div_AddGroupOTDay.Visible = false;
        btnAddGroupOTDay.Visible = false;
        PanelAddGroupOTDay.Visible = false;
        Div_GvDetailExcelImport.Visible = false;
        Div_BackToDetailGroupOTDay.Visible = false;

        GvDetailGroupOTDay.Visible = false;
        setGridData(GvDetailGroupOTDay, null);

        GvViewDetailGroupOTDay.Visible = false;
        setGridData(GvViewDetailGroupOTDay, null);

        //tab import ot day
        _PanelImportOTDay.Visible = false;
        Div_GvDetailImportOTDay.Visible = false;

        GvDetailImportOTDay.Visible = false;
        setGridData(GvDetailImportOTDay, null);

        //set tab import detail ot day
        _PanelDetailImportOTDay.Visible = false;
        GvDetailImportOTDay.Visible = false;
        setGridData(GvDetailImportOTDay, null);
        div_headdetail_otday.Visible = false;

        _PanelBackDetailImportOTDay.Visible = false;

        setFormData(FvViewDetailEmployeeImportDay, FormViewMode.ReadOnly, null);

        divLogDetailEmployeeImportDay.Visible = false;

        GvDetailEmployeeImportOTDay.Visible = false;
        setGridData(GvDetailEmployeeImportOTDay, null);

        divGvViewDetailEmployeeImportDay.Visible = false;
        GvViewDetailEmployeeImportDay.Visible = false;
        setGridData(GvViewDetailEmployeeImportDay, null);

        txt_dateot_dayform.Text = string.Empty;
        txt_dateot_dayto.Text = string.Empty;
        txt_time_dayform.Text = string.Empty;
        txt_time_dayto.Text = string.Empty;

        //set tab report
        _PanelReportDay.Visible = false;

        GvSearchReportOTDay.Visible = false;
        setGridData(GvSearchReportOTDay, null);

        divReportGvViewDetailEmployeeImportDay.Visible = false;
        GvReportEmployeeImportDay.Visible = false;
        setGridData(GvReportEmployeeImportDay, null);
        setFormData(FvReportViewDetailEmployeeImportDay, FormViewMode.ReadOnly, null);

        _PanelBackToDetailReportImportOTDay.Visible = false;

        txt_dateot_dayform_report.Text = string.Empty;
        txt_dateot_dayto_report.Text = string.Empty;
        txt_time_dayform_report.Text = string.Empty;
        txt_time_dayto_report.Text = string.Empty;
        ////linkBtnTrigger(btnImportExcelOTDay);

        //tab report
        _PanelReportOTMonth.Visible = false;
        GvReportOTMonth.Visible = false;
        setGridData(GvReportOTMonth, null);
        _PanelExportExcelReportMonth.Visible = false;


        //tab report

        //tab detail admin

        _PanelDetailAdminOTDay.Visible = false;
        GvDetailAdminOTDay.Visible = false;
        setGridData(GvDetailAdminOTDay, null);
        _divheader_empshiftadmin.Visible = false;

        setFormData(FvDetailAdminOTDay, FormViewMode.ReadOnly, null);
        _PanelGvViewDetailAdminOTDay.Visible = false;
        _PnDetailBackToDetailAdmin.Visible = false;

        //tab dtail employee
        _PanelDetailOTDayToEmployee.Visible = false;

        GvDetailToEmployeeCreateAdmin.Visible = false;
        setGridData(GvDetailToEmployeeCreateAdmin, null);
        _divheader_empshiftempployee.Visible = false;

        GvDetailOTDayToEmployee.Visible = false;
        setGridData(GvDetailOTDayToEmployee, null);

        div_LogViewDetailDayAdminCreate.Visible = false;
        setFormData(FvDetailOTDayToEmployee, FormViewMode.ReadOnly, null);

        //doc wait approve
        _PanelGvWaitApproveOTDayAdminCrate.Visible = false;
        div_btnApproveHeadAdmincreate.Visible = false;
        div_btnApproveAdminEditOTDay.Visible = false;


        divheadapprove_otday.Visible = false;
        divhrapprove_otday.Visible = false;
        divadminapprove_otday.Visible = false;

        liWaitApproveHeadAdminCreate.Attributes.Add("class", "");
        liWaitApproveHRAdminCreate.Attributes.Add("class", "");
        liWaitApproveAdminEdit.Attributes.Add("class", "");

        liDetailAdminOTDay.Attributes.Add("class", "");
        liDetailEmployeeOTDay.Attributes.Add("class", "");

        _PndocDetailEmployeeOTDay.Visible = false;
        _PnBackToDetailEmployee.Visible = false;

        switch (activeTab)
        {
            case "docDetail":

                switch (_chk_tab)
                {
                    case 1: //day
                        ////liDetailOTDay.Attributes.Add("class", "active");
                        _PanelDetailDay.Visible = true;
                        switch (nodeidx)
                        {
                            case 0:
                                _divheaderday.Visible = true;
                                getDetailOT(GvDetailOTDay, _chk_tab);
                                break;

                            case 4: //รอตรวจสอบ OT แบบปกติ รายวัน
                            case 6:

                                //litDebug.Text = "4";

                                Update_BackToDetailOTDay.Visible = true;
                                getViewOTMonth(FvDetailOTDay, uidx);

                                getViewDetailOTMonth(GvViewDetailOTDay, uidx, 0);

                                div_LogViewDetailDay.Visible = true;
                                getLogDetailOTMonth(rptLogViewDetailDay, uidx);
                                break;
                        }


                        break;
                    case 2: //month
                        ////liDetailMonth.Attributes.Add("class", "active");
                        _PanelDetailMonth.Visible = true;

                        switch (nodeidx)
                        {
                            case 0:
                                ////_divheadermonth.Visible = true;
                                ///div_searchDetailOTMonth.Visible = true;
                                ////getDetailOT(GvDetailOTMont, _chk_tab);
                                ///
                                _PanelSearchDetail.Visible = true;
                                getDetailOT(GvDetailOTMont, _chk_tab);

                                ddlSearchMonth.ClearSelection();
                                ddlSearchemp.ClearSelection();

                                txtSearchCostcenter.Text = string.Empty;
                                txtSearchEmpcode.Text = string.Empty;


                                getddlYear(ddlSearchYear);
                                getOrganizationList(ddlSearchorg);
                                getDepartmentList(ddlSearchrdept, int.Parse(ddlSearchorg.SelectedValue));
                                getSectionList(ddlSearchrsec, int.Parse(ddlSearchorg.SelectedValue), int.Parse(ddlSearchrdept.SelectedValue));
                                getStatusDocList(ddlStatusDoc);



                                break;
                            case 1: //user edit doc
                                Update_BackToDetail.Visible = true;
                                getViewOTMonth(FvDetailOTMont, uidx);

                                string getPathFileMemo = ConfigurationSettings.AppSettings["path_file_otmonth"];
                                string uidx_1 = uidx.ToString();

                                HiddenField hfmonth_ot = (HiddenField)FvDetailOTMont.FindControl("hfmonth_ot");
                                string prevMonth = DateTime.Now.AddMonths(-1).ToString("MM");

                                //ddl startedit
                                data_overtime data_u0_timestart_otmonth = new data_overtime();
                                ovt_timestart_otmonth_detail u0_timestart_otmonth = new ovt_timestart_otmonth_detail();
                                data_u0_timestart_otmonth.ovt_timestart_otmonth_list = new ovt_timestart_otmonth_detail[1];
                                u0_timestart_otmonth.condition = 2;
                                //u0_timestart_otmonth.u0_doc1_idx = _u1idx;
                                u0_timestart_otmonth.u0_doc_idx = uidx;
                                u0_timestart_otmonth.emp_idx = _emp_idx;
                                //u0_timestart_otmonth.emp_code = _tbEmpCode.ToString();
                                //u0_timestart_otmonth.date_start = _date_start;

                                data_u0_timestart_otmonth.ovt_timestart_otmonth_list[0] = u0_timestart_otmonth;


                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timestart_otmonth));
                                data_u0_timestart_otmonth = callServicePostOvertime(_urlGetTimeStartScanOTMont, data_u0_timestart_otmonth);
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timestart_otmonth));

                                ViewState["vs_TimeStartEdit"] = data_u0_timestart_otmonth.ovt_timestart_otmonth_list;
                                //ddl startedit

                                //ddl end edit
                                data_overtime data_u0_timeend_otmonth = new data_overtime();
                                ovt_timestart_otmonth_detail u0_timeend_otmonth = new ovt_timestart_otmonth_detail();
                                data_u0_timeend_otmonth.ovt_timestart_otmonth_list = new ovt_timestart_otmonth_detail[1];

                                u0_timeend_otmonth.condition = 3;
                                //u0_timeend_otmonth.u0_doc1_idx = _u1idx;
                                u0_timeend_otmonth.u0_doc_idx = uidx;
                                u0_timeend_otmonth.emp_idx = _emp_idx;
                                //u0_timeend_otmonth.emp_code = _tbEmpCode.ToString();
                                //u0_timeend_otmonth.date_start = _date_start;

                                data_u0_timeend_otmonth.ovt_timestart_otmonth_list[0] = u0_timeend_otmonth;
                                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b)); 
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timeend_otmonth));

                                data_u0_timeend_otmonth = callServicePostOvertime(_urlGetTimeStartScanOTMont, data_u0_timeend_otmonth);
                                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_timeend_otmonth));

                                ViewState["vs_TimeEndEdit"] = data_u0_timeend_otmonth.ovt_timestart_otmonth_list;

                                ////setDdlData(ddlName, data_u0_timeend_otmonth.ovt_timestart_otmonth_list, "time_end", "month_idx");
                                ////ddlName.SelectedValue = _month_idx_end.ToString();

                                //ddl end edit




                                //litDebug.Text = prevMonth;
                                var _convert_month = "";
                                if (int.Parse(hfmonth_ot.Value) < 10)
                                {
                                    _convert_month = "0" + hfmonth_ot.Value;
                                    //litDebug.Text = _convert_month.ToString();
                                }
                                else
                                {
                                    _convert_month = hfmonth_ot.Value;
                                }

                                if (_cempidx == _emp_idx) //permission empcreate can edit
                                {
                                    //litDebug.Text = "3";
                                    linkBtnTrigger(btnSaveEditOTMonth);

                                    IFormatProvider culture_edit = new CultureInfo("en-US", true);
                                    DateTime DateToday_edit = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    string input_dayedit = DateToday_edit.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    string[] result_edit = input_dayedit.Split(new string[] { "/" }, StringSplitOptions.None);

                                    string setday_edit = "01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31";
                                    //string setday_editFileMemo = "12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28";
                                    string[] setDayEdit = setday_edit.Split(',');
                                    if (Array.IndexOf(setDayEdit, result_edit[0].ToString()) > -1 && prevMonth == _convert_month) //can edit
                                    {
                                        //litDebug.Text = "18";
                                        getViewDetailEditOTMonth(GvViewEditDetailOTMont, uidx, 0);

                                        Update_PanelSaveEditDocMonth.Visible = true;

                                        GvFileMenoOTMonth.Visible = true;
                                        GetPathFile(uidx_1.ToString(), getPathFileMemo, GvFileMenoOTMonth);

                                        //litDebug1.Text = uidx_1.ToString();//ViewState["Vs_CheckFileMemo"].ToString();

                                        if (ViewState["Vs_CheckFileMemo"].ToString() == "yes")// has file canedit memo
                                        {
                                            div_EditFileMemo.Visible = true;
                                        }

                                    }
                                    else
                                    {
                                        //litDebug.Text = "8";
                                        linkBtnTrigger(btnSaveEditOTMonth);

                                        getViewDetailOTMonth(GvViewDetailOTMont, uidx, 0);
                                        GvFileMenoOTMonth.Visible = true;
                                        GetPathFile(uidx_1.ToString(), getPathFileMemo, GvFileMenoOTMonth);

                                        if (ViewState["Vs_CheckFileMemo"].ToString() == "yes")// has file canedit memo
                                        {
                                            div_EditFileMemo.Visible = true;
                                        }

                                        getFileViewDetailOTMonth(GvFileMenoOTMonth, uidx, 0);
                                    }

                                }
                                else
                                {
                                    //litDebug.Text = "28";
                                    getViewDetailOTMonth(GvViewDetailOTMont, uidx, 0);
                                    GvFileMenoOTMonth.Visible = true;
                                    GetPathFile(uidx_1.ToString(), getPathFileMemo, GvFileMenoOTMonth);
                                }


                                div_LogViewDetailMonth.Visible = true;
                                getLogDetailOTMonth(rptLogViewDetailMonth, uidx);

                                break;
                            case 2:
                                //litDebug.Text = staidx.ToString();

                                Update_BackToDetail.Visible = true;
                                getViewOTMonth(FvDetailOTMont, uidx);

                                string getPathFileMemo_2 = ConfigurationSettings.AppSettings["path_file_otmonth"];
                                string uidx_2 = uidx.ToString();

                                if (staidx == 9)
                                {
                                    getViewDetailOTMonth(GvViewDetailOTMont, uidx, 1);

                                    //litDebug.Text = "555";
                                    GvFileMenoOTMonth.Visible = true;
                                    GetPathFile(uidx_2.ToString(), getPathFileMemo_2, GvFileMenoOTMonth);

                                }
                                else
                                {
                                    getViewDetailOTMonth(GvViewDetailOTMont, uidx, 0);
                                    GvFileMenoOTMonth.Visible = true;
                                    GetPathFile(uidx_2.ToString(), getPathFileMemo_2, GvFileMenoOTMonth);

                                }


                                div_LogViewDetailMonth.Visible = true;
                                getLogDetailOTMonth(rptLogViewDetailMonth, uidx);

                                break;
                            case 3:
                                Update_BackToDetail.Visible = true;
                                getViewOTMonth(FvDetailOTMont, uidx);

                                string getPathFileMemo_3 = ConfigurationSettings.AppSettings["path_file_otmonth"];
                                string uidx_3 = uidx.ToString();

                                getViewDetailOTMonth(GvViewDetailOTMont, uidx, 0);
                                GvFileMenoOTMonth.Visible = true;
                                GetPathFile(uidx_3.ToString(), getPathFileMemo_3, GvFileMenoOTMonth);


                                div_LogViewDetailMonth.Visible = true;
                                getLogDetailOTMonth(rptLogViewDetailMonth, uidx);

                                break;
                            case 8: //head user set ot

                                Update_BackToDetail.Visible = true;
                                getViewOTMonth(FvDetailOTMont, uidx);

                                getViewDetailOTMonth(GvViewDetailHeadSetOTMont, uidx, 0);

                                div_LogViewDetailMonth.Visible = true;
                                getLogDetailOTMonth(rptLogViewDetailMonth, uidx);


                                break;
                        }

                        break;


                }
                setOntop.Focus();
                break;
            case "docCreate":

                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                switch (_chk_tab)
                {
                    case 1:

                        ////liCreateOTDay.Attributes.Add("class", "active");

                        data_overtime data_permission_admin = new data_overtime();
                        ovt_permission_admin_detail m0_permission_admin = new ovt_permission_admin_detail();
                        data_permission_admin.ovt_permission_admin_list = new ovt_permission_admin_detail[1];

                        m0_permission_admin.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());

                        data_permission_admin.ovt_permission_admin_list[0] = m0_permission_admin;
                        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b)); 


                        data_permission_admin = callServicePostOvertime(_urlGetPermissionAdminCreate, data_permission_admin);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_permission_admin));

                        if (data_permission_admin.return_code == 0)
                        {
                            _PanelCreateOTDay.Visible = true;
                            ////getTimeStartOTDay();
                            ////getTimeEndOTDay();
                            ////getTimeStartOTDay_Scan();
                            ////getTimeEndOTDay_Scan();

                            ////getDetailOTDayAdmin(GvDetailCreateOTDay);

                        }
                        else
                        {
                            _PanelPermissionCreateOTShift2.Visible = true;

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- คุณไม่มีสิทธิ์สร้างรายการ ---');", true);
                            break;
                        }
                        ////sss



                        ////_PanelCreateDay.Visible = true;
                        ////getGroupOTDay(ddlgroup_otday, 0);
                        //getViewDetailGroupOTDay(GvGroupCreateOTDay, uidx);
                        break;
                    case 2:

                        ////liCreateOTMonth.Attributes.Add("class", "active");

                        if (ViewState["time_idx_permission"].ToString() == "1") //กะคงที่
                        {
                            _PanelCreateMonth.Visible = true;
                        }
                        else
                        {

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาตรวจสอบกะการทำงานก่อนสร้างรายการ ---');", true);
                            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert(--- กรุณาตรวจสอบกะการทำงานก่อนสร้างรายการ ---);"), true);
                            break;
                        }

                        break;

                }
                setOntop.Focus();
                break;
            case "docSetOTMonth":
                setFormData(fvEmpDetailSetOTMonth, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                _PanelSetOTMonth.Visible = true;

                getEmployeeSetOTMonth();//get employee set ot month by head user

                break;
            case "docWaitApprove":
                switch (_chk_tab)
                {
                    case 1: //day
                        ////lbWaitDetailApproveDay.Attributes.Add("class", "active");

                        _PanelWaitApproveDay.Visible = true;
                        switch (nodeidx)
                        {
                            case 0:


                                _div_headerwaitapprove_otday.Visible = true;
                                getWaitApproveDetailOT(GvWaitDetailOT, _chk_tab);
                                break;
                            case 4: //head user approve

                                Update_BackToWaitApproveOTDay.Visible = true;
                                getViewOTMonth(FvDetailOTDayWaitapprove, uidx);

                                getViewDetailOTDay(GvViewWaitApproveOTDay, uidx, 0);

                                setFormData(FvHeadUserApproveOTDay, FormViewMode.Insert, null);

                                _div_LogViewDetailWaitDay.Visible = true;
                                getLogDetailOTMonth(rptLogWaitViewDetailDay, uidx);

                                Repeater rptBindbtnApproveOtDay = (Repeater)FvHeadUserApproveOTDay.FindControl("rptBindbtnApproveOtDay");
                                getbtnDecisionApprove(rptBindbtnApproveOtDay, nodeidx, 0);


                                break;
                            case 6:
                                Update_BackToWaitApproveOTDay.Visible = true;
                                getViewOTMonth(FvDetailOTDayWaitapprove, uidx);

                                getViewDetailOTDay(GvViewWaitApproveOTDay, uidx, 0);

                                setFormData(FvHrApproveOTDay, FormViewMode.Insert, null);

                                _div_LogViewDetailWaitDay.Visible = true;
                                getLogDetailOTMonth(rptLogWaitViewDetailDay, uidx);

                                Repeater rptBindHRbtnApproveOTDay = (Repeater)FvHrApproveOTDay.FindControl("rptBindHRbtnApproveOTDay");
                                getbtnDecisionApprove(rptBindHRbtnApproveOTDay, nodeidx, 0);

                                break;
                        }
                        break;
                    case 2: //month
                        ////lbWaitDetailApproveMonth.Attributes.Add("class", "active");
                        _PanelWaitApproveMonth.Visible = true;

                        string getPathFileMemo_2wait = ConfigurationSettings.AppSettings["path_file_otmonth"];
                        string uidx_2wait = uidx.ToString();

                        switch (nodeidx)
                        {
                            case 0:
                                ////div_SearchWaitApproveOTMonth.Visible = true;
                                ////_div_headerwaitapprove.Visible = true;
                                ////getWaitApproveDetailOT(GvWaitDetailOTMonth, _chk_tab);

                                _PanelSearchWaitApprove.Visible = true;
                                getWaitApproveDetailOT(GvWaitDetailOTMonth, _chk_tab);

                                getddlYear(ddlSearchYear_WaitApprove);
                                ddlSearchMonth_WaitApprove.ClearSelection();
                                ddlSearch_emp_WaitApprove.ClearSelection();
                                txt_SearchCost_WaitApprove.Text = string.Empty;
                                txt_SearchEmpCode_WaitApprove.Text = string.Empty;

                                getOrganizationList(ddlSearchorg_WaitApprove);
                                getDepartmentList(ddlSearch_rdept_WaitApprove, int.Parse(ddlSearchorg_WaitApprove.SelectedValue));
                                getSectionList(ddlSearch_rsec_WaitApprove, int.Parse(ddlSearchorg_WaitApprove.SelectedValue), int.Parse(ddlSearch_rdept_WaitApprove.SelectedValue));





                                break;
                            case 2: //head user approve

                                //litDebug.Text = _chk_tab.ToString();

                                Update_BackToWaitApprove.Visible = true;
                                getViewOTMonth(FvDetailOTMontWaitapprove, uidx);
                                getViewDetailOTMonth(GvViewWaitApproveOTMont, uidx, 0);
                                div_GvViewWaitApproveOTMont.Visible = true;

                                GvFileMenoWaitOTMonth.Visible = true;
                                GetPathFile(uidx_2wait.ToString(), getPathFileMemo_2wait, GvFileMenoWaitOTMonth);

                                setFormData(FvHeadUserApprove, FormViewMode.Insert, null);

                                _div_LogViewDetailWaitMonth.Visible = true;
                                getLogDetailOTMonth(rptLogWaitViewDetailMonth, uidx);

                                DropDownList ddlApproveStatus = (DropDownList)FvHeadUserApprove.FindControl("ddlApproveStatus");
                                Repeater rptBindbtnApprove = (Repeater)FvHeadUserApprove.FindControl("rptBindbtnApprove");

                                getDecisionApprove(ddlApproveStatus, nodeidx, 0);
                                getbtnDecisionApprove(rptBindbtnApprove, nodeidx, 0);



                                break;
                            case 3: //hr approve

                                Update_BackToWaitApprove.Visible = true;
                                getViewOTMonth(FvDetailOTMontWaitapprove, uidx);
                                getViewDetailOTMonth(GvViewWaitApproveOTMont, uidx, 0);
                                div_GvViewWaitApproveOTMont.Visible = true;
                                //TextBox lbl_comment_hr_approve = (TextBox)GvViewWaitApproveOTMont.FindControl("lbl_comment_hr_approve");
                                //lbl_comment_hr_approve.Enabled = true;

                                GvFileMenoWaitOTMonth.Visible = true;
                                GetPathFile(uidx_2wait.ToString(), getPathFileMemo_2wait, GvFileMenoWaitOTMonth);

                                setFormData(FvHrApproveOTMonth, FormViewMode.Insert, null);

                                _div_LogViewDetailWaitMonth.Visible = true;
                                getLogDetailOTMonth(rptLogWaitViewDetailMonth, uidx);

                                DropDownList ddlHrApproveStatus = (DropDownList)FvHrApproveOTMonth.FindControl("ddlHrApproveStatus");
                                Repeater rptBindHRbtnApprove = (Repeater)FvHrApproveOTMonth.FindControl("rptBindHRbtnApprove");
                                getDecisionApprove(ddlHrApproveStatus, nodeidx, 0);
                                getbtnDecisionApprove(rptBindHRbtnApprove, nodeidx, 0);

                                setOntop.Focus();

                                break;

                        }
                        break;

                }
                setOntop.Focus();

                break;
            case "docReport":
                switch (_chk_tab)
                {
                    case 1://day
                        lbReportDay.Attributes.Add("class", "active");
                        _PanelReportDay.Visible = true;
                        break;
                    case 2:
                        lbReportMonth.Attributes.Add("class", "active");
                        _PanelReportMonth.Visible = true;
                        break;
                }
                break;
            case "docAddGroupOTDay":
                switch (_chk_tab)
                {
                    case 0: //show detail group ot
                        div_AddGroupOTDay.Visible = true;
                        btnAddGroupOTDay.Visible = true;
                        getDetailGroupOTDay();
                        break;

                    case 1: //create admint add ot group
                        PanelAddGroupOTDay.Visible = true;
                        break;
                    case 2://view detail

                        Div_BackToDetailGroupOTDay.Visible = true;
                        getViewDetailGroupOTDay(GvViewDetailGroupOTDay, uidx);

                        break;

                }

                break;
            case "docImportOTDay":
                switch (_chk_tab)
                {
                    case 0:

                        linkBtnTrigger(btnImportExcelOTDay);
                        _PanelImportOTDay.Visible = true;
                        getLocation(ddlplace, 0);

                        getOrganizationList(ddlorg);
                        getDepartmentList(ddlrdept, int.Parse(ddlorg.SelectedValue));
                        getSectionList(ddlrsec, int.Parse(ddlorg.SelectedValue), int.Parse(ddlrdept.SelectedValue));


                        getTypeDayWork(ddl_type_daywork);
                        getTypeOT(ddl_typeot);

                        break;
                }
                setOntop.Focus();
                break;
            case "docDetailImportOTDay":

                ViewState["Vs_SetHeader_Filter"] = 0;

                switch (_chk_tab)
                {
                    case 0:
                        _PanelDetailImportOTDay.Visible = true;
                        div_headdetail_otday.Visible = true;

                        getDetailEmployeeImportOTDay(GvDetailEmployeeImportOTDay, 1);
                        break;
                    case 1://view detail import ot day 

                        switch (nodeidx)
                        {
                            case 10:
                                _PanelDetailImportOTDay.Visible = true;
                                _PanelBackDetailImportOTDay.Visible = true;

                                getViewDetailImportDay(FvViewDetailEmployeeImportDay, uidx, 1); //detail emp create import
                                getViewDetailEmployeeImportDay(GvViewDetailEmployeeImportDay, uidx, 0); //detail emp create import

                                //litDebug.Text = nodeidx.ToString();
                                //log detail
                                divLogDetailEmployeeImportDay.Visible = true;
                                getLogDetailImportOTDay(rptLogDetailEmployeeImportDay, uidx);


                                ViewState["Vs_SetHeader_Filter"] = _chk_tab;
                                break;
                        }
                        break;
                }
                setOntop.Focus();
                break;
            case "docReportOTDay":

                switch (_chk_tab)
                {
                    case 0:
                        _PanelReportDay.Visible = true;
                        getLocation(ddlplace_report, 0);

                        getOrganizationList(ddlorg_report);
                        getDepartmentList(ddlrdept_report, int.Parse(ddlorg_report.SelectedValue));
                        getSectionList(ddlrsec_report, int.Parse(ddlorg_report.SelectedValue), int.Parse(ddlrdept_report.SelectedValue));

                        getTypeDayWork(ddl_type_daywork_report);
                        getTypeOT(ddl_typeot_report);

                        break;
                    case 1://view report detail

                        //_PanelBackDetailImportOTDay.Visible = true;
                        _PanelBackToDetailReportImportOTDay.Visible = true;
                        getViewDetailImportDay(FvReportViewDetailEmployeeImportDay, uidx, 1); //detail emp create import
                        //divReportGvViewDetailEmployeeImportDay.Visible = true;
                        getViewDetailEmployeeImportDay(GvReportEmployeeImportDay, uidx, 0); //detail emp create import

                        break;
                }
                setOntop.Focus();
                break;

            case "docReportOTMonth":
                _PanelReportOTMonth.Visible = true;

                ////string _otx1 = "x1";
                ////string _otx2 = "x1.5";
                ////string _otx3 = "x3";

                switch (_chk_tab)
                {

                    case 0:
                        getddlYear(ddlYearReport);
                        ddlMonthReport.ClearSelection();
                        ddlempMonthReport.ClearSelection();
                        txtCostcenterReport.Text = string.Empty;

                        getOrganizationList(ddlorgMonthReport);
                        getDepartmentList(ddlrdeptMonthReport, int.Parse(ddlorgMonthReport.SelectedValue));
                        getSectionList(ddlrsecMonthReport, int.Parse(ddlorgMonthReport.SelectedValue), int.Parse(ddlrdeptMonthReport.SelectedValue));

                        //getEmpList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)

                        break;


                    case 1: //ot x1

                        data_overtime data_search_reportmonth = new data_overtime();
                        ovt_report_otmonth_detail search_reportmonth_detail = new ovt_report_otmonth_detail();
                        data_search_reportmonth.ovt_report_otmonth_list = new ovt_report_otmonth_detail[1];

                        search_reportmonth_detail.cemp_idx = _emp_idx;
                        search_reportmonth_detail.month_idx = int.Parse(ddlMonthReport.SelectedValue);
                        search_reportmonth_detail.year_idx = int.Parse(ddlYearReport.SelectedValue);
                        search_reportmonth_detail.condition = 1;
                        search_reportmonth_detail.condition_ot = _otx1;
                        search_reportmonth_detail.org_idx = int.Parse(ddlorgMonthReport.SelectedValue);
                        search_reportmonth_detail.rdept_idx = int.Parse(ddlrdeptMonthReport.SelectedValue);
                        search_reportmonth_detail.rsec_idx = int.Parse(ddlrsecMonthReport.SelectedValue);
                        search_reportmonth_detail.emp_idx = int.Parse(ddlempMonthReport.SelectedValue);
                        search_reportmonth_detail.costcenter = txtCostcenterReport.Text;


                        data_search_reportmonth.ovt_report_otmonth_list[0] = search_reportmonth_detail;

                        //data_search_month = callServicePostOvertime(_urlGetDateScanOTMont, data_search_month);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth));
                        data_search_reportmonth = callServicePostOvertime(_urlGetReportOTMonth, data_search_reportmonth);

                        if (data_search_reportmonth.return_code == 0)
                        {
                            ViewState["Vs_ReportOTMonth"] = data_search_reportmonth.ovt_report_otmonth_list;
                            _PanelExportExcelReportMonth.Visible = true;
                            GvReportOTMonth.Visible = true;
                            setGridData(GvReportOTMonth, ViewState["Vs_ReportOTMonth"]);
                        }
                        else
                        {
                            ViewState["Vs_ReportOTMonth"] = null;

                            GvReportOTMonth.Visible = true;
                            setGridData(GvReportOTMonth, ViewState["Vs_ReportOTMonth"]);
                        }

                        break;
                    case 2: //ot x1.5

                        data_overtime data_search_reportmonth2 = new data_overtime();
                        ovt_report_otmonth_detail search_reportmonth_detail2 = new ovt_report_otmonth_detail();
                        data_search_reportmonth2.ovt_report_otmonth_list = new ovt_report_otmonth_detail[1];

                        search_reportmonth_detail2.cemp_idx = _emp_idx;
                        search_reportmonth_detail2.month_idx = int.Parse(ddlMonthReport.SelectedValue);
                        search_reportmonth_detail2.year_idx = int.Parse(ddlYearReport.SelectedValue);
                        search_reportmonth_detail2.condition = 2;
                        search_reportmonth_detail2.condition_ot = _otx2;
                        search_reportmonth_detail2.org_idx = int.Parse(ddlorgMonthReport.SelectedValue);
                        search_reportmonth_detail2.rdept_idx = int.Parse(ddlrdeptMonthReport.SelectedValue);
                        search_reportmonth_detail2.rsec_idx = int.Parse(ddlrsecMonthReport.SelectedValue);
                        search_reportmonth_detail2.emp_idx = int.Parse(ddlempMonthReport.SelectedValue);
                        search_reportmonth_detail2.costcenter = txtCostcenterReport.Text;


                        data_search_reportmonth2.ovt_report_otmonth_list[0] = search_reportmonth_detail2;

                        //data_search_month = callServicePostOvertime(_urlGetDateScanOTMont, data_search_month);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth));
                        data_search_reportmonth2 = callServicePostOvertime(_urlGetReportOTMonth, data_search_reportmonth2);

                        if (data_search_reportmonth2.return_code == 0)
                        {
                            ViewState["Vs_ReportOTMonth"] = data_search_reportmonth2.ovt_report_otmonth_list;
                            _PanelExportExcelReportMonth.Visible = true;
                            GvReportOTMonth.Visible = true;
                            setGridData(GvReportOTMonth, ViewState["Vs_ReportOTMonth"]);
                        }
                        else
                        {
                            ViewState["Vs_ReportOTMonth"] = null;

                            GvReportOTMonth.Visible = true;
                            setGridData(GvReportOTMonth, ViewState["Vs_ReportOTMonth"]);
                        }


                        break;
                    case 3: //ot x3

                        data_overtime data_search_reportmonth3 = new data_overtime();
                        ovt_report_otmonth_detail search_reportmonth_detail3 = new ovt_report_otmonth_detail();
                        data_search_reportmonth3.ovt_report_otmonth_list = new ovt_report_otmonth_detail[1];

                        search_reportmonth_detail3.cemp_idx = _emp_idx;
                        search_reportmonth_detail3.month_idx = int.Parse(ddlMonthReport.SelectedValue);
                        search_reportmonth_detail3.year_idx = int.Parse(ddlYearReport.SelectedValue);
                        search_reportmonth_detail3.condition = 3;
                        search_reportmonth_detail3.condition_ot = _otx3;
                        search_reportmonth_detail3.org_idx = int.Parse(ddlorgMonthReport.SelectedValue);
                        search_reportmonth_detail3.rdept_idx = int.Parse(ddlrdeptMonthReport.SelectedValue);
                        search_reportmonth_detail3.rsec_idx = int.Parse(ddlrsecMonthReport.SelectedValue);
                        search_reportmonth_detail3.emp_idx = int.Parse(ddlempMonthReport.SelectedValue);
                        search_reportmonth_detail3.costcenter = txtCostcenterReport.Text;


                        data_search_reportmonth3.ovt_report_otmonth_list[0] = search_reportmonth_detail3;

                        //data_search_month = callServicePostOvertime(_urlGetDateScanOTMont, data_search_month);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth));
                        data_search_reportmonth3 = callServicePostOvertime(_urlGetReportOTMonth, data_search_reportmonth3);

                        if (data_search_reportmonth3.return_code == 0)
                        {
                            ViewState["Vs_ReportOTMonth"] = data_search_reportmonth3.ovt_report_otmonth_list;
                            _PanelExportExcelReportMonth.Visible = true;
                            GvReportOTMonth.Visible = true;
                            setGridData(GvReportOTMonth, ViewState["Vs_ReportOTMonth"]);
                        }
                        else
                        {
                            ViewState["Vs_ReportOTMonth"] = null;

                            GvReportOTMonth.Visible = true;
                            setGridData(GvReportOTMonth, ViewState["Vs_ReportOTMonth"]);
                        }


                        break;
                }
                break;

            case "docDetailAdminOTDay":

                switch (_chk_tab)
                {
                    case 1: // detail admin
                        _PanelDetailAdminOTDay.Visible = true;
                        liDetailAdminOTDay.Attributes.Add("class", "active");
                        switch (u1idx)
                        {
                            case 0:
                                data_overtime data_u0doc_otday = new data_overtime();
                                ovt_u0doc_otday_detail u0doc_otday_detail = new ovt_u0doc_otday_detail();
                                data_u0doc_otday.ovt_u0doc_otday_list = new ovt_u0doc_otday_detail[1];

                                u0doc_otday_detail.cemp_idx = _emp_idx;
                                u0doc_otday_detail.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());

                                data_u0doc_otday.ovt_u0doc_otday_list[0] = u0doc_otday_detail;

                                //data_search_month = callServicePostOvertime(_urlGetDateScanOTMont, data_search_month);
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth));
                                data_u0doc_otday = callServicePostOvertime(_urlGetDetailAdminCreateOTDay, data_u0doc_otday);


                                ViewState["Vs_DetailAdminOTDay"] = data_u0doc_otday.ovt_u0doc_otday_list;
                                _divheader_empshiftadmin.Visible = true;


                                GvDetailAdminOTDay.Visible = true;
                                setGridData(GvDetailAdminOTDay, ViewState["Vs_DetailAdminOTDay"]);


                                break;
                            case 1:


                                _PanelGvViewDetailAdminOTDay.Visible = true;
                                _PnDetailBackToDetailAdmin.Visible = true;

                                getDetailOTDayToEmployee(FvDetailAdminOTDay, uidx);


                                //bind detail emp tp gv
                                data_overtime data_u1doc_otday_viewgv = new data_overtime();
                                ovt_u1doc_otday_detail u1doc_otday_detail_viewgv = new ovt_u1doc_otday_detail();
                                data_u1doc_otday_viewgv.ovt_u1doc_otday_list = new ovt_u1doc_otday_detail[1];

                                u1doc_otday_detail_viewgv.u0_docday_idx = uidx;

                                data_u1doc_otday_viewgv.ovt_u1doc_otday_list[0] = u1doc_otday_detail_viewgv;

                                //data_search_month = callServicePostOvertime(_urlGetDateScanOTMont, data_search_month);
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1doc_otday_viewgv));
                                data_u1doc_otday_viewgv = callServicePostOvertime(_urlGetViewDetailGvAdminOT, data_u1doc_otday_viewgv);

                                ViewState["Vs_ViewDetailGvAdminOTDay"] = data_u1doc_otday_viewgv.ovt_u1doc_otday_list;
                                GvViewDetailAdminOTDay.Visible = true;
                                setGridData(GvViewDetailAdminOTDay, ViewState["Vs_ViewDetailGvAdminOTDay"]);


                                break;
                        }

                        break;

                    case 2:

                        _PndocDetailEmployeeOTDay.Visible = true;

                        liDetailEmployeeOTDay.Attributes.Add("class", "active");
                        switch (staidx)
                        {
                            case 0:
                                data_overtime data_u0doc_otday = new data_overtime();
                                ovt_u1doc_otday_detail u0doc_otday_detail = new ovt_u1doc_otday_detail();
                                data_u0doc_otday.ovt_u1doc_otday_list = new ovt_u1doc_otday_detail[1];

                                u0doc_otday_detail.cemp_idx = _emp_idx;
                                u0doc_otday_detail.emp_idx = _emp_idx;
                                u0doc_otday_detail.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());

                                data_u0doc_otday.ovt_u1doc_otday_list[0] = u0doc_otday_detail;

                                //data_search_month = callServicePostOvertime(_urlGetDateScanOTMont, data_search_month);
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth));
                                data_u0doc_otday = callServicePostOvertime(_urlGetDetailToEmployeeOTDay, data_u0doc_otday);


                                ViewState["Vs_GvDetailToEmployeeCreateAdmin"] = data_u0doc_otday.ovt_u1doc_otday_list;
                                _divheader_empshiftempployee.Visible = true;
                                GvDetailToEmployeeCreateAdmin.Visible = true;
                                setGridData(GvDetailToEmployeeCreateAdmin, ViewState["Vs_GvDetailToEmployeeCreateAdmin"]);

                                break;

                            case 1:

                                _PanelDetailOTDayToEmployee.Visible = true;
                                _PnBackToDetailEmployee.Visible = true;

                                getDetailOTDayToEmployee(FvDetailOTDayToEmployee, uidx);

                                data_overtime data_u0doc_otday_emp = new data_overtime();
                                ovt_u1doc_otday_detail u0doc_otday_detail_emp = new ovt_u1doc_otday_detail();
                                data_u0doc_otday_emp.ovt_u1doc_otday_list = new ovt_u1doc_otday_detail[1];

                                u0doc_otday_detail_emp.u0_docday_idx = uidx;
                                u0doc_otday_detail_emp.u1_docday_idx = u1idx;
                                u0doc_otday_detail_emp.emp_idx = _emp_idx;
                                u0doc_otday_detail_emp.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());

                                data_u0doc_otday_emp.ovt_u1doc_otday_list[0] = u0doc_otday_detail_emp;

                                //data_search_month = callServicePostOvertime(_urlGetDateScanOTMont, data_search_month);
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth));
                                data_u0doc_otday_emp = callServicePostOvertime(_urlGetViewDetailToEmployeeOTDay, data_u0doc_otday_emp);


                                ViewState["Vs_DetailToEmployeeOTDay"] = data_u0doc_otday_emp.ovt_u1doc_otday_list;
                                GvDetailOTDayToEmployee.Visible = true;
                                setGridData(GvDetailOTDayToEmployee, ViewState["Vs_DetailToEmployeeOTDay"]);


                                div_LogViewDetailDayAdminCreate.Visible = true;
                                getLogDetailOTDayAdminCreate(rptLogViewDetailDayAdminCreate, uidx, u1idx);

                                break;
                        }

                        break;
                }
                break;
            case "docDetailEmployeeOTDay":

                switch (_chk_tab)
                {
                    case 0:



                        break;
                    case 1:


                        break;
                }
                break;
            case "docApproveOTDayADminCreate":

                //var check_tab_noidx = "";

                ViewState["Vs_check_tab_noidx"] = 0;


                switch (_chk_tab)
                {
                    case 0:
                        _PanelGvWaitApproveOTDayAdminCrate.Visible = true;

                        switch (nodeidx)
                        {
                            case 12: //head user 1/2 approve

                                liWaitApproveHeadAdminCreate.Attributes.Add("class", "active");
                                ViewState["Vs_check_tab_noidx"] = nodeidx.ToString();
                                divheadapprove_otday.Visible = true;

                                getWaitApproveDetailOTDayAdminCreate(GvWaitApproveOTDayAdminCrate, nodeidx, 1);

                                if (ViewState["Vs_WaitApproveDetailOTDayAdminCreate"] != null)
                                {
                                    div_btnApproveHeadAdmincreate.Visible = true;
                                    Repeater rptBindbtnApproveOtDayAdmin = (Repeater)_PanelGvWaitApproveOTDayAdminCrate.FindControl("rptBindbtnApproveOtDayAdmin");
                                    getbtnDecisionApprove(rptBindbtnApproveOtDayAdmin, nodeidx, 0);

                                }


                                break;
                            case 13: //hr approve 

                                liWaitApproveHRAdminCreate.Attributes.Add("class", "active");

                                ViewState["Vs_check_tab_noidx"] = nodeidx.ToString();
                                divhrapprove_otday.Visible = true;
                                getWaitApproveDetailOTDayAdminCreate(GvWaitApproveOTDayAdminCrate, nodeidx, 2);

                                if (ViewState["Vs_WaitApproveDetailOTDayAdminCreate"] != null)
                                {
                                    div_btnApproveHeadAdmincreate.Visible = true;
                                    Repeater rptBindbtnApproveOtDayAdmin = (Repeater)_PanelGvWaitApproveOTDayAdminCrate.FindControl("rptBindbtnApproveOtDayAdmin");
                                    getbtnDecisionApprove(rptBindbtnApproveOtDayAdmin, nodeidx, 0);

                                }

                                break;
                            case 11: // hr edit ot 

                                liWaitApproveAdminEdit.Attributes.Add("class", "active");
                                ViewState["Vs_check_tab_noidx"] = nodeidx.ToString();
                                divadminapprove_otday.Visible = true;
                                getWaitApproveDetailOTDayAdminCreate(GvWaitApproveOTDayAdminCrate, nodeidx, 3);


                                if (ViewState["Vs_WaitApproveDetailOTDayAdminCreate"] != null)
                                {
                                    div_btnApproveAdminEditOTDay.Visible = true;

                                }
                                break;
                        }
                        break;
                }
                setOntop.Focus();
                break;
        }
    }

    protected void setActiveTab(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cemp_idx, int u1idx)
    {
        setActiveView(activeTab, uidx, nodeidx, staidx, actor_idx, _chk_tab, _cemp_idx, u1idx);
        switch (activeTab)
        {
            case "docDetail":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");
                li7.Attributes.Add("class", "");
                li8.Attributes.Add("class", "");
                li9.Attributes.Add("class", "");
                li10.Attributes.Add("class", "");
                //li11.Attributes.Add("class", "");
                li12.Attributes.Add("class", "");
                break;
            case "docCreate":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");
                li7.Attributes.Add("class", "");
                li8.Attributes.Add("class", "");
                li9.Attributes.Add("class", "");
                li10.Attributes.Add("class", "");
                //li11.Attributes.Add("class", "");
                li12.Attributes.Add("class", "");
                break;
            case "docSetOTMonth":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");
                li7.Attributes.Add("class", "");
                li8.Attributes.Add("class", "");
                li9.Attributes.Add("class", "");
                li10.Attributes.Add("class", "");
                //li11.Attributes.Add("class", "");
                li12.Attributes.Add("class", "");
                break;
            case "docWaitApprove":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");
                li7.Attributes.Add("class", "");
                li8.Attributes.Add("class", "");
                li9.Attributes.Add("class", "");
                li10.Attributes.Add("class", "");
                //li11.Attributes.Add("class", "");
                li12.Attributes.Add("class", "");
                break;
            case "docReport":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "active");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");
                li7.Attributes.Add("class", "");
                li8.Attributes.Add("class", "");
                li9.Attributes.Add("class", "");
                li10.Attributes.Add("class", "");
                //li11.Attributes.Add("class", "");
                li12.Attributes.Add("class", "");
                break;
            case "docAddGroupOTDay":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "active");
                li6.Attributes.Add("class", "");
                li7.Attributes.Add("class", "");
                li8.Attributes.Add("class", "");
                li9.Attributes.Add("class", "");
                li10.Attributes.Add("class", "");
                //li11.Attributes.Add("class", "");
                li12.Attributes.Add("class", "");
                break;
            case "docImportOTDay":
                linkBtnTrigger(btnImportExcelOTDay);

                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "active");
                li7.Attributes.Add("class", "");
                li8.Attributes.Add("class", "");
                li9.Attributes.Add("class", "");
                li10.Attributes.Add("class", "");
                //li11.Attributes.Add("class", "");
                li12.Attributes.Add("class", "");
                break;
            case "docDetailImportOTDay":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");
                li7.Attributes.Add("class", "active");
                li8.Attributes.Add("class", "");
                li9.Attributes.Add("class", "");
                li10.Attributes.Add("class", "");
                //li11.Attributes.Add("class", "");
                li12.Attributes.Add("class", "");
                break;
            case "docReportOTDay":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");
                li7.Attributes.Add("class", "");
                li8.Attributes.Add("class", "active");
                li9.Attributes.Add("class", "");
                li10.Attributes.Add("class", "");
                //li11.Attributes.Add("class", "");
                li12.Attributes.Add("class", "");

                break;
            case "docReportOTMonth":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");
                li7.Attributes.Add("class", "");
                li8.Attributes.Add("class", "");
                li9.Attributes.Add("class", "active");
                li10.Attributes.Add("class", "");
                //li11.Attributes.Add("class", "");
                li12.Attributes.Add("class", "");

                break;
            case "docDetailAdminOTDay":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");
                li7.Attributes.Add("class", "");
                li8.Attributes.Add("class", "");
                li9.Attributes.Add("class", "");
                li10.Attributes.Add("class", "active");
                //li11.Attributes.Add("class", "");
                li12.Attributes.Add("class", "");

                break;
            case "docDetailEmployeeOTDay":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");
                li7.Attributes.Add("class", "");
                li8.Attributes.Add("class", "");
                li9.Attributes.Add("class", "");
                li10.Attributes.Add("class", "");
                // li11.Attributes.Add("class", "active");
                li12.Attributes.Add("class", "");

                break;
            case "docApproveOTDayADminCreate":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");
                li7.Attributes.Add("class", "");
                li8.Attributes.Add("class", "");
                li9.Attributes.Add("class", "");
                li10.Attributes.Add("class", "");
                //li11.Attributes.Add("class", "");
                li12.Attributes.Add("class", "active");

                break;

        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {

            case "rptBindbtnApprove":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Repeater rptBindbtnApprove = (Repeater)FvHeadUserApprove.FindControl("rptBindbtnApprove");
                    var chk_coler = (Label)e.Item.FindControl("lbcheck_coler");
                    var btnHeadUserSaveApprove = (LinkButton)e.Item.FindControl("btnHeadUserSaveApprove");

                    for (int k = 0; k <= rptBindbtnApprove.Items.Count; k++)
                    {
                        btnHeadUserSaveApprove.CssClass = ConfigureColors(k);
                        //Console.WriteLine(i);
                    }


                }
                break;
            case "rptBindbtnApproveOtDayAdmin":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Repeater rptBindbtnApproveOtDayAdmin = (Repeater)_PanelGvWaitApproveOTDayAdminCrate.FindControl("rptBindbtnApproveOtDayAdmin");
                    var chk_coler = (Label)e.Item.FindControl("lbcheck_colerotday_admincreate");
                    var btnHeadUserApproveOTDayAdminCreate = (LinkButton)e.Item.FindControl("btnHeadUserApproveOTDayAdminCreate");

                    for (int k = 0; k <= rptBindbtnApproveOtDayAdmin.Items.Count; k++)
                    {
                        btnHeadUserApproveOTDayAdminCreate.CssClass = ConfigureColors(k);
                        //Console.WriteLine(i);
                    }


                }
                break;
            case "rptBindHRbtnApprove":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Repeater rptBindHRbtnApprove = (Repeater)FvHrApproveOTMonth.FindControl("rptBindHRbtnApprove");
                    var chk_coler = (Label)e.Item.FindControl("lb_hrcheck_coler");
                    var btnHRSaveApprove = (LinkButton)e.Item.FindControl("btnHRSaveApprove");

                    for (int k = 0; k <= rptBindHRbtnApprove.Items.Count; k++)
                    {
                        btnHRSaveApprove.CssClass = ConfigureColors(k);
                        //Console.WriteLine(i);
                    }


                }
                break;
            case "rptBindbtnApproveOtDay":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Repeater rptBindbtnApproveOtDay = (Repeater)FvHeadUserApproveOTDay.FindControl("rptBindbtnApproveOtDay");
                    var lbcheck_colerotday = (Label)e.Item.FindControl("lbcheck_colerotday");
                    var btnHeadUserSaveApproveOTDay = (LinkButton)e.Item.FindControl("btnHeadUserSaveApproveOTDay");

                    for (int k = 0; k <= rptBindbtnApproveOtDay.Items.Count; k++)
                    {
                        btnHeadUserSaveApproveOTDay.CssClass = ConfigureColorsOTDay(k);
                        //Console.WriteLine(i);
                    }


                }
                break;
            case "rptBindHRbtnApproveOTDay":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Repeater rptBindHRbtnApproveOTDay = (Repeater)FvHrApproveOTDay.FindControl("rptBindHRbtnApproveOTDay");
                    var lb_hrcheck_colerotday = (Label)e.Item.FindControl("lb_hrcheck_colerotday");
                    var btnHRSaveApproveOTDay = (LinkButton)e.Item.FindControl("btnHRSaveApproveOTDay");

                    for (int k = 0; k <= rptBindHRbtnApproveOTDay.Items.Count; k++)
                    {
                        btnHRSaveApproveOTDay.CssClass = ConfigureColorsOTDayHR(k);
                        //Console.WriteLine(i);
                    }


                }
                break;
        }
    }

    #endregion reuse

    #region ConfigureColors
    protected string ConfigureColors(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }

    protected string ConfigureColorsOTDay(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }

    protected string ConfigureColorsOTDayHR(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }
    #endregion ConfigureColors

    #region data excel

    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }

        //set merge cell
        ////int max_row = dt.Rows.Count;


        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);

            //set merge cell
            ////var cra = new NPOI.SS.Util.CellRangeAddress(1, max_row, 0, 0);
            ////sheet1.AddMergedRegion(cra);

            //var cra1 = new NPOI.SS.Util.CellRangeAddress(1, max_row, 1, 1);
            //sheet1.AddMergedRegion(cra1);


            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }

    ////protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName, int _set_value)
    ////{
    ////    IWorkbook workbook;


    ////    if (extension == "xlsx")
    ////    {
    ////        workbook = new XSSFWorkbook();



    ////    }
    ////    else if (extension == "xls")
    ////    {
    ////        workbook = new HSSFWorkbook();


    ////    }
    ////    else
    ////    {
    ////        throw new Exception("This format is not supported");
    ////    }

    ////    ISheet sheet1 = workbook.CreateSheet("Sheet 1");
    ////    IRow row1 = sheet1.CreateRow(0);

    ////    ICellStyle testeStyle = workbook.CreateCellStyle();
    ////    testeStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Medium;
    ////    testeStyle.FillForegroundColor = IndexedColors.BrightGreen.Index;
    ////    testeStyle.FillPattern = FillPattern.SolidForeground;

    ////    //Create a Title row
    ////    //var titleFont = workbook.CreateFont();
    ////    //titleFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
    ////    //titleFont.FontHeightInPoints = 11;
    ////    //titleFont.Underline = NPOI.SS.UserModel.FontUnderlineType.Single;

    ////    //var titleStyle = workbook.CreateCellStyle();
    ////    //titleStyle.SetFont(titleFont);

    ////    //HSSFCellStyle Style = workbook.CreateCellStyle() as HSSFCellStyle;

    ////    for (int j = 0; j < dt.Columns.Count; j++)
    ////    {
    ////        ICell cell = row1.CreateCell(j);
    ////        String columnName = dt.Columns[j].ToString();
    ////        cell.SetCellValue(columnName);
    ////    }

    ////    //set merge cell
    ////    int max_row = dt.Rows.Count - 1;

    ////    for (int i = 0; i < dt.Rows.Count; i++)
    ////    {


    ////        IRow row = sheet1.CreateRow(i + 1);

    ////        //set merge cell
    ////        var cra = new NPOI.SS.Util.CellRangeAddress(1, max_row, 0, 0);
    ////        sheet1.AddMergedRegion(cra);

    ////        var cra1 = new NPOI.SS.Util.CellRangeAddress(1, max_row, 1, 1);
    ////        sheet1.AddMergedRegion(cra1);

    ////        for (int j = 0; j < dt.Columns.Count; j++)
    ////        {


    ////            ICell cell = row.CreateCell(j);
    ////            String columnName = dt.Columns[j].ToString();
    ////            cell.SetCellValue(dt.Rows[i][columnName].ToString());

    ////            //Style.Style.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
    ////            //Style.VerticalAlignment = VerticalAlignment.Center;
    ////            //Style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
    ////            //set font
    ////            //cell.CellStyle = titleStyle;

    ////            //set export color total expenses green
    ////            if (_set_value == 1)
    ////            {
    ////                //if ((i + 1) == dt.Rows.Count && (dt.Rows[i][columnName].ToString() == "Total :" || ((i + 1) == dt.Rows.Count && (j + 1) == dt.Columns.Count)))
    ////                if ((i + 1) == dt.Rows.Count && (dt.Rows[i][columnName].ToString() == "Total :")
    ////                    || ((i + 1) == dt.Rows.Count && (j + 1) == dt.Columns.Count)
    ////                    || ((i + 1) == dt.Rows.Count && (j == 5))
    ////                    )
    ////                {
    ////                    cell.CellStyle = testeStyle;
    ////                    //cell.CellStyle.WrapText = true;
    ////                }


    ////            }

    ////        }

    ////    }





    ////    using (var exportData = new MemoryStream())
    ////    {
    ////        Response.Clear();
    ////        workbook.Write(exportData);

    ////        if (extension == "xlsx")
    ////        {
    ////            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    ////            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));

    ////            Response.BinaryWrite(exportData.ToArray());
    ////        }
    ////        else if (extension == "xls")
    ////        {
    ////            Response.ContentType = "application/vnd.ms-excel";
    ////            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
    ////            Response.BinaryWrite(exportData.GetBuffer());
    ////        }
    ////        Response.End();
    ////    }


    ////}




    #endregion data excel



}