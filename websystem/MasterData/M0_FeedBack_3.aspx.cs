﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_masterdata_M0_FeedBack_3 : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_FeedBack _data_FeedBack = new data_FeedBack();
    

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetm0FeedBack3 = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0FeedBack3"];
    static string _urlSetm0FeedBack3 = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsm0FeedBack3"];
    static string _urlSetUpdm0FeedBack3 = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdm0FeedBack3"];
    static string _urlDeletem0FeedBack3 = _serviceUrl + ConfigurationManager.AppSettings["urlDeletem0FeedBack3"];



    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
    }

    #region selected   
    protected void actionIndex()
    {

        data_FeedBack data_FeedBack = new data_FeedBack();
        data_FeedBack.M0_FeedBack_3_list = new M0_FeedBack_3_detail[1];

        M0_FeedBack_3_detail _M0_FeedBack_3_detailindex = new M0_FeedBack_3_detail();

        _M0_FeedBack_3_detailindex.MFBIDX3 = 0;

        data_FeedBack.M0_FeedBack_3_list[0] = _M0_FeedBack_3_detailindex;
        //litDebug.Text = _funcTool.convertObjectToXml(data_FeedBack);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_FeedBack));
        data_FeedBack = callServiceNetwork(_urlGetm0FeedBack3, data_FeedBack);

         setGridData(GvMaster, data_FeedBack.M0_FeedBack_3_list);

    }
    #endregion selected 

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _MFBIDX3;
        string _txtQuestion;
        int _cemp_idx;

        M0_FeedBack_3_detail objM0_FeedBack_3 = new M0_FeedBack_3_detail();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":
                _txtQuestion = ((TextBox)ViewInsert.FindControl("txtQuestion")).Text.Trim();
                DropDownList _ddlMFB3Status = (DropDownList)ViewInsert.FindControl("ddlMFB3Status");
                _cemp_idx = emp_idx;
                M0_FeedBack_3_detail obj = new M0_FeedBack_3_detail();
                _data_FeedBack.M0_FeedBack_3_list = new M0_FeedBack_3_detail[1];
                obj.MFBIDX3 = 0;//_type_idx; 
                obj.Question = _txtQuestion;
                obj.MFB3Status = int.Parse(_ddlMFB3Status.SelectedValue);
                obj.CEmpIDX = _cemp_idx;

                _data_FeedBack.M0_FeedBack_3_list[0] = obj;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_FeedBack));
               
                _data_FeedBack = callServiceNetwork(_urlSetm0FeedBack3, _data_FeedBack);

                if (_data_FeedBack.return_code == 0)
                {
                    actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_FeedBack.return_code.ToString() + " - " + _data_FeedBack.return_msg);
                }

                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnDelete":

                _MFBIDX3 = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_FeedBack.M0_FeedBack_3_list = new M0_FeedBack_3_detail[1];
                objM0_FeedBack_3.MFBIDX3 = _MFBIDX3;
                objM0_FeedBack_3.CEmpIDX = _cemp_idx;

                _data_FeedBack.M0_FeedBack_3_list[0] = objM0_FeedBack_3;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_FeedBack));

                _data_FeedBack = callServiceNetwork(_urlDeletem0FeedBack3, _data_FeedBack);


                if (_data_FeedBack.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_FeedBack.return_code.ToString() + " - " + _data_FeedBack.return_msg);
                }

                break;




        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                        
                    }
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int MFBIDX3_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtQuestionUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtQuestionUpdate");
                var ddlMFB3StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlMFB3StatusUpdate");

                GvMaster.EditIndex = -1;

                _data_FeedBack.M0_FeedBack_3_list = new M0_FeedBack_3_detail[1];
                M0_FeedBack_3_detail _FeedBack_3_detail = new M0_FeedBack_3_detail();

                _FeedBack_3_detail.MFBIDX3 = MFBIDX3_update;
                _FeedBack_3_detail.Question = txtQuestionUpdate.Text;
                _FeedBack_3_detail.MFB3Status = int.Parse(ddlMFB3StatusUpdate.SelectedValue);
                _FeedBack_3_detail.CEmpIDX = emp_idx;

                _data_FeedBack.M0_FeedBack_3_list[0] = _FeedBack_3_detail;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_FeedBack));
                _data_FeedBack = callServiceNetwork(_urlSetUpdm0FeedBack3, _data_FeedBack);

                if (_data_FeedBack.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    actionIndex();


                }
                else
                {
                    setError(_data_FeedBack.return_code.ToString() + " - " + _data_FeedBack.return_msg);
                }


                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        //switch (fvName.ID)
        //{



        //}
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_FeedBack callServiceNetwork(string _cmdUrl, data_FeedBack _data_FeedBack)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_FeedBack);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_FeedBack = (data_FeedBack)_funcTool.convertJsonToObject(typeof(data_FeedBack), _localJson);

        return _data_FeedBack;
    }


    #endregion reuse

}