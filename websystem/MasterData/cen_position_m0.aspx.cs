﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.Data;
using System.Data.SqlClient;
using ASP;

public partial class websystem_MasterData_cen_position_m0 : System.Web.UI.Page
{

    function_tool _functionTool = new function_tool();
    data_cen_master _data_cen_master = new data_cen_master();
    data_employee _dataEmployee = new data_employee();
    string _localJson = string.Empty;
    string textstatus = string.Empty;
    int _emp_idx = 0;
    //string textstatus = string.Empty;
    //string msg = string.Empty;
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCenMasterList"];
    static string _urlSetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlSetCenMasterList"];


    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);
            DropDownList ddlSearchOrg = (DropDownList)FormViewS.FindControl("DropdownOrg_s");
            selectDdl(ddlSearchOrg, "3", "", "", "", "", "", "");

            DropDownList ddlSearchWg = (DropDownList)FormViewS.FindControl("DropdownWg_s");
            ddlSearchWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));

            DropDownList ddlSearchLw = (DropDownList)FormViewS.FindControl("DropdownLw_s");
            ddlSearchLw.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));

            DropDownList ddlSearchDept = (DropDownList)FormViewS.FindControl("DropdownDept_s");
            ddlSearchDept.Items.Insert(0, new ListItem("---เลือกฝ่าย---", ""));

            DropDownList ddlSearchSec = (DropDownList)FormViewS.FindControl("DropdownSec_s");
            ddlSearchSec.Items.Insert(0, new ListItem("---เลือกแผนก---", ""));

            DropDownList ddlSearchJobgrade = (DropDownList)FormViewS.FindControl("DropDownGrade_s");
            selectDdl(ddlSearchJobgrade, "9", "", "", "", "", "", "");

            FormViewS.Visible = true;
            ViewState["forSearch"] = null;
            ViewState["forWordSearch"] = null;
            showPosition();


        }
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {

        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        hlSetTotop.Focus();
        FvInsertEdit.Visible = false;
        gvPosition.Visible = false;
        lbCreate.Visible = false;
        FormViewS.Visible = false;
        switch (cmdName)
        {
            case "cmdCreate":
                FvInsertEdit.Visible = true;
                _functionTool.setFvData(FvInsertEdit, FormViewMode.Insert, null);
                DropDownList ddlInsertOrg = (DropDownList)FvInsertEdit.FindControl("DropdownOrg");
                selectDdl(ddlInsertOrg, "3", "", "", "", "", "","");

                DropDownList ddlInsertWg = (DropDownList)FvInsertEdit.FindControl("DropdownWg");
                ddlInsertWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));

                DropDownList ddlInsertLw = (DropDownList)FvInsertEdit.FindControl("DropdownLw");
                ddlInsertLw.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));

                DropDownList ddlInsertDept = (DropDownList)FvInsertEdit.FindControl("DropdownDept");
                ddlInsertDept.Items.Insert(0, new ListItem("---เลือกฝ่าย---", ""));

                DropDownList ddlInsertSec = (DropDownList)FvInsertEdit.FindControl("DropdownSec");
                ddlInsertSec.Items.Insert(0, new ListItem("---เลือกแผนก---", ""));

                DropDownList ddlInsertJobgrade = (DropDownList)FvInsertEdit.FindControl("DropDownGrade");
                selectDdl(ddlInsertJobgrade, "9", "", "", "", "", "", "");
                break;

            case "cmdSave":

                InsertPosition(_functionTool.convertToInt(cmdArg));
                break;

            case "cmdEdit":

                FvInsertEdit.Visible = true;
                _data_cen_master.master_mode = "8";
                _search_cen_master_detail.s_org_idx = "";
                _search_cen_master_detail.s_wg_idx = "";
                _search_cen_master_detail.s_lw_idx = "";
                _search_cen_master_detail.s_dept_idx = "";
                _search_cen_master_detail.s_sec_idx = "";
                _search_cen_master_detail.s_pos_idx = cmdArg;

                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

                _functionTool.setFvData(FvInsertEdit, FormViewMode.Edit, _data_cen_master.cen_pos_list_m0);

                DropDownList ddlEditorg = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");
                selectDdl(ddlEditorg, "3", "", "", "", "", "","");//select
                ddlEditorg.SelectedValue = _data_cen_master.cen_pos_list_m0[0].org_idx.ToString();//choose

                DropDownList ddlEditWg = (DropDownList)FvInsertEdit.FindControl("DropDownWg");
                selectDdl(ddlEditWg, "4", _data_cen_master.cen_pos_list_m0[0].org_idx.ToString(), "", "", "", "", "");
                ddlEditWg.SelectedValue = _data_cen_master.cen_pos_list_m0[0].wg_idx.ToString();

                DropDownList ddlEditLw = (DropDownList)FvInsertEdit.FindControl("DropDownLw");
                selectDdl(ddlEditLw, "5", "", _data_cen_master.cen_pos_list_m0[0].wg_idx.ToString(), "", "", "", "");
                ddlEditLw.SelectedValue = _data_cen_master.cen_pos_list_m0[0].lw_idx.ToString();

                DropDownList ddlEditDept = (DropDownList)FvInsertEdit.FindControl("DropDownDept");
                selectDdl(ddlEditDept, "6", "", "", _data_cen_master.cen_pos_list_m0[0].lw_idx.ToString(), "", "", "");
                ddlEditDept.SelectedValue = _data_cen_master.cen_pos_list_m0[0].dept_idx.ToString();

                DropDownList ddlEditSec = (DropDownList)FvInsertEdit.FindControl("DropDownSec");
                selectDdl(ddlEditSec, "7", "", "", "", _data_cen_master.cen_pos_list_m0[0].dept_idx.ToString(), "", "");
                ddlEditSec.SelectedValue = _data_cen_master.cen_pos_list_m0[0].sec_idx.ToString();

                DropDownList ddlEditjobgrade = (DropDownList)FvInsertEdit.FindControl("DropDownGrade");
                selectDdl(ddlEditjobgrade, "9", "", "", "", "", "", "");
                ddlEditjobgrade.SelectedValue = _data_cen_master.cen_pos_list_m0[0].jobgrade_idx.ToString();


                FvInsertEdit.Visible = true;


                break;

            case "editSave":

                InsertPosition(_functionTool.convertToInt(cmdArg));

                break;

            case "cmdDelete":

                cmdDelete(int.Parse(cmdArg));


                break;

            case "cmdCancel":
                gvPosition.Visible = true;
                lbCreate.Visible = true;
                FormViewS.Visible = true;
                if (ViewState["forSearch"] == null)
                {
                    showPosition();
                }
                else
                {
                    _functionTool.setGvData(gvPosition, ((data_cen_master)ViewState["forSearch"]).cen_pos_list_m0);

                }
                break;

            case "cmdSearch":

                checkBoxSearch();
                lbCreate.Visible = true;
                gvPosition.Visible = true;
                FormViewS.Visible = true;
                break;

            case "cmdReset":
                lbCreate.Visible = true;
                gvPosition.Visible = true;
                FormViewS.Visible = true;
                _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);
                DropDownList ddlSearchOrg = (DropDownList)FormViewS.FindControl("DropdownOrg_s");
                selectDdl(ddlSearchOrg, "3", "", "", "", "", "", "");

                DropDownList ddlSearchWg = (DropDownList)FormViewS.FindControl("DropdownWg_s");
                ddlSearchWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));

                DropDownList ddlSearchLw = (DropDownList)FormViewS.FindControl("DropdownLw_s");
                ddlSearchLw.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));

                DropDownList ddlSearchDept = (DropDownList)FormViewS.FindControl("DropdownDept_s");
                ddlSearchDept.Items.Insert(0, new ListItem("---เลือกฝ่าย---", ""));

                DropDownList ddlSearchSec = (DropDownList)FormViewS.FindControl("DropdownSec_s");
                ddlSearchSec.Items.Insert(0, new ListItem("---เลือกแผนก---", ""));

                DropDownList ddlSearchJobgrade = (DropDownList)FormViewS.FindControl("DropDownGrade_s");
                selectDdl(ddlSearchJobgrade, "9", "", "", "", "", "", "");

                ViewState["forSearch"] = null;
                ViewState["forWordSearch"] = null;
                showPosition();
                break;
        }
    }

    protected void showPosition()
    {


        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();


        if (ViewState["forSearch"] == null)
        {

            _search_cen_master_detail.s_org_idx = "";
            _search_cen_master_detail.s_wg_idx = "";
            _search_cen_master_detail.s_lw_idx = "";
            _search_cen_master_detail.s_dept_idx = "";
            _search_cen_master_detail.s_sec_idx = "";
            _search_cen_master_detail.s_pos_idx = "";
            _data_cen_master.master_mode = "8";

            _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
            _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;

            _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);

            _functionTool.setGvData(gvPosition, _data_cen_master.cen_pos_list_m0);

        }
        else
        {
            _functionTool.setGvData(gvPosition, ((data_cen_master)ViewState["forSearch"]).cen_pos_list_m0);
        }


    }


    protected void InsertPosition(int id)
    {
        TextBox tex_TH_name = (TextBox)FvInsertEdit.FindControl("tb_posnameth");
        TextBox tex_EN_name = (TextBox)FvInsertEdit.FindControl("tb_posnameen");
        TextBox manpower = (TextBox)FvInsertEdit.FindControl("tbmanpower");
        DropDownList dropD_jobgrade = (DropDownList)FvInsertEdit.FindControl("DropDownGrade");

        DropDownList dropD_status = (DropDownList)FvInsertEdit.FindControl("ddStatus");
        DropDownList dropD_Org = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");
        DropDownList dropD_Wg = (DropDownList)FvInsertEdit.FindControl("DropDownWg");
        DropDownList dropD_Lw = (DropDownList)FvInsertEdit.FindControl("DropDownLw");
        DropDownList dropD_Dept = (DropDownList)FvInsertEdit.FindControl("DropDownDept");
        DropDownList dropD_Sec = (DropDownList)FvInsertEdit.FindControl("DropDownSec");



        data_cen_master _data_cen_master = new data_cen_master();
        cen_pos_detail_m0 _cen_pos_detail_m0 = new cen_pos_detail_m0();

        _data_cen_master.master_mode = "8";
        _cen_pos_detail_m0.pos_idx = id;
        _cen_pos_detail_m0.pos_name_th = tex_TH_name.Text.Trim();
        _cen_pos_detail_m0.pos_name_en = tex_EN_name.Text.Trim();
        _cen_pos_detail_m0.pos_status = _functionTool.convertToInt(dropD_status.SelectedValue);
        _cen_pos_detail_m0.org_idx = _functionTool.convertToInt(dropD_Org.SelectedValue);
        _cen_pos_detail_m0.wg_idx = _functionTool.convertToInt(dropD_Wg.SelectedValue);
        _cen_pos_detail_m0.lw_idx = _functionTool.convertToInt(dropD_Lw.SelectedValue);
        _cen_pos_detail_m0.dept_idx = _functionTool.convertToInt(dropD_Dept.SelectedValue);
        _cen_pos_detail_m0.sec_idx = _functionTool.convertToInt(dropD_Sec.SelectedValue);
        _cen_pos_detail_m0.jobgrade_idx = _functionTool.convertToInt(dropD_jobgrade.SelectedValue);
        _cen_pos_detail_m0.man_power = _functionTool.convertToInt(manpower.Text.Trim());
        _cen_pos_detail_m0.cemp_idx = _emp_idx;


        _data_cen_master.cen_pos_list_m0 = new cen_pos_detail_m0[1];
        _data_cen_master.cen_pos_list_m0[0] = _cen_pos_detail_m0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlSetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_cen_master));

        if (_data_cen_master.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
            FvInsertEdit.Visible = true;
            lbCreate.Visible = false;
        }
        else
        {
            lbCreate.Visible = true;
            FvInsertEdit.Visible = false;
            gvPosition.Visible = true;
            _functionTool.setFvData(FvInsertEdit, FormViewMode.Insert, null);
            DropDownList ddlInsertOrg = (DropDownList)FvInsertEdit.FindControl("DropdownOrg");
            selectDdl(ddlInsertOrg, "3", "", "", "", "", "", "");

            DropDownList ddlInsertWg = (DropDownList)FvInsertEdit.FindControl("DropdownWg");
            ddlInsertWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));

            DropDownList ddlInsertLw = (DropDownList)FvInsertEdit.FindControl("DropdownLw");
            ddlInsertLw.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));

            DropDownList ddlInsertDept = (DropDownList)FvInsertEdit.FindControl("DropdownDept");
            ddlInsertDept.Items.Insert(0, new ListItem("---เลือกฝ่าย---", ""));

            DropDownList ddlInsertSec = (DropDownList)FvInsertEdit.FindControl("DropdownSec");
            ddlInsertSec.Items.Insert(0, new ListItem("---เลือกแผนก---", ""));

            DropDownList ddlInsertJobgrade = (DropDownList)FvInsertEdit.FindControl("DropDownGrade");
            selectDdl(ddlInsertJobgrade, "9", "", "", "", "", "", "");

            ViewState["forSearch"] = null;
            ViewState["forWordSearch"] = null;
            showPosition();
            FormViewS.Visible = true;
        }
    }


    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        data_cen_master _data_cen_master = new data_cen_master();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();

        cen_wg_detail_m0 _cen_wg_detail_m0 = new cen_wg_detail_m0();
        cen_lw_detail_m0 _cen_lw_detail_m0 = new cen_lw_detail_m0();
        cen_dept_detail_m0 _cen_dept_detail_m0 = new cen_dept_detail_m0();



        DropDownList dropD_Org = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");
        DropDownList dropD_Wg = (DropDownList)FvInsertEdit.FindControl("DropDownWg");
        DropDownList dropD_Lw = (DropDownList)FvInsertEdit.FindControl("DropDownLw");
        DropDownList dropD_Dept = (DropDownList)FvInsertEdit.FindControl("DropDownDept");
        DropDownList dropD_Sec = (DropDownList)FvInsertEdit.FindControl("DropDownSec");

        DropDownList dropD_Org_s = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
        DropDownList dropD_Wg_s = (DropDownList)FormViewS.FindControl("DropDownWg_s");
        DropDownList dropD_Lw_s = (DropDownList)FormViewS.FindControl("DropDownLw_s");
        DropDownList dropD_Dept_s = (DropDownList)FormViewS.FindControl("DropDownDept_s");
        DropDownList dropD_Sec_s = (DropDownList)FormViewS.FindControl("DropDownSec_s");

        switch (ddlName.ID)
        {
            case "DropDownOrg":

                _data_cen_master.master_mode = "4";
                _search_cen_master_detail.s_org_idx = dropD_Org.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text += HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setDdlData(dropD_Wg, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                dropD_Wg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));
                break;

            case "DropDownOrg_s":

                _data_cen_master.master_mode = "4";
                _search_cen_master_detail.s_org_idx = dropD_Org_s.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text += HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setDdlData(dropD_Wg_s, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                dropD_Wg_s.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));
                break;

            case "DropDownWg":


                _data_cen_master.master_mode = "5";
                _search_cen_master_detail.s_wg_idx = dropD_Wg.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setDdlData(dropD_Lw, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                dropD_Lw.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));
                break;

            case "DropDownWg_s":


                _data_cen_master.master_mode = "5";
                _search_cen_master_detail.s_wg_idx = dropD_Wg_s.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setDdlData(dropD_Lw_s, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                dropD_Lw_s.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));
                break;

            case "DropDownLw":


                _data_cen_master.master_mode = "6";
                _search_cen_master_detail.s_lw_idx = dropD_Lw.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setDdlData(dropD_Dept, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                dropD_Dept.Items.Insert(0, new ListItem("---เลือกฝ่าย---", ""));
                break;

            case "DropDownLw_s":


                _data_cen_master.master_mode = "6";
                _search_cen_master_detail.s_lw_idx = dropD_Lw_s.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setDdlData(dropD_Dept_s, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                dropD_Dept_s.Items.Insert(0, new ListItem("---เลือกฝ่าย---", ""));
                break;

            case "DropDownDept":


                _data_cen_master.master_mode = "7";
                _search_cen_master_detail.s_dept_idx = dropD_Dept.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setDdlData(dropD_Sec, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                dropD_Sec.Items.Insert(0, new ListItem("---เลือกแผนก---", ""));
                break;

            case "DropDownDept_s":


                _data_cen_master.master_mode = "7";
                _search_cen_master_detail.s_dept_idx = dropD_Dept_s.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setDdlData(dropD_Sec_s, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                dropD_Sec_s.Items.Insert(0, new ListItem("---เลือกแผนก---", ""));
                break;
        }
    }


    protected void checkBoxSearch()
    {
        data_cen_master _data_cen_master = new data_cen_master();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        DropDownList ddl_searchOrg = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
        DropDownList ddl_searchWg = (DropDownList)FormViewS.FindControl("DropDownWg_s");
        DropDownList ddl_searchLw = (DropDownList)FormViewS.FindControl("DropDownLw_s");
        DropDownList ddl_searchDept = (DropDownList)FormViewS.FindControl("DropDownDept_s");
        DropDownList ddl_searchSec = (DropDownList)FormViewS.FindControl("DropDownSec_s");
        DropDownList ddl_searchjobgrade = (DropDownList)FormViewS.FindControl("DropDownGrade_s");
        DropDownList ddl_status_for_s = (DropDownList)FormViewS.FindControl("DropDownstatus_s");
        TextBox searchbox = (TextBox)FormViewS.FindControl("tb_pos_s");

        //_search_cen_master_detail.s_org_name = searchbox.Text.Trim();
        _search_cen_master_detail.s_pos_name = searchbox.Text.Trim();
        _search_cen_master_detail.s_org_idx = ddl_searchOrg.SelectedValue;
        _search_cen_master_detail.s_wg_idx = ddl_searchWg.SelectedValue;
        _search_cen_master_detail.s_lw_idx = ddl_searchLw.SelectedValue;
        _search_cen_master_detail.s_dept_idx = ddl_searchDept.SelectedValue;
        _search_cen_master_detail.s_sec_idx = ddl_searchSec.SelectedValue;
        _search_cen_master_detail.s_status = ddl_status_for_s.SelectedValue;
        _search_cen_master_detail.s_jobgrade_idx = ddl_searchjobgrade.SelectedValue;
        _data_cen_master.master_mode = "8";

        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);

        ViewState["forSearch"] = _data_cen_master;
        //ViewState["forSearch2"] = _data_cen_master;

        showPosition();


    }


    protected void selectDdl(DropDownList ddlName, string masterMode, string _org_idx, string _wg_idx, string _lw_idx, string _dept_idx, string _sec_idx ,string _jobgrade_idx)
    {
        data_cen_master _data_cen_master = new data_cen_master();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();


        _data_cen_master.master_mode = masterMode;
        _search_cen_master_detail.s_org_idx = _org_idx;
        _search_cen_master_detail.s_wg_idx = _wg_idx;
        _search_cen_master_detail.s_lw_idx = _lw_idx;
        _search_cen_master_detail.s_dept_idx = _dept_idx;
        _search_cen_master_detail.s_sec_idx = _sec_idx;
        _search_cen_master_detail.s_jobgrade_idx = _jobgrade_idx;

        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
        // litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        switch (masterMode)
        {
            case "3":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือกองค์กร---", ""));


                break;

            case "4":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือกกลุ่มงาม---", ""));

                break;

            case "5":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));


                break;

            case "6":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือกฝ่าย---", ""));


                break;
            case "7":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือกแผนก---", ""));


                break;

            case "9":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_jobgrade_list_m0, "jobgrade_name", "jobgrade_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือก Job Grade---", ""));


                break;
        }

    }

    protected void cmdDelete(int cmdArg)
    {
        data_cen_master _data_cen_master = new data_cen_master();
        cen_pos_detail_m0 _cen_pos_detail_m0 = new cen_pos_detail_m0();

        _cen_pos_detail_m0.pos_idx = cmdArg;
        _cen_pos_detail_m0.pos_status = 9;
        _cen_pos_detail_m0.cemp_idx = _emp_idx;
        _data_cen_master.master_mode = "8";

        _data_cen_master.cen_pos_list_m0 = new cen_pos_detail_m0[1];
        _data_cen_master.cen_pos_list_m0[0] = _cen_pos_detail_m0;

        //--debug
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        _data_cen_master = callServiceMaster(_urlSetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _functionTool.setGvData(gvPosition, _data_cen_master.cen_pos_list_m0);

        if (_data_cen_master.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถลบข้อมูลนี้ได้!!!');", true);
            _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);
            DropDownList ddlSearchOrg = (DropDownList)FormViewS.FindControl("DropdownOrg_s");
            selectDdl(ddlSearchOrg, "3", "", "", "", "", "", "");

            DropDownList ddlSearchWg = (DropDownList)FormViewS.FindControl("DropdownWg_s");
            ddlSearchWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));

            DropDownList ddlSearchLw = (DropDownList)FormViewS.FindControl("DropdownLw_s");
            ddlSearchLw.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));

            DropDownList ddlSearchDept = (DropDownList)FormViewS.FindControl("DropdownDept_s");
            ddlSearchDept.Items.Insert(0, new ListItem("---เลือกฝ่าย---", ""));

            DropDownList ddlSearchSec = (DropDownList)FormViewS.FindControl("DropdownSec_s");
            ddlSearchSec.Items.Insert(0, new ListItem("---เลือกแผนก---", ""));

            DropDownList ddlSearchJobgrade = (DropDownList)FormViewS.FindControl("DropDownGrade_s");
            selectDdl(ddlSearchJobgrade, "9", "", "", "", "", "", "");

            gvPosition.Visible = true;
            FormViewS.Visible = true;
            lbCreate.Visible = true;
            ViewState["forSearch"] = null;
            ViewState["forWordSearch"] = null;
            showPosition();
        }
        else
        {
            _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);
            DropDownList ddlSearchOrg = (DropDownList)FormViewS.FindControl("DropdownOrg_s");
            selectDdl(ddlSearchOrg, "3", "", "", "", "", "", "");

            DropDownList ddlSearchWg = (DropDownList)FormViewS.FindControl("DropdownWg_s");
            ddlSearchWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));

            DropDownList ddlSearchLw = (DropDownList)FormViewS.FindControl("DropdownLw_s");
            ddlSearchLw.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));

            DropDownList ddlSearchDept = (DropDownList)FormViewS.FindControl("DropdownDept_s");
            ddlSearchDept.Items.Insert(0, new ListItem("---เลือกฝ่าย---", ""));

            DropDownList ddlSearchSec = (DropDownList)FormViewS.FindControl("DropdownSec_s");
            ddlSearchSec.Items.Insert(0, new ListItem("---เลือกแผนก---", ""));

            DropDownList ddlSearchJobgrade = (DropDownList)FormViewS.FindControl("DropDownGrade_s");
            selectDdl(ddlSearchJobgrade, "9", "", "", "", "", "", "");

            gvPosition.Visible = true;
            FormViewS.Visible = true;
            lbCreate.Visible = true;
            ViewState["forSearch"] = null;
            ViewState["forWordSearch"] = null;
            showPosition();
        }

        

    }


    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        data_cen_master _data_cen_master = new data_cen_master();
        switch (GvName.ID)
        {
            case "gvPosition":
                gvPosition.PageIndex = e.NewPageIndex;
                showPosition();
                break;

        }
        hlSetTotop.Focus();
    }

    protected string convertStatus(int status)
    {
        if (status == 1)
            textstatus = "online";
        else
            textstatus = "offline";
        return textstatus;
    }
    protected string convertCss(int status)
    {
        if (status == 1)
            textstatus = "text-success";
        else
            textstatus = "text-danger";
        return textstatus;
    }

    protected data_cen_master callServiceMaster(string _cmdUrl, data_cen_master _data_cen_master)
    {
        _localJson = _functionTool.convertObjectToJson(_data_cen_master);

        _localJson = _functionTool.callServicePost(_cmdUrl, _localJson);

        _data_cen_master = (data_cen_master)_functionTool.convertJsonToObject(typeof(data_cen_master), _localJson);


        return _data_cen_master;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _functionTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_functionTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
}