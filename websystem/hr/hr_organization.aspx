<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_organization.aspx.cs" Inherits="websystem_hr_hr_organization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:MultiView ID="mvSystem" runat="server">
            <asp:View ID="viewList" runat="server">
                <div class="form-group">
                    <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdCreate" CommandArgument="0"><i class="fa fa-plus" aria-hidden="true"></i></asp:LinkButton>
                </div>
                <asp:GridView ID="gvOrgList" runat="server" AutoGenerateColumns="False"
                CssClass="table table-striped table-bordered table-hover table-responsive"
                DataKeyNames="org_idx">
                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <asp:Label ID="lblContentNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name (TH)">
                            <ItemTemplate>
                                <asp:Label ID="lblOrgNameTH" runat="server" Text='<%# Eval("org_name_th") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name (EN)">
                            <ItemTemplate>
                                <asp:Label ID="lblOrgNameEN" runat="server" Text='<%# Eval("org_name_en") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblOrgStatus" runat="server" Text='<%# Eval("org_status") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> --%>
                        <asp:TemplateField HeaderText="Manage">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbEdit" CssClass="text-edit" runat="server" CommandName="cmdEdit" data-toggle="tooltip" OnCommand="btnCommand" title="Edit" CommandArgument='<%#Eval("org_idx") %>'><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="lbDelete" CssClass="text-trash" runat="server" CommandName="cmdDelete" data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบองค์กรนี้ใช่หรือไม่ ?')" CommandArgument='<%#Eval("org_idx") %>' title="Delete"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:View>
            <asp:View ID="viewForm" runat="server">
                <asp:FormView ID="fvOrganization" runat="server" DefaultMode="Insert" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">สร้างองค์กร</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">ชื่อ (TH)
                                            <span class="text-danger">*</span> :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbOrgNameTH" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="requiredOrgNameTH" ValidationGroup="formInsert" runat="server" Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbOrgNameTH" Font-Size="12px" ForeColor="Red" ErrorMessage="Required!"></asp:RequiredFieldValidator>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">ชื่อ (EN)
                                            <span class="text-danger">*</span> :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbOrgNameEN" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="requiredOrgNameEN" ValidationGroup="formInsert" runat="server" Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbOrgNameEN" Font-Size="12px" ForeColor="Red" ErrorMessage="Required!"></asp:RequiredFieldValidator>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                    <%-- <div class="form-group">
                                        <label class="col-sm-4 control-label">ที่อยู่  :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbOrgAddress" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">จังหวัด  :</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlProvince" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">อำเภอ  :</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlAmphur" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">ตำบล  :</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlDistrict" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">รหัสไปรษณีย์  :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbPostCode" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div> --%>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"></label>
                                        <div class="col-sm-4">
                                            <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0" ValidationGroup="formInsert">
                                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                            <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                            <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                                <i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">แก้ไของค์กร</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">ชื่อ (TH)
                                            <span class="text-danger">*</span> :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbOrgNameTH" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>'></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">ชื่อ (EN)
                                            <span class="text-danger">*</span> :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbOrgNameEN" runat="server" CssClass="form-control" Text='<%# Eval("org_name_en") %>'></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                    <%-- <div class="form-group">
                                        <label class="col-sm-4 control-label">ที่อยู่  :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbOrgAddress" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">จังหวัด  :</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlProvince" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">อำเภอ  :</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlAmphur" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">ตำบล  :</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlDistrict" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">รหัสไปรษณีย์  :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbPostCode" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div> --%>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"></label>
                                        <div class="col-sm-4">
                                            <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument='<%# Eval("org_idx") %>'>
                                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                            <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>