﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class websystem_hr_pms_performance : System.Web.UI.Page
{
    #region Connect

    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    data_cen_master _data_cen_master = new data_cen_master();

    data_cen_employee _data_cen_employee = new data_cen_employee();

    data_pms _datapms = new data_pms();

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- re organization --//
    static string _urlCenGetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlCenGetCenMasterList"];
    static string _urlGetCenEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCenEmployeeList"];

    static string _urlSelectMaster = _serviceUrl + ConfigurationManager.AppSettings["urlPMSSelectMaster_FormResult"];
    static string _urlInsertSystem = _serviceUrl + ConfigurationManager.AppSettings["urlPMSInsertSystem_FormResult"];
    static string _urlSelectSystem = _serviceUrl + ConfigurationManager.AppSettings["urlPMSSelectSystem_FormResult"];
    static string _urlPMSApproveAllSystem = _serviceUrl + ConfigurationManager.AppSettings["urlPMSApproveAllSystem"];

    int count = 0;
    int count_improve = 0;
    int count_star = 0;

    int[] rdept_qmr = { 20 }; //QMR:26

    //set sumtotal value
    decimal tot_sumpoint_kpi = 0;
    decimal tot_sumcore_value = 0;
    decimal totsolid_sumcore_value = 0;
    decimal totdotted_sumcore_value = 0;
    decimal tot_sumcompetencies = 0;
    decimal totsolid_sumcompetencies = 0;
    decimal totdotted_sumcompetencies = 0;


    //set detail factor
    string set_FactorName = "1. สรุปคะแนนดัชนีชี้วัดผลการปฏิบัติงาน(KPIs),2. วัฒนธรรมองค์กร เถ้าแก่น้อยสปีริต(TKN Spirit),3. ความสามารถตามหน้าที่งาน(Functional Competency),4. สถิติการปฏิบัติงาน(Time Attendance) 21/10/2562-20/10/2563";
    string set_TimePerformance = "ลาป่วย,ลากิจ,ขาดงาน,มาสาย-กลับก่อน";
    string set_calFactor = "";
    string set_scoreBefore = "0.00,0.00,0.00,0.00";
    string set_scoreDottedBefore = "0.00,0.00,0.00,0.00";
    string set_scoreSolidBefore = "0.00,0.00,0.00,0.00";
    string set_scoreAfter = "0.00,0.00,0.00,0.00";

    string set_timeAttendanceScore = "";

    string set_timescore_performance = "";
    string set_score_performance = "";

    //set sumpoint cal
    decimal sumpoint_kpi = 4;
    decimal sumpoint_corvalue = 0;
    decimal sumpoint_competencies = 0;


    double temps_total_score = 0;

    //set permission recheck performance
    string set_emp_idx_permission = "1413,172,24047,36583,33301";

    int _permission = 0;
    int set_permission_tab = 0;


    #endregion

    #region PageLoad
    protected void Page_Init(object sender, EventArgs e)
    {

        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);
        getCenEmployeeProfile(_emp_idx);



        //Set Permission Tab with HR
        string[] setTabHR = set_emp_idx_permission.Split(',');
        for (int i = 0; i < setTabHR.Length; i++)
        {
            if (setTabHR[i] == _emp_idx.ToString())
            {
                li3.Visible = true;
                _permission = 1;

                break;
            }
            else
            {
                li3.Visible = false;
                _permission = 0;

            }

        }

        //Set Permission Tab with HR

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            initPage();
            initPageLoad();

            getCountWaitApprove3Chiefs();
            getCountWaitApprove2Chiefs();
            getCountWaitApproveChiefs();
            getCountWaitApprove();

            getPermissionUserCreate();


            //Set Permission Tab can approve

            //litDebug.Text = set_permission_tab.ToString();
            if (set_permission_tab == 1)
            {
                li2.Visible = true;

                li4.Visible = true;
                li5.Visible = true;
                li6.Visible = true;

            }
            else
            {

                li2.Visible = false;

                li4.Visible = false;
                li5.Visible = false;
                li6.Visible = false;



            }




            ViewState["u0_docidx_detail"] = 0; //set docidx default
            ViewState["vs_CheckPermission_Detail"] = 0; //set permission default
            ViewState["vs_CheckPermission_Detail_Dotted"] = 0;
            ViewState["vs_CheckPermission_Detail_Solid"] = 0;

            ViewState["vs_Dotted"] = 0;
            ViewState["dotted_detail"] = 0;



        }

        //linkBtnTrigger(lbCreate);
        //linkBtnTrigger(lbLabResult);


    }

    #endregion

    #region event command

    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0);

    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            case "cmdSave":

                int _txt_core = 0;
                int _txt_com = 0;
                int _txt_dev = 0;
                int _txt_star = 0;

                //node 1 insert
                if (ViewState["u0_docidx_detail"].ToString() == "0")
                {

                    GridView GvCoreValue = (GridView)_PanelCoreValue.FindControl("GvCoreValue");
                    Label lit_total_core_value = (Label)GvCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value");

                    Label lit_total_Competencies = (Label)GvCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies");


                    data_pms data_insert_form = new data_pms();

                    //insert u0 document ot month
                    pmsu0_DocFormDetail u0_insert = new pmsu0_DocFormDetail();
                    data_insert_form.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

                    //u0 document insert

                    u0_insert.u0_docidx = 0;
                    u0_insert.cemp_idx = _emp_idx;
                    u0_insert.emp_idx = _emp_idx;
                    u0_insert.acidx = 1;
                    u0_insert.noidx = 1;
                    u0_insert.decision = int.Parse(cmdArg);
                    u0_insert.condition = 1;
                    u0_insert.u0_typeidx = int.Parse(ViewState["u0_typeidx"].ToString());
                    u0_insert.sum_mine_core_value = lit_total_core_value.Text;
                    u0_insert.sum_mine_competency = lit_total_Competencies.Text;

                    data_insert_form.Boxpmsu0_DocFormDetail[0] = u0_insert;

                    //u0 document insert

                    //u1 document insert
                    int i = 0;
                    var u1_corevalue = new pmsu1_DocFormDetail[GvCoreValue.Rows.Count + GvCompetencies.Rows.Count];

                    foreach (GridViewRow row in GvCoreValue.Rows)
                    {
                        RadioButtonList rdochoice_mine = (RadioButtonList)row.FindControl("rdochoice_mine");
                        Label lblm1_coreidx = (Label)row.FindControl("lblm1_coreidx");
                        Label lblcore_reason = (Label)row.FindControl("lblcore_reason");
                        TextBox txtcore_insert = (TextBox)row.FindControl("txtcore_insert");


                        data_insert_form.Boxpmsu1_DocFormDetail = new pmsu1_DocFormDetail[1];
                        u1_corevalue[i] = new pmsu1_DocFormDetail();

                        if (rdochoice_mine.SelectedValue != "0" && rdochoice_mine.SelectedValue != null && rdochoice_mine.SelectedValue != "")
                        {
                            u1_corevalue[i].m0_typeidx = 1;
                            u1_corevalue[i].m1_typeidx = int.Parse(lblm1_coreidx.Text); // เอาเข้าแทน m1_coreidx
                            u1_corevalue[i].m0_point_mine = rdochoice_mine.SelectedValue.ToString();


                            //if(int.Parse(ViewState["org_permission"] ))
                            if (int.Parse(ViewState["org_permission"].ToString()) == 3) //org res
                            {
                                if (txtcore_insert.Text == "")
                                {
                                    _txt_core = 1;
                                    u1_corevalue[i].remark_mine = txtcore_insert.Text;

                                }
                                else
                                {
                                    _txt_core = 0;
                                    u1_corevalue[i].remark_mine = txtcore_insert.Text;

                                }


                            }
                            else
                            {
                                _txt_core = 0;
                                u1_corevalue[i].remark_mine = txtcore_insert.Text;

                            }

                        }

                        data_insert_form.Boxpmsu1_DocFormDetail = u1_corevalue;

                        i++;
                    }


                    foreach (GridViewRow row in GvCompetencies.Rows)
                    {
                        RadioButtonList rdochoicecom_mine = (RadioButtonList)row.FindControl("rdochoicecom_mine");
                        Label lblm1_typeidx = (Label)row.FindControl("lblm1_typeidx");
                        TextBox txtcom_insert = (TextBox)row.FindControl("txtcom_insert");

                        u1_corevalue[i] = new pmsu1_DocFormDetail();

                        if (rdochoicecom_mine.SelectedValue != "0" && rdochoicecom_mine.SelectedValue != null && rdochoicecom_mine.SelectedValue != "")
                        {
                            u1_corevalue[i].m0_typeidx = 2;
                            u1_corevalue[i].m1_typeidx = int.Parse(lblm1_typeidx.Text);
                            u1_corevalue[i].m0_point_mine = rdochoicecom_mine.SelectedValue.ToString();


                            if (int.Parse(ViewState["org_permission"].ToString()) == 3) //org res
                            {

                                if (txtcom_insert.Text == "")
                                {
                                    _txt_com = 1;
                                    u1_corevalue[i].remark_mine = txtcom_insert.Text;

                                }
                                else
                                {
                                    _txt_com = 0;
                                    u1_corevalue[i].remark_mine = txtcom_insert.Text;

                                }

                            }
                            else
                            {

                                _txt_com = 0;
                                u1_corevalue[i].remark_mine = txtcom_insert.Text;

                            }


                        }

                        data_insert_form.Boxpmsu1_DocFormDetail = u1_corevalue;

                        i++;

                    }

                    //u1 document insert

                    //u2 document insert impro
                    TextBox txtcomment = (TextBox)FvComment.FindControl("txtcomment");
                    TextBox txtcomment_star = (TextBox)FvStar.FindControl("txtcomment_star");

                    string set_type = "3,4";
                    string[] setType = set_type.Split(',');

                    var u2_insert = new pmsu2_DocFormDetail[setType.Length];
                    for (int k = 0; k < setType.Length; k++)
                    {

                        data_insert_form.Boxpmsu2_DocFormDetail = new pmsu2_DocFormDetail[1];
                        u2_insert[k] = new pmsu2_DocFormDetail();

                        if (setType[k] == "3")
                        {
                            u2_insert[k].m0_typeidx = 3;

                            if (int.Parse(ViewState["org_permission"].ToString()) == 3) //org res
                            {

                                if (txtcomment.Text == "")
                                {
                                    _txt_dev = 1;
                                    u2_insert[k].comment_name = txtcomment.Text;

                                }
                                else
                                {
                                    _txt_dev = 0;
                                    u2_insert[k].comment_name = txtcomment.Text;

                                }



                            }
                            else
                            {

                                _txt_dev = 0;

                                u2_insert[k].comment_name = txtcomment.Text; // 

                            }



                        }
                        else // fv Star
                        {
                            u2_insert[k].m0_typeidx = 4;

                            if (int.Parse(ViewState["org_permission"].ToString()) == 3) //org res
                            {

                                if (txtcomment.Text == "")
                                {
                                    _txt_star = 1;
                                    u2_insert[k].comment_name = txtcomment_star.Text; // 

                                }
                                else
                                {

                                    _txt_star = 0;
                                    u2_insert[k].comment_name = txtcomment_star.Text; // 

                                }

                            }
                            else
                            {

                                _txt_star = 0;

                                u2_insert[k].comment_name = txtcomment_star.Text; // 

                            }

                        }
                        data_insert_form.Boxpmsu2_DocFormDetail = u2_insert;

                    }


                    //insert kpi
                    int j = 0;
                    var u0_individual_kpi = new pmsm2_individualDetail[GvKPIs.Rows.Count];
                    foreach (GridViewRow row in GvKPIs.Rows)
                    {
                        TextBox txtkpi_insert = (TextBox)row.FindControl("txtkpi_insert");
                        RadioButtonList rdoperformance_level = (RadioButtonList)row.FindControl("rdoperformance_level");

                        data_insert_form.Boxpmsm2_individual_list = new pmsm2_individualDetail[1];
                        u0_individual_kpi[j] = new pmsm2_individualDetail();

                        if (rdoperformance_level.SelectedValue != "0" && rdoperformance_level.SelectedValue != null && rdoperformance_point.SelectedValue != "")
                        {
                            u0_individual_kpi[j].comment = txtkpi_insert.Text;
                            u0_individual_kpi[j].m2_kpi_idx = int.Parse(rdoperformance_level.SelectedValue);

                        }

                        data_insert_form.Boxpmsm2_individual_list = u0_individual_kpi;

                        j++;
                    }

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_insert_form));
                    if (int.Parse(ViewState["org_permission"].ToString()) == 3)
                    {

                        if (_txt_core == 1 || _txt_com == 1 || _txt_dev == 1 || _txt_star == 1)
                        {

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลให้ครบถ้วน !!');", true);

                        }
                        else
                        {

                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_insert_form));

                            data_insert_form = callServicePostPMS(_urlInsertSystem, data_insert_form);
                            setActiveTab("docDetail", 0, 0, 0);

                            getPermissionUserCreate();
                            getCountWaitApprove();
                            setOntop.Focus();

                        }


                    }
                    else
                    {

                        data_insert_form = callServicePostPMS(_urlInsertSystem, data_insert_form);
                        setActiveTab("docDetail", 0, 0, 0);

                        getPermissionUserCreate();
                        getCountWaitApprove();

                        setOntop.Focus();

                    }


                }
                else //update node 1 /solid/dotted/approve1,2,3
                {


                    //u0 document update
                    data_pms data_update_form = new data_pms();

                    //insert u0 document ot month
                    pmsu0_DocFormDetail u0_update = new pmsu0_DocFormDetail();
                    data_update_form.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

                    //u0 document update

                    u0_update.cemp_idx = _emp_idx;
                    u0_update.emp_idx = int.Parse(ViewState["cemp_idx_detail"].ToString());
                    u0_update.u0_docidx = int.Parse(ViewState["u0_docidx_detail"].ToString());
                    u0_update.acidx = int.Parse(ViewState["acidx_detail"].ToString());
                    u0_update.noidx = int.Parse(ViewState["unidx_detail"].ToString());
                    u0_update.decision = int.Parse(cmdArg);
                    u0_update.condition = 1;

                    //u0_update.sum_mine_core_value = lit_total_core_value.Text;
                    //u0_update.sum_mine_competency = lit_total_Competencies.Text;

                    data_update_form.Boxpmsu0_DocFormDetail[0] = u0_update;


                    switch (ViewState["unidx_detail"].ToString())
                    {
                        case "1":
                        case "2":
                        case "3":


                            //u1 document insert
                            int i = 0;
                            var u1_corevalue = new pmsu1_DocFormDetail[GvViewCoreValue.Rows.Count + GvViewCompetencies.Rows.Count];

                            foreach (GridViewRow row in GvViewCoreValue.Rows)
                            {
                                RadioButtonList rdochoice_mine_detail = (RadioButtonList)row.FindControl("rdochoice_mine_detail");
                                RadioButtonList rdochoice_dotted_detail = (RadioButtonList)row.FindControl("rdochoice_dotted_detail");
                                TextBox txtremarkdotted_core = (TextBox)row.FindControl("txtremarkdotted_core");
                                RadioButtonList rdochoice_head_detail = (RadioButtonList)row.FindControl("rdochoice_head_detail");
                                TextBox txtremarkhead_core = (TextBox)row.FindControl("txtremarkhead_core");


                                Label lblm1_coreidx = (Label)row.FindControl("lblm1_coreidx");
                                TextBox txtremarkmine_core = (TextBox)row.FindControl("txtremarkmine_core");

                                data_update_form.Boxpmsu1_DocFormDetail = new pmsu1_DocFormDetail[1];
                                u1_corevalue[i] = new pmsu1_DocFormDetail();

                                if (rdochoice_mine_detail.SelectedValue != "0" && rdochoice_mine_detail.SelectedValue != null && rdochoice_mine_detail.SelectedValue != "")
                                {
                                    u1_corevalue[i].u0_docidx = int.Parse(ViewState["u0_docidx_detail"].ToString());
                                    u1_corevalue[i].m0_typeidx = 1;
                                    u1_corevalue[i].m1_typeidx = int.Parse(lblm1_coreidx.Text); // เอาเข้าแทน m1_coreidx

                                    u1_corevalue[i].m0_point_mine = rdochoice_mine_detail.SelectedValue.ToString();

                                    if (int.Parse(ViewState["org_permission"].ToString()) == 3) //org res
                                    {
                                        if (txtremarkmine_core.Text == "")
                                        {
                                            _txt_core = 1;
                                            u1_corevalue[i].remark_mine = txtremarkmine_core.Text;

                                        }
                                        else
                                        {
                                            _txt_core = 0;
                                            u1_corevalue[i].remark_mine = txtremarkmine_core.Text;

                                        }

                                    }
                                    else
                                    {

                                        _txt_core = 0;
                                        u1_corevalue[i].remark_mine = txtremarkmine_core.Text;

                                    }



                                    u1_corevalue[i].m0_point_dotted = rdochoice_dotted_detail.SelectedValue.ToString();
                                    u1_corevalue[i].remark_dotted = txtremarkdotted_core.Text;

                                    u1_corevalue[i].m0_point_solid = rdochoice_head_detail.SelectedValue.ToString();
                                    u1_corevalue[i].remark_solid = txtremarkhead_core.Text;


                                }

                                data_update_form.Boxpmsu1_DocFormDetail = u1_corevalue;

                                i++;
                            }


                            foreach (GridViewRow row in GvViewCompetencies.Rows)
                            {

                                RadioButtonList rdochoicecom_mine_detail = (RadioButtonList)row.FindControl("rdochoicecom_mine_detail");
                                RadioButtonList rdochoicecom_dotted_detail = (RadioButtonList)row.FindControl("rdochoicecom_dotted_detail");
                                TextBox txtremark_dotted = (TextBox)row.FindControl("txtremark_dotted");
                                RadioButtonList rdochoicecom_head_detail = (RadioButtonList)row.FindControl("rdochoicecom_head_detail");
                                TextBox txtremark_head = (TextBox)row.FindControl("txtremark_head");


                                Label lblm1_typeidx = (Label)row.FindControl("lblm1_typeidx");
                                TextBox txtremark_detail = (TextBox)row.FindControl("txtremark_detail");

                                data_update_form.Boxpmsu1_DocFormDetail = new pmsu1_DocFormDetail[1];
                                u1_corevalue[i] = new pmsu1_DocFormDetail();

                                if (rdochoicecom_mine_detail.SelectedValue != "0" && rdochoicecom_mine_detail.SelectedValue != null && rdochoicecom_mine_detail.SelectedValue != "")
                                {
                                    u1_corevalue[i].m0_typeidx = 2;
                                    u1_corevalue[i].m1_typeidx = int.Parse(lblm1_typeidx.Text);
                                    u1_corevalue[i].m0_point_mine = rdochoicecom_mine_detail.SelectedValue.ToString();

                                    // int _txt_core = 0;
                                    // int _txt_com = 0;
                                    // int _txt_dev = 0;
                                    // int _txt_star = 0;

                                    if (int.Parse(ViewState["org_permission"].ToString()) == 3) //org res
                                    {
                                        if (txtremark_detail.Text == "")
                                        {

                                            _txt_com = 1;

                                            u1_corevalue[i].remark_mine = txtremark_detail.Text;

                                        }
                                        else
                                        {

                                            _txt_com = 0;

                                            u1_corevalue[i].remark_mine = txtremark_detail.Text;

                                        }



                                    }
                                    else
                                    {

                                        _txt_com = 0;

                                        u1_corevalue[i].remark_mine = txtremark_detail.Text;

                                    }

                                    u1_corevalue[i].m0_point_dotted = rdochoicecom_dotted_detail.SelectedValue.ToString();
                                    u1_corevalue[i].remark_dotted = txtremark_dotted.Text;
                                    u1_corevalue[i].m0_point_solid = rdochoicecom_head_detail.SelectedValue.ToString();
                                    u1_corevalue[i].remark_solid = txtremark_head.Text;

                                }

                                data_update_form.Boxpmsu1_DocFormDetail = u1_corevalue;

                                i++;

                            }

                            //u1 document insert


                            //u2 document insert impro
                            TextBox txtcomment_view = (TextBox)FvViewComment.FindControl("txtcomment_view");
                            TextBox txtcomment_star_view = (TextBox)FvViewStar.FindControl("txtcomment_star_view");
                            Label lbl_u2idx_view = (Label)FvViewComment.FindControl("lbl_u2idx_view");
                            Label lbl_u2idx_star_view = (Label)FvViewStar.FindControl("lbl_u2idx_star_view");

                            string set_type = "3,4";
                            string[] setType = set_type.Split(',');

                            var u2_insert = new pmsu2_DocFormDetail[setType.Length];
                            for (int k = 0; k < setType.Length; k++)
                            {

                                data_update_form.Boxpmsu2_DocFormDetail = new pmsu2_DocFormDetail[1];
                                u2_insert[k] = new pmsu2_DocFormDetail();

                                if (setType[k] == "3")
                                {
                                    u2_insert[k].u0_docidx = int.Parse(ViewState["u0_docidx_detail"].ToString());

                                    if ((int.Parse(ViewState["unidx_detail"].ToString()) == 1
                                    || int.Parse(ViewState["unidx_detail"].ToString()) == 2
                                    || int.Parse(ViewState["unidx_detail"].ToString()) == 3) && lbl_u2idx_view.Text != "")
                                    {

                                        u2_insert[k].u2_docidx = int.Parse(lbl_u2idx_view.Text);
                                    }
                                    else
                                    {

                                        u2_insert[k].u2_docidx = 0;

                                    }

                                    u2_insert[k].m0_typeidx = 3;

                                    if (int.Parse(ViewState["org_permission"].ToString()) == 3) //org res
                                    {
                                        if (txtcomment_view.Text == "")
                                        {

                                            _txt_dev = 1;

                                            u2_insert[k].comment_name = txtcomment_view.Text; // 

                                        }
                                        else
                                        {

                                            _txt_dev = 0;

                                            u2_insert[k].comment_name = txtcomment_view.Text; // 

                                        }

                                    }
                                    else
                                    {

                                        _txt_dev = 0;

                                        u2_insert[k].comment_name = txtcomment_view.Text; // 

                                    }


                                }
                                else // fv Star
                                {
                                    u2_insert[k].u0_docidx = int.Parse(ViewState["u0_docidx_detail"].ToString());

                                    if ((int.Parse(ViewState["unidx_detail"].ToString()) == 1
                                    || int.Parse(ViewState["unidx_detail"].ToString()) == 2
                                    || int.Parse(ViewState["unidx_detail"].ToString()) == 3) && lbl_u2idx_star_view.Text != "")
                                    {

                                        u2_insert[k].u2_docidx = int.Parse(lbl_u2idx_star_view.Text);
                                    }
                                    else
                                    {

                                        u2_insert[k].u2_docidx = 0;

                                    }

                                    u2_insert[k].m0_typeidx = 4;

                                    if (int.Parse(ViewState["org_permission"].ToString()) == 3) //org res
                                    {
                                        if (txtcomment_star_view.Text == "")
                                        {

                                            _txt_star = 1;

                                            u2_insert[k].comment_name = txtcomment_star_view.Text; // 

                                        }
                                        else
                                        {

                                            _txt_star = 0;

                                            u2_insert[k].comment_name = txtcomment_star_view.Text; // 

                                        }

                                    }
                                    else
                                    {

                                        _txt_star = 0;

                                        u2_insert[k].comment_name = txtcomment_star_view.Text; //

                                    }



                                }
                                data_update_form.Boxpmsu2_DocFormDetail = u2_insert;

                            }

                            //insert kpi
                            int j = 0;
                            var u0_individual_kpi = new pmsm2_individualDetail[GvViewKPIs.Rows.Count];
                            foreach (GridViewRow row in GvViewKPIs.Rows)
                            {
                                TextBox txtkpi_view = (TextBox)row.FindControl("txtkpi_view");
                                RadioButtonList rdoperformance_level_view = (RadioButtonList)row.FindControl("rdoperformance_level_view");

                                data_update_form.Boxpmsm2_individual_list = new pmsm2_individualDetail[1];
                                u0_individual_kpi[j] = new pmsm2_individualDetail();

                                if (rdoperformance_level_view.SelectedValue != "0" && rdoperformance_level_view.SelectedValue != null && rdoperformance_level_view.SelectedValue != "")
                                {

                                    u0_individual_kpi[j].comment = txtkpi_view.Text;
                                    u0_individual_kpi[j].m2_kpi_idx = int.Parse(rdoperformance_level_view.SelectedValue);

                                }

                                data_update_form.Boxpmsm2_individual_list = u0_individual_kpi;

                                j++;
                            }


                            break;
                        case "4":

                            break;

                    }

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_update_form));

                    if (int.Parse(ViewState["org_permission"].ToString()) == 3) // check res
                    {

                        if (_txt_core == 1 || _txt_com == 1 || _txt_dev == 1 || _txt_star == 1)
                        {

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลให้ครบถ้วน !!');", true);

                        }
                        else
                        {

                            data_update_form = callServicePostPMS(_urlInsertSystem, data_update_form);
                            setActiveTab("docDetail", 0, 0, 0);

                            getCountWaitApprove3Chiefs();
                            getCountWaitApprove2Chiefs();
                            getCountWaitApproveChiefs();
                            getCountWaitApprove();
                            getPermissionUserCreate();
                            setOntop.Focus();

                        }


                    }
                    else
                    {

                        data_update_form = callServicePostPMS(_urlInsertSystem, data_update_form);
                        setActiveTab("docDetail", 0, 0, 0);

                        getCountWaitApprove3Chiefs();
                        getCountWaitApprove2Chiefs();
                        getCountWaitApproveChiefs();
                        getCountWaitApprove();
                        getPermissionUserCreate();
                        setOntop.Focus();

                    }


                    // data_update_form = callServicePostPMS(_urlInsertSystem, data_update_form);
                    // setActiveTab("docDetail", 0, 0, 0);


                }

                break;
            case "cmdHRCheck":

                //u0 document update
                data_pms data_update_hr = new data_pms();

                //insert u0 document ot month
                pmsu0_DocFormDetail u0_update_hr = new pmsu0_DocFormDetail();
                data_update_hr.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

                //u0 document update

                u0_update_hr.cemp_idx = _emp_idx; //hr
                u0_update_hr.emp_idx = int.Parse(ViewState["cemp_idx_detail"].ToString());//emp_create performance
                u0_update_hr.u0_docidx = int.Parse(ViewState["u0_docidx_detail"].ToString()); //join pms_docform_u0

                u0_update_hr.acidx = 7;
                u0_update_hr.noidx = 7;
                u0_update_hr.decision = int.Parse(cmdArg);//12
                u0_update_hr.condition = 2;

                //u0_update.sum_mine_core_value = lit_total_core_value.Text;
                //u0_update.sum_mine_competency = lit_total_Competencies.Text;

                data_update_hr.Boxpmsu0_DocFormDetail[0] = u0_update_hr;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_update_hr));

                data_update_hr = callServicePostPMS(_urlInsertSystem, data_update_hr);



                getCountWaitApprove3Chiefs();
                getCountWaitApprove2Chiefs();
                getCountWaitApproveChiefs();
                getCountWaitApprove();
                getPermissionUserCreate();
                setActiveTab("docCheckPerformance", 0, 0, 0);



                setOntop.Focus();



                break;
            case "cmdSaveApprove1":

                data_pms data_approve_1 = new data_pms();

                //update u1 document approve
                var _u0doc_approve = new pmsu0_DocFormDetail[GvApprove1.Rows.Count];
                int sum_approve_ = 0;
                int count_approve_ = 0;
                //string u0_sentmail = "";

                foreach (GridViewRow gv_row in GvApprove1.Rows)
                {

                    data_approve_1.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

                    CheckBox chk_approve1 = (CheckBox)gv_row.FindControl("chk_approve1");
                    Label lblu0_docidx = (Label)gv_row.FindControl("lblu0_docidx");
                    Label lbl_cemp_idx = (Label)gv_row.FindControl("lbl_cemp_idx");

                    if (chk_approve1.Checked == true)
                    {

                        _u0doc_approve[count_approve_] = new pmsu0_DocFormDetail();

                        _u0doc_approve[count_approve_].cemp_idx = _emp_idx;
                        _u0doc_approve[count_approve_].emp_idx = int.Parse(lbl_cemp_idx.Text);//emp_create performance
                        _u0doc_approve[count_approve_].u0_docidx = int.Parse(lblu0_docidx.Text); //join pms_docform_u0
                        _u0doc_approve[count_approve_].noidx = 4;
                        _u0doc_approve[count_approve_].acidx = 4;
                        _u0doc_approve[count_approve_].decision = int.Parse(cmdArg);
                        //_u0doc_approve[count_approve_].condition = 3;

                        sum_approve_ = sum_approve_ + 1;
                        count_approve_++;

                    }

                }
                //update u1 document ot shift rotate
                if (sum_approve_ == 0)
                {
                    //litDebug.Text = "no selected";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาเลือกรายการ ก่อนทำการบันทึก ---');", true);
                    break;
                }
                else
                {
                    data_approve_1.Boxpmsu0_DocFormDetail = _u0doc_approve;
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_approve_1));
                    //data_approve_1 = callServicePostPMS(_urlInsertSystem, data_approve_1);
                    data_approve_1 = callServicePostPMS(_urlPMSApproveAllSystem, data_approve_1);


                    getCountWaitApprove();
                    getCountWaitApprove2Chiefs();
                    getCountWaitApprove3Chiefs();
                    getCountWaitApproveChiefs();
                    getPermissionUserCreate();
                    setOntop.Focus();

                }

                break;
            case "cmdSaveApprove2":

                data_pms data_approve_2 = new data_pms();

                //update u1 document approve
                var _u0doc_approve2 = new pmsu0_DocFormDetail[GvApprove2.Rows.Count];
                int sum_approve_2 = 0;
                int count_approve_2 = 0;
                //string u0_sentmail = "";

                foreach (GridViewRow gv_row2 in GvApprove2.Rows)
                {

                    data_approve_2.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

                    CheckBox chk_approve2 = (CheckBox)gv_row2.FindControl("chk_approve2");
                    Label lblu0_docidx2 = (Label)gv_row2.FindControl("lblu0_docidx");
                    Label lbl_cemp_idx2 = (Label)gv_row2.FindControl("lbl_cemp_idx");

                    if (chk_approve2.Checked == true)
                    {

                        _u0doc_approve2[count_approve_2] = new pmsu0_DocFormDetail();

                        _u0doc_approve2[count_approve_2].cemp_idx = _emp_idx;
                        _u0doc_approve2[count_approve_2].emp_idx = int.Parse(lbl_cemp_idx2.Text);//emp_create performance
                        _u0doc_approve2[count_approve_2].u0_docidx = int.Parse(lblu0_docidx2.Text); //join pms_docform_u0
                        _u0doc_approve2[count_approve_2].noidx = 5;
                        _u0doc_approve2[count_approve_2].acidx = 5;
                        _u0doc_approve2[count_approve_2].decision = int.Parse(cmdArg);
                        //_u0doc_approve[count_approve_].condition = 3;

                        sum_approve_2 = sum_approve_2 + 1;
                        count_approve_2++;

                    }

                }
                //update u1 document ot shift rotate
                if (sum_approve_2 == 0)
                {
                    //litDebug.Text = "no selected";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาเลือกรายการ ก่อนทำการบันทึก ---');", true);
                    break;
                }
                else
                {
                    data_approve_2.Boxpmsu0_DocFormDetail = _u0doc_approve2;
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_approve_2));

                    data_approve_2 = callServicePostPMS(_urlPMSApproveAllSystem, data_approve_2);

                    getCountWaitApprove();
                    getCountWaitApproveChiefs();

                    getCountWaitApprove3Chiefs();
                    getCountWaitApprove2Chiefs();
                    getPermissionUserCreate();
                    setOntop.Focus();

                }

                break;
            case "cmdSaveApprove3":

                data_pms data_approve_3 = new data_pms();

                //update u1 document approve
                var _u0doc_approve3 = new pmsu0_DocFormDetail[GvApprove3.Rows.Count];
                int sum_approve_3 = 0;
                int count_approve_3 = 0;
                //string u0_sentmail = "";

                foreach (GridViewRow gv_row3 in GvApprove3.Rows)
                {

                    data_approve_3.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

                    CheckBox chk_approve3 = (CheckBox)gv_row3.FindControl("chk_approve3");
                    Label lblu0_docidx3 = (Label)gv_row3.FindControl("lblu0_docidx");
                    Label lbl_cemp_idx3 = (Label)gv_row3.FindControl("lbl_cemp_idx");

                    if (chk_approve3.Checked == true)
                    {

                        _u0doc_approve3[count_approve_3] = new pmsu0_DocFormDetail();

                        _u0doc_approve3[count_approve_3].cemp_idx = _emp_idx;
                        _u0doc_approve3[count_approve_3].emp_idx = int.Parse(lbl_cemp_idx3.Text);//emp_create performance
                        _u0doc_approve3[count_approve_3].u0_docidx = int.Parse(lblu0_docidx3.Text); //join pms_docform_u0
                        _u0doc_approve3[count_approve_3].noidx = 6;
                        _u0doc_approve3[count_approve_3].acidx = 6;
                        _u0doc_approve3[count_approve_3].decision = int.Parse(cmdArg);
                        //_u0doc_approve[count_approve_].condition = 3;

                        sum_approve_3 = sum_approve_3 + 1;
                        count_approve_3++;

                    }

                }
                //update u1 document ot shift rotate
                if (sum_approve_3 == 0)
                {
                    //litDebug.Text = "no selected";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาเลือกรายการ ก่อนทำการบันทึก ---');", true);
                    break;
                }
                else
                {
                    data_approve_3.Boxpmsu0_DocFormDetail = _u0doc_approve3;
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_approve_3));

                    data_approve_3 = callServicePostPMS(_urlPMSApproveAllSystem, data_approve_3);

                    getCountWaitApprove();
                    getCountWaitApproveChiefs();
                    getCountWaitApprove2Chiefs();
                    getCountWaitApprove3Chiefs();
                    getPermissionUserCreate();
                    setOntop.Focus();

                }

                break;
            case "cmdSearch":

                getSearchDetailIndex();


                break;
            case "cmdReset":

                setActiveTab("docDetail", 0, 0, 0);


                break;
            case "cmdSearchWaitApprove":

                getSearchDetailWaitApprove();


                break;
            case "cmdSearchWaitApprove1":

                getSearchDetailWaitApprove1();


                break;
            case "cmdSearchWaitApprove2":

                getSearchDetailWaitApprove2();


                break;
            case "cmdSearchWaitApprove3":

                getSearchDetailWaitApprove3();


                break;
            case "cmdResetWaitApprove":

                setActiveTab("docApprove", 0, 0, 0);


                break;
            case "cmdResetWaitApprove1":

                setActiveTab("docApprove1", 0, 0, 0);


                break;
            case "cmdResetWaitApprove2":

                setActiveTab("docApprove2", 0, 0, 0);


                break;
            case "cmdResetWaitApprove3":

                setActiveTab("docApprove3", 0, 0, 0);


                break;
            case "cmdSearchCheckPerformance":

                getSearchDetailCheckPerformance();


                break;
            case "cmdResetCheckPerformance":

                setActiveTab("docCheckPerformance", 0, 0, 0);


                break;
            case "cmdCancel":

                //setActiveTab("docDetail", 0, 0, 0);
                getCountWaitApprove();
                setOntop.Focus();

                break;
            case "cmdViewDetail":

                string[] arg_detail = new string[12];
                arg_detail = e.CommandArgument.ToString().Split(';');

                ViewState["u0_docidx_detail"] = arg_detail[0];
                ViewState["unidx_detail"] = arg_detail[1];
                ViewState["acidx_detail"] = arg_detail[2];
                ViewState["staidx_detail"] = arg_detail[3];
                ViewState["cemp_idx_detail"] = arg_detail[4];
                ViewState["form_name_detail"] = arg_detail[5];
                ViewState["sum_solid_core_value_detail"] = arg_detail[6];
                ViewState["sum_solid_competency_detail"] = arg_detail[7];
                ViewState["u0_typeidx_detail"] = arg_detail[8];
                ViewState["solid_detail"] = arg_detail[9];
                ViewState["dotted_detail"] = arg_detail[10];
                ViewState["org_idx_detail"] = arg_detail[11];


                //set_timeAttendanceScore

                if (ViewState["dotted_detail"] == null)
                {
                    ViewState["dotted_detail"] = 0;
                }

                //check permission view detail
                if (ViewState["vsCount_GvDetailApprove"] != null)
                {

                    pmsu0_DocFormDetail[] _templistPermissionDetail = (pmsu0_DocFormDetail[])ViewState["vsCount_GvDetailApprove"];
                    var _linqDataListPermissionDetail = (from dt in _templistPermissionDetail
                                                         where
                                                         dt.u0_docidx == int.Parse(ViewState["u0_docidx_detail"].ToString())
                                                         && dt.unidx == int.Parse(ViewState["unidx_detail"].ToString())
                                                         &&
                                                         (
                                                            (dt.unidx == 2 && dt.eval_emp_type == 3 && dt.eval_emp_idx == _emp_idx) //dotted
                                                            || (dt.unidx == 3 && dt.eval_emp_type == 2 && dt.eval_emp_idx == _emp_idx) //solid
                                                            || (dt.unidx == 4 && dt.eval_emp_type == 4 && dt.eval_emp_idx == _emp_idx) //approve 1
                                                            || (dt.unidx == 5 && dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                                            || (dt.unidx == 6 && dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3
                                                         )

                                                         select dt).Distinct().ToList();


                    pmsu0_DocFormDetail[] _templistPermissionDetail_Dotted = (pmsu0_DocFormDetail[])ViewState["vsCount_GvDetailApprove"];
                    var _linqDataListPermissionDetail_Dotted = (from dt in _templistPermissionDetail_Dotted
                                                                where
                                                                (dt.u0_docidx == int.Parse(ViewState["u0_docidx_detail"].ToString())
                                                                && dt.eval_emp_idx == _emp_idx
                                                                && dt.eval_emp_type == 3
                                                                ) // dotted

                                                                select dt).Distinct().ToList();

                    pmsu0_DocFormDetail[] _templistPermissionDetail_Solid = (pmsu0_DocFormDetail[])ViewState["vsCount_GvDetailApprove"];
                    var _linqDataListPermissionDetail_Solid = (from dt in _templistPermissionDetail_Solid
                                                               where
                                                               (dt.u0_docidx == int.Parse(ViewState["u0_docidx_detail"].ToString())
                                                               && dt.eval_emp_idx == _emp_idx
                                                               && (dt.eval_emp_type == 2 || dt.eval_emp_type == 4 || dt.eval_emp_type == 5 || dt.eval_emp_type == 6)
                                                               ) // solid

                                                               select dt).Distinct().ToList();


                    pmsu0_DocFormDetail[] _templistPer_Dotted = (pmsu0_DocFormDetail[])ViewState["vsCount_GvDetailApprove"];
                    var _linqDataListPer_Dotted = (from dt in _templistPer_Dotted
                                                   where
                                                   (dt.u0_docidx == int.Parse(ViewState["u0_docidx_detail"].ToString())
                                                   //&& dt.eval_emp_idx == _emp_idx
                                                   && dt.eval_emp_type == 3
                                                   ) // dotted

                                                   select dt).Distinct().ToList();


                    ViewState["vs_CheckPermission_Detail"] = _linqDataListPermissionDetail.Count();
                    ViewState["vs_CheckPermission_Detail_Dotted"] = _linqDataListPermissionDetail_Dotted.Count();
                    ViewState["vs_CheckPermission_Detail_Solid"] = _linqDataListPermissionDetail_Solid.Count();

                    ViewState["vs_Dotted"] = _linqDataListPer_Dotted.Count();

                }

                // litDebug.Text = ViewState["vs_CheckPermission_Detail"].ToString() + "|" + _emp_idx.ToString() + "|" + ViewState["vs_CheckPermission_Detail_Dotted"].ToString() + "|"
                // + ViewState["vs_CheckPermission_Detail_Solid"].ToString() + "|" + ViewState["vs_Dotted"].ToString() + "|" + ViewState["solid_detail"].ToString() + "|" + ViewState["dotted_detail"].ToString();



                setActiveTab("docDetail", int.Parse(ViewState["u0_docidx_detail"].ToString()), 0, int.Parse(ViewState["staidx_detail"].ToString()));
                //setFormDataActor(int.Parse(arg_detail[3]), int.Parse(arg_detail[4]), int.Parse(arg_detail[5]), int.Parse(arg_detail[2]));

                break;



        }
    }


    #endregion event command

    #region paging
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "GvDetail":

                DropDownList ddlOrganization = (DropDownList)fvSearch.FindControl("ddlOrganization");
                DropDownList ddlWorkGroup = (DropDownList)fvSearch.FindControl("ddlWorkGroup");
                DropDownList ddlLineWork = (DropDownList)fvSearch.FindControl("ddlLineWork");
                DropDownList ddlDepartment = (DropDownList)fvSearch.FindControl("ddlDepartment");
                DropDownList ddlSection = (DropDownList)fvSearch.FindControl("ddlSection");
                DropDownList ddlPosition = (DropDownList)fvSearch.FindControl("ddlPosition");
                TextBox txt_empcode = (TextBox)fvSearch.FindControl("txt_empcode");


                var selectedEmpCode_ = new List<string>();
                string input_ = txt_empcode.Text;
                string[] recepempcode_ = input_.Split(',');
                foreach (string rempcode_ in recepempcode_)
                {
                    if (rempcode_ != String.Empty)
                    {
                        //mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                        selectedEmpCode_.Add(rempcode_);
                    }
                }


                pmsu0_DocFormDetail[] _templist_Detail = (pmsu0_DocFormDetail[])ViewState["vs_GvDetailIndex"];
                var _linqDataListDetail = (from dt in _templist_Detail

                                           where
                                           (int.Parse(ddlOrganization.SelectedValue) == 0 || dt.org_idx == int.Parse(ddlOrganization.SelectedValue))
                                           && (int.Parse(ddlWorkGroup.SelectedValue) == 0 || dt.wg_idx == int.Parse(ddlWorkGroup.SelectedValue))
                                           && (int.Parse(ddlLineWork.SelectedValue) == 0 || dt.lw_idx == int.Parse(ddlLineWork.SelectedValue))
                                           && (int.Parse(ddlDepartment.SelectedValue) == 0 || dt.rdept_idx == int.Parse(ddlDepartment.SelectedValue))
                                           && (int.Parse(ddlSection.SelectedValue) == 0 || dt.rsec_idx == int.Parse(ddlSection.SelectedValue))
                                           && (int.Parse(ddlPosition.SelectedValue) == 0 || dt.rpos_idx == int.Parse(ddlPosition.SelectedValue))
                                           && (selectedEmpCode_.Count == 0 || selectedEmpCode_.Contains(dt.emp_code))


                                           &&
                                           (
                                               (dt.cemp_idx == _emp_idx)
                                           || (dt.eval_emp_type == 3 && dt.eval_emp_idx == _emp_idx) //dotted
                                           || (dt.eval_emp_type == 2 && dt.eval_emp_idx == _emp_idx) //solid
                                           || (dt.eval_emp_type == 4 && dt.eval_emp_idx == _emp_idx) //approve 1
                                           || (dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                           || (dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3 

                                           )

                                           select new
                                           {
                                               dt.u0_docidx,
                                               dt.unidx,
                                               dt.acidx,
                                               dt.staidx,
                                               dt.sum_kpis_value,
                                               dt.sum_mine_core_value,
                                               dt.sum_solid_core_value,
                                               dt.sum_dotted_core_value,
                                               dt.sum_mine_competency,
                                               dt.sum_solid_competency,
                                               dt.sum_dotted_competency,
                                               dt.current_status,
                                               dt.org_idx,
                                               dt.wg_idx,
                                               dt.lw_idx,
                                               dt.org_name_th,
                                               dt.rdept_idx,
                                               dt.dept_name_th,
                                               dt.rsec_idx,
                                               dt.sec_name_th,
                                               dt.rpos_idx,
                                               dt.pos_name_th,
                                               dt.emp_name_th,
                                               dt.solid,
                                               dt.dotted,
                                               dt.pos_name,
                                               dt.jobgrade_level,
                                               dt.cemp_idx,
                                               dt.emp_code,
                                               dt.form_name,
                                               dt.u0_typeidx

                                           }
                                           ).Distinct().ToList();



                setGridData(GvDetail, _linqDataListDetail.ToList());
                //ViewState["Vs_Detail"]
                break;
        }
    }

    #endregion paging

    #region getdata
    protected void getPermissionUserCreate()
    {

        data_pms data_u0doc = new data_pms();
        pmsu0_DocFormDetail u0doc_detail = new pmsu0_DocFormDetail();
        data_u0doc.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0doc_detail.condition = 1;
        u0doc_detail.cemp_idx = _emp_idx;
        u0doc_detail.org_idx = int.Parse(ViewState["org_permission"].ToString());
        u0doc_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0doc_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString()); ;
        //u0doc_detail.jobgrade_level = jobgrade_level;

        data_u0doc.Boxpmsu0_DocFormDetail[0] = u0doc_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_u0doc = callServicePostPMS(_urlSelectSystem, data_u0doc);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc));

        ViewState["vs_DetailUserCreate"] = data_u0doc.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_DetailUserCreate"] != null)
        {

            pmsu0_DocFormDetail[] _templist_DetailUser = (pmsu0_DocFormDetail[])ViewState["vs_DetailUserCreate"];
            var _linqDataListUserDetail = (from dt in _templist_DetailUser
                                           where
                                           (dt.cemp_idx == _emp_idx)


                                           select new
                                           {
                                               dt.u0_docidx,
                                               dt.cemp_idx,


                                           }).Distinct().ToList();




            //setGridData(gvName, _linqDataListDetail);
            ViewState["vs_CheckUserCreate"] = _linqDataListUserDetail.Count();

            //litDebug.Text = ViewState["vs_CheckUserCreate"].ToString();

            //check user create performance
            if (int.Parse(ViewState["vs_CheckUserCreate"].ToString())  >= 1)
            {
                li1.Visible = false;

            }
            else
            {
                li1.Visible = true;

            }


        }
        else
        {

            ViewState["vs_CheckUserCreate"] = 0;
            //litDebug.Text = ViewState["vs_CheckUserCreate"].ToString();

        }


    }

    protected void getPoint(GridView gvName)
    {

        data_pms data_u0_formdetail = new data_pms();
        pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
        data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0_formdetail.condition = 8;

        data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

        data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);

        ViewState["vs_GvMasterPoint"] = data_u0_formdetail.Boxpmsm0_DocFormDetail;
        setGridData(gvName, ViewState["vs_GvMasterPoint"]);

    }

    protected void getCoreValue(GridView gvName)
    {

        data_pms data_u0_formdetail = new data_pms();
        pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
        data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0_formdetail.condition = 9;

        data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

        data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);

        ViewState["vs_GvCoreValue"] = data_u0_formdetail.Boxpmsm0_DocFormDetail;
        setGridData(gvName, ViewState["vs_GvCoreValue"]);



        //string returnWeightScore = "";

        //returnWeightScore = Convert.ToDouble(((Convert.ToDouble(kpi_weight) / 100) * Convert.ToDouble(_point))).ToString();
        //return returnWeightScore;


    }

    protected void getSubCoreValue(GridView gvName, int m1_coreidx)
    {


        data_pms data_u0_formdetail = new data_pms();
        pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
        data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0_formdetail.condition = 10;
        u0_formdetail.m1_coreidx = m1_coreidx;

        data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

        data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);

        ViewState["vs_GvSubCoreValue"] = data_u0_formdetail.Boxpmsm0_DocFormDetail;
        setGridData(gvName, ViewState["vs_GvSubCoreValue"]);

    }

    protected void getCompentency(GridView gvName, int posidx, int orgidx)
    {

        data_pms data_u0_formdetail = new data_pms();
        pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
        data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0_formdetail.condition = 11;


        data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

        //master
        pmsm0_DocFormDetail m0_formdetail = new pmsm0_DocFormDetail();
        data_u0_formdetail.Boxpmsm0_DocFormDetail = new pmsm0_DocFormDetail[1];

        m0_formdetail.orgidx = orgidx;
        m0_formdetail.posidx = posidx;

        data_u0_formdetail.Boxpmsm0_DocFormDetail[0] = m0_formdetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

        data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

        if (data_u0_formdetail.ReturnCode.ToString() == "0")
        {

            getCoreValue(GvCoreValue);
            setGridData(gvName, data_u0_formdetail.Boxpmsm0_DocFormDetail);


            ViewState["rtcode_form"] = data_u0_formdetail.ReturnCode.ToString();

            sumpoint_competencies = Convert.ToDecimal((Convert.ToDecimal(gvName.Rows.Count) * Convert.ToDecimal(4)));//Convert.ToDecimal(gvName.Rows.Count);
            //litDebug.Text = sumpoint_competencies.ToString();

            setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_cen_employee)ViewState["vsCenEmpProfile"]).cen_employee_list_u0);

            getIndividual(GvKPIs);
            getPoint(GvMasterPoint);
            

            GvFactor.Visible = true;
            getFactor(GvFactor);


            // //change posidx to employee group
            mergeCell(GvCoreValue);
            mergeCell(GvCompetencies);

            setFormData(FvComment, FormViewMode.Insert, null);
            setFormData(FvStar, FormViewMode.Insert, null);

            lblFormName.Text = ViewState["form_name"].ToString();

            getbtnDecision(rptBindbtnNode1, 1, 0);

        }
        else
        {

            // litDebug.Text = "TOEI";
            // li0.Attributes.Add("class", "active");
            getCountWaitApprove();

            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีแบบฟอร์มประเมิน กรุณาติดต่อทาง HR !!');", true);

        }



    }

    protected void getSubcompentency(GridView gvName, int u0typidx, int m1_typidx)
    {

        data_pms data_u0_formdetail = new data_pms();
        pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
        data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0_formdetail.condition = 12;
        u0_formdetail.u0_typeidx = u0typidx;
        u0_formdetail.m1_typeidx = m1_typidx;

        data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

        data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);
        setGridData(gvName, data_u0_formdetail.Boxpmsm0_DocFormDetail);

    }

    protected void getIndividual(GridView gvName)
    {

        data_pms data_u0_formdetail = new data_pms();
        pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
        data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0_formdetail.condition = 22;
        u0_formdetail.cemp_idx = _emp_idx;

        data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

        data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

        ViewState["vs_GvIndividual"] = data_u0_formdetail.Boxpmsm0_DocFormDetail;
        setGridData(gvName, ViewState["vs_GvIndividual"]);

    }

    protected void getbtnDecision(Repeater rptName, int _m0_node_idx, int _decision_idx)
    {

        data_pms data_u0_formdetail = new data_pms();
        pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
        data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0_formdetail.condition = 24;
        u0_formdetail.noidx = _m0_node_idx;

        data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);


        setRepeaterData(rptName, data_u0_formdetail.Boxpmsm0_DocFormDetail);

    }

    protected void getDetail(GridView gvName)
    {

        data_pms data_u0doc = new data_pms();
        pmsu0_DocFormDetail u0doc_detail = new pmsu0_DocFormDetail();
        data_u0doc.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0doc_detail.condition = 1;
        u0doc_detail.cemp_idx = _emp_idx;
        u0doc_detail.org_idx = int.Parse(ViewState["org_permission"].ToString());
        u0doc_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0doc_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString()); ;
        //u0doc_detail.jobgrade_level = jobgrade_level;

        data_u0doc.Boxpmsu0_DocFormDetail[0] = u0doc_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_u0doc = callServicePostPMS(_urlSelectSystem, data_u0doc);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc));


        ViewState["vs_GvDetailIndex"] = data_u0doc.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_GvDetailIndex"] != null)
        {

            pmsu0_DocFormDetail[] _templist_Detail = (pmsu0_DocFormDetail[])ViewState["vs_GvDetailIndex"];
            var _linqDataListDetail = (from dt in _templist_Detail
                                       where
                                       (dt.cemp_idx == _emp_idx)
                                       || (dt.eval_emp_type == 3 && dt.eval_emp_idx == _emp_idx) //dotted
                                       || (dt.eval_emp_type == 2 && dt.eval_emp_idx == _emp_idx) //solid
                                       || (dt.eval_emp_type == 4 && dt.eval_emp_idx == _emp_idx) //approve 1
                                       || (dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                       || (dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3

                                       select new
                                       {
                                           dt.u0_docidx,
                                           dt.unidx,
                                           dt.acidx,
                                           dt.staidx,
                                           dt.sum_kpis_value,
                                           dt.sum_mine_core_value,
                                           dt.sum_solid_core_value,
                                           dt.sum_dotted_core_value,
                                           dt.sum_mine_competency,
                                           dt.sum_solid_competency,
                                           dt.sum_dotted_competency,
                                           dt.current_status,
                                           dt.org_idx,
                                           dt.wg_idx,
                                           dt.lw_idx,
                                           dt.org_name_th,
                                           dt.rdept_idx,
                                           dt.dept_name_th,
                                           dt.rsec_idx,
                                           dt.sec_name_th,
                                           dt.rpos_idx,
                                           dt.pos_name_th,
                                           dt.emp_name_th,
                                           dt.solid,
                                           dt.dotted,
                                           dt.pos_name,
                                           dt.jobgrade_level,
                                           dt.cemp_idx,
                                           dt.emp_code,
                                           dt.form_name,
                                           dt.u0_typeidx

                                       }
                                       ).Distinct().ToList();



            //((data_pms) ViewState["Vs_Detail"]).pmsu0_DocFormDetail = _linqDataListDetail.ToList();
            //ViewState["vs_GvDetailIndex"] = _linqDataListDetail.ToList();
            setGridData(gvName, _linqDataListDetail.ToList());
            //setGridData(gvName, ViewState["Vs_Detail"]);
            //ViewState["vs_GvDetailIndexAll"] = _linqDataListDetail.ToList();



        }
        else
        {
            ViewState["vs_GvDetailIndex"] = null;


            setGridData(gvName, ViewState["vs_GvDetailIndex"]);

        }

    }

    protected void getSearchDetailIndex()
    {

        DropDownList ddlOrganization = (DropDownList)fvSearch.FindControl("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList)fvSearch.FindControl("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList)fvSearch.FindControl("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList)fvSearch.FindControl("ddlDepartment");
        DropDownList ddlSection = (DropDownList)fvSearch.FindControl("ddlSection");
        DropDownList ddlPosition = (DropDownList)fvSearch.FindControl("ddlPosition");
        TextBox txt_empcode = (TextBox)fvSearch.FindControl("txt_empcode");


        var selectedEmpCode_ = new List<string>();
        string input_ = txt_empcode.Text;
        string[] recepempcode_ = input_.Split(',');
        foreach (string rempcode_ in recepempcode_)
        {
            if (rempcode_ != String.Empty)
            {
                //mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                selectedEmpCode_.Add(rempcode_);
            }
        }



        data_pms data_u0doc = new data_pms();
        pmsu0_DocFormDetail u0doc_detail = new pmsu0_DocFormDetail();
        data_u0doc.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0doc_detail.condition = 1;
        u0doc_detail.cemp_idx = _emp_idx;
        u0doc_detail.org_idx = int.Parse(ViewState["org_permission"].ToString());
        u0doc_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0doc_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString()); ;
        //u0doc_detail.jobgrade_level = jobgrade_level;

        data_u0doc.Boxpmsu0_DocFormDetail[0] = u0doc_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_u0doc = callServicePostPMS(_urlSelectSystem, data_u0doc);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc));

        ViewState["vs_GvDetailIndexALL"] = data_u0doc.Boxpmsu0_DocFormDetail;
        ViewState["vs_GvDetailIndex"] = data_u0doc.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_GvDetailIndex"] != null)
        {


            pmsu0_DocFormDetail[] _templist_Detail = (pmsu0_DocFormDetail[])ViewState["vs_GvDetailIndexALL"];
            var _linqDataListDetail = (from dt in _templist_Detail
                                       where
                                       (int.Parse(ddlOrganization.SelectedValue) == 0 || dt.org_idx == int.Parse(ddlOrganization.SelectedValue))
                                       && (int.Parse(ddlWorkGroup.SelectedValue) == 0 || dt.wg_idx == int.Parse(ddlWorkGroup.SelectedValue))
                                       && (int.Parse(ddlLineWork.SelectedValue) == 0 || dt.lw_idx == int.Parse(ddlLineWork.SelectedValue))
                                       && (int.Parse(ddlDepartment.SelectedValue) == 0 || dt.rdept_idx == int.Parse(ddlDepartment.SelectedValue))
                                       && (int.Parse(ddlSection.SelectedValue) == 0 || dt.rsec_idx == int.Parse(ddlSection.SelectedValue))
                                       && (int.Parse(ddlPosition.SelectedValue) == 0 || dt.rpos_idx == int.Parse(ddlPosition.SelectedValue))
                                       && (selectedEmpCode_.Count == 0 || selectedEmpCode_.Contains(dt.emp_code))


                                       &&
                                       (
                                           (dt.cemp_idx == _emp_idx)
                                       || (dt.eval_emp_type == 3 && dt.eval_emp_idx == _emp_idx) //dotted
                                       || (dt.eval_emp_type == 2 && dt.eval_emp_idx == _emp_idx) //solid
                                       || (dt.eval_emp_type == 4 && dt.eval_emp_idx == _emp_idx) //approve 1
                                       || (dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                       || (dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3 

                                       )

                                       select new
                                       {
                                           dt.u0_docidx,
                                           dt.unidx,
                                           dt.acidx,
                                           dt.staidx,
                                           dt.sum_kpis_value,
                                           dt.sum_mine_core_value,
                                           dt.sum_solid_core_value,
                                           dt.sum_dotted_core_value,
                                           dt.sum_mine_competency,
                                           dt.sum_solid_competency,
                                           dt.sum_dotted_competency,
                                           dt.current_status,
                                           dt.org_idx,
                                           dt.wg_idx,
                                           dt.lw_idx,
                                           dt.org_name_th,
                                           dt.rdept_idx,
                                           dt.dept_name_th,
                                           dt.rsec_idx,
                                           dt.sec_name_th,
                                           dt.rpos_idx,
                                           dt.pos_name_th,
                                           dt.emp_name_th,
                                           dt.solid,
                                           dt.dotted,
                                           dt.pos_name,
                                           dt.jobgrade_level,
                                           dt.cemp_idx,
                                           dt.emp_code,
                                           dt.form_name,
                                           dt.u0_typeidx

                                       }).Distinct().ToList();

            //ViewState["Vs_Detail"] = _linqDataListDetail.ToList();
            setGridData(GvDetail, _linqDataListDetail.ToList());
            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_linqDataListDetail));



        }
        else
        {
            ViewState["vs_GvDetailIndex"] = null;


            setGridData(GvDetail, ViewState["vs_GvDetailIndex"]);

        }


    }

    protected void getDetailWaitApprove(GridView gvName)
    {

        data_pms data_u0doc = new data_pms();
        pmsu0_DocFormDetail u0doc_detail = new pmsu0_DocFormDetail();
        data_u0doc.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0doc_detail.condition = 1;
        u0doc_detail.cemp_idx = _emp_idx;
        u0doc_detail.org_idx = int.Parse(ViewState["org_permission"].ToString());
        u0doc_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0doc_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString()); ;
        //u0doc_detail.jobgrade_level = jobgrade_level;

        data_u0doc.Boxpmsu0_DocFormDetail[0] = u0doc_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_u0doc = callServicePostPMS(_urlSelectSystem, data_u0doc);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc));

        ViewState["vs_GvDetailApprove"] = data_u0doc.Boxpmsu0_DocFormDetail;
        //ViewState["vs_GvDetailIndex"] = data_u0doc.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_GvDetailApprove"] != null)
        {

            pmsu0_DocFormDetail[] _templistApprove_Detail = (pmsu0_DocFormDetail[])ViewState["vs_GvDetailApprove"];
            var _linqDataListApproveDetail = (from dt in _templistApprove_Detail
                                              where
                                              (dt.unidx == 2 && dt.eval_emp_type == 3 && dt.eval_emp_idx == _emp_idx) //dotted
                                              || (dt.unidx == 3 && dt.eval_emp_type == 2 && dt.eval_emp_idx == _emp_idx) //solid
                                              || (dt.unidx == 4 && dt.eval_emp_type == 4 && dt.eval_emp_idx == _emp_idx) //approve 1
                                              || (dt.unidx == 5 && dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                              || (dt.unidx == 6 && dt.staidx != 2 && dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3

                                              select new
                                              {
                                                  dt.u0_docidx,
                                                  dt.unidx,
                                                  dt.acidx,
                                                  dt.staidx,
                                                  dt.sum_kpis_value,
                                                  dt.sum_mine_core_value,
                                                  dt.sum_solid_core_value,
                                                  dt.sum_dotted_core_value,
                                                  dt.sum_mine_competency,
                                                  dt.sum_solid_competency,
                                                  dt.sum_dotted_competency,
                                                  dt.current_status,
                                                  dt.org_idx,
                                                  dt.org_name_th,
                                                  dt.rdept_idx,
                                                  dt.dept_name_th,
                                                  dt.rsec_idx,
                                                  dt.sec_name_th,
                                                  dt.rpos_idx,
                                                  dt.pos_name_th,
                                                  dt.emp_name_th,
                                                  dt.solid,
                                                  dt.dotted,
                                                  dt.pos_name,
                                                  dt.jobgrade_level,
                                                  dt.cemp_idx,
                                                  dt.emp_code,
                                                  dt.form_name,
                                                  dt.u0_typeidx

                                              }).Distinct().ToList();



            setGridData(gvName, _linqDataListApproveDetail);


        }
        else
        {
            ViewState["vs_GvDetailApprove"] = null;



            setGridData(gvName, ViewState["vs_GvDetailApprove"]);

        }

    }

    protected void getSearchDetailWaitApprove()
    {

        DropDownList ddlOrganization = (DropDownList)fvSearchApprove.FindControl("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList)fvSearchApprove.FindControl("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList)fvSearchApprove.FindControl("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList)fvSearchApprove.FindControl("ddlDepartment");
        DropDownList ddlSection = (DropDownList)fvSearchApprove.FindControl("ddlSection");
        DropDownList ddlPosition = (DropDownList)fvSearchApprove.FindControl("ddlPosition");

        TextBox txt_empcode = (TextBox)fvSearchApprove.FindControl("txt_empcode");


        var selectedEmpCode_ = new List<string>();
        string input_ = txt_empcode.Text;
        string[] recepempcode_ = input_.Split(',');
        foreach (string rempcode_ in recepempcode_)
        {
            if (rempcode_ != String.Empty)
            {
                //mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                selectedEmpCode_.Add(rempcode_);
            }
        }

        data_pms data_u0doc = new data_pms();
        pmsu0_DocFormDetail u0doc_detail = new pmsu0_DocFormDetail();
        data_u0doc.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0doc_detail.condition = 1;
        u0doc_detail.cemp_idx = _emp_idx;
        u0doc_detail.org_idx = int.Parse(ViewState["org_permission"].ToString());
        u0doc_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0doc_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString()); ;
        //u0doc_detail.jobgrade_level = jobgrade_level;

        data_u0doc.Boxpmsu0_DocFormDetail[0] = u0doc_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_u0doc = callServicePostPMS(_urlSelectSystem, data_u0doc);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc));

        ViewState["vs_GvDetailApproveAll"] = data_u0doc.Boxpmsu0_DocFormDetail;
        //ViewState["vs_GvDetailIndex"] = data_u0doc.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_GvDetailApprove"] != null)
        {

            pmsu0_DocFormDetail[] _templistApprove_Detail = (pmsu0_DocFormDetail[])ViewState["vs_GvDetailApproveAll"];
            var _linqDataListApproveDetail = (from dt in _templistApprove_Detail
                                              where
                                              (int.Parse(ddlOrganization.SelectedValue) == 0 || dt.org_idx == int.Parse(ddlOrganization.SelectedValue))
                                              && (int.Parse(ddlWorkGroup.SelectedValue) == 0 || dt.wg_idx == int.Parse(ddlWorkGroup.SelectedValue))
                                              && (int.Parse(ddlLineWork.SelectedValue) == 0 || dt.lw_idx == int.Parse(ddlLineWork.SelectedValue))
                                              && (int.Parse(ddlDepartment.SelectedValue) == 0 || dt.rdept_idx == int.Parse(ddlDepartment.SelectedValue))
                                              && (int.Parse(ddlSection.SelectedValue) == 0 || dt.rsec_idx == int.Parse(ddlSection.SelectedValue))
                                              && (int.Parse(ddlPosition.SelectedValue) == 0 || dt.rpos_idx == int.Parse(ddlPosition.SelectedValue))
                                              && (selectedEmpCode_.Count == 0 || selectedEmpCode_.Contains(dt.emp_code))

                                              &&
                                              (
                                                (dt.unidx == 2 && dt.eval_emp_type == 3 && dt.eval_emp_idx == _emp_idx) //dotted
                                                || (dt.unidx == 3 && dt.eval_emp_type == 2 && dt.eval_emp_idx == _emp_idx) //solid
                                                || (dt.unidx == 4 && dt.eval_emp_type == 4 && dt.eval_emp_idx == _emp_idx) //approve 1
                                                || (dt.unidx == 5 && dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                                || (dt.unidx == 6 && dt.staidx != 2 && dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3

                                              )


                                              select new
                                              {
                                                  dt.u0_docidx,
                                                  dt.unidx,
                                                  dt.acidx,
                                                  dt.staidx,
                                                  dt.sum_kpis_value,
                                                  dt.sum_mine_core_value,
                                                  dt.sum_solid_core_value,
                                                  dt.sum_dotted_core_value,
                                                  dt.sum_mine_competency,
                                                  dt.sum_solid_competency,
                                                  dt.sum_dotted_competency,
                                                  dt.current_status,
                                                  dt.org_idx,
                                                  dt.org_name_th,
                                                  dt.rdept_idx,
                                                  dt.dept_name_th,
                                                  dt.rsec_idx,
                                                  dt.sec_name_th,
                                                  dt.rpos_idx,
                                                  dt.pos_name_th,
                                                  dt.emp_name_th,
                                                  dt.solid,
                                                  dt.dotted,
                                                  dt.pos_name,
                                                  dt.jobgrade_level,
                                                  dt.cemp_idx,
                                                  dt.emp_code,
                                                  dt.form_name,
                                                  dt.u0_typeidx

                                              }).Distinct().ToList();

            setGridData(GvApprove, _linqDataListApproveDetail);


        }
        else
        {
            ViewState["vs_GvDetailApprove"] = null;


            setGridData(GvApprove, ViewState["vs_GvDetailApprove"]);

        }

    }

    protected void getDetailCheckPerformance(GridView gvName)
    {

        data_pms data_u0doc = new data_pms();
        pmsu0_DocFormDetail u0doc_detail = new pmsu0_DocFormDetail();
        data_u0doc.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0doc_detail.condition = 1;
        u0doc_detail.cemp_idx = _emp_idx;
        u0doc_detail.org_idx = int.Parse(ViewState["org_permission"].ToString());
        u0doc_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0doc_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString()); ;
        //u0doc_detail.jobgrade_level = jobgrade_level;

        data_u0doc.Boxpmsu0_DocFormDetail[0] = u0doc_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_u0doc = callServicePostPMS(_urlSelectSystem, data_u0doc);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc));

        ViewState["vs_GvCheckPerformance"] = data_u0doc.Boxpmsu0_DocFormDetail;
        //ViewState["vs_GvDetailIndex"] = data_u0doc.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_GvCheckPerformance"] != null)
        {

            pmsu0_DocFormDetail[] _templistApprove_Detail = (pmsu0_DocFormDetail[])ViewState["vs_GvCheckPerformance"];
            var _linqDataListApproveDetail = (from dt in _templistApprove_Detail
                                              where
                                              dt.staidx == 2
                                              select new
                                              {
                                                  dt.u0_docidx,
                                                  dt.unidx,
                                                  dt.acidx,
                                                  dt.staidx,
                                                  dt.sum_kpis_value,
                                                  dt.sum_mine_core_value,
                                                  dt.sum_solid_core_value,
                                                  dt.sum_dotted_core_value,
                                                  dt.sum_mine_competency,
                                                  dt.sum_solid_competency,
                                                  dt.sum_dotted_competency,
                                                  dt.current_status,
                                                  dt.org_idx,
                                                  dt.org_name_th,
                                                  dt.rdept_idx,
                                                  dt.dept_name_th,
                                                  dt.rsec_idx,
                                                  dt.sec_name_th,
                                                  dt.rpos_idx,
                                                  dt.pos_name_th,
                                                  dt.emp_name_th,
                                                  dt.solid,
                                                  dt.dotted,
                                                  dt.pos_name,
                                                  dt.jobgrade_level,
                                                  dt.cemp_idx,
                                                  dt.form_name,
                                                  dt.u0_typeidx

                                              }).Distinct().ToList();

            setGridData(gvName, _linqDataListApproveDetail);


        }
        else
        {
            ViewState["vs_GvCheckPerformance"] = null;


            setGridData(gvName, ViewState["vs_GvCheckPerformance"]);

        }

    }

    protected void getSearchDetailCheckPerformance()
    {

        TextBox txt_empcode = (TextBox)fvSearchCheckPerformance.FindControl("txt_empcode");


        var selectedEmpCode_ = new List<string>();
        string input_ = txt_empcode.Text;
        string[] recepempcode_ = input_.Split(',');
        foreach (string rempcode_ in recepempcode_)
        {
            if (rempcode_ != String.Empty)
            {
                //mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                selectedEmpCode_.Add(rempcode_);
            }
        }

        data_pms data_u0doc = new data_pms();
        pmsu0_DocFormDetail u0doc_detail = new pmsu0_DocFormDetail();
        data_u0doc.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0doc_detail.condition = 1;
        u0doc_detail.cemp_idx = _emp_idx;
        u0doc_detail.org_idx = int.Parse(ViewState["org_permission"].ToString());
        u0doc_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0doc_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString()); ;
        //u0doc_detail.jobgrade_level = jobgrade_level;

        data_u0doc.Boxpmsu0_DocFormDetail[0] = u0doc_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_u0doc = callServicePostPMS(_urlSelectSystem, data_u0doc);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc));

        ViewState["vs_GvCheckPerformanceAll"] = data_u0doc.Boxpmsu0_DocFormDetail;
        //ViewState["vs_GvDetailIndex"] = data_u0doc.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_GvCheckPerformance"] != null)
        {

            pmsu0_DocFormDetail[] _templistApprove_Detail = (pmsu0_DocFormDetail[])ViewState["vs_GvCheckPerformanceAll"];
            var _linqDataListApproveDetail = (from dt in _templistApprove_Detail
                                              where
                                              dt.staidx == 2
                                              && (selectedEmpCode_.Count == 0 || selectedEmpCode_.Contains(dt.emp_code))
                                              select new
                                              {
                                                  dt.u0_docidx,
                                                  dt.unidx,
                                                  dt.acidx,
                                                  dt.staidx,
                                                  dt.sum_kpis_value,
                                                  dt.sum_mine_core_value,
                                                  dt.sum_solid_core_value,
                                                  dt.sum_dotted_core_value,
                                                  dt.sum_mine_competency,
                                                  dt.sum_solid_competency,
                                                  dt.sum_dotted_competency,
                                                  dt.current_status,
                                                  dt.org_idx,
                                                  dt.org_name_th,
                                                  dt.rdept_idx,
                                                  dt.dept_name_th,
                                                  dt.rsec_idx,
                                                  dt.sec_name_th,
                                                  dt.rpos_idx,
                                                  dt.pos_name_th,
                                                  dt.emp_name_th,
                                                  dt.solid,
                                                  dt.dotted,
                                                  dt.pos_name,
                                                  dt.jobgrade_level,
                                                  dt.cemp_idx,
                                                  dt.emp_code,
                                                  dt.form_name,
                                                  dt.u0_typeidx

                                              }).Distinct().ToList();

            setGridData(GvCheckPerformance, _linqDataListApproveDetail);


        }
        else
        {
            ViewState["vs_GvCheckPerformance"] = null;


            setGridData(GvCheckPerformance, ViewState["vs_GvCheckPerformance"]);

        }

    }

    protected void getCheckPermissionDetailIndex()
    {

        data_pms data_u0doc = new data_pms();
        pmsu0_DocFormDetail u0doc_detail = new pmsu0_DocFormDetail();
        data_u0doc.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0doc_detail.condition = 1;
        u0doc_detail.cemp_idx = _emp_idx;
        u0doc_detail.org_idx = int.Parse(ViewState["org_permission"].ToString());
        u0doc_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0doc_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString()); ;
        //u0doc_detail.jobgrade_level = jobgrade_level;

        data_u0doc.Boxpmsu0_DocFormDetail[0] = u0doc_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_u0doc = callServicePostPMS(_urlSelectSystem, data_u0doc);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc));

        ViewState["vs_CheckPermissionDetail"] = data_u0doc.Boxpmsu0_DocFormDetail;


    }

    protected void getCountWaitApprove()
    {

        data_pms data_u0doc = new data_pms();
        pmsu0_DocFormDetail u0doc_detail = new pmsu0_DocFormDetail();
        data_u0doc.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0doc_detail.condition = 1;
        u0doc_detail.cemp_idx = _emp_idx;
        u0doc_detail.org_idx = int.Parse(ViewState["org_permission"].ToString());
        u0doc_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0doc_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString()); ;
        //u0doc_detail.jobgrade_level = jobgrade_level;

        data_u0doc.Boxpmsu0_DocFormDetail[0] = u0doc_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_u0doc = callServicePostPMS(_urlSelectSystem, data_u0doc);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc));

        ViewState["vsCount_GvDetailApprove"] = data_u0doc.Boxpmsu0_DocFormDetail;
        //ViewState["vs_GvDetailIndex"] = data_u0doc.Boxpmsu0_DocFormDetail;

        if (ViewState["vsCount_GvDetailApprove"] != null)
        {

            pmsu0_DocFormDetail[] _templistApprove_CountDetail = (pmsu0_DocFormDetail[])ViewState["vsCount_GvDetailApprove"];
            var _linqDataListApproveDetail_Count = (from dt in _templistApprove_CountDetail
                                                    where

                                                    (dt.unidx == 2 && dt.eval_emp_type == 3 && dt.eval_emp_idx == _emp_idx) //dotted
                                                    || (dt.unidx == 3 && dt.eval_emp_type == 2 && dt.eval_emp_idx == _emp_idx) //solid
                                                    || (dt.unidx == 4 && dt.eval_emp_type == 4 && dt.eval_emp_idx == _emp_idx) //approve 1
                                                    || (dt.unidx == 5 && dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                                    || (dt.unidx == 6 && dt.staidx != 2 && dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3

                                                    select dt).Distinct().ToList();

            //setGridData(gvName, _linqDataListApproveDetail);

            //set permission tab approve can approve

            if (_linqDataListApproveDetail_Count.Count() > 0)
            {
                set_permission_tab = 1;

                //ViewState["vs_CheckPermission_Detail"] = _linqDataListApproveDetail_Count.Count();


                lbApprove.Text = " รายการรออนุมัติ <span class='badge progress-bar-danger' >" + _linqDataListApproveDetail_Count.Count().ToString() + "</span>";
                setActiveTab("docApprove", 0, 0, 0);

            }
            else
            {
                //set_permission_tab = 1;
                lbApprove.Text = " รายการรออนุมัติ <span class='badge progress-bar-danger' >" + "0" + "</span>";
                setActiveTab("docDetail", 0, 0, 0);

            }

        }
        else
        {

            set_permission_tab = 0; //set permission tab approve

            lbApprove.Text = " รายการรออนุมัติ <span class='badge progress-bar-danger' >" + "0" + "</span>";
            setActiveTab("docDetail", 0, 0, 0);


        }

    }

    protected void getViewCoreValue(GridView gvName, int u0_docidx)
    {


        data_pms data_corevalue = new data_pms();
        pmsu0_DocFormDetail u1corevalue_detail = new pmsu0_DocFormDetail();
        data_corevalue.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];


        u1corevalue_detail.condition = 2;
        u1corevalue_detail.u0_docidx = u0_docidx;

        data_corevalue.Boxpmsu0_DocFormDetail[0] = u1corevalue_detail;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_corevalue = callServicePostPMS(_urlSelectSystem, data_corevalue);
        setGridData(gvName, data_corevalue.Boxpmsu1_DocFormDetail);

    }

    protected void getViewcompetencise(GridView gvName, int u0_docidx, int tidx, int u0_typeidx)
    {


        data_pms data_corevalue = new data_pms();
        pmsu0_DocFormDetail u1corevalue_detail = new pmsu0_DocFormDetail();
        data_corevalue.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];


        u1corevalue_detail.condition = 3;
        u1corevalue_detail.u0_docidx = u0_docidx;
        u1corevalue_detail.TIDX = tidx;
        u1corevalue_detail.u0_typeidx = u0_typeidx;
        u1corevalue_detail.org_idx = int.Parse(ViewState["org_idx_detail"].ToString());

        data_corevalue.Boxpmsu0_DocFormDetail[0] = u1corevalue_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_corevalue));

        data_corevalue = callServicePostPMS(_urlSelectSystem, data_corevalue);
        setGridData(gvName, data_corevalue.Boxpmsu1_DocFormDetail);

    }

    protected void getViewDevStarDetail(GridView gvName, int u0_docidx, int m0_typeidx)
    {

        data_pms data_comment = new data_pms();
        pmsu0_DocFormDetail udoccomment_detail = new pmsu0_DocFormDetail();
        data_comment.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        udoccomment_detail.condition = 4;
        udoccomment_detail.u0_docidx = u0_docidx;
        udoccomment_detail.m0_typeidx = m0_typeidx;

        data_comment.Boxpmsu0_DocFormDetail[0] = udoccomment_detail;

        data_comment = callServicePostPMS(_urlSelectSystem, data_comment);

        //ViewState["vs_ViewDevStarDetail"] = data_comment.Boxpmsu2_DocFormDetail;
        setGridData(gvName, data_comment.Boxpmsu2_DocFormDetail);


    }

    protected void getViewIndividual(GridView gvName)
    {

        data_pms data_u0_formdetail = new data_pms();
        pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
        data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0_formdetail.condition = 25;
        u0_formdetail.cemp_idx = int.Parse(ViewState["cemp_idx_detail"].ToString());

        data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

        data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

        ViewState["vs_ViewGvIndividual"] = data_u0_formdetail.Boxpmsm0_DocFormDetail;
        setGridData(gvName, ViewState["vs_ViewGvIndividual"]);

    }

    protected void getLog(Repeater rpName, int u0_docidx)
    {

        data_pms data_log = new data_pms();
        pmsu0_DocFormDetail u0log_detail = new pmsu0_DocFormDetail();
        data_log.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0log_detail.condition = 5;
        u0log_detail.u0_docidx = u0_docidx;

        data_log.Boxpmsu0_DocFormDetail[0] = u0log_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_log));

        data_log = callServicePostPMS(_urlSelectSystem, data_log);
        setRepeaterData(rpName, data_log.Boxpmsu0_DocFormDetail);


    }

    protected void getFactor(GridView gvName)
    {

        data_pms data_factor = new data_pms();
        data_pms data_factor_insert = new data_pms();

        pmsu0_DocFormDetail u0factor_detail = new pmsu0_DocFormDetail();
        data_factor.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0factor_detail.condition = 6;
        u0factor_detail.org_idx = int.Parse(ViewState["org_permission"].ToString());
        u0factor_detail.group_idx = int.Parse(ViewState["empgroup_idx"].ToString()); //change

        data_factor.Boxpmsu0_DocFormDetail[0] = u0factor_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        data_factor = callServicePostPMS(_urlSelectSystem, data_factor);
        ViewState["vs_ViewGvFactor"] = data_factor.Boxpmsm0_factor_list;

        //set topic name
        string[] setFactorName = set_FactorName.ToString().Split(',');

        //set weight
        set_calFactor = data_factor.Boxpmsm0_factor_list[0].cal_detail.ToString();
        string[] setcalFactor = set_calFactor.Split(',');

        //set score before
        //set_scoreBefore = tot_sumpoint_kpi.ToString() + "," + "0.00" + "," + "0.00" + "," + "0.00";
        string[] setscoreBefore = set_scoreBefore.ToString().Split(',');

        var m0_factor = new pmsm0_factorDetail[setcalFactor.Length];
        for (int k = 0; k < setcalFactor.Length; k++)
        {

            data_factor_insert.Boxpmsm0_factor_list = new pmsm0_factorDetail[1];
            m0_factor[k] = new pmsm0_factorDetail();

            m0_factor[k].factor_name = setFactorName[k].ToString();
            m0_factor[k].cal_detail = setcalFactor[k].ToString();
            m0_factor[k].score_before = setscoreBefore[k].ToString();
            //m0_factor[k].score_after = "0.00";

            data_factor_insert.Boxpmsm0_factor_list = m0_factor;

        }

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        setGridData(gvName, data_factor_insert.Boxpmsm0_factor_list);

    }

    protected void getViewFactor(GridView gvName)
    {

        data_pms data_factor = new data_pms();
        data_pms data_factor_insert = new data_pms();

        pmsu0_DocFormDetail u0factor_detail = new pmsu0_DocFormDetail();
        data_factor.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0factor_detail.condition = 6;
        u0factor_detail.org_idx = int.Parse(ViewState["org_permission"].ToString());
        u0factor_detail.group_idx = int.Parse(ViewState["empgroup_idx"].ToString()); //change

        data_factor.Boxpmsu0_DocFormDetail[0] = u0factor_detail;

        data_factor = callServicePostPMS(_urlSelectSystem, data_factor);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        //ViewState["vs_ViewGvFactor"] = data_factor.Boxpmsm0_factor_list;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        //set topic name
        string[] setFactorName = set_FactorName.ToString().Split(',');

        //set weight
        set_calFactor = data_factor.Boxpmsm0_factor_list[0].cal_detail.ToString();
        string[] setcalFactor = set_calFactor.Split(',');

        //set score before
        //set_scoreBefore = tot_sumpoint_kpi.ToString() + "," + "0.00" + "," + "0.00" + "," + "0.00";
        string[] setscoreBefore = set_scoreBefore.ToString().Split(',');

        //dotted
        string[] setscoreDottedBefore = set_scoreDottedBefore.ToString().Split(',');

        //solid
        string[] setscoreSolidBefore = set_scoreSolidBefore.ToString().Split(',');

        //total
        //string[] setscoreAfter = set_scoreAfter.ToString().Split(',');

        // litDebug.Text = ViewState["dotted_detail"].ToString() + "|" + ViewState["solid_detail"].ToString() + "| user" + set_scoreBefore.ToString() + "| Dotted" + set_scoreDottedBefore.ToString() + "| Solid" + set_scoreSolidBefore.ToString();

        var m0_factor = new pmsm0_factorDetail[setcalFactor.Length];
        for (int k = 0; k < setcalFactor.Length; k++)
        {

            data_factor_insert.Boxpmsm0_factor_list = new pmsm0_factorDetail[1];
            m0_factor[k] = new pmsm0_factorDetail();

            m0_factor[k].factor_name = setFactorName[k].ToString();
            m0_factor[k].cal_detail = setcalFactor[k].ToString();
            m0_factor[k].score_before = setscoreBefore[k].ToString();

            // ViewState["solid_detail"] = arg_detail[9];
            // ViewState["dotted_detail"] = arg_detail[10]; 


            if (int.Parse(ViewState["unidx_detail"].ToString()) > 1)
            {

                if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0) //have dotted
                {

                    m0_factor[k].score_dotted_before = Convert.ToDouble(((Convert.ToDouble(setscoreDottedBefore[k].ToString()))
                        * (Convert.ToDouble(ViewState["dotted_detail"].ToString()) / Convert.ToDouble(100)))).ToString();

                    // if (k < 3)
                    // {

                    //     m0_factor[k].score_dotted_before = Convert.ToDouble(((Convert.ToDouble(setscoreDottedBefore[k].ToString()))
                    //     * (Convert.ToDouble(ViewState["dotted_detail"].ToString()) / Convert.ToDouble(100)))).ToString();
                    // }
                    // else
                    // {

                    //     m0_factor[k].score_dotted_before = Convert.ToDouble(((Convert.ToDouble(setscoreDottedBefore[k].ToString()))
                    //     * (Convert.ToDouble(ViewState["dotted_detail"].ToString()) / Convert.ToDouble(100)))).ToString();

                    // }

                }
                else // no dotted
                {

                    m0_factor[k].score_dotted_before = Convert.ToDouble(0.00).ToString();

                }

            }

            if (int.Parse(ViewState["unidx_detail"].ToString()) > 2)
            {



                if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)//have dotted
                {


                    m0_factor[k].score_solid_before = Convert.ToDouble(((Convert.ToDouble(setscoreSolidBefore[k].ToString()))
                  * (Convert.ToDouble(ViewState["solid_detail"].ToString()) / Convert.ToDouble(100)))).ToString();
                    //     if (k < 3)
                    //     {
                    //         m0_factor[k].score_solid_before = Convert.ToDouble(((Convert.ToDouble(setscoreSolidBefore[k].ToString()))
                    //    * (Convert.ToDouble(ViewState["solid_detail"].ToString()) / Convert.ToDouble(100)))).ToString();

                    //     }
                    //     else
                    //     {

                    //         m0_factor[k].score_solid_before = Convert.ToDouble(((Convert.ToDouble(setscoreSolidBefore[k].ToString()))
                    //    * (Convert.ToDouble(ViewState["solid_detail"].ToString()) / Convert.ToDouble(100)))).ToString();

                    //     }


                }
                else //no dotted
                {
                    if (k < 3)
                    {
                        m0_factor[k].score_solid_before = Convert.ToDouble(((Convert.ToDouble(setscoreSolidBefore[k].ToString()))
                    * (Convert.ToDouble(ViewState["solid_detail"].ToString()) / Convert.ToDouble(100)))).ToString();

                    }
                    else
                    {
                        // / Convert.ToDouble(2)

                        m0_factor[k].score_solid_before = Convert.ToDouble(((Convert.ToDouble(setscoreSolidBefore[k].ToString())))).ToString();

                    }

                }

                m0_factor[k].score_after = Convert.ToDouble(((Convert.ToDouble(m0_factor[k].score_dotted_before.ToString()) + Convert.ToDouble(m0_factor[k].score_solid_before.ToString())) * (Convert.ToDouble(m0_factor[k].cal_detail.ToString()) / Convert.ToDouble(100)))).ToString();


            }


            data_factor_insert.Boxpmsm0_factor_list = m0_factor;


        }
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor_insert));

        setGridData(gvName, data_factor_insert.Boxpmsm0_factor_list);

    }

    protected void getTimePerformance(GridView gvName)
    {

        data_pms data_time = new data_pms();
        data_pms data_timeperformance = new data_pms();


        pmsu0_DocFormDetail u0time_detail = new pmsu0_DocFormDetail();
        data_time.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];


        u0time_detail.condition = 8;
        u0time_detail.cemp_idx = _emp_idx;

        data_time.Boxpmsu0_DocFormDetail[0] = u0time_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        data_time = callServicePostPMS(_urlSelectSystem, data_time);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_time));

        // string set_timescore_performance = "";
        // string set_score_performance = "";

        //set topic name

        if (data_time.ReturnCode == "0")
        {

            string[] setTimePerformance = set_TimePerformance.ToString().Split(',');

            set_timescore_performance = data_time.pms_time_list_m0[0].time_score_detail.ToString();
            string[] settimescore_performance = set_timescore_performance.Split(',');

            set_score_performance = data_time.pms_time_list_m0[0].score_detail.ToString();
            string[] setscore_performance = set_score_performance.Split(',');

            var m0_timeperformance = new pms_time_detail_m0[setTimePerformance.Length];


            for (int k = 0; k < setTimePerformance.Length; k++)
            {

                data_timeperformance.pms_time_list_m0 = new pms_time_detail_m0[1];
                m0_timeperformance[k] = new pms_time_detail_m0();

                m0_timeperformance[k].time_performance_name = setTimePerformance[k].ToString();
                m0_timeperformance[k].time_score_detail = settimescore_performance[k].ToString();
                m0_timeperformance[k].score_detail = setscore_performance[k].ToString();
                //m0_factor[k].score_after = "0.00";

                data_timeperformance.pms_time_list_m0 = m0_timeperformance;

            }

            setGridData(gvName, data_timeperformance.pms_time_list_m0);

        }



    }

    protected void getViewTimePerformance(GridView gvName)
    {

        data_pms data_time = new data_pms();
        data_pms data_timeperformance = new data_pms();


        pmsu0_DocFormDetail u0time_detail = new pmsu0_DocFormDetail();
        data_time.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];


        u0time_detail.condition = 8;
        u0time_detail.cemp_idx = int.Parse(ViewState["cemp_idx_detail"].ToString());//_emp_idx;

        data_time.Boxpmsu0_DocFormDetail[0] = u0time_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        data_time = callServicePostPMS(_urlSelectSystem, data_time);

        // string set_timescore_performance = "";
        // string set_score_performance = "";

        if (data_time.ReturnCode == "0")
        {

            //set topic name
            string[] setTimePerformance = set_TimePerformance.ToString().Split(',');

            set_timescore_performance = data_time.pms_time_list_m0[0].time_score_detail.ToString();
            string[] settimescore_performance = set_timescore_performance.Split(',');

            set_score_performance = data_time.pms_time_list_m0[0].score_detail.ToString();
            string[] setscore_performance = set_score_performance.Split(',');

            var m0_timeperformance = new pms_time_detail_m0[setTimePerformance.Length];

            //time
            set_timeAttendanceScore = data_time.pms_time_list_m0[0].total_score.ToString();

            for (int k = 0; k < setTimePerformance.Length; k++)
            {

                data_timeperformance.pms_time_list_m0 = new pms_time_detail_m0[1];
                m0_timeperformance[k] = new pms_time_detail_m0();

                m0_timeperformance[k].time_performance_name = setTimePerformance[k].ToString();
                m0_timeperformance[k].time_score_detail = settimescore_performance[k].ToString();
                m0_timeperformance[k].score_detail = setscore_performance[k].ToString();
                //m0_factor[k].score_after = "0.00";

                data_timeperformance.pms_time_list_m0 = m0_timeperformance;

            }

            setGridData(gvName, data_timeperformance.pms_time_list_m0);

        }


    }

    protected void getViewScoreTimePerformance()
    {

        data_pms data_time = new data_pms();
        data_pms data_timeperformance = new data_pms();


        pmsu0_DocFormDetail u0time_detail = new pmsu0_DocFormDetail();
        data_time.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];


        u0time_detail.condition = 8;
        u0time_detail.cemp_idx = int.Parse(ViewState["cemp_idx_detail"].ToString());//_emp_idx;

        data_time.Boxpmsu0_DocFormDetail[0] = u0time_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        data_time = callServicePostPMS(_urlSelectSystem, data_time);

        // string set_timescore_performance = "";
        // string set_score_performance = "";

        if (data_time.ReturnCode == "0")
        {

            //time
            set_timeAttendanceScore = data_time.pms_time_list_m0[0].total_score.ToString();

        }
        else
        {

            //time
            set_timeAttendanceScore = "0.00";

        }


    }

    protected void getScoreTimePerformance()
    {

        data_pms data_time = new data_pms();
        data_pms data_timeperformance = new data_pms();


        pmsu0_DocFormDetail u0time_detail = new pmsu0_DocFormDetail();
        data_time.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];


        u0time_detail.condition = 8;
        u0time_detail.cemp_idx = _emp_idx;

        data_time.Boxpmsu0_DocFormDetail[0] = u0time_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        data_time = callServicePostPMS(_urlSelectSystem, data_time);

        // string set_timescore_performance = "";
        // string set_score_performance = "";

        if (data_time.ReturnCode == "0")
        {

            //time
            set_timeAttendanceScore = data_time.pms_time_list_m0[0].total_score.ToString();

        }
        else
        {

            //time
            set_timeAttendanceScore = "0.00";

        }


    }

    protected void getFormviewComment(FormView fvName, int u0_docidx, int _noidx)
    {
        data_pms data_comment1 = new data_pms();
        pmsu0_DocFormDetail udoccomment_detail1 = new pmsu0_DocFormDetail();
        data_comment1.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        udoccomment_detail1.condition = 4;
        udoccomment_detail1.u0_docidx = u0_docidx;
        udoccomment_detail1.m0_typeidx = 3;

        data_comment1.Boxpmsu0_DocFormDetail[0] = udoccomment_detail1;

        data_comment1 = callServicePostPMS(_urlSelectSystem, data_comment1);

        ViewState["vs_ViewcommentDetail"] = data_comment1.Boxpmsu2_DocFormDetail;

        pmsu2_DocFormDetail[] _templist_IdSetTest1 = (pmsu2_DocFormDetail[])ViewState["vs_ViewcommentDetail"];

        var _linqDataList1 = (from dt in _templist_IdSetTest1
                              where
                              dt.cemp_idx == _emp_idx
                              && dt.noidx == _noidx

                              select dt).ToList();


        if (_linqDataList1.Count() > 0)
        {
            setFormData(fvName, FormViewMode.ReadOnly, _linqDataList1.ToList());

        }
        else if (int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0)
        {

            setFormData(fvName, FormViewMode.Insert, null);

        }

    }

    protected void getFormviewStar(FormView fvName, int u0_docidx, int _noidx)
    {
        data_pms data_comment = new data_pms();
        pmsu0_DocFormDetail udoccomment_detail = new pmsu0_DocFormDetail();
        data_comment.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        udoccomment_detail.condition = 4;
        udoccomment_detail.u0_docidx = int.Parse(ViewState["u0_docidx_detail"].ToString());
        udoccomment_detail.m0_typeidx = 4;

        data_comment.Boxpmsu0_DocFormDetail[0] = udoccomment_detail;

        data_comment = callServicePostPMS(_urlSelectSystem, data_comment);

        ViewState["vs_ViewDevStarDetail"] = data_comment.Boxpmsu2_DocFormDetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_comment));

        pmsu2_DocFormDetail[] _templist_IdSetTest = (pmsu2_DocFormDetail[])ViewState["vs_ViewDevStarDetail"];

        var _linqDataList = (from dt in _templist_IdSetTest
                             where
                             dt.cemp_idx == _emp_idx
                             && dt.noidx == _noidx
                             select dt).ToList();

        if (_linqDataList.Count() > 0)
        {
            setFormData(fvName, FormViewMode.ReadOnly, _linqDataList.ToList());

        }
        else if (int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0)
        {

            setFormData(fvName, FormViewMode.Insert, null);

        }



    }

    protected void getSearchDetailWaitApprove1()
    {

        DropDownList ddlOrganization = (DropDownList)fvSearchApprove1.FindControl("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList)fvSearchApprove1.FindControl("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList)fvSearchApprove1.FindControl("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList)fvSearchApprove1.FindControl("ddlDepartment");
        DropDownList ddlSection = (DropDownList)fvSearchApprove1.FindControl("ddlSection");
        DropDownList ddlPosition = (DropDownList)fvSearchApprove1.FindControl("ddlPosition");

        TextBox txt_empcode = (TextBox)fvSearchApprove1.FindControl("txt_empcode");


        var selectedEmpCode_ = new List<string>();
        string input_ = txt_empcode.Text;
        string[] recepempcode_ = input_.Split(',');
        foreach (string rempcode_ in recepempcode_)
        {
            if (rempcode_ != String.Empty)
            {
                //mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                selectedEmpCode_.Add(rempcode_);
            }
        }

        data_pms data_u0doc = new data_pms();
        pmsu0_DocFormDetail u0doc_detail = new pmsu0_DocFormDetail();
        data_u0doc.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0doc_detail.condition = 9;
        u0doc_detail.cemp_idx = _emp_idx;

        data_u0doc.Boxpmsu0_DocFormDetail[0] = u0doc_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_u0doc = callServicePostPMS(_urlSelectSystem, data_u0doc);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc));

        ViewState["vs_GvDetailApproveChiefsAll"] = data_u0doc.Boxpmsu0_DocFormDetail;
        //ViewState["vs_GvDetailIndex"] = data_u0doc.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_GvDetailApproveChiefs"] != null)
        {

            pmsu0_DocFormDetail[] _templistApprove_Detail = (pmsu0_DocFormDetail[])ViewState["vs_GvDetailApproveChiefsAll"];
            var _linqDataListApproveDetail = (from dt in _templistApprove_Detail
                                              where

                                              (int.Parse(ddlOrganization.SelectedValue) == 0 || dt.org_idx == int.Parse(ddlOrganization.SelectedValue))
                                            && (int.Parse(ddlWorkGroup.SelectedValue) == 0 || dt.wg_idx == int.Parse(ddlWorkGroup.SelectedValue))
                                            && (int.Parse(ddlLineWork.SelectedValue) == 0 || dt.lw_idx == int.Parse(ddlLineWork.SelectedValue))
                                            && (int.Parse(ddlDepartment.SelectedValue) == 0 || dt.rdept_idx == int.Parse(ddlDepartment.SelectedValue))
                                            && (int.Parse(ddlSection.SelectedValue) == 0 || dt.rsec_idx == int.Parse(ddlSection.SelectedValue))
                                            && (int.Parse(ddlPosition.SelectedValue) == 0 || dt.rpos_idx == int.Parse(ddlPosition.SelectedValue))
                                            && (selectedEmpCode_.Count == 0 || selectedEmpCode_.Contains(dt.emp_code))




                                           && (dt.unidx == 4 && dt.eval_emp_type == 4 && dt.eval_emp_idx == _emp_idx) //approve 1
                                                                                                                      // || (dt.unidx == 5 && dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                                                                                                      // || (dt.unidx == 6 && dt.staidx != 2 && dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3

                                              select new
                                              {
                                                  dt.u0_docidx,
                                                  dt.unidx,
                                                  dt.acidx,
                                                  dt.staidx,
                                                  dt.sum_kpis_value,
                                                  dt.sum_mine_core_value,
                                                  dt.sum_solid_core_value,
                                                  dt.sum_dotted_core_value,
                                                  dt.sum_mine_competency,
                                                  dt.sum_solid_competency,
                                                  dt.sum_dotted_competency,
                                                  dt.current_status,
                                                  dt.org_idx,
                                                  dt.org_name_th,
                                                  dt.rdept_idx,
                                                  dt.dept_name_th,
                                                  dt.rsec_idx,
                                                  dt.sec_name_th,
                                                  dt.rpos_idx,
                                                  dt.pos_name_th,
                                                  dt.emp_name_th,
                                                  dt.solid,
                                                  dt.dotted,
                                                  dt.pos_name,
                                                  dt.jobgrade_level,
                                                  dt.cemp_idx,
                                                  dt.emp_code,
                                                  dt.form_name,
                                                  dt.u0_typeidx,
                                                  dt.total_sum_kpis_value,
                                                  dt.total_corevalue,
                                                  dt.total_competencies,
                                                  dt.total_score,
                                                  dt.wg_name_th,
                                                  dt.lw_name_th
                                              }).Distinct().ToList();


            if (_linqDataListApproveDetail.Count() > 0)
            {
                rptViewBindbtnNode4.Visible = true;
                getbtnDecision(rptViewBindbtnNode4, 4, 0);
                setGridData(GvApprove1, _linqDataListApproveDetail);

            }
            else
            {
                rptViewBindbtnNode4.Visible = false;
                setGridData(GvApprove1, _linqDataListApproveDetail);

            }




        }
        else
        {
            ViewState["vs_GvDetailApproveChiefs"] = null;


            setGridData(GvApprove1, ViewState["vs_GvDetailApproveChiefs"]);

        }

    }

    protected void getWaitApproveChiefs(GridView gvName)
    {

        data_pms data_waitapprove = new data_pms();


        pmsu0_DocFormDetail u0_waitapprove_detail = new pmsu0_DocFormDetail();
        data_waitapprove.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];


        u0_waitapprove_detail.condition = 9;
        u0_waitapprove_detail.cemp_idx = _emp_idx;

        data_waitapprove.Boxpmsu0_DocFormDetail[0] = u0_waitapprove_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        data_waitapprove = callServicePostPMS(_urlSelectSystem, data_waitapprove);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_time));

        ViewState["vs_GvDetailApproveChiefs"] = data_waitapprove.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_GvDetailApproveChiefs"] != null)
        {
            pmsu0_DocFormDetail[] _templistApprove_Detail = (pmsu0_DocFormDetail[])ViewState["vs_GvDetailApproveChiefs"];
            var _linqDataListApproveDetail = (from dt in _templistApprove_Detail
                                              where
                                              (dt.unidx == 4 && dt.eval_emp_type == 4 && dt.eval_emp_idx == _emp_idx) //approve 1
                                                                                                                      // || (dt.unidx == 5 && dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                                                                                                      // || (dt.unidx == 6 && dt.staidx != 2 && dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3

                                              select new
                                              {
                                                  dt.u0_docidx,
                                                  dt.unidx,
                                                  dt.acidx,
                                                  dt.staidx,
                                                  dt.sum_kpis_value,
                                                  dt.sum_mine_core_value,
                                                  dt.sum_solid_core_value,
                                                  dt.sum_dotted_core_value,
                                                  dt.sum_mine_competency,
                                                  dt.sum_solid_competency,
                                                  dt.sum_dotted_competency,
                                                  dt.current_status,
                                                  dt.org_idx,
                                                  dt.org_name_th,
                                                  dt.rdept_idx,
                                                  dt.dept_name_th,
                                                  dt.rsec_idx,
                                                  dt.sec_name_th,
                                                  dt.rpos_idx,
                                                  dt.pos_name_th,
                                                  dt.emp_name_th,
                                                  dt.solid,
                                                  dt.dotted,
                                                  dt.pos_name,
                                                  dt.jobgrade_level,
                                                  dt.cemp_idx,
                                                  dt.emp_code,
                                                  dt.form_name,
                                                  dt.u0_typeidx,
                                                  dt.total_sum_kpis_value,
                                                  dt.total_corevalue,
                                                  dt.total_competencies,
                                                  dt.total_score,
                                                  dt.wg_name_th,
                                                  dt.lw_name_th
                                              }).Distinct().ToList();

            setGridData(gvName, _linqDataListApproveDetail);

            if (_linqDataListApproveDetail.Count() > 0)
            {
                rptViewBindbtnNode4.Visible = true;
                getbtnDecision(rptViewBindbtnNode4, 4, 0);

            }




        }
        else
        {

            ViewState["vs_GvDetailApproveChiefs"] = null;
            setGridData(gvName, ViewState["vs_GvDetailApproveChiefs"]);



        }


    }

    protected void getCountWaitApproveChiefs()
    {

        data_pms data_waitapprove = new data_pms();


        pmsu0_DocFormDetail u0_waitapprove_detail = new pmsu0_DocFormDetail();
        data_waitapprove.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];


        u0_waitapprove_detail.condition = 9;
        u0_waitapprove_detail.cemp_idx = _emp_idx;

        data_waitapprove.Boxpmsu0_DocFormDetail[0] = u0_waitapprove_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        data_waitapprove = callServicePostPMS(_urlSelectSystem, data_waitapprove);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_time));

        ViewState["vs_CountGvDetailApproveChiefs"] = data_waitapprove.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_CountGvDetailApproveChiefs"] != null)
        {
            pmsu0_DocFormDetail[] _templistApprove_Detail = (pmsu0_DocFormDetail[])ViewState["vs_CountGvDetailApproveChiefs"];
            var _linqCountDataListApproveDetail = (from dt in _templistApprove_Detail
                                                   where
                                                   (dt.unidx == 4 && dt.eval_emp_type == 4 && dt.eval_emp_idx == _emp_idx) //approve 1
                                                                                                                           // || (dt.unidx == 5 && dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                                                                                                           // || (dt.unidx == 6 && dt.staidx != 2 && dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3

                                                   select new
                                                   {
                                                       dt.u0_docidx,
                                                       dt.unidx,
                                                       dt.acidx,
                                                       dt.staidx,
                                                       dt.sum_kpis_value,
                                                       dt.sum_mine_core_value,
                                                       dt.sum_solid_core_value,
                                                       dt.sum_dotted_core_value,
                                                       dt.sum_mine_competency,
                                                       dt.sum_solid_competency,
                                                       dt.sum_dotted_competency,
                                                       dt.current_status,
                                                       dt.org_idx,
                                                       dt.org_name_th,
                                                       dt.rdept_idx,
                                                       dt.dept_name_th,
                                                       dt.rsec_idx,
                                                       dt.sec_name_th,
                                                       dt.rpos_idx,
                                                       dt.pos_name_th,
                                                       dt.emp_name_th,
                                                       dt.solid,
                                                       dt.dotted,
                                                       dt.pos_name,
                                                       dt.jobgrade_level,
                                                       dt.cemp_idx,
                                                       dt.emp_code,
                                                       dt.form_name,
                                                       dt.u0_typeidx,
                                                       dt.total_sum_kpis_value,
                                                       dt.total_corevalue,
                                                       dt.total_competencies,
                                                       dt.total_score,
                                                       dt.wg_name_th,
                                                       dt.lw_name_th
                                                   }).Distinct().ToList();

            if (_linqCountDataListApproveDetail.Count() > 0)
            {

                //ViewState["vs_CheckPermission_Detail"] = _linqDataListApproveDetail_Count.Count();


                lbApprove1.Text = " รออนุมัติ Approve1 <span class='badge progress-bar-danger' >" + _linqCountDataListApproveDetail.Count().ToString() + "</span>";
                setActiveTab("docApprove1", 0, 0, 0);



            }
            else
            {

                lbApprove1.Text = " รออนุมัติ Approve1 <span class='badge progress-bar-danger' >" + "0" + "</span>";
                setActiveTab("docApprove1", 0, 0, 0);

            }


        }
        else
        {

            ViewState["vs_CountGvDetailApproveChiefs"] = 0;
            lbApprove1.Text = " รออนุมัติ Approve1 <span class='badge progress-bar-danger' >" + "0" + "</span>";
            setActiveTab("docDetail", 0, 0, 0);


        }


    }

    protected void getWaitApprove2Chiefs(GridView gvName)
    {

        data_pms data_waitapprove = new data_pms();


        pmsu0_DocFormDetail u0_waitapprove_detail = new pmsu0_DocFormDetail();
        data_waitapprove.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];


        u0_waitapprove_detail.condition = 9;
        u0_waitapprove_detail.cemp_idx = _emp_idx;

        data_waitapprove.Boxpmsu0_DocFormDetail[0] = u0_waitapprove_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        data_waitapprove = callServicePostPMS(_urlSelectSystem, data_waitapprove);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_time));

        ViewState["vs_GvDetailApprove2Chiefs"] = data_waitapprove.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_GvDetailApprove2Chiefs"] != null)
        {
            pmsu0_DocFormDetail[] _templistApprove_Detail = (pmsu0_DocFormDetail[])ViewState["vs_GvDetailApprove2Chiefs"];
            var _linqDataListApproveDetail = (from dt in _templistApprove_Detail
                                              where
                                              (dt.unidx == 5 && dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                                                                                                      // || (dt.unidx == 5 && dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                                                                                                      // || (dt.unidx == 6 && dt.staidx != 2 && dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3

                                              select new
                                              {
                                                  dt.u0_docidx,
                                                  dt.unidx,
                                                  dt.acidx,
                                                  dt.staidx,
                                                  dt.sum_kpis_value,
                                                  dt.sum_mine_core_value,
                                                  dt.sum_solid_core_value,
                                                  dt.sum_dotted_core_value,
                                                  dt.sum_mine_competency,
                                                  dt.sum_solid_competency,
                                                  dt.sum_dotted_competency,
                                                  dt.current_status,
                                                  dt.org_idx,
                                                  dt.org_name_th,
                                                  dt.rdept_idx,
                                                  dt.dept_name_th,
                                                  dt.rsec_idx,
                                                  dt.sec_name_th,
                                                  dt.rpos_idx,
                                                  dt.pos_name_th,
                                                  dt.emp_name_th,
                                                  dt.solid,
                                                  dt.dotted,
                                                  dt.pos_name,
                                                  dt.jobgrade_level,
                                                  dt.cemp_idx,
                                                  dt.emp_code,
                                                  dt.form_name,
                                                  dt.u0_typeidx,
                                                  dt.total_sum_kpis_value,
                                                  dt.total_corevalue,
                                                  dt.total_competencies,
                                                  dt.total_score,
                                                  dt.wg_name_th,
                                                  dt.lw_name_th
                                              }).Distinct().ToList();

            setGridData(gvName, _linqDataListApproveDetail);

            if (_linqDataListApproveDetail.Count() > 0)
            {
                rptViewBindbtnNode5.Visible = true;
                getbtnDecision(rptViewBindbtnNode5, 5, 0);

            }


        }
        else
        {

            ViewState["vs_GvDetailApprove2Chiefs"] = null;
            setGridData(gvName, ViewState["vs_GvDetailApprove2Chiefs"]);



        }


    }

    protected void getSearchDetailWaitApprove2()
    {

        DropDownList ddlOrganization = (DropDownList)fvSearchApprove2.FindControl("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList)fvSearchApprove2.FindControl("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList)fvSearchApprove2.FindControl("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList)fvSearchApprove2.FindControl("ddlDepartment");
        DropDownList ddlSection = (DropDownList)fvSearchApprove2.FindControl("ddlSection");
        DropDownList ddlPosition = (DropDownList)fvSearchApprove2.FindControl("ddlPosition");

        TextBox txt_empcode = (TextBox)fvSearchApprove2.FindControl("txt_empcode");


        var selectedEmpCode_ = new List<string>();
        string input_ = txt_empcode.Text;
        string[] recepempcode_ = input_.Split(',');
        foreach (string rempcode_ in recepempcode_)
        {
            if (rempcode_ != String.Empty)
            {
                //mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                selectedEmpCode_.Add(rempcode_);
            }
        }

        data_pms data_u0doc = new data_pms();
        pmsu0_DocFormDetail u0doc_detail = new pmsu0_DocFormDetail();
        data_u0doc.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0doc_detail.condition = 9;
        u0doc_detail.cemp_idx = _emp_idx;

        data_u0doc.Boxpmsu0_DocFormDetail[0] = u0doc_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_u0doc = callServicePostPMS(_urlSelectSystem, data_u0doc);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc));

        ViewState["vs_GvDetailApprove2ChiefsAll"] = data_u0doc.Boxpmsu0_DocFormDetail;
        //ViewState["vs_GvDetailIndex"] = data_u0doc.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_GvDetailApprove2Chiefs"] != null)
        {

            pmsu0_DocFormDetail[] _templistApprove_Detail = (pmsu0_DocFormDetail[])ViewState["vs_GvDetailApprove2ChiefsAll"];
            var _linqDataListApproveDetail = (from dt in _templistApprove_Detail
                                              where

                                              (int.Parse(ddlOrganization.SelectedValue) == 0 || dt.org_idx == int.Parse(ddlOrganization.SelectedValue))
                                            && (int.Parse(ddlWorkGroup.SelectedValue) == 0 || dt.wg_idx == int.Parse(ddlWorkGroup.SelectedValue))
                                            && (int.Parse(ddlLineWork.SelectedValue) == 0 || dt.lw_idx == int.Parse(ddlLineWork.SelectedValue))
                                            && (int.Parse(ddlDepartment.SelectedValue) == 0 || dt.rdept_idx == int.Parse(ddlDepartment.SelectedValue))
                                            && (int.Parse(ddlSection.SelectedValue) == 0 || dt.rsec_idx == int.Parse(ddlSection.SelectedValue))
                                            && (int.Parse(ddlPosition.SelectedValue) == 0 || dt.rpos_idx == int.Parse(ddlPosition.SelectedValue))
                                            && (selectedEmpCode_.Count == 0 || selectedEmpCode_.Contains(dt.emp_code))

                                            && (dt.unidx == 5 && dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                                                                                                       // || (dt.unidx == 5 && dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                                                                                                       // || (dt.unidx == 6 && dt.staidx != 2 && dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3

                                              select new
                                              {
                                                  dt.u0_docidx,
                                                  dt.unidx,
                                                  dt.acidx,
                                                  dt.staidx,
                                                  dt.sum_kpis_value,
                                                  dt.sum_mine_core_value,
                                                  dt.sum_solid_core_value,
                                                  dt.sum_dotted_core_value,
                                                  dt.sum_mine_competency,
                                                  dt.sum_solid_competency,
                                                  dt.sum_dotted_competency,
                                                  dt.current_status,
                                                  dt.org_idx,
                                                  dt.org_name_th,
                                                  dt.rdept_idx,
                                                  dt.dept_name_th,
                                                  dt.rsec_idx,
                                                  dt.sec_name_th,
                                                  dt.rpos_idx,
                                                  dt.pos_name_th,
                                                  dt.emp_name_th,
                                                  dt.solid,
                                                  dt.dotted,
                                                  dt.pos_name,
                                                  dt.jobgrade_level,
                                                  dt.cemp_idx,
                                                  dt.emp_code,
                                                  dt.form_name,
                                                  dt.u0_typeidx,
                                                  dt.total_sum_kpis_value,
                                                  dt.total_corevalue,
                                                  dt.total_competencies,
                                                  dt.total_score,
                                                  dt.wg_name_th,
                                                  dt.lw_name_th
                                              }).Distinct().ToList();


            if (_linqDataListApproveDetail.Count() > 0)
            {
                rptViewBindbtnNode5.Visible = true;
                getbtnDecision(rptViewBindbtnNode5, 5, 0);
                setGridData(GvApprove2, _linqDataListApproveDetail);

            }
            else
            {
                rptViewBindbtnNode5.Visible = false;
                setGridData(GvApprove2, _linqDataListApproveDetail);

            }

        }
        else
        {
            ViewState["vs_GvDetailApprove2Chiefs"] = null;


            setGridData(GvApprove2, ViewState["vs_GvDetailApprove2Chiefs"]);

        }

    }

    protected void getCountWaitApprove2Chiefs()
    {

        data_pms data_waitapprove = new data_pms();


        pmsu0_DocFormDetail u0_waitapprove_detail = new pmsu0_DocFormDetail();
        data_waitapprove.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];


        u0_waitapprove_detail.condition = 9;
        u0_waitapprove_detail.cemp_idx = _emp_idx;

        data_waitapprove.Boxpmsu0_DocFormDetail[0] = u0_waitapprove_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        data_waitapprove = callServicePostPMS(_urlSelectSystem, data_waitapprove);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_time));

        ViewState["vs_CountGvDetailApprove2Chiefs"] = data_waitapprove.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_CountGvDetailApprove2Chiefs"] != null)
        {
            pmsu0_DocFormDetail[] _templistApprove_Detail = (pmsu0_DocFormDetail[])ViewState["vs_CountGvDetailApprove2Chiefs"];
            var _linqCountDataListApproveDetail = (from dt in _templistApprove_Detail
                                                   where
                                                   (dt.unidx == 5 && dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                                                                                                           // || (dt.unidx == 5 && dt.eval_emp_type == 5 && dt.eval_emp_idx == _emp_idx) //approve 2
                                                                                                                           // || (dt.unidx == 6 && dt.staidx != 2 && dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3

                                                   select new
                                                   {
                                                       dt.u0_docidx,
                                                       dt.unidx,
                                                       dt.acidx,
                                                       dt.staidx,
                                                       dt.sum_kpis_value,
                                                       dt.sum_mine_core_value,
                                                       dt.sum_solid_core_value,
                                                       dt.sum_dotted_core_value,
                                                       dt.sum_mine_competency,
                                                       dt.sum_solid_competency,
                                                       dt.sum_dotted_competency,
                                                       dt.current_status,
                                                       dt.org_idx,
                                                       dt.org_name_th,
                                                       dt.rdept_idx,
                                                       dt.dept_name_th,
                                                       dt.rsec_idx,
                                                       dt.sec_name_th,
                                                       dt.rpos_idx,
                                                       dt.pos_name_th,
                                                       dt.emp_name_th,
                                                       dt.solid,
                                                       dt.dotted,
                                                       dt.pos_name,
                                                       dt.jobgrade_level,
                                                       dt.cemp_idx,
                                                       dt.emp_code,
                                                       dt.form_name,
                                                       dt.u0_typeidx,
                                                       dt.total_sum_kpis_value,
                                                       dt.total_corevalue,
                                                       dt.total_competencies,
                                                       dt.total_score,
                                                       dt.wg_name_th,
                                                       dt.lw_name_th
                                                   }).Distinct().ToList();

            if (_linqCountDataListApproveDetail.Count() > 0)
            {

                //ViewState["vs_CheckPermission_Detail"] = _linqDataListApproveDetail_Count.Count();


                lbApprove2.Text = " รออนุมัติ Approve2 <span class='badge progress-bar-danger' >" + _linqCountDataListApproveDetail.Count().ToString() + "</span>";
                setActiveTab("docApprove2", 0, 0, 0);



            }
            else
            {

                lbApprove2.Text = " รออนุมัติ Approve2 <span class='badge progress-bar-danger' >" + "0" + "</span>";
                setActiveTab("docApprove2", 0, 0, 0);

            }


        }
        else
        {

            ViewState["vs_CountGvDetailApprove2Chiefs"] = 0;
            lbApprove2.Text = " รออนุมัติ Approve2 <span class='badge progress-bar-danger' >" + "0" + "</span>";
            setActiveTab("docDetail", 0, 0, 0);


        }


    }

    protected void getSearchDetailWaitApprove3()
    {

        DropDownList ddlOrganization = (DropDownList)fvSearchApprove3.FindControl("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList)fvSearchApprove3.FindControl("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList)fvSearchApprove3.FindControl("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList)fvSearchApprove3.FindControl("ddlDepartment");
        DropDownList ddlSection = (DropDownList)fvSearchApprove3.FindControl("ddlSection");
        DropDownList ddlPosition = (DropDownList)fvSearchApprove3.FindControl("ddlPosition");

        TextBox txt_empcode = (TextBox)fvSearchApprove3.FindControl("txt_empcode");


        var selectedEmpCode_ = new List<string>();
        string input_ = txt_empcode.Text;
        string[] recepempcode_ = input_.Split(',');
        foreach (string rempcode_ in recepempcode_)
        {
            if (rempcode_ != String.Empty)
            {
                //mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                selectedEmpCode_.Add(rempcode_);
            }
        }

        data_pms data_u0doc = new data_pms();
        pmsu0_DocFormDetail u0doc_detail = new pmsu0_DocFormDetail();
        data_u0doc.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

        u0doc_detail.condition = 9;
        u0doc_detail.cemp_idx = _emp_idx;

        data_u0doc.Boxpmsu0_DocFormDetail[0] = u0doc_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        data_u0doc = callServicePostPMS(_urlSelectSystem, data_u0doc);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc));

        ViewState["vs_GvDetailApprove3ChiefsAll"] = data_u0doc.Boxpmsu0_DocFormDetail;
        //ViewState["vs_GvDetailIndex"] = data_u0doc.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_GvDetailApprove3Chiefs"] != null)
        {

            pmsu0_DocFormDetail[] _templistApprove_Detail = (pmsu0_DocFormDetail[])ViewState["vs_GvDetailApprove3ChiefsAll"];
            var _linqDataListApproveDetail = (from dt in _templistApprove_Detail
                                              where

                                              (int.Parse(ddlOrganization.SelectedValue) == 0 || dt.org_idx == int.Parse(ddlOrganization.SelectedValue))
                                            && (int.Parse(ddlWorkGroup.SelectedValue) == 0 || dt.wg_idx == int.Parse(ddlWorkGroup.SelectedValue))
                                            && (int.Parse(ddlLineWork.SelectedValue) == 0 || dt.lw_idx == int.Parse(ddlLineWork.SelectedValue))
                                            && (int.Parse(ddlDepartment.SelectedValue) == 0 || dt.rdept_idx == int.Parse(ddlDepartment.SelectedValue))
                                            && (int.Parse(ddlSection.SelectedValue) == 0 || dt.rsec_idx == int.Parse(ddlSection.SelectedValue))
                                            && (int.Parse(ddlPosition.SelectedValue) == 0 || dt.rpos_idx == int.Parse(ddlPosition.SelectedValue))
                                            && (selectedEmpCode_.Count == 0 || selectedEmpCode_.Contains(dt.emp_code))

                                            && (dt.unidx == 6 && dt.staidx != 2 && dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3

                                              select new
                                              {
                                                  dt.u0_docidx,
                                                  dt.unidx,
                                                  dt.acidx,
                                                  dt.staidx,
                                                  dt.sum_kpis_value,
                                                  dt.sum_mine_core_value,
                                                  dt.sum_solid_core_value,
                                                  dt.sum_dotted_core_value,
                                                  dt.sum_mine_competency,
                                                  dt.sum_solid_competency,
                                                  dt.sum_dotted_competency,
                                                  dt.current_status,
                                                  dt.org_idx,
                                                  dt.org_name_th,
                                                  dt.rdept_idx,
                                                  dt.dept_name_th,
                                                  dt.rsec_idx,
                                                  dt.sec_name_th,
                                                  dt.rpos_idx,
                                                  dt.pos_name_th,
                                                  dt.emp_name_th,
                                                  dt.solid,
                                                  dt.dotted,
                                                  dt.pos_name,
                                                  dt.jobgrade_level,
                                                  dt.cemp_idx,
                                                  dt.emp_code,
                                                  dt.form_name,
                                                  dt.u0_typeidx,
                                                  dt.total_sum_kpis_value,
                                                  dt.total_corevalue,
                                                  dt.total_competencies,
                                                  dt.total_score,
                                                  dt.wg_name_th,
                                                  dt.lw_name_th
                                              }).Distinct().ToList();


            if (_linqDataListApproveDetail.Count() > 0)
            {
                rptViewBindbtnNode6.Visible = true;
                getbtnDecision(rptViewBindbtnNode6, 6, 0);
                setGridData(GvApprove3, _linqDataListApproveDetail);

            }
            else
            {
                rptViewBindbtnNode6.Visible = false;
                setGridData(GvApprove3, _linqDataListApproveDetail);

            }

        }
        else
        {
            ViewState["vs_GvDetailApprove3Chiefs"] = null;


            setGridData(GvApprove3, ViewState["vs_GvDetailApprove3Chiefs"]);

        }

    }

    protected void getWaitApprove3Chiefs(GridView gvName)
    {

        data_pms data_waitapprove = new data_pms();


        pmsu0_DocFormDetail u0_waitapprove_detail = new pmsu0_DocFormDetail();
        data_waitapprove.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];


        u0_waitapprove_detail.condition = 9;
        u0_waitapprove_detail.cemp_idx = _emp_idx;

        data_waitapprove.Boxpmsu0_DocFormDetail[0] = u0_waitapprove_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        data_waitapprove = callServicePostPMS(_urlSelectSystem, data_waitapprove);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_time));

        ViewState["vs_GvDetailApprove3Chiefs"] = data_waitapprove.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_GvDetailApprove3Chiefs"] != null)
        {
            pmsu0_DocFormDetail[] _templistApprove_Detail = (pmsu0_DocFormDetail[])ViewState["vs_GvDetailApprove3Chiefs"];
            var _linqDataListApproveDetail = (from dt in _templistApprove_Detail
                                              where
                                              (dt.unidx == 6 && dt.staidx != 2 && dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3


                                              select new
                                              {
                                                  dt.u0_docidx,
                                                  dt.unidx,
                                                  dt.acidx,
                                                  dt.staidx,
                                                  dt.sum_kpis_value,
                                                  dt.sum_mine_core_value,
                                                  dt.sum_solid_core_value,
                                                  dt.sum_dotted_core_value,
                                                  dt.sum_mine_competency,
                                                  dt.sum_solid_competency,
                                                  dt.sum_dotted_competency,
                                                  dt.current_status,
                                                  dt.org_idx,
                                                  dt.org_name_th,
                                                  dt.rdept_idx,
                                                  dt.dept_name_th,
                                                  dt.rsec_idx,
                                                  dt.sec_name_th,
                                                  dt.rpos_idx,
                                                  dt.pos_name_th,
                                                  dt.emp_name_th,
                                                  dt.solid,
                                                  dt.dotted,
                                                  dt.pos_name,
                                                  dt.jobgrade_level,
                                                  dt.cemp_idx,
                                                  dt.emp_code,
                                                  dt.form_name,
                                                  dt.u0_typeidx,
                                                  dt.total_sum_kpis_value,
                                                  dt.total_corevalue,
                                                  dt.total_competencies,
                                                  dt.total_score,
                                                  dt.wg_name_th,
                                                  dt.lw_name_th
                                              }).Distinct().ToList();

            setGridData(gvName, _linqDataListApproveDetail);

            if (_linqDataListApproveDetail.Count() > 0)
            {
                rptViewBindbtnNode6.Visible = true;
                getbtnDecision(rptViewBindbtnNode6, 6, 0);

            }


        }
        else
        {

            ViewState["vs_GvDetailApprove3Chiefs"] = null;
            setGridData(gvName, ViewState["vs_GvDetailApprove3Chiefs"]);



        }


    }

    protected void getCountWaitApprove3Chiefs()
    {

        data_pms data_waitapprove = new data_pms();


        pmsu0_DocFormDetail u0_waitapprove_detail = new pmsu0_DocFormDetail();
        data_waitapprove.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];


        u0_waitapprove_detail.condition = 9;
        u0_waitapprove_detail.cemp_idx = _emp_idx;

        data_waitapprove.Boxpmsu0_DocFormDetail[0] = u0_waitapprove_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_factor));

        data_waitapprove = callServicePostPMS(_urlSelectSystem, data_waitapprove);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_time));

        ViewState["vs_CountGvDetailApprove3Chiefs"] = data_waitapprove.Boxpmsu0_DocFormDetail;

        if (ViewState["vs_CountGvDetailApprove3Chiefs"] != null)
        {
            pmsu0_DocFormDetail[] _templistApprove_Detail = (pmsu0_DocFormDetail[])ViewState["vs_CountGvDetailApprove3Chiefs"];
            var _linqCountDataListApproveDetail = (from dt in _templistApprove_Detail
                                                   where
                                                   (dt.unidx == 6 && dt.staidx != 2 && dt.eval_emp_type == 6 && dt.eval_emp_idx == _emp_idx) //approve 3


                                                   select new
                                                   {
                                                       dt.u0_docidx,
                                                       dt.unidx,
                                                       dt.acidx,
                                                       dt.staidx,
                                                       dt.sum_kpis_value,
                                                       dt.sum_mine_core_value,
                                                       dt.sum_solid_core_value,
                                                       dt.sum_dotted_core_value,
                                                       dt.sum_mine_competency,
                                                       dt.sum_solid_competency,
                                                       dt.sum_dotted_competency,
                                                       dt.current_status,
                                                       dt.org_idx,
                                                       dt.org_name_th,
                                                       dt.rdept_idx,
                                                       dt.dept_name_th,
                                                       dt.rsec_idx,
                                                       dt.sec_name_th,
                                                       dt.rpos_idx,
                                                       dt.pos_name_th,
                                                       dt.emp_name_th,
                                                       dt.solid,
                                                       dt.dotted,
                                                       dt.pos_name,
                                                       dt.jobgrade_level,
                                                       dt.cemp_idx,
                                                       dt.emp_code,
                                                       dt.form_name,
                                                       dt.u0_typeidx,
                                                       dt.total_sum_kpis_value,
                                                       dt.total_corevalue,
                                                       dt.total_competencies,
                                                       dt.total_score,
                                                       dt.wg_name_th,
                                                       dt.lw_name_th
                                                   }).Distinct().ToList();

            if (_linqCountDataListApproveDetail.Count() > 0)
            {

                //ViewState["vs_CheckPermission_Detail"] = _linqDataListApproveDetail_Count.Count();


                lbApprove3.Text = " รออนุมัติ Approve3 <span class='badge progress-bar-danger' >" + _linqCountDataListApproveDetail.Count().ToString() + "</span>";
                setActiveTab("docApprove3", 0, 0, 0);



            }
            else
            {

                lbApprove3.Text = " รออนุมัติ Approve3 <span class='badge progress-bar-danger' >" + "0" + "</span>";
                setActiveTab("docApprove3", 0, 0, 0);


            }


        }
        else
        {

            ViewState["vs_CountGvDetailApprove3Chiefs"] = 0;
            lbApprove3.Text = " รออนุมัติ Approve3 <span class='badge progress-bar-danger' >" + "0" + "</span>";
            setActiveTab("docDetail", 0, 0, 0);


        }


    }

    #endregion getdata

    #region dropdown list

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "-1"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        data_employee _dataEmployee_rdept = new data_employee();
        _dataEmployee_rdept.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee_rdept.department_list[0] = _deptList;

        _dataEmployee_rdept = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee_rdept);
        setDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "-1"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "-1"));
    }
    #endregion dropdown list

    protected string getWeightScore(double kpi_weight, double _point)
    {
        string returnWeightScore = "";

        returnWeightScore = Convert.ToDouble(((Convert.ToDouble(kpi_weight) / 100) * Convert.ToDouble(_point))).ToString();
        return returnWeightScore;

    }

    protected string getKpiScore(double kpi_weight, double _point)
    {
        string returnKpiScore = "";

        returnKpiScore = Convert.ToDouble(((Convert.ToDouble(kpi_weight) / Convert.ToDouble(_point)) * Convert.ToDouble(100))).ToString();
        return returnKpiScore;

    }

    protected string getCoreValueScore(double corevalue_weight, double _point)
    {
        string returnCoreValueScore = "";

        returnCoreValueScore = Convert.ToDouble(((Convert.ToDouble(corevalue_weight) / Convert.ToDouble(_point)) * Convert.ToDouble(100))).ToString();
        return returnCoreValueScore;

    }

    protected string getCompetenciesScore(double competencies_weight, double _point)
    {
        string returnCompetenciesScore = "";

        returnCompetenciesScore = Convert.ToDouble(((Convert.ToDouble(competencies_weight) / Convert.ToDouble(_point)) * Convert.ToDouble(100))).ToString();
        return returnCompetenciesScore;

    }

    protected string getTimeAttendanceScore(double time_score, double _point)
    {
        string returnTimeScore = "";

        returnTimeScore = Convert.ToDouble(((Convert.ToDouble(time_score) / Convert.ToDouble(_point)) * Convert.ToDouble(100))).ToString();
        return returnTimeScore;

    }

    protected decimal getSumpointCorvalue()
    {
        //decimal returnSumpointCorvalue = "";

        GridView GvCoreValue = (GridView)_PanelCoreValue.FindControl("GvCoreValue");

        sumpoint_corvalue = Convert.ToDecimal((Convert.ToDecimal(GvCoreValue.Rows.Count) * Convert.ToDecimal(4)));//Convert.ToDecimal(gvName.Rows.Count);
        //litDebug.Text = sumpoint_corvalue.ToString();

        return sumpoint_corvalue;

    }

    protected decimal getSumpointCompetencies()
    {
        //decimal returnSumpointCorvalue = "";

        GridView GvCompetencies = (GridView)_PanelCompetencies.FindControl("GvCompetencies");

        sumpoint_competencies = Convert.ToDecimal((Convert.ToDecimal(GvCompetencies.Rows.Count) * Convert.ToDecimal(4)));//Convert.ToDecimal(gvName.Rows.Count);


        //litDebug.Text = sumpoint_competencies.ToString();

        return sumpoint_competencies;

    }

    protected string getViewKpiScore(double kpi_weight, double _point)
    {
        string returnKpiScore = "";

        returnKpiScore = Convert.ToDouble(((Convert.ToDouble(kpi_weight) / Convert.ToDouble(_point)) * Convert.ToDouble(100))).ToString();
        return returnKpiScore;

    }

    protected string getViewCoreValueScore(double corevalue_weight, double _point)
    {
        string returnCoreValueScore = "";

        returnCoreValueScore = Convert.ToDouble(((Convert.ToDouble(corevalue_weight) / Convert.ToDouble(_point)) * Convert.ToDouble(100))).ToString();
        return returnCoreValueScore;

    }

    protected string getViewCompetenciesScore(double competencies_weight, double _point)
    {
        string returnCompetenciesScore = "";

        returnCompetenciesScore = Convert.ToDouble(((Convert.ToDouble(competencies_weight) / Convert.ToDouble(_point)) * Convert.ToDouble(100))).ToString();
        return returnCompetenciesScore;

    }

    protected decimal getViewSumpointCorvalue()
    {
        //decimal returnSumpointCorvalue = "";

        GridView GvViewCoreValue = (GridView)docDetail.FindControl("GvViewCoreValue");

        sumpoint_corvalue = Convert.ToDecimal((Convert.ToDecimal(GvViewCoreValue.Rows.Count) * Convert.ToDecimal(4)));//Convert.ToDecimal(gvName.Rows.Count);
        //litDebug.Text = sumpoint_corvalue.ToString();

        return sumpoint_corvalue;

    }

    protected decimal getViewSumpointCompetencies()
    {
        //decimal returnSumpointCorvalue = "";

        GridView GvViewCompetencies = (GridView)docDetail.FindControl("GvViewCompetencies");

        sumpoint_competencies = Convert.ToDecimal((Convert.ToDecimal(GvViewCompetencies.Rows.Count) * Convert.ToDecimal(4)));//Convert.ToDecimal(gvName.Rows.Count);


        //litDebug.Text = sumpoint_competencies.ToString();

        return sumpoint_competencies;

    }

    protected void setGridColor()
    {
        foreach (GridViewRow row_ in GvViewCoreValue.Rows)
        {
            UpdatePanel _PnSolidSuperiorcor = (UpdatePanel)row_.FindControl("_PnSolidSuperiorcor");
            _PnSolidSuperiorcor.Attributes.Add("style", "background-color: #FBCAC0");

        }

        foreach (GridViewRow row_1 in GvViewCompetencies.Rows)
        {
            UpdatePanel _PnSuperiorcom = (UpdatePanel)row_1.FindControl("_PnSuperiorcom");
            _PnSuperiorcom.Attributes.Add("style", "background-color: #FBCAC0");

        }
    }


    #region SelectedIndexChanged RadioButtonList  
    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is RadioButtonList)
        {
            RadioButtonList rdoname = (RadioButtonList)sender;

            switch (rdoname.ID)
            {
                case "rdoperformance_level":

                    var rdoperformance_level = (RadioButtonList)sender;
                    //var rdoperformance_point = (RadioButtonList)sender;
                    var rowGrid_insert = (GridViewRow)rdoperformance_level.NamingContainer;

                    RadioButtonList rdoperformance_point = (RadioButtonList)rowGrid_insert.FindControl("rdoperformance_point");
                    rdoperformance_point.SelectedValue = rdoperformance_level.SelectedValue;

                    Label lit_total_weighted_score_ = (Label)GvKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score");
                    Label lit_total_core_value_ = (Label)GvCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value");
                    Label lit_total_Competencies_ = (Label)GvCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies");




                    int count_ = 0;

                    if (rdoperformance_point.SelectedValue != "")
                    {
                        foreach (GridViewRow row in GvKPIs.Rows)
                        {

                            //RadioButtonList rdoperformance_level = (RadioButtonList)row.FindControl("rdoperformance_level");
                            RadioButtonList rdoperformance_level_ = (RadioButtonList)row.FindControl("rdoperformance_level");
                            RadioButtonList rdoperformance_point_ = (RadioButtonList)row.FindControl("rdoperformance_point");
                            Label lblkpi_weight = (Label)row.FindControl("lblkpi_weight");
                            Label lblpoint = (Label)row.FindControl("lblpoint");

                            Label lblweighted_score = (Label)row.FindControl("lblweighted_score");


                            //chkstatus_otbefore.Checked = true;
                            if (rdoperformance_point_.SelectedValue != "")
                            {
                                lblweighted_score.Text = getWeightScore(Convert.ToDouble(lblkpi_weight.Text), Convert.ToDouble(rdoperformance_level_.SelectedIndex));//Convert.ToDouble(((Convert.ToDouble(lblkpi_weight.Text) / 100) * Convert.ToDouble(rdoperformance_level_.SelectedIndex))).ToString();


                                tot_sumpoint_kpi += Convert.ToDecimal(lblweighted_score.Text);
                                Label lit_total_weighted_score = (Label)GvKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score");
                                lit_total_weighted_score.Text = String.Format("{0:N2}", tot_sumpoint_kpi);

                                getSumpointCorvalue();
                                getSumpointCompetencies();
                                getScoreTimePerformance();

                                set_scoreBefore = getKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                getCoreValueScore(Convert.ToDouble(lit_total_core_value_.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                getCompetenciesScore(Convert.ToDouble(lit_total_Competencies_.Text), Convert.ToDouble(sumpoint_competencies)) + ","
                                + set_timeAttendanceScore.ToString();



                            }

                            count_++;

                        }
                        getFactor(GvFactor);

                    }


                    break;

                case "rdoperformance_level_view":

                    var rdoperformance_level_view = (RadioButtonList)sender;
                    //var rdoperformance_point = (RadioButtonList)sender;
                    var rowGrid_insert1 = (GridViewRow)rdoperformance_level_view.NamingContainer;

                    //RadioButtonList rdoperformance_point_view = (RadioButtonList)rowGrid_insert1.FindControl("rdoperformance_point_view");
                    //rdoperformance_point_view.SelectedValue = rdoperformance_level_view.SelectedValue;

                    int count_1 = 0;

                    if (rdoperformance_point_view.SelectedValue != "")
                    {
                        foreach (GridViewRow row in GvViewKPIs.Rows)
                        {

                            //RadioButtonList rdoperformance_level = (RadioButtonList)row.FindControl("rdoperformance_level");
                            RadioButtonList rdoperformance_level_view_ = (RadioButtonList)row.FindControl("rdoperformance_level_view");
                            RadioButtonList rdoperformance_point_view_ = (RadioButtonList)row.FindControl("rdoperformance_point_view");
                            Label lblkpi_weight_view = (Label)row.FindControl("lblkpi_weight_view");

                            Label lblweighted_score_view = (Label)row.FindControl("lblweighted_score_view");


                            //chkstatus_otbefore.Checked = true;
                            if (rdoperformance_point_view_.SelectedValue != "")
                            {
                                lblweighted_score_view.Text = getWeightScore(Convert.ToDouble(lblkpi_weight_view.Text), Convert.ToDouble(rdoperformance_level_view_.SelectedIndex));//Convert.ToDouble(((Convert.

                                tot_sumpoint_kpi += Convert.ToDecimal(lblweighted_score_view.Text);
                                Label lit_total_weighted_score_view = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");
                                lit_total_weighted_score_view.Text = String.Format("{0:N2}", tot_sumpoint_kpi);

                                Label lit_total_weighted_score_view_ = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");
                                Label lit_total_core_value_detail_ = (Label)GvViewCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value_detail");
                                Label lit_total_Competencies_view_ = (Label)GvViewCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies_view");


                                Panel div_dotted_core_value = (Panel)GvViewCoreValue.FooterRow.Cells[5].FindControl("div_dotted_core_value");
                                Label lit_totaldotted_core_value_detail = (Label)div_dotted_core_value.FindControl("lit_totaldotted_core_value_detail");

                                Panel div_dotted_Competencies = (Panel)GvViewCompetencies.FooterRow.Cells[5].FindControl("div_dotted_Competencies");
                                Label lit_totaldotted_Competencies_view = (Label)div_dotted_Competencies.FindControl("lit_totaldotted_Competencies_view");

                                Label lit_totalsolid_core_value_detail = (Label)GvViewCoreValue.FooterRow.Cells[5].FindControl("lit_totalsolid_core_value_detail");
                                Label lit_totalsolid_Competencies_view = (Label)GvViewCompetencies.FooterRow.Cells[5].FindControl("lit_totalsolid_Competencies_view");


                                getViewSumpointCorvalue();
                                getViewSumpointCompetencies();
                                getViewScoreTimePerformance();

                                // litDebug.Text += lit_totaldotted_core_value_detail.Text + "|" + lit_totaldotted_Competencies_view.Text + "|" + lit_totalsolid_core_value_detail.Text + "|" + lit_totalsolid_Competencies_view.Text;

                                //litDebug.Text += set_timeAttendanceScore.ToString() + "|";

                                set_scoreBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                getViewCoreValueScore(Convert.ToDouble(lit_total_core_value_detail_.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                getViewCompetenciesScore(Convert.ToDouble(lit_total_Competencies_view_.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                                //dotted
                                if (int.Parse(ViewState["unidx_detail"].ToString()) > 1)
                                {
                                    if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                                    {
                                        set_scoreDottedBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                        getViewCoreValueScore(Convert.ToDouble(lit_totaldotted_core_value_detail.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                        getViewCompetenciesScore(Convert.ToDouble(lit_totaldotted_Competencies_view.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                                    }
                                }

                                //solid
                                if (int.Parse(ViewState["unidx_detail"].ToString()) > 2)
                                {
                                    set_scoreSolidBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                    getViewCoreValueScore(Convert.ToDouble(lit_totalsolid_core_value_detail.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                    getViewCompetenciesScore(Convert.ToDouble(lit_totalsolid_Competencies_view.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();
                                }






                                //getViewFactor(GvViewFactor);

                            }

                            count_1++;



                        }

                        getViewFactor(GvViewFactor);

                        setGridColor();


                    }

                    break;

                case "rdochoice_mine":

                    var rdochoice_mine = (RadioButtonList)sender;
                    //var rdoperformance_point = (RadioButtonList)sender;
                    var rowGrid = (GridViewRow)rdochoice_mine.NamingContainer;

                    if (GvKPIs.Rows.Count > 0)
                    {
                        Label lit_total_weighted_score_mine = (Label)GvKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score");

                        tot_sumpoint_kpi = Convert.ToDecimal(lit_total_weighted_score_mine.Text);

                    }
                    else
                    {

                        tot_sumpoint_kpi = Convert.ToDecimal("0.00");


                    }



                    Label lit_total_core_value_mine = (Label)GvCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value");
                    Label lit_total_Competencies_mine = (Label)GvCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies");

                    int count_mine = 0;

                    if (rdochoice_mine.SelectedValue != "")
                    {
                        foreach (GridViewRow row in GvCoreValue.Rows)
                        {

                            RadioButtonList rdochoice_mine_ = (RadioButtonList)row.FindControl("rdochoice_mine");

                            if (rdochoice_mine_.SelectedValue != "")
                            {

                                tot_sumcore_value += Convert.ToDecimal(rdochoice_mine_.SelectedValue);
                                Label lit_total_core_value = (Label)GvCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value");
                                lit_total_core_value.Text = String.Format("{0:N2}", tot_sumcore_value);

                                getSumpointCorvalue();
                                getSumpointCompetencies();
                                getScoreTimePerformance();

                                //litDebug.Text = lit_total_weighted_score_.Text + "|" + tot_sumcore_value.ToString() + "|" + sumpoint_corvalue.ToString();

                                set_scoreBefore = getKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                getCoreValueScore(Convert.ToDouble(tot_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                getCompetenciesScore(Convert.ToDouble(lit_total_Competencies_mine.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                                // if(GvKPIs.Rows.Count > 0)
                                // {

                                //     set_scoreBefore = getKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                //     getCoreValueScore(Convert.ToDouble(tot_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                //     getCompetenciesScore(Convert.ToDouble(lit_total_Competencies_mine.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();


                                // }
                                // else{

                                //     set_scoreBefore = getKpiScore(Convert.ToDouble(0.00), Convert.ToDouble(sumpoint_kpi)) + "," +
                                //     getCoreValueScore(Convert.ToDouble(tot_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                //     getCompetenciesScore(Convert.ToDouble(lit_total_Competencies_mine.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                                // }



                                // set_scoreBefore = lit_total_weighted_score_.Text + "," + tot_sumcore_value + "," + lit_total_Competencies_.Text + "," + "0.00";


                            }
                            count_mine++;

                        }

                        getFactor(GvFactor);

                    }

                    break;

                case "rdochoicecom_mine":

                    var rdochoicecom_mine = (RadioButtonList)sender;
                    //var rdoperformance_point = (RadioButtonList)sender;
                    var rowGrid_ = (GridViewRow)rdochoicecom_mine.NamingContainer;


                    if (GvKPIs.Rows.Count > 0)
                    {

                        Label lit_total_weighted_score_mine_ = (Label)GvKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score");

                        tot_sumpoint_kpi = Convert.ToDecimal(lit_total_weighted_score_mine_.Text);

                    }
                    else
                    {

                        tot_sumpoint_kpi = Convert.ToDecimal("0.00");


                    }

                    Label lit_total_core_value_mine_ = (Label)GvCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value");
                    Label lit_total_Competencies_mine_ = (Label)GvCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies");


                    int count_comp = 0;

                    if (rdochoicecom_mine.SelectedValue != "")
                    {
                        foreach (GridViewRow row in GvCompetencies.Rows)
                        {

                            RadioButtonList rdochoicecom_mine_ = (RadioButtonList)row.FindControl("rdochoicecom_mine");

                            if (rdochoicecom_mine_.SelectedValue != "")
                            {

                                tot_sumcompetencies += Convert.ToDecimal(rdochoicecom_mine_.SelectedValue);
                                Label lit_total_Competencies = (Label)GvCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies");
                                lit_total_Competencies.Text = String.Format("{0:N2}", tot_sumcompetencies);

                                // set_scoreBefore = lit_total_weighted_score_.Text + "," + lit_total_core_value_.Text + "," + tot_sumcompetencies + "," + "0.00";

                                getSumpointCorvalue();
                                getSumpointCompetencies();
                                getScoreTimePerformance();

                                set_scoreBefore = getKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                getCoreValueScore(Convert.ToDouble(lit_total_core_value_mine_.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                getCompetenciesScore(Convert.ToDouble(tot_sumcompetencies), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();


                            }
                            count_comp++;

                        }

                        getFactor(GvFactor);

                    }

                    break;
                case "rdochoice_mine_detail":

                    var rdochoice_mine_detail = (RadioButtonList)sender;
                    //var rdoperformance_point = (RadioButtonList)sender;
                    //var rowGrid = (GridViewRow)rdochoice_mine_detail.NamingContainer;

                    int count_minedetail = 0;

                    if (rdochoice_mine_detail.SelectedValue != "")
                    {
                        foreach (GridViewRow row in GvViewCoreValue.Rows)
                        {

                            RadioButtonList rdochoice_mine_detail_ = (RadioButtonList)row.FindControl("rdochoice_mine_detail");

                            if (rdochoice_mine_detail_.SelectedValue != "")
                            {

                                tot_sumcore_value += Convert.ToDecimal(rdochoice_mine_detail_.SelectedValue);
                                Label lit_total_core_value_detail = (Label)GvViewCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value_detail");
                                lit_total_core_value_detail.Text = String.Format("{0:N2}", tot_sumcore_value);

                                //Label lit_total_weighted_score_view_1 = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");

                                if (GvViewKPIs.Rows.Count > 0)
                                {

                                    Label lit_total_weighted_score_view_1 = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");

                                    tot_sumpoint_kpi = Convert.ToDecimal(lit_total_weighted_score_view_1.Text);

                                }
                                else
                                {

                                    tot_sumpoint_kpi = Convert.ToDecimal("0.00");


                                }



                                Label lit_total_core_value_detail_1 = (Label)GvViewCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value_detail");
                                Label lit_total_Competencies_view_1 = (Label)GvViewCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies_view");


                                getViewSumpointCorvalue();
                                getViewSumpointCompetencies();
                                getViewScoreTimePerformance();

                                set_scoreBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                getViewCoreValueScore(Convert.ToDouble(lit_total_core_value_detail_1.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                getViewCompetenciesScore(Convert.ToDouble(lit_total_Competencies_view_1.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                                //getViewFactor(GvViewFactor);

                            }
                            count_minedetail++;

                        }

                        getViewFactor(GvViewFactor);

                    }

                    break;
                case "rdochoice_head_detail":

                    var rdochoice_head_detail = (RadioButtonList)sender;
                    //var rdoperformance_point = (RadioButtonList)sender;
                    //var rowGrid = (GridViewRow)rdochoice_mine_detail.NamingContainer;

                    int countsolid_minedetail = 0;

                    if (rdochoice_head_detail.SelectedValue != "")
                    {
                        foreach (GridViewRow row in GvViewCoreValue.Rows)
                        {

                            RadioButtonList rdochoice_head_detail_ = (RadioButtonList)row.FindControl("rdochoice_head_detail");

                            if (rdochoice_head_detail_.SelectedValue != "")
                            {

                                totsolid_sumcore_value += Convert.ToDecimal(rdochoice_head_detail_.SelectedValue);
                                Label lit_totalsolid_core_value_detail = (Label)GvViewCoreValue.FooterRow.Cells[5].FindControl("lit_totalsolid_core_value_detail");
                                lit_totalsolid_core_value_detail.Text = String.Format("{0:N2}", totsolid_sumcore_value);


                                //Label lit_total_weighted_score_view_s = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");


                                if (GvViewKPIs.Rows.Count > 0)
                                {
                                    Label lit_total_weighted_score_view_s = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");

                                    tot_sumpoint_kpi = Convert.ToDecimal(lit_total_weighted_score_view_s.Text);

                                }
                                else
                                {

                                    tot_sumpoint_kpi = Convert.ToDecimal("0.00");


                                }



                                Label lit_total_core_value_detail_s = (Label)GvViewCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value_detail");
                                Label lit_total_Competencies_view_s = (Label)GvViewCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies_view");

                                Label lit_totaldotted_core_value_detail_s = (Label)GvViewCoreValue.FooterRow.Cells[5].FindControl("lit_totaldotted_core_value_detail");
                                Label lit_totaldotted_Competencies_view_s = (Label)GvViewCompetencies.FooterRow.Cells[5].FindControl("lit_totaldotted_Competencies_view");

                                //Label lit_totalsolid_core_value_detail_s = (Label)GvViewCoreValue.FooterRow.Cells[5].FindControl("lit_totalsolid_core_value_detail");
                                Label lit_totalsolid_Competencies_view_s = (Label)GvViewCompetencies.FooterRow.Cells[5].FindControl("lit_totalsolid_Competencies_view");

                                getViewSumpointCorvalue();
                                getViewSumpointCompetencies();
                                getViewScoreTimePerformance();

                                set_scoreBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                getViewCoreValueScore(Convert.ToDouble(lit_total_core_value_detail_s.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                getViewCompetenciesScore(Convert.ToDouble(lit_total_Competencies_view_s.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                                //dotted
                                if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                                {
                                    set_scoreDottedBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                    getViewCoreValueScore(Convert.ToDouble(lit_totaldotted_core_value_detail_s.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                    getViewCompetenciesScore(Convert.ToDouble(lit_totaldotted_Competencies_view_s.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();
                                }

                                //solid
                                set_scoreSolidBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                getViewCoreValueScore(Convert.ToDouble(totsolid_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                getViewCompetenciesScore(Convert.ToDouble(lit_totalsolid_Competencies_view_s.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();


                                //getViewFactor(GvViewFactor);

                            }
                            countsolid_minedetail++;

                        }

                        getViewFactor(GvViewFactor);
                        setGridColor();

                    }



                    break;
                case "rdochoice_dotted_detail":

                    var rdochoice_dotted_detail = (RadioButtonList)sender;
                    //var rdoperformance_point = (RadioButtonList)sender;
                    //var rowGrid = (GridViewRow)rdochoice_mine_detail.NamingContainer;

                    int countdotted_minedetail = 0;

                    if (rdochoice_dotted_detail.SelectedValue != "")
                    {
                        foreach (GridViewRow row in GvViewCoreValue.Rows)
                        {

                            RadioButtonList rdochoice_dotted_detail_ = (RadioButtonList)row.FindControl("rdochoice_dotted_detail");

                            if (rdochoice_dotted_detail_.SelectedValue != "")
                            {

                                totdotted_sumcore_value += Convert.ToDecimal(rdochoice_dotted_detail_.SelectedValue);
                                Label lit_totaldotted_core_value_detail = (Label)GvViewCoreValue.FooterRow.Cells[5].FindControl("lit_totaldotted_core_value_detail");
                                lit_totaldotted_core_value_detail.Text = String.Format("{0:N2}", totdotted_sumcore_value);

                                //Label lit_total_weighted_score_view_ = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");

                                if (GvViewKPIs.Rows.Count > 0)
                                {
                                    //Label lit_total_weighted_score_view_s = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");
                                    Label lit_total_weighted_score_view_ = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");

                                    tot_sumpoint_kpi = Convert.ToDecimal(lit_total_weighted_score_view_.Text);

                                }
                                else
                                {

                                    tot_sumpoint_kpi = Convert.ToDecimal("0.00");


                                }



                                Label lit_total_core_value_detail_ = (Label)GvViewCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value_detail");
                                Label lit_total_Competencies_view_ = (Label)GvViewCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies_view");

                                //Label lit_totaldotted_core_value_detail = (Label)GvViewCoreValue.FooterRow.Cells[5].FindControl("lit_totaldotted_core_value_detail");
                                Label lit_totaldotted_Competencies_view = (Label)GvViewCompetencies.FooterRow.Cells[5].FindControl("lit_totaldotted_Competencies_view");

                                // Label lit_totalsolid_core_value_detail_ = (Label)GvViewCoreValue.FooterRow.Cells[5].FindControl("lit_totalsolid_core_value_detail");
                                // Label lit_totalsolid_Competencies_view_ = (Label)GvViewCompetencies.FooterRow.Cells[5].FindControl("lit_totalsolid_Competencies_view");


                                getViewSumpointCorvalue();
                                getViewSumpointCompetencies();
                                getViewScoreTimePerformance();

                                set_scoreBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                getViewCoreValueScore(Convert.ToDouble(lit_total_core_value_detail_.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                getViewCompetenciesScore(Convert.ToDouble(lit_total_Competencies_view_.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                                //dotted
                                if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                                {
                                    set_scoreDottedBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                    getViewCoreValueScore(Convert.ToDouble(totdotted_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                    getViewCompetenciesScore(Convert.ToDouble(lit_totaldotted_Competencies_view.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();
                                }



                                //getViewFactor(GvViewFactor);



                            }
                            countdotted_minedetail++;

                        }

                        getViewFactor(GvViewFactor);

                    }

                    break;
                case "rdochoicecom_mine_detail":

                    var rdochoicecom_mine_detail = (RadioButtonList)sender;
                    //var rdoperformance_point = (RadioButtonList)sender;
                    //var rowGrid_ = (GridViewRow)rdochoicecom_mine_detail.NamingContainer;

                    int count_compview = 0;

                    if (rdochoicecom_mine_detail.SelectedValue != "")
                    {
                        foreach (GridViewRow row in GvViewCompetencies.Rows)
                        {

                            RadioButtonList rdochoicecom_mine_detail_ = (RadioButtonList)row.FindControl("rdochoicecom_mine_detail");

                            if (rdochoicecom_mine_detail_.SelectedValue != "")
                            {

                                tot_sumcompetencies += Convert.ToDecimal(rdochoicecom_mine_detail_.SelectedValue);
                                Label lit_total_Competencies_view = (Label)GvViewCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies_view");
                                lit_total_Competencies_view.Text = String.Format("{0:N2}", tot_sumcompetencies);

                                //Label lit_total_weighted_score_view_2 = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");

                                if (GvViewKPIs.Rows.Count > 0)
                                {
                                    Label lit_total_weighted_score_view_2 = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");

                                    tot_sumpoint_kpi = Convert.ToDecimal(lit_total_weighted_score_view_2.Text);

                                }
                                else
                                {

                                    tot_sumpoint_kpi = Convert.ToDecimal("0.00");


                                }


                                Label lit_total_core_value_detail_2 = (Label)GvViewCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value_detail");
                                Label lit_total_Competencies_view_2 = (Label)GvViewCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies_view");




                                getViewSumpointCorvalue();
                                getViewSumpointCompetencies();
                                getViewScoreTimePerformance();

                                set_scoreBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                getViewCoreValueScore(Convert.ToDouble(lit_total_core_value_detail_2.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                getViewCompetenciesScore(Convert.ToDouble(lit_total_Competencies_view_2.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                                //getViewFactor(GvViewFactor);

                            }
                            count_compview++;

                        }

                        getViewFactor(GvViewFactor);

                    }

                    break;
                case "rdochoicecom_head_detail":

                    var rdochoicecom_head_detail = (RadioButtonList)sender;
                    //var rdoperformance_point = (RadioButtonList)sender;
                    //var rowGrid_ = (GridViewRow)rdochoicecom_mine_detail.NamingContainer;

                    int countsolid_compview = 0;

                    if (rdochoicecom_head_detail.SelectedValue != "")
                    {
                        foreach (GridViewRow row in GvViewCompetencies.Rows)
                        {

                            RadioButtonList rdochoicecom_head_detail_ = (RadioButtonList)row.FindControl("rdochoicecom_head_detail");

                            if (rdochoicecom_head_detail_.SelectedValue != "")
                            {

                                totsolid_sumcompetencies += Convert.ToDecimal(rdochoicecom_head_detail_.SelectedValue);
                                Label lit_totalsolid_Competencies_view = (Label)GvViewCompetencies.FooterRow.Cells[5].FindControl("lit_totalsolid_Competencies_view");
                                lit_totalsolid_Competencies_view.Text = String.Format("{0:N2}", totsolid_sumcompetencies);

                                //Label lit_total_weighted_score_view_s1 = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");

                                if (GvViewKPIs.Rows.Count > 0)
                                {
                                    Label lit_total_weighted_score_view_s1 = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");

                                    tot_sumpoint_kpi = Convert.ToDecimal(lit_total_weighted_score_view_s1.Text);

                                }
                                else
                                {

                                    tot_sumpoint_kpi = Convert.ToDecimal("0.00");


                                }


                                Label lit_total_core_value_detail_s1 = (Label)GvViewCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value_detail");
                                Label lit_total_Competencies_view_s1 = (Label)GvViewCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies_view");

                                Label lit_totaldotted_core_value_detail_s1 = (Label)GvViewCoreValue.FooterRow.Cells[5].FindControl("lit_totaldotted_core_value_detail");
                                Label lit_totaldotted_Competencies_view_s1 = (Label)GvViewCompetencies.FooterRow.Cells[5].FindControl("lit_totaldotted_Competencies_view");

                                Label lit_totalsolid_core_value_detail_s1 = (Label)GvViewCoreValue.FooterRow.Cells[5].FindControl("lit_totalsolid_core_value_detail");
                                //Label lit_totalsolid_Competencies_view_s = (Label)GvViewCompetencies.FooterRow.Cells[5].FindControl("lit_totalsolid_Competencies_view");

                                getViewSumpointCorvalue();
                                getViewSumpointCompetencies();
                                getViewScoreTimePerformance();

                                set_scoreBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                getViewCoreValueScore(Convert.ToDouble(lit_total_core_value_detail_s1.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                getViewCompetenciesScore(Convert.ToDouble(lit_total_Competencies_view_s1.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                                //dotted
                                if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                                {
                                    set_scoreDottedBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                    getViewCoreValueScore(Convert.ToDouble(lit_totaldotted_core_value_detail_s1.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                    getViewCompetenciesScore(Convert.ToDouble(lit_totaldotted_Competencies_view_s1.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();
                                }

                                //solid
                                set_scoreSolidBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                getViewCoreValueScore(Convert.ToDouble(lit_totalsolid_core_value_detail_s1.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                getViewCompetenciesScore(Convert.ToDouble(totsolid_sumcompetencies), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();


                                //getViewFactor(GvViewFactor);

                            }
                            countsolid_compview++;

                        }

                        getViewFactor(GvViewFactor);
                        setGridColor();

                    }

                    break;
                case "rdochoicecom_dotted_detail":

                    var rdochoicecom_dotted_detail = (RadioButtonList)sender;
                    //var rdoperformance_point = (RadioButtonList)sender;
                    //var rowGrid_ = (GridViewRow)rdochoicecom_mine_detail.NamingContainer;

                    int countdotted_compview = 0;

                    if (rdochoicecom_dotted_detail.SelectedValue != "")
                    {
                        foreach (GridViewRow row in GvViewCompetencies.Rows)
                        {

                            RadioButtonList rdochoicecom_dotted_detail_ = (RadioButtonList)row.FindControl("rdochoicecom_dotted_detail");

                            if (rdochoicecom_dotted_detail_.SelectedValue != "")
                            {

                                totdotted_sumcompetencies += Convert.ToDecimal(rdochoicecom_dotted_detail_.SelectedValue);
                                Label lit_totaldotted_Competencies_view = (Label)GvViewCompetencies.FooterRow.Cells[5].FindControl("lit_totaldotted_Competencies_view");
                                lit_totaldotted_Competencies_view.Text = String.Format("{0:N2}", totdotted_sumcompetencies);

                                //Label lit_total_weighted_score_view_2 = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");

                                if (GvViewKPIs.Rows.Count > 0)
                                {
                                    Label lit_total_weighted_score_view_2 = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");

                                    tot_sumpoint_kpi = Convert.ToDecimal(lit_total_weighted_score_view_2.Text);

                                }
                                else
                                {

                                    tot_sumpoint_kpi = Convert.ToDecimal("0.00");


                                }



                                Label lit_total_core_value_detail_2 = (Label)GvViewCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value_detail");
                                Label lit_total_Competencies_view_2 = (Label)GvViewCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies_view");

                                //Label lit_totaldotted_core_value_detail = (Label)GvViewCoreValue.FooterRow.Cells[5].FindControl("lit_totaldotted_core_value_detail");
                                //Label lit_totaldotted_Competencies_view = (Label)GvViewCompetencies.FooterRow.Cells[5].FindControl("lit_totaldotted_Competencies_view");


                                getViewSumpointCorvalue();
                                getViewSumpointCompetencies();
                                getViewScoreTimePerformance();

                                set_scoreBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                getViewCoreValueScore(Convert.ToDouble(lit_total_core_value_detail_2.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                getViewCompetenciesScore(Convert.ToDouble(lit_total_Competencies_view_2.Text), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                                //dotted
                                if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                                {
                                    set_scoreDottedBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                                    getViewCoreValueScore(Convert.ToDouble(lit_total_core_value_detail_2.Text), Convert.ToDouble(sumpoint_corvalue)) + "," +
                                    getViewCompetenciesScore(Convert.ToDouble(totdotted_sumcompetencies), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                                }

                                //getViewFactor(GvViewFactor);

                            }
                            countdotted_compview++;

                        }

                        getViewFactor(GvViewFactor);

                    }

                    break;


            }

        }


    }
    #endregion


    #region Gridview
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {


            case "GvApprove1":
            case "GvApprove2":
            case "GvApprove3":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lbl_sum_total = (Label)e.Row.FindControl("lbl_sum_total");
                    Label lbl_sum_kpis_value = (Label)e.Row.FindControl("lbl_sum_kpis_value");
                    Label lbl_total_corevalue = (Label)e.Row.FindControl("lbl_total_corevalue");
                    Label lbl_total_competencies = (Label)e.Row.FindControl("lbl_total_competencies");
                    Label lbl_total_score = (Label)e.Row.FindControl("lbl_total_score");

                    lbl_sum_total.Text = Convert.ToDecimal(Convert.ToDecimal(lbl_sum_kpis_value.Text) + Convert.ToDecimal(lbl_total_corevalue.Text)
                    + Convert.ToDecimal(lbl_total_competencies.Text) + Convert.ToDecimal(lbl_total_score.Text)).ToString();

                }
                break;


            case "GvDetail":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lbl_cemp_idx = (Label)e.Row.FindControl("lbl_cemp_idx");
                    Label lblu0_docidx = (Label)e.Row.FindControl("lblu0_docidx");

                    Panel _DivDottedDetail = (Panel)e.Row.FindControl("_DivDottedDetail");
                    Panel _DivSolidDetail = (Panel)e.Row.FindControl("_DivSolidDetail");

                    _DivDottedDetail.Visible = false;
                    _DivSolidDetail.Visible = false;

                    pmsu0_DocFormDetail[] _templistPermissionDetail_Solid = (pmsu0_DocFormDetail[])ViewState["vs_CheckPermissionDetail"];
                    var _linqDataListPermissionDetail_Solid = (from dt in _templistPermissionDetail_Solid
                                                               where
                                                               (dt.u0_docidx == int.Parse(lblu0_docidx.Text)
                                                               && dt.eval_emp_idx == _emp_idx
                                                               && (dt.eval_emp_type == 2 || dt.eval_emp_type == 4 || dt.eval_emp_type == 5 || dt.eval_emp_type == 6)
                                                               ) // solid

                                                               select dt).Distinct().ToList();

                    pmsu0_DocFormDetail[] _templistPermissionDetail_Dotted = (pmsu0_DocFormDetail[])ViewState["vs_CheckPermissionDetail"];
                    var _linqDataListPermissionDetail_Dotted = (from dt in _templistPermissionDetail_Dotted
                                                                where
                                                                (dt.u0_docidx == int.Parse(lblu0_docidx.Text)
                                                                && dt.eval_emp_idx == _emp_idx
                                                                && dt.eval_emp_type == 3
                                                                ) // dotted

                                                                select dt).Distinct().ToList();

                    pmsu0_DocFormDetail[] _templistPer_Dotted = (pmsu0_DocFormDetail[])ViewState["vs_CheckPermissionDetail"];
                    var _linqDataListPer_Dotted = (from dt in _templistPer_Dotted
                                                   where
                                                   (dt.u0_docidx == int.Parse(lblu0_docidx.Text)
                                                   && dt.eval_emp_type == 3
                                                   ) // dotted

                                                   select dt).Distinct().ToList();


                    //GvDetail.Columns[2].Visible = false

                    if (_linqDataListPermissionDetail_Dotted.Count() > 0)
                    {
                        if (_linqDataListPer_Dotted.Count() > 0)
                        {
                            _DivDottedDetail.Visible = true;

                        }


                    }

                    //litDebug.Text = _emp_idx.ToString();
                    if (_linqDataListPermissionDetail_Solid.Count() > 0)
                    {
                        if (_linqDataListPer_Dotted.Count() > 0)
                        {
                            _DivDottedDetail.Visible = true;

                        }

                        _DivSolidDetail.Visible = true;


                    }



                }
                break;

            case "GvKPIs":

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].Text = "ดัชนีชี้วัดผลปฏิบัติงาน" + "<br />" + "Performance indicators";
                    e.Row.Cells[1].Text = "หน่วย" + "<br />" + "Unit";
                    e.Row.Cells[2].Text = "น้ำหนัก" + "<br />" + "Weight";
                    e.Row.Cells[3].Text = "เป้าหมาย" + "<br />" + "Target";
                    e.Row.Cells[4].Text = "ผลลัพธ์" + "<br />" + "Result";
                    e.Row.Cells[5].Text = "คะแนน" + "<br />" + "KPIs Score";
                    e.Row.Cells[6].Text = "คะแนนที่ได้" + "<br />" + "Weighted Score";

                }
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label lblm1_kpi_idx = (Label)e.Row.FindControl("lblm1_kpi_idx");
                    Label lblweighted_score_ = (Label)e.Row.FindControl("lblweighted_score");



                    data_pms data_u0_formdetail = new data_pms();
                    pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
                    data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

                    u0_formdetail.condition = 23;
                    u0_formdetail.m1_kpi_idx = int.Parse(lblm1_kpi_idx.Text);

                    data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;
                    // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

                    data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);

                    ViewState["vs_PointChangeted"] = data_u0_formdetail.Boxpmsm0_DocFormDetail;


                    setRdoData(rdoperformance_level, data_u0_formdetail.Boxpmsm0_DocFormDetail, "perf_detail", "m2_kpi_idx");
                    setRdoData(rdoperformance_point, data_u0_formdetail.Boxpmsm0_DocFormDetail, "pref_point", "m2_kpi_idx");


                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    Label lit_total_weighted_score = (Label)e.Row.FindControl("lit_total_weighted_score");
                    lit_total_weighted_score.Text = String.Format("{0:N2}", tot_sumpoint_kpi); // Convert.ToString(tot_actual);

                    getScoreTimePerformance();

                    set_scoreBefore = "0.00" + "," + "0.00" + "," + "0.00" + "," + set_timeAttendanceScore.ToString();
                    getFactor(GvFactor);



                }

                break;
            case "GvMasterPoint":
            case "GvViewMasterPoint":

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].Text = "คะแนน" + "<br />" + "Score";
                    e.Row.Cells[1].Text = "ความหมาย" + "<br />" + "Rating Name";
                    e.Row.Cells[2].Text = "แนวทางของเกณฑ์ประเมินพฤติกรรม" + "<br />" + "Definition";
                }

                break;
            case "GvCoreValue":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblm1_coreidx = (Label)e.Row.FindControl("lblm1_coreidx");
                    GridView GvSub_CoreValue = (GridView)e.Row.FindControl("GvSub_CoreValue");
                    GridView GvSelfAssessment = (GridView)e.Row.FindControl("GvSelfAssessment");

                    RadioButtonList rdochoice_mine = (RadioButtonList)e.Row.FindControl("rdochoice_mine");

                    RadioButtonList rdochoice_head = (RadioButtonList)e.Row.FindControl("rdochoice_head");

                    int rowindex = e.Row.RowIndex;
                    count++;

                    e.Row.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml("#DAEEF3");
                    e.Row.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml("#DAEEF3");

                    getSubCoreValue(GvSub_CoreValue, int.Parse(lblm1_coreidx.Text));


                    data_pms data_u0_formdetail = new data_pms();
                    pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
                    data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

                    u0_formdetail.condition = 8;

                    data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;
                    // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

                    data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);

                    setRdoData(rdochoice_mine, data_u0_formdetail.Boxpmsm0_DocFormDetail, "namechoice", "m0_point");



                }

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].ColumnSpan = 3;
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[2].Visible = false;

                    e.Row.Cells[0].Text = "ปัจจัยประเมิน" + "<br />" + "Core Values Factor";
                    e.Row.Cells[1].Text = "ปัจจัยประเมิน" + "<br />" + "Core Values Factor";
                    e.Row.Cells[2].Text = "ปัจจัยประเมิน" + "<br />" + "Core Values Factor";
                    e.Row.Cells[3].Text = "พฤติกรรมการทำงานที่บริษัทคาดหวัง" + "<br />" + "Behavioral Indicators";
                    e.Row.Cells[4].Text = "ประเมินตนเอง" + "<br />" + "Self Assessment (1-4)";
                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    Label lit_total_core_value = (Label)e.Row.FindControl("lit_total_core_value");
                    lit_total_core_value.Text = String.Format("{0:N2}", tot_sumcore_value); // Convert.ToString(tot_actual);

                    getScoreTimePerformance();

                    set_scoreBefore = tot_sumpoint_kpi.ToString() + "," + lit_total_core_value.Text + "," + "0.00" + "," + set_timeAttendanceScore.ToString();
                    //getFactor(GvFactor);

                }

                break;
            case "GvCompetencies":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblu0_typeidx = (Label)e.Row.FindControl("lblu0_typeidx");
                    Label lblm1_typeidx = (Label)e.Row.FindControl("lblm1_typeidx");
                    Label lblform_name = (Label)e.Row.FindControl("lblform_name");
                    Label lblno = (Label)e.Row.FindControl("lblno");

                    GridView GvSub_Competencies = (GridView)e.Row.FindControl("GvSub_Competencies");
                    RadioButtonList rdochoicecom_mine = (RadioButtonList)e.Row.FindControl("rdochoicecom_mine");
                    RadioButtonList rdochoicecom_head = (RadioButtonList)e.Row.FindControl("rdochoicecom_head");
                    count++;
                    lblno.Text = count.ToString();
                    e.Row.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml("#E4DFEC");
                    e.Row.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml("#E4DFEC");


                    ViewState["form_name"] = lblform_name.Text;
                    ViewState["u0_typeidx"] = lblu0_typeidx.Text;

                    getSubcompentency(GvSub_Competencies, int.Parse(lblu0_typeidx.Text), int.Parse(lblm1_typeidx.Text));

                    data_pms data_u0_formdetail = new data_pms();
                    pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
                    data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

                    u0_formdetail.condition = 8;

                    data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;
                    // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

                    data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);

                    setRdoData(rdochoicecom_mine, data_u0_formdetail.Boxpmsm0_DocFormDetail, "namechoice", "m0_point");

                }

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].ColumnSpan = 3;
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[2].Visible = false;

                    e.Row.Cells[0].Text = "ปัจจัยประเมิน" + "<br />" + "Competency Factor";
                    e.Row.Cells[1].Text = "ปัจจัยประเมิน" + "<br />" + "Competency Factor";
                    e.Row.Cells[2].Text = "ปัจจัยประเมิน" + "<br />" + "Competency Factor";
                    e.Row.Cells[3].Text = "พฤติกรรมการทำงานที่บริษัทคาดหวัง" + "<br />" + "Behavioral Indicators";
                    e.Row.Cells[4].Text = "ประเมินตนเอง" + "<br />" + "Self Assessment (1-4)";

                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    Label lit_total_Competencies = (Label)e.Row.FindControl("lit_total_Competencies");
                    lit_total_Competencies.Text = String.Format("{0:N2}", tot_sumcompetencies); // Convert.ToString(tot_actual);

                    getScoreTimePerformance();

                    set_scoreBefore = tot_sumpoint_kpi.ToString() + "," + tot_sumcore_value.ToString() + "," + lit_total_Competencies.Text + "," + set_timeAttendanceScore.ToString();
                    //getFactor (GvFactor);

                }
                break;
            case "GvViewKPIs":

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].Text = "ดัชนีชี้วัดผลปฏิบัติงาน" + "<br />" + "Performance indicators";
                    e.Row.Cells[1].Text = "หน่วย" + "<br />" + "Unit";
                    e.Row.Cells[2].Text = "น้ำหนัก" + "<br />" + "Weight";
                    e.Row.Cells[3].Text = "เป้าหมาย" + "<br />" + "Target";
                    e.Row.Cells[4].Text = "ผลลัพธ์" + "<br />" + "Result";
                    e.Row.Cells[5].Text = "คะแนน" + "<br />" + "KPIs Score";
                    e.Row.Cells[6].Text = "คะแนนที่ได้" + "<br />" + "Weighted Score";

                }
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {


                    Label lblm1_kpi_idx_view = (Label)e.Row.FindControl("lblm1_kpi_idx_view");
                    Label lblm2_kpi_idx_view = (Label)e.Row.FindControl("lblm2_kpi_idx_view");
                    Label lblweighted_score_view = (Label)e.Row.FindControl("lblweighted_score_view");
                    Label lblkpi_weight_view = (Label)e.Row.FindControl("lblkpi_weight_view");

                    RadioButtonList rdoperformance_level_view = (RadioButtonList)e.Row.FindControl("rdoperformance_level_view");
                    TextBox txtkpi_view = (TextBox)e.Row.FindControl("txtkpi_view");

                    //set data
                    rdoperformance_level_view.Enabled = false;
                    txtkpi_view.Enabled = false;


                    //getDateSampleListEdit(rdoperformance_level_view, int.Parse(lblm1_kpi_idx_view.Text));

                    data_pms data_u0_formdetail = new data_pms();
                    pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
                    data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

                    u0_formdetail.condition = 23;
                    u0_formdetail.m1_kpi_idx = int.Parse(lblm1_kpi_idx_view.Text);

                    data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

                    data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);

                    ViewState["vs_ViewPointChangeted"] = data_u0_formdetail.Boxpmsm0_DocFormDetail;

                    setRdoData(rdoperformance_level_view, data_u0_formdetail.Boxpmsm0_DocFormDetail, "perf_detail", "m2_kpi_idx");

                    setRdoData(rdoperformance_point_view, data_u0_formdetail.Boxpmsm0_DocFormDetail, "pref_point", "m2_kpi_idx");
                    rdoperformance_level_view.SelectedValue = lblm2_kpi_idx_view.Text;
                    rdoperformance_point_view.SelectedValue = lblm2_kpi_idx_view.Text;

                    lblweighted_score_view.Text = getWeightScore(Convert.ToDouble(lblkpi_weight_view.Text), Convert.ToDouble(rdoperformance_level_view.SelectedIndex));

                    if (lblweighted_score_view.Text != "")
                    {
                        tot_sumpoint_kpi += Convert.ToDecimal(lblweighted_score_view.Text);

                    }

                    if (_emp_idx == int.Parse(ViewState["cemp_idx_detail"].ToString()) && ViewState["unidx_detail"].ToString() == "1")
                    {
                        rdoperformance_level_view.Enabled = true;
                        txtkpi_view.Enabled = true;

                    }
                    else if (ViewState["unidx_detail"].ToString() == "2" && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0)
                    {
                        rdoperformance_level_view.Enabled = true;
                        txtkpi_view.Enabled = true;


                    }
                    else if (ViewState["unidx_detail"].ToString() == "3" && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0)
                    {
                        rdoperformance_level_view.Enabled = true;
                        txtkpi_view.Enabled = true;

                    }


                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    Label lit_total_weighted_score_view = (Label)e.Row.FindControl("lit_total_weighted_score_view");
                    lit_total_weighted_score_view.Text = String.Format("{0:N2}", tot_sumpoint_kpi); // Convert.ToString(tot_actual);

                    //set_scoreBefore = "0.00" + "," + "0.00" + "," + "0.00" + "," + "0.00";
                    // Label lit_total_weighted_score_view_ = (Label)GvViewKPIs.FooterRow.Cells[6].FindControl("lit_total_weighted_score_view");
                    // Label lit_total_core_value_detail_ = (Label)GvViewCoreValue.FooterRow.Cells[4].FindControl("lit_total_core_value_detail");
                    // Label lit_total_Competencies_view_ = (Label)GvViewCompetencies.FooterRow.Cells[4].FindControl("lit_total_Competencies_view");

                    getViewSumpointCorvalue();
                    getViewSumpointCompetencies();
                    getViewScoreTimePerformance();

                    set_scoreBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                    getViewCoreValueScore(Convert.ToDouble(tot_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                    getViewCompetenciesScore(Convert.ToDouble(tot_sumcompetencies), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();


                    //dotted
                    if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                    {
                        set_scoreDottedBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                        getViewCoreValueScore(Convert.ToDouble(totdotted_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                        getViewCompetenciesScore(Convert.ToDouble(totdotted_sumcompetencies), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();
                    }
                    //solid
                    set_scoreSolidBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                    getViewCoreValueScore(Convert.ToDouble(totsolid_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                    getViewCompetenciesScore(Convert.ToDouble(totsolid_sumcompetencies), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                    //getViewFactor(GvViewFactor);

                }

                break;
            case "GvViewCoreValue":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblm1_coreidx = (Label)e.Row.FindControl("lblm1_coreidx");
                    GridView gvsubcorevalue_view = (GridView)e.Row.FindControl("gvsubcorevalue_view");
                    RadioButtonList rdochoice_mine_detail = (RadioButtonList)e.Row.FindControl("rdochoice_mine_detail");
                    RadioButtonList rdochoice_head_detail = (RadioButtonList)e.Row.FindControl("rdochoice_head_detail");
                    RadioButtonList rdochoice_dotted_detail = (RadioButtonList)e.Row.FindControl("rdochoice_dotted_detail");

                    TextBox txtremarkmine_core = (TextBox)e.Row.FindControl("txtremarkmine_core");
                    Panel panel_update_comment_coremine = (Panel)e.Row.FindControl("panel_update_comment_coremine");
                    Panel panel_update_comment_corehead = (Panel)e.Row.FindControl("panel_update_comment_corehead");
                    Label lblanswer = (Label)e.Row.FindControl("lblanswer");
                    Label lblanswer_head = (Label)e.Row.FindControl("lblanswer_head");
                    Label lblanswer_dotted = (Label)e.Row.FindControl("lblanswer_dotted");

                    UpdatePanel _PnSolidSuperiorcor = (UpdatePanel)e.Row.FindControl("_PnSolidSuperiorcor");
                    UpdatePanel _PnDottedSuperiorcor = (UpdatePanel)e.Row.FindControl("_PnDottedSuperiorcor");

                    TextBox txtremarkhead_core = (TextBox)e.Row.FindControl("txtremarkhead_core");
                    TextBox txtremarkdotted_core = (TextBox)e.Row.FindControl("txtremarkdotted_core");

                    //set bind
                    GvViewCoreValue.Columns[5].Visible = false;
                    rdochoice_mine_detail.Enabled = false;
                    txtremarkmine_core.Enabled = false;

                    _PnSolidSuperiorcor.Visible = false;
                    rdochoice_head_detail.Enabled = false;
                    txtremarkhead_core.Enabled = false;

                    _PnDottedSuperiorcor.Visible = false;
                    rdochoice_dotted_detail.Enabled = false;
                    txtremarkdotted_core.Enabled = false;


                    //set bind

                    int point_convert_mine;
                    decimal point_mine;

                    int point_convert_head;
                    decimal point_head;

                    int point_convert_dotted;
                    decimal point_dotted;

                    int rowindex = e.Row.RowIndex;
                    count++;

                    e.Row.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml("#DAEEF3");
                    e.Row.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml("#DAEEF3");


                    point_mine = Convert.ToDecimal(lblanswer.Text);
                    point_convert_mine = Decimal.ToInt32(point_mine);

                    point_head = Convert.ToDecimal(lblanswer_head.Text);
                    point_convert_head = Decimal.ToInt32(point_head);

                    point_dotted = Convert.ToDecimal(lblanswer_dotted.Text);
                    point_convert_dotted = Decimal.ToInt32(point_dotted);

                    getSubCoreValue(gvsubcorevalue_view, int.Parse(lblm1_coreidx.Text));

                    data_pms data_u0_formdetail = new data_pms();
                    pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
                    data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

                    u0_formdetail.condition = 8;

                    data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;
                    // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

                    data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);

                    setRdoData(rdochoice_mine_detail, data_u0_formdetail.Boxpmsm0_DocFormDetail, "namechoice", "m0_point");
                    rdochoice_mine_detail.SelectedValue = point_convert_mine.ToString();


                    setRdoData(rdochoice_head_detail, data_u0_formdetail.Boxpmsm0_DocFormDetail, "namechoice", "m0_point");
                    rdochoice_head_detail.SelectedValue = point_convert_head.ToString();


                    setRdoData(rdochoice_dotted_detail, data_u0_formdetail.Boxpmsm0_DocFormDetail, "namechoice", "m0_point");
                    rdochoice_dotted_detail.SelectedValue = point_convert_dotted.ToString();

                    if (_emp_idx == int.Parse(ViewState["cemp_idx_detail"].ToString()) && ViewState["unidx_detail"].ToString() == "1")
                    {
                        rdochoice_mine_detail.Enabled = true;
                        txtremarkmine_core.Enabled = true;

                    }

                    //litDebug.Text += Truncate("1 จำเป็นต้องได้รับการพัฒนา ไม่สามารถคาดหวังผลงานได้", 10).ToString() + "|";




                    //check dotted
                    if (ViewState["unidx_detail"].ToString() == "2" && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0) //dotted
                    {


                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {

                            GvViewCoreValue.Columns[5].Visible = true;

                            _PnDottedSuperiorcor.Visible = true;
                            rdochoice_dotted_detail.Enabled = true;
                            txtremarkdotted_core.Enabled = true;


                        }


                    }
                    else if (int.Parse(ViewState["unidx_detail"].ToString()) > 2 && (int.Parse(ViewState["vs_CheckPermission_Detail_Dotted"].ToString()) > 0 || int.Parse(ViewState["vs_CheckPermission_Detail_Solid"].ToString()) > 0))
                    {


                        //_PnDottedSuperiorcor.Visible = true;
                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {
                            GvViewCoreValue.Columns[5].Visible = true;
                            _PnDottedSuperiorcor.Visible = true;

                            //_PnDottedSuperiorcor.Attributes.Add("class","#FF0000");


                        }

                    }
                    else if (ViewState["staidx_detail"].ToString() == "2" && _permission == 1)
                    {
                        GvViewCoreValue.Columns[5].Visible = true;
                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {
                            _PnDottedSuperiorcor.Visible = true;
                        }
                        //_PnDottedSuperiorcor.Attributes.Add("class","#FF0000");

                    }


                    //check solid
                    if (ViewState["unidx_detail"].ToString() == "3" && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0) //solid
                    {

                        GvViewCoreValue.Columns[5].Visible = true;
                        _PnSolidSuperiorcor.Visible = true;
                        rdochoice_head_detail.Enabled = true;
                        txtremarkhead_core.Enabled = true;

                        _PnSolidSuperiorcor.Attributes.Add("style", "background-color: #FBCAC0");
                        //_PnSolidSuperiorcor.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#FF0000");
                        //_PnSolidSuperiorcor.BackgroundColor = "#FF0000";

                    }
                    else if (int.Parse(ViewState["unidx_detail"].ToString()) > 3 && int.Parse(ViewState["vs_CheckPermission_Detail_Solid"].ToString()) > 0)
                    {

                        GvViewCoreValue.Columns[5].Visible = true;
                        _PnSolidSuperiorcor.Visible = true;
                        //_PnSolidSuperiorcor.Attributes.Add("class","#FF0000");

                    }
                    else if (ViewState["staidx_detail"].ToString() == "2" && _permission == 1)
                    {

                        GvViewCoreValue.Columns[5].Visible = true;
                        _PnSolidSuperiorcor.Visible = true;
                        //_PnSolidSuperiorcor.Attributes.Add("class","#FF0000");

                    }


                    //check approve1/2/3
                    if ((ViewState["unidx_detail"].ToString() == "4" || ViewState["unidx_detail"].ToString() == "5" || ViewState["unidx_detail"].ToString() == "6")
                    && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0) //approve1/2/3
                    {

                        GvViewCoreValue.Columns[5].Visible = true;
                        _PnSolidSuperiorcor.Visible = true;

                    }

                    //RadioButtonList rdochoice_mine_detail = (RadioButtonList)e.Row.FindControl("rdochoice_mine_detail");
                    tot_sumcore_value += Convert.ToDecimal(rdochoice_mine_detail.SelectedValue);

                    if (rdochoice_head_detail.SelectedValue != "")
                    {
                        totsolid_sumcore_value += Convert.ToDecimal(rdochoice_head_detail.SelectedValue);

                    }

                    if (rdochoice_dotted_detail.SelectedValue != "")
                    {
                        totdotted_sumcore_value += Convert.ToDecimal(rdochoice_dotted_detail.SelectedValue);

                    }

                }

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].ColumnSpan = 3;
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[2].Visible = false;

                    e.Row.Cells[0].Text = "ปัจจัยประเมิน" + "<br />" + "Core Values Factor";
                    e.Row.Cells[1].Text = "ปัจจัยประเมิน" + "<br />" + "Core Values Factor";
                    e.Row.Cells[2].Text = "ปัจจัยประเมิน" + "<br />" + "Core Values Factor";
                    e.Row.Cells[3].Text = "พฤติกรรมการทำงานที่บริษัทคาดหวัง" + "<br />" + "Behavioral Indicators";
                    e.Row.Cells[4].Text = "ประเมิน" + "<br />" + "Rating(1-4)";
                    e.Row.Cells[5].Text = "Solid/ Dotted ประเมิน" + "<br />" + "Superior's Rating(1-4)";
                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {



                    Label lit_total_core_value_detail = (Label)e.Row.FindControl("lit_total_core_value_detail");
                    lit_total_core_value_detail.Text = String.Format("{0:N2}", tot_sumcore_value); // Convert.ToString(tot_actual);

                    Label lit_totalsolid_core_value_detail = (Label)e.Row.FindControl("lit_totalsolid_core_value_detail");
                    Label lit_totaldotted_core_value_detail = (Label)e.Row.FindControl("lit_totaldotted_core_value_detail");

                    Panel div_dotted_core_value = (Panel)e.Row.FindControl("div_dotted_core_value");
                    Panel div_solid_core_value = (Panel)e.Row.FindControl("div_solid_core_value");

                    div_dotted_core_value.Visible = false;
                    div_solid_core_value.Visible = false;


                    //check dotted
                    if (ViewState["unidx_detail"].ToString() == "2" && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0) //dotted
                    {
                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {
                            div_dotted_core_value.Visible = true;

                            //lit_totaldotted_core_value_detail.Visible = true;
                            lit_totaldotted_core_value_detail.Text = String.Format("{0:N2}", totdotted_sumcore_value); // Convert.ToString
                        }

                    }
                    else if (int.Parse(ViewState["unidx_detail"].ToString()) > 2 && (int.Parse(ViewState["vs_CheckPermission_Detail_Dotted"].ToString()) > 0 || int.Parse(ViewState["vs_CheckPermission_Detail_Solid"].ToString()) > 0))
                    {
                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {
                            div_dotted_core_value.Visible = true;

                            //lit_totaldotted_core_value_detail.Visible = true;

                            lit_totaldotted_core_value_detail.Text = String.Format("{0:N2}", totdotted_sumcore_value); // Convert.ToString
                        }

                    }
                    else if (ViewState["staidx_detail"].ToString() == "2" && _permission == 1)
                    {
                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {
                            div_dotted_core_value.Visible = true;

                            //lit_totaldotted_core_value_detail.Visible = true;

                            lit_totaldotted_core_value_detail.Text = String.Format("{0:N2}", totdotted_sumcore_value); // Convert.ToString
                        }

                    }


                    //check solid
                    if (ViewState["unidx_detail"].ToString() == "3" && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0) //solid
                    {


                        div_solid_core_value.Visible = true;

                        //lit_totalsolid_core_value_detail.Visible = true;
                        lit_totalsolid_core_value_detail.Text = String.Format("{0:N2}", totsolid_sumcore_value);

                    }
                    else if (int.Parse(ViewState["unidx_detail"].ToString()) > 3 && int.Parse(ViewState["vs_CheckPermission_Detail_Solid"].ToString()) > 0)
                    {

                        div_solid_core_value.Visible = true;

                        //lit_totalsolid_core_value_detail.Visible = true;
                        lit_totalsolid_core_value_detail.Text = String.Format("{0:N2}", totsolid_sumcore_value);

                    }
                    else if (ViewState["staidx_detail"].ToString() == "2" && _permission == 1)
                    {
                        div_solid_core_value.Visible = true;

                        //lit_totalsolid_core_value_detail.Visible = true;
                        lit_totalsolid_core_value_detail.Text = String.Format("{0:N2}", totsolid_sumcore_value);

                    }

                    getViewSumpointCorvalue();
                    getViewSumpointCompetencies();
                    getViewScoreTimePerformance();

                    set_scoreBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                    getViewCoreValueScore(Convert.ToDouble(tot_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                    getViewCompetenciesScore(Convert.ToDouble(tot_sumcompetencies), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                    //dotted
                    if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                    {
                        set_scoreDottedBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                        getViewCoreValueScore(Convert.ToDouble(totdotted_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                        getViewCompetenciesScore(Convert.ToDouble(totdotted_sumcompetencies), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();
                    }
                    //solid
                    set_scoreSolidBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                    getViewCoreValueScore(Convert.ToDouble(totsolid_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                    getViewCompetenciesScore(Convert.ToDouble(totsolid_sumcompetencies), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                    //getViewFactor(GvViewFactor);

                }

                break;
            case "GvViewCompetencies":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblu0_typeidx = (Label)e.Row.FindControl("lblu0_typeidx");
                    Label lblm1_typeidx = (Label)e.Row.FindControl("lblm1_typeidx");
                    Label lblform_name = (Label)e.Row.FindControl("lblform_name");
                    Label lblno = (Label)e.Row.FindControl("lblno");

                    Label lblanswer = (Label)e.Row.FindControl("lblanswer_com");
                    Label lblanswer_head = (Label)e.Row.FindControl("lblanswer_headcom");
                    Label lblanswer_dottedcom = (Label)e.Row.FindControl("lblanswer_dottedcom");


                    GridView gvsubcompetencies_view = (GridView)e.Row.FindControl("gvsubcompetencies_view");
                    RadioButtonList rdochoicecom_mine_detail = (RadioButtonList)e.Row.FindControl("rdochoicecom_mine_detail");
                    TextBox txtremark_detail = (TextBox)e.Row.FindControl("txtremark_detail");

                    RadioButtonList rdochoicecom_head_detail = (RadioButtonList)e.Row.FindControl("rdochoicecom_head_detail");
                    RadioButtonList rdochoicecom_dotted_detail = (RadioButtonList)e.Row.FindControl("rdochoicecom_dotted_detail");
                    UpdatePanel _PnSuperiorcom = (UpdatePanel)e.Row.FindControl("_PnSuperiorcom");
                    UpdatePanel _PnDottedSuperiorcom = (UpdatePanel)e.Row.FindControl("_PnDottedSuperiorcom");


                    TextBox txtremark_head = (TextBox)e.Row.FindControl("txtremark_head");
                    TextBox txtremark_dotted = (TextBox)e.Row.FindControl("txtremark_dotted");


                    //set bind
                    GvViewCompetencies.Columns[5].Visible = false;
                    rdochoicecom_mine_detail.Enabled = false;
                    txtremark_detail.Enabled = false;

                    _PnSuperiorcom.Visible = false;
                    rdochoicecom_head_detail.Enabled = false;
                    txtremark_head.Enabled = false;

                    _PnDottedSuperiorcom.Visible = false;
                    rdochoicecom_dotted_detail.Enabled = false;
                    txtremark_dotted.Enabled = false;

                    int point_convert_mine;
                    decimal point_mine;

                    int point_convert_head;
                    decimal point_head;

                    int point_convert_dotted;
                    decimal point_dotted;

                    point_mine = Convert.ToDecimal(lblanswer.Text);
                    point_convert_mine = Decimal.ToInt32(point_mine);

                    point_head = Convert.ToDecimal(lblanswer_head.Text);
                    point_convert_head = Decimal.ToInt32(point_head);

                    point_dotted = Convert.ToDecimal(lblanswer_dottedcom.Text);
                    point_convert_dotted = Decimal.ToInt32(point_dotted);

                    count++;
                    lblno.Text = count.ToString();
                    e.Row.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml("#E4DFEC");
                    e.Row.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml("#E4DFEC");

                    getSubcompentency(gvsubcompetencies_view, int.Parse(lblu0_typeidx.Text), int.Parse(lblm1_typeidx.Text));

                    data_pms data_u0_formdetail = new data_pms();
                    pmsu0_DocFormDetail u0_formdetail = new pmsu0_DocFormDetail();
                    data_u0_formdetail.Boxpmsu0_DocFormDetail = new pmsu0_DocFormDetail[1];

                    u0_formdetail.condition = 8;

                    data_u0_formdetail.Boxpmsu0_DocFormDetail[0] = u0_formdetail;
                    // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_formdetail));

                    data_u0_formdetail = callServicePostPMS(_urlSelectMaster, data_u0_formdetail);

                    setRdoData(rdochoicecom_mine_detail, data_u0_formdetail.Boxpmsm0_DocFormDetail, "namechoice", "m0_point");
                    rdochoicecom_mine_detail.SelectedValue = point_convert_mine.ToString();



                    setRdoData(rdochoicecom_head_detail, data_u0_formdetail.Boxpmsm0_DocFormDetail, "namechoice", "m0_point");
                    rdochoicecom_head_detail.SelectedValue = point_convert_head.ToString();

                    setRdoData(rdochoicecom_dotted_detail, data_u0_formdetail.Boxpmsm0_DocFormDetail, "namechoice", "m0_point");
                    rdochoicecom_dotted_detail.SelectedValue = point_convert_dotted.ToString();

                    if (_emp_idx == int.Parse(ViewState["cemp_idx_detail"].ToString()) && ViewState["unidx_detail"].ToString() == "1") //user
                    {
                        rdochoicecom_mine_detail.Enabled = true;
                        txtremark_detail.Enabled = true;

                    }


                    //dotted
                    if (ViewState["unidx_detail"].ToString() == "2" && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0) //dotted
                    {


                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {
                            GvViewCompetencies.Columns[5].Visible = true;
                            _PnDottedSuperiorcom.Visible = true;
                            rdochoicecom_dotted_detail.Enabled = true;
                            txtremark_dotted.Enabled = true;



                        }


                    }
                    else if (int.Parse(ViewState["unidx_detail"].ToString()) > 2 && (int.Parse(ViewState["vs_CheckPermission_Detail_Dotted"].ToString()) > 0 || int.Parse(ViewState["vs_CheckPermission_Detail_Solid"].ToString()) > 0))
                    {

                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {
                            GvViewCompetencies.Columns[5].Visible = true;
                            _PnDottedSuperiorcom.Visible = true;
                        }

                    }
                    else if (ViewState["staidx_detail"].ToString() == "2" && _permission == 1)
                    {

                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {
                            GvViewCompetencies.Columns[5].Visible = true;
                            _PnDottedSuperiorcom.Visible = true;
                        }

                    }



                    //solid
                    if (ViewState["unidx_detail"].ToString() == "3" && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0) //solid
                    {

                        GvViewCompetencies.Columns[5].Visible = true;
                        _PnSuperiorcom.Visible = true;
                        rdochoicecom_head_detail.Enabled = true;
                        txtremark_head.Enabled = true;

                        _PnSuperiorcom.Attributes.Add("style", "background-color: #FBCAC0");

                    }
                    else if (int.Parse(ViewState["unidx_detail"].ToString()) > 3 && int.Parse(ViewState["vs_CheckPermission_Detail_Solid"].ToString()) > 0)
                    {
                        GvViewCompetencies.Columns[5].Visible = true;
                        _PnSuperiorcom.Visible = true;
                        rdochoicecom_head_detail.Visible = true;

                    }
                    else if (ViewState["staidx_detail"].ToString() == "2" && _permission == 1)
                    {
                        GvViewCompetencies.Columns[5].Visible = true;
                        _PnSuperiorcom.Visible = true;
                        rdochoicecom_head_detail.Visible = true;

                    }


                    if ((ViewState["unidx_detail"].ToString() == "4" || ViewState["unidx_detail"].ToString() == "5" || ViewState["unidx_detail"].ToString() == "6")
                    && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0) // approve1/2/3
                    {

                        GvViewCompetencies.Columns[5].Visible = true;
                        _PnSuperiorcom.Visible = true;

                    }


                    tot_sumcompetencies += Convert.ToDecimal(rdochoicecom_mine_detail.SelectedValue);

                    if (rdochoicecom_head_detail.SelectedValue != "")
                    {
                        totsolid_sumcompetencies += Convert.ToDecimal(rdochoicecom_head_detail.SelectedValue);
                    }

                    if (rdochoicecom_dotted_detail.SelectedValue != "")
                    {
                        totdotted_sumcompetencies += Convert.ToDecimal(rdochoicecom_dotted_detail.SelectedValue);
                    }



                }
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].ColumnSpan = 3;
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[2].Visible = false;

                    e.Row.Cells[0].Text = "ปัจจัยประเมิน" + "<br />" + "Competency Factor";
                    e.Row.Cells[1].Text = "ปัจจัยประเมิน" + "<br />" + "Competency Factor";
                    e.Row.Cells[2].Text = "ปัจจัยประเมิน" + "<br />" + "Competency Factor";
                    e.Row.Cells[3].Text = "พฤติกรรมการทำงานที่บริษัทคาดหวัง" + "<br />" + "Behavioral Indicators";
                    e.Row.Cells[4].Text = "ประเมิน" + "<br />" + "Rating(1-4)";
                    e.Row.Cells[5].Text = "Solid/ Dotted ประเมิน" + "<br />" + "Superior's Rating(1-4)";
                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    Label lit_total_Competencies_view = (Label)e.Row.FindControl("lit_total_Competencies_view");
                    lit_total_Competencies_view.Text = String.Format("{0:N2}", tot_sumcompetencies); // Convert.ToString(tot_actual);

                    Label lit_totalsolid_Competencies_view = (Label)e.Row.FindControl("lit_totalsolid_Competencies_view");
                    Label lit_totaldotted_Competencies_view = (Label)e.Row.FindControl("lit_totaldotted_Competencies_view");
                    //lit_totalsolid_Competencies_view.Text = String.Format("{0:N2}", totsolid_sumcompetencies); // Convert.ToString

                    Panel div_dotted_Competencies = (Panel)e.Row.FindControl("div_dotted_Competencies");
                    Panel div_solid_Competencies = (Panel)e.Row.FindControl("div_solid_Competencies");

                    div_dotted_Competencies.Visible = false;
                    div_solid_Competencies.Visible = false;


                    //check dotted
                    if (ViewState["unidx_detail"].ToString() == "2" && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0) //dotted
                    {

                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {
                            div_dotted_Competencies.Visible = true;
                            //lit_totaldotted_Competencies_view.Visible = true;

                            lit_totaldotted_Competencies_view.Text = String.Format("{0:N2}", totdotted_sumcompetencies); // Convert.ToString


                        }

                    }
                    else if (int.Parse(ViewState["unidx_detail"].ToString()) > 2 && (int.Parse(ViewState["vs_CheckPermission_Detail_Dotted"].ToString()) > 0 || int.Parse(ViewState["vs_CheckPermission_Detail_Solid"].ToString()) > 0))
                    {

                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {
                            div_dotted_Competencies.Visible = true;
                            //lit_totaldotted_Competencies_view.Visible = true;
                            lit_totaldotted_Competencies_view.Text = String.Format("{0:N2}", totdotted_sumcompetencies); // Convert.ToString

                        }

                    }
                    else if (ViewState["staidx_detail"].ToString() == "2" && _permission == 1)
                    {
                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {
                            div_dotted_Competencies.Visible = true;
                            //lit_totaldotted_Competencies_view.Visible = true;
                            lit_totaldotted_Competencies_view.Text = String.Format("{0:N2}", totdotted_sumcompetencies); // Convert.ToString
                        }

                    }


                    //check solid
                    if (ViewState["unidx_detail"].ToString() == "3" && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0) //solid
                    {
                        div_solid_Competencies.Visible = true;

                        //lit_totalsolid_Competencies_view.Visible = true;
                        lit_totalsolid_Competencies_view.Text = String.Format("{0:N2}", totsolid_sumcompetencies);

                    }
                    else if (int.Parse(ViewState["unidx_detail"].ToString()) > 3 && int.Parse(ViewState["vs_CheckPermission_Detail_Solid"].ToString()) > 0)
                    {
                        div_solid_Competencies.Visible = true;

                        //lit_totalsolid_Competencies_view.Visible = true;
                        lit_totalsolid_Competencies_view.Text = String.Format("{0:N2}", totsolid_sumcompetencies);

                    }
                    else if (ViewState["staidx_detail"].ToString() == "2" && _permission == 1)
                    {
                        div_solid_Competencies.Visible = true;

                        //lit_totalsolid_Competencies_view.Visible = true;
                        lit_totalsolid_Competencies_view.Text = String.Format("{0:N2}", totsolid_sumcompetencies);
                    }


                    getViewSumpointCorvalue();
                    getViewSumpointCompetencies();
                    getViewScoreTimePerformance();

                    set_scoreBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                    getViewCoreValueScore(Convert.ToDouble(tot_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                    getViewCompetenciesScore(Convert.ToDouble(tot_sumcompetencies), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();

                    //dotted
                    if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                    {
                        set_scoreDottedBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                        getViewCoreValueScore(Convert.ToDouble(totdotted_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                        getViewCompetenciesScore(Convert.ToDouble(totdotted_sumcompetencies), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();
                    }
                    //solid
                    set_scoreSolidBefore = getViewKpiScore(Convert.ToDouble(tot_sumpoint_kpi), Convert.ToDouble(sumpoint_kpi)) + "," +
                    getViewCoreValueScore(Convert.ToDouble(totsolid_sumcore_value), Convert.ToDouble(sumpoint_corvalue)) + "," +
                    getViewCompetenciesScore(Convert.ToDouble(totsolid_sumcompetencies), Convert.ToDouble(sumpoint_competencies)) + "," + set_timeAttendanceScore.ToString();



                    //getViewFactor(GvViewFactor);

                }
                break;
            case "GvViewComment":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    if (_emp_idx == int.Parse(ViewState["cemp_idx_detail"].ToString()) && ViewState["unidx_detail"].ToString() == "1")
                    {
                        GvViewComment.Columns[2].Visible = false;

                    }
                    else
                    {

                        GvViewComment.Columns[2].Visible = false;

                    }

                }

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].Text = "ชื่อพฤติกรรม/ปัจจัย" + "<br />" + "(Behavior/Factor Name)";
                    e.Row.Cells[1].Text = "ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" + "<br />" + "Comment & Development Actions Plan (please identify action, timeline and person in charge)";
                }

                break;
            case "GvViewStar":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    if (_emp_idx == int.Parse(ViewState["cemp_idx_detail"].ToString()) && ViewState["unidx_detail"].ToString() == "1")
                    {
                        GvViewStar.Columns[2].Visible = false;
                    }
                    else
                    {
                        GvViewStar.Columns[2].Visible = false;
                    }


                }

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].Text = "ชื่อพฤติกรรม/ปัจจัย" + "<br />" + "(Behavior/Factor Name)";
                    e.Row.Cells[1].Text = "ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" + "<br />" + "Comment & Development Actions Plan (please identify action, timeline and person in charge)";
                }
                break;
            case "GvFactor":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    GridView GvSub_Factor = (GridView)e.Row.FindControl("GvSub_Factor");

                    if (e.Row.RowIndex == 3)
                    {
                        GvSub_Factor.Visible = true;
                        getTimePerformance(GvSub_Factor);
                    }
                    else
                    {
                        GvSub_Factor.Visible = false;

                    }

                }

                break;
            case "GvViewFactor":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    //set default bind
                    GvViewFactor.Columns[3].Visible = false;
                    GvViewFactor.Columns[4].Visible = false;
                    GvViewFactor.Columns[5].Visible = false;
                    GvViewFactor.Columns[6].Visible = false;
                    //set default bind

                    GridView GvViewSub_Factor = (GridView)e.Row.FindControl("GvViewSub_Factor");
                    Label lblscore_after_detail = (Label)e.Row.FindControl("lblscore_after_detail");
                    Label lbltotal_score_detail = (Label)e.Row.FindControl("lbltotal_score_detail");
                    Label lblcal_detail = (Label)e.Row.FindControl("lblcal_detail");

                    if (e.Row.RowIndex == 3)
                    {
                        GvViewSub_Factor.Visible = true;
                        getViewTimePerformance(GvViewSub_Factor);
                    }
                    else
                    {
                        GvViewSub_Factor.Visible = false;

                    }


                    if (int.Parse(ViewState["unidx_detail"].ToString()) > 2)
                    {
                        temps_total_score += Convert.ToDouble(lblscore_after_detail.Text);

                    }

                    //check dotted
                    if (ViewState["unidx_detail"].ToString() == "2" && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0) //dotted
                    {

                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {
                            GvViewFactor.Columns[3].Visible = true;


                        }

                    }
                    else if (int.Parse(ViewState["unidx_detail"].ToString()) > 2 && (int.Parse(ViewState["vs_CheckPermission_Detail_Dotted"].ToString()) > 0 || int.Parse(ViewState["vs_CheckPermission_Detail_Solid"].ToString()) > 0))
                    {

                        if (int.Parse(ViewState["vs_Dotted"].ToString()) > 0)
                        {
                            GvViewFactor.Columns[3].Visible = true;

                        }

                    }
                    else if (ViewState["staidx_detail"].ToString() == "2" && _permission == 1)
                    {
                        GvViewFactor.Columns[3].Visible = true;

                    }


                    //check solid
                    if (ViewState["unidx_detail"].ToString() == "3" && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0) //solid
                    {
                        GvViewFactor.Columns[3].Visible = true;
                        GvViewFactor.Columns[4].Visible = true;
                        GvViewFactor.Columns[5].Visible = true;


                    }
                    else if (int.Parse(ViewState["unidx_detail"].ToString()) > 3 && int.Parse(ViewState["vs_CheckPermission_Detail_Solid"].ToString()) > 0)
                    {
                        GvViewFactor.Columns[3].Visible = true;
                        GvViewFactor.Columns[4].Visible = true;
                        GvViewFactor.Columns[5].Visible = true;


                    }
                    else if (ViewState["staidx_detail"].ToString() == "2" && _permission == 1)
                    {
                        GvViewFactor.Columns[3].Visible = true;
                        GvViewFactor.Columns[4].Visible = true;
                        GvViewFactor.Columns[5].Visible = true;

                    }


                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    Label lit_total_score = (Label)e.Row.FindControl("lit_total_score");
                    lit_total_score.Text = String.Format("{0:N2}", temps_total_score); // Convert.ToString(tot_actual);


                }

                break;


        }
    }

    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvCoreValue":
            case "GvViewCoreValue":

                for (int rowIndex = GvName.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvName.Rows[rowIndex];
                    GridViewRow previousRow = GvName.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lblCorevalue")).Text == ((Label)previousRow.Cells[0].FindControl("lblCorevalue")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                }

                break;

            case "GvCompetencies":
            case "GvViewCompetencies":

                for (int rowIndex = GvName.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvName.Rows[rowIndex];
                    GridViewRow previousRow = GvName.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lblCompetencies")).Text == ((Label)previousRow.Cells[0].FindControl("lblCompetencies")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                }

                break;
            case "GvViewFactor":
                for (int rowIndex = GvName.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvName.Rows[rowIndex];
                    GridViewRow previousRow = GvName.Rows[rowIndex + 1];
                    Label lbltotal_score_detail = (Label)currentRow.FindControl("lbltotal_score_detail");
                    Label lbltotal_score_detail_ = (Label)previousRow.FindControl("lbltotal_score_detail");

                    //lbltotal_score_detail.Text = temps_total_score.ToString();

                    if (((Label)currentRow.Cells[6].FindControl("lbltotal_score_detail")).Text == ((Label)previousRow.Cells[6].FindControl("lbltotal_score_detail")).Text)
                    {

                        //itDebug.Text = currentRow.Cells[6].RowSpan.ToString() + "|" + previousRow.Cells[6].RowSpan.Tos

                        if (previousRow.Cells[6].RowSpan < 2)
                        {
                            currentRow.Cells[6].RowSpan = 2;
                            //lbltotal_score_detail.Text = temps_total_score.ToString();


                        }
                        else
                        {
                            currentRow.Cells[6].RowSpan = previousRow.Cells[6].RowSpan + 1;
                            //lbltotal_score_detail.Text = temps_total_score.ToString();

                        }
                        // lbltotal_score_detail.Text = temps_total_score.ToString();
                        // lbltotal_score_detail_.Visible = false;
                        previousRow.Cells[6].Visible = false;
                    }

                }

                break;
        }
    }

    #endregion Gridview

    #region ddlSelectedIndexChanged
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlOrganization = (DropDownList)fvSearch.FindControl("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList)fvSearch.FindControl("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList)fvSearch.FindControl("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList)fvSearch.FindControl("ddlDepartment");
        DropDownList ddlSection = (DropDownList)fvSearch.FindControl("ddlSection");
        DropDownList ddlPosition = (DropDownList)fvSearch.FindControl("ddlPosition");

        // DropDownList ddlOrganization_ = (DropDownList)fvSearchApprove.FindControl ("ddlOrganization");
        // DropDownList ddlWorkGroup_ = (DropDownList)fvSearchApprove.FindControl ("ddlWorkGroup");
        // DropDownList ddlLineWork_ = (DropDownList)fvSearchApprove.FindControl ("ddlLineWork");
        // DropDownList ddlDepartment_ = (DropDownList)fvSearchApprove.FindControl ("ddlDepartment");
        // DropDownList ddlSection_ = (DropDownList)fvSearchApprove.FindControl ("ddlSection");
        // DropDownList ddlPosition_ = (DropDownList)fvSearchApprove.FindControl ("ddlPosition");

        switch (ddlName.ID)
        {
            case "ddlOrganization":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _orgSelected = new search_cen_master_detail();
                    _orgSelected.s_org_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("4", _orgSelected);
                    _funcTool.setDdlData(ddlWorkGroup, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                }
                else
                {
                    ddlWorkGroup.Items.Clear();
                }

                ddlLineWork.Items.Clear();
                ddlDepartment.Items.Clear();
                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlWorkGroup.Items.Insert(0, new ListItem("--- เลือกกลุ่มงาน ---", "0"));
                ddlLineWork.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlWorkGroup":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _wgSelected = new search_cen_master_detail();
                    _wgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _wgSelected.s_wg_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("5", _wgSelected);
                    _funcTool.setDdlData(ddlLineWork, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                }
                else
                {
                    ddlLineWork.Items.Clear();
                }

                ddlDepartment.Items.Clear();
                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlLineWork.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlLineWork":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _lgSelected = new search_cen_master_detail();
                    _lgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _lgSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _lgSelected.s_lw_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("6", _lgSelected);
                    _funcTool.setDdlData(ddlDepartment, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                }
                else
                {
                    ddlDepartment.Items.Clear();
                }

                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlDepartment":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _deptSelected = new search_cen_master_detail();
                    _deptSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _deptSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _deptSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _deptSelected.s_dept_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("7", _deptSelected);
                    _funcTool.setDdlData(ddlSection, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                }
                else
                {
                    ddlSection.Items.Clear();
                }

                ddlPosition.Items.Clear();
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlSection":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _secSelected = new search_cen_master_detail();
                    _secSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _secSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _secSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _secSelected.s_dept_idx = ddlDepartment.SelectedValue;
                    _secSelected.s_sec_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("8", _secSelected);
                    _funcTool.setDdlData(ddlPosition, _data_cen_master.cen_pos_list_m0, "pos_name_th", "pos_idx");
                }
                else
                {
                    ddlPosition.Items.Clear();
                }

                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;


        }
    }

    protected void ddlSelectedIndexChangedApprove(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlOrganization = (DropDownList)fvSearchApprove.FindControl("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList)fvSearchApprove.FindControl("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList)fvSearchApprove.FindControl("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList)fvSearchApprove.FindControl("ddlDepartment");
        DropDownList ddlSection = (DropDownList)fvSearchApprove.FindControl("ddlSection");
        DropDownList ddlPosition = (DropDownList)fvSearchApprove.FindControl("ddlPosition");

        switch (ddlName.ID)
        {
            case "ddlOrganization":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _orgSelected = new search_cen_master_detail();
                    _orgSelected.s_org_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("4", _orgSelected);
                    _funcTool.setDdlData(ddlWorkGroup, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                }
                else
                {
                    ddlWorkGroup.Items.Clear();
                }

                ddlLineWork.Items.Clear();
                ddlDepartment.Items.Clear();
                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlWorkGroup.Items.Insert(0, new ListItem("--- เลือกกลุ่มงาน ---", "0"));
                ddlLineWork.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlWorkGroup":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _wgSelected = new search_cen_master_detail();
                    _wgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _wgSelected.s_wg_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("5", _wgSelected);
                    _funcTool.setDdlData(ddlLineWork, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                }
                else
                {
                    ddlLineWork.Items.Clear();
                }

                ddlDepartment.Items.Clear();
                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlLineWork.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlLineWork":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _lgSelected = new search_cen_master_detail();
                    _lgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _lgSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _lgSelected.s_lw_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("6", _lgSelected);
                    _funcTool.setDdlData(ddlDepartment, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                }
                else
                {
                    ddlDepartment.Items.Clear();
                }

                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlDepartment":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _deptSelected = new search_cen_master_detail();
                    _deptSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _deptSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _deptSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _deptSelected.s_dept_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("7", _deptSelected);
                    _funcTool.setDdlData(ddlSection, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                }
                else
                {
                    ddlSection.Items.Clear();
                }

                ddlPosition.Items.Clear();
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlSection":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _secSelected = new search_cen_master_detail();
                    _secSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _secSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _secSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _secSelected.s_dept_idx = ddlDepartment.SelectedValue;
                    _secSelected.s_sec_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("8", _secSelected);
                    _funcTool.setDdlData(ddlPosition, _data_cen_master.cen_pos_list_m0, "pos_name_th", "pos_idx");
                }
                else
                {
                    ddlPosition.Items.Clear();
                }

                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;


        }
    }

    protected void ddlSelectedIndexChangedApprove1(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlOrganization = (DropDownList)fvSearchApprove1.FindControl("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList)fvSearchApprove1.FindControl("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList)fvSearchApprove1.FindControl("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList)fvSearchApprove1.FindControl("ddlDepartment");
        DropDownList ddlSection = (DropDownList)fvSearchApprove1.FindControl("ddlSection");
        DropDownList ddlPosition = (DropDownList)fvSearchApprove1.FindControl("ddlPosition");

        switch (ddlName.ID)
        {
            case "ddlOrganization":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _orgSelected = new search_cen_master_detail();
                    _orgSelected.s_org_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("4", _orgSelected);
                    _funcTool.setDdlData(ddlWorkGroup, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                }
                else
                {
                    ddlWorkGroup.Items.Clear();
                }

                ddlLineWork.Items.Clear();
                ddlDepartment.Items.Clear();
                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlWorkGroup.Items.Insert(0, new ListItem("--- เลือกกลุ่มงาน ---", "0"));
                ddlLineWork.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlWorkGroup":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _wgSelected = new search_cen_master_detail();
                    _wgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _wgSelected.s_wg_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("5", _wgSelected);
                    _funcTool.setDdlData(ddlLineWork, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                }
                else
                {
                    ddlLineWork.Items.Clear();
                }

                ddlDepartment.Items.Clear();
                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlLineWork.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlLineWork":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _lgSelected = new search_cen_master_detail();
                    _lgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _lgSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _lgSelected.s_lw_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("6", _lgSelected);
                    _funcTool.setDdlData(ddlDepartment, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                }
                else
                {
                    ddlDepartment.Items.Clear();
                }

                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlDepartment":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _deptSelected = new search_cen_master_detail();
                    _deptSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _deptSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _deptSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _deptSelected.s_dept_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("7", _deptSelected);
                    _funcTool.setDdlData(ddlSection, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                }
                else
                {
                    ddlSection.Items.Clear();
                }

                ddlPosition.Items.Clear();
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlSection":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _secSelected = new search_cen_master_detail();
                    _secSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _secSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _secSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _secSelected.s_dept_idx = ddlDepartment.SelectedValue;
                    _secSelected.s_sec_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("8", _secSelected);
                    _funcTool.setDdlData(ddlPosition, _data_cen_master.cen_pos_list_m0, "pos_name_th", "pos_idx");
                }
                else
                {
                    ddlPosition.Items.Clear();
                }

                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;


        }
    }

    protected void ddlSelectedIndexChangedApprove2(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlOrganization = (DropDownList)fvSearchApprove2.FindControl("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList)fvSearchApprove2.FindControl("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList)fvSearchApprove2.FindControl("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList)fvSearchApprove2.FindControl("ddlDepartment");
        DropDownList ddlSection = (DropDownList)fvSearchApprove2.FindControl("ddlSection");
        DropDownList ddlPosition = (DropDownList)fvSearchApprove2.FindControl("ddlPosition");

        switch (ddlName.ID)
        {
            case "ddlOrganization":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _orgSelected = new search_cen_master_detail();
                    _orgSelected.s_org_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("4", _orgSelected);
                    _funcTool.setDdlData(ddlWorkGroup, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                }
                else
                {
                    ddlWorkGroup.Items.Clear();
                }

                ddlLineWork.Items.Clear();
                ddlDepartment.Items.Clear();
                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlWorkGroup.Items.Insert(0, new ListItem("--- เลือกกลุ่มงาน ---", "0"));
                ddlLineWork.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlWorkGroup":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _wgSelected = new search_cen_master_detail();
                    _wgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _wgSelected.s_wg_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("5", _wgSelected);
                    _funcTool.setDdlData(ddlLineWork, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                }
                else
                {
                    ddlLineWork.Items.Clear();
                }

                ddlDepartment.Items.Clear();
                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlLineWork.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlLineWork":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _lgSelected = new search_cen_master_detail();
                    _lgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _lgSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _lgSelected.s_lw_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("6", _lgSelected);
                    _funcTool.setDdlData(ddlDepartment, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                }
                else
                {
                    ddlDepartment.Items.Clear();
                }

                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlDepartment":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _deptSelected = new search_cen_master_detail();
                    _deptSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _deptSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _deptSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _deptSelected.s_dept_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("7", _deptSelected);
                    _funcTool.setDdlData(ddlSection, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                }
                else
                {
                    ddlSection.Items.Clear();
                }

                ddlPosition.Items.Clear();
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlSection":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _secSelected = new search_cen_master_detail();
                    _secSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _secSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _secSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _secSelected.s_dept_idx = ddlDepartment.SelectedValue;
                    _secSelected.s_sec_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("8", _secSelected);
                    _funcTool.setDdlData(ddlPosition, _data_cen_master.cen_pos_list_m0, "pos_name_th", "pos_idx");
                }
                else
                {
                    ddlPosition.Items.Clear();
                }

                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;


        }
    }

    protected void ddlSelectedIndexChangedApprove3(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlOrganization = (DropDownList)fvSearchApprove3.FindControl("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList)fvSearchApprove3.FindControl("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList)fvSearchApprove3.FindControl("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList)fvSearchApprove3.FindControl("ddlDepartment");
        DropDownList ddlSection = (DropDownList)fvSearchApprove3.FindControl("ddlSection");
        DropDownList ddlPosition = (DropDownList)fvSearchApprove3.FindControl("ddlPosition");

        switch (ddlName.ID)
        {
            case "ddlOrganization":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _orgSelected = new search_cen_master_detail();
                    _orgSelected.s_org_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("4", _orgSelected);
                    _funcTool.setDdlData(ddlWorkGroup, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                }
                else
                {
                    ddlWorkGroup.Items.Clear();
                }

                ddlLineWork.Items.Clear();
                ddlDepartment.Items.Clear();
                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlWorkGroup.Items.Insert(0, new ListItem("--- เลือกกลุ่มงาน ---", "0"));
                ddlLineWork.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlWorkGroup":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _wgSelected = new search_cen_master_detail();
                    _wgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _wgSelected.s_wg_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("5", _wgSelected);
                    _funcTool.setDdlData(ddlLineWork, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                }
                else
                {
                    ddlLineWork.Items.Clear();
                }

                ddlDepartment.Items.Clear();
                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlLineWork.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlLineWork":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _lgSelected = new search_cen_master_detail();
                    _lgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _lgSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _lgSelected.s_lw_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("6", _lgSelected);
                    _funcTool.setDdlData(ddlDepartment, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                }
                else
                {
                    ddlDepartment.Items.Clear();
                }

                ddlSection.Items.Clear();
                ddlPosition.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlDepartment":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _deptSelected = new search_cen_master_detail();
                    _deptSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _deptSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _deptSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _deptSelected.s_dept_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("7", _deptSelected);
                    _funcTool.setDdlData(ddlSection, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                }
                else
                {
                    ddlSection.Items.Clear();
                }

                ddlPosition.Items.Clear();
                ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;
            case "ddlSection":
                if (_funcTool.convertToInt(ddlName.SelectedValue) > 0)
                {
                    search_cen_master_detail _secSelected = new search_cen_master_detail();
                    _secSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _secSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _secSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _secSelected.s_dept_idx = ddlDepartment.SelectedValue;
                    _secSelected.s_sec_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList("8", _secSelected);
                    _funcTool.setDdlData(ddlPosition, _data_cen_master.cen_pos_list_m0, "pos_name_th", "pos_idx");
                }
                else
                {
                    ddlPosition.Items.Clear();
                }

                ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                break;


        }
    }

    #endregion

    #region onSelectedCheckChanged
    protected void onSelectedCheckChanged(object sender, EventArgs e)
    {
        var cbName = (CheckBox)sender;

        //var chkName = (CheckBoxList)sender;

        switch (cbName.ID)
        {

            case "chkallApprove1":
                int chack_all = 0;

                if (chkallApprove1.Checked)
                {

                    foreach (GridViewRow row in GvApprove1.Rows)
                    {
                        CheckBox chk_approve1 = (CheckBox)row.FindControl("chk_approve1");

                        chk_approve1.Checked = true;
                        chack_all++;
                    }
                }
                else
                {
                    foreach (GridViewRow row in GvApprove1.Rows)
                    {
                        CheckBox chk_approve1 = (CheckBox)row.FindControl("chk_approve1");

                        chk_approve1.Checked = false;
                        chack_all++;
                    }
                }

                break;
            case "chkallApprove2":
                int chack_all2 = 0;

                if (chkallApprove2.Checked)
                {

                    foreach (GridViewRow row2 in GvApprove2.Rows)
                    {
                        CheckBox chk_approve2 = (CheckBox)row2.FindControl("chk_approve2");

                        chk_approve2.Checked = true;
                        chack_all2++;
                    }
                }
                else
                {
                    foreach (GridViewRow row2 in GvApprove2.Rows)
                    {
                        CheckBox chk_approve2 = (CheckBox)row2.FindControl("chk_approve2");

                        chk_approve2.Checked = false;
                        chack_all2++;
                    }
                }

                break;
            case "chkallApprove3":
                int chack_all3 = 0;

                if (chkallApprove3.Checked)
                {

                    foreach (GridViewRow row3 in GvApprove3.Rows)
                    {
                        CheckBox chk_approve3 = (CheckBox)row3.FindControl("chk_approve3");

                        chk_approve3.Checked = true;
                        chack_all3++;
                    }
                }
                else
                {
                    foreach (GridViewRow row3 in GvApprove3.Rows)
                    {
                        CheckBox chk_approve3 = (CheckBox)row3.FindControl("chk_approve3");

                        chk_approve3.Checked = false;
                        chack_all3++;
                    }
                }

                break;

        }
    }




    #endregion

    #region repeater
    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {

            case "rptBindbtnNode1":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //Repeater rptBindbtnNode1 = (Repeater)FvHeadUserApprove.FindControl("rptBindbtnNode1");
                    var lbcheck_colerNode1 = (Label)e.Item.FindControl("lbcheck_colerNode1");

                    var btn_Node1 = (LinkButton)e.Item.FindControl("btn_Node1");

                    for (int k = 0; k <= rptBindbtnNode1.Items.Count; k++)
                    {

                        btn_Node1.CssClass = ConfigureColors(k);
                        btn_Node1.Text = getStatus(k) + " " + lbcheck_colerNode1.Text;
                        //Console.WriteLine(i);
                    }


                }
                break;
            case "rptViewBindbtnNode1":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //Repeater rptBindbtnNode1 = (Repeater)FvHeadUserApprove.FindControl("rptBindbtnNode1");
                    var lbcheck_colerNode1_view = (Label)e.Item.FindControl("lbcheck_colerNode1_view");

                    var btn_ViewNode1 = (LinkButton)e.Item.FindControl("btn_ViewNode1");

                    for (int k = 0; k <= rptViewBindbtnNode1.Items.Count; k++)
                    {

                        if (int.Parse(ViewState["unidx_detail"].ToString()) > 3)
                        {
                            btn_ViewNode1.CssClass = ConfigureColorsNode(k);

                        }
                        else
                        {
                            btn_ViewNode1.CssClass = ConfigureColors(k);

                        }



                        btn_ViewNode1.Text = getStatus(k) + " " + lbcheck_colerNode1_view.Text;
                        //Console.WriteLine(i);
                    }


                }
                break;
            case "rptViewBindbtnNode4":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //Repeater rptBindbtnNode1 = (Repeater)FvHeadUserApprove.FindControl("rptBindbtnNode1");
                    var lbcheck_colerNode4_view = (Label)e.Item.FindControl("lbcheck_colerNode4_view");

                    var btn_ViewNode4 = (LinkButton)e.Item.FindControl("btn_ViewNode4");

                    for (int k = 0; k <= rptViewBindbtnNode4.Items.Count; k++)
                    {

                        btn_ViewNode4.CssClass = ConfigureColorsNode(k);

                        btn_ViewNode4.Text = getStatus(k) + " " + lbcheck_colerNode4_view.Text;
                        //Console.WriteLine(i);
                    }


                }
                break;
            case "rptViewBindbtnNode5":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //Repeater rptBindbtnNode1 = (Repeater)FvHeadUserApprove.FindControl("rptBindbtnNode1");
                    var lbcheck_colerNode5_view = (Label)e.Item.FindControl("lbcheck_colerNode5_view");

                    var btn_ViewNode5 = (LinkButton)e.Item.FindControl("btn_ViewNode5");

                    for (int k = 0; k <= rptViewBindbtnNode5.Items.Count; k++)
                    {

                        btn_ViewNode5.CssClass = ConfigureColorsNode(k);

                        btn_ViewNode5.Text = getStatus(k) + " " + lbcheck_colerNode5_view.Text;
                        //Console.WriteLine(i);
                    }


                }
                break;
            case "rptViewBindbtnNode6":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //Repeater rptBindbtnNode1 = (Repeater)FvHeadUserApprove.FindControl("rptBindbtnNode1");
                    var lbcheck_colerNode6_view = (Label)e.Item.FindControl("lbcheck_colerNode6_view");

                    var btn_ViewNode6 = (LinkButton)e.Item.FindControl("btn_ViewNode6");

                    for (int k = 0; k <= rptViewBindbtnNode6.Items.Count; k++)
                    {

                        btn_ViewNode6.CssClass = ConfigureColorsNode(k);

                        btn_ViewNode6.Text = getStatus(k) + " " + lbcheck_colerNode6_view.Text;
                        //Console.WriteLine(i);
                    }


                }
                break;


        }
    }
    #endregion repeater

    #region ConfigureColors
    protected string ConfigureColors(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }

    protected string ConfigureColorsNode(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }
    #endregion ConfigureColors

    protected string getStatus(int _btnidx)
    {
        if (_btnidx == 0)
        {

            return "<i class='fa fa-save'></i>";
        }
        else
        {
            return "<i class='glyphicon glyphicon-envelope'></i>";
        }
    }

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {
        clearSession();
        clearViewState();

        //setActiveTab("docDetail", 0, 0, 0);

    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected string getConvertString(string _varchar)
    {
        if (_varchar != "")
        {

            //litDebug.Text = String.Format("{0:N2}", Convert.ToDecimal(_varchar)).ToString();
            return String.Format("{0:N2}", Convert.ToDecimal(_varchar));

        }
        else
        {
            return "0.00";
        }
    }

    protected string Truncate(string value, int maxLength)
    {

        // int maxLength = 10;


        if (string.IsNullOrEmpty(value)) return value;


        return value.Length <= maxLength ? value : value.Substring(0, maxLength) + "...";
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        chkName.Items.Clear();
        // bind items
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected void setRdoData(RadioButtonList rdoName, Object obj, string _data_text, string _data_value)
    {
        rdoName.Items.Clear();
        rdoName.DataSource = obj;
        rdoName.DataTextField = _data_text;
        rdoName.DataValueField = _data_value;
        rdoName.DataBind();

    }



    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_cen_master getMasterList(string _masterMode, search_cen_master_detail _searchData)
    {
        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.master_mode = _masterMode;
        _data_cen_master.search_cen_master_list[0] = _searchData;
        //litDebug.Text = _funcTool.convertObjectToJson(_data_cen_master);

        _data_cen_master = callServicePostCenMaster(_urlCenGetCenMasterList, _data_cen_master);
        return _data_cen_master;
    }

    protected data_cen_master callServicePostCenMaster(string _cmdUrl, data_cen_master _data_cen_master)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_cen_master);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_cen_master = (data_cen_master)_funcTool.convertJsonToObject(typeof(data_cen_master), _localJson);

        return _data_cen_master;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_cen_employee callServicePostCenEmployee(string _cmdUrl, data_cen_employee _data_cen_employee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_cen_employee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_cen_employee = (data_cen_employee)_funcTool.convertJsonToObject(typeof(data_cen_employee), _localJson);

        return _data_cen_employee;
    }

    protected data_pms callServicePostPMS(string _cmdUrl, data_pms _data_pms)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_data_pms);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _data_pms = (data_pms)_funcTool.convertJsonToObject(typeof(data_pms), _localJson);


        return _data_pms;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void getCenEmployeeProfile(int _emp_idx)
    {

        data_cen_employee _data_cen_employee = new data_cen_employee();
        search_cen_employee_detail search_detail_list = new search_cen_employee_detail();
        _data_cen_employee.search_cen_employee_list = new search_cen_employee_detail[1];

        _data_cen_employee.employee_mode = "1";
        search_detail_list.s_emp_idx = _emp_idx.ToString();


        _data_cen_employee.search_cen_employee_list[0] = search_detail_list;

        _data_cen_employee = callServicePostCenEmployee(_urlGetCenEmployeeList, _data_cen_employee);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cen_employee));
        ViewState["vsCenEmpProfile"] = _data_cen_employee;

        _data_cen_employee = (data_cen_employee)ViewState["vsCenEmpProfile"];
        //check permission
        // // foreach (int item in rdept_qmr)
        // // {
        // //     if (_dataEmployee.employee_list[0].rdept_idx == item)
        // //     {
        // //         _flag_qmr = true;
        // //         break;
        // //     }
        // // }
        //check permission
        ViewState["org_permission"] = _data_cen_employee.cen_employee_list_u0[0].org_idx;
        ViewState["rsec_permission"] = _data_cen_employee.cen_employee_list_u0[0].sec_idx;
        ViewState["rdept_permission"] = _data_cen_employee.cen_employee_list_u0[0].dept_idx;
        ViewState["rpos_permission"] = _data_cen_employee.cen_employee_list_u0[0].pos_idx;
        //ViewState["joblevel_permission"] = _data_cen_employee.cen_employee_list_u0[0].jobgrade_level;
        ViewState["emp_start_date"] = _data_cen_employee.cen_employee_list_u0[0].emp_start_date;
        ViewState["emp_email"] = _data_cen_employee.cen_employee_list_u0[0].emp_org_mail;
        ViewState["empgroup_idx"] = _data_cen_employee.cen_employee_list_u0[0].empgroup_idx;

    }

    protected void setActiveView(string activeTab, int uidx, int u1idx, int staidx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        //set tab docCreate
        setGridData(GvKPIs, null);
        setGridData(GvMasterPoint, null);
        setGridData(GvCoreValue, null);
        setGridData(GvCompetencies, null);

        GvFactor.Visible = false;
        setGridData(GvFactor, null);


        setFormData(FvComment, FormViewMode.ReadOnly, null);
        setFormData(FvStar, FormViewMode.ReadOnly, null);

        fvSearch.Visible = false;

        //set tab detail
        GvDetail.Visible = false;
        setGridData(GvDetail, null);

        setFormData(fvViewEmpDetail, FormViewMode.ReadOnly, null);
        _DivFormName.Visible = false;

        _DivNameKPI.Visible = false;

        GvViewKPIs.Visible = false;
        setGridData(GvViewKPIs, null);

        _DivPoint.Visible = false;

        GvViewMasterPoint.Visible = false;
        setGridData(GvViewMasterPoint, null);

        GvViewCoreValue.Visible = false;
        setGridData(GvViewCoreValue, null);

        GvViewCompetencies.Visible = false;
        setGridData(GvViewCompetencies, null);

        _Divworkdetail.Visible = false;
        _Divdev.Visible = false;
        _DivComment.Visible = false;

        GvViewComment.Visible = false;
        setGridData(GvViewComment, null);

        GvViewStar.Visible = false;
        setGridData(GvViewStar, null);

        _DivStar.Visible = false;
        _DivStarDetail.Visible = false;

        div_ViewNode1.Visible = false;
        rptViewBindbtnNode1.Visible = false;
        btnHRCheckPermission.Visible = false;


        div_LogViewDetail.Visible = false;

        setFormData(FvViewComment, FormViewMode.ReadOnly, null);
        setFormData(FvViewStar, FormViewMode.ReadOnly, null);



        GvViewFactor.Visible = false;
        setGridData(GvViewFactor, null);


        //setFormData(FvViewStarUser, FormViewMode.ReadOnly, null);

        //set tab approve
        fvSearchApprove.Visible = false;

        GvApprove.Visible = false;
        setGridData(GvApprove, null);

        //set tab check performance
        GvCheckPerformance.Visible = false;
        setGridData(GvCheckPerformance, null);

        fvSearchCheckPerformance.Visible = false;

        //set tab approve1
        GvApprove1.Visible = false;
        setGridData(GvApprove1, null);

        rptViewBindbtnNode4.Visible = false;

        GvApprove2.Visible = false;
        setGridData(GvApprove2, null);

        rptViewBindbtnNode5.Visible = false;

        GvApprove3.Visible = false;
        setGridData(GvApprove3, null);

        rptViewBindbtnNode6.Visible = false;

        //set tab approve 1
        fvSearchApprove1.Visible = false;

        //set tab approve 2
        fvSearchApprove2.Visible = false;

        //set tab approve 3
        fvSearchApprove3.Visible = false;


        switch (activeTab)
        {

            case "docDetail":

                if (uidx == 0) //index detail
                {

                    getCheckPermissionDetailIndex();

                    GvDetail.Visible = true;
                    getDetail(GvDetail);
                    getScoreTimePerformance();

                    // get organization
                    DropDownList ddlOrganization = (DropDownList)fvSearch.FindControl("ddlOrganization");
                    DropDownList ddlWorkGroup = (DropDownList)fvSearch.FindControl("ddlWorkGroup");
                    DropDownList ddlLineWork = (DropDownList)fvSearch.FindControl("ddlLineWork");
                    DropDownList ddlDepartment = (DropDownList)fvSearch.FindControl("ddlDepartment");
                    DropDownList ddlSection = (DropDownList)fvSearch.FindControl("ddlSection");
                    DropDownList ddlPosition = (DropDownList)fvSearch.FindControl("ddlPosition");
                    TextBox txt_empcode = (TextBox)fvSearch.FindControl("txt_empcode");

                    fvSearch.Visible = true;

                    _data_cen_master = getMasterList("3", null);
                    ddlOrganization.Items.Clear();
                    ddlWorkGroup.Items.Clear();
                    ddlLineWork.Items.Clear();
                    ddlDepartment.Items.Clear();
                    ddlSection.Items.Clear();
                    ddlPosition.Items.Clear();
                    txt_empcode.Text = String.Empty;

                    _funcTool.setDdlData(ddlOrganization, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                    ddlOrganization.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
                    ddlWorkGroup.Items.Insert(0, new ListItem("--- เลือกกลุ่มงาน ---", "0"));
                    ddlLineWork.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                    ddlDepartment.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                    ddlSection.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                    ddlPosition.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));
                }
                else //view detail
                {
                    //litDebug.Text = uidx.ToString();
                    _DivFormName.Visible = true;
                    //setFormData(fvViewEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                    getCenEmployeeProfile(int.Parse(ViewState["cemp_idx_detail"].ToString()));
                    setFormData(fvViewEmpDetail, FormViewMode.ReadOnly, ((data_cen_employee)ViewState["vsCenEmpProfile"]).cen_employee_list_u0);
                    getViewScoreTimePerformance();



                    _DivNameKPI.Visible = true;
                    //getIndividual(GvKPIs);
                    GvViewKPIs.Visible = true;
                    getViewIndividual(GvViewKPIs);


                    _DivPoint.Visible = true;
                    GvViewMasterPoint.Visible = true;
                    getPoint(GvViewMasterPoint);

                    GvViewCoreValue.Visible = true;
                    getViewCoreValue(GvViewCoreValue, int.Parse(ViewState["u0_docidx_detail"].ToString()));


                    GvViewCompetencies.Visible = true;
                    ////ViewState["TIDX_create"] = 2; //change to employee droup
                    getViewcompetencise(GvViewCompetencies, int.Parse(ViewState["u0_docidx_detail"].ToString()), int.Parse(ViewState["empgroup_idx"].ToString()), int.Parse(ViewState["u0_typeidx_detail"].ToString()));


                    mergeCell(GvViewCoreValue);
                    mergeCell(GvViewCompetencies);

                    _Divworkdetail.Visible = true;
                    _Divdev.Visible = true;
                    _DivComment.Visible = true;


                    lblFormNameView.Text = ViewState["form_name_detail"].ToString();

                    _DivStar.Visible = true;
                    _DivStarDetail.Visible = true;




                    div_LogViewDetail.Visible = true;
                    getLog(rptLogViewDetail, int.Parse(ViewState["u0_docidx_detail"].ToString()));

                    switch (int.Parse(ViewState["unidx_detail"].ToString()))
                    {
                        case 1:

                            GvViewFactor.Visible = true;
                            getViewFactor(GvViewFactor);

                            mergeCell(GvViewFactor);

                            getFormviewComment(FvViewComment, int.Parse(ViewState["u0_docidx_detail"].ToString()), int.Parse(ViewState["unidx_detail"].ToString()));
                            getFormviewStar(FvViewStar, int.Parse(ViewState["u0_docidx_detail"].ToString()), int.Parse(ViewState["unidx_detail"].ToString()));

                            GvViewComment.Visible = true;
                            getViewDevStarDetail(GvViewComment, int.Parse(ViewState["u0_docidx_detail"].ToString()), 3);

                            GvViewStar.Visible = true;
                            getViewDevStarDetail(GvViewStar, int.Parse(ViewState["u0_docidx_detail"].ToString()), 4);



                            //
                            div_ViewNode1.Visible = true;
                            if (int.Parse(ViewState["cemp_idx_detail"].ToString()) == _emp_idx)
                            {

                                rptViewBindbtnNode1.Visible = true;
                                getbtnDecision(rptViewBindbtnNode1, int.Parse(ViewState["unidx_detail"].ToString()), 0);

                            }

                            break;
                        case 2:
                        case 3:


                            GvViewFactor.Visible = true;
                            getViewFactor(GvViewFactor);

                            mergeCell(GvViewFactor);

                            getFormviewComment(FvViewComment, int.Parse(ViewState["u0_docidx_detail"].ToString()), int.Parse(ViewState["unidx_detail"].ToString()));
                            getFormviewStar(FvViewStar, int.Parse(ViewState["u0_docidx_detail"].ToString()), int.Parse(ViewState["unidx_detail"].ToString()));

                            // setFormData(FvViewComment, FormViewMode.Insert, null);
                            // setFormData(FvViewStar, FormViewMode.Insert, null);


                            GvViewComment.Visible = true;
                            getViewDevStarDetail(GvViewComment, int.Parse(ViewState["u0_docidx_detail"].ToString()), 3);

                            GvViewStar.Visible = true;
                            getViewDevStarDetail(GvViewStar, int.Parse(ViewState["u0_docidx_detail"].ToString()), 4);


                            div_ViewNode1.Visible = true;
                            if (int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0)
                            {

                                rptViewBindbtnNode1.Visible = true;
                                getbtnDecision(rptViewBindbtnNode1, int.Parse(ViewState["unidx_detail"].ToString()), 0);

                            }



                            break;
                        case 4:
                        case 5:
                        case 6:



                            GvViewFactor.Visible = true;
                            getViewFactor(GvViewFactor);

                            mergeCell(GvViewFactor);

                            GvViewComment.Visible = true;
                            getViewDevStarDetail(GvViewComment, int.Parse(ViewState["u0_docidx_detail"].ToString()), 3);

                            GvViewStar.Visible = true;
                            getViewDevStarDetail(GvViewStar, int.Parse(ViewState["u0_docidx_detail"].ToString()), 4);


                            div_ViewNode1.Visible = true;
                            if (ViewState["staidx_detail"].ToString() != "2" && int.Parse(ViewState["vs_CheckPermission_Detail"].ToString()) > 0) // check status in status success
                            {

                                rptViewBindbtnNode1.Visible = true;

                                getbtnDecision(rptViewBindbtnNode1, int.Parse(ViewState["unidx_detail"].ToString()), 0);

                            }
                            else if (ViewState["staidx_detail"].ToString() == "2" && _permission == 1)
                            {
                                btnHRCheckPermission.Visible = true;
                            }


                            break;


                    }

                }


                setOntop.Focus();
                break;


            case "docCreate":

                //setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                getCenEmployeeProfile(_emp_idx);
                getScoreTimePerformance();
                getCompentency(GvCompetencies, int.Parse(ViewState["empgroup_idx"].ToString()), int.Parse(ViewState["org_permission"].ToString()));


                break;

            case "docApprove":



                //getViewScoreTimePerformance();
                GvApprove.Visible = true;
                getDetailWaitApprove(GvApprove);

                DropDownList ddlOrganization_ = (DropDownList)fvSearchApprove.FindControl("ddlOrganization");
                DropDownList ddlWorkGroup_ = (DropDownList)fvSearchApprove.FindControl("ddlWorkGroup");
                DropDownList ddlLineWork_ = (DropDownList)fvSearchApprove.FindControl("ddlLineWork");
                DropDownList ddlDepartment_ = (DropDownList)fvSearchApprove.FindControl("ddlDepartment");
                DropDownList ddlSection_ = (DropDownList)fvSearchApprove.FindControl("ddlSection");
                DropDownList ddlPosition_ = (DropDownList)fvSearchApprove.FindControl("ddlPosition");
                TextBox txt_empcode_ = (TextBox)fvSearchApprove.FindControl("txt_empcode");

                fvSearchApprove.Visible = true;

                _data_cen_master = getMasterList("3", null);


                txt_empcode_.Text = String.Empty;
                ddlOrganization_.Items.Clear();
                ddlWorkGroup_.Items.Clear();
                ddlLineWork_.Items.Clear();
                ddlDepartment_.Items.Clear();
                ddlSection_.Items.Clear();
                ddlPosition_.Items.Clear();

                _funcTool.setDdlData(ddlOrganization_, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                ddlOrganization_.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
                ddlWorkGroup_.Items.Insert(0, new ListItem("--- เลือกกลุ่มงาน ---", "0"));
                ddlLineWork_.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment_.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection_.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition_.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));

                setOntop.Focus();



                break;
            case "docCheckPerformance":

                TextBox txt_empcode_check = (TextBox)fvSearchCheckPerformance.FindControl("txt_empcode");

                GvCheckPerformance.Visible = true;
                getDetailCheckPerformance(GvCheckPerformance);

                fvSearchCheckPerformance.Visible = true;
                txt_empcode_check.Text = String.Empty;

                setOntop.Focus();



                break;
            case "docApprove1":

                DropDownList ddlOrganization_1 = (DropDownList)fvSearchApprove1.FindControl("ddlOrganization");
                DropDownList ddlWorkGroup_1 = (DropDownList)fvSearchApprove1.FindControl("ddlWorkGroup");
                DropDownList ddlLineWork_1 = (DropDownList)fvSearchApprove1.FindControl("ddlLineWork");
                DropDownList ddlDepartment_1 = (DropDownList)fvSearchApprove1.FindControl("ddlDepartment");
                DropDownList ddlSection_1 = (DropDownList)fvSearchApprove1.FindControl("ddlSection");
                DropDownList ddlPosition_1 = (DropDownList)fvSearchApprove1.FindControl("ddlPosition");
                TextBox txt_empcode_1 = (TextBox)fvSearchApprove1.FindControl("txt_empcode");

                fvSearchApprove1.Visible = true;

                _data_cen_master = getMasterList("3", null);


                txt_empcode_1.Text = String.Empty;
                ddlOrganization_1.Items.Clear();
                ddlWorkGroup_1.Items.Clear();
                ddlLineWork_1.Items.Clear();
                ddlDepartment_1.Items.Clear();
                ddlSection_1.Items.Clear();
                ddlPosition_1.Items.Clear();

                _funcTool.setDdlData(ddlOrganization_1, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                ddlOrganization_1.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
                ddlWorkGroup_1.Items.Insert(0, new ListItem("--- เลือกกลุ่มงาน ---", "0"));
                ddlLineWork_1.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment_1.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection_1.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition_1.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));


                GvApprove1.Visible = true;
                getWaitApproveChiefs(GvApprove1);
                setOntop.Focus();

                break;
            case "docApprove2":

                DropDownList ddlOrganization_2 = (DropDownList)fvSearchApprove2.FindControl("ddlOrganization");
                DropDownList ddlWorkGroup_2 = (DropDownList)fvSearchApprove2.FindControl("ddlWorkGroup");
                DropDownList ddlLineWork_2 = (DropDownList)fvSearchApprove2.FindControl("ddlLineWork");
                DropDownList ddlDepartment_2 = (DropDownList)fvSearchApprove2.FindControl("ddlDepartment");
                DropDownList ddlSection_2 = (DropDownList)fvSearchApprove2.FindControl("ddlSection");
                DropDownList ddlPosition_2 = (DropDownList)fvSearchApprove2.FindControl("ddlPosition");
                TextBox txt_empcode_2 = (TextBox)fvSearchApprove2.FindControl("txt_empcode");

                fvSearchApprove2.Visible = true;

                _data_cen_master = getMasterList("3", null);

                txt_empcode_2.Text = String.Empty;
                ddlOrganization_2.Items.Clear();
                ddlWorkGroup_2.Items.Clear();
                ddlLineWork_2.Items.Clear();
                ddlDepartment_2.Items.Clear();
                ddlSection_2.Items.Clear();
                ddlPosition_2.Items.Clear();

                _funcTool.setDdlData(ddlOrganization_2, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                ddlOrganization_2.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
                ddlWorkGroup_2.Items.Insert(0, new ListItem("--- เลือกกลุ่มงาน ---", "0"));
                ddlLineWork_2.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment_2.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection_2.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition_2.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));

                GvApprove2.Visible = true;
                getWaitApprove2Chiefs(GvApprove2);
                setOntop.Focus();

                break;
            case "docApprove3":

                DropDownList ddlOrganization_3 = (DropDownList)fvSearchApprove3.FindControl("ddlOrganization");
                DropDownList ddlWorkGroup_3 = (DropDownList)fvSearchApprove3.FindControl("ddlWorkGroup");
                DropDownList ddlLineWork_3 = (DropDownList)fvSearchApprove3.FindControl("ddlLineWork");
                DropDownList ddlDepartment_3 = (DropDownList)fvSearchApprove3.FindControl("ddlDepartment");
                DropDownList ddlSection_3 = (DropDownList)fvSearchApprove3.FindControl("ddlSection");
                DropDownList ddlPosition_3 = (DropDownList)fvSearchApprove3.FindControl("ddlPosition");
                TextBox txt_empcode_3 = (TextBox)fvSearchApprove3.FindControl("txt_empcode");

                fvSearchApprove3.Visible = true;

                _data_cen_master = getMasterList("3", null);

                txt_empcode_3.Text = String.Empty;
                ddlOrganization_3.Items.Clear();
                ddlWorkGroup_3.Items.Clear();
                ddlLineWork_3.Items.Clear();
                ddlDepartment_3.Items.Clear();
                ddlSection_3.Items.Clear();
                ddlPosition_3.Items.Clear();

                _funcTool.setDdlData(ddlOrganization_3, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                ddlOrganization_3.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
                ddlWorkGroup_3.Items.Insert(0, new ListItem("--- เลือกกลุ่มงาน ---", "0"));
                ddlLineWork_3.Items.Insert(0, new ListItem("--- เลือกสายงาน ---", "0"));
                ddlDepartment_3.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
                ddlSection_3.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
                ddlPosition_3.Items.Insert(0, new ListItem("--- เลือกตำแหน่ง ---", "0"));



                GvApprove3.Visible = true;
                getWaitApprove3Chiefs(GvApprove3);
                setOntop.Focus();

                break;

        }

    }

    protected void setActiveTab(string activeTab, int uidx, int u1idx, int staidx)
    {
        setActiveView(activeTab, uidx, u1idx, staidx);
        switch (activeTab)
        {
            case "docDetail":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");

                break;
            case "docCreate":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");

                break;
            case "docApprove":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");


                break;
            case "docCheckPerformance":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");

                break;
            case "docApprove1":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "active");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "");

                break;
            case "docApprove2":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "active");
                li6.Attributes.Add("class", "");

                break;
            case "docApprove3":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                li6.Attributes.Add("class", "active");

                break;

        }
    }

    #endregion reuse

}