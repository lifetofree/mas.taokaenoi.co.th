﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_sf_certificate
/// </summary>
[Serializable]
[XmlRoot("data_certificate_doc")]

public class data_certificate_doc
{
    [XmlElement("ReturnCode")]
    public int ReturnCode { get; set; }
    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }

    [XmlElement("certificate_doc_list")]
    public certificate_doc_detail[] certificate_doc_list { get; set;}

    [XmlElement("certificate_doc_list_u1")]
    public certificate_doc_detail_u1[] certificate_doc_list_u1 { get; set; }

    [XmlElement("certificate_doc_list_u2")]
    public certificate_doc_detail_u2[] certificate_doc_list_u2 { get; set; }

    [XmlElement("certificate_doc_list_notification_m1")]
    public certificate_doc_detail_notification_m1[] certificate_doc_list_notification_m1 { get; set; }


    [XmlElement("certificate_doc_list_frequency_m0")]
    public certificate_doc_detail_frequency_m0[] certificate_doc_list_frequency_m0 { get; set; }

    [XmlElement("certificate_doc_list_notification_m0")]
    public certificate_doc_detail_notification_m0[] certificate_doc_list_notification_m0 { get; set; }

    [XmlElement("certificate_doc_list_permission")]
    public certificate_doc_detail_permission[] certificate_doc_list_permission { get; set; }

}


#region u0
[Serializable]
public class certificate_doc_detail
{
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("uemp_idx")]
    public int uemp_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("premission_idx")]
    public int premission_idx { get; set; }

    [XmlElement("type_status")]
    public int type_status { get; set; }

    [XmlElement("status_type_Name")]
    public string status_type_Name { get; set; }

    [XmlElement("m0_supidx")]
    public int m0_supidx { get; set; }

    [XmlElement("sup_name")]
    public string sup_name { get; set; }

    [XmlElement("email")]
    public string email { get; set; }

    [XmlElement("type_system")]
    public int type_system { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }

    [XmlElement("LocStatus")]
    public int LocStatus { get; set; }

    [XmlElement("permission_idx")]
    public int permission_idx { get; set; }

    [XmlElement("permission_name")]
    public string permission_name { get; set; }

    [XmlElement("frequency_idx")]
    public int frequency_idx { get; set; }

    [XmlElement("frequency_name")]
    public string frequency_name { get; set; }

    [XmlElement("frequency_status")]
    public int frequency_status { get; set; }

    [XmlElement("month_idx")]
    public int month_idx { get; set; }

    [XmlElement("month_name_th")]
    public string month_name_th { get; set; }

    [XmlElement("month_condition")]
    public int month_condition { get; set; }

    [XmlElement("m0_notification_idx")]
    public int m0_notification_idx { get; set; }

    [XmlElement("m0_notification_name")]
    public string m0_notification_name { get; set; }

    [XmlElement("notification_status")]
    public int notification_status { get; set; }

    [XmlElement("m1_notification_idx")]
    public int m1_notification_idx { get; set; }

    [XmlElement("m1_notification_name")]
    public string m1_notification_name { get; set; }

    [XmlElement("m1_notification_status")]
    public int m1_notification_status { get; set; }

    [XmlElement("u0doc_idx")]
    public int u0doc_idx { get; set; }
    
    [XmlElement("date_condition")]
    public int date_condition { get; set; }   
    
    [XmlElement("certificate_name")]
    public string certificate_name { get; set; } 

    [XmlElement("certificate_detail")]
    public string certificate_detail { get; set; }

    [XmlElement("contact")]
    public string contact { get; set; }

    [XmlElement("phone_no")]
    public string phone_no { get; set; }

    [XmlElement("remark")]
    public string remark { get; set; }

    [XmlElement("date_start")]
    public string date_start { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("notification_idx")]
    public int notification_idx { get; set; }

    [XmlElement("u0_status")]
    public int u0_status { get; set; }

    [XmlElement("u2doc_idx")]
    public int u2doc_idx { get; set; }

    [XmlElement("u2_status")]
    public int u2_status { get; set; }


    [XmlElement("u1_status")]
    public int u1_status { get; set; }


    [XmlElement("status_name")]
    public string status_name { get; set; }


    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rel_idx")]
    public int rel_idx { get; set; }


    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }


    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }


    [XmlElement("admin_idx")]
    public int admin_idx { get; set; }

    

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }




}
#endregion

#region u1
[Serializable]
public class certificate_doc_detail_u1
{
  
    [XmlElement("u1doc_idx")]
    public int u1doc_idx { get; set; }

    [XmlElement("u0doc_idx")]
    public int u0doc_idx { get; set; }

    [XmlElement("month_idx")]
    public int month_idx { get; set; }

    [XmlElement("m1_notification_idx")]
    public int m1_notification_idx { get;set;}

    [XmlElement("u1_status")]
    public int u1_status { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    
    [XmlElement("month_name_th")]
    public string month_name_th { get; set; }


}
# endregion

#region u2
[Serializable]
public class certificate_doc_detail_u2
{
    [XmlElement("u2doc_idx")]
    public int u2doc_idx { get; set; }

    [XmlElement("u0doc_idx")]
    public int u0doc_idx { get; set; }

    [XmlElement("date_condition")]
    public int date_condition  { get;set;}

    [XmlElement("u2_status")]
    public int u2_status { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }



}

#endregion


#region notification_m1
[Serializable]
public class certificate_doc_detail_notification_m1
{
    
    [XmlElement("m1_notification_idx")]
    public int m1_notification_idx { get; set; }

    [XmlElement("m1_notification_name")]
    public string m1_notification_name { get; set; }


    [XmlElement("m0_notification_idx")]
    public int m0_notification_idx { get; set; }

    [XmlElement("m1_notification_status")]
    public int m1_notification_status{ get; set; }


    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

}
#endregion


#region frequency_m0
[Serializable]
public class certificate_doc_detail_frequency_m0
{

    [XmlElement("frequency_idx")]
    public int frequency_idx { get; set; }

    [XmlElement("frequency_name")]
    public string frequency_name { get; set; }

    [XmlElement("frequency_status")]
    public int frequency_status { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

}
#endregion


#region notification_m0
[Serializable]
public class certificate_doc_detail_notification_m0
{

    [XmlElement("m0_notification_idx")]
    public int m0_notification_idx { get; set; }

    [XmlElement("m0_notification_name")]
    public string m0_notification_name { get; set; }



    [XmlElement("m0_notification_status")]
    public int m0_notification_status { get; set; }


    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

}
#endregion


#region  permission
[Serializable]
public class certificate_doc_detail_permission
{

    [XmlElement("permission_idx")]
    public int permission_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }


    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }


    [XmlElement("rpos_idx")]
    public int rpos_idx    { get; set; }


    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }


    [XmlElement("create_date")]
    public string create_date { get; set; }


    [XmlElement("permission_name")]
    public string permission_name { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("permission_status")]
    public int permission_status { get; set; }


    [XmlElement("admin_idx")]
    public int admin_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }





}
#endregion



