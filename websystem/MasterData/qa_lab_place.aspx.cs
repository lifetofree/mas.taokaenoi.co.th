using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_lab_place: System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    data_qa _data_qa = new data_qa();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlQaSetplace = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetplace"];
    static string _urlQagetplace = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetplace"];
    static string _urlQaDeleteplace = _serviceUrl + ConfigurationManager.AppSettings["urlQaDeleteplace"];



    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion


    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();

        }

    }
    #endregion


    #region initPage
    protected void initPage()
    {
        MvMaster_place.SetActiveView(view1_place);
        Select_Place();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region Insert&Select&Update
    protected void Insert_Place()      
    {
        FormView fvDocDetail_insert_qa = (FormView)view1_place.FindControl("Fv_Insert_Result");
        TextBox tex_name_place = (TextBox)fvDocDetail_insert_qa.FindControl("txtplace_names");
        TextBox tex_code_place = (TextBox)fvDocDetail_insert_qa.FindControl("txtplace_code");
        DropDownList dropD_status_place = (DropDownList)fvDocDetail_insert_qa.FindControl("ddPlaceadd");

        data_qa Place_b = new data_qa();
        qa_m0_place_detail Place_s = new qa_m0_place_detail();
        Place_b.qa_m0_place_list = new qa_m0_place_detail[1];
        Place_s.cemp_idx = _emp_idx;
        Place_s.place_name = tex_name_place.Text;
        Place_s.place_code = tex_code_place.Text;
        Place_s.place_status = int.Parse(dropD_status_place.SelectedValue);
        Place_b.qa_m0_place_list[0] = Place_s;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        Place_b = callServicePostMasterQA(_urlQaSetplace, Place_b);
        if (Place_b.return_code == 101)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            tex_name_place.Text = String.Empty;
            tex_code_place.Text = String.Empty;
        }
    }
    protected void Select_Place()  
    {
        data_qa Place_b = new data_qa();
        qa_m0_place_detail Place_s = new qa_m0_place_detail();

        Place_b.qa_m0_place_list = new qa_m0_place_detail[1];
        Place_b.qa_m0_place_list[0] = Place_s;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        Place_b = callServicePostMasterQA(_urlQagetplace, Place_b);
        setGridData(Gv_select_place, Place_b.qa_m0_place_list);
        Gv_select_place.DataSource = Place_b.qa_m0_place_list;
        Gv_select_place.DataBind();
    }
    #endregion

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion
    
    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }


    #endregion

    #region callService
    protected data_qa callServicePostMasterQA(string _cmdUrl, data_qa _data_qa)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);


        return _data_qa;
    }
    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    //protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    //{
    //    // convert to json
    //    _localJson = _funcTool.convertObjectToJson(_dataEmployee);
    //    //litDebug.Text = _localJson;

    //    // call services
    //    _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

    //    // convert json to object
    //    _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

    //    return _dataEmployee;
    //}
    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName) {

            case "cmdAddplace":

                Gv_select_place.EditIndex = -1;
                Select_Place();
                btn_addplace.Visible = false;
                setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                Fv_Insert_Result.Visible = true;
                //Gv_select_place.Visible = false;
               
               
                break;

            case "Lbtn_cancel_place":  
                btn_addplace.Visible = true;
              //  setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                Fv_Insert_Result.Visible = false;
                // Gv_select_place.Visible = true;
                SETFOCUS_ONTOP.Focus();
                break;

            case "Lbtn_submit_place":
                Insert_Place();
              //  setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                Fv_Insert_Result.Visible = false;
                btn_addplace.Visible = true;
                Select_Place();
                SETFOCUS_ONTOP.Focus();
                break;
            case "btnTodelete_Place":
               // ViewState["DELETE_PLACE"] = int.Parse(cmdArg);

                int a = int.Parse(cmdArg);
                data_qa Place_de = new data_qa();
                qa_m0_place_detail Place_sde = new qa_m0_place_detail();

                Place_de.qa_m0_place_list = new qa_m0_place_detail[1];

                Place_sde.place_idx = int.Parse(a.ToString());

                Place_de.qa_m0_place_list[0] = Place_sde;
              //  test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_de));
                Place_de = callServicePostMasterQA(_urlQaDeleteplace, Place_de);

                if (Place_de.return_code == 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);

                }

                else

                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);


                }

                Select_Place();
                Fv_Insert_Result.Visible = false;
                btn_addplace.Visible = true;
                break;  
        }
        
        }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gv_select_place":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbplace_status = (Label)e.Row.Cells[3].FindControl("lbplace_status");
                    Label place_statusOpen = (Label)e.Row.Cells[3].FindControl("place_statusOpen");
                    Label place_statusClose = (Label)e.Row.Cells[3].FindControl("place_statusClose");

                    ViewState["_place_status"] = lbplace_status.Text;


                    if (ViewState["_place_status"].ToString() == "1")
                    {
                        place_statusOpen.Visible = true;
                    }
                    else if (ViewState["_place_status"].ToString() == "0")
                    {
                        place_statusClose.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    btn_addplace.Visible = true;
                    setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                }

                

                break;

           
        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_place":
                Gv_select_place.EditIndex = e.NewEditIndex;
                Select_Place();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_place":
                var id_place = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("ID_place");
                var name_place = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Name_place");
                var code_place = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Code_place");
                var status_place = (DropDownList)Gv_select_place.Rows[e.RowIndex].FindControl("ddEdit_place");


                Gv_select_place.EditIndex = -1;


                data_qa place_Update = new data_qa();
                place_Update.qa_m0_place_list = new qa_m0_place_detail[1];
                qa_m0_place_detail place_s = new qa_m0_place_detail();
                place_s.place_idx =  int.Parse(id_place.Text);
                place_s.place_status = int.Parse(status_place.SelectedValue);
                place_s.place_name = name_place.Text;
                place_s.place_code = code_place.Text;


                //ViewState["_NAMEPLACE"] = name_place.Text;
                //ViewState["_CODEPLACE"] = code_place.Text;

                 place_Update.qa_m0_place_list[0] = place_s;
                 //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(place_Update));
                place_Update = callServicePostMasterQA(_urlQaSetplace, place_Update);
                Select_Place();
                //if (place_Update.return_code == 0)
                //{

                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);

                //}

                //else

                //{


                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);


                //}


                SETFOCUS_ONTOP.Focus();
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_place":
                Gv_select_place.EditIndex = -1;
                Select_Place();
                btn_addplace.Visible = true;
                SETFOCUS_ONTOP.Focus();
                break;
            
        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_place":
                Gv_select_place.PageIndex = e.NewPageIndex;
                Gv_select_place.DataBind();
                Select_Place();
                break;
            
        }
    }
    #endregion


}