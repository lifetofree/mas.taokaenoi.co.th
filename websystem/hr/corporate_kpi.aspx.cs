﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_corporate_kpi : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();
    data_kpi _data_kpi = new data_kpi();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];

    static string _urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetPlace"];

    //-- employee --//

    //-- KPI --//
    static string _urlGetCorporateKpi = _serviceUrl + ConfigurationManager.AppSettings["urlGetCorporateKpi"];
    static string _urlSetCorporateKpi = _serviceUrl + ConfigurationManager.AppSettings["urlSetCorporateKpi"];
    static string _urlGetTQACorporateKpi = _serviceUrl + ConfigurationManager.AppSettings["urlGetTQACorporateKpi"];
    //-- KPI --//


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;

    string month_pre = "";
    string month_current = "";


    //ViewState["vs_ViewWaitApproveRoomBooking"]

    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;
        ViewState["emp_type_permission"] = _dataEmployee.employee_list[0].emp_type_idx;
        ViewState["time_idx_permission"] = _dataEmployee.employee_list[0].ACIDX; // shift type

        ViewState["Vs_check_tab_noidx"] = 0;

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            initPage();
            initPageLoad();

            //getCountWaitDetailApproveOTAdminCreate();

        }

    }

    #region set/get bind data

    protected void getDetailCorporateKpi(GridView gvName, int u0_detail_idx)
    {

        data_kpi data_kpi_details_u0 = new data_kpi();

        kpi_search_template_detail kpi_search_template = new kpi_search_template_detail();
        data_kpi_details_u0.kpi_search_template_list = new kpi_search_template_detail[1];

        kpi_u0_detail kpi_details_u0 = new kpi_u0_detail();
        data_kpi_details_u0.kpi_details_u0_list = new kpi_u0_detail[1];

        kpi_search_template.s_condition = "0";
        kpi_search_template.s_cemp_idx = _emp_idx.ToString();

        data_kpi_details_u0.kpi_search_template_list[0] = kpi_search_template;
        data_kpi_details_u0.kpi_details_u0_list[0] = kpi_details_u0;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_kpi_details_u0));
        data_kpi_details_u0 = callServicePostCorporateKpi(_urlGetCorporateKpi, data_kpi_details_u0);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_kpi_details_u0));

        if (data_kpi_details_u0.return_code == 0)
        {
            ViewState["Vs_DetailCorporateKpi"] = data_kpi_details_u0.kpi_details_u0_list;

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_DetailCorporateKpi"]);
        }
        else
        {
            ViewState["Vs_DetailCorporateKpi"] = null;

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_DetailCorporateKpi"]);
        }

    }

    protected void getDetailInsertCorporateKpi(GridView gvName, int u0_detail_idx)
    {

        kpi_u0_detail[] _value_detailInsert_Corporate = (kpi_u0_detail[])ViewState["Vs_DetailCorporateKpi"];

        var _linq_detailInsert_Corporate = (from data in _value_detailInsert_Corporate
                                            where
                                            data.u0_detail_idx != 5

                                            select new
                                            {
                                                data.u0_detail_idx,
                                                data.details_title,
                                                data.weight,
                                                data.m0_unit_type_idx,
                                                data.unit_name,
                                                data.annual_target,
                                                data.u0_detail_status,

                                            }
                                       ).Distinct().ToList();

        //ViewState["Vs_DetailInsertCorporateKpi"] = _linq_detailInsert_Corporate.ToList();
        setGridData(gvName, _linq_detailInsert_Corporate.ToList());

    }

    protected void getDetailValueCorporateKpi(int u0_detail_idx, int u1_detail_idx)
    {

        //data_kpi data_kpi_details_u2 = new data_kpi();

        //kpi_search_template_detail kpi_search_template = new kpi_search_template_detail();
        //data_kpi_details_u2.kpi_search_template_list = new kpi_search_template_detail[1];

        //kpi_u2_detail kpi_details_u2 = new kpi_u2_detail();
        //data_kpi_details_u2.kpi_details_u2_list = new kpi_u2_detail[1];

        //kpi_search_template.s_condition = "1";
        //kpi_search_template.s_u1_detail_idx = "0";
        //kpi_search_template.s_cemp_idx = _emp_idx.ToString();

        //data_kpi_details_u2.kpi_search_template_list[0] = kpi_search_template;
        //data_kpi_details_u2.kpi_details_u2_list[0] = kpi_details_u2;

        //data_kpi_details_u2 = callServicePostCorporateKpi(_urlGetCorporateKpi, data_kpi_details_u2);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_kpi_details_u2));

        //ViewState["Vs_DetailValueCorporateKpi"] = data_kpi_details_u2.kpi_details_u2_list;

        //kpi_u2_detail[] _item_value_corporatekpi = (kpi_u2_detail[])ViewState["Vs_DetailValueCorporateKpi"];

        //var _linq_corporatekpi = (from data in _item_value_corporatekpi
                                
        //                          select new
        //                          {
        //                              data.u2_detail_idx,
        //                              // dt.month_idx
        //                          }

        //                    ).ToList();


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_kpi_details_u0));

        ////if (data_kpi_details_u0.return_code == 0)
        ////{
        ////    ViewState["Vs_DetailCorporateKpi"] = data_kpi_details_u0.kpi_details_u0_list;

        ////    gvName.Visible = true;
        ////    setGridData(gvName, ViewState["Vs_DetailCorporateKpi"]);
        ////}
        ////else
        ////{
        ////    ViewState["Vs_DetailCorporateKpi"] = null;

        ////    gvName.Visible = true;
        ////    setGridData(gvName, ViewState["Vs_DetailCorporateKpi"]);
        ////}

    }

    protected void getDetailTQA(GridView gvName, int u0_detail_idx)
    {

        data_kpi data_kpi_details_u1 = new data_kpi();

        kpi_search_template_detail kpi_search_template = new kpi_search_template_detail();
        data_kpi_details_u1.kpi_search_template_list = new kpi_search_template_detail[1];

        kpi_u1_detail kpi_details_u1 = new kpi_u1_detail();
        data_kpi_details_u1.kpi_details_u1_list = new kpi_u1_detail[1];

        kpi_search_template.s_condition = "0";
        kpi_search_template.s_cemp_idx = _emp_idx.ToString();

        data_kpi_details_u1.kpi_search_template_list[0] = kpi_search_template;
        data_kpi_details_u1.kpi_details_u1_list[0] = kpi_details_u1;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_kpi_details_u0));
        data_kpi_details_u1 = callServicePostCorporateKpi(_urlGetTQACorporateKpi, data_kpi_details_u1);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_kpi_details_u1));

        if (data_kpi_details_u1.return_code == 0)
        {
            ViewState["Vs_DetailTQA"] = data_kpi_details_u1.kpi_details_u1_list;

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_DetailTQA"]);
        }
        else
        {
            ViewState["Vs_DetailTQA"] = null;

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_DetailTQA"]);
        }

    }

    protected void GetPathFile(string _u0_doc_idx, string path, GridView gvName)
    {
        try
        {
            string filePathLotus = Server.MapPath(path + _u0_doc_idx);
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
            SearchDirectories(myDirLotus, _u0_doc_idx, gvName);
            gvName.Visible = true;

            ViewState["Vs_CheckFileMemo"] = "yes";

        }
        catch (Exception ex)
        {
            gvName.Visible = false;
            ViewState["Vs_CheckFileMemo"] = "no";
            //txt.Text = ex.ToString();
        }
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target, GridView gvName)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");


        FileInfo[] files = dir.GetFiles();
        int i = 0;
        foreach (FileInfo file in files)
        {
            //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
            //{
            string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
            dt1.Rows.Add(file.Name);
            dt1.Rows[i][1] = f[0];
            i++;
            //}
        }


        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gvName.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvName.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvName.DataSource = null;
            gvName.DataBind();

        }


    }

    #endregion set/get bind data  

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0, 0);

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            
            case "cmdViewUserShiftRotate":

                string[] arg_viewuser = new string[1];
                arg_viewuser = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_viewuser = int.Parse(arg_viewuser[0]);
                int _u1_doc_idx_viewuser = int.Parse(arg_viewuser[1]);
                // string _decision_name_saveotday = arg_viewuser[1];


                setActiveTab("docDetail", _u0_doc_idx_viewuser, 0, 0, 0, 0, 0, _u1_doc_idx_viewuser);
                break;
            case "cmdDocCancel":

                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);
                break;
            case "cmdInsertCorporateKPI":

                setActiveTab("docCorparateKPIs", 0, 0, 0, 0, int.Parse(cmdArg.ToString()), 0, 0);

                break;
            case "cmdSaveCorporateKpi":

                IFormatProvider culture_search = new CultureInfo("en-US", true);
                //string Month_current = DateTime.Now.ToString("yyyy MM", culture_search); //Date Time Today Incheck

                string Year_current = DateTime.Now.ToString("yyyy", culture_search);

                string prevMonth = DateTime.Now.AddMonths(-1).ToString("MM");
                string varDay = DateTime.Now.AddDays(-1).ToString("DD");


                data_kpi data_kpi_details_u2 = new data_kpi();
                //insert u2 document corporate
                var _u2_doc_idx = new kpi_u2_detail[GvInsertCorporateKpi.Rows.Count];
                int sum_ = 0;
                int count_ = 0;
                //int sum_total_price = 0;

                foreach (GridViewRow gv_row in GvInsertCorporateKpi.Rows)
                {

                    data_kpi_details_u2.kpi_details_u2_list = new kpi_u2_detail[1];

                    Label lbl_u0_detail_idx = (Label)gv_row.FindControl("lbl_u0_detail_idx");

                    Label lbl_u2_idx_jan = (Label)gv_row.FindControl("lbl_u2_idx_jan");
                    TextBox txt_jan_target = (TextBox)gv_row.FindControl("txt_jan_target");
                    TextBox txt_jan_actual = (TextBox)gv_row.FindControl("txt_jan_actual");

                    Label lbl_u2_idx_feb = (Label)gv_row.FindControl("lbl_u2_idx_feb");
                    TextBox txt_feb_target = (TextBox)gv_row.FindControl("txt_feb_target");
                    TextBox txt_feb_actual = (TextBox)gv_row.FindControl("txt_feb_actual");

                    Label lbl_u2_idx_mar = (Label)gv_row.FindControl("lbl_u2_idx_mar");
                    TextBox txt_mar_target = (TextBox)gv_row.FindControl("txt_mar_target");
                    TextBox txt_mar_actual = (TextBox)gv_row.FindControl("txt_mar_actual");

                    Label lbl_u2_idx_apr = (Label)gv_row.FindControl("lbl_u2_idx_apr");
                    TextBox txt_apr_target = (TextBox)gv_row.FindControl("txt_apr_target");
                    TextBox txt_apr_actual = (TextBox)gv_row.FindControl("txt_apr_actual");

                    Label lbl_u2_idx_may = (Label)gv_row.FindControl("lbl_u2_idx_may");
                    TextBox txt_may_target = (TextBox)gv_row.FindControl("txt_may_target");
                    TextBox txt_may_actual = (TextBox)gv_row.FindControl("txt_may_actual");

                    Label lbl_u2_idx_jun = (Label)gv_row.FindControl("lbl_u2_idx_jun");
                    TextBox txt_jun_target = (TextBox)gv_row.FindControl("txt_jun_target");
                    TextBox txt_jun_actual = (TextBox)gv_row.FindControl("txt_jun_actual");

                    Label lbl_u2_idx_jul = (Label)gv_row.FindControl("lbl_u2_idx_jul");
                    TextBox txt_jul_target = (TextBox)gv_row.FindControl("txt_jul_target");
                    TextBox txt_jul_actual = (TextBox)gv_row.FindControl("txt_jul_actual");

                    Label lbl_u2_idx_aug = (Label)gv_row.FindControl("lbl_u2_idx_aug");
                    TextBox txt_aug_target = (TextBox)gv_row.FindControl("txt_aug_target");
                    TextBox txt_aug_actual = (TextBox)gv_row.FindControl("txt_aug_actual");

                    Label lbl_u2_idx_sep = (Label)gv_row.FindControl("lbl_u2_idx_sep");
                    TextBox txt_sep_target = (TextBox)gv_row.FindControl("txt_sep_target");
                    TextBox txt_sep_actual = (TextBox)gv_row.FindControl("txt_sep_actual");

                    Label lbl_u2_idx_oct = (Label)gv_row.FindControl("lbl_u2_idx_oct");
                    TextBox txt_oct_target = (TextBox)gv_row.FindControl("txt_oct_target");
                    TextBox txt_oct_actual = (TextBox)gv_row.FindControl("txt_oct_actual");

                    Label lbl_u2_idx_nov = (Label)gv_row.FindControl("lbl_u2_idx_nov");
                    TextBox txt_nov_target = (TextBox)gv_row.FindControl("txt_nov_target");
                    TextBox txt_nov_actual = (TextBox)gv_row.FindControl("txt_nov_actual");

                    Label lbl_u2_idx_dec = (Label)gv_row.FindControl("lbl_u2_idx_dec");
                    TextBox txt_dec_target = (TextBox)gv_row.FindControl("txt_dec_target");
                    TextBox txt_dec_actual = (TextBox)gv_row.FindControl("txt_dec_actual");


                    _u2_doc_idx[count_] = new kpi_u2_detail();
                    _u2_doc_idx[count_].cemp_idx = _emp_idx;

                    _u2_doc_idx[count_].u0_detail_idx = int.Parse(lbl_u0_detail_idx.Text);

                    // "2019-01-01 00:00:00.000"

                    switch (prevMonth)
                    {

                        case "01":
                            _u2_doc_idx[count_].u2_detail_idx_pre = int.Parse(lbl_u2_idx_jan.Text);
                            _u2_doc_idx[count_].month_pre = "01/01/" + Year_current.ToString();
                            _u2_doc_idx[count_].target_pre = txt_jan_target.Text;
                            _u2_doc_idx[count_].actual_pre = txt_jan_actual.Text;

                            _u2_doc_idx[count_].u2_detail_idx = int.Parse(lbl_u2_idx_feb.Text);
                            _u2_doc_idx[count_].month = "01/02/" + Year_current.ToString();
                            _u2_doc_idx[count_].target = txt_feb_target.Text;
                            _u2_doc_idx[count_].actual = txt_feb_actual.Text;
                            
                            break;
                        case "02":
                            _u2_doc_idx[count_].u2_detail_idx_pre = int.Parse(lbl_u2_idx_feb.Text);
                            _u2_doc_idx[count_].month_pre = "01/02/" + Year_current.ToString();
                            _u2_doc_idx[count_].target_pre = txt_feb_target.Text;
                            _u2_doc_idx[count_].actual_pre = txt_feb_actual.Text;

                            _u2_doc_idx[count_].u2_detail_idx = int.Parse(lbl_u2_idx_mar.Text);
                            _u2_doc_idx[count_].month = "01/03/" + Year_current.ToString();
                            _u2_doc_idx[count_].target = txt_mar_target.Text;
                            _u2_doc_idx[count_].actual = txt_mar_actual.Text;
                            break;
                        case "03":
                            _u2_doc_idx[count_].u2_detail_idx_pre = int.Parse(lbl_u2_idx_mar.Text);
                            _u2_doc_idx[count_].month_pre = "01/03/" + Year_current.ToString();
                            _u2_doc_idx[count_].target_pre = txt_mar_target.Text;
                            _u2_doc_idx[count_].actual_pre = txt_mar_actual.Text;

                            _u2_doc_idx[count_].u2_detail_idx = int.Parse(lbl_u2_idx_apr.Text);
                            _u2_doc_idx[count_].month = "01/04/" + Year_current.ToString();
                            _u2_doc_idx[count_].target = txt_apr_target.Text;
                            _u2_doc_idx[count_].actual = txt_apr_actual.Text;
                            break;
                        case "04":
                            _u2_doc_idx[count_].u2_detail_idx_pre = int.Parse(lbl_u2_idx_apr.Text);
                            _u2_doc_idx[count_].month_pre = "01/04/" + Year_current.ToString();
                            _u2_doc_idx[count_].target_pre = txt_apr_target.Text;
                            _u2_doc_idx[count_].actual_pre = txt_apr_actual.Text;

                            _u2_doc_idx[count_].u2_detail_idx = int.Parse(lbl_u2_idx_may.Text);
                            _u2_doc_idx[count_].month = "01/05/" + Year_current.ToString();
                            _u2_doc_idx[count_].target = txt_may_target.Text;
                            _u2_doc_idx[count_].actual = txt_may_actual.Text;
                            break;
                        case "05":
                            _u2_doc_idx[count_].u2_detail_idx_pre = int.Parse(lbl_u2_idx_may.Text);
                            _u2_doc_idx[count_].month_pre = "01/05/" + Year_current.ToString();
                            _u2_doc_idx[count_].target_pre = txt_may_target.Text;
                            _u2_doc_idx[count_].actual_pre = txt_may_actual.Text;

                            _u2_doc_idx[count_].u2_detail_idx = int.Parse(lbl_u2_idx_jun.Text);
                            _u2_doc_idx[count_].month = "01/06/" + Year_current.ToString();
                            _u2_doc_idx[count_].target = txt_jun_target.Text;
                            _u2_doc_idx[count_].actual = txt_jun_actual.Text;
                            break;
                        case "06":
                            _u2_doc_idx[count_].u2_detail_idx_pre = int.Parse(lbl_u2_idx_jun.Text);
                            _u2_doc_idx[count_].month_pre = "01/06/" + Year_current.ToString();
                            _u2_doc_idx[count_].target_pre = txt_jun_target.Text;
                            _u2_doc_idx[count_].actual_pre = txt_jun_actual.Text;

                            _u2_doc_idx[count_].u2_detail_idx = int.Parse(lbl_u2_idx_jul.Text);
                            _u2_doc_idx[count_].month = "01/07/" + Year_current.ToString();
                            _u2_doc_idx[count_].target = txt_jul_target.Text;
                            _u2_doc_idx[count_].actual = txt_jul_actual.Text;
                            break;
                        case "07":
                            _u2_doc_idx[count_].u2_detail_idx_pre = int.Parse(lbl_u2_idx_jul.Text);
                            _u2_doc_idx[count_].month_pre = "01/07/" + Year_current.ToString();
                            _u2_doc_idx[count_].target_pre = txt_jul_target.Text;
                            _u2_doc_idx[count_].actual_pre = txt_jul_actual.Text;

                            _u2_doc_idx[count_].u2_detail_idx = int.Parse(lbl_u2_idx_jul.Text);
                            _u2_doc_idx[count_].month = "01/08/" + Year_current.ToString();
                            _u2_doc_idx[count_].target = txt_aug_target.Text;
                            _u2_doc_idx[count_].actual = txt_aug_actual.Text;

                            break;
                        case "08":
                            _u2_doc_idx[count_].u2_detail_idx_pre = int.Parse(lbl_u2_idx_aug.Text);
                            _u2_doc_idx[count_].month_pre = "01/08/" + Year_current.ToString();
                            _u2_doc_idx[count_].target_pre = txt_aug_target.Text;
                            _u2_doc_idx[count_].actual_pre = txt_aug_actual.Text;

                            _u2_doc_idx[count_].u2_detail_idx = int.Parse(lbl_u2_idx_sep.Text);
                            _u2_doc_idx[count_].month = "01/09/" + Year_current.ToString();
                            _u2_doc_idx[count_].target = txt_sep_target.Text;
                            _u2_doc_idx[count_].actual = txt_sep_actual.Text;
                            break;
                        case "09":
                            _u2_doc_idx[count_].u2_detail_idx_pre = int.Parse(lbl_u2_idx_sep.Text);
                            _u2_doc_idx[count_].month_pre = "01/09/" + Year_current.ToString();
                            _u2_doc_idx[count_].target_pre = txt_sep_target.Text;
                            _u2_doc_idx[count_].actual_pre = txt_sep_actual.Text;

                            _u2_doc_idx[count_].u2_detail_idx = int.Parse(lbl_u2_idx_oct.Text);
                            _u2_doc_idx[count_].month = "01/10/" + Year_current.ToString();
                            _u2_doc_idx[count_].target = txt_oct_target.Text;
                            _u2_doc_idx[count_].actual = txt_oct_actual.Text;
                            break;
                        case "10":
                            _u2_doc_idx[count_].u2_detail_idx_pre = int.Parse(lbl_u2_idx_oct.Text);
                            _u2_doc_idx[count_].month_pre = "01/10/" + Year_current.ToString();
                            _u2_doc_idx[count_].target_pre = txt_oct_target.Text;
                            _u2_doc_idx[count_].actual_pre = txt_oct_actual.Text;

                            _u2_doc_idx[count_].u2_detail_idx = int.Parse(lbl_u2_idx_nov.Text);
                            _u2_doc_idx[count_].month = "01/11/" + Year_current.ToString();
                            _u2_doc_idx[count_].target = txt_nov_target.Text;
                            _u2_doc_idx[count_].actual = txt_nov_actual.Text;
                            break;

                        case "11":
                            _u2_doc_idx[count_].u2_detail_idx_pre = int.Parse(lbl_u2_idx_nov.Text);
                            _u2_doc_idx[count_].month_pre = "01/11/" + Year_current.ToString();
                            _u2_doc_idx[count_].target_pre = txt_nov_target.Text;
                            _u2_doc_idx[count_].actual_pre = txt_nov_actual.Text;

                            _u2_doc_idx[count_].u2_detail_idx = int.Parse(lbl_u2_idx_dec.Text);
                            _u2_doc_idx[count_].month = "01/12/" + Year_current.ToString();
                            _u2_doc_idx[count_].target = txt_dec_target.Text;
                            _u2_doc_idx[count_].actual = txt_dec_actual.Text;
                            break;
                        case "12":
                            //_u2_doc_idx[count_].month_pre = "12";
                            //_u2_doc_idx[count_].target_pre = txt_jan_target.Text;
                            //_u2_doc_idx[count_].actual_pre = txt_jan_actual.Text;
                            _u2_doc_idx[count_].u2_detail_idx = int.Parse(lbl_u2_idx_jan.Text);
                            _u2_doc_idx[count_].month = "01/01/" + Year_current.ToString();
                            _u2_doc_idx[count_].target = txt_jan_target.Text;
                            _u2_doc_idx[count_].actual = txt_jan_actual.Text;
                            break;
                    }

                    count_++;
                }
                //insert u2 document corporate

                data_kpi_details_u2.kpi_details_u2_list = _u2_doc_idx;
                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_kpi_details_u2));
                data_kpi_details_u2 = callServicePostCorporateKpi(_urlSetCorporateKpi, data_kpi_details_u2);

                setActiveTab("docCorparateKPIs", 0, 0, 0, 0, 0, 0, 0);


                break;
            case "cmdCancel":
                setActiveTab("docCorparateKPIs", 0, 0, 0, 0, 0, 0, 0);
                break;

        }

    }
    //endbtn

    #endregion event command

    #region Changed
    protected void onSelectedCheckChanged(object sender, EventArgs e)
    {
        var cbName = (CheckBox)sender;

        //var chkName = (CheckBoxList)sender;

        switch (cbName.ID)
        {

            case "chkallApprove":
                //int chack_all = 0;

                //if (chkallApprove.Checked)
                //{

                //    foreach (GridViewRow row in GvWaitApprove.Rows)
                //    {
                //        CheckBox chk_ot_approve = (CheckBox)row.FindControl("chk_ot_approve");

                //        chk_ot_approve.Checked = true;
                //        chack_all++;
                //    }
                //}
                //else
                //{
                //    foreach (GridViewRow row in GvWaitApprove.Rows)
                //    {
                //        CheckBox chk_ot_approve = (CheckBox)row.FindControl("chk_ot_approve");

                //        chk_ot_approve.Checked = false;
                //        chack_all++;
                //    }
                //}

                break;

          

        }
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;
        //DropDownList ddlorg_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlorg_rp");
        //DropDownList ddldep_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddldep_rp");
        //DropDownList ddlsec_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlsec_rp");

        switch (ddlName.ID)
        {

            case "ddl_timeholiday":

                ////GridView grid = sender as GridView;

                ////int RowIndex = ((GridViewRow)(((DropDownList)sender).Parent.Parent)).RowIndex;
                ////////int RowIndex_Label = ((GridViewRow)(((Label)sender).Parent.Parent)).RowIndex;
                //////// GridViewRow RowIndex = GvCreateShiftRotate.SelectedRow;

                ////DropDownList ddl_timeholiday = (DropDownList)GvCreateShiftRotate.Rows[RowIndex].FindControl("ddl_timeholiday");
                ////DropDownList ddl_timeafter = (DropDownList)GvCreateShiftRotate.Rows[RowIndex].FindControl("ddl_timeafter");
                ////DropDownList ddl_timebefore = (DropDownList)GvCreateShiftRotate.Rows[RowIndex].FindControl("ddl_timebefore");

                ////Label lbl_hour_otbefore = (Label)GvCreateShiftRotate.Rows[RowIndex].FindControl("lbl_hour_otbefore");
                ////Label lbl_hour_otafter = (Label)GvCreateShiftRotate.Rows[RowIndex].FindControl("lbl_hour_otafter");

                ////if (ddl_timeholiday.SelectedValue.ToString() == "8" && ddl_timeafter.SelectedValue != "")
                ////{
                ////    ddl_timeafter.Visible = true;
                ////    lbl_hour_otafter.Visible = true;

                ////}

                ////if (ddl_timeholiday.SelectedValue.ToString() == "8" && ddl_timebefore.SelectedValue != "")
                ////{

                ////    ddl_timebefore.Visible = true;
                ////    lbl_hour_otbefore.Visible = true;

                ////}


                break;

            case "ddlorg":

                //getDepartmentList(ddlrdept, int.Parse(ddlorg.SelectedValue));

                //ddlrsec.Items.Clear();
                //ddlrsec.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

                break;
            case "ddlrdept":

                //getSectionList(ddlrsec, int.Parse(ddlorg.SelectedItem.Value), int.Parse(ddlrdept.SelectedItem.Value));
                break;



        }
    }

    protected void TextBoxChanged(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;

        switch (txtName.ID)
        {

            ////case "txt_timeend_job":

            ////    //linkBtnTrigger(btnSaveDetailMA);

            ////    foreach (GridViewRow row in GvCreateOTMonth.Rows)
            ////    {
            ////        CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");
            ////        UpdatePanel Update_Timestart = (UpdatePanel)row.FindControl("Update_Timestart");
            ////        UpdatePanel Update_Timeend = (UpdatePanel)row.FindControl("Update_Timeend");
            ////        TextBox txt_job = (TextBox)row.FindControl("txt_job");
            ////        TextBox txt_timestart_job = (TextBox)row.FindControl("txt_timestart_job");
            ////        TextBox txt_timeend_job = (TextBox)row.FindControl("txt_timeend_job");
            ////        TextBox txt_sum_hour = (TextBox)row.FindControl("txt_sum_hour");
            ////        TextBox txt_head_set = (TextBox)row.FindControl("txt_head_set");
            ////        TextBox txt_remark = (TextBox)row.FindControl("txt_remark");
            ////        decimal total_hour = 0;


            ////        //TimeSpan diff = secondDate - firstDate;
            ////        //double hours = diff.TotalHours;

            ////        litDebug.Text = "3333";

            ////        if (chk_date_ot.Checked)
            ////        {
            ////            if (txt_timestart_job.Text != "" && txt_timeend_job.Text != "")
            ////            {
            ////                //linkBtnTrigger(btnSaveDetailMA);
            ////                //litDebug.Text = Convert.ToInt32(txt_Quantity_txt.Text).ToString();
            ////                total_hour = Convert.ToDecimal(txt_timestart_job.Text) * Convert.ToDecimal(txt_timeend_job.Text);
            ////                //litDebug.Text = Convert.ToDecimal(total_price).ToString();
            ////                txt_sum_hour.Text = Convert.ToDecimal(total_hour).ToString();

            ////            }
            ////            else
            ////            {
            ////                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรอกราคา/หน่วย ---');", true);

            ////                break;
            ////            }
            ////        }


            ////    }

            ////    break;




        }

    }

    #endregion Changed

    #region gridview
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "GvDetailOTShiftRotate":
                //setGridData(GvDetailOTShiftRotate, ViewState["Vs_DetailShiftRotateToUser"]);

                break;
      
        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvDetailCorporateKpi":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label lbl_u0_detail_idx = (Label)e.Row.FindControl("lbl_u0_detail_idx");

                    #region set month value

                    Label lbl_set_month_jan = (Label)e.Row.FindControl("lbl_set_month_jan");
                    Label lbl_set_month_feb = (Label)e.Row.FindControl("lbl_set_month_feb");
                    Label lbl_set_month_mar = (Label)e.Row.FindControl("lbl_set_month_mar");
                    Label lbl_set_month_apr = (Label)e.Row.FindControl("lbl_set_month_apr");
                    Label lbl_set_month_may = (Label)e.Row.FindControl("lbl_set_month_may");
                    Label lbl_set_month_jun = (Label)e.Row.FindControl("lbl_set_month_jun");
                    Label lbl_set_month_jul = (Label)e.Row.FindControl("lbl_set_month_jul");
                    Label lbl_set_month_aug = (Label)e.Row.FindControl("lbl_set_month_aug");
                    Label lbl_set_month_sep = (Label)e.Row.FindControl("lbl_set_month_sep");
                    Label lbl_set_month_oct = (Label)e.Row.FindControl("lbl_set_month_oct");
                    Label lbl_set_month_nov = (Label)e.Row.FindControl("lbl_set_month_nov");
                    Label lbl_set_month_dec = (Label)e.Row.FindControl("lbl_set_month_dec");


                    lbl_set_month_jan.Text = "1";
                    lbl_set_month_feb.Text = "2";
                    lbl_set_month_mar.Text = "3";
                    lbl_set_month_apr.Text = "4";
                    lbl_set_month_may.Text = "5";
                    lbl_set_month_jun.Text = "6";
                    lbl_set_month_jul.Text = "7";
                    lbl_set_month_aug.Text = "8";
                    lbl_set_month_sep.Text = "9";
                    lbl_set_month_oct.Text = "10";
                    lbl_set_month_nov.Text = "11";
                    lbl_set_month_dec.Text = "12";

                    #endregion set month value

                    #region set target/actual month value
                    Label lbl_target_jan = (Label)e.Row.FindControl("lbl_target_jan");
                    Label lbl_actual_jan = (Label)e.Row.FindControl("lbl_actual_jan");

                    Label lbl_target_feb = (Label)e.Row.FindControl("lbl_target_feb");
                    Label lbl_actual_feb = (Label)e.Row.FindControl("lbl_actual_feb");

                    Label lbl_target_mar = (Label)e.Row.FindControl("lbl_target_mar");
                    Label lbl_actual_mar = (Label)e.Row.FindControl("lbl_actual_mar");

                    Label lbl_target_apr = (Label)e.Row.FindControl("lbl_target_apr");
                    Label lbl_actual_apr = (Label)e.Row.FindControl("lbl_actual_apr");

                    Label lbl_target_may = (Label)e.Row.FindControl("lbl_target_may");
                    Label lbl_actual_may = (Label)e.Row.FindControl("lbl_actual_may");

                    Label lbl_target_jun = (Label)e.Row.FindControl("lbl_target_jun");
                    Label lbl_actual_jun = (Label)e.Row.FindControl("lbl_actual_jun");

                    Label lbl_target_jul = (Label)e.Row.FindControl("lbl_target_jul");
                    Label lbl_actual_jul = (Label)e.Row.FindControl("lbl_actual_jul");

                    Label lbl_target_aug = (Label)e.Row.FindControl("lbl_target_aug");
                    Label lbl_actual_aug = (Label)e.Row.FindControl("lbl_actual_aug");

                    Label lbl_target_sep = (Label)e.Row.FindControl("lbl_target_sep");
                    Label lbl_actual_sep = (Label)e.Row.FindControl("lbl_actual_sep");

                    Label lbl_target_oct = (Label)e.Row.FindControl("lbl_target_oct");
                    Label lbl_actual_oct = (Label)e.Row.FindControl("lbl_actual_oct");

                    Label lbl_target_nov = (Label)e.Row.FindControl("lbl_target_nov");
                    Label lbl_actual_nov = (Label)e.Row.FindControl("lbl_actual_nov");

                    Label lbl_target_dec = (Label)e.Row.FindControl("lbl_target_dec");
                    Label lbl_actual_dec = (Label)e.Row.FindControl("lbl_actual_dec");
                    #endregion set target/actual month value

                    data_kpi data_kpi_details_u2 = new data_kpi();

                    kpi_search_template_detail kpi_search_template = new kpi_search_template_detail();
                    data_kpi_details_u2.kpi_search_template_list = new kpi_search_template_detail[1];

                    kpi_u2_detail kpi_details_u2 = new kpi_u2_detail();
                    data_kpi_details_u2.kpi_details_u2_list = new kpi_u2_detail[1];

                    kpi_search_template.s_condition = "1";
                    kpi_search_template.s_u1_detail_idx = "0";
                    kpi_search_template.s_cemp_idx = _emp_idx.ToString();

                    data_kpi_details_u2.kpi_search_template_list[0] = kpi_search_template;
                    data_kpi_details_u2.kpi_details_u2_list[0] = kpi_details_u2;

                    data_kpi_details_u2 = callServicePostCorporateKpi(_urlGetCorporateKpi, data_kpi_details_u2);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_kpi_details_u2));

                    ViewState["Vs_DetailValueCorporateKpi"] = data_kpi_details_u2.kpi_details_u2_list;

                    kpi_u2_detail[] _item_value_corporatekpi = (kpi_u2_detail[])ViewState["Vs_DetailValueCorporateKpi"];

                    var _linq_corporatekpi = (from data in _item_value_corporatekpi
                                              where
                                              data.u0_detail_idx == int.Parse(lbl_u0_detail_idx.Text)

                                              select data

                                        ).ToList();


                    for (int _count = 0; _count < _linq_corporatekpi.Count(); _count++)
                    {
                        //jan
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_jan.Text)
                        {

                            lbl_target_jan.Text = _linq_corporatekpi[_count].target.ToString();
                            lbl_actual_jan.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //feb
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_feb.Text)
                        {

                            lbl_target_feb.Text = _linq_corporatekpi[_count].target.ToString();
                            lbl_actual_feb.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //mar
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_mar.Text)
                        {

                            lbl_target_mar.Text = _linq_corporatekpi[_count].target.ToString();
                            lbl_actual_mar.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //apr
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_apr.Text)
                        {

                            lbl_target_apr.Text = _linq_corporatekpi[_count].target.ToString();
                            lbl_actual_apr.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //may
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_may.Text)
                        {

                            lbl_target_may.Text = _linq_corporatekpi[_count].target.ToString();
                            lbl_actual_may.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //jun
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_jun.Text)
                        {

                            lbl_target_jun.Text = _linq_corporatekpi[_count].target.ToString();
                            lbl_actual_jun.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //jul
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_jul.Text)
                        {

                            lbl_target_jul.Text = _linq_corporatekpi[_count].target.ToString();
                            lbl_actual_jul.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //aug
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_aug.Text)
                        {

                            lbl_target_aug.Text = _linq_corporatekpi[_count].target.ToString();
                            lbl_actual_aug.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //sep
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_sep.Text)
                        {

                            lbl_target_sep.Text = _linq_corporatekpi[_count].target.ToString();
                            lbl_actual_sep.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //oct
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_oct.Text)
                        {

                            lbl_target_oct.Text = _linq_corporatekpi[_count].target.ToString();
                            lbl_actual_oct.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //nov
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_nov.Text)
                        {

                            lbl_target_nov.Text = _linq_corporatekpi[_count].target.ToString();
                            lbl_actual_nov.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //dec
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_dec.Text)
                        {

                            lbl_target_dec.Text = _linq_corporatekpi[_count].target.ToString();
                            lbl_actual_dec.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }


                    }


                }
                break;

            case "GvInsertCorporateKpi":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    IFormatProvider culture_search = new CultureInfo("en-US", true);
                    //string Month_current = DateTime.Now.ToString("yyyy MM", culture_search); //Date Time Today Incheck

                    //string Month_current = DateTime.Now.ToString("MM", culture_search);
                    string prevMonth = DateTime.Now.AddMonths(-1).ToString("MM");
                    string varDay = DateTime.Now.AddDays(-1).ToString("DD");

                    //litDebug.Text = prevMonth.ToString();

                    #region set month value

                    Label lbl_set_month_jan = (Label)e.Row.FindControl("lbl_set_month_jan");
                    Label lbl_set_month_feb = (Label)e.Row.FindControl("lbl_set_month_feb");
                    Label lbl_set_month_mar = (Label)e.Row.FindControl("lbl_set_month_mar");
                    Label lbl_set_month_apr = (Label)e.Row.FindControl("lbl_set_month_apr");
                    Label lbl_set_month_may = (Label)e.Row.FindControl("lbl_set_month_may");
                    Label lbl_set_month_jun = (Label)e.Row.FindControl("lbl_set_month_jun");
                    Label lbl_set_month_jul = (Label)e.Row.FindControl("lbl_set_month_jul");
                    Label lbl_set_month_aug = (Label)e.Row.FindControl("lbl_set_month_aug");
                    Label lbl_set_month_sep = (Label)e.Row.FindControl("lbl_set_month_sep");
                    Label lbl_set_month_oct = (Label)e.Row.FindControl("lbl_set_month_oct");
                    Label lbl_set_month_nov = (Label)e.Row.FindControl("lbl_set_month_nov");
                    Label lbl_set_month_dec = (Label)e.Row.FindControl("lbl_set_month_dec");


                    lbl_set_month_jan.Text = "1";
                    lbl_set_month_feb.Text = "2";
                    lbl_set_month_mar.Text = "3";
                    lbl_set_month_apr.Text = "4";
                    lbl_set_month_may.Text = "5";
                    lbl_set_month_jun.Text = "6";
                    lbl_set_month_jul.Text = "7";
                    lbl_set_month_aug.Text = "8";
                    lbl_set_month_sep.Text = "9";
                    lbl_set_month_oct.Text = "10";
                    lbl_set_month_nov.Text = "11";
                    lbl_set_month_dec.Text = "12";

                    #endregion set month value

                    #region set u2idx
                    Label lbl_u2_idx_jan = (Label)e.Row.FindControl("lbl_u2_idx_jan");
                    Label lbl_u2_idx_feb = (Label)e.Row.FindControl("lbl_u2_idx_feb");
                    Label lbl_u2_idx_mar = (Label)e.Row.FindControl("lbl_u2_idx_mar");
                    Label lbl_u2_idx_apr = (Label)e.Row.FindControl("lbl_u2_idx_apr");
                    Label lbl_u2_idx_may = (Label)e.Row.FindControl("lbl_u2_idx_may");
                    Label lbl_u2_idx_jun = (Label)e.Row.FindControl("lbl_u2_idx_jun");
                    Label lbl_u2_idx_jul = (Label)e.Row.FindControl("lbl_u2_idx_jul");
                    Label lbl_u2_idx_aug = (Label)e.Row.FindControl("lbl_u2_idx_aug");
                    Label lbl_u2_idx_sep = (Label)e.Row.FindControl("lbl_u2_idx_sep");
                    Label lbl_u2_idx_oct = (Label)e.Row.FindControl("lbl_u2_idx_oct");
                    Label lbl_u2_idx_nov = (Label)e.Row.FindControl("lbl_u2_idx_nov");
                    Label lbl_u2_idx_dec = (Label)e.Row.FindControl("lbl_u2_idx_dec");



                    #endregion set u2idx

                    Label lbl_u0_detail_idx = (Label)e.Row.FindControl("lbl_u0_detail_idx");

                    #region value insert or update kpi

                    TextBox txt_jan_target = (TextBox)e.Row.FindControl("txt_jan_target");
                    TextBox txt_jan_actual = (TextBox)e.Row.FindControl("txt_jan_actual");

                    TextBox txt_feb_target = (TextBox)e.Row.FindControl("txt_feb_target");
                    TextBox txt_feb_actual = (TextBox)e.Row.FindControl("txt_feb_actual");

                    TextBox txt_mar_target = (TextBox)e.Row.FindControl("txt_mar_target");
                    TextBox txt_mar_actual = (TextBox)e.Row.FindControl("txt_mar_actual");

                    TextBox txt_apr_target = (TextBox)e.Row.FindControl("txt_apr_target");
                    TextBox txt_apr_actual = (TextBox)e.Row.FindControl("txt_apr_actual");

                    TextBox txt_may_target = (TextBox)e.Row.FindControl("txt_may_target");
                    TextBox txt_may_actual = (TextBox)e.Row.FindControl("txt_may_actual");

                    TextBox txt_jun_target = (TextBox)e.Row.FindControl("txt_jun_target");
                    TextBox txt_jun_actual = (TextBox)e.Row.FindControl("txt_jun_actual");

                    TextBox txt_jul_target = (TextBox)e.Row.FindControl("txt_jul_target");
                    TextBox txt_jul_actual = (TextBox)e.Row.FindControl("txt_jul_actual");

                    TextBox txt_aug_target = (TextBox)e.Row.FindControl("txt_aug_target");
                    TextBox txt_aug_actual = (TextBox)e.Row.FindControl("txt_aug_actual");

                    TextBox txt_sep_target = (TextBox)e.Row.FindControl("txt_sep_target");
                    TextBox txt_sep_actual = (TextBox)e.Row.FindControl("txt_sep_actual");

                    TextBox txt_oct_target = (TextBox)e.Row.FindControl("txt_oct_target");
                    TextBox txt_oct_actual = (TextBox)e.Row.FindControl("txt_oct_actual");

                    TextBox txt_nov_target = (TextBox)e.Row.FindControl("txt_nov_target");
                    TextBox txt_nov_actual = (TextBox)e.Row.FindControl("txt_nov_actual");

                    TextBox txt_dec_target = (TextBox)e.Row.FindControl("txt_dec_target");
                    TextBox txt_dec_actual = (TextBox)e.Row.FindControl("txt_dec_actual");


                    #endregion value insert or update kpi

                    data_kpi data_kpi_details_u2 = new data_kpi();

                    kpi_search_template_detail kpi_search_template = new kpi_search_template_detail();
                    data_kpi_details_u2.kpi_search_template_list = new kpi_search_template_detail[1];
                    kpi_u2_detail kpi_details_u2 = new kpi_u2_detail();
                    data_kpi_details_u2.kpi_details_u2_list = new kpi_u2_detail[1];

                    kpi_search_template.s_condition = "1";
                    kpi_search_template.s_u1_detail_idx = "0";
                    kpi_search_template.s_cemp_idx = _emp_idx.ToString();

                    data_kpi_details_u2.kpi_search_template_list[0] = kpi_search_template;
                    data_kpi_details_u2.kpi_details_u2_list[0] = kpi_details_u2;

                    data_kpi_details_u2 = callServicePostCorporateKpi(_urlGetCorporateKpi, data_kpi_details_u2);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_kpi_details_u2));

                    ViewState["Vs_DetailValueCorporateKpi"] = data_kpi_details_u2.kpi_details_u2_list;

                    kpi_u2_detail[] _item_value_corporatekpi = (kpi_u2_detail[])ViewState["Vs_DetailValueCorporateKpi"];

                    var _linq_corporatekpi = (from data in _item_value_corporatekpi
                                              where
                                              data.u0_detail_idx == int.Parse(lbl_u0_detail_idx.Text)

                                              select data

                                        ).ToList();
                    //check column month
                    switch (prevMonth)
                    {

                        case "01":
                            GvInsertCorporateKpi.Columns[6].Visible = true;
                            GvInsertCorporateKpi.Columns[7].Visible = true;
                            break;
                        case "02":
                            GvInsertCorporateKpi.Columns[7].Visible = true;
                            GvInsertCorporateKpi.Columns[8].Visible = true;
                            break;
                        case "03":
                            GvInsertCorporateKpi.Columns[8].Visible = true;
                            GvInsertCorporateKpi.Columns[9].Visible = true;
                            break;
                        case "04":
                            GvInsertCorporateKpi.Columns[9].Visible = true;
                            GvInsertCorporateKpi.Columns[10].Visible = true;
                            break;
                        case "05":
                            GvInsertCorporateKpi.Columns[10].Visible = true;
                            GvInsertCorporateKpi.Columns[11].Visible = true;
                            break;
                        case "06":
                            GvInsertCorporateKpi.Columns[11].Visible = true;
                            GvInsertCorporateKpi.Columns[12].Visible = true;
                            break;
                        case "07":

                            GvInsertCorporateKpi.Columns[12].Visible = true;
                            GvInsertCorporateKpi.Columns[13].Visible = true;
                            break;
                        case "08":
                            GvInsertCorporateKpi.Columns[13].Visible = true;
                            GvInsertCorporateKpi.Columns[14].Visible = true;
                            break;
                        case "09":
                            GvInsertCorporateKpi.Columns[14].Visible = true;
                            GvInsertCorporateKpi.Columns[15].Visible = true;
                            break;
                        case "10":
                            GvInsertCorporateKpi.Columns[15].Visible = true;
                            GvInsertCorporateKpi.Columns[16].Visible = true;
                            break;
                        case "11":
                            GvInsertCorporateKpi.Columns[16].Visible = true;
                            GvInsertCorporateKpi.Columns[17].Visible = true;
                            break;
                        case "12":
                            GvInsertCorporateKpi.Columns[17].Visible = true;
                            GvInsertCorporateKpi.Columns[6].Visible = true;
                            break;

                    }
                    //check column month

                    for (int _count = 0; _count < _linq_corporatekpi.Count(); _count++)
                    {
                        //jan
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_jan.Text)
                        {
                            lbl_u2_idx_jan.Text = _linq_corporatekpi[_count].u2_detail_idx.ToString();
                            txt_jan_target.Text = _linq_corporatekpi[_count].target.ToString();
                            txt_jan_actual.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //feb
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_feb.Text)
                        {
                            lbl_u2_idx_feb.Text = _linq_corporatekpi[_count].u2_detail_idx.ToString();
                            txt_feb_target.Text = _linq_corporatekpi[_count].target.ToString();
                            txt_feb_actual.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //mar
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_mar.Text)
                        {
                            lbl_u2_idx_mar.Text = _linq_corporatekpi[_count].u2_detail_idx.ToString();
                            txt_mar_target.Text = _linq_corporatekpi[_count].target.ToString();
                            txt_mar_actual.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //apr
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_apr.Text)
                        {
                            lbl_u2_idx_apr.Text = _linq_corporatekpi[_count].u2_detail_idx.ToString();
                            txt_apr_target.Text = _linq_corporatekpi[_count].target.ToString();
                            txt_apr_actual.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //may
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_may.Text)
                        {
                            lbl_u2_idx_may.Text = _linq_corporatekpi[_count].u2_detail_idx.ToString();
                            txt_may_target.Text = _linq_corporatekpi[_count].target.ToString();
                            txt_may_actual.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //jun
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_jun.Text)
                        {
                            lbl_u2_idx_jun.Text = _linq_corporatekpi[_count].u2_detail_idx.ToString();
                            txt_jun_target.Text = _linq_corporatekpi[_count].target.ToString();
                            txt_jun_actual.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //jul
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_jul.Text)
                        {
                            lbl_u2_idx_jul.Text = _linq_corporatekpi[_count].u2_detail_idx.ToString();
                            txt_jul_target.Text = _linq_corporatekpi[_count].target.ToString();
                            txt_jul_actual.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //aug
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_aug.Text)
                        {
                            lbl_u2_idx_aug.Text = _linq_corporatekpi[_count].u2_detail_idx.ToString();
                            txt_aug_target.Text = _linq_corporatekpi[_count].target.ToString();
                            txt_aug_actual.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //sep
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_sep.Text)
                        {
                            lbl_u2_idx_sep.Text = _linq_corporatekpi[_count].u2_detail_idx.ToString();
                            txt_sep_target.Text = _linq_corporatekpi[_count].target.ToString();
                            txt_sep_actual.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //oct
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_oct.Text)
                        {
                            lbl_u2_idx_oct.Text = _linq_corporatekpi[_count].u2_detail_idx.ToString();
                            txt_oct_target.Text = _linq_corporatekpi[_count].target.ToString();
                            txt_oct_actual.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //nov
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_nov.Text)
                        {
                            lbl_u2_idx_nov.Text = _linq_corporatekpi[_count].u2_detail_idx.ToString();
                            txt_nov_target.Text = _linq_corporatekpi[_count].target.ToString();
                            txt_nov_actual.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }

                        //dec
                        if (_linq_corporatekpi[_count].u0_detail_idx.ToString() == lbl_u0_detail_idx.Text && _linq_corporatekpi[_count].month_idx.ToString() == lbl_set_month_dec.Text)
                        {
                            lbl_u2_idx_dec.Text = _linq_corporatekpi[_count].u2_detail_idx.ToString();
                            txt_dec_target.Text = _linq_corporatekpi[_count].target.ToString();
                            txt_dec_actual.Text = _linq_corporatekpi[_count].actual.ToString();
                            //litDebug.Text += _linq_corporatekpi[_count].month_idx.ToString() + "|";//+ "|" + _linq_corporatekpi[_count].target.ToString();
                        }


                    }





                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {


                }

                break;


        }
    }

    protected void getHourToOT(DropDownList ddlNane, double _hour)
    {
        ddlNane.AppendDataBoundItems = true;
        ddlNane.Items.Clear();



        if (Convert.ToDouble(_hour) >= 0.5)
        {
            double _timescan = Convert.ToDouble(_hour);
            for (double i = 0.0; i <= _timescan; i += 0.5)
            {
                ddlNane.Items.Add((i).ToString());
            }

            ddlNane.Items.FindByValue(Convert.ToString(_timescan)).Selected = true;

        }
        else
        {
            ddlNane.Visible = false;
            ddlNane.SelectedValue = "0";
        }
    }

    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvViewDetailReport":
                //for (int rowIndex = GvViewDetailReport.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                //{
                //    GridViewRow currentRow = GvViewDetailReport.Rows[rowIndex];
                //    GridViewRow previousRow = GvViewDetailReport.Rows[rowIndex + 1];

                //    if (((Label)currentRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text == ((Label)previousRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text)
                //    {
                //        if (previousRow.Cells[0].RowSpan < 2)
                //        {
                //            currentRow.Cells[0].RowSpan = 2;

                //        }
                //        else
                //        {
                //            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                //        }
                //        previousRow.Cells[0].Visible = false;
                //    }


                //    if (((Label)currentRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text == ((Label)previousRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text)
                //    {
                //        if (previousRow.Cells[1].RowSpan < 2)
                //        {
                //            currentRow.Cells[1].RowSpan = 2;

                //        }
                //        else
                //        {
                //            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                //        }
                //        previousRow.Cells[1].Visible = false;
                //    }


                //}
                break;
        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;

            switch (cmdName)
            {
                case "cmdRemoveCar":
                    //GridView gvAddCarBooking = (GridView)FvHrApprove.FindControl("gvAddCarBooking");
                    //GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    //int rowIndex = rowSelect.RowIndex;
                    //DataSet dsContacts = (DataSet)ViewState["vsCarDetailList"];
                    //dsContacts.Tables["dsCarDetailTable"].Rows[rowIndex].Delete();
                    //dsContacts.AcceptChanges();
                    //setGridData(gvAddCarBooking, dsContacts.Tables["dsCarDetailTable"]);
                    //if (dsContacts.Tables["dsCarDetailTable"].Rows.Count < 1)
                    //{
                    //    gvAddCarBooking.Visible = false;
                    //}
                    break;

            }
        }
    }

    #endregion gridview

    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {
                case "FvWaitApproveDetail":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {

                    }

                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {


                    }
                    break;

            }
        }
    }
    #endregion Formview Databind

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {

        setActiveTab("docCorparateKPIs", 0, 0, 0, 0, 0, 0, 0);

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_kpi callServicePostCorporateKpi(string _cmdUrl, data_kpi _data_kpi)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_kpi);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_kpi = (data_kpi)_funcTool.convertJsonToObject(typeof(data_kpi), _localJson);

        return _data_kpi;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx, int u1idx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
        setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);

        //set tab
        Pn_DetailInsertCorporateKpi.Visible = false;

        //set tab active insert corporate kpi
        liInsertCorporateKPI.Attributes.Add("class", "");
        //liInsertTQA.Attributes.Add("class", "");


        switch (activeTab)
        {
            case "docCorparateKPIs":

                getDetailCorporateKpi(GvDetailCorporateKpi, uidx);
                getDetailValueCorporateKpi(0, 0);

                switch (_chk_tab)
                {
                    case 1: //insert corporate kpi

                        Pn_DetailInsertCorporateKpi.Visible = true;

                        liInsertCorporateKPI.Attributes.Add("class", "active");
                        setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                        //getDetailCorporateKpi(GvInsertCorporateKpi, uidx);
                        getDetailInsertCorporateKpi(GvInsertCorporateKpi, uidx);
                        //getHistoryEmployee(emp_idx, fvEmployeeProfile);

                        break;
                    //case 2: //detail

                    //    break;
                }

                //setOntop.Focus();
                break;
            case "docDetailKPIs":

                getDetailTQA(GvDetailTQA, uidx);



                break;

        }
    }

    protected void setActiveTab(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cemp_idx, int u1idx)
    {
        setActiveView(activeTab, uidx, nodeidx, staidx, actor_idx, _chk_tab, _cemp_idx, u1idx);
        switch (activeTab)
        {
            case "docCorparateKPIs":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");

                break;
            case "docDetailKPIs":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
               

                break;

        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {

            case "rptBindbtnApprove":
                //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                //{
                //    //Repeater rptBindbtnApprove = (Repeater)FvHeadUserApprove.FindControl("rptBindbtnApprove");
                //    var chk_coler = (Label)e.Item.FindControl("lbcheck_coler_approve");
                //    var btnApprove = (LinkButton)e.Item.FindControl("btnApprove");

                //    for (int k = 0; k <= rptBindbtnApprove.Items.Count; k++)
                //    {
                //        btnApprove.CssClass = ConfigureColors(k);
                //        //Console.WriteLine(i);
                //    }


                //}
                break;

        }
    }

    #endregion reuse

    #region ConfigureColors
    protected string ConfigureColors(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }

    protected string ConfigureColorsOTDay(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }

    protected string ConfigureColorsOTDayHR(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }
    #endregion ConfigureColors

    #region data excel

    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }

        //set merge cell
        ////int max_row = dt.Rows.Count;


        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);

            //set merge cell
            ////var cra = new NPOI.SS.Util.CellRangeAddress(1, max_row, 0, 0);
            ////sheet1.AddMergedRegion(cra);

            //var cra1 = new NPOI.SS.Util.CellRangeAddress(1, max_row, 1, 1);
            //sheet1.AddMergedRegion(cra1);


            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }

    #endregion data excel


}