﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="labis_print.aspx.cs" Inherits="websystem_qa_labis_print" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

    <title>Print Sample Code</title>

    <style type="text/css" media="print,screen">
        @page {
            size: A4 landscape;
            margin: 25px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

        .formPrint {
            margin: 10pt; /*from 5 to 10*/
            padding: 10pt; /*add*/
            width: 842pt;
        }
    </style>

</head>
<body onload="window.print()">

    <form id="form1" runat="server" width="100%">
        <script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>
        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <div class="formPrint">
            <div class="headOrg" style="text-align: center; padding: 10px;">
                <strong>Sample list</strong>
            </div>
            <asp:GridView ID="gvSample" runat="server" AutoGenerateColumns="False"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12" Width="100%"
                OnRowDataBound="gvRowDataBound"
                DataKeyNames="u1_qalab_idx">
                <HeaderStyle CssClass="info" Height="30px" Font-Size="Small" />
                <RowStyle Font-Size="Small" />
                <Columns>
                    <asp:TemplateField HeaderText="QR Code" HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="center">
                        <ItemTemplate>
                            <div style="text-align: center">
                                <asp:Image ID="ImagShowQRCode" Width="70" Height="70" runat="server"></asp:Image>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Bar Code" HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="center">
                        <ItemTemplate>
                            <div style="text-align: center">
                                <asp:Image ID="ImagShowBarCode" Width="100" Height="50" runat="server"></asp:Image>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Sample Code" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <div style="padding: 10px;">
                                <asp:Label ID="lblu0IDX_Sample" runat="server" Visible="false" Text='<%# Eval("u0_qalab_idx") %>'></asp:Label>
                                <asp:Label ID="lblDocIDX_Sample" runat="server" Visible="false" Text='<%# Eval("u1_qalab_idx") %>'></asp:Label>
                                <asp:Label ID="lblSampleCode" runat="server" CssClass="col-sm-12" Text='<%# Eval("sample_code") %>'></asp:Label>
                                <asp:Label ID="lblCount_All" runat="server" Visible="false" Text='<%# Eval("Count_All") %>'></asp:Label>
                                <asp:Label ID="lblCount_Success" runat="server" Visible="false" Text='<%# Eval("Count_Success") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tested Set" Visible="false" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <div style="text-align: center">
                                <asp:Label ID="lblsetTestedItems" runat="server" Visible="true" Text='<%# Eval("set_test_name") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tested Items" HeaderStyle-Width="20%">
                        <ItemTemplate>

                            <div style="padding: 15px;">
                                <table class="table table-striped f-s-12" style="width: 100%">
                                    <asp:Repeater ID="rp_test_sample" runat="server">

                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbltestdetailname" runat="server" Text='<%# " - " + Eval("test_detail_name") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="u2_idx_n3" runat="server" visible="false">
                                                <td>
                                                    <asp:Label ID="lblu2qalab_idx" runat="server" Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="test_detail_idx_n11" runat="server" visible="false">
                                                <td>
                                                    <asp:Label ID="lbltest_detail_idx_n11" runat="server" Text='<%# Eval("test_detail_idx") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>

                                    </asp:Repeater>
                                </table>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tested Set" HeaderStyle-Width="1%">
                        <ItemTemplate>
                            <div style="text-align: center">
                                <asp:Label ID="lbtestedSet" runat="server" Text='<%# Eval("set_test_name") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="status" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="center">
                        <ItemTemplate>
                            <asp:Label ID="lblstatusname" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
            <br />
            <br />
            <%-- <div class="headPrint">--%>
            <div class="pull-right" style="text-align: right;">
                PRINT DATE :
                    <asp:Label ID="lblPrintDate" runat="server" />
            </div>
            <%-- </div>--%>
            <div class="page-break" id="pb2" runat="server" visible="false"></div>
        </div>

    </form>
</body>
</html>
