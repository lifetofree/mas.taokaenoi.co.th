﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("datama_online")]
public class datama_online
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }

    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }

    [XmlElement("ReturnU0Code")]
    public string ReturnU0Code { get; set; }

    [XmlElement("ReturnORGTH")]
    public string ReturnORGTH { get; set; }

    [XmlElement("ReturnDepTH")]
    public string ReturnDepTH { get; set; }

    [XmlElement("ReturnSecTH")]
    public string ReturnSecTH { get; set; }

    [XmlElement("ReturnEmpName")]
    public string ReturnEmpName { get; set; }

    [XmlElement("ReturnAdmin")]
    public string ReturnAdmin { get; set; }

    [XmlElement("ReturnStatus")]
    public string ReturnStatus { get; set; }

    [XmlElement("ReturnCreateDate")]
    public string ReturnCreateDate { get; set; }

    [XmlElement("ReturnEmail")]
    public string ReturnEmail { get; set; }


    [XmlElement("BoxM0_MaList")]
    public M0_maList[] BoxM0_MaList { get; set; }

    [XmlElement("BoxMa_Online_List")]
    public Ma_Online_List[] BoxMa_Online_List { get; set; }

    [XmlElement("BoxMa1_Online_List")]
    public Ma1_Online_List[] BoxMa1_Online_List { get; set; }

    [XmlElement("BoxMa2_Online_List")]
    public Ma2_Online_List[] BoxMa2_Online_List { get; set; }

    [XmlElement("Export_BoxMa_Online_List")]
    public Export_Ma_Online_List[] Export_BoxMa_Online_List { get; set; }


}

public class M0_maList
{
    [XmlElement("m0idx")]
    public int m0idx { get; set; }

    [XmlElement("typeidx")]
    public int typeidx { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("m0status")]
    public int m0status { get; set; }

    [XmlElement("ma_name")]
    public string ma_name { get; set; }

    [XmlElement("m0_tdidx")]
    public int m0_tdidx { get; set; }

    [XmlElement("detail")]
    public string detail { get; set; }

}

public class Ma_Online_List
{
    //ViewDevices
    [XmlElement("m0_tdidx")]
    public int m0_tdidx { get; set; }

    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }

    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("Search")]
    public string Search { get; set; }

    [XmlElement("name_m0_typedevice")]
    public string name_m0_typedevice { get; set; }

    [XmlElement("EmpCode")]
    public string EmpCode { get; set; }

    [XmlElement("Email")]
    public string Email { get; set; }

    [XmlElement("MobileNo")]
    public string MobileNo { get; set; }

    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }


    [XmlElement("u0_acc")]
    public string u0_acc { get; set; }

    [XmlElement("EmpName")]
    public string EmpName { get; set; }

    [XmlElement("ma_name")]
    public string ma_name { get; set; }


    //U0_Document

    [XmlElement("u0idx")]
    public int u0idx { get; set; }

    [XmlElement("deviceidx")]
    public int deviceidx { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }


    [XmlElement("NOrgIDX")]
    public int NOrgIDX { get; set; }

    [XmlElement("NRDeptIDX")]
    public int NRDeptIDX { get; set; }

    [XmlElement("NRSecIDX")]
    public int NRSecIDX { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("doc_status")]
    public int doc_status { get; set; }

    [XmlElement("orgidx")]
    public int orgidx { get; set; }

    [XmlElement("DocCode")]
    public string DocCode { get; set; }

    [XmlElement("comment_it")]
    public string comment_it { get; set; }

    [XmlElement("RdeptIDX")]
    public int RdeptIDX { get; set; }

    [XmlElement("RsecIDX")]
    public int RsecIDX { get; set; }

    [XmlElement("AEmpIDX")]
    public int AEmpIDX { get; set; }

    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("AdminName")]
    public string AdminName { get; set; }

    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("m0idx")]
    public int m0idx { get; set; }

    [XmlElement("comment_user")]
    public string comment_user { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("u2idx")]
    public int u2idx { get; set; }

    [XmlElement("nodidx")]
    public int nodidx { get; set; }

    [XmlElement("node_desc")]
    public string node_desc { get; set; }

    [XmlElement("IFSearchbetween")]
    public int IFSearchbetween { get; set; }

    [XmlElement("EndDate")]
    public string EndDate { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("countdept")]
    public int countdept { get; set; }


}

public class Ma1_Online_List
{
    [XmlElement("m0idx")]
    public int m0idx { get; set; }

    [XmlElement("u0idx")]
    public int u0idx { get; set; }


    [XmlElement("u1idx")]
    public int u1idx { get; set; }

    [XmlElement("u1status")]
    public int u1status { get; set; }


}

public class Ma2_Online_List
{
    [XmlElement("u2idx")]
    public int u2idx { get; set; }

    [XmlElement("approveidx")]
    public int approveidx { get; set; }

    [XmlElement("u2status")]
    public int u2status { get; set; }

    [XmlElement("comment_user")]
    public string comment_user { get; set; }
}

public class Export_Ma_Online_List
{

    [XmlElement("รหัสเอกสาร")]
    public string รหัสเอกสาร { get; set; }

    [XmlElement("เลขทะเบียนอุปกรณ์")]
    public string เลขทะเบียนอุปกรณ์ { get; set; }

    [XmlElement("วันที่สร้าง")]
    public string วันที่สร้าง { get; set; }

    [XmlElement("ผู้สร้าง")]
    public string ผู้สร้าง { get; set; }

    [XmlElement("ความคิดเห็นเจ้าหน้าที่")]
    public string ความคิดเห็นเจ้าหน้าที่ { get; set; }

    [XmlElement("บริษัทผู้ถือครอง")]
    public string บริษัทผู้ถือครอง { get; set; }

    [XmlElement("ฝ่ายผู้ถือครอง")]
    public string ฝ่ายผู้ถือครอง { get; set; }

    [XmlElement("แผนกผู้ถือครอง")]
    public string แผนกผู้ถือครอง { get; set; }

    [XmlElement("ผู้ถือครอง")]
    public string ผู้ถือครอง { get; set; }


}