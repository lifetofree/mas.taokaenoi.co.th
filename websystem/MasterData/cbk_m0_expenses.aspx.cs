﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_cbk_m0_expenses : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_carbooking _data_carbooking = new data_carbooking();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- carbooking --//
    static string _urlSetCbkm0Place = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkm0Place"];
    static string _urlGetCbkm0Place = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0Place"];
    static string _urlSetCbkm0PlaceDel = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkm0PlaceDel"];

    static string _urlGetCbkM0Expenses = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkM0Expenses"];
    static string _urlSetCbkm0Expenses = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkm0Expenses"];
    static string _urlSetCbkm0ExpensesDel = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkm0ExpensesDel"];


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            Select_Expenses();


        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MvSystem.SetActiveView(ViewIndex);
        Select_Expenses();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddExpenses":

                GvDetail.EditIndex = -1;
                Select_Expenses();

                btn_addexpenses.Visible = false;
                setFormData(FvInsert, FormViewMode.Insert, null);
                FvInsert.Visible = true;
                //Gv_select_place.Visible = false;


                break;

            case "CmdSave":
                Insert_Expenses();
                Select_Expenses();
                btn_addexpenses.Visible = true;
                FvInsert.Visible = false;
                break;

            case "cmdCancel":
                btn_addexpenses.Visible = true;
                FvInsert.Visible = false;

                break;

            case "cmdDelete":

                int expenses_idx_del = int.Parse(cmdArg);

                data_carbooking data_m0_expenses_del = new data_carbooking();
                cbk_m0_expenses_detail m0_expenses_del = new cbk_m0_expenses_detail();
                data_m0_expenses_del.cbk_m0_expenses_list = new cbk_m0_expenses_detail[1];

                m0_expenses_del.expenses_idx = expenses_idx_del;
                m0_expenses_del.cemp_idx = _emp_idx;

                data_m0_expenses_del.cbk_m0_expenses_list[0] = m0_expenses_del;

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_expenses_del));
                data_m0_expenses_del = callServicePostCarBooking(_urlSetCbkm0ExpensesDel, data_m0_expenses_del);

                Select_Expenses();
                //Gv_select_unit.Visible = false;
                btn_addexpenses.Visible = true;
                FvInsert.Visible = false;
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void Insert_Expenses()
    {

        TextBox txtexpenses_name = (TextBox)FvInsert.FindControl("txtexpenses_name");
        DropDownList ddexpenses = (DropDownList)FvInsert.FindControl("ddexpenses");

        data_carbooking data_m0_expenses = new data_carbooking();
        cbk_m0_expenses_detail m0_expenses_insert = new cbk_m0_expenses_detail();
        data_m0_expenses.cbk_m0_expenses_list = new cbk_m0_expenses_detail[1];

        m0_expenses_insert.expenses_idx = 0;
        m0_expenses_insert.expenses_name = txtexpenses_name.Text;
        m0_expenses_insert.cemp_idx = _emp_idx;
        m0_expenses_insert.expenses_status = int.Parse(ddexpenses.SelectedValue);

        data_m0_expenses.cbk_m0_expenses_list[0] = m0_expenses_insert;
        data_m0_expenses = callServicePostCarBooking(_urlSetCbkm0Expenses, data_m0_expenses);

        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
        if (data_m0_expenses.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            txtexpenses_name.Text = String.Empty;
            //tex_place_code.Text = String.Empty;
        }
    }

    protected void Select_Expenses()
    {

        data_carbooking data_m0_expenses_detail = new data_carbooking();
        cbk_m0_expenses_detail m0_expenses_detail = new cbk_m0_expenses_detail();
        data_m0_expenses_detail.cbk_m0_expenses_list = new cbk_m0_expenses_detail[1];

        m0_expenses_detail.condition = 0;

        data_m0_expenses_detail.cbk_m0_expenses_list[0] = m0_expenses_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_expenses_detail = callServicePostCarBooking(_urlGetCbkM0Expenses, data_m0_expenses_detail);

        setGridData(GvDetail, data_m0_expenses_detail.cbk_m0_expenses_list);
        //Gv_select_place.DataSource = m0_place_detail.qa_cims_m0_place_list;
        //Gv_select_place.DataBind();
    }
    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvDetail":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_expenses_status = (Label)e.Row.Cells[3].FindControl("lbl_expenses_status");
                    Label expenses_statusOnline = (Label)e.Row.Cells[3].FindControl("expenses_statusOnline");
                    Label expenses_statusOffline = (Label)e.Row.Cells[3].FindControl("expenses_statusOffline");

                    ViewState["vs_expenses_status"] = lbl_expenses_status.Text;


                    if (ViewState["vs_expenses_status"].ToString() == "1")
                    {
                        expenses_statusOnline.Visible = true;
                    }
                    else if (ViewState["vs_expenses_status"].ToString() == "0")
                    {
                        expenses_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    btn_addexpenses.Visible = true;
                    FvInsert.Visible = false;

                }

                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetail":
                GvDetail.EditIndex = e.NewEditIndex;
                Select_Expenses();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvDetail":

                var txt_expenses_idx_edit = (TextBox)GvDetail.Rows[e.RowIndex].FindControl("txt_expenses_idx_edit");
                var txt_expenses_name_edit = (TextBox)GvDetail.Rows[e.RowIndex].FindControl("txt_expenses_name_edit");
                var ddlexpenses_status_edit = (DropDownList)GvDetail.Rows[e.RowIndex].FindControl("ddlexpenses_status_edit");


                GvDetail.EditIndex = -1;


                data_carbooking data_m0_expenses = new data_carbooking();
                cbk_m0_expenses_detail m0_expenses_edit = new cbk_m0_expenses_detail();
                data_m0_expenses.cbk_m0_expenses_list = new cbk_m0_expenses_detail[1];

                m0_expenses_edit.expenses_idx = int.Parse(txt_expenses_idx_edit.Text);
                m0_expenses_edit.expenses_name = txt_expenses_name_edit.Text;
                m0_expenses_edit.cemp_idx = _emp_idx;
                m0_expenses_edit.expenses_status = int.Parse(ddlexpenses_status_edit.SelectedValue);

                data_m0_expenses.cbk_m0_expenses_list[0] = m0_expenses_edit;
                data_m0_expenses = callServicePostCarBooking(_urlSetCbkm0Expenses, data_m0_expenses);


                if (data_m0_expenses.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                    Select_Expenses();
                }
                else
                {
                    Select_Expenses();
                }
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvDetail":
                GvDetail.EditIndex = -1;
                Select_Expenses();
                //btn_addplace.Visible = true;
                //SETFOCUS_ONTOP.Focus();
                //Gv_select_unit.Visible = false;
                btn_addexpenses.Visible = true;
                FvInsert.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvDetail":
                GvDetail.PageIndex = e.NewPageIndex;
                GvDetail.DataBind();
                Select_Expenses();
                break;

        }
    }
    #endregion

    #region callService 

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_carbooking callServicePostCarBooking(string _cmdUrl, data_carbooking _data_carbooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_carbooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_carbooking = (data_carbooking)_funcTool.convertJsonToObject(typeof(data_carbooking), _localJson);


        return _data_carbooking;
    }
    #endregion callService Functions
}