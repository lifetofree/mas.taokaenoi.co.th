﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="chr_m0_iso.aspx.cs" Inherits="websystem_MasterData_chr_m0_iso" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-user"></i><strong>&nbsp; ISO Change Request</strong></h3>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">

                                <asp:LinkButton ID="btnshow" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Add User SAP" runat="server" CommandName="CmdAdd" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add ISO </strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="System" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlsystem" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกระบบ...</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddlsystem" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกระบบ"
                                                    ValidationExpression="กรุณาเลือกระบบ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label2" runat="server" Text="Organization" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlorgidx" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกองค์กร...</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddlorgidx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกองค์กร"
                                                    ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                            </div>



                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="ISO Code" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtcode" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกข้อมูล"
                                                    ValidationExpression="กรุณากรอกข้อมูล"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtcode"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="ISO Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtname" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtname" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกข้อมูล"
                                                    ValidationExpression="กรุณากรอกข้อมูล"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtname"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label4" runat="server" Text="ISO Version" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtver" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtver" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกข้อมูล"
                                                    ValidationExpression="กรุณากรอกข้อมูล"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtver"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label5" runat="server" Text="ISO Result" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtresult" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txtresult" Font-Size="11"
                                                    ErrorMessage="Please enter ISO Result" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationGroup="Save" Display="None"
                                                    ErrorMessage="กรุณาตรวจสอบข้อมูลที่กรอก" Font-Size="11"
                                                    ControlToValidate="txtresult"
                                                    ValidationExpression="^([0-9]{1,2})[./-]+([0-9]{1,2})[./-]+([0-9]{2}|[0-9]{2})$" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="lbl" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="2" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                </div>
                            </asp:Panel>


                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="ISOIDX"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblstidx" runat="server" Visible="false" Text='<%# Eval("ISOIDX") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtISOIDX" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("ISOIDX")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbSysSapIDX1" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทระบบ" />
                                                        <div class="col-sm-4">
                                                            <asp:Label ID="lbSysSapIDX" runat="server" Text='<%# Bind("Sysidx") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlSysSapIDX" AutoPostBack="true" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValid22ator1" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="ddlSysSapIDX" Font-Size="11"
                                                            ErrorMessage="Please check System"
                                                            ValidationExpression="Please check System" InitialValue="00" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValid22ator1" Width="160" />
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbOrganization1" CssClass="col-sm-3 control-label" runat="server" Text="Organization" />
                                                        <div class="col-sm-4">
                                                            <asp:Label ID="lbOrganization" runat="server" Text='<%# Bind("OrgIDX") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlorgidx_edit" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="ddlorgidx_edit" Font-Size="11"
                                                            ErrorMessage="ประเภท Organization"
                                                            ValidationExpression="ประเภท Organization" InitialValue="00" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbDoccode" CssClass="col-sm-3 control-label" runat="server" Text="ISO Code" />
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtDoccodeUpdate" runat="server" CssClass="form-control" MaxLength="50" Text='<%# Eval("Doccode")%>' />
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RqDoccode" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="txtDoccodeUpdate" Font-Size="11"
                                                            ErrorMessage="Please check ISO Document" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqDoccode" Width="160" />
                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbDocname" CssClass="col-sm-3 control-label" runat="server" Text="ISO Name" />
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtDocnameUpdate" runat="server" CssClass="form-control" MaxLength="50" Text='<%# Eval("Docname")%>' />
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RqDocname" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="txtDocnameUpdate" Font-Size="11"
                                                            ErrorMessage="Please check Docname" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqDocname" Width="160" />
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbVersion" CssClass="col-sm-3 control-label" runat="server" Text="ISO Version" />
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtVersionUpdate" runat="server" CssClass="form-control" MaxLength="25" Text='<%# Eval("VersionIso")%>' />
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RqVersion" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="txtVersionUpdate" Font-Size="11"
                                                            ErrorMessage="Please check Version" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqVersion" Width="160" />
                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbResult" CssClass="col-sm-3 control-label" runat="server" Text="ISO Result" />
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtResultUpdate" runat="server" CssClass="form-control" MaxLength="10" Text='<%# Eval("Result")%>' />
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RqResult" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="txtResultUpdate" Font-Size="11"
                                                            ErrorMessage="Please check Result" />
                                                        <asp:RegularExpressionValidator ID="RqResult2" runat="server" ValidationGroup="Save" Display="None"
                                                            ErrorMessage="กรุณาตรวจสอบข้อมูลที่กรอก Ex.01/02/59" Font-Size="11"
                                                            ControlToValidate="txtResultUpdate"
                                                            ValidationExpression="^([0-9]{1,2})[./-]+([0-9]{1,2})[./-]+([0-9]{2}|[0-9]{2})$" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqResult" Width="160" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqResult2" Width="160" />

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="Last Change By" />
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtLastChange" runat="server" CssClass="form-control" Enabled="false" MaxLength="10" Text='<%# Eval("FullNameTH")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbisostatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-2">
                                                            <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("ISO_Status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-4 col-sm-offset-8">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </EditItemTemplate>
                                        <%-- <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtm0idx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("ISOIDX")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="Type Topic" />
                                                        <div class="col-sm-4">
                                                            <asp:Label ID="lbTMIDX" runat="server" Text='<%# Bind("TMIDX") %>' Visible="False" />
                                                            <asp:DropDownList ID="ddlTMIDX_Update" AutoPostBack="true" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldffValid22ator1" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="ddlTMIDX_Update" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกประเภทหัวข้อ"
                                                            ValidationExpression="กรุณาเลือกประเภทหัวข้อ" InitialValue="00" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldffValid22ator1" Width="160" />
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="Type Topic" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtname_edit" runat="server" CssClass="form-control" Text='<%# Eval("topic")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtname_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาใส่สถานะรายการ"
                                                            ValidationExpression="กรุณาใส่สถานะรายการ"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtname_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("m0status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>--%>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="System" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("System_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OrgNameTH" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            &nbsp;&nbsp;&nbsp;<asp:Label ID="lblTypeeCode" runat="server" CssClass="col-sm-10" Text='<%# Eval("OrgNameTH") %>'></asp:Label>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Doccode" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            &nbsp;&nbsp;&nbsp;<asp:Label ID="lblDoccode" runat="server" CssClass="col-sm-10" Text='<%# Eval("Doccode") %>'></asp:Label>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Docname" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            &nbsp;&nbsp;&nbsp;<asp:Label ID="lblDocname" runat="server" CssClass="col-sm-10" Text='<%# Eval("Docname") %>'></asp:Label>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Version" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            &nbsp;&nbsp;&nbsp;<asp:Label ID="lblVersionIso" runat="server" CssClass="col-sm-10" Text='<%# Eval("VersionIso") %>'></asp:Label>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Result" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                            &nbsp;&nbsp;&nbsp;<asp:Label ID="lblResult" runat="server" CssClass="col-sm-10" Text='<%# Eval("Result") %>'></asp:Label>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Last Change By" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            &nbsp;&nbsp;&nbsp;<asp:Label ID="lblLastChange" runat="server" CssClass="col-sm-10" Text='<%# Eval("FullNameTH") %>'></asp:Label>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstatus" runat="server" Text='<%# Eval("ISO_StatusDetail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("ISOIDX") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>


                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

