using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

public partial class websystem_hr_hr_checkin_report : System.Web.UI.Page {
    #region initial function/data
    function_tool _funcTool = new function_tool ();

    data_cen_master _data_cen_master = new data_cen_master ();
    data_cen_employee _data_cen_employee = new data_cen_employee ();
    data_tkn_checkin _data_tkn_checkin = new data_tkn_checkin ();

    int _emp_idx = 0;
    int _default_int = 0;
    bool _b_permission = false;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlCenGetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlCenGetCenMasterList"];
    static string _urlHrGetHrCheckin = _serviceUrl + ConfigurationManager.AppSettings["urlHrGetHrCheckin"];
    static string _urlHrSetHrCheckin = _serviceUrl + ConfigurationManager.AppSettings["urlHrSetHrCheckin"];

    static int[] _permission = { 172, 24047, 1413, 36583, 35578, 126, 36461 };
    #endregion initail function/data

    protected void Page_Init (object sender, EventArgs e) {
        _emp_idx = _funcTool.convertToInt (Session["emp_idx"].ToString ());

        // check permission
        foreach (int _pass in _permission) {
            if (_emp_idx == _pass) {
                li1.Visible = true;
                continue;
            }
        }

        // if (!_b_permission) {
        //     Response.Redirect (ResolveUrl ("~/"));
        // }
    }

    protected void Page_Load (object sender, EventArgs e) {
        if (!IsPostBack) {
            initPage ();
        }

        setTrigger ();
    }

    #region event command
    protected void navCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "navList":
                clearViewState ();
                setActiveTab ("viewList", 0);
                break;
            case "navSetting":
                clearViewState ();
                setActiveTab ("viewSetting", 0);
                break;
                //     case "navReport":
                //         clearViewState ();
                //         setActiveTab ("viewReport", 0);
                //         break;
        }
    }

    protected void btnCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "cmdSearch":
                TextBox tbStartDate = (TextBox) fvSearch.FindControl ("tbStartDate");
                TextBox tbEndDate = (TextBox) fvSearch.FindControl ("tbEndDate");
                TextBox tbEmpCode = (TextBox) fvSearch.FindControl ("tbEmpCode");
                TextBox tbEmpName = (TextBox) fvSearch.FindControl ("tbEmpName");
                DropDownList ddlOrganization = (DropDownList) fvSearch.FindControl ("ddlOrganization");
                DropDownList ddlWorkGroup = (DropDownList) fvSearch.FindControl ("ddlWorkGroup");
                DropDownList ddlLineWork = (DropDownList) fvSearch.FindControl ("ddlLineWork");
                DropDownList ddlDepartment = (DropDownList) fvSearch.FindControl ("ddlDepartment");
                DropDownList ddlSection = (DropDownList) fvSearch.FindControl ("ddlSection");

                search_tkn_checkin_detail _checkinSearch = new search_tkn_checkin_detail ();
                _checkinSearch.s_start_date = tbStartDate.Text.Trim ();
                _checkinSearch.s_end_date = tbEndDate.Text.Trim ();
                _checkinSearch.s_emp_code = tbEmpCode.Text.Trim ();
                _checkinSearch.s_emp_name = tbEmpName.Text.Trim ();
                _checkinSearch.s_org_idx = ddlOrganization.SelectedValue;
                _checkinSearch.s_wg_idx = ddlWorkGroup.SelectedValue;
                _checkinSearch.s_lw_idx = ddlLineWork.SelectedValue;
                _checkinSearch.s_dept_idx = ddlDepartment.SelectedValue;
                _checkinSearch.s_sec_idx = ddlSection.SelectedValue;
                _data_tkn_checkin = getCheckinList ("1", _checkinSearch);
                _funcTool.setGvData (gvDataList, _data_tkn_checkin.tkn_checkin_list);
                break;
            case "cmdReset":
                _funcTool.setFvData (fvSearch, FormViewMode.Insert, null);
                setActiveTab ("viewList", 0);
                break;
            case "cmdExport":
                if (ViewState["ReportSearchList"] == null) {
                    ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export');", true);
                    return;
                }

                _data_tkn_checkin = (data_tkn_checkin) ViewState["ReportSearchList"];
                tkn_checkin_detail[] _reportData = (tkn_checkin_detail[]) _data_tkn_checkin.tkn_checkin_list;
                if (_reportData == null) {
                    ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export');", true);
                    return;
                }

                DataTable tableReport = new DataTable ();
                int _count_row = 0;

                tableReport.Columns.Add ("#", typeof (String));
                tableReport.Columns.Add ("รหัสพนักงาน", typeof (String));
                tableReport.Columns.Add ("ชื่อ-นามสกุล", typeof (String));
                tableReport.Columns.Add ("Latitude", typeof (String));
                tableReport.Columns.Add ("Longitude", typeof (String));
                tableReport.Columns.Add ("วันเวลาที่เช็คอิน", typeof (String));
                tableReport.Columns.Add ("สถานที่เช็คอิน", typeof (String));
                tableReport.Columns.Add ("รหัสประเทศ", typeof (String));
                tableReport.Columns.Add ("ประเทศ", typeof (String));
                tableReport.Columns.Add ("รหัสไปรษณีย์", typeof (String));
                tableReport.Columns.Add ("จังหวัด", typeof (String));
                tableReport.Columns.Add ("อำเภอ", typeof (String));
                tableReport.Columns.Add ("ตำบล", typeof (String));
                tableReport.Columns.Add ("องค์กร", typeof (String));
                tableReport.Columns.Add ("กลุ่มงาน", typeof (String));
                tableReport.Columns.Add ("สายงาน", typeof (String));
                tableReport.Columns.Add ("ฝ่าย", typeof (String));
                tableReport.Columns.Add ("แผนก", typeof (String));

                foreach (var temp_row in _reportData) {
                    DataRow add_row = tableReport.NewRow ();

                    add_row[0] = (_count_row + 1).ToString ();
                    add_row[1] = temp_row.emp_code.ToString ();
                    add_row[2] = temp_row.emp_name_th.ToString ();
                    add_row[3] = temp_row.latitude.ToString ();
                    add_row[4] = temp_row.longitude.ToString ();
                    add_row[5] = temp_row.create_date.ToString ();
                    add_row[6] = temp_row.street.ToString ();
                    add_row[7] = temp_row.iso_country_code.ToString ();
                    add_row[8] = temp_row.country.ToString ();
                    add_row[9] = temp_row.postal_code.ToString ();
                    add_row[10] = temp_row.admin_area.ToString ();
                    add_row[11] = temp_row.sub_admin_area.ToString ();
                    add_row[12] = temp_row.locality.ToString ();
                    add_row[13] = temp_row.org_name_th.ToString ();
                    add_row[14] = temp_row.wg_name_th.ToString ();
                    add_row[15] = temp_row.lw_name_th.ToString ();
                    add_row[16] = temp_row.dept_name_th.ToString ();
                    add_row[17] = temp_row.sec_name_th.ToString ();

                    tableReport.Rows.InsertAt (add_row, _count_row++);
                }

                WriteExcelWithNPOI (tableReport, "xls", "checkin-report");
                break;
            case "cmdEdit":
                search_tkn_checkin_detail _settingSearch = new search_tkn_checkin_detail ();
                _settingSearch.s_m0_idx = cmdArg;
                _data_tkn_checkin = getCheckinList ("2", _settingSearch);
                _funcTool.setFvData (fvSettingList, FormViewMode.Edit, _data_tkn_checkin.tkn_checkin_setting_list_m0);
                break;
            case "cmdResetForm":
            case "cmdCancelForm":
                _funcTool.setFvData (fvSettingList, FormViewMode.Insert, null);
                break;
            case "cmdSaveForm":
                tkn_checkin_setting_detail_m0 _saveZone = new tkn_checkin_setting_detail_m0 ();
                _saveZone.m0_idx = _funcTool.convertToInt (cmdArg);
                _saveZone.postal_code = ((TextBox) fvSettingList.FindControl ("tbPostalCode")).Text.Trim ();
                _saveZone.province = ((TextBox) fvSettingList.FindControl ("tbProvince")).Text.Trim ();
                _saveZone.amphure = ((TextBox) fvSettingList.FindControl ("tbAmphure")).Text.Trim ();
                _saveZone.district = ((TextBox) fvSettingList.FindControl ("tbDistrict")).Text.Trim ();
                _saveZone.places = ((TextBox) fvSettingList.FindControl ("tbPlaces")).Text.Trim ();
                _saveZone.status_zone = _funcTool.convertToInt (((DropDownList) fvSettingList.FindControl ("ddlZone")).SelectedValue);
                _saveZone.zone_name = ((DropDownList) fvSettingList.FindControl ("ddlZone")).SelectedItem.Text;

                _data_tkn_checkin.checkin_mode = "2";
                _data_tkn_checkin.tkn_checkin_setting_list_m0 = new tkn_checkin_setting_detail_m0[1];
                _data_tkn_checkin.tkn_checkin_setting_list_m0[0] = _saveZone;

                _data_tkn_checkin = callServicePostCheckin (_urlHrSetHrCheckin, _data_tkn_checkin);
                _funcTool.setFvData (fvSettingList, FormViewMode.Insert, null);
                setActiveTab ("viewSetting", 0);
                break;
        }
    }

    protected void ddlSelectedIndexChanged (object sender, EventArgs e) {
        DropDownList ddlName = (DropDownList) sender;

        DropDownList ddlOrganization = (DropDownList) fvSearch.FindControl ("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList) fvSearch.FindControl ("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList) fvSearch.FindControl ("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList) fvSearch.FindControl ("ddlDepartment");
        DropDownList ddlSection = (DropDownList) fvSearch.FindControl ("ddlSection");

        switch (ddlName.ID) {
            case "ddlOrganization":
                if (_funcTool.convertToInt (ddlName.SelectedValue) > 0) {
                    search_cen_master_detail _orgSelected = new search_cen_master_detail ();
                    _orgSelected.s_org_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList ("4", _orgSelected);
                    _funcTool.setDdlData (ddlWorkGroup, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                } else {
                    ddlWorkGroup.Items.Clear ();
                }

                ddlLineWork.Items.Clear ();
                ddlDepartment.Items.Clear ();
                ddlSection.Items.Clear ();
                ddlWorkGroup.Items.Insert (0, new ListItem ("--- เลือกกลุ่มงาน ---", ""));
                ddlLineWork.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", ""));
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", ""));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                break;
            case "ddlWorkGroup":
                if (_funcTool.convertToInt (ddlName.SelectedValue) > 0) {
                    search_cen_master_detail _wgSelected = new search_cen_master_detail ();
                    _wgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _wgSelected.s_wg_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList ("5", _wgSelected);
                    _funcTool.setDdlData (ddlLineWork, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                } else {
                    ddlLineWork.Items.Clear ();
                }

                ddlDepartment.Items.Clear ();
                ddlSection.Items.Clear ();
                ddlLineWork.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", ""));
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", ""));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                break;
            case "ddlLineWork":
                if (_funcTool.convertToInt (ddlName.SelectedValue) > 0) {
                    search_cen_master_detail _lgSelected = new search_cen_master_detail ();
                    _lgSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _lgSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _lgSelected.s_lw_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList ("6", _lgSelected);
                    _funcTool.setDdlData (ddlDepartment, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                } else {
                    ddlDepartment.Items.Clear ();
                }

                ddlSection.Items.Clear ();
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", ""));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                break;
            case "ddlDepartment":
                if (_funcTool.convertToInt (ddlName.SelectedValue) > 0) {
                    search_cen_master_detail _deptSelected = new search_cen_master_detail ();
                    _deptSelected.s_org_idx = ddlOrganization.SelectedValue;
                    _deptSelected.s_wg_idx = ddlWorkGroup.SelectedValue;
                    _deptSelected.s_lw_idx = ddlLineWork.SelectedValue;
                    _deptSelected.s_dept_idx = ddlName.SelectedValue;
                    _data_cen_master = getMasterList ("7", _deptSelected);
                    _funcTool.setDdlData (ddlSection, _data_cen_master.cen_sec_list_m0, "sec_name_th", "sec_idx");
                } else {
                    ddlSection.Items.Clear ();
                }

                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));
                break;
        }
    }
    #endregion event command

    #region RowDatabound
    protected void gvRowDataBound (object sender, GridViewRowEventArgs e) {
        var gvName = (GridView) sender;
    }
    #endregion RowDatabound

    #region paging
    protected void gvPageIndexChanging (object sender, GridViewPageEventArgs e) {
        GridView gvName = (GridView) sender;
        gvName.PageIndex = e.NewPageIndex;

        // switch (gvName.ID) {
        //     case "gvList":
        //         if (ViewState["EmpSearchList"] == null) {
        //             search_cen_employee_detail _allEmployee = new search_cen_employee_detail ();
        //             _data_cen_employee = getEmployeeList ("99", _allEmployee);
        //             ViewState["EmpSearchList"] = _data_cen_employee;
        //         }

        //         _funcTool.setGvData (gvList, ((data_cen_employee) ViewState["EmpSearchList"]).cen_employee_list_u0);
        //         break;
        // }

        hlSetTotop.Focus ();
    }
    #endregion paging

    #region reuse
    protected void initPage () {
        clearSession ();
        clearViewState ();

        setActiveTab ("viewList", 0);

        hlSetTotop.Focus ();
    }

    protected void clearSession () {
        ViewState["ReportSearchList"] = null;
    }

    protected void clearViewState () {
        ViewState["EmpSearchList"] = null;
        ViewState["EmpAllList"] = null;
        ViewState["EmpDetail"] = null;
    }

    protected void setActiveTab (string activeTab, int doc_idx) {
        setActiveView (activeTab, doc_idx);
        setActiveTabBar (activeTab);

        switch (activeTab) {
            case "viewList":
                // get organization
                DropDownList ddlOrganization = (DropDownList) fvSearch.FindControl ("ddlOrganization");
                DropDownList ddlWorkGroup = (DropDownList) fvSearch.FindControl ("ddlWorkGroup");
                DropDownList ddlLineWork = (DropDownList) fvSearch.FindControl ("ddlLineWork");
                DropDownList ddlDepartment = (DropDownList) fvSearch.FindControl ("ddlDepartment");
                DropDownList ddlSection = (DropDownList) fvSearch.FindControl ("ddlSection");

                _data_cen_master = getMasterList ("3", null);
                ddlOrganization.Items.Clear ();
                ddlWorkGroup.Items.Clear ();
                ddlLineWork.Items.Clear ();
                ddlDepartment.Items.Clear ();
                ddlSection.Items.Clear ();
                _funcTool.setDdlData (ddlOrganization, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                ddlOrganization.Items.Insert (0, new ListItem ("--- เลือกองค์กร ---", ""));
                ddlWorkGroup.Items.Insert (0, new ListItem ("--- เลือกกลุ่มงาน ---", ""));
                ddlLineWork.Items.Insert (0, new ListItem ("--- เลือกสายงาน ---", ""));
                ddlDepartment.Items.Insert (0, new ListItem ("--- เลือกฝ่าย ---", ""));
                ddlSection.Items.Insert (0, new ListItem ("--- เลือกแผนก ---", ""));

                _funcTool.setGvData (gvDataList, null);
                break;
            case "viewSetting":
                search_tkn_checkin_detail _settingSearch = new search_tkn_checkin_detail ();
                _settingSearch.s_m0_idx = "";
                _data_tkn_checkin = getCheckinList ("2", _settingSearch);
                _funcTool.setGvData (gvSettingList, _data_tkn_checkin.tkn_checkin_setting_list_m0);
                break;
        }

        hlSetTotop.Focus ();
    }

    protected void setActiveView (string activeTab, int doc_idx) {
        mvSystem.SetActiveView ((View) mvSystem.FindControl (activeTab));
    }

    protected void setActiveTabBar (string activeTab) {
        li0.Attributes.Add ("class", "");
        li1.Attributes.Add ("class", "");
        switch (activeTab) {
            case "viewList":
                li0.Attributes.Add ("class", "active");
                break;
            case "viewSetting":
                li1.Attributes.Add ("class", "active");
                break;
        }
    }

    protected void linkBtnTrigger (LinkButton linkBtnID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger ();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add (triggerLinkBtn);
    }

    protected void ddlTrigger (DropDownList ddlID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerDdl = new PostBackTrigger ();
        triggerDdl.ControlID = ddlID.UniqueID;
        updatePanel.Triggers.Add (triggerDdl);
    }

    protected void gvTrigger (GridView gvID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerGv = new PostBackTrigger ();
        triggerGv.ControlID = gvID.UniqueID;
        updatePanel.Triggers.Add (triggerGv);
    }

    protected void fvTrigger (FormView fvID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerFv = new PostBackTrigger ();
        triggerFv.ControlID = fvID.UniqueID;
        updatePanel.Triggers.Add (triggerFv);
    }

    protected void setTrigger () {
        // nav trigger
        linkBtnTrigger (lbList);
        linkBtnTrigger (lbSetting);

        DropDownList ddlOrganization = (DropDownList) fvSearch.FindControl ("ddlOrganization");
        DropDownList ddlWorkGroup = (DropDownList) fvSearch.FindControl ("ddlWorkGroup");
        DropDownList ddlLineWork = (DropDownList) fvSearch.FindControl ("ddlLineWork");
        DropDownList ddlDepartment = (DropDownList) fvSearch.FindControl ("ddlDepartment");
        DropDownList ddlSection = (DropDownList) fvSearch.FindControl ("ddlSection");
        ddlTrigger (ddlOrganization);
        ddlTrigger (ddlWorkGroup);
        ddlTrigger (ddlLineWork);
        ddlTrigger (ddlDepartment);
        ddlTrigger (ddlSection);

        LinkButton lbSearchData = (LinkButton) fvSearch.FindControl ("lbSearchData");
        LinkButton lbResetData = (LinkButton) fvSearch.FindControl ("lbResetData");
        LinkButton lbExportData = (LinkButton) fvSearch.FindControl ("lbExportData");
        linkBtnTrigger (lbSearchData);
        linkBtnTrigger (lbResetData);
        linkBtnTrigger (lbExportData);

        try { gvTrigger (gvSettingList); } catch { }
        try { fvTrigger (fvSettingList); } catch { }
    }

    protected data_cen_master getMasterList (string _masterMode, search_cen_master_detail _searchData) {
        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.master_mode = _masterMode;
        _data_cen_master.search_cen_master_list[0] = _searchData;
        // litDebug.Text = _funcTool.convertObjectToJson(_data_cen_master);

        _data_cen_master = callServicePostCenMaster (_urlCenGetCenMasterList, _data_cen_master);
        return _data_cen_master;
    }

    protected data_tkn_checkin getCheckinList (string _checkinMode, search_tkn_checkin_detail _searchData) {
        _data_tkn_checkin.search_tkn_checkin_list = new search_tkn_checkin_detail[1];
        _data_tkn_checkin.checkin_mode = _checkinMode;
        _data_tkn_checkin.search_tkn_checkin_list[0] = _searchData;
        // litDebug.Text = _funcTool.convertObjectToJson(_data_cen_master);

        _data_tkn_checkin = callServicePostCheckin (_urlHrGetHrCheckin, _data_tkn_checkin);
        ViewState["ReportSearchList"] = _data_tkn_checkin;
        return _data_tkn_checkin;
    }

    protected data_cen_master callServicePostCenMaster (string _cmdUrl, data_cen_master _data_cen_master) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_cen_master);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);

        // convert json to object
        _data_cen_master = (data_cen_master) _funcTool.convertJsonToObject (typeof (data_cen_master), _local_json);

        return _data_cen_master;
    }

    protected data_tkn_checkin callServicePostCheckin (string _cmdUrl, data_tkn_checkin _data_tkn_checkin) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_tkn_checkin);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);
        // litDebug.Text = _local_json;

        // convert json to object
        _data_tkn_checkin = (data_tkn_checkin) _funcTool.convertJsonToObject (typeof (data_tkn_checkin), _local_json);

        return _data_tkn_checkin;
    }

    protected void WriteExcelWithNPOI (DataTable dt, String extension, String fileName) {
        IWorkbook workbook;
        if (extension == "xlsx") {
            workbook = new XSSFWorkbook ();
        } else if (extension == "xls") {
            workbook = new HSSFWorkbook ();
        } else {
            throw new Exception ("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet ("Sheet 1");
        IRow row1 = sheet1.CreateRow (0);
        for (int j = 0; j < dt.Columns.Count; j++) {
            ICell cell = row1.CreateCell (j);
            String columnName = dt.Columns[j].ToString ();
            cell.SetCellValue (columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++) {
            IRow row = sheet1.CreateRow (i + 1);
            for (int j = 0; j < dt.Columns.Count; j++) {
                ICell cell = row.CreateCell (j);
                String columnName = dt.Columns[j].ToString ();
                cell.SetCellValue (dt.Rows[i][columnName].ToString ());
            }
        }
        using (var exportData = new MemoryStream ()) {
            Response.Clear ();
            workbook.Write (exportData);
            if (extension == "xlsx") {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader ("Content-Disposition", string.Format ("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite (exportData.ToArray ());
            } else if (extension == "xls") {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader ("Content-Disposition", string.Format ("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite (exportData.GetBuffer ());
            }
            Response.End ();
        }
    }
    #endregion reuse
}