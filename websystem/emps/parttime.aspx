﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="parttime.aspx.cs" Inherits="websystem_emps_parttime" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
   <asp:Literal ID="test" runat="server"></asp:Literal>

   <!-- START Parttime Button Insert -->
   <div id="divToInsert" runat="server" class="col-md-12 m-b-10">
      <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server" CommandName="btnToInsert" OnCommand="btnCommand" Text="สร้างข้อตกลง" />
   </div>
   <!-- END Parttime Button Insert -->

   <!-- START Parttime Grid View -->
   <div class="col-md-12">
      <asp:GridView ID="GvMaster"
         runat="server"
         AutoGenerateColumns="false"
         DataKeyNames="m0_parttime_idx"
         CssClass="table table-striped table-bordered table-responsive col-md-12"
         HeaderStyle-CssClass="info"
         HeaderStyle-ForeColor="#31708f"
         AllowPaging="true"
         PageSize="10">
         <PagerStyle CssClass="PageCustom" />
         <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
         <EmptyDataTemplate>
            <div class="text-center">No result</div>
         </EmptyDataTemplate>
         <Columns>
            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
               <ItemTemplate>
                  <%# (Container.DataItemIndex +1) %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="รหัสข้อตกลง" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
               <ItemTemplate>
                  <small>
                     <asp:Label ID="parttimeCode" runat="server" Text='<%# Eval("parttime_code") %>' />
                  </small>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ชื่อข้อตกลงภาษาไทย" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
               <ItemTemplate>
                  <small>
                     <asp:Label ID="parttimeNameTH" runat="server" Text='<%# Eval("parttime_name_th") %>' />
                  </small>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
               <ItemTemplate>
                  <small>
                     <asp:Label ID="parttimeStatus" runat="server" Text='<%# getStatus((int)Eval("parttime_status")) %>' />
                  </small>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
               <ItemTemplate>
                  <asp:LinkButton CssClass="text-read" runat="server" data-toggle="tooltip" title="รายละเอียด" CommandName="btnView" OnCommand="btnCommand" CommandArgument='<%# Eval("m0_parttime_idx") %>'>
                     <i class="fa fa-eye fa-lg"></i>
                  </asp:LinkButton>
                  <asp:LinkButton CssClass="text-edit" runat="server" CommandName="btnToUpdate" data-toggle="tooltip" title="แก้ไข" OnCommand="btnCommand" CommandArgument='<%# Eval("m0_parttime_idx") %>'>
                           <i class="fa fa-pencil fa-lg"></i>
                  </asp:LinkButton>
                  <asp:LinkButton CssClass="text-trash" runat="server" data-toggle="tooltip" title="ลบ" CommandName="btnBan" OnCommand="btnCommand" CommandArgument='<%# Eval("m0_parttime_idx") %>' OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                           <i class="fa fa-trash fa-lg"></i>
                  </asp:LinkButton>
               </ItemTemplate>
            </asp:TemplateField>
         </Columns>
      </asp:GridView>
   </div>
   <!-- END Parttime Grid View -->

   <!-- START Parttime Create & Read & Update Form View -->
   <asp:FormView ID="fvCRUD" runat="server" Width="100%" CssClass="col-md-12" OnDataBound="FvDetail_DataBound">
      <%-- START Create Form --%>
      <InsertItemTemplate>
         <div class="col-md-12">
            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger m-b-10" runat="server" OnCommand="btnCommand" CommandName="btnCancel">
                <i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ
            </asp:LinkButton>
         </div>

         <!-- START Tab Menu -->
         <div class="row">
            <div class="col-md-12">
               <nav class="navbar navbar-default">
                  <div class="container-fluid">
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                           <span class="sr-only">Toggle navigation</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                        </button>
                        <span class="navbar-brand">Menu</span>
                     </div>
                     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav" role="tablist">
                           <li class="active"><a href="#shift-work" aria-controls="shift-work" role="tab" data-toggle="tab">ข้อตกลง<span class="sr-only">(current)</span></a></li>
                           <%--<li><a href="#add-money" aria-controls="add-money" role="tab" data-toggle="tab">เงื่อนไขข้อตกลงเงินเพิ่ม</a></li>
                           <li><a href="#less-money" aria-controls="less-money" role="tab" data-toggle="tab">เงื่อนไขข้อตกลงเงินหัก</a></li>--%>
                        </ul>
                     </div>
                  </div>
               </nav>
            </div>
         </div>
         <!-- END Tab Menu -->

         <!-- START Tab Content -->
         <div class="tab-content">
            <!-- START (1)Shift Work Content -->
            <div role="tabpanel" class="tab-pane in active" id="shift-work">
               <!-- START Work Description -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">รายละเอียดข้อตกลง</div>
                        <div class="panel-body">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">รหัสข้อตกลง</label>
                                 <asp:UpdatePanel ID="panelShiftWordCode" runat="server">
                                    <ContentTemplate>
                                       <asp:TextBox ID="txtShiftWorkCode" runat="server"
                                          CssClass="form-control f-s-13" placeholder="รหัสข้อตกลง..." />
                                       <asp:RequiredFieldValidator ID="requiredShiftWordCode"
                                          ValidationGroup="saveShiftWork" runat="server"
                                          Display="Dynamic"
                                          SetFocusOnError="true"
                                          ControlToValidate="txtShiftWorkCode"
                                          Font-Size="13px" ForeColor="Red"
                                          ErrorMessage="กรุณากรอกรหัสข้อตกลง" />
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ชื่อข้อตกลงภาษาไทย</label>
                                 <asp:UpdatePanel ID="panelShiftWorkNameTH" runat="server">
                                    <ContentTemplate>
                                       <asp:TextBox ID="txtShiftWorkNameTH" runat="server"
                                          CssClass="form-control f-s-13" placeholder="ชื่อข้อตกลงภาษาไทย..." />
                                       <asp:RequiredFieldValidator ID="requiredShiftWorkNameTH"
                                          ValidationGroup="saveShiftWork" runat="server"
                                          Display="Dynamic"
                                          SetFocusOnError="true"
                                          ControlToValidate="txtShiftWorkNameTH"
                                          Font-Size="13px" ForeColor="Red"
                                          ErrorMessage="กรุณากรอกชื่อข้อตกลงภาษาไทย" />
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ชื่อข้อตกลงภาษาอังกฤษ</label>
                                 <asp:TextBox ID="txtShiftWorkNameENG" runat="server"
                                    CssClass="form-control f-s-13" placeholder="ชื่อข้อตกลงภาษาอังกฤษ..." />
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">หมายเหตุ</label>
                                 <asp:TextBox ID="txtShiftWorkNote" runat="server"
                                    CssClass="form-control f-s-13" placeholder="หมายเหตุ..." />
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">สีข้อตกลง</label>
                                 <asp:TextBox ID="txtShiftWorkBgColor" runat="server"
                                    CssClass="form-control f-s-13 colorpicker"
                                    placeholder="สีข้อตกลง..." />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END Work Description -->

               <!-- START Between Work Time & Between Into Work Time -->
               <div class="row">
                  <!-- Start Between Work Time -->
                  <div class="col-md-6">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">ช่วงเวลางาน</div>
                        <div class="panel-body">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">เวลาเริ่มงาน</label>
                                 <asp:DropDownList ID="ddlShiftWorkStartTime" runat="server"
                                    CssClass="form-control f-s-13" AutoPostBack="false">
                                    <asp:ListItem Value="2" Selected="True" Text="วันนี้" />
                                    <asp:ListItem Value="3" Text="พรุ่งนี้" />
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:UpdatePanel ID="panelShiftWorkStartTime" runat="server">
                                    <ContentTemplate>
                                       <asp:TextBox ID="txtShiftWorkStartTime" runat="server"
                                          CssClass="form-control f-s-13 txtShiftWorkStartTime"
                                          placeholder="เวลาเริ่มงาน..." />
                                       <asp:RequiredFieldValidator ID="requiredShiftWorkStartTime"
                                          ValidationGroup="saveShiftWork" runat="server"
                                          Display="Dynamic" SetFocusOnError="true"
                                          ControlToValidate="txtShiftWorkStartTime"
                                          Font-Size="13px" ForeColor="Red"
                                          ErrorMessage="กรุณากำหนดเวลาเริ่มงาน" />
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">เวลาเลิกงาน</label>
                                 <asp:DropDownList ID="ddlShiftWorkEndTime" runat="server"
                                    CssClass="form-control f-s-13" AutoPostBack="false">
                                    <asp:ListItem Value="2" Selected="True">วันนี้</asp:ListItem>
                                    <asp:ListItem Value="3">พรุ่งนี้</asp:ListItem>
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:UpdatePanel ID="panelShiftWorkEndTime" runat="server">
                                    <ContentTemplate>
                                       <asp:TextBox ID="txtShiftWorkEndTime" runat="server"
                                          CssClass="form-control f-s-13 txtShiftWorkEndTime"
                                          placeholder="เวลาเลิกงาน..." />
                                       <asp:RequiredFieldValidator ID="requiredShiftWorkEndTime"
                                          ValidationGroup="saveShiftWork" runat="server"
                                          Display="Dynamic" SetFocusOnError="true"
                                          ControlToValidate="txtShiftWorkEndTime"
                                          Font-Size="13px" ForeColor="Red"
                                          ErrorMessage="กรุณากำหนดเวลาเลิกงาน" />
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- End Between Work Time -->

                  <!-- Start Between Into Work Time -->
                  <div class="col-md-6">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">ช่วงเวลาบันทึกเข้างาน</div>
                        <div class="panel-body">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ตั้งแต่</label>
                                 <asp:DropDownList ID="ddlShiftWorkBetweenStartTime"
                                    runat="server"
                                    CssClass="form-control f-s-13" AutoPostBack="false">
                                    <asp:ListItem Value="2" Selected="True" Text="วันนี้" />
                                    <asp:ListItem Value="3" Text="พรุ่งนี้" />
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:UpdatePanel ID="panelShiftWorkBetweenStartTime" runat="server">
                                    <ContentTemplate>
                                       <asp:TextBox ID="txtShiftWorkBetweenStartTime" runat="server"
                                          CssClass="form-control f-s-13 txtShiftWorkBetweenStartTime"
                                          placeholder="ตั้งแต่เวลา..." />
                                       <asp:RequiredFieldValidator ID="requiredShiftWorkBetweenStartTime"
                                          ValidationGroup="saveShiftWork" runat="server"
                                          Display="Dynamic" SetFocusOnError="true"
                                          ControlToValidate="txtShiftWorkBetweenStartTime"
                                          Font-Size="13px" ForeColor="Red"
                                          ErrorMessage="กรุณากำหนดเวลาเริ่มต้นบันทึกเข้างาน" />
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ถึง</label>
                                 <asp:DropDownList ID="ddlShiftWorkBetweenEndTime" runat="server"
                                    CssClass="form-control f-s-13" AutoPostBack="false">
                                    <asp:ListItem Value="2" Selected="True" Text="วันนี้" />
                                    <asp:ListItem Value="3" Text="พรุ่งนี้" />
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:UpdatePanel ID="panelShiftWorkBetweenEndTime" runat="server">
                                    <ContentTemplate>
                                       <asp:TextBox ID="txtShiftWorkBetweenEndTime" runat="server"
                                          CssClass="form-control f-s-13 txtShiftWorkBetweenEndTime"
                                          placeholder="ถึงเวลา..." />
                                       <asp:RequiredFieldValidator ID="requiredShiftWorkBetweenEndTime"
                                          ValidationGroup="saveShiftWork" runat="server"
                                          Display="Dynamic" SetFocusOnError="true"
                                          ControlToValidate="txtShiftWorkBetweenEndTime"
                                          Font-Size="13px" ForeColor="Red"
                                          ErrorMessage="กรุณากำหนดเวลาสิ้นสุดบันทึกเข้างาน" />
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- End Between Into Work Time -->
               </div>
               <!-- END Between Work Time & Between Into Work Time -->

               <!-- START Between Break Time & Other -->
               <div class="row">
                  <!-- START Between Break Time -->
                  <div class="col-md-6">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">พักระหว่างงาน</div>
                        <div class="panel-body">
                           <div class="col-md-12">
                              <div class="checkbox">
                                 <label>
                                    <asp:CheckBox ID="chkShiftWorkAllowBreak"
                                       runat="server"
                                       CssClass="chkShiftWorkAllowBreak" />
                                    พักระหว่างงาน
                                 </label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">เวลาพัก</label>
                                 <asp:DropDownList Enabled="false"
                                    ID="ddlShiftWorkBreakStartTime"
                                    runat="server"
                                    CssClass="form-control f-s-13 ddlShiftWorkBreakStartTime"
                                    AutoPostBack="false">
                                    <asp:ListItem Value="2" Selected="True" Text="วันนี้" />
                                    <asp:ListItem Value="3" Text="พรุ่งนี้" />
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:TextBox Enabled="false"
                                    ID="txtShiftWorkBreakStartTime"
                                    runat="server"
                                    CssClass="form-control f-s-13 txtShiftWorkBreakStartTime"
                                    placeholder="เวลาเริ่มพัก..." />
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ถึง</label>
                                 <asp:DropDownList Enabled="false"
                                    ID="ddlShiftWorkBreakEndTime"
                                    runat="server"
                                    CssClass="form-control f-s-13 ddlShiftWorkBreakEndTime"
                                    AutoPostBack="false">
                                    <asp:ListItem Value="2" Selected="True" Text="วันนี้" />
                                    <asp:ListItem Value="3" Text="พรุ่งนี้" />
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:TextBox Enabled="false"
                                    ID="txtShiftWorkBreakEndTime"
                                    runat="server"
                                    CssClass="form-control f-s-13 txtShiftWorkBreakEndTime"
                                    placeholder="ถึงเวลา..." />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- END Between Break Time -->

                  <!-- START Other -->
                  <div class="col-md-6">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">อื่นๆ</div>
                        <div class="panel-body">
                           <div class="row">
                              <div class="col-md-7">
                                 <div class="form-group">
                                    <label class="f-s-13">จำนวนครั้งรูดบัตร</label>
                                    <asp:TextBox ID="txtShiftWorkAmountScanCard"
                                       runat="server"
                                       CssClass="form-control f-s-13"
                                       placeholder="จำนวนครั้งรูดบัตร...">
                                    </asp:TextBox>
                                 </div>
                              </div>
                              <div class="col-md-5">
                                 <label></label>
                                 <div class="checkbox">
                                    <label>
                                       <asp:CheckBox ID="chkShiftWorkTimeStand"
                                          runat="server" />
                                       เวลางานคงที่
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">จำนวน ชม. งาน</label>
                                 <asp:TextBox ID="txtShiftWorkAmountWorkHours" runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="จำนวน ชม. งาน...">
                                 </asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">จำนวน ชม. พัก</label>
                                 <asp:TextBox ID="txtShiftWorkAmountBreakHours"
                                    runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="จำนวน ชม. พัก...">
                                 </asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">ผลตอบแทน</label>
                                 <asp:DropDownList ID="ddlShiftWorkReward" runat="server"
                                    CssClass="form-control f-s-13" AutoPostBack="false" />
                                 <asp:RequiredFieldValidator ID="requiredShiftWorkReward"
                                    ValidationGroup="saveShiftWork" runat="server"
                                    Display="Dynamic"
                                    SetFocusOnError="true"
                                    ControlToValidate="ddlShiftWorkReward"
                                    Font-Size="13px" ForeColor="Red"
                                    ErrorMessage="กรุณาเลือกผลตอบแทน" />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- END Other -->

                  <div class="col-md-6">
                     <div class="form-group">
                        <label>สถานะ</label>
                        <asp:DropDownList ID="ddlParttimeStatus" runat="server"
                           CssClass="form-control">
                           <asp:ListItem Value="1" Text="Online" />
                           <asp:ListItem Value="0" Text="Offline" />
                        </asp:DropDownList>
                     </div>
                  </div>
               </div>
               <!-- END Between Break Time & Other -->

               <!-- START Button Save & Cancel -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsertShiftWork" OnCommand="btnCommand" ValidationGroup="saveShiftWork" Text="บันทึก" />
                        <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>
                     </div>
                  </div>
               </div>
               <!-- END Button Save & Cancel -->
            </div>
            <!-- END (1)Shift Work Content -->

            <!-- START (2)Add Money Content -->
            <div role="tabpanel" class="tab-pane" id="add-money">

               <!-- START Add Money Description -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">รายละเอียดข้อตกลงเงินเพิ่ม</div>
                        <div class="panel-body">
                           <div class="col-md-12">
                              <div class="checkbox">
                                 <label class="m-r-10">
                                    <asp:CheckBox ID="chkAddMoneyAllowOnline" runat="server" />
                                    ยังใช้งาน
                                 </label>
                                 <label>
                                    <asp:CheckBox ID="chkAddMoneyNeedApprove" runat="server" />
                                    ต้องการการอนุมัติ
                                 </label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ชื่อข้อตกลงภาษาไทย</label>
                                 <asp:TextBox ID="txtAddMoneyNameTH" runat="server" CssClass="form-control f-s-13" placeholder="ชื่อข้อตกลงภาษาไทย..."></asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ชื่อข้อตกลงภาษาอังกฤษ</label>
                                 <asp:TextBox ID="txtAddMoneyNameENG" runat="server" CssClass="form-control f-s-13" placeholder="ชื่อข้อตกลงภาษาอังกฤษ..."></asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">หมายเหตุ</label>
                                 <asp:TextBox ID="txtAddMoneyNote" runat="server" CssClass="form-control f-s-13" placeholder="หมายเหตุ..."></asp:TextBox>
                              </div>
                           </div>

                        </div>
                     </div>
                  </div>
               </div>
               <!-- END Add Money Description -->

               <!-- START Condition Process -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">กำหนดเงื่อนไขจะคำนวณเมื่อไร</div>
                        <div class="panel-body">
                           <!-- START Condition Process Description -->
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="f-s-13">ลักษณะการรูดบัตร</label>
                                    <asp:DropDownList ID="ddlAddMoneyTypeScanCard" runat="server"
                                       CssClass="form-control f-s-13" AutoPostBack="false">
                                       <asp:ListItem Value="1">ล่วงเวลาก่อนเข้างาน</asp:ListItem>
                                    </asp:DropDownList>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="f-s-13">คำนวณเมื่อไร</label>
                                    <asp:DropDownList ID="ddlAddMoneyWhenProcess" runat="server"
                                       CssClass="form-control f-s-13" AutoPostBack="false">
                                       <asp:ListItem Value="1">เมื่อมีการบันทีกเวลาทั้งเข้าและออก
                                       </asp:ListItem>
                                    </asp:DropDownList>
                                 </div>
                              </div>
                           </div>
                           <!-- END Condition Process Description -->

                           <!-- START Between Start Time & Between End Time -->
                           <div class="row">
                              <!-- START Between Start Time -->
                              <div class="col-md-6">
                                 <div class="panel panel-info">
                                    <div class="panel-heading f-bold">ช่วงเวลาเข้า</div>
                                    <div class="panel-body">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">ตั้งแต่</label>
                                             <asp:DropDownList ID="ddlAddMoneyFromTimeInto"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                AutoPostBack="false">
                                                <asp:ListItem Value="1">เมื่อวาน</asp:ListItem>
                                                <asp:ListItem Value="2" Selected="True">วันนี้
                                                </asp:ListItem>
                                                <asp:ListItem Value="3">พรุ่งนี้</asp:ListItem>
                                             </asp:DropDownList>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">&nbsp;</label>
                                             <asp:TextBox ID="txtAddMoneyFromTimeInto"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                placeholder="ตั้งแต่เวลา..."></asp:TextBox>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">ถึง</label>
                                             <asp:DropDownList ID="ddlAddMoneyToTimeInto"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                AutoPostBack="false">
                                                <asp:ListItem Value="1">เมื่อวาน</asp:ListItem>
                                                <asp:ListItem Value="2" Selected="True">วันนี้
                                                </asp:ListItem>
                                                <asp:ListItem Value="3">พรุ่งนี้</asp:ListItem>
                                             </asp:DropDownList>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">&nbsp;</label>
                                             <asp:TextBox ID="txtAddMoneyToTimeInto" runat="server"
                                                CssClass="form-control f-s-13"
                                                placeholder="ถึงเวลา..."></asp:TextBox>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- END Between Start Time -->

                              <!-- START Between End Time -->
                              <div class="col-md-6">
                                 <div class="panel panel-info">
                                    <div class="panel-heading f-bold">ช่วงเวลาออก</div>
                                    <div class="panel-body">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">ตั้งแต่</label>
                                             <asp:DropDownList ID="ddlAddMoneyFromTimeOuter"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                AutoPostBack="false">
                                                <asp:ListItem Value="1">เมื่อวาน</asp:ListItem>
                                                <asp:ListItem Value="2" Selected="True">วันนี้
                                                </asp:ListItem>
                                                <asp:ListItem Value="3">พรุ่งนี้</asp:ListItem>
                                             </asp:DropDownList>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">&nbsp;</label>
                                             <asp:TextBox ID="txtAddMoneyFromTimeOuter"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                placeholder="ตั้งแต่..."></asp:TextBox>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">ถึง</label>
                                             <asp:DropDownList ID="ddlAddMoneyToTimeOuter"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                AutoPostBack="false">
                                                <asp:ListItem Value="1">เมื่อวาน</asp:ListItem>
                                                <asp:ListItem Value="2" Selected="True">วันนี้
                                                </asp:ListItem>
                                                <asp:ListItem Value="3">พรุ่งนี้</asp:ListItem>
                                             </asp:DropDownList>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">&nbsp;</label>
                                             <asp:TextBox ID="txtAddMoneyToTimeOuter"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                placeholder="ถึง..."></asp:TextBox>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- END Between End Time -->
                           </div>
                           <!-- END Between Start Time & Between End Time -->
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END Condition Process -->

               <!-- START Formula Process -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">
                           กรณีที่ตรงตามเงื่อนไขจะคำนวณโดยสูตรการคำนวณ
                        </div>
                        <div class="panel-body">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">วิธีคำนวณ</label>
                                 <asp:DropDownList ID="ddlAddMoneyCalculus" runat="server"
                                    CssClass="form-control f-s-13" AutoPostBack="false">
                                    <asp:ListItem Value="1">ล่วงเวลาก่อนเข้างาน</asp:ListItem>
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ปัดเศษนาที</label>
                                 <asp:DropDownList ID="ddlAddMoneyRoundMinute" runat="server"
                                    CssClass="form-control f-s-13" AutoPostBack="false">
                                    <asp:ListItem Value="1">เมื่อมีการบันทีกเวลาทั้งเข้าและออก
                                    </asp:ListItem>
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">สูตร</label>
                                 <asp:TextBox ID="txtAddMoneyFormula" runat="server"
                                    CssClass="form-control f-s-13" placeholder="สูตร...">
                                 </asp:TextBox>
                              </div>
                           </div>

                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">เวลาเทียบเข้า</label>
                                 <asp:DropDownList ID="ddlAddMoneyCompareTimeInto" runat="server"
                                    CssClass="form-control f-s-13"
                                    AutoPostBack="false">
                                    <asp:ListItem Value="1">เมื่อวาน</asp:ListItem>
                                    <asp:ListItem Value="2" Selected="True">วันนี้
                                    </asp:ListItem>
                                    <asp:ListItem Value="3">พรุ่งนี้</asp:ListItem>
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:TextBox ID="txtAddMoneyCompareTimeInto" runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="เวลา..."></asp:TextBox>
                              </div>
                           </div>

                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">เวลาเทียบออก</label>
                                 <asp:DropDownList ID="ddlAddMoneyCompareTimeOuter" runat="server"
                                    CssClass="form-control f-s-13"
                                    AutoPostBack="false">
                                    <asp:ListItem Value="1">เมื่อวาน</asp:ListItem>
                                    <asp:ListItem Value="2" Selected="True">วันนี้
                                    </asp:ListItem>
                                    <asp:ListItem Value="3">พรุ่งนี้</asp:ListItem>
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:TextBox ID="txtAddMoneyCompareTimeOuter" runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="เวลา..."></asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">บันทึกผลการคำนวณเป็น</label>
                                 <asp:DropDownList ID="ddlAddMoneyFinalProcess" runat="server"
                                    CssClass="form-control f-s-13" AutoPostBack="false">
                                    <asp:ListItem Value="1">ค่าล่วงเวลา x 1.5 (ชั่วโมง)</asp:ListItem>
                                 </asp:DropDownList>
                              </div>
                           </div>

                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">ผลการคำนวณต่ำกว่า</label>
                                 <asp:TextBox ID="txtAddMoneyFinalLow" runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="ผลการคำนวณต่ำกว่า..."></asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">ปัดเป็น</label>
                                 <asp:TextBox ID="txtAddMoneyFinalLowRound" runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="ปัดเป็น..."></asp:TextBox>
                              </div>
                           </div>

                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">ผลการคำนวณสูงกว่า</label>
                                 <asp:TextBox ID="txtAddMoneyFinalHigh" runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="ผลการคำนวณสูงกว่า..."></asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">ปัดเป็น</label>
                                 <asp:TextBox ID="txtAddMoneyFinalHighRound" runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="ปัดเป็น..."></asp:TextBox>
                              </div>
                           </div>

                        </div>
                     </div>
                  </div>
               </div>
               <!-- END Formula Process -->

               <!-- START Button Save & Cancel -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <asp:LinkButton CssClass="btn btn-success" runat="server" CommandName="btnInsertAddMoney" OnCommand="btnCommand">บันทึก</asp:LinkButton>
                        <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>
                     </div>
                  </div>
               </div>
               <!-- END Button Save & Cancel -->

            </div>
            <!-- END (2)Add Money Content -->

            <!-- START (3)Less Money Content -->
            <div role="tabpanel" class="tab-pane" id="less-money">

               <!-- START Less Money Description -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">รายละเอียดข้อตกลงเงินหัก</div>
                        <div class="panel-body">
                           <div class="col-md-12">
                              <div class="checkbox">
                                 <label class="m-r-10">
                                    <asp:CheckBox ID="chkLessMoneyAllowOnline"
                                       runat="server" />
                                    ยังใช้งาน
                                 </label>
                                 <label>
                                    <asp:CheckBox ID="chkLessMoneyNeedApprove"
                                       runat="server" />
                                    ต้องการการอนุมัติ
                                 </label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ชื่อข้อตกลงภาษาไทย</label>
                                 <asp:TextBox ID="txtLessMoneyNameTH" runat="server" CssClass="form-control f-s-13" placeholder="ชื่อข้อตกลงภาษาไทย..."></asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ชื่อข้อตกลงภาษาอังกฤษ</label>
                                 <asp:TextBox ID="txtLessMoneyNameENG" runat="server" CssClass="form-control f-s-13" placeholder="ชื่อข้อตกลงภาษาอังกฤษ..."></asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">หมายเหตุ</label>
                                 <asp:TextBox ID="txtLessMoneyNote" runat="server" CssClass="form-control f-s-13" placeholder="หมายเหตุ..."></asp:TextBox>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END Less Money Description -->

               <!-- START Condition Process -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">กำหนดเงื่อนไขจะคำนวณเมื่อไร</div>
                        <div class="panel-body">
                           <!-- START Condition Process Description -->
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="f-s-13">ลักษณะการรูดบัตร</label>
                                    <asp:DropDownList ID="ddlLessMoneyTypeScanCard"
                                       runat="server"
                                       CssClass="form-control f-s-13" AutoPostBack="false">
                                       <asp:ListItem Value="1">
                                                        ล่วงเวลาก่อนเข้างาน
                                       </asp:ListItem>
                                    </asp:DropDownList>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="f-s-13">คำนวณเมื่อไร</label>
                                    <asp:DropDownList ID="ddlLessMoneyWhenProcess" runat="server"
                                       CssClass="form-control f-s-13" AutoPostBack="false">
                                       <asp:ListItem Value="1">เมื่อมีการบันทีกเวลาทั้งเข้าและออก
                                       </asp:ListItem>
                                    </asp:DropDownList>
                                 </div>
                              </div>
                           </div>
                           <!-- END Condition Process Description -->

                           <!-- START Between Start Time & Between End Time -->
                           <div class="row">
                              <!-- START Between Start Time -->
                              <div class="col-md-6">
                                 <div class="panel panel-info">
                                    <div class="panel-heading f-bold">ช่วงเวลาเข้า</div>
                                    <div class="panel-body">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">ตั้งแต่</label>
                                             <asp:DropDownList ID="ddlLessMoneyFromTimeInto"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                AutoPostBack="false">
                                                <asp:ListItem Value="1">เมื่อวาน</asp:ListItem>
                                                <asp:ListItem Value="2" Selected="True">วันนี้
                                                </asp:ListItem>
                                                <asp:ListItem Value="3">พรุ่งนี้</asp:ListItem>
                                             </asp:DropDownList>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">&nbsp;</label>
                                             <asp:TextBox ID="txtLessMoneyFromTimeInto"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                placeholder="ตั้งแต่เวลา..."></asp:TextBox>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">ถึง</label>
                                             <asp:DropDownList ID="ddlLessMoneyToTimeInto"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                AutoPostBack="false">
                                                <asp:ListItem Value="1">เมื่อวาน</asp:ListItem>
                                                <asp:ListItem Value="2" Selected="True">วันนี้
                                                </asp:ListItem>
                                                <asp:ListItem Value="3">พรุ่งนี้</asp:ListItem>
                                             </asp:DropDownList>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">&nbsp;</label>
                                             <asp:TextBox ID="txtLessMoneyToTimeInto"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                placeholder="ถึงเวลา..."></asp:TextBox>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- END Between Start Time -->

                              <!-- START Between End Time -->
                              <div class="col-md-6">
                                 <div class="panel panel-info">
                                    <div class="panel-heading f-bold">ช่วงเวลาออก</div>
                                    <div class="panel-body">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">ตั้งแต่</label>
                                             <asp:DropDownList
                                                ID="ddlLessMoneyFromTimeOuter"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                AutoPostBack="false">
                                                <asp:ListItem Value="1">
                                                                    เมื่อวาน
                                                </asp:ListItem>
                                                <asp:ListItem Value="2"
                                                   Selected="True">
                                                                    วันนี้
                                                </asp:ListItem>
                                                <asp:ListItem Value="3">
                                                                    พรุ่งนี้
                                                </asp:ListItem>
                                             </asp:DropDownList>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">&nbsp;</label>
                                             <asp:TextBox ID="txtLessMoneyFromTimeOuter"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                placeholder="ตั้งแต่..."></asp:TextBox>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">ถึง</label>
                                             <asp:DropDownList
                                                ID="ddlLessMoneyToTimeOuter"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                AutoPostBack="false">
                                                <asp:ListItem Value="1">
                                                                    เมื่อวาน
                                                </asp:ListItem>
                                                <asp:ListItem Value="2"
                                                   Selected="True">
                                                                    วันนี้
                                                </asp:ListItem>
                                                <asp:ListItem Value="3">
                                                                    พรุ่งนี้
                                                </asp:ListItem>
                                             </asp:DropDownList>
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="f-s-13">&nbsp;</label>
                                             <asp:TextBox ID="txtLessMoneyToTimeOuter"
                                                runat="server"
                                                CssClass="form-control f-s-13"
                                                placeholder="ถึง..."></asp:TextBox>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- END Between End Time -->
                           </div>
                           <!-- END Between Start Time & Between End Time -->
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END Condition Process -->

               <!-- START Formula Process -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">
                           กรณีที่ตรงตามเงื่อนไขจะคำนวณโดยสูตรการคำนวณ
                        </div>
                        <div class="panel-body">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">วิธีคำนวณ</label>
                                 <asp:DropDownList ID="ddlLessMoneyCalculus" runat="server"
                                    CssClass="form-control f-s-13" AutoPostBack="false">
                                    <asp:ListItem Value="1">ล่วงเวลาก่อนเข้างาน</asp:ListItem>
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ปัดเศษนาที</label>
                                 <asp:DropDownList ID="ddlLessMoneyRoundMinute"
                                    runat="server"
                                    CssClass="form-control f-s-13" AutoPostBack="false">
                                    <asp:ListItem Value="1">เมื่อมีการบันทีกเวลาทั้งเข้าและออก
                                    </asp:ListItem>
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">สูตร</label>
                                 <asp:TextBox ID="txtLessMoneyFormula" runat="server"
                                    CssClass="form-control f-s-13" placeholder="สูตร...">
                                 </asp:TextBox>
                              </div>
                           </div>

                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">เวลาเทียบเข้า</label>
                                 <asp:DropDownList ID="ddlLessMoneyCompareTimeInto"
                                    runat="server"
                                    CssClass="form-control f-s-13"
                                    AutoPostBack="false">
                                    <asp:ListItem Value="1">เมื่อวาน</asp:ListItem>
                                    <asp:ListItem Value="2" Selected="True">วันนี้
                                    </asp:ListItem>
                                    <asp:ListItem Value="3">พรุ่งนี้</asp:ListItem>
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:TextBox ID="txtLessMoneyCompareTimeInto" runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="เวลา..."></asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">เวลาเทียบออก</label>
                                 <asp:DropDownList ID="ddlLessMoneyCompareTimeOuter"
                                    runat="server"
                                    CssClass="form-control f-s-13"
                                    AutoPostBack="false">
                                    <asp:ListItem Value="1">เมื่อวาน</asp:ListItem>
                                    <asp:ListItem Value="2" Selected="True">วันนี้
                                    </asp:ListItem>
                                    <asp:ListItem Value="3">พรุ่งนี้</asp:ListItem>
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:TextBox ID="txtLessMoneyCompareTimeOuter"
                                    runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="เวลา..."></asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">บันทึกผลการคำนวณเป็น</label>
                                 <asp:DropDownList ID="ddlLessMoneyFinalProcess"
                                    runat="server"
                                    CssClass="form-control f-s-13" AutoPostBack="false">
                                    <asp:ListItem Value="1">
                                                    ค่าล่วงเวลา x 1.5 (ชั่วโมง)
                                    </asp:ListItem>
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">ผลการคำนวณต่ำกว่า</label>
                                 <asp:TextBox ID="txtLessMoneyFinalLow" runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="ผลการคำนวณต่ำกว่า..."></asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">ปัดเป็น</label>
                                 <asp:TextBox ID="txtLessMoneyFinalLowRound" runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="ปัดเป็น..."></asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">ผลการคำนวณสูงกว่า</label>
                                 <asp:TextBox ID="txtLessMoneyFinalHigh" runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="ผลการคำนวณสูงกว่า..."></asp:TextBox>
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="form-group">
                                 <label class="f-s-13">ปัดเป็น</label>
                                 <asp:TextBox ID="txtLessMoneyFinalHighRound" runat="server"
                                    CssClass="form-control f-s-13"
                                    placeholder="ปัดเป็น..."></asp:TextBox>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END Formula Process -->

               <!-- START Button Save & Cancel -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <asp:LinkButton CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand">บันทึก</asp:LinkButton>
                        <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>
                     </div>
                  </div>
               </div>
               <!-- END Button Save & Cancel -->

            </div>
            <!-- END (3)Less Money Content -->
         </div>
         <!-- END Tab Content -->
      </InsertItemTemplate>
      <%-- END Create Form --%>

      <%-- START Update Form --%>
      <EditItemTemplate>
         <div class="row">
            <div class="col-md-12">
               <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger m-b-10" runat="server" OnCommand="btnCommand" CommandName="btnCancel">
                     <i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ
               </asp:LinkButton>
            </div>
         </div>
         <!-- START Tab Menu -->
         <div class="row">
            <div class="col-md-12">
               <nav class="navbar navbar-default">
                  <div class="container-fluid">
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                           <span class="sr-only">Toggle navigation</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                        </button>
                        <span class="navbar-brand">Menu</span>
                     </div>
                     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav" role="tablist">
                           <li class="active"><a href="#shift-work-read" aria-controls="shift-work-read" role="tab" data-toggle="tab">ข้อตกลง<span class="sr-only">(current)</span></a></li>
                           <%--<li><a href="#add-money-read" aria-controls="add-money-read" role="tab" data-toggle="tab">เงื่อนไขข้อตกลงเงินเพิ่ม</a></li>
                           <li><a href="#less-money-read" aria-controls="less-money-read" role="tab" data-toggle="tab">เงื่อนไขข้อตกลงเงินหัก</a></li>--%>
                        </ul>
                     </div>
                  </div>
               </nav>
            </div>
         </div>
         <!-- END Tab Menu -->

         <!-- Start Tab Content -->
         <div class="tab-content">
            <!-- Start (1)Shift Work Content -->
            <div role="tabpanel" class="tab-pane fade in active" id="shift-work-update">
               <!-- Start Work Description -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">รายละเอียดข้อตกลง</div>
                        <div class="panel-body">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">รหัสข้อตกลง</label>
                                 <asp:TextBox ID="txtShiftWorkCodeUpdate" runat="server" CssClass="form-control f-s-13" placeholder="รหัสข้อตกลง..." Text='<%# Eval("parttime_code") %>' />
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ชื่อข้อตกลงภาษาไทย</label>
                                 <asp:TextBox ID="txtShiftWorkNameTHUpdate" runat="server" CssClass="form-control f-s-13" placeholder="ชื่อข้อตกลงภาษาไทย..." Text='<%# Eval("parttime_name_th") %>' />
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ชื่อข้อตกลงภาษาอังกฤษ</label>
                                 <asp:TextBox ID="txtShiftWorkNameENGUpdate" runat="server" CssClass="form-control f-s-13" placeholder="ชื่อข้อตกลงภาษาอังกฤษ..." Text='<%# Eval("parttime_name_eng") %>' />
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">หมายเหตุ</label>
                                 <asp:TextBox ID="txtShiftWorkNoteUpdate" runat="server" CssClass="form-control f-s-13" placeholder="หมายเหตุ..." Text='<%# Eval("parttime_note") %>' />
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">สีข้อตกลง</label>
                                 <asp:TextBox ID="txtShiftWorkBgColorUpdate" runat="server"
                                    CssClass="form-control f-s-13 colorpicker"
                                    placeholder="สีข้อตกลง..." Text='<%# Eval("parttime_bg_color") %>' />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- End Work Description -->

               <!-- Start Between Work Time & Between Into Work Time -->
               <div class="row">
                  <!-- Start Between Work Time -->
                  <div class="col-md-6">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">ช่วงเวลางาน</div>
                        <div class="panel-body">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">เวลาเริ่มงาน</label>
                                 <asp:Label ID="lblShiftWorkStartTimeUpdate" runat="server"
                                    Text='<%# Eval("parttime_start_date") %>' Visible="false" />
                                 <asp:DropDownList ID="ddlShiftWorkStartTimeUpdate" runat="server"
                                    CssClass="form-control f-s-13">
                                    <asp:ListItem Value="1" Text="เมื่อวาน" />
                                    <asp:ListItem Value="2" Selected="True" Text="วันนี้" />
                                    <asp:ListItem Value="3" Text="พรุ่งนี้" />
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:TextBox ID="txtShiftWorkStartTimeUpdate" runat="server"
                                    CssClass="form-control f-s-13 txtShiftWorkStartTime"
                                    placeholder="เวลาเริ่มงาน..." Text='<%# Eval("pt_start_time") %>' />
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">เวลาเลิกงาน</label>
                                 <asp:Label ID="lblShiftWorkEndTimeUpdate" runat="server"
                                    Text='<%# Eval("parttime_end_date") %>' Visible="false" />
                                 <asp:DropDownList ID="ddlShiftWorkEndTimeUpdate" runat="server"
                                    CssClass="form-control f-s-13">
                                    <asp:ListItem Value="1" Text="เมื่อวาน" />
                                    <asp:ListItem Value="2" Selected="True" Text="วันนี้" />
                                    <asp:ListItem Value="3" Text="พรุ่งนี้" />
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:TextBox ID="txtShiftWorkEndTimeUpdate" runat="server"
                                    CssClass="form-control f-s-13 txtShiftWorkEndTime"
                                    placeholder="เวลาเลิกงาน..."
                                    Text='<%# Eval("pt_end_time") %>' />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- End Between Work Time -->

                  <!-- Start Between Into Work Time -->
                  <div class="col-md-6">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">ช่วงเวลาบันทึกเข้างาน</div>
                        <div class="panel-body">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ตั้งแต่</label>
                                 <asp:Label ID="lblShiftWorkBetweenStartTimeUpdate" runat="server"
                                    Text='<%# Eval("parttime_btw_start_date") %>' Visible="false" />
                                 <asp:DropDownList ID="ddlShiftWorkBetweenStartTimeUpdate" runat="server"
                                    CssClass="form-control f-s-13">
                                    <asp:ListItem Value="1" Text="เมื่อวาน" />
                                    <asp:ListItem Value="2" Selected="True" Text="วันนี้" />
                                    <asp:ListItem Value="3" Text="พรุ่งนี้" />
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:TextBox ID="txtShiftWorkBetweenStartTimeUpdate" runat="server"
                                    CssClass="form-control f-s-13 txtShiftWorkBetweenStartTime"
                                    placeholder="ตั้งแต่เวลา..." Text='<%# Eval("pt_btw_start_time") %>' />
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ถึง</label>
                                 <asp:Label ID="lblShiftWorkBetweenEndTimeUpdate" runat="server"
                                    Text='<%# Eval("parttime_btw_end_date") %>' Visible="false" />
                                 <asp:DropDownList ID="ddlShiftWorkBetweenEndTimeUpdate" runat="server"
                                    CssClass="form-control f-s-13">
                                    <asp:ListItem Value="1" Text="เมื่อวาน" />
                                    <asp:ListItem Value="2" Selected="True" Text="วันนี้" />
                                    <asp:ListItem Value="3" Text="พรุ่งนี้" />
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:TextBox ID="txtShiftWorkBetweenEndTimeUpdate" runat="server"
                                    CssClass="form-control f-s-13 txtShiftWorkBetweenEndTime"
                                    placeholder="ถึงเวลา..." Text='<%# Eval("pt_btw_end_time") %>' />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- End Between Into Work Time -->
               </div>
               <!-- End Between Work Time & Between Into Work Time -->

               <!-- Start Between Break Time & Other -->
               <div class="row">
                  <!-- Start Between Break Time -->
                  <div class="col-md-6">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">พักระหว่างงาน</div>
                        <div class="panel-body">
                           <div class="col-md-12">
                              <div class="checkbox">
                                 <label>
                                    <asp:Label ID="lblBreakDate" runat="server"
                                       Text='<%# Eval("parttime_break_start_date") %>' Visible="false" />
                                    <asp:CheckBox ID="chkShiftWorkAllowBreakUpdate"
                                       runat="server" CssClass="chkShiftWorkAllowBreakUpdate" />
                                    พักระหว่างงาน 
                                 </label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">เวลาพัก</label>
                                 <asp:Label ID="lblShiftWorkBreakStartTimeUpdate" runat="server"
                                    Text='<%# Eval("parttime_break_start_date") %>' Visible="false" />
                                 <asp:DropDownList ID="ddlShiftWorkBreakStartTimeUpdate" runat="server"
                                    CssClass="form-control f-s-13 ddlShiftWorkBreakStartTimeUpdate">
                                    <asp:ListItem Value="1" Text="เมื่อวาน" />
                                    <asp:ListItem Value="2" Selected="True" Text="วันนี้" />
                                    <asp:ListItem Value="3" Text="พรุ่งนี้" />
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:TextBox ID="txtShiftWorkBreakStartTimeUpdate" runat="server"
                                    placeholder="เวลาเริ่มพัก..."
                                    CssClass="form-control f-s-13 txtShiftWorkBreakStartTimeUpdate"
                                    Text='<%# Eval("parttime_break_start_time").ToString() == "00:00:00" ? null : Eval("pt_break_start_time") %>' />
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">ถึง</label>
                                 <asp:Label ID="lblShiftWorkBreakEndTimeUpdate" runat="server"
                                    Text='<%# Eval("parttime_break_end_date") %>' Visible="false" />
                                 <asp:DropDownList ID="ddlShiftWorkBreakEndTimeUpdate" runat="server"
                                    CssClass="form-control f-s-13 ddlShiftWorkBreakEndTimeUpdate">
                                    <asp:ListItem Value="1" Text="เมื่อวาน" />
                                    <asp:ListItem Value="2" Selected="True" Text="วันนี้" />
                                    <asp:ListItem Value="3" Text="พรุ่งนี้" />
                                 </asp:DropDownList>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">&nbsp;</label>
                                 <asp:TextBox ID="txtShiftWorkBreakEndTimeUpdate" runat="server"
                                    placeholder="ถึงเวลา..."
                                    CssClass="form-control f-s-13 txtShiftWorkBreakEndTimeUpdate"
                                    Text='<%# Eval("parttime_break_end_time").ToString() == "00:00:00" ? null : Eval("pt_break_end_time") %>' />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- End Between Break Time -->

                  <!-- Start Other -->
                  <div class="col-md-6">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">อื่นๆ</div>
                        <div class="panel-body">
                           <div class="row">
                              <div class="col-md-7">
                                 <div class="form-group">
                                    <label class="f-s-13">จำนวนครั้งรูดบัตร</label>
                                    <asp:TextBox ID="txtShiftWorkAmountScanCardUpdate"
                                       runat="server"
                                       CssClass="form-control f-s-13"
                                       placeholder="จำนวนครั้งรูดบัตร..."
                                       Text='<%# Eval("parttime_amount_scan_card") %>' />
                                 </div>
                              </div>
                              <div class="col-md-5">
                                 <label></label>
                                 <div class="checkbox">
                                    <label>
                                       <asp:Label ID="statusTimeStand" runat="server" Text='<%# Eval("parttime_time_stand") %>' Visible="false" />
                                       <asp:CheckBox ID="chkShiftWorkTimeStandUpdate" runat="server" />
                                       เวลางานคงที่
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">จำนวน ชม. งาน</label>
                                 <asp:TextBox ID="txtShiftWorkAmountWorkHoursUpdate" runat="server"
                                    CssClass="form-control f-s-13" placeholder="จำนวน ชม. งาน..."
                                    Text='<%# Eval("parttime_amount_work_hours") %>' />
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label class="f-s-13">จำนวน ชม. พัก</label>
                                 <asp:TextBox ID="txtShiftWorkAmountBreakHoursUpdate"
                                    runat="server" CssClass="form-control f-s-13" placeholder="จำนวน ชม. พัก..."
                                    Text='<%# Eval("parttime_amount_break_hours") %>' />
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label class="f-s-13">ผลตอบแทน</label>
                                 <asp:Label ID="lblShiftWorkRewardUpdate" runat="server" Text='<%# Eval("m0_reward_idx_ref") %>' Visible="False" />
                                 <asp:DropDownList ID="ddlShiftWorkRewardUpdate" runat="server"
                                    CssClass="form-control f-s-13" AutoPostBack="false" />
                                 <asp:RequiredFieldValidator ID="requiredShiftWorkRewardUpdate"
                                    ValidationGroup="saveShiftWorkUpdate" runat="server"
                                    Display="Dynamic"
                                    SetFocusOnError="true"
                                    ControlToValidate="ddlShiftWorkRewardUpdate"
                                    Font-Size="13px" ForeColor="Red"
                                    ErrorMessage="กรุณาเลือกผลตอบแทน" />
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- End Other -->

                  <div class="col-md-6">
                     <div class="form-group">
                        <label>สถานะ</label>
                        <asp:Label Visible="false" runat="server" ID="parttimeStatusForCheck" Text='<%# Eval("parttime_status") %>' />
                        <asp:DropDownList ID="ddlParttimeStatusUpdate" runat="server" CssClass="form-control">
                           <asp:ListItem Value="1" Text="Online" />
                           <asp:ListItem Value="0" Text="Offline" />
                        </asp:DropDownList>
                     </div>
                  </div>
               </div>
               <!-- End Between Break Time & Other -->

               <!-- Start Button Save & Cancel -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <asp:LinkButton CssClass="btn btn-success" runat="server" CommandName="btnUpdateShiftWork" CommandArgument='<%# Eval("m0_parttime_idx") %>' 
                           ValidationGroup="saveShiftWorkUpdate" OnCommand="btnCommand">บันทึกการเปลี่ยนแปลง</asp:LinkButton>
                        <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>
                     </div>
                  </div>
               </div>
               <!-- End Button Save & Cancel -->
            </div>
            <!-- End (1)Shift Work Content -->
         </div>
         <!-- End Tab Content -->
      </EditItemTemplate>
      <%-- END Update Form --%>

      <%-- START Read Description --%>
      <ItemTemplate>
         <div class="row">
            <div class="col-md-12">
               <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger m-b-10" runat="server" OnCommand="btnCommand" CommandName="btnCancel"><i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ</asp:LinkButton>
            </div>
         </div>
         <!-- START Tab Menu -->
         <div class="row">
            <div class="col-md-12">
               <nav class="navbar navbar-default">
                  <div class="container-fluid">
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                           <span class="sr-only">Toggle navigation</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                        </button>
                        <span class="navbar-brand">Menu</span>
                     </div>
                     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav" role="tablist">
                           <li class="active"><a href="#shift-work-read" aria-controls="shift-work-read" role="tab" data-toggle="tab">ข้อตกลง <span class="sr-only">(current)</span></a></li>
                           <%--<li><a href="#add-money-read" aria-controls="add-money-read" role="tab" data-toggle="tab">เงื่อนไขข้อตกลงเงินเพิ่ม</a></li>
                           <li><a href="#less-money-read" aria-controls="less-money-read" role="tab" data-toggle="tab">เงื่อนไขข้อตกลงเงินหัก</a></li>--%>
                        </ul>
                     </div>
                  </div>
               </nav>
            </div>
         </div>
         <!-- END Tab Menu -->

         <!-- START Tab Content -->
         <div class="tab-content">
            <!-- Start (1)Shift Work Content -->
            <div role="tabpanel" class="tab-pane in active" id="shift-work-read">
               <!-- Start Work Description -->
               <div class="col-md-12">
                  <div class="panel panel-info">
                     <div class="panel-heading f-bold">รายละเอียดข้อตกลง</div>
                     <div class="panel-body">
                        <div class="form-group">
                           <asp:Label ID="lblParttimeCode" CssClass="col-md-3 text-right m-b-5" runat="server"
                              Text="รหัสข้อตกลง : " />
                           <div class="col-md-9 m-b-5">
                              <asp:Label ID="parttimeCode" runat="server"
                                 Text='<%# Eval("parttime_code").ToString() == "" ? "-" : Eval("parttime_code") %>' />
                           </div>
                           <asp:Label ID="lblParttimeNameTH" CssClass="col-md-3 text-right m-b-5" runat="server"
                              Text="ชื่อข้อตกลงภาษาไทย : " />
                           <div class="col-md-9 m-b-5">
                              <asp:Label ID="parttimeNameTH" runat="server"
                                 Text='<%# Eval("parttime_name_th").ToString() == "" ? "-" : Eval("parttime_name_th") %>' />
                           </div>
                           <asp:Label ID="lblParttimeNameENG" CssClass="col-md-3 text-right m-b-5" runat="server"
                              Text="ชื่อข้อตกลงภาษาอังกฤษ : " />
                           <div class="col-md-9 m-b-5">
                              <asp:Label ID="parttimeNameENG" runat="server"
                                 Text='<%# Eval("parttime_name_eng").ToString() == "" ? "-" : Eval("parttime_name_eng") %>' />
                           </div>
                           <asp:Label ID="lblParttimeNote" CssClass="col-md-3 text-right" runat="server"
                              Text="หมายเหตุ : " />
                           <div class="col-md-9 m-b-5">
                              <asp:Label ID="parttimeNote" runat="server"
                                 Text='<%# Eval("parttime_note").ToString() == "" ? "-" : Eval("parttime_note") %>' />
                           </div>
                           <asp:Label ID="lblParttimeBgColor" CssClass="col-md-3 text-right" runat="server"
                              Text="สีข้อตกลง : " />
                           <div class="col-md-9">
                              <%# getColorParttime((string)Eval("parttime_bg_color")) %>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- End Work Description -->

               <!-- Start Work Between Date Time -->
               <div class="col-md-6">
                  <div class="panel panel-info">
                     <div class="panel-heading f-bold">ช่วงเวลางาน</div>
                     <div class="panel-body">
                        <div class="form-group">
                           <asp:Label ID="lblStartDateTime" CssClass="col-md-5 text-right m-b-5" runat="server"
                              Text="วัน-เวลาเริ่มงาน : " />
                           <div class="col-md-7 m-b-5">
                              <asp:Label ID="startDateTime" runat="server"
                                 Text='<%# getWhenDate((int)Eval("parttime_start_date")) + " " + Eval("parttime_start_time") + " น." %>' />
                           </div>
                           <asp:Label ID="lblEndDateTime" CssClass="col-md-5 text-right" runat="server"
                              Text="วัน-เวลาเลิกงาน : " />
                           <div class="col-md-7">
                              <asp:Label ID="endDateTime" runat="server"
                                 Text='<%# getWhenDate((int)Eval("parttime_end_date")) + " " + Eval("parttime_end_time") + " น." %>' />
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- End Work Between Date Time -->

               <!-- Start Work Into Date Time -->
               <div class="col-md-6">
                  <div class="panel panel-info">
                     <div class="panel-heading f-bold">ช่วงเวลาบันทึกเข้างาน</div>
                     <div class="panel-body">
                        <div class="form-group">
                           <asp:Label ID="lblBtwStartDateTime" CssClass="col-md-5 text-right m-b-5" runat="server"
                              Text="ตั้งแต่ : " />
                           <div class="col-md-7 m-b-5">
                              <asp:Label ID="btwStartDateTime" runat="server"
                                 Text='<%# getWhenDate((int)Eval("parttime_btw_start_date")) + " " + Eval("parttime_btw_start_time") + " น." %>' />
                           </div>
                           <asp:Label ID="lblBtwEndDateTime" CssClass="col-md-5 text-right" runat="server"
                              Text="ถึง : " />
                           <div class="col-md-7">
                              <asp:Label ID="btwEndDateTime" runat="server"
                                 Text='<%# getWhenDate((int)Eval("parttime_btw_end_date")) + " " + Eval("parttime_btw_end_time") + " น." %>' />
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- End Work Into Date Time -->

               <!-- Start Break Date Time -->
               <div class="col-md-6">
                  <div class="panel panel-info">
                     <div class="panel-heading f-bold">พักระหว่างงาน</div>
                     <div class="panel-body">
                        <asp:Label ID="lblNotFoundForCheck" Visible="false" runat="server"
                           Text='<%# getWhenDate((int)Eval("parttime_break_start_date")) + " " + Eval("parttime_break_end_time") %>' />
                        <div id="divData" runat="server">
                           <div class="form-group">
                              <asp:Label ID="lblBreakStartDateTime" CssClass="col-md-5 text-right m-b-5" runat="server"
                                 Text="ตั้งแต่ : " />
                              <div class="col-md-7 m-b-5">
                                 <asp:Label ID="breakStartDateTime" runat="server"
                                    Text='<%# getWhenDate((int)Eval("parttime_break_start_date")) + " " + Eval("parttime_break_start_time") + " น." %>' />
                              </div>
                              <asp:Label ID="lblBreakEndDateTime" CssClass="col-md-5 text-right" runat="server"
                                 Text="ถึง : " />
                              <div class="col-md-7">
                                 <asp:Label ID="breakEndDateTime" runat="server"
                                    Text='<%# getWhenDate((int)Eval("parttime_break_end_date")) + " " + Eval("parttime_break_end_time") + " น." %>' />
                              </div>
                           </div>
                        </div>
                        <asp:Label ID="lblNotFound" Visible="false" CssClass="col-md-12 text-center" runat="server" Text="ไม่มีการพักระหว่างงาน" />
                     </div>
                  </div>
               </div>
               <!-- End Break Date Time -->

               <!-- Start Others -->
               <div class="col-md-6">
                  <div class="panel panel-info">
                     <div class="panel-heading f-bold">อื่นๆ</div>
                     <div class="panel-body">
                        <div class="form-group">
                           <asp:Label ID="lblAmountScanCard" CssClass="col-md-5 text-right m-b-5" runat="server"
                              Text="จำนวนครั้งที่รูดบัตร : " />
                           <div class="col-md-7 m-b-5">
                              <asp:Label ID="amountScanCard" runat="server"
                                 Text='<%# Eval("parttime_amount_scan_card") %>' />
                           </div>
                           <asp:Label ID="lblStatusTimeStand" CssClass="col-md-5 text-right" runat="server"
                              Text="เวลางานคงที่ : " />
                           <div class="col-md-7 m-b-5">
                              <asp:Label ID="statusTimeStand" runat="server"
                                 Text='<%# Eval("status_time_stand") %>' />
                           </div>
                           <asp:Label ID="lblAmountWorkHours" CssClass="col-md-5 text-right" runat="server"
                              Text="จำนวน ชม. งาน : " />
                           <div class="col-md-7 m-b-5">
                              <asp:Label ID="amountWorkHours" runat="server"
                                 Text='<%# Eval("parttime_amount_work_hours") %>' />
                           </div>
                           <asp:Label ID="lblAmountBreakHours" CssClass="col-md-5 text-right" runat="server"
                              Text="จำนวน ชม. พัก : " />
                           <div class="col-md-7 m-b-5">
                              <asp:Label ID="amountBreakHours" runat="server"
                                 Text='<%# Eval("parttime_amount_break_hours") %>' />
                           </div>
                           <asp:Label ID="lblReward" CssClass="col-md-5 text-right" runat="server"
                              Text="ผลตอบแทน : " />
                           <div class="col-md-7">
                              <asp:Label ID="reward" runat="server"
                                 Text='<%# Eval("reward_name") %>' />
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- End Others -->
               <div class="clearfix"></div>
            </div>
            <!-- End (1)Shift Work Content -->

            <!-- Start (2)Add Money Content -->
            <div role="tabpanel" class="tab-pane" id="add-money-read">
               bbbb
            </div>
            <!-- End (2)Add Money Content -->

            <!-- Start (3)Less Money Content -->
            <div role="tabpanel" class="tab-pane" id="less-money-read">
               cccc
            </div>
            <!-- End (3)Less Money Content -->
         </div>
         <!-- END Tab Content -->
      </ItemTemplate>
      <%-- END Read Description --%>
   </asp:FormView>
   <!-- END Parttime Create & Update & Read Form View -->

</asp:Content>

