﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{
   public void ProcessRequest(HttpContext context)
   {
      string callback = context.Request.QueryString["callback"];
      int changeAnnounceIdxRef = 0;
      int parttimeIdx = 0;
      string dateEvent = String.Empty;
      int.TryParse(context.Request.QueryString["changeAnnounceIdxRef"], out changeAnnounceIdxRef);
      int.TryParse(context.Request.QueryString["parttimeIdx"], out parttimeIdx);
      dateEvent = context.Request.QueryString["dateEvent"];
      string json = this.createChangeEmpShift(changeAnnounceIdxRef, parttimeIdx, dateEvent);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string createChangeEmpShift(int changeAnnounceIdxRef, int parttimeIdx, string dateEvent)
   {
      List<object> emps = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            int statusOnline = 1;
            cmd.Connection = conn;
            conn.Open();
            cmd.CommandText = "INSERT INTO emps_u1_change_announce (u0_change_announce_idx_ref, m0_parttime_idx_ref, u1_change_announce_date, u1_change_announce_status) VALUES (@changeAnnounceIdxRef, @parttimeIdx, @dateEvent, " + statusOnline + ")";
            cmd.Parameters.AddWithValue("@changeAnnounceIdxRef", changeAnnounceIdxRef);
            cmd.Parameters.AddWithValue("@parttimeIdx", parttimeIdx);
            cmd.Parameters.AddWithValue("@dateEvent", dateEvent);
            cmd.ExecuteNonQuery();
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emps));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}
