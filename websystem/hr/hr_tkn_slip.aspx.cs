using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ConfidentialProtect;
using SelectPdf;

public partial class websystem_hr_hr_tkn_slip : System.Web.UI.Page {
    #region initial function/data
    function_tool _funcTool = new function_tool ();
    CProtect _cProtect = new CProtect ();

    data_employee _data_employee = new data_employee ();
    data_hr_payroll _data_hr_payroll = new data_hr_payroll ();

    int _emp_idx = 0;
    int _default_int = 0;
    int _serviceType = 1;
    bool _b_permission = false;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile_Mobile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile_Mobile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlSlipGetTKNSlipData = _serviceUrl + ConfigurationManager.AppSettings["urlSlipGetTKNSlipData"];
    static string _urlSlipSetTKNSlipData = _serviceUrl + ConfigurationManager.AppSettings["urlSlipSetTKNSlipData"];

    static string _path_file_hr_eslip = ConfigurationManager.AppSettings["path_file_hr_eslip"];

    static int[] _permission = { 172, 23069 };
    #endregion initial function/data

    protected void Page_Init (object sender, EventArgs e) {
        _emp_idx = _funcTool.convertToInt (Session["emp_idx"].ToString ());

        // check permission
        foreach (int _pass in _permission) {
            if (_emp_idx == _pass) {
                _b_permission = true;
                continue;
            }
        }

        // if (!_b_permission) {
        //     Response.Redirect (ResolveUrl ("~/"));
        // }
    }

    protected void Page_Load (object sender, EventArgs e) {
        if (!IsPostBack) {
            initPage ();
        }

        // if (Session["auth"] == null) {
        //     setActiveTab ("viewAuth", 0);
        // }

        setTrigger ();
    }

    #region event command
    protected void navCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "navList":
                clearViewState ();
                setActiveTab ("viewList", 0);
                break;
            case "navImport":
                clearViewState ();
                setActiveTab ("viewImport", 0);
                break;
            case "navReport":
                clearViewState ();
                setActiveTab ("viewReport", 0);
                break;
        }
    }

    protected void btnCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "cmdAuth":
                // check password
                _data_employee = callServiceGetEmployee (_urlGetMyProfile_Mobile + _emp_idx.ToString ());
                try {
                    if (_funcTool.getMd5Sum (tbPassword.Text.Trim ()) == _data_employee.employee_list[0].emp_password) {
                        Session["auth"] = _funcTool.getRandomPasswordUsingGuid (32);
                        setActiveTab ("viewList", 0);
                    } else {
                        ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('รหัสผ่านไม่ถูกต้อง');", true);
                    }
                } catch (Exception ex) {
                    ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('รหัสผ่านไม่ถูกต้อง');", true);
                }
                break;
            case "cmdImport":
                gvImport.Visible = false;

                TextBox tbPayDate = (TextBox) fvUploadFiles.FindControl ("tbPayDate");
                FileUpload fuUpload = (FileUpload) fvUploadFiles.FindControl ("fuUpload");
                if (fuUpload.HasFile) {
                    ViewState["ImportData"] = null;

                    string dateTimeNow = DateTime.Now.ToString ("yyyyMMdd_HHmmss_ffff");
                    string FileName = Path.GetFileName (fuUpload.PostedFile.FileName);
                    string extension = Path.GetExtension (fuUpload.PostedFile.FileName);
                    string newFileName = dateTimeNow + extension.ToLower ();
                    string folderPath = _path_file_hr_eslip;
                    string filePath = Server.MapPath (folderPath + newFileName);
                    if (extension.ToLower () == ".xlsx") //extension.ToLower() == ".xls" || 
                    {
                        fuUpload.SaveAs (filePath);
                        string conStr = String.Empty;
                        if (extension.ToLower () == ".xls") {
                            conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        } else {
                            conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        }

                        conStr = String.Format (conStr, filePath, "Yes");
                        OleDbConnection connExcel = new OleDbConnection (conStr);
                        OleDbCommand cmdExcel = new OleDbCommand ();
                        OleDbDataAdapter oda = new OleDbDataAdapter ();
                        DataTable dt = new DataTable ();

                        cmdExcel.Connection = connExcel;
                        connExcel.Open ();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable (OleDbSchemaGuid.Tables, null);
                        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString ();
                        connExcel.Close ();
                        connExcel.Open ();
                        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                        oda.SelectCommand = cmdExcel;
                        oda.Fill (dt);
                        connExcel.Close ();

                        File.Delete (filePath);

                        DataTable tableImport = new DataTable ();

                        int _count_row = 0;
                        int _count_table_row = dt.Rows.Count;
                        int _count_dt = 0;

                        gvImport.Visible = true;
                        tableImport.Columns.Add ("4001", typeof (String)); //รหัส
                        tableImport.Columns.Add ("4002", typeof (String)); //ชื่อพนักงาน
                        tableImport.Columns.Add ("4003", typeof (String)); //วันที่เรื่มงาน
                        tableImport.Columns.Add ("4004", typeof (String)); //สาขา
                        tableImport.Columns.Add ("4005", typeof (String)); //ประเภทการจ้าง
                        tableImport.Columns.Add ("4006", typeof (String)); //แผนก
                        tableImport.Columns.Add ("4007", typeof (String)); //ตำแหน่ง
                        tableImport.Columns.Add ("4008", typeof (String)); //เลขที่บัญชี
                        tableImport.Columns.Add ("4009", typeof (String)); //วันที่จ่าย
                        tableImport.Columns.Add ("4010", typeof (String)); //เงินได้สะสมต่อปี
                        tableImport.Columns.Add ("4011", typeof (String)); //ภาษีสะสมต่อปี
                        tableImport.Columns.Add ("4012", typeof (String)); //เงินสะสมกองทุนต่อปี
                        tableImport.Columns.Add ("4013", typeof (String)); //เงินประกันสังคมต่อปี
                        tableImport.Columns.Add ("4014", typeof (String)); //ค่าสดหย่อนอื่นๆ
                        tableImport.Columns.Add ("1000", typeof (String)); //อัตราเงินเดือน/ค่าตอบแทน
                        tableImport.Columns.Add ("1100", typeof (String)); //เงินเดือน
                        tableImport.Columns.Add ("1230", typeof (String)); //ค่าตำแหน่ง
                        tableImport.Columns.Add ("1272", typeof (String)); //ค่าปฏิบัติงาน (ประจำ)
                        tableImport.Columns.Add ("1440", typeof (String)); //ค่าประสบการณ์
                        tableImport.Columns.Add ("1510", typeof (String)); //ค่าน้ำมัน/เดินทาง
                        tableImport.Columns.Add ("1511", typeof (String)); //ค่าเดินทางอื่นๆ
                        tableImport.Columns.Add ("1310", typeof (String)); //ค่าที่พัก
                        tableImport.Columns.Add ("1270", typeof (String)); //ค่าชำนาญ
                        tableImport.Columns.Add ("1410", typeof (String)); //ค่าทักษะ
                        tableImport.Columns.Add ("1350", typeof (String)); //ค่าโทรศัพท์
                        tableImport.Columns.Add ("1551", typeof (String)); //ค่าซ่อมบำรุงรถ
                        tableImport.Columns.Add ("1271", typeof (String)); //ค่าปฏิบัติงาน SAP
                        tableImport.Columns.Add ("1341", typeof (String)); //เงินช่วยเหลือคนพิการ
                        tableImport.Columns.Add ("1225", typeof (String)); //ประธานกรรมการบริษัท
                        tableImport.Columns.Add ("1226", typeof (String)); //กรรมการและกรรมการบริหาร
                        tableImport.Columns.Add ("1224", typeof (String)); //กรรมการอิสระ
                        tableImport.Columns.Add ("1228", typeof (String)); //ประธานกรรมการตรวจสอบ
                        tableImport.Columns.Add ("1227", typeof (String)); //กรรมการตรวจสอบ
                        tableImport.Columns.Add ("1412", typeof (String)); //เบี้ยขยัน(พิเศษเหมาประจำ)
                        tableImport.Columns.Add ("1555", typeof (String)); //โบนัส(พิเศษ)
                        tableImport.Columns.Add ("1554", typeof (String)); //โบนัสประจำปี
                        tableImport.Columns.Add ("1430", typeof (String)); //ค่าสวัสดิการ
                        tableImport.Columns.Add ("1250", typeof (String)); //ค่าเบี้ยประชุม(Board)
                        tableImport.Columns.Add ("1444", typeof (String)); //ค่าทอด
                        tableImport.Columns.Add ("1531", typeof (String)); //ค่าจ้างค้างรับ
                        tableImport.Columns.Add ("1530", typeof (String)); //เงินได้ค้างรับ
                        tableImport.Columns.Add ("1550", typeof (String)); //เงินได้อื่นๆ
                        tableImport.Columns.Add ("1342", typeof (String)); //เงินช่วยเหลือพิเศษ
                        tableImport.Columns.Add ("1273", typeof (String)); //ค่าปฏิบัติงาน (พิเศษ)
                        tableImport.Columns.Add ("1567", typeof (String)); //ค่าโอนย้าย RJN
                        tableImport.Columns.Add ("1552", typeof (String)); //คืนเงินภาษี
                        tableImport.Columns.Add ("1557", typeof (String)); //คืนเงินประกันสังคม
                        tableImport.Columns.Add ("1423", typeof (String)); //คืนเงินพักร้อน
                        tableImport.Columns.Add ("1541", typeof (String)); //Incentive
                        tableImport.Columns.Add ("1422", typeof (String)); //เงินค่าบอกกล่าวล่วงหน้า
                        tableImport.Columns.Add ("1424", typeof (String)); //เงินเกษียณ
                        tableImport.Columns.Add ("1229", typeof (String)); //ค่าตอบแทนพิเศษ
                        tableImport.Columns.Add ("1421", typeof (String)); //เงินค่าชดเชยกรณีเลิกจ้าง
                        tableImport.Columns.Add ("1110", typeof (String)); //ค่าล่วงเวลา 1.0
                        tableImport.Columns.Add ("1120", typeof (String)); //ค่าล่วงเวลา 1.5
                        tableImport.Columns.Add ("1140", typeof (String)); //ค่าล่วงเวลา 3.0
                        tableImport.Columns.Add ("1153", typeof (String)); //ค่าล่วงเวลาค้างรับ
                        tableImport.Columns.Add ("1210", typeof (String)); //ค่ากะ
                        tableImport.Columns.Add ("1211", typeof (String)); //ค่ากะ(คนลาออกไม่คิด ปกสค.)
                        tableImport.Columns.Add ("1260", typeof (String)); //เบี้ยเลี้ยง
                        tableImport.Columns.Add ("1411", typeof (String)); //เบี้ยขยัน
                        tableImport.Columns.Add ("1152", typeof (String)); //ค่าล่วงเวลา(เหมาไม่คงที่)
                        tableImport.Columns.Add ("1151", typeof (String)); //ค่าล่วงเวลา(เหมาวันหยุดนักขัตฯ)
                        tableImport.Columns.Add ("1150", typeof (String)); //ค่าล่วงเวลาเหมา(เงินได้ประจำ)
                        tableImport.Columns.Add ("1999", typeof (String)); //รวมเงินได้
                        tableImport.Columns.Add ("2110", typeof (String)); //หักขาด
                        tableImport.Columns.Add ("2119", typeof (String)); //Leave without pay
                        tableImport.Columns.Add ("2192", typeof (String)); //หักวันหยุด(ไม่รับค่าจ้าง)
                        tableImport.Columns.Add ("2410", typeof (String)); //ไม่สแกนนิ้วเข้า(หักขาดงาน)
                        tableImport.Columns.Add ("2420", typeof (String)); //ไม่สแกนนิ้วออก(หักขาดงาน)
                        tableImport.Columns.Add ("2190", typeof (String)); //หักพักงาน
                        tableImport.Columns.Add ("2151", typeof (String)); //หักลากิจ
                        tableImport.Columns.Add ("2116", typeof (String)); //จ่ายค่าแรง 75% /วัน
                        tableImport.Columns.Add ("2150", typeof (String)); //หักลากิจ(เกิน 7 วัน)
                        tableImport.Columns.Add ("2131", typeof (String)); //หักลาป่วยเกิน 30 วัน
                        tableImport.Columns.Add ("2180", typeof (String)); //หักลาบวช (วันที่ 8-15 นับรวมวันหยุด)
                        tableImport.Columns.Add ("2142", typeof (String)); //ป่วยในงาน(จ่าย40% ใบรับรองฯไม่เกิน 1 ปี)
                        tableImport.Columns.Add ("2171", typeof (String)); //หักลาคลอด(วันที่ 46-98)
                        tableImport.Columns.Add ("2155", typeof (String)); //หักลากิจ(เกิน 7 วัน)
                        tableImport.Columns.Add ("2141", typeof (String)); //หักลากิจ(พิเศษ)
                        tableImport.Columns.Add ("2114", typeof (String)); //หักค่าแรง (บาท)
                        tableImport.Columns.Add ("2115", typeof (String)); //หักค่าแรง (วัน)
                        tableImport.Columns.Add ("2342", typeof (String)); //หักคืนค่าล่วงเวลา
                        tableImport.Columns.Add ("2350", typeof (String)); //เงินหักธนาคารอาคารสงเคราะห์
                        tableImport.Columns.Add ("2351", typeof (String)); //เงินหักกรมบังคับคดี
                        tableImport.Columns.Add ("2353", typeof (String)); //เงินหักกยศ.
                        tableImport.Columns.Add ("2354", typeof (String)); //เงินหักกรอ.
                        tableImport.Columns.Add ("2341", typeof (String)); //หักอื่นๆ(คิดภาษี)
                        tableImport.Columns.Add ("2352", typeof (String)); //เงินหักชำระสินเชื่อ​ Pico
                        tableImport.Columns.Add ("2901", typeof (String)); //เงินสะสมประกันสังคม
                        tableImport.Columns.Add ("2902", typeof (String)); //เงินสมทบกองทุนสำรองเลี้ยงชีพลูกจ้าง
                        tableImport.Columns.Add ("2903", typeof (String)); //ภาษี
                        tableImport.Columns.Add ("2999", typeof (String)); //รวมเงินหัก
                        tableImport.Columns.Add ("3000", typeof (String)); //เงินได้สุทธิ
                        tableImport.Columns.Add ("5110", typeof (String)); //ค่าล่วงเวลา 1.0(จำนวน)
                        tableImport.Columns.Add ("5120", typeof (String)); //ค่าล่วงเวลา 1.5(จำนวน)
                        tableImport.Columns.Add ("5140", typeof (String)); //ค่าล่วงเวลา 3.0(จำนวน)
                        tableImport.Columns.Add ("6152", typeof (String)); //ลากิจ(จ่ายค่าจ้าง)(จำนวน)
                        tableImport.Columns.Add ("6140", typeof (String)); //ลาป่วยมีใบรับรองแพทย์(จำนวน)
                        tableImport.Columns.Add ("6130", typeof (String)); //ลาป่วยไม่มีใบรับรองแพทย์(จำนวน)
                        tableImport.Columns.Add ("6160", typeof (String)); //พักผ่อนประจำปี(จำนวน)
                        tableImport.Columns.Add ("6162", typeof (String)); //ลาวันเกิด(จำนวน)
                        tableImport.Columns.Add ("6170", typeof (String)); //ลาคลอด( วันที่ 1-45 )(จำนวน)
                        tableImport.Columns.Add ("6163", typeof (String)); //ลาดูแลภรรยาคลอดบุตร(จำนวน)
                        tableImport.Columns.Add ("6181", typeof (String)); //ลาบวช (จ่ายค่าจ้าง 1-7 วันแรก)(จำนวน)
                        tableImport.Columns.Add ("6183", typeof (String)); //ลารับราชการทหาร(60วัน)(จำนวน)
                        tableImport.Columns.Add ("6182", typeof (String)); //ลาทำหมันชาย(จ่ายค่าจ้างตามใบรับรองแทพย์)(จำนวน) 

                        for (int i = 1; i <= _count_table_row - 1; i++) {
                            if (
                                (dt.Rows[i][0].ToString ().Trim () != String.Empty)
                            ) {
                                DataRow add_row = tableImport.NewRow ();

                                add_row[0] = dt.Rows[i][0].ToString ().Trim (); //dt.Rows[i][0].ToString ().Trim ().Replace (",", "");
                                add_row[1] = dt.Rows[i][1].ToString ().Trim ();
                                add_row[2] = ((dt.Rows[i][2].ToString ().Trim ()).Split(' '))[0];
                                add_row[3] = dt.Rows[i][3].ToString ().Trim ();
                                add_row[4] = dt.Rows[i][4].ToString ().Trim ();
                                add_row[5] = dt.Rows[i][5].ToString ().Trim ();
                                add_row[6] = dt.Rows[i][6].ToString ().Trim ();
                                add_row[7] = dt.Rows[i][7].ToString ().Trim ();
                                add_row[8] = ((dt.Rows[i][8].ToString ().Trim ()).Split(' '))[0];
                                add_row[9] = dt.Rows[i][9].ToString ().Trim ();
                                add_row[10] = dt.Rows[i][10].ToString ().Trim ();
                                add_row[11] = dt.Rows[i][11].ToString ().Trim ();
                                add_row[12] = dt.Rows[i][12].ToString ().Trim ();
                                add_row[13] = dt.Rows[i][13].ToString ().Trim ();
                                add_row[14] = dt.Rows[i][14].ToString ().Trim ();
                                add_row[15] = dt.Rows[i][15].ToString ().Trim ();
                                add_row[16] = dt.Rows[i][16].ToString ().Trim ();
                                add_row[17] = dt.Rows[i][17].ToString ().Trim ();
                                add_row[18] = dt.Rows[i][18].ToString ().Trim ();
                                add_row[19] = dt.Rows[i][19].ToString ().Trim ();
                                add_row[20] = dt.Rows[i][20].ToString ().Trim ();
                                add_row[21] = dt.Rows[i][21].ToString ().Trim ();
                                add_row[22] = dt.Rows[i][22].ToString ().Trim ();
                                add_row[23] = dt.Rows[i][23].ToString ().Trim ();
                                add_row[24] = dt.Rows[i][24].ToString ().Trim ();
                                add_row[25] = dt.Rows[i][25].ToString ().Trim ();
                                add_row[26] = dt.Rows[i][26].ToString ().Trim ();
                                add_row[27] = dt.Rows[i][27].ToString ().Trim ();
                                add_row[28] = dt.Rows[i][28].ToString ().Trim ();
                                add_row[29] = dt.Rows[i][29].ToString ().Trim ();
                                add_row[30] = dt.Rows[i][30].ToString ().Trim ();
                                add_row[31] = dt.Rows[i][31].ToString ().Trim ();
                                add_row[32] = dt.Rows[i][32].ToString ().Trim ();
                                add_row[33] = dt.Rows[i][33].ToString ().Trim ();
                                add_row[34] = dt.Rows[i][34].ToString ().Trim ();
                                add_row[35] = dt.Rows[i][35].ToString ().Trim ();
                                add_row[36] = dt.Rows[i][36].ToString ().Trim ();
                                add_row[37] = dt.Rows[i][37].ToString ().Trim ();
                                add_row[38] = dt.Rows[i][38].ToString ().Trim ();
                                add_row[39] = dt.Rows[i][39].ToString ().Trim ();
                                add_row[40] = dt.Rows[i][40].ToString ().Trim ();
                                add_row[41] = dt.Rows[i][41].ToString ().Trim ();
                                add_row[42] = dt.Rows[i][42].ToString ().Trim ();
                                add_row[43] = dt.Rows[i][43].ToString ().Trim ();
                                add_row[44] = dt.Rows[i][44].ToString ().Trim ();
                                add_row[45] = dt.Rows[i][45].ToString ().Trim ();
                                add_row[46] = dt.Rows[i][46].ToString ().Trim ();
                                add_row[47] = dt.Rows[i][47].ToString ().Trim ();
                                add_row[48] = dt.Rows[i][48].ToString ().Trim ();
                                add_row[49] = dt.Rows[i][49].ToString ().Trim ();
                                add_row[50] = dt.Rows[i][50].ToString ().Trim ();
                                add_row[51] = dt.Rows[i][51].ToString ().Trim ();
                                add_row[52] = dt.Rows[i][52].ToString ().Trim ();
                                add_row[53] = dt.Rows[i][53].ToString ().Trim ();
                                add_row[54] = dt.Rows[i][54].ToString ().Trim ();
                                add_row[55] = dt.Rows[i][55].ToString ().Trim ();
                                add_row[56] = dt.Rows[i][56].ToString ().Trim ();
                                add_row[57] = dt.Rows[i][57].ToString ().Trim ();
                                add_row[58] = dt.Rows[i][58].ToString ().Trim ();
                                add_row[59] = dt.Rows[i][59].ToString ().Trim ();
                                add_row[60] = dt.Rows[i][60].ToString ().Trim ();
                                add_row[61] = dt.Rows[i][61].ToString ().Trim ();
                                add_row[62] = dt.Rows[i][62].ToString ().Trim ();
                                add_row[63] = dt.Rows[i][63].ToString ().Trim ();
                                add_row[64] = dt.Rows[i][64].ToString ().Trim ();
                                add_row[65] = dt.Rows[i][65].ToString ().Trim ();
                                add_row[66] = dt.Rows[i][66].ToString ().Trim ();
                                add_row[67] = dt.Rows[i][67].ToString ().Trim ();
                                add_row[68] = dt.Rows[i][68].ToString ().Trim ();
                                add_row[69] = dt.Rows[i][69].ToString ().Trim ();
                                add_row[70] = dt.Rows[i][70].ToString ().Trim ();
                                add_row[71] = dt.Rows[i][71].ToString ().Trim ();
                                add_row[72] = dt.Rows[i][72].ToString ().Trim ();
                                add_row[73] = dt.Rows[i][73].ToString ().Trim ();
                                add_row[74] = dt.Rows[i][74].ToString ().Trim ();
                                add_row[75] = dt.Rows[i][75].ToString ().Trim ();
                                add_row[76] = dt.Rows[i][76].ToString ().Trim ();
                                add_row[77] = dt.Rows[i][77].ToString ().Trim ();
                                add_row[78] = dt.Rows[i][78].ToString ().Trim ();
                                add_row[79] = dt.Rows[i][79].ToString ().Trim ();
                                add_row[80] = dt.Rows[i][80].ToString ().Trim ();
                                add_row[81] = dt.Rows[i][81].ToString ().Trim ();
                                add_row[82] = dt.Rows[i][82].ToString ().Trim ();
                                add_row[83] = dt.Rows[i][83].ToString ().Trim ();
                                add_row[84] = dt.Rows[i][84].ToString ().Trim ();
                                add_row[85] = dt.Rows[i][85].ToString ().Trim ();
                                add_row[86] = dt.Rows[i][86].ToString ().Trim ();
                                add_row[87] = dt.Rows[i][87].ToString ().Trim ();
                                add_row[88] = dt.Rows[i][88].ToString ().Trim ();
                                add_row[89] = dt.Rows[i][89].ToString ().Trim ();
                                add_row[90] = dt.Rows[i][90].ToString ().Trim ();
                                add_row[91] = dt.Rows[i][91].ToString ().Trim ();
                                add_row[92] = dt.Rows[i][92].ToString ().Trim ();
                                add_row[93] = dt.Rows[i][93].ToString ().Trim ();
                                add_row[94] = dt.Rows[i][94].ToString ().Trim ();
                                add_row[95] = dt.Rows[i][95].ToString ().Trim ();
                                add_row[96] = dt.Rows[i][96].ToString ().Trim ();
                                add_row[97] = dt.Rows[i][97].ToString ().Trim ();
                                add_row[98] = dt.Rows[i][98].ToString ().Trim ();
                                add_row[99] = dt.Rows[i][99].ToString ().Trim ();
                                add_row[100] = dt.Rows[i][100].ToString ().Trim ();
                                add_row[101] = dt.Rows[i][101].ToString ().Trim ();
                                add_row[102] = dt.Rows[i][102].ToString ().Trim ();
                                add_row[103] = dt.Rows[i][103].ToString ().Trim ();
                                add_row[104] = dt.Rows[i][104].ToString ().Trim ();
                                add_row[105] = dt.Rows[i][105].ToString ().Trim ();
                                add_row[106] = dt.Rows[i][106].ToString ().Trim ();

                                tableImport.Rows.InsertAt (add_row, _count_row++);
                            }

                            if (dt.Rows.Count > 0) {
                                ViewState["ExportData"] = tableImport;
                                _funcTool.setGvData (gvImport, tableImport);
                                // _funcTool.setGvData (gvTestTranspose, dtConvertSetData (tableImport));
                            } 
                        }
                    }
                } else {
                    ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('ไม่พบไฟล์ที่ต้องการอัพโหลดค่ะ');", true);
                    return;
                }
                break;
        }
    }
    #endregion event command

    #region reuse
    protected void initPage () {
        clearSession ();
        clearViewState ();

        // setActiveTab ("viewAuth", 0);
        // setActiveTab ("viewList", 0);
        setActiveTab ("viewImport", 0);

        hlSetTotop.Focus ();
    }

    protected void clearSession () {
        Session["auth"] = null;
    }

    protected void clearViewState () {
        ViewState["ExportData"] = null;
    }

    protected void setActiveTab (string activeTab, int doc_idx) {
        setActiveView (activeTab, doc_idx);
        setActiveTabBar (activeTab);

        divMenu.Style.Remove ("display");
        lbRelease.Visible = false;
        lbSaveData.Visible = false;

        switch (activeTab) {
            case "viewAuth":
                divMenu.Style.Add ("display", "none");
                break;
            case "viewList":
                lbRelease.Visible = true;
                ViewState["ImportData"] = null;
                // search_slip_data_detail _search_slip_list = new search_slip_data_detail ();
                // _search_slip_list.s_u0_idx = "0";
                // _search_slip_list.s_emp_idx = _emp_idx.ToString ();
                // _data_hr_payroll.search_slip_data_list = new search_slip_data_detail[1];
                // _data_hr_payroll.search_slip_data_list[0] = _search_slip_list;
                // //  litDebug.Text = _funcTool.convertObjectToJson(_data_hr_payroll);
                // _data_hr_payroll = callServicePostHrPayroll (_urlSlipGetTKNSlipData, _data_hr_payroll);
                // _funcTool.setGvData (gvList, _data_hr_payroll.en_slip_data_list_u0);
                break;
            case "viewImport":
                lbRelease.Visible = true;
                // ViewState["ImportData"] = null;
                // _funcTool.setFvData (fvUploadFiles, FormViewMode.Insert, null);
                _funcTool.setGvData (gvImport, null);
                _funcTool.setGvData (gvTestTranspose, null);
                // gvImport.Visible = false;
                // gvImport2.Visible = false;
                break;
            case "viewReport":
                lbRelease.Visible = true;
                break;
        }
    }

    protected void setActiveView (string activeTab, int doc_idx) {
        mvSystem.SetActiveView ((View) mvSystem.FindControl (activeTab));
    }

    protected data_employee callServiceGetEmployee (string _cmdUrl) {
        // call services
        _local_json = _funcTool.callServiceGet (_cmdUrl);

        // convert json to object
        _data_employee = (data_employee) _funcTool.convertJsonToObject (typeof (data_employee), _local_json);

        return _data_employee;
    }

    protected data_employee callServicePostEmployee (string _cmdUrl, data_employee _data_employee) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_employee);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);

        // convert json to object
        _data_employee = (data_employee) _funcTool.convertJsonToObject (typeof (data_employee), _local_json);

        return _data_employee;
    }

    protected data_hr_payroll callServicePostHrPayroll (string _cmdUrl, data_hr_payroll _data_hr_payroll) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_hr_payroll);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);

        // convert json to object
        _data_hr_payroll = (data_hr_payroll) _funcTool.convertJsonToObject (typeof (data_hr_payroll), _local_json);

        return _data_hr_payroll;
    }

    protected data_employee getOrganizationList () {
        _data_employee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details ();
        _orgList.org_idx = 0;
        _data_employee.organization_list[0] = _orgList;

        _data_employee = callServicePostEmployee (_urlGetOrganizationList, _data_employee);
        return _data_employee;
    }

    protected void setActiveTabBar (string activeTab) {
        switch (activeTab) {
            case "viewList":
                li0.Attributes.Add ("class", "active");
                li1.Attributes.Add ("class", "");
                li2.Attributes.Add ("class", "");
                break;
            case "viewImport":
                li0.Attributes.Add ("class", "");
                li1.Attributes.Add ("class", "active");
                li2.Attributes.Add ("class", "");
                break;
            case "viewReport":
                li0.Attributes.Add ("class", "");
                li1.Attributes.Add ("class", "");
                li2.Attributes.Add ("class", "active");
                break;
        }
    }

    protected void linkBtnTrigger (LinkButton linkBtnID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger ();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add (triggerLinkBtn);
    }

    protected void linkGvTrigger (GridView linkGvID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerGridView = new PostBackTrigger ();
        triggerGridView.ControlID = linkGvID.UniqueID;
        updatePanel.Triggers.Add (triggerGridView);
    }

    protected void setTrigger () {
        linkBtnTrigger (lbAuth);
        linkBtnTrigger (lbRelease);

        // nav trigger
        linkBtnTrigger (lbList);
        linkBtnTrigger (lbImport);
        linkBtnTrigger (lbReport);

        LinkButton lbImportData = (LinkButton) fvUploadFiles.FindControl ("lbImportData");
        try { linkBtnTrigger (lbImportData); } catch { }
        try { linkBtnTrigger (lbSaveData); } catch { }
        try { linkBtnTrigger (lbExportData); } catch { }

        // slip list
        try { linkGvTrigger (gvList); } catch { }
    }

    protected string getEncrypted (string dataIn) {
        return _cProtect.CProtectEncrypt (dataIn);
    }

    protected string getDecrypted (string dataIn) {
        return _cProtect.CProtectDecrypt (dataIn);
    }

    private DataTable dtConvertSetData (DataTable dt) {
        DataTable dtNew = new DataTable ();

        dtNew.Columns.Add ("emp_code", typeof (String));
        dtNew.Columns.Add ("code_name", typeof (String));
        dtNew.Columns.Add ("pay_value", typeof (String));

        int _count = 0;
        for (int i = 0; i < dt.Rows.Count; i++) {
            for (int j = 0; j < dt.Columns.Count; j++) {
                DataRow dr = dtNew.NewRow ();
                dr[0] = dt.Rows[i][0].ToString ().Trim (); //emp_code
                dr[1] = dt.Columns[j].ColumnName.ToString ();
                dr[2] = dt.Rows[i][j].ToString ().Trim ();

                dtNew.Rows.InsertAt (dr, _count++);
            }
        }
        return dtNew;
    }
    #endregion reuse
}