﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="labis_print_barcode.aspx.cs" Inherits="websystem_qa_labis_print_barcode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

    <title>Print Sample Code</title>

    <style type="text/css" media="print,screen">
        @page {
            /*size: A4 landscape;*/
            size: A4 portrait;
            margin: 25px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

        .formPrint {
            margin: 10pt; /*from 5 to 10*/
            padding: 10pt; /*add*/
            width: 842pt;
        }
    </style>

</head>
<body onload="window.print()">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <form id="form1" runat="server" width="100%">
        <script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>
        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <div>
            <div class="headOrg" style="text-align: center;">
                <%--<strong>Sample list</strong>--%>
            </div>
            <%--    OnRowDataBound="gvRowDataBound"--%>

            <asp:ListView ID="grdDrink" runat="server"
                GroupItemCount="2"
                OnItemDataBound="ContactsListView_ItemDataBound">
                <EmptyDataTemplate>
                    <table class="table" style="width: 100%">
                        <tr>
                            <td>--- No result ---</td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <EmptyItemTemplate>
                    <td />
                </EmptyItemTemplate>
                <GroupTemplate>
                    <tr id="itemPlaceholderContainer" runat="server">
                        <td id="itemPlaceholder" runat="server"></td>
                    </tr>
                </GroupTemplate>
                <ItemTemplate>
                    <td runat="server">
                        <table>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div id="list" runat="server" style="height: 160px;">
                                            <div id="entry">
                                                <div>
                                                    <figure>
                                                        <asp:Label ID="lbPrintU1Doc_1" Visible="false" Text='<%# Eval("u1_qalab_idx") %>' runat="server"></asp:Label>

                                                        <p>
                                                            <b>Lab code :</b>
                                                            <asp:Label ID="lblsampleCode_1" Text='<%# Eval("sample_code") %>' runat="server"></asp:Label>
                                                            <%--<asp:Label ID="lbl_cemp_idx_detail_1" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />--%>
                                                            <%-- <asp:Label ID="lbl_emp_name_th_detail_1" runat="server" Text='<%# Eval("emp_name_th") %>' />--%>
                                                        </p>
                                                        <p>
                                                            <b>Test items :</b>
                                                            <div style="padding: 1px;">
                                                                <table class="table table-striped" style="width: 100%">
                                                                    <asp:Repeater ID="rp_test_sample_1" runat="server">

                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lbltestdetailname_1" runat="server" Text='<%# " - " + Eval("test_detail_name") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="u2_idx_n3_1" runat="server" visible="false">
                                                                                <td>
                                                                                    <asp:Label ID="lblu2qalab_idx_1" runat="server" Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="test_detail_idx_n11_1" runat="server" visible="false">
                                                                                <td>
                                                                                    <asp:Label ID="lbltest_detail_idx_n11_1" runat="server" Text='<%# Eval("test_detail_idx") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>

                                                                    </asp:Repeater>
                                                                </table>
                                                            </div>
                                                        </p>
                                                        <p>
                                                            <b>Barcode :</b>
                                                            <asp:Image ID="ImagShowBarCode_1" Width="100" Height="20" runat="server"></asp:Image>
                                                        </p>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </ItemTemplate>
                <LayoutTemplate>
                    <table class="table" style="width: 100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="groupPlaceholderContainer" runat="server" style="width: 100%">
                                        <tr id="groupPlaceholder"></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr></tr>
                        </tbody>
                    </table>
                </LayoutTemplate>
            </asp:ListView>

            <br />
            <br />
            <br />
            <br />


            <%-- <asp:Repeater ID="rptPrintReciveDate_1"
                OnItemDataBound="rptOnRowDataBound" runat="server">--%>
            <%-- <ItemTemplate>--%>
            <div class="col-md-6">



                <asp:DataList ID="dtlSystem"
                    runat="server"
                    OnItemDataBound="dlDate_ItemDataBound"
                    RepeatColumns="2"
                    CellSpacing="2"
                    RepeatLayout="Table">
                    <ItemTemplate>

                        <div class="panel-body">
                            <div class="panel-heading">ป้ายชี้บ้างตัวอย่าง</div>
                            <table class="table" style="border: 0px">
                                <tr>
                                    <asp:Label ID="lbPrintU1Doc_11" Visible="false" Text='<%# Eval("u1_qalab_idx") %>' runat="server"></asp:Label>
                                    <th colspan="2">
                                        <p>
                                            <b>Lab code :</b>
                                            <asp:Label ID="lblsampleCode_11" Text='<%# Eval("sample_code") %>' runat="server"></asp:Label>
                                            <%--<asp:Label ID="lbl_cemp_idx_detail_1" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />--%>
                                            <%-- <asp:Label ID="lbl_emp_name_th_detail_1" runat="server" Text='<%# Eval("emp_name_th") %>' />--%>
                                        </p>
                                    </th>
                                </tr>
                                <%--<tr>
                                    <td colspan="2">
                                        <%# Eval("EmpDept") %>,  <%# Eval("EmpCity") %><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Salary:  
                                    </td>
                                    <td>
                                        <%# Eval("Salary")%>  
                                    </td>
                                </tr>
                                <tr>
                                    <td>Designation:  
                                    </td>
                                    <td>
                                        <%# Eval("Designation")%>  
                                    </td>
                                </tr>--%>
                            </table>
                        </div>
                    </ItemTemplate>
                </asp:DataList>


            </div>

            <%--  </ItemTemplate>

            </asp:Repeater>--%>


            <table class="table" style="width: 100%">

                <asp:Repeater ID="rptPrintReciveDate" OnItemDataBound="rptOnRowDataBound" runat="server">
                    <ItemTemplate>
                        <tr style="width: 100%">
                            <asp:Label CssClass="btn btn-default tar" ID="lbPrintSampleCodeU1Doc" data-toggle="tooltip" Visible="false" title="Print" runat="server"
                                Text='<%# Eval("u1_qalab_idx") %>'></asp:Label>

                            <asp:GridView ID="gvPrintReciveDate" runat="server"
                                FooterStyle-Height="0" HeaderStyle-Wrap="true"
                                FooterStyle-Wrap="false" AllowPaging="True"
                                AllowSorting="True" RowStyle-Font-Size="8"
                                AutoGenerateColumns="false" HeaderStyle-Width="5%"
                                BackColor="White" BorderWidth="1px" Width="100%"
                                CellPadding="0" Font-Size="10" OnRowDataBound="gvRowDataBound"
                                PageSize="14">
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="ป้ายชี้บ่งตัวอย่าง" HeaderStyle-Width="12%">
                                        <ItemTemplate>

                                            <asp:Label ID="lbPrintU1Doc" Visible="false" Text='<%# Eval("u1_qalab_idx") %>' runat="server"></asp:Label>

                                            <p>
                                                <b>Lab code :</b>
                                                <asp:Label ID="lblsampleCode" Text='<%# Eval("sample_code") %>' runat="server"></asp:Label>
                                                <%--<asp:Label ID="lbl_cemp_idx_detail" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />--%>
                                                <%-- <asp:Label ID="lbl_emp_name_th_detail" runat="server" Text='<%# Eval("emp_name_th") %>' />--%>
                                            </p>
                                            <p>
                                                <b>Test items :</b>
                                                <div style="padding: 1px;">
                                                    <table class="table table-striped" style="width: 50%">
                                                        <asp:Repeater ID="rp_test_sample" runat="server">

                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lbltestdetailname" runat="server" Text='<%# " - " + Eval("test_detail_name") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr id="u2_idx_n3" runat="server" visible="false">
                                                                    <td>
                                                                        <asp:Label ID="lblu2qalab_idx" runat="server" Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr id="test_detail_idx_n11" runat="server" visible="false">
                                                                    <td>
                                                                        <asp:Label ID="lbltest_detail_idx_n11" runat="server" Text='<%# Eval("test_detail_idx") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>

                                                        </asp:Repeater>
                                                    </table>
                                                </div>
                                            </p>
                                            <%--<p>
                                                <b>ฝ่าย:</b>
                                                <asp:Label ID="lbl_dept_name_th_detail" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                            </p>--%>
                                            <p>
                                                <b>Barcode :</b>
                                                <%--<div style="text-align: center">--%>
                                                <asp:Image ID="ImagShowBarCode" Width="100" Height="20" runat="server"></asp:Image>
                                                <%-- </div>--%>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>
                        </tr>
                        <tr>
                            <asp:Label CssClass="btn btn-default tar" ID="sample_code" data-toggle="tooltip" Visible="false" runat="server"
                                Text='<%# Eval("sample_code") %>'></asp:Label>
                        </tr>
                        <br />

                    </ItemTemplate>

                </asp:Repeater>
            </table>
            <div class="pull-right" style="text-align: right;">
                <asp:Label ID="lblPrintDate1" Font-Size="7" Text="PRINT DATE :" runat="server" />
                <span>
                    <asp:Label ID="lblPrintDate" Font-Size="7" runat="server" /></span>

            </div>
            <%-- </div>--%>
            <div class="page-break" id="pb2" runat="server" visible="false"></div>
        </div>
    </form>
</body>
</html>

