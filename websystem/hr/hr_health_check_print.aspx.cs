﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;

public partial class websystem_hr_hr_health_check_print : System.Web.UI.Page
{


    #region Connect
    function_tool _funcTool = new function_tool();
    data_employee _dataEmployee = new data_employee();
    data_hr_healthcheck _data_hr_healthcheck = new data_hr_healthcheck();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];
    static string _urlGetViewEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewEmployeeList"];
    //-- employee --//

    //-- HR HealthCheck --//
    static string _urlGetEmployeeHistory = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployeeHistory"];
    static string _urlGetHistoryType = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryType"];
    static string _urlGetFormTypeHistoryCheck = _serviceUrl + ConfigurationManager.AppSettings["urlGetFormTypeHistoryCheck"];
    static string _urlGetDetailTopicForm = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailTopicForm"];

    static string _urlGetEmployeeProfileHistory = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployeeProfileHistory"];
    static string _urlGetDetailSetTopicForm = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailSetTopicForm"];
    static string _urlGetDetailTypeHealthCheck = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailTypeHealthCheck"];

    static string _urlSetHistoryWorkCheck = _serviceUrl + ConfigurationManager.AppSettings["urlSetHistoryWorkCheck"];
    static string _urlSetHistoryInjuryCheck = _serviceUrl + ConfigurationManager.AppSettings["urlSetHistoryInjuryCheck"];
    static string _urlGetNameDoctorForm = _serviceUrl + ConfigurationManager.AppSettings["urlGetNameDoctorForm"];
    static string _urlGetDetailDoctorInForm = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailDoctorInForm"];
    static string _urlGetHistoryWorkEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryWorkEmployee"];
    static string _urlGetHistoryInjuryEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryInjuryEmployee"];
    static string _urlSetHistoryHealthCheck = _serviceUrl + ConfigurationManager.AppSettings["urlSetHistoryHealthCheck"];
    static string _urlSetHistorySickCheck = _serviceUrl + ConfigurationManager.AppSettings["urlSetHistorySickCheck"];
    static string _urlGetHistoryHealthEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryHealthEmployee"];
    static string _urlGetHistorySickEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistorySickEmployee"];
    static string _urlGetHistorySickEverSick = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistorySickEverSick"];
    static string _urlGetHistorySickRelationFamily = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistorySickRelationFamily"];
    static string _urlGetHistoryHealthDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryHealthDetail"];
    static string _urlGetHistoryHealthRikeDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryHealthRikeDetail"];


    //-- HR HealthCheck --//


    int _emp_idx = 0;
    int _default_int = 0;
    int _r1Regis = 0;
    int ExternalID = 2;
    int ExternalCondition = 6;

    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        string url = Request.QueryString["url"];
        if (Session["_SESSION_EMPIDX"].ToString() != null)
        {
            ViewState["_PRINT_EMPIDX"] = Session["_SESSION_EMPIDX"].ToString();
            ViewState["_PRINT_TYPE"] = Session["_SESSION_TYPEPRINT"].ToString();
            ViewState["_PRINT_TITLE"] = Session["_SESSION_TITLE_PRINT"].ToString();
            ViewState["_PRINT_U2IDX"] = Session["_SESSION_U2IDX"].ToString();


        }
        else if (Session["_SESSION_EMPIDX"].ToString() == null)
        {
            Response.AddHeader("REFRESH", "3;URL=" + ResolveUrl("~/login?url=" + url));
        }

        //litDebug.Text = ViewState["_PRINT_TITLE"].ToString();

        getPrint(int.Parse(ViewState["_PRINT_EMPIDX"].ToString()), ViewState["_PRINT_TYPE"].ToString());

    }


    #region select data

    protected void getPrint(int _emp_idx_detail, string type)
    {

        divHistoryInjuryProfile.Visible = false;
        divHistoryWorkProfile.Visible = false;
        setFormData(fvHistoryEmployee, FormViewMode.ReadOnly, null);


        switch (type)
        {
            case "Injury":
                lbTitlePrint.Text = ViewState["_PRINT_TITLE"].ToString();
                getHistoryInjury(_emp_idx_detail, gvInjuryProfile);
                divHistoryInjuryProfile.Visible = true;
                break;
            case "HistoryEmployee":
                lbTitlePrint.Text = ViewState["_PRINT_TITLE"].ToString();

                getHistoryEmployee(_emp_idx_detail);
              
                break;
            case "HistoryWork":
                lbTitlePrint.Text = ViewState["_PRINT_TITLE"].ToString();

                getHistoryWork(_emp_idx_detail, gvWork);
                divHistoryWorkProfile.Visible = true;
                break;
            case "HistorySick":
                lbTitlePrint.Text = ViewState["_PRINT_TITLE"].ToString();

                getHistorySick(_emp_idx_detail);
               // divHistoryWorkProfile.Visible = true;
                break;
            case "HistoryHealth":
                lbTitlePrint.Text = ViewState["_PRINT_TITLE"].ToString();

                getDetailHistoryHealth(_emp_idx_detail, int.Parse(ViewState["_PRINT_U2IDX"].ToString()));
                // divHistoryWorkProfile.Visible = true;
                break;

        }
    }

    protected void getHistoryInjury(int _emp_idx_injury, GridView gvName)
    {

        data_hr_healthcheck _data_hr_hisinjury = new data_hr_healthcheck();

        _data_hr_hisinjury.healthcheck_u2_history_injury_list = new hr_healthcheck_u2_history_injury_detail[1];
        hr_healthcheck_u2_history_injury_detail _hr_historyinjury_list = new hr_healthcheck_u2_history_injury_detail();
        _hr_historyinjury_list.emp_idx = _emp_idx_injury;//int.Parse(rd.ToString());

        _data_hr_hisinjury.healthcheck_u2_history_injury_list[0] = _hr_historyinjury_list;

        _data_hr_hisinjury = callServicePostHRHealthCheck(_urlGetHistoryInjuryEmployee, _data_hr_hisinjury);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_hisinjury));
        setGridData(gvName, _data_hr_hisinjury.healthcheck_u2_history_injury_list);
        divHistoryInjuryProfile.Visible = true;
    }

    protected void getHistoryEmployee(int _emp_idx_history)
    {

        data_hr_healthcheck _data_hr_healthcheck = new data_hr_healthcheck();

        _data_hr_healthcheck.healthcheck_historyemployee_list = new hr_healthcheck_historyemployee_detail[1];
        hr_healthcheck_historyemployee_detail _hr_healthcheck_list = new hr_healthcheck_historyemployee_detail();
        _hr_healthcheck_list.EmpIDX = _emp_idx_history;//int.Parse(rd.ToString());

        _data_hr_healthcheck.healthcheck_historyemployee_list[0] = _hr_healthcheck_list;

        _data_hr_healthcheck = callServicePostHRHealthCheck(_urlGetEmployeeHistory, _data_hr_healthcheck);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_healthcheck));

        setFormData(fvHistoryEmployee, FormViewMode.ReadOnly, _data_hr_healthcheck.healthcheck_historyemployee_list);

    }

    protected void getHistoryWork(int emp_idx_work, GridView gvName)
    {

        data_hr_healthcheck _data_hr_hiswork = new data_hr_healthcheck();

        _data_hr_hiswork.healthcheck_u2_history_work_list = new hr_healthcheck_u2_history_work_detail[1];
        hr_healthcheck_u2_history_work_detail _hr_historywork_list = new hr_healthcheck_u2_history_work_detail();
        _hr_historywork_list.emp_idx = emp_idx_work;//int.Parse(rd.ToString());

        _data_hr_hiswork.healthcheck_u2_history_work_list[0] = _hr_historywork_list;

        _data_hr_hiswork = callServicePostHRHealthCheck(_urlGetHistoryWorkEmployee, _data_hr_hiswork);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_healthcheck));

        ViewState["vs_HistoryworkWork_List"] = _data_hr_hiswork.healthcheck_u2_history_work_list;
        //setGridData(gvWork, ViewState["vs_HistoryworkDetail_List"]);
        setGridData(gvName, ViewState["vs_HistoryworkWork_List"]);
        divHistoryWorkProfile.Visible = true;
    }

    protected void getHistorySick(int emp_idx_sick)
    {

        data_hr_healthcheck _data_hr_hissick = new data_hr_healthcheck();

        _data_hr_hissick.healthcheck_u2_history_sick_list = new hr_healthcheck_u2_history_sick_detail[1];
        hr_healthcheck_u2_history_sick_detail _hr_historysick_list = new hr_healthcheck_u2_history_sick_detail();
        _hr_historysick_list.emp_idx = emp_idx_sick;//int.Parse(rd.ToString());

        _data_hr_hissick.healthcheck_u2_history_sick_list[0] = _hr_historysick_list;

        //_data_hr_hissick = callServicePostHRHealthCheck(_urlGetHistorySickEmployee, _data_hr_hissick);
        _data_hr_hissick = callServicePostHRHealthCheck(_urlGetHistorySickEmployee, _data_hr_hissick);
        setFormData(fvSick, FormViewMode.ReadOnly, _data_hr_hissick.healthcheck_u2_history_sick_list);
       // div_btnedit_Historysick.Visible = true;

        RadioButtonList rdoSmoking_hr = (RadioButtonList)fvSick.FindControl("rdoSmoking_hr");
        Panel update_stopsmoking_hr = (Panel)fvSick.FindControl("update_stopsmoking_hr");
        Panel update_eversmoking_hr = (Panel)fvSick.FindControl("update_eversmoking_hr");
        Repeater rpt_eversick = (Repeater)fvSick.FindControl("rpt_eversick");
        Repeater rptRelationfamily = (Repeater)fvSick.FindControl("rptRelationfamily");

        //bind detail repeater
        if (_data_hr_hissick.return_code == 0)
        {

            ViewState["vs_u2_idx_historysick_edit"] = _data_hr_hissick.healthcheck_u2_history_sick_list[0].u2_historysick_idx;

            //div_btnedit_Historysick.Visible = true;
            //EmptyData.Visible = false;

            //check radio ever smoking
            if (rdoSmoking_hr.SelectedValue == "3")
            {
                update_stopsmoking_hr.Visible = true;
                update_eversmoking_hr.Visible = false;
            }
            else if (rdoSmoking_hr.SelectedValue == "2")
            {
                update_eversmoking_hr.Visible = true;
                update_stopsmoking_hr.Visible = false;
            }
            else
            {
                update_eversmoking_hr.Visible = false;
                update_stopsmoking_hr.Visible = false;
            }

            //check radio ever alcohol
            RadioButtonList rdo_alcohol_hr = (RadioButtonList)fvSick.FindControl("rdo_alcohol_hr");
            Panel update_alcohol_hr = (Panel)fvSick.FindControl("update_alcohol_hr");
            if (rdo_alcohol_hr.SelectedValue == "6")
            {
                update_alcohol_hr.Visible = true;

            }
            else
            {
                update_alcohol_hr.Visible = false;
            }

            ViewState["vs_u0_historysick_idx_eversick"] = _data_hr_hissick.healthcheck_u2_history_sick_list[0].u0_historysick_idx;
            data_hr_healthcheck _data_hr_eversick = new data_hr_healthcheck();

            _data_hr_eversick.healthcheck_u3_history_sick_list = new hr_healthcheck_u3_history_sick_detail[1];
            hr_healthcheck_u3_history_sick_detail _hr_eversick_list = new hr_healthcheck_u3_history_sick_detail();
            _hr_eversick_list.u0_historysick_idx = int.Parse(ViewState["vs_u0_historysick_idx_eversick"].ToString());

            _data_hr_eversick.healthcheck_u3_history_sick_list[0] = _hr_eversick_list;

            _data_hr_eversick = callServicePostHRHealthCheck(_urlGetHistorySickEverSick, _data_hr_eversick);

            setRepeaterData(rpt_eversick, _data_hr_eversick.healthcheck_u3_history_sick_list);

            data_hr_healthcheck _data_hr_relationfamily = new data_hr_healthcheck();

            _data_hr_relationfamily.healthcheck_u4_history_sick_list = new hr_healthcheck_u4_history_sick_detail[1];
            hr_healthcheck_u4_history_sick_detail _hr_relationfamily_list = new hr_healthcheck_u4_history_sick_detail();
            _hr_relationfamily_list.u0_historysick_idx = int.Parse(ViewState["vs_u0_historysick_idx_eversick"].ToString());

            _data_hr_relationfamily.healthcheck_u4_history_sick_list[0] = _hr_relationfamily_list;

            _data_hr_relationfamily = callServicePostHRHealthCheck(_urlGetHistorySickRelationFamily, _data_hr_relationfamily);

            setRepeaterData(rptRelationfamily, _data_hr_relationfamily.healthcheck_u4_history_sick_list);


            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_relationfamily));
            //litDebug.Text = _data_hr_hissick.healthcheck_u2_history_sick_list[0].u0_historysick_idx.ToString();
        }
        else
        {
            //EmptyData.Visible = true;
            //div_btnedit_Historysick.Visible = false;
        }


    }

    protected void getDetailHistoryHealth(int emp_idx_health, int _u2_health_idx_detail)
    {

        data_hr_healthcheck _data_m0_detailhistory_helth = new data_hr_healthcheck();

        _data_m0_detailhistory_helth.healthcheck_u2_history_health_list = new hr_healthcheck_u2_history_health_detail[1];
        hr_healthcheck_u2_history_health_detail _m0_history_health = new hr_healthcheck_u2_history_health_detail();

        _m0_history_health.u2_historyhealth_idx = _u2_health_idx_detail;

        _data_m0_detailhistory_helth.healthcheck_u2_history_health_list[0] = _m0_history_health;

        _data_m0_detailhistory_helth = callServicePostHRHealthCheck(_urlGetHistoryHealthDetail, _data_m0_detailhistory_helth);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_m0_detailhistory_helth));
        setFormData(fvDetailHealthCheck, FormViewMode.ReadOnly, _data_m0_detailhistory_helth.healthcheck_u2_history_health_list);

        getDetailTypeHealthCheckView();
        ViewState["vs_u2idx_edit_historyhealth"] = int.Parse(_u2_health_idx_detail.ToString());

        Repeater rpt_RikeDetail = (Repeater)fvDetailHealthCheck.FindControl("rpt_RikeDetail");

        if (_data_m0_detailhistory_helth.return_code == 0)
        {
            ViewState["vs_HistoryHealthDetail_u2"] = _data_m0_detailhistory_helth.healthcheck_u2_history_health_list[0].u2_historyhealth_idx;

            data_hr_healthcheck _data_hr_healthcheck_u3 = new data_hr_healthcheck();

            _data_hr_healthcheck_u3.healthcheck_u3_history_health_list = new hr_healthcheck_u3_history_health_detail[1];
            hr_healthcheck_u3_history_health_detail _hr_healthcheck_list_u3 = new hr_healthcheck_u3_history_health_detail();
            _hr_healthcheck_list_u3.u2_historyhealth_idx = int.Parse(ViewState["vs_HistoryHealthDetail_u2"].ToString());

            _data_hr_healthcheck_u3.healthcheck_u3_history_health_list[0] = _hr_healthcheck_list_u3;

            _data_hr_healthcheck_u3 = callServicePostHRHealthCheck(_urlGetHistoryHealthRikeDetail, _data_hr_healthcheck_u3);
            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_healthcheck_u3));
            setRepeaterData(rpt_RikeDetail, _data_hr_healthcheck_u3.healthcheck_u3_history_health_list);


        }
    }

    protected void getDetailTypeHealthCheckView()
    {
        Label _rao_idx_detailtype = (Label)fvDetailHealthCheck.FindControl("rao_idx_detailtype");
        RadioButtonList rdoDetailType_detail = (RadioButtonList)fvDetailHealthCheck.FindControl("rdoDetailType_detail");

        data_hr_healthcheck _data_m0_detailtype = new data_hr_healthcheck();

        _data_m0_detailtype.healthcheck_m0_detailtype_list = new hr_healthcheck_m0_detailtype_detail[1];
        hr_healthcheck_m0_detailtype_detail _m0_detailtype = new hr_healthcheck_m0_detailtype_detail();

        _data_m0_detailtype.healthcheck_m0_detailtype_list[0] = _m0_detailtype;

        _data_m0_detailtype = callServicePostHRHealthCheck(_urlGetDetailTypeHealthCheck, _data_m0_detailtype);

        rdoDetailType_detail.DataSource = _data_m0_detailtype.healthcheck_m0_detailtype_list;
        rdoDetailType_detail.DataTextField = "detail_typecheck";
        rdoDetailType_detail.DataValueField = "m0_detail_typecheck_idx";
        rdoDetailType_detail.DataBind();
        rdoDetailType_detail.SelectedValue = _rao_idx_detailtype.Text;

    }

    #endregion


    #region call service

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee getViewEmployeeList(data_employee _data_employee)
    {
        _data_employee = callServicePostEmployee(_urlGetViewEmployeeList, _data_employee);
        return _data_employee;
    }

    protected data_hr_healthcheck callServicePostHRHealthCheck(string _cmdUrl, data_hr_healthcheck _data_hr_healthcheck)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_hr_healthcheck);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_hr_healthcheck = (data_hr_healthcheck)_funcTool.convertJsonToObject(typeof(data_hr_healthcheck), _localJson);

        return _data_hr_healthcheck;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion


}