﻿using AjaxControlToolkit;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_TrnVideo : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_employee = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_employee"];

    static string _urlGetel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_training"];

    static string _urlGetErrorel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlGetErrorel_m0_training"];
    static string _urlGetel_lu_position = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_lu_position"];
    //
    static string _urlGetel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_course"];
    static string _urlSetInsel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_course"];
    static string _urlDelel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_u_course"];
    static string _urlSetUpdel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_u_course"];

    string _localJson = "";
    int _tempInt = 0;

    int emp_idx = 0;

    string _FromcourseRunNo = "u_course";
    string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
    string _PathFileimage = ConfigurationSettings.AppSettings["path_flieimage_elearning"];
    string _PathFileUrl = ConfigurationSettings.AppSettings["path_flie_elearningUrl"];
    string _PathFileimageUrl = ConfigurationSettings.AppSettings["path_flieimage_elearningUrl"];
    string _Folder_course_video = "course_video";
    string _Folder_course_video_image = "course_video_image";
    private string ffmpegPhysicalPath = @"C:\***\***\ffmpeg.exe";

    //_object
    FormView _FormView;
    TextBox _txtcourse_no;
    TextBox _txttraining_group_name;
    TextBox _txttraining_branch_name;
    DropDownList _ddlu0_course_idx_ref;
    TextBox _txtcourse_date;
    TextBox _txtlevel_code;
    TextBox _txtscore_through_per;
    TextBox _txtvideo_title;
    TextBox _txtvideo_description;
    TextBox _txtvideo_item;
    FileUpload _fldvideo_name;
    FileUpload _fldvideo_images;
    DropDownList _ddlcourse_status;
    TextBox _txtcourse_score;
    TextBox _txttraining_plan_year;
    Label _lbRequiredfldvideo_name;
    //DropDownList _ddlvideo_name;
   // DropDownList _ddlvideo_images;
    TextBox _txthddfld_video_name;
    TextBox _txthddfld_video_images;
    TextBox _txtu0_course_idx_ref;
    Image _imgWaiting;

    #endregion Init

    #region Constant
    public static class Constants
    {
        public const int SELECT_ALL = 20;
        public const int SELECT_WHERE = 21;
        public const int NUMBER_NULL = -999;
    }
    #endregion Constant

    
    private void Page_Init(object sender, EventArgs e)
    {
        ClearApplicationCache();
    }
    
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        
        if (!IsPostBack)
        {
           
            _func_dmu.zSetDdlMonth(ddlMonthSearch_L);
            ddlMonthSearch_L.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
            _func_dmu.zDropDownList(ddlYearSearch_L,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "COURSE-YEAR"
                                );

            getDatasetEmpty();
            zSetMode(2);

        }
        
        setTrigger();


    }

    private void setTrigger()
    {
       
        linkBtnTrigger(btnListData);
        linkBtnTrigger(btnInsert);
        linkBtnTrigger(btnFilter);
        linkBtnTrigger(btnSaveInsert);
        linkBtnTrigger(btnCancel);
        gridViewTrigger(GvListData);
        linkBtnTrigger(btnCancelDetail);
        linkBtnTrigger(btnSaveUpdate);
        linkBtnTrigger(btnUpdateCancel);
        linkBtnTrigger(btnToInsertDetail);
        
    }

    #endregion Page Load

    protected void gridViewTrigger(GridView linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    /* Start Std*/
    #region Std

    #region zSetMode
    public void zSetMode(int AiMode)
    {
        pnlSave.Visible = false;
        switch (AiMode)
        {
            case 0:  //insert mode
                {
                    pnlListData.Visible = false;
                    MultiViewBody.SetActiveView(View_Insert);
                    fvCRUD.Visible = true;
                    //fv_Update.Visible = false;
                    fvCRUD.ChangeMode(FormViewMode.Insert);
                    setActiveTab("I");
                    SetViewStateEmpty();
                    actionIndex();
                    Select_Page1showdata();

                }
                break;
            case 1:  //update mode
                {
                    pnlListData.Visible = false;
                    MultiViewBody.SetActiveView(View_Update);
                    fv_Update.Visible = true;
                    fv_Update.ChangeMode(FormViewMode.Edit);
                    setActiveTab("I");
                    SetViewStateEmpty();
                    actionIndex();
                }
                break;
            case 2://preview mode
                {
                    pnlListData.Visible = true;
                    setActiveTab("P");
                    ShowListData();
                    MultiViewBody.SetActiveView(View_ListDataPag);

                }
                break;
            case 3://Detail mode
                {
                    pnlListData.Visible = true;
                    //setActiveTab("");
                    setActiveTab("I");
                    MultiViewBody.SetActiveView(viewList);

                }
                break;
            case 4:  //insert detail mode
                {
                    pnlListData.Visible = false;
                    MultiViewBody.SetActiveView(View_Insert);
                    fvCRUD.Visible = true;
                    //fv_Update.Visible = false;
                    fvCRUD.ChangeMode(FormViewMode.Insert);
                    setActiveTab("I");
                    SetViewStateEmpty();
                    actionIndex();

                }
                break;
        }
    }
    #endregion zSetMode


    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        liInsert.Attributes.Add("class", "");
        liListData.Attributes.Add("class", "");
        switch (activeTab)
        {
            case "P":
                liListData.Attributes.Add("class", "active");

                break;
            case "I":
                liInsert.Attributes.Add("class", "active");
                break;

        }
    }
    #endregion setActiveTab

    #endregion Std

    #region Action
    protected void actionIndex()
    {
        // Select_DetailList();
    }
    protected void Select_DetailList()
    {
        _data_elearning.employeeM_action = new employeeM[1];
        employeeM obj = new employeeM();
        obj.EmpIDX = emp_idx;
        _data_elearning.employeeM_action[0] = obj;
        //litDebug.Text = _funcTool.convertObjectToXml(_data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_employee, _data_elearning);
        // _func_dmu.zSetFormViewData(FvDetailUser, _data_elearning.employeeM_action);
        if (_data_elearning.employeeM_action != null)
        {
            foreach (var item in _data_elearning.employeeM_action)
            {
                ViewState["DocCode"] = item.EmpCode;
                ViewState["CEmpIDX_Create"] = item.EmpIDX;
                ViewState["RPosIDX_J"] = item.RPosIDX_J;
                ViewState["RDeptID"] = item.RDeptID;
            }
        }



    }

    private void SetViewStateEmpty()
    {
        ViewState["DocCode"] = "";
        ViewState["CEmpIDX_Create"] = "";
        ViewState["RPosIDX_J"] = "0";
        ViewState["RDeptID"] = "0";
        getDatasetEmpty();
        DataSet dsEmpTraning = (DataSet)ViewState["vsEmpTraningList"];


    }
    public Boolean getDataCheckbox(string id)
    {
        int i = 0;
        if ((id != "") && (id != null))
        {
            i = int.Parse(id);
        }
        if (i == 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }
    protected void Select_Page1showdata()
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_course_action = new course[1];
        course obj = new course();
        obj.operation_status_id = "U6-EMPTY";
        dataelearning.el_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, dataelearning);
        _func_dmu.zSetFormViewData(fvCRUD, dataelearning.el_course_action);

        _FormView = getFv("I");
        _txttraining_group_name = (TextBox)_FormView.FindControl("txttraining_group_name");
        _txttraining_branch_name = (TextBox)_FormView.FindControl("txttraining_branch_name");
        _ddlu0_course_idx_ref = (DropDownList)_FormView.FindControl("ddlu0_course_idx_ref");
        _txtcourse_date = (TextBox)_FormView.FindControl("txtcourse_date");
        _txtlevel_code = (TextBox)_FormView.FindControl("txtlevel_code");
        _txtcourse_score = (TextBox)_FormView.FindControl("txtcourse_score");
        _txttraining_plan_year = (TextBox)_FormView.FindControl("txttraining_plan_year");

        _txttraining_plan_year.Text = DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture);
        Hddfld_course_no.Value = "";
        Hddfld_u0_course_idx_ref.Value = "";
        Hddfld_u6_course_idx.Value = "";
        Hddfld_status.Value = "I";

        _func_dmu.zDropDownList(_ddlu0_course_idx_ref,
                                "",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-COURSE-U0-LOOKUP-VIDEO"
                                );


    }
    private FormView getFv(string _sMode)
    {
        if (_sMode == "I")
        {
            return fvCRUD;
        }
        else
        {
            return fv_Update;
        }
    }
    protected void getDatasetEmpty() // สร้าง dataset
    {
        CreateDs_dsEmployeeTable();
        CreateDs_dsAddVedio();
    }
    private void CreateDs_dsEmployeeTable()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsEmployee");
        ds.Tables["dsEmployee"].Columns.Add("EmpIDX", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("DocCode", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("EmpCode", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("FullNameTH", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("RDeptID", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("RPosIDX_J", typeof(String));
        ViewState["vsEmployeeList"] = ds;
    }

    private void ShowListData()
    {

        _data_elearning.el_course_action = new course[1];
        course obj = new course();
        obj.filter_keyword = txtFilterKeyword_L.Text;
        obj.zmonth = int.Parse(ddlMonthSearch_L.SelectedValue);
        obj.zyear = int.Parse(ddlYearSearch_L.SelectedValue);
        obj.operation_status_id = "U6";
        _data_elearning.el_course_action[0] = obj;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, _data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _func_dmu.zSetGridData(GvListData, _data_elearning.el_course_action);

    }

    protected void onSelectedIndexChanged(object sender, EventArgs e)
    {

        //var chkName = (CheckBoxList)sender;
        if (sender is CheckBox)
        {
            var rdName = (CheckBox)sender;
            switch (rdName.ID)
            {
                case "GvPosition_cb_RPosIDX_J":

                    break;

            }
        }

    }
    protected void onRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        string sGvName = GvName.ID;
        if (sGvName == "GvListData")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnDetail_GvListData = (LinkButton)e.Row.Cells[9].FindControl("btnDetail_GvListData");
                LinkButton btnDelete_GvListData = (LinkButton)e.Row.Cells[9].FindControl("btnDelete_GvListData");
                //TextBox txtapprove_status = (TextBox)e.Row.Cells[9].FindControl("txtapprove_status_GvListData");
                linkBtnTrigger(btnDetail_GvListData);
                linkBtnTrigger(btnDelete_GvListData);
                //Boolean bBl = true;
                //if (txtapprove_status.Text == "4") // 4	อนุมัติ
                //{
                //    bBl = false;
                //}
                //else if (txtapprove_status.Text == "6")
                //{
                //    bBl = false;
                //}
                //btnUpdate_GvListData.Visible = bBl;
                //btnDelete_GvListData.Visible = bBl;
            }
        }
    }
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvTraningList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                // showmodal_traning(txtsearch_modal_training.Text);
                break;
            case "gvModalTraning":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                //showmodal_traning_app(txtsearch_modal_training_app.Text);
                break;
            case "GvListData":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListData();
                SETFOCUS.Focus();
                break;

        }
    }
    protected string getStatus(int status)
    {
        //if (status == 1)
        //{
        //    return "<span style='color:#E87E04;font-weight:700;'>รอการอนุมัติ</span>";
        //}
        //else if (status == 2)
        //{
        //    return "<span style='color:#26A65B;font-weight:700;'>ผ่านการอนุมัติ</span>";
        //}
        //else if (status == 3)
        //{
        //    return "<span style='color:#F22613;font-weight:700;'>ไม่ผ่านการอนุมัติ</span>";
        //}
        //else
        //{
        //    return string.Empty;
        //}
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    protected string getStatusName(int status)
    {
        if (status == 1)
        {
            return "<span style='color:#26A65B;font-weight:700;'>ใช้งาน</span>";
        }
        else
        {
            return "<span style='color:#F22613;font-weight:700;'>ไม่ใช้งาน</span>";
        }

    }
    protected string getCourseType(int course_type_etraining, int course_type_elearning)
    {
        string a = "";
        if (course_type_etraining > 0)
        {
            a = "E-Training";
        }
        string b = "";
        if (course_type_elearning > 0)
        {
            b = "E-Learning";
        }
        string sValue = "";
        if ((a != "") && (b != ""))
        {
            sValue = a + " / " + b;
        }
        else if (a != "")
        {
            sValue = a;
        }
        else if (b != "")
        {
            sValue = b;
        }
        return sValue;
    }
    protected string getpriority(int status)
    {

        if (status == 1)
        {
            return "<span>Must</span>";
        }
        else if (status == 2)
        {
            return "<span>Need</span>";
        }
        else if (status == 3)
        {
            return "<span>Want</span>";
        }
        else
        {
            return "<span style='font-weight:700;'>-</span>";
        }
    }
    protected string getStatustest(int status)
    {

        //if (status > 0)
        //{
        //    return "<span style='color:#26A65B;font-weight:700;'>มีแบบทดสอบ</span>";
        //}
        //else 
        //{
        //    return "<span style='color:#F22613;font-weight:700;'>ไม่มีแบบทดสอบ</span>";
        //}
        if (status > 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string getStatusEvalue(int status)
    {

        //if (status > 0)
        //{
        //    return "<span style='color:#26A65B;font-weight:700;'>มีแบบประเมิน</span>";
        //}
        //else 
        //{
        //    return "<span style='color:#F22613;font-weight:700;'>ไม่มีแบบประเมิน</span>";
        //}
        if (status > 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    #endregion Action

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _m0_training_idx;
        int _cemp_idx;

        m0_training objM0_ProductType = new m0_training();

        switch (cmdName)
        {
            case "btnListData":
                zSetMode(2);
                break;

            case "btnInsert":

                getDatasetEmpty();
                zSetMode(0);
                MultiViewBody.SetActiveView(View_Insert);
                pnlSave.Visible = true;
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnSaveInsert":
                
                if (checkError(0) == false)
                {

                    if (zSave(0) == true)
                    {
                        if (Hddfld_u0_course_idx_ref.Value == "")
                        {
                            Page.Response.Redirect(Page.Request.Url.ToString(), true);
                        }
                        else
                        {
                            _m0_training_idx = _func_dmu.zStringToInt(Hddfld_u0_course_idx_ref.Value);
                            if (_m0_training_idx > 0)
                            {
                                zSetMode(3);
                                zShowdataDetail(_m0_training_idx);
                                pnlSave.Visible = true;

                            }
                        }

                    }

                }

                break;
            case "btnDelete":
                _m0_training_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;
               // zDelete(_m0_training_idx);
                ShowListData();
                break;
            case "btnFilter":
                // actionIndex();
                ShowListData();
                break;

            case "btnUpdateDetail":
                _m0_training_idx = int.Parse(cmdArg);
                getDatasetEmpty();
                if (_m0_training_idx > 0)
                {
                    zSetMode(1);
                    zShowdataUpdate(_m0_training_idx);
                }
                //if (Hddfld_course_no.Value == "")
                //{
                //    zSetMode(2);
                //}
                break;
            case "btnSaveUpdate":
                if (zSave(_func_dmu.zStringToInt(Hddfld_u6_course_idx.Value)) == true)
                {
                    zSetMode(3);
                    zShowdataDetail(_func_dmu.zStringToInt(Hddfld_u0_course_idx_ref.Value));
                }
                break;
            case "btnDetail":
                _m0_training_idx = int.Parse(cmdArg);
                getDatasetEmpty();
                if (_m0_training_idx > 0)
                {

                    zSetMode(3);
                    zShowdataDetail(_m0_training_idx);

                    pnlSave.Visible = true;
                }
                if (Hddfld_course_no.Value == "")
                {
                    zSetMode(2);
                }
                break;
            case "btnUpdateCancel":
                zSetMode(3);
                zShowdataDetail(_func_dmu.zStringToInt(Hddfld_u0_course_idx_ref.Value));
                break;
            case "btnDeleteDetail":
                zDeleteDetail(cmdArg);
                zSetMode(3);
                zShowdataDetail(_func_dmu.zStringToInt(Hddfld_u0_course_idx_ref.Value));
                break;
            case "btnToInsertDetail":
                _m0_training_idx = _func_dmu.zStringToInt(Hddfld_u0_course_idx_ref.Value);
                getDatasetEmpty();
                if (_m0_training_idx > 0)
                {
                    getDatasetEmpty();
                    zSetMode(4);
                    MultiViewBody.SetActiveView(View_Insert);
                    zShowdataInsertDetail(_m0_training_idx);
                    pnlSave.Visible = true;

                }
                break;
        }
    }
    #endregion btnCommand

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        trainingLoolup obj_trainingLoolup = new trainingLoolup();
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_course_no.Value));
        if (sender is FormView)
        {
            var FvName = (FormView)sender;
            if (FvName.ID == "fvCRUD")
            {

            }
            else if (FvName.ID == "fv_Update")
            {

            }

        }
        else if (sender is DropDownList)
        {
            var FvName = (DropDownList)sender;
            if (FvName.ID == "ddlu0_course_idx_ref")
            {
                _txttraining_group_name = (TextBox)_FormView.FindControl("txttraining_group_name");
                _txttraining_branch_name = (TextBox)_FormView.FindControl("txttraining_branch_name");
                _txtcourse_date = (TextBox)_FormView.FindControl("txtcourse_date");
                _txtlevel_code = (TextBox)_FormView.FindControl("txtlevel_code");
                _txtcourse_score = (TextBox)_FormView.FindControl("txtcourse_score");
                _txtcourse_no = (TextBox)_FormView.FindControl("txtcourse_no");
                _txtscore_through_per = (TextBox)_FormView.FindControl("txtscore_through_per");
                _txttraining_group_name.Text = "";
                _txttraining_branch_name.Text = "";
                _txtcourse_date.Text = "";
                _txtlevel_code.Text = "";
                _txtcourse_score.Text = "";
                _txtcourse_no.Text = "";
                _txtscore_through_per.Text = "";
                if (_func_dmu.zStringToInt(FvName.SelectedValue) > 0)
                {
                    _data_elearning = _func_dmu.zShowDataDsLookup("TRN-COURSE-U0-LOOKUP-VIDEO",
                                            "idx",
                                            _func_dmu.zStringToInt(FvName.SelectedValue)
                                            );
                    if (_data_elearning.trainingLoolup_action != null)
                    {

                        obj_trainingLoolup = _data_elearning.trainingLoolup_action[0];
                        _txttraining_group_name.Text = obj_trainingLoolup.training_group_name;
                        _txttraining_branch_name.Text = obj_trainingLoolup.training_branch_name;
                        _txtcourse_date.Text = obj_trainingLoolup.doc_date;
                        _txtlevel_code.Text = obj_trainingLoolup.level_code.ToString();
                        _txtcourse_score.Text = obj_trainingLoolup.score.ToString();
                        _txtcourse_no.Text = obj_trainingLoolup.doc_no;
                        _txtscore_through_per.Text = obj_trainingLoolup.score_through_per.ToString();
                    }
                }
            }
            //else if (FvName.ID == "ddlvideo_name")
            //{

            //    _fldvideo_name = (FileUpload)FvName.FindControl("fldvideo_name");
               
            //    if (FvName.SelectedValue == "0")
            //    {
            //        _fldvideo_name.Visible = false;
            //    }
            //    else
            //    {
            //        _fldvideo_name.Visible = true;
            //    }
            //}
            //else if (FvName.ID == "ddlvideo_images")
            //{

            //    _fldvideo_images = (FileUpload)FvName.FindControl("fldvideo_images");

            //    if (FvName.SelectedValue == "0")
            //    {
            //        _fldvideo_images.Visible = false;
            //    }
            //    else
            //    {
            //        _fldvideo_images.Visible = true;
            //    }

            //}

        }
    }


    #endregion FvDetail_DataBound

    #region Check Session Redirect
    protected void checkSessionRedirect()
    {
        //if (Session["sesLoginIdxBackEnd"] == null)
        //{
        //   Response.Redirect(ResolveUrl("~/login"));
        //}
    }
    #endregion Check Session Redirect

    private void CreateDs_dsAddVedio()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsAddVedio");
        ds.Tables["dsAddVedio"].Columns.Add("id", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("item", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("title", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("description", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("file", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("file_path", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("fileimage", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("fileimage_path", typeof(String));
        ViewState["vsAddVedioList"] = ds;

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void BtnTrigger(Button linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }


    private void ClearTraning()
    {
        Select_Page1showdata();
    }


    public Boolean getValue(string id = "0")
    {
        Boolean rBoolean = false;

        if (id == "1")
        {
            rBoolean = true;
        }

        return rBoolean;
    }




    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {

                case "btnRemove_GvDeptTraningList":

                    break;

            }
        }
    }
    private string getbranchcode(int id)
    {
        string sreturn = "";
        _data_elearning.trainingLoolup_action = new trainingLoolup[1];
        trainingLoolup obj = new trainingLoolup();
        obj.idx = id;
        obj.operation_status_id = "TRN-GROUP";
        _data_elearning.trainingLoolup_action[0] = obj;
        _data_elearning = _func_dmu.zCallServicePostNetwork(_func_dmu._urlGetel_lu_TraningAll, _data_elearning);
        if (_data_elearning.trainingLoolup_action != null)
        {
            obj = _data_elearning.trainingLoolup_action[0];
            sreturn = obj.doc_no;
        }

        return sreturn;
    }
    private Boolean zSaveInsert()
    {
        Boolean _Boolean = false;
        // U6
        TextBox txtcourse_no = (TextBox)fvCRUD.FindControl("txtcourse_no");
        DropDownList ddlm0_training_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_training_group_idx_ref");
        TextBox ddm0_training_branch_idx = (TextBox)fvCRUD.FindControl("ddm0_training_branch_idx");
        TextBox txtcourse_name = (TextBox)fvCRUD.FindControl("txtcourse_name");
        TextBox txtcourse_date = (TextBox)fvCRUD.FindControl("txtcourse_date");
        TextBox txtcourse_remark = (TextBox)fvCRUD.FindControl("txtcourse_remark");
        // DropDownList ddllevel_code = (DropDownList)fvCRUD.FindControl("ddllevel_code");
        TextBox txtcourse_score = (TextBox)fvCRUD.FindControl("txtcourse_score");
        CheckBox cbcourse_type_etraining = (CheckBox)fvCRUD.FindControl("cbcourse_type_etraining");
        CheckBox cbcourse_type_elearning = (CheckBox)fvCRUD.FindControl("cbcourse_type_elearning");
        DropDownList ddlcourse_priority = (DropDownList)fvCRUD.FindControl("ddlcourse_priority");
        DropDownList ddlcourse_status = (DropDownList)fvCRUD.FindControl("ddlcourse_status");
        GridView GvCourse = (GridView)fvCRUD.FindControl("GvCourse");
        GridView GvM0_EmpTarget = (GridView)fvCRUD.FindControl("GvM0_EmpTarget");

        string m0_prefix = "xx";
        // m0_prefix = getbranchcode(int.Parse(ddlm0_training_group_idx_ref.SelectedValue));
        if (m0_prefix == "")
        {
            //  showAlert("รหัสกลุ่มวิชาไม่ถูกต้อง");

        }
        else
        {
            int idx = 0, icourse_score = 0, u0_course_idx_ref = 0;
            course obj_course = new course();
            _data_elearning.el_course_action = new course[1];
            obj_course.course_no = _func_dmu.zRun_Number(_FromcourseRunNo, m0_prefix, "YYYY-COURSE", "N", "0000");

            obj_course.course_date = _func_dmu.zDateToDB(txtcourse_date.Text);
            obj_course.m0_training_group_idx_ref = int.Parse(ddlm0_training_group_idx_ref.SelectedValue);
            //obj_course.m0_training_branch_idx_ref = int.Parse(ddm0_training_branch_idx.SelectedValue);
            obj_course.course_name = txtcourse_name.Text;
            obj_course.course_remark = txtcourse_remark.Text;
            //obj_course.level_code = int.Parse(ddllevel_code.SelectedValue);
            obj_course.course_priority = int.Parse(ddlcourse_priority.SelectedValue);
            obj_course.course_status = int.Parse(ddlcourse_status.SelectedValue);

            if (txtcourse_score.Text != "")
            {
                icourse_score = int.Parse(txtcourse_score.Text);
            }
            obj_course.course_score = icourse_score;
            obj_course.course_created_by = emp_idx;
            obj_course.course_updated_by = emp_idx;
            obj_course.course_type_etraining = _func_dmu.zBooleanToInt(cbcourse_type_etraining.Checked);
            obj_course.course_type_elearning = _func_dmu.zBooleanToInt(cbcourse_type_elearning.Checked);
            obj_course.course_plan_status = "I";

            //node
            obj_course.approve_status = 0;
            obj_course.u0_idx = 5;
            obj_course.node_idx = 9;
            obj_course.actor_idx = 1;
            obj_course.app_flag = 0;
            obj_course.app_user = 0;

            obj_course.operation_status_id = "U0";
            _data_elearning.el_course_action[0] = obj_course;
            _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, _data_elearning);
            u0_course_idx_ref = _data_elearning.return_code;
            if (u0_course_idx_ref > 0)
            {



            }
            _Boolean = true;
        }
        return _Boolean;

    }


    public void showAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }
    

    private void CreateDs_Clone()
    {
        string sDs = "dsclone";
        string sVs = "vsclone";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("zId_G", typeof(String));
        ds.Tables[sDs].Columns.Add("zId", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }


    private Boolean zSave(int id)
    {
        
        Boolean _Boolean = false;
        string su0_course_idx = "";

        string sMode = _func_dmu.zGetMode(Hddfld_u6_course_idx.Value), sDocno = "";
        string m0_prefix = "";
        int _iMonth;
        _FormView = getFv(sMode);
        _txtcourse_no = (TextBox)_FormView.FindControl("txtcourse_no");
        _txttraining_group_name = (TextBox)_FormView.FindControl("txttraining_group_name");
        _txttraining_branch_name = (TextBox)_FormView.FindControl("txttraining_branch_name");
        
        _txtcourse_date = (TextBox)_FormView.FindControl("txtcourse_date");
        _txtlevel_code = (TextBox)_FormView.FindControl("txtlevel_code");
        _txtscore_through_per = (TextBox)_FormView.FindControl("txtscore_through_per");
        _txtvideo_title = (TextBox)_FormView.FindControl("txtvideo_title");
        _txtvideo_description = (TextBox)_FormView.FindControl("txtvideo_description");
        _txtvideo_item = (TextBox)_FormView.FindControl("txtvideo_item");
        _ddlcourse_status = (DropDownList)_FormView.FindControl("ddlcourse_status");
        _txtcourse_score = (TextBox)_FormView.FindControl("txtcourse_score");
        _txttraining_plan_year = (TextBox)_FormView.FindControl("txttraining_plan_year");
        _fldvideo_name = (FileUpload)_FormView.FindControl("fldvideo_name");
        _fldvideo_images = (FileUpload)_FormView.FindControl("fldvideo_images");
        //_ddlvideo_name = (DropDownList)_FormView.FindControl("ddlvideo_name");
       // _ddlvideo_images = (DropDownList)_FormView.FindControl("ddlvideo_images");
        _txthddfld_video_name = (TextBox)_FormView.FindControl("txthddfld_video_name");
        _txthddfld_video_images = (TextBox)_FormView.FindControl("txthddfld_video_images");
        

        if (id == 0)
        {
            _ddlu0_course_idx_ref = (DropDownList)_FormView.FindControl("ddlu0_course_idx_ref");
            su0_course_idx = _ddlu0_course_idx_ref.SelectedValue;
        }
        else
        {
            _txtu0_course_idx_ref = (TextBox)_FormView.FindControl("txtu0_course_idx_ref");
            su0_course_idx = _txtu0_course_idx_ref.Text;
        }
            course obj_course = new course();
        _data_elearning.el_course_action = new course[1];
        obj_course.u6_course_idx = id;
        obj_course.u0_course_idx_ref = _func_dmu.zStringToInt(su0_course_idx);
        obj_course.video_title = _txtvideo_title.Text;
        obj_course.video_description = _txtvideo_description.Text;
        obj_course.video_item = _func_dmu.zStringToInt(_txtvideo_item.Text);
        obj_course.score_through_per = _func_dmu.zStringToDecimal(_txtscore_through_per.Text);
        obj_course.course_status = _func_dmu.zStringToInt(_ddlcourse_status.SelectedValue);
        //obj_course.video_description = _txtvideo_description.Text;
        //  obj_course.video_description = _txtvideo_description.Text;

        obj_course.course_created_by = emp_idx;
        obj_course.course_updated_by = emp_idx;
        obj_course.operation_status_id = "U6";

        if (id == 0)
        {
            
            obj_course.video_name = zUploadfileVideo1(_fldvideo_name, _txtcourse_no.Text, _txtvideo_item.Text);
            obj_course.video_images = zUploadfileImage(_fldvideo_images, _txtcourse_no.Text, _txtvideo_item.Text);
        }
        else
        {

            if (_fldvideo_name.HasFile == false)
            {
                obj_course.video_name = _txthddfld_video_name.Text;
            }
            else
            {
                zDelVideo(_txtcourse_no.Text, _txthddfld_video_name.Text);
                obj_course.video_name = zUploadfileVideo1(_fldvideo_name, _txtcourse_no.Text, _txtvideo_item.Text);
            }

            if (_fldvideo_images.HasFile == false)
            {
                obj_course.video_images = _txthddfld_video_images.Text;
            }
            else
            {
                zDelImages(_txtcourse_no.Text, _txthddfld_video_images.Text);
                obj_course.video_images = zUploadfileImage(_fldvideo_images, _txtcourse_no.Text, _txtvideo_item.Text);
            }
        }


        //node
        obj_course.approve_status = 0;
        obj_course.u0_idx = 11;
        obj_course.node_idx = 13;
        obj_course.actor_idx = 1;
        obj_course.app_flag = 0;
        obj_course.app_user = 0;

        _data_elearning.el_course_action[0] = obj_course;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        //_func_dmu.zObjectToXml(_data_elearning);
        if (sMode == "I")
        {
            _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, _data_elearning);
        }
        else
        {
            _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_course, _data_elearning);
        }

        //zUploadfileVideo(fldvideo_namexxx);
        _Boolean = true;
        return _Boolean;
    }
    public Boolean checkError(int id)
    {
        Boolean _Boolean = false;
        string sMode = _func_dmu.zGetMode(Hddfld_u6_course_idx.Value), sDocno = "";
        string m0_prefix = "";
        _FormView = getFv(sMode);
        _txtcourse_no = (TextBox)_FormView.FindControl("txtcourse_no");
        _txttraining_group_name = (TextBox)_FormView.FindControl("txttraining_group_name");
        _txttraining_branch_name = (TextBox)_FormView.FindControl("txttraining_branch_name");
        _ddlu0_course_idx_ref = (DropDownList)_FormView.FindControl("ddlu0_course_idx_ref");
        _txtcourse_date = (TextBox)_FormView.FindControl("txtcourse_date");
        _txtlevel_code = (TextBox)_FormView.FindControl("txtlevel_code");
        _txtscore_through_per = (TextBox)_FormView.FindControl("txtscore_through_per");
        _txtvideo_title = (TextBox)_FormView.FindControl("txtvideo_title");
        _txtvideo_description = (TextBox)_FormView.FindControl("txtvideo_description");
        _txtvideo_item = (TextBox)_FormView.FindControl("txtvideo_item");
        _ddlcourse_status = (DropDownList)_FormView.FindControl("ddlcourse_status");
        _txtcourse_score = (TextBox)_FormView.FindControl("txtcourse_score");
        _txttraining_plan_year = (TextBox)_FormView.FindControl("txttraining_plan_year");
        _fldvideo_name = (FileUpload)fvCRUD.FindControl("fldvideo_name");
        _fldvideo_images = (FileUpload)_FormView.FindControl("fldvideo_images");
        //showAlert(fldvideo_namexxx.HasFile.ToString());

        course obj_course = new course();
        _data_elearning.el_course_action = new course[1];
        obj_course.u6_course_idx = id;
        obj_course.u0_course_idx_ref = _func_dmu.zStringToInt(_ddlu0_course_idx_ref.SelectedValue);
        obj_course.video_item = _func_dmu.zStringToInt(_txtvideo_item.Text);
        obj_course.course_status = _func_dmu.zStringToInt(_ddlcourse_status.SelectedValue);
        obj_course.operation_status_id = "U6-DUPICAT";
        _data_elearning.el_course_action[0] = obj_course;
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, _data_elearning);
        int _int = 0;
        if (_data_elearning.el_course_action != null)
        {
            foreach (var item in _data_elearning.el_course_action)
            {
                _int = item.video_item;
            }
        }
        if (_int > 0)
        {
            _Boolean = true;
            showAlert("ลำดับนี้มีในระบบแล้วกรุณากรอกใหม่");
        }
        else
        {
            //if(_txtvideo_title.Text.Trim() == "")
            //{
            //    _Boolean = true;
            //}
            //if (_func_dmu.zStringToInt(_txtscore_through_per.Text) == 0)
            //{
            //    _Boolean = true;
            //}
            //if (_func_dmu.zStringToInt(_txtvideo_item.Text) == 0)
            //{
            //    _Boolean = true;
            //}
            //if (_fldvideo_name.HasFile == false)
            //{
            //    _Boolean = true;
            //}

        }
        //if (_Boolean == false)
        //{
        //    panel6x.Visible = true;
        //}
        return _Boolean;
    }
    private void zUploadfileVideo(FileUpload afileUpload)
    {
        if (afileUpload.HasFile == false)
        {
            return;
        }
        // FileUpload obj = new FileUpload();
        string FileName = afileUpload.FileName;
        string DestPath = Server.MapPath(_PathFile);
        string strFinalFileName = Path.GetFileName(afileUpload.FileName);
        long FileLength = afileUpload.PostedFile.ContentLength;
        long uploadchunklimit;
        int SizeLimit = (int)FileLength;
        if (FileLength <= 1024)
        {
            uploadchunklimit = 1;
            SizeLimit = (int)FileLength;
        }
        else if (FileLength > 10240)
        {
            uploadchunklimit = FileLength / 10240;
            SizeLimit = 10;
        }
        else if (FileLength <= 10240 && FileLength > 1024)
        {
            uploadchunklimit = FileLength / 1024;
        }
        else
        {
            uploadchunklimit = FileLength / 1024;
        }
        string _videofile = _PathFile + _Folder_course_video;
        long lngSize = (long)SizeLimit;
        lngSize *= 1024 * 1024;
        string ext = Path.GetExtension(afileUpload.PostedFile.FileName);
        string strDestFileName = Server.MapPath(_videofile) + "\\" + Guid.NewGuid() + ext;
        string strSrcFile = Server.MapPath(_videofile + "/" + Path.GetFileName(strDestFileName));
        string strDestFile = Server.MapPath("mergefile") + "//" + Path.GetFileName(strDestFileName);
        string strFinalDest = Server.MapPath("FinalFile");
        Process(afileUpload.PostedFile.FileName, strDestFileName, lngSize, afileUpload.PostedFile);
        MergerProcess(strSrcFile, strDestFile, strFinalDest);

    }

    //**********  Start *************//
    private string zUploadfileVideo1(FileUpload afileUpload, string docno, string item)
    {
        string filenamedata = "";
        string _itemNameNew = "";
        string datetimeNow = "";
        if (afileUpload.HasFile == false)
        {

        }
        else
        {
            string _Path = _PathFile + _Folder_course_video + "\\" + docno;// + "\\" + item.ToString();
            string mapPath = HttpContext.Current.Server.MapPath(_Path);
            string phyicalFilePath = string.Empty;
            string _itemExtension = "";
            String filename = string.Empty;
            if (Directory.Exists(mapPath) == false)
            {
                Directory.CreateDirectory(mapPath);
            }
            try
            {

                filename = afileUpload.FileName;
                _itemExtension = Path.GetExtension(afileUpload.PostedFile.FileName);
                datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                _itemNameNew = item.ToString() + "_" + emp_idx.ToString() + datetimeNow + _itemExtension.ToLower();
                phyicalFilePath = mapPath + "\\" + _itemNameNew;
                afileUpload.SaveAs(phyicalFilePath);
                filenamedata = _itemNameNew;
                //var flvpath = ConvertToFLV(phyicalFilePath);
                //var tumbnail = CreateThumbnail(phyicalFilePath);

                //Response.Write("{success:true, name:\"" + filename + "\", path:\"" + _Path + "/" +
                //                       Path.GetFileName(flvpath) + "\", image:\"" + _Path + "/" + Path.GetFileName(tumbnail) + "\"}");

            }
            catch (Exception)
            {
                //  Response.Write("{success:false}");

            }
        }

        return filenamedata;

    }
    private void zDelVideo(string docno, string Video)
    {
        if ((docno != "") && (Video != ""))
        {
            string _Path = _PathFile + _Folder_course_video + "\\" + docno + "\\" + Video;
            try
            {
                File.Delete(Server.MapPath(_Path));
            }
            catch { }
        }

    }
    private void zDelImages(string docno, string Images)
    {
        if ((docno != "") && (Images != ""))
        {
            string _Path = _PathFile + _Folder_course_video_image + "\\" + docno + "\\" + Images;
            try
            {
                File.Delete(Server.MapPath(_Path));
            }
            catch { }
        }

    }
    private string zUploadfileImage(FileUpload afileUpload, string docno, string item)
    {
        string filenamedata = "";
        if (afileUpload.HasFile == false)
        {

        }
        else
        {
            string _Path = _PathFile + _Folder_course_video_image + "\\" + docno;// + "\\" + item.ToString();
            string mapPath = HttpContext.Current.Server.MapPath(_Path);
            string phyicalFilePath = string.Empty;
            string _itemExtension = "";
            string _itemNameNew = "";
            string datetimeNow = "";
            String filename = string.Empty;
            if (Directory.Exists(mapPath) == false)
            {
                Directory.CreateDirectory(mapPath);
            }
            try
            {

                filename = afileUpload.FileName;
                _itemExtension = Path.GetExtension(afileUpload.PostedFile.FileName);
                datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                _itemNameNew = item.ToString()+"_"+emp_idx.ToString() + datetimeNow + _itemExtension.ToLower();
                phyicalFilePath = mapPath + "\\" + _itemNameNew;
                afileUpload.SaveAs(phyicalFilePath);
                filenamedata = _itemNameNew;

            }
            catch (Exception)
            {
                //  Response.Write("{success:false}");

            }
        }

        return filenamedata;
        
    }


    private string CreateThumbnail(string phyicalFilePath)
    {
        if (AuthenticationHelper.ImpersonateValidUser("user", ".", "*****"))
        {
            //var argument = string.Format("-ss 12 -i {0} -f image2 -vframes 1 {1}", phyicalFilePath, Path.ChangeExtension(phyicalFilePath, "jpg"));
            var argument = string.Format("-y -i {0} -an -ss 00:00:14.35 -r 1 -vframes 1 -f mjpeg  {1}", phyicalFilePath, Path.ChangeExtension(phyicalFilePath, "jpg"));

            ProcessStartInfo process = new ProcessStartInfo(ffmpegPhysicalPath, argument);
            Process proc = new Process();
            proc.StartInfo = process;
            proc.Start();
            proc.WaitForExit();
            AuthenticationHelper.UndoImpersonation();
            return Path.ChangeExtension(phyicalFilePath, "jpg");
        }

        return string.Empty;
    }
    private string ConvertToFLV(string phyicalFilePath)
    {
        if (Path.GetExtension(phyicalFilePath).Equals(".flv")) return phyicalFilePath;
        if (AuthenticationHelper.ImpersonateValidUser("user", ".", "*********"))
        {
            var argument = string.Format("-i {0} -vcodec flv -f flv -r 29.97 -s 320x240 -aspect 4:3 -b 300k -g 160 -cmp dct  -subcmp dct  -mbd 2 -flags +aic+cbp+mv0+mv4 -trellis 1 -ac 1 -ar 22050 -ab 56k {1}", phyicalFilePath, Path.ChangeExtension(phyicalFilePath, "flv"));
            // var argument = string.Format("-i {0} -crf 35.0 -vcodec libx264 -acodec libfaac -ar 48000 -ab 128k -coder 1 -flags +loop -cmp +chroma -partitions +parti4x4+partp8x8+partb8x8 -me_method hex -subq 6 -me_range 16 -g 250 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -b_strategy 1 -threads 0 {1}", phyicalFilePath, Path.ChangeExtension(phyicalFilePath, "mp4"));

            ProcessStartInfo process = new ProcessStartInfo(ffmpegPhysicalPath, argument);
            Process proc = new Process();
            proc.StartInfo = process;
            proc.Start();
            proc.WaitForExit();
            AuthenticationHelper.UndoImpersonation();
            return Path.ChangeExtension(phyicalFilePath, "flv");
        }

        return string.Empty;
    }
    public static class AuthenticationHelper
    {
        public const int LOGON32_LOGON_INTERACTIVE = 2;
        public const int LOGON32_PROVIDER_DEFAULT = 0;


        private static WindowsImpersonationContext impersonationContext;

        [DllImport("advapi32.dll")]
        public static extern int LogonUserA(String lpszUserName,
                                            String lpszDomain,
                                            String lpszPassword,
                                            int dwLogonType,
                                            int dwLogonProvider,
                                            ref IntPtr phToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int DuplicateToken(IntPtr hToken,
                                                int impersonationLevel,
                                                ref IntPtr hNewToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool RevertToSelf();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern bool CloseHandle(IntPtr handle);


        public static bool ImpersonateValidUser(String userName, String domain, String password)
        {
            WindowsIdentity tempWindowsIdentity;
            IntPtr token = IntPtr.Zero;
            IntPtr tokenDuplicate = IntPtr.Zero;

            if (RevertToSelf())
            {
                if (LogonUserA(userName, domain, password, LOGON32_LOGON_INTERACTIVE,
                               LOGON32_PROVIDER_DEFAULT, ref token) != 0)
                {
                    if (DuplicateToken(token, 2, ref tokenDuplicate) != 0)
                    {
                        tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                        impersonationContext = tempWindowsIdentity.Impersonate();
                        if (impersonationContext != null)
                        {
                            CloseHandle(token);
                            CloseHandle(tokenDuplicate);
                            return true;
                        }
                    }
                }
            }
            if (token != IntPtr.Zero)
                CloseHandle(token);
            if (tokenDuplicate != IntPtr.Zero)
                CloseHandle(tokenDuplicate);
            return false;
        }

        public static void UndoImpersonation()
        {
            impersonationContext.Undo();
        }
    }


    //**********  End *************//

    //*** Start Upload file ***//

    private FileStream fSIn, fSout;
    private int preDefinedCacheSize;
    public string MergerProcess(string strSrcPath, string strDestPath, string strFilnalDest)
    {
        string _Path = "";
        try
        {
            string[] strFiles = Directory.GetFiles(strSrcPath, "*.part");

            this.fSout = new FileStream(strDestPath, FileMode.Create);
            BinaryWriter wFSOut = new BinaryWriter(this.fSout);
            long fileSizes = 0;
            fileSizes = this.GetSizes(strFiles);
            foreach (string a in strFiles)
            {
                this.preDefinedCacheSize = this.DefineCache();
                this.fSIn = new FileStream(strSrcPath + "\\" + this.FileName(a), FileMode.Open);
                BinaryReader rFSIn = new BinaryReader(this.fSIn);
                if (this.preDefinedCacheSize > this.fSIn.Length - this.fSIn.Position)
                {
                    this.preDefinedCacheSize = (int)this.fSIn.Length - (int)this.fSIn.Position;
                }

                byte[] buffer = new byte[this.preDefinedCacheSize];
                while (this.fSIn.Position != this.fSIn.Length)
                {
                    rFSIn.Read(buffer, 0, this.preDefinedCacheSize);
                    wFSOut.Write(buffer);
                    Thread.Sleep(1);
                }

                rFSIn.Close();
                this.fSIn.Close();
            }

            wFSOut.Close();
            this.fSout.Close();
            string strFolderToDelete = strSrcPath;
            if (Directory.Exists(strFolderToDelete))
            {
                Directory.Delete(strFolderToDelete, true);
            }

            if (File.Exists(strDestPath))
            {
                File.Copy(strDestPath, strFilnalDest + "//" + Path.GetFileName(strDestPath), false);
                File.Delete(strDestPath);
            }
            _Path = Path.GetFileName(strDestPath);
        }
        catch (Exception ex)
        {
            object[] customval = new object[0];
            //AppError.ErrorMsg(ex.StackTrace, "UploadChunk.cs", "MergerProcess", customval);
        }
        return _Path;

    }


    public void Process(string strSrcPath, string strDestPath, long lngFileSize, System.Web.HttpPostedFile fsi)
    {
        string strDirectory = string.Empty, strNewFileNames = string.Empty;
        long fileSize = 0;
        int intCounter = 0;
        try
        {
            //// Code to Check whether it is logical or not to Continue...

            ////FSIn = new FileStream(strSrcPath, FileMode.Open);
            ////BinaryReader rFSIn = new BinaryReader(FSIn);
            BinaryReader rFSIn = new BinaryReader(fsi.InputStream);
            ////FileSize = FSIn.Length;
            fileSize = fsi.ContentLength;

            strDirectory = strDestPath;

            ////split it to parts in a folder Called "FileName"
            System.IO.Directory.CreateDirectory(strDirectory);

            ////begin writing
            ////while (FSIn.Position != FSIn.Length)
            while (rFSIn.BaseStream.Position != fsi.ContentLength)
            {
                this.preDefinedCacheSize = this.DefineCache();
                byte[] buffer = new byte[this.preDefinedCacheSize];
                strNewFileNames = strDirectory + "\\" + intCounter.ToString() + ".part";
                this.fSout = new FileStream(strNewFileNames, FileMode.Create);
                BinaryWriter wFSOut = new BinaryWriter(this.fSout);
                ////while ((FSout.Position < lngFileSize) && (FSIn.Position != FSIn.Length))
                while ((this.fSout.Position < lngFileSize) && (rFSIn.BaseStream.Position != fsi.ContentLength))
                {
                    ////if (((FSIn.Length - FSIn.Position) < Math.Min(PreDefinedCacheSize, (int)lngFileSize)) && (PreDefinedCacheSize > lngFileSize))
                    if (((fsi.ContentLength - rFSIn.BaseStream.Position) < Math.Min(this.preDefinedCacheSize, (int)lngFileSize)) && (this.preDefinedCacheSize > lngFileSize))
                    {
                        this.preDefinedCacheSize = (int)fsi.ContentLength - (int)rFSIn.BaseStream.Position;
                        rFSIn.Read(buffer, 0, this.preDefinedCacheSize);
                        wFSOut.Write(buffer);
                        Thread.Sleep(1);
                    }
                    else
                    {
                        if (this.preDefinedCacheSize > lngFileSize)
                        {
                            this.preDefinedCacheSize = (int)lngFileSize;
                        }

                        rFSIn.Read(buffer, 0, this.preDefinedCacheSize);
                        wFSOut.Write(buffer);
                        Thread.Sleep(1);
                    }
                }

                wFSOut.Close();
                this.fSout.Close();
                intCounter++;
            }

            ////finish
            rFSIn.Close();
        }
        catch (Exception ex)
        {
            object[] customval = new object[0];
            //AppError.ErrorMsg(ex.StackTrace, "UploadChunk.cs", "Process", customval);
        }
    }

    private int DefineCache()
    {
        return 8192 * 2;
    }

    private string FileName(string strString)
    {
        return strString.Substring(strString.LastIndexOf("\\"));
    }
    private long GetSizes(string[] strFileZ)
    {
        long intSizeToReturn = 0;
        foreach (string a in strFileZ)
        {
            FileStream tmpFS = new FileStream(a, FileMode.Open);
            intSizeToReturn += tmpFS.Length;
            tmpFS.Close();
        }

        return intSizeToReturn;
    }

    //*** End Upload file ***//
    public string getImgUrl(string ADocno = "", string AImage = "")
    {
        string sPathImage = "";
       // string host = HttpContext.Current.Request.Url.Host;
       // host = _func_dmu.zHostname(host);
       // host = "http://" + host;
        if (AImage == "")
        {
            sPathImage = _PathFileimage + _func_dmu._IconnoImage;
        }
        else
        {
            sPathImage = _PathFile + _Folder_course_video_image + "/" + ADocno + "/" + AImage;
        }
        sPathImage = ResolveUrl(sPathImage);

        return sPathImage;
    }
    public string getvideoUrl(string ADocno = "", string AImage = "")
    {
        string sPathImage = "";
        //string host = HttpContext.Current.Request.Url.Host;
        //host = _func_dmu.zHostname(host);
        //host = "http://" + host;
        if (AImage == "")
        {
            sPathImage = _PathFileimage + _func_dmu._IconnoImage;
        }
        else
        {
            sPathImage = _PathFile + _Folder_course_video + "/" + ADocno + "/" + AImage;
        }

        sPathImage = ResolveUrl(sPathImage);
        return sPathImage;
    }
    public string getVideoUrl(string ADocno = "", string AImage = "")
    {
        string sPathImage = "";
        //string host = HttpContext.Current.Request.Url.Host;
        //host = _func_dmu.zHostname(host);
        //host = "http://"+host;
        if (AImage == "")
        {
            sPathImage = _PathFileimage + _func_dmu._IconnoImage;
        }
        else
        {
            sPathImage = _PathFile + _Folder_course_video + "/" + ADocno + "/" + AImage;
        }

        sPathImage = ResolveUrl(sPathImage);
        return sPathImage;
    }

    private void zShowdataDetail(int id)
    {

        _data_elearning.el_course_action = new course[1];
        course obj = new course();
        obj.u0_course_idx_ref = id;
        obj.operation_status_id = "U6-FULL-DETIAL";
        _data_elearning.el_course_action[0] = obj;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, _data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _func_dmu.zSetRepeaterData(rptList, _data_elearning.el_course_action);
        setHiddenFieldEmpty();
        ltTitle.Text = "";
        if (_data_elearning.el_course_action != null)
        {
            obj = _data_elearning.el_course_action[0];
            ltTitle.Text = "หลักสูตร : " + obj.course_no + " - " + obj.course_name;
            Hddfld_course_no.Value = obj.course_no;
            Hddfld_status.Value = "D";
            Hddfld_u0_course_idx_ref.Value = obj.u0_course_idx_ref.ToString();
            Hddfld_u6_course_idx.Value = obj.u6_course_idx.ToString();
        }


    }
    private void setHiddenFieldEmpty()
    {
        Hddfld_course_no.Value = "";
        Hddfld_status.Value = "";
        Hddfld_u0_course_idx_ref.Value = "";
        Hddfld_u6_course_idx.Value = "";
    }
    #region repeater
    protected void rptItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rptName = (Repeater)sender;
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (rptName.ID == "rptList")
            {
                LinkButton btnUpdateDetail = (LinkButton)e.Item.FindControl("btnUpdateDetail");
                LinkButton btnDeleteDetail = (LinkButton)e.Item.FindControl("btnDeleteDetail");
                linkBtnTrigger(btnUpdateDetail);
                linkBtnTrigger(btnDeleteDetail);

            }
            else if (rptName.ID == "")
            {

            }
        }
    }
    #endregion repeater

    public string Truncate(string value, int maxLength)
    {
        if (string.IsNullOrEmpty(value)) return value;
        return value.Length <= maxLength ? value : value.Substring(0, maxLength) + "...";
    }

    private void zShowdataUpdate(int id)
    {
        _data_elearning.el_course_action = new course[1];
        course obj = new course();
        obj.u6_course_idx = id;
        obj.operation_status_id = "U6-FULL";
        _data_elearning.el_course_action[0] = obj;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, _data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _func_dmu.zSetFormViewData(fv_Update, _data_elearning.el_course_action);
        _FormView = getFv("E");
        _fldvideo_name = (FileUpload)_FormView.FindControl("fldvideo_name");
        _fldvideo_images = (FileUpload)_FormView.FindControl("fldvideo_images");
        //_ddlvideo_name = (DropDownList)_FormView.FindControl("ddlvideo_name");
        //_ddlvideo_images = (DropDownList)_FormView.FindControl("ddlvideo_images");

        //_ddlvideo_name.SelectedValue = "0";
        //_ddlvideo_images.SelectedValue = "0";
        //_fldvideo_name.Visible = false;
        //_fldvideo_images.Visible = false;
        setHiddenFieldEmpty();
        if (_data_elearning.el_course_action != null)
        {
            obj = _data_elearning.el_course_action[0];
            Hddfld_course_no.Value = obj.course_no;
            Hddfld_status.Value = "E";
            Hddfld_u0_course_idx_ref.Value = obj.u0_course_idx_ref.ToString();
            Hddfld_u6_course_idx.Value = obj.u6_course_idx.ToString();
        }
    }
    private void zDeleteDetail(string sArg)
    {
        
        string  su6_course_idx = "", scourse_no="", svideo_name = "",
            svideo_images = "";
        string[] calc = new string[4];
        if (sArg != "0")
        {
            calc = sArg.Split('|');
        }
        else
        {
            calc = new string[4] { "", "", "", "" };
        }
        su6_course_idx = calc[0];
        scourse_no = calc[1];
        svideo_name = calc[2];
        svideo_images = calc[3];

        if (_func_dmu.zStringToInt(su6_course_idx) == 0)
        {
            return;
        }

        //_FormView = getFv("E");
        //_txtcourse_no = (TextBox)_FormView.FindControl("txtcourse_no");
        //_txthddfld_video_name = (TextBox)_FormView.FindControl("txthddfld_video_name");
        //_txthddfld_video_images = (TextBox)_FormView.FindControl("txthddfld_video_images");

        course obj_course = new course();
        _data_elearning.el_course_action = new course[1];
        obj_course.u6_course_idx = _func_dmu.zStringToInt(su6_course_idx);
        obj_course.course_updated_by = emp_idx;
        obj_course.operation_status_id = "U6";
        _data_elearning.el_course_action[0] = obj_course;
        _func_dmu.zCallServiceNetwork(_urlDelel_u_course, _data_elearning);
        zDelVideo(scourse_no, svideo_name);
        zDelImages(scourse_no, svideo_images);

    }



    #region reuse
    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }
    #endregion

    private void zShowdataInsertDetail(int id)
    {
        
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_course_action = new course[1];
        course obj_course_empty = new course();
        obj_course_empty.operation_status_id = "U6-EMPTY";
        dataelearning.el_course_action[0] = obj_course_empty;
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, dataelearning);
        _func_dmu.zSetFormViewData(fvCRUD, dataelearning.el_course_action);

        _FormView = getFv("I");
        _txttraining_group_name = (TextBox)_FormView.FindControl("txttraining_group_name");
        _txttraining_branch_name = (TextBox)_FormView.FindControl("txttraining_branch_name");
        _ddlu0_course_idx_ref = (DropDownList)_FormView.FindControl("ddlu0_course_idx_ref");
        _txtcourse_date = (TextBox)_FormView.FindControl("txtcourse_date");
        _txtlevel_code = (TextBox)_FormView.FindControl("txtlevel_code");
        _txtcourse_score = (TextBox)_FormView.FindControl("txtcourse_score");
        _txttraining_plan_year = (TextBox)_FormView.FindControl("txttraining_plan_year");
        _txtcourse_no = (TextBox)_FormView.FindControl("txtcourse_no");
        _txtvideo_title = (TextBox)_FormView.FindControl("txtvideo_title");
        _txtvideo_description = (TextBox)_FormView.FindControl("txtvideo_description");
        _txtscore_through_per = (TextBox)_FormView.FindControl("txtscore_through_per");

        _txttraining_plan_year.Text = DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture);
        Hddfld_course_no.Value = "";
        //Hddfld_u0_course_idx_ref.Value = "";
        Hddfld_u6_course_idx.Value = "";
        Hddfld_status.Value = "I";
        
        _txttraining_group_name.Text = "";
        _txttraining_branch_name.Text = "";
        _txtcourse_date.Text = "";
        _txtlevel_code.Text = "";
        _txtcourse_score.Text = "";
        _txtcourse_no.Text = "";
        

        _func_dmu.zDropDownList(_ddlu0_course_idx_ref,
                                "",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-COURSE-U0-LOOKUP-VIDEO"
                                );
        
        _ddlu0_course_idx_ref.SelectedValue = id.ToString();
        dataelearning = _func_dmu.zShowDataDsLookup("TRN-COURSE-U0-LOOKUP-VIDEO",
                                    "idx",
                                    _func_dmu.zStringToInt(_ddlu0_course_idx_ref.SelectedValue)
                                    );
        if (dataelearning.trainingLoolup_action != null)
        {
            foreach (var item in dataelearning.trainingLoolup_action)
            {
                _txttraining_group_name.Text = item.training_group_name;
                _txttraining_branch_name.Text = item.training_branch_name;
                _txtcourse_date.Text = item.doc_date;
                _txtlevel_code.Text = item.level_code.ToString();
                _txtcourse_score.Text = item.score.ToString();
                _txtcourse_no.Text = item.doc_no;
                _txtscore_through_per.Text = item.score_through_per.ToString();
            }

        }

        
    }

    
}