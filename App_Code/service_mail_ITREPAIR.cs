﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.IO;


/// <summary>
/// Summary description for service_mail_ITREPAIR
/// </summary>
public class service_mail_ITREPAIR
{
    public service_mail_ITREPAIR()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void SendHtmlFormattedEmail(string recepientEmailList, string subject, string body)
    {
        using (MailMessage mailMessage = new MailMessage())
        {
            mailMessage.From = new MailAddress("noreply@taokaenoi.co.th");
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            string[] recepientEmail = recepientEmailList.Split(',');
            foreach (string rEmail in recepientEmail)
            {
                if (rEmail != String.Empty)
                {
                    mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                }
            }
            mailMessage.Bcc.Add(new MailAddress("noreply@taokaenoi.co.th"));
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;
            System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
            NetworkCred.UserName = "noreply@taokaenoi.co.th";
            NetworkCred.Password = "knlolksosgsuakcd";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mailMessage);
        }
    }


    public void SendHtmlFormattedEmailComment(string recepientEmailList, string subject, string body)
    {
        using (MailMessage mailMessage = new MailMessage())
        {
            mailMessage.From = new MailAddress("noreply@taokaenoi.co.th");
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            string[] recepientEmail = recepientEmailList.Split(',');
            foreach (string rEmail in recepientEmail)
            {
                if (rEmail != String.Empty)
                {
                    mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                }
            }
            mailMessage.Bcc.Add(new MailAddress("noreply@taokaenoi.co.th"));
            mailMessage.ReplyToList.Add("sap@taokaenoi.co.th");

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;
            System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
            NetworkCred.UserName = "noreply@taokaenoi.co.th";
            NetworkCred.Password = "knlolksosgsuakcd";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mailMessage);
           }
    }




    #region api_itrepair create
    public string ITRepairCreateBody(UserRequestList insert)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", insert.DocCode);
        body = body.Replace("{Priority_name}", insert.Priority_name);
        body = body.Replace("{FullNameTH}", insert.EmpName);
        body = body.Replace("{RDeptName}", insert.RDeptName);
        body = body.Replace("{MobileNo}", insert.MobileNo);
        body = body.Replace("{LocName}", insert.LocName);
        body = body.Replace("{UserLogonName}", insert.UserLogonName); 
        body = body.Replace("{detailUser}", insert.detailUser);
        body = body.Replace("{Email}", insert.Email);

        return body;
    }

    #endregion 

    #region api_itrepair sapaccept
    public string ITRepairSapAcceptBody(UserRequestList _u0_userrequest)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_sapaccept.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{AdminName}", _u0_userrequest.AdminName);
      
        return body;
    }

    #endregion

    #region api_itrepair senduser
    public string ITRepairSendUserBody(UserRequestList _u0_userrequest, string urqidx)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_senduser.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{AdminName}", _u0_userrequest.AdminName);
        body = body.Replace("URQIDX", urqidx);


        return body;
    }

    #endregion

    #region api_itrepair useraccept
    public string ITRepairUserAcceptBody(UserRequestList _u0_userrequest)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_useraccept.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
       
        return body;
    }

    #endregion 

    #region api_itrepair senduser
    public string ITRepairUsernotAcceptBody(UserRequestList _u0_userrequest, string urqidx)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_usernotaccept.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{AdminName}", _u0_userrequest.AdminName);
        body = body.Replace("URQIDX", urqidx);

        return body;
    }

    #endregion

    #region api_itrepair comment
    public string ITRepairCommentBody(UserRequestList _u0_userrequest)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_comment.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{FullNameTH}", _u0_userrequest.FullNameTH);
        body = body.Replace("{CommentAuto}", _u0_userrequest.CommentAuto);
        body = body.Replace("{CDate}", _u0_userrequest.CDate);
        body = body.Replace("{CTime}", _u0_userrequest.CTime);

        return body;
    }

    #endregion

    #region api_itrepair usernotaccept
    public string ITRepairUserAcceptBody(UserRequestList _u0_userrequest, string urqidx)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ITRepair_usernotaccept.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{DocCode}", _u0_userrequest.DocCode);
        body = body.Replace("{detailUser}", _u0_userrequest.detailUser);
        body = body.Replace("{AdminName}", _u0_userrequest.AdminName);
        body = body.Replace("URQIDX", urqidx);


        return body;
    }

    #endregion


}