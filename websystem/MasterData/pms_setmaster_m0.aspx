﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="pms_setmaster_m0.aspx.cs" Inherits="websystem_MasterData_pms_setmaster_m0" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script src='<%=ResolveUrl("~/Scripts/tinymce/jquery.tinymce.min.js")%>'></script>
    <script src='<%=ResolveUrl("~/Scripts/tinymce/tinymce.js")%>'></script>
    <script src='<%=ResolveUrl("~/Scripts/tinymce/tinymce.min.js")%>'></script>

    <script type="text/javascript">


        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            // inline: true,
            force_br_newlines: true,
            force_p_newlines: false,
            forced_root_block: "",
            menubar: false,
            resize: false,
            statusbar: false,
            entity_encoding: 'raw',
            entities: "160,nbsp,38,amp,34,quot,162,cent,8364,euro,163,pound,165,yen,169,copy,174,reg,8482,trade,8240,permil,60,lt,62,gt,8804,le,8805,ge,176,deg,8722,minus",
            //entities: '160,nbsp,60,lt,62,gt',
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo ",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });

            }

        });

        //var plainText = tinymce.activeEditor.getContent().replace(/<[^>] *>/ g, "");
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        //var my_editor = tinymce.get('require');
        //var content = my_editor.getContent();
        //if ($(my_editor.getBody()).text() == '') alert('กรุณาป้อนข้อมูลให้ครบ!!');

        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                //inline: true,
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: "",
                menubar: false,
                resize: false,
                statusbar: false,
                entity_encoding: 'raw',
                entities: "160,nbsp,38,amp,34,quot,162,cent,8364,euro,163,pound,165,yen,169,copy,174,reg,8482,trade,8240,permil,60,lt,62,gt,8804,le,8805,ge,176,deg,8722,minus",
                //entities: '160,nbsp,60,lt,62,gt',
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }


            });

        })

    </script>

    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <!-- background-color: #020202 -->

    
   
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden; background-color: #eef0f1e3 ;" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left" runat="server">
                    <li id="_divMenuLiToViewIndex" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivIndex"   runat="server"
                            CommandName="_divMenuBtnToDivIndex"
                            OnCommand="btnCommand" Text="รายการหัวข้อ" />
                    </li>


                    <li id="_divMenuLiToViewAdd" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAdd" runat="server"
                            CommandName="_divMenuBtnToDivAdd"
                            OnCommand="btnCommand" Text="เพิ่มข้อมูล" />
                    </li>
                </ul>

                <%--<ul class="nav navbar-nav navbar-right" runat="server">

                    <li id="_divMenuLiToViewManual" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDivManual" runat="server"
                            NavigateUrl="https://docs.google.com/document/d/18VKWH8jnN4i-KJLloZ7YLKVBNzrPanSXkTDIAdcBFMY/edit?usp=sharing" Target="_blank"
                            CommandName="_divMenuBtnToDivManual"
                            OnCommand="btnCommand" Text="คู่มือการใช้งาน" />
                    </li>

                    <li id="_divMenuLiToViewFlow" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDivFlow" runat="server"
                            NavigateUrl="https://drive.google.com/file/d/1vFkON0MNMnm-cPJbNRdjP-qmq-SKOpxq/view?usp=sharing" Target="_blank"
                            CommandName="_divMenuBtnToDivFlow"
                            OnCommand="btnCommand" Text="Flow Chart" />
                    </li>
                </ul>--%>
            </div>
        </nav>
    </div>
   
    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">

            <div class="col-md-12">
                <asp:Literal ID="litPanelTitle" runat="server" Text="Title"></asp:Literal>
                
            </div>
            

            <div class="col-md-12" runat="server" visible="false">

                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: khaki; text-align: left">
                        <h4><b>
                            <asp:Label ID="lblheadbanner" runat="server"></asp:Label></b></h4>
                    </blockquote>
                </div>
            </div>
            

            <div class="clearfix"></div>
            <br>
           

            <div id="div_btn" runat="server" class="col-md-12">
                <div class="form-group">
                    <ul class="nav nav-tabs bg-default" >

                        <li id="_divMenuLiToViewFormResult" runat="server" class="active">
                            <asp:LinkButton ID="btndata_formresult" CssClass="btn btn-default  btn-sm" runat="server" CommandName="cmddata_formresult" OnCommand="btnCommand" title="ข้อมูลแบบฟอร์มประเมิน"><h5><i class="glyphicon glyphicon-list-alt"></i> แบบฟอร์มประเมิน</h5></asp:LinkButton>
                        </li>
                        <li id="_divMenuLiToViewTopic" runat="server">
                            <asp:LinkButton ID="btndbuy_topic" CssClass="btn btn-default  btn-sm" runat="server" CommandName="cmddata_topic" OnCommand="btnCommand" title="ข้อมูลหัวข้อหลัก"><h5><i class="glyphicon glyphicon-align-justify"></i> Master หัวข้อหลัก</h5></asp:LinkButton>
                        </li>
                        <li id="_divMenuLiToViewSubTopic" runat="server">
                            <asp:LinkButton ID="btndbuy_subtopic" CssClass="btn btn-default  btn-sm" runat="server" CommandName="cmddata_subtopic" OnCommand="btnCommand" title="ข้อมูลหัวข้อย่อย"><h5><i class="
glyphicon glyphicon-th-list"></i> Master หัวข้อย่อย</h5></asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-12" id="div_formresult" runat="server">
               
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GvForm_Result" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-md-12"
                            HeaderStyle-CssClass="success small"
                            HeaderStyle-Height="40px"
                            AllowPaging="false"
                            DataKeyNames="u0_typeidx"
                            OnRowDataBound="Master_RowDataBound"
                            OnRowEditing="Master_RowEditing"
                            OnRowCancelingEdit="Master_RowCancelingEdit"
                            OnRowUpdating="Master_RowUpdating">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">ไม่มีแบบฟอร์มประเมิน</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="องค์กร" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOrgNameTH" runat="server" Text='<%# Eval("OrgNameTH") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <div class="form-horizontal" role="form">
                                            <div class="panel-heading">
                                                <div class="form-group">
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txtu0_typeidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("u0_typeidx")%>' />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label2" CssClass="col-md-3 control-label" runat="server" Text="องค์กร" />
                                                    <div class="col-md-9">
                                                        <asp:TextBox ID="txtorgidx" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("orgidx")%>' />
                                                        <asp:DropDownList ID="ddlorgidx_edit" Enabled="false" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label26" runat="server" Text="ชื่อฟอร์ม" CssClass="col-md-3 control-label text_right"></asp:Label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox ID="txtnform_name" runat="server" CssClass="form-control" Text='<%# Eval("form_name")%>' />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label17" runat="server" Text="กลุ่มพนักงาน" CssClass="col-md-3 control-label text_right"></asp:Label>
                                                    <div class="col-md-9">
                                                        

                                                        <asp:CheckBoxList ID="chkposidx_edit" runat="server" Enabled="false" CellPadding="5" CellSpacing="5" ValidationGroup="Create_Actor1" RepeatColumns="2" Width="50%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />


                                                        <asp:TextBox ID="txtposidx_comma" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("posidx_comma")%>' />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="lbstatate" CssClass="col-md-3 control-label" runat="server" Text="Status" />
                                                    <div class="col-md-9">
                                                        <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                            <asp:ListItem Value="1">ใช้งานปกติ</asp:ListItem>
                                                            <asp:ListItem Value="2">ปิดการใช้งาน</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>


                                                <div class="col-md-12">
                                                    
                                                    <div class="form-group pull-right">

                                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i> SAVE</asp:LinkButton>
                                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                
                                                    </div>
                                                    
                                                </div>

                                            </div>
                                        </div>


                                    </EditItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อฟอร์ม" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lblform_name" runat="server" Text='<%# Eval("form_name") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="กลุ่มพนักงาน" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lblpos_name" runat="server" Text='<%# Eval("pos_name") %>'></asp:Label>
                                        <asp:Label ID="lblposidx_comma" Visible="false" runat="server" Text='<%# Eval("posidx_comma")%>' />

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lblcreate_date" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>
                                        <asp:Label ID="lblstatusidx" Visible="false" runat="server" Text='<%#Eval("status") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="จัดการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btndetail" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdDetail" OnCommand="btnCommand" data-toggle="tooltip" title="View" CommandArgument='<%# Eval("u0_typeidx") + ";" + Eval("status")%>'><i class="glyphicon glyphicon-folder-open"></i></asp:LinkButton>
                                        <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                        <asp:LinkButton ID="btnadd" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdAdd_form" OnCommand="btnCommand" data-toggle="tooltip" title="Add" CommandArgument='<%# Eval("orgidx")+ ";" + Eval("posidx_comma")+ ";" + Eval("form_name")%>'><i class="glyphicon glyphicon-plus"></i></asp:LinkButton>
                                        <asp:LinkButton ID="btndelete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelete_form" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("u0_typeidx") %>'><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>

                </asp:UpdatePanel>


            </div>

            <div class="col-md-12" id="div_topic" runat="server" visible="false">

                
                <asp:UpdatePanel ID="upActor1Node1Files" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GvTopic" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-md-12"
                            HeaderStyle-CssClass="info small"
                            HeaderStyle-Height="40px"
                            AllowPaging="true"
                            PageSize="10"
                            OnRowDataBound="Master_RowDataBound"
                            OnRowEditing="Master_RowEditing"
                            OnRowCancelingEdit="Master_RowCancelingEdit"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            OnRowUpdating="Master_RowUpdating"
                            DataKeyNames="m1_typeidx">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="หัวข้อหลัก" ItemStyle-Width="70%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbldoccode" runat="server" Text='<%# Eval("m1_type_name") %>'></asp:Label>
                                        <asp:Label ID="lblm1_typeidx" Visible="false" runat="server" Text='<%# Eval("m1_typeidx") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <div class="form-horizontal" role="form">
                                            <div class="panel-heading">
                                                <div class="form-group">
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txtm1_typeidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m1_typeidx")%>' />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label26" runat="server" Text="หัวข้อหลัก" CssClass="col-md-3 control-label text_right"></asp:Label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox ID="txtname_edit" MaxLength="1000" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("m1_type_name")%>' />
                                                    </div>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtname_edit" Font-Size="11"
                                                        ErrorMessage="กรุณาใส่สถานะรายการ"
                                                        ValidationExpression="กรุณาใส่สถานะรายการ"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                        ValidationGroup="Save_edit" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtname_edit"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                </div>



                                                <div class="form-group">
                                                    <asp:Label ID="lbstatate" CssClass="col-md-3 control-label" runat="server" Text="Status" />
                                                    <div class="col-md-9">
                                                        <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                            <asp:ListItem Value="1">ใช้งานปกติ</asp:ListItem>
                                                            <asp:ListItem Value="2">ปิดการใช้งาน</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>


                                                <div class="col-md-12">
                                                    
                                                    <div class="form-group pull-right">
                                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i> SAVE</asp:LinkButton>
                                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                
                                                    </div>
                                                    
                                                </div>

                                            </div>
                                        </div>


                                    </EditItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="จัดการ" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                        <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelTopic" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m1_typeidx") + ";" + Eval("m1_type_name") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>

                </asp:UpdatePanel>

                   
            </div>

            <div class="col-md-12" id="div_subtopic" runat="server" visible="false">
                
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GvSubTopic" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-md-12"
                            HeaderStyle-CssClass="warning small"
                            HeaderStyle-Height="40px"
                            AllowPaging="true"
                            PageSize="10"
                            OnRowDataBound="Master_RowDataBound"
                            OnRowEditing="Master_RowEditing"
                            OnRowCancelingEdit="Master_RowCancelingEdit"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            OnRowUpdating="Master_RowUpdating"
                            DataKeyNames="m2_typeidx">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="หัวข้อหลัก" ItemStyle-Width="35%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lblm1_type_name" runat="server" Text='<%# Eval("m1_type_name") %>'></asp:Label>
                                        <asp:Label ID="lblm2_typeidx" Visible="false" runat="server" Text='<%# Eval("m2_typeidx") %>'></asp:Label>

                                    </ItemTemplate>

                                    <EditItemTemplate>
                                        <div class="form-horizontal" role="form">
                                            <div class="panel-heading">
                                                <div class="form-group">
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txtm2_typeidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m2_typeidx")%>' />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label2" CssClass="col-md-3 control-label" runat="server" Text="หัวข้อหลัก" />
                                                    <div class="col-md-9">
                                                        <asp:TextBox ID="txtm1_typeidx" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("m1_typeidx")%>' />
                                                        <asp:DropDownList ID="ddltopic_edit" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save_edit" runat="server" Display="None"
                                                            ControlToValidate="ddltopic_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกหัวข้อหลัก"
                                                            ValidationExpression="กรุณาเลือกหัวข้อหลัก" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidadtorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />


                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label26" runat="server" Text="หัวข้อย่อย" CssClass="col-md-3 control-label text_right"></asp:Label>
                                                    <div class="col-md-9">
                                                        <asp:TextBox ID="txtname_edit" MaxLength="1000" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("m2_type_name")%>' />
                                                    </div>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtname_edit" Font-Size="11"
                                                        ErrorMessage="กรุณาใส่หัวข้อย่อย"
                                                        ValidationExpression="กรุณาใส่หัวข้อย่อย"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                        ValidationGroup="Save_edit" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtname_edit"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                </div>



                                                <div class="form-group">
                                                    <asp:Label ID="lbstatate" CssClass="col-md-3 control-label" runat="server" Text="Status" />
                                                    <div class="col-md-9">
                                                        <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                            <asp:ListItem Value="1">ใช้งานปกติ</asp:ListItem>
                                                            <asp:ListItem Value="2">ปิดการใช้งาน</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>


                                                <div class="col-md-12">
                                                    
                                                    <div class="form-group pull-right">
                                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i> SAVE</asp:LinkButton>
                                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                
                                                    </div>
                                                    
                                                </div>

                                            </div>
                                        </div>


                                    </EditItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="หัวข้อย่อย" ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lblm2_type_name" runat="server" Text='<%# Eval("m2_type_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="จัดการ" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                        <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelSubTopic" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m1_typeidx") + ";" + Eval("m2_typeidx")+ ";" + Eval("m2_type_name") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>

                </asp:UpdatePanel>

                   
            </div>

            <div class="col-md-12" id="div_formdetail" runat="server">
               
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GvForm_Detail" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-md-12"
                            HeaderStyle-CssClass="success small"
                            HeaderStyle-Height="40px"
                            AllowPaging="false"
                            OnRowDataBound="Master_RowDataBound"
                            OnRowEditing="Master_RowEditing"
                            OnRowCancelingEdit="Master_RowCancelingEdit"
                            OnRowUpdating="Master_RowUpdating"
                            DataKeyNames="r0_typeidx">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="หัวข้อหลัก" ItemStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lblm1_type_name" runat="server" Text='<%# Eval("m1_type_name") %>'></asp:Label>
                                    </ItemTemplate>

                                    <EditItemTemplate>
                                        <div class="form-horizontal" role="form">
                                            <div class="panel-heading">
                                                <div class="form-group">
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txtr0_typeidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("r0_typeidx")%>' />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label2" CssClass="col-md-3 control-label" runat="server" Text="หัวข้อหลัก" />
                                                    <div class="col-md-8">
                                                        <asp:TextBox ID="txtm1_typeidx_edit" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("m1_typeidx")%>' />
                                                        <asp:DropDownList ID="ddltopic_edit" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label12" CssClass="col-md-3 control-label" runat="server" Text="หัวข้อย่อย" />
                                                    <div class="col-md-8">
                                                        <asp:TextBox ID="txtm2_typeidx_edit" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("m2_typeidx")%>' />
                                                        <asp:DropDownList ID="ddlsubtopic_edit" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <asp:Label ID="lbstatate" CssClass="col-md-3 control-label" runat="server" Text="Status" />
                                                    <div class="col-md-8">
                                                        <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status") %>'>
                                                            <asp:ListItem Value="1">ใช้งานปกติ</asp:ListItem>
                                                            <asp:ListItem Value="2">ปิดการใช้งาน</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>


                                                <div class="col-md-1 col-md-offset-10">
                                                    <div class="form-group pull-right">
                                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </EditItemTemplate>



                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="หัวข้อย่อย" ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lblm2_type_name" runat="server" Text='<%# Eval("m2_type_name") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstatus" runat="server" Text='<%# getStatus((int)Eval("status")) %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="จัดการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnedit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                        <asp:LinkButton ID="btndelete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelete_formdetail" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("r0_typeidx") %>'><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                    </ContentTemplate>

                </asp:UpdatePanel>

               

                <div class="form-group pull-right">
                    
                    <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการกลับหน้าหลักแบบฟอร์มประเมินใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                   
                </div>


            </div>

        </asp:View>

        <asp:View ID="ViewInsert" runat="server">

            <div class="col-md-12">
                <asp:Image ID="imgheadcreate" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/pms-formresult/banner_form1.png" Style="height: 100%; width: 100%;" />
            </div>
            


            <div id="div_userCreate" runat="server" visible="false">
                <div class="col-md-12" >
                    <div class="panel panel-info" >
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                        </div>
                        <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Label7" CssClass="col-md-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label9" CssClass="col-md-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>


                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Label4" CssClass="col-md-2 control-label" runat="server" Text="บริษัท : " />
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label34" CssClass="col-md-3 control-label" runat="server" Text="ฝ่าย : " />
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>


                                        <%-------------- แผนก,ตำแหน่ง --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Labedl1" class="col-md-2 control-label" runat="server" Text="แผนก : " />
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label8" CssClass="col-md-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>

                                        <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Label5" class="col-md-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                            <div class="col-md-3">
                                                <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                                <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label6" CssClass="col-md-3 control-label" runat="server" Text="E-mail : " />
                                            <div class="col-md-3">
                                                <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                                <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
            </div>

            
            <div class="clearfix"></div>
            <br>


            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; สร้างแบบประเมิน</strong></h3>
                    </div>
                    <div class="form-horizontal" role="form">
                        <div class="panel-body">

                            <div class="col-md-12">
                                <div class="col-md-4">

                                    <asp:ImageButton ID="imgtopic" CssClass="img-responsive" runat="server" ToolTip="สร้างหัวข้อหลัก" Width="70%" CommandName="btnsystem" CommandArgument="1" OnCommand="btnCommand" ImageUrl="~/images/pms-formresult/btntopic.png" />
                                </div>

                                <div class="col-md-4">

                                    <asp:ImageButton ID="imgsubtopic" CssClass="img-responsive" ToolTip="สร้างข้อย่อย" runat="server" Width="70%" CommandName="btnsystem" CommandArgument="2" OnCommand="btnCommand" ImageUrl="~/images/pms-formresult/btnsubtopic.png" />
                                </div>

                                <div class="col-md-4">

                                    <asp:ImageButton ID="imgformresult" CssClass="img-responsive" ToolTip="สร้างฟอร์มประเมิน" runat="server" Width="70%" CommandName="btnsystem" CommandArgument="3" OnCommand="btnCommand" ImageUrl="~/images/pms-formresult/btnform.png" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div id="divadd_topic" runat="server" visible="false">

                <div class="col-md-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading" style="background-color: darksalmon;">
                            <h3 class="panel-title" style="color: white;"><i class="fa fa-cart-plus"></i><strong>&nbsp; เพิ่มหัวข้อหลัก</strong></h3>
                        </div>

                        <asp:FormView ID="fvadd_topic" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <%-- <div class="alert-message alert-message-success">
                                            <blockquote class="danger" style="font-size: small; background-color: blueviolet; text-align: left">
                                                <h4 style="color: whitesmoke"><b>สร้างหัวข้อหลัก</b></h4>
                                            </blockquote>

                                        </div>--%>
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddltype" CssClass="form-control" runat="server" Visible="false">
                                                <asp:ListItem Value="2" Selected="True">Competency</asp:ListItem>

                                            </asp:DropDownList>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label59" runat="server" Text="หัวข้อหลัก" CssClass="col-md-2 control-label"></asp:Label>
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txttopic" TextMode="MultiLine" Rows="3" MaxLength="1000" ValidationGroup="SaveTopic_DataSet" runat="server" CssClass="form-control "></asp:TextBox>
                                                <%--tinymce --%>
                                                <%-- CssClass="form-control"--%>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveTopic_DataSet" runat="server" Display="None" ControlToValidate="txttopic" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกหัวข้อหลัก"
                                                    ValidationExpression="กรุณากรอกหัวข้อหลัก"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                    ValidationGroup="SaveTopic_DataSet" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txttopic"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />


                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label4" runat="server" Text="สถานะการใช้งาน" CssClass="col-md-2 control-label"></asp:Label>
                                            <div class="col-md-9">
                                                <asp:DropDownList ID="ddlstatus" ValidationGroup="SaveTopic_DataSet" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">ใช้งานปกติ</asp:ListItem>
                                                    <asp:ListItem Value="2">ปิดการใช้งาน</asp:ListItem>

                                                </asp:DropDownList>


                                            </div>
                                        </div>


                                        <div class="form-group" runat="server">
                                            <div class="col-md-2 col-md-offset-2">
                                                <asp:LinkButton ID="btnAdddatasettopic" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdInsert_Dataset" OnCommand="btnCommand" ValidationGroup="SaveTopic_DataSet" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')" title="Add">ADD +</asp:LinkButton>
                                            </div>
                                        </div>

                                        <div class="form-group">


                                            <div class="col-md-12">
                                                <asp:GridView ID="GvTopicAdd" Visible="false"
                                                    runat="server"
                                                    HeaderStyle-CssClass="info"
                                                    CssClass="table table-striped table-responsive"
                                                    GridLines="None"
                                                    OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false">


                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>



                                                        <asp:TemplateField HeaderText="หัวข้อหลัก" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="75%" Visible="true" ItemStyle-Font-Size="Small">

                                                            <ItemTemplate>
                                                                <asp:Label ID="lbm0_typeidx" Visible="false" runat="server" Text='<%# Eval("m0_typeidx") %>' />
                                                                <asp:Literal ID="lbm1_type_name" runat="server" Text='<%# Eval("m1_type_name") %>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" Visible="true" ItemStyle-Font-Size="Small">

                                                            <ItemTemplate>
                                                                <asp:Label ID="lbstatus" Visible="false" runat="server" Text='<%# Eval("status") %>' />
                                                                <asp:Literal ID="lbstatus_name" runat="server" Text='<%# Eval("status_name") %>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btndeletetopic" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="btndeletetopic"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                    </Columns>

                                                </asp:GridView>
                                            </div>

                                        </div>

                                       
                                        <div class="form-group" runat="server" visible="false" id="div_save">
                                            <div class="col-md-12">
                                                <div class="pull-right">
                                                    <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i> SAVE</asp:LinkButton>
                                                    <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>


            </div>

            <div id="divadd_subtopic" runat="server" visible="false">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color: darkcyan;">
                            <h3 class="panel-title" style="color: white;"><i class="fa fa-cart-plus"></i><strong>&nbsp; เพิ่มหัวข้อย่อย</strong></h3>
                        </div>

                        <asp:FormView ID="fvadd_subtopic" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <%-- <div class="alert-message alert-message-success">
                                            <blockquote class="danger" style="font-size: small; background-color: chocolate; text-align: left">
                                                <h4 style="color: whitesmoke"><b>สร้างหัวข้อย่อย</b></h4>
                                            </blockquote>

                                        </div>--%>

                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server" Text="หัวข้อหลัก" CssClass="col-md-2 control-label"></asp:Label>
                                            <div class="col-md-9">
                                                <asp:DropDownList ID="ddltopic" ValidationGroup="SaveSubTopic_DataSet" CssClass="form-control" runat="server">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveSubTopic_DataSet" runat="server" Display="None"
                                                    ControlToValidate="ddltopic" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกหัวข้อหลัก"
                                                    ValidationExpression="กรุณาเลือกหัวข้อหลัก" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label59" runat="server" Text="หัวข้อย่อย" CssClass="col-md-2 control-label"></asp:Label>
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txtsubtopic" MaxLength="1000" TextMode="MultiLine" Rows="3" ValidationGroup="SaveSubTopic_DataSet" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveSubTopic_DataSet" runat="server" Display="None" ControlToValidate="txtsubtopic" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกหัวข้อหลัก"
                                                    ValidationExpression="กรุณากรอกหัวข้อหลัก"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                    ValidationGroup="SaveSubTopic_DataSet" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtsubtopic"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />


                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label4" runat="server" Text="สถานะการใช้งาน" CssClass="col-md-2 control-label"></asp:Label>
                                            <div class="col-md-9">
                                                <asp:DropDownList ID="ddlstatus_sub" ValidationGroup="SaveSubTopic_DataSet" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">ใช้งานปกติ</asp:ListItem>
                                                    <asp:ListItem Value="2">ปิดการใช้งาน</asp:ListItem>

                                                </asp:DropDownList>


                                            </div>
                                        </div>


                                        <div class="form-group" runat="server">
                                            <div class="col-md-2 col-md-offset-2">
                                                <asp:LinkButton ID="btnAdddatasetsubtopic" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdInsert_Dataset" OnCommand="btnCommand" ValidationGroup="SaveSubTopic_DataSet" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')" title="Add">ADD +</asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="form-group">


                                            <div class="col-md-12">
                                                <asp:GridView ID="GvSubTopicAdd" Visible="false"
                                                    runat="server"
                                                    HeaderStyle-CssClass="info"
                                                    CssClass="table table-striped table-responsive"
                                                    GridLines="None"
                                                    OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false">


                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>



                                                        <asp:TemplateField HeaderText="หัวข้อหลัก" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="40%" Visible="true" ItemStyle-Font-Size="Small">

                                                            <ItemTemplate>
                                                                <asp:Label ID="lbm1_typeidx" Visible="false" runat="server" Text='<%# Eval("m1_typeidx") %>' />
                                                                <asp:Literal ID="lbm1_type_name" runat="server" Text='<%# Eval("m1_type_name") %>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="หัวข้อย่อย" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="45%" Visible="true" ItemStyle-Font-Size="Small">

                                                            <ItemTemplate>
                                                                <asp:Literal ID="lbm2_type_name" runat="server" Text='<%# Eval("m2_type_name") %>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" Visible="true" ItemStyle-Font-Size="Small">

                                                            <ItemTemplate>
                                                                <asp:Label ID="lbstatus" Visible="false" runat="server" Text='<%# Eval("status_sub") %>' />
                                                                <asp:Literal ID="lbstatus_name" runat="server" Text='<%# Eval("status_name") %>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btndeletesubtopic" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="btndeletesubtopic"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                    </Columns>

                                                </asp:GridView>
                                            </div>

                                        </div>

                                        <div class="form-group" runat="server" visible="false" id="div_save_sub">
                                            <div class="col-md-12">
                                                <div class="pull-right">
                                                    <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i> SAVE</asp:LinkButton>
                                                    <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>

                    </div>
                </div>
            </div>

            <div id="divadd_formresult" runat="server" visible="false">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color: orchid;">
                            <h3 class="panel-title" style="color: white;"><i class="fa fa-cart-plus"></i><strong>&nbsp; เพิ่มแบบฟอร์มประเมิน</strong></h3>
                        </div>

                        <asp:FormView ID="fvadd_formresult" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <%--<div class="alert-message alert-message-success">
                                            <blockquote class="danger" style="font-size: small; background-color: mediumblue; text-align: left">
                                                <h4 style="color: whitesmoke"><b>สร้างแบบฟอร์มประเมิน</b></h4>
                                            </blockquote>

                                        </div>--%>


                                        <div class="form-group">
                                            <asp:Label ID="Label10" runat="server" Text="องค์กร" CssClass="col-md-2 control-label"></asp:Label>
                                            <div class="col-md-9">
                                                <asp:DropDownList ID="ddlorgidx" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="SaveForm_DataSet" CssClass="form-control" runat="server">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveForm_DataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlorgidx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกองค์กร"
                                                    ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidadtorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label13" runat="server" Text="ชื่อฟอร์ม" CssClass="col-md-2 control-label"></asp:Label>
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txtform_name" Enabled="false" MaxLength="500" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldVaslidator8" ValidationGroup="SaveForm_DataSet" runat="server" Display="None" ControlToValidate="txtform_name" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกชื่อฟอร์ม"
                                                    ValidationExpression="กรุณากรอกชื่อฟอร์ม"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVaslidator8" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressiosnValidator3" runat="server"
                                                    ValidationGroup="SaveForm_DataSet" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 500 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtform_name"
                                                    ValidationExpression="^[\s\S]{0,500}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressiosnValidator3" Width="160" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label11" runat="server" Text="กลุ่มพนักงาน" CssClass="col-md-2 control-label"></asp:Label>
                                            <div class="col-md-10">
                                                <%--   OnDataBound="Checkboxlist_RowDataBound" AutoPostBack="true"--%>
                                                

                                                <asp:CheckBoxList ID="chkposidx" runat="server" Enabled="false" CellPadding="5" CellSpacing="5" ValidationGroup="Create_Actor1" RepeatColumns="2" Width="50%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />




                                                <%--                                                <asp:RequiredFieldValidator ID="RequiredFieldeValidator6"
                                                    ValidationGroup="SaveForm_DataSet" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกตำแหน่ง"
                                                    Display="None" SetFocusOnError="true" ControlToValidate="chkposidx" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenxder7" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldeValidator6" Width="160"
                                                    PopupPosition="BottomLeft" />--%>



                                                <%--   <asp:DropDownList ID="ddlposidx" ValidationGroup="SaveForm_DataSet" CssClass="form-control" runat="server">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveForm_DataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlposidx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกตำแหน่ง"
                                                    ValidationExpression="กรุณาเลือกตำแหน่ง" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Labels1" runat="server" Text="หัวข้อหลัก" CssClass="col-md-2 control-label"></asp:Label>
                                            <div class="col-md-9">
                                                <asp:DropDownList ID="ddltopic_form" Enabled="false" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="SaveForm_DataSet" CssClass="form-control" runat="server">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveForm_DataSet" runat="server" Display="None"
                                                    ControlToValidate="ddltopic_form" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกหัวข้อหลัก"
                                                    ValidationExpression="กรุณาเลือกหัวข้อหลัก" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouotExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                            </div>
                                        </div>

                                        <div class="form-group" id="div_chk2type" runat="server" visible="false">
                                            <asp:Label ID="Label3" runat="server" Text="หัวข้อย่อย" CssClass="col-md-2 control-label"></asp:Label>
                                            <div class="col-md-9">
                                                <asp:CheckBoxList ID="chkm2type" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                </asp:CheckBoxList>
                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                                    ValidationGroup="SaveForm_DataSet" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกหัวข้อย่อย"
                                                    Display="None" SetFocusOnError="true" ControlToValidate="chkm2type" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160"
                                                    PopupPosition="BottomLeft" />--%>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label4" runat="server" Text="สถานะการใช้งาน" CssClass="col-md-2 control-label"></asp:Label>
                                            <div class="col-md-9">
                                                <asp:DropDownList ID="ddlstatus" ValidationGroup="SaveSubTopic_DataSet" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">ใช้งานปกติ</asp:ListItem>
                                                    <asp:ListItem Value="2" Selected="True">ปิดการใช้งาน</asp:ListItem>

                                                </asp:DropDownList>


                                            </div>
                                        </div>
                                        <div class="form-group" runat="server">
                                            <div class="col-md-2 col-md-offset-2">
                                                <asp:LinkButton ID="btnAdddataset_form" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdInsert_Dataset" OnCommand="btnCommand" ValidationGroup="SaveForm_DataSet" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')" title="Add">ADD +</asp:LinkButton>
                                            </div>
                                        </div>

                                        <div class="form-group">


                                            <div class="col-md-12">
                                                <asp:GridView ID="GvFormResultAdd" Visible="false"
                                                    runat="server"
                                                    HeaderStyle-CssClass="info"
                                                    CssClass="table table-striped table-responsive"
                                                    GridLines="None"
                                                    OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false">


                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>


                                                        <asp:TemplateField HeaderText="ข้อมูลแบบฟอร์มประเมิน" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" Visible="true" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <strong>
                                                                        <asp:Label ID="Label15" runat="server">องค์กร: </asp:Label></strong>
                                                                    <asp:Label ID="lblorgidx" Visible="false" runat="server" Text='<%# Eval("orgidx") %>' />

                                                                    <asp:Literal ID="lblorgname" runat="server" Text='<%# Eval("orgname") %>' />
                                                                    </p>
                                                                    <strong>
                                                                        <asp:Label ID="Label19" runat="server">กลุ่มพนักงาน: </asp:Label></strong>
                                                                    <asp:Label ID="lblposidx" Visible="false" runat="server" Text='<%# Eval("posidx_comma") %>' />

                                                                    <asp:Literal ID="lblposname" runat="server" Text='<%# Eval("posname") %>' />
                                                                    </p>
                                                                      <strong>
                                                                          <asp:Label ID="Label14" runat="server">ชื่อฟอร์ม: </asp:Label></strong>
                                                                    <asp:Label ID="Label16" runat="server" Text='<%# Eval("form_name") %>' />

                                                    </p>                                                                  

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="หัวข้อหลัก" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" Visible="true" ItemStyle-Font-Size="Small">

                                                            <ItemTemplate>
                                                                <asp:Label ID="lbm1_typeidx" Visible="false" runat="server" Text='<%# Eval("m1_typeidx") %>' />
                                                                <asp:Literal ID="lbm1_type_name" runat="server" Text='<%# Eval("m1_type_name") %>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="หัวข้อย่อย" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="50%" Visible="true" ItemStyle-Font-Size="Small">

                                                            <ItemTemplate>
                                                                <asp:Literal ID="lbm2_typeidx_comma" Visible="false" runat="server" Text='<%# Eval("m2_typeidx_comma") %>' />
                                                                <asp:Literal ID="lbm2_type_name" runat="server" Text='<%# Eval("m2_type_name") %>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" Visible="true" ItemStyle-Font-Size="Small">

                                                            <ItemTemplate>
                                                                <asp:Label ID="lbstatus" Visible="false" runat="server" Text='<%# Eval("status") %>' />
                                                                <asp:Literal ID="lbstatus_name" runat="server" Text='<%# Eval("status_name") %>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btndeleteformresult" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="btndeleteformresult"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                    </Columns>

                                                </asp:GridView>
                                            </div>

                                        </div>

                                        <div class="form-group" runat="server" visible="false" id="div_save_form">
                                            <div class="col-md-12">
                                                <div class="pull-right">
                                                    <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"> <i class="fa fa-save"></i> SAVE</asp:LinkButton>
                                                    <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>

