﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_m0_sap_module_lv3 : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();


    private string ODSP_Reletion = "ODSP_Reletion";
    data_employee dtEmployee = new data_employee();

    dataODSP_Relation dtODSP = new dataODSP_Relation();
    DataReservation datareservation = new DataReservation();
    DataSupportIT _dtsupport = new DataSupportIT();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetM0SapModuleLv1 = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0SapModuleLv1"];
    static string _urlGetMasterModuleSapLv1 = _serviceUrl + ConfigurationManager.AppSettings["urlGetMasterModuleSapLv1"];
    static string _urlSetDelModuleSapLv1 = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelModuleSapLv1"];
    static string _urlSetModuleSapLv1 = _serviceUrl + ConfigurationManager.AppSettings["urlSetModuleSapLv1"];
    static string _urlSetModuleSapLv1Update = _serviceUrl + ConfigurationManager.AppSettings["urlSetModuleSapLv1Update"];

    //
    static string _urlGetMasterModuleSapLv3 = _serviceUrl + ConfigurationManager.AppSettings["urlGetMasterModuleSapLv3"];
    static string _urlSetModuleSapLv3 = _serviceUrl + ConfigurationManager.AppSettings["urlSetModuleSapLv3"];
    static string _urlSetModuleSapLv3Update = _serviceUrl + ConfigurationManager.AppSettings["urlSetModuleSapLv3Update"];
    static string _urlSetDelModuleSapLv3 = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelModuleSapLv3"];


    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectSystem();
            SelectMasterList();
            div_selectGvmaster.Visible = true;
        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_Status()
    {
     

        DataSupportIT _dtsupport_insert = new DataSupportIT();
        _dtsupport_insert.BoxMSAPlist = new MSAPlist[1];
        MSAPlist dtsupport_insert = new MSAPlist();

        dtsupport_insert.SysIDX = int.Parse(ddl_System.SelectedValue);
        dtsupport_insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        dtsupport_insert.MS3_Name = txtname.Text;
        dtsupport_insert.MS3_Code = txtcode.Text;
        dtsupport_insert.MS3Status = int.Parse(ddStatusadd.SelectedValue);

        _dtsupport_insert.BoxMSAPlist[0] = dtsupport_insert;

        _dtsupport_insert = callServiceDevices(_urlSetModuleSapLv3, _dtsupport_insert);
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport_insert));
        if (_dtsupport_insert.ReturnCode == "2")
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ผิดพลาด! มีข้อมูลนี้อยู่ในระบบแล้ว');", true);
        }

    }

    protected void SelectMasterList()
    {

        DataSupportIT _dtsupport_select = new DataSupportIT();
        _dtsupport_select.BoxMSAPlist = new MSAPlist[1];
        MSAPlist dtsupport_list = new MSAPlist();

        _dtsupport_select.BoxMSAPlist[0] = dtsupport_list;

        _dtsupport_select = callServiceDevices(_urlGetMasterModuleSapLv3, _dtsupport_select);
        setGridData(GvMaster, _dtsupport_select.BoxMSAPlist);
    }

    protected DataSupportIT callServiceDevices(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);

        return _dtsupport;
    }

    protected void SelectSystem()
    {

        ddl_System.Items.Clear();
        ddl_System.AppendDataBoundItems = true;
        ddl_System.Items.Add(new ListItem("กรุณาเลือกระบบ...", "0"));

        DataSupportIT _dtsupport = new DataSupportIT();
        _dtsupport.BoxMSAPlist = new MSAPlist[1];
        MSAPlist m0_sap = new MSAPlist();

        _dtsupport.BoxMSAPlist[0] = m0_sap;
        _dtsupport = callServiceDevices(_urlGetM0SapModuleLv1, _dtsupport);

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));
        ddl_System.DataSource = _dtsupport.BoxMSAPlist;
        ddl_System.DataTextField = "SysNameTH";
        ddl_System.DataValueField = "SysIDX";
        ddl_System.DataBind();

    }

    protected void Update_Master_List()
    {

        DataSupportIT _dtsupport_update = new DataSupportIT();
        _dtsupport_update.BoxMSAPlist = new MSAPlist[1];
        MSAPlist dtsupport = new MSAPlist();

        dtsupport.MS3IDX = int.Parse(ViewState["MS3IDX_update"].ToString());
        dtsupport.MS3_Code = ViewState["MS3_Code_Update"].ToString();
        dtsupport.MS3_Name = ViewState["MS3_Name_Update"].ToString();
        dtsupport.SysIDX = int.Parse(ViewState["ddl_SysIDX_edit_Update"].ToString());
        dtsupport.MS3Status = int.Parse(ViewState["MS3Status_Update"].ToString());
        dtsupport.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport_update.BoxMSAPlist[0] = dtsupport;
        _dtsupport_update = callServiceDevices(_urlSetModuleSapLv3Update, _dtsupport_update);

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport_update));
        if (_dtsupport_update.ReturnCode == "2")
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            SelectMasterList();

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้อยู่ในระบบแล้ว ไม่สามารถอัพเดทข้อมูลได้');", true);

        }

    }

    protected void Delete_Master_List()
    {
        DataSupportIT _dtsupport_delete = new DataSupportIT();
        _dtsupport_delete.BoxMSAPlist = new MSAPlist[1];
        MSAPlist dtsupport_delete = new MSAPlist();

        dtsupport_delete.MS3IDX = int.Parse(ViewState["MS3IDX_Delete_Update"].ToString());
        dtsupport_delete.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport_delete.BoxMSAPlist[0] = dtsupport_delete;

        _dtsupport_delete = callServiceDevices(_urlSetDelModuleSapLv3, _dtsupport_delete);
    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        //var txtstatus_edit = (TextBox)e.Row.FindControl("txtstatus_edit");
                        var ddl_SysIDX_edit = (DropDownList)e.Row.FindControl("ddl_SysIDX_edit");
                        var lbl_SysIDX_edit = (Label)e.Row.FindControl("lbl_SysIDX_edit");

                        ddl_SysIDX_edit.AppendDataBoundItems = true;
                        ddl_SysIDX_edit.Items.Add(new ListItem("กรุณาเลือกระบบ...", "0"));
                        DataSupportIT _dtsupport = new DataSupportIT();
                        _dtsupport.BoxMSAPlist = new MSAPlist[1];
                        MSAPlist m0_sap = new MSAPlist();

                        _dtsupport.BoxMSAPlist[0] = m0_sap;
                        _dtsupport = callServiceDevices(_urlGetM0SapModuleLv1, _dtsupport);

                        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));
                        ddl_SysIDX_edit.DataSource = _dtsupport.BoxMSAPlist;
                        ddl_SysIDX_edit.DataTextField = "SysNameTH";
                        ddl_SysIDX_edit.DataValueField = "SysIDX";
                        ddl_SysIDX_edit.DataBind();
                        ddl_SysIDX_edit.SelectedValue = lbl_SysIDX_edit.Text;


                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();
                setOntop.Focus();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int MS3IDX_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());

                var lbl_SysIDX_edit = (Label)GvMaster.Rows[e.RowIndex].FindControl("lbl_SysIDX_edit");
                var txtMS3IDX_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtMS3IDX_edit");
                var txtMS3_Code_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtMS3_Code_edit");
                var txt_MS3_Name_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txt_MS3_Name_edit");
                //var ddl_SysIDX_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddl_SysIDX_edit");
                var ddStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");


                GvMaster.EditIndex = -1;

                ViewState["MS3IDX_update"] = MS3IDX_update;
                ViewState["MS3_Name_Update"] = txt_MS3_Name_edit.Text;
                ViewState["MS3_Code_Update"] = txtMS3_Code_edit.Text;
                ViewState["MS3Status_Update"] = ddStatusUpdate.SelectedValue;
                ViewState["ddl_SysIDX_edit_Update"] = lbl_SysIDX_edit.Text;


                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAddModule":

                btnaddmodule.Visible = false;
                Panel_AddModule.Visible = true;

                div_selectGvmaster.Visible = false;
                setOntop.Focus();

                break;

            case "btnCancel":

                btnaddmodule.Visible = true;
                Panel_AddModule.Visible = false;

                div_selectGvmaster.Visible = true;
                setOntop.Focus();
                //txtstatus.Text = String.Empty;
                //ddl_downtime.SelectedValue = "0";

                break;

            case "btnAdd":
                Insert_Status();
                setOntop.Focus();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "CmdDel":

                int MS3IDX_Delete = int.Parse(cmdArg);
                ViewState["MS3IDX_Delete_Update"] = MS3IDX_Delete;

                Delete_Master_List();
                SelectMasterList();
                setOntop.Focus();

                break;
        }



    }
    #endregion
}
