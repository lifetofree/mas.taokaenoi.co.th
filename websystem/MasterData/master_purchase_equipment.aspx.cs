﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_master_purchase_equipment : System.Web.UI.Page
{

    #region initial function/data

    function_tool _funcTool = new function_tool();
    data_purchase _data_purchase  = new data_purchase();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlSetequipment_purchasetype = _serviceUrl + ConfigurationManager.AppSettings["urlSetequipment_purchasetype"];
    static string _urlGetequipment_purchasetype = _serviceUrl + ConfigurationManager.AppSettings["urlGetequipment_purchasetype"];
    static string _urlSetstatus_equipmenttype = _serviceUrl + ConfigurationManager.AppSettings["urlSetstatus_equipmenttype"];
    static string _urlSetupdate_equipmenttype = _serviceUrl + ConfigurationManager.AppSettings["urlSetupdate_equipmenttype"];
    static string _urlGetmasterUnit = _serviceUrl + ConfigurationManager.AppSettings["urlGetmasterUnit"];



    string _localJson = "";
    int _tempInt = 0;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            action_select_equipmentype();

        }
    }


    #region gvPageIndexChanging

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvequipment_purchasetype":
                gvequipment_purchasetype.PageIndex = e.NewPageIndex;
                gvequipment_purchasetype.DataBind();
                action_select_equipmentype();
                break;
        }

    }

    #endregion

    #region gvRowUpdating

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvequipment_purchasetype":

                int txt_equipment_idx = Convert.ToInt32(gvequipment_purchasetype.DataKeys[e.RowIndex].Values[0].ToString());
                var txtupdate_namequipment = (TextBox)gvequipment_purchasetype.Rows[e.RowIndex].FindControl("txtupdate_namequipment");
                var update_status_equipmenttype = (DropDownList)gvequipment_purchasetype.Rows[e.RowIndex].FindControl("ddlstatus_equipmenttype");

                gvequipment_purchasetype.EditIndex = -1;

                ViewState["equipmenttype_idx_update"] = txt_equipment_idx;
                ViewState["name_equipmenttype_update"] = txtupdate_namequipment.Text;
                ViewState["status_equipmenttype_update"] = update_status_equipmenttype.SelectedValue;
                action_update_equipmentype();
                action_select_equipmentype();
                break;
        }

    }

#endregion

    #region gvRowDataBound

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvequipment_purchasetype":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lb_status_node = (Label)e.Row.Cells[2].FindControl("label_status_equipment");
                    Label status_online = (Label)e.Row.Cells[2].FindControl("status_online");
                    Label status_offline = (Label)e.Row.Cells[2].FindControl("status_offline");

                    ViewState["status_node"] = lb_status_node.Text;

                    if (ViewState["status_node"].ToString() == "9")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_node"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                break;
        }
    }

    #endregion

    #region gvRowCancelingEdit

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvequipment_purchasetype":
                gvequipment_purchasetype.EditIndex = -1;
                action_select_equipmentype();
                break;
        }
    }

    #endregion

    #region GvRowEditing

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvequipment_purchasetype":
                gvequipment_purchasetype.EditIndex = e.NewEditIndex;
                action_select_equipmentype();
                break;
        }
    }

#endregion

    #region setGridViewDataBind
    protected void setGridViewDataBind(GridView gridViewName, Object obj)
    {
        gridViewName.DataSource = obj;
        gridViewName.DataBind();
    }
    #endregion

    #region insert data
    protected void action_insert_equipmentype()
    {
        _data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
        m0_type_purchase_equipment_detail insert_equipmentype = new m0_type_purchase_equipment_detail();

        insert_equipmentype.name_equipment_type = txtequipmenttype.Text;
        insert_equipmentype.m0_unit_idx = int.Parse(ddltype_unitequipment.SelectedValue);
        insert_equipmentype.status_equipment = 1;

        _data_purchase.m0_type_purchase_equipment_list[0] = insert_equipmentype;
        _data_purchase = callServicePurchase(_urlSetequipment_purchasetype, _data_purchase);

        txtequipmenttype.Text = String.Empty;
        ddltype_unitequipment.SelectedValue = "0";
    }

    #endregion

    #region select data
    protected void action_select_equipmentype()
    {
        _data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
        m0_type_purchase_equipment_detail select_equipmentype = new m0_type_purchase_equipment_detail();

        select_equipmentype.status_equipment = 0;

        _data_purchase.m0_type_purchase_equipment_list[0] = select_equipmentype;
        _data_purchase = callServicePurchase(_urlGetequipment_purchasetype, _data_purchase);

        setGridViewDataBind(gvequipment_purchasetype, _data_purchase.m0_type_purchase_equipment_list);
    }
    #endregion

    #region select dropdownlist


    protected void action_select_masterunit()
    {

        ddltype_unitequipment.Items.Clear();
        _data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
        m0_type_purchase_equipment_detail select_masterunit = new m0_type_purchase_equipment_detail();

        select_masterunit.status_equipment = 2;


        _data_purchase.m0_type_purchase_equipment_list[0] = select_masterunit;
        _data_purchase = callServicePurchase(_urlGetmasterUnit, _data_purchase);


        ddltype_unitequipment.AppendDataBoundItems = true;
        ddltype_unitequipment.Items.Add(new ListItem("กรุณาเลือกหน่วยอุปกรณ์", "0"));
        ddltype_unitequipment.DataSource = _data_purchase.m0_type_purchase_equipment_list;
        ddltype_unitequipment.DataTextField = "nameth_unit";
        ddltype_unitequipment.DataValueField = "m0_unit_idx";
        ddltype_unitequipment.DataBind();


    }


    #endregion

    #region delete data

    protected void action_delete_equipmentype()
    {
        _data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
        m0_type_purchase_equipment_detail delete_equipmentype = new m0_type_purchase_equipment_detail();

        delete_equipmentype.m0_equipment_type_idx = int.Parse(ViewState["m0_equipment_type_idx"].ToString()); 

        _data_purchase.m0_type_purchase_equipment_list[0] = delete_equipmentype;
        _data_purchase = callServicePurchase(_urlSetstatus_equipmenttype, _data_purchase);

    }

    #endregion

    #region update data
    protected void action_update_equipmentype()
    {
        _data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
        m0_type_purchase_equipment_detail update_equipmentype = new m0_type_purchase_equipment_detail();

        update_equipmentype.m0_equipment_type_idx = int.Parse(ViewState["equipmenttype_idx_update"].ToString());
        update_equipmentype.name_equipment_type = ViewState["name_equipmenttype_update"].ToString();
        update_equipmentype.status_equipment = int.Parse(ViewState["status_equipmenttype_update"].ToString());

        _data_purchase.m0_type_purchase_equipment_list[0] = update_equipmentype;
        _data_purchase = callServicePurchase(_urlSetupdate_equipmenttype, _data_purchase);

    }
    #endregion

    #region call method
    protected data_purchase callServicePurchase(string _cmdUrl, data_purchase _data_purchase)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_purchase);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_purchase = (data_purchase)_funcTool.convertJsonToObject(typeof(data_purchase), _localJson);

        return _data_purchase;
    }
    #endregion

    #region btn_Command

    protected void btn_command(object sender, CommandEventArgs e)
    {
        string cmd_name = e.CommandName.ToString();
        string cmd_arg = e.CommandArgument.ToString();

        switch(cmd_name)
        {
            case "editequipment":


                break;

            case "showboxinsert":
                content_insert_equipmentype.Visible = true;
                btnshowboxinsert.Visible = false;
                btnhiddenboxinsert.Visible = true;
                action_select_masterunit();
                break;

            case "hiddenboxinsert":
                content_insert_equipmentype.Visible = false;
                btnshowboxinsert.Visible = true;
                btnhiddenboxinsert.Visible = false;
                txtequipmenttype.Text = String.Empty;
                break;

            case "save_equipment":
                action_insert_equipmentype();
                action_select_equipmentype();
                break;

            case "deletequipment":
                ViewState["m0_equipment_type_idx"] = int.Parse(cmd_arg);
                action_delete_equipmentype();
                action_select_equipmentype();
                break;
        }
    }

#endregion


}