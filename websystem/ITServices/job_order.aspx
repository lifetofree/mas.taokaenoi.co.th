﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="job_order.aspx.cs" Inherits="websystem_ITServices_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

    <style type="text/css">
        .DisplayDesc {
            width: 200px;
            word-break: break-all;
        }

        .DisplayDesc1 {
            width: 700px;
            word-break: break-all;
        }


        .DisplayDiv {
            width: 200px;
            OVERFLOW: hidden;
            TEXT-OVERFLOW: ellipsis;
        }

        .cutdiv {
            width: 500px;
            word-break: break-all;
        }

        .cutdiv1 {
            width: 700px;
            word-break: break-all;
        }

        .cutsh {
            width: 500px;
            OVERFLOW: hidden;
            TEXT-OVERFLOW: ellipsis;
        }
    </style>
    <script type="text/javascript">
        function openModal() {
            $('#opmodal').modal('show');
            $('#sentmodal').modal('show');
            $('#sentmodal_1').modal('show');
        }
    </script>
    <script type="text/javascript">
        function dotim() {
            tinyMCE.triggerSave();
        }

    </script>
    <script type="text/javascript">


        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            // inline: true,
            force_br_newlines: true,
            force_p_newlines: false,
            forced_root_block: "",
            menubar: false,
            resize: false,
            statusbar: false,
            entity_encoding: 'raw',
            entities: "160,nbsp,38,amp,34,quot,162,cent,8364,euro,163,pound,165,yen,169,copy,174,reg,8482,trade,8240,permil,60,lt,62,gt,8804,le,8805,ge,176,deg,8722,minus",
            //entities: '160,nbsp,60,lt,62,gt',
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo ",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });

            }

        });

        //var plainText = tinymce.activeEditor.getContent().replace(/<[^>] *>/ g, "");
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        //var my_editor = tinymce.get('require');
        //var content = my_editor.getContent();
        //if ($(my_editor.getBody()).text() == '') alert('กรุณาป้อนข้อมูลให้ครบ!!');

        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                //inline: true,
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: "",
                menubar: false,
                resize: false,
                statusbar: false,
                entity_encoding: 'raw',
                entities: "160,nbsp,38,amp,34,quot,162,cent,8364,euro,163,pound,165,yen,169,copy,174,reg,8482,trade,8240,permil,60,lt,62,gt,8804,le,8805,ge,176,deg,8722,minus",
                //entities: '160,nbsp,60,lt,62,gt',
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }


            });

            $(".multi").MultiFile();


        })

    </script>

    <style type="text/css">
        .wrapword_p1 {
            word-wrap: break-word;
        }
    </style>
    <script>

        $(function () {

            $('.from-date-datepickerexpire_p').datetimepicker({
                format: 'DD/MM/YYYY',
                // minDate: moment().add(-1, 'days')
                minDate: moment()
                //  ignoreReadonly: true
            });

            $('.to-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment()
                //  minDate: moment().add(-1,'days')

            });
            $('.show-from-onclick1').click(function () {
                $('.datetimepicker-from1').data("DateTimePicker").show();
            });


            $('.show-to-onclick1').click(function () {
                $('.datetimepicker-to1').data("DateTimePicker").show();
            });


        });


        var dat = Sys.WebForms.PageRequestManager.getInstance();
        dat.add_endRequest(function () {
            $(function () {
                $('.from-date-datepickerexpire_p').datetimepicker({
                    format: 'DD/MM/YYYY',
                    minDate: new Date()
                    //minDate: moment().add(-1,'days')
                    //ignoreReadonly: true
                });
                $('.to-date-datepickerexpire').datetimepicker({
                    format: 'DD/MM/YYYY',
                    minDate: new Date()
                    //  minDate: moment().add(-1,'days')
                });
                $('.show-from-onclick1').click(function () {
                    $('.datetimepicker-from1').data("DateTimePicker").show();
                });
                $('.show-to-onclick1').click(function () {
                    $('.datetimepicker-to1').data("DateTimePicker").show();
                });
            });
        });
    </script>
    <script type="text/javascript">
        $('.datetimepicker-from').datetimepicker({
            format: 'DD/MM/YYYY',
            //   minDate: moment().add(-1,'days'),
            ignoreReadonly: true

        });
        $('.clear-from-onclick').click(function () {
            $('.datetimepicker-from').val('');
            $('#<%= txtdateId.ClientID %>').val('');
        });
        $('.clear-to-onclick').click(function () {
            $('.datetimepicker-to').val('');
            $('#<%= txtdateIdf.ClientID %>').val('');
        });
        $('.clear-from-onclick').click(function () {
            $('.datetimepicker-from').val('');
            $('#<%= Hidd_stastdate.ClientID %>').val('');
        });
        $('.clear-to-onclick').click(function () {
            $('.datetimepicker-to').val('');
            $('#<%= Hidd_enddate.ClientID %>').val('');
        });


        $('.datetimepicker-from').on('dp.change', function (e) {
            if ($(this).val() != "") {
                var dateChangeAnnounceVal = $(this).val();
                var splitDateChangeAnnounce = dateChangeAnnounceVal.split("/");
                var interDateChangeAnnounce = splitDateChangeAnnounce[2] + '/' + splitDateChangeAnnounce[1] + '/' + splitDateChangeAnnounce[0];
                // $('.datetimepicker-to').data("DateTimePicker").minDate(e.date);
                $('#<%= txtstartdate.ClientID %>').val($('.datetimepicker-from').val());
                $('#<%= txtdateId.ClientID %>').val($('.datetimepicker-from').val());
                $('#<%= Hidd_stastdate.ClientID %>').val($('.datetimepicker-from').val());

            }
        });

        $('.show-from-onclick').click(function () {
            $('.datetimepicker-from').data("DateTimePicker").show();
        });


        $('.datetimepicker-to').datetimepicker({
            format: 'DD/MM/YYYY',
            //  minDate: moment().add(-1,'days'),
            ignoreReadonly: true
        });

        $('.show-to-onclick').click(function () {
            $('.datetimepicker-to').data("DateTimePicker").show();
        });

        $('.datetimepicker-to').on('dp.change', function (e) {
            if ($(this).val() != "") {
                var dateChangeAnnounceVal = $(this).val();
                var splitDateChangeAnnounce = dateChangeAnnounceVal.split("/");
                var interDateChangeAnnounce = splitDateChangeAnnounce[2] + '/' + splitDateChangeAnnounce[1] + '/' + splitDateChangeAnnounce[0];

                $('#<%= txtfinaldate.ClientID %>').val($('.datetimepicker-to').val());
                $('#<%= txtdateIdf.ClientID %>').val($('.datetimepicker-to').val());
                $('#<%= Hidd_enddate.ClientID %>').val($('.datetimepicker-to').val());

            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.clear-from-onclick').click(function () {
                $('.datetimepicker-from').val('');
                $('#<%= txtdateId.ClientID %>').val('');
            });
            $('.clear-from-onclick').click(function () {
                $('.datetimepicker-from').val('');
                $('#<%= Hidd_stastdate.ClientID %>').val('');
            });
            $('.clear-to-onclick').click(function () {
                $('.datetimepicker-to').val('');
                $('#<%= txtdateIdf.ClientID %>').val('');
            });
            $('.clear-to-onclick').click(function () {
                $('.datetimepicker-to').val('');
                $('#<%= Hidd_enddate.ClientID %>').val('');
            });

            $('.datetimepicker-from').datetimepicker({
                format: 'DD/MM/YYYY',
                // minDate: moment().add(-1,'days'),
                ignoreReadonly: true

            });
            $('.datetimepicker-from').on('dp.change', function (e) {

                if ($(this).val() != "") {
                    var dateChangeAnnounceVal = $(this).val();
                    var splitDateChangeAnnounce = dateChangeAnnounceVal.split("/");
                    var interDateChangeAnnounce = splitDateChangeAnnounce[2] + '/' + splitDateChangeAnnounce[1] + '/' + splitDateChangeAnnounce[0];
                    // $('.datetimepicker-to').data("DateTimePicker").minDate(e.date);
                    $('#<%= txtstartdate.ClientID %>').val($('.datetimepicker-from').val());
                    $('#<%= txtdateId.ClientID %>').val($('.datetimepicker-from').val());
                    $('#<%= Hidd_stastdate.ClientID %>').val($('.datetimepicker-from').val());

                }



            });

            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                // minDate: moment().add(-1,'days'),
                ignoreReadonly: true
            });

            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });

            $('.datetimepicker-to').on('dp.change', function (e) {
                if ($(this).val() != "") {
                    var dateChangeAnnounceVal = $(this).val();
                    var splitDateChangeAnnounce = dateChangeAnnounceVal.split("/");
                    var interDateChangeAnnounce = splitDateChangeAnnounce[2] + '/' + splitDateChangeAnnounce[1] + '/' + splitDateChangeAnnounce[0];
                    $('#<%= txtfinaldate.ClientID %>').val($('.datetimepicker-to').val());
                    $('#<%= txtdateIdf.ClientID %>').val($('.datetimepicker-to').val());
                    $('#<%= Hidd_enddate.ClientID %>').val($('.datetimepicker-to').val());

                }
            });

        });
    </script>


    <asp:HyperLink ID="SETFOCUS_ONTOP" runat="server"></asp:HyperLink>
    <asp:Literal ID="time_present_use" runat="server" Visible="false"></asp:Literal>
    <asp:Literal ID="tostid" Visible="false" runat="server"></asp:Literal>
    <asp:Literal ID="delete_ondelete" Visible="true" runat="server"></asp:Literal>
    <asp:Literal ID="tangura" Visible="true" runat="server"></asp:Literal>
    <asp:Literal ID="test_l" Visible="true" runat="server"></asp:Literal>

    <asp:TextBox ID="add_reqi" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>
    <asp:TextBox ID="main_id_to_sql" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>
    <%-- <asp:TextBox ID="addrequir1" Visible="true" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>--%>
    <div class="col-md-12" role="main">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li_about_rdep" runat="server">
                            <asp:LinkButton ID="about_rdep" runat="server" CommandName="view1" OnCommand="navCommand" CommandArgument="view1">รายการภายในฝ่าย</asp:LinkButton>
                        </li>
                        <li id="li_create_story_rdep" runat="server">
                            <asp:LinkButton ID="create_story_rdep" runat="server" CommandName="create_story" OnCommand="navCommand" CommandArgument="create_story">สร้างรายการ</asp:LinkButton>
                        </li>
                        <li id="li_all_page_dredp" runat="server">
                            <asp:LinkButton ID="all_page_dredp" runat="server" CommandName="viewall_pages" OnCommand="navCommand" CommandArgument="viewall_pages">รายการทั้งหมด</asp:LinkButton>
                        </li>
                         <li id="li4" runat="server" class="dropdown">
                        <asp:LinkButton ID="lbtab5" runat="server" class="dropdown-toggle" data-toggle="dropdown"
                            role="button" aria-haspopup="true" aria-expanded="false">คู่มือการใช้งาน<span class="caret"></span></asp:LinkButton>

                        <ul class="dropdown-menu">
                            <li>
                                <asp:HyperLink runat="server" ID="HyperLink3" Visible="true" data-toggle="tooltip"
                                    data-original-title="คู่มือการใช้งาน" NavigateUrl='https://drive.google.com/open?id=1J6Cj1fkVl56kw7sNVx4C5JNW3i-Vi36tu9WrwTgvPlc' Target="_blank"><i class="fa fa-book"></i> คู่มือการใช้งาน</asp:HyperLink>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <asp:HyperLink runat="server" ID="HyperLink4" Visible="true" data-toggle="tooltip"
                                    data-original-title="Flow Chart" NavigateUrl='https://drive.google.com/open?id=0B4dWU9CW8MrEdDk3amJ3YkxXUzg' Target="_blank"><i class="fa fa-book"></i> Flow Chart</asp:HyperLink>
                            </li>
                        </ul>
                    </li>
                     
                  
                        <%--<li id="li_head_rdep" runat="server">
                            <asp:LinkButton ID="head_rdep" runat="server" CommandName="viewhead" OnCommand="navCommand" CommandArgument="viewhead">หัวหน้าฝ่ายของ USER</asp:LinkButton>
                        </li>--%>
                        <%-- <li id="li_mis_rdep" runat="server">
                            <asp:LinkButton ID="mis_rdep" runat="server" CommandName="viewmis" OnCommand="navCommand" CommandArgument="viewmis">พนักงานฝ่าย MIS</asp:LinkButton>
                        </li>--%>
                        <%--<li id="li_head_Mis" runat="server">
                            <asp:LinkButton ID="mishead_rdep" runat="server" CommandName="view2" OnCommand="navCommand" CommandArgument="view2">หัวหน้าฝ่าย MIS</asp:LinkButton>
                        </li>--%>
                    </ul>
                </div>
            </div>
        </nav>

        <asp:MultiView ID="multimaster" runat="server" ActiveViewIndex="0">
            <asp:View ID="viewall_pages" runat="server">
                <asp:LinkButton ID="btnshow_searchall" class="btn btn-primary m-b-10 f-s-14" runat="server"
                    Visible="false" CommandName="show_searchall" OnCommand="btnuser"><i class="glyphicon glyphicon-search"></i> แสดงเครื่องมือค้นหา</asp:LinkButton>
                <asp:LinkButton ID="btnclose_searchall" class="btn btn-danger m-b-10 f-s-14" runat="server"
                    Visible="false" CommandName="close_searchall" OnCommand="btnuser" Text="ซ่อนเครื่องมือค้นหา" />

                <div id="Divseach_all" runat="server" visible="false">
                    <div class="panel panel-default">
                        <div class="panel-body">




                            <div class="form-group col-md-12">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label class="f-s-13">วันที่สร้าง</label>
                                    <%--<br />--%>
                                    <asp:DropDownList ID="DDseach_dateall" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                        <asp:ListItem Value="0">เลือกเงื่อนไข</asp:ListItem>
                                        <asp:ListItem Value="1">มากกว่า ></asp:ListItem>
                                        <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                        <asp:ListItem Value="3">ระหว่าง <></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-3">
                                    <label class="f-s-13">ตั้งแต่วันที่</label>
                                    <div class='input-group date'>
                                        <asp:HiddenField ID="Hidd_stastdate" runat="server" />
                                        <asp:TextBox ID="txtseach_stastdate" Enabled="false" runat="server" CssClass="form-control datetimepicker-from cursor-pointer" placeholder="ตั้งแต่วันที่" ReadOnly="true" />
                                        <span class="input-group-addon show-from-onclick ">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                        <span class="input-group-addon clear-from-onclick">
                                            <span class="fa fa-times"></span>
                                        </span>
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    <label class="f-s-13">ถึงวันที่</label>
                                    <div class='input-group date'>
                                        <asp:HiddenField ID="Hidd_enddate" runat="server" />
                                        <asp:TextBox ID="txtseach_enddate" runat="server" CssClass="form-control datetimepicker-to cursor-pointer" placeholder="ถึงวันที่"
                                            ReadOnly="true" Enabled="false" />
                                        <span class="input-group-addon show-to-onclick ">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                        <span class="input-group-addon clear-to-onclick">
                                            <span class="fa fa-times"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">
                                    <label class="f-s-13">บริษัท</label>
                                    <%--<br />--%>
                                    <asp:DropDownList ID="ddOrg_seachall" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" />
                                </div>
                                <div class="col-md-3">
                                    <label class="f-s-13">ฝ่าย</label>
                                    <%--<br />--%>
                                    <asp:DropDownList ID="ddDept_seachall" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" />
                                </div>
                                <div class="col-md-3">
                                    <label class="f-s-13">แผนก</label>
                                    <%--<br />--%>
                                    <asp:DropDownList ID="ddSec_seachall" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4">
                                        <label class="f-s-13">รหัสรายการ</label>
                                        <br />
                                        <asp:TextBox ID="txtseach_noinvoice" runat="server" MaxLength="8" CssClass="form-control" placeholder="ป้อนรหัสรายการ เช่น JB600001" />
                                        <br />
                                    </div>
                                    <div class="col-md-5">
                                        <label class="f-s-13">ชื่อ-นามสกุล ผู้สร้าง</label>
                                        <asp:TextBox ID="Txtseach_names" runat="server" CssClass="form-control" placeholder="ป้อนชื่อ-นามสกุล ของผู้สร้างรายการ" />
                                    </div>
                                </div>
                            </div>


                            <!--btn search-->
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-3">
                                        <asp:LinkButton ID="libtn_seachallpage" data-toggle="tooltip" title="ค้นหา" runat="server" CssClass="btn btn-success" OnCommand="btnuser" CommandName="seachallpage"><i class="glyphicon glyphicon-search"></i> </asp:LinkButton>
                                        <asp:LinkButton ID="libtn_resetallpage" data-toggle="tooltip" title="รีเซ็ต" runat="server" CssClass="btn btn-warning f-s-13" OnCommand="btnuser" CommandName="resetallpage"><i class="glyphicon glyphicon-repeat"></i> </asp:LinkButton>
                                    </div>
                                    <div class="col-md-8"></div>
                                </div>
                            </div>

                            <!--///////////-->
                        </div>
                    </div>
                </div>

                <asp:Panel ID="Pl_allsta" runat="server">
                    <%--allwork--%>
                    <asp:GridView ID="gv_allwork" runat="server"
                        AutoGenerateColumns="false"
                        DataKeyNames="u0_jo_idx"
                        CssClass="table table-striped table-bordered table-hover table-responsive wordwrap_p1 col-md-12"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="10"
                        OnRowDataBound="Master_RowDataBound"
                        OnRowEditing="Master_RowEditing"
                        OnRowUpdating="Master_RowUpdating"
                        OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        AutoPostBack="False">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center; color: red"><b>ไม่มีข้อมูล !! ที่ที่ถูกสร้างขึ้นมา</b> </div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="รหัสรายการ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="10%">

                                <ItemTemplate>

                                    <asp:Label ID="invoice_title" runat="server" Text='<%# Eval("no_invoice") %>' />
                                    <asp:Label ID="u0_main" runat="server" Visible="false" Text='<%# Eval("id_statework") %>' />
                                    <asp:Label ID="node_to_m0_db" runat="server" Visible="false" Text='<%# Eval("to_m0_node") %>' />
                                    <asp:Label ID="actor_to_m0_db" runat="server" Visible="false" Text='<%# Eval("to_m0_actor") %>' />


                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ผู้สร้างรายการ" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                <ItemTemplate>
                                    <small>
                                        <strong>
                                            <asp:Label ID="org_emp" runat="server">บริษัท: </asp:Label></strong>
                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("Organize") %>' />
                                        </p>
                                           
                                        <strong>
                                            <asp:Label ID="dep_emp" runat="server">ฝ่าย: </asp:Label></strong>
                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("Deptname") %>' />
                                        </p>
                                           
                                        <strong>
                                            <asp:Label ID="sec_emp" runat="server">แผนก: </asp:Label></strong>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Secname") %>' />
                                        </p>
                                                
                                        <strong>
                                            <asp:Label ID="name_emp" runat="server">ชื่อผู้สร้าง: </asp:Label></strong>
                                        <asp:Label ID="lblempname" runat="server" Text='<%# Eval("AdminMain") %>' />
                                        <asp:Label ID="lbl_rsec_checkpermission_all" Visible="false" runat="server" Text='<%# Eval("rsec_creator") %>' />
                                        <asp:Label ID="gv_all_sec" Visible="false" runat="server" Text='<%# Eval("sec_mis") %>' />
                                        </p>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หัวข้อความต้องการ" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="20%">
                                <ItemStyle Wrap="True" />
                                <ItemTemplate>
                                    <div style="word-break: break-all;">
                                        <asp:Label ID="title_allpage" runat="server" Text='<%# Eval("title_jo") %>' />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่" ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                <ItemTemplate>
                                    <small>
                                        <strong>
                                            <asp:Label ID="time_crea" runat="server">วันที่สร้าง: </asp:Label></strong>
                                        <asp:Label ID="rdep_num_look" Visible="false" runat="server" Text='<%# Eval("rdep_num") %>'></asp:Label>
                                        <asp:Label ID="day_create_allpage" runat="server" Text='<%# Eval("day_created") %>' />
                                        <asp:Label ID="timelook_all" runat="server" Text='<%# Eval("u0_day_sent_time") %>' />
                                        </p>
                                         <strong>
                                             <asp:Label ID="day_start" runat="server">วันที่เริ่มงาน: </asp:Label></strong>
                                        <asp:Label ID="day_start_job_all" Visible="true" runat="server" Text='<%# Eval("day_start") %>' />
                                        </p>
                                        <strong>
                                            <asp:Label ID="day_end" runat="server">วันที่สิ้นสุดงาน: </asp:Label></strong>
                                        <asp:Label ID="day_end_job_all" Visible="true" runat="server" Text='<%# Eval("day_finish") %>' />
                                        </p>


                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="_node_all" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>' />
                                    <asp:Label ID="actor_all" runat="server" Visible="false" Text='<%# Eval("m0_actor_idx") %>' />
                                    <asp:Label ID="_idstate_all" runat="server" Visible="false" Text='<%# Eval("id_statework") %>' />
                                    <asp:Label ID="showstatus" runat="server" Text='<%# Eval("statenode_name") %>' />
                                    <asp:Label ID="statenode_allpage" runat="server" Text='<%# Eval("StatusDoc") %>' />


                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                                HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:UpdatePanel ID="up7" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="lb_speed_lookmis" CssClass="btn btn-sm btn-primary" runat="server" Visible="false" CommandName="lookmis" OnCommand="btncommand" data-toggle="tooltip" title="ตารางการดำเนินงาน" CommandArgument='<%# Eval("u0_jo_idx") + ";" + "0"+";" + Eval("no_invoice") +";" +"0"%>'><i class="glyphicon glyphicon-calendar"></i></asp:LinkButton>
                                                    <asp:LinkButton CssClass="btn btn-sm btn-info" ID="lb_speed_lestho" runat="server" Visible="false" CommandName="lookhead" OnCommand="btncommand" data-toggle="tooltip" title="สำหรับหัวหน้าแผนก" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("no_invoice") +";" + "2" %>'><i class="glyphicon glyphicon-envelope"></i></asp:LinkButton>

                                                    <%--<asp:LinkButton ID="lb_speed_addafter" CssClass="btn btn-sm btn-info" runat="server" Visible="false" CommandName="addaf" OnCommand="btnuser"
                                                        data-toggle="tooltip" Style="background-color: darkslateblue" title="ส่งรายการความต้องการ" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework") %>'
                                                        OnClientClick="return confirm('คุณต้องการส่งข้อมูล ใช่หรือไม่')"><i class="glyphicon glyphicon-export"></i>
                                                    </asp:LinkButton>--%>

                                                    <asp:LinkButton ID="lb_speed_sentaccept" class="btn btn-sm btn-info" runat="server" Visible="false" CommandName="showsent" OnCommand="btnuser"
                                                        data-toggle="tooltip" Style="background-color: forestgreen" title="ระยะเวลาในการดำเนินงาน" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework") %>'>
                                                    <i class="glyphicon glyphicon-time"></i>
                                                    </asp:LinkButton>
                                                    <%--<asp:LinkButton ID="lb_speed_edit1" Visible="true" class="btn btn-info btn-sm" Style="background-color: lightseagreen" runat="server" data-toggle="tooltip" title="รายละเอียดของรายการ" CommandName="edit1" OnCommand="btnuser" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework")+ ";" + Eval("jo_idx_ref")+ ";" + Eval("no_invoice")  %>'><i class="glyphicon glyphicon-user"></i></asp:LinkButton>--%>
                                                    <asp:LinkButton ID="lb_speed_detail" class="btn btn-sm btn-info" runat="server" Visible="false" CommandName="detail"
                                                        data-toggle="tooltip" title="สำหรับหัวหน้าฝ่าย MIS" OnCommand="btnuser" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework")  + ";" + Eval("day_start") + ";" + Eval("day_finish") + ";" + Eval("jo_idx_ref")+ ";" + Eval("no_invoice")  %>'>
                                                    <i class="glyphicon glyphicon-new-window"></i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="lb_speed_edit1" class="btn btn-info btn-sm" Style="background-color: lightseagreen" runat="server" data-toggle="tooltip" title="รายละเอียดของรายการ" CommandName="edit1" OnCommand="btnuser" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework")+ ";" + Eval("jo_idx_ref")+ ";" + Eval("no_invoice")+";"+Eval("emp_id_creator")  %>'><i class="glyphicon glyphicon-list-alt"></i></asp:LinkButton>
                                                </ContentTemplate>

                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="lb_speed_edit1" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                            <%--<asp:LinkButton ID="delete" class="btn-info btn-sm" Style="background-color: #e12121" runat="server" Visible="true" data-toggle="tooltip" title="ลบรายการ"
                                            CommandName="del" OnCommand="btnuser" CommandArgument='<%# Eval("u0_jo_idx") %>'
                                            OnClientClick="return confirm('คุณต้องการ ลบข้อมูลใช่หรือไม่')">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                        </asp:LinkButton>--%>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
                <div class="col-lg-12" runat="server" id="div1">
                    <div id="sentmodal_1" class="modal fade" role="dialog">
                        <div class="modal-dialog" style="width: 45%">
                            <!--Modal content-->
                            <div class="modal-content" style="background: whitesmoke">
                                <div class="modal-header" style="color: #31708f; background: #d9edf7">

                                    <h4 class="modal-title">พิจารณาผลรับทราบระยะเวลาในการดำเนินงาน</h4>
                                </div>
                                <div class="modal-body">

                                    <div class="panel-body">
                                        <asp:FormView ID="FormView1" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="DetailUsersent_DataBound">
                                            <ItemTemplate>
                                                <div class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:Label ID="lsdate" CssClass="col-sm-3 control-label" Font-Bold="true" runat="server" Text="วันที่เริ่มงาน : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="sdate" runat="server" Font-Bold="true" CssClass="form-control" Maxlengh="100%" Enabled="false" Style="text-align: center" Text='<%# Eval("day_start") %>'></asp:TextBox>
                                                                <asp:Label ID="reqtest" CssClass="control-labelnotop col-xs-12" runat="server" Visible="false" Text='<%# Eval("require_jo") %>' />
                                                                <asp:Label ID="sec_mis_date" CssClass="control-labelnotop col-xs-12" runat="server" Visible="false" Text='<%# Eval("sec_mis") %>' />

                                                            </div>
                                                            <asp:Label ID="lfdate" CssClass="col-sm-3 control-label" Font-Bold="true" runat="server" Text="วันที่สิ้นสุดงาน : " />

                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="fdate" runat="server" Font-Bold="true" CssClass="form-control" Maxlengh="100%" Enabled="false" Style="text-align: center" Text='<%# Eval("day_finish") %>'></asp:TextBox>
                                                            </div>
                                                            <div class="col-sm-1"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <%--<asp:Label ID="day_a" CssClass="col-sm-4 control-label" Font-Bold="true" runat="server" Text="ระยะเวลาในการทำงาน : " />--%>
                                                        <div class="col-sm-4 text-right">
                                                            <label>ระยะเวลาในการทำงาน:</label>
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_date_detail" Font-Bold="true" runat="server" CssClass="form-control" Maxlengh="100%" Enabled="false" Style="text-align: center" Text='<%# Eval("day_amount") %>'></asp:TextBox>
                                                        </div>
                                                        <asp:Label ID="day" CssClass="col-sm-1 control-label" Font-Bold="true" runat="server" Text="วัน" />
                                                        <div class="col-sm-3"></div>
                                                    </div>
                                                </div>

                                            </ItemTemplate>
                                        </asp:FormView>
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label>พิจารณาผล:</label>
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm6">
                                                <asp:LinkButton ID="Linacceptsent" class="btn btn-success " runat="server" Visible="true" CommandName="acceptsent" CommandArgument="1" OnCommand="btnuser"
                                                    Text="ยอมรับ" OnClientClick="return confirm('คุณต้องการ เซ็นยอมรับใช่หรือไม่')" />
                                                <asp:LinkButton ID="Linclosemishead2" class="btn btn-warning " runat="server" Visible="true" CommandName="closemishead2" CommandArgument="1" OnCommand="btnuser"
                                                    Text="ไม่ยอมรับ" OnClientClick="return confirm('คุณต้องการ ไม่ยอมรับใช่หรือไม่')" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3 text-right">
                                                <label>comment:</label>
                                            </div>
                                            <%-- <div class="col-sm-2"></div>--%>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txt_comment_usertimeall" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="2" placeholder="เเสดงความคิดเห็น..." MaxLength="500"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />
                                        <%--  <div class="form-group" style="text-align: center">
                                           
                                        </div>--%>
                                        <div class="col-sm-offset-10">
                                            <asp:LinkButton ID="LinkButton5" class="btn btn-danger" data-toggle="tooltip" title="ปิด" runat="server" data-dismiss="modal"
                                                CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:View>

            <asp:View ID="viewhead" runat="server">
                <%--viewhead--%>
                <%--<div class="col-md-12">
                        <asp:LinkButton ID="create_new" CommandName="lb_build_plus_1" OnCommand="btnonclick" runat="server" CssClass="btn btn-success" style="font-size:100%"><i class="glyphicon glyphicon-plus"> สร้างหัวข้อ</i></asp:LinkButton>
                        <asp:LinkButton ID="my_list" CommandName="lb_all_list" OnCommand="btnonclick" runat="server" CssClass="btn btn-primary pull-right" style="font-size:100%"><i class="glyphicon glyphicon-list-alt"> รายการอื่นๆ</i></asp:LinkButton>
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-info f-s-14" style="margin-top:1%"><asp:label ID="label1" runat="server" style="margin-left:2%;font-size:120%"> Head</asp:label> </div>
                    </div>--%>

                <asp:Panel ID="plhead" runat="server" Visible="false">
                    <asp:GridView ID="Gvmaster" runat="server"
                        AutoGenerateColumns="false"
                        DataKeyNames="u0_jo_idx"
                        CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="10"
                        OnRowDataBound="Master_RowDataBound"
                        OnRowEditing="Master_RowEditing"
                        OnRowUpdating="Master_RowUpdating"
                        OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        AutoPostBack="False">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center; color: red"><b>ไม่มีข้อมูล !! เข้ามาส่วน HEAD</b> </div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="รหัสรายการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <asp:Label ID="num" runat="server" Visible="false" Text='<%# Eval("u0_jo_idx") %>' />
                                        <asp:Label ID="invoice_id_head" runat="server" Text='<%# Eval("no_invoice") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หัวข้อความต้องการ" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="title" runat="server" Text='<%# Eval("title_jo") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่สร้างรายการ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="time_test" runat="server" Text='<%# Eval("day_created") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <asp:Label ID="statenode" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:LinkButton CssClass="btn-sm btn-info" ID="lestho" runat="server" CommandName="lookhead" OnCommand="btncommand" data-toggle="tooltip" title="รายละเอียดรายการของพนักงานในฝ่าย" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("no_invoice") %>'><i class="glyphicon glyphicon-envelope"></i></asp:LinkButton>
                                        <%--<asp:LinkButton style="font-size:15px; color:red" ID="Delete" runat="server" CommandName="Deleat" OnCommand="btnonclick" data-toggle="tooltip" title="ลบทิ้งไป" CommandArgument='<%# Eval("u0_jo_idx") %>' OnClientClick="return confirm('อย่าลบกรุไปๆๆๆ')"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>--%>
                                        <%--<asp:LinkButton id="lbview1" runat="server" OnCommand="btnonclick" CommandName="lbview1"><i class="glyphicon glyphicon-envelope"></i></asp:LinkButton>--%>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
                <asp:Panel ID="divformhead" runat="server" Visible="false">
                    <div class="form-group">
                        <%--   <asp:LinkButton ID="LinkButton6" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server" CommandName="btnbackmis" OnCommand="btnallback"><i class="fa fa-reply">&nbsp;กลับ</i></asp:LinkButton>--%>

                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server" CommandName="btnbackhead" OnCommand="btnallback"><i class="fa fa-reply">&nbsp;กลับ</i></asp:LinkButton>
                        <asp:LinkButton ID="Linkback_head" Visible="false" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server" CommandName="btnbackalllist" OnCommand="btnallback"><i class="fa fa-reply">&nbsp;กลับ</i></asp:LinkButton>
                        <asp:LinkButton ID="lb_rdef_same_head" Visible="false" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server" CommandName="back2" OnCommand="btnallback"><i class=" fa fa-reply">&nbsp;กลับ</i></asp:LinkButton>
                    </div>
                    <asp:FormView ID="formviewhead" runat="server" OnDataBound="formviewhead_DataBound" DefaultMode="readonly">

                        <ItemTemplate>

                            <%--<asp:Label ID="num" runat="server" Visible="true" Text='<%# Eval("u0_jo_idx") %>' />--%>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div>
                                        <i class="glyphicon glyphicon-user"></i>
                                        <asp:Label ID="name" Font-Bold="true" runat="server" Text="ข้อมูลผู้ระบุความต้องการ"></asp:Label>
                                    </div>
                                </div>

                                <div class="panel-body">

                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>รหัสผู้ทำรายการ :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="id_emprequire" runat="server" Text='<%# Eval("Codeemp") %>'></asp:Label>
                                        </div>
                                        <div class="col-md-2 text_right">
                                            <label>ชื่อผู้ทำรายการ :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="name_emprequire" runat="server" Text='<%# Eval("AdminMain") %>'></asp:Label>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>บริษัท :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="company_emprequire" runat="server" Text='<%# Eval("Organize") %>'></asp:Label>
                                        </div>
                                        <div class="col-md-2 text_right">
                                            <label>ฝ่าย :</label>
                                        </div>
                                        <div class="col-md-4">

                                            <asp:Label ID="faction_emprequire" runat="server" Text='<%# Eval("Deptname") %>'></asp:Label>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>แผนก :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="department_emprequire" runat="server" Text='<%# Eval("Secname") %>'></asp:Label>
                                        </div>
                                        <div class="col-md-2 text_right">
                                            <label>ตําแหน่ง :</label>
                                        </div>
                                        <div class="col-md-4">

                                            <asp:Label ID="position_emprequire" runat="server" Text='<%# Eval("Posname") %>'></asp:Label>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>เบอร์ติดต่อ :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="tel_emprequire" runat="server" Text='<%# Eval("emp_mobile_no") %>'></asp:Label>
                                        </div>
                                        <div class="col-md-2 text_right">
                                            <label>E-mail :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="mail_emprequire" runat="server" Text='<%# Eval("emp_email") %>'></asp:Label>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>Cost Center :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="cost_emprequire" runat="server" Text='<%# Eval("costcenter_no") %>'></asp:Label>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div>
                                        <i class="fa fa-file-text"></i>
                                        <asp:Label ID="Label8" Font-Bold="true" runat="server" Text="รายละเอียดรายการ"></asp:Label>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <asp:TextBox ID="form_node" runat="server" Visible="false" Text='<%# Eval("to_m0_node") %>'></asp:TextBox>
                                    <asp:TextBox ID="form_actor" runat="server" Visible="false" Text='<%# Eval("to_m0_actor") %>'></asp:TextBox>

                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>รหัสรายการ :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="iddoc" runat="server" Text='<%# Eval("no_invoice") %>'></asp:Label>
                                            <asp:Label ID="sec_mis1" Visible="false" runat="server" Text='<%# Eval("sec_mis") %>'></asp:Label>
                                        </div>
                                        <div class="col-md-2 text_right">
                                            <label>วันที่สร้างรายการ : </label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="datecarteread" runat="server" Text='<%# Eval("day_created") %>'></asp:Label>
                                            <asp:Label ID="lb_datetime_cre" runat="server" Text='<%# Eval("u0_day_sent_time") %>'></asp:Label>
                                        </div>

                                        <%--                                        <div class="col-md-2 text_right">
                                            <label>เวลาที่สร้างรายการ : </label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="lb_datetime_cre" runat="server" Text='<%# Eval("u0_day_sent_time") %>'></asp:Label>
                                        </div>--%>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>หัวข้อความต้องการ : </label>
                                        </div>
                                        <div class="col-md-5 DisplayDesc1">
                                            <asp:Label ID="titledoc" runat="server" Text='<%# Eval("title_jo") %>'></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label style="margin-top: 1%;">รายละเอียด : </label>
                                        </div>
                                        <div class="col-md-2 cutdiv1">
                                            <asp:Label ID="oak007" runat="server" CssClass="cutsh"></asp:Label>
                                            <%--<asp:TextBox id="requiredoc" runat="server" TextMode="MultiLine"  disabled="disabled"  CssClass="col-md-8" rows="5"></asp:TextBox>
                                                <asp:TextBox id="testtest" runat="server" TextMode="MultiLine" Visible="false"  CssClass="col-md-8" rows="5" Text='<%# Eval("require_jo") %>'></asp:TextBox>--%>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text-right">
                                            <label>ไฟล์ในการร้องขอ:</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <asp:GridView ID="Gv_filehead" Visible="true" runat="server" ShowHeader="false" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="Master_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <div class="col-lg-11">
                                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                            </div>
                                                            <div class="col-lg-1">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-primary btn-sm pull-right" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>
                                                                <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text-right">
                                            <label>พิจารณาผล:</label>
                                        </div>
                                        <div class="col-md-10">
                                            <asp:Panel ID="view_dep" runat="server" Visible="false">
                                                <asp:LinkButton ID="Vokgogo" CssClass="btn btn-success" runat="server" CommandName="submithead" OnCommand="btninsert" Visible="true"
                                                    data-toggle="tooltip" title="อนุมัติ" OnClientClick="return confirm('คุณต้องการอนุมัติงานใช่หรือไม่')" CommandArgument='0'>อนุมัติ</asp:LinkButton>
                                                <asp:LinkButton ID="Vnookgo" CssClass="btn btn-danger" data-toggle="tooltip" title="ส่งกลับไปแก้ไข" runat="server" Visible="true"
                                                    CommandName="cancelbu" OnCommand="btninsert" OnClientClick="return confirm('คุณต้องการส่งงานกลับไปแก้ไขหรือไม่')" CommandArgument='0'>ไม่อนุมัติ</asp:LinkButton>
                                            </asp:Panel>
                                            <asp:Panel ID="viewall" runat="server" Visible="false">
                                                <asp:LinkButton ID="Vokgogo_allpage" CssClass="btn btn-success" runat="server" CommandName="submithead" OnCommand="btninsert" Visible="true"
                                                    data-toggle="tooltip" title="อนุมัติ" OnClientClick="return confirm('คุณต้องการอนุมัติงานใช่หรือไม่')" CommandArgument='1'>อนุมัติ</asp:LinkButton>
                                                <asp:LinkButton ID="Vnookgo_allpage" CssClass="btn btn-danger" data-toggle="tooltip" title="ส่งกลับไปแก้ไข" runat="server" Visible="true"
                                                    CommandName="cancelbu" OnCommand="btninsert" OnClientClick="return confirm('คุณต้องการส่งงานกลับไปแก้ไขหรือไม่')" CommandArgument='1'>ไม่อนุมัติ</asp:LinkButton>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text-right">
                                            <label>comment:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txt_comment" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" placeholder="เเสดงความคิดเห็น..." MaxLength="500"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                        </ItemTemplate>

                    </asp:FormView>

                </asp:Panel>
            </asp:View>

            <asp:View ID="viewmis" runat="server">
                <%--viewmis
                <%--<div class="col-md-12">
                        <asp:LinkButton ID="lb_build_plus" CommandName="lb_build_plus_1" OnCommand="btnonclick" runat="server" CssClass="btn btn-success" style="font-size:100%"><i class="glyphicon glyphicon-plus"> สร้างหัวข้อ</i></asp:LinkButton>
                        <asp:LinkButton ID="lb_all_list" CommandName="lb_all_list" OnCommand="btnonclick" runat="server" CssClass="btn btn-primary pull-right" style="font-size:100%"><i class="glyphicon glyphicon-list-alt"> รายการอื่นๆ</i></asp:LinkButton>
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-info f-s-14" style="margin-top:1%"><asp:label ID="mislookulook" runat="server" style="margin-left:2%;font-size:120%">MIS</asp:label> </div>
                    </div>--%>

                <asp:Panel ID="plmis" runat="server" Visible="false">
                    <asp:GridView ID="Gvmastermis" runat="server"
                        AutoGenerateColumns="false"
                        DataKeyNames="u0_jo_idx"
                        CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="10"
                        OnRowDataBound="Master_RowDataBound"
                        OnRowEditing="Master_RowEditing"
                        OnRowUpdating="Master_RowUpdating"
                        OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        AutoPostBack="False">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center; color: red"><b>ไม่มีข้อมูล !! เข้ามาส่วน MIS</b> </div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="รหัสรายการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <asp:Label ID="num" runat="server" Visible="false" Text='<%# Eval("u0_jo_idx") %>' />
                                        <asp:Label ID="invoice" runat="server" Text='<%# Eval("no_invoice") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หัวข้อความต้องการ" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="title" runat="server" Text='<%# Eval("title_jo") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่สร้างรายการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="statenode" runat="server" Text='<%# Eval("day_created") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="idworkdoc_en" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:LinkButton CssClass="btn-sm btn-primary" ID="Editmis" runat="server" CommandName="lookmis" OnCommand="btncommand" data-toggle="tooltip" title="ตารางการแบ่งงาน" CommandArgument='<%# Eval("u0_jo_idx") + ";" + "1"+";" + Eval("no_invoice") %>'><i class="fa fa-table"></i></asp:LinkButton>
                                        <%--<asp:LinkButton style="font-size:15px; color:red" ID="Delete" runat="server" CommandName="Deleat" OnCommand="btnonclick" data-toggle="tooltip" title="ลบทิ้งไป" CommandArgument='<%# Eval("u0_jo_idx") %>' OnClientClick="return confirm('อย่าลบกรุไปๆๆๆ')"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>--%>
                                        <%--<asp:LinkButton id="lbview1" runat="server" OnCommand="btnonclick" CommandName="lbview1"><i class="glyphicon glyphicon-envelope"></i></asp:LinkButton>--%>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </asp:View>

            <asp:View ID="viewmisshare" runat="server">
                <div class="form-group">
                    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server" CommandName="btnbackmis" OnCommand="btnallback"><i class="fa fa-reply">&nbsp;กลับ</i></asp:LinkButton>
                    <asp:LinkButton ID="LinkBu_back_missh" Visible="false" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server" CommandName="btnbackalllist" OnCommand="btnallback"><i class="fa fa-reply">&nbsp;กลับ</i></asp:LinkButton>
                    <asp:LinkButton ID="lb_lookformis" Visible="false" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server" CommandName="back2" OnCommand="btnallback"><i class="fa fa-reply">&nbsp;กลับ</i></asp:LinkButton>

                    <asp:LinkButton ID="name_crate" runat="server" CssClass="btn btn-success" CommandName="name_crate" Visible="true" OnCommand="btnonclick"><i class="fa fa-id-card-o">&nbsp;ข้อมูลผู้สร้างรายการ</i></asp:LinkButton>
                    <asp:LinkButton ID="Title_create" runat="server" CssClass="btn btn-success" Visible="false" CommandName="Title_create" OnCommand="btnonclick"><i class="fa fa-file-text-o">&nbsp;หัวข้อและรายละเอียด</i></asp:LinkButton>

                </div>
                <asp:Panel ID="plname_cre" runat="server" Visible="true">
                    <asp:Label ID="Label2" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Label4" runat="server" Visible="false"></asp:Label>

                    <asp:TextBox ID="Textbox5" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="Textbox6" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="Textbox7" runat="server" Visible="false"></asp:TextBox>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div><i class="fa fa-file-text">&nbsp<asp:Label ID="name" Font-Bold="true" runat="server" Text="รายละเอียดรายการ"></asp:Label></i></div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12 form-group">
                                <div class="col-md-2 text-right">
                                    <label>รหัสรายการ:</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Literal ID="lb_titleshow" runat="server"></asp:Literal>
                                </div>
                                <div class="col-md-2 text-right">
                                    <label>วันที่สร้างรายการ:</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Literal ID="lb_daycre" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <div class="col-md-2 text-right">
                                    <label>วันที่เริ่มงาน:</label>
                                </div>
                                <div class="col-md-4">

                                    <asp:Label ID="lb_daystart" runat="server" CssClass="date_starts"></asp:Label>
                                </div>
                                <div class="col-md-2 text-right">
                                    <label>วันสิ้นสุดงาน:</label>
                                </div>
                                <div class="col-md-4 textleft">
                                    <asp:Literal ID="lb_dayfinish" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <div class="col-md-2 text-right">
                                    <label>หัวข้อความต้องการ:</label>
                                </div>
                                <div class="col-md-10 cutdiv1">
                                    <asp:Literal ID="nameisthis" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <div class="col-md-2 text-right">
                                    <label>รายละเอียด:</label>
                                </div>
                                <div class="col-md-4 cutdiv1">
                                    <asp:Label ID="tb_er_Vlb" runat="server" Visible="true" CssClass="cutsh"></asp:Label>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-2 text-right">
                                        <label>ไฟล์ในการร้องขอ:</label>
                                    </div>

                                    <div class="col-md-8">
                                        <asp:GridView ID="Gv_filemisshared" Visible="true" ShowHeader="false" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="Master_RowDataBound">

                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div class="col-lg-11">
                                                            <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                        </div>
                                                        <div class="col-lg-1">
                                                            <%--<asp:HyperLink runat="server" ID="btnDL11" CssClass="" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i>ไฟล์</asp:HyperLink>--%>
                                                            <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-primary btn-sm pull-right" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                            <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                        </div>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>

                            </div>



                        </div>
                    </div>

                </asp:Panel>

                <asp:Panel ID="Pl_data_req" runat="server" Visible="false">
                    <%--<asp:Label ID="num" runat="server" Visible="true" Text='<%# Eval("u0_jo_idx") %>' />--%>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div>
                                <i class="glyphicon glyphicon-user"></i>
                                <asp:Label ID="Label8" Font-Bold="true" runat="server" Text="ข้อมูลผู้ระบุความต้องการ"></asp:Label>
                            </div>
                            <%-- <div style="font-size: 150%"><i class="fa fa-id-card-o"></i>&nbsp ข้อมูลผู้ส่งความต้องการ</div>--%>
                        </div>
                        <div class="panel-body">

                            <div class="col-md-12 form-group">
                                <div class="col-md-2 text_right">
                                    <label>รหัสผู้ทำรายการ :</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="id_emprequire" runat="server" Text='<%# Eval("Codeemp") %>'></asp:Label>
                                </div>
                                <div class="col-md-2 text_right">
                                    <label>ชื่อผู้ทำรายการ :</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="name_emprequire" runat="server" Text='<%# Eval("AdminMain") %>'></asp:Label>
                                </div>
                            </div>

                            <div class="col-md-12 form-group">
                                <div class="col-md-2 text_right">
                                    <label>บริษัท :</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="company_emprequire" runat="server" Text='<%# Eval("Organize") %>'></asp:Label>
                                </div>
                                <div class="col-md-2 text_right">
                                    <label>ฝ่าย :</label>
                                </div>
                                <div class="col-md-4">

                                    <asp:Label ID="faction_emprequire" runat="server" Text='<%# Eval("Deptname") %>'></asp:Label>
                                </div>
                            </div>

                            <div class="col-md-12 form-group">
                                <div class="col-md-2 text_right">
                                    <label>แผนก :</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="department_emprequire" runat="server" Text='<%# Eval("Secname") %>'></asp:Label>
                                </div>
                                <div class="col-md-2 text_right">
                                    <label>ตําแหน่ง :</label>
                                </div>
                                <div class="col-md-4">

                                    <asp:Label ID="position_emprequire" runat="server" Text='<%# Eval("Posname") %>'></asp:Label>
                                </div>
                            </div>

                            <div class="col-md-12 form-group">
                                <div class="col-md-2 text_right">
                                    <label>เบอร์ติดต่อ :</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="tel_emprequire" runat="server" Text='<%# Eval("emp_mobile_no") %>'></asp:Label>
                                </div>
                                <div class="col-md-2 text_right">
                                    <label>E-mail :</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="mail_emprequire" runat="server" Text='<%# Eval("emp_email") %>'></asp:Label>
                                </div>
                            </div>

                            <div class="col-md-12 form-group">
                                <div class="col-md-2 text_right">
                                    <label>Cost Center :</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="cost_emprequire" runat="server" Text='<%# Eval("costcenter_no") %>'></asp:Label>
                                </div>

                            </div>

                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="pl_titleinsert" runat="server" Visible="false">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div><i class="fa fa-table">&nbsp<asp:Label ID="Label6" Font-Bold="true" runat="server" Text="หัวข้อการส่งมอบงาน"></asp:Label></i></div>
                            <%-- <div style="font-size: 140%"><i class="fa fa-table"></i>&nbsp หัวข้อการส่งมอบ</div>--%>
                        </div>

                        <div class="panel-body">
                            <div class=" form-group col-md-12">

                                <div class="col-md-2 text-right">
                                    <label>วันที่ในการส่งมอบ:</label>
                                </div>
                                <div class="col-md-7">
                                    <div class='input-group date'>

                                        <asp:TextBox ID="start_requiremis" runat="server" CssClass="form-control from-date-datepickerexpire datetimepicker-last" placeholder="กรุณาเลือกวันที่" Maxlengh="100%"></asp:TextBox>
                                        <span class="input-group-addon show-last-onclick"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="addstory" runat="server" Display="Dynamic" SetFocusOnError="true" ControlToValidate="start_requiremis"
                                        Font-Size="1em" ForeColor="Red" ErrorMessage=" *กรอกวันที่" />
                                </div>
                                <div class="col-md-12">
                                    <div class='input-group date'>
                                    </div>
                                </div>
                            </div>
                            <div class=" form-group col-md-12">
                                <div class="col-md-2 text-right">
                                    <label>หัวข้อที่ส่งมอบ:</label>
                                </div>

                                <div class="col-md-7">

                                    <asp:TextBox ID="title_indb" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="8" placeholder="เพิ่มหัวข้อส่งมอบ" MaxLength="250"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="Requiredtitle" ValidationGroup="addstory" runat="server" Display="Dynamic" SetFocusOnError="true" ControlToValidate="title_indb"
                                        Font-Size="1em" ForeColor="Red" ErrorMessage=" *ระบุหัวข้อ" />
                                    <asp:RegularExpressionValidator ID="ReExtitle_db" runat="server" ValidationGroup="addstory" Display="Dynamic" ErrorMessage="*คุณกรอกข้อมูลเกิน 250 ตัวอักษรหรือกรอกอักขระพิเศษ"
                                        Font-Size="1em" ForeColor="Red" ControlToValidate="title_indb" ValidationExpression="^[\s\S]{0,250}$" SetFocusOnError="true" />


                                    <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                            ValidationGroup="SaveAdd" Display="None"
                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                            ControlToValidate="txtremark"
                                            ValidationExpression="^[\s\S]{0,1000}$"
                                            SetFocusOnError="true" />--%>
                                </div>

                            </div>
                            <div class="col-md-12" style="margin-top: 2%">
                                <%--  <div class="col-md-8"></div>--%>
                                <div class="col-md-9">
                                    <asp:Panel ID="pnlAction" runat="server" CssClass="pull-right" Visible="true">
                                        <asp:LinkButton CssClass="btn btn-success" runat="server" CommandName="addtitlerequire" OnCommand="btninsert" ID="addtitlerequire"
                                            data-toggle="tooltip" title="เพิ่มหัวข้อการส่งมอบงาน" ValidationGroup="addstory">เพิ่มหัวข้อ</asp:LinkButton>
                                    </asp:Panel>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <asp:Panel ID="pl_gvdetail" runat="server" Visible="false">
                                <div class="form-group ">
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10" style="margin-top: 2%">

                                            <asp:GridView ID="gvinfv_showtitle" runat="server" Visible="true"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                                                HeaderStyle-CssClass="info"
                                                AllowPaging="true"
                                                PageSize="10"
                                                OnRowDataBound="Master_RowDataBound"
                                                OnRowDeleting="Master_RowDelete"
                                                OnRowEditing="Master_RowEditing"
                                                OnRowUpdating="Master_RowUpdating"
                                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                                OnPageIndexChanging="Master_PageIndexChanging"
                                                AutoPostBack="false">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center; color: red"><b>ข้อมูลที่ถูกเพิ่มมาถูกลบ</b> </div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lbnumber" runat="server" Visible="false" />
                                                                <%# (Container.DataItemIndex +1) %>
                                                            </small>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>

                                                            <div class="panel-body">
                                                                <div class="form-horizontal" role="form">

                                                                    <div class="form-group">
                                                                        <div class="col-md-3 text-right">
                                                                            <label>วันที่ในการส่งมอบ:</label>
                                                                        </div>
                                                                        <div class="col-md-7">
                                                                            <div class='input-group date'>
                                                                                <asp:TextBox ID="dateedit" runat="server" CssClass="form-control from-date-datepickerexpire_1 datetimepicker-last-edit" Text='<%# Eval("timeline") %>' placeholder="กรุณาเลือกวันที่" Maxlengh="100%"></asp:TextBox>
                                                                                <span class="input-group-addon show-last-onclick-edit"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                                <asp:TextBox ID="id_looo" Visible="false" runat="server" Text='<%# Eval("u0_jo_idx") %>'></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="Requirededittitle_date"
                                                                                    ValidationGroup="editstory" runat="server"
                                                                                    Display="Dynamic"
                                                                                    SetFocusOnError="true"
                                                                                    ControlToValidate="dateedit"
                                                                                    Font-Size="1em" ForeColor="Red"
                                                                                    ErrorMessage=" *กรอกวันที่" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class=" form-group">
                                                                        <div class="col-md-3 text-right">
                                                                            <label>หัวข้อที่ส่งมอบ:</label>
                                                                        </div>
                                                                        <div class="col-md-7">
                                                                            <asp:TextBox ID="titleedit" CssClass="form-control" TextMode="MultiLine" Rows="8" runat="server" Text='<%# Eval("title_1") %>'></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="Requirededittitle"
                                                                                ValidationGroup="editstory" runat="server"
                                                                                Display="Dynamic"
                                                                                SetFocusOnError="true"
                                                                                ControlToValidate="titleedit"
                                                                                Font-Size="1em" ForeColor="Red"
                                                                                ErrorMessage=" *กรอกหัวข้อ" />
                                                                            <asp:RegularExpressionValidator ID="Regulartitle" runat="server" ValidationGroup="editstory" Display="Dynamic" ErrorMessage="*คุณกรอกข้อมูลเกิน 250 ตัวอักษรหรือกรอกอักขระพิเศษ"
                                                                                Font-Size="1em" ForeColor="Red" ControlToValidate="titleedit" ValidationExpression="^[\s\S]{0,250}$" SetFocusOnError="true" />
                                                                        </div>


                                                                        <div class="col-md-2"></div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <%--   <div class="col-md-5"></div>--%>
                                                                        <div class="col-md-10">
                                                                            <asp:Panel ID="pnlAction" runat="server" CssClass="pull-right" Visible="true">
                                                                                <asp:LinkButton CssClass="btn btn-success" ValidationGroup="editstory" ID="savesija" runat="server" title="ยืนยัน" Text="ยืนยัน" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขหัวข้อนี้ใช่หรือไม่?')"></asp:LinkButton>
                                                                                <asp:LinkButton CssClass="btn btn-danger" ID="cancelsija" runat="server" title="ยกเลิก" Text="ยกเลิก" CommandName="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกการแก้ไขหัวข้อนี้หรือหรือไม่?')"></asp:LinkButton>
                                                                            </asp:Panel>
                                                                        </div>
                                                                        <div class="col-md-2"></div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="เรื่อง" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemStyle Wrap="True" />
                                                        <ItemTemplate>
                                                            <small>
                                                                <div class="DisplayDiv">
                                                                    <asp:Label CssClass="DisplayDesc" ID="titleshow" runat="server" Text='<%# Eval("title_1") %>'></asp:Label>
                                                                    <asp:Label ID="oak" runat="server"></asp:Label>
                                                                </div>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="วันที่ในการส่งมอบ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="timedateline" runat="server" Text='<%# Eval("timeline") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <%--<asp:TemplateField HeaderText="ดู" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <small>
                                                                        <asp:label id="sad" runat="server" Text='<%# Eval("u0_jo_idx") %>'></asp:label>
                                                                    </small>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                    <asp:TemplateField HeaderText="ดำเนินการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:LinkButton CssClass="btn btn-success" ID="Edit" runat="server" CommandName="Edit" OnCommand="btnonclick" data-toggle="tooltip" title="แก้ไข"><i  class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                                                <asp:LinkButton Style="font-size: 100%;" CssClass="btn btn-danger" ID="delettitle" runat="server" CommandName="delete" data-toggle="tooltip" title="ลบ" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่?')"><i class="glyphicon glyphicon-remove" ></i></asp:LinkButton>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="nannasi" runat="server" Visible="true">

                                <div class="col-md-12" style="margin-top: 1%">
                                    <div class="col-md-2 col-md-offset-10">
                                        <asp:CheckBox ID="rdbt_end" AutoPostBack="true" Visible="true" runat="server" OnCheckedChanged="rdbt_end_CheckedChanged" Text="ส่งงานครั้งสุดท้าย" />
                                    </div>
                                    <asp:Panel ID="save_allpage" runat="server" Visible="false">
                                        <div class="col-md-2 col-sm-offset-10">
                                            <asp:LinkButton ID="saveall" CssClass="btn btn-success" data-toggle="tooltip" title="บันทึกหัวข้อการส่งมอบ" Visible="true" runat="server" CommandName="savework" OnCommand="btnonclick" CommandArgument='0' OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่ไหม ?')"><i class="glyphicon glyphicon-hdd"></i></asp:LinkButton>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="save_view1" runat="server" Visible="false">
                                        <div class="col-md-2 col-sm-offset-10">
                                            <asp:LinkButton ID="save_viewdep" CssClass="btn btn-success" data-toggle="tooltip" title="บันทึกหัวข้อการส่งมอบ" Visible="true" runat="server" CommandName="savework" OnCommand="btnonclick" CommandArgument='1' OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่ไหม ?')"><i class="glyphicon glyphicon-hdd"></i></asp:LinkButton>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </asp:Panel>

                <div class="form-group">
                    <asp:LinkButton ID="look_log_u0" runat="server" CssClass="btn btn-success" CommandName="look_log_u0" Visible="true" OnCommand="btnonclick" CommandArgument='<%# Eval("u0_jo_idx") %>'><i class="glyphicon glyphicon-eye-open"> ประวัติการกระทำ</i>  </asp:LinkButton>
                    <asp:LinkButton ID="look_u0" runat="server" CssClass="btn btn-success" Visible="false" CommandName="look_u0" OnCommand="btnonclick" CommandArgument='<%# Eval("u0_jo_idx") %>'><i class="glyphicon glyphicon-list-alt"> การแบ่งงาน</i></asp:LinkButton>
                </div>

                <div class="panel panel-default">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <asp:Label ID="title_old" Font-Bold="true" Text=" หัวข้อที่เคยทำการส่งมอบ" runat="server" CssClass="fa fa-table"></asp:Label>
                            <asp:Label ID="title_old_log" Font-Bold="true" Visible="false" Text=" ประวัติการทำงาน" runat="server" CssClass="fa fa-table"></asp:Label>
                        </div>
                    </div>
                    <div class="panel-body">
                        <asp:Panel ID="pl_gvold" runat="server" Visible="true">
                            <asp:GridView ID="gvanthoer_old" runat="server" Visible="true"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-responsive footable word-wrap col-md-12"
                                HeaderStyle-CssClass="info"
                                DataKeyNames="u1_jo_idx"
                                AllowPaging="false"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowDeleting="Master_RowDelete"
                                OnRowEditing="Master_RowEditing"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                AutoPostBack="false">
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center; color: red;"><b>ไม่มีข้อมูล !!</b> </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <%# (Container.DataItemIndex +1) %>
                                            </small>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <div class="col-md-3 text-right">
                                                            <label>วันที่ในการส่งมอบ:</label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="dateedit2" runat="server" CssClass="form-control from-date-datepickerexpire_2 datetimepicker-last-edit-pass" Text='<%# Eval("u1_day_sent") %>' placeholder="กรุณาเลือกวันที่" Maxlengh="100%"></asp:TextBox>
                                                                <span class="input-group-addon show-last-onclick-edit-pass"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                <asp:RequiredFieldValidator ID="RequiredFielddeteed"
                                                                    ValidationGroup="editstory" runat="server"
                                                                    Display="Dynamic"
                                                                    SetFocusOnError="true"
                                                                    ControlToValidate="dateedit2"
                                                                    ForeColor="Red"
                                                                    Font-Size="1em" ErrorMessage=" *กรุณากรอกวันที่ส่งมอบ" />

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class=" form-group">
                                                        <div class="col-md-3 text-right">
                                                            <label>หัวข้อที่ส่งมอบ:</label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <asp:TextBox ID="wow" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("u1_jo_idx") %>'></asp:TextBox>
                                                            <asp:TextBox ID="wow_id_u0" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("u0_jo_idx_ref") %>'></asp:TextBox>
                                                            <asp:TextBox ID="titleedit1" CssClass="form-control" TextMode="MultiLine" Rows="8" runat="server" MaxLength="250" Text='<%# Eval("u1_title") %>'></asp:TextBox>
                                                            <asp:TextBox ID="CopyTitle" runat="server" Visible="false" TextMode="MultiLine" Rows="8" Text='<%# Eval("u1_title") %>'></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="Requirededittitle"
                                                                ValidationGroup="editstory" runat="server"
                                                                Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="titleedit1"
                                                                Font-Size="1em"
                                                                ForeColor="Red"
                                                                ErrorMessage=" *กรอกหัวข้อที่ส่งมอบงาน" />
                                                            <asp:RegularExpressionValidator ID="Regulartitleedit" runat="server" ValidationGroup="editstory" Display="Dynamic" ErrorMessage="*คุณกรอกข้อมูลเกิน 250 ตัวอักษร"
                                                                Font-Size="1em" ForeColor="Red" ControlToValidate="titleedit1" ValidationExpression="^[\s\S]{0,250}$" SetFocusOnError="true" />

                                                        </div>
                                                        <div class="col-md-2"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <%-- <div class="col-md-7"></div>--%>
                                                        <div class="col-md-10">
                                                            <asp:Panel ID="pnlAction" runat="server" CssClass="pull-right" Visible="true">
                                                                <asp:LinkButton CssClass="btn btn-success" ValidationGroup="editstory" ID="Update" runat="server" title="ยืนยัน" Text="ยืนยัน" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขหัวข้อนี้ใช่หรือไม่?')"></asp:LinkButton>
                                                                <asp:LinkButton CssClass="btn btn-danger" ID="Cancel" runat="server" title="ยกเลิก" CommandName="Cancel" Text="ยกเลิก" OnClientClick="return confirm('คุณต้องการยกเลิกการแก้ไขหัวข้อนี้หรือหรือไม่?')"></asp:LinkButton>
                                                            </asp:Panel>
                                                        </div>
                                                        <div class="col-md-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="หัวข้อในการส่งมอบ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemStyle Wrap="True" />
                                        <ItemTemplate>
                                            <small>
                                                <div class="DisplayDiv">
                                                    <asp:Label ID="titleshow1" Visible="true" CssClass="DisplayDesc" runat="server" Text='<%# Eval("u1_title") %>'></asp:Label>
                                                    <asp:Label ID="Label13" Visible="false" runat="server" Text='<%# Eval("u0_jo_idx_ref") %>'></asp:Label>
                                                    <asp:Label ID="Label14" Visible="false" runat="server" Text='<%# Eval("u1_jo_idx") %>'></asp:Label>
                                                    <asp:Label ID="lateshow" CssClass="DisplayDesc" Visible="false" runat="server" Text=" (ล่าช้า)"></asp:Label>
                                                    <asp:Label ID="u1_emp_cre" Visible="false" runat="server" Text='<%# Eval("u1_emp_sent") %>'></asp:Label>
                                                    <asp:Label ID="name_empsend" Visible="false" runat="server" Text='<%# Eval("createmp") %>'></asp:Label>
                                                    <asp:Label ID="missend" Visible="false" runat="server" Text='<%# Eval("sec_mis") %>'></asp:Label>

                                                </div>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันที่ในการส่งมอบ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="timedateline1" runat="server" Text='<%# Eval("u1_day_sent") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ผู้ที่เซ็นรับงาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="text_userhelp" runat="server" Visible="false" Text="รอ mis ส่งงานกลับ"></asp:Label>
                                                <asp:Label ID="textalert" Visible="false" runat="server" Text="รอดำเนินการ"></asp:Label>
                                                <asp:TextBox ID="lmao" runat="server" Visible="false" Text='<%# Eval("u1_status") %>'></asp:TextBox>
                                                <asp:LinkButton CssClass="btn btn-success" Visible="true" ID="user_okok" CommandArgument='<%# Eval("u1_jo_idx") + ";" + Eval("u0_jo_idx_ref")+ ";"+ Eval("u1_title")+";"+(Container.DataItemIndex) %>' runat="server" CommandName="user_okok" data-toggle="tooltip" title="กดเพื่อยอมรับเรื่อง" OnClientClick="return confirm('คุณต้องการยอมรับเรื่อง หรือไม่')" OnCommand="btnonclick"><i  class="glyphicon glyphicon-ok"></i></asp:LinkButton>
                                                <asp:LinkButton CssClass="btn btn-danger" ID="user_nook" Visible="true" CommandArgument='<%# Eval("u1_jo_idx") + ";"  + Eval("u0_jo_idx_ref")+ ";"+ Eval("u1_title") +";"+(Container.DataItemIndex) %>' runat="server" CommandName="user_nook" data-toggle="tooltip" title="กดเพื่อปฏิเสธการรับเรื่อง" OnClientClick="return confirm('คุณต้องการยอมรับเรื่อง หรือไม่')" OnCommand="btnonclick"><i  class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                <asp:Label ID="user_accept" runat="server" Visible="false" Text='<%# Eval("receive_emp_name") %>'></asp:Label>
                                                <%--  <asp:Label ID="Label18" runat="server" Visible="true" Text='<%# Eval("name_description") %>'></asp:Label>--%>

                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="วันที่รับงาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="day_accept" runat="server" Visible="false" Text='<%# Eval("u1_day_receive") %>'></asp:Label>
                                                <asp:Label ID="time_accept" runat="server" Visible="false" Text='<%# Eval("u1_day_receive_time") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="อื่นๆ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="textalert_antoher" runat="server" Visible="false" Text="ไม่สามารถดำเนินการได้ เนื่องจากคุณไม่ได้อยู่ในแผนกที่ดำเนินงาน"></asp:Label>
                                                <asp:LinkButton Visible="false" Style="font-size: 100%;" CssClass="btn btn-success" CommandArgument='<%# Eval("u1_jo_idx") + ";" + Eval("u0_jo_idx_ref") + ";"+ Eval("u1_title")+";"+(Container.DataItemIndex)  %>' ID="editsuccess" runat="server" CommandName="editsuccess" OnCommand="btnonclick" data-toggle="tooltip" title="ส่งหัวข้อการส่งมอบงาน" OnClientClick="return confirm('กดเพื่อส่งหัวข้ออีกครั้ง?')"><i class="glyphicon glyphicon-send" style="font-size:120%"></i></asp:LinkButton>
                                                <asp:Label ID="id_all" Visible="false" runat="server" Text='<%# Eval("u0_jo_idx_ref") %>'></asp:Label>
                                                <asp:LinkButton Visible="false" CssClass="btn btn-success" ID="Edit" runat="server" CommandName="Edit" OnCommand="btnonclick" data-toggle="tooltip" title="แก้ไขหัวข้อการส่งมอบงาน"><i  class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                                <asp:Label ID="id_center" Visible="false" runat="server" Text='<%# Eval("u1_jo_idx") %>'></asp:Label>
                                                <asp:LinkButton Visible="false" Style="font-size: 100%;" CssClass="btn btn-danger" CommandArgument='<%# Eval("u1_jo_idx") + ";"+ Eval("u0_jo_idx_ref")+ ";"+ Eval("u1_title") +";"+(Container.DataItemIndex)  %>' ID="delettitle" runat="server" CommandName="deletenaja" OnCommand="btnonclick" data-toggle="tooltip" title="ลบหัวข้อการส่งมอบงาน" OnClientClick="return confirm('คุณต้องการลบหัวข้อนี้ใชหรือไม่?')"><i class="glyphicon glyphicon-remove" ></i></asp:LinkButton>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ความคิดเห็น" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>

                                                <asp:TextBox ID="txt_commentu1" runat="server" CssClass="form-control" MaxLength="500" placeholder="เเสดงความคิดเห็น..." Rows="3"></asp:TextBox>
                                                 <asp:Label ID="u1com" runat="server" Visible="false" Text='<%# Eval("u1_jo_idx") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>

                        <asp:Panel ID="pl_log_gvold" runat="server" Visible="false">
                            <asp:GridView ID="gv_logold" runat="server" Visible="true"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                                HeaderStyle-CssClass="info"
                                DataKeyNames="l0_jo_idx"
                                AllowPaging="false"
                                OnRowDataBound="Master_RowDataBound"
                                AutoPostBack="false">
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center; color: red"><b>ไม่มีการกระทำกับ การแบ่งงานนี้</b> </div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="วัน/เวลา" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="log_m0_dat" runat="server" Text='<%# Eval("day_create") %>'></asp:Label>
                                                <asp:Label ID="log_m0_dat_time" runat="server" Text='<%# Eval("u1_day_sent_time") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemStyle Wrap="true" />
                                        <ItemTemplate>
                                            <small>
                                                <div class="DisplayDiv">
                                                    <asp:Label ID="log_req" CssClass="DisplayDesc" runat="server" Text='<%# Eval("requried_old") %>'></asp:Label>
                                                </div>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ผู้ดำเนินการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="log_name" runat="server" Text='<%# Eval("name_emp") %>'></asp:Label>
                                                <asp:Label ID="Label17" runat="server" Text="("></asp:Label>
                                                <asp:Label ID="Label18" runat="server" Text='<%# Eval("name_description") %>'></asp:Label>
                                                <asp:Label ID="Label19" runat="server" Text=")"></asp:Label>

                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ดำเนินการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="log_m0_node" runat="server" Text='<%# Eval("node_name_l0") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ผลการดำเนินการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="log_m0_sta" runat="server" Text='<%# Eval("statenode_name_l0") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ความคิดเห็น" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                 <asp:Label ID="log_commentu1" runat="server" Text='<%# Eval("comment") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </asp:Panel>

                        <asp:Panel ID="pl_succ" runat="server" Visible="true">
                            <div class="col-md-12">
                                <div class="col-md-2 col-md-offset-10">
                                    <asp:LinkButton Style="font-size: 100%;" Visible="false" CssClass="btn btn-success" ID="successwork" runat="server" CommandName="MISOKSTA" OnCommand="btnonclick" data-toggle="tooltip" title="กดเพื่อเซ็นปิดรายการ" OnClientClick="return confirm('คุณต้องการเซ็นปิดรายการนี้หรือไม่?')"><i class="glyphicon glyphicon-saved" ></i></asp:LinkButton>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>

            </asp:View>

            <asp:View ID="create_story" runat="server">
                <div>
                    <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                        <ItemTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <asp:Label ID="Label1" Font-Bold="true" Text=" รายละเอียดผู้ใช้งาน" runat="server" CssClass="glyphicon glyphicon-user"></asp:Label>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">รหัสพนักงาน:</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">ชื่อ - นามสกุล:</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">องค์กร:</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">ฝ่าย:</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">แผนก:</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">ตำแหน่ง:</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </ItemTemplate>
                    </asp:FormView>



                    <asp:Panel ID="remoteuser" Visible="true" runat="server">
                        <div class="panel panel-info" runat="server" id="insert" visible="true">
                            <div class="panel-heading">
                                <asp:Label ID="Label10" Font-Bold="true" Text="สร้างรายการความต้องการ" runat="server" CssClass="glyphicon glyphicon-file"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12 word-wrap">

                                    <div class="form-group col-md-12">

                                        <div class="col-md-9">
                                            <asp:Label ID="reinput1" CssClass="control-label" Font-Bold="true" runat="server" Text="ระบุหัวข้อความต้องการ : " />
                                            <asp:TextBox runat="server" ID="titlename" placeholder="กรุณากรอกหัวข้อ" Style="margin-left: 2px" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Saveinsert" runat="server" Display="Dynamic"
                                                ControlToValidate="titlename" ForeColor="Red" Font-Size="8" ErrorMessage="*กรุณากรอกข้อมูลให้ครบ" />
                                            <%--<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />--%>
                                            <asp:RegularExpressionValidator ID="Regulartitle" runat="server" ValidationGroup="Saveinsert" Display="Dynamic" ErrorMessage="*คุณกรอกข้อมูลเกิน 250 ตัวอักษร"
                                                Font-Size="8" ForeColor="Red" ControlToValidate="titlename" ValidationExpression="^[\s\S]{0,250}$" SetFocusOnError="true" />
                                            <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regulartitle" Width="220" />--%>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div class="col-sm-9">
                                            <asp:Label ID="reinput2" CssClass="control-label" Font-Bold="true" runat="server" Text="ระบุรายละเอียดความต้องการ : " />
                                            <br />
                                            <asp:TextBox ID="require" runat="server" TextMode="MultiLine" CssClass="form-control tinymce" Rows="7" placeholder="กรุณากรอกรายละเอียดความต้องการ"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator_require" runat="server" ValidationGroup="Saveinsert" Display="Dynamic" ErrorMessage="*คุณกรอกข้อมูลเกิน 1000 ตัวอักษร"
                                                Font-Size="8" ForeColor="Red" ControlToValidate="require" ValidationExpression="^[\s\S]{0,1000}$" SetFocusOnError="true" />
                                            <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator_require" Width="220" />--%>

                                            <asp:RequiredFieldValidator ID="requiredreq" ValidationGroup="Saveinsert" runat="server" SetFocusOnError="true"
                                                ControlToValidate="require" Font-Size="8" ForeColor="Red" Display="Dynamic" ErrorMessage="*กรุณากรอกข้อมูลให้ครบ" />
                                            <%--<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopRight" TargetControlID="requiredreq" Width="220" />--%>
                                        </div>
                                        <%-------------- Upload File --------------%>
                                        <%--<div class="form-group col-md-12">--%>
                                        <div class="col-sm-9">
                                            <%--<div class="panel panel-success">--%>
                                            <%-- <div class="panel-body">--%>
                                            <label><small>อัพโหลดไฟล์  (upload File)</small></label>

                                            <asp:FileUpload ID="UploadFileComment" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                CssClass="control-label" accept="jpg|pdf|png|jpeg" />
                                            <asp:RegularExpressionValidator ID="RegularExpre_UploadFileComment"
                                                runat="server" ErrorMessage="*กรุณาเลือกเฉพาะไฟล์ jpg|jpeg|png หรือ pdf*" SetFocusOnError="true"
                                                Display="Dynamic" Font-Size="8" ForeColor="Red" ControlToValidate="UploadFileComment" ValidationExpression="[a-zA-Z\\].*(.jpg|.JPG|.pdf|.PDF|.JPEG|.jpeg|.PNG|.png)$"
                                                ValidationGroup="Saveinsert" />
                                            <div class="col-sm-3"></div>
                                            <p class="help-block"><font color="#ff6666"> **นามสกุลที่รองรับ  JPG, JPEG, PNG และ PDF</font></p>
                                            <%--</div>--%>
                                            <%--</div>--%>
                                        </div>
                                        <%--</div>--%>

                                        <%--<div class="form-group col-md-12">--%>
                                        <div class="col-sm-6">
                                            <asp:Label ID="Lasecmis" CssClass="control-label" Font-Bold="true" runat="server" Text="แผนกที่ดำเนินงาน : " />
                                            <br />
                                            <asp:DropDownList ID="ddlinsert_sec" runat="server" CssClass="form-control" ValidationGroup="Saveinsert" />
                                            <asp:RequiredFieldValidator ID="Requiredddlinsert_Sec" ValidationGroup="Saveinsert" runat="server" Display="Dynamic"
                                                ControlToValidate="ddlinsert_sec" Font-Size="8" ForeColor="Red"
                                                ErrorMessage="*กรุณาเลือกแผนก"
                                                ValidationExpression="แผนก" InitialValue="0" />
                                            <%--<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlinsert_Sec" Width="220" />--%>
                                            <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlSec_excel" Width="160" />--%>
                                        </div>
                                        <br />

                                        <%--  </div>--%>
                                    </div>
                                    <div class="form-group">
                                        <div class=" col-md-12">
                                            <%--<div class="col-md-1"></div>--%>
                                            <asp:UpdatePanel ID="updatebtnsave" UpdateMode="Always" runat="server">
                                                <ContentTemplate>
                                                    <div class="col-md-11">
                                                        <asp:LinkButton ID="trueadd" class="btn btn-success" runat="server" Visible="true"
                                                            data-toggle="tooltip" title="เพิ่มรายการ" CommandName="cmdadd" OnCommand="btnuser" ValidationGroup="Saveinsert"
                                                            OnClientClick="return confirm('คุณต้องการสร้างรายการ ใช่หรือไม่')">
                                                        <i class="glyphicon glyphicon-plus"></i>
                                                        </asp:LinkButton>
                                                        <%--    </div>
                                                <div class="col-md-1">--%>
                                                        <asp:LinkButton ID="reset" class="btn btn-warning" runat="server" Visible="true" CommandName="reset"
                                                            data-toggle="tooltip" title="รีเซ็ต" OnCommand="btnuser">
                                                        <i class="glyphicon glyphicon-repeat"></i>
                                                        </asp:LinkButton>
                                                        <%--  </div>
                                        <div class="col-md-1">--%>
                                                        <asp:LinkButton ID="closejob" class="btn btn-danger" runat="server" Visible="true"
                                                            data-toggle="tooltip" title="ยกเลิกการสร้างรายการ" CommandName="close" OnCommand="btnuser">
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                        </asp:LinkButton>
                                                    </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="trueadd" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </asp:View>

            <asp:View ID="View1" runat="server">
                <asp:LinkButton ID="searchbutton" class="btn btn-primary m-b-10 f-s-14" runat="server"
                    Visible="false" CommandName="showsearch" OnCommand="btnuser"><i class="glyphicon glyphicon-search"></i> แสดงเครื่องมือค้นหา</asp:LinkButton>
                <asp:LinkButton ID="searchhidden" class="btn btn-danger m-b-10 f-s-14" runat="server"
                    Visible="false" CommandName="showsearch1" OnCommand="btnuser" Text="ซ่อนเครื่องมือค้นหา" />

                <div id="_divsearch" runat="server" visible="false">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="form-group col-md-12">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-3">
                                        <label class="f-s-13">วันที่สร้าง</label>
                                        <%--<br />--%>
                                        <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            <asp:ListItem Value="0">เลือกเงื่อนไข</asp:ListItem>
                                            <asp:ListItem Value="1">มากกว่า ></asp:ListItem>
                                            <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                            <asp:ListItem Value="3">ระหว่าง <></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="f-s-13">ตั้งแต่วันที่</label>
                                        <div class='input-group date'>
                                            <asp:HiddenField ID="txtdateId" runat="server" />
                                            <asp:TextBox ID="dateId" Enabled="false" runat="server" CssClass="form-control datetimepicker-from cursor-pointer" placeholder="ตั้งแต่วันที่" ReadOnly="true" />
                                            <span class="input-group-addon show-from-onclick ">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            <span class="input-group-addon clear-from-onclick">
                                                <span class="fa fa-times"></span>
                                            </span>
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <label class="f-s-13">ถึงวันที่</label>
                                        <div class='input-group date'>
                                            <asp:HiddenField ID="txtdateIdf" runat="server" />
                                            <asp:TextBox ID="dateIdf" runat="server" CssClass="form-control datetimepicker-to cursor-pointer" placeholder="ถึงวันที่"
                                                ReadOnly="true" Enabled="false" />
                                            <span class="input-group-addon show-to-onclick ">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            <span class="input-group-addon clear-to-onclick">
                                                <span class="fa fa-times"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4">
                                        <label class="f-s-13">แผนก</label>
                                        <%--<br />--%>
                                        <asp:DropDownList ID="ddSec_dept" CssClass="form-control" runat="server" />
                                        <br />
                                    </div>
                                    <div class="col-md-5">
                                        <label class="f-s-13">ชื่อ-นามสกุล ผู้สร้าง</label>
                                        <asp:TextBox ID="empidsearch" runat="server" CssClass="form-control" placeholder="ป้อนชื่อ-นามสกุล ของผู้สร้างรายการ" />
                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4">
                                        <label class="f-s-13">รหัสรายการความต้องการ</label>

                                        <asp:TextBox ID="TempCodeId" runat="server" MaxLength="8" CssClass="form-control" placeholder="ป้อนรหัสรายการ เช่น JB600001" />
                                    </div>
                                </div>

                            </div>
                            <!--btn search-->
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-3">
                                        <asp:LinkButton ID="btnSearch" data-toggle="tooltip" title="ค้นหา" runat="server" CssClass="btn btn-success" OnCommand="btnuser" CommandName="btncommandsearch"><i class="glyphicon glyphicon-search"></i> </asp:LinkButton>
                                        <asp:LinkButton ID="btnReset" data-toggle="tooltip" title="รีเซ็ต" runat="server" CssClass="btn btn-warning f-s-13" OnCommand="btnuser" CommandName="resetsearch"><i class="glyphicon glyphicon-repeat"></i> </asp:LinkButton>
                                    </div>
                                    <div class="col-md-8"></div>
                                </div>
                            </div>

                            <!--///////////-->
                        </div>
                    </div>
                </div>

                <asp:Panel ID="grid" Visible="true" runat="server">
                    <asp:GridView ID="jogrid" runat="server" AutoGenerateColumns="false" DataKeyNames="u0_jo_idx" CssClass="table table-striped table-bordered table-hover table-responsive wordwrap_p1 col-md-12"
                        HeaderStyle-CssClass="info" OnRowDataBound="Master_RowDataBound" AllowPaging="true" PageSize="10"
                        AutoPostBack="False" OnPageIndexChanging="Master_PageIndexChanging" OnRowEditing="Master_RowEditing"
                        OnRowCancelingEdit="Master_RowCancelingEdit" OnRowUpdating="Master_RowUpdating">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />

                        <EmptyDataTemplate>
                            <div style="text-align: center; color: red"><b>!! ยังไม่มีข้อมูลเข้ามา</b> </div>
                        </EmptyDataTemplate>


                        <Columns>

                            <asp:TemplateField HeaderText="รหัสรายการ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="gencode" runat="server" Text='<%# Eval("no_invoice") %>' />
                                    <%--<asp:Label ID="u0_jojrid" runat="server" Visible="false" Text='<%# Eval("u0_jo_idx") %>' />
                                        <%# (Container.DataItemIndex +1) %>--%>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ผู้สร้างรายการ" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                <ItemTemplate>
                                    <small>
                                        <strong>
                                            <asp:Label ID="org_emp" runat="server">บริษัท: </asp:Label></strong>
                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("Organize") %>' />
                                        </p>
                                           
                                        <strong>
                                            <asp:Label ID="dep_emp" runat="server">ฝ่าย: </asp:Label></strong>
                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("Deptname") %>' />
                                        </p>
                                           
                                        <strong>
                                            <asp:Label ID="lbl_rsec" runat="server">แผนก: </asp:Label></strong>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Secname") %>' />
                                        </p>
                                                
                                        <strong>
                                            <asp:Label ID="name_emp" runat="server">ชื่อผู้สร้าง: </asp:Label></strong>
                                        <asp:Label ID="lblempname" runat="server" Text='<%# Eval("AdminMain") %>' />
                                        <asp:Label ID="lbl_rsec_checkpermission" Visible="false" runat="server" Text='<%# Eval("rsec_creator") %>' />
                                        <asp:Label ID="jogrid_sec" Visible="false" runat="server" Text='<%# Eval("sec_mis") %>' />
                                        </p>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หัวข้อความต้องการ" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center"
                                HeaderStyle-Width="20%">
                                <ItemStyle Wrap="True" />
                                <ItemTemplate>
                                    <div style="word-break: break-all;">
                                        <asp:Label ID="title" runat="server" Text='<%# Eval("title_jo") %>' />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่" ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                <ItemTemplate>
                                    <small>
                                        <strong>
                                            <asp:Label ID="day_create" runat="server">วันที่สร้าง: </asp:Label></strong>
                                        <asp:Label ID="rdep_num_look" Visible="false" runat="server" Text='<%# Eval("rdep_num") %>'></asp:Label>
                                        <asp:Label ID="day_create_jogrid" runat="server" Text='<%# Eval("day_created") %>' />
                                        <asp:Label ID="timelook_all" runat="server" Text='<%# Eval("u0_day_sent_time") %>' />
                                        </p>
                                        <%--<strong>
                                            <asp:Label ID="time_create" runat="server">เวลาที่สร้าง:</asp:Label></strong>
                                        <asp:Label ID="timelook_all" runat="server" Text='<%# Eval("u0_day_sent_time") %>' />
                                        </p>--%>
                                        <strong>
                                            <asp:Label ID="day_start" runat="server">วันที่เริ่มงาน: </asp:Label></strong>
                                        <asp:Label ID="day_start_job" Visible="true" runat="server" Text='<%# Eval("day_start") %>' />
                                        </p>
                                        <strong>
                                            <asp:Label ID="day_end" runat="server">วันที่สิ้นสุดงาน: </asp:Label></strong>
                                        <asp:Label ID="day_end_job" Visible="true" runat="server" Text='<%# Eval("day_finish") %>' />
                                        </p>

                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>


                                    <asp:Label ID="_actorm0" runat="server" Visible="false" Text='<%# Eval("m0_actor_idx") %>' />
                                    <asp:Label ID="_nodem0" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>' />
                                    <asp:Label ID="_idlog" runat="server" Visible="false" Text='<%# Eval("jo_idx_ref") %>' />
                                    <asp:Label ID="_idstate" runat="server" Visible="false" Text='<%# Eval("id_statework") %>' />

                                    <asp:Label ID="showact" runat="server" Text='<%# Eval("statenode_name") %>' />
                                    <asp:Label ID="anthoername" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                    <!--<asp:Label ID="act" runat="server" Text='<%# Eval("name_actor") %>' />-->
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                                HeaderStyle-Width="15%">

                                <ItemTemplate>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:UpdatePanel ID="up7" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:LinkButton CssClass="btn btn-sm btn-info" ID="gohost_speed" Visible="false" runat="server" CommandName="lookhead" OnCommand="btncommand" data-toggle="tooltip" title="สำหรับหัวหน้าเเผนก" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("no_invoice")+";" + "1" %>'><i class="glyphicon glyphicon-envelope"></i></asp:LinkButton>

                                                    <asp:LinkButton ID="detailj2_te" class="btn btn-sm btn-info" runat="server" Visible="false" CommandName="detail"
                                                        data-toggle="tooltip" title="สำหรับหัวหน้าฝ่าย MIS" OnCommand="btnuser" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework")  + ";" + Eval("day_start") + ";" + Eval("day_finish") + ";" + Eval("jo_idx_ref")+ ";" + Eval("no_invoice")  %>'>
                                            <i class="glyphicon glyphicon-new-window"></i></asp:LinkButton>

                                                    <asp:LinkButton ID="sentaccept" class="btn btn-info btn-sm" Style="background-color: forestgreen" runat="server" Visible="false" CommandName="showsent" OnCommand="btnuser"
                                                        data-toggle="tooltip" title="ระยะเวลาที่ดำเนินงาน" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework") %>'>
                                                    <i class="glyphicon glyphicon-time"></i></asp:LinkButton>

                                                    <asp:LinkButton ID="sentrup" CssClass="btn btn-sm btn-primary" runat="server" Visible="false" data-toggle="tooltip" title="ตารางการดำเนินงาน" CommandName="lookmis" OnCommand="btncommand" CommandArgument='<%# Eval("u0_jo_idx") + ";" + "0"+";" + Eval("no_invoice") + ";" + "1" %>'><i class="glyphicon glyphicon-calendar"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="edit1" class="btn btn-info btn-sm" Style="background-color: lightseagreen" runat="server" data-toggle="tooltip" title="รายละเอียดของรายการ" CommandName="edit1" OnCommand="btnuser" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework")+ ";" + Eval("jo_idx_ref")+ ";" + Eval("no_invoice")+";"+Eval("emp_id_creator")  %>'><i class="glyphicon glyphicon-list-alt"></i></asp:LinkButton>

                                                </ContentTemplate>

                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="edit1" />
                                                </Triggers>

                                            </asp:UpdatePanel>
                                            <%--<asp:LinkButton ID="delete" class="btn-info btn-sm" Style="background-color: #e12121" runat="server" Visible="true" data-toggle="tooltip" title="ลบรายการ"
                                            CommandName="del" OnCommand="btnuser" CommandArgument='<%# Eval("u0_jo_idx") %>'
                                            OnClientClick="return confirm('คุณต้องการ ลบข้อมูลใช่หรือไม่')">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                        </asp:LinkButton>--%>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </asp:Panel>


                <div class="col-lg-12" runat="server" id="div_sentaccept">
                    <div id="sentmodal" class="modal open" role="dialog">
                        <div class="modal-dialog" style="width: 45%">
                            <!--Modal content-->
                            <div class="modal-content" style="background: whitesmoke">
                                <div class="modal-header" style="color: #31708f; background: #d9edf7">

                                    <asp:Label ID="lsdate" CssClass="fa fa-calendar" Font-Bold="true" runat="server" Text=" พิจารณาผลรับทราบระยะเวลาในการดำเนินงาน" />


                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">
                                        <asp:FormView ID="DetailUsersent" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="DetailUsersent_DataBound">
                                            <ItemTemplate>
                                                <div class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:Label ID="lsdate" CssClass="col-sm-3 control-label" Font-Bold="true" runat="server" Text="วันที่เริ่มงาน : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="sdate" runat="server" Font-Bold="true" CssClass="form-control" Maxlengh="100%" Enabled="false" Style="text-align: center" Text='<%# Eval("day_start") %>'></asp:TextBox>
                                                                <asp:Label ID="reqtest" CssClass="control-labelnotop col-xs-12" runat="server" Visible="false" Text='<%# Eval("require_jo") %>' />
                                                                <asp:Label ID="sec_mis_date" CssClass="control-labelnotop col-xs-12" runat="server" Visible="false" Text='<%# Eval("sec_mis") %>' />

                                                            </div>
                                                            <asp:Label ID="lfdate" CssClass="col-sm-3 control-label" Font-Bold="true" runat="server" Text="วันที่สิ้นสุดงาน : " />

                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="fdate" runat="server" Font-Bold="true" CssClass="form-control" Maxlengh="100%" Enabled="false" Style="text-align: center" Text='<%# Eval("day_finish") %>'></asp:TextBox>
                                                            </div>
                                                            <div class="col-sm-1"></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <%--<asp:Label ID="day_a" CssClass="col-sm-4 control-label" Font-Bold="true" runat="server" Text="ระยะเวลาในการทำงาน : " />--%>
                                                        <div class="col-sm-4 text-right">
                                                            <label>ระยะเวลาในการทำงาน:</label>
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_date_detail" Font-Bold="true" runat="server" CssClass="form-control" Maxlengh="100%" Enabled="false" Style="text-align: center" Text='<%# Eval("day_amount") %>'></asp:TextBox>
                                                        </div>
                                                        <asp:Label ID="day" CssClass="col-sm-1 control-label" Font-Bold="true" runat="server" Text="วัน" />
                                                        <div class="col-sm-3"></div>
                                                    </div>
                                                </div>

                                            </ItemTemplate>
                                        </asp:FormView>

                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label>พิจารณาผล:</label>
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm6">
                                                <asp:LinkButton ID="yomrup" class="btn btn-success" runat="server" Visible="true" CommandName="acceptsent" CommandArgument="0" OnCommand="btnuser"
                                                    Text="ยอมรับ" OnClientClick="return confirm('คุณต้องการ เซ็นยอมรับใช่หรือไม่')" />
                                                <asp:LinkButton ID="maiyomrup" class="btn btn-warning " runat="server" Visible="true" CommandName="closemishead2" CommandArgument="0" OnCommand="btnuser"
                                                    Text="ไม่ยอมรับ" OnClientClick="return confirm('คุณต้องการ ไม่ยอมรับใช่หรือไม่')" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-3 text-right">
                                                <label>comment:</label>
                                            </div>
                                            <%-- <div class="col-sm-2"></div>--%>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txt_comment_usertime" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="2" placeholder="เเสดงความคิดเห็น..." MaxLength="500"></asp:TextBox>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="form-group">
                                            <div class="col-sm-offset-10">
                                                <asp:LinkButton ID="Cancel1" class="btn btn-danger" data-toggle="tooltip" title="ปิด" runat="server" data-dismiss="modal"
                                                    CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </asp:View>
            <%--     <asp:View ID="View2" runat="server">
                <asp:Panel ID="grid2" Visible="true" runat="server">
                    <asp:GridView ID="jogrid2" runat="server" AutoGenerateColumns="false" DataKeyNames="u0_jo_idx" CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                        OnRowDataBound="Master_RowDataBound" HeaderStyle-CssClass="info" AllowPaging="true" PageSize="10" AutoPostBack="False"
                        OnPageIndexChanging="Master_PageIndexChanging" OnRowEditing="Master_RowEditing" OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnRowUpdating="Master_RowUpdating">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center; color: red"><b>ไม่มีข้อมูล !! ที่เข้ามาถึง ส่วน MIS HEAD</b> </div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="ID" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="10%"
                                Visible="false">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="u0" runat="server" Visible="false" Text='<%# Eval("u0_jo_idx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    
                                    </small>
                                    <asp:HiddenField ID="nodehf" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                                    <asp:HiddenField ID="actorhf" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                                    <asp:HiddenField ID="statehf" runat="server" Value='<%# Eval("id_statework") %>' />
                                    <asp:HiddenField ID="log" runat="server" Value='<%# Eval("jo_idx_ref") %>' />
                                    <asp:Label ID="dstart" runat="server" Visible="false" Text='<%# Eval("day_start") %>' />
                                    <asp:Label ID="dfinish" runat="server" Visible="false" Text='<%# Eval("day_finish") %>' />
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รหัสรายการ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                                HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="nvj2" runat="server" Text='<%# Eval("no_invoice") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>



                            <asp:TemplateField HeaderText="หัวข้อความต้องการ" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center"
                                HeaderStyle-Width="25%">
                                <ItemStyle Wrap="True" />
                                <ItemTemplate>
                                    <asp:Label CssClass="DisplayDesc" ID="titlej2" runat="server" Text='<%# Eval("title_jo") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่เริ่มต้น" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                                HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="dst" runat="server" Text='<%# Eval("day_start") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่สิ้นสุด" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                                HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="dfn" runat="server" Text='<%# Eval("day_finish") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="30%">
                                <ItemTemplate>
                                    <asp:Label ID="statusmishead" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>




                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="f-s-13 text-center"
                                HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="detailj2" class="btn-sm btn-info" runat="server" Visible="true" CommandName="detail"
                                        data-toggle="tooltip" title="รายละเอียดที่ร้องขอเพื่อกำหนดเวลา" OnCommand="btnuser" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework")  + ";" + Eval("day_start") + ";" + Eval("day_finish") + ";" + Eval("jo_idx_ref")+ ";" + Eval("no_invoice")  %>'>
                                            <i class="glyphicon glyphicon-new-window"></i>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </asp:Panel>
                <br />
            </asp:View>--%>
            <asp:View ID="View3" runat="server">

                <!--<asp:Label ID="oaklog" runat="server" CssClass="form-control" visible="false" Text='<%# Eval("oakja") %>' />-->
                <div class="w3-container">
                    <div class="row">
                        <%--<asp:LinkButton ID="LinkButton6" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server" CommandName="back2" OnCommand="btnallback"><i class="fa fa-reply">&nbsp;กลับ</i></asp:LinkButton>--%>
                        <asp:LinkButton ID="btnback" class="btn btn-danger" runat="server" Visible="true"
                            data-toggle="tooltip" title="ย้อนกลับ" CommandName="back" OnCommand="btnallback">
                               <i class="fa fa-reply">&nbsp;กลับ</i>
                        </asp:LinkButton>

                        <asp:LinkButton ID="btnview3_to2" OnClientClick="dotim()" class="btn btn-danger" runat="server" Visible="false"
                            data-toggle="tooltip" title="ย้อนกลับ" CommandName="btnbackalllist" OnCommand="btnallback">
                            <i class="fa fa-reply">&nbsp;กลับ</i>
                        </asp:LinkButton>
                        <%--<asp:LinkButton ID="back_rdepsamw" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server" CommandName="back2" OnCommand="btnallback"><i class="fa fa-reply">&nbsp;กลับ</i></asp:LinkButton>--%>
                        <asp:LinkButton ID="back_rdepsamw" Visible="false" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server" CommandName="back2" OnCommand="btnallback"><i class="fa fa-reply">&nbsp;กลับ</i></asp:LinkButton>
                    </div>
                </div>
                <br />

                <asp:Panel ID="Panel1" Visible="true" runat="server">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <asp:Label ID="Label12" Font-Bold="true" Text="ข้อมูลผู้ระบุความต้องการ" runat="server" CssClass="glyphicon glyphicon-user"></asp:Label>
                            <%-- <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp;ข้อมูลส่วนตัว USER</strong></h3>--%>
                        </div>
                        <div class="panel-body">
                            <asp:FormView ID="DetailUserviewH" runat="server" DefaultMode="ReadOnly" OnDataBound="Detail_DataBound" Width="100%">
                                <ItemTemplate>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>รหัสผู้ทำรายการ :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="id_emprequireDe" runat="server" Text='<%# Eval("Codeemp") %>'></asp:Label>
                                        </div>
                                        <div class="col-md-2 text_right">
                                            <label>ชื่อผู้ทำรายการ :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="name_emprequireDe" runat="server" Text='<%# Eval("AdminMain") %>'></asp:Label>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>บริษัท :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="company_emprequireDe" runat="server" Text='<%# Eval("Organize") %>'></asp:Label>
                                        </div>
                                        <div class="col-md-2 text_right">
                                            <label>ฝ่าย :</label>
                                        </div>
                                        <div class="col-md-4">

                                            <asp:Label ID="faction_emprequireDe" runat="server" Text='<%# Eval("Deptname") %>'></asp:Label>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>แผนก :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="department_emprequireDe" runat="server" Text='<%# Eval("Secname") %>'></asp:Label>
                                        </div>
                                        <div class="col-md-2 text_right">
                                            <label>ตําแหน่ง :</label>
                                        </div>
                                        <div class="col-md-4">

                                            <asp:Label ID="position_emprequireDe" runat="server" Text='<%# Eval("Posname") %>'></asp:Label>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>เบอร์ติดต่อ :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="tel_emprequireDe" runat="server" Text='<%# Eval("emp_mobile_no") %>'></asp:Label>
                                        </div>
                                        <div class="col-md-2 text_right">
                                            <label>E-mail :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="mail_emprequireDe" runat="server" Text='<%# Eval("emp_email") %>'></asp:Label>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>Cost Center :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="cost_emprequireDe" runat="server" Text='<%# Eval("costcenter_no") %>'></asp:Label>
                                        </div>

                                    </div>

                                </ItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                </asp:Panel>



                <div class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-file-text"></i>
                            <asp:Label ID="Label9" Font-Bold="true" runat="server">&nbsp;รายละเอียดรายการ</asp:Label>
                        </div>
                        <div class="panel-body">

                            <asp:FormView ID="DetailUser" runat="server" OnDataBound="Detail_DataBound" Width="100%">

                                <ItemTemplate>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>รหัสรายการ :</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="iddoc" runat="server" Text='<%# Eval("no_invoice") %>'></asp:Label>

                                        </div>

                                        <div class="col-md-2 text_right">
                                            <label>วันที่สร้างรายการ : </label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="datecarteread" runat="server" Text='<%# Eval("day_created") %>'></asp:Label>
                                            <asp:Label ID="actorcode2" runat="server" Text='<%# Eval("u0_day_sent_time") %>' />
                                        </div>

                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>หัวข้อความต้องการ:</label>
                                        </div>
                                        <div class="col-md-5 cutdiv1">
                                            <asp:Label ID="titledoc" runat="server" Text='<%# Eval("title_jo") %>'></asp:Label>
                                        </div>
                                        <%-- 
                                        <div class="col-md-2 text-right">
                                            <label>เวลาที่สร้างรายการ:</label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="actorcode2" CssClass="control-labelnotop col-xs-12 col-sm-8 col-md-9 col-lg-10" runat="server" Text='<%# Eval("u0_day_sent_time") %>' />
                                        </div>--%>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text_right">
                                            <label>รายละเอียดรายการ:</label>
                                        </div>
                                        <div class="col-md-2 cutdiv1">
                                            <asp:Label ID="Label3" runat="server" CssClass="cutsh"></asp:Label>
                                            <asp:Label ID="reqcode" runat="server" Text='<%# Eval("require_jo") %>' />
                                            <asp:Label ID="reqcodetest" runat="server" Visible="false" Text='<%# Eval("require_jo") %>' />
                                            <asp:Label ID="sec_misH" Visible="false" runat="server" Text='<%# Eval("sec_mis") %>'></asp:Label>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="col-md-2 text-right">
                                            <label>ไฟล์ในการร้องขอ:</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <asp:GridView ID="Gv_filemisH" Visible="true" runat="server" ShowHeader="false" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="Master_RowDataBound">

                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <div class="col-lg-11">
                                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                            </div>
                                                            <div class="col-lg-1">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-primary btn-sm pull-right" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div class="col-lg-1"></div>
                                    </div>
                                </ItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-list-alt"></i>
                            <asp:Label ID="l11" Font-Bold="true" runat="server">&nbsp; กำหนดระยะเวลาในการดำเนินงานและอนุมัติรายการ</asp:Label>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal" role="form">
                                <%--<div class="col-md-4"></div>--%>
                                <div class="form-group">
                                    <div class="col-md-2 text-right">

                                        <label>ระบุวันที่เริ่มงาน:</label>
                                        <%--<asp:Label ID="addstart" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่เริ่มงาน : " />--%>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">

                                            <div class='input-group date'>
                                                <!--<asp:HiddenField ID="txtstartdate" runat="server" />-->
                                                <asp:TextBox ID="startdate" runat="server" CssClass="form-control from-date-datepickerexpire_p datetimepicker-from1" placeholder="กรุณาเลือกวันที่"
                                                    SetFocusOnError="true" />
                                                <span class="input-group-addon show-from-onclick1"><span class="glyphicon glyphicon-calendar"></span></span>

                                            </div>


                                            <asp:RequiredFieldValidator ID="requiredsdate" ValidationGroup="dategroup" runat="server" Display="Dynamic" Font-Size="8" ForeColor="Red" ControlToValidate="startdate"
                                                ErrorMessage="*กรุณากรอกข้อมูลให้ครบ" />
                                            <%--<ajaxToolkit:ValidatorCalloutExtender ID="extendersdate" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="requiredsdate"
                                                Width="170" PopupPosition="BottomLeft" />--%>
                                        </div>
                                        <div class="col-md-7"></div>
                                    </div>

                                </div>

                                <div class="form-group ">
                                    <div class="col-md-2 text-right">
                                        <label>ระบุวันที่สิ้นสุดงาน:</label>
                                        <%-- <asp:Label ID="addfinal" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่สิ้นสุดงาน : " />--%>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <!--<asp:HiddenField ID="txtfinaldate" runat="server" />-->
                                                <asp:TextBox ID="finaldate" runat="server" CssClass="form-control to-date-datepickerexpire datetimepicker-to1" placeholder="กรุณาเลือกวันที่"
                                                    Maxlengh="100%" SetFocusOnError="true" />
                                                <span class="input-group-addon show-to-onclick1"><span class="glyphicon glyphicon-calendar"></span></span>

                                            </div>
                                            <asp:RequiredFieldValidator ID="requiredfdate" ValidationGroup="dategroup" runat="server" Display="Dynamic" Font-Size="8" ForeColor="Red" ControlToValidate="finaldate"
                                                ErrorMessage="*กรุณากรอกข้อมูลให้ครบ" />
                                            <%--<ajaxToolkit:ValidatorCalloutExtender ID="extenderfdate" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="requiredfdate"
                                                Width="170" PopupPosition="BottomLeft" />--%>
                                        </div>
                                        <div class="col-md-7"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-2 text-right">
                                    <label>พิจารณาผล:</label>
                                </div>
                                <div class="col-md-10">
                                    <%--<div class="col-md-1">--%>


                                    <asp:LinkButton ID="accept" class="btn btn-success" runat="server" data-toggle="tooltip" title="อนุมัติ" Visible="false" CommandName="accept" OnCommand="btnuser"
                                        Text="อนุมัติ" ValidationGroup="dategroup" CommandArgument='0' OnClientClick="return confirm('คุณต้องการ อนุมัติใช่หรือไม่')" />
                                    <%--<div class="col-md-1">--%>
                                    <asp:LinkButton ID="decline" class="btn btn-danger" runat="server" Visible="false" data-toggle="tooltip" title="ไม่อนุมัติ" CommandName="closemishead" OnCommand="btnuser"
                                        Text="ไม่อนุมัติ" CommandArgument='0' OnClientClick="return confirm('คุณต้องการ ไม่อนุมัติใช่หรือไม่')" />

                                    <asp:LinkButton ID="accept_allpage" class="btn btn-success" runat="server" data-toggle="tooltip" title="อนุมัติ" Visible="false" CommandName="accept" OnCommand="btnuser"
                                        Text="อนุมัติ" ValidationGroup="dategroup" CommandArgument='1' OnClientClick="return confirm('คุณต้องการ อนุมัติใช่หรือไม่')" />
                                    <%--<div class="col-md-1">--%>
                                    <asp:LinkButton ID="declineall_page" class="btn btn-danger" runat="server" Visible="false" data-toggle="tooltip" title="ไม่อนุมัติ" CommandName="closemishead" OnCommand="btnuser"
                                        Text="ไม่อนุมัติ" CommandArgument='1' OnClientClick="return confirm('คุณต้องการ ไม่อนุมัติใช่หรือไม่')" />

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-2 text-right">
                                    <label>comment:</label>
                                </div>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txt_comment_misH" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" placeholder="เเสดงความคิดเห็น..." MaxLength="500"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <%--                </div>--%>
            </asp:View>
            <asp:View ID="View4" runat="server">

                <div class="row">
                    <asp:LinkButton ID="btnback2" OnClientClick="dotim()" class="btn btn-danger" runat="server" Visible="true"
                        data-toggle="tooltip" title="ย้อนกลับ" CommandName="back2" OnCommand="btnallback">
                            <i class="fa fa-reply">&nbsp;กลับ</i>
                    </asp:LinkButton>
                    <asp:LinkButton ID="btnV4back_all" OnClientClick="dotim()" class="btn btn-danger" runat="server" Visible="false"
                        data-toggle="tooltip" title="ย้อนกลับ" CommandName="backall" OnCommand="btnallback">
                            <i class="fa fa-reply">&nbsp;กลับ</i>
                    </asp:LinkButton>
                </div>
                <br />
                <div class="form-horizontal" role="form">
                    <asp:TextBox ID="u0_idx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("u0_jo_idx")%>' />


                    <asp:Panel ID="userpanel2" Visible="true" runat="server">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <asp:Label ID="Label1" Font-Bold="true" Text="ข้อมูลผู้ระบุความต้องการ" runat="server" CssClass="glyphicon glyphicon-user"></asp:Label>
                                <%-- <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp;ข้อมูลส่วนตัว USER</strong></h3>--%>
                            </div>
                            <div class="panel-body">
                                <asp:FormView ID="DetailUserpv" runat="server" DefaultMode="ReadOnly" OnDataBound="Detail_DataBound" Width="100%">
                                    <ItemTemplate>
                                        <div class="col-md-12 form-group">
                                            <div class="col-md-2 text_right">
                                                <label>รหัสผู้ทำรายการ :</label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label ID="v4_id_emprequireDe" runat="server" Text='<%# Eval("Codeemp") %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 text_right">
                                                <label>ชื่อผู้ทำรายการ :</label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label ID="v4_name_emprequireDe" runat="server" Text='<%# Eval("AdminMain") %>'></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <div class="col-md-2 text_right">
                                                <label>บริษัท :</label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label ID="v4_company_emprequireDe" runat="server" Text='<%# Eval("Organize") %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 text_right">
                                                <label>ฝ่าย :</label>
                                            </div>
                                            <div class="col-md-4">

                                                <asp:Label ID="v4_faction_emprequireDe" runat="server" Text='<%# Eval("Deptname") %>'></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <div class="col-md-2 text_right">
                                                <label>แผนก :</label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label ID="v4_department_emprequireDe" runat="server" Text='<%# Eval("Secname") %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 text_right">
                                                <label>ตําแหน่ง :</label>
                                            </div>
                                            <div class="col-md-4">

                                                <asp:Label ID="v4_position_emprequireDe" runat="server" Text='<%# Eval("Posname") %>'></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <div class="col-md-2 text_right">
                                                <label>เบอร์ติดต่อ :</label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label ID="v4_tel_emprequireDe" runat="server" Text='<%# Eval("emp_mobile_no") %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 text_right">
                                                <label>E-mail :</label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label ID="v4_mail_emprequireDe" runat="server" Text='<%# Eval("emp_email") %>'></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <div class="col-md-2 text_right">
                                                <label>Cost Center :</label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label ID="v4_cost_emprequireDe" runat="server" Text='<%# Eval("costcenter_no") %>'></asp:Label>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="userpanel" Visible="true" runat="server">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <asp:Label ID="Label5" Font-Bold="true" Text=" รายละเอียดรายการ" runat="server" CssClass="fa fa-file-text"></asp:Label>
                            </div>
                            <div class="panel-body">

                                <asp:FormView ID="DetaileditUser" runat="server" DefaultMode="ReadOnly" OnDataBound="Detail_DataBound" Width="100%">

                                    <ItemTemplate>

                                        <div class="panel-body">

                                            <div class="col-md-12 form-group">
                                                <div class="col-md-2 text-right">
                                                    <label>รหัสรายการ:</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:Label ID="noinvoice2" CssClass="control-labelnotop col-xs-12 col-sm-8 col-md-9 col-lg-10" runat="server" Text='<%# Eval("no_invoice") %>' />
                                                </div>

                                                <div class="col-md-2 text-right">
                                                    <label>วันที่สร้างรายการ:</label>
                                                </div>
                                                <div class="col-md-4 textleft">
                                                    <asp:Label ID="datecode2" runat="server" Text='<%# Eval("day_created") %>' />
                                                    <asp:Label ID="actorcode2" runat="server" Text='<%# Eval("u0_day_sent_time") %>' />
                                                </div>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <div class="col-md-2 text-right">
                                                    <label>หัวข้อความต้องการ:</label>
                                                </div>
                                                <div class="col-md-8 cutdiv1">
                                                    <asp:Label ID="titlecode2" CssClass="cutsh" runat="server" Text='<%# Eval("title_jo") %>' />
                                                    <asp:Label ID="sec_mis_edit" Visible="false" runat="server" Text='<%# Eval("sec_mis") %>' />
                                                </div>
                                                <%--<div class="col-md-2 text-right">
                                                    <label>เวลาที่สร้างรายการ:</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:Label ID="actorcode2" CssClass="control-labelnotop col-xs-12 col-sm-8 col-md-9 col-lg-10" runat="server" Text='<%# Eval("u0_day_sent_time") %>' />
                                                </div>--%>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <div class="col-md-2 text-right">
                                                    <label>รายละเอียดรายการ:</label>
                                                </div>
                                                <div class="col-md-8 cutdiv1">
                                                    <asp:Label ID="reqcode_j" CssClass="cutsh" runat="server" Text='<%# Eval("require_jo") %>' />
                                                </div>
                                            </div>

                                            <%--<div class="form-group">--%>
                                            <div class="col-md-12 form-group">
                                                <div class="col-md-2 text-right">
                                                    <label>ไฟล์ในการร้องขอ:</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:GridView ID="gvFile" Visible="true" runat="server" ShowHeader="false" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="Master_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <div class="col-lg-11">
                                                                        <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                    </div>
                                                                    <div class="col-lg-1">
                                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-primary btn-sm pull-right" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                    </div>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                            <%--</div>--%>
                                        </div>
                                        <%--<div class="form-group">--%>
                                        <div class="col-md-12 form-group">
                                            <div class="col-md-2 text-right"></div>
                                            <div class="col-md-6">
                                                <asp:LinkButton ID="btnedit" class="btn btn-warning" runat="server" Visible="false"
                                                    CommandName="edit2" OnCommand="btnuser" data-toggle="tooltip" Text="เเก้ไขข้อมูลรายการความต้องการ" title="เเก้ไขข้อมูลรายการความต้องการ"
                                                    CommandArgument='<%# Eval("u0_jo_idx") %>'>
                                                          <%--  <i class="glyphicon glyphicon-pencil"></i>--%>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="addafter" class="btn btn-info" runat="server" Visible="false" CommandName="addaf" OnCommand="btnuser"
                                                    data-toggle="tooltip" Text="ส่งรายการความต้องการ" title="ส่งรายการความต้องการ" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework") %>'
                                                    OnClientClick="return confirm('คุณต้องการส่งข้อมูล ใช่หรือไม่')">
                                                      <%--<i class="glyphicon glyphicon-export"></i>--%>
                                                </asp:LinkButton>

                                            </div>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <div class="col-md-2 text-right">
                                                <asp:Label ID="labcomment_noedit" runat="server" Font-Bold="true" Text="comment:" Visible="false"></asp:Label>
                                            </div>
                                            <div class="col-md-8">
                                                <asp:TextBox ID="txt_comment_noedit" runat="server" Visible="false" placeholder="เเสดงความคิดเห็น..." CssClass="form-control" TextMode="MultiLine" Rows="3" MaxLength="500"></asp:TextBox>
                                            </div>
                                        </div>

                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label23" Font-Bold="true" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="หัวข้อความต้องการ : " />
                                                <div class="col-xs-9">
                                                    <asp:TextBox ID="titledit" CssClass="form-control" runat="server" Text='<%# Eval("title_jo") %>' Enabled="false" />
                                                    <asp:RequiredFieldValidator ID="requiredtitledit" ValidationGroup="edit_job" runat="server" Display="Dynamic" SetFocusOnError="true"
                                                        ControlToValidate="titledit" Font-Size="0.5em" ForeColor="Red" ErrorMessage="*กรุณากรอกข้อมูล" />
                                                    <asp:Label ID="sec_mis_edit2" Visible="false" runat="server" Text='<%# Eval("sec_mis") %>' />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label33" Font-Bold="true" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รายละเอียดรายการ: " />
                                                <div class="col-xs-9">

                                                    <asp:TextBox runat="server" ID="reqeditshow" TextMode="MultiLine" CssClass="col-xs-10 tinymce" Rows="8" Text='<%# Eval("require_jo") %>'
                                                        AutoPostBack="false" />
                                                    <asp:TextBox runat="server" ID="reqedit" Visible="false" TextMode="MultiLine" CssClass="col-xs-10" Rows="8" Text='<%# Eval("require_jo") %>' />
                                                    <asp:RequiredFieldValidator ID="requiredreqedit" ValidationGroup="edit_job" runat="server" Display="Dynamic" SetFocusOnError="true"
                                                        ControlToValidate="reqeditshow" Font-Size="1.0em" ForeColor="Red" ErrorMessage="*กรุณากรอกข้อมูล" />
                                                    <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" PopupPosition="BottomRight"
                                                        HighlightCssClass="validatorCalloutHighlight111" TargetControlID="requiredreqedit" Width="220" />--%>

                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="edit_job" Display="Dynamic" ErrorMessage="*คุณกรอกข้อมูลเกิน 1000 ตัวอักษร"
                                                        Font-Size="8" ForeColor="Red" ControlToValidate="reqeditshow" ValidationExpression="^[\s\S]{0,1000}$" SetFocusOnError="true" />
                                                    <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="220" />--%>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <asp:Label ID="Label11" Font-Bold="true" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text=" ไฟล์ในการร้องขอ: " />
                                                <%--<div class="col-sm-2"></div>--%>
                                                <div class="col-sm-9">
                                                    <asp:GridView ID="gvFile" Visible="true" runat="server" ShowHeader="false" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="Master_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <div class="col-lg-11">
                                                                        <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                    </div>
                                                                    <div class="col-lg-1">
                                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-primary btn-sm pull-right" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                    </div>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-9">
                                                    <asp:Label ID="upfile" runat="server" Text="**เพิ่มไฟล์รายการใหม่(ถ้าต้องการ)"></asp:Label>
                                                    <%--  <div class="panel panel-success">--%>
                                                    <%--<div class="panel-body">--%>
                                                    <label><small>อัพโหลดไฟล์  (upload File)</small></label>
                                                    <asp:FileUpload ID="UploadFileComment_edit" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                        CssClass="control-label" accept="jpg|pdf|png|jpeg" />
                                                    <asp:RegularExpressionValidator ID="RegularExpre_UploadFileComment1"
                                                        runat="server" ErrorMessage="*กรุณาเลือกเฉพาะไฟล์ jpg|jpeg|png หรือ pdf*" Font-Size="8" ForeColor="Red" SetFocusOnError="true"
                                                        Display="Dynamic" ControlToValidate="UploadFileComment_edit" ValidationExpression="[a-zA-Z\\].*(.jpg|.JPG|.pdf|.PDF|.JPEG|.jpeg|.PNG|.png)$"
                                                        ValidationGroup="edit_job" />
                                                    <%--<ajaxToolkit:ValidatorCalloutExtender ID="Validator_UploadFileComment" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpre_UploadFileComment1" Width="220" />--%>
                                                    <div class="col-sm-3"></div>
                                                    <p class="help-block"><font color="#ff6666">**นามสกุลที่รองรับ  JPG, JPEG, PNG และ PDF</font></p>
                                                    <%--</div>--%>
                                                    <%--</div>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <div class="col-md-2 text-right">
                                                <asp:Label ID="labcomment" runat="server" Font-Bold="true" Text="comment:" Visible="true"></asp:Label>
                                            </div>
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txt_comment_edit" runat="server" Visible="true" placeholder="เเสดงความคิดเห็น..." CssClass="form-control" TextMode="MultiLine" Rows="3" MaxLength="500"></asp:TextBox>
                                            </div>
                                        </div>
                                    </EditItemTemplate>
                                </asp:FormView>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-2">
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>

                                                    <asp:LinkButton ID="truebut" class="btn btn-success" runat="server" Visible="false"
                                                        CommandName="Updateedit" OnCommand="btnuser" ValidationGroup="edit_job" data-toggle="tooltip"
                                                        title="อัพเดท" CommandArgument='<%# Eval("u0_jo_idx") %>'>
                                            <i class="glyphicon glyphicon-ok"></i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="cmdcancel" class="btn btn-danger" runat="server" Visible="false"
                                                        data-toggle="tooltip" title="ยกเลิก" CommandName="cancelbut" OnCommand="btnuser" OnClientClick="dotim()">
                                            <i class="glyphicon glyphicon-remove"></i>
                                                    </asp:LinkButton>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="truebut" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                            <%--<div class="col-md-1">OnClientClick="dotim()" </div>--%>
                                        </div>
                                        <div class="col-sm-10"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="panel panel-default">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <asp:Label ID="Label7" Font-Bold="true" Text=" หัวข้อที่เคยทำการส่งมอบ" runat="server" CssClass="fa fa-table"></asp:Label>
                        </div>
                    </div>
                    <div class="panel-body">
                        <asp:Panel ID="gridshowu1" Visible="true" runat="server">
                            <asp:GridView ID="u1grid" runat="server" AutoGenerateColumns="false" DataKeyNames="u1_jo_idx" CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                                HeaderStyle-CssClass="info" AllowPaging="true" PageSize="10" AutoPostBack="False" OnPageIndexChanging="Master_PageIndexChanging">
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center; color: red"><b>ไม่มีข้อมูล !! ส่วนรายละเอียดงาน</b> </div>
                                </EmptyDataTemplate>

                                <Columns>
                                    <asp:TemplateField HeaderText="ผู้ส่งมอบหัวข้อ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                                        HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="u1employee" runat="server" Text='<%# Eval("createmp") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="วันที่ส่งงาน" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                                        HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="u1daysent" runat="server" Text='<%# Eval("u1_day_sent") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="หัวข้อในการส่งมอบ" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center"
                                        HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="word-break: break-all;">
                                                <asp:Label ID="u1title" runat="server" Text='<%# Eval("u1_title") %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="วันที่รับงาน" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                                        HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="u1dayre" runat="server" Text='<%# Eval("u1_day_receive") %>' />
                                            <asp:Label ID="u1time" runat="server" Text='<%# Eval("u1_day_receive_time") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ผู้เซ็นรับงาน" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                                        HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="u1employeerecive" runat="server" Text='<%# Eval("receive_emp_name") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <asp:Label ID="label_log" CssClass="fa fa-building-o" Font-Bold="true" runat="server" Text=" ประวัติรายการในระบบ Job Order" />
                        </div>
                    </div>
                    <div class="panel-body">
                        <asp:GridView ID="gv_logjob" runat="server" Visible="true"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                            HeaderStyle-CssClass="info"
                            AllowPaging="false"
                            OnRowDataBound="Master_RowDataBound"
                            AutoPostBack="false">
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center; color: red"><b>ไม่มีข้อมูลประวัติการกระทำของ รายการนี้</b> </div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="วัน/เวลา" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <%--<asp:Label ID="u0log_idx" runat="server" Visible="false" Text='<%# Eval("u0_jo_idx") %>' />--%>
                                            <asp:Label ID="log_date" runat="server" Text='<%# Eval("day_created") %>'></asp:Label>
                                            <asp:Label ID="log_time" runat="server" Text='<%# Eval("day_created_time_log") %>'></asp:Label>

                                            <%-- <asp:Label ID="log_m0_dat_time" runat="server" Text='<%# Eval("u1_day_sent_time") %>'></asp:Label>--%>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ผู้ดำเนินการ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemStyle Wrap="true" />
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="log_actor" CssClass="DisplayDesc" runat="server" Text='<%# Eval("nameActor") %>'></asp:Label>
                                            <asp:Label ID="log" CssClass="DisplayDesc" runat="server" Text="("></asp:Label>
                                            <asp:Label ID="log_pos" CssClass="DisplayDesc" runat="server" Text='<%# Eval("name_description") %>'></asp:Label>
                                            <asp:Label ID="Label16" CssClass="DisplayDesc" runat="server" Text=")"></asp:Label>

                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ดำเนินการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="log_nodename" runat="server" Text='<%# Eval("node_name") %>'></asp:Label>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ผลการดำเนินการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="log_name_sta" runat="server" Text='<%# Eval("statenode_name") %>'></asp:Label>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ความคิดเห็น" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <asp:Label ID="log_name_sta" runat="server" Text='<%# Eval("comment") %>'></asp:Label>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>

                <%--<div class="panel panel-default">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <asp:Label ID="page_log" CssClass="fa fa-building-o" Font-Bold="true" runat="server" Text=" ประวัติรายการในระบบ Job Order" />
                        </div>
                        <div class="panel-body">

                            <asp:Label ID="u0log_idx" runat="server" Visible="false" Text='<%# Eval("u0_jo_idx") %>' />

                            <asp:Repeater ID="rplog" runat="server">
                                <HeaderTemplate>
                                    <div class="row">
                                        <label class="col-sm-3 control-label">วัน/เวลา</label>
                                        <label class="col-sm-3 control-label">ผู้ดำเนินการ</label>
                                        <label class="col-sm-3 control-label">ดำเนินการ</label>
                                        <label class="col-sm-3 control-label">ผลการดำเนินการ</label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <span><small><%#Eval("day_created")%></small></span>
                                        </div>
                                        <div class="col-sm-3">
                                            <span><small><%# Eval("nameActor")%></small></span>
                                        </div>
                                        <div class="col-sm-3">
                                            <span><small><%# Eval("node_name") %></small></span>
                                        </div>
                                        <div class="col-sm-3">
                                            <span><small><%# Eval("statenode_name") %></small></span>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>

                    </div>
                </div>--%>
            </asp:View>

        </asp:MultiView>
    </div>

    <script>
        //$('input[name="daterange"]').daterangepicke({
        //    locate: {
        //        format: 'DD/MM/YYYY'
        //    },
        //    startDate: moment(),
        //    endDate: '20/07/2020'
        //},
        //    function (start, end, label) {
        //        alert("A new rara: " + start.format('DD/MM/YYYY') + ' to ' + end.format('DD/MM/YYYY'));
        //    });
        $(function () {

            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment()

            });
            $('.from-date-datepickerexpire_1').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment()

            });
            $('.from-date-datepickerexpire_2').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment()

            });
            $('.show-last-onclick').click(function () {
                $('.datetimepicker-last').data("DateTimePicker").show();
            });
            $('.show-last-onclick-edit').click(function () {
                $('.datetimepicker-last-edit').data("DateTimePicker").show();
            });
            $('.show-last-onclick-edit-pass').click(function () {
                $('.datetimepicker-last-edit-pass').data("DateTimePicker").show();
            });
        });


        var dat = Sys.WebForms.PageRequestManager.getInstance();
        dat.add_endRequest(function () {
            $(function () {
                var dates = $('#<%= lb_daystart.ClientID %>').html().split("/");
                var dateUtc = dates[2] + '-' + dates[1] + '-' + dates[0];
                var minDate = new Date(dateUtc);

                $('.from-date-datepickerexpire').datetimepicker({
                    format: 'DD/MM/YYYY'
                    , minDate: minDate
                });
                $('.from-date-datepickerexpire_1').datetimepicker({
                    format: 'DD/MM/YYYY'
                    , minDate: new Date() //minDate().add(-1, 'days')  //  ถ้าเป็ย -1 จะได้นะจ้า         

                });
                $('.from-date-datepickerexpire_2').datetimepicker({
                    format: 'DD/MM/YYYY'
                    , minDate: new Date()//moment().add(-1, 'days')



                });


                $('.show-last-onclick').click(function () {
                    $('.datetimepicker-last').data("DateTimePicker").show();
                });
                $('.show-last-onclick-edit').click(function () {
                    $('.datetimepicker-last-edit').data("DateTimePicker").show();
                });
                $('.show-last-onclick-edit-pass').click(function () {
                    $('.datetimepicker-last-edit-pass').data("DateTimePicker").show();
                });
            });

        });

    </script>

</asp:Content>

