﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="law_trademarks_contract.aspx.cs" Inherits="websystem_laws_law_trademarks_contract" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbDetail" runat="server" CommandName="cmdDetail" OnCommand="navCommand" CommandArgument="docDetail"> Detail Document</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docCreate"> Craete Document</asp:LinkButton>
                        </li>

                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbWaitApprove" runat="server" CommandName="cmdWaitApprove" OnCommand="navCommand" CommandArgument="docWaitApprove"> Waiting Approve</asp:LinkButton>
                        </li>

                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbRegistration" runat="server" Visible="false" CommandName="cmdRegistration" OnCommand="navCommand" CommandArgument="docRegistration"> Record Sheet</asp:LinkButton>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right" runat="server">

                        <li id="_divMenuLiToDocument" runat="server">

                            <asp:HyperLink ID="hplFlowOvertime" NavigateUrl="https://drive.google.com/file/d/1sQWujxbiWPFaGv82CC6RZ9PUQA5z-eSp/view?usp=sharing" Target="_blank" runat="server" CommandName="cmdFlowOvertime" OnCommand="btnCommand"><i class="fas fa-book"></i> Flow การทำงาน</asp:HyperLink>

                        </li>
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->

            </div>
        </nav>
    </div>
    <!--tab menu-->

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug1" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Detail-->
        <asp:View ID="docDetail" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="upMain_HeadDetail" runat="server">
                    <ContentTemplate>
                        <div class="row">
                            <asp:Literal ID="litPanelTitleHeadDetail" runat="server" Text="Title"></asp:Literal>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>


                <!-- Form view Search Detail -->
                <asp:FormView ID="fvSearchDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Search Detail</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document Type</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlTypeSearch" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                           <%-- <asp:RequiredFieldValidator ID="Req_ddlTypeSearch"
                                                runat="server" ControlToValidate="ddlTypeSearch" InitialValue="0"
                                                Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Select Document Type"
                                                ValidationGroup="SearchDetail" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlTypeSearch" Width="200" PopupPosition="BottomLeft" />--%>

                                        </div>

                                        <label class="col-sm-2 control-label">Type</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlSubTypeSearch" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document No</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txt_documentno_Search" runat="server" CssClass="form-control" MaxLength="8" placeholder="Document No" >
                                            </asp:TextBox>


                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="btnSearchDetail" runat="server" CssClass="btn btn-primary" data-original-title="Search" data-toggle="tooltip" ValidationGroup="SearchDetail" CommandName="cmdSearchDetail" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> Search</asp:LinkButton>

                                            <asp:LinkButton ID="btnResetDetail" runat="server" CssClass="btn btn-default" data-original-title="Refresh" data-toggle="tooltip" CommandName="cmdResetDetail" OnCommand="btnCommand"><span class="glyphicon glyphicon-refresh"></span> Refresh</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form view Search Detail -->

                <div id="div_gvDetailList" runat="server" style="overflow: auto">

                    <asp:GridView ID="gvDetailList" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                        HeaderStyle-CssClass="info"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        AllowPaging="True"
                        PageSize="10"
                        DataKeyNames="u0_doc_idx">
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="Document No." ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lblu0_doc_idx" runat="server" Visible="false" Text='<%#Eval("u0_doc_idx") %>'></asp:Label>
                                    <asp:Label ID="lbldocrequest_code" runat="server" Text='<%#Eval("docrequest_code") %>'></asp:Label>
                                    <%--<asp:Label ID="lblDocIDX" runat="server" Visible="false" Text='<%# Eval("u0_qalab_idx") %>'></asp:Label>
                                <asp:Label ID="lblCemp_idx_per" runat="server" Visible="false" Text='<%# Eval("cemp_idx") %>'></asp:Label>
                                <asp:Label ID="lblplaceIDX" runat="server" Visible="false" Text='<%# Eval("place_idx") %>'></asp:Label>
                               
                                <asp:Label ID="lblDocCode" runat="server" Text='<%# Eval("document_code") %>'></asp:Label>--%>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Create Date" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>

                                    <asp:Label ID="lblcreate_date" runat="server" Text='<%#Eval("create_date") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Document Detail" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>

                                    <p>
                                        <b>Document Type :</b>
                                        <asp:Label ID="lbltype_name_en" runat="server" Text='<%# Eval("type_name_en") %>' />
                                    </p>
                                    <p>
                                        <b>Sub Document Type :</b>
                                        <asp:Label ID="lblsubtype_name_en" runat="server" Text='<%# Eval("subtype_name_en") %>' />
                                    </p>
                                    <p>
                                        <b>Job Type :</b>
                                        <asp:Label ID="lbljobtype_name_en" runat="server" Text='<%# Eval("jobtype_name_en") %>'></asp:Label>
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Document Status" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="lblcurrent_status" runat="server" Text='<%# Eval("current_status") %>'></asp:Label>
                                    <%--<asp:Label ID="lblDocStatus" Visible="false" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>--%>
                                    <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                    <asp:Label ID="lblnoidx" runat="server" Visible="false" Text='<%# Eval("noidx") %>'></asp:Label>
                                    <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                <ItemTemplate>

                                    <asp:LinkButton ID="btnViewDetail" CssClass="btn-sm btn-info" runat="server" CommandName="cmdViewDetail" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx") + ";" + Eval("noidx") + ";" + Eval("acidx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx")  %>' data-toggle="tooltip" title="View"><i class="fa fa-file"></i></asp:LinkButton>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>

                <!-- Form Detail Employee  -->
                <asp:FormView ID="fvEmpViewDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>


                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel">
                            <%--<div class="panel-heading">
                               
                                <img src='<%= ResolveUrl("~/images/law-trademarks/header-create1.png") %>' style="width: 70%; height: 100%;" class="img-responsive img-fluid" alt="User Detail" />
                            </div>--%>

                            <div class="row">
                                <div class="row col-md-12">
                                    <div class="col-md-6" style="margin-top: -10px; padding: 0px;">
                                        <img src='<%= ResolveUrl("~/images/law-trademarks/user-detail.png") %>' style="width: 100%;" class="img-responsive img-fluid" alt="User Detail" />
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>

                            <div class="panel-body img-responsive" style="background-color: transparent; background-image: url('images/law-trademarks/body-viewdetail.png'); background-size: 100% 100%;" id="pn_viewbgbodyuser" runat="server">
                                <%--<div class="panel-body bg-template-resign">--%>
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Employee Code</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Name - Surname</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Organization</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_en") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Department</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Section</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_en") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Position</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Phone</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbemp_mobile_no" runat="server" CssClass="form-control" Text='<%# Eval("emp_mobile_no") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">E-mail</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbemp_email" runat="server" CssClass="form-control" Text='<%# Eval("emp_email") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee-->

                <!-- Form view Detail Document -->
                <asp:FormView ID="fvDocumentDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEACIDX" runat="server" Value='<%# Eval("acidx") %>' />
                        <asp:HiddenField ID="hfNOIDX" runat="server" Value='<%# Eval("noidx") %>' />
                        <asp:HiddenField ID="hfSTAIDX" runat="server" Value='<%# Eval("staidx") %>' />
                        <div class="panel">
                            <%--<div class="panel-heading">
                                
                                <img src='<%= ResolveUrl("~/images/law-trademarks/document-detail.png") %>' style="width: 70%; height: 100%;" class="img-fluid" alt="User Detail" />
                               
                            </div>--%>

                            <div class="row">
                                <div class="row col-md-12">
                                    <div class="col-md-6" style="margin-top: -10px; padding: 0px;">
                                        <img src='<%= ResolveUrl("~/images/law-trademarks/document-detail.png") %>' style="width: 100%;" class="img-fluid" alt="User Detail" />
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>


                            <div class="panel-body" style="background-color: transparent; background-image: url('images/law-trademarks/body-detail.png'); background-size: 100% 100%;" id="pn_detailbgbody" runat="server">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document Request Code</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbdocrequest_code_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Create Date</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbcreate_date_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("create_date") %>' Enabled="false" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document Type</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbtype_name_en_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Type</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbsubtype_name_en_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Country</label>

                                        <div class="col-sm-4">
                                            <div style="overflow-y: scroll; width: 100%; height: 200px">
                                                <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />--%>

                                                <asp:CheckBoxList ID="chkCountryview" runat="server" Enabled="false" CellPadding="5" CellSpacing="5" RepeatColumns="2" Width="100%" Font-Bold="false"
                                                    RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />


                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">Job Type</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_jobtype_name_en_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("jobtype_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Topic Document</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbtopic_name_viewdetail" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" CssClass="form-control" Text='<%# Eval("topic_name") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Detail Document</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_doc_detail_viewdetail" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" CssClass="form-control" Text='<%# Eval("doc_detail") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Detail Law Officer</label>
                                        <div class="col-sm-4">
                                            <asp:GridView ID="gvViewDetailLawOfficer"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                HeaderStyle-CssClass="default"
                                                AllowPaging="true"
                                                OnRowDataBound="gvRowDataBound"
                                                OnPageIndexChanging="gvPageIndexChanging"
                                                PageSize="10">
                                                <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">No result</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Name - LastName" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblu3_doc_idx_view" runat="server" Text='<%# Eval("u3_doc_idx") %>' Visible="false" />
                                                            <asp:Label ID="lbl_emp_name_en_view" runat="server" Text='<%# Eval("emp_name_en") %>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Position(EN)" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblrpos_idx_view" runat="server" Text='<%# Eval("rpos_idx") %>' Visible="false" />
                                                            <asp:Label ID="lbl_pos_name_en_view" runat="server" Text='<%# Eval("pos_name_en") %>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                        <label class="col-sm-2 control-label">File Attach(User create) </label>
                                        <div class="col-sm-4">
                                            <asp:GridView ID="gvFileDocViewDetail"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                HeaderStyle-CssClass="default"
                                                AllowPaging="true"
                                                OnRowDataBound="gvRowDataBound"
                                                OnPageIndexChanging="gvPageIndexChanging"
                                                PageSize="10">
                                                <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">No result</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="File Attach">
                                                        <ItemTemplate>
                                                            <div class="col-lg-10">
                                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">File Memo(Law Officer)</label>
                                        <div class="col-sm-4">
                                            <asp:GridView ID="gvFileMemoLawOfficer"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                HeaderStyle-CssClass="default"
                                                AllowPaging="true"
                                                OnRowDataBound="gvRowDataBound"
                                                OnPageIndexChanging="gvPageIndexChanging"
                                                PageSize="10">
                                                <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">No result</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="File Attach">
                                                        <ItemTemplate>
                                                            <div class="col-lg-10">
                                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                        <label class="col-sm-2 control-label">File Memo(User)</label>
                                        <div class="col-sm-4">
                                            <asp:GridView ID="gvFileMemoUser"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                HeaderStyle-CssClass="default"
                                                AllowPaging="true"
                                                OnRowDataBound="gvRowDataBound"
                                                OnPageIndexChanging="gvPageIndexChanging"
                                                PageSize="10">
                                                <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">No result</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="File Attach">
                                                        <ItemTemplate>
                                                            <div class="col-lg-10">
                                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbBackDocument" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdBackDocument"><i class="fa fa-reply" aria-hidden="true"></i> Back</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>

                            </div>


                        </div>
                    </ItemTemplate>
                </asp:FormView>
                <!-- Form view Detail Document -->

                <!-- Log Detail Document -->
                <div id="div_LogViewDetail" runat="server" visible="false">
                    <%--<div class="panel-heading">Log Detail</div>--%>
                    <div class="panel panel-info">
                        <%-- <div class="panel-heading">
                            <h3 class="panel-title">Log Detail</h3>
                        </div>--%>

                        <div class="row">
                            <div class="row col-md-12">
                                <div class="col-md-6" style="margin-top: -10px; padding: 0px;">
                                    <img src='<%= ResolveUrl("~/images/law-trademarks/log-detail.png") %>' style="width: 100%;" class="img-fluid" alt="Log Detail" />
                                </div>
                                <div class="col-md-6"></div>
                            </div>
                        </div>


                        <div class="panel-body">
                            <div id="div_rptLogViewDetail" runat="server" style="overflow-x: auto; width: 100%;">
                                <%--<div id="div_rptLogViewDetail" runat="server" style="overflow: auto; height: 300px">--%>
                                <%--<div id="div_GvDetailOTMont" style="overflow-x: auto; width: 100%" runat="server">--%>
                                <table class="table table-bordered table-striped table-responsive col-md-12">
                                    <asp:Repeater ID="rptLogViewDetail" runat="server">
                                        <HeaderTemplate>
                                            <tr>
                                                <th>date / time</th>
                                                <th>operator</th>
                                                <th>process</th>
                                                <%--<th>ผลการดำเนินการ</th>--%>
                                                <th>comment</th>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td data-th="date / time"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                                <td data-th="operator"><%# Eval("emp_name_en") %> ( <%# Eval("current_artor") %> )</td>
                                                <td data-th="process"><%# Eval("current_decision") %></td>
                                                <td data-th="comment"><%# Eval("comment") %></td>
                                                <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                        <%-- <div class="m-t-10 m-b-10"></div>--%>
                    </div>

                    <!-- Log Detail Document -->

                </div>
        </asp:View>
        <!--View Detail-->

        <!--View Create-->
        <asp:View ID="docCreate" runat="server">
            <div class="col-md-12">

                <div class="col-md-12" id="div_fileshow" runat="server" style="color: transparent;">
                    <asp:FileUpload ID="FileShowUpload" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />
                </div>

                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <%--<div class="panel panel-default">--%>
                        <div class="panel">
                            <%-- <div class="panel-heading">--%>
                            <%--<h3 class="panel-title">User Detail</h3>--%>
                            <%--<img src='<%= ResolveUrl("~/images/law-trademarks/header-create1.png") %>' class="img-fluid" width="100%" height="100px" alt="User Detail" />--%>
                            <%-- <img src='<%= ResolveUrl("~/images/law-trademarks/header-create1.png") %>' style="width: 70%; height: 100%;" class="img-responsive img-fluid" alt="User Detail" />--%>
                            <%-- </div>--%>

                            <div class="row">
                                <div class="row col-md-12">
                                    <div class="col-md-6" style="margin-top: -10px; padding: 0px;">
                                        <%-- <asp:Literal ID="litPanelTitle" runat="server" Text="Title"></asp:Literal>--%>
                                        <img src='<%= ResolveUrl("~/images/law-trademarks/user-detail.png") %>' style="width: 100%;" class="img-fluid" alt="User Detail" />
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>


                            <div class="panel-body" style="background-color: transparent; background-image: url('images/law-trademarks/body-create.png'); background-size: 100% 100%" id="pn_bgbodyuser" runat="server">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Employee Code</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Name - Surname</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Organization</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_en") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Department</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Section</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_en") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Position</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Phone</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbemp_mobile_no" runat="server" CssClass="form-control" Text='<%# Eval("emp_mobile_no") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">E-mail</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbemp_email" runat="server" CssClass="form-control" Text='<%# Eval("emp_email") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->

                <!-- Form Create Document-->
                <asp:FormView ID="FvCreate" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <InsertItemTemplate>
                        <%--<img src='<%= ResolveUrl("~/masterpage/images/logo_mas_01.png") %>' class="img-fluid" width="70%" alt="MAS" />--%>
                        <div class="panel">
                            <%--  <div class="panel-heading">
                                <img src='<%= ResolveUrl("~/images/law-trademarks/header-create2.png") %>' style="width: 70%; height: 100%;" class="img-responsive img-fluid" alt="Create Document" />
                            </div>--%>

                            <div class="row">
                                <div class="row col-md-12">
                                    <div class="col-md-6" style="margin-top: -10px; padding: 0px;">
                                        <%-- <asp:Literal ID="litPanelTitle" runat="server" Text="Title"></asp:Literal>--%>
                                        <img src='<%= ResolveUrl("~/images/law-trademarks/header-create2.png") %>' style="width: 100%;" class="img-responsive img-fluid" alt="Create Document" />
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>

                            <div class="panel-body" style="background-color: transparent; background-image: url('images/law-trademarks/body-create.png'); background-size: 100% 100%" id="pn_bgbody" runat="server">

                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document Type</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlType" AutoPostBack="true" runat="server" ValidationGroup="SaveCreate" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Re_ddlType"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlType" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Select Document Type"
                                                ValidationGroup="SaveCreate" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1233" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlType" Width="200" PopupPosition="BottomLeft" />

                                        </div>
                                        <label class="col-sm-2 control-label">Type</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlSubType" runat="server" CssClass="form-control" ValidationGroup="SaveCreate">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Re_ddlSubType"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlSubType" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Select Type"
                                                ValidationGroup="SaveCreate" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlSubType" Width="200" PopupPosition="BottomLeft" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Country</label>
                                        <div class="col-sm-4">

                                            <div style="overflow-y: scroll; width: 100%; height: 200px">
                                                <asp:CheckBoxList ID="chkCountry" runat="server" CellPadding="5" CellSpacing="5" RepeatColumns="2" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />
                                            </div>


                                            <%--<asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control">
                                            </asp:DropDownList>--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Job Type</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlJobtype" runat="server" CssClass="form-control" ValidationGroup="SaveCreate">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Req_ddlJobtype"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlJobtype" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Select Job Type"
                                                ValidationGroup="SaveCreate" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlJobtype" Width="200" PopupPosition="BottomLeft" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Topic Document</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_topic_document" runat="server" CssClass="form-control" ValidationGroup="SaveCreate" TextMode="MultiLine" Rows="2" placeholder="Topic Document ..." Style="overflow: auto"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Req_txt_topic_document"
                                                runat="server" ControlToValidate="txt_topic_document" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Topic Document"
                                                ValidationGroup="SaveCreate" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txt_topic_document" Width="200" PopupPosition="BottomLeft" />
                                        </div>
                                        <label class="col-sm-2 control-label">Detail Document</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_detail_document" runat="server" CssClass="form-control" ValidationGroup="SaveCreate" TextMode="MultiLine" Rows="2" placeholder="Detail Document ..." Style="overflow: auto"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Req_txt_detail_document"
                                                runat="server" ControlToValidate="txt_detail_document" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Detail Document"
                                                ValidationGroup="SaveCreate" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txt_detail_document" Width="200" PopupPosition="BottomLeft" />
                                        </div>

                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel_FvCreate" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Attach File</label>
                                                <div class="col-sm-4">
                                                    <asp:FileUpload ID="UploadFile" AutoPostBack="true" ViewStateMode="Enabled" ValidationGroup="SaveCreate"
                                                        CssClass="control-label UploadFile multi max-1 accept-png|jpg|pdf maxsize-1024 with-preview" runat="server" />

                                                    <asp:RequiredFieldValidator ID="Req_UploadFile"
                                                        runat="server" ControlToValidate="UploadFile" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*Detail Document"
                                                        ValidationGroup="SaveCreate" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadFile" Width="200" PopupPosition="BottomLeft" />

                                                    <small>
                                                        <p class="help-block"><font color="red">**Attach File name jpg, png, pdf</font></p>
                                                    </small>

                                                </div>
                                                <label class="col-sm-6 control-label"></label>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">

                                                    <asp:LinkButton ID="btnSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdSave" ValidationGroup="SaveCreate">
                                                <i class="fas fa-save"></i> Save</asp:LinkButton>

                                                    <asp:LinkButton ID="btnCancelCreate" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdBackDocument">
                                                <i class="fas fa-times"></i> Cancel</asp:LinkButton>

                                                </div>
                                            </div>

                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSave" />
                                        </Triggers>

                                    </asp:UpdatePanel>

                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form Create Document-->

            </div>
        </asp:View>
        <!--View Create-->

        <!-- View Wait Approve -->
        <asp:View ID="docWaitApprove" runat="server">
            <div class="col-md-12">

                <%--<div id="div_gvWaitApprove" runat="server" style="overflow: auto">--%>
                <div id="div_gvWaitApprove" runat="server" style="overflow-x: auto; width: 100%;">
                    <asp:GridView ID="gvWaitApprove" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                        HeaderStyle-CssClass="warning"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        AllowPaging="True"
                        PageSize="10"
                        DataKeyNames="u0_doc_idx">
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- No Data ---</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="Document No." ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lblu0_doc_idx_wait" runat="server" Visible="false" Text='<%#Eval("u0_doc_idx") %>'></asp:Label>
                                    <asp:Label ID="lbldocrequest_code_wait" runat="server" Text='<%#Eval("docrequest_code") %>'></asp:Label>
                                    <%--<asp:Label ID="lblDocIDX" runat="server" Visible="false" Text='<%# Eval("u0_qalab_idx") %>'></asp:Label>
                                <asp:Label ID="lblCemp_idx_per" runat="server" Visible="false" Text='<%# Eval("cemp_idx") %>'></asp:Label>
                                <asp:Label ID="lblplaceIDX" runat="server" Visible="false" Text='<%# Eval("place_idx") %>'></asp:Label>
                               
                                <asp:Label ID="lblDocCode" runat="server" Text='<%# Eval("document_code") %>'></asp:Label>--%>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Create Date" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>

                                    <asp:Label ID="lblcreate_date_wait" runat="server" Text='<%#Eval("create_date") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Document Detail" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>

                                    <p>
                                        <b>Document Type :</b>
                                        <asp:Label ID="lbltype_name_en_wait" runat="server" Text='<%# Eval("type_name_en") %>' />
                                    </p>
                                    <p>
                                        <b>Sub Document Type :</b>
                                        <asp:Label ID="lblsubtype_name_en_wait" runat="server" Text='<%# Eval("subtype_name_en") %>' />
                                    </p>
                                    <p>
                                        <b>Job Type :</b>
                                        <asp:Label ID="lbljobtype_name_en_wait" runat="server" Text='<%# Eval("jobtype_name_en") %>'></asp:Label>
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Document Status" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="lblcurrent_status_wait" runat="server" Text='<%# Eval("current_status") %>'></asp:Label>
                                    <%--<asp:Label ID="lblDocStatus" Visible="false" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>--%>
                                    <asp:Label ID="lblstaidx_wait" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                    <asp:Label ID="lblnoidx_wait" runat="server" Visible="false" Text='<%# Eval("noidx") %>'></asp:Label>
                                    <asp:Label ID="lblacidx_wait" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <asp:UpdatePanel ID="Update_ViewWaitDetail" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="btnViewWaitDetail" CssClass="btn-sm btn-info" runat="server" CommandName="cmdViewWaitDetail" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("noidx") + ";" + Eval("acidx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx") %>' data-toggle="tooltip" title="View"><i class="fa fa-file"></i></asp:LinkButton>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnViewWaitDetail" />
                                        </Triggers>

                                    </asp:UpdatePanel>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>

                <!-- Form Detail Employee Wait Approve  -->
                <asp:FormView ID="fvEmpViewDetailApprove" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <%--<div class="panel-heading">
                                <h3 class="panel-title">User Detail</h3>
                            </div>--%>

                            <div class="row">
                                <div class="row col-md-12">
                                    <div class="col-md-6" style="margin-top: -10px; padding: 0px;">
                                        <img src='<%= ResolveUrl("~/images/law-trademarks/user-detail.png") %>' style="width: 100%;" class="img-fluid" alt="User Detail" />
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Employee Code</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Name - Surname</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Organization</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_en") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Department</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Section</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_en") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Position</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Phone</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbemp_mobile_no" runat="server" CssClass="form-control" Text='<%# Eval("emp_mobile_no") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">E-mail</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbemp_email" runat="server" CssClass="form-control" Text='<%# Eval("emp_email") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Wait Approve -->

                <!-- Form view waitapprove Detail Document -->
                <asp:FormView ID="fvWaitApproveDocumentDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfACIDX" runat="server" Value='<%# Eval("acidx") %>' />
                        <asp:HiddenField ID="hfNOIDX" runat="server" Value='<%# Eval("noidx") %>' />
                        <asp:HiddenField ID="hfSTAIDX" runat="server" Value='<%# Eval("staidx") %>' />
                        <div class="panel panel-default">
                            <%--<div class="panel-heading">
                                <h3 class="panel-title">Document Detail</h3>
                            </div>--%>

                            <div class="row">
                                <div class="row col-md-12">
                                    <div class="col-md-6" style="margin-top: -10px; padding: 0px;">
                                        <img src='<%= ResolveUrl("~/images/law-trademarks/document-detail.png") %>' style="width: 100%;" class="img-fluid" alt="User Detail" />
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>


                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document Request Code</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbdocrequest_code_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Create Date</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbcreate_date_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("create_date") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document Type</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbtype_name_en_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Type</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbsubtype_name_en_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Country</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />--%>

                                            <div style="overflow-y: scroll; width: 100%; height: 200px">
                                                <asp:CheckBoxList ID="chkCountryview" runat="server" Enabled="false" CellPadding="5" CellSpacing="5" RepeatColumns="2" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" />
                                            </div>

                                        </div>
                                        <label class="col-sm-2 control-label">Job Type</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_jobtype_name_en_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("jobtype_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Topic Document</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbtopic_name_viewdetail" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" CssClass="form-control" Text='<%# Eval("topic_name") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Detail Document</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_doc_detail_viewdetail" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" CssClass="form-control" Text='<%# Eval("doc_detail") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Detail Law Officer</label>
                                        <div class="col-sm-4">

                                            <asp:GridView ID="gvDetailLawOfficer"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                HeaderStyle-CssClass="default"
                                                AllowPaging="true"
                                                OnRowDataBound="gvRowDataBound"
                                                OnPageIndexChanging="gvPageIndexChanging"
                                                PageSize="10">
                                                <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">No result</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Name - LastName" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblu3_doc_idx_viewapprove" runat="server" Text='<%# Eval("u3_doc_idx") %>' Visible="false" />
                                                            <asp:Label ID="lbl_emp_name_en_viewapprove" runat="server" Text='<%# Eval("emp_name_en") %>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Position(EN)" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblrpos_idx_viewapprove" runat="server" Text='<%# Eval("rpos_idx") %>' Visible="false" />
                                                            <asp:Label ID="lbl_pos_name_en_viewapprove" runat="server" Text='<%# Eval("pos_name_en") %>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                        <label class="col-sm-2 control-label">File Attach(User create) </label>
                                        <div class="col-sm-4">
                                            <asp:GridView ID="gvFileDocViewDetail"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                HeaderStyle-CssClass="default"
                                                AllowPaging="true"
                                                OnRowDataBound="gvRowDataBound"
                                                OnPageIndexChanging="gvPageIndexChanging"
                                                PageSize="10">
                                                <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">No result</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="File Attach">
                                                        <ItemTemplate>
                                                            <div class="col-lg-10">
                                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">File Memo(Law Officer)</label>
                                        <div class="col-sm-4">
                                            <asp:GridView ID="gvFileMemoLawOfficer"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                HeaderStyle-CssClass="default"
                                                AllowPaging="true"
                                                OnRowDataBound="gvRowDataBound"
                                                OnPageIndexChanging="gvPageIndexChanging"
                                                PageSize="10">
                                                <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">No result</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="File Attach">
                                                        <ItemTemplate>
                                                            <div class="col-lg-10">
                                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                        <label class="col-sm-2 control-label">File Memo(User)</label>
                                        <div class="col-sm-4">
                                            <asp:GridView ID="gvFileMemoUser"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                HeaderStyle-CssClass="default"
                                                AllowPaging="true"
                                                OnRowDataBound="gvRowDataBound"
                                                OnPageIndexChanging="gvPageIndexChanging"
                                                PageSize="10">
                                                <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">No result</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="File Attach">
                                                        <ItemTemplate>
                                                            <div class="col-lg-10">
                                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbBackDocWaitApprove" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdBackDocWaitApprove"><i class="fa fa-reply" aria-hidden="true"></i> Back</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </ItemTemplate>
                    <EditItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfACIDX" runat="server" Value='<%# Eval("acidx") %>' />
                        <asp:HiddenField ID="hfNOIDX" runat="server" Value='<%# Eval("noidx") %>' />
                        <asp:HiddenField ID="hfSTAIDX" runat="server" Value='<%# Eval("staidx") %>' />


                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Edit Document by User</h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document Request Code</label>
                                        <div class="col-sm-4">
                                            <asp:Label ID="lbldecision_idx" runat="server" Visible="false" />
                                            <asp:TextBox ID="tbdocrequest_code_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Create Date</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbcreate_date_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("create_date") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document Type</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbtype_name_en_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />


                                        </div>
                                        <label class="col-sm-2 control-label">Type</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbsubtype_name_en_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Country</label>
                                        <div class="col-sm-4">
                                            <div style="overflow-y: scroll; width: 100%; height: 200px">
                                                <asp:CheckBoxList ID="chkCountry" runat="server" CellPadding="5" Enabled="false" CellSpacing="5" RepeatColumns="2" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" Style="overflow: auto;" />
                                            </div>
                                            <%--<asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control">
                                            </asp:DropDownList>--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Job Type</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_jobtype_name_en_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("jobtype_name_en") %>' Enabled="false" />

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Topic Document</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_topic_document" runat="server" CssClass="form-control" ValidationGroup="SaveCreate" TextMode="MultiLine" Rows="2" placeholder="Topic Document ..." Style="overflow: auto" Text='<%# Eval("topic_name") %>'></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Req_txt_topic_document"
                                                runat="server" ControlToValidate="txt_topic_document" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Topic Document"
                                                ValidationGroup="SaveCreate" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txt_topic_document" Width="200" PopupPosition="BottomLeft" />
                                        </div>
                                        <label class="col-sm-2 control-label">Detail Document</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_detail_document" runat="server" CssClass="form-control" ValidationGroup="SaveCreate" TextMode="MultiLine" Rows="2" placeholder="Detail Document ..." Style="overflow: auto" Text='<%# Eval("doc_detail") %>'></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Req_txt_detail_document"
                                                runat="server" ControlToValidate="txt_detail_document" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*Detail Document"
                                                ValidationGroup="SaveCreate" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txt_detail_document" Width="200" PopupPosition="BottomLeft" />
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">File Attach(User create) </label>
                                        <div class="col-sm-4">
                                            <asp:GridView ID="gvFileDocViewDetail"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                HeaderStyle-CssClass="default"
                                                AllowPaging="true"
                                                OnRowDataBound="gvRowDataBound"
                                                OnPageIndexChanging="gvPageIndexChanging"
                                                PageSize="10">
                                                <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">No result</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="File Attach">
                                                        <ItemTemplate>
                                                            <div class="col-lg-10">
                                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                        <label class="col-sm-6 control-label"></label>

                                    </div>

                                    <asp:UpdatePanel ID="Panel_FvEditDocument" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Attach Edit File</label>
                                                <div class="col-sm-4">
                                                    <asp:FileUpload ID="UploadFileEditDocument" AutoPostBack="true" ViewStateMode="Enabled" ValidationGroup="SaveCreate"
                                                        CssClass="control-label UploadFileEditDocument multi max-2 accept-png|jpg|pdf maxsize-1024 with-preview" runat="server" />

                                                    <%--   <asp:RequiredFieldValidator ID="Req_UploadFile"
                                                        runat="server" ControlToValidate="UploadFile" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*Detail Document"
                                                        ValidationGroup="SaveCreate" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadFile" Width="200" PopupPosition="BottomLeft" />--%>

                                                    <small>
                                                        <p class="help-block"><font color="red">**Attach File name jpg, png, pdf</font></p>
                                                    </small>

                                                </div>
                                                <label class="col-sm-6 control-label"></label>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">

                                                    <asp:LinkButton ID="btnSaveEditDocument" CssClass="btn btn-warning" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdSaveEditDocument" ValidationGroup="SaveCreate">
                                                <i class="fa fa-edit"></i> Edit</asp:LinkButton>

                                                    <asp:LinkButton ID="btnCancelEditDocument" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdBackDocWaitApprove">
                                                <i class="fas fa-times"></i> Cancel</asp:LinkButton>

                                                </div>
                                            </div>

                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSaveEditDocument" />
                                        </Triggers>

                                    </asp:UpdatePanel>

                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view waitapprove Detail Document -->

                <asp:FormView ID="fvDirectorUser" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Approve By Director User</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlDirectorUserApprove" runat="server" CssClass="form-control" ValidationGroup="SaveDirectorUserApprove" />
                                            <asp:RequiredFieldValidator ID="Req_ddlDirectorUserApprove" ValidationGroup="SaveDirectorUserApprove" runat="server" Display="None"
                                                ControlToValidate="ddlDirectorUserApprove" Font-Size="11"
                                                ErrorMessage="*Please Select Status"
                                                ValidationExpression="*Please Select Status" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlDirectorUserApprove" Width="200" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_comment_DirectorUserApprove" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSaveDirectorUserApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveDirectorUserApprove"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancelDirectorUserApprove" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <asp:FormView ID="fvMgLaw" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Approve By Manager Law</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlMgLawApprove" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" ValidationGroup="SaveddlMgLawApproveApprove" />
                                            <asp:RequiredFieldValidator ID="Req_ddlMgLawApprove" ValidationGroup="SaveddlMgLawApproveApprove" runat="server" Display="None"
                                                ControlToValidate="ddlMgLawApprove" Font-Size="11"
                                                ErrorMessage="*Please Select Status"
                                                ValidationExpression="*Please Select Status" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlMgLawApprove" Width="200" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_comment_MgLawApprove" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <asp:UpdatePanel ID="_Panel_selectofficer_law" runat="server" Visible="false">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Select Law Officer</label>
                                                <div class="col-sm-6">
                                                    <asp:CheckBoxList ID="chkLawOfficer" runat="server" CellPadding="5" CellSpacing="5" RepeatColumns="3" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" Style="overflow: auto;" />

                                                    <%-- <asp:TextBox ID="TextBox1" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />--%>
                                                </div>
                                                <label class="col-sm-4"></label>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSaveApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveddlMgLawApproveApprove"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <asp:FormView ID="fvLaw" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Approve By Law Officer</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlLawApprove" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" ValidationGroup="SaveddlLawApprove" />
                                            <asp:RequiredFieldValidator ID="Req_ddlLawApprove" ValidationGroup="SaveddlLawApprove" runat="server" Display="None"
                                                ControlToValidate="ddlLawApprove" Font-Size="11"
                                                ErrorMessage="*Please Select Status"
                                                ValidationExpression="*Please Select Status" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1666" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlLawApprove" Width="200" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_comment_LawApprove" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <asp:UpdatePanel ID="_Panel_File_memo" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="div_lawfilememo" runat="server" visible="false">
                                                <label class="col-sm-2 control-label">Attrach File memo</label>
                                                <div class="col-sm-6">
                                                    <asp:FileUpload ID="UploadFileMemo" CssClass="control-label UploadFileMemo multi max-2 accept-png|jpg|pdf maxsize-1024 with-preview" ValidationGroup="SaveddlLawApprove" runat="server" />

                                                    <asp:RequiredFieldValidator ID="Req_UploadFileMemo" ValidationGroup="SaveddlLawApprove" runat="server" Display="None"
                                                        ControlToValidate="UploadFileMemo" Font-Size="11"
                                                        ErrorMessage="*Please Attrach File memo"
                                                        ValidationExpression="*Please Attrach File memo" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadFileMemo" Width="200" PopupPosition="BottomLeft" />

                                                </div>
                                                <label class="col-sm-4"></label>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <asp:LinkButton ID="lbDocSaveApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveddlLawApprove"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                                    <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                                </div>
                                            </div>
                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbDocSaveApprove" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                        </div>

                    </InsertItemTemplate>
                </asp:FormView>

                <asp:FormView ID="fvMgLawMemo" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Check Memo By Manager Law</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlMgLawApproveMemo" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" ValidationGroup="SaveddlMgLawApproveMemo" />
                                            <asp:RequiredFieldValidator ID="Req_ddlMgLawApproveMemo" ValidationGroup="SaveddlMgLawApproveMemo" runat="server" Display="None"
                                                ControlToValidate="ddlMgLawApproveMemo" Font-Size="11"
                                                ErrorMessage="*Please Select Status"
                                                ValidationExpression="*Please Select Status" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlMgLawApproveMemo" Width="200" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_comment_MgLawApproveMemo" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <%--   <asp:UpdatePanel ID="_Panel_File_memo" runat="server" Visible="false">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">File memo</label>
                                                <div class="col-sm-6">
                                                    <asp:FileUpload ID="UploadFileMemo" CssClass="control-label UploadFileMemo multi max-1 accept-png|jpg|pdf maxsize-1024 with-preview" ValidationGroup="SaveddlLawApprove" runat="server" />

                                                </div>
                                                <label class="col-sm-4"></label>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>--%>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSaveApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveddlMgLawApproveMemo"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <asp:FormView ID="fvUserCreateFileMemo" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Attach File Memo by User</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">

                                            <asp:Label ID="lbldecision_idx" runat="server" Visible="false" />
                                            <asp:TextBox ID="txt_comment_UserCreateFileMemo" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <asp:UpdatePanel ID="Panel_UserCreateFileMemo" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Attach File memo</label>
                                                <div class="col-sm-6">
                                                    <asp:FileUpload ID="UploadUserCreateFileMemo" CssClass="control-label UploadUserCreateFileMemo multi max-2 accept-png|jpg|pdf maxsize-1024 with-preview" ValidationGroup="SaveUserCreateFileMemo" runat="server" />

                                                    <asp:RequiredFieldValidator ID="Req_UploadUserCreateFileMemo" ValidationGroup="SaveUserCreateFileMemo" runat="server" Display="None"
                                                        ControlToValidate="UploadUserCreateFileMemo" Font-Size="11"
                                                        ErrorMessage="*Please Attach File memo"
                                                        ValidationExpression="*Please Attrach File memo" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7222" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadUserCreateFileMemo" Width="200" PopupPosition="BottomLeft" />

                                                </div>
                                                <label class="col-sm-4"></label>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <asp:LinkButton ID="lbDocSaveUserCreateFileMemo" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveUserCreateFileMemo"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                                    <asp:LinkButton ID="lbDocCancelUserCreateFileMemo" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbDocSaveUserCreateFileMemo" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <asp:FormView ID="fvLawOfficerEditFileMemo" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Attach File Memo by Law Officer</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">

                                            <asp:Label ID="lbldecision_idx" runat="server" Visible="false" />
                                            <asp:TextBox ID="txt_comment_LawOfficerEditFileMemo" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <asp:UpdatePanel ID="Panel_LawOfficerEditFileMemo" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Attach File memo</label>
                                                <div class="col-sm-6">
                                                    <asp:FileUpload ID="UploadLawOfficerEditFileMemo" CssClass="control-label UploadLawOfficerEditFileMemo multi max-2 accept-png|jpg|pdf maxsize-1024 with-preview" ValidationGroup="SaveLawOfficerEditFileMemo" runat="server" />

                                                    <asp:RequiredFieldValidator ID="Req_UploadLawOfficerEditFileMemo" ValidationGroup="SaveLawOfficerEditFileMemo" runat="server" Display="None"
                                                        ControlToValidate="UploadLawOfficerEditFileMemo" Font-Size="11"
                                                        ErrorMessage="*Please Attach File memo"
                                                        ValidationExpression="*Please Attrach File memo" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender32" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadLawOfficerEditFileMemo" Width="200" PopupPosition="BottomLeft" />

                                                </div>
                                                <label class="col-sm-4"></label>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <asp:LinkButton ID="lbDocSaveLawOfficerEditFileMemo" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveLawOfficerEditFileMemo"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                                    <asp:LinkButton ID="lbDocCancelSaveLawOfficerEditFileMemo" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbDocSaveLawOfficerEditFileMemo" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <asp:FormView ID="fvDirectorUserCheckMemo" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Check Memo By Director User</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlDirectorUserCheckMemo" runat="server" CssClass="form-control" ValidationGroup="SaveDirectorUserCheckMemo" />
                                            <asp:RequiredFieldValidator ID="Req_ddlDirectorUserCheckMemo" ValidationGroup="SaveDirectorUserCheckMemo" runat="server" Display="None"
                                                ControlToValidate="ddlDirectorUserCheckMemo" Font-Size="11"
                                                ErrorMessage="*Please Select Status"
                                                ValidationExpression="*Please Select Status" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlDirectorUserCheckMemo" Width="200" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_comment_DirectorUserCheckMemo" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSaveApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveDirectorUserCheckMemo"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <asp:FormView ID="fvLawOfficerCheckMemo" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Check Memo By Law Officer</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlLawOfficerCheckMemo" runat="server" CssClass="form-control" ValidationGroup="SaveLawOfficerCheckMemo" />
                                            <asp:RequiredFieldValidator ID="Req_ddlLawOfficerCheckMemo" ValidationGroup="SaveLawOfficerCheckMemo" runat="server" Display="None"
                                                ControlToValidate="ddlLawOfficerCheckMemo" Font-Size="11"
                                                ErrorMessage="*Please Select Status"
                                                ValidationExpression="*Please Select Status" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlLawOfficerCheckMemo" Width="200" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_comment_LawOfficerCheckMemo" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSaveApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveLawOfficerCheckMemo"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <!-- Log Detail WaitApprove Document Style="overflow: auto" -->
                <div id="div_LogViewDetail_WaitApprove" runat="server" visible="false">
                    <div class="panel panel-info">
                        <%-- <div class="panel-heading">
                            <h3 class="panel-title">Log Detail</h3>
                        </div>--%>
                        <div class="row">
                            <div class="row col-md-12">
                                <div class="col-md-6" style="margin-top: -10px; padding: 0px;">
                                    <img src='<%= ResolveUrl("~/images/law-trademarks/log-detail.png") %>' style="width: 100%;" class="img-fluid" alt="User Detail" />
                                </div>
                                <div class="col-md-6"></div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <%--<div id="div_rptLogWaitApprove" runat="server" style="overflow: auto; height: 300px">--%>
                            <div id="div_rptLogWaitApprove" runat="server" style="overflow-x: auto; width: 100%;">
                                <table class="table table-bordered table-striped table-responsive col-md-12">
                                    <asp:Repeater ID="rptLogWaitApprove" runat="server">
                                        <HeaderTemplate>
                                            <tr>
                                                <th>date / time</th>
                                                <th>operator</th>
                                                <th>process</th>
                                                <%--<th>ผลการดำเนินการ</th>--%>
                                                <th>comment</th>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td data-th="date / time"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                                <td data-th="operator"><%# Eval("emp_name_en") %> ( <%# Eval("current_artor") %> )</td>
                                                <td data-th="process"><%# Eval("current_decision") %></td>
                                                <td data-th="comment"><%# Eval("comment") %></td>
                                                <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                            <%-- <div class="m-t-10 m-b-10"></div>--%>
                        </div>
                    </div>
                </div>
                <!-- Log Detail WaitApprove Document -->

                <div class="col-md-12" id="div1" runat="server" style="color: transparent;">
                    <asp:FileUpload ID="FileUpload1" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />
                </div>

            </div>
        </asp:View>
        <!-- View Wait Approve -->

        <!-- View Registration Record Sheet -->
        <asp:View ID="docRegistration" runat="server">
            <div class="col-md-12">

                <div id="div_gvRecordSheet" runat="server" style="overflow: auto">
                    <asp:GridView ID="gvRecordSheet" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                        HeaderStyle-CssClass="success"
                        OnPageIndexChanging="gvPageIndexChanging"
                        AllowPaging="True"
                        PageSize="10"
                        OnRowDataBound="gvRowDataBound">
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- No Data ---</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="Document Code" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lblu0_doc_idx_record" runat="server" Text='<%#Eval("u0_doc_idx") %>'></asp:Label>
                                    <asp:Label ID="lbl_u1_doc_idx_record" runat="server" Text='<%#Eval("u1_doc_idx") %>'></asp:Label>
                                    <%--<asp:Label ID="lblDocIDX" runat="server" Visible="false" Text='<%# Eval("u0_qalab_idx") %>'></asp:Label>
                                <asp:Label ID="lblCemp_idx_per" runat="server" Visible="false" Text='<%# Eval("cemp_idx") %>'></asp:Label>
                                <asp:Label ID="lblplaceIDX" runat="server" Visible="false" Text='<%# Eval("place_idx") %>'></asp:Label>
                               
                                <asp:Label ID="lblDocCode" runat="server" Text='<%# Eval("document_code") %>'></asp:Label>--%>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Create Date" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>

                                    <asp:Label ID="lblcreate_date_record" runat="server" Text='<%#Eval("create_date") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Country" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>

                                    <asp:Label ID="lblcountry_name_en_record" runat="server" Text='<%#Eval("country_name_en") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Document Detail" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>

                                    <p>
                                        <b>Document Type :</b>
                                        <asp:Label ID="lbltype_name_en_record" runat="server" Text='<%# Eval("type_name_en") %>' />
                                    </p>
                                    <p>
                                        <b>Sub Document Type :</b>
                                        <asp:Label ID="lblsubtype_name_en_record" runat="server" Text='<%# Eval("subtype_name_en") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Document Status" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="lblcurrent_status_wait" runat="server" Text='<%# Eval("current_status") %>'></asp:Label>
                                    <%--<asp:Label ID="lblDocStatus" Visible="false" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>--%>
                                    <asp:Label ID="lblstaidx_wait" runat="server" Visible="true" Text='<%# Eval("staidx") %>'></asp:Label>
                                    <asp:Label ID="lblnoidx_wait" runat="server" Visible="true" Text='<%# Eval("noidx") %>'></asp:Label>
                                    <asp:Label ID="lblacidx_wait" runat="server" Visible="true" Text='<%# Eval("acidx") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>

                                    <asp:UpdatePanel ID="Panel_btnRecordSheet" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="btnEditRecordSheet" CssClass="btn btn-warning btn-sm" runat="server" CommandName="cmdEditRecordSheet" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("u1_doc_idx") + ";" + Eval("noidx") + ";" + Eval("acidx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx") + ";" + Eval("type_idx") %>' data-toggle="tooltip" title="Edit Record"><i class="fa fa-edit"></i></asp:LinkButton>

                                            <asp:LinkButton ID="btnSaveRecordSheet" CssClass="btn btn-success btn-sm" runat="server" CommandName="cmdSaveRecordSheet" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("u1_doc_idx") + ";" + Eval("noidx") + ";" + Eval("acidx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx") + ";" + Eval("type_idx") %>' data-toggle="tooltip" title="Save Record"><i class="fa fa-save"></i></asp:LinkButton>

                                            <asp:LinkButton ID="btnViewRecordSheet" CssClass="btn btn-info btn-sm" runat="server" CommandName="cmdViewRecordSheet" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("u1_doc_idx") + ";" + Eval("noidx") + ";" + Eval("acidx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx") + ";" + Eval("type_idx") %>' data-toggle="tooltip" title="View"><i class="fa fa-file"></i></asp:LinkButton>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>

                <asp:UpdatePanel ID="Panel_HeaderRecordSheet" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <center><h3 class="panel-title">Record Sheet</h3></center>
                            </div>
                        </div>

                    </ContentTemplate>

                </asp:UpdatePanel>

                <!-- Form view Registration Detail -->
                <asp:FormView ID="fvRegistrationDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <asp:HiddenField ID="hfACIDX" runat="server" Value='<%# Eval("acidx") %>' />
                        <asp:HiddenField ID="hfNOIDX" runat="server" Value='<%# Eval("noidx") %>' />
                        <asp:HiddenField ID="hfSTAIDX" runat="server" Value='<%# Eval("staidx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Registration Details</h3>
                            </div>

                            <div class="panel-body">

                                <div id="div_Registration" runat="server" style="overflow: auto;">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Document Request Code</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">Create Date</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Document Type</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbdocrequest_code_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />
                                            </div>
                                            <label class="col-sm-2 control-label">Sub Document Type</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbcreate_date_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Owner</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />--%>
                                            </div>
                                            <label class="col-sm-2 control-label">Owner Address</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Incorporated in</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />--%>
                                            </div>
                                            <%--<label class="col-sm-2 control-label">Owner Address</label>--%>
                                            <div class="col-sm-6">
                                                <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                            </div>
                                        </div>

                                        <hr />
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Address Of Record</h3>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Country</label>
                                            <div class="col-sm-4">
                                                <div style="overflow-y: scroll; width: 100%; height: 200px">
                                                    <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />--%>
                                                    <asp:CheckBoxList ID="chkCountryview" runat="server" Enabled="false" CellPadding="5" CellSpacing="5" RepeatColumns="2" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" Style="overflow: auto;" />

                                                </div>
                                            </div>
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-4">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Application No</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />--%>
                                            </div>
                                            <label class="col-sm-2 control-label">Application Date</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Registration No</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />--%>
                                            </div>
                                            <label class="col-sm-2 control-label">Registration Date</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Trademark Status</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />--%>
                                            </div>
                                            <label class="col-sm-2 control-label">Trademark Sub Status</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">File Reference</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />--%>
                                            </div>
                                            <label class="col-sm-2 control-label">Record Reference</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Next Renewal Due</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />--%>
                                            </div>
                                            <label class="col-sm-2 control-label">Type Of Registrtion</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Supervisors</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />--%>
                                            </div>
                                            <label class="col-sm-2 control-label">Supervisors</label>
                                            <div class="col-sm-4">
                                                <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </div>

                        </div>
                    </ItemTemplate>
                    <InsertItemTemplate>

                        <div class="panel panel-default">

                            <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                            <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                            <asp:HiddenField ID="hfACIDX" runat="server" Value='<%# Eval("acidx") %>' />
                            <asp:HiddenField ID="hfNOIDX" runat="server" Value='<%# Eval("noidx") %>' />
                            <asp:HiddenField ID="hfSTAIDX" runat="server" Value='<%# Eval("staidx") %>' />

                            <div class="panel-heading">
                                <h3 class="panel-title">Registration Details</h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document Request Code</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Create Date</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document Type</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbtype_name_en" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Sub Document Type</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbsubtype_name_en" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Owner</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbowner_idx" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("owner_idx") %>' Enabled="false" />--%>
                                            <asp:DropDownList ID="ddlowner_idx" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-sm-2 control-label">Owner Address</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbowner_address_en" runat="server" placeholder="Owner Address ..." TextMode="MultiLine" Rows="2" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Incorporated in</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbincorporated_in_idx" Visible="false" runat="server" CssClass="form-control" Enabled="false" />
                                            <asp:DropDownList ID="ddlincorporated_in_idx" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <%--<label class="col-sm-2 control-label">Owner Address</label>--%>
                                        <div class="col-sm-6">
                                            <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <hr />
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Address Of Record</h3>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Country</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbcountry_name_en" runat="server" CssClass="form-control" Enabled="false" />


                                        </div>
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-4">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Application No</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_application_no" runat="server" placeholder="Application No ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Application Date</label>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txt_application_date" runat="server" placeholder="Application Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Registration No</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_registration_no" runat="server" placeholder="Registration No ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Registration Date</label>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txt_registration_date" runat="server" placeholder="Registration Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />

                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Trademark Status</label>
                                        <div class="col-sm-4">

                                            <asp:DropDownList ID="ddlTrademarkStatus" AutoPostBack="true" placeholder="Trademark Status ..." runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-sm-2 control-label">Trademark Sub Status</label>
                                        <div class="col-sm-4">

                                            <asp:DropDownList ID="ddlTrademarkSubStatus" AutoPostBack="true" runat="server" placeholder="Trademark Sub Status ..." ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">File Reference</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_File_Reference" runat="server" placeholder="File Reference ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Record Reference</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_Record_Reference" runat="server" placeholder="Record Reference ..." CssClass="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Next Renewal Due</label>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="tb_Next_Renewal_Due" runat="server" placeholder="Next Renewal Due ..." CssClass="form-control from-date-datepicker cursor-pointer" />

                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">Type Of Registrtion</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlTypeOfRegistrtion" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Supervisors</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbemp_name_en_head" runat="server" Enabled="false" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Supervisors</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbemp_name_en_mgemp" runat="server" Enabled="false" CssClass="form-control" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>
                    <EditItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <asp:HiddenField ID="hfACIDX" runat="server" Value='<%# Eval("acidx") %>' />
                        <asp:HiddenField ID="hfNOIDX" runat="server" Value='<%# Eval("noidx") %>' />
                        <asp:HiddenField ID="hfSTAIDX" runat="server" Value='<%# Eval("staidx") %>' />

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Registration Details</h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document Request Code</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Create Date</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Document Type</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbdocrequest_code_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("type_name_en") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">Sub Document Type</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbcreate_date_viewdetail" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Owner</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbowner_idx" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("owner_idx") %>' Enabled="false" />
                                            <asp:DropDownList ID="ddlowner_idx" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-sm-2 control-label">Owner Address</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbowner_address_en" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control" Text='<%# Eval("owner_address_en") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Incorporated in</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbincorporated_in_idx" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("incorporated_in_idx") %>' Enabled="false" />
                                            <asp:DropDownList ID="ddlincorporated_in_idx" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <%--<label class="col-sm-2 control-label">Owner Address</label>--%>
                                        <div class="col-sm-6">
                                            <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <hr />
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Address Of Record</h3>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Country</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbcountry_name_en" runat="server" CssClass="form-control" Text='<%# Eval("country_name_en") %>' Enabled="false" />


                                        </div>
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-4">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Application No</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_application_no" runat="server" CssClass="form-control" Text='<%# Eval("application_no") %>' />
                                        </div>
                                        <label class="col-sm-2 control-label">Application Date</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_application_date" runat="server" CssClass="form-control" Text='<%# Eval("application_date") %>' />
                                            <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text='<%# Eval("subtype_name_en") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Registration No</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_registration_no" runat="server" CssClass="form-control" Text='<%# Eval("registration_no") %>' />
                                        </div>
                                        <label class="col-sm-2 control-label">Registration Date</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_registration_date" runat="server" CssClass="form-control" Text='<%# Eval("registration_date") %>' />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Trademark Status</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="TextBox7" runat="server" CssClass="form-control" Text='<%# Eval("owner_idx") %>' Enabled="false" />
                                            <asp:DropDownList ID="DropDownList1" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-sm-2 control-label">Trademark Sub Status</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="TextBox8" runat="server" CssClass="form-control" Text='<%# Eval("owner_idx") %>' Enabled="false" />
                                            <asp:DropDownList ID="DropDownList2" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">File Reference</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="TextBox6" runat="server" CssClass="form-control" Text='<%# Eval("application_no") %>' />
                                        </div>
                                        <label class="col-sm-2 control-label">Record Reference</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="TextBox5" runat="server" CssClass="form-control" Text='<%# Eval("application_no") %>' />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Next Renewal Due</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control" Text='<%# Eval("application_no") %>' />
                                        </div>
                                        <label class="col-sm-2 control-label">Type Of Registrtion</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="TextBox4" runat="server" CssClass="form-control" Text='<%# Eval("application_no") %>' />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Supervisors</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbemp_name_en_head" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_name_en_head") %>' />
                                        </div>
                                        <label class="col-sm-2 control-label">Supervisors</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbemp_name_en_mgemp" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_name_en_mgemp") %>' />
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view Registration Detail -->

                <!-- Form view Publication Detail -->
                <asp:FormView ID="fvPublicationDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Publication Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Publication Date</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Journal Volume</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Pub. Reg. Date</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Registry Reference</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Final Office Action Date</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Office Action Date</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Date Declaration Filed</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </ItemTemplate>
                    <InsertItemTemplate>

                        <div class="panel panel-default">
                            <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                            <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                            <div class="panel-heading">
                                <h3 class="panel-title">Publication Details</h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Publication Date</label>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="tbPublication_Date" runat="server" placeholder="Publication Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="tbGrant_date" runat="server" placeholder="Notice of Allowance/Grant date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Journal Volume</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbJournal_Volume" runat="server" placeholder="Journal Volume ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Pub. Reg. Date</label>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="tb_Pub_Date" runat="server" placeholder="Pub. Reg. Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Registry Reference</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_Registry_Reference" runat="server" placeholder="Registry Reference ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Final Office Action Date</label>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="tbFinal_Office_Date" runat="server" placeholder="Final Office Action Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Office Action Date</label>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="tb_Office_Action_Date" runat="server" placeholder="Office Action Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">Date Declaration Filed</label>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="tb_Date_Declaration_Filed" runat="server" placeholder="Date Declaration Filed ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <%--<div class="form-group">
                                                        <label class="col-sm-2 control-label"></label>
                                                        <div class="col-sm-2">
                                                            <asp:LinkButton ID="btnAddPublicationTolist" runat="server" CssClass="btn btn-primary col-md-12" Text="AddPublication" OnCommand="btnCommand"
                                                                CommandName="cmdAddPublicationList" ValidationGroup="addPublicationList" />
                                                        </div>
                                                        <label class="col-sm-8 control-label"></label>

                                                    </div>--%>

                                    <%--  <div class="form-group">
                                                        <label class="col-sm-2 control-label"></label>
                                                        <div class="col-sm-10">

                                                            <asp:GridView ID="gvPublicationList"
                                                                runat="server"
                                                                CssClass="table table-striped table-responsive"
                                                                GridLines="None"
                                                                OnRowCommand="onRowCommand"
                                                                AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                        HeaderStyle-CssClass="text-center"
                                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                        <ItemTemplate>
                                                                            <%# (Container.DataItemIndex + 1) %>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:BoundField DataField="drPlacePerText" HeaderText="สถานที่" ItemStyle-CssClass="text-center"
                                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                        HeaderStyle-CssClass="text-center">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="btnRemovePlacePer" runat="server"
                                                                                CssClass="btn btn-danger btn-xs"
                                                                                OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                                CommandName="btnRemovePlacePer"><i class="fa fa-times"></i>
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>


                                                        </div>

                                                    </div>--%>
                                </div>

                            </div>


                        </div>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Publication Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Publication Date</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Journal Volume</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Pub. Reg. Date</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Registry Reference</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Final Office Action Date</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Office Action Date</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Date Declaration Filed</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view Publication Detail -->

                <!-- Form view Dependent Registration Detail -->
                <asp:FormView ID="fvDependentDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Dependent Registration</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Dependent Registration</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbdependent_registration" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" Text='<%# Eval("dependent_registration") %>' Enabled="false" />
                                        </div>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </ItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Dependent Registration</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Dependent Registration</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbdependent_registration" placeholder="Dependent Registration ..." TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Dependent Registration</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Dependent Registration</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbdependent_registration" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" Text='<%# Eval("dependent_registration") %>' />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view Dependent Registration Detail -->

                <!-- Form view Priorities Detail -->
                <asp:FormView ID="fvPrioritiesDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Priorities</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Type of priority</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Priority Date</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Priority No</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Country</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Priority No</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Country</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </ItemTemplate>
                    <InsertItemTemplate>


                        <div class="panel panel-default">
                            <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                            <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />

                            <div class="panel-heading">
                                <h3 class="panel-title">Priorities</h3>
                            </div>

                            <div class="panel-body">

                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Type of priority</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbType_of_priority" runat="server" placeholder="Type of priority ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Priority Date</label>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="tb_Priority_Date" runat="server" placeholder="Priority Date ..." CssClass="form-control from-date-datepicker cursor-pointer" />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Priority No</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tb_Priority_No" runat="server" placeholder="Priority No ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Country</label>
                                        <div class="col-sm-4">

                                            <asp:DropDownList ID="ddlCountry_priority" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlStatus_priority" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-sm-6 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-2">
                                            <asp:LinkButton ID="btnAddPrioritiesTolist" runat="server" CssClass="btn btn-primary" Text="Add Priorities" OnCommand="btnCommand"
                                                CommandName="cmdAddPrioritiesList" ValidationGroup="addPrioritiesList" />
                                        </div>
                                        <label class="col-sm-8 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">

                                            <asp:GridView ID="gvPrioritiesList"
                                                runat="server"
                                                CssClass="table table-bordered table-striped table-responsive col-md-12"
                                                HeaderStyle-CssClass="info"
                                                OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex + 1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="drTypeofpriorityText" HeaderText="Type of priority" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drPriorityDateText" HeaderText="Priority Date" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drPriorityNoText" HeaderText="Priority No" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drCountrypriorityText" HeaderText="Country" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drStatuspriorityText" HeaderText="Status" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnRemovePriorities" runat="server"
                                                                CssClass="btn btn-danger btn-xs"
                                                                OnClientClick="return confirm('Delete data Priorities?')"
                                                                CommandName="cmdRemovePriorities"><i class="fa fa-times"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                    </div>


                                </div>
                            </div>


                        </div>

                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Priorities</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Type of priority</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Priority Date</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Priority No</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Country</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-6 control-label"></label>

                                    </div>


                                </div>
                            </div>

                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view Priorities Detail -->

                <!-- Form view Client Detail-->
                <asp:FormView ID="fvClientDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Client Details</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Details</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Contact</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Reference</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </ItemTemplate>
                    <InsertItemTemplate>
                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Client Details</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Details</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbClientDetail" runat="server" placeholder="Details ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Contact</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbClientContact" runat="server" placeholder="Contact ..." CssClass="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Reference</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbClientReference" runat="server" placeholder="Reference ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlClientStatus" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-2">
                                            <asp:LinkButton ID="btnAddClientTolist" runat="server" CssClass="btn btn-primary" Text="Add Client" OnCommand="btnCommand"
                                                CommandName="cmdAddClientList" ValidationGroup="addClientList" />
                                        </div>
                                        <label class="col-sm-8 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">

                                            <asp:GridView ID="gvClientList"
                                                runat="server"
                                                CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                HeaderStyle-CssClass="info"
                                                OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex + 1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="drClientDetailText" HeaderText="Details" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drClientContactText" HeaderText="Contact" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drClientReferenceText" HeaderText="Reference" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drClientStatusText" HeaderText="Status" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnRemoveClient" runat="server"
                                                                CssClass="btn btn-danger btn-xs"
                                                                OnClientClick="return confirm('Delete data Client?')"
                                                                CommandName="cmdRemoveClient"><i class="fa fa-times"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>


                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Client Details</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Details</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Contact</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Reference</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view Client Detail-->

                <!-- Form view Agent Detail -->
                <asp:FormView ID="fvAgentDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Agent Details</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Details</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Contact</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Reference</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </ItemTemplate>
                    <InsertItemTemplate>
                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Agent Details</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Details</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbAgentDetail" runat="server" placeholder="Details ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Contact</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbAgentContact" runat="server" placeholder="Contact ..." CssClass="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Reference</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbAgentReference" runat="server" placeholder="Reference ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlAgentStatus" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-2">
                                            <asp:LinkButton ID="btnAddAgentTolist" runat="server" CssClass="btn btn-primary" Text="Add Agent" OnCommand="btnCommand"
                                                CommandName="cmdAddAgentList" ValidationGroup="addAgentList" />
                                        </div>
                                        <label class="col-sm-8 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:GridView ID="gvAgentList"
                                                runat="server"
                                                CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                                                HeaderStyle-CssClass="info"
                                                OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex + 1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="drAgentDetailText" HeaderText="Details" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drAgentContactText" HeaderText="Contact" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drAgentReferenceText" HeaderText="Reference" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drAgentStatusText" HeaderText="Status" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnRemoveAgent" runat="server"
                                                                CssClass="btn btn-danger btn-xs"
                                                                OnClientClick="return confirm('Delete data Agent?')"
                                                                CommandName="cmdRemoveAgent"><i class="fa fa-times"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Agent Details</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Details</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Contact</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Reference</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view Agent Detail -->

                <!-- Form view Goods Detail -->
                <asp:FormView ID="fvGoodsDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Goods</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Int. Class</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Goods</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Local language</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-6 control-label"></label>
                                        <%--<div class="col-sm-4">
                                                           
                                                        </div>--%>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </ItemTemplate>
                    <InsertItemTemplate>
                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Goods</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Int. Class</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbGoods_IntClass" runat="server" placeholder="Int. Class ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Goods</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbGoodsDetail" runat="server" placeholder="Goods ..." CssClass="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Local language</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlLocal_language" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                        <label class="col-sm-6 control-label"></label>
                                        <%--<div class="col-sm-4">
                                                           
                                                        </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-2">
                                            <asp:LinkButton ID="btnAddGoodsTolist" runat="server" CssClass="btn btn-primary" Text="Add Goods" OnCommand="btnCommand"
                                                CommandName="cmdAddGoodsList" ValidationGroup="addGoodsList" />
                                        </div>
                                        <label class="col-sm-8 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">

                                            <asp:GridView ID="gvGoodsList"
                                                runat="server"
                                                CssClass="table table-bordered table-striped table-responsive"
                                                HeaderStyle-CssClass="info"
                                                OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex + 1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="drGoodsIntClassText" HeaderText="Details" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drGoodsDetailText" HeaderText="Contact" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="drLocallanguageText" HeaderText="Reference" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnRemoveGoods" runat="server"
                                                                CssClass="btn btn-danger btn-xs"
                                                                OnClientClick="return confirm('Delete data Goods?')"
                                                                CommandName="cmdRemoveGoods"><i class="fa fa-times"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>


                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Goods</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Int. Class</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-2 control-label">Goods</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbcreate_date_u0_detail" runat="server" CssClass="form-control" Text='<%# Eval("create_date_u0") %>' Enabled="false" />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Local language</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="tbdocrequest_code_detail" runat="server" CssClass="form-control" Text='<%# Eval("docrequest_code") %>' Enabled="false" />--%>
                                        </div>
                                        <label class="col-sm-6 control-label"></label>
                                        <%--<div class="col-sm-4">
                                                           
                                                        </div>--%>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view Goods Detail -->

                <!-- Form view Additional Details -->
                <asp:FormView ID="fvAdditionalDetail" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" placeholder="Additional Details ..." Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view Additional Details -->

                <!-- Form view Additional Details -->
                <asp:FormView ID="fvTademarkProfile" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Trademark Profile</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Name Tademark Profile</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbName_trademark" runat="server" placeholder="Name Tademark Profile ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Doc (Local Language)</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlLocal_language_trademark" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Profile Owner</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbProfileOwner" placeholder="Profile Owner ..." runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Profile Owner (Local Language)</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlProfileOwnerLanguage" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Profile Client</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbProfileClient" runat="server" placeholder="Profile Client ..." CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Profile Client (Local Language)</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlProfileClientLanguage" AutoPostBack="true" runat="server" ValidationGroup="SaveRecordsheet" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Translation</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbTranslation" runat="server" placeholder="Translation ..." TextMode="MultiLine" Rows="2" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-2 control-label">Transliteration</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbTransliteration" runat="server" placeholder="Transliteration ..." TextMode="MultiLine" Rows="2" CssClass="form-control" />
                                        </div>
                                    </div>

                                    <asp:UpdatePanel ID="Panel_UploadFileTrademark" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Local Filename</label>
                                                <div class="col-sm-6">
                                                    <asp:FileUpload ID="UploadFileTrademark" CssClass="control-label UploadFileTrademark multi max-2 accept-png|jpg|pdf maxsize-1024 with-preview" ValidationGroup="SaveLawOfficerEditFileMemo" runat="server" />


                                                </div>
                                                <label class="col-sm-4"></label>
                                            </div>


                                            <%--<div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <asp:LinkButton ID="lbDocSaveLawOfficerEditFileMemo" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveLawOfficerEditFileMemo"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                                    <asp:LinkButton ID="lbDocCancelSaveLawOfficerEditFileMemo" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                                </div>
                                            </div>--%>
                                        </ContentTemplate>
                                        <Triggers>
                                            <%-- <asp:PostBackTrigger ControlID="lbDocSaveLawOfficerEditFileMemo" />--%>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view Additional Details -->

                <!-- gvActions Details -->
                <div id="div_Actions" runat="server" style="overflow: auto">
                    <asp:GridView ID="gvActions" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                        HeaderStyle-CssClass="default"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        AllowPaging="True"
                        PageSize="10">
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- No Data ---</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <%# (Container.DataItemIndex + 1) %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Actions" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="40%">
                                <ItemTemplate>

                                    <asp:TextBox ID="tbActionsDetail" TextMode="MultiLine" placeholder="Actions ..." Rows="2" runat="server" CssClass="form-control" />

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Owner" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="40%">
                                <ItemTemplate>

                                    <asp:TextBox ID="tbOwnerDetail" TextMode="MultiLine" placeholder="Owner ..." Rows="2" runat="server" CssClass="form-control" />

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Agent" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                <ItemTemplate>

                                    <asp:Label ID="lblaction_idx" runat="server" Visible="false" Text='<%# Eval("action_idx") %>' />
                                    <asp:TextBox ID="tb_action_name_en" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("action_name_en") %>' />


                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
                <!-- gvActions Details -->

                <!-- Form view Background of the invention -->
                <asp:FormView ID="fvBackground" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Background of the invention</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Background of the invention</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbBackgroundDetail" TextMode="MultiLine" placeholder="Background of the invention ..." Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view Background of the invention -->

                <!-- Form view Summary of the invention -->
                <asp:FormView ID="fvSummary" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Summary of the invention</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Summary of the invention</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbSummaryDetail" TextMode="MultiLine" placeholder="Summary of the invention ..." Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view Summary of the invention -->

                <!-- Form view Claims -->
                <asp:FormView ID="fvClaims" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Claims</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Claims</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbClaimsDetail" TextMode="MultiLine" placeholder="Claims ..." Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view Claims -->

                <!-- Form view Abstract -->
                <asp:FormView ID="fvAbstract" runat="server" DefaultMode="ReadOnly" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <InsertItemTemplate>

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfU1IDX" runat="server" Value='<%# Eval("u1_doc_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Abstract</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Abstract</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbAbstractDetail" TextMode="MultiLine" placeholder="Claims ..." Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Additional Details</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Additional Details</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tbAdditionalDetail" TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4 control-label"></label>
                                        <%--<label class="col-sm-2 control-label">Notice of Allowance/Grant date</label>
                                                        <div class="col-sm-4">
                                                            
                                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <!-- Form view Abstract -->

                <asp:UpdatePanel ID="Panel_SaveRecordSheet" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form-group">
                            <div class="pull-right">
                                <asp:LinkButton ID="btnSaveTrademarkRecordSheet" CssClass="btn btn-success" data-toggle="tooltip" title="Save Record" runat="server"
                                    CommandName="cmdSaveTrademarkRecordSheet" OnCommand="btnCommand"><i class="fa fa-save" aria-hidden="true"></i> Save RecordSheet</asp:LinkButton>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnSaveTrademarkRecordSheet" />
                    </Triggers>
                </asp:UpdatePanel>

                <!-- Log Detail Record Status -->
                <div id="div_LogRecordSheet" runat="server" visible="false">
                    <%--<div class="panel-heading">Log Detail</div>--%>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">Record Status</h3>
                        </div>
                        <div class="panel-body">
                            <div id="divLogRecordSheet" runat="server" style="overflow: auto; height: 150px">
                                <table class="table table-bordered table-striped table-responsive col-md-12">
                                    <asp:Repeater ID="rptLogRecordSheet" runat="server">
                                        <HeaderTemplate>
                                            <tr>
                                                <th>date / time</th>
                                                <th>operator</th>
                                                <th>process</th>
                                                <%--<th>ผลการดำเนินการ</th>--%>
                                                <th>comment</th>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td data-th="date / time"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                                <td data-th="operator"><%# Eval("emp_name_en") %> ( <%# Eval("current_artor") %> )</td>
                                                <td data-th="process"><%# Eval("current_decision") %></td>
                                                <td data-th="comment"><%# Eval("comment") %></td>
                                                <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                        <%-- <div class="m-t-10 m-b-10"></div>--%>
                    </div>
                </div>
                <!-- Log Detail Record Status -->

            </div>
        </asp:View>
        <!-- View Registration Record Sheet -->
    </asp:MultiView>
    <!--multiview-->

    <!--script-->
    <script src='<%=ResolveUrl("~/Scripts/jquery.MultiFileNew.js")%>'></script>

    <script>
        $(function () {
            $('.UploadFile').MultiFile({

            });

        });
        $(function () {
            $('.UploadFileMemo').MultiFile({

            });

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.UploadFile').MultiFile({

                });

            });

            $(function () {
                $('.UploadFileMemo').MultiFile({

                });

            });
        });

        //})
    </script>

    <script type="text/javascript">
        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }


        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });

            $('.show-from-onclick').click(function () {

                $('.from-date-datepicker').data("DateTimePicker").show;
            });
        });

        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true
                });

                $('.show-from-onclick').click(function () {
                    //$('.from-date-datepicker').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                    $('.from-date-datepicker').data("DateTimePicker").show;
                });
            });
        });

    </script>


    <!--script-->

    <style type="text/css">
        .bg-template-resign {
            background-image: url('images/law-trademarks/body-viewdetail.png');
            background-size: 100% 100%;
            width: 70%;
            height 100
            /*tex -alig
                    }
    </style>

</asp:Content>
