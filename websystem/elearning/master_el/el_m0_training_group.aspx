﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_m0_training_group.aspx.cs" Inherits="websystem_el_m0_training_group" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="row">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary pull-right" runat="server"
                    data-toggle="tooltip" title="สร้างหัวข้อ/หลักสูตร"
                    CommandName="btnToInsert" OnCommand="btnCommand">
                     <i class="fa fa-plus-square"></i> สร้างกลุ่มวิชา</asp:LinkButton>
            </div>

            <asp:Panel ID="pnlsearch" runat="server">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>กลุ่มวิชา</label>
                                <asp:TextBox ID="txtFilterKeyword" runat="server"
                                    CssClass="form-control"
                                    placeholder="กลุ่มวิชา..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnFilter" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnFilter" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="m0_training_group_idx"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>

                                <asp:TextBox ID="m0_training_group_idx" runat="server" CssClass="form-control"
                                    Visible="False" Text='<%# Eval("m0_training_group_idx")%>' />

                                <div class="col-md-8 col-md-offset-2">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">รหัส</label>
                                                <asp:UpdatePanel ID="paneltraining_group_codeUpdate" runat="server">
                                                    <ContentTemplate>

                                                        <asp:TextBox ID="txttraining_group_codeUpdate" runat="server" CssClass="form-control"
                                                            Text='<%# Eval("training_group_code")%>' Enabled="false" />
                                                        <asp:RequiredFieldValidator ID="Requiredtxttraining_group_codeUpdate"
                                                            ValidationGroup="savetraining_group_nameUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txttraining_group_codeUpdate"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกรหัส" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">กลุ่มวิชา</label>
                                                <asp:UpdatePanel ID="paneltraining_group_nameUpdate" runat="server">
                                                    <ContentTemplate>

                                                        <asp:TextBox ID="txttraining_group_nameUpdate" runat="server" CssClass="form-control"
                                                            Text='<%# Eval("training_group_name")%>' />
                                                        <asp:RequiredFieldValidator ID="requiredtraining_group_nameUpdate"
                                                            ValidationGroup="savetraining_group_nameUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txttraining_group_nameUpdate"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกกลุ่มวิชา" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">สีกลุ่มวิชา</label>
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>

                                                        <asp:TextBox ID="txttraining_group_colorUpdate" runat="server"
                                                            CssClass="form-control"
                                                            Text='<%# Eval("training_group_color")%>'
                                                            BackColor='<%# getColor((string)Eval("training_group_color")) %>' />
                                                        <asp:RequiredFieldValidator ID="Requiredtxttraining_group_colorUpdate"
                                                            ValidationGroup="save" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txttraining_group_colorUpdate"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกสีกลุ่มวิชา" />
                                                        <asp:ColorPickerExtender ID="ColorPickerExtenderUpdate" runat="server"
                                                            TargetControlID="txttraining_group_colorUpdate"
                                                            PopupButtonID="btncolor"
                                                            OnClientColorSelectionChanged="Color_Change"></asp:ColorPickerExtender>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">หมายเหตุ</label>
                                                <asp:UpdatePanel ID="paneltraining_group_remarkUpdate" runat="server">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txttraining_group_remarkUpdate" runat="server" CssClass="form-control"
                                                            TextMode="MultiLine" Rows="4"
                                                            Text='<%# Eval("training_group_remark")%>'
                                                            placeholder="หมายเหตุ..." />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">สถานะ</label>
                                                <asp:DropDownList ID="ddltraining_group_statusUpdate" AutoPostBack="false" runat="server"
                                                    CssClass="form-control" SelectedValue='<%# Eval("training_group_status") %>'>
                                                    <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                                </asp:DropDownList>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                                ValidationGroup="savetraining_group_nameUpdate" CommandName="Update"
                                                OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"
                                                data-toggle="tooltip" title="บันทึก"
                                                Text="<i class='fa fa-save fa-lg'></i> บันทึก">
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                                Text="<i class='fa fa-close fa-lg'></i> ยกเลิก"
                                                data-toggle="tooltip" title="ยกเลิก"
                                                CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รหัสกลุ่มวิชา" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="training_group_code" runat="server" Text='<%# Eval("training_group_code") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="training_group_name" runat="server" Text='<%# Eval("training_group_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สีกลุ่มวิชา" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="training_group_color" runat="server"
                                        CssClass="form-control pull-left"
                                        BackColor='<%# getColor((string)Eval("training_group_color")) %>'
                                        Text='<%# Eval("training_group_color") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="training_group_status" runat="server" Text='<%# getStatus((int)Eval("training_group_status")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("m0_training_group_idx") %>'
                                    OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>

        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <%--<i class="glyphicon glyphicon-blackboard"></i>--%>
                        <strong>&nbsp; กลุ่มวิชา
                        </strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-8 col-md-offset-2">

                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>รหัส</label>
                                    <asp:UpdatePanel ID="pnltraining_group_code" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txttraining_group_code" runat="server" CssClass="form-control"
                                                MaxLength="10" placeholder="รหัส..." />
                                            <asp:RequiredFieldValidator ID="Requiredtraining_group_code"
                                                ValidationGroup="save" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txttraining_group_code"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกรหัส" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>กลุ่มวิชา</label>
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txttraining_group_name" runat="server" CssClass="form-control"
                                                placeholder="กลุ่มวิชา..." />
                                            <asp:RequiredFieldValidator ID="requiredtraining_group_name"
                                                ValidationGroup="save" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txttraining_group_name"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกกลุ่มวิชา" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>สีกลุ่มวิชา</label>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txttraining_group_color" runat="server"
                                                CssClass="form-control"
                                                BackColor="#008CBA"
                                                Text="#008CBA" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                ValidationGroup="save" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txttraining_group_color"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกสีกลุ่มวิชา" />
                                            <%--<asp:ColorPickerExtender ID="txtBGColor_ColorPickerExtender" runat="server"
                                                Enabled="True" TargetControlID="txttraining_group_color"
                                                
                                                OnClientColorSelectionChanged="colorChanged"></asp:ColorPickerExtender>--%>

                                            <asp:ColorPickerExtender ID="ColorPickerExtender" runat="server"
                                                TargetControlID="txttraining_group_color"
                                                PopupButtonID="btncolor"
                                                OnClientColorSelectionChanged="Color_Change"></asp:ColorPickerExtender>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>
                            </div>



                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>หมายเหตุ</label>
                                    <asp:UpdatePanel ID="pnltraining_group_remark" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txttraining_group_remark" runat="server" CssClass="form-control"
                                                TextMode="MultiLine" Rows="4"
                                                placeholder="หมายเหตุ..." />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>สถานะ</label>
                                    <asp:DropDownList ID="ddltraining_group_status" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="1" Text="Online" />
                                        <asp:ListItem Value="0" Text="Offline" />
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">

                                    <asp:LinkButton CssClass="btn btn-success" runat="server"
                                        CommandName="btnInsert" OnCommand="btnCommand"
                                        Text="<i class='fa fa-save fa-lg'></i> บันทึก"
                                        data-toggle="tooltip" title="บันทึก"
                                        ValidationGroup="save" />
                                    <asp:LinkButton CssClass="btn btn-danger"
                                        data-toggle="tooltip" title="ยกเลิก" runat="server"
                                        Text="<i class='fa fa-close fa-lg'></i> ยกเลิก"
                                        CommandName="btnCancel" OnCommand="btnCommand" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </asp:View>
        <!-- End Insert Form -->


    </asp:MultiView>

    <script type="text/javascript">  
        function Color_Change(sender) {
            sender.get_element().blur();
            sender.get_element().value = "#" + sender.get_selectedColor();
            //sender.get_element().style.color = '#' + sender.get_selectedColor();
            sender.get_element().style.backgroundColor = '#' + sender.get_selectedColor();
        }
    </script>
    <script type="text/javascript">
        function colorChanged(sender) {
            sender.get_element().blur();
            sender.get_element().style.color = '#' + sender.get_selectedColor();
            sender.get_element().style.backgroundColor = '#' + sender.get_selectedColor();
        }
    </script>

</asp:Content>
