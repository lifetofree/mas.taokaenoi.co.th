﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_networkdevices
/// </summary>
/// 

[Serializable]
[XmlRoot("data_FeedBack")]
public class data_FeedBack
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("M0_FeedBack_1_list")]
    public M0_FeedBack_1_detail[] M0_FeedBack_1_list { get; set; }
    [XmlElement("M0_System_list")]
    public M0_System_detail[] M0_System_list { get; set; }
    [XmlElement("M0_FeedBack_2_list")]
    public M0_FeedBack_2_detail[] M0_FeedBack_2_list { get; set; }
    [XmlElement("M0_FeedBack_3_list")]
    public M0_FeedBack_3_detail[] M0_FeedBack_3_list { get; set; }
    [XmlElement("U_FeedBack_list")]
    public U_FeedBack_detail[] U_FeedBack_list { get; set; }

    [XmlElement("T0_FeedBack_1_list")]
    public T0_FeedBack_1_detail[] T0_FeedBack_1_list { get; set; }
    [XmlElement("T0_FeedBack_2_list")]
    public T0_FeedBack_2_detail[] T0_FeedBack_2_list { get; set; }
    [XmlElement("T0_FeedBack_3_list")]
    public T0_FeedBack_3_detail[] T0_FeedBack_3_list { get; set; }

}

[Serializable]
public class M0_FeedBack_1_detail
{
    [XmlElement("MFBIDX1")]
    public int MFBIDX1 { get; set; }

    [XmlElement("SysIDX")]
    public int SysIDX { get; set; }

    [XmlElement("QuestionSet")]
    public string QuestionSet { get; set; }

    [XmlElement("MFB1Status")]
    public int MFB1Status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("SysNameEN")]
    public string SysNameEN { get; set; }

}

[Serializable]
public class M0_System_detail
{
    [XmlElement("SysIDX")]
    public int SysIDX { get; set; }
    [XmlElement("SysNameTH")]
    public string SysNameTH { get; set; }
    [XmlElement("SysNameEN")]
    public string SysNameEN { get; set; }
}

[Serializable]
public class M0_FeedBack_2_detail
{
    [XmlElement("MFBIDX2")]
    public int MFBIDX2 { get; set; }
    [XmlElement("Topic")]
    public string Topic { get; set; }
    [XmlElement("MFB2Status")]
    public int MFB2Status { get; set; }
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }
}

[Serializable]
public class M0_FeedBack_3_detail
{
    [XmlElement("MFBIDX3")]
    public int MFBIDX3 { get; set; }
    [XmlElement("Question")]
    public string Question { get; set; }
    [XmlElement("MFB3Status")]
    public int MFB3Status { get; set; }
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }
}

public class U_FeedBack_detail
{
    [XmlElement("UIDX")]
    public int UIDX { get; set; }
    [XmlElement("MFBIDX")]
    public int MFBIDX { get; set; }
    [XmlElement("MFBIDX1")]
    public int MFBIDX1 { get; set; }
    [XmlElement("MFBIDX2")]
    public int MFBIDX2 { get; set; }
    [XmlElement("MFBIDX3")]
    public int MFBIDX3 { get; set; }
    [XmlElement("MFBStatus")]
    public int MFBStatus { get; set; }
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    //Lookup
    [XmlElement("QuestionSet")]
    public string QuestionSet { get; set; }
    [XmlElement("Topic")]
    public string Topic { get; set; }
    [XmlElement("Question")]
    public string Question { get; set; }

}


[Serializable]
public class T0_FeedBack_1_detail
{
    [XmlElement("TFBIDX1")]
    public int TFBIDX1 { get; set; }

    [XmlElement("SysIDX")]
    public int SysIDX { get; set; }

    [XmlElement("QuestionSet")]
    public string QuestionSet { get; set; }

    [XmlElement("TFB1Status")]
    public int TFB1Status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("SysNameEN")]
    public string SysNameEN { get; set; }

}

[Serializable]
public class T0_FeedBack_2_detail
{
    [XmlElement("TFBIDX2")]
    public int TFBIDX2 { get; set; }
    [XmlElement("Topic")]
    public string Topic { get; set; }
    [XmlElement("TFB2Status")]
    public int TFB2Status { get; set; }
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }
}

[Serializable]
public class T0_FeedBack_3_detail
{
    [XmlElement("TFBIDX3")]
    public int TFBIDX3 { get; set; }
    [XmlElement("Question")]
    public string Question { get; set; }
    [XmlElement("TFB3Status")]
    public int TFB3Status { get; set; }
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }
}