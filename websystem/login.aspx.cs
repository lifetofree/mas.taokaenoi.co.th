﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections;

public partial class websystem_login : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlCheckLogin = _serviceUrl + ConfigurationManager.AppSettings["urlCheckLogin"];
    static string urlGetEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee"];
    static string urlSet_reset_password_user = _serviceUrl + ConfigurationManager.AppSettings["urlSet_reset_password_user"];

    string _localJson = "";
    string _initPass = "25d55ad283aa400af464c76d713c07ad";
	int _tempInt = 0;
    #endregion initial function/data

    private void Page_Init(object sender, System.EventArgs e)
    {
        ClearApplicationCache();

        if (Request.Form["emp_idx"] != null)
        {
			bool _result = Int32.TryParse(Request.Form["emp_idx"], out _tempInt);
			if(_result && _tempInt > 0)
			{
				Session["emp_idx"] = Request.Form["emp_idx"];
				// go to page
				string url = Request.QueryString["url"];
				if(url == null)
				{
                    //Response.Redirect(ResolveUrl("~/"));
                    Response.Redirect(ResolveUrl("https://mas.taokaenoi.co.th/login"));
                }
				else
				{
					Response.Redirect(ResolveUrl("~/" + url));
				}
			}
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        tbEmpCode.Focus();

        if(!IsPostBack)
        {
            Session.Clear();
            divShowError.Visible = false;
        }
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }



    #region btn command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdLogin":
                string _empCode = tbEmpCode.Text.Trim();
                string _empPass = _funcTool.getMd5Sum(tbEmpPass.Text.Trim());
                if(_empCode != String.Empty && _empPass != String.Empty)
                {
                    // set data
                    _dataEmployee.employee_list = new employee_detail[1];
                    employee_detail _empDetail = new employee_detail();
                    _empDetail.emp_code = _empCode;
                    _empDetail.emp_password = _empPass;
                    _dataEmployee.employee_list[0] = _empDetail;

                    // convert to json
                    _localJson = _funcTool.convertObjectToJson(_dataEmployee);

                    // call services
                    _localJson = _funcTool.callServiceGet(_urlCheckLogin + _localJson);

                    // convert json to object
                    _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

                    // check return_code
                    if(int.Parse(_dataEmployee.return_code) == 0)
                    {
                        // create session
                        Session["emp_idx"] = _dataEmployee.employee_list[0].emp_idx;
                        //Session["rdept_idx"] = _dataEmployee.employee_list[0].rdept_idx;
                        //Session["rsec_idx"] = _dataEmployee.employee_list[0].rsec_idx;


                        // check initial password or not
                        if (_empPass != _initPass)
                        {
                            // go to page
                            string url = Request.QueryString["url"];
                            if(url == null)
                            {
                                // Response.Redirect(ResolveUrl("~/"));
                                Response.Redirect(ResolveUrl("https://mas.taokaenoi.co.th"));
                            }
                            else
                            {
                                Response.Redirect(ResolveUrl(url));                               

                            }
                        }
                        else
                        {
                            Session["reset_type"] = 1;
                            Response.Redirect(ResolveUrl("~/changepassword"));
                        }
                    }
                    else
                    {
                        divShowError.Visible = !divShowError.Visible;
                        litErrorCode.Text = _dataEmployee.return_code.ToString();
                        //litDebug.Text = "error : " + _dataEmployee.return_code.ToString() + " - " + _dataEmployee.return_msg;
                    }
                }
            break;

            case "cmdResetpass":
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                break;
            case "cmdContracthr":
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_Contract();", true);
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "แจ้งเตือน", "alert('" + "หากไม่สามารถ Reset Password กรุณาติดต่อ HR ครับ" + "')", true);
                break;
            case "btnClose_Change_Password":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnChange_Password":

                _dataEmployee.employee_list = new employee_detail[1];
                employee_detail _dataEmployee_ = new employee_detail();

                _dataEmployee_.emp_code = txtempcode.Text;
                _dataEmployee_.identity_card = txtidcard.Text;

                _dataEmployee.employee_list[0] = _dataEmployee_;
                //Ladbel7.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
                _dataEmployee = callServiceEmployee(urlSet_reset_password_user, _dataEmployee);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                //Ladbel7.Text = _dataEmployee.return_code.ToString();
                if (_dataEmployee.return_code == "0") // Yes
                {
                    lbltextalert.ForeColor = System.Drawing.Color.Red;
                    lbltextalert.Text = "Reset Password เสร็จเรียบร้อย";
                }
                else // No
                {
                    lbltextalert.ForeColor = System.Drawing.Color.Red;
                    lbltextalert.Text = "Reset Password ไม่สำเร็จ เนื่องจากข้อมูลรหัสพนักงานและรหัสบัตรประชาชนไม่ตรงกัน";
                }

                break;

        }
    }
    #endregion btn command

    public static string Sayhello(string name)
    {
        return "Hello " + name + " !";
    }

    #region reuse
    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }
    #endregion
}
