﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="drc_m0_manage_setname.aspx.cs" Inherits="websystem_MasterData_drc_m0_manage_setname" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <div class="col-sm-12">
        <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
        <asp:Literal ID="test_lab" runat="server"></asp:Literal>
    </div>

    <asp:MultiView ID="MvMaster_lab" runat="server">
        <asp:View ID="view_Genaral" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btn_addsetname" Visible="true" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มชุดข้อมูล" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddSetname" CommandArgument="0" title="เพิ่มชุดข้อมูล"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มชุดข้อมูล</asp:LinkButton>
            </div>

            <%--  DIV ADD--%>
            <asp:FormView ID="FvInsertSetname" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มชุดข้อมูล</h3>

                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="la_nameset" runat="server" Text="ชื่อชุดข้อมูล" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtset_name" runat="server" CssClass="form-control" placeholder="กรอกชื่อชุดข้อมูล ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_txtset_name" runat="server"
                                                    ControlToValidate="txtset_name" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อชุดข้อมูล" ValidationGroup="saveInsert" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderlabTname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtset_name" Width="250" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_status_setname" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddl_status_setname" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-9">
                                                <asp:LinkButton ID="btnSaveInsertSet" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" OnCommand="btnCommand" CommandName="cmdSave" ValidationGroup="saveInsert" CommandArgument="0"></asp:LinkButton>

                                                <asp:LinkButton ID="btnCancelInsertSet" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0"></asp:LinkButton>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <hr />
                                <%--  /DIV END ADD--%>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <%--select setname--%>
            <asp:GridView ID="GvSetNameDRC" runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่มีข้อมูล --</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="8%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="lbl_sidx" runat="server" Visible="false" Text='<%# Eval("sidx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_sidx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("sidx") %>'></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="La_labname" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อชุดข้อมูล" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_set_name_edit" runat="server" CssClass="form-control " Text='<%# Eval("set_name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Re_txt_set_name_edit" runat="server"
                                                    ControlToValidate="txt_set_name_edit" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อชุดข้อมูล" ValidationGroup="Edit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txt_set_name_edit" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_name_edit" Width="250" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbupdates_lab" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="dd_set_status_edit" Text='<%# Eval("set_status") %>'
                                                    CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                    <asp:ListItem Value="0">Offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-9">
                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Edit" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อชุดข้อมูล" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="30%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_set_name" runat="server"
                                        Text='<%# Eval("set_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_set_status" Visible="false" runat="server"
                                        Text='<%# Eval("set_status") %>'></asp:Label>
                                    <asp:Label ID="lbl_set_statusOpen" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="lbl_set_statusClose" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="btnViewSetname" CssClass="text-read" runat="server" CommandName="cmdViewSetname"
                                    data-toggle="tooltip" OnCommand="btnCommand"
                                    CommandArgument='<%#Eval("sidx")+ ";" + Eval("set_name")%>' title="view">
                                        <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="cmdDeleteSetname"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบแลปนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("sidx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
        </asp:View>

        <asp:View ID="view_CreateRoot" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnBack" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdback" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>

            </div>
            <div class="form-group">
                <asp:LinkButton ID="btnAddRootSet" CssClass="btn btn-primary" Visible="true" runat="server" data-original-title="เพิ่มสถานะ" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddSetname" CommandArgument="1" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มสถานะ</asp:LinkButton>
            </div>
            <!--formview insert Level 2-->
            <asp:FormView ID="fvform_Insert_Root" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มข้อมูลสถานะ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ชื่อชุดข้อมูล</label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txt_sidx_root" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>
                                        <%--<asp:TextBox ID="TextBox1" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("sidx") %>'></asp:TextBox>--%>
                                        <asp:TextBox ID="txt_sidx_root_list" runat="server" CssClass="form-control" placeholder="ชื่อชุดข้อมูล ..." Enabled="false" />

                                    </div>

                                    <div class="col-sm-3"></div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ชื่อสถานะ</label>
                                    <div class="col-sm-6">
                                        <%-- <asp:TextBox ID="txt_IDtestDetail" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("test_detail_idx") %>'></asp:TextBox>--%>
                                        <asp:TextBox ID="txt_statusname_set" CssClass="form-control" runat="server">
                                        </asp:TextBox>

                                        <asp:RequiredFieldValidator ID="Re_txt_statusname_set" runat="server"
                                            ControlToValidate="txt_statusname_set" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกชื่อสถานะ" ValidationGroup="SaveEdit" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txt_statusname_set" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_statusname_set" Width="250" />

                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidatorDD_EquipmentName" runat="server"
                                            ControlToValidate="DD_EquipmentName" InitialValue="0" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกชื่ออุปกรณ์" ValidationGroup="SaveM1Lab" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" PopupPosition="BottomRight"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorDD_EquipmentName" Width="250" />--%>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">สถานะ</label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlRootSetStatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <asp:LinkButton ID="lnkbtnSave" ValidationGroup="SaveEdit" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="1"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <%--select lab M1--%>

            <asp:GridView ID="GvRootSetName" runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่มีข้อมูล --</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="8%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="lblUIDX" runat="server" Visible="false" Text='<%# Eval("sidx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                       
                                        <div class="form-group">
                                            <asp:Label ID="lbl_Setname_edit" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อชุดข้อมูล" />
                                            <div class="col-sm-7">
                                                <asp:Label ID="lblUIDX_edit" runat="server" Visible="false" Text='<%# Eval("sidx") %>' />
                                                <asp:TextBox ID="txt_Setname_idx_edit" Visible="false" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:TextBox ID="txt_Setname_edit" Visible="true" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                               <%-- <asp:DropDownList ID="DD_equipmentname" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEditlabm1" runat="server"
                                                    ControlToValidate="DD_equipmentname" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกชื่ออุปกรณ์" ValidationGroup="EditM1Lab" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderlabm1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorEditlabm1" Width="200" />--%>
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lb_testdetail" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อสถานะ" />
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txt_set_name_edit" Visible="true" runat="server" CssClass="form-control" Text='<%# Eval("set_name") %>'></asp:TextBox>
                                               
                                                <asp:RequiredFieldValidator ID="Re_txt_set_name_edit" runat="server"
                                                    ControlToValidate="txt_set_name_edit" 
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อสถานะ" ValidationGroup="Edit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderlabm1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_name_edit" Width="200" />
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="lbupdates_lab" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddlset_status_edit" Text='<%# Eval("set_status") %>'
                                                    CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                    <asp:ListItem Value="0">Offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>
                                      
                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-4">
                                                <asp:LinkButton ID="btnupdate" ValidationGroup="Edit" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อชุดข้อมูล" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <%-- <asp:Label ID="m2Lab_idx" runat="server" Visible="false" Text='<%# Eval("equipment_idx") %>' />--%>
                                    <asp:Label ID="lb_set_name" Visible="true" runat="server"
                                        Text='<%# Eval("set_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_set_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("set_status") %>'></asp:Label>
                                    <asp:Label ID="lbl_set_status_Online" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="lbl_set_status_Offline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="btnTodeleteRoot"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการที่ตรวจนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("sidx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

        </asp:View>
    </asp:MultiView>
</asp:Content>
