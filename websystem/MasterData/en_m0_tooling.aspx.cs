﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_en_m0_tooling : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    data_en_planning _dtenplan = new data_en_planning();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlSelect_MasterData = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_tooling"];
    static string urlnsert_MasterData = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Master_tooling"];
    static string urlUpdate_MasterData = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Master_tooling"];
    static string urlDelete_MasterData = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Master_tooling"];

    static string _urlSelect_TypeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeMachineItem"];
    static string _urlSelect_GroupCodeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_GroupCodeMachine"];
    static string _urlSelect_Group = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Group_Machine"];



    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            select_typemachine(ddltype_search);
            SelectMasterList();

        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
        linkBtnTrigger(btnshow);


    }
    #region Manage SQL
    protected void SelectMasterList()
    {

        _dtenplan.BoxEN_ToolingList = new Tooling_Detail[1];
        Tooling_Detail dtsupport = new Tooling_Detail();

        dtsupport.TmcIDX = int.Parse(ddltype_search.SelectedValue);
        dtsupport.TCIDX = int.Parse(ddltypecode_search.SelectedValue);
        dtsupport.GCIDX = int.Parse(ddlgroupcode_search.SelectedValue);
        dtsupport.tooling_name_th = txtsearchtool.Text;
        _dtenplan.BoxEN_ToolingList[0] = dtsupport;

        _dtenplan = callServicePostENPlanning(urlSelect_MasterData, _dtenplan);
        setGridData(GvMaster, _dtenplan.BoxEN_ToolingList);
    }

    protected void Insert_Master_List()
    {
        _dtenplan.BoxEN_ToolingList = new Tooling_Detail[1];
        Tooling_Detail insert = new Tooling_Detail();

        insert.TmcIDX = int.Parse(ddltypemachine.SelectedValue);
        insert.TCIDX = int.Parse(ddltypecode.SelectedValue);
        insert.GCIDX = int.Parse(ddlgroupmachine.SelectedValue);
        insert.tooling_name_en = txtnameen.Text;
        insert.tooling_name_th = txtnameth.Text;
        insert.tooling_status = int.Parse(ddStatusadd.SelectedValue);
        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        if (txtmat.Text == "")
        {
            insert.mat_number = 0;
        }
        else
        {
            insert.mat_number = int.Parse(txtmat.Text);
        }
        insert.choose_type = 2;

        _dtenplan.BoxEN_ToolingList[0] = insert;
        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));
        _dtenplan = callServicePostENPlanning(urlnsert_MasterData, _dtenplan);

    }

    protected void Update_Master_List()
    {
        _dtenplan.BoxEN_ToolingList = new Tooling_Detail[1];
        Tooling_Detail update = new Tooling_Detail();
        update.TmcIDX = int.Parse(ViewState["ddltypemachine_edit"].ToString());
        update.TCIDX = int.Parse(ViewState["ddltypecode_edit"].ToString());
        update.GCIDX = int.Parse(ViewState["ddlgroupmachine_edit"].ToString());
        update.tooling_name_th = ViewState["txtnameth_edit"].ToString();
        update.tooling_name_en = ViewState["txtnameen_edit"].ToString();
        update.mat_number = int.Parse(ViewState["txtmat_edit"].ToString());
        update.tooling_status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        update.m0toidx = int.Parse(ViewState["m0toidx"].ToString());
        _dtenplan.BoxEN_ToolingList[0] = update;

        _dtenplan = callServicePostENPlanning(urlUpdate_MasterData, _dtenplan);

    }

    protected void Delete_Master_List()
    {
        _dtenplan.BoxEN_ToolingList = new Tooling_Detail[1];
        Tooling_Detail delete = new Tooling_Detail();

        delete.m0toidx = int.Parse(ViewState["m0toidx"].ToString());
        delete.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtenplan.BoxEN_ToolingList[0] = delete;

        _dtenplan = callServicePostENPlanning(urlDelete_MasterData, _dtenplan);
    }
    #endregion

    #region Master Form
    protected void select_typemachine(DropDownList ddlName)
    {
        data_en_planning _dtenplan1 = new data_en_planning();

        _dtenplan1.BoxEN_TypeItemList = new TypeItem_Detail[1];
        TypeItem_Detail typemachine = new TypeItem_Detail();
        _dtenplan1.BoxEN_TypeItemList[0] = typemachine;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtenplan));
        _dtenplan1 = callServicePostENPlanning(_urlSelect_TypeMachine, _dtenplan1);
        setDdlData(ddlName, _dtenplan1.BoxEN_TypeItemList, "NameEN", "TmcIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทเครื่องจักร...", "0"));

    }

    protected void select_typecodemachine(DropDownList ddlName, int tmcidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeCodeList = new TypeCode_Detail[1];
        TypeCode_Detail groupmachine = new TypeCode_Detail();
        groupmachine.TmcIDX = tmcidx;

        _dtenplan.BoxEN_TypeCodeList[0] = groupmachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_Group, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeCodeList, "NameTypecode", "TCIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรหัสกลุ่มเครื่องจักร...", "0"));

    }

    protected void select_groupmachine(DropDownList ddlName, int tcidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_GroupCodeList = new GroupCode_Detail[1];
        GroupCode_Detail groupmachine = new GroupCode_Detail();
        groupmachine.TCIDX = tcidx;

        _dtenplan.BoxEN_GroupCodeList[0] = groupmachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_GroupCodeMachine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_GroupCodeList, "NameTypecode", "GCIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรหัสกลุ่ม...", "0"));

    }

    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected data_en_planning callServicePostENPlanning(string _cmdUrl, data_en_planning _dtenplan)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtenplan);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtenplan = (data_en_planning)_funcTool.convertJsonToObject(typeof(data_en_planning), _localJson);

        return _dtenplan;
    }
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }



    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        var txtTmcIDX = (TextBox)e.Row.FindControl("txtTmcIDX");
                        var txtTCIDX = (TextBox)e.Row.FindControl("txtTCIDX");
                        var txtGCIDX = (TextBox)e.Row.FindControl("txtGCIDX");
                        var ddltypemachine_edit = (DropDownList)e.Row.FindControl("ddltypemachine_edit");
                        var ddltypecode_edit = (DropDownList)e.Row.FindControl("ddltypecode_edit");
                        var ddlgroupmachine_edit = (DropDownList)e.Row.FindControl("ddlgroupmachine_edit");


                        select_typemachine(ddltypemachine_edit);
                        ddltypemachine_edit.SelectedValue = txtTmcIDX.Text;
                        select_typecodemachine(ddltypecode_edit, int.Parse(ddltypemachine_edit.SelectedValue));
                        ddltypecode_edit.SelectedValue = txtTCIDX.Text;
                        select_groupmachine(ddlgroupmachine_edit, int.Parse(ddltypecode_edit.SelectedValue));
                        ddlgroupmachine_edit.SelectedValue = txtGCIDX.Text;

                    }
                }


                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                btnshow.Visible = false;
                SETBoxAllSearch.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                btnshow.Visible = true;
                SETBoxAllSearch.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int m0toidx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtnameth_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtnameth_edit");
                var txtnameen_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtnameen_edit");
                var txtmat_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtmat_edit");

                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");
                var ddltypemachine_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddltypemachine_edit");
                var ddltypecode_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddltypecode_edit");
                var ddlgroupmachine_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlgroupmachine_edit");

                GvMaster.EditIndex = -1;

                ViewState["m0toidx"] = m0toidx;
                ViewState["txtnameth_edit"] = txtnameth_edit.Text;
                ViewState["txtnameen_edit"] = txtnameen_edit.Text;
                ViewState["txtmat_edit"] = txtmat_edit.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;
                ViewState["ddltypemachine_edit"] = ddltypemachine_edit.SelectedValue;
                ViewState["ddltypecode_edit"] = ddltypecode_edit.SelectedValue;
                ViewState["ddlgroupmachine_edit"] = ddlgroupmachine_edit.SelectedValue;

                Update_Master_List();
                SelectMasterList();
                btnshow.Visible = true;
                SETBoxAllSearch.Visible = true;
                break;
        }
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;


        switch (ddName.ID)
        {
            case "ddltypemachine":

                select_typecodemachine(ddltypecode, int.Parse(ddltypemachine.SelectedValue));
                linkBtnTrigger(lbladd);

                break;
            case "ddltypecode":
                select_groupmachine(ddlgroupmachine, int.Parse(ddltypecode.SelectedValue));
                linkBtnTrigger(lbladd);
                break;


            case "ddltypemachine_edit":
                var ddledit = (DropDownList)sender;
                var row = (GridViewRow)ddledit.NamingContainer;

                var ddltypecode_edit = (DropDownList)row.FindControl("ddltypecode_edit");
                var ddltypemachine_edit = (DropDownList)row.FindControl("ddltypemachine_edit");

                select_typecodemachine(ddltypecode_edit, int.Parse(ddltypemachine_edit.SelectedValue));

                break;

            case "ddltypecode_edit":
                var ddledit1 = (DropDownList)sender;
                var row1 = (GridViewRow)ddledit1.NamingContainer;

                var ddltypecode_edit1 = (DropDownList)row1.FindControl("ddltypecode_edit");
                var ddlgroupmachine_edit = (DropDownList)row1.FindControl("ddlgroupmachine_edit");

                select_groupmachine(ddlgroupmachine_edit, int.Parse(ddltypecode_edit1.SelectedValue));

                break;
            case "ddltype_search":
                select_typecodemachine(ddltypecode_search, int.Parse(ddltype_search.SelectedValue));

                break;
            case "ddltypecode_search":
                select_groupmachine(ddlgroupcode_search, int.Parse(ddltypecode_search.SelectedValue));
                break;

            case "ddlchoose":
                linkBtnTrigger(lbladd);
                if (ddlchoose.SelectedValue == "1")
                {
                    div_adding.Visible = false;
                    div_import.Visible = true;
                    upload.Enabled = true;
                }
                else if (ddlchoose.SelectedValue == "2")
                {
                    div_adding.Visible = true;
                    div_import.Visible = false;
                }
                else
                {
                    div_adding.Visible = false;
                    div_import.Visible = false;
                }

                break;
        }
    }
    #endregion

    protected void ImportFileTooling(string filePath, string Extension, string isHDR, string fileName)
    {
        string conStr = String.Empty;
        conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
        conStr = String.Format(conStr, filePath, "Yes");
        OleDbConnection connExcel = new OleDbConnection(conStr);
        OleDbCommand cmdExcel = new OleDbCommand();
        OleDbDataAdapter oda = new OleDbDataAdapter();
        DataTable dt = new DataTable();

        cmdExcel.Connection = connExcel;
        connExcel.Open();
        DataTable dtExcelSchema;
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
        connExcel.Close();
        connExcel.Open();
        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
        oda.SelectCommand = cmdExcel;
        oda.Fill(dt);
        connExcel.Close();


        string idxCreated = String.Empty;
        for (var i = 0; i <= dt.Rows.Count - 1;)
        {
            if (dt.Rows[i][1].ToString().Trim() != "Material Description")
            {
                Tooling_Detail import = new Tooling_Detail();
                _dtenplan.BoxEN_ToolingList = new Tooling_Detail[1];

                import.choose_type = 1;
                import.TmcIDX = int.Parse(ddltypemachine.SelectedValue);
                import.TCIDX = int.Parse(ddltypecode.SelectedValue);
                import.GCIDX = int.Parse(ddlgroupmachine.SelectedValue);
                import.mat_number = int.Parse(dt.Rows[i][0].ToString());
                import.tooling_name_en = dt.Rows[i][1].ToString().Trim();
                import.tooling_name_th = dt.Rows[i][1].ToString().Trim();
                import.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

                _dtenplan.BoxEN_ToolingList[0] = import;
                _dtenplan = callServicePostENPlanning(urlnsert_MasterData, _dtenplan);

            }
            i++;
        }
    }

    protected void SetDefaultAdd()
    {
        txtnameth.Text = String.Empty;
        txtnameen.Text = String.Empty;
        txtmat.Text = String.Empty;
        ddStatusadd.SelectedValue = "1";
        SETBoxAllSearch.Visible = false;
        select_typemachine(ddltypemachine);
        ddltypemachine.SelectedValue = "0";
        ddltypecode.SelectedValue = "0";
        ddlgroupmachine.SelectedValue = "0";
        ddlchoose.SelectedValue = "0";
        upload.Enabled = false;
        div_adding.Visible = false;
    }

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":

                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                linkBtnTrigger(lbladd);
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                SETBoxAllSearch.Visible = true;
                break;

            case "btnAdd":
                if (ddlchoose.SelectedValue == "2")
                {
                    Insert_Master_List();
                }
                else if (ddlchoose.SelectedValue == "1")
                {
                    if (upload.HasFile)
                    {
                        string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                        string FileName = Path.GetFileName(upload.PostedFile.FileName);
                        string extension = Path.GetExtension(upload.PostedFile.FileName);
                        string newFileName = datetimeNow + extension.ToLower();
                        string folderPath = ConfigurationManager.AppSettings["path_tooling_import"];
                        string filePath = Server.MapPath(folderPath + newFileName);
                        if (extension.ToLower() == ".xls" || extension.ToLower() == ".xlsx")
                        {
                            upload.SaveAs(filePath);
                            ImportFileTooling(filePath, extension, "Yes", FileName);
                            File.Delete(filePath);

                        }
                        else
                        {
                            _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");
                        }
                    }
                }
                // Page.Response.Redirect(Page.Request.Url.ToString(), true);
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                SelectMasterList();
                SETBoxAllSearch.Visible = true;
                break;
            case "CmdDel":
                int m0toidx = int.Parse(cmdArg);
                ViewState["m0toidx"] = m0toidx;
                Delete_Master_List();
                SelectMasterList();

                break;

            case "btnsearch":
                SelectMasterList();
                break;
            case "BtnBack":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;
        }



    }
    #endregion
}