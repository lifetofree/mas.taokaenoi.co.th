﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="change_password.aspx.cs" Inherits="websystem_change_password" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MAS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

    <link rel="shortcut icon" href="./../images/Logo_TKN-01.png" type="image/x-icon" />
    <%--<link rel="icon" href="./../images/favicontkn.ico" type="image/x-icon" />--%>
    <link rel="icon" type="image/png" sizes="16x16" href="./../images/Logo_TKN-01.png" />

</head>
<body>
    <form id="formMaster" runat="server">
		<asp:ScriptManager ID="tsmMaster" runat="server"></asp:ScriptManager>
		<script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
		<script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
		<script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>

		<asp:UpdatePanel ID="upChangePassword" runat="server">
			<ContentTemplate>
				<asp:Literal ID="litDebug" runat="server"></asp:Literal>
				<div class="container">
					<div class="row vertical-offset-100">
						<div class="col-md-4 col-md-offset-4">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">เปลี่ยน Password</h3>
								</div>
								<div class="panel-body">
									<fieldset>
										<div class="form-group">
											<asp:TextBox ID="tbEmpPass" runat="server" CssClass="form-control" placeholder="Old Password" MaxLength="20" TextMode="Password"  ValidationGroup="formChangePassword"></asp:textbox>
                      						<asp:RequiredFieldValidator ID="rfvEmpPass" ValidationGroup="formChangePassword" runat="server" Display="None" SetFocusOnError="true" ControlToValidate="tbEmpPass" ErrorMessage="กรุณากรอกรหัสผ่านเดิม" />
                      						<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceEmpPass" TargetControlID="rfvEmpPass" HighlightCssClass="validatorCalloutHighlight" />
										</div>
										<div class="form-group">
											<asp:TextBox ID="tbNewPassword" runat="server" CssClass="form-control" placeholder="Enter New Password" MaxLength="20" TextMode="Password" ValidationGroup="formChangePassword"></asp:textbox>
											<asp:RequiredFieldValidator ID="rfvNewPassword" ValidationGroup="formChangePassword" runat="server" Display="None" SetFocusOnError="true" ControlToValidate="tbNewPassword" ErrorMessage="กรุณากรอกรหัสผ่านใหม่" />
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceNewPassword" TargetControlID="rfvNewPassword" HighlightCssClass="validatorCalloutHighlight" />
											<asp:RegularExpressionValidator ID="revNewPassword" runat="server" ErrorMessage="Password ต้องประกอบด้วย<br />- ตัวอักษรภาษาอังกฤษ<br />(a-z,A-Z)<br />- ตัวเลข(0-9)<br />- อักขระพิเศษ<br /> (เช่น @ $ ! #) <br />- มีความยาว 8-20 ตัวอักษร" Display="None" ControlToValidate="tbNewPassword" ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)(?=.*[$@$!%*#?&])^[a-zA-Z0-9$@$!%*#?&]{8,20}$" ValidationGroup="formChangePassword" />
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceNewPasswordFormat" TargetControlID="revNewPassword" HighlightCssClass="validatorCalloutHighlight" />
										</div>
										<div class="form-group">
											<asp:TextBox ID="tbNewPassword2" runat="server" CssClass="form-control" placeholder="Re-enter New Password" MaxLength="20" TextMode="Password" ValidationGroup="formChangePassword"></asp:textbox>
											<asp:RequiredFieldValidator ID="rfvNewPassword2" ValidationGroup="formChangePassword" runat="server" Display="None" SetFocusOnError="true" ControlToValidate="tbNewPassword2" ErrorMessage="กรุณากรอกรหัสผ่านอีกครั้ง" />
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceNewPassword2" TargetControlID="rfvNewPassword2" HighlightCssClass="validatorCalloutHighlight" />
											<asp:CompareValidator ID="cvNewPassword2" runat="server" ControlToCompare="tbNewPassword" ControlToValidate="tbNewPassword2" ErrorMessage="รหัสที่กรอกไม่ตรงกัน" Display="None" SetFocusOnError="true" ValidationGroup="formChangePassword" />
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceNewPassword2Compare" TargetControlID="cvNewPassword2" HighlightCssClass="validatorCalloutHighlight" />
										</div>
										<asp:Button ID="btnChangePassword" CssClass="btn btn-lg btn-success btn-block" runat="server" data-original-title="Send" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdChangePassword" Text="Send" ValidationGroup="formChangePassword" />
									</fieldset>
								</div>
							</div>
							<div ID="divShowError" runat="server" class="alert alert-danger" role="alert" visible="false">
								<strong>Error </strong><asp:Literal ID="litErrorCode" runat="server"></asp:Literal>
							</div>
						</div>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
		<div>
			<asp:UpdateProgress ID="UpdateProgress" runat="server">
				<ProgressTemplate>
					<div ID="modalPopup" runat="server" class="modalPopup">
						<div class="centerPopup">
								<asp:Image ID="imgWaiting" ImageUrl="~/masterpage/images/loading.gif" AlternateText="Processing" runat="server" Width="50" Height="50" />
						</div>
					</div>
				</ProgressTemplate>
			</asp:UpdateProgress>
		</div>
  	</form>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").tooltip({ selector: '[data-toggle=tooltip]' });
        });
    </script>
</body>
</html>
