﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ReportCHR.aspx.cs" Inherits="websystem_ITServices_ReportCHR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <asp:Literal ID="text" runat="server"></asp:Literal>
            <div id="BoxTabMenuIndex" runat="server">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand">Menu</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav ulActiveMenu" runat="server">
                            <li id="IndexList" runat="server">
                                <asp:LinkButton ID="lbindex" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand">ข้อมูลทั่วไป</asp:LinkButton>
                            </li>
                            <li id="Li1" runat="server">
                                <asp:LinkButton ID="lbladd" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand">สร้าง Change Request</asp:LinkButton>
                            </li>
                            <li id="Li2" runat="server">
                                <asp:LinkButton ID="lblcancel" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand">ยกเลิก Change Request</asp:LinkButton>
                            </li>
                            <li id="Li3" runat="server">
                                <asp:LinkButton ID="lblapprove" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand">รายการที่รออนุมัติ</asp:LinkButton>
                            </li>
                            <li id="Li5" runat="server">
                                <asp:LinkButton ID="lblmandays" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand">Mandays</asp:LinkButton>

                            </li>
                            <li id="Li4" runat="server">
                                <asp:LinkButton ID="lbladmin" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand">SAP Admin</asp:LinkButton>

                            </li>
                            <li id="Li6" runat="server">
                                <asp:LinkButton ID="lblreport" CssClass="btn_menulist" runat="server" CommandName="btnreport" OnCommand="btnCommand">สรุปรายงาน</asp:LinkButton>

                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">
                <asp:View ID="ViewReport" runat="server">

                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ค้นหารายงาน</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">

                                    <asp:Label ID="Label34" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทรายงาน : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddltypereport" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกประเภทรายงาน</asp:ListItem>
                                            <asp:ListItem Value="1">ตาราง</asp:ListItem>
                                            <asp:ListItem Value="2">กราฟ</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SearchReport" runat="server" Display="None" ControlToValidate="ddltypereport" Font-Size="11"
                                            ErrorMessage="กรุณาเลือกประเภทรายงาน"
                                            ValidationExpression="กรุณาเลือกประเภทรายงาน"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                    </div>
                                    <div id="div_chart" runat="server" visible="false">
                                        <asp:Label ID="Label30" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทกราฟ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlchartsap" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกประเภทกราฟ...</asp:ListItem>
                                                <asp:ListItem Value="1">Module/จำนวนครั้ง</asp:ListItem>
                                                <asp:ListItem Value="2">ประเภทการปิดงาน/ครั้ง</asp:ListItem>
                                                <asp:ListItem Value="3">Summary by Dept</asp:ListItem>
                                                <asp:ListItem Value="4">Downtimes</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SearchReport" runat="server" Display="None"
                                                ControlToValidate="ddlchartsap" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทกราฟ"
                                                ValidationExpression="กรุณาเลือกประเภทกราฟ" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                        </div>
                                        <p class="help-block"><font color="red">**</font></p>
                                    </div>
                                </div>


                                <div class="form-group" runat="server" id="div_checkdate">
                                    <asp:Label ID="Label8" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากวันที่ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlTypeSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            <asp:ListItem Text="เลือกประเภทการค้นหา ...." Value="00"></asp:ListItem>
                                            <asp:ListItem Text="วันที่รับงาน" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="วันที่ปิดงาน" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="วันที่แจ้งงาน" Value="3"></asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="rqyear" ValidationGroup="SearchReport" runat="server" Display="None"
                                            ControlToValidate="ddlTypeSearchDate" Font-Size="11"
                                            ErrorMessage="กรุณาเลือกประเภทปี"
                                            ValidationExpression="กรุณาเลือกประเภทการค้นหา" InitialValue="00" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="rqyear" Width="160" />
                                    </div>
                                    <p class="help-block"><font color="red">**</font></p>
                                </div>

                                <div class="form-group" id="panel_searchdate" runat="server">
                                    <asp:Label ID="Label9" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-2">
                                        <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            <asp:ListItem Value="00">เลือกเงื่อนไข</asp:ListItem>
                                            <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                            <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                            <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3 ">

                                        <div class='input-group date'>
                                            <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SearchReport" runat="server" Display="None"
                                                ControlToValidate="AddStartdate" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <asp:TextBox ID="AddEndDate" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                        </div>
                                    </div>
                                    <p class="help-block"><font color="red">**</font></p>
                                </div>

                                <div id="div_organite" runat="server">
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากองค์กร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlorgidx" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกองค์กร....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ค้นหาจากฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlrdeptidx" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                    </div>
                                </div>

                                <asp:Panel ID="panel_table" runat="server" Visible="false">


                                    <div class="form-group">
                                        <asp:Label ID="Label6" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากแผนก : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlrsecidx" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกแผนก....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                        <asp:Label ID="Label4" CssClass="col-sm-3 control-label" runat="server" Text="ค้นหารับงาน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlaccept" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกผู้รับงาน....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label10" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlchr" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกประเภทรายการ....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                        <asp:Label ID="Label18" CssClass="col-sm-3 control-label" runat="server" Text="ขั้นตอนดำเนินการ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlSearchStatus" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกขั้นตอนดำเนินการ....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <asp:Label ID="Label5" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาผู้ปิดงานหลัก : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlacceptsap" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกผู้ปิดงานหลัก....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                        <asp:Label ID="Label3" CssClass="col-sm-3 control-label" runat="server" Text="ค้นหาผู้ปิดงานที่เกี่ยวข้อง : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlacceptsap1" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกผู้ปิดงานที่เกี่ยวข้อง....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label24" CssClass="col-sm-2 control-label" runat="server" Text="เคสปิดงาน(Lv1) :  " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlSAPLV1" AutoPostBack="true" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                        </div>

                                        <asp:Label ID="Label25" CssClass="col-sm-3 control-label" runat="server" Text="เคสปิดงาน(Lv2) : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlSAPLV2" AutoPostBack="true" CssClass="form-control" runat="server">
                                            </asp:DropDownList>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label26" CssClass="col-sm-2 control-label" runat="server" Text="เคสปิดงาน(Lv3) :  " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlSAPLV3" AutoPostBack="true" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>




                                </asp:Panel>

                                <asp:Panel ID="panel_chart" runat="server" Visible="false">
                                    <div class="form-group" id="div_month" runat="server" visible="false">
                                        <asp:Label ID="Label814" runat="server" Text="เดือน" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-3" runat="server">
                                            <asp:DropDownList ID="ddlmonth" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="00">Select Month....</asp:ListItem>
                                                <asp:ListItem Value="1">มกราคม</asp:ListItem>
                                                <asp:ListItem Value="2">กุมภาพันธ์</asp:ListItem>
                                                <asp:ListItem Value="3">มีนาคม</asp:ListItem>
                                                <asp:ListItem Value="4">เมษายน</asp:ListItem>
                                                <asp:ListItem Value="5">พฤกษภาคม</asp:ListItem>
                                                <asp:ListItem Value="6">มิถุนายน</asp:ListItem>
                                                <asp:ListItem Value="7">กรกฎาคม</asp:ListItem>
                                                <asp:ListItem Value="8">สิงหาคม</asp:ListItem>
                                                <asp:ListItem Value="9">กันยายน</asp:ListItem>
                                                <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                            </asp:DropDownList>
                                           <%-- <asp:RequiredFieldValidator ID="rqmonth" ValidationGroup="SearchReport" runat="server" Display="None"
                                                ControlToValidate="ddlmonth" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกเดือน"
                                                ValidationExpression="กรุณาเลือกเดือน" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="rqmonth" Width="160" />
                                   --%>     </div>
                                        <asp:Label ID="Label165" runat="server" Text="ปี" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-3" runat="server">
                                            <asp:DropDownList ID="ddlyear" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="00">Select Year....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label20" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาผู้ปิดงานหลัก : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddladminaccept" runat="server" CssClass="form-control">
                                            </asp:DropDownList>

                                        </div>

                                        <asp:Label ID="Label17" CssClass="col-sm-3 control-label" runat="server" Text="ค้นหาผู้ปิดงานที่เกี่ยวข้อง : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddladminaccept1" runat="server" CssClass="form-control">
                                            </asp:DropDownList>

                                        </div>
                                    </div>


                                </asp:Panel>


                                <div class="form-group" id="div_button" runat="server" visible="false">
                                    <div class="col-sm-4 col-sm-offset-8">
                                        <asp:LinkButton ID="btnsearch" CssClass="btn btn-success btn-sm" ValidationGroup="SearchReport" runat="server" CommandName="btnsearch" OnCommand="btnCommand" title="Search"><i class="fa fa-search"></i> Search</asp:LinkButton>
                                        <asp:LinkButton ID="btnexport" CssClass="btn btn-primary  btn-sm" runat="server" CommandName="btnexport" OnCommand="btnCommand" title="Export"><i class="glyphicon glyphicon-export"></i> Export Excel</asp:LinkButton>
                                        <asp:LinkButton ID="btnrefresh" CssClass="btn btn-info  btn-sm" runat="server" CommandName="btnrefresh" OnCommand="btnCommand" title="Refresh"><i class="glyphicon glyphicon-refresh"></i> Refresh</asp:LinkButton>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>


                    <div class="panel panel-primary" id="grid1" runat="server" visible="false">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-align-justify"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <asp:GridView ID="GvCHR"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    DataKeyNames="u0idx"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnPageIndexChanging="Master_PageIndexChanging">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center;">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>


                                    <Columns>
                                        <asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                </small>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รหัสเอกสาร." HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("doc_code") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="TR Number." HeaderStyle-Font-Size="Smaller" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label60" runat="server"> หลัก: </asp:Label></strong>
                                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("Tcode1") %>' />
                                                    <hr />
                                                    <strong>
                                                        <asp:Label ID="Label61" runat="server"> เสริม: </asp:Label></strong>
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("Tcode2") %>' />


                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลผู้สร้าง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                            <ItemTemplate>

                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label60" runat="server"> องค์กร: </asp:Label></strong>
                                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label61" runat="server"> ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                    <br />

                                                    <strong>
                                                        <asp:Label ID="Label51" runat="server"> แผนก: </asp:Label></strong>
                                                    <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("SecNameTH") %>' />

                                                    <br />

                                                    <strong>
                                                        <asp:Label ID="Label16" runat="server"> ชื่อผู้สร้าง: </asp:Label></strong>
                                                    <asp:Literal ID="Literal11" runat="server" Text='<%# Eval("EmpName") %>' />
                                                </small>


                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ระบบ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblsecname" runat="server" Text='<%# Eval("System_name") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เงื่อนไขในการแจ้ง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblsecname" runat="server" Text='<%# Eval("Typename") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Usersap" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("Usersap") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ผู้อนุมัติ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label60" runat="server"> ผู้อนุมัติลำดับที่ 1: </asp:Label></strong>
                                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("Approve1") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label61" runat="server"> ผู้อนุมัติลำดับที่ 2: </asp:Label></strong>
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("Approve2") %>' />
                                                    <br />

                                                    <strong>
                                                        <asp:Label ID="Label51" runat="server"> ผู้อนุมัติลำดับที่ 3: </asp:Label></strong>
                                                    <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("Approve3") %>' />
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="เจ้าหน้าที่ SAP" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label7" runat="server"> เจ้าหน้าที่ SAP รับงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("AdminName") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label60" runat="server"> เจ้าหน้าที่ SAP หลัก : </asp:Label></strong>
                                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("AdminMain") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label61" runat="server"> เจ้าหน้าที่ SAP เสริม: </asp:Label></strong>
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("AdminAdd") %>' />

                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ระบบเดิม" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("old_sys") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ระบบใหม่" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("new_sys") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="comment" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("comment") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ขั้นตอนดำเนินการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("node_desc") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ปิดงาน" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label7" runat="server"> ปิดงานLV1 SAPหลัก: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Name_Code1") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label60" runat="server"> ปิดงานLV2 SAPหลัก: </asp:Label></strong>
                                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("TypeClose1_name") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label61" runat="server"> ปิดงานLV3 SAPหลัก: </asp:Label></strong>
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("TypeClose2_name") %>' />
                                                    <br />

                                                    <strong>
                                                        <asp:Label ID="Label14" runat="server">mandays SAPหลัก: </asp:Label></strong>
                                                    <asp:Literal ID="Literal9" runat="server" Text='<%# Eval("mandays") %>' />

                                                    <hr />
                                                    <strong>
                                                        <asp:Label ID="Label11" runat="server"> ปิดงานLV1 SAPเสริม: </asp:Label></strong>
                                                    <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("Name_Code11") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label12" runat="server"> ปิดงานLV2 SAPเสริม: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("close2") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label13" runat="server"> ปิดงานLV3 SAPเสริม: </asp:Label></strong>
                                                    <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("close3") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label15" runat="server">mandays SAPเสริม: </asp:Label></strong>
                                                    <asp:Literal ID="Literal10" runat="server" Text='<%# Eval("mandays_1") %>' />

                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="วันที่ดำเนินการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="12%">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label7" runat="server"> วันที่แจ้ง: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("datecreate") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label60" runat="server"> วันที่รับงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("dategetjob1") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label61" runat="server"> วันที่ปิดงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("dateendjob") %>' />

                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Downtime" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("Downtime") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>



                                    </Columns>

                                </asp:GridView>


                            </div>
                        </div>
                    </div>



                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="true">
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับที่" ItemStyle-Width="100">
                                <ItemTemplate>
                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>




                    <div class="panel panel-info" id="grant" runat="server" visible="false">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                        </div>
                        <div class="panel-body">
                            <asp:Literal ID="litReportChart" runat="server" />
                          </div>
                    </div>

                     <div id="gvchart_priority" runat="server" visible="false">
                        <div class="col-lg-12">

                            <asp:GridView ID="GvChartSAP"
                                runat="server"
                                AutoGenerateColumns="false"
                                DataKeyNames="u0idx"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="info"
                                HeaderStyle-Height="40px"
                                OnRowDataBound="Master_RowDataBound"
                                ShowFooter="true">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center;">Data Cannot Be Found</div>
                                </EmptyDataTemplate>


                                <Columns>
                                    <asp:TemplateField HeaderText="เดือน." HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="lit_month" runat="server" Text='<%# Eval("monthdowntime") %>' /></small>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Downtime / Hours" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="lit_time" runat="server" Text='<%# Eval("sumtime") %>'></asp:Literal>
                                            </small>
                                        </ItemTemplate>
                                        <EditItemTemplate />
                                    </asp:TemplateField>

                                </Columns>

                            </asp:GridView>

                        </div>

                    </div>

                </asp:View>






            </asp:MultiView>

        </ContentTemplate>

        <Triggers>
            <asp:PostBackTrigger ControlID="btnsearch" />
            <asp:PostBackTrigger ControlID="btnexport" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

