﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_management.aspx.cs" Inherits="websystem_el_management" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    
    

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            
            <div class="row">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary pull-right" runat="server"
                    data-toggle="tooltip" title="สร้างE-mail"
                    CommandName="btnToInsert" OnCommand="btnCommand">
                     <i class="fa fa-plus-square"></i> สร้าง Role</asp:LinkButton>
            </div>

            <asp:Panel ID="pnlsearch" runat="server">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Role</label>
                                <asp:TextBox ID="txtFilterKeyword" runat="server"
                                    CssClass="form-control"
                                    placeholder="E-mail..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnFilter" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnFilter" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="m0_email_idx"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="E-mail" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="email_name" runat="server" Text='<%# Eval("email_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="email_status" runat="server" Text='<%# getStatus((int)Eval("email_status")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("m0_email_idx") %>'
                                    OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>

        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                      
                        <strong>&nbsp; Role
                        </strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-8 col-md-offset-2">



                    </div>
                </div>
            </div>
        </asp:View>
        <!-- End Insert Form -->


    </asp:MultiView>

</asp:Content>
