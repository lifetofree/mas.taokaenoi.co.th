﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_hr_rbk_m0_conference : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_roombooking _data_roombooking = new data_roombooking();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    //-- employee --//

    //-- roombooking --//
    static string _urlSetRbkm0Conference = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0Conference_Devices"];
    static string _urlGetRbkm0Conference = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Conference_Devices"];
    static string _urlDeleteRbkm0Conference = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteRbkm0Conference_Devices"];


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectConferenceDetail();

        }
        linkBtnTrigger(btnAddConference);
    }
    #endregion Page Load

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region Command SQL
    protected void SelectConferenceDetail()
    {

        data_roombooking data_m0_con_detail = new data_roombooking();
        rbk_m0_conference_detail m0_con_detail = new rbk_m0_conference_detail();
        data_m0_con_detail.rbk_m0_conference_list = new rbk_m0_conference_detail[1];

        m0_con_detail.condition = 0;

        data_m0_con_detail.rbk_m0_conference_list[0] = m0_con_detail;

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0_food_detail));
        data_m0_con_detail = callServicePostRoomBooking(_urlGetRbkm0Conference, data_m0_con_detail);

        setGridData(GvConferenceInRoom, data_m0_con_detail.rbk_m0_conference_list);

    }

    protected void InsertConferenceDetail(int m0_conidx, bool piccheck, bool soundcheck, int qtymic, string people_desc, string conferencename, int constatus, int cempidx)
    {

        data_roombooking data_m0_con_detail = new data_roombooking();
        rbk_m0_conference_detail m0_con_detail = new rbk_m0_conference_detail();
        data_m0_con_detail.rbk_m0_conference_list = new rbk_m0_conference_detail[1];

        m0_con_detail.conference_name = conferencename;
        m0_con_detail.m0_conidx = m0_conidx;
        if (piccheck == true)
        {
            m0_con_detail.pic_check = 1;

        }
        else
        {
            m0_con_detail.pic_check = 0;

        }

        if (soundcheck == true)
        {
            m0_con_detail.sound_check = 1;

        }
        else
        {
            m0_con_detail.sound_check = 0;

        }

        m0_con_detail.qty_mic = qtymic;
        m0_con_detail.people_desc = people_desc;
        m0_con_detail.constatus = constatus;
        m0_con_detail.cemp_idx = cempidx;

        data_m0_con_detail.rbk_m0_conference_list[0] = m0_con_detail;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_con_detail));
        data_m0_con_detail = callServicePostRoomBooking(_urlSetRbkm0Conference, data_m0_con_detail);

        if (data_m0_con_detail.return_code == 00)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            if (m0_conidx == 0)
            {
                if (UploadFile.HasFile)
                {
                    string getPathfile = ConfigurationManager.AppSettings["path_room_conference"];
                    string fileName_upload = data_m0_con_detail.return_code.ToString();
                    string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                    if (!Directory.Exists(filePath_upload))
                    {
                        Directory.CreateDirectory(filePath_upload);
                    }
                    string extension = Path.GetExtension(UploadFile.FileName);

                    UploadFile.SaveAs(Server.MapPath(getPathfile + fileName_upload) + "\\" + fileName_upload + extension);

                }
            }
        }


    }
    #endregion

    #region callService 

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_roombooking callServicePostRoomBooking(string _cmdUrl, data_roombooking _data_roombooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_roombooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_roombooking = (data_roombooking)_funcTool.convertJsonToObject(typeof(data_roombooking), _localJson);


        return _data_roombooking;
    }
    #endregion callService Functions

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region Directories_File URL

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr = dt.NewRow();
        DataColumn dc = new DataColumn("FileName", typeof(string));
        dc = new DataColumn("Download", typeof(string));
        dt.Columns.Add("FileName");
        dt.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                }
            }

            ViewState["path"] = "1";
        }
        catch
        {
            ViewState["path"] = "0";
        }
    }

    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvConferenceInRoom":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    if (GvConferenceInRoom.EditIndex != e.Row.RowIndex)
                    {
                        Label lbl_m0_conidx = (Label)e.Row.FindControl("lbl_m0_conidx");
                        Label lbl_con_status = (Label)e.Row.FindControl("lbl_con_status");
                        Label con_statusOnline = (Label)e.Row.FindControl("con_statusOnline");
                        Label con_statusOffline = (Label)e.Row.FindControl("con_statusOffline");
                        Label lbl_pic_check = (Label)e.Row.FindControl("lbl_pic_check");
                        Label lbl_sound_check = (Label)e.Row.FindControl("lbl_sound_check");
                        CheckBox chkpic_show = (CheckBox)e.Row.FindControl("chkpic_show");
                        CheckBox chksound_show = (CheckBox)e.Row.FindControl("chksound_show");
                        Image image = (Image)e.Row.FindControl("image");

                        ViewState["vs_conference_status"] = lbl_con_status.Text;


                        if (ViewState["vs_conference_status"].ToString() == "1")
                        {
                            con_statusOnline.Visible = true;
                        }
                        else if (ViewState["vs_conference_status"].ToString() == "0")
                        {
                            con_statusOffline.Visible = true;
                        }

                        if (lbl_pic_check.Text == "1")
                        {
                            chkpic_show.Checked = true;
                        }
                        else
                        {
                            chkpic_show.Checked = false;
                        }

                        if (lbl_sound_check.Text == "1")
                        {
                            chksound_show.Checked = true;
                        }
                        else
                        {
                            chksound_show.Checked = false;
                        }

                        string getPathLotus = ConfigurationSettings.AppSettings["path_room_conference"];
                        string path = lbl_m0_conidx.Text + "/";
                        string namefile = lbl_m0_conidx.Text + ".jpg";
                        string pic = getPathLotus + path + namefile;

                        string filePathLotus = Server.MapPath(getPathLotus + lbl_m0_conidx.Text);
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
                        SearchDirectories(myDirLotus, lbl_m0_conidx.Text);

                        if (ViewState["path"].ToString() != "0")
                        {
                            image.Visible = true;
                            image.ImageUrl = pic;
                            image.Height = 150;
                            image.Width = 150;
                        }
                        else
                        {
                            image.Visible = false;
                        }


                    }
                    else
                    {
                        TextBox txtshow_pic = (TextBox)e.Row.FindControl("txtshow_pic");
                        TextBox txtshow_sound = (TextBox)e.Row.FindControl("txtshow_sound");
                        CheckBox chkpic_edit = (CheckBox)e.Row.FindControl("chkpic_edit");
                        CheckBox chksound_edit = (CheckBox)e.Row.FindControl("chksound_edit");

                        if (txtshow_pic.Text == "1")
                        {
                            chkpic_edit.Checked = true;
                        }
                        else
                        {
                            chkpic_edit.Checked = false;
                        }

                        if (txtshow_sound.Text == "1")
                        {
                            chksound_edit.Checked = true;
                        }
                        else
                        {
                            chksound_edit.Checked = false;
                        }

                    }
                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    btnAddConference.Visible = true;
                    FvInsert.Visible = false;

                }

                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvConferenceInRoom":
                GvConferenceInRoom.EditIndex = e.NewEditIndex;
                SelectConferenceDetail();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvConferenceInRoom":

                var txt_m0_conidx_edit = (TextBox)GvConferenceInRoom.Rows[e.RowIndex].FindControl("txt_m0_conidx_edit");
                var txt_con_name_edit = (TextBox)GvConferenceInRoom.Rows[e.RowIndex].FindControl("txt_con_name_edit");
                var chkpic_edit = (CheckBox)GvConferenceInRoom.Rows[e.RowIndex].FindControl("chkpic_edit");
                var chksound_edit = (CheckBox)GvConferenceInRoom.Rows[e.RowIndex].FindControl("chksound_edit");
                var txtqtymic = (TextBox)GvConferenceInRoom.Rows[e.RowIndex].FindControl("txtqtymic");
                var txtpeople_desc = (TextBox)GvConferenceInRoom.Rows[e.RowIndex].FindControl("txtpeople_desc");
                var ddlcon_status_edit = (DropDownList)GvConferenceInRoom.Rows[e.RowIndex].FindControl("ddlcon_status_edit");


                GvConferenceInRoom.EditIndex = -1;

                InsertConferenceDetail(int.Parse(txt_m0_conidx_edit.Text), chkpic_edit.Checked, chksound_edit.Checked, int.Parse(txtqtymic.Text), txtpeople_desc.Text, txt_con_name_edit.Text, int.Parse(ddlcon_status_edit.SelectedValue), _emp_idx);
                SelectConferenceDetail();

                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvConferenceInRoom":
                GvConferenceInRoom.EditIndex = -1;
                SelectConferenceDetail();

                btnAddConference.Visible = true;
                FvInsert.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvConferenceInRoom":
                GvConferenceInRoom.PageIndex = e.NewPageIndex;
                SelectConferenceDetail();
                break;

        }
    }
    #endregion

    #region Trigger
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void linkFvTrigger(FormView linkFvID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerFv = new PostBackTrigger();
        triggerFv.ControlID = linkFvID.UniqueID;
        updatePanel.Triggers.Add(triggerFv);
    }
    #endregion

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        CheckBox chkpic = (CheckBox)FvInsert.FindControl("chkpic");
        CheckBox chksound = (CheckBox)FvInsert.FindControl("chksound");
        TextBox txtcon_name = (TextBox)FvInsert.FindControl("txtcon_name");
        TextBox txtqtymic = (TextBox)FvInsert.FindControl("txtqtymic");
        TextBox txtpeople_desc = (TextBox)FvInsert.FindControl("txtpeople_desc");
        DropDownList ddlConStatus = (DropDownList)FvInsert.FindControl("ddlConStatus");

        switch (cmdName)
        {
            case "cmdAddConference":

                btnAddConference.Visible = false;
                linkFvTrigger(FvInsert);
                setFormData(FvInsert, FormViewMode.Insert, null);
                FvInsert.Visible = true;

                break;

            case "CmdSave":
                InsertConferenceDetail(0, chkpic.Checked, chksound.Checked, int.Parse(txtqtymic.Text), txtpeople_desc.Text, txtcon_name.Text, int.Parse(ddlConStatus.SelectedValue), _emp_idx);
                SelectConferenceDetail();
                btnAddConference.Visible = true;
                setFormData(FvInsert, FormViewMode.Insert, null);
                FvInsert.Visible = false;
                break;

            case "CmdCancel":
                btnAddConference.Visible = true;
                FvInsert.Visible = false;

                break;

            case "CmdDelete":

                /*  int food_del = int.Parse(cmdArg);

                  data_roombooking data_m0_food_del = new data_roombooking();
                  rbk_m0_food_detail m0_food_del = new rbk_m0_food_detail();
                  data_m0_food_del.rbk_m0_food_list = new rbk_m0_food_detail[1];

                  m0_food_del.food_idx = food_del;
                  m0_food_del.cemp_idx = _emp_idx;

                  data_m0_food_del.rbk_m0_food_list[0] = m0_food_del;
                  data_m0_food_del = callServicePostRoomBooking(_urlSetRbkm0FoodDel, data_m0_food_del);

                  SelectFoodDetail();
                  //Gv_select_unit.Visible = false;
                  btnAddFood.Visible = true;
                  FvInsert.Visible = false; */
                break;


        }
    }
    #endregion btnCommand

}