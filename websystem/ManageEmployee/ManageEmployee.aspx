﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ManageEmployee.aspx.cs" Inherits="websystem_ManageEmployee_ManageEmployee" Debug="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="check_XML" runat="server"></asp:Literal>
    <asp:Literal ID="check_XML1" runat="server"></asp:Literal>
    <asp:Literal ID="check_XML2" runat="server"></asp:Literal>

    <%--//ไว้ตรวจสอบ XML ตอนจะส่งเข้า POX--%>
    <%-- <div class="panel-body">--%>
    <div id="divMenu_bar" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav ulActiveMenu" runat="server">
                    <li id="IndexList" runat="server">
                        <asp:LinkButton ID="lbMenu0" runat="server"
                            CommandName="btnViewSelected"
                            OnCommand="btnCommand" Text="ข้อมูลทั่วไป" />
                    </li>
                    <%--      <li id="ListReport" runat="server">
                        <asp:LinkButton ID="lbMenureport" runat="server"
                            CommandName="tabreportExcel"
                            OnCommand="btnCommand" Text="รายงาน" />
                    </li>--%>
                </ul>
            </div>
        </nav>
    </div>
    <!-- menu -->

    <asp:UpdatePanel ID="upload_profile1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">
                    <div class="col-lg-12" runat="server" id="BoxSearch">
                        <asp:Label ID="emp_idx_last" runat="server" Text='<%# Eval("emp_idx_last") %>'></asp:Label>

                        <div class="form-horizontal" role="form">
                            <h4><span class="label label-default">ค้นหาข้อมูล</span></h4>
                            <div class="panel panel-danger">
                                <div class="form-group">
                                    <br />
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-11">
                                        <small class="list-group-item-heading text_right"><font color="#1A5276">
                                            <b>* Select corporate parties Or fill out the 'fill' before searching. </b>
                                            *เลือกองค์กร,ฝ่าย หรือกรอกข้อมูลในช่อง 'กรอกข้อมูล' ก่อนทำการค้นหา</font></small>
                                    </div>
                                    <br />
                                    <br />
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-1">
                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                            <b>Organization  </b></small></font>
                                        <small>
                                            <p class="list-group-item-text">เลือกองค์กร</p>
                                        </small>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:DropDownList ID="ddl_Select_Organization_search"
                                            runat="server" CssClass="form-control fa-align-left"
                                            AutoPostBack="true" OnSelectedIndexChanged="DdSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1">
                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                            <b>Department  </b></small></font>
                                        <small>
                                            <p class="list-group-item-text">เลือกฝ่าย</p>
                                        </small>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:DropDownList ID="ddl_Select_Department_search"
                                            runat="server" CssClass="form-control fa-align-left" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1">
                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                            <b>completetext </b></small></font>
                                        <small>
                                            <p class="list-group-item-text">กรอกข้อมูล</p>
                                        </small>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="txt_search" runat="server" placeholder="กรอกข้อมูล..."
                                            CssClass="form-control left" ValidationGroup="formInsert"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:LinkButton ID="Search" runat="server" CssClass="btn btn-info btn-sm"
                                            data-toggle="tooltip" title="ค้นหา" CommandName="btn_search_data"
                                            OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i> ค้นหา</asp:LinkButton>
                                    </div>
                                    <br />
                                    <br />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-4">
                                    <asp:LinkButton ID="BoxButtonAddEmployee"
                                        CssClass="btn btn-success" data-toggle="tooltip" title="เพิ่มข้อมูลพนักงาน"
                                        runat="server" CommandName="BtnAddEmployee" OnCommand="btnCommand">
                                        <i class="glyphicon glyphicon-plus-sign"></i> เพิ่มข้อมูลพนักงาน</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%-------------- BoxSearch End--------------%>
                    <%-------------------------------------------------ข้อมูลที่เป็น GrideView----------------------------------------%>
                    <div class="form-group">

                        <h4>
                            <label class="col-sm-3 control-label">รายชื่อพนักงานทั้งหมด</label></h4>
                    </div>
                    <h4>&nbsp;</h4>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <asp:GridView ID="gvEmployee"
                                runat="server"
                                AutoGenerateColumns="false"
                                DataKeyNames="emp_idx"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="info"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                PageSize="10"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowDataBound="Master_RowDataBound">
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                    FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูลพนักงาน</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <div style="text-align: center; padding-top: 57px;">
                                                <asp:Label ID="emp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>' />
                                                <%# (Container.DataItemIndex +1) %>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <div style="padding-top: 60px;">
                                                    <asp:Label ID="lb_detailsd" runat="server"
                                                        CssClass="col-sm-12" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                </div>
                                            </small>
                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="รายละเอียดพนักงาน" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="Description" runat="server"> 
                                                     <p><b>ชื่อ - สกุล: &nbsp;</b> <span><%# Eval("prefix_th") %>&nbsp;<%# Eval("emp_firstname_th") %>
                                                    &nbsp;&nbsp;<%# Eval("emp_lastname_th") %></span>
                                        </p>
                                            </asp:Label>
                                            <p>
                                                <asp:Label ID="lborgNameShiftTime" runat="server" CssClass="col-sm-12">
                                                             <p><b>องค์กร:</b> &nbsp;<%# Eval("org_name_th") %></p>
                                                </asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="LabelshiftOrgtIDX" runat="server" Visible="false"
                                                    Text='<%# Eval("org_idx") %>'></asp:Label>
                                                <asp:Label ID="labeltextOrgshiftdefault" runat="server"
                                                    CssClass="col-sm-12" Visible="false">
                                                                  <p><b>องค์กร: &nbsp;</b><font color="sandybrown">ยังไม่มีข้อมูลองค์กร</font></p>
                                                </asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="lbdeptNameShiftTime" runat="server" CssClass="col-sm-12">
                                                             <p><b>ฝ่าย:</b> &nbsp;<%# Eval("dept_name_th") %></p>
                                                </asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="LabelshiftDeptIDX" runat="server" Visible="false"
                                                    Text='<%# Eval("rdept_idx") %>'></asp:Label>
                                                <asp:Label ID="labeltextDeptshiftdefault" runat="server"
                                                    CssClass="col-sm-12" Visible="false">
                                                                  <p><b>ฝ่าย: &nbsp;</b><font color="sandybrown">ยังไม่มีข้อมูลฝ่าย</font></p>
                                                </asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="lbsecNameShiftTime" runat="server" CssClass="col-sm-12">
                                                             <p><b>แผนก:</b> &nbsp;<%# Eval("sec_name_th") %></p>
                                                </asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="LabelshiftSecIDX" runat="server" Visible="false"
                                                    Text='<%# Eval("rsec_idx") %>'></asp:Label>
                                                <asp:Label ID="labeltextSecshiftdefault" runat="server"
                                                    CssClass="col-sm-12" Visible="false">
                                                                  <p><b>แผนก: &nbsp;</b><font color="sandybrown">ยังไม่มีข้อมูลแผนก</font></p>
                                                </asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="lbPossitionNameShiftTime" runat="server" CssClass="col-sm-12">
                                                             <p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("pos_name_th") %></p>
                                                </asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="LabelshiftPosIDX" runat="server" Visible="false"
                                                    Text='<%# Eval("rpos_idx") %>'></asp:Label>
                                                <asp:Label ID="labeltextPosShiftdefault" runat="server"
                                                    CssClass="col-sm-12" Visible="false">
                                                                  <p><b>ตำแหน่ง: &nbsp;</b><font color="sandybrown">ยังไม่มีข้อมูลตำแหน่ง</font></p>
                                                </asp:Label>
                                            </p>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="label_statusemp" runat="server" Visible="false"
                                                CssClass="col-sm-12" Text='<%# Eval("emp_status") %>'></asp:Label>
                                            <div style="text-align: center; color: red; margin-top: 50px;">
                                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                                    data-toggle="tooltip" data-placement="top" title="พ้นสภาพพนักงาน"
                                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red; padding-top: 5px;">
                                                    <small><i class="glyphicon glyphicon-remove-sign"></i>&nbsp;พ้นสภาพพนักงาน</small>
                                                </asp:Label>
                                                <asp:Label ID="status_online" runat="server" Visible="false"
                                                    data-toggle="tooltip" data-placement="top" title="เป็นพนักงาน" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green; padding-top: 5px;">
                                                    <small><i class="glyphicon glyphicon-ok-sign"></i>&nbsp;เป็นพนักงาน</small>
                                                </asp:Label>
                                               <%-- <asp:Label ID="typeShiftTime" runat="server" Visible="false"
                                                    CssClass="col-sm-12" Text='<%# Eval("idxShiftTime") %>'></asp:Label>
                                                <asp:Label ID="NoneShiftTime" runat="server" Visible="true"
                                                    data-toggle="tooltip" data-placement="top"
                                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <small><p>ยังไม่มีการกำหนดตารางงาน</p></small></div>
                                                </asp:Label>
                                                <asp:Label ID="haveShiftTime" runat="server" Visible="true"
                                                    data-toggle="tooltip" data-placement="top"
                                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                   <small><p><b>ประเภท : </b> <%# Eval("shift_time") %> &nbsp;</p></small>
                                                </asp:Label>--%>
                                            </div>
                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="การจัดการ" ItemStyle-HorizontalAlign="center"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="up7" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div style="margin-top: 60px;">
                                                        <asp:LinkButton ID="lbView" CssClass="btn btn-primary btn-sm"
                                                            runat="server" data-toggle="tooltip" data-placement="top" title="แก้ไขข้อมูล"
                                                            CommandArgument='<%# Eval("emp_idx")+ ";" + Eval("rsec_idx") + ";" + Eval("rpos_idx")+ ";" + Eval("rdept_idx") + ";" + Eval("org_idx") %>'
                                                            CommandName="cmdedit" OnCommand="btnCommand"><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="reset_" CssClass="btn btn-default btn-sm" runat="server" data-toggle="tooltip"
                                                            data-placement="top" title="รีเซ็ตรหัสผ่าน" CommandName="cmdreset_pass" OnCommand="btnCommand"
                                                            CommandArgument='<%#Eval("emp_idx") %>' OnClientClick="if(!confirm('ยืนยันรีเซ็ตรหัสผ่าน?')) return false;">
                                                        <i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="delete" CssClass="btn btn-danger btn-sm" runat="server" data-toggle="tooltip"
                                                            title="ลบ" CommandName="cmddelete" OnCommand="btnCommand" CommandArgument='<%#Eval("emp_idx") %>'
                                                            OnClientClick="if(!confirm('ยืนยันการลบข้อมูล?')) return false;">
                                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                    </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="lbView" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </asp:View>

                <asp:View ID="ViewAddDataEmployee" runat="server">

                    <div class="col-lg-12" runat="server" id="Div1">
                        <asp:LinkButton ID="LinkButton1" CssClass="btn btn-primary" data-toggle="tooltip"
                            title="กลับไปก่อนหน้า" runat="server" CommandName="btnBack" OnCommand="btnCommand">
                            <i class="glyphicon glyphicon-circle-arrow-left"></i>&nbsp; กลับ
                        </asp:LinkButton>
                        <h5>&nbsp;</h5>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-picture"></i>
                                    <strong>&nbsp; เพิ่มภาพพนักงาน</strong></h3>
                            </div>
                            <div class="panel-body">
                                <asp:Repeater ID="Repeater_picture" runat="server">
                                    <ItemTemplate>
                                        <div class="row">
                                            <center><asp:Image ID="img_profile" runat="server"
                                                 ImageUrl='<%# retrunpat((int)Eval("emp_idx_last"))%>' 
                                                style="width: 150px; height: 150px" class="img-thumbnail">
                                                    </asp:Image>
                                            </center>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <center>
                                    <div id="ImagProfile_default" runat="server">
                                            <img class="media-object img-thumbnail"
                                                src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>'
                                                width="150px" height="150px" style="margin-right: 8px; margin-top: -5px;">
                                        </div>
                                       <%-- </asp:Panel>--%>
                                </center>
                                <h5>&nbsp;</h5>
                                <center><asp:FileUpload ID="upload_profile_pic11" AutoPostBack="true"
                                     ClientIDMode="Static" CssClass="btn btn-primary" 
                                    runat="server" accept="jpg" />
                                </center>
                                <h5>&nbsp;</h5>
                                <center><asp:LinkButton ID="btn_save_file1" runat="server"
                                     CssClass="btn btn-warning btn-sm" data-toggle="tooltip" 
                                    title="อัพโหลดภาพ" CommandName="savefile_picture" OnCommand="btnCommand">
                                    <i class="glyphicon glyphicon-picture"></i> อัพโหลดภาพ
                                        </asp:LinkButton>
                                </center>
                                <center><font color="#1A5276"><small class="text_right">
                                    <b>Picture Employee </b><p>ภาพพนักงาน</small></font>
                                </center>
                            </div>
                        </div>

                        <asp:Panel ID="BoxAddData" runat="server">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-plus-sign"></i>
                                        <i class="glyphicon glyphicon-user"></i><strong>&nbsp; เพิ่มข้อมูลพนักงาน</strong></h3>
                                </div>
                                <div class="panel-body">

                                    <asp:FormView ID="fvAddDataEmployee" OnDataBound="FormView_DataBound"
                                        runat="server" DefaultMode="Insert" Width="100%">
                                        <InsertItemTemplate>
                                            <%------------------------------------------ เพิ่มข้อมูลเบื้องต้น------------------------------------------%>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <i class="glyphicon glyphicon-file"></i>&nbsp;
                                                    <b>Profile</b> (ข้อมูลพนักงานเบื้องต้น)
                                                </div>
                                                <div class="panel-body">

                                                    <asp:Label ID="emplast" runat="server"></asp:Label>
                                                    <div class="form-horizontal" role="form">
                                                        <%--    <hr />--%>

                                                        <div class="form-group">

                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Employee Type </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ประเภทพนักงาน</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddEmpType" runat="server"
                                                                    CssClass="form-control fa-align-left">
                                                                    <asp:ListItem Value="0">เลือกประเภทพนักงาน</asp:ListItem>
                                                                    <asp:ListItem Value="1">รายวัน</asp:ListItem>
                                                                    <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                                                                    <asp:ListItem Value="3">รายเดือน จ่าย 2 ครั้ง</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Employee In </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">วันที่เริ่มเข้าทำงาน</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtEmpin" runat="server" placeholder="เลือกวันเริ่มงาน..."
                                                                    CssClass="form-control from-date-datepicker" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Organization </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">บริษัท</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddOrganiztion" runat="server"
                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b> Sarary</b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">เงินเดือนพนักงาน</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtSarary" runat="server" CssClass="form-control left"
                                                                    ValidationGroup="formInsert" placeholder="กรุณากรอกเงินเดือน..."></asp:TextBox>
                                                            </div>
                                                            <asp:RegularExpressionValidator ID="RegulartxtSarary"
                                                                runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                                                Display="None" ControlToValidate="txtSarary" ValidationExpression="^[0-9',.\s]{1,10}$"
                                                                ValidationGroup="formInsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtSarary" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegulartxtSarary" Width="160" />
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Department </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ฝ่าย</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddDepartment" runat="server"
                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>CostCenter </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ศูนย์ค่าใช้จ่าย</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddCostCenter" runat="server"
                                                                    CssClass="form-control fa-align-left"
                                                                    AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right"
                                                                    ><b>Section </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">แผนก</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddDivision" runat="server"
                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Position </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ตำแหน่ง</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddPosition" runat="server"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="DdSelectedIndexChanged"
                                                                    CssClass="form-control fa-align-left">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <asp:LinkButton ID="addPosison" CssClass="btn btn-success"
                                                                    data-toggle="tooltip" title="เพิ่มตำแหน่ง" runat="server"
                                                                    CommandName="btnaddPosison" OnCommand="btnCommand">
                                                                    <i class="glyphicon glyphicon-plus-sign"></i> เพิ่ม
                                                                </asp:LinkButton>
                                                                <font color="red"><small class="list-group-item-heading text_right">
                                                                    <b>**กรณีมีหลายตำแหน่งให้กดปุ่มเพิ่ม</b></small></font>
                                                            </div>
                                                        </div>

                                                        <div class="panel-body">
                                                            <div class="form-horizontal" role="form">
                                                                <asp:GridView ID="gvAddPosision"
                                                                    runat="server" Visible="false"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                    HeaderStyle-CssClass="info"
                                                                    HeaderStyle-Height="40px"
                                                                    AllowPaging="true"
                                                                    PageSize="10"
                                                                    OnPageIndexChanging="Master_PageIndexChanging"
                                                                    OnRowDeleting="Gv_RowDeleting"
                                                                    OnRowDataBound="Master_RowDataBound" Width="100%">
                                                                    <PagerStyle CssClass="pageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                                        FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">ไม่พบตำแหน่งงาน</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="ตำแหน่งหลัก" ItemStyle-HorizontalAlign="center"
                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:RadioButton ID="rbtnSelect" value="" Style="position: relative"
                                                                                        AutoPostBack="true" runat="server" OnCheckedChanged="rbtnSelect_CheckedChanged" />
                                                                                    <asp:Panel ID="alert_selectPosition" runat="server">
                                                                                        <div class="alert alert-danger fade in">
                                                                                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                                                                                            <strong></strong>*กรุณาเลือกตำแหน่งหลัก
                                                                                        </div>
                                                                                    </asp:Panel>
                                                                                </small>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ตำแหน่งที่" HeaderStyle-CssClass="text-center"
                                                                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblIDX_position" runat="server" Visible="false" />
                                                                                <%# (Container.DataItemIndex +1) %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="บริษัท" ItemStyle-HorizontalAlign="Left"
                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:Label ID="lbailsd" runat="server" CssClass="col-sm-12"
                                                                                        Text='<%# Eval("Organiztion_Name") %>'></asp:Label>
                                                                                </small>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left"
                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:Label ID="itle" runat="server" CssClass="col-sm-12"
                                                                                        Text='<%# Eval("Department_Name") %>'></asp:Label>
                                                                                </small>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left"
                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:Label ID="lb_detail" runat="server" CssClass="col-sm-12"
                                                                                        Text='<%# Eval("Division_Name") %>'></asp:Label></small>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left"
                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:Label ID="lbposision" runat="server" CssClass="col-sm-12"
                                                                                        Text='<%# Eval("Position_Name") %>'></asp:Label></small>
                                                                                <asp:Label ID="Position_idx" runat="server" Visible="false"
                                                                                    CssClass="col-sm-12" Text='<%# Eval("Position") %>'></asp:Label>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="การจัดการ" ItemStyle-HorizontalAlign="center"
                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnDelPosision" runat="server" Text="ลบ"
                                                                                    CssClass="btn btn-danger btn-sm"
                                                                                    OnClientClick="return confirm('คุณต้องการลบรายการนี้หรือไม่?')"
                                                                                    CommandName="Delete" />
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%-- ผู้อนุมัติใบลา--%>
                                            <asp:Panel ID="BoxApprovers" runat="server" Visible="true">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <i class="glyphicon glyphicon-file"></i>&nbsp;
                                                        <b>Set Approvers</b> (กำหนดผู้อนุมัติใบลา)
                                                    </div>
                                                    <div class="panel-body">
                                                        <h5>&nbsp;</h5>
                                                        <div class="form-horizontal" role="form">
                                                            <div class="form-group">
                                                                <div class="col-sm-6">
                                                                    <div class="panel panel-success">

                                                                        <div class="panel-body">
                                                                            <div class="form-group">
                                                                                <div class="col-sm-4">
                                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                        <b> Approver leave 1 </b></small></font>
                                                                                    <small>
                                                                                        <p class="list-group-item-text">ผู้อนุมัติใบลาคนที่ 1</p>
                                                                                    </small>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <asp:DropDownList ID="ddlApprover_Leave1" runat="server"
                                                                                        CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="panel panel-success">
                                                                        <div class="panel-body">
                                                                            <div class="form-group">
                                                                                <div class="col-sm-4">
                                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                        <b> Approver leave 2</b></small></font>
                                                                                    <small>
                                                                                        <p class="list-group-item-text">ผู้อนุมัติใบลาคนที่ 2</p>
                                                                                    </small>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <asp:DropDownList ID="ddlApprover_Leave2" runat="server"
                                                                                        CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <i class="glyphicon glyphicon-file"></i>&nbsp;
                                                    <b>Set Shift Time Information</b> (เลือกข้อมูลตารางงานพนักงาน)
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">
                                                        <div class="form-group">
                                                            <asp:FormView ID="formview_shiftTime" OnDataBound="FormView_DataBound"
                                                                runat="server" DefaultMode="Insert" Width="100%">
                                                                <InsertItemTemplate>

                                                                    <div class="col-lg-6" runat="server" id="Div6">
                                                                        <div class="panel panel-info">
                                                                            <div class="panel-heading">
                                                                                <h3 class="panel-title">
                                                                                    <i class="glyphicon glyphicon-edit"></i><strong>&nbsp; เลือกประเภทตารางงาน</strong></h3>
                                                                            </div>
                                                                            <div class="panel-body">
                                                                                <div class="pull-left">
                                                                                    <a href="#">
                                                                                        <img class="media-object img-thumbnail"
                                                                                            src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/iconTime.png")%>'
                                                                                            width="60px" height="60px" style="margin-right: 8px; margin-top: -5px;">
                                                                                    </a>
                                                                                </div>
                                                                                <div class="post-content">
                                                                                    <div style="margin-top: -10px;">
                                                                                        &nbsp;&nbsp;&nbsp;    
                                                                                    <div class="col-sm-8">
                                                                                        <asp:RadioButton ID="checkstaticShift" runat="server" OnCheckedChanged="RadioSelected_insert_IndexChanged"
                                                                                            AutoPostBack="true" />&nbsp; <font color="#1A5276">กะคงที่ (Shift Time Static)</font>
                                                                                    </div>
                                                                                        <div class="col-sm-8">
                                                                                            <asp:RadioButton ID="checkDynamicShift" runat="server" OnCheckedChanged="RadioSelected_insert_IndexChanged"
                                                                                                AutoPostBack="true" />&nbsp; <font color="#1A5276">กะหมุนเวียน (Shift Time Dynamic)</font>
                                                                                        </div>
                                                                                    </div>
                                                                                    <h5>&nbsp;</h5>
                                                                                    <h5>&nbsp;</h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6" runat="server" id="Div7">
                                                                        <div class="panel panel-info">
                                                                            <div class="panel-heading">
                                                                                <h3 class="panel-title">
                                                                                    <i class="glyphicon glyphicon-edit"></i><strong>&nbsp; กำหนดตารางงาน</strong></h3>
                                                                            </div>
                                                                            <div class="panel-body">
                                                                                <div class="pull-left">
                                                                                    <a href="#">
                                                                                        <img class="media-object img-thumbnail"
                                                                                            src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/iconTime.png")%>'
                                                                                            width="60px" height="60px" style="margin-right: 8px; margin-top: -5px;">
                                                                                    </a>
                                                                                </div>
                                                                                <div class="post-content">
                                                                                    <div style="margin-top: -10px;">
                                                                                        <div class="form-group">
                                                                                            <div class="col-sm-3">
                                                                                                <font color="#1A5276"><div class="list-group-item-heading text_left">กะเวลางาน
                                                                                    <br />
                                                                                    <p>Shift Time </p>
                                                                                                      </div></font>
                                                                                            </div>
                                                                                            <div class="col-sm-6">
                                                                                                <asp:DropDownList ID="ddlist_ShiftTime"
                                                                                                    runat="server" CssClass="form-control fa-align-left"
                                                                                                    AutoPostBack="true" Enabled="false">
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </InsertItemTemplate>
                                                            </asp:FormView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <%--  ปิดฟอร์มผู้อนุมัติใบลา--%>
                                            <%--     <hr />--%>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <i class="glyphicon glyphicon-file"></i>&nbsp;
                                                    <b>personal Information</b> (ข้อมูลส่วนตัวพนักงาน)
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="text_right">
                                                                    <b>First Name(Thai) </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ชื่อภาษาไทย</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <asp:DropDownList ID="ddPrefixTH" runat="server"
                                                                    CssClass="form-control" Width="80">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtNameTH" runat="server" placeholder="กรอกชื่อภาษาไทย"
                                                                    CssClass="form-control left" ValidationGroup="formInsert">
                                                                </asp:TextBox>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="C_NameTH" runat="server"
                                                                ControlToValidate="txtNameTH" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรอกข้อมูลชื่อ" ValidationGroup="formInsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameTH" Width="160" />

                                                            <asp:RegularExpressionValidator ID="R_NamTH" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น"
                                                                SetFocusOnError="true" Display="None" ControlToValidate="txtNameTH"
                                                                ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="formInsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH2" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NamTH" Width="160" />
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>LastName</b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">นามสกุล</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtLastnameTH" runat="server" CssClass="form-control left"
                                                                    ValidationGroup="formInsert" placeholder="กรอกนามสกุลไทย"></asp:TextBox>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="C_LastnameTH" runat="server"
                                                                ControlToValidate="txtLastnameTH" Display="None"
                                                                SetFocusOnError="true" ErrorMessage="*กรุณากรอกข้อมูลนามสกุล"
                                                                ValidationGroup="formInsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameTH" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="C_LastnameTH" Width="160" />

                                                            <asp:RegularExpressionValidator ID="R_LastNameTH" runat="server"
                                                                ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true"
                                                                Display="None" ControlToValidate="txtLastnameTH"
                                                                ValidationExpression="^[ก-๙'.\s]{1,250}$"
                                                                ValidationGroup="formInsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastNameTH2" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="R_LastNameTH" Width="160" />
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="text_right">
                                                                    <b>First Name(English) </b></small></font>

                                                                <small>
                                                                    <p class="list-group-item-text">ชื่อภาษาอังกฤษ</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <asp:DropDownList ID="ddPrefixEN" runat="server"
                                                                    CssClass="form-control" Width="80">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtNameEN" runat="server"
                                                                    placeholder="กรอกชื่อภาษาอังกฤษ"
                                                                    CssClass="form-control left"
                                                                    ValidationGroup="formInsert"></asp:TextBox>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="C_NameEN" runat="server"
                                                                ControlToValidate="txtNameEN" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรอกข้อมูล" ValidationGroup="Save" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="C_NameEN" Width="160" />

                                                            <asp:RegularExpressionValidator ID="R_NameEN" runat="server"
                                                                ErrorMessage="*เเฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true"
                                                                Display="None" ControlToValidate="txtNameEN"
                                                                ValidationExpression="^[a-zA-Z'.\s]{1,250}$"
                                                                ValidationGroup="Save" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN2" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="R_NameEN" Width="160" />
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276">
                                                                    <small class="list-group-item-heading text_right">
                                                                    <b>LastName</b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">นามสกุล</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtLastnameEN" runat="server"
                                                                    CssClass="form-control left"
                                                                    ValidationGroup="formInsert"
                                                                    placeholder="กรอกชื่อนามสกุลภาษาอังกฤษ"></asp:TextBox>
                                                            </div>

                                                            <asp:RequiredFieldValidator ID="C_LastnameEN" runat="server"
                                                                ControlToValidate="txtLastnameEN" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรอกข้อมูล" ValidationGroup="Save" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="C_LastnameEN" Width="160" />

                                                            <asp:RegularExpressionValidator ID="R_LastNameEN" runat="server"
                                                                ErrorMessage="*เฉพาะตัวอักษรภาษาอังกฤษเท่านั้น"
                                                                SetFocusOnError="true" Display="None"
                                                                ControlToValidate="txtLastnameEN"
                                                                ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="Save" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN2" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="R_LastNameEN" Width="160" />
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="text_right">
                                                                    <b>NickName (Thai) </b></small></font>

                                                                <small>
                                                                    <p class="list-group-item-text">ชื่อเล่น (ภาษาไทย)</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtNickNameTH" runat="server"
                                                                    placeholder="กรอกชื่อเล่นไทย"
                                                                    CssClass="form-control left"
                                                                    ValidationGroup="formInsert"></asp:TextBox>
                                                            </div>
                                                            <asp:RegularExpressionValidator ID="R_NickNameTH" runat="server"
                                                                ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true"
                                                                Display="None" ControlToValidate="txtNickNameTH"
                                                                ValidationExpression="^[ก-๙'.\s]{1,250}$"
                                                                ValidationGroup="Save" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NickNameTH" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="R_NickNameTH" Width="160" />
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>NickName (English)</b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ชื่อเล่น (ภาษาอังกฤษ)</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtNickNameEN" runat="server" placeholder="กรอกชื่อเล่นอังกฤษ"
                                                                    CssClass="form-control left" ValidationGroup="formInsert">
                                                                </asp:TextBox>
                                                            </div>
                                                            <asp:RegularExpressionValidator ID="R_NickNameEN_" runat="server"
                                                                ErrorMessage="*เฉพาะตัวอักษรภาษอังกฤษเท่านั้น" SetFocusOnError="true"
                                                                Display="None" ControlToValidate="txtNickNameEN"
                                                                ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="SaveE" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_NickNameEN"
                                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="R_NickNameEN_" Width="160" />
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276">
                                                                    <small class="list-group-item-heading text_right">
                                                                    <b>Sex </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">เพศ</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlistSex" runat="server" OnSelectedIndexChanged="DdSelectedIndexChanged"
                                                                    CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                    <asp:ListItem Value="0">เลือกเพศ</asp:ListItem>
                                                                    <asp:ListItem Value="1">ชาย</asp:ListItem>
                                                                    <asp:ListItem Value="2">หญิง</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276">
                                                                    <small class="list-group-item-heading text_right">
                                                                    <b>Birthday </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">วันเกิด</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="txtBirthday" runat="server"
                                                                        placeholder="เลือกวันเกิด..."
                                                                        CssClass="form-control from-date-datepicker"
                                                                        AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element=""
                                                                        class="glyphicon glyphicon-calendar"></span></span>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="C_Birthday" runat="server"
                                                                    ControlToValidate="txtBirthday" Display="None"
                                                                    SetFocusOnError="true"
                                                                    ErrorMessage="*เลือกวันที่" ValidationGroup="Save" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_Birthday" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight"
                                                                    TargetControlID="C_Birthday" Width="160" />

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276">
                                                                    <small class="list-group-item-heading text_right">
                                                                    <b>Nationality </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">สัญชาติ</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlNationality" runat="server"
                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276">
                                                                    <small class="list-group-item-heading text_right">
                                                                    <b>Race </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">เชื้อชาติ</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddRace" runat="server"
                                                                    CssClass="form-control fa-align-left"
                                                                    AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276">
                                                                    <small class="list-group-item-heading text_right">
                                                                    <b>Religion </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ศาสนา</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddReligion" runat="server"
                                                                    CssClass="form-control fa-align-left"
                                                                    AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--    <h5>&nbsp;</h5>--%>

                                            <%-----------------กรอกที่อยู่พนักงาน--------------   --%>

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <i class="glyphicon glyphicon-map-marker"></i>&nbsp;
                                                    <b>Address & Contact (Present)</b>  ที่อยู่และการติดต่อ(ปัจจุบัน)
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">
                                                        <%--  <div class="panel-body">--%>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Address </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ที่อยู่ปัจจุบัน</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtPresentAddress" runat="server"
                                                                    CssClass="form-control left" ValidationGroup="formInsert"
                                                                    placeholder="กรอกที่อยู่ปัจจุบัน..." TextMode="MultiLine" Rows="4"></asp:TextBox>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="C_PresentAddress" runat="server"
                                                                ControlToValidate="txtPresentAddress" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรอกข้อมูล" ValidationGroup="Save" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_PresentAddress" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_PresentAddress" Width="160" />
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Country </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ประเทศ</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddCountry" runat="server"
                                                                    CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Province </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">จังหวัด</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddProvince" runat="server"
                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Amphoe </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">อำเภอ</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddAmphoe" runat="server"
                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>District </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ตำบล</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddDistrict" runat="server"
                                                                    CssClass="form-control fa-align-left" OnSelectedIndexChanged="DdSelectedIndexChanged"
                                                                    AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="text_right">
                                                                    <b>ZipCode</b> </small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">รหัสไปรษณีย์</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control"
                                                                    ValidationGroup="formInsert" placeholder="กรอกรหัสไปรษณีย์"></asp:TextBox>
                                                            </div>
                                                            <asp:RegularExpressionValidator ID="R_ZipCode" runat="server"
                                                                ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None"
                                                                ControlToValidate="txtZipCode" ValidationExpression="^[0-9'.\s]{1,5}$" ValidationGroup="formInsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_ZipCode2" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_ZipCode" Width="160" />
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Email </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">อีเมล</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control left"
                                                                    ValidationGroup="formInsert" placeholder="กรอก e-mail"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="R_Email" runat="server"
                                                                    SetFocusOnError="true" Display="None" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                    ControlToValidate="txtEmail" ErrorMessage="*กรอกในรูปแบบEmail">
                                                                </asp:RegularExpressionValidator>
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_Email2" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Email" Width="160" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="text_right">
                                                                    <b>Mobile Phone </b> </small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">เบอร์โทรศัพท์มือถือ</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtMobilePhone" runat="server"
                                                                    placeholder="กรอกเบอร์โทรศัพท์"
                                                                    CssClass="form-control" MaxLength="10"
                                                                    ValidationGroup="formInsert"></asp:TextBox>
                                                            </div>
                                                            <asp:RegularExpressionValidator ID="R_txtMobilePhone" runat="server"
                                                                ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="dynamic"
                                                                ControlToValidate="txtMobilePhone" ValidationExpression="\d*"
                                                                ValidationGroup="formInsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_txtMobilePhone" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="R_txtMobilePhone" Width="160" />
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Phone Home </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">เบอร์บ้าน</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtHomePhone" runat="server" MaxLength="10"
                                                                    CssClass="form-control left" placeholder="กรอกเบอร์โทรศัพท์บ้าน"
                                                                    ValidationGroup="formInsert"></asp:TextBox>
                                                            </div>
                                                            <asp:RegularExpressionValidator ID="R_txtHomePhone" runat="server"
                                                                ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None"
                                                                ControlToValidate="txtHomePhone"
                                                                ValidationExpression="^[0-9'.\s]{1,10}$" ValidationGroup="formInsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_txtHomePhone"
                                                                runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="R_txtHomePhone" Width="160" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <i class="glyphicon glyphicon-user"></i>&nbsp;
                                                        <b>Address & Contact (Permanent)</b>&nbsp; ที่อยู่และการติดต่อ(ทะเบียนบ้าน)
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">
                                                        <div class="form-group">
                                                            <div class="col-sm-4">
                                                                <asp:CheckBox ID="chk_sameAddress_present" runat="server"
                                                                    AutoPostBack="true" OnCheckedChanged="chkCheckedChanged"
                                                                    Checked="true" />&nbsp; เหมือนกับที่อยู่ปัจจุบัน
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <asp:CheckBox ID="chk_notSameAddress_present" runat="server"
                                                                    AutoPostBack="true" OnCheckedChanged="chkCheckedChanged" />&nbsp; ไม่เหมือนกับที่อยู่ปัจจุบัน
                                                            </div>
                                                        </div>

                                                        <%----------------------เริ่มกล่องเพิ่มข้อมูลที่อยู่ตามทะเบียนบ้าน---------------%>

                                                        <asp:Panel ID="BoxAddAddress" runat="server" Visible="false">
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Permanentaddress </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">ที่อยู่ตามทะเบียนบ้าน</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <asp:TextBox ID="txtPermanentaddress" runat="server" placeholder="กรอกที่อยู่ตามทะเบียนบ้าน"
                                                                        CssClass="form-control left" OnTextChanged="TxtTextChanged" ValidationGroup="formInsert"
                                                                        TextMode="MultiLine" Rows="4">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Country </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">ประเทศ</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:DropDownList ID="ddCountry2" runat="server"
                                                                        CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Province </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">จังหวัด</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:DropDownList ID="ddProvince2" runat="server"
                                                                        CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Amphoe </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">อำเภอ</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:DropDownList ID="ddAmphoe2" runat="server"
                                                                        CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>District </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">ตำบล</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:DropDownList ID="ddDistrict2" runat="server"
                                                                        CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">

                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="text_right">
                                                                            <b>ZipCode </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">รหัสไปรษณีย์</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txtZipcode2" runat="server" placeholder="กรอกรหัสไปรษณีย์"
                                                                        CssClass="form-control" MaxLength="5" OnTextChanged="TxtTextChanged" ValidationGroup="save1">
                                                                    </asp:TextBox>
                                                                    <asp:RegularExpressionValidator ID="Regular_txtZipcode21"
                                                                        runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                                                        Display="None" ControlToValidate="txtZipcode2" ValidationExpression="^[0-9]{1,10}$"
                                                                        ValidationGroup="save1" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validator_txtZipcode21" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regular_txtZipcode21" Width="160" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Mobile Phone </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">เบอร์โทรศัพท์</p>
                                                                    </small>
                                                                </div>

                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txtHomePhone2" runat="server"
                                                                        MaxLength="10" placeholder="กรอกเบอร์โทรศัพท์"
                                                                        CssClass="form-control left" OnTextChanged="TxtTextChanged"
                                                                        ValidationGroup="formInsert"></asp:TextBox>
                                                                </div>
                                                                <asp:RegularExpressionValidator ID="RegulartxtHomePhone2"
                                                                    runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                                                    Display="None" ControlToValidate="txtHomePhone2"
                                                                    ValidationExpression="^[0-9]{1,10}$"
                                                                    ValidationGroup="formInsert" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorHomePhone2" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight"
                                                                    TargetControlID="RegulartxtHomePhone2" Width="160" />
                                                            </div>
                                                        </asp:Panel>
                                                        <%----------------------ปิดกล่องเพิ่มข้อมูลที่อยู่ตามทะเบียนบ้าน---------------%>
                                                    </div>
                                                </div>
                                            </div>


                                            <%-- <hr />--%>
                                            <%----------------------เพิ่มข้อมูลบุคคลอ้างอิง---------------%>

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <i class="glyphicon glyphicon-user"></i>&nbsp;
                                                    <b>Reference persons</b>&nbsp; (บุคคลอ้างอิง)
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="text_right">
                                                                    <b>Reference Name</b> </small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ชื่อบุคคลอ้างอิง</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txt_fullname_reference" runat="server" placeholder="กรอกชื่อ - นามสกุล"
                                                                    CssClass="form-control" ValidationGroup="formInsert">
                                                                </asp:TextBox>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="text_right">
                                                                    <b>Relationship</b> </small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ความสัมพันธ์</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txt_relationship" runat="server" placeholder="ระบุความสัมพันธ์"
                                                                    CssClass="form-control" ValidationGroup="formInsert">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="text_right">
                                                                    <b>Address</b> </small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ที่อยู่</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txt_address_reference" runat="server" placeholder="กรอกที่อยู่"
                                                                    CssClass="form-control" ValidationGroup="formInsert">
                                                                </asp:TextBox>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="text_right">
                                                                    <b>Mobile Phone</b> </small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">เบอร์โทรศัพท์</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txt_mobile_number" MaxLength="10" runat="server"
                                                                    placeholder="กรอกเบอร์โทรศัพท์"
                                                                    CssClass="form-control" ValidationGroup="formInsert">
                                                                </asp:TextBox>
                                                            </div>
                                                            <asp:RegularExpressionValidator ID="R_txt_mobile_number"
                                                                runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น"
                                                                SetFocusOnError="true" Display="None" ControlToValidate="txt_mobile_number"
                                                                ValidationExpression="^[0-9'.\s]{1,10}$" ValidationGroup="formInsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="v_txt_mobile_number" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="R_txt_mobile_number" Width="160" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%-- <hr />--%>
                                            <%----------------------เพิ่มข้อมูลพนักงานไทย---------------%>
                                            <asp:Panel ID="BoxEmployeeTH" runat="server">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <i class="glyphicon glyphicon-user"></i>&nbsp;
                                                        <b>Employee Thai Nationality</b>&nbsp; (ข้อมูลพนักงานไทย)
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="form-horizontal" role="form">
                                                            <asp:Panel ID="Military" runat="server">
                                                                <div class="form-group">
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b> MilitaryStatus</b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">สถานะทางทหาร</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <asp:DropDownList ID="ddMilitary" runat="server"
                                                                            CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="text_right">
                                                                        <b>ID Card</b> </small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">เลขบัตรประชาชน</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txtIDCard" runat="server" MaxLength="13" placeholder="กรอกเลขบัตรประจำตัว 13 หลัก"
                                                                        CssClass="form-control" ValidationGroup="formInsert"></asp:TextBox>
                                                                </div>
                                                                <asp:RegularExpressionValidator ID="RegulartxtIDCard"
                                                                    runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                                                    Display="None" ControlToValidate="txtIDCard" ValidationExpression="^[0-9]{1,13}$"
                                                                    ValidationGroup="formInsert" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtIDCard" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegulartxtIDCard" Width="160" />
                                                            </div>
                                                            <div class="form-group">

                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="text_right">
                                                                        <b>Issued At</b> </small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">ออกให้ ณ</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txtIIssuedAtIDCard" runat="server" placeholder="กรอกสถานที่ออกบัตร"
                                                                        CssClass="form-control" ValidationGroup="formInsert"></asp:TextBox>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b>Expiration Date </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">วันหมดอายุ</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txtDateExpIDCard" runat="server" placeholder="เลือกวันหมดอายุบัตร"
                                                                            CssClass="form-control from-date-datepicker"
                                                                            AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                                        <span class="input-group-addon"><span data-icon-element=""
                                                                            class="fa fa-calendar"></span></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <hr />
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />

                                            </asp:Panel>
                                            <%----------------------ปิดการเพิ่มข้อมูลพนักงานไทย---------------%>
                                            <%----------------------เปิดการเพิ่มข้อมูลพนักงานชาวต่างชาติ---------------%>
                                            <asp:Panel ID="BoxEmployeeEN" runat="server" Visible="false">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <i class="glyphicon glyphicon-user"></i>&nbsp;
                                                        <b>Employee Foreing Nationality</b>&nbsp; (ข้อมูลพนักงานชาวต่างชาติ)
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="form-horizontal" role="form">
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b>Permanentaddress </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">ที่อยู่ตามทะเบียนบ้าน</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <asp:TextBox ID="txtPermanentaddressEN" runat="server" placeholder="กรอกที่อยู่ตามทะเบียนบ้าน"
                                                                        CssClass="form-control left" ValidationGroup="formInsert"
                                                                        TextMode="MultiLine" Rows="4"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b>Country </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">ประเทศ</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:DropDownList ID="ddlCountryEN" runat="server"
                                                                        CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b>Phone Home  </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">เบอร์โทรศัพท์</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txtPhoneEN" runat="server" MaxLength="10"
                                                                        placeholder="กรอกเบอร์โทรศัพท์"
                                                                        CssClass="form-control fa-align-left"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator ID="RegulartxtPhoneEN" runat="server"
                                                                        ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None"
                                                                        ControlToValidate="txtPhoneEN" ValidationExpression="\d*"
                                                                        ValidationGroup="formInsert" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtPhoneEN"
                                                                        runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight"
                                                                        TargetControlID="RegulartxtPhoneEN" Width="160" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b>Passport ID  </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">เลขที่หนังสือเดินทาง</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txt_Passport_Issued_No" runat="server" placeholder="กรอกเลขที่หนังสือเดินทาง"
                                                                        CssClass="form-control fa-align-left"></asp:TextBox>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                <b>Passport Issued At </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">
                                                                            สถานที่ออกหนังสือเดินทาง
                                                                        </p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txt_PassportIssued" runat="server" placeholder="กรอกสถานที่ออกหนังสือเดินทาง"
                                                                        CssClass="form-control fa-align-left"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b>Issued Date </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">วันที่ออกหนังสือเดินทาง</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txt_Issued_Date" runat="server" placeholder="เลือกวันที่ออกหนังสือเดินทาง"
                                                                        CssClass="form-control from-date-datepicker"></asp:TextBox>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                <b>ExpDate </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">
                                                                            วันหมดอายุหนังสือเดินทาง
                                                                        </p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txt_ExpDate" runat="server" placeholder="เลือกวันที่หมดอายุหนังสือเดินทาง"
                                                                        CssClass="form-control from-date-datepicker"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b>Work Permit No.</b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">เลขที่ใบอนุญาตทำงาน</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txt_perIssued_No" runat="server" placeholder="กรอกเลขที่ใบอนุญาติทำงาน"
                                                                        CssClass="form-control fa-align-left" AutoPostBack="true"></asp:TextBox>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                <b>Work Permit Issued At </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">
                                                                            สถานที่ออกใบอนุญาตทำงาน
                                                                        </p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txt_perIssued_at" runat="server" placeholder="กรอกสถานที่ออกใบอนุญาตทำงาน"
                                                                        CssClass="form-control fa-align-left" AutoPostBack="true"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b>Work Permit Issued Date</b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">วันที่ออกใบอนุญาตทำงาน</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txt_perIssued_date" runat="server" placeholder="เลือกวันที่ออกใบอนุญาตทำงาน"
                                                                        CssClass="form-control from-date-datepicker" AutoPostBack="true"></asp:TextBox>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                <b>Work Permit Expire Date </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">
                                                                            วันหมดอายุใบอนุญาตทำงาน
                                                                        </p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txt_perExpire_date" runat="server" placeholder="เลือกวันหมดอายุใบอนุญาตทำงาน"
                                                                        CssClass="form-control from-date-datepicker" AutoPostBack="true"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <%----------------------ปิดการเพิ่มข้อมูลพนักงานชาวต่างชาติ---------------%>
                                            <asp:Panel ID="boxadd_Social_Security" runat="server" Visible="true">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <i class="glyphicon glyphicon-user"></i>&nbsp;
                                                        <b>Social Security</b>&nbsp; (ประกันสังคม)
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="form-horizontal" role="form">

                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b>Currently used hospital </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">สถานพยาบาลที่ใช้</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:DropDownList ID="ddHospital" runat="server"
                                                                        CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">

                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="text_right">
                                                                        <b>Social Security No</b> </small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">เลขที่ประกันสังคม</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <asp:TextBox ID="txtSocial" runat="server" MaxLength="13" placeholder="กรอกเลขประกันสังคม"
                                                                        CssClass="form-control" ValidationGroup="formInsert"></asp:TextBox>
                                                                </div>
                                                                <asp:RegularExpressionValidator ID="R_Social" runat="server"
                                                                    ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None"
                                                                    ControlToValidate="txtSocial" ValidationExpression="^[0-9]{1,30}$" ValidationGroup="Save" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_Social" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Social" Width="160" />
                                                                <div class="col-sm-2">
                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b>Expiration Date </b></small></font>
                                                                    <small>
                                                                        <p class="list-group-item-text">วันหมดอายุ</p>
                                                                    </small>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txtSocialDateExp" runat="server" placeholder="เลือกวันที่บัตรหมดอายุ"
                                                                            CssClass="form-control from-date-datepicker" AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </asp:Panel>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <i class="glyphicon glyphicon-heart-empty"></i>&nbsp;
                                                    <b>Health</b>&nbsp; (สุขภาพ)
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Height </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ส่วนสูง</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtHeight" runat="server"
                                                                    CssClass="form-control left" placeholder="กรอกส่วนสูง"
                                                                    ValidationGroup="formInsert"></asp:TextBox>
                                                            </div>
                                                            <asp:RegularExpressionValidator ID="R_Height" runat="server"
                                                                ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None"
                                                                ControlToValidate="txtHeight" ValidationExpression="^[0-9'.\s]{1,3}$"
                                                                ValidationGroup="Save" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="VS_Height2" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Height" Width="160" />
                                                            <div class="col-sm-1">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b> Cm. </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ซม.</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b> Weight </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">น้ำหนัก</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtWeight" runat="server"
                                                                    CssClass="form-control left" placeholder="กรอกน้ำหนัก"
                                                                    ValidationGroup="formInsert"></asp:TextBox>
                                                            </div>
                                                            <asp:RegularExpressionValidator ID="R_Weigth" runat="server"
                                                                ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None"
                                                                ControlToValidate="txtWeight" ValidationExpression="^[0-9'.\s]{1,3}$" ValidationGroup="Save" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_Weigth2" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Weigth" Width="160" />
                                                            <div class="col-sm-1">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b> Kg. </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">กก.</p>
                                                                </small>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b> BloodGroup</b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">กรุ๊ปเลือด</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddlBloodgroup" runat="server"
                                                                    CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b> Scar </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">รอยตำหนิ</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtScar" runat="server" placeholder="กรอกรอยตำหนิ"
                                                                    CssClass="form-control left" ValidationGroup="formInsert"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <%----------------------เพิ่มข้อมูลการศึกษา----------------%>

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <i class="glyphicon glyphicon-education"></i>&nbsp;
                                                    <b>Education</b>&nbsp; (ประวัติการศึกษา)
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b> Graduates</b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">จบการศึกษาระดับ</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlEducation" runat="server"
                                                                    CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">

                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Year of graduation</b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ปีที่สำเร็จการศึกษา</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtgraduation_year" runat="server" placeholder="กรอกปี ค.ศ. ที่จบการศึกษา"
                                                                    CssClass="form-control fa-align-left"></asp:TextBox>
                                                            </div>
                                                            <asp:RegularExpressionValidator ID="R_txtgraduation_year" runat="server"
                                                                ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None"
                                                                ControlToValidate="txtgraduation_year" ValidationExpression="^[0-9'.\s]{1,10}$" ValidationGroup="formInsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_txtgraduation_year" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_txtgraduation_year" Width="160" />
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b> GPA</b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">เกรดเฉลี่ย</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtGPA" runat="server" placeholder="กรอกเกรดเฉลี่ย"
                                                                    CssClass="form-control fa-align-left"></asp:TextBox>
                                                            </div>
                                                            <asp:RegularExpressionValidator ID="R_txtGPA" runat="server"
                                                                ErrorMessage="*เฉพาะตัวเลขรูปแบบ 0.00 เท่านั้น" SetFocusOnError="true" Display="None"
                                                                ControlToValidate="txtGPA" ValidationExpression="^\d*\.?\d{2}$" ValidationGroup="formInsert" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="C_txtGPA" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_txtGPA" Width="160" />
                                                        </div>
                                                        <div class="form-group">

                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>Faculty</b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">คณะ</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlFaculty" runat="server"
                                                                    CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b> Major</b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">เรียนสาขา</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtmajor" runat="server" placeholder="กรอกสาขาที่เรียน"
                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                    <b>University </b></small></font>
                                                                <small>
                                                                    <p class="list-group-item-text">ชื่อสถานบันศึกษา</p>
                                                                </small>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtUniversity" runat="server" placeholder="กรอกชื่อสถานบันศึกษา"
                                                                    CssClass="form-control left" ValidationGroup="formInsert"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <%----------------------ปิดการเพิ่มข้อมูลการศึกษา----------------%>
                                            <%--     <hr />--%>

                                            <%----------------------เพิ่มข้อมูลประสบการณ์ทำงาน---------------%>

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <i class="glyphicon glyphicon-time"></i>&nbsp;
                                                    <b>Experience</b>&nbsp; (ประสบการณ์ทำงาน)
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">
                                                        <div class="form-group">
                                                            <div class="col-sm-5">
                                                                <asp:CheckBox ID="chk_nothave_Experience" runat="server"
                                                                    Checked="true" OnCheckedChanged="chkSelectedIndexChanged"
                                                                    AutoPostBack="true" />&nbsp; ไม่มีประสบการณ์ทำงาน (No Experience)
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <asp:CheckBox ID="chk_have_Experience" runat="server"
                                                                    AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />&nbsp; มีประสบการณ์ทำงาน (Experience)
                                                            </div>
                                                        </div>
                                                        <asp:Panel ID="BoxAddExperience" runat="server" Visible="false">

                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading">
                                                                    <h3 class="panel-title">
                                                                        <i class="fa fa-user"></i>&nbsp;<strong>&nbsp; กรอกรายละเอียด (ประสบการณ์ทำงาน)</strong></h3>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-2">
                                                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b> Company </b></small></font>
                                                                            <small>
                                                                                <p class="list-group-item-text">ชื่อบริษัท</p>
                                                                            </small>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txtOrganiztion_Old" runat="server" placeholder="กรอกชื่อบริษัท"
                                                                                CssClass="form-control left" ValidationGroup="formInsert"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-2">
                                                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b> Position </b></small></font>
                                                                            <small>
                                                                                <p class="list-group-item-text">ตำแหน่ง</p>
                                                                            </small>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txtPositionOld" runat="server" placeholder="กรอกชื่อตำแหน่ง"
                                                                                CssClass="form-control left" ValidationGroup="formInsert"></asp:TextBox>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b> Job Brief </b></small></font>
                                                                            <small>
                                                                                <p class="list-group-item-text">ลักษณะงานโดยย่อ</p>
                                                                            </small>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_Job_Brief" runat="server" placeholder="กรอกลักษณะงานโดยย่อ"
                                                                                CssClass="form-control left" ValidationGroup="formInsert"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-2">
                                                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b>Work In </b></small></font>
                                                                            <small>
                                                                                <p class="list-group-item-text">วันที่เริ่มเข้าทำงาน</p>
                                                                            </small>
                                                                        </div>

                                                                        <div class="col-sm-3">
                                                                            <div class='input-group date'>

                                                                                <asp:TextBox ID="txtWorkInOld" runat="server" placeholder="เลือกวันที่เริ่มงาน"
                                                                                    CssClass="form-control from-date-datepicker"></asp:TextBox>
                                                                                <span class="input-group-addon"><span data-icon-element=""
                                                                                    class="fa fa-calendar"></span></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b>resign </b></small></font>
                                                                            <small>
                                                                                <p class="list-group-item-text">วันที่ลาออก</p>
                                                                            </small>
                                                                        </div>

                                                                        <div class="col-sm-3">
                                                                            <div class='input-group date'>

                                                                                <asp:TextBox ID="txtresign_Old" runat="server" placeholder="เลือกวันที่ลาออก"
                                                                                    CssClass="form-control from-date-datepicker"></asp:TextBox>
                                                                                <span class="input-group-addon"><span data-icon-element=""
                                                                                    class="fa fa-calendar"></span></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-2">
                                                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b>Motive </b></small></font>
                                                                            <small>
                                                                                <p class="list-group-item-text">สาเหตุการลาออก</p>
                                                                            </small>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txtMotive" runat="server" CssClass="form-control left" placeholder="ระบุสาเหตุการลาออก"
                                                                                ValidationGroup="formInsert"></asp:TextBox>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b>Salary </b></small></font>
                                                                            <small>
                                                                                <p class="list-group-item-text">อัตราค่าจ้าง</p>
                                                                            </small>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txtSalary_Old" runat="server" placeholder="ระบุอัตราค่าจ้าง"
                                                                                CssClass="form-control left" ValidationGroup="formInsert_"></asp:TextBox>

                                                                            <asp:RegularExpressionValidator ID="RegulartxtSalary_Old"
                                                                                runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                                                                Display="None" ControlToValidate="txtSalary_Old" ValidationExpression="^[0-9',.\s]{1,20}$"
                                                                                ValidationGroup="formInsert_" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtSalary_Old" runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegulartxtSalary_Old" Width="160" />
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <asp:LinkButton ID="btnaddExperience" CssClass="btn btn-success" ValidationGroup="formInsert_"
                                                                                data-toggle="tooltip" title="เพิ่มข้อมูล" runat="server" CommandName="btnaddExperience"
                                                                                OnCommand="btnCommand"><i class="glyphicon glyphicon-plus-sign"></i> เพิ่ม</asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <div class="form-horizontal" role="form">
                                                                            <asp:GridView ID="gvExperience" runat="server" Visible="false"
                                                                                AutoGenerateColumns="false"
                                                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                                HeaderStyle-CssClass="info"
                                                                                HeaderStyle-Height="40px"
                                                                                AllowPaging="true"
                                                                                PageSize="10"
                                                                                OnPageIndexChanging="Master_PageIndexChanging"
                                                                                OnRowDeleting="Gv_RowDeleting"
                                                                                OnRowDataBound="Master_RowDataBound" Width="100%">
                                                                                <PagerStyle CssClass="pageCustom" />
                                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                                                    FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                                                                                <EmptyDataTemplate>
                                                                                    <div style="text-align: center">ไม่มีข้อมูลประสบการณ์ทำงาน</div>
                                                                                </EmptyDataTemplate>

                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center"
                                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                        <ItemTemplate>
                                                                                            <small>
                                                                                                <asp:Label ID="Experience_IDX" runat="server" Visible="false" />
                                                                                                <%# (Container.DataItemIndex +1) %>
                                                                                            </small>
                                                                                        </ItemTemplate>

                                                                                        <EditItemTemplate />
                                                                                        <FooterTemplate />
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="บริษัท" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center"
                                                                                        HeaderStyle-Font-Size="Small">
                                                                                        <ItemTemplate>
                                                                                            <small>
                                                                                                <asp:Label ID="lbName_Organiztion_Old" runat="server"
                                                                                                    CssClass="col-sm-12" Text='<%# Eval("Name_Organiztion_Old") %>'></asp:Label>
                                                                                            </small>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left"
                                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                        <ItemTemplate>
                                                                                            <small>
                                                                                                <asp:Label ID="lbPosition_Old" runat="server"
                                                                                                    CssClass="col-sm-12" Text='<%# Eval("Position_Old") %>'></asp:Label>
                                                                                            </small>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="ลักษณะงานโดยย่อ" ItemStyle-HorizontalAlign="Left"
                                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                        <ItemTemplate>
                                                                                            <small>
                                                                                                <asp:Label ID="Job_Brief" runat="server"
                                                                                                    CssClass="col-sm-12" Text='<%# Eval("Job_Brief") %>'></asp:Label></small>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="วันที่เริ่มงาน" ItemStyle-HorizontalAlign="Left"
                                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                        <ItemTemplate>
                                                                                            <small>
                                                                                                <asp:Label ID="lablebWork_In_Old" runat="server"
                                                                                                    CssClass="col-sm-12" Text='<%# Eval("date_Work_In_Old") %>'></asp:Label>
                                                                                            </small>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="วันที่ลาออก" ItemStyle-HorizontalAlign="Left"
                                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                        <ItemTemplate>
                                                                                            <small>
                                                                                                <asp:Label ID="labelresign_Old" runat="server"
                                                                                                    CssClass="col-sm-12" Text='<%# Eval("date_resign_Old") %>'></asp:Label></small>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="สาเหตุการลาออก" ItemStyle-HorizontalAlign="Left"
                                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                        <ItemTemplate>
                                                                                            <small>
                                                                                                <asp:Label ID="lbMotive" runat="server"
                                                                                                    CssClass="col-sm-12" Text='<%# Eval("Motive") %>'></asp:Label></small>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="อัตราค่าจ้าง" ItemStyle-HorizontalAlign="Left"
                                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                        <ItemTemplate>
                                                                                            <small>
                                                                                                <asp:Label ID="lbSalary_Old" runat="server"
                                                                                                    CssClass="col-sm-12" Text='<%# Eval("Salary_Old") %>'></asp:Label></small>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>


                                                                                    <asp:TemplateField HeaderText="การจัดการ" ItemStyle-HorizontalAlign="center"
                                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="btnExperience" runat="server"
                                                                                                Text="ลบ" CssClass="btn btn-danger btn-sm"
                                                                                                OnClientClick="return confirm('คุณต้องการลบรายการนี้หรือไม่?')" CommandName="Delete" />
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate />
                                                                                        <FooterTemplate />
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <%--  <hr />--%>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>

                                            <%----------------------ปิดการเพิ่มข้อมูลการทำงาน----------------%>

                                            <asp:LinkButton ID="BtnCancel" CssClass="btn btn-default pull-right"
                                                data-toggle="tooltip" title="ยกเลิก" runat="server"
                                                CommandName="BtnCancel" OnCommand="btnCommand">
                                                <i class="fa fa-cancel"></i> ยกเลิก</asp:LinkButton>
                                            &nbsp;&nbsp;<asp:LinkButton ID="BtnSave" CssClass="btn btn-success pull-right"
                                                data-toggle="tooltip" title="บันทึกข้อมูล" runat="server"
                                                ValidationGroup="formInsert" CommandName="BtnSaveDataEmploy"
                                                OnCommand="btnCommand" OnClientClick="if(!confirm('คุณต้องการบันทึกข้อมูลพนักงานหรือไม่?')) return false;">
                                                <i class="fa fa-save"></i> บันทึก</asp:LinkButton>
                                            </div>
                                            </div>
                                        </InsertItemTemplate>
                                    </asp:FormView>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </asp:View>

                <asp:View ID="viewEdit_dataEmployee" runat="server">
                    <div class="col-lg-12" runat="server" id="Div2">
                        <asp:LinkButton ID="Button" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="กลับไปก่อนหน้า"
                            runat="server" CommandName="btnBack" OnCommand="btnCommand">
                            <i class="glyphicon glyphicon-circle-arrow-left"></i>&nbsp; กลับ</asp:LinkButton>
                        <h5>&nbsp;</h5>
                        <asp:Panel ID="Panel1" runat="server">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-edit"></i>&nbsp;
                                        <i class="glyphicon glyphicon-user"></i><strong>&nbsp; แก้ไขข้อมูลพนักงาน</strong></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                               <asp:LinkButton ID="BtnRemovePosision" CssClass="btn btn-danger btn-sm"
                                                data-toggle="tooltip" title="โอนย้ายตำแหน่ง" AutoPostBack="true" runat="server"
                                                CommandName="BtnRemovePosision" OnCommand="btnCommand">โอนย้ายตำแหน่ง</asp:LinkButton>
                                            <asp:LinkButton ID="BtnadjustPay" CssClass="btn btn-success btn-sm" title="ปรับเปลี่ยนค่าจ้าง"
                                                runat="server" CommandName="BtnadjustPay" OnCommand="btnCommand">ปรับเปลี่ยนค่าจ้าง</asp:LinkButton>
                                            <asp:LinkButton ID="btnPenaltyEmployee__" CssClass="btn btn-primary btn-sm"
                                                data-toggle="tooltip" title="บันทึกการลงโทษพนักงาน" runat="server"
                                                CommandName="btnPenaltyEmployee" OnCommand="btnCommand">บันทึกการลงโทษพนักงาน</asp:LinkButton>
                                        </div>
                                        <%-------------------------------- เปิดฟอร์มการโยนย้ายตำแหน่ง-------------------------%>
                                        <asp:Panel ID="showalert_position" runat="server" Visible="false">
                                            <div class="col-sm-12" id="divShowalert">
                                                <div class="form-group">
                                                    <div class="alert alert-warning" role="alert">
                                                        <strong>!!</strong>ยังไม่มีข้อมูลตำแหน่งของพนักงาน ต้องทำการเพิ่มตำแหน่งก่อนถึงจะสามารถโอนย้ายตำแหน่งได้
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <div class="form-group">
                                            <div id="BoxRemovePosisionn" runat="server" visible="true">
                                                <h5>&nbsp;</h5>
                                                <asp:Panel ID="panelMovePosition" runat="server" Visible="true">
                                                    <div class="panel panel-danger">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title"><i class="fa fa-edit"></i>&nbsp;
                                                                <i class="fa fa-user"></i><strong>&nbsp; โอนย้ายตำแหน่ง</strong></h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <h4><span class="label label-default">ตำแหน่งปัจจุบัน</span></h4>
                                                            <p>
                                                                <font color="red">** ทำการเลือกตำแหน่งปัจจุบันทีจะทำการโอนย้ายตำแหน่งใหม่ค่ะ</font>
                                                                <asp:LinkButton ID="resetSelect" CssClass="btn btn-danger pull-right"
                                                                    data-toggle="tooltip" title="ล้างค่า" runat="server" OnClick="ClickReset">ล้างค่า</asp:LinkButton>
                                                            </p>
                                                            <br />
                                                            <asp:GridView ID="gv_present_position" runat="server" AutoGenerateColumns="false" DataKeyNames="EODSPIDX"
                                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12" HeaderStyle-CssClass="info"
                                                                HeaderStyle-Height="40px" AllowPaging="true" PageSize="10" OnRowEditing="row_editing" OnRowUpdating="row_updating"
                                                                OnRowCancelingEdit="row_canceling_edit" OnPageIndexChanging="Master_PageIndexChanging" OnRowDataBound="row_DataBound">
                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">ไม่พบข้อมูลตำแหน่งพนักงาน</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="center"
                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:RadioButton ID="radio_select" 
                                                                                    Style="position: relative; color: green;" AutoPostBack="true"
                                                                                    runat="server" OnCheckedChanged="radio_MovePosition_CheckedChanged" />

                                                                            </small>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lb_position_idx" runat="server" Visible="false"
                                                                                Text='<%# Eval("EODSPIDX") %>' />
                                                                            <%# (Container.DataItemIndex +1) %>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="ชื่อบริษัท" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label ID="lb_OrgNameTH" runat="server" CssClass="col-sm-12"
                                                                                    Text='<%# Eval("OrgNameTH") %>'></asp:Label>
                                                                                <asp:Label ID="id_" runat="server" Visible="false"
                                                                                    Text='<%# Eval("EODSPIDX") %>' />
                                                                            </small>
                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label ID="lb_DeptNameTH" runat="server"
                                                                                    CssClass="col-sm-12" Text='<%# Eval("DeptNameTH") %>'></asp:Label>
                                                                                <asp:Label ID="id_s" runat="server" Visible="false"
                                                                                    Text='<%# Eval("RDeptIDX") %>' />
                                                                            </small>
                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label ID="lb_SecNameTH" runat="server"
                                                                                    CssClass="col-sm-12" Text='<%# Eval("SecNameTH") %>'></asp:Label>
                                                                                <asp:Label ID="id_ss" runat="server" Visible="false"
                                                                                    Text='<%# Eval("RSecIDX") %>' />
                                                                            </small>
                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label ID="lb_PosNameTH" runat="server" CssClass="col-sm-12"
                                                                                    Text='<%# Eval("PosNameTH") %>'></asp:Label>
                                                                                  <asp:Label ID="lb_id_position" runat="server" Visible="false"
                                                                                    CssClass="col-sm-12" Text='<%# Eval("RPosIDX") %>'></asp:Label>
                                                                                <asp:Label ID="lb_statuss" runat="server" Visible="false"
                                                                                    CssClass="col-sm-12" Text='<%# Eval("EStaOut") %>'></asp:Label>
                                                                            </small>
                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="สถานะตำแหน่ง" ItemStyle-HorizontalAlign="center"
                                                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <small><b>
                                                                                <asp:Label ID="lb_EStaOut" runat="server"
                                                                                    Text='<%# Eval("EStaOut").ToString() == "1" ? "ตำแหน่งหลัก" : "ตำแหน่งรักษาการ" %>'
                                                                                    CssClass='<%# Eval("EStaOut").ToString() == "1" ? "text-success bg-success" : "text-default bg-default" %>' />
                                                                                </div>

                                                                            </b></small>
                                                                        </ItemTemplate>

                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            <h4><span class="label label-default">ตำแหน่งที่จะย้าย</span></h4>
                                                            <div class="panel panel-danger">
                                                                <h5>&nbsp;</h5>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b>Date of transfer  </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">วันที่มีผลโอนย้าย</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class='input-group date'>
                                                                            <asp:TextBox ID="AddStartdatea" runat="server"
                                                                                CssClass="from-date-datepicker form-control"
                                                                                AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                                            <span class="input-group-addon"><span data-icon-element=""
                                                                                class="fa fa-calendar"></span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <h3>&nbsp;</h3>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1"></div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b>Organization  </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">บริษัท</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <asp:DropDownList ID="ddlchangOrganization" runat="server"
                                                                            CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                            OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                        <b>Department New  </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">ฝ่ายใหม่</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <asp:DropDownList ID="ddlchangDepartment" runat="server"
                                                                            CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                            OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <h3>&nbsp;</h3>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="text_right">
                                                                        <b>Section New </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">แผนกใหม่</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <asp:DropDownList ID="ddlchangSection" runat="server"
                                                                            CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                            OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="text_right">
                                                                        <b>Position New </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">ตำแหน่งใหม่</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <asp:DropDownList ID="ddlchangPosision" runat="server"
                                                                            CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <h5>&nbsp;</h5>
                                                                    <h5>&nbsp;</h5>
                                                                    <div class="col-sm-9"></div>
                                                                    <div class="col-sm-3">
                                                                        <asp:LinkButton ID="BtnCancel1" CssClass="btn btn-default pull-right"
                                                                            data-toggle="tooltip" title="ยกเลิก" runat="server" CommandName="BtnCancel"
                                                                            OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยกเลิกการปรับเปลี่ยนตำแหน่งใหม่ใช่หรือไม่ ?')">
                                                                        <i class="fa fa-cancel"></i> ยกเลิก</asp:LinkButton>
                                                                        <asp:LinkButton ID="btnsave_positionnew" CssClass="btn btn-success pull-right"
                                                                            data-toggle="tooltip" title="บันทึกข้อมูล" runat="server"
                                                                            CommandName="btnsave_positionnew" OnCommand="btnCommand"
                                                                            OnClientClick="return confirm('คุณต้องการบันทึกข้อมูลตำแหน่งใหม่ใช่หรือไม่ ?')">
                                                                        <i class="fa fa-save"></i> บันทึก</asp:LinkButton>&nbsp;
                                                                    </div>

                                                                </div>
                                                                <h5>&nbsp;</h5>
                                                            </div>
                                                            <h5>&nbsp;</h5>
                                                            <div class="col-lg-12">
                                                                <div class="panel panel-warning">
                                                                    <div class="panel-heading">
                                                                        <i class="fa fa-file"></i>&nbsp;
                                                                    <b>History</b> (ประวัติการโอนย้าย)
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <asp:Repeater ID="history_transfer_position" runat="server">
                                                                            <HeaderTemplate>
                                                                                <div class="row">
                                                                                    <label class="col-sm-2 control-label"><small>วันที่</small></label>
                                                                                    <label class="col-sm-3 control-label"><small>ตำแหน่งเดิม</small></label>
                                                                                    <label class="col-sm-3 control-label"><small>ตำแหน่งใหม่</small></label>
                                                                                    <label class="col-sm-3 control-label"><small>วันที่มีผล</small></label>
                                                                                    <%--   <label class="col-sm-2 control-label"><small>โทษที่ได้รับ</small></label>
                                                                            <label class="col-sm-2 control-label"><small>ผู้ออกใบเตือน</small></label>
                                                                            <label class="col-sm-2 control-label"><small>รายละเอียดความผิด</small></label>--%>
                                                                                </div>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <div class="row">
                                                                                    <div class="col-sm-2">
                                                                                        <small><span><%# Eval("createdate") %></span></small>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <small><span><%# Eval("position_th_old") %></span></small>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <small><span><%# Eval("position_th_new") %></span></small>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <small><span><%# Eval("effecttive_date") %></span></small>
                                                                                    </div>
                                                                                    <%-- <div class="col-sm-2">
                                                                                <small><span><%# Eval("penalty_was") %></span></small>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <small><span><%# Eval("name__") %></span></small>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <small><span><%# Eval("details__") %></span></small>
                                                                            </div>--%>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </div>
                                                                </div>
                                                                <!-- history log -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                        <%--    ---------------------  ปิดฟอร์มการโอนย้ายตำแหน่ง-----------------------------------%>
                                        <%------------------------------------ เปิดฟอร์มการปรับค่าจ้าง----------------------------------------------%>
                                        <div class="form-group">
                                            <h5>&nbsp;</h5>

                                            <asp:Panel ID="BoxEdit_Sarary" runat="server" Visible="true">
                                                <asp:FormView ID="fvEdit_Sarary" runat="server" Width="100%" DefaultMode="Edit">
                                                    <EditItemTemplate>
                                                        <div class="panel panel-success">
                                                            <div class="panel-heading">
                                                                <h3 class="panel-title">
                                                                    <i class="fa fa-edit"></i>&nbsp;<strong>&nbsp; แก้ไขค่าจ้างพนักงาน</strong></h3>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="form-group">
                                                                    <div class="col-sm-1">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276">
                                                                            <small class="list-group-item-heading text_right">
                                                                                <b>Sarary Old </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">อัตราค่าจ้างเดิม</p>
                                                                        </small>
                                                                    </div>

                                                                    <div class="col-sm-3">
                                                                        <asp:TextBox ID="txtSarary_old" runat="server"
                                                                            CssClass="form-control left" ValidationGroup="formInsert"
                                                                            Text='<%# Eval("sarary_old") %>' Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="text_right">
                                                                            <b>Sarary New </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">อัตราค่าจ้างใหม่</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <asp:TextBox ID="txtSarary_new" runat="server"
                                                                            placeholder="กรอกอัตราค่าจ้างใหม่" CssClass="form-control left"
                                                                            ValidationGroup="formInsertsarary"></asp:TextBox>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldtxtSarary_new" runat="server"
                                                                        ControlToValidate="txtSarary_new" Display="None" SetFocusOnError="true"
                                                                        ErrorMessage="*กรอกข้อมูลเงินเดือน" ValidationGroup="formInsertsarary" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="V_txtSarary_new" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtxtSarary_new" Width="160" />

                                                                    <asp:RegularExpressionValidator ID="RegulartxtSarary_new"
                                                                        runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                                                        Display="None" ControlToValidate="txtSarary_new" ValidationExpression="^[0-9',.\s]{1,20}$"
                                                                        ValidationGroup="formInsertsarary" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtSarary_new" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegulartxtSarary_new" Width="160" />
                                                                </div>
                                                                <h3>&nbsp;</h3>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Date of Approve  </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">วันที่มีผลอนุมัติ</p>
                                                                        </small>
                                                                    </div>

                                                                    <div class="col-sm-3">
                                                                        <div class='input-group date'>

                                                                            <asp:TextBox ID="txt_date_approve_sarary" runat="server"
                                                                                CssClass="from-date-datepicker form-control"
                                                                                AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                                            <span class="input-group-addon"><span data-icon-element=""
                                                                                class="fa fa-calendar"></span></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Motive of pay </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">เหตุผลการปรับค่าจ้าง</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <asp:DropDownList ID="ddl_motive" runat="server"
                                                                            CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                            <asp:ListItem Value="0">เลือกเหตุผล</asp:ListItem>
                                                                            <asp:ListItem Value="1">ปรับผ่านทดลองงาน</asp:ListItem>
                                                                            <asp:ListItem Value="2">ปรับประจำปี</asp:ListItem>
                                                                            <asp:ListItem Value="3">ปรับกรณีพิเศษ</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <h3>&nbsp;</h3>
                                                                <asp:Panel ID="box_date_Probation" runat="server" Visible="false">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-1">
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Probation Date </b></small></font>
                                                                            <small>
                                                                                <p class="list-group-item-text">วันที่ผ่านการทดลองงาน</p>
                                                                            </small>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class='input-group date'>
                                                                                <asp:TextBox ID="txt_date_Probation" runat="server"
                                                                                    CssClass="from-date-datepicker form-control"
                                                                                    AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                                                <span class="input-group-addon"><span data-icon-element=""
                                                                                    class="fa fa-calendar"></span></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                                <h1>&nbsp;</h1>

                                                                <div class="form-group">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <i class="fa fa-file"></i>&nbsp;
                                                                            <b>Benefits New</b> (ผลประโยชน์ที่ได้รับใหม่)
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel panel-success">
                                                                        <div class="checkbox checkbox-primary"
                                                                            style="margin-top: 0.0%; margin-left: 9%; margin-bottom: 2.5%;">
                                                                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                                                            <asp:CheckBoxList ID="cbrecipients" runat="server" CellPadding="1"
                                                                                CellSpacing="4" RepeatColumns="3" Width="100%"
                                                                                RepeatDirection="Vertical" RepeatLayout="Table"
                                                                                TextAlign="Right" />

                                                                        </div>
                                                                    </div>

                                                                    <asp:Repeater ID="Repeater1" runat="server">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txt_id_benafits" Text='<%# Eval("benefit_idx") %>'
                                                                                runat="server"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>


                                                                    <asp:LinkButton ID="LinkButton4" CssClass="btn btn-default pull-right"
                                                                        data-toggle="tooltip" title="ยกเลิก" runat="server" CommandName="BtnCancel"
                                                                        OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยกเลิกการปรับเงินเดือนใช่หรือไม่ ?')">
                                                                                <i class="fa fa-cancel"></i> ยกเลิก</asp:LinkButton>&nbsp;
                                                                    <asp:LinkButton ID="save_sarary" CssClass="btn btn-success pull-right" ValidationGroup="formInsertsarary"
                                                                        data-toggle="tooltip" title="บันทึกข้อมูล" runat="server" CommandName="btn_save_sarary"
                                                                        OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกการปรับข้อมูลเงินเดือนใช่หรือไม่ ?')">
                                                                                <i class="fa fa-save"></i> บันทึก</asp:LinkButton>
                                                                </div>
                                                                <h5>&nbsp;</h5>

                                                                <%--   ประวัติ--%>
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <i class="fa fa-file"></i>&nbsp;
                                                                        <b>History</b> (ประวัติการปรับค่าจ้าง)
                                                                    </div>
                                                                    <div class="panel-body">
                                                                        <asp:Repeater ID="sarary_history" runat="server">
                                                                            <HeaderTemplate>
                                                                                <div class="row">
                                                                                    <small>
                                                                                        <label class="col-sm-2 control-label">วันที่</label></small>
                                                                                    <small>
                                                                                        <label class="col-sm-2 control-label">อัตราค่าจ้างเดิม</label></small>
                                                                                    <small>
                                                                                        <label class="col-sm-2 control-label">อัตราค่าจ้างใหม่</label></small>
                                                                                    <small>
                                                                                        <label class="col-sm-2 control-label">วันที่มีผลอนุมัติ</label></small>
                                                                                    <small>
                                                                                        <label class="col-sm-3 control-label">เหตุผลการปรับค่าจ้าง</label></small>
                                                                                    <%--<small><label class="col-sm-2 control-label">สวัสดิการ</label></small></small>--%>
                                                                                </div>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <div class="row">
                                                                                    <div class="col-sm-2">
                                                                                        <small><span><%# Eval("date_create_sarary") %></span></small>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <small><span><%# Eval("sarary_old") %></span></small>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <small><span><%# Eval("sarary_new") %></span></small>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <small><span><%# Eval("date_approve_sarary") %></span></small>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <small><span><%# Eval("motive_of_pay") %></span></small>
                                                                                    </div>
                                                                                    <%-- <div class="col-sm-2">
                                                                                         <small><span><%# Eval("Benefits_new_") %></span></small>
                                                                                    </div>--%>
                                                                                </div>
                                                                                <asp:Label ID="beflist" Visible="false" runat="server"
                                                                                    Text='<%# Eval("Benefits_new_") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </div>
                                                                </div>
                                                                <!-- history log -->
                                                                <asp:GridView ID="gvBenefitsList"
                                                                    runat="server" Visible="false"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="info"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="5"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-Width="3%"
                                                                            HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="สวัสดิการ" HeaderStyle-Width="7%"
                                                                            HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="list_BenefitsList" runat="server"
                                                                                        Text='<%# Eval("Benefits_list")%>'></asp:Label>
                                                                                    <asp:Label ID="list_BenefitsListID" Visible="false"
                                                                                        runat="server" Text='<%# Eval("BenefitsID")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <%--    </ItemTemplate>
                                               </asp:Repeater>--%>
                                                    </EditItemTemplate>
                                                </asp:FormView>

                                            </asp:Panel>

                                        </div>
                                        <%-- ------------------------------- ปิดฟอร์มการปรับค่าจ้าง----------------------------------------------%>
                                        <%--   <div class="form-group">--%>
                                        <%-- ---------เปิดฟอร์มบันทึกการลงโทษพนักงาน------------------%>
                                        <div id="BoxPenaltyEmployee" runat="server">
                                            <asp:UpdatePanel ID="upload_file_penalty" runat="server" UpdateMode="Conditional"
                                                ChildrenAsTriggers="true">
                                                <ContentTemplate>
                                                    <%--     <hr />--%>
                                                    <%--   <h5>&nbsp;</h5>--%>
                                                    <%--<div class="form-group">--%>
                                                    <asp:Panel ID="Panel4" runat="server">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h3 class="panel-title"><i class="fa fa-user"></i>&nbsp;<strong>
                                                                    &nbsp; บันทึกการลงโทษของพนักงาน</strong></h3>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="form-group">
                                                                    <div class="col-sm-1">
                                                                    </div>
                                                                    <%--     <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>No. Warning Letter </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">เลขที่หนังสือเตือน</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <asp:TextBox ID="txt_warninig_number" runat="server"
                                                                            CssClass="form-control left" ValidationGroup="formInsert"
                                                                            placeholder="เลขที่หนังสือเตือน"></asp:TextBox>
                                                                    </div>--%>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Date of  Warning Letter  </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">วันที่ออกหนังสือเตือน</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class='input-group date'>

                                                                            <asp:TextBox ID="txt_date_of_warning" runat="server"
                                                                                CssClass="from-date-datepicker form-control"
                                                                                AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                                            <span class="input-group-addon"><span data-icon-element=""
                                                                                class="fa fa-calendar"></span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <h3>&nbsp;</h3>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>penalty Number </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">การลงโทษครั้งที่</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <asp:TextBox ID="txt_number_penalty" runat="server"
                                                                            CssClass="form-control left" ValidationGroup="formInsert"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="R_txt_number_penalty" runat="server"
                                                                            ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None"
                                                                            ControlToValidate="txt_number_penalty" ValidationExpression="^[0-9]{1,10}$" ValidationGroup="Save" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_txt_number_penalty" runat="Server"
                                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_txt_number_penalty" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Delinquent Date  </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">วันที่กระทำผิด</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class='input-group date'>

                                                                            <asp:TextBox ID="txt_date_delinquent" runat="server"
                                                                                CssClass="from-date-datepicker form-control"
                                                                                AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                                            <span class="input-group-addon"><span data-icon-element=""
                                                                                class="fa fa-calendar"></span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <h1>&nbsp;</h1>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Delinquent Details </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">รายละเอียดความผิด</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-8">
                                                                        <asp:TextBox ID="txt_details_penalty" runat="server"
                                                                            CssClass="form-control left" ValidationGroup="formInsert"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <h1>&nbsp;</h1>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>The penalty was </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">โทษที่ได้รับ</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <asp:DropDownList ID="ddl_type_penalty" runat="server"
                                                                            CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                            OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                            <asp:ListItem Value="0">เลือกโทษที่ได้รับ</asp:ListItem>
                                                                            <asp:ListItem Value="1">ตักเตือนด้วยวาจา</asp:ListItem>
                                                                            <asp:ListItem Value="2">ตักเตือนด้วยลายลักษณ์อักษร</asp:ListItem>
                                                                            <asp:ListItem Value="3">พักงาน</asp:ListItem>
                                                                            <asp:ListItem Value="4">เลิกจ้าง โดยไม่จ่ายค่าชดเชย</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <h1>&nbsp;</h1>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Quantity Day </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">จำนวนวัน</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txt_quantity_day" runat="server"
                                                                            CssClass="form-control left" ValidationGroup="formInsert" Enabled="false"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="Regulartxt_quantity_day" runat="server"
                                                                            ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None"
                                                                            ControlToValidate="txt_quantity_day" ValidationExpression="^[0-9]{1,10}$" ValidationGroup="Save" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_quantity_day" runat="Server"
                                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regulartxt_quantity_day" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Day </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">วัน</p>
                                                                        </small>
                                                                    </div>
                                                                </div>
                                                                <h3>&nbsp;</h3>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>From Date  </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">ตั้งแต่วันที่</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class='input-group date'>

                                                                            <asp:TextBox ID="txt_start_date_break" runat="server"
                                                                                CssClass="from-date-datepicker form-control" AutoComplete="off"
                                                                                MaxLengh="100%" Enabled="false"></asp:TextBox>
                                                                            <span class="input-group-addon"><span data-icon-element=""
                                                                                class="fa fa-calendar"></span></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>To Date  </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">ถึงวันที่</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class='input-group date'>
                                                                            <asp:TextBox ID="txt_end_date_break" runat="server"
                                                                                CssClass="from-date-datepicker form-control" AutoComplete="off"
                                                                                MaxLengh="100%" Enabled="false"></asp:TextBox>
                                                                            <span class="input-group-addon"><span data-icon-element=""
                                                                                class="fa fa-calendar"></span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                </div>
                                                                <h1>&nbsp;</h1>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Upload File </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">เลือกเอกสารเพิ่มเติม</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-9">
                                                                        <asp:FileUpload ID="upload_file" ClientIDMode="Static" runat="server"
                                                                            CssClass="control-label multi" accept="pdf" />
                                                                        <p class="help-block"><font color="#ff6666">**แนบเอกสารเพิ่มเติม นามสกุลที่รองรับ  pdf  </font></p>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Next penalty </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">โทษครั้งต่อไป</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <asp:DropDownList ID="ddl_next_penalty" runat="server" CssClass="form-control fa-align-left">
                                                                            <asp:ListItem Value="0">เลือกโทษครั้งต่อไป</asp:ListItem>
                                                                            <asp:ListItem Value="1">ตักเตือนด้วยวาจา</asp:ListItem>
                                                                            <asp:ListItem Value="2">ตักเตือนด้วยลายลักษณ์อักษร</asp:ListItem>
                                                                            <asp:ListItem Value="3">พักงาน</asp:ListItem>
                                                                            <asp:ListItem Value="4">เลิกจ้าง โดยไม่จ่ายค่าชดเชย</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <h1>&nbsp;</h1>
                                                                <div class="form-group">
                                                                    <div class="col-sm-1">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                            <b>Name of the warning letter </b></small></font>
                                                                        <small>
                                                                            <p class="list-group-item-text">ชื่อผู้ออกหนังสือเตือน</p>
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <asp:TextBox ID="name_of_warning" runat="server"
                                                                            CssClass="form-control left" ValidationGroup="formInsert" placeholder="ลงชื่อผู้ออกหนังสือเตือน">
                                                                        </asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <h1>&nbsp;</h1>
                                                                <h4>&nbsp;</h4>
                                                                <%--     <hr />--%>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <asp:LinkButton ID="LinkButton6" CssClass="btn btn-default pull-right"
                                                                            data-toggle="tooltip" title="ยกเลิก" runat="server" CommandName="BtnCancel"
                                                                            OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยกเลิกบันทึกการงโทษพนักงานใช่หรือไม่ ?')">
                                                                        ยกเลิก</asp:LinkButton>
                                                                        <asp:LinkButton ID="SaveWarning" CssClass="btn btn-success pull-right"
                                                                            data-toggle="tooltip" title="บันทึกข้อมูล" runat="server" CommandName="btnSaveWarning"
                                                                            OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกข้อมูลใช่หรือไม่ ?')">
                                                                            บันทึก</asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                                <h5>&nbsp;</h5>
                                                                <%--   ประวัติ--%>
                                                                <div class="form-group">
                                                                    <div class="col-lg-12">
                                                                        <div class="panel panel-warning">
                                                                            <div class="panel-heading"><i class="fa fa-file"></i>&nbsp;<b>History Penalty</b> (ประวัติการบันทึกโทษพนักงาน)</div>
                                                                            <div class="panel-body">
                                                                                <asp:GridView ID="gridviewHistory" runat="server" ShowHeader="true" AutoGenerateColumns="false"
                                                                                    Visible="true" GridLines="None" CssClass="table table-striped table-hover " OnRowDataBound="Master_RowDataBound">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="วันที่สร้าง" HeaderStyle-Width="5%" ItemStyle-HorizontalAlign="Left"
                                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                            <ItemTemplate>
                                                                                                <small>
                                                                                                    <div style="padding-top: 5px;">
                                                                                                        <asp:Label ID="labledateCreate" runat="server"
                                                                                                            CssClass="col-sm-12" Text='<%# Eval("create_date_penalty") %>'></asp:Label>
                                                                                                    </div>
                                                                                                </small>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                         <asp:TemplateField HeaderText="วันที่ออกใบเตือน" HeaderStyle-Width="7%" ItemStyle-HorizontalAlign="Left"
                                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                            <ItemTemplate>
                                                                                                <small>
                                                                                                    <div style="padding-top: 5px;">
                                                                                                        <asp:Label ID="labledateCreate1" runat="server"
                                                                                                            CssClass="col-sm-12" Text='<%# Eval("date_of_warning") %>'></asp:Label>
                                                                                                    </div>
                                                                                                </small>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                          <asp:TemplateField HeaderText="ครั้งที่"  HeaderStyle-Width="3%" ItemStyle-HorizontalAlign="Left"
                                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                            <ItemTemplate>
                                                                                                <small>
                                                                                                    <div style="padding-top: 5px;">
                                                                                                        <asp:Label ID="labledateCreate2" runat="server"
                                                                                                            CssClass="col-sm-12" Text='<%# Eval("penalty_number") %>'></asp:Label>
                                                                                                    </div>
                                                                                                </small>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                          <asp:TemplateField HeaderText="วันที่กระทำผิด" HeaderStyle-Width="5%"  ItemStyle-HorizontalAlign="Left"
                                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                            <ItemTemplate>
                                                                                                <small>
                                                                                                    <div style="padding-top: 5px;">
                                                                                                        <asp:Label ID="labledateCreate3" runat="server"
                                                                                                            CssClass="col-sm-12" Text='<%# Eval("date_delinquent") %>'></asp:Label>
                                                                                                    </div>
                                                                                                </small>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                          <asp:TemplateField HeaderText="โทษที่ได้รับ" HeaderStyle-Width="10%"  ItemStyle-HorizontalAlign="Left"
                                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                            <ItemTemplate>
                                                                                                <small>
                                                                                                    <div style="padding-top: 5px;">
                                                                                                        <asp:Label ID="labledateCreate4" runat="server"
                                                                                                            CssClass="col-sm-12" Text='<%# Eval("date_delinquent") %>'></asp:Label>
                                                                                                    </div>
                                                                                                </small>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="ผู้ออกใบเตือน" HeaderStyle-Width="25%" ItemStyle-HorizontalAlign="center"
                                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                            <ItemTemplate>
                                                                                                <small>
                                                                                                    <div style="padding-top: 5px;">
                                                                                                        <asp:Label ID="labledateCreate5" runat="server"
                                                                                                            CssClass="col-sm-12" Text='<%# Eval("name__") %>'></asp:Label>
                                                                                                    </div>
                                                                                                </small>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                           <asp:TemplateField HeaderText="รายละเอียดความผิด"  HeaderStyle-Width="25%" ItemStyle-HorizontalAlign="Left"
                                                                                            HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small">
                                                                                            <ItemTemplate>
                                                                                                <small>
                                                                                                    <div style="padding-top: 5px;">
                                                                                                        <asp:Label ID="labledateCreate6" runat="server"
                                                                                                            CssClass="col-sm-12" Text='<%# Eval("details__") %>'></asp:Label>
                                                                                                    </div>
                                                                                                </small>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                         <asp:TemplateField HeaderText="ไฟล์" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="center"
                                                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                            <ItemTemplate>
                                                                                                <small>
                                                                                                      <asp:TextBox ID="valueDocumentCode" runat="server" Visible="false"
                                                                                                            CssClass="col-sm-12" Text='<%# Eval("warninig_number") %>'></asp:TextBox>
                                                                                                   <asp:HyperLink runat="server" Visible="false" ID="fileDocument" data-original-title="ไฟล์แนบ"
                                                                        Target="_blank" NavigateUrl='<%# Eval("warninig_number") %>'>ไฟล์แนบ</asp:HyperLink>
                                                                                                </small>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>

                                                                                    </Columns>
                                                                                </asp:GridView>


                                                                            </div>
                                                                        </div>
                                                                        <%--  <small><span><%# Eval("details__") %></span></small>--%><%-- NavigateUrl='<%# retrunpat((int)Eval("midx")) %>'--%>

                                                                        <!-- history log -->
                                                                    </div>
                                                                </div>
                                                    </asp:Panel>
                                                    <%-- ------------------------------- ปิดฟอร์มบันทึกการลงโทษพนักงาน----------------------------------------------%>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="SaveWarning" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                        <%-- ------------------------------- เปิดฟอร์มแก้ไขข้อมูลพนักงาน----------------------------------------------%>
                                        <div class="form-horizontal" role="form">
                                            <div class="form-group">
                                                <div class="row">
                                                    <asp:GridView ID="gvpicture" runat="server" CssClass="table"
                                                        BorderColor="White"
                                                        ShowHeader="false" border-color="#FFFFFF" HeaderStyle-BorderStyle="None" GridLines="None"
                                                        BorderStyle="None" HeaderStyle-BorderColor="White" FooterStyle-BorderColor="White"
                                                        AutoGenerateColumns="false" Visible="true"
                                                        OnRowDataBound="Master_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField ControlStyle-BorderColor="White">
                                                                <ItemTemplate>
                                                                    <asp:Repeater ID="Repeater_pictureProfile" runat="server">
                                                                        <ItemTemplate>
                                                                            <div class="row">
                                                                                <center><asp:Image ID="ImagProfile" runat="server" 
                                                                    ImageUrl='<%# retrunpat_edit((string)Eval("emp_code"))%>' 
                                                                    style="width: 150px; height: 150px" class="img-thumbnail img-thumbnail">
                                                                 </asp:Image></center>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                    <asp:TextBox ID="txtemp_code"
                                                                        Text='<%# Eval("emp_code") %>' runat="server"
                                                                        CssClass="form-control left" Visible="false">
                                                                    </asp:TextBox>
                                                                    <center>
                                                                   <asp:Panel id="ImagProfile_defaultedit"
                                                                        runat="server" Visible="false">
                                                                    <img class="media-object img-thumbnail"
                                                                     src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>'
                                                                    width="150px" height="150px"  
                                                                    style="margin-right: 8px; margin-top: 5px; margin-bottom: -5px;">
                                                                    </asp:Panel>
                                                                 </center>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <h5>&nbsp;</h5>
                                                    <center><asp:FileUpload ID="upload_profile_pic" ClientIDMode="Static" 
                                                        CssClass="btn btn-primary btn-file" runat="server" accept="jpg" />
                                                                </center>
                                                    <h5>&nbsp;</h5>
                                                    <center><asp:LinkButton ID="btn_save_pic" runat="server" 
                                                        CssClass="btn btn-warning btn-sm" data-toggle="tooltip" 
                                                        title="อัพโหลดภาพ" CommandName="save_picture_edit" OnCommand="btnCommand">
                                                        <i class="glyphicon glyphicon-picture"></i> อัพโหลดภาพ
                                                            </asp:LinkButton></center>
                                                    <h5>&nbsp;</h5>
                                                    <asp:FormView ID="fvEdit_dataEmployee" DataKeyNames="emp_idx"
                                                        runat="server" Width="100%" DefaultMode="Edit">
                                                        <EditItemTemplate>
                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading">
                                                                    <i class="glyphicon glyphicon-file"></i>
                                                                    &nbsp;<b>Profile</b> (ข้อมูลพนักงานเบื้องต้น)
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Employee In </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">วันที่เริ่มเข้าทำงาน</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <div class='input-group date'>
                                                                                    <asp:TextBox ID="txtEmpin_Edit" runat="server"
                                                                                        CssClass="from-date-datepicker form-control"
                                                                                        AutoComplete="off" MaxLengh="100%"
                                                                                        ValidationGroup="formEdit_Save" Enabled="false"
                                                                                        Text='<%# Eval("emp_start_date") %>'></asp:TextBox>
                                                                                    <span class="input-group-addon"><span data-icon-element=""
                                                                                        class="glyphicon glyphicon-calendar"></span></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276">
                                                                                    <small class="list-group-item-heading text_right">
                                                                                    <b>Employee Satatus </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">สถานะพนักงาน</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-5">
                                                                                <asp:CheckBox ID="chk_probation" runat="server" AutoPostBack="true"
                                                                                    OnCheckedChanged="chkSelectedIndexChanged" />&nbsp;
                                                                                <span>
                                                                                    <asp:Label ID="doing" runat="server"
                                                                                        Visible="false" class="label label-primary"
                                                                                        Text="อยู่ระหว่างทดลองงาน" /></span>

                                                                                <asp:CheckBox ID="chk_pass_probation" runat="server" AutoPostBack="true"
                                                                                    OnCheckedChanged="chkSelectedIndexChanged" />&nbsp;
                                                                                <span class="label label-success">ผ่านการทดลองงาน</span>
                                                                                <asp:CheckBox ID="chk_termination" runat="server" AutoPostBack="true"
                                                                                    OnCheckedChanged="chkSelectedIndexChanged" />&nbsp; 
                                                                                <span class="label label-danger">พ้นสภาพพนักงาน</span> &nbsp;
                                                                                 <asp:TextBox ID="txt_status_employee" runat="server" Visible="false"
                                                                                     CssClass="form-control" Text='<%# Eval("emp_status") %>'></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <asp:Panel ID="box_probation_date" runat="server" Visible="false">
                                                                            <div class="form-group">
                                                                                <div class="col-sm-2">
                                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Probation Date </b></small></font>
                                                                                    <small>
                                                                                        <p class="list-group-item-text">วันที่ผ่านการทดลองงาน</p>
                                                                                    </small>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <div class='input-group date'>
                                                                                        <asp:TextBox ID="TextBox1" runat="server"
                                                                                            CssClass="from-date-datepicker form-control"
                                                                                            AutoComplete="off" MaxLengh="100%"
                                                                                            ValidationGroup="formEdit_Save" Enabled="false"
                                                                                            Text='<%# Eval("emp_probation_date") %>'></asp:TextBox>
                                                                                        <span class="input-group-addon"><span data-icon-element=""
                                                                                            class="glyphicon glyphicon-calendar"></span></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </asp:Panel>
                                                                        <asp:Panel ID="box_probation_edit" runat="server" Visible="false">
                                                                            <div class="form-group">
                                                                                <div class="col-sm-2">
                                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Probation Date </b></small></font>
                                                                                    <small>
                                                                                        <p class="list-group-item-text">วันที่ผ่านการทดลองงาน</p>
                                                                                    </small>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <div class='input-group date'>
                                                                                        <asp:TextBox ID="txt_probation_edit" runat="server"
                                                                                            CssClass="from-date-datepicker form-control"
                                                                                            AutoComplete="off" MaxLengh="100%"
                                                                                            ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                        <span class="input-group-addon"><span data-icon-element=""
                                                                                            class="glyphicon glyphicon-calendar"></span></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </asp:Panel>
                                                                        <asp:Panel ID="box_emp_end_date" runat="server" Visible="false">
                                                                            <div class="form-group">
                                                                                <div class="col-sm-2">
                                                                                    <font color="#1A5276">
                                                                                        <small class="list-group-item-heading text_right">
                                                                                    <b>Probation Date </b></small></font>
                                                                                    <small>
                                                                                        <p class="list-group-item-text">วันที่พ้นสภาพพนักงาน</p>
                                                                                    </small>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <div class='input-group date'>
                                                                                        <asp:TextBox ID="txt_emp_end_date"
                                                                                            runat="server" CssClass="from-date-datepicker form-control"
                                                                                            AutoComplete="off" MaxLengh="100%"
                                                                                            ValidationGroup="formEdit_Save" Enabled="false"
                                                                                            Text='<%# Eval("emp_end_date") %>'></asp:TextBox>
                                                                                        <span class="input-group-addon"><span data-icon-element=""
                                                                                            class="glyphicon glyphicon-calendar"></span></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </asp:Panel>
                                                                        <asp:Panel ID="box_add_end_date" runat="server" Visible="false">
                                                                            <div class="form-group">
                                                                                <div class="col-sm-2">
                                                                                    <font color="#1A5276">
                                                                                        <small class="list-group-item-heading text_right">
                                                                                    <b>Probation Date </b></small></font>
                                                                                    <small>
                                                                                        <p class="list-group-item-text">วันที่พ้นสภาพพนักงาน</p>
                                                                                    </small>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <div class='input-group date'>
                                                                                        <asp:TextBox ID="txt_emp_end_date_add" runat="server"
                                                                                            CssClass="from-date-datepicker form-control"
                                                                                            AutoComplete="off" MaxLengh="100%" AutoPostBack="true"
                                                                                            ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                        <span class="input-group-addon"><span data-icon-element=""
                                                                                            class="glyphicon glyphicon-calendar"></span></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </asp:Panel>
                                                                        <asp:Label ID="LoEmpCode" runat="server"
                                                                            Visible="false" Text='<%# Eval("emp_idx") %>' />
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right">
                                                                                    <b>Employee Code</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">รหัสพนักงาน</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtEmpCode_new_Edit"
                                                                                    Text='<%# Eval("emp_code") %>' runat="server"
                                                                                    CssClass="form-control left"
                                                                                    ValidationGroup="formEdit_Save" Enabled="false">
                                                                                </asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276">
                                                                                    <small class="list-group-item-heading text_right">
                                                                                    <b>Employee Type </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ประเภทพนักงาน</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txttype" Text='<%# Eval("emp_type_idx") %>'
                                                                                    runat="server" Visible="false" CssClass="form-control left"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddEmpType_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" Text='<%# Eval("emp_type_idx") %>'
                                                                                    AutoPostBack="true" ValidationGroup="formEdit_Save">
                                                                                    <asp:ListItem Value="0">เลือกประเภทพนักงาน</asp:ListItem>
                                                                                    <asp:ListItem Value="1">รายวัน</asp:ListItem>
                                                                                    <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                                                                                    <asp:ListItem Value="3">รายเดือน จ่าย 2 ครั้ง</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row-fluid">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <div class="panel panel-primary">
                                                                            <div class="panel-heading">
                                                                                <h4 class="panel-title">
                                                                                    <i class="fa fa-user"></i>&nbsp;&nbsp; ตำแหน่งพนักงาน</h4>
                                                                            </div>
                                                                            <div class="panel-body">
                                                                                <div class="form-group">
                                                                                    <div class="col-sm-12">
                                                                                        <asp:LinkButton ID="show_position_edit" CssClass="btn btn-success btn-sm"
                                                                                            runat="server" data-toggle="tooltip" title="เพิ่มตำแหน่งพนักงาน"
                                                                                            OnCommand="btnCommand" CommandName="show_position_edit">
                                                                                            <i class="glyphicon glyphicon-plus-sign"></i> เพิ่มตำแหน่ง
                                                                                        </asp:LinkButton>
                                                                                        <br />
                                                                                        <asp:Panel ID="boxedit_addposition" runat="server" Visible="false">
                                                                                            <h4><span class="label label-default">เพิ่มตำแหน่ง</span></h4>
                                                                                            <div class="panel panel-success">
                                                                                                <div class="form-group">
                                                                                                    <h5>&nbsp;</h5>
                                                                                                    <div class="col-sm-1"></div>
                                                                                                    <asp:Label ID="Label1" CssClass="col-sm-3 control-label"
                                                                                                        runat="server" Text="ชื่อบริษัท" />
                                                                                                    <div class="col-sm-5">
                                                                                                        <asp:DropDownList ID="ddl_add_org_idx" runat="server"
                                                                                                            CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                                            OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                                                        </asp:DropDownList>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="form-group">
                                                                                                    <div class="col-sm-1"></div>
                                                                                                    <asp:Label ID="Label8" CssClass="col-sm-3 control-label"
                                                                                                        runat="server" Text="ชื่อฝ่าย" />
                                                                                                    <div class="col-sm-5">
                                                                                                        <asp:DropDownList ID="ddl_add_rdept_idx" runat="server"
                                                                                                            CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                                            OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                                                        </asp:DropDownList>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="form-group">
                                                                                                    <div class="col-sm-1"></div>
                                                                                                    <asp:Label ID="Label9" CssClass="col-sm-3 control-label"
                                                                                                        runat="server" Text="ชื่อแผนก" />
                                                                                                    <div class="col-sm-5">
                                                                                                        <asp:DropDownList ID="ddl_add_rsec_idx" runat="server"
                                                                                                            CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                                            OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                                                        </asp:DropDownList>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="form-group">
                                                                                                    <div class="col-sm-1"></div>
                                                                                                    <asp:Label ID="Label10" CssClass="col-sm-3 control-label"
                                                                                                        runat="server" Text="ชื่อตำแหน่ง" />
                                                                                                    <div class="col-sm-5">
                                                                                                        <asp:DropDownList ID="ddl_add_rpos_idx" runat="server"
                                                                                                            OnSelectedIndexChanged="DdSelectedIndexChanged"
                                                                                                            CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                                                        </asp:DropDownList>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="form-group">
                                                                                                    <div class="col-sm-1"></div>
                                                                                                    <asp:Label ID="Label11" CssClass="col-sm-3 control-label"
                                                                                                        runat="server" Text="สถานะตำแหน่ง" />
                                                                                                    <div class="col-sm-5">
                                                                                                        <asp:DropDownList ID="ddl_add_rpos_status"
                                                                                                            runat="server" CssClass="form-control fa-align-left"
                                                                                                            AutoPostBack="true">
                                                                                                            <asp:ListItem Value="1">ตำแหน่งหลัก</asp:ListItem>
                                                                                                            <asp:ListItem Value="0">ตำแหน่งรักษาการ</asp:ListItem>
                                                                                                        </asp:DropDownList>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="form-group">
                                                                                                    <div class="col-sm-7"></div>
                                                                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                                <div class="col-sm-3">
                                                                                                    &nbsp;
                                                                                    <asp:LinkButton ID="LinkButton2" CssClass="btn btn-success btn-sm"
                                                                                        runat="server" data-toggle="tooltip" title="บันทึกตำแหน่ง"
                                                                                        OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการเพิ่มตำแหน่งนี้ใช่หรือไม่ ?')"
                                                                                        CommandName="save_position_edit">
                                                                                        <i class="glyphicon glyphicon-floppy-disk"></i> บันทึก</asp:LinkButton>
                                                                                                    <asp:LinkButton ID="LinkButton3" CssClass="btn btn-danger btn-sm"
                                                                                                        runat="server" data-toggle="tooltip" title="ยกเลิก" OnCommand="btnCommand"
                                                                                                        CommandName="cancel__">
                                                                                                        <i class="glyphicon glyphicon-remove-circle"></i> ยกเลิก</asp:LinkButton>
                                                                                                </div>
                                                                                                </div>
                                                                                                <h5>&nbsp;</h5>
                                                                                            </div>
                                                                                        </asp:Panel>
                                                                                        <h5>&nbsp;</h5>
                                                                                        <p>
                                                                                            <font color="red">** กรณีต้องการเปลี่ยนตำแหน่งหลัก 
                                                                                            ให้ท่านเปลี่ยนสถานะที่เป็นตำแหน่งรักษาการให้เป็นตำแหน่งหลักก่อนเสมอ
                                                                                        </p>
                                                                                        </font>
                                                                                         <asp:Panel ID="showalert" runat="server" Visible="true">
                                                                                             <div class="col-sm-12" id="divShowalert">
                                                                                                 <div class="form-group">
                                                                                                     <div class="alert alert-warning" role="alert">
                                                                                                         <strong>!!</strong>ยังไม่มีข้อมูลตำแหน่งของพนักงาน
                                                                                                     </div>
                                                                                                 </div>
                                                                                             </div>
                                                                                         </asp:Panel>
                                                                                        <asp:GridView ID="gv_edit_position"
                                                                                            runat="server"
                                                                                            AutoGenerateColumns="false"
                                                                                            DataKeyNames="EODSPIDX"
                                                                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                                            HeaderStyle-CssClass="info"
                                                                                            HeaderStyle-Height="40px"
                                                                                            AllowPaging="true"
                                                                                            PageSize="10"
                                                                                            OnRowEditing="row_editing"
                                                                                            OnRowUpdating="row_updating"
                                                                                            OnRowCancelingEdit="row_canceling_edit"
                                                                                            OnPageIndexChanging="Master_PageIndexChanging"
                                                                                            OnRowDataBound="row_DataBound">
                                                                                            <PagerStyle CssClass="pageCustom" />
                                                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                                                                FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                                                            <EmptyDataTemplate>
                                                                                                <div style="text-align: center">ไม่พบข้อมูลตำแหน่งพนักงาน</div>
                                                                                            </EmptyDataTemplate>
                                                                                            <Columns>
                                                                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lb_position_idx" runat="server"
                                                                                                            Visible="false" Text='<%# Eval("EODSPIDX") %>' />
                                                                                                        <%# (Container.DataItemIndex +1) %>
                                                                                                    </ItemTemplate>
                                                                                                    <EditItemTemplate>
                                                                                                        <div class="panel-body">
                                                                                                            <div class="form-horizontal" role="form">

                                                                                                                <div class="form-group">
                                                                                                                    <div class="col-sm-2">
                                                                                                                        <asp:TextBox ID="txtposition_idx"
                                                                                                                            runat="server" CssClass="form-control" Visible="false"
                                                                                                                            Text='<%# Eval("EODSPIDX")%>' />
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="form-group">
                                                                                                                    <asp:Label ID="Label1" CssClass="col-sm-3 control-label"
                                                                                                                        runat="server" Text="ชื่อบริษัท" />
                                                                                                                    <div class="col-sm-5">
                                                                                                                        <asp:TextBox ID="txt_org_idx_update" Visible="false"
                                                                                                                            runat="server" CssClass="form-control"
                                                                                                                            Text='<%# Eval("OrgIDX")%>' />
                                                                                                                        <asp:DropDownList ID="ddl_edit_org_idx" runat="server"
                                                                                                                            CssClass="form-control fa-align-left"
                                                                                                                            OnSelectedIndexChanged="DdSelectedIndexChanged"
                                                                                                                            AutoPostBack="true">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="form-group">
                                                                                                                    <asp:Label ID="Label2" CssClass="col-sm-3 control-label"
                                                                                                                        runat="server" Text="ฝ่าย" />
                                                                                                                    <div class="col-sm-5">
                                                                                                                        <asp:TextBox ID="txt_department_edit" Visible="false"
                                                                                                                            runat="server" CssClass="form-control"
                                                                                                                            Text='<%# Eval("RDeptIDX")%>' />
                                                                                                                        <asp:DropDownList ID="ddl_edit_department" runat="server"
                                                                                                                            CssClass="form-control fa-align-left"
                                                                                                                            OnSelectedIndexChanged="DdSelectedIndexChanged"
                                                                                                                            AutoPostBack="true">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="form-group">
                                                                                                                    <asp:Label ID="Label3" CssClass="col-sm-3 control-label"
                                                                                                                        runat="server" Text="แผนก" />
                                                                                                                    <div class="col-sm-5">
                                                                                                                        <asp:TextBox ID="txt_section_edit" Visible="false"
                                                                                                                            runat="server" CssClass="form-control"
                                                                                                                            Text='<%# Eval("RSecIDX")%>' />
                                                                                                                        <asp:DropDownList ID="ddl_edit_section" runat="server"
                                                                                                                            CssClass="form-control fa-align-left"
                                                                                                                            OnSelectedIndexChanged="DdSelectedIndexChanged"
                                                                                                                            AutoPostBack="true">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="form-group">
                                                                                                                    <asp:Label ID="Label4" CssClass="col-sm-3 control-label"
                                                                                                                        runat="server" Text="ตำแหน่ง" />
                                                                                                                    <div class="col-sm-5">
                                                                                                                        <asp:TextBox ID="txt_position_edit" Visible="false"
                                                                                                                            runat="server" CssClass="form-control"
                                                                                                                            Text='<%# Eval("RPosIDX")%>' />
                                                                                                                        <asp:DropDownList ID="ddl_edit_position" runat="server"
                                                                                                                            CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="form-group">
                                                                                                                    <asp:Label ID="Label5" CssClass="col-sm-3 control-label"
                                                                                                                        runat="server" Text="สถานะตำแหน่ง" />
                                                                                                                    <div class="col-sm-5">
                                                                                                                        <asp:DropDownList ID="ddl_edit_status_position"
                                                                                                                            runat="server" CssClass="form-control fa-align-left"
                                                                                                                            Text='<%# Eval("EStaOut")%>' AutoPostBack="true">
                                                                                                                            <asp:ListItem Value="1">ตำแหน่งหลัก</asp:ListItem>
                                                                                                                            <asp:ListItem Value="0">ตำแหน่งรักษาการ</asp:ListItem>
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="form-group">
                                                                                                                    <div class="col-sm-4 col-sm-offset-8">
                                                                                                                        <asp:LinkButton ID="lbCmdUpdate"
                                                                                                                            CssClass="btn btn-success" runat="server"
                                                                                                                            ValidationGroup="Save" CommandName="Update"
                                                                                                                            OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                                                                                                            <i class="fa fa-check"></i></asp:LinkButton>
                                                                                                                        <asp:LinkButton ID="lbCmdCancel"
                                                                                                                            CssClass="btn btn-default" runat="server"
                                                                                                                            CommandName="Cancel"><i class="fa fa-times"></i>
                                                                                                                        </asp:LinkButton>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </EditItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="ชื่อบริษัท" ItemStyle-HorizontalAlign="Left"
                                                                                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                                    <ItemTemplate>
                                                                                                        <small>
                                                                                                            <asp:Label ID="lb_OrgNameTH" runat="server"
                                                                                                                CssClass="col-sm-12" Text='<%# Eval("OrgNameTH") %>'></asp:Label>
                                                                                                        </small>
                                                                                                    </ItemTemplate>

                                                                                                    <EditItemTemplate />
                                                                                                    <FooterTemplate />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left"
                                                                                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                                    <ItemTemplate>
                                                                                                        <small>
                                                                                                            <asp:Label ID="lb_DeptNameTH" runat="server"
                                                                                                                CssClass="col-sm-12" Text='<%# Eval("DeptNameTH") %>'></asp:Label>
                                                                                                        </small>
                                                                                                    </ItemTemplate>

                                                                                                    <EditItemTemplate />
                                                                                                    <FooterTemplate />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left"
                                                                                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                                    <ItemTemplate>
                                                                                                        <small>
                                                                                                            <asp:Label ID="lb_SecNameTH" runat="server" CssClass="col-sm-12"
                                                                                                                Text='<%# Eval("SecNameTH") %>'></asp:Label>
                                                                                                            <%-- <asp:Label ID="lb_rsecIdx" Visible="false" runat="server" CssClass="col-sm-12"
                                                                                                                Text='<%# Eval("rsec_idx") %>'></asp:Label>--%>
                                                                                                        </small>
                                                                                                    </ItemTemplate>

                                                                                                    <EditItemTemplate />
                                                                                                    <FooterTemplate />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left"
                                                                                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                                    <ItemTemplate>
                                                                                                        <small>
                                                                                                            <asp:Label ID="lb_PosNameTH" runat="server" CssClass="col-sm-12"
                                                                                                                Text='<%# Eval("PosNameTH") %>'></asp:Label>
                                                                                                        </small>
                                                                                                    </ItemTemplate>

                                                                                                    <EditItemTemplate />
                                                                                                    <FooterTemplate />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="สถานะตำแหน่ง" ItemStyle-HorizontalAlign="center"
                                                                                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                                    <ItemTemplate>
                                                                                                        <small><b>
                                                                                                            <asp:Label ID="lb_EStaOut" runat="server"
                                                                                                                Text='<%# Eval("EStaOut").ToString() == "1" ?
                                                                                                                    "ตำแหน่งหลัก" : "ตำแหน่งรักษาการ" %>'
                                                                                                                CssClass='<%# Eval("EStaOut").ToString() == "1" ? 
                                                                                                                    "text-success bg-success" : "text-default bg-default" %>' />
                                                                                                            </div>
                                                                                                        </b></small>
                                                                                                    </ItemTemplate>

                                                                                                    <EditItemTemplate />
                                                                                                    <FooterTemplate />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="การจัดการ" ItemStyle-HorizontalAlign="center"
                                                                                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                                    <ItemTemplate>

                                                                                                        <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm"
                                                                                                            runat="server" CommandName="Edit" data-toggle="tooltip"
                                                                                                            title="แก้ไขข้อมูล"><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                                                                                        <asp:LinkButton ID="delete" CssClass="btn btn-danger btn-sm"
                                                                                                            runat="server" data-toggle="tooltip" title="ลบ" CommandName="delete_position"
                                                                                                            OnCommand="btnCommand" CommandArgument='<%# Eval("EODSPIDX") %>'
                                                                                                            OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่ ?')">
                                                                                                            <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                                                                    </ItemTemplate>

                                                                                                    <EditItemTemplate />
                                                                                                    <FooterTemplate />
                                                                                                </asp:TemplateField>

                                                                                            </Columns>

                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <%-- ผู้อนุมัติใบลา--%>
                                                            <asp:Panel ID="BoxApprovers_edit" runat="server" Visible="true">
                                                                <div class="panel panel-primary">
                                                                    <div class="panel-heading">
                                                                        <i class="glyphicon glyphicon-file"></i>&nbsp;<b>Approvers</b> (ผู้อนุมัติใบลา)
                                                                    </div>
                                                                    <h5>&nbsp;</h5>
                                                                    <div class="panel-body">
                                                                        <div class="form-horizontal" role="form">
                                                                            <div class="form-group">
                                                                                <div class="col-sm-6">
                                                                                    <div class="panel panel-success">

                                                                                        <div class="panel-body">
                                                                                            <div class="form-group">
                                                                                                <div class="col-sm-4">
                                                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                                        <b> Approver leave 1 </b></small></font>
                                                                                                    <small>
                                                                                                        <p class="list-group-item-text">ผู้อนุมัติใบลาคนที่ 1</p>
                                                                                                    </small>
                                                                                                </div>
                                                                                                <div class="col-sm-6">
                                                                                                    <asp:TextBox ID="txt_rsec_edit_appropve1"
                                                                                                        Visible="false" Text='<%# Eval("emp_idx_approve1") %>'
                                                                                                        runat="server" CssClass="form-control left"
                                                                                                        ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                                    <asp:DropDownList ID="ddlApprover_Leave1_edit"
                                                                                                        runat="server" CssClass="form-control fa-align-left"
                                                                                                        AutoPostBack="true">
                                                                                                    </asp:DropDownList>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <div class="panel panel-success">
                                                                                        <div class="panel-body">
                                                                                            <div class="form-group">
                                                                                                <div class="col-sm-4">
                                                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                                        <b> Approver leave 2</b></small></font>
                                                                                                    <small>
                                                                                                        <p class="list-group-item-text">ผู้อนุมัติใบลาคนที่ 2</p>
                                                                                                    </small>
                                                                                                </div>
                                                                                                <div class="col-sm-6">
                                                                                                    <asp:TextBox ID="txt_rsec_edit_appropve2" Visible="false"
                                                                                                        Text='<%# Eval("emp_idx_approve2") %>' runat="server"
                                                                                                        CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                                    <asp:DropDownList ID="ddlApprover_Leave2_edit" runat="server"
                                                                                                        CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                                                    </asp:DropDownList>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>

                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading">
                                                                    <i class="glyphicon glyphicon-file"></i>&nbsp;<b>Shift Time Information</b> (ข้อมูลตารางงานพนักงาน)
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">

                                                                        <asp:FormView ID="editformview_shiftTime" OnDataBound="FormView_DataBound"
                                                                            runat="server" DefaultMode="Insert" Width="100%">
                                                                            <InsertItemTemplate>
                                                                                <div class="col-lg-6" runat="server" id="Div6">
                                                                                    <div class="panel panel-info">
                                                                                        <div class="panel-heading">
                                                                                            <h3 class="panel-title">
                                                                                                <i class="glyphicon glyphicon-edit"></i><strong>&nbsp; ประเภทตารางงาน</strong></h3>
                                                                                        </div>
                                                                                        <div class="panel-body">
                                                                                            <div class="pull-left">
                                                                                                <a href="#">
                                                                                                    <img class="media-object img-thumbnail"
                                                                                                        src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/iconTime.png")%>'
                                                                                                        width="60px" height="60px" style="margin-right: 8px; margin-top: -5px;">
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="post-content">
                                                                                                <div style="margin-top: -10px;">
                                                                                                    &nbsp;&nbsp;&nbsp;    
                                                                                        <div class="col-sm-8">
                                                                                            <asp:RadioButton ID="checkstaticShift_edit" runat="server" OnCheckedChanged="RadioSelectedIndexChanged" AutoPostBack="true" />&nbsp; <font color="#1A5276">กะคงที่ (Shift Time Static)</font>
                                                                                        </div>
                                                                                                    <div class="col-sm-8">
                                                                                                        <asp:RadioButton ID="checkDynamicShift_edit" runat="server" OnCheckedChanged="RadioSelectedIndexChanged"
                                                                                                            AutoPostBack="true" />&nbsp; <font color="#1A5276">กะหมุนเวียน (Shift Time Dynamic)</font>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <asp:LinkButton ID="resetShifttime" runat="server"
                                                                                                    CommandName="resetShifttime"
                                                                                                    OnCommand="btnCommand" AutoPostBack="true"
                                                                                                    CssClass="btn btn-danger btn-sm" data-toggle="tooltip"
                                                                                                    title="ล้างค่า">ล้างค่า
                                                                                                </asp:LinkButton>

                                                                                            </div>
                                                                                            <h5>&nbsp;</h5>
                                                                                            <%-- <h5>&nbsp;</h5>--%>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-6" runat="server" id="Div7">
                                                                                    <div class="panel panel-info">
                                                                                        <div class="panel-heading">
                                                                                            <h3 class="panel-title">
                                                                                                <i class="glyphicon glyphicon-edit"></i><strong>&nbsp; ตารางงาน</strong></h3>
                                                                                        </div>
                                                                                        <div class="panel-body">
                                                                                            <div class="pull-left">
                                                                                                <a href="#">
                                                                                                    <img class="media-object img-thumbnail"
                                                                                                        src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/iconTime.png")%>'
                                                                                                        width="60px" height="60px" style="margin-right: 8px; margin-top: -5px;">
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="post-content">
                                                                                                &nbsp;&nbsp;&nbsp;    
                                                                                    <div class="form-group">
                                                                                        <div style="margin-top: -10px;">
                                                                                            <div class="col-sm-3">
                                                                                                <font color="#1A5276"><small class="list-group-item-heading text_left">กะเวลางาน
                                                                                    <br />
                                                                                    <p>Shift Time </p>
                                                                                                      </small></font>
                                                                                            </div>
                                                                                            <div class="col-sm-6">
                                                                                                <asp:DropDownList ID="ddlist_edit_ShiftTime"
                                                                                                    runat="server" CssClass="form-control fa-align-left"
                                                                                                    AutoPostBack="true" Enabled="false">
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="margin-top: 0px; margin-bottom: 0px;">
                                                                                    <div class="col-lg-12" runat="server" id="Div8">
                                                                                        <div class="panel panel-info">
                                                                                            <div class="panel-heading">
                                                                                                <h3 class="panel-title">
                                                                                                    <i class="glyphicon glyphicon-file"></i><strong>&nbsp; ข้อมูลกะงานของพนักงาน</strong></h3>
                                                                                            </div>
                                                                                            <div class="panel-body">
                                                                                                <div class="col-lg-12 pull-left">
                                                                                                    <asp:Label ID="alertNoneShiftTime" Width="100%" runat="server" Text="">
                                                            <div class="alert alert-warning" role="alert">
                                                            <small>ยังไม่มีข้อมูลกะงานของพนักงานท่านนี้..</small>
                                                            </div>
                                                                                                    </asp:Label>
                                                                                                    <asp:Repeater ID="Information_alertHaveShiftTime" runat="server">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="txtidxTypeShiftTime" Text='<%# Eval("idxTypeShiftTime") %>'
                                                                                                                runat="server" CssClass="control-label" Visible="false"></asp:TextBox>
                                                                                                             <asp:TextBox ID="txtshiftTime_idx" Text='<%# Eval("shiftTime_idx") %>'
                                                                                                                runat="server" CssClass="control-label" Visible="false"></asp:TextBox>
                                                                                                            <asp:Label ID="alertHaveShiftTime" Width="100%" runat="server" Visible="true" Text="">
                                                            <div class="alert alert-success" role="alert">
                                                                  <p>ประเภทกะ :  <%# Eval("nameType_shift") %>&nbsp; เวลา :<%# Eval("nameTime_shift") %></p> 
                                                            </div>
                                                                                                            </asp:Label>
                                                                                                            <asp:Label ID="alertHaveShiftTimeDynamic" Width="100%" runat="server" Visible="false" Text="">
                                                            <div class="alert alert-success" role="alert">
                                                                  <p>ประเภทกะ :  <%# Eval("nameType_shift") %></p> 
                                                            </div>
                                                                                                            </asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:Repeater>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                            </InsertItemTemplate>
                                                                        </asp:FormView>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading">
                                                                    <i class="glyphicon glyphicon-file"></i>&nbsp;<b>personal Information</b> (ข้อมูลส่วนตัวพนักงาน)
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right"><b>First Name(Thai) </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ชื่อภาษาไทย</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                                <asp:TextBox ID="txtPrefixTHE" Text='<%# Eval("prefix_idx") %>'
                                                                                    runat="server" CssClass="control-label" Visible="false"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddPrefixTH_Edit" runat="server"
                                                                                    CssClass="form-control" Width="80" ValidationGroup="formEdit_Save">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="txtNameTH_Edit" Text='<%# Eval("emp_firstname_th") %>'
                                                                                    runat="server" CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>LastName</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">นามสกุล</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtLastnameTH_Edit" Text='<%# Eval("emp_lastname_th") %>'
                                                                                    runat="server" CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right">
                                                                                    <b>First Name(English) </b></small></font>

                                                                                <small>
                                                                                    <p class="list-group-item-text">ชื่อภาษาอังกฤษ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                                <asp:TextBox ID="txtPrefixEN" Text='<%# Eval("prefix_idx") %>'
                                                                                    runat="server" CssClass="control-label" Visible="false"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddPrefixEN_Edit" runat="server"
                                                                                    CssClass="form-control" Width="80">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="txtNameEN_Edit" Text='<%# Eval("emp_firstname_en") %>'
                                                                                    runat="server" CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>LastName</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">นามสกุล</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtLastnameEN_Edit" Text='<%# Eval("emp_lastname_en") %>'
                                                                                    runat="server" CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right">
                                                                                    <b>NickName (Thai) </b></small></font>

                                                                                <small>
                                                                                    <p class="list-group-item-text">ชื่อเล่น (ภาษาไทย)</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtNickNameTH_Edit" Text='<%# Eval("emp_nickname_th") %>'
                                                                                    runat="server" CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>NickName (English)</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ชื่อเล่น (ภาษาอังกฤษ)</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtNickNameEN_Edit" Text='<%# Eval("emp_nickname_en") %>'
                                                                                    runat="server" CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Sex </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เพศ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtidSex" Visible="false" Text='<%# Eval("sex_idx") %>'
                                                                                    runat="server" CssClass="form-control left"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddlistSex_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" OnSelectedIndexChanged="DdSelectedIndexChanged"
                                                                                    Text='<%# Eval("sex_idx") %>' AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                    <asp:ListItem Value="0">เลือกเพศ</asp:ListItem>
                                                                                    <asp:ListItem Value="1">ชาย</asp:ListItem>
                                                                                    <asp:ListItem Value="2">หญิง</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Birthday </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">วันเกิด</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <div class='input-group date'>
                                                                                    <asp:TextBox ID="txtBirthday_Edit"
                                                                                        runat="server" CssClass="from-date-datepicker form-control" AutoPostBack="true"
                                                                                        AutoComplete="off" MaxLengh="100%" ValidationGroup="formEdit_Save"
                                                                                        Text='<%# Eval("emp_birthday") %>'></asp:TextBox>
                                                                                    <span class="input-group-addon"><span data-icon-element=""
                                                                                        class="glyphicon glyphicon-calendar"></span></span>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Nationality </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">สัญชาติ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtEdit_Nationality" Text='<%# Eval("nat_idx") %>'
                                                                                    runat="server" CssClass="control-label"
                                                                                    Visible="false"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddNationality_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Race </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เชื้อชาติ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtEdit_Race" Text='<%# Eval("race_idx") %>'
                                                                                    runat="server" CssClass="control-label" Visible="false"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddRace_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Religion </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ศาสนา</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtEdit_Religion" Text='<%# Eval("rel_idx") %>'
                                                                                    runat="server" CssClass="control-label" Visible="false"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddReligion_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <%-----------------กรอกที่อยู่พนักงาน--------------   --%>

                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading">
                                                                    <i class="glyphicon glyphicon-map-marker"></i>&nbsp;
                                                                    <b>Address & Contact (Present)</b>&nbsp; ที่อยู่และการติดต่อ(ปัจจุบัน)
                                                                </div>

                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Address </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ที่อยู่ปัจจุบัน</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <asp:TextBox ID="txtPresentAddress_Edit" Text='<%# Eval("emp_address") %>'
                                                                                    runat="server" CssClass="form-control left" ValidationGroup="formEdit_Save"
                                                                                    placeholder="" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Country </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ประเทศ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtEdit_Country" runat="server" CssClass="control-label"
                                                                                    Visible="false" Text='<%# Eval("country_idx") %>'></asp:TextBox>
                                                                                <asp:DropDownList ID="ddCountry_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Province </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">จังหวัด</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtEdit_Province" Text='<%# Eval("prov_idx") %>'
                                                                                    runat="server" CssClass="control-label" Visible="false"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddProvince_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left"
                                                                                    AutoPostBack="true" ValidationGroup="formEdit_Save"
                                                                                    OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276">
                                                                                    <small class="list-group-item-heading text_right">
                                                                                    <b>Amphoe </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">อำเภอ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtEdit_Amphoe" Text='<%# Eval("amp_idx") %>'
                                                                                    runat="server" CssClass="control-label" Visible="false"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddAmphoe_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save"
                                                                                    OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>District </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ตำบล</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtEdit_District" Text='<%# Eval("dist_idx") %>'
                                                                                    runat="server" CssClass="control-label" Visible="false"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddDistrict_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">

                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right">
                                                                                    <b>ZipCode</b> </small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">รหัสไปรษณีย์</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtZipCode_Edit"
                                                                                    Text='<%# Eval("post_code") %>'
                                                                                    runat="server" CssClass="form-control"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Email </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">อีเมล</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtEmail_Edit"
                                                                                    Text='<%# Eval("emp_email") %>'
                                                                                    runat="server" CssClass="form-control left"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right">
                                                                                    <b>Mobile Phone </b> <i class="fa fa-phone"></i></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เบอร์โทรศัพท์มือถือ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtMobilePhone_Edit" Text='<%# Eval("emp_mobile_no") %>'
                                                                                    runat="server" CssClass="form-control" MaxLength="10"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:TextBox>
                                                                                <asp:RegularExpressionValidator ID="R_txtMobilePhone_Edit" runat="server"
                                                                                    ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="dynamic"
                                                                                    ControlToValidate="txtMobilePhone_Edit" ValidationExpression="\d*"
                                                                                    ValidationGroup="formEdit_Save" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_R_txtMobilePhone_Edit"
                                                                                    runat="Server"
                                                                                    HighlightCssClass="validatorCalloutHighlight"
                                                                                    TargetControlID="R_txtMobilePhone_Edit" Width="160" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Phone Home </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เบอร์บ้าน</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtHomePhone_Edit"
                                                                                    Text='<%# Eval("emp_phone_no") %>'
                                                                                    runat="server" CssClass="form-control left"></asp:TextBox>
                                                                                <asp:RegularExpressionValidator ID="RegulartxtHomePhone_Edit" runat="server"
                                                                                    ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="dynamic"
                                                                                    ControlToValidate="txtHomePhone_Edit" ValidationExpression="\d*"
                                                                                    ValidationGroup="formEdit_Save" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtHomePhone_Edit"
                                                                                    runat="Server"
                                                                                    HighlightCssClass="validatorCalloutHighlight"
                                                                                    TargetControlID="RegulartxtHomePhone_Edit" Width="160" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </EditItemTemplate>
                                                    </asp:FormView>
                                                    <hr />
                                                    <asp:FormView ID="fv_edit_reference_persons" runat="server"
                                                        Width="100%" DefaultMode="Edit">
                                                        <EditItemTemplate>
                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading">
                                                                    <i class="glyphicon glyphicon-user"></i>&nbsp;
                                                                    <b>Reference persons</b>&nbsp; (บุคคลอ้างอิง)
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right">
                                                                                    <b>Reference Name</b> </small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ชื่อบุคคลอ้างอิง</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_fullname_reference_edit"
                                                                                    Text='<%# Eval("fullname_reference") %>'
                                                                                    runat="server" CssClass="form-control"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>

                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right">
                                                                                    <b>Relationship</b> </small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ความสัมพันธ์</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_relationship_edit"
                                                                                    Text='<%# Eval("relationship") %>'
                                                                                    runat="server" CssClass="form-control"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right">
                                                                                    <b>Address</b> </small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ที่อยู่</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_address_reference_edit"
                                                                                    Text='<%# Eval("address_reference") %>'
                                                                                    runat="server" CssClass="form-control"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right">
                                                                                    <b>Mobile Phone</b> </small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เบอร์โทรศัพท์</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_mobile_number_edit" MaxLength="10"
                                                                                    Text='<%# Eval("mobile_number") %>' runat="server"
                                                                                    CssClass="form-control"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </EditItemTemplate>
                                                    </asp:FormView>
                                                    <asp:Panel ID="box_insert_reference_persons" Visible="false" runat="server">
                                                        <div class="panel panel-primary">
                                                            <div class="panel-heading">
                                                                <i class="glyphicon glyphicon-user"></i>&nbsp;
                                                                <b>Reference persons</b>&nbsp; (บุคคลอ้างอิง)
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="form-horizontal" role="form">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-2">
                                                                            <font color="#1A5276"><small class="text_right">
                                                                                <b>Reference Name</b> </small></font>
                                                                            <small>
                                                                                <p class="list-group-item-text">ชื่อบุคคลอ้างอิง</p>
                                                                            </small>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_fullname_reference_"
                                                                                runat="server" CssClass="form-control"
                                                                                ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <font color="#1A5276"><small class="text_right">
                                                                                <b>Relationship</b> </small></font>
                                                                            <small>
                                                                                <p class="list-group-item-text">ความสัมพันธ์</p>
                                                                            </small>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_relationship_" runat="server"
                                                                                CssClass="form-control" ValidationGroup="formEdit_Save">
                                                                            </asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-2">
                                                                            <font color="#1A5276"><small class="text_right">
                                                                                <b>Address</b> </small></font>
                                                                            <small>
                                                                                <p class="list-group-item-text">ที่อยู่</p>
                                                                            </small>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_address_reference_"
                                                                                runat="server" CssClass="form-control"
                                                                                ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <font color="#1A5276"><small class="text_right">
                                                                                <b>Mobile Phone</b> </small></font>
                                                                            <small>
                                                                                <p class="list-group-item-text">เบอร์โทรศัพท์</p>
                                                                            </small>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_mobile_number_" runat="server" MaxLength="10"
                                                                                CssClass="form-control" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="R_txt_mobile_number_" runat="server"
                                                                                ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="dynamic"
                                                                                ControlToValidate="txt_mobile_number_" ValidationExpression="\d*"
                                                                                ValidationGroup="formEdit_Save" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="V_txt_mobile_number_" runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_txt_mobile_number_" Width="160" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                    <hr />
                                                    <%----------------------แก้ไขข้อมูลพนักงานไทย---------------%>
                                                    <asp:FormView ID="fvEdit_dataEmployeeTHai" runat="server"
                                                        Width="100%" DefaultMode="Edit">
                                                        <EditItemTemplate>
                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading">
                                                                    <i class="glyphicon glyphicon-user"></i>&nbsp;
                                                                    <b>Employee Thai Nationality</b>&nbsp; (ข้อมูลพนักงานไทย)
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <asp:Panel ID="edit_Military" runat="server">
                                                                                <div class="col-sm-2">
                                                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b> MilitaryStatus</b></small></font>
                                                                                    <small>
                                                                                        <p class="list-group-item-text">สถานะทางทหาร</p>
                                                                                    </small>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <asp:TextBox ID="txt_MilIDX_edit" Visible="false"
                                                                                        Text='<%# Eval("MilIDX") %>' runat="server" CssClass="form-control"
                                                                                        ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                    <asp:DropDownList ID="ddMilitary_Edit" runat="server"
                                                                                        CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                        ValidationGroup="formEdit_Save">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </asp:Panel>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right">
                                                                                    <b>ID Card</b> </small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เลขบัตรประชาชน</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtIDCard_Edit" Text='<%# Eval("IdentityCard") %>'
                                                                                    runat="server" CssClass="form-control" MaxLength="13"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">

                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right">
                                                                                    <b>Issued At</b> </small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ออกให้ ณ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtIIssuedAtIDCard_Edit"
                                                                                    Text='<%# Eval("IssuedAt") %>' runat="server"
                                                                                    CssClass="form-control" ValidationGroup="formEdit_Save">
                                                                                </asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Expiration Date </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">วันหมดอายุ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <div class='input-group date'>
                                                                                    <asp:TextBox ID="txtDateExpIDCard_Edit" runat="server"
                                                                                        Text='<%# Eval("IDateExpired") %>'
                                                                                        CssClass="from-date-datepicker form-control"
                                                                                        AutoComplete="off" MaxLengh="100%" ValidationGroup="formEdit_Save">
                                                                                    </asp:TextBox>
                                                                                    <span class="input-group-addon"><span data-icon-element=""
                                                                                        class="fa fa-calendar"></span></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <hr />
                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading">
                                                                    <i class="glyphicon glyphicon-heart-empty"></i>&nbsp;
                                                                    <b>Address & Contact (Permanent</b>&nbsp; ที่อยู่และการติดต่อ(ทะเบียนบ้าน)
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Permanentaddress </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ที่อยู่ตามทะเบียนบ้าน</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <asp:TextBox ID="txtPermanentaddresst_Edit" Text='<%# Eval("EmpAddress") %>'
                                                                                    runat="server" CssClass="form-control left"
                                                                                    ValidationGroup="formEdit_Save" placeholder=""
                                                                                    TextMode="MultiLine" Rows="4">
                                                                                </asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Country </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ประเทศ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_Country_Permanent" Visible="false"
                                                                                    Text='<%# Eval("CountryIDX") %>' runat="server"
                                                                                    CssClass="form-control" ValidationGroup="formEdit_Save">
                                                                                </asp:TextBox>
                                                                                <asp:DropDownList ID="ddCountry_Permanent_Edit"
                                                                                    runat="server" CssClass="form-control fa-align-left"
                                                                                    AutoPostBack="true" ValidationGroup="formEdit_Save">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Province </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">จังหวัด</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_Province_Permanent" Visible="false"
                                                                                    Text='<%# Eval("ProvIDX") %>' runat="server" CssClass="form-control"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddProvince_Permanent_Edit"
                                                                                    OnSelectedIndexChanged="DdSelectedIndexChanged"
                                                                                    runat="server" CssClass="form-control fa-align-left"
                                                                                    AutoPostBack="true" ValidationGroup="formEdit_Save">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Amphoe </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">อำเภอ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_Amphoe_Permanent_Edit" Visible="false"
                                                                                    Text='<%# Eval("AmpIDX") %>' runat="server" CssClass="form-control"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddAmphoe_Permanent_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save"
                                                                                    OnSelectedIndexChanged="DdSelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>District </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ตำบล</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_District_Permanent_Edit" Visible="false"
                                                                                    Text='<%# Eval("DistIDX") %>' runat="server"
                                                                                    CssClass="form-control" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddDistrict_Permanent_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">

                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right">
                                                                                    <b>ZipCode </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">รหัสไปรษณีย์</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtZipcode_Permanent_Edit" MaxLength="5"
                                                                                    Text='<%# Eval("PostCode") %>' runat="server"
                                                                                    CssClass="form-control" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Mobile Phone </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เบอร์โทรศัพท์</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtHomePhone_Permanent_Edit" MaxLength="10"
                                                                                    Text='<%# Eval("PhoneNo") %>' runat="server"
                                                                                    CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                <asp:RegularExpressionValidator ID="R_txtHomePhone_Permanent_Edit" runat="server"
                                                                                    ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="dynamic"
                                                                                    ControlToValidate="txtHomePhone_Permanent_Edit" ValidationExpression="\d*"
                                                                                    ValidationGroup="formEdit_Save" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_txtHomePhone_Permanent_Edit" runat="Server"
                                                                                    HighlightCssClass="validatorCalloutHighlight"
                                                                                    TargetControlID="R_txtHomePhone_Permanent_Edit" Width="160" />
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <%----------------------ปิดกล่องเพิ่มข้อมูลที่อยู่ตามทะเบียนบ้าน---------------%>
                                                            <hr />
                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading">
                                                                    <i class="glyphicon glyphicon-heart-empty"></i>&nbsp;
                                                                    <b>Social Security </b>&nbsp; ประกันสังคม
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Currently used hospital </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">สถานพยาบาลที่ใช้</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="lb_edit_hospital" Visible="false"
                                                                                    Text='<%# Eval("SocHosIDX") %>' runat="server"
                                                                                    CssClass="form-control" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddHospital_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">

                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="text_right">
                                                                                    <b>Social Security No</b> </small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เลขที่ประกันสังคม</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtSocial_Edit" Text='<%# Eval("SocNo") %>'
                                                                                    runat="server" CssClass="form-control"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Expiration Date </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">วันหมดอายุ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <div class='input-group date'>
                                                                                    <asp:TextBox ID="txtSocialDateExp_Edit" runat="server"
                                                                                        Text='<%# Eval("SocDateExpired") %>'
                                                                                        CssClass="from-date-datepicker form-control"
                                                                                        AutoComplete="off" MaxLengh="100%"
                                                                                        ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                    <span class="input-group-addon"><span data-icon-element=""
                                                                                        class="fa fa-calendar"></span></span>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </EditItemTemplate>
                                                    </asp:FormView>
                                                    <%----------------------แก้ไขข้อมูลชาวต่างชาติ---------------%>
                                                    <asp:FormView ID="fv_edit_employee_Foreing"
                                                        runat="server" Width="100%" DefaultMode="Edit">
                                                        <EditItemTemplate>

                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading">
                                                                    <i class="glyphicon glyphicon-user"></i>&nbsp;
                                                                    <b>Employee Foreing Nationality</b>&nbsp; (ข้อมูลพนักงานชาวต่างชาติ)
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Permanentaddress </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ที่อยู่ตามทะเบียนบ้าน</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <asp:TextBox ID="txtPermanentaddressEN_edit"
                                                                                    Text='<%# Eval("EmpAddress") %>' runat="server"
                                                                                    CssClass="form-control left" ValidationGroup="formEdit_Save"
                                                                                    placeholder="" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Country </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ประเทศ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtEdit_FR_Country" runat="server"
                                                                                    CssClass="control-label" Visible="false"
                                                                                    Text='<%# Eval("CountryIDX") %>'></asp:TextBox>
                                                                                <asp:DropDownList ID="ddlCountryEN_edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Phone Home  </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เบอร์โทรศัพท์</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtPhoneEN_edit" runat="server" MaxLength="10"
                                                                                    Text='<%# Eval("PhoneNo") %>' CssClass="form-control fa-align-left"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                <asp:RegularExpressionValidator ID="RtxtPhoneEN_edit" runat="server"
                                                                                    ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="dynamic"
                                                                                    ControlToValidate="txtPhoneEN_edit"
                                                                                    ValidationExpression="\d*"
                                                                                    ValidationGroup="formEdit_Save" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="VRtxtPhoneEN_edit"
                                                                                    runat="Server"
                                                                                    HighlightCssClass="validatorCalloutHighlight"
                                                                                    TargetControlID="RtxtPhoneEN_edit" Width="160" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Passport ID  </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เลขที่หนังสือเดินทาง</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_Passport_Issued_No_edit" runat="server"
                                                                                    Text='<%# Eval("VisaIDX") %>' CssClass="form-control fa-align-left"
                                                                                    AutoPostBack="true" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                <b>Passport Issued At </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">
                                                                                        สถานที่ออกหนังสือเดินทาง
                                                                                    </p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_PassportIssued_edit"
                                                                                    runat="server" Text='<%# Eval("Passport_Issued") %>'
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Issued Date </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">วันที่ออกหนังสือเดินทาง</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_Issued_Date_edit" runat="server"
                                                                                    Text='<%# Eval("TrDateIssued") %>'
                                                                                    CssClass="form-control from-date-datepicker" AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                <b>ExpDate </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">
                                                                                        วันหมดอายุหนังสือเดินทาง
                                                                                    </p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_ExpDate_edit" Text='<%# Eval("TrExpired") %>'
                                                                                    runat="server" CssClass="form-control from-date-datepicker"
                                                                                    AutoPostBack="true" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Work Permit No.</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เลขที่ใบอนุญาตทำงาน</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_perIssued_No_edit" runat="server"
                                                                                    Text='<%# Eval("WPermitNo") %>' CssClass="form-control fa-align-left"
                                                                                    AutoPostBack="true" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                <b>Work Permit Issued At </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">
                                                                                        สถานที่ออกใบอนุญาตทำงาน
                                                                                    </p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_perIssued_at_edit" runat="server"
                                                                                    Text='<%# Eval("WPermit_Issued") %>' CssClass="form-control fa-align-left"
                                                                                    AutoPostBack="true" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Work Permit Issued Date</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">วันที่ออกใบอนุญาตทำงาน</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_perIssued_date_edit" runat="server"
                                                                                    Text='<%# Eval("WDateIssued") %>' CssClass="form-control from-date-datepicker"
                                                                                    AutoPostBack="true" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                <b>Work Permit Expire Date </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">
                                                                                        วันหมดอายุใบอนุญาตทำงาน
                                                                                    </p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_perExpire_date_edit" runat="server"
                                                                                    Text='<%# Eval("WDateExpired") %>' CssClass="form-control from-date-datepicker"
                                                                                    AutoPostBack="true" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </EditItemTemplate>
                                                    </asp:FormView>
                                                    <h5>&nbsp;</h5>
                                                    <%----------------------ปิดแก้ไขข้อมูลพนักงานไทย---------------%>
                                                    <asp:FormView ID="fv_edit_Health" runat="server" Width="100%" DefaultMode="Edit">
                                                        <EditItemTemplate>
                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading">
                                                                    <i class="glyphicon glyphicon-heart-empty"></i>
                                                                    &nbsp;<b> Health</b>&nbsp; (สุขภาพ)
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276">
                                                                                    <small class="list-group-item-heading text_right">
                                                                        <b>Height </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ส่วนสูง</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="txtHeight_Edit" Text='<%# Eval("Height") %>'
                                                                                    runat="server" CssClass="form-control left"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b> Cm. </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ซม.</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b> Weight </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">น้ำหนัก</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:TextBox ID="txtWeight_Edit" Text='<%# Eval("Weight") %>'
                                                                                    runat="server" CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b> Kg. </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">กก.</p>
                                                                                </small>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b> BloodGroup</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">กรุ๊ปเลือด</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-2">

                                                                                <asp:TextBox ID="txt_Bloodgroup_edit" Visible="false"
                                                                                    Text='<%# Eval("BTypeIDX") %>' runat="server"
                                                                                    CssClass="form-control" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddBloodgroup_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b> Scar </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">รอยตำหนิ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <asp:TextBox ID="txtScar_Edit" Text='<%# Eval("Scar") %>'
                                                                                    runat="server" CssClass="form-control left"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </EditItemTemplate>
                                                    </asp:FormView>
                                                    <hr />
                                                    <%----------------------เพิ่มข้อมูลการศึกษา----------------%>
                                                    <asp:FormView ID="fv_edit_Education" runat="server" Width="100%" DefaultMode="Edit">
                                                        <EditItemTemplate>
                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading">
                                                                    <i class="glyphicon glyphicon-education"></i>
                                                                    &nbsp;<b>Education</b>&nbsp; (ประวัติการศึกษา)
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b> Graduates</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">จบการศึกษาระดับ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_Education_edit" Visible="false"
                                                                                    Text='<%# Eval("Edu_name") %>' runat="server"
                                                                                    CssClass="form-control" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddlEducation_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b> Year of graduation</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ปีที่สำเร็จการศึกษา</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_edit_graduation" Visible="true"
                                                                                    Text='<%# Eval("academic_year") %>' runat="server"
                                                                                    CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>

                                                                                <asp:TextBox ID="TextBox3" Visible="false" runat="server"
                                                                                    CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b> GPA</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เกรดเฉลี่ย</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_edit_GPA" Visible="true"
                                                                                    Text='<%# Eval("GPA") %>' runat="server"
                                                                                    CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b> Major</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เรียนสาขา</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_major_edit" Visible="true" Text='<%# Eval("Major") %>'
                                                                                    runat="server" CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>Faculty</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">คณะ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_Faculty_edit" Visible="false"
                                                                                    Text='<%# Eval("Faculty") %>' runat="server"
                                                                                    CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                                <asp:DropDownList ID="ddlFaculty_Edit" runat="server"
                                                                                    CssClass="form-control fa-align-left" AutoPostBack="true"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>University </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ชื่อสถานบันศึกษา</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <asp:TextBox ID="txt_University_Edit" Text='<%# Eval("University") %>'
                                                                                    runat="server" CssClass="form-control left" ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </EditItemTemplate>
                                                    </asp:FormView>

                                                    <asp:Panel ID="box_insert_education" Visible="false" runat="server">
                                                        <div class="row">

                                                            <div class="panel panel-primary">
                                                                <div class="panel-heading">
                                                                    <i class="glyphicon glyphicon-education"></i>
                                                                    &nbsp;<b>Education</b>&nbsp; (ประวัติการศึกษา)
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b> Graduates</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">จบการศึกษาระดับ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="ddlEducation_insert"
                                                                                    runat="server" CssClass="form-control fa-align-left"
                                                                                    AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276">
                                                                                    <small class="list-group-item-heading text_right">
                                                                                    <b> Year of graduation</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ปีที่สำเร็จการศึกษา</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_insert_graduation" Visible="true"
                                                                                    runat="server" CssClass="form-control left"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                    <b>GPA</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เกรดเฉลี่ย</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_insert_GPA" Visible="true"
                                                                                    runat="server" CssClass="form-control left"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276">
                                                                                    <small class="list-group-item-heading text_right">
                                                                                    <b> Major</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">เรียนสาขา</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_major_insert" Visible="true"
                                                                                    runat="server" CssClass="form-control left"
                                                                                    ValidationGroup="formEdit_Save"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276">
                                                                                    <small class="list-group-item-heading text_right">
                                                                                    <b>Faculty</b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">คณะ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:DropDownList ID="ddlFaculty_insert"
                                                                                    runat="server" CssClass="form-control fa-align-left"
                                                                                    AutoPostBack="true" ValidationGroup="formEdit_Save">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276">
                                                                                    <small class="list-group-item-heading text_right">
                                                                                    <b>University </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ชื่อสถานบันศึกษา</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-8">
                                                                                <asp:TextBox ID="txt_University_insert" runat="server"
                                                                                    CssClass="form-control left"
                                                                                    ValidationGroup="formEdit_Save">
                                                                                </asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                    <%----------------------ปิดการเพิ่มข้อมูลการศึกษา----------------%>
                                                    <hr />
                                                    <%----------------------เพิ่มข้อมูลประสบการณ์ทำงาน---------------%>
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading">
                                                            <i class="glyphicon glyphicon-time"></i>&nbsp;
                                                            <b>Experience</b>&nbsp; (ประสบการณ์ทำงาน)
                                                        </div>

                                                        <div class="panel-body">
                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <asp:LinkButton ID="add_Experience" CssClass="btn btn-success"
                                                                        data-toggle="tooltip" title="เพิ่มประสบการณ์ทำงาน" runat="server"
                                                                        CommandName="add_Experience" OnCommand="btnCommand">
                                                                        <i class="glyphicon glyphicon-plus-sign"></i> เพิ่มประสบการณ์ทำงาน</asp:LinkButton>
                                                                    <div style="padding-top: 20px;"></div>
                                                                    <div id="boxaddExperience" runat="server" visible="false">
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b> Company </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ชื่อบริษัท</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtOrganiztion_Old_edit" runat="server" placeholder="กรอกชื่อบริษัท"
                                                                                    CssClass="form-control left" ValidationGroup="formInsert_edit"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b> Position </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ตำแหน่ง</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtPositionOld_edit" runat="server" placeholder="กรอกชื่อตำแหน่ง"
                                                                                    CssClass="form-control left" ValidationGroup="formInsert_edit"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b> Job Brief </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">ลักษณะงานโดยย่อ</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txt_Job_Brief_edit" runat="server" placeholder="กรอกลักษณะงานโดยย่อ"
                                                                                    CssClass="form-control left" ValidationGroup="formInsert"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b>Work In </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">วันที่เริ่มเข้าทำงาน</p>
                                                                                </small>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <div class='input-group date'>

                                                                                    <asp:TextBox ID="txtWorkInOld_edit" runat="server" placeholder="เลือกวันที่เริ่มงาน"
                                                                                        CssClass="form-control from-date-datepicker"></asp:TextBox>
                                                                                    <span class="input-group-addon"><span data-icon-element=""
                                                                                        class="fa fa-calendar"></span></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b>resign </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">วันที่ลาออก</p>
                                                                                </small>
                                                                            </div>

                                                                            <div class="col-sm-3">
                                                                                <div class='input-group date'>

                                                                                    <asp:TextBox ID="txtresign_Old_edit" runat="server" placeholder="เลือกวันที่ลาออก"
                                                                                        CssClass="form-control from-date-datepicker"></asp:TextBox>
                                                                                    <span class="input-group-addon"><span data-icon-element=""
                                                                                        class="fa fa-calendar"></span></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b>Motive </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">สาเหตุการลาออก</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtMotive_edit" runat="server" CssClass="form-control left" placeholder="ระบุสาเหตุการลาออก"
                                                                                    ValidationGroup="formInsert_edit"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <font color="#1A5276"><small class="list-group-item-heading text_right">
                                                                                <b>Salary </b></small></font>
                                                                                <small>
                                                                                    <p class="list-group-item-text">อัตราค่าจ้าง</p>
                                                                                </small>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="txtSalary_Old_edit" runat="server" placeholder="ระบุอัตราค่าจ้าง"
                                                                                    CssClass="form-control left" ValidationGroup="formInsert_edit"></asp:TextBox>

                                                                                <asp:RegularExpressionValidator ID="RegulartxtSalary_Old_edit"
                                                                                    runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                                                                    Display="None" ControlToValidate="txtSalary_Old_edit" ValidationExpression="^[0-9',.\s]{1,20}$"
                                                                                    ValidationGroup="formInsert_edit" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtSalary_Old_edit" runat="Server"
                                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegulartxtSalary_Old_edit" Width="160" />
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <asp:LinkButton ID="addExperience_edit" CssClass="btn btn-success" ValidationGroup="formInsert_edit"
                                                                                    data-toggle="tooltip" title="เพิ่มข้อมูล" runat="server" CommandName="btnaddExperience_edit"
                                                                                    OnCommand="btnCommand"><i class="glyphicon glyphicon-plus-sign"></i> เพิ่ม</asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                        <h5>&nbsp;</h5>
                                                                    </div>
                                                                    <asp:GridView ID="gv_edit_Experience" runat="server"
                                                                        AutoGenerateColumns="false" DataKeyNames="exper_idx_"
                                                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                        HeaderStyle-CssClass="info" HeaderStyle-Height="40px"
                                                                        AllowPaging="true" PageSize="10"
                                                                        OnRowEditing="row_editing" OnRowUpdating="row_updating"
                                                                        OnRowCancelingEdit="row_canceling_edit"
                                                                        OnPageIndexChanging="Master_PageIndexChanging"
                                                                        OnRowDataBound="row_DataBound">
                                                                        <PagerStyle CssClass="pageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                                            FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">ไม่มีประสบการณ์ทำงาน</div>
                                                                        </EmptyDataTemplate>
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lb_id_table_exper" runat="server" Visible="false"
                                                                                        Text='<%# Eval("exper_idx_") %>' />
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <div class="panel-body">
                                                                                        <div class="form-horizontal" role="form">

                                                                                            <div class="form-group">
                                                                                                <div class="col-sm-2">
                                                                                                    <asp:TextBox ID="id_table_exper" runat="server"
                                                                                                        CssClass="form-control" Visible="false" Text='<%# Eval("exper_idx_")%>' />
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <asp:Label ID="Label1" CssClass="col-sm-3 control-label"
                                                                                                    runat="server" Text="ชื่อบริษัท" />
                                                                                                <div class="col-sm-5">
                                                                                                    <asp:TextBox ID="txt_name_company_edit" Visible="true"
                                                                                                        runat="server" CssClass="form-control" Text='<%# Eval("name_company")%>' />

                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <asp:Label ID="Label2" CssClass="col-sm-3 control-label"
                                                                                                    runat="server" Text="ตำแหน่ง" />
                                                                                                <div class="col-sm-5">
                                                                                                    <asp:TextBox ID="txt_position_old_edit" Visible="true"
                                                                                                        runat="server" CssClass="form-control" Text='<%# Eval("position_old_company")%>' />
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <asp:Label ID="Label7" CssClass="col-sm-3 control-label"
                                                                                                    runat="server" Text="ลักษณะงานโดยย่อ" />
                                                                                                <div class="col-sm-5">
                                                                                                    <asp:TextBox ID="txt_job_brief_details_edit"
                                                                                                        Visible="true" runat="server" CssClass="form-control"
                                                                                                        Text='<%# Eval("job__details")%>' />
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <asp:Label ID="Label6" CssClass="col-sm-3 control-label"
                                                                                                    runat="server" Text="เงินเดือน" />
                                                                                                <div class="col-sm-5">
                                                                                                    <asp:TextBox ID="txt_edit_salary_old_company"
                                                                                                        Visible="true" runat="server" Text='<%# Eval("salary_old_company")%>'
                                                                                                        CssClass="form-control" />
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <asp:Label ID="Label3" CssClass="col-sm-3 control-label"
                                                                                                    runat="server" Text="วันเริ่มงาน" />
                                                                                                <div class="col-sm-5">
                                                                                                    <asp:TextBox ID="txt_workIn_old_edit" Visible="true"
                                                                                                        runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("workIn_old_company")%>' />
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <asp:Label ID="Label4" CssClass="col-sm-3 control-label"
                                                                                                    runat="server" Text="วันที่ออก" />
                                                                                                <div class="col-sm-5">
                                                                                                    <asp:TextBox ID="txt_resign_old_edit" Visible="true"
                                                                                                        runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("resign_old_company")%>' />
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <asp:Label ID="Label5" CssClass="col-sm-3 control-label"
                                                                                                    runat="server" Text="เหตุผล" />
                                                                                                <div class="col-sm-5">
                                                                                                    <asp:TextBox ID="txt_motive_edit" Visible="true"
                                                                                                        runat="server" Text='<%# Eval("motive_resign")%>'
                                                                                                        CssClass="form-control" />

                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <div class="col-sm-4 col-sm-offset-8">
                                                                                                    <asp:LinkButton ID="lbCmdUpdate_" CssClass="btn btn-success"
                                                                                                        runat="server" ValidationGroup="Save" CommandName="Update"
                                                                                                        OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                                                                            <i class="fa fa-check"></i></asp:LinkButton>
                                                                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default"
                                                                                                        runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="ชื่อบริษัท" ItemStyle-HorizontalAlign="Left"
                                                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                <ItemTemplate>
                                                                                    <small>
                                                                                        <asp:Label ID="lb_name_company" runat="server" CssClass="col-sm-12"
                                                                                            Text='<%# Eval("name_company") %>'></asp:Label>
                                                                                    </small>
                                                                                </ItemTemplate>

                                                                                <EditItemTemplate />
                                                                                <FooterTemplate />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left"
                                                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                <ItemTemplate>
                                                                                    <small>
                                                                                        <asp:Label ID="lb_position_old_company" runat="server"
                                                                                            CssClass="col-sm-12" Text='<%# Eval("position_old_company") %>'></asp:Label>
                                                                                    </small>
                                                                                </ItemTemplate>

                                                                                <EditItemTemplate />
                                                                                <FooterTemplate />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="ลักษณะงานโดยย่อ" ItemStyle-HorizontalAlign="Left"
                                                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                <ItemTemplate>
                                                                                    <small>
                                                                                        <asp:Label ID="lb_job_brief_details" runat="server" CssClass="col-sm-12"
                                                                                            Text='<%# Eval("job__details") %>'></asp:Label>
                                                                                    </small>
                                                                                </ItemTemplate>

                                                                                <EditItemTemplate />
                                                                                <FooterTemplate />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="เงินเดือน" ItemStyle-HorizontalAlign="Left"
                                                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                <ItemTemplate>
                                                                                    <small>
                                                                                        <asp:Label ID="lb_salary_old" runat="server" CssClass="col-sm-12"
                                                                                            Text='<%# Eval("salary_old_company") %>'></asp:Label>
                                                                                    </small>
                                                                                </ItemTemplate>

                                                                                <EditItemTemplate />
                                                                                <FooterTemplate />
                                                                            </asp:TemplateField>
                                                                            <%--   <asp:TemplateField HeaderText="เงินเดือน" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lb_salary_old" runat="server" CssClass="col-sm-12" Text='<%# Eval("salary_old") %>'></asp:Label>
                                                        </small>
                                                    </ItemTemplate>

                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>--%>
                                                                            <asp:TemplateField HeaderText="วันเริ่มงาน" ItemStyle-HorizontalAlign="Left"
                                                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                <ItemTemplate>
                                                                                    <small>
                                                                                        <asp:Label ID="lb_workIn_old" runat="server" CssClass="col-sm-12"
                                                                                            Text='<%# Eval("workIn_old_company") %>'></asp:Label>
                                                                                    </small>
                                                                                </ItemTemplate>

                                                                                <EditItemTemplate />
                                                                                <FooterTemplate />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="วันที่ออก" ItemStyle-HorizontalAlign="Left"
                                                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                <ItemTemplate>
                                                                                    <small>
                                                                                        <asp:Label ID="lb_resign_old" runat="server" CssClass="col-sm-12"
                                                                                            Text='<%# Eval("resign_old_company") %>'></asp:Label>
                                                                                    </small>
                                                                                </ItemTemplate>

                                                                                <EditItemTemplate />
                                                                                <FooterTemplate />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="เหตุผล" ItemStyle-HorizontalAlign="Left"
                                                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                <ItemTemplate>
                                                                                    <small>
                                                                                        <asp:Label ID="lb_motive" runat="server" CssClass="col-sm-12"
                                                                                            Text='<%# Eval("motive_resign") %>'></asp:Label>
                                                                                    </small>
                                                                                </ItemTemplate>

                                                                                <EditItemTemplate />
                                                                                <FooterTemplate />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="การจัดการ" ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server"
                                                                                        CommandName="Edit" data-toggle="tooltip" title="แก้ไขข้อมูล">
                                                                            <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                                                                    <asp:LinkButton ID="delete" CssClass="btn btn-danger btn-sm"
                                                                                        runat="server" data-toggle="tooltip" title="ลบ"
                                                                                        CommandName="delete_exper" OnCommand="btnCommand"
                                                                                        CommandArgument='<%# Eval("exper_idx_") %>'
                                                                                        OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่ ?')">
                                                                            <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate />
                                                                                <FooterTemplate />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-sm-offset-10">

                                                        <asp:LinkButton ID="Btn" CssClass="btn btn-default pull-right"
                                                            data-toggle="tooltip" title="ยกเลิก" runat="server" CommandName="BtnCancel"
                                                            OnCommand="btnCommand">ยกเลิก</asp:LinkButton>
                                                        &nbsp;&nbsp;<asp:LinkButton ID="BtnSaveEdit_" CssClass="btn btn-success pull-right"
                                                            data-toggle="tooltip" title="บันทึกข้อมูล" runat="server" ValidationGroup="formEdit_Save"
                                                            CommandName="BtnSaveEdit" OnCommand="btnCommand"
                                                            OnClientClick="if(!confirm('คุณต้องการบันทึกการแก้ไขข้อมูลนี้หรือไม่?')) return false;">
                                                         บันทึก</asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </asp:View>

                <asp:View ID="ViewSetShiftTime" runat="server">
                    <div class="form-horizontal" role="form">
                        <div class="col-lg-12" runat="server" id="Div3">
                            <div class="panel-body">
                                <div style="margin-top: -15px;">
                                    <div class="form-group">
                                        <h4><span class="label label-default">ค้นหาข้อมูล</span></h4>
                                        <div class="panel panel-danger">
                                            <div class="form-group">
                                                <br />
                                                <div class="col-sm-1"></div>
                                                <div class="col-sm-11">
                                                    <small class="list-group-item-heading text_right"><font color="#1A5276">
                                            <b>* สามารถค้นหาข้อมูลได้ด้วยการเลือก ประเภทกะ, รหัสพนักงาน , หรือ ชื่อพนักงาน</b>
                                            </font></small>
                                                </div>
                                                <br />
                                                <br />
                                                <div class="col-sm-1"></div>
                                                <div class="col-sm-1">
                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                            <b>Type Shift  </b></small></font>
                                                    <small>
                                                        <p class="list-group-item-text">ประเภทกะ</p>
                                                    </small>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddl_type_shift"
                                                        runat="server" CssClass="form-control fa-align-left"
                                                        AutoPostBack="true">
                                                        <asp:ListItem Value="0">เลือกประเภทกะ</asp:ListItem>
                                                        <asp:ListItem Value="1">กะคงที่</asp:ListItem>
                                                        <asp:ListItem Value="2">กะหมุนเวียน</asp:ListItem>
                                                        <%--        <asp:ListItem Value="3">พนักงานที่ยังไม่กำหนดกะ</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-2">
                                                    <%--  <asp:CheckBox ID="Checkshow" runat="server" />--%>
                                                    <font color="#1A5276"><small class="list-group-item-heading text_right">
                                            <b>completetext </b></small></font>
                                                    <small>
                                                        <p class="list-group-item-text">
                                                            กรอกชื่อหรือรหัสพนักงาน
                                                        </p>
                                                    </small>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtsearching_shiftTime" runat="server" placeholder="กรอกข้อมูล..."
                                                        CssClass="form-control left"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:LinkButton ID="btnsearching" runat="server" CssClass="btn btn-info btn-md"
                                                        data-toggle="tooltip" title="ค้นหา" CommandName="search_shiftTime"
                                                        OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i> ค้นหา</asp:LinkButton>
                                                </div>
                                                <br />
                                                <br />
                                            </div>
                                        </div>


                                        <h4><span class="label label-default">รายชื่อพนักงานทั้งหมด</span></h4>
                                        <asp:GridView ID="gvListNameShiftTime"
                                            runat="server"
                                            AutoGenerateColumns="false"
                                            DataKeyNames="emp_shift_idx"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                            HeaderStyle-CssClass="info"
                                            HeaderStyle-Height="40px"
                                            AllowPaging="true"
                                            PageSize="5"
                                            OnPageIndexChanging="Master_PageIndexChanging"
                                            OnRowDataBound="Master_RowDataBound">
                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10"
                                                FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">ไม่พบข้อมูลพนักงาน</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <div style="text-align: center; padding-top: 60px;">
                                                            <asp:Label ID="emp_idxlist" runat="server" Visible="false" Text='<%# Eval("emp_shift_idx") %>' />
                                                            <%# (Container.DataItemIndex +1) %>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <div style="text-align: center; padding-top: 60px;">
                                                                <asp:Label ID="empCodelist" runat="server"
                                                                    CssClass="col-sm-12" Text='<%# Eval("employee_code") %>'></asp:Label>
                                                            </div>
                                                        </small>
                                                    </ItemTemplate>
                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="รายละเอียดพนักงาน" ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                                                    ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Description" runat="server"> 
                                                     <p><b>ชื่อ - สกุล: &nbsp;</b><span><%# Eval("fullname_employee") %></span>
                                        </p>
                                                        </asp:Label>
                                                        <p>
                                                            <asp:Label ID="lborgNameShiftTime" runat="server" CssClass="col-sm-12">
                                                             <p><b>องค์กร:</b> &nbsp;<%# Eval("org_shiftTime") %></p>
                                                            </asp:Label>
                                                        </p>
                                                        <p>
                                                            <asp:Label ID="LabelshiftOrgtIDX" runat="server" Visible="false"
                                                                Text='<%# Eval("org_idx_shiftTime") %>'></asp:Label>
                                                            <asp:Label ID="labeltextOrgshiftdefault" runat="server"
                                                                CssClass="col-sm-12" Visible="false">
                                                                  <p><b>องค์กร: &nbsp;</b><font color="sandybrown">ยังไม่มีข้อมูลองค์กร</font></p>
                                                            </asp:Label>
                                                        </p>
                                                        <p>
                                                            <asp:Label ID="lbdeptNameShiftTime" runat="server" CssClass="col-sm-12">
                                                             <p><b>ฝ่าย:</b> &nbsp;<%# Eval("dept_shiftTime") %></p>
                                                            </asp:Label>
                                                        </p>
                                                        <p>
                                                            <asp:Label ID="LabelshiftDeptIDX" runat="server" Visible="false"
                                                                Text='<%# Eval("rdept_idx_shiftTime") %>'></asp:Label>
                                                            <asp:Label ID="labeltextDeptshiftdefault" runat="server"
                                                                CssClass="col-sm-12" Visible="false">
                                                                  <p><b>ฝ่าย: &nbsp;</b><font color="sandybrown">ยังไม่มีข้อมูลฝ่าย</font></p>
                                                            </asp:Label>
                                                        </p>
                                                        <p>
                                                            <asp:Label ID="lbsecNameShiftTime" runat="server" CssClass="col-sm-12">
                                                             <p><b>แผนก:</b> &nbsp;<%# Eval("sec_shiftTime") %></p>
                                                            </asp:Label>
                                                        </p>
                                                        <p>
                                                            <asp:Label ID="LabelshiftSecIDX" runat="server" Visible="false"
                                                                Text='<%# Eval("rsec_idx_shiftTime") %>'></asp:Label>
                                                            <asp:Label ID="labeltextSecshiftdefault" runat="server"
                                                                CssClass="col-sm-12" Visible="false">
                                                                  <p><b>แผนก: &nbsp;</b><font color="sandybrown">ยังไม่มีข้อมูลแผนก</font></p>
                                                            </asp:Label>
                                                        </p>
                                                        <p>
                                                            <asp:Label ID="lbPossitionNameShiftTime" runat="server" CssClass="col-sm-12">
                                                             <p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("pos_shiftTime") %></p>
                                                            </asp:Label>
                                                        </p>
                                                        <p>
                                                            <asp:Label ID="LabelshiftPosIDX" runat="server" Visible="false"
                                                                Text='<%# Eval("rpos_idx_shiftTime") %>'></asp:Label>
                                                            <asp:Label ID="labeltextPosShiftdefault" runat="server"
                                                                CssClass="col-sm-12" Visible="false">
                                                                  <p><b>ตำแหน่ง: &nbsp;</b><font color="sandybrown">ยังไม่มีข้อมูลตำแหน่ง</font></p>
                                                            </asp:Label>
                                                        </p>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ตารางงาน" ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                                                    ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="typeShiftTime" runat="server" Visible="false"
                                                            CssClass="col-sm-12" Text='<%# Eval("idxTypeShiftTime") %>'></asp:Label>
                                                        <asp:Label ID="NoneShiftTime" runat="server" Visible="true"
                                                            data-toggle="tooltip" data-placement="top"
                                                            CssClass="col-sm-12">
                                                <div style="text-align: center; color: red; padding-top: 60px;">
                                                   <p>ยังไม่มีการกำหนดตารางงาน</p>
                                                        </asp:Label>
                                                        <asp:Label ID="haveShiftTime" runat="server" Visible="true"
                                                            data-toggle="tooltip" data-placement="top"
                                                            CssClass="col-sm-12">
                                                <div style="text-align: center; color: green; padding-top: 60px;">
                                                   <p><b>ประเภท : </b> <%# Eval("nameType_shift") %> &nbsp;<b>เวลางาน : </b> <%# Eval("nameTime_shift") %> </p>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="การจัดการ" ItemStyle-HorizontalAlign="center"
                                                    HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                                                    ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <div style="text-align: center; padding-top: 60px;">
                                                            <asp:LinkButton ID="editshiftTime" CssClass="btn btn-primary btn-sm"
                                                                runat="server" data-toggle="tooltip" data-placement="top"
                                                                title="แก้ไขตารางงาน" CommandArgument='<%# Eval("emp_shift_idx") %>'
                                                                CommandName="editshiftTime" OnCommand="btnCommand">แก้ไขตารางงาน</asp:LinkButton>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="VieweditSetShiftTime" runat="server">
                    <asp:HyperLink ID="setFocus" runat="server"></asp:HyperLink>
                    <div class="form-horizontal" role="form">
                        <div class="col-lg-12" runat="server" id="Div4">
                            <div class="form-group">
                                <div class="col-lg-12" runat="server" id="Div5">
                                    <div class="panel panel-info">

                                        <div class="panel-heading">
                                            <h3 class="panel-title">
                                                <i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลพนักงาน</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="pull-left">
                                                <a href="#">
                                                    <img class="media-object img-thumbnail"
                                                        src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>'
                                                        width="80px" height="80px" style="margin-top: 10px;">
                                                </a>
                                            </div>

                                            <div class="post-content">
                                                <asp:Repeater ID="Information_empShift" runat="server">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="hiddenOrg" runat="server" Visible="false" Text='<%# Eval("org_idx") %>'></asp:TextBox>
                                                        <div style="margin-top: 15px;">
                                                            &nbsp;&nbsp;&nbsp;
                                                        <div class="col-sm-4">
                                                            <small class="list-group-item-heading text_left">
                                                                <b><font color="#1A5276">รหัสพนักงาน :</font>&nbsp;</b>
                                                                <span><%# Eval("emp_code") %></span></small>
                                                        </div>

                                                            <div class="col-sm-4">
                                                                <small class="list-group-item-heading text_left">
                                                                    <b><font color="#1A5276">ชื่อ - นามสกุล :</font>&nbsp;</b>
                                                                    <span><%# Eval("emp_firstname_th") %>&nbsp;<%# Eval("emp_lastname_th") %></span></small>
                                                            </div>
                                                            <div class="col-sm-4">

                                                                <small class="list-group-item-heading text_left">
                                                                    <b><font color="#1A5276">องค์กร :</font>&nbsp;</b>
                                                                    <span><%# Eval("org_name_th") %></span></small>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <small class="list-group-item-heading text_left">
                                                                    <b><font color="#1A5276">ฝ่าย :</font>&nbsp;</b>
                                                                    <span><%# Eval("dept_name_th") %></span></small>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <small class="list-group-item-heading text_left">
                                                                    <b><font color="#1A5276">แผนก :</font>&nbsp;</b>
                                                                    <span><%# Eval("sec_name_th") %></span></small>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <small class="list-group-item-heading text_left">
                                                                    <b><font color="#1A5276">ตำแหน่ง :</font>&nbsp;</b>
                                                                    <span><%# Eval("pos_name_th") %></span></small>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </asp:View>

            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script>
        $(document).ready(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        })
    </script>



</asp:Content>

