﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data.SqlClient;


using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Text.RegularExpressions;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using DotNet.Highcharts;

public partial class websystem_hr_perf_evalu_emp_profile : System.Web.UI.Page
{
    //2018-12-28 08:52:39.950
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();
    data_hr_evaluation _data_hr_evaluation = new data_hr_evaluation();
    function_dmu _func_dmu = new function_dmu();
    string _localJson = "";
    int emp_idx = 0;
    #region URL

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetWHEREEmpIDX = _serviceUrl + ConfigurationManager.AppSettings["urlGetWHEREEmpIDX"];

    static string _urlGetits_u_hr_evaluation_monthly = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_u_hr_evaluation_monthly"];
    static string _urlGetits_u_hr_evaluation = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_u_hr_evaluation"];

    #endregion

    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            //IndexList.Attributes.Add("class", "active");
            select_empIdx_present();
            show_ViewProfile();


        }
    }
    #endregion

    #region Select

    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["emp_type_idx"] = _dtEmployee.employee_list[0].emp_type_idx;
    }


    protected void SelectGetProfileList()
    {

        _dtEmployee = new data_employee();
        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail _dtemp = new employee_detail();

        _dtemp.emp_idx = int.Parse(ViewState["showempidx"].ToString()); //
        _dtEmployee.employee_list[0] = _dtemp;

        _dtEmployee = callServicePostEmp(_urlGetWHEREEmpIDX, _dtEmployee);
        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
        // ViewState["ShowEmpFV"] = _dtEmployee.employee_list;
        setFormViewData(FvProfile, _dtEmployee.employee_list_tel);


    }


    public void getCountBookingReportGraph(int iYear)
    {
       
       // litDebug.Text = iYear.ToString();
         _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        hr_perf_evalu_emp_monthly select_obj = new hr_perf_evalu_emp_monthly();
        select_obj.emp_idx = emp_idx;
        select_obj.zyear = iYear;
        select_obj.operation_status_id = "grade_employee_profile_graph";
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
        //litDebug.Text = ViewState["admin_idx"].ToString();
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, _data_hr_evaluation);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
        ViewState["Vs_ReportCountGarp"] = _data_hr_evaluation.hr_perf_evalu_emp_monthly_action;
        if (_data_hr_evaluation.hr_perf_evalu_emp_monthly_action != null)
        {

            //litDebug.Text = "3333";
            int count = _data_hr_evaluation.hr_perf_evalu_emp_monthly_action.Length;
            string[] Dept_name = new string[count];
            object[] Count_booking = new object[count];
            object[] grade_name = new object[count];
            int i = 0;
            int iTotal = 0;
            foreach (var data in _data_hr_evaluation.hr_perf_evalu_emp_monthly_action)
            {
                int g = 5;
                Dept_name[i] = data.zyear.ToString() + " / " + data.grade;
                grade_name[i] = data.grade_total.ToString();
                // grade_name[i] = data.grade;
                iTotal += g;

                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "เกรด" } } });
            chart.SetXAxis(new XAxis { Categories = Dept_name });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {

                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "เกรด",
                    Data = new Data(grade_name)
                }
            );
            //  Update_PanelGraph.Visible = true;
            litReportChart.Text = chart.ToHtmlString();

        }
        else
        {
            //Update_PanelGraph.Visible = false;

        }


    }

    private int getGrade(int count_grade, int count_grade_criteria)
    {
        int _grade = 0;
        _grade = count_grade;

        return _grade;

    }

    private string getGrade(string str)
    {
        if ((str == "") || (str == null))
        {
            str = "-";
        }
        return str;
    }

    protected data_hr_evaluation callServicePostPerfEvaluEmp(string _cmdUrl, data_hr_evaluation _dt)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dt);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dt = (data_hr_evaluation)_funcTool.convertJsonToObject(typeof(data_hr_evaluation), _localJson);

        return _dt;
    }
    #endregion

    #region Call Services
    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }
    
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "gvprofileKPI":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lb_ItemIndex = (Label)e.Row.FindControl("lb_ItemIndex");
                    if (lb_ItemIndex.Text == "1")
                    {
                        e.Row.Cells[8].ColumnSpan = 2;
                        e.Row.Cells[9].Visible = false;
                    }
                }
                break;

        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                break;

        }
    }

    #endregion

    #region Trigger
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }



    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvProfile":
                FormView FvProfile = (FormView)ViewDetail.FindControl("FvProfile");

                if (FvProfile.CurrentMode == FormViewMode.ReadOnly)
                {


                }
                else if (FvProfile.CurrentMode == FormViewMode.Edit)
                {

                    /// ddllocname.SelectedValue = lbLocIDX.Text;


                }

                break;
        }
    }
    #endregion



    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {



            case "btnrefresh":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "ViewProfile":

                break;



        }
    }
    #endregion

    private void show_ViewProfile()
    {
        int empidx = int.Parse(ViewState["EmpIDX"].ToString());

        if (empidx == 0)
        {
            ViewState["showempidx"] = int.Parse(ViewState["EmpIDX"].ToString());
        }
        else
        {
            ViewState["showempidx"] = empidx;
        }

        MvMaster.SetActiveView(ViewDetail);
        SelectGetProfileList();
        SelectGetgradeprofile();


    }

    #region xml
    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }


    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    #endregion

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        FormView FvProfile1 = (FormView)ViewDetail.FindControl("FvProfile");

        switch (ddName.ID)
        {

            case "":

                break;



        }
    }

    protected void CreateDshr_gradeprofile()
    {
        string sDs = "dshr_gradeprofile";
        string sVs = "vshr_gradeprofile";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);

        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("year_1", typeof(String));
        ds.Tables[sDs].Columns.Add("year_2", typeof(String));
        ds.Tables[sDs].Columns.Add("year_3", typeof(String));
        ds.Tables[sDs].Columns.Add("year_name_1", typeof(String));
        ds.Tables[sDs].Columns.Add("year_name_2", typeof(String));
        ds.Tables[sDs].Columns.Add("year_name_3", typeof(String));

        ViewState[sVs] = ds;

    }

    protected void CreateDshr_kpi()
    {
        string sDs = "dshr_kpi";
        string sVs = "vshr_kpi";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);

        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("data_name", typeof(String));
        ds.Tables[sDs].Columns.Add("corp_kpi", typeof(String));
        ds.Tables[sDs].Columns.Add("taokae_d", typeof(String));
        ds.Tables[sDs].Columns.Add("tkn_value", typeof(String));
        ds.Tables[sDs].Columns.Add("absence", typeof(String));
        ds.Tables[sDs].Columns.Add("total_deduction", typeof(String));
        ds.Tables[sDs].Columns.Add("punishment1", typeof(String));
        ds.Tables[sDs].Columns.Add("punishment2", typeof(String));
        ds.Tables[sDs].Columns.Add("total_leader_ass_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("grade", typeof(String));
        ds.Tables[sDs].Columns.Add("grade_name", typeof(String));
        ds.Tables[sDs].Columns.Add("zyear", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_status", typeof(String));
        ds.Tables[sDs].Columns.Add("description", typeof(String));

        ViewState[sVs] = ds;

    }
    protected void SelectGetgradeprofile()
    {
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        hr_perf_evalu_emp_monthly select_obj = new hr_perf_evalu_emp_monthly();
        select_obj.emp_idx = emp_idx;
        select_obj.operation_status_id = "employee_profile_year";
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, _data_hr_evaluation);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));

        int iYear = 0;
        if (_data_hr_evaluation.hr_perf_evalu_emp_monthly_action != null)
        {
            foreach (var item in _data_hr_evaluation.hr_perf_evalu_emp_monthly_action)
            {
                iYear = item.zyear;
            }
        }
        /////////////////////////////////
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
         select_obj = new hr_perf_evalu_emp_monthly();
        select_obj.emp_idx = emp_idx;
        select_obj.zyear = iYear;
        select_obj.operation_status_id = "grade_employee_profile_kpi";
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, _data_hr_evaluation);
        // litDebug.Text = emp_idx.ToString();
        CreateDshr_kpi();
        //  _result = "ผลการประเมิน ปัจจุบัน : ";
        //  lb_result.Text = "";
        Label lb_year = (Label)FvProfile.FindControl("lb_year");
        Label lb_evaluation_criteria = (Label)FvProfile.FindControl("lb_evaluation_criteria");
        lb_year.Text = "-";
        int _iYear = 0;
        DataSet dsContacts = (DataSet)ViewState["vshr_kpi"];
        if (_data_hr_evaluation.hr_perf_evalu_emp_monthly_action != null)
        {
            int ic = 0;
            string _sYear = "";
            foreach (var vdr in _data_hr_evaluation.hr_perf_evalu_emp_monthly_action)
            {
                DataRow drContacts = dsContacts.Tables["dshr_kpi"].NewRow();

                drContacts["data_name"] = vdr.data_name;
                drContacts["corp_kpi"] = getformate_kpiDecimal(vdr.corp_kpi);
                drContacts["taokae_d"] = getformate_kpi(vdr.taokae_d);
                drContacts["tkn_value"] = getformate_kpi(vdr.tkn_value);
                drContacts["absence"] = getformate_kpi(vdr.absence);
                drContacts["total_deduction"] = getformate_kpiDecimal(vdr.total_qty); //total_deduction
                if (vdr.item_no == 1)
                {
                    drContacts["punishment1"] = "ตักเตือนด้วยลายลักษณ์อักษร";
                    drContacts["punishment2"] = "พักงาน";
                    drContacts["grade"] = vdr.total_leader_ass_qty.ToString() + " %";
                }
                else
                {
                    drContacts["punishment1"] = getformate_kpi(vdr.punishment1);
                    drContacts["punishment2"] = getformate_kpi(vdr.punishment2);
                    drContacts["grade"] = vdr.grade;

                }
                drContacts["total_leader_ass_qty"] = getformate_kpi(vdr.total_leader_ass_qty);

                drContacts["grade_name"] = vdr.grade_name;
                drContacts["zyear"] = vdr.zyear;
                drContacts["description"] = vdr.description;

                _iYear = vdr.zyear;
                lb_year.Text = (vdr.zyear + 543).ToString();
                lb_evaluation_criteria.Text = getformate_evl(vdr.pms_name) + " / " + getformate_evl(vdr.performane_name);

                dsContacts.Tables["dshr_kpi"].Rows.Add(drContacts);
            }


        }
        ViewState["vshr_kpi"] = dsContacts;
        // GridView gvprofileKPI = (GridView)FvProfile.FindControl("gvprofileKPI");
        _func_dmu.zSetGridData(gvprofileKPI, dsContacts.Tables["dshr_kpi"]);

        getCountBookingReportGraph(iYear);

        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        select_obj = new hr_perf_evalu_emp_monthly();
        select_obj.emp_idx = emp_idx;
        select_obj.zyear = _iYear;
        select_obj.operation_status_id = "grade_employee_profile_tkn";
        _data_hr_evaluation.hr_perf_evalu_emp_monthly_action[0] = select_obj;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_evaluation));
        _data_hr_evaluation = callServicePostPerfEvaluEmp(_urlGetits_u_hr_evaluation_monthly, _data_hr_evaluation);
        setdefault();
        Label lb_punishment1 = (Label)Fv1.FindControl("lb_punishment1");
        Label lb_punishment2 = (Label)Fv1.FindControl("lb_punishment2");
        Label lb_late_mm = (Label)Fv1.FindControl("lb_late_mm");
        Label lb_missing_qty = (Label)Fv1.FindControl("lb_missing_qty");
        lb_punishment1.Text = "-";
        lb_punishment2.Text = "-";
        lb_late_mm.Text = "-";
        lb_missing_qty.Text = "-";
        if (_data_hr_evaluation.hr_perf_evalu_emp_monthly_action != null)
        {
            int ic = 0;
            string _sYear = "";
            int total1 = 0, total2 = 0, total3 = 0, total4 = 0;
            foreach (var vdr in _data_hr_evaluation.hr_perf_evalu_emp_monthly_action)
            {
                //ประเมินตนเอง TKN Value
                lb_emp_ownership.Text = vdr.ass_self_ownership_qty.ToString();
                lb_emp_improvement_continueously.Text = vdr.ass_self_improvement_continuously_qty.ToString();
                lb_emp_commitment.Text = vdr.ass_self_commitment_qty.ToString();

                total1 = vdr.ass_self_ownership_qty + vdr.ass_self_improvement_continuously_qty + vdr.ass_self_commitment_qty;
                lb_emp_tkn_value.Text = total1.ToString();

                lb_leader_ownership.Text = vdr.leader_ownership_qty.ToString();
                lb_leader_improvement_continueously.Text = vdr.leader_improvement_continuously_qty.ToString();
                lb_leader_commitment.Text = vdr.leader_commitment_qty.ToString();

                total2 = vdr.leader_ownership_qty + vdr.leader_improvement_continuously_qty + vdr.leader_commitment_qty;
                lb_leader_tkn_value.Text = total2.ToString();

                //LEADERSHIP CAPABILITIES ความเป็นผู้นำ
                lb_emp_innovation.Text = vdr.ass_self_innovation.ToString();
                lb_emp_achieving_result.Text = vdr.ass_self_achieveing_result.ToString();
                lb_emp_relations.Text = vdr.ass_self_relation.ToString();
                lb_emp_job_knowledge.Text = vdr.ass_self_job_knowledge.ToString();
                lb_emp_art_communication.Text = vdr.ass_self_art_communication.ToString();
                lb_emp_leadership.Text = vdr.ass_self_leadership.ToString();
                lb_emp_decision_making_planning.Text = vdr.ass_self_decision_making_planning.ToString();

                total3 = vdr.ass_self_innovation +
                    vdr.ass_self_achieveing_result +
                    vdr.ass_self_relation +
                    vdr.ass_self_job_knowledge +
                    vdr.ass_self_art_communication +
                    vdr.ass_self_leadership +
                    vdr.ass_self_decision_making_planning;
                lb_emp_leadership_capabilities.Text = total3.ToString();

                lb_leader_innovation.Text = vdr.leader_innovation.ToString();
                lb_leader_achieving_result.Text = vdr.leader_achieveing_result.ToString();
                lb_leader_relations.Text = vdr.leader_relation.ToString();
                lb_leader_job_knowledge.Text = vdr.leader_job_knowledge.ToString();
                lb_leader_art_communication.Text = vdr.leader_art_communication.ToString();
                lb_leader_leadership.Text = vdr.leader_leadership.ToString();
                lb_leader_decision_making_planning.Text = vdr.leader_decision_making_planning.ToString();

                total4 = vdr.leader_innovation +
                    vdr.leader_achieveing_result +
                    vdr.leader_relation +
                    vdr.leader_job_knowledge +
                    vdr.leader_art_communication +
                    vdr.leader_leadership +
                    vdr.leader_decision_making_planning;
                lb_leader_leadership_capabilities.Text = total4.ToString();

                lb_punishment1.Text = getformate_kpi(vdr.punishment1);
                lb_punishment2.Text = getformate_kpi(vdr.punishment2);
                lb_late_mm.Text = getformate_kpi(vdr.late_mm + vdr.Back_before_mm);
                lb_missing_qty.Text = getformate_kpiDecimal(vdr.missing_qty);

            }
        }



    }

    private void setdefault()
    {
        lb_emp_ownership.Text = "";
        lb_emp_improvement_continueously.Text = "";
        lb_emp_commitment.Text = "";
        lb_emp_tkn_value.Text = "";

        lb_leader_ownership.Text = "";
        lb_leader_improvement_continueously.Text = "";
        lb_leader_commitment.Text = "";
        lb_leader_tkn_value.Text = "";

        //LEADERSHIP CAPABILITIES ความเป็นผู้นำ
        lb_emp_innovation.Text = "";
        lb_emp_achieving_result.Text = "";
        lb_emp_relations.Text = "";
        lb_emp_job_knowledge.Text = "";
        lb_emp_art_communication.Text = "";
        lb_emp_leadership.Text = "";
        lb_emp_decision_making_planning.Text = "";

        lb_leader_innovation.Text = "";
        lb_leader_achieving_result.Text = "";
        lb_leader_relations.Text = "";
        lb_leader_job_knowledge.Text = "";
        lb_leader_art_communication.Text = "";
        lb_leader_leadership.Text = "";
        lb_leader_decision_making_planning.Text = "";



    }

    protected void grvMergeHeader_RowCreated(object sender, GridViewRowEventArgs e)
    {
        var gridViewName = (GridView)sender;
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            TableCell HeaderCell = new TableCell();
            HeaderCell.Text = "";
            HeaderCell.ColumnSpan = 6;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "โทษทางวินัย (คะแนน)";
            HeaderCell.ColumnSpan = 2;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "ผลการปฎิบัติงาน";
            HeaderCell.ColumnSpan = 2;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);
            // GridView gvprofileKPI = (GridView)FvProfile.FindControl("gvprofileKPI");
            gvprofileKPI.Controls[0].Controls.AddAt(0, HeaderGridRow);

        }
    }

    private string getformate_kpi(int _i)
    {
        string _str = "";
        if (_i == 0)
        {
            _str = "-";
        }
        else
        {
            _str = _i.ToString();
        }
        return _str;
    }
    private string getformate_kpiDecimal( decimal _i)
    {
        string _str = "";
        if (_i == 0)
        {
            _str = "-";
        }
        else
        {
            _str = _i.ToString();
        }
        return _str;
    }
    private string getformate_evl(string _i)
    {
        string _str = "";
        if (_i == "")
        {
            _str = "-";
        }
        else
        {
            _str = _i;
        }
        return _str;
    }

}