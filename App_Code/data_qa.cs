﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_softwarelicense
/// </summary>
/// 
[Serializable]
[XmlRoot("data_qa")]
public class data_qa
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_typemat")]
    public string return_typemat { get; set; }

    // master data //
    [XmlElement("qa_m0_place_list")]
    public qa_m0_place_detail[] qa_m0_place_list { get; set; }

    [XmlElement("qa_m0_setname_list")]
    public qa_m0_setname_detail[] qa_m0_setname_list { get; set; }

    [XmlElement("qa_m0_datesample_list")]
    public qa_m0_datesample_detail[] qa_m0_datesample_list { get; set; }

    [XmlElement("qa_m0_material_list")]
    public qa_m0_material_detail[] qa_m0_material_list { get; set; }

    [XmlElement("qa_log_material_list")]
    public qa_log_material_detail[] qa_log_material_list { get; set; }

    [XmlElement("qa_m0_test_detail_list")]
    public qa_m0_test_detail[] qa_m0_test_detail_list { get; set; }

    [XmlElement("qa_r0_set_test_detail_list")]
    public qa_r0_set_test_detail[] qa_r0_set_test_detail_list { get; set; }

    [XmlElement("qa_m0_form_detail_list")]
    public qa_m0_form_detail[] qa_m0_form_detail_list { get; set; }

    [XmlElement("qa_m0_option_list")]
    public qa_m0_option_detail[] qa_m0_option_list { get; set; }

    [XmlElement("qa_r0_form_create_list")]
    public qa_r0_form_create_detail[] qa_r0_form_create_list { get; set; }

    [XmlElement("qa_r1_form_create_list")]
    public qa_r1_form_create_detail[] qa_r1_form_create_list { get; set; }

    [XmlElement("qa_m0_lab_list")]
    public qa_m0_lab_detail[] qa_m0_lab_list { get; set; }

    [XmlElement("qa_m0_lab_type_list")]
    public qa_m0_lab_type_detail[] qa_m0_lab_type_list { get; set; }

    [XmlElement("qa_m0_test_type_list")]
    public qa_m0_test_type[] qa_m0_test_type_list { get; set; }

    [XmlElement("qa_r0_set_test_dataset_list")]
    public qa_r0_set_test_dataset_detail[] qa_r0_set_test_dataset_list { get; set; }

    [XmlElement("qa_m1_lab_list")]
    public qa_m1_lab[] qa_m1_lab_list { get; set; }

    [XmlElement("bind_qa_lab_result_list")]
    public bind_qa_lab_result_detail[] bind_qa_lab_result_list { get; set; }

    // master data //

    //document
    [XmlElement("qa_lab_u0_qalab_list")]
    public qa_lab_u0_qalab[] qa_lab_u0_qalab_list { get; set; }

    [XmlElement("qa_lab_u1_qalab_list")]
    public qa_lab_u1_qalab[] qa_lab_u1_qalab_list { get; set; }

    [XmlElement("qa_lab_u2_qalab_list")]
    public qa_lab_u2_qalab[] qa_lab_u2_qalab_list { get; set; }

    [XmlElement("qa_lab_u3_qalab_list")]
    public qa_lab_u3_qalab[] qa_lab_u3_qalab_list { get; set; }

    [XmlElement("qa_lab_u4_qalab_list")]
    public qa_lab_u4_qalab[] qa_lab_u4_qalab_list { get; set; }

    [XmlElement("qa_lab_u5_qalab_list")]
    public qa_lab_u5_qalab[] qa_lab_u5_qalab_list { get; set; }

    [XmlElement("qa_lab_u0doc_qalab_list")]
    public qa_lab_u0doc_qalab[] qa_lab_u0doc_qalab_list { get; set; }

    [XmlElement("qa_lab_vtestdetail_list")]
    public qa_lab_vtestdetail[] qa_lab_vtestdetail_list { get; set; }

    [XmlElement("bind_qa_m0_test_detail_list")]
    public bindqa_m0_test_detail[] bind_qa_m0_test_detail_list { get; set; }

    [XmlElement("qa_m0_history_doc_list")]
    public qa_m0_history_doc_detail[] qa_m0_history_doc_list { get; set; }

    [XmlElement("qa_m0_history_sample_list")]
    public qa_m0_history_sample_detail[] qa_m0_history_sample_list { get; set; }

    [XmlElement("bind_qa_node_decision_list")]
    public bind_qa_node_decision_detail[] bind_qa_node_decision_list { get; set; }

    [XmlElement("bindlab_qa_m0_testdetail_list")]
    public bindlab_m0_test_detail[] bindlab_qa_m0_testdetail_list { get; set; }

    [XmlElement("bind_qa_lab_form_list")]
    public bind_qa_lab_form_detail[] bind_qa_lab_form_list { get; set; }

    [XmlElement("search_document_list")]
    public seach_document_detail[] search_document_list { get; set; }

    [XmlElement("search_document_inlab_list")]
    public seach_document_inlab_detail[] search_document_inlab_list { get; set; }

    [XmlElement("qa_lqb_export_data_list")]
    public qa_lqb_export_data_detail[] qa_lqb_export_data_list { get; set; }


    //document
}

[Serializable]
public class qa_m0_place_detail
{
    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("place_code")]
    public string place_code { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("place_status")]
    public int place_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }


}

[Serializable]
public class qa_m0_setname_detail
{
    [XmlElement("set_idx")]
    public int set_idx { get; set; }

    [XmlElement("set_name")]
    public string set_name { get; set; }

    [XmlElement("set_detail")]
    public string set_detail { get; set; }

    [XmlElement("setroot_idx")]
    public int setroot_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("set_name_status")]
    public int set_name_status { get; set; }


}

[Serializable]
public class qa_m0_datesample_detail
{
    [XmlElement("date_sample_idx")]
    public int date_sample_idx { get; set; }

    [XmlElement("date_sample_name")]
    public string date_sample_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("status")]
    public int status { get; set; }


}

[Serializable]
public class qa_m0_material_detail
{
    [XmlElement("material_idx")]
    public int material_idx { get; set; }

    [XmlElement("material_code")]
    public string material_code { get; set; }

    [XmlElement("material_name")]
    public string material_name { get; set; }

    [XmlElement("material_count")]
    public int material_count { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("type_matcode")]
    public string type_matcode { get; set; }



}

[Serializable]
public class qa_log_material_detail
{
    [XmlElement("material_idx")]
    public int material_idx { get; set; }

    [XmlElement("material_code")]
    public string material_code { get; set; }

    [XmlElement("material_name")]
    public string material_name { get; set; }

    [XmlElement("material_count")]
    public int material_count { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

}

[Serializable]
public class qa_m0_test_detail
{
    [XmlElement("test_detail_idx")]
    public int test_detail_idx { get; set; }

    [XmlElement("test_detail_name")]
    public string test_detail_name { get; set; }

    [XmlElement("test_detail_time")]
    public int test_detail_time { get; set; }

    [XmlElement("test_type_idx")]
    public int test_type_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("test_detail_status")]
    public int test_detail_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("selected")]
    public bool selected { get; set; }

    [XmlElement("root_detail")]
    public int root_detail { get; set; }

    [XmlElement("test_type_name_th")]
    public string test_type_name_th { get; set; }
}

[Serializable]
public class qa_r0_set_test_detail
{
    [XmlElement("set_test_detail_root_idx")]
    public int set_test_detail_root_idx { get; set; }

    [XmlElement("set_test_detail_name")]
    public string set_test_detail_name { get; set; }

    [XmlElement("test_detail_time")]
    public int test_detail_time { get; set; }

    [XmlElement("set_test_detail_idx")]
    public int set_test_detail_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("set_test_detail_status")]
    public int set_test_detail_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }


}

[Serializable]
public class qa_lab_u0_qalab
{
    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("admin_emp_idx")]
    public int admin_emp_idx { get; set; }

    [XmlElement("admin_org_idx")]
    public int admin_org_idx { get; set; }

    [XmlElement("admin_rdept_idx")]
    public int admin_rdept_idx { get; set; }

    [XmlElement("admin_rsec_idx")]
    public int admin_rsec_idx { get; set; }

    [XmlElement("simple_form")]
    public int simple_form { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("cemp_org_idx")]
    public int cemp_org_idx { get; set; }

    [XmlElement("cemp_rdept_idx")]
    public int cemp_rdept_idx { get; set; }

    [XmlElement("cemp_rsec_idx")]
    public int cemp_rsec_idx { get; set; }

    [XmlElement("received_date")]
    public string received_date { get; set; }

    [XmlElement("closed_date")]
    public string closed_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("set_test_detail_idx")]
    public int set_test_detail_idx { get; set; }

    [XmlElement("sent_labout")]
    public int sent_labout { get; set; }


}

[Serializable]
public class qa_lab_u1_qalab
{
    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("u1_list")]
    public string u1_list { get; set; }

    [XmlElement("u1_export_excel_string")]
    public string u1_export_excel_string { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }

    [XmlElement("sample_name")]
    public string sample_name { get; set; }

    [XmlElement("sample_code")]
    public string sample_code { get; set; }

    [XmlElement("date_sample1_idx")]
    public int date_sample1_idx { get; set; }

    [XmlElement("data_sample1")]
    public string data_sample1 { get; set; }

    [XmlElement("date_sample2_idx")]
    public int date_sample2_idx { get; set; }

    [XmlElement("data_sample2")]
    public string data_sample2 { get; set; }

    [XmlElement("batch")]
    public string batch { get; set; }

    [XmlElement("cabinet")]
    public string cabinet { get; set; }

    [XmlElement("shift_time")]
    public string shift_time { get; set; }

    [XmlElement("time")]
    public string time { get; set; }

    [XmlElement("customer_name")]
    public string customer_name { get; set; }

    [XmlElement("other_details")]
    public string other_details { get; set; }

    [XmlElement("material_code")]
    public string material_code { get; set; }

    [XmlElement("material_name")]
    public string material_name { get; set; }

    [XmlElement("certificate")]
    public int certificate { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("count_testu1")]
    public int count_testu1 { get; set; }

    [XmlElement("received_date")]
    public string received_date { get; set; }

    [XmlElement("received_sample_date")]
    public string received_sample_date { get; set; }

    [XmlElement("test_date")]
    public string test_date { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("m0_lab_idx")]
    public int m0_lab_idx { get; set; }

    [XmlElement("lab_name")]
    public string lab_name { get; set; }

    [XmlElement("Count_All")]
    public int Count_All { get; set; }

    [XmlElement("Count_Success")]
    public int Count_Success { get; set; }

    [XmlElement("Count_Place")]
    public int Count_Place { get; set; }

    [XmlElement("r1_form_create_idx")]
    public int r1_form_create_idx { get; set; }

    [XmlElement("form_detail_name")]
    public string form_detail_name { get; set; }

    [XmlElement("r0_form_create_name")]
    public string r0_form_create_name { get; set; }

    [XmlElement("detail_tested")]
    public string detail_tested { get; set; }

    [XmlElement("test_detail_name")]
    public string test_detail_name { get; set; }

    [XmlElement("test_detail_idx")]
    public int test_detail_idx { get; set; }

    [XmlElement("test_type_name_th")]
    public string test_type_name_th { get; set; }

    [XmlElement("str_u1edit")]
    public string str_u1edit { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("set_test_name")]
    public string set_test_name { get; set; }

    [XmlElement("test_detailidx_insert")]
    public string test_detailidx_insert { get; set; }

    [XmlElement("count_test_detail_idx")]
    public int count_test_detail_idx { get; set; }

    [XmlElement("materail_type")]
    public string materail_type { get; set; }

    [XmlElement("org_idx_production")]
    public int org_idx_production { get; set; }

    [XmlElement("org_idx_production_name")]
    public string org_idx_production_name { get; set; }

    [XmlElement("rdept_idx_production")]
    public int rdept_idx_production { get; set; }

    [XmlElement("rdept_idx_production_name")]
    public string rdept_idx_production_name { get; set; }

    [XmlElement("rsec_idx_production")]
    public int rsec_idx_production { get; set; }

    [XmlElement("rsec_idx_production_name")]
    public string rsec_idx_production_name { get; set; }

    [XmlElement("settest_idx")]
    public int settest_idx { get; set; }

    [XmlElement("lab_out")]
    public string lab_out { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("EXP_MFD")]
    public string EXP_MFD { get; set; }

    
}

[Serializable]
public class qa_lab_u2_qalab
{
    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }

    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("received_sample_date")]
    public string received_sample_date { get; set; }

    [XmlElement("test_date")]
    public string test_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("form_detail_idx")]
    public int form_detail_idx { get; set; }

    [XmlElement("lab_type_idx")]
    public int lab_type_idx { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("test_detail_idx")]
    public int test_detail_idx { get; set; }

    [XmlElement("m0_lab_idx")]
    public int m0_lab_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("count_chktest")]
    public int count_chktest { get; set; }

    [XmlElement("material_code")]
    public string material_code { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("admin_emp_idx")]
    public int admin_emp_idx { get; set; }

    [XmlElement("admin_org_idx")]
    public int admin_org_idx { get; set; }

    [XmlElement("admin_rdept_idx")]
    public int admin_rdept_idx { get; set; }

    [XmlElement("admin_rsec_idx")]
    public int admin_rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("testdetail_insert")]
    public string testdetail_insert { get; set; }

   

}

[Serializable]
public class qa_lab_u3_qalab
{
    [XmlElement("u3_qalab_idx")]
    public int u3_qalab_idx { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

}

[Serializable]
public class qa_lab_u4_qalab
{
    [XmlElement("u4_qalab_idx")]
    public int u4_qalab_idx { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("count_detail_tested")]
    public int count_detail_tested { get; set; }

    [XmlElement("count_sample_tested")]
    public int count_sample_tested { get; set; }

}

[Serializable]
public class qa_lab_u5_qalab
{
    [XmlElement("u4_qalab_idx")]
    public int u4_qalab_idx { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("count_detail_tested")]
    public int count_detail_tested { get; set; }

    [XmlElement("count_sample_tested")]
    public int count_sample_tested { get; set; }

    [XmlElement("form_detail_idx")]
    public int form_detail_idx { get; set; }

    [XmlElement("r1_form_create_idx")]
    public int r1_form_create_idx { get; set; }

    [XmlElement("detail_tested")]
    public string detail_tested { get; set; }

}

[Serializable]
public class qa_lab_u0doc_qalab
{
    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("admin_emp_idx")]
    public int admin_emp_idx { get; set; }

    [XmlElement("admin_org_idx")]
    public int admin_org_idx { get; set; }

    [XmlElement("admin_rdept_idx")]
    public int admin_rdept_idx { get; set; }

    [XmlElement("admin_rsec_idx")]
    public int admin_rsec_idx { get; set; }

    [XmlElement("simple_form")]
    public int simple_form { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("cemp_org_idx")]
    public int cemp_org_idx { get; set; }

    [XmlElement("cemp_rdept_idx")]
    public int cemp_rdept_idx { get; set; }

    [XmlElement("cemp_rsec_idx")]
    public int cemp_rsec_idx { get; set; }

    [XmlElement("received_date")]
    public string received_date { get; set; }

    [XmlElement("closed_date")]
    public string closed_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("cemp_rsec")]
    public string cemp_rsec { get; set; }

    [XmlElement("test_detail_name")]
    public string test_detail_name { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_email_sec")]
    public string emp_email_sec { get; set; }

    [XmlElement("sample_code")]
    public string sample_code { get; set; }

    [XmlElement("lab_out")]
    public string lab_out { get; set; }

}

[Serializable]
public class qa_lab_vtestdetail
{
    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("admin_emp_idx")]
    public int admin_emp_idx { get; set; }

    [XmlElement("admin_org_idx")]
    public int admin_org_idx { get; set; }

    [XmlElement("admin_rdept_idx")]
    public int admin_rdept_idx { get; set; }

    [XmlElement("admin_rsec_idx")]
    public int admin_rsec_idx { get; set; }

    [XmlElement("simple_form")]
    public int simple_form { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("cemp_org_idx")]
    public int cemp_org_idx { get; set; }

    [XmlElement("cemp_rdept_idx")]
    public int cemp_rdept_idx { get; set; }

    [XmlElement("cemp_rsec_idx")]
    public int cemp_rsec_idx { get; set; }

    [XmlElement("received_date")]
    public string received_date { get; set; }

    [XmlElement("closed_date")]
    public string closed_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("test_detail_idx")]
    public int test_detail_idx { get; set; }

    [XmlElement("test_detail_name")]
    public string test_detail_name { get; set; }

    [XmlElement("sample_code")]
    public string sample_code { get; set; }

}

[Serializable]
public class bindqa_m0_test_detail
{
    [XmlElement("test_detail_idx")]
    public int test_detail_idx { get; set; }

    [XmlElement("test_detail_name")]
    public string test_detail_name { get; set; }

    [XmlElement("test_detail_time")]
    public int test_detail_time { get; set; }

    [XmlElement("test_type_idx")]
    public int test_type_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("test_detail_status")]
    public int test_detail_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("selected")]
    public bool selected { get; set; }

    [XmlElement("root_detail")]
    public int root_detail { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }


}

[Serializable]
public class qa_m0_history_doc_detail
{
    [XmlElement("u4_qalab_idx")]
    public int u4_qalab_idx { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }

    [XmlElement("sample_code")]
    public string sample_code { get; set; }

    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("count_detail_tested")]
    public int count_detail_tested { get; set; }

    [XmlElement("count_sample_tested")]
    public int count_sample_tested { get; set; }

    [XmlElement("current_artor")]
    public string current_artor { get; set; }

    [XmlElement("current_decision")]
    public string current_decision { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

}

[Serializable]
public class qa_m0_history_sample_detail
{
    [XmlElement("u4_qalab_idx")]
    public int u4_qalab_idx { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("count_detail_tested")]
    public int count_detail_tested { get; set; }

    [XmlElement("count_sample_tested")]
    public int count_sample_tested { get; set; }

    [XmlElement("current_artor")]
    public string current_artor { get; set; }

    [XmlElement("current_decision")]
    public string current_decision { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

}

[Serializable]
public class bind_qa_node_decision_detail
{
    [XmlElement("u4_qalab_idx")]
    public int u4_qalab_idx { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("count_detail_tested")]
    public int count_detail_tested { get; set; }

    [XmlElement("count_sample_tested")]
    public int count_sample_tested { get; set; }

    [XmlElement("current_artor")]
    public string current_artor { get; set; }

    [XmlElement("current_decision")]
    public string current_decision { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("noidx")]
    public int noidx { get; set; }

    [XmlElement("decision_name")]
    public string decision_name { get; set; }

}

[Serializable]
public class qa_m0_form_detail
{
    [XmlElement("form_detail_idx")]
    public int form_detail_idx { get; set; }

    [XmlElement("form_detail_name")]
    public string form_detail_name { get; set; }

    [XmlElement("form_detail")]
    public string form_detail { get; set; }

    [XmlElement("form_detail_root_idx")]
    public int form_detail_root_idx { get; set; }

    [XmlElement("set_idx")]
    public int set_idx { get; set; }

    [XmlElement("set_name")]
    public string set_name { get; set; }

    [XmlElement("option_idx")]
    public int option_idx { get; set; }

    [XmlElement("option_name")]
    public string option_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("form_datail_status")]
    public int form_datail_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }


    [XmlElement("test_ddl")]
    public string test_ddl { get; set; }


}

[Serializable]
public class qa_m0_option_detail
{
    [XmlElement("option_idx")]
    public int option_idx { get; set; }

    [XmlElement("option_name")]
    public string option_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("option_status")]
    public int option_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }


}

[Serializable]
public class qa_r0_form_create_detail
{
    [XmlElement("r0_form_create_idx")]
    public int r0_form_create_idx { get; set; }

    [XmlElement("r0_form_create_name")]
    public string r0_form_create_name { get; set; }

    [XmlElement("test_detail_idx")]
    public int test_detail_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("r0_form_create_status")]
    public int r0_form_create_status { get; set; }

    [XmlElement("form_detail_name")]
    public string form_detail_name { get; set; }

    [XmlElement("test_detail_name")]
    public string test_detail_name { get; set; }


}

[Serializable]
public class qa_r1_form_create_detail
{
    [XmlElement("r1_form_create_idx")] //---
    public int r1_form_create_idx { get; set; }

    [XmlElement("r0_form_create_idx")] //---
    public int r0_form_create_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("form_detail_idx")]  //---
    public int form_detail_idx { get; set; }

    [XmlElement("r1_form_create_status")]  //---
    public int r1_form_create_status { get; set; }

    [XmlElement("form_detail_name")]  //---
    public string form_detail_name { get; set; }

    [XmlElement("r0_form_create_name")]  //---
    public string r0_form_create_name { get; set; }

    [XmlElement("text_name_setform")]  //---
    public string text_name_setform { get; set; }

    [XmlElement("r1_form_detail_root_idx")]  //---
    public int r1_form_detail_root_idx { get; set; }


}

[Serializable]
public class qa_m0_lab_detail
{
    [XmlElement("m0_lab_idx")]
    public int m0_lab_idx { get; set; }

    [XmlElement("lab_name")]
    public string lab_name { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("lab_type_idx")]
    public int lab_type_idx { get; set; }

    [XmlElement("lab_type_name")]
    public string lab_type_name { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("lab_status")]
    public int lab_status { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

}

[Serializable]
public class qa_m0_lab_type_detail
{
    [XmlElement("lab_type_idx")]
    public int lab_type_idx { get; set; }

    [XmlElement("lab_type_name")]
    public string lab_type_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("lab_type_status")]
    public int lab_type_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

}

[Serializable]
public class qa_m0_test_type
{
    [XmlElement("test_type_idx")]
    public int test_type_idx { get; set; }

    [XmlElement("test_type_name_th")]
    public string test_type_name_th { get; set; }

    [XmlElement("test_type_name_en")]
    public string test_type_name_en { get; set; }

    [XmlElement("test_type_symbol")]
    public string test_type_symbol { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("test_type_status")]
    public int test_type_status { get; set; }
}

[Serializable]
public class qa_r0_set_test_dataset_detail
{
    [XmlElement("test_detail_idx")]
    public int test_detail_idx { get; set; }

    [XmlElement("test_detail_name")]
    public string test_detail_name { get; set; }

    [XmlElement("test_type_name_en")]
    public string test_type_name_en { get; set; }

    [XmlElement("test_type_symbol")]
    public string test_type_symbol { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("test_type_status")]
    public int test_type_status { get; set; }

    [XmlElement("test_detail_time")]
    public int test_detail_time { get; set; }
}

[Serializable]
public class bindlab_m0_test_detail
{
    [XmlElement("test_detail_idx")]
    public int test_detail_idx { get; set; }

    [XmlElement("test_detail_name")]
    public string test_detail_name { get; set; }

    [XmlElement("test_detail_time")]
    public int test_detail_time { get; set; }

    [XmlElement("test_type_idx")]
    public int test_type_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("test_detail_status")]
    public int test_detail_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("selected")]
    public bool selected { get; set; }

    [XmlElement("root_detail")]
    public int root_detail { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }

}

[Serializable]
public class qa_m1_lab
{
    [XmlElement("m1_lab_idx")]
    public int m1_lab_idx { get; set; }

    [XmlElement("m0_lab_idx")]
    public int m0_lab_idx { get; set; }

    [XmlElement("test_detail_idx")]
    public int test_detail_idx { get; set; }

    [XmlElement("certificate")]
    public int certificate { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("status")]
    public string status { get; set; }

    [XmlElement("test_detail_name")]
    public string test_detail_name { get; set; }

    [XmlElement("lab_name")]
    public string lab_name { get; set; }

}

[Serializable]
public class bind_qa_lab_form_detail
{
    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }

    [XmlElement("r1_form_create_idx")]
    public int r1_form_create_idx { get; set; }

    [XmlElement("r0_form_create_idx")]
    public int r0_form_create_idx { get; set; }

    [XmlElement("r0_form_create_name")]
    public string r0_form_create_name { get; set; }

    [XmlElement("certificate")]
    public int certificate { get; set; }

    [XmlElement("test_detail_idx")]
    public int test_detail_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("sample_code")]
    public string sample_code { get; set; }

    [XmlElement("test_detail_name")]
    public string test_detail_name { get; set; }

    [XmlElement("form_detail_name")]
    public string form_detail_name { get; set; }

    [XmlElement("form_detail_idx")]
    public int form_detail_idx { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("option_idx")]
    public int option_idx { get; set; }

    [XmlElement("form_detail_root_idx")]
    public int form_detail_root_idx { get; set; }

    [XmlElement("set_idx")]
    public int set_idx { get; set; }

    [XmlElement("r1_form_detail_root_idx")]
    public int r1_form_detail_root_idx { get; set; }

    [XmlElement("text_name_setform")]
    public string text_name_setform { get; set; }

    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("lab_type_idx")]
    public int lab_type_idx { get; set; }

    [XmlElement("m0_lab_idx")]
    public int m0_lab_idx { get; set; }

}

[Serializable]
public class bind_qa_lab_result_detail
{
    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }

    [XmlElement("u5_qalab_idx")]
    public int u5_qalab_idx { get; set; }

    [XmlElement("sample_code")]
    public string sample_code { get; set; }

    [XmlElement("text_name_setform")]
    public string text_name_setform { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("test_detail_name")]
    public string test_detail_name { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("detail_tested")]
    public string detail_tested { get; set; }

    [XmlElement("result_list")]
    public string result_list { get; set; }

    [XmlElement("test_detail_idx")]
    public int test_detail_idx { get; set; }

    [XmlElement("m0_lab")]
    public int m0_lab { get; set; }

}

[Serializable]
public class seach_document_detail
{
    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("sample_code")]
    public string sample_code { get; set; }

    [XmlElement("date_sample1_idx")]
    public int date_sample1_idx { get; set; }

    [XmlElement("data_sample1")]
    public string data_sample1 { get; set; }

    [XmlElement("date_sample2_idx")]
    public int date_sample2_idx { get; set; }

    [XmlElement("data_sample2")]
    public string data_sample2 { get; set; }

    [XmlElement("batch")]
    public string batch { get; set; }

    [XmlElement("cabinet")]
    public string cabinet { get; set; }

    [XmlElement("shift_time")]
    public string shift_time { get; set; }

    [XmlElement("time")]
    public string time { get; set; }

    [XmlElement("customer_name")]
    public string customer_name { get; set; }

    [XmlElement("other_details")]
    public string other_details { get; set; }

    [XmlElement("material_code")]
    public string material_code { get; set; }

    [XmlElement("material_name")]
    public string material_name { get; set; }

    [XmlElement("certificate")]
    public int certificate { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("count_testu1")]
    public int count_testu1 { get; set; }

    [XmlElement("received_date")]
    public string received_date { get; set; }

    [XmlElement("received_sample_date")]
    public string received_sample_date { get; set; }

    [XmlElement("test_date")]
    public string test_date { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("m0_lab_idx")]
    public int m0_lab_idx { get; set; }

    [XmlElement("lab_name")]
    public string lab_name { get; set; }

    [XmlElement("Count_All")]
    public int Count_All { get; set; }

    [XmlElement("Count_Success")]
    public int Count_Success { get; set; }

    [XmlElement("r1_form_create_idx")]
    public int r1_form_create_idx { get; set; }

    [XmlElement("form_detail_name")]
    public string form_detail_name { get; set; }

    [XmlElement("r0_form_create_name")]
    public string r0_form_create_name { get; set; }

    [XmlElement("detail_tested")]
    public string detail_tested { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

}

[Serializable]
public class seach_document_inlab_detail
{
    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("u2_qalab_idx")]
    public int u2_qalab_idx { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("sample_code")]
    public string sample_code { get; set; }

    [XmlElement("date_sample1_idx")]
    public int date_sample1_idx { get; set; }

    [XmlElement("data_sample1")]
    public string data_sample1 { get; set; }

    [XmlElement("date_sample2_idx")]
    public int date_sample2_idx { get; set; }

    [XmlElement("data_sample2")]
    public string data_sample2 { get; set; }

    [XmlElement("batch")]
    public string batch { get; set; }

    [XmlElement("cabinet")]
    public string cabinet { get; set; }

    [XmlElement("shift_time")]
    public string shift_time { get; set; }

    [XmlElement("time")]
    public string time { get; set; }

    [XmlElement("customer_name")]
    public string customer_name { get; set; }

    [XmlElement("other_details")]
    public string other_details { get; set; }

    [XmlElement("material_code")]
    public string material_code { get; set; }

    [XmlElement("material_name")]
    public string material_name { get; set; }

    [XmlElement("certificate")]
    public int certificate { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("count_testu1")]
    public int count_testu1 { get; set; }

    [XmlElement("received_date")]
    public string received_date { get; set; }

    [XmlElement("received_sample_date")]
    public string received_sample_date { get; set; }

    [XmlElement("test_date")]
    public string test_date { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("m0_lab_idx")]
    public int m0_lab_idx { get; set; }

    [XmlElement("lab_name")]
    public string lab_name { get; set; }

    [XmlElement("Count_All")]
    public int Count_All { get; set; }

    [XmlElement("Count_Success")]
    public int Count_Success { get; set; }

    [XmlElement("r1_form_create_idx")]
    public int r1_form_create_idx { get; set; }

    [XmlElement("form_detail_name")]
    public string form_detail_name { get; set; }

    [XmlElement("r0_form_create_name")]
    public string r0_form_create_name { get; set; }

    [XmlElement("detail_tested")]
    public string detail_tested { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class qa_lqb_export_data_detail
{
    [XmlElement("DocumentCode")]
    public string DocumentCode { get; set; }

    [XmlElement("SampleCode")]
    public string SampleCode { get; set; }

    [XmlElement("ชื่อตัวอย่าง")]
    public string ชื่อตัวอย่าง { get; set; }

    [XmlElement("TestItem")]
    public string TestItem { get; set; }

    [XmlElement("Result")]
    public string Result { get; set; }

    [XmlElement("u1_qalab_idx")]
    public int u1_qalab_idx { get; set; }

}



