﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="lab-formprint.aspx.cs" Inherits="websystem_qa_lab_formprint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

     <style type="text/css" media="print,screen">
        @page {
            size: A4 landscape;
            margin: 25px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

        .formPrint {
            margin: 10pt; /*from 5 to 10*/
            padding: 10pt; /*add*/
            width: 842pt;
        }
    </style>


    <title></title>
</head>
<body onload="window.print()">
     <div class="formPrint">
    
        <img src='<%=ResolveUrl("~/uploadfiles/qa_lab/lab_form/lab_form.jpg")%>' width="100%" />

    </div>
</body>
</html>
