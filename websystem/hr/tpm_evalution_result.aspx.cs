using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;

using System.Drawing;
using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Enums;

public partial class websystem_hr_tpm_evalution_result : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_employee _data_employee = new data_employee();
    data_tpm_report _data_tpm_report = new data_tpm_report();

    int _emp_idx = 0;
    int _default_int = 0;
    bool _b_permission = false;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];
    static string _urlGetViewEmployeeListSmall = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewEmployeeListSmall"];

    static string _urlTpmGetTpmReport = _serviceUrl + ConfigurationManager.AppSettings["urlTpmGetTpmReport"];
    static string _urlTpmGetTpmPermission = _serviceUrl + ConfigurationManager.AppSettings["urlTpmGetTpmPermission"];

    // set color for graph
    static string[] _s_color = new string[10] { "#7cb5ec", "#aa4643", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1" };

    static int[] _i_chief = new int[1] { 11 };
    static int[] _i_director = new int[2] { 7, 10 };
    static int[] _i_manager = new int[3] { 5, 6, 9 };
    static int[] _i_sup = new int[2] { 4, 15 };
    static int[] _i_officer = new int[2] { 2, 3 };
    static int[] _i_staff = new int[1] { 14 };

    static int[] _permission = { 172, 24047 };
    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = _funcTool.convertToInt(Session["emp_idx"].ToString());

        // check permission
        foreach (int _pass in _permission)
        {
            if (_emp_idx == _pass)
            {
                _b_permission = true;
                continue;
            }
        }

        if (!_b_permission)
        {
            // Response.Redirect(ResolveUrl("~/"));
            lbHr.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
        }

        setTrigger();
    }

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "navList":
                clearViewState();
                setActiveTab("viewList", 0);
                break;
            case "navHead":
                clearViewState();
                setActiveTab("viewHead", 0);
                break;
            case "navHr":
                clearViewState();
                setActiveTab("viewHr", 0);
                break;
        }
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        DropDownList ddlHeadOrg = (DropDownList)fvSearchHead.FindControl("ddlHeadOrg");
        DropDownList ddlHeadDept = (DropDownList)fvSearchHead.FindControl("ddlHeadDept");
        DropDownList ddlHeadSec = (DropDownList)fvSearchHead.FindControl("ddlHeadSec");
        DropDownList ddlHeadPos = (DropDownList)fvSearchHead.FindControl("ddlHeadPos");
        TextBox tbSearchHeadEmpCode = (TextBox)fvSearchHead.FindControl("tbSearchEmpCode");
        DropDownList ddlHeadType = (DropDownList)fvSearchHead.FindControl("ddlHeadType");

        DropDownList ddlHrOrg = (DropDownList)fvSearchHr.FindControl("ddlHrOrg");
        DropDownList ddlHrDept = (DropDownList)fvSearchHr.FindControl("ddlHrDept");
        DropDownList ddlHrSec = (DropDownList)fvSearchHr.FindControl("ddlHrSec");
        DropDownList ddlHrPos = (DropDownList)fvSearchHr.FindControl("ddlHrPos");
        TextBox tbSearchHrEmpCode = (TextBox)fvSearchHr.FindControl("tbSearchEmpCode");
        DropDownList ddlHrType = (DropDownList)fvSearchHr.FindControl("ddlHrType");

        switch (cmdName)
        {
            case "cmdSearchHead":
                _data_tpm_report = (data_tpm_report)ViewState["ReportList"];
                tpm_report_detail[] _tempData = (tpm_report_detail[])_data_tpm_report.tpm_report_list;
                // select all
                var _linqSearch = from o in _tempData
                                  select o;

                if (_funcTool.convertToInt(ddlHeadOrg.SelectedValue) > 0)
                    _linqSearch = _linqSearch
                                    .Where(o1 => o1.org_name_th.Contains(ddlHeadOrg.SelectedItem.Text)
                                    );
                if (_funcTool.convertToInt(ddlHeadDept.SelectedValue) > 0)
                    _linqSearch = _linqSearch
                                    .Where(o2 => o2.dept_name_th.Contains(ddlHeadDept.SelectedItem.Text)
                                    );
                if (_funcTool.convertToInt(ddlHeadSec.SelectedValue) > 0)
                    _linqSearch = _linqSearch
                                    .Where(o3 => o3.sec_name_th.Contains(ddlHeadSec.SelectedItem.Text)
                                    );
                if (_funcTool.convertToInt(ddlHeadPos.SelectedValue) > 0)
                    _linqSearch = _linqSearch
                                    .Where(o4 => o4.pos_name_th.Contains(ddlHeadPos.SelectedItem.Text)
                                    );
                if (tbSearchHeadEmpCode.Text.Trim() != "")
                    _linqSearch = _linqSearch
                                    .Where(o5 => o5.emp_code.Contains(tbSearchHeadEmpCode.Text.Trim())
                                    );

                ViewState["ReportListExport"] = _linqSearch.ToArray();

                _funcTool.setGvData(gvHeadList, _linqSearch.ToList());
                generateChart((tpm_report_detail[])ViewState["ReportListExport"], "head");
                break;
            case "cmdClearHead":
                clearViewState();
                setActiveTab("viewHead", 0);
                break;
            case "cmdSearchHr":
                _data_tpm_report = (data_tpm_report)ViewState["ReportList"];
                tpm_report_detail[] _tempData2 = (tpm_report_detail[])_data_tpm_report.tpm_report_list;
                // select all
                var _linqSearch2 = from o in _tempData2
                                   select o;

                if (_funcTool.convertToInt(ddlHrOrg.SelectedValue) > 0)
                    _linqSearch2 = _linqSearch2
                                    .Where(o1 => o1.org_name_th.Contains(ddlHrOrg.SelectedItem.Text)
                                    );
                if (_funcTool.convertToInt(ddlHrDept.SelectedValue) > 0)
                    _linqSearch2 = _linqSearch2
                                    .Where(o2 => o2.dept_name_th.Contains(ddlHrDept.SelectedItem.Text)
                                    );
                if (_funcTool.convertToInt(ddlHrSec.SelectedValue) > 0)
                    _linqSearch2 = _linqSearch2
                                    .Where(o3 => o3.sec_name_th.Contains(ddlHrSec.SelectedItem.Text)
                                    );
                if (_funcTool.convertToInt(ddlHrPos.SelectedValue) > 0)
                    _linqSearch2 = _linqSearch2
                                    .Where(o4 => o4.pos_name_th.Contains(ddlHrPos.SelectedItem.Text)
                                    );
                if (tbSearchHrEmpCode.Text.Trim() != "")
                    _linqSearch2 = _linqSearch2
                                    .Where(o5 => o5.emp_code.Contains(tbSearchHrEmpCode.Text.Trim())
                                    );

                ViewState["ReportListExport"] = _linqSearch2.ToArray();

                _funcTool.setGvData(gvHrList, _linqSearch2.ToList());
                generateChart((tpm_report_detail[])ViewState["ReportListExport"], "hr");
                break;
            case "cmdClearHr":
                clearViewState();
                setActiveTab("viewHr", 0);
                break;
            case "cmdExportHead":
            case "cmdExportHr":
                // export data
                tpm_report_detail[] _reportData = (tpm_report_detail[])ViewState["ReportListExport"];
                DataTable tableReport = new DataTable();
                int _count_row = 0;

                if (_reportData == null)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export ค่ะ');", true);
                    return;
                }

                tableReport.Columns.Add("#", typeof(String));
                tableReport.Columns.Add("รหัสพนักงาน", typeof(String));
                tableReport.Columns.Add("ชื่อ-นามสกุล", typeof(String));
                tableReport.Columns.Add("องค์กร/ฝ่าย/แผนก/ตำแหน่ง", typeof(String));
                tableReport.Columns.Add("Corp KPIs", typeof(String));
                tableReport.Columns.Add("Individual KPIs", typeof(String));
                tableReport.Columns.Add("Core Value", typeof(String));
                tableReport.Columns.Add("Competencies", typeof(String));
                tableReport.Columns.Add("Time Attendance", typeof(String));
                tableReport.Columns.Add("Summary", typeof(String));

                foreach (var temp_row in _reportData)
                {
                    DataRow add_row = tableReport.NewRow();

                    add_row[0] = (_count_row + 1).ToString();
                    add_row[1] = temp_row.emp_code.ToString();
                    add_row[2] = temp_row.emp_name_th.ToString();
                    add_row[3] = "องค์กร : " + temp_row.org_name_th.ToString() + "\nฝ่าย : " + temp_row.dept_name_th.ToString() + "\nแผนก : " + temp_row.sec_name_th.ToString() + "\nตำแหน่ง : " + temp_row.pos_name_th.ToString();
                    add_row[4] = temp_row.score_cal_1.ToString();
                    add_row[5] = temp_row.score_cal_2.ToString();
                    add_row[6] = temp_row.score_cal_3.ToString();
                    add_row[7] = temp_row.score_cal_4.ToString();
                    add_row[8] = temp_row.score_cal_5.ToString();
                    add_row[9] = (temp_row.score_cal_1 + temp_row.score_cal_2 + temp_row.score_cal_3 + temp_row.score_cal_4 + temp_row.score_cal_5).ToString();

                    tableReport.Rows.InsertAt(add_row, _count_row++);
                }

                WriteExcelWithNPOI(tableReport, "xls", "report-list");
                break;
        }
    }
    #endregion event command

    #region databound
    protected void fvDataBound(object sender, EventArgs e)
    {
        var fvName = (FormView)sender;
        // switch (fvName.ID)
        // {
        //     case "fvEmpProfile":
        //         break;
        // }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvHeadList":
            case "gvHrList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblScoreCal1 = (Label)e.Row.FindControl("lblScoreCal1");
                    Label lblScoreCal2 = (Label)e.Row.FindControl("lblScoreCal2");
                    Label lblScoreCal3 = (Label)e.Row.FindControl("lblScoreCal3");
                    Label lblScoreCal4 = (Label)e.Row.FindControl("lblScoreCal4");
                    Label lblScoreCal5 = (Label)e.Row.FindControl("lblScoreCal5");
                    Label lblScoreCalSum = (Label)e.Row.FindControl("lblScoreCalSum");

                    lblScoreCalSum.Text = _funcTool.setDecimalPlaces(((
                        _funcTool.convertToDecimal(lblScoreCal1.Text) +
                        _funcTool.convertToDecimal(lblScoreCal2.Text) +
                        _funcTool.convertToDecimal(lblScoreCal3.Text) +
                        _funcTool.convertToDecimal(lblScoreCal4.Text) +
                        _funcTool.convertToDecimal(lblScoreCal5.Text))), 4).ToString();
                }
                break;
        }
    }
    #endregion databound

    #region gridview paging
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvHeadList":
            case "gvHrList":
                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                _funcTool.setGvData(gridViewName, ((tpm_report_detail[])ViewState["ReportListExport"]));
                break;
        }

        hlSetTotop.Focus();
    }
    #endregion gridview paging

    #region dropdown list
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlHeadOrg = (DropDownList)fvSearchHead.FindControl("ddlHeadOrg");
        DropDownList ddlHeadDept = (DropDownList)fvSearchHead.FindControl("ddlHeadDept");
        DropDownList ddlHeadSec = (DropDownList)fvSearchHead.FindControl("ddlHeadSec");
        DropDownList ddlHeadPos = (DropDownList)fvSearchHead.FindControl("ddlHeadPos");
        DropDownList ddlHeadType = (DropDownList)fvSearchHead.FindControl("ddlHeadType");
        LinkButton lbHeadExport = (LinkButton)fvSearchHead.FindControl("lbExport");

        DropDownList ddlHrOrg = (DropDownList)fvSearchHr.FindControl("ddlHrOrg");
        DropDownList ddlHrDept = (DropDownList)fvSearchHr.FindControl("ddlHrDept");
        DropDownList ddlHrSec = (DropDownList)fvSearchHr.FindControl("ddlHrSec");
        DropDownList ddlHrPos = (DropDownList)fvSearchHr.FindControl("ddlHrPos");
        DropDownList ddlHrType = (DropDownList)fvSearchHr.FindControl("ddlHrType");
        LinkButton lbHrExport = (LinkButton)fvSearchHr.FindControl("lbExport");

        _data_tpm_report = (data_tpm_report)ViewState["ReportList"];
        tpm_report_detail[] _tempData = (tpm_report_detail[])_data_tpm_report.tpm_report_list;
        // select all
        var _linqSearch = from o in _tempData
                          select o;

        switch (ddlName.ID)
        {
            case "ddlHeadOrg":
                _data_employee = getDepartmentList(_funcTool.convertToInt(ddlHeadOrg.SelectedValue));
                ddlHeadDept.Items.Clear();
                _funcTool.setDdlData(ddlHeadDept, _data_employee.department_list, "dept_name_th", "rdept_idx");
                ddlHeadDept.Items.Insert(0, new ListItem("--- ฝ่าย ---", "0"));
                ddlHeadSec.Items.Clear();
                ddlHeadSec.Items.Insert(0, new ListItem("--- แผนก ---", "0"));
                ddlHeadPos.Items.Clear();
                ddlHeadPos.Items.Insert(0, new ListItem("--- ตำแหน่ง ---", "0"));

                if (_funcTool.convertToInt(ddlHeadOrg.SelectedValue) > 0)
                    _linqSearch = _linqSearch
                                    .Where(o1 => o1.org_name_th.Contains(ddlHeadOrg.SelectedItem.Text)
                                    );
                else
                    _linqSearch = from o in _tempData
                                  select o;

                ViewState["ReportListExport"] = _linqSearch.ToArray();
                _funcTool.setGvData(gvHeadList, (tpm_report_detail[])ViewState["ReportListExport"]);
                generateChart((tpm_report_detail[])ViewState["ReportListExport"], "head");
                break;
            case "ddlHeadDept":
                _data_employee = getSectionList(_funcTool.convertToInt(ddlHeadOrg.SelectedValue), _funcTool.convertToInt(ddlHeadDept.SelectedValue));
                ddlHeadSec.Items.Clear();
                _funcTool.setDdlData(ddlHeadSec, _data_employee.section_list, "sec_name_th", "rsec_idx");
                ddlHeadSec.Items.Insert(0, new ListItem("--- แผนก ---", "0"));
                ddlHeadPos.Items.Clear();
                ddlHeadPos.Items.Insert(0, new ListItem("--- ตำแหน่ง ---", "0"));

                if (_funcTool.convertToInt(ddlHeadDept.SelectedValue) > 0)
                    _linqSearch = _linqSearch
                                    .Where(o2 => o2.dept_name_th.Contains(ddlHeadDept.SelectedItem.Text)
                                    );
                else
                {
                    _linqSearch = from o in _tempData
                                  select o;
                    _linqSearch = _linqSearch
                                    .Where(o1 => o1.org_name_th.Contains(ddlHeadOrg.SelectedItem.Text)
                                    );
                }

                ViewState["ReportListExport"] = _linqSearch.ToArray();
                _funcTool.setGvData(gvHeadList, (tpm_report_detail[])ViewState["ReportListExport"]);
                generateChart((tpm_report_detail[])ViewState["ReportListExport"], "head");
                break;
            case "ddlHeadSec":
                _data_employee = getPositionList(_funcTool.convertToInt(ddlHeadOrg.SelectedValue), _funcTool.convertToInt(ddlHeadDept.SelectedValue), _funcTool.convertToInt(ddlHeadSec.SelectedValue));
                ddlHeadPos.Items.Clear();
                _funcTool.setDdlData(ddlHeadPos, _data_employee.position_list, "pos_name_th", "rpos_idx");
                ddlHeadPos.Items.Insert(0, new ListItem("--- ตำแหน่ง ---", "0"));

                if (_funcTool.convertToInt(ddlHeadSec.SelectedValue) > 0)
                    _linqSearch = _linqSearch
                                    .Where(o3 => o3.sec_name_th.Contains(ddlHeadSec.SelectedItem.Text)
                                    );
                else
                {
                    _linqSearch = from o in _tempData
                                  select o;
                    _linqSearch = _linqSearch
                                    .Where(o1 => o1.org_name_th.Contains(ddlHeadOrg.SelectedItem.Text)
                                    );
                    _linqSearch = _linqSearch
                                    .Where(o2 => o2.dept_name_th.Contains(ddlHeadDept.SelectedItem.Text)
                                    );
                }

                ViewState["ReportListExport"] = _linqSearch.ToArray();
                _funcTool.setGvData(gvHeadList, (tpm_report_detail[])ViewState["ReportListExport"]);
                generateChart((tpm_report_detail[])ViewState["ReportListExport"], "head");
                break;
            case "ddlHeadPos":
                if (_funcTool.convertToInt(ddlHeadPos.SelectedValue) > 0)
                    _linqSearch = _linqSearch
                                    .Where(o4 => o4.pos_name_th.Contains(ddlHeadPos.SelectedItem.Text)
                                    );
                else
                {
                    _linqSearch = from o in _tempData
                                  select o;
                    _linqSearch = _linqSearch
                                    .Where(o1 => o1.org_name_th.Contains(ddlHeadOrg.SelectedItem.Text)
                                    );
                    _linqSearch = _linqSearch
                                    .Where(o2 => o2.dept_name_th.Contains(ddlHeadDept.SelectedItem.Text)
                                    );
                    _linqSearch = _linqSearch
                                    .Where(o3 => o3.sec_name_th.Contains(ddlHeadSec.SelectedItem.Text)
                                    );
                }

                ViewState["ReportListExport"] = _linqSearch.ToArray();
                _funcTool.setGvData(gvHeadList, (tpm_report_detail[])ViewState["ReportListExport"]);
                generateChart((tpm_report_detail[])ViewState["ReportListExport"], "head");
                break;
            case "ddlHeadType":
                switch (ddlHeadType.SelectedValue)
                {
                    case "1":
                        gvHeadList.Visible = true;
                        divHeadGraph.Visible = false;
                        lbHeadExport.Visible = true;
                        break;
                    case "2":
                        gvHeadList.Visible = false;
                        divHeadGraph.Visible = true;
                        lbHeadExport.Visible = false;
                        break;
                }
                break;
            case "ddlHrOrg":
                _data_employee = getDepartmentList(_funcTool.convertToInt(ddlHrOrg.SelectedValue));
                ddlHrDept.Items.Clear();
                _funcTool.setDdlData(ddlHrDept, _data_employee.department_list, "dept_name_th", "rdept_idx");
                ddlHrDept.Items.Insert(0, new ListItem("--- ฝ่าย ---", "0"));
                ddlHrSec.Items.Clear();
                ddlHrSec.Items.Insert(0, new ListItem("--- แผนก ---", "0"));
                ddlHrPos.Items.Clear();
                ddlHrPos.Items.Insert(0, new ListItem("--- ตำแหน่ง ---", "0"));

                if (_funcTool.convertToInt(ddlHrOrg.SelectedValue) > 0)
                    _linqSearch = _linqSearch
                                    .Where(o1 => o1.org_name_th.Contains(ddlHrOrg.SelectedItem.Text)
                                    );
                else
                    _linqSearch = from o in _tempData
                                  select o;

                ViewState["ReportListExport"] = _linqSearch.ToArray();
                _funcTool.setGvData(gvHrList, (tpm_report_detail[])ViewState["ReportListExport"]);
                generateChart((tpm_report_detail[])ViewState["ReportListExport"], "hr");
                break;
            case "ddlHrDept":
                _data_employee = getSectionList(_funcTool.convertToInt(ddlHrOrg.SelectedValue), _funcTool.convertToInt(ddlHrDept.SelectedValue));
                ddlHrSec.Items.Clear();
                _funcTool.setDdlData(ddlHrSec, _data_employee.section_list, "sec_name_th", "rsec_idx");
                ddlHrSec.Items.Insert(0, new ListItem("--- แผนก ---", "0"));
                ddlHrPos.Items.Clear();
                ddlHrPos.Items.Insert(0, new ListItem("--- ตำแหน่ง ---", "0"));

                if (_funcTool.convertToInt(ddlHrDept.SelectedValue) > 0)
                    _linqSearch = _linqSearch
                                    .Where(o2 => o2.dept_name_th.Contains(ddlHrDept.SelectedItem.Text)
                                    );
                else
                {
                    _linqSearch = from o in _tempData
                                  select o;
                    _linqSearch = _linqSearch
                                    .Where(o1 => o1.org_name_th.Contains(ddlHrOrg.SelectedItem.Text)
                                    );
                }

                ViewState["ReportListExport"] = _linqSearch.ToArray();
                _funcTool.setGvData(gvHrList, (tpm_report_detail[])ViewState["ReportListExport"]);
                generateChart((tpm_report_detail[])ViewState["ReportListExport"], "hr");
                break;
            case "ddlHrSec":
                _data_employee = getPositionList(_funcTool.convertToInt(ddlHrOrg.SelectedValue), _funcTool.convertToInt(ddlHrDept.SelectedValue), _funcTool.convertToInt(ddlHrSec.SelectedValue));
                ddlHrPos.Items.Clear();
                _funcTool.setDdlData(ddlHrPos, _data_employee.position_list, "pos_name_th", "rpos_idx");
                ddlHrPos.Items.Insert(0, new ListItem("--- ตำแหน่ง ---", "0"));

                if (_funcTool.convertToInt(ddlHrSec.SelectedValue) > 0)
                    _linqSearch = _linqSearch
                                    .Where(o3 => o3.sec_name_th.Contains(ddlHrSec.SelectedItem.Text)
                                    );
                else
                {
                    _linqSearch = from o in _tempData
                                  select o;
                    _linqSearch = _linqSearch
                                    .Where(o1 => o1.org_name_th.Contains(ddlHrOrg.SelectedItem.Text)
                                    );
                    _linqSearch = _linqSearch
                                    .Where(o2 => o2.dept_name_th.Contains(ddlHrDept.SelectedItem.Text)
                                    );
                }

                ViewState["ReportListExport"] = _linqSearch.ToArray();
                _funcTool.setGvData(gvHrList, (tpm_report_detail[])ViewState["ReportListExport"]);
                generateChart((tpm_report_detail[])ViewState["ReportListExport"], "hr");
                break;
            case "ddlHrPos":
                if (_funcTool.convertToInt(ddlHrPos.SelectedValue) > 0)
                    _linqSearch = _linqSearch
                                    .Where(o4 => o4.pos_name_th.Contains(ddlHrPos.SelectedItem.Text)
                                    );
                else
                {
                    _linqSearch = from o in _tempData
                                  select o;
                    _linqSearch = _linqSearch
                                    .Where(o1 => o1.org_name_th.Contains(ddlHrOrg.SelectedItem.Text)
                                    );
                    _linqSearch = _linqSearch
                                    .Where(o2 => o2.dept_name_th.Contains(ddlHrDept.SelectedItem.Text)
                                    );
                    _linqSearch = _linqSearch
                                    .Where(o3 => o3.sec_name_th.Contains(ddlHrSec.SelectedItem.Text)
                                    );
                }

                ViewState["ReportListExport"] = _linqSearch.ToArray();
                _funcTool.setGvData(gvHrList, (tpm_report_detail[])ViewState["ReportListExport"]);
                generateChart((tpm_report_detail[])ViewState["ReportListExport"], "hr");
                break;
            case "ddlHrType":
                switch (ddlHrType.SelectedValue)
                {
                    case "1":
                        gvHrList.Visible = true;
                        divHrGraph.Visible = false;
                        lbHrExport.Visible = true;
                        break;
                    case "2":
                        gvHrList.Visible = false;
                        divHrGraph.Visible = true;
                        lbHrExport.Visible = false;
                        break;
                }

                _funcTool.setGvData(gvHrList, (tpm_report_detail[])ViewState["ReportListExport"]);
                generateChart((tpm_report_detail[])ViewState["ReportListExport"], "hr");
                break;
        }
    }

    protected data_employee getOrganizationList()
    {
        _data_employee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _data_employee.organization_list[0] = _orgList;

        _data_employee = callServicePostEmployee(_urlGetOrganizationList, _data_employee);
        return _data_employee;
    }

    protected data_employee getDepartmentList(int _org_idx)
    {
        _data_employee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _data_employee.department_list[0] = _deptList;

        _data_employee = callServicePostEmployee(_urlGetDepartmentList, _data_employee);
        return _data_employee;
    }

    protected data_employee getSectionList(int _org_idx, int _rdept_idx)
    {
        _data_employee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _data_employee.section_list[0] = _secList;

        _data_employee = callServicePostEmployee(_urlGetSectionList, _data_employee);
        return _data_employee;
    }

    protected data_employee getPositionList(int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _data_employee.position_list = new position_details[1];
        position_details _pos_list = new position_details();
        _pos_list.org_idx = _org_idx;
        _pos_list.rdept_idx = _rdept_idx;
        _pos_list.rsec_idx = _rsec_idx;
        _data_employee.position_list[0] = _pos_list;

        _data_employee = callServicePostEmployee(_urlGetPositionList, _data_employee);
        return _data_employee;
    }
    #endregion dropdown list

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveTab("viewList", 0);

        hlSetTotop.Focus();
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        ViewState["ReportList"] = null;
        ViewState["ReportListExport"] = null; //linq result
    }

    protected void setActiveTab(string activeTab, int doc_idx)
    {
        setActiveView(activeTab, doc_idx);
        setActiveTabBar(activeTab);

        switch (activeTab)
        {
            case "viewList":
                search_key_employee _search_key = new search_key_employee();
                _search_key.s_emp_idx = _emp_idx.ToString();
                _data_employee.search_key_emp_list = new search_key_employee[1];
                _data_employee.search_key_emp_list[0] = _search_key;
                _data_employee = callServicePostEmployee(_urlGetViewEmployeeListSmall, _data_employee);
                _funcTool.setFvData(fvEmpProfile, FormViewMode.ReadOnly, _data_employee.employee_list_small);

                search_tpm_report_detail _search_my_emp = new search_tpm_report_detail();
                _search_my_emp.s_emp_idx = _emp_idx.ToString();
                _search_my_emp.s_report_mode = "1";
                _data_tpm_report.search_tpm_report_list = new search_tpm_report_detail[1];
                _data_tpm_report.search_tpm_report_list[0] = _search_my_emp;
                _data_tpm_report = callServicePostTpmReport(_urlTpmGetTpmReport, _data_tpm_report);

                // litDebug.Text = _funcTool.convertObjectToJson(_data_tpm_report);

                // set data to template
                if (_data_tpm_report == null) { return; }
                if (_data_tpm_report.tpm_report_list == null) { return; }
                if (_data_tpm_report.tpm_report_list.Count() > 0)
                {
                    tpm_report_detail[] _my_detail = new tpm_report_detail[1];
                    _my_detail[0] = _data_tpm_report.tpm_report_list[0];
                    litPercentCal1.Text = _funcTool.setDecimalPlaces(_my_detail[0].cal_1, 0).ToString() + "%";
                    litPercentCal2.Text = _funcTool.setDecimalPlaces(_my_detail[0].cal_2, 0).ToString() + "%";
                    litPercentCal3.Text = _funcTool.setDecimalPlaces(_my_detail[0].cal_3, 0).ToString() + "%";
                    litPercentCal4.Text = _funcTool.setDecimalPlaces(_my_detail[0].cal_4, 0).ToString() + "%";
                    litPercentCal5.Text = _funcTool.setDecimalPlaces(_my_detail[0].cal_5, 0).ToString() + "%";

                    litPercentSum.Text = _funcTool.setDecimalPlaces((_my_detail[0].cal_1 + _my_detail[0].cal_2 + _my_detail[0].cal_3 + _my_detail[0].cal_4 + _my_detail[0].cal_5), 0).ToString() + "%";

                    litSumCal1.Text = _my_detail[0].sum_cal_1.ToString();
                    litSumCal2.Text = _my_detail[0].sum_cal_2.ToString();
                    litSumCal3.Text = _my_detail[0].sum_cal_3.ToString();
                    litSumCal4.Text = _my_detail[0].sum_cal_4.ToString();
                    litSumCal5.Text = _my_detail[0].sum_cal_5.ToString();

                    litScoreCal1.Text = _my_detail[0].score_cal_1.ToString();
                    litScoreCal2.Text = _my_detail[0].score_cal_2.ToString();
                    litScoreCal3.Text = _my_detail[0].score_cal_3.ToString();
                    litScoreCal4.Text = _my_detail[0].score_cal_4.ToString();
                    litScoreCal5.Text = _my_detail[0].score_cal_5.ToString();

                    litScoreSum.Text = (_my_detail[0].score_cal_1 + _my_detail[0].score_cal_2 + _my_detail[0].score_cal_3 + _my_detail[0].score_cal_4 + _my_detail[0].score_cal_5).ToString();
                }
                break;
            case "viewHead":
                search_tpm_report_detail _search_head_emp = new search_tpm_report_detail();
                _search_head_emp.s_emp_idx = _emp_idx.ToString();
                _search_head_emp.s_report_mode = "2";
                _data_tpm_report.search_tpm_report_list = new search_tpm_report_detail[1];
                _data_tpm_report.search_tpm_report_list[0] = _search_head_emp;
                _data_tpm_report = callServicePostTpmReport(_urlTpmGetTpmReport, _data_tpm_report);

                clearViewState();
                ViewState["ReportList"] = _data_tpm_report;
                ViewState["ReportListExport"] = _data_tpm_report.tpm_report_list;
                divSearchHead.Visible = ((((data_tpm_report)ViewState["ReportList"]).tpm_report_list) != null) ? ((((data_tpm_report)ViewState["ReportList"]).tpm_report_list.Count() > 0) ? true : false) : false;

                DropDownList ddlHeadOrg = (DropDownList)fvSearchHead.FindControl("ddlHeadOrg");
                DropDownList ddlHeadDept = (DropDownList)fvSearchHead.FindControl("ddlHeadDept");
                DropDownList ddlHeadSec = (DropDownList)fvSearchHead.FindControl("ddlHeadSec");
                DropDownList ddlHeadPos = (DropDownList)fvSearchHead.FindControl("ddlHeadPos");
                TextBox tbSearchHeadEmpCode = (TextBox)fvSearchHead.FindControl("tbSearchEmpCode");
                DropDownList ddlHeadType = (DropDownList)fvSearchHead.FindControl("ddlHeadType");
                LinkButton lbHeadExport = (LinkButton)fvSearchHead.FindControl("lbExport");

                _data_employee = getOrganizationList();
                ddlHeadOrg.Items.Clear();
                _funcTool.setDdlData(ddlHeadOrg, _data_employee.organization_list, "org_name_th", "org_idx");
                ddlHeadOrg.Items.Insert(0, new ListItem("--- องค์กร ---", "0"));
                ddlHeadDept.Items.Clear();
                ddlHeadDept.Items.Insert(0, new ListItem("--- ฝ่าย ---", "0"));
                ddlHeadSec.Items.Clear();
                ddlHeadSec.Items.Insert(0, new ListItem("--- แผนก ---", "0"));
                ddlHeadPos.Items.Clear();
                ddlHeadPos.Items.Insert(0, new ListItem("--- ตำแหน่ง ---", "0"));
                ddlHeadType.SelectedValue = "1";
                tbSearchHeadEmpCode.Text = String.Empty;

                gvHeadList.Visible = false;
                divHeadGraph.Visible = false;
                lbHeadExport.Visible = false;

                switch (ddlHeadType.SelectedValue)
                {
                    case "1":
                        gvHeadList.Visible = true;
                        divHeadGraph.Visible = false;
                        lbHeadExport.Visible = true;
                        break;
                    case "2":
                        gvHeadList.Visible = false;
                        divHeadGraph.Visible = true;
                        lbHeadExport.Visible = false;
                        break;
                }

                _funcTool.setGvData(gvHeadList, ((data_tpm_report)ViewState["ReportList"]).tpm_report_list);
                generateChart((tpm_report_detail[])ViewState["ReportListExport"], "head");

                break;
            case "viewHr":
                search_tpm_report_detail _search_hr_emp = new search_tpm_report_detail();
                _search_hr_emp.s_emp_idx = _emp_idx.ToString();
                _search_hr_emp.s_report_mode = "3";
                _data_tpm_report.search_tpm_report_list = new search_tpm_report_detail[1];
                _data_tpm_report.search_tpm_report_list[0] = _search_hr_emp;
                _data_tpm_report = callServicePostTpmReport(_urlTpmGetTpmReport, _data_tpm_report);

                clearViewState();
                ViewState["ReportList"] = _data_tpm_report;
                ViewState["ReportListExport"] = _data_tpm_report.tpm_report_list;
                divSearchHr.Visible = ((((data_tpm_report)ViewState["ReportList"]).tpm_report_list) != null) ? ((((data_tpm_report)ViewState["ReportList"]).tpm_report_list.Count() > 0) ? true : false) : false;

                DropDownList ddlHrOrg = (DropDownList)fvSearchHr.FindControl("ddlHrOrg");
                DropDownList ddlHrDept = (DropDownList)fvSearchHr.FindControl("ddlHrDept");
                DropDownList ddlHrSec = (DropDownList)fvSearchHr.FindControl("ddlHrSec");
                DropDownList ddlHrPos = (DropDownList)fvSearchHr.FindControl("ddlHrPos");
                TextBox tbSearchHrEmpCode = (TextBox)fvSearchHr.FindControl("tbSearchEmpCode");
                DropDownList ddlHrType = (DropDownList)fvSearchHr.FindControl("ddlHrType");
                LinkButton lbHrExport = (LinkButton)fvSearchHr.FindControl("lbExport");

                _data_employee = getOrganizationList();
                ddlHrOrg.Items.Clear();
                _funcTool.setDdlData(ddlHrOrg, _data_employee.organization_list, "org_name_th", "org_idx");
                ddlHrOrg.Items.Insert(0, new ListItem("--- องค์กร ---", "0"));
                ddlHrDept.Items.Clear();
                ddlHrDept.Items.Insert(0, new ListItem("--- ฝ่าย ---", "0"));
                ddlHrSec.Items.Clear();
                ddlHrSec.Items.Insert(0, new ListItem("--- แผนก ---", "0"));
                ddlHrPos.Items.Clear();
                ddlHrPos.Items.Insert(0, new ListItem("--- ตำแหน่ง ---", "0"));
                ddlHrType.SelectedValue = "1";
                tbSearchHrEmpCode.Text = String.Empty;

                gvHrList.Visible = false;
                divHrGraph.Visible = false;
                lbHrExport.Visible = false;

                switch (ddlHrType.SelectedValue)
                {
                    case "1":
                        gvHrList.Visible = true;
                        divHrGraph.Visible = false;
                        lbHrExport.Visible = true;
                        break;
                    case "2":
                        gvHrList.Visible = false;
                        divHrGraph.Visible = true;
                        lbHrExport.Visible = false;
                        break;
                }

                _funcTool.setGvData(gvHrList, ((data_tpm_report)ViewState["ReportList"]).tpm_report_list);
                generateChart((tpm_report_detail[])ViewState["ReportListExport"], "hr");
                break;
        }

        hlSetTotop.Focus();
    }

    protected void setActiveView(string activeTab, int doc_idx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _data_employee)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_employee);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _local_json);

        return _data_employee;
    }

    protected data_tpm_report callServicePostTpmReport(string _cmdUrl, data_tpm_report _data_tpm_report)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_tpm_report);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_tpm_report = (data_tpm_report)_funcTool.convertJsonToObject(typeof(data_tpm_report), _local_json);

        return _data_tpm_report;
    }

    protected void setActiveTabBar(string activeTab)
    {
        switch (activeTab)
        {
            case "viewList":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                break;
            case "viewHead":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                break;
            case "viewHr":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                break;
        }
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void dropDownListTrigger(DropDownList ddlID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerDdl = new PostBackTrigger();
        triggerDdl.ControlID = ddlID.UniqueID;
        updatePanel.Triggers.Add(triggerDdl);
    }

    protected void gridViewTrigger(GridView gridViewID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerGridView = new PostBackTrigger();
        triggerGridView.ControlID = gridViewID.UniqueID;
        updatePanel.Triggers.Add(triggerGridView);
    }

    protected void setTrigger()
    {
        // nav trigger
        linkBtnTrigger(lbList);
        linkBtnTrigger(lbHead);
        linkBtnTrigger(lbHr);

        // fvSearchHead trigger
        LinkButton lbHeadSearch = (LinkButton)fvSearchHead.FindControl("lbSearch");
        LinkButton lbHeadClear = (LinkButton)fvSearchHead.FindControl("lbClear");
        LinkButton lbHeadExport = (LinkButton)fvSearchHead.FindControl("lbExport");
        try { linkBtnTrigger(lbHeadSearch); } catch { }
        try { linkBtnTrigger(lbHeadClear); } catch { }
        try { linkBtnTrigger(lbHeadExport); } catch { }

        DropDownList ddlHeadOrg = (DropDownList)fvSearchHead.FindControl("ddlHeadOrg");
        DropDownList ddlHeadDept = (DropDownList)fvSearchHead.FindControl("ddlHeadDept");
        DropDownList ddlHeadSec = (DropDownList)fvSearchHead.FindControl("ddlHeadSec");
        DropDownList ddlHeadPos = (DropDownList)fvSearchHead.FindControl("ddlHeadPos");
        DropDownList ddlHeadType = (DropDownList)fvSearchHead.FindControl("ddlHeadType");
        try { dropDownListTrigger(ddlHeadOrg); } catch { }
        try { dropDownListTrigger(ddlHeadDept); } catch { }
        try { dropDownListTrigger(ddlHeadSec); } catch { }
        try { dropDownListTrigger(ddlHeadPos); } catch { }
        try { dropDownListTrigger(ddlHeadType); } catch { }

        // fvSearchHr trigger
        LinkButton lbHrSearch = (LinkButton)fvSearchHr.FindControl("lbSearch");
        LinkButton lbHrClear = (LinkButton)fvSearchHr.FindControl("lbClear");
        LinkButton lbHrExport = (LinkButton)fvSearchHr.FindControl("lbExport");
        try { linkBtnTrigger(lbHrSearch); } catch { }
        try { linkBtnTrigger(lbHrClear); } catch { }
        try { linkBtnTrigger(lbHrExport); } catch { }

        DropDownList ddlHrOrg = (DropDownList)fvSearchHr.FindControl("ddlHrOrg");
        DropDownList ddlHrDept = (DropDownList)fvSearchHr.FindControl("ddlHrDept");
        DropDownList ddlHrSec = (DropDownList)fvSearchHr.FindControl("ddlHrSec");
        DropDownList ddlHrPos = (DropDownList)fvSearchHr.FindControl("ddlHrPos");
        DropDownList ddlHrType = (DropDownList)fvSearchHr.FindControl("ddlHrType");
        try { dropDownListTrigger(ddlHrOrg); } catch { }
        try { dropDownListTrigger(ddlHrDept); } catch { }
        try { dropDownListTrigger(ddlHrSec); } catch { }
        try { dropDownListTrigger(ddlHrPos); } catch { }
        try { dropDownListTrigger(ddlHrType); } catch { }

        gridViewTrigger(gvHeadList);
        gridViewTrigger(gvHrList);
    }

    public decimal setDecimal(decimal _data_in, int places)
    {
        return _funcTool.setDecimalPlaces(_data_in, places);
    }

    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }

    protected Highcharts setChart(string _chartName, string _chartTitle, int _chartColor, tpm_report_detail[] _chartData)
    {
        tpm_report_detail[] _reportData = _chartData;
        var _linqSearch = from o in _reportData
                          select o;
        // var _selectScore = (from x in _linqSearch
        //                    .Where(y => y.score_cal_3 > 0 && y.score_cal_4 > 0)
        //                    .GroupBy(x => (x.score_cal_1 + x.score_cal_2 + x.score_cal_3 + x.score_cal_4 + x.score_cal_5))
        //                    .OrderBy(z => (z.First().score_cal_1 + z.First().score_cal_2 + z.First().score_cal_3 + z.First().score_cal_4 + z.First().score_cal_5))
        //                     select x);
        var _selectScore = (from x in _linqSearch
                           .Where(y => y.score_cal_3 > 0 && y.score_cal_4 > 0)
                           .GroupBy(x => (_funcTool.setDecimalPlaces((x.score_cal_1 + x.score_cal_2 + x.score_cal_3 + x.score_cal_4 + x.score_cal_5), 0)))
                           .OrderBy(z => (z.First().score_cal_1 + z.First().score_cal_2 + z.First().score_cal_3 + z.First().score_cal_4 + z.First().score_cal_5))
                            select x);
        int _countScore = _selectScore.Count();
        string[] _valScore = new string[_countScore];

        int i = 0;
        foreach (var item in _selectScore.ToList())
        {
            // try { _valScore[i] = (item.ElementAt(0).score_cal_1 + item.ElementAt(0).score_cal_2 + item.ElementAt(0).score_cal_3 + item.ElementAt(0).score_cal_4 + item.ElementAt(0).score_cal_5).ToString(); }
            try { _valScore[i] = _funcTool.setDecimalPlaces((item.ElementAt(0).score_cal_1 + item.ElementAt(0).score_cal_2 + item.ElementAt(0).score_cal_3 + item.ElementAt(0).score_cal_4 + item.ElementAt(0).score_cal_5), 0).ToString(); }
            catch { _valScore[i] = "0"; };
            i++;
        }

        int _countRow = _selectScore.Count();
        object[] v_display = new object[_countRow];
        for (int n = 0; n < _countRow; n++)
        {
            // var _selectValue = (from x in _linqSearch
            //                     .Where(x => (x.score_cal_1 + x.score_cal_2 + x.score_cal_3 + x.score_cal_4 + x.score_cal_5).ToString() == _valScore[n])
            //                     select x).ToList();
            var _selectValue = (from x in _linqSearch
                                .Where(x => _funcTool.setDecimalPlaces((x.score_cal_1 + x.score_cal_2 + x.score_cal_3 + x.score_cal_4 + x.score_cal_5), 0).ToString() == _valScore[n])
                                select x).ToList();
            try { v_display[n] = _selectValue.Count(); }
            catch { v_display[n] = 0; };
        }

        string _chartId = "chart" + System.Guid.NewGuid().ToString();
        Highcharts chart = new Highcharts(_chartName)
                .InitChart(new Chart
                {
                    DefaultSeriesType = ChartTypes.Spline,
                    // DefaultSeriesType = ChartTypes.Column,
                    MarginRight = 25,
                    MarginBottom = 50,
                    ClassName = _chartName
                })
                .SetTitle(new Title
                {
                    Text = "PMS : Score" + "(" + _chartTitle + ")",
                    X = -20
                })
                .SetSubtitle(new Subtitle
                {
                    Text = "Source: mas.taokaenoi.co.th",
                    X = -20
                })
                .SetXAxis(new XAxis
                {
                    Type = AxisTypes.Linear,
                    TickPixelInterval = 50,
                    Labels = new XAxisLabels
                    {
                        Rotation = -45
                    },
                    Categories = _valScore
                })
                .SetYAxis(new YAxis
                {
                    Title = new YAxisTitle { Text = "Count" },
                    PlotLines = new[]
                            {
                                new YAxisPlotLines
                                    {
                                        Value = 0,
                                        Width = 1,
                                        Color = ColorTranslator.FromHtml("#808080")
                                    }
                            }
                })
                .SetTooltip(new Tooltip
                {
                    // Formatter = @"function() {
                    //                     return '<b>'+ this.series.name +'</b><br/>'+
                    //                 Highcharts.numberFormat(this.x, 2) +', Count '+ this.y +'';
                    //             }"
                    Formatter = @"function() {
                                        return '<b>'+ this.series.name +'</b><br/>'+
                                    Highcharts.numberFormat(this.x, 0) +', Count '+ this.y +'';
                                }"
                })
                .SetLegend(new Legend { Enabled = false })
                .SetSeries(new[]
                    {
                        new Series { Name = "Score", Data = new Data(v_display), Color = System.Drawing.ColorTranslator.FromHtml(_s_color[_chartColor]) }
                    }
                );

        return chart;
    }

    protected void generateChart(tpm_report_detail[] _allData, string _sType)
    {
        switch (_sType)
        {
            case "head":
                divHeadChart1.Visible = false;
                divHeadChart2.Visible = false;
                divHeadChart3.Visible = false;
                divHeadChart4.Visible = false;
                divHeadChart5.Visible = false;
                divHeadChart6.Visible = false;
                break;
            case "hr":
                divHrChart1.Visible = false;
                divHrChart2.Visible = false;
                divHrChart3.Visible = false;
                divHrChart4.Visible = false;
                divHrChart5.Visible = false;
                divHrChart6.Visible = false;
                break;
        }

        if (_allData.Count() > 0)
        {
            switch (_sType)
            {
                case "head":
                    divHeadChart1.Visible = true;
                    litHeadChart1.Text = setChart("chart1", "All", 0, _allData).ToHtmlString();
                    break;
                case "hr":
                    divHrChart1.Visible = true;
                    litHrChart1.Text = setChart("chart1", "All", 0, _allData).ToHtmlString();
                    break;
            }
        }

        var _chiefData = (from m2 in _allData
                            .Where(m2 => _i_chief.Contains(m2.group_idx))
                          select m2);
        if (_chiefData.Count() > 0)
        {
            switch (_sType)
            {
                case "head":
                    divHeadChart2.Visible = true;
                    litHeadChart2.Text = setChart("chart2", "Chief", 1, (tpm_report_detail[])_chiefData.ToArray()).ToHtmlString();
                    break;
                case "hr":
                    divHrChart2.Visible = true;
                    litHrChart2.Text = setChart("chart2", "Chief", 1, (tpm_report_detail[])_chiefData.ToArray()).ToHtmlString();
                    break;
            }
        }

        var _directorData = (from m3 in _allData
                            .Where(m3 => _i_director.Contains(m3.group_idx))
                             select m3);
        if (_directorData.Count() > 0)
        {
            switch (_sType)
            {
                case "head":
                    divHeadChart3.Visible = true;
                    litHeadChart3.Text = setChart("chart3", "Director", 2, (tpm_report_detail[])_directorData.ToArray()).ToHtmlString();
                    break;
                case "hr":
                    divHrChart3.Visible = true;
                    litHrChart3.Text = setChart("chart3", "Director", 2, (tpm_report_detail[])_directorData.ToArray()).ToHtmlString();
                    break;
            }
        }

        var _managerData = (from m4 in _allData
                            .Where(m4 => _i_manager.Contains(m4.group_idx))
                            select m4);

        if (_managerData.Count() > 0)
        {
            switch (_sType)
            {
                case "head":
                    divHeadChart4.Visible = true;
                    litHeadChart4.Text = setChart("chart4", "Manager", 3, (tpm_report_detail[])_managerData.ToArray()).ToHtmlString();
                    break;
                case "hr":
                    divHrChart4.Visible = true;
                    litHrChart4.Text = setChart("chart4", "Manager", 3, (tpm_report_detail[])_managerData.ToArray()).ToHtmlString();
                    break;
            }
        }

        var _officerData = (from m5 in _allData
                            .Where(m5 => _i_officer.Contains(m5.group_idx))
                            select m5);
        if (_officerData.Count() > 0)
        {
            switch (_sType)
            {
                case "head":
                    divHeadChart5.Visible = true;
                    litHeadChart5.Text = setChart("chart5", "Officer", 4, (tpm_report_detail[])_officerData.ToArray()).ToHtmlString();
                    break;
                case "hr":
                    divHrChart5.Visible = true;
                    litHrChart5.Text = setChart("chart5", "Officer", 4, (tpm_report_detail[])_officerData.ToArray()).ToHtmlString();
                    break;
            }
        }

        var _staffData = (from m6 in _allData
                            .Where(m6 => _i_staff.Contains(m6.group_idx))
                          select m6);
        if (_staffData.Count() > 0)
        {
            switch (_sType)
            {
                case "head":
                    divHeadChart6.Visible = true;
                    litHeadChart6.Text = setChart("chart6", "Staff", 5, (tpm_report_detail[])_staffData.ToArray()).ToHtmlString();
                    break;
                case "hr":
                    divHrChart6.Visible = true;
                    litHrChart6.Text = setChart("chart6", "Staff", 5, (tpm_report_detail[])_staffData.ToArray()).ToHtmlString();
                    break;
            }
        }
    }
    #endregion reuse
}