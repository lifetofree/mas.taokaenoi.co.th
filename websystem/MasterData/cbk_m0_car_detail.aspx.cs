﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_cbk_m0_car_detail : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();
    data_carbooking _data_carbooking = new data_carbooking();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- car --//
    static string _urlGetCbkm0TypeCar = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0TypeCar"];
    static string _urlGetCbkm0DetailTypeCar = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0DetailTypeCar"];
    static string _urlGetCbkm0Province = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0Province"];
    static string _urlGetCbkm0Admin = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0Admin"];

    static string _urlGetCbkm0Costcenter = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0Costcenter"];
    static string _urlGetCbkm0Type = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0Type"];
    static string _urlGetCbkm0BrandCar = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0BrandCar"];
    static string _urlGetCbkm0EngineCar = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0EngineCar"];
    static string _urlGetCbkm0FuelCar = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0FuelCar"];

    static string _urlSetCbkm0CarDetail = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkm0CarDetail"];
    static string _urlGetCbkm0DetailCar = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0DetailCar"];
    static string _urlGetCbkm0DetailCarView = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0DetailCarView"];
    static string _urlGetCbkLogTaxRoomDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkLogTaxRoomDetail"];

    static string _urlSetCbkm1CarDetailTax = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkm1CarDetailTax"];
    static string _urlSetCbkm0DetailCarDel = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkm0DetailCarDel"];
    //-- car --//

    //--master data ma car --//
    static string _urlSetCbkm0TopicMa = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkm0TopicMa"];
    static string _urlGetCbkTopicMA = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkTopicMA"];
    static string _urlSetCbkm0TopicMADel = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkm0TopicMADel"];
    static string _urlGetCbkddlCarregisterMA = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkddlCarregisterMA"];
    static string _urlGetCbkDetailTypeCostMA = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkDetailTypeCostMA"];
    static string _urlGetCbkDetailCarMA = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkDetailCarMA"];
    static string _urlSetCbkSaveDetailCarMa = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkSaveDetailCarMa"];
    static string _urlGetCbkViewDetailCarMA = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkViewDetailCarMA"];
    static string _urlGetCbkViewTopicDetailCarMA = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkViewTopicDetailCarMA"];
    static string _urlGetCbkLogDataioCarMA = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkLogDataioCarMA"];
    static string _urlGetCbkSearchReportTableCarMA = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkSearchReportTableCarMA"];
    //--master data ma car --//


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;

    decimal tot_actual = 0;
    decimal tot_actual_report = 0;
    int _set_statusFilter = 0;
    //ViewState["vs_ViewWaitApproveRoomBooking"]

    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            initPageLoad();
        }

        linkBtnTrigger(lbCreateDetail);
        linkBtnTrigger(lbCreateCar_MA);
        //linkBtnTrigger(btnViewCarDetail);

    }

    #region bind data

    protected void getDetailTypeCostMA(int _m0_car_idx)
    {

        data_carbooking data_m0_car_detailtype = new data_carbooking();
        cbk_m0_car_detail m0_car_detailtype = new cbk_m0_car_detail();
        data_m0_car_detailtype.cbk_m0_car_list = new cbk_m0_car_detail[1];

        m0_car_detailtype.m0_car_idx = _m0_car_idx;

        data_m0_car_detailtype.cbk_m0_car_list[0] = m0_car_detailtype;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_car_register));        
        data_m0_car_detailtype = callServicePostCarBooking(_urlGetCbkDetailTypeCostMA, data_m0_car_detailtype);

        if (data_m0_car_detailtype.return_code == 0)
        {
            linkBtnTrigger(btnSaveDetailMA);
            UpdatePanel Panel_CreateMA = (UpdatePanel)FvInsertCarMA.FindControl("Panel_CreateMA");
            TextBox txt_DetailTypeCar = (TextBox)FvInsertCarMA.FindControl("txt_DetailTypeCar_ma");
            TextBox txt_CostCenter_detail = (TextBox)FvInsertCarMA.FindControl("txt_CostCenter_detail_ma");

            txt_DetailTypeCar.Text = data_m0_car_detailtype.cbk_m0_car_list[0].detailtype_car_name;
            txt_CostCenter_detail.Text = data_m0_car_detailtype.cbk_m0_car_list[0].costcenter_no;
        }

    }

    protected void getCarregisterCarMA(DropDownList ddlName, int _type_car_idx, int _carrisgister_idx)
    {

        data_carbooking data_m0_car_register = new data_carbooking();
        cbk_m0_car_detail m0_car_register_detail = new cbk_m0_car_detail();
        data_m0_car_register.cbk_m0_car_list = new cbk_m0_car_detail[1];
        m0_car_register_detail.type_car_idx = _type_car_idx;

        data_m0_car_register.cbk_m0_car_list[0] = m0_car_register_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_car_register));        
        data_m0_car_register = callServicePostCarBooking(_urlGetCbkddlCarregisterMA, data_m0_car_register);

        setDdlData(ddlName, data_m0_car_register.cbk_m0_car_list, "car_register", "m0_car_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกทะเบียนรถ ---", "0"));
        ddlName.SelectedValue = _carrisgister_idx.ToString();

        //linkBtnTrigger(btnSaveDetailMA);



    }

    protected void getTypeCar(DropDownList ddlName, int _type_car_idx)
    {

        data_carbooking data_m0_typecar_detail = new data_carbooking();
        cbk_m0_typecar_detail m0_typecar_detail = new cbk_m0_typecar_detail();
        data_m0_typecar_detail.cbk_m0_typecar_list = new cbk_m0_typecar_detail[1];

        data_m0_typecar_detail.cbk_m0_typecar_list[0] = m0_typecar_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_typecar_detail = callServicePostCarBooking(_urlGetCbkm0TypeCar, data_m0_typecar_detail);

        setDdlData(ddlName, data_m0_typecar_detail.cbk_m0_typecar_list, "type_car_name", "type_car_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทรถ ---", "0"));
        ddlName.SelectedValue = _type_car_idx.ToString();

    }

    protected void getDetailTypeCar(DropDownList ddlName, int _type_car_idx, int _detail_typecar_idx)
    {

        data_carbooking data_m1_typecar_detail = new data_carbooking();
        cbk_m1_typecar_detail m1_typecar_detail = new cbk_m1_typecar_detail();
        data_m1_typecar_detail.cbk_m1_typecar_list = new cbk_m1_typecar_detail[1];

        m1_typecar_detail.condition = 0;
        m1_typecar_detail.type_car_idx = _type_car_idx;

        data_m1_typecar_detail.cbk_m1_typecar_list[0] = m1_typecar_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m1_typecar_detail = callServicePostCarBooking(_urlGetCbkm0DetailTypeCar, data_m1_typecar_detail);

        //litDebug.Text = data_m1_typecar_detail.cbk_m1_typecar_list.Length.ToString();

        //if(data_m1_typecar_detail.cbk_m1_typecar_list.Length > 0)
        //{
        //    litDebug.Text = data_m1_typecar_detail.cbk_m1_typecar_list.Length.ToString();
        //}
        //else
        //{
        //    litDebug.Text = "2222";
        //}

        setDdlData(ddlName, data_m1_typecar_detail.cbk_m1_typecar_list, "detailtype_car_name", "detailtype_car_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกรายละเอียดประเภทรถ ---", "0"));

        if (_detail_typecar_idx > 0)
        {
            ddlName.SelectedValue = _detail_typecar_idx.ToString();

        }


    }

    protected void getDetailProvince(DropDownList ddlName, int _province_idx)
    {

        data_carbooking data_m0_province_detail = new data_carbooking();
        cbk_m0_province_detail m0_province_detail = new cbk_m0_province_detail();
        data_m0_province_detail.cbk_m0_province_list = new cbk_m0_province_detail[1];

        data_m0_province_detail.cbk_m0_province_list[0] = m0_province_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_province_detail = callServicePostCarBooking(_urlGetCbkm0Province, data_m0_province_detail);

        setDdlData(ddlName, data_m0_province_detail.cbk_m0_province_list, "ProvName", "ProvIDX");
        ddlName.Items.Insert(0, new ListItem("--- เลือกจังหวัด ---", "0"));

        ddlName.SelectedValue = _province_idx.ToString();


    }

    protected void getDetailAdmin(DropDownList ddlName, int _admin_idx)
    {

        data_carbooking data_m0_admin_detail = new data_carbooking();
        cbk_m0_admin_detail m0_admin_detail = new cbk_m0_admin_detail();
        data_m0_admin_detail.cbk_m0_admin_list = new cbk_m0_admin_detail[1];

        data_m0_admin_detail.cbk_m0_admin_list[0] = m0_admin_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_admin_detail = callServicePostCarBooking(_urlGetCbkm0Admin, data_m0_admin_detail);

        setDdlData(ddlName, data_m0_admin_detail.cbk_m0_admin_list, "emp_name_th", "emp_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกผู้ดูแล ---", "0"));
        ddlName.SelectedValue = _admin_idx.ToString();

    }

    protected void getCostcenter(DropDownList ddlName, int _costcenter_idx)
    {

        data_carbooking data_m0_costcenter_detail = new data_carbooking();
        cbk_m0_costcenter_detail m0_costcenter_detail = new cbk_m0_costcenter_detail();
        data_m0_costcenter_detail.cbk_m0_costcenter_list = new cbk_m0_costcenter_detail[1];

        data_m0_costcenter_detail.cbk_m0_costcenter_list[0] = m0_costcenter_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_costcenter_detail = callServicePostCarBooking(_urlGetCbkm0Costcenter, data_m0_costcenter_detail);

        setDdlData(ddlName, data_m0_costcenter_detail.cbk_m0_costcenter_list, "CostNo", "CostIDX");
        ddlName.Items.Insert(0, new ListItem("--- เลือก CostCenter ---", "0"));
        ddlName.SelectedValue = _costcenter_idx.ToString();

    }

    protected void getType(DropDownList ddlName, int _type_idx)
    {

        data_carbooking data_m0_type_detail = new data_carbooking();
        cbk_m0_type_detail m0_type_detail = new cbk_m0_type_detail();
        data_m0_type_detail.cbk_m0_type_list = new cbk_m0_type_detail[1];

        data_m0_type_detail.cbk_m0_type_list[0] = m0_type_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_type_detail = callServicePostCarBooking(_urlGetCbkm0Type, data_m0_type_detail);

        setDdlData(ddlName, data_m0_type_detail.cbk_m0_type_list, "type_name", "type_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภท ---", "0"));
        ddlName.SelectedValue = _type_idx.ToString();

    }

    protected void getBrandCar(DropDownList ddlName, int _brandcar_idx)
    {

        data_carbooking data_m0_brandcar_detail = new data_carbooking();
        cbk_m0_brandcar_detail m0_brandcar_detail = new cbk_m0_brandcar_detail();
        data_m0_brandcar_detail.cbk_m0_brandcar_list = new cbk_m0_brandcar_detail[1];

        data_m0_brandcar_detail.cbk_m0_brandcar_list[0] = m0_brandcar_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_brandcar_detail = callServicePostCarBooking(_urlGetCbkm0BrandCar, data_m0_brandcar_detail);

        setDdlData(ddlName, data_m0_brandcar_detail.cbk_m0_brandcar_list, "brand_car_name", "brand_car_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกยี่ห้อรถ ---", "0"));
        ddlName.SelectedValue = _brandcar_idx.ToString();

    }

    protected void getEngineCar(DropDownList ddlName, int _engine_brand_idx)
    {

        data_carbooking data_m0_enginecar_detail = new data_carbooking();
        cbk_m0_enginecar_detail m0_enginecar_detail = new cbk_m0_enginecar_detail();
        data_m0_enginecar_detail.cbk_m0_enginecar_list = new cbk_m0_enginecar_detail[1];

        data_m0_enginecar_detail.cbk_m0_enginecar_list[0] = m0_enginecar_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_enginecar_detail = callServicePostCarBooking(_urlGetCbkm0EngineCar, data_m0_enginecar_detail);

        setDdlData(ddlName, data_m0_enginecar_detail.cbk_m0_enginecar_list, "engine_brand_name", "engine_brand_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกยี่ห้อเครื่องยนต์ ---", "0"));
        ddlName.SelectedValue = _engine_brand_idx.ToString();

    }

    protected void getFuelCar(DropDownList ddlName, int _fuel_idx)
    {

        data_carbooking data_m0_fuelcar_detail = new data_carbooking();
        cbk_m0_fuelcar_detail m0_fuelcar_detail = new cbk_m0_fuelcar_detail();
        data_m0_fuelcar_detail.cbk_m0_fuelcar_list = new cbk_m0_fuelcar_detail[1];

        data_m0_fuelcar_detail.cbk_m0_fuelcar_list[0] = m0_fuelcar_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_fuelcar_detail = callServicePostCarBooking(_urlGetCbkm0FuelCar, data_m0_fuelcar_detail);

        setDdlData(ddlName, data_m0_fuelcar_detail.cbk_m0_fuelcar_list, "fuel_name", "fuel_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกเชื้อเพลิง ---", "0"));
        ddlName.SelectedValue = _fuel_idx.ToString();

    }

    protected void getDetailCar()
    {

        data_carbooking data_m0_car_detail = new data_carbooking();
        cbk_m0_car_detail m0_car_detail = new cbk_m0_car_detail();
        data_m0_car_detail.cbk_m0_car_list = new cbk_m0_car_detail[1];

        m0_car_detail.condition = 0;

        data_m0_car_detail.cbk_m0_car_list[0] = m0_car_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail));        
        data_m0_car_detail = callServicePostCarBooking(_urlGetCbkm0DetailCar, data_m0_car_detail);

        ViewState["vs_DetailCar"] = data_m0_car_detail.cbk_m0_car_list;

        div_GvCarDetail.Visible = true;
        GvCarDetail.Visible = true;
        setGridData(GvCarDetail, ViewState["vs_DetailCar"]);

    }

    protected void actionReadCarDetail(int _m0_car_idx)
    {

        data_carbooking data_m0_car_detail_view = new data_carbooking();
        cbk_m0_car_detail m0_car_detail_view = new cbk_m0_car_detail();
        data_m0_car_detail_view.cbk_m0_car_list = new cbk_m0_car_detail[1];

        m0_car_detail_view.condition = 0;
        m0_car_detail_view.m0_car_idx = _m0_car_idx;

        data_m0_car_detail_view.cbk_m0_car_list[0] = m0_car_detail_view;
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_room_detail_edit));        
        data_m0_car_detail_view = callServicePostCarBooking(_urlGetCbkm0DetailCarView, data_m0_car_detail_view);
        FvDetailCar.Visible = true;
        setFormData(FvDetailCar, FormViewMode.ReadOnly, data_m0_car_detail_view.cbk_m0_car_list);
        //linkBtnTrigger(btnSaveUpdate);

    }

    protected void actionEditCarDetail(int _m0_car_idx)
    {

        data_carbooking data_m0_car_detail_view = new data_carbooking();
        cbk_m0_car_detail m0_car_detail_view = new cbk_m0_car_detail();
        data_m0_car_detail_view.cbk_m0_car_list = new cbk_m0_car_detail[1];

        m0_car_detail_view.condition = 0;
        m0_car_detail_view.m0_car_idx = _m0_car_idx;

        data_m0_car_detail_view.cbk_m0_car_list[0] = m0_car_detail_view;
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_room_detail_edit));        
        data_m0_car_detail_view = callServicePostCarBooking(_urlGetCbkm0DetailCarView, data_m0_car_detail_view);
        FvDetailCar.Visible = true;
        setFormData(FvDetailCar, FormViewMode.Edit, data_m0_car_detail_view.cbk_m0_car_list);
        //linkBtnTrigger(btnSaveUpdate);

    }

    protected void actionLogCarTax(int _m0_car_idx)
    {

        data_carbooking data_m0_car_detail_log = new data_carbooking();
        cbk_m0_car_detail m0_car_detail_log = new cbk_m0_car_detail();
        data_m0_car_detail_log.cbk_m0_car_list = new cbk_m0_car_detail[1];

        //m0_car_detail_log.condition = 0;
        m0_car_detail_log.m0_car_idx = _m0_car_idx;

        data_m0_car_detail_log.cbk_m0_car_list[0] = m0_car_detail_log;
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_room_detail_edit));        
        data_m0_car_detail_log = callServicePostCarBooking(_urlGetCbkLogTaxRoomDetail, data_m0_car_detail_log);

        //div_LogTax.Visible = true;
        Panel_LogTax.Visible = true;
        GvLogTax.Visible = true;
        ViewState["vs_DetailCar_logTax"] = data_m0_car_detail_log.cbk_m0_car_list;
        //setRepeaterData(rptLogTax, data_m0_car_detail_log.cbk_m0_car_list);
        setGridData(GvLogTax, ViewState["vs_DetailCar_logTax"]);
        //setFormData(FvDetailCar, FormViewMode.ReadOnly, data_m0_car_detail_view.cbk_m0_car_list);
        //linkBtnTrigger(btnSaveUpdate);

    }

    protected void Insert_TopicMA()
    {

        TextBox txt_topic_name = (TextBox)FvInsert.FindControl("txt_topic_name");
        //TextBox txt_detailtopic_name = (TextBox)FvInsert.FindControl("txt_detailtopic_name");
        DropDownList ddlStatusTopic = (DropDownList)FvInsert.FindControl("ddlStatusTopic");

        data_carbooking data_m0_topic_ma = new data_carbooking();
        cbk_m0_topic_ma_detail m0_topic_ma_insert = new cbk_m0_topic_ma_detail();
        data_m0_topic_ma.cbk_m0_topic_ma_list = new cbk_m0_topic_ma_detail[1];

        m0_topic_ma_insert.topic_ma_idx = 0;
        m0_topic_ma_insert.topic_ma_name = txt_topic_name.Text;
        //m0_topic_ma_insert.detail_ma_name = txt_detailtopic_name.Text;
        m0_topic_ma_insert.cemp_idx = _emp_idx;
        m0_topic_ma_insert.topic_ma_status = int.Parse(ddlStatusTopic.SelectedValue);

        data_m0_topic_ma.cbk_m0_topic_ma_list[0] = m0_topic_ma_insert;

        data_m0_topic_ma = callServicePostCarBooking(_urlSetCbkm0TopicMa, data_m0_topic_ma);

        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
        if (data_m0_topic_ma.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            txt_topic_name.Text = String.Empty;
            //txt_detailtopic_name.Text = String.Empty;
        }
    }

    protected void Select_TopicMA(int _codition_topic)
    {

        data_carbooking data_m0_topic_detail = new data_carbooking();
        cbk_m0_topic_ma_detail m0_topic_detail = new cbk_m0_topic_ma_detail();
        data_m0_topic_detail.cbk_m0_topic_ma_list = new cbk_m0_topic_ma_detail[1];

        m0_topic_detail.condition = _codition_topic;

        data_m0_topic_detail.cbk_m0_topic_ma_list[0] = m0_topic_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_topic_detail = callServicePostCarBooking(_urlGetCbkTopicMA, data_m0_topic_detail);

        if (_codition_topic == 0)
        {
            setGridData(GvDetail, data_m0_topic_detail.cbk_m0_topic_ma_list);
        }
        else
        {
            setGridData(GvTopicDetailMa, data_m0_topic_detail.cbk_m0_topic_ma_list);
        }

        //Gv_select_place.DataSource = m0_place_detail.qa_cims_m0_place_list;
        //Gv_select_place.DataBind();
    }

    protected void getDetailCarMA()
    {

        data_carbooking data_u0doc_ma = new data_carbooking();
        cbk_u0_document_ma_detail u0doc_ma_detail = new cbk_u0_document_ma_detail();
        data_u0doc_ma.cbk_u0_document_ma_list = new cbk_u0_document_ma_detail[1];

        u0doc_ma_detail.condition = 0;

        data_u0doc_ma.cbk_u0_document_ma_list[0] = u0doc_ma_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail));        
        data_u0doc_ma = callServicePostCarBooking(_urlGetCbkDetailCarMA, data_u0doc_ma);

        ViewState["vs_DetailCarMA"] = data_u0doc_ma.cbk_u0_document_ma_list;

        GVDetailCarMA.Visible = true;
        setGridData(GVDetailCarMA, ViewState["vs_DetailCarMA"]);

    }

    protected void getViewDetailCarMA(int _u0_document_ma_idx_view)
    {

        data_carbooking data_u0doc_viewma = new data_carbooking();
        cbk_u0_document_ma_detail u0doc_ma_viewdetail = new cbk_u0_document_ma_detail();
        data_u0doc_viewma.cbk_u0_document_ma_list = new cbk_u0_document_ma_detail[1];

        u0doc_ma_viewdetail.u0_document_ma_idx = _u0_document_ma_idx_view;

        data_u0doc_viewma.cbk_u0_document_ma_list[0] = u0doc_ma_viewdetail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail));        
        data_u0doc_viewma = callServicePostCarBooking(_urlGetCbkViewDetailCarMA, data_u0doc_viewma);

        ViewState["vs_ViewDetailCarMA"] = data_u0doc_viewma.cbk_u0_document_ma_list;

        FvViewDetailCarMa.Visible = true;
        setFormData(FvViewDetailCarMa, FormViewMode.ReadOnly, data_u0doc_viewma.cbk_u0_document_ma_list);


    }

    protected void getTopicDetailCarMA(int _u0_document_ma_idx_viewtopic)
    {

        data_carbooking data_u2doc_topic = new data_carbooking();
        cbk_u2_document_ma_detail u2doc_ma_topic = new cbk_u2_document_ma_detail();
        data_u2doc_topic.cbk_u2_document_ma_list = new cbk_u2_document_ma_detail[1];

        u2doc_ma_topic.u0_document_ma_idx = _u0_document_ma_idx_viewtopic;

        data_u2doc_topic.cbk_u2_document_ma_list[0] = u2doc_ma_topic;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail));        
        data_u2doc_topic = callServicePostCarBooking(_urlGetCbkViewTopicDetailCarMA, data_u2doc_topic);

        ViewState["vs_ViewDetailTopicCarMA"] = data_u2doc_topic.cbk_u2_document_ma_list;

        setGridData(GvTopicDetailCarMa, ViewState["vs_ViewDetailTopicCarMA"]);
        //setFormData(FvViewDetailCarMa, FormViewMode.ReadOnly, data_u2doc_topic.cbk_u0_document_ma_list);


    }

    protected void getLogDetailCarMA(int _u0_document_ma_idx_log)
    {

        data_carbooking data_u1_logma = new data_carbooking();
        cbk_u1_document_ma_detail u1_logma = new cbk_u1_document_ma_detail();
        data_u1_logma.cbk_u1_document_ma_list = new cbk_u1_document_ma_detail[1];

        u1_logma.u0_document_ma_idx = _u0_document_ma_idx_log;

        data_u1_logma.cbk_u1_document_ma_list[0] = u1_logma;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail));        
        data_u1_logma = callServicePostCarBooking(_urlGetCbkLogDataioCarMA, data_u1_logma);

        ViewState["vs_LogViewDetailTopicCarMA"] = data_u1_logma.cbk_u1_document_ma_list;

        setRepeaterData(rptLogDetailCarMA, ViewState["vs_LogViewDetailTopicCarMA"]);

    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
    }

    protected void getddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    #endregion bind data

    #region selected Changed
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {
            case "ddlTypeCar":

                DropDownList ddlTypeCar = (DropDownList)FvInsertDetailCar.FindControl("ddlTypeCar");
                DropDownList ddlDetailTypeCar = (DropDownList)FvInsertDetailCar.FindControl("ddlDetailTypeCar");


                getDetailTypeCar(ddlDetailTypeCar, int.Parse(ddlTypeCar.SelectedValue), 0);
                //linkBtnTrigger(btnSaveDetailMA);
                break;
            case "ddlTypeCar_edit":

                DropDownList ddlTypeCar_edit = (DropDownList)FvDetailCar.FindControl("ddlTypeCar_edit");
                DropDownList ddlDetailTypeCar_edit = (DropDownList)FvDetailCar.FindControl("ddlDetailTypeCar_edit");
                TextBox txt_detailtype_car_idx = (TextBox)FvDetailCar.FindControl("txt_detailtype_car_idx");

                if (ddlTypeCar_edit.SelectedValue != "0")
                {
                    getDetailTypeCar(ddlDetailTypeCar_edit, int.Parse(ddlTypeCar_edit.SelectedValue), 0);

                    //litDebug.Text = ddlTypeCar_edit.SelectedValue.ToString();
                }


                break;
            case "ddlTypeCarFilter":

                if (ddlTypeCarFilter.SelectedValue != "0")
                {
                    //setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                    //litDebug.Text = "1";

                    data_carbooking data_m0_car_detail = new data_carbooking();
                    cbk_m0_car_detail m0_car_detail = new cbk_m0_car_detail();
                    data_m0_car_detail.cbk_m0_car_list = new cbk_m0_car_detail[1];

                    m0_car_detail.condition = 0;

                    data_m0_car_detail.cbk_m0_car_list[0] = m0_car_detail;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail));        
                    data_m0_car_detail = callServicePostCarBooking(_urlGetCbkm0DetailCar, data_m0_car_detail);



                    cbk_m0_car_detail[] _item_FilterStatus = (cbk_m0_car_detail[])data_m0_car_detail.cbk_m0_car_list;

                    var _linq_FilterStatusIndex = (from data in _item_FilterStatus
                                                   where
                                                   data.type_car_idx == int.Parse(ddlTypeCarFilter.SelectedValue)

                                                   select data
                                            ).ToList();

                    _set_statusFilter = int.Parse(ddlTypeCarFilter.SelectedValue);

                    //

                    ViewState["vs_DetailCar"] = _linq_FilterStatusIndex;
                    setGridData(GvCarDetail, ViewState["vs_DetailCar"]);
                    //ViewState["Va_DetailCarbooking"] = null;

                }
                else
                {

                    getDetailCar();

                }

                break;

            case "ddlTypeCar_MA":

                DropDownList ddlTypeCar_MA = (DropDownList)FvInsertCarMA.FindControl("ddlTypeCar_MA");
                DropDownList ddlCarregister_MA = (DropDownList)FvInsertCarMA.FindControl("ddlCarregister_MA");
                UpdatePanel Panel_CreateMA_ = (UpdatePanel)FvInsertCarMA.FindControl("Panel_CreateMA");
                TextBox txt_DetailTypeCar_ = (TextBox)FvInsertCarMA.FindControl("txt_DetailTypeCar_ma");
                TextBox txt_CostCenter_detail_ = (TextBox)FvInsertCarMA.FindControl("txt_CostCenter_detail_ma");

                txt_DetailTypeCar_.Text = string.Empty;
                txt_CostCenter_detail_.Text = string.Empty;

                ////Panel_CreateMA_.Visible = false;
                getCarregisterCarMA(ddlCarregister_MA, int.Parse(ddlTypeCar_MA.SelectedValue), 0);

                break;

            case "ddlCarregister_MA":


                DropDownList ddlCarregister_MA_ = (DropDownList)FvInsertCarMA.FindControl("ddlCarregister_MA");
                UpdatePanel Panel_CreateMA = (UpdatePanel)FvInsertCarMA.FindControl("Panel_CreateMA");
                TextBox txt_DetailTypeCar = (TextBox)FvInsertCarMA.FindControl("txt_DetailTypeCar_ma");
                TextBox txt_CostCenter_detail = (TextBox)FvInsertCarMA.FindControl("txt_CostCenter_detail_ma");

                txt_DetailTypeCar.Text = string.Empty;
                txt_CostCenter_detail.Text = string.Empty;

                ////Panel_CreateMA.Visible = false;
                setGridData(GvTopicDetailMa, null);

                if (ddlCarregister_MA_.SelectedValue != "0")
                {
                    ////Panel_CreateMA.Visible = true;
                    getDetailTypeCostMA(int.Parse(ddlCarregister_MA_.SelectedValue));

                    //get condition topic online
                    linkBtnTrigger(btnSaveDetailMA);
                    Select_TopicMA(1);


                }
                else
                {
                    ////Panel_CreateMA.Visible = false;

                }

                break;
            case "ddlTypeReport":

                Update_PnTableReportDetail.Visible = false;
                ////Update_PnGraphReportDetail.Visible = false;

                Update_PanelReportTable.Visible = false;
                ////Update_PanelGraph.Visible = false;

                ddlSearchDateReport.ClearSelection();
                //// ddlTypeGraph.ClearSelection();
                //// ddlmonth.ClearSelection();

                ddlTypeCarMA_report.ClearSelection();

                txtAddEndDateReport.Enabled = false;
                txtAddStartdateReport.Text = string.Empty;
                txtAddEndDateReport.Text = string.Empty;

                ddlrdeptidx.Items.Clear();
                ddlrdeptidx.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));

                ddlCarregisterMa_report.Items.Clear();
                ddlCarregisterMa_report.Items.Insert(0, new ListItem("--- เลือกทะเบียนรถ ---", "0"));


                ////switch (ddlTypeReport.SelectedValue)
                ////{
                ////    case "1": //ตาราง
                ////        Update_PnTableReportDetail.Visible = true;
                ////        getOrganizationList(ddlorgidx);
                ////        //getPlace(ddlPlaceReportSearchDetail, 0);
                ////        //getStatusDocumentReport(ddlStatusCarBooking, 0);
                ////        //getDetailCarUse(ddlCarUseReportSearchDetail, 0);

                ////        //setActiveTab(string activeTab, int uidx, int u1idx, int staidx, int _chk_tab)
                ////        //setActiveTab("docReport", 0, 0, 0, int.Parse(ddlTypeReport.SelectedValue));

                ////        break;
                ////    case "2": //กราฟ

                ////        //Update_PnGraphReportDetail.Visible = true;
                ////        //getddlYear(ddlyear);

                ////        //Update_PnTableReportDetail.Visible = false;
                ////        //setActiveTab("docReport", 0, 0, 0, int.Parse(ddlTypeReport.SelectedValue));
                ////        break;

                ////}

                break;
            case "ddlTypeCarMA_report":

                //DropDownList ddlTypeCar_MA = (DropDownList)FvInsertCarMA.FindControl("ddlTypeCar_MA");
                //DropDownList ddlCarregister_MA = (DropDownList)FvInsertCarMA.FindControl("ddlCarregister_MA");
                //UpdatePanel Panel_CreateMA_ = (UpdatePanel)FvInsertCarMA.FindControl("Panel_CreateMA");
                //TextBox txt_DetailTypeCar_ = (TextBox)FvInsertCarMA.FindControl("txt_DetailTypeCar_ma");
                //TextBox txt_CostCenter_detail_ = (TextBox)FvInsertCarMA.FindControl("txt_CostCenter_detail_ma");

                //txt_DetailTypeCar_.Text = string.Empty;
                //txt_CostCenter_detail_.Text = string.Empty;

                //Panel_CreateMA_.Visible = false;

                getCarregisterCarMA(ddlCarregisterMa_report, int.Parse(ddlTypeCarMA_report.SelectedValue), 0);

                break;
            case "ddlSearchDateReport":
                if (int.Parse(ddlSearchDateReport.SelectedValue) == 3) //between
                {
                    txtAddEndDateReport.Enabled = true;
                }
                else
                {
                    txtAddEndDateReport.Enabled = false;
                }
                break;
            case "ddlorgidx":
                getDepartmentList(ddlrdeptidx, int.Parse(ddlorgidx.SelectedValue));
                break;

        }
    }

    #endregion selected Changed

    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {
                case "FvDetailCar":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {
                        Label lblm0_car_idx_detail = (Label)FvName.FindControl("lblm0_car_idx_detail");

                        //litDebug.Text = lblm0_car_idx_detail.Text;
                        if (lblm0_car_idx_detail.Text != "")
                        {
                            var btnViewCarDetail = (HyperLink)FvName.FindControl("btnViewCarDetail");

                            string filePath_View = ConfigurationManager.AppSettings["path_flie_cardetail"];
                            string directoryName_View = lblm0_car_idx_detail.Text;//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text;

                            if (Directory.Exists(Server.MapPath(filePath_View + directoryName_View)))
                            {
                                string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View + directoryName_View));
                                List<ListItem> files = new List<ListItem>();
                                foreach (string path in filesPath)
                                {
                                    string getfiles = "";
                                    getfiles = Path.GetFileName(path);
                                    btnViewCarDetail.NavigateUrl = filePath_View + directoryName_View + "/" + getfiles;

                                    HtmlImage imgedit = new HtmlImage();
                                    imgedit.Src = filePath_View + directoryName_View + "/" + getfiles;
                                    imgedit.Height = 100;
                                    btnViewCarDetail.Controls.Add(imgedit);

                                }
                                btnViewCarDetail.Visible = true;
                            }
                            else
                            {
                                btnViewCarDetail.Visible = false;
                            }

                        }



                    }

                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {
                        //////linkBtnTrigger(btnSaveUpdate);
                        ////CleardataSetEquimentListUpdate();

                        Label lbl_m0_car_idx_edit = (Label)FvName.FindControl("lbl_m0_car_idx_edit");
                        TextBox txt_type_car_idx = (TextBox)FvName.FindControl("txt_type_car_idx");
                        DropDownList ddlTypeCar_edit = (DropDownList)FvName.FindControl("ddlTypeCar_edit");

                        TextBox txt_detailtype_car_idx = (TextBox)FvName.FindControl("txt_detailtype_car_idx");
                        DropDownList ddlDetailTypeCar_edit = (DropDownList)FvName.FindControl("ddlDetailTypeCar_edit");

                        TextBox txt_car_province_idx = (TextBox)FvName.FindControl("txt_car_province_idx");
                        DropDownList ddlcar_province = (DropDownList)FvName.FindControl("ddlcar_province");

                        TextBox txt_admin_idx = (TextBox)FvName.FindControl("txt_admin_idx");
                        DropDownList ddladmin_idx = (DropDownList)FvName.FindControl("ddladmin_idx");

                        TextBox txt_costcenter_idx = (TextBox)FvName.FindControl("txt_costcenter_idx");
                        DropDownList ddlcostcenter_idx = (DropDownList)FvName.FindControl("ddlcostcenter_idx");

                        TextBox txt_type_idx = (TextBox)FvName.FindControl("txt_type_idx");
                        DropDownList ddltype_idx = (DropDownList)FvName.FindControl("ddltype_idx");

                        TextBox txt_brand_car_idx = (TextBox)FvName.FindControl("txt_brand_car_idx");
                        DropDownList ddlbrand_car_idx = (DropDownList)FvName.FindControl("ddlbrand_car_idx");

                        TextBox txt_engine_brand_idx = (TextBox)FvName.FindControl("txt_engine_brand_idx");
                        DropDownList ddl_engine_brand_idx_edit = (DropDownList)FvName.FindControl("ddl_engine_brand_idx_edit");

                        TextBox txt_fuel_idx = (TextBox)FvName.FindControl("txt_fuel_idx");
                        DropDownList ddlfuel_idx_edit = (DropDownList)FvName.FindControl("ddlfuel_idx_edit");

                        //Label lbl_place_name_edit = (Label)FvName.FindControl("lbl_place_name_edit");
                        ////Label lbl_room_name_en_edit = (Label)FvName.FindControl("lbl_room_name_en_edit");
                        ////DropDownList ddlPlaceUpdate = (DropDownList)FvName.FindControl("ddlPlaceUpdate");

                        getTypeCar(ddlTypeCar_edit, int.Parse(txt_type_car_idx.Text));
                        getDetailTypeCar(ddlDetailTypeCar_edit, int.Parse(ddlTypeCar_edit.SelectedValue), int.Parse(txt_detailtype_car_idx.Text));
                        getDetailProvince(ddlcar_province, int.Parse(txt_car_province_idx.Text));
                        getDetailAdmin(ddladmin_idx, int.Parse(txt_admin_idx.Text));
                        getCostcenter(ddlcostcenter_idx, int.Parse(txt_costcenter_idx.Text));
                        getType(ddltype_idx, int.Parse(txt_type_idx.Text));
                        getBrandCar(ddlbrand_car_idx, int.Parse(txt_brand_car_idx.Text));
                        getEngineCar(ddl_engine_brand_idx_edit, int.Parse(txt_engine_brand_idx.Text));
                        getFuelCar(ddlfuel_idx_edit, int.Parse(txt_fuel_idx.Text));


                        //////File Datil Edit
                        //////var lbl_m0_room_idx_file = (Label)FvName.FindControl("lbl_m0_room_idx");
                        //////var lbl_place_name_file = (Label)FvName.FindControl("lbl_place_name");
                        //////var lbl_room_name_en_view_file = (Label)FvName.FindControl("lbl_room_name_en_view");

                        var btnViewCarDetailEdit = (HyperLink)FvName.FindControl("btnViewCarDetailEdit");

                        string filePath_View_edit = ConfigurationManager.AppSettings["path_flie_cardetail"];
                        string directoryName_View_edit = lbl_m0_car_idx_edit.Text;//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text;

                        if (Directory.Exists(Server.MapPath(filePath_View_edit + directoryName_View_edit)))
                        {
                            string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View_edit + directoryName_View_edit));
                            List<ListItem> files = new List<ListItem>();
                            foreach (string path in filesPath)
                            {
                                string getfiles = "";
                                getfiles = Path.GetFileName(path);
                                btnViewCarDetailEdit.NavigateUrl = filePath_View_edit + directoryName_View_edit + "/" + getfiles;

                                HtmlImage imgedit = new HtmlImage();
                                imgedit.Src = filePath_View_edit + directoryName_View_edit + "/" + getfiles;
                                imgedit.Height = 100;
                                btnViewCarDetailEdit.Controls.Add(imgedit);


                            }
                            btnViewCarDetailEdit.Visible = true;
                        }
                        else
                        {
                            btnViewCarDetailEdit.Visible = false;
                        }

                    }
                    break;
                case "FvViewDetailCarMa":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {
                        Label lbl_price_total_repair_viewma = (Label)FvName.FindControl("lbl_price_total_repair_viewma");
                        Label lbl_sum_price_total_repair_decimal_value = (Label)FvName.FindControl("lbl_sum_price_total_repair_decimal_value");

                        Label lbl_vat_viewma = (Label)FvName.FindControl("lbl_vat_viewma");
                        Label lbl_sum_vat_decimal_value = (Label)FvName.FindControl("lbl_sum_vat_decimal_value");

                        Label lbl_price_net_viewma = (Label)FvName.FindControl("lbl_price_net_viewma");
                        Label lbl_sum_price_net_decimal_value = (Label)FvName.FindControl("lbl_sum_price_net_decimal_value");

                        lbl_sum_price_total_repair_decimal_value.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_price_total_repair_viewma.Text));
                        lbl_sum_vat_decimal_value.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_vat_viewma.Text));
                        lbl_sum_price_net_decimal_value.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_price_net_viewma.Text));


                        //file detail
                        Label lbl_u0_document_ma_idx_viewma = (Label)FvName.FindControl("lbl_u0_document_ma_idx_viewma");
                        Label lbl_car_register_viewma = (Label)FvName.FindControl("lbl_car_register_viewma");


                        if (lbl_u0_document_ma_idx_viewma.Text != "")
                        {
                            var btnViewFileCarMa = (HyperLink)FvName.FindControl("btnViewFileCarMa");

                            string filePath_ViewCarma = ConfigurationManager.AppSettings["path_flie_car_ma"];
                            string directoryName_ViewCarma = lbl_car_register_viewma.Text + "-" + lbl_u0_document_ma_idx_viewma.Text;//_u0_doc_idx.ToString();//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text;

                            if (Directory.Exists(Server.MapPath(filePath_ViewCarma + directoryName_ViewCarma)))
                            {
                                string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_ViewCarma + directoryName_ViewCarma));
                                List<ListItem> files = new List<ListItem>();
                                foreach (string path in filesPath)
                                {
                                    string getfiles = "";
                                    getfiles = Path.GetFileName(path);
                                    btnViewFileCarMa.NavigateUrl = filePath_ViewCarma + directoryName_ViewCarma + "/" + getfiles;

                                    //HtmlImage imgedit = new HtmlImage();
                                    //imgedit.Src = filePath_View + directoryName_View + "/" + getfiles;
                                    //imgedit.Height = 100;
                                    //btnViewCarDetailOut.Controls.Add(imgedit);

                                }
                                btnViewFileCarMa.Visible = true;
                            }
                            else
                            {
                                btnViewFileCarMa.Visible = false;
                            }

                        }



                    }
                    break;

            }
        }
    }
    #endregion Formview Databind

    #region gridview
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {

            case "GvCarDetail":

                if (ViewState["vs_DetailCar"].ToString() != "")
                {
                    setGridData(GvCarDetail, ViewState["vs_DetailCar"]);
                }
                setOntop.Focus();

                break;
            case "GvLogTax":
                setGridData(GvLogTax, ViewState["vs_DetailCar_logTax"]);
                break;

            case "GvDetailTableReport":
                setGridData(GvDetailTableReport, ViewState["Vs_DetailReportCarMATable"]);
                break;
            case "GVDetailCarMA":
                setGridData(GVDetailCarMA, ViewState["vs_DetailCarMA"]);

                setOntop.Focus();
                break;



        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvCarDetail":

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    //DropDownList ddlStatusFilter = (DropDownList)e.Row.FindControl("ddlStatusFilter");
                    //getStatusDocument(ddlTypeCarFilter, _set_statusFilter);
                    getTypeCar(ddlTypeCarFilter, _set_statusFilter);
                    //getDetailTypeCar(ddlDetailTypeCar, int.Parse(ddlTypeCar.SelectedValue), 0);
                }

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lbl_m0_car_idx_detail = (Label)e.Row.FindControl("lbl_m0_car_idx_detail");
                    Label lbl_m0_car_status = (Label)e.Row.FindControl("lbl_m0_car_status");
                    Label m0_car_statusOnline = (Label)e.Row.FindControl("m0_car_statusOnline");
                    Label m0_car_statusOffline = (Label)e.Row.FindControl("m0_car_statusOffline");
                    //Repeater rptEquimentDetail = (Repeater)e.Row.FindControl("rptEquimentDetail");

                    ViewState["vs_m0_car_status"] = lbl_m0_car_status.Text;


                    if (ViewState["vs_m0_car_status"].ToString() == "1")
                    {
                        m0_car_statusOnline.Visible = true;
                    }
                    else if (ViewState["vs_m0_car_status"].ToString() == "0")
                    {
                        m0_car_statusOffline.Visible = true;
                    }

                    //View File In Detail Car
                    var btnViewFileCar = (HyperLink)e.Row.FindControl("btnViewFileCar");
                    //var btnViewFileRoom_ = (Image)e.Row.FindControl("btnViewFileRoom");

                    string filePath_View = ConfigurationManager.AppSettings["path_flie_cardetail"];
                    string directoryName = lbl_m0_car_idx_detail.Text;//lbl_place_name_file.Text.Trim() + lbl_room_name_en_view_file.Text + lbl_m0_room_idx_file.Text;

                    if (Directory.Exists(Server.MapPath(filePath_View + directoryName)))
                    {
                        string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View + directoryName));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string path in filesPath)
                        {
                            string getfiles = "";
                            getfiles = Path.GetFileName(path);
                            btnViewFileCar.NavigateUrl = filePath_View + directoryName + "/" + getfiles;
                            //btnViewFileRoom_.ImageUrl = filePath_View + directoryName + "/" + getfiles;

                            HtmlImage img = new HtmlImage();
                            img.Src = filePath_View + directoryName + "/" + getfiles;
                            img.Height = 100;
                            btnViewFileCar.Controls.Add(img);



                        }
                        btnViewFileCar.Visible = true;
                    }
                    else
                    {
                        btnViewFileCar.Visible = false;
                        //btnViewFileRoom_.Text = "-";
                    }
                    //View File In Detail Car

                }

                break;

            case "GvTopicDetailCarMa":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label lbl_price_total_ma = (Label)e.Row.FindControl("lbl_price_total_ma");
                    Label lbl_sum_price_total_decimal_value = (Label)e.Row.FindControl("lbl_sum_price_total_decimal_value");

                    Label lbl_price_unit_ma = (Label)e.Row.FindControl("lbl_price_unit_ma");
                    Label lbl_sum_price_unit_decimal_value = (Label)e.Row.FindControl("lbl_sum_price_unit_decimal_value");



                    lbl_sum_price_unit_decimal_value.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_price_unit_ma.Text));
                    lbl_sum_price_total_decimal_value.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_price_total_ma.Text));



                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    cbk_u2_document_ma_detail[] _item_Totalprice = (cbk_u2_document_ma_detail[])ViewState["vs_ViewDetailTopicCarMA"];

                    var _linqTotalTopicMA = (from data in _item_Totalprice
                                             select new
                                             {
                                                 data.price_total,
                                                 data.sum_price_total_decimal


                                             }).ToList();


                    if (_linqTotalTopicMA.Count() > 0)
                    {

                        for (int z = 0; z < _linqTotalTopicMA.Count(); z++)
                        {
                            //returnResult += Convert.ToInt32(_linqTest[z].sum_expenses.ToString());
                            tot_actual += Convert.ToDecimal(_linqTotalTopicMA[z].sum_price_total_decimal.ToString());


                        }
                    }

                    Label lit_total_price = (Label)e.Row.FindControl("lit_total_price");
                    //lbl.Text = returnResult.ToString();//ViewState["Total_Expenses"].ToString();//tot_actual.ToString();

                    lit_total_price.Text = String.Format("{0:N2}", tot_actual); // Convert.ToString(tot_actual);
                    //litDebug.Text = String.Format("{0:N2}", tot_actual);

                }

                break;

            case "GvDetailTableReport":

                decimal sum_price_total_repair = 0;
                decimal sum_discount = 0;
                decimal sum_vat = 0;
                decimal sum_price_net = 0;
                decimal sum_price_net_cal = 0;
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label lbl_u0_document_ma_idx_reportma = (Label)e.Row.FindControl("lbl_u0_document_ma_idx_reportma");
                    Label lbl_car_register_reportma = (Label)e.Row.FindControl("lbl_car_register_reportma");

                    GridView GvDetailTopicReport = (GridView)e.Row.Cells[1].FindControl("GvDetailTopicReport");

                    Label lbl_price_total_repair_reportma = (Label)e.Row.FindControl("lbl_price_total_repair_reportma");
                    Label lbl_price_total_repair_reportma_dicimal = (Label)e.Row.FindControl("lbl_price_total_repair_reportma_dicimal");

                    Label lbl_discount_reportma = (Label)e.Row.FindControl("lbl_discount_reportma");
                    Label lbl_discount_reportma_decimal = (Label)e.Row.FindControl("lbl_discount_reportma_decimal");

                    Label lbl_vat_reportma = (Label)e.Row.FindControl("lbl_vat_reportma");
                    Label lbl_vat_reportma_decimal = (Label)e.Row.FindControl("lbl_vat_reportma_decimal");

                    Label lbl_price_net_reportma = (Label)e.Row.FindControl("lbl_price_net_reportma");
                    Label lbl_price_net_reportma_decimal = (Label)e.Row.FindControl("lbl_price_net_reportma_decimal");

                    lbl_price_total_repair_reportma_dicimal.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_price_total_repair_reportma.Text));
                    lbl_discount_reportma_decimal.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_discount_reportma.Text));
                    lbl_vat_reportma_decimal.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_vat_reportma.Text));
                    lbl_price_net_reportma_decimal.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_price_net_reportma.Text));

                    //ViewState["u0idx"] = lbl_u0_document_ma_idx_reportma.Text;
                    //view file detail report

                    HyperLink btnViewFileCarmaReport = (HyperLink)e.Row.FindControl("btnViewFileCarmaReport");
                    //HiddenField hidFile112 = (HiddenField)e.Row.FindControl("hidFile112");
                    string filePath_ViewCarmaReport = ConfigurationManager.AppSettings["path_flie_car_ma"];
                    string directoryName_ViewCarmaReport = lbl_car_register_reportma.Text;
                    string fileName_ViewUsecar_U2 = directoryName_ViewCarmaReport + "-" + lbl_u0_document_ma_idx_reportma.Text;

                    if (Directory.Exists(Server.MapPath(filePath_ViewCarmaReport + fileName_ViewUsecar_U2)))
                    {

                        string[] filesPath_view = Directory.GetFiles(Server.MapPath(filePath_ViewCarmaReport + fileName_ViewUsecar_U2));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string path in filesPath_view)
                        {
                            string getfiles = "";
                            getfiles = Path.GetFileName(path);

                            btnViewFileCarmaReport.NavigateUrl = filePath_ViewCarmaReport + fileName_ViewUsecar_U2 + "/" + getfiles;

                            //litDebug.Text = filePath_view + directoryName + "/" + getfiles;
                        }
                        btnViewFileCarmaReport.Visible = true;
                    }
                    else
                    {
                        btnViewFileCarmaReport.Visible = false;

                    }
                    //view file detail report


                    //bind data
                    data_carbooking data_u2doc_topic_report = new data_carbooking();
                    cbk_u2_document_ma_detail u2doc_ma_topic_report = new cbk_u2_document_ma_detail();
                    data_u2doc_topic_report.cbk_u2_document_ma_list = new cbk_u2_document_ma_detail[1];

                    u2doc_ma_topic_report.u0_document_ma_idx = int.Parse(lbl_u0_document_ma_idx_reportma.Text);

                    data_u2doc_topic_report.cbk_u2_document_ma_list[0] = u2doc_ma_topic_report;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail));        
                    data_u2doc_topic_report = callServicePostCarBooking(_urlGetCbkViewTopicDetailCarMA, data_u2doc_topic_report);

                    ViewState["vs_ViewDetailTopicCarMA_report"] = data_u2doc_topic_report.cbk_u2_document_ma_list;
                    setGridData(GvDetailTopicReport, ViewState["vs_ViewDetailTopicCarMA_report"]);
                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    cbk_searchreport_macar_detail[] _item_Total_GvOut = (cbk_searchreport_macar_detail[])ViewState["Vs_DetailReportCarMATable"];

                    var _linqTotalTopicMA_gvout = (from data in _item_Total_GvOut
                                                   select new
                                                   {
                                                       data.price_total_repair,
                                                       data.discount,
                                                       data.vat,
                                                       data.price_net


                                                   }).ToList();

                    foreach (var item in _linqTotalTopicMA_gvout)
                    {

                        sum_price_total_repair += Convert.ToDecimal(item.price_total_repair);
                        sum_discount += Convert.ToDecimal(item.discount);
                        sum_vat += Convert.ToDecimal(item.vat);
                        sum_price_net += Convert.ToDecimal(item.price_net);
                    }
                    //litDebug.Text = sum.ToString();



                    //litDebug.Text = tot_actual_report.ToString();fffff
                    Label lit_price_total_report = (Label)e.Row.FindControl("lit_price_total_report");
                    lit_price_total_report.Text = String.Format("{0:N2}", Convert.ToDecimal(sum_price_total_repair.ToString())); //sum_price_total_repair.ToString();

                    Label lit_discount_report = (Label)e.Row.FindControl("lit_discount_report");
                    lit_discount_report.Text = String.Format("{0:N2}", Convert.ToDecimal(sum_discount.ToString())); //sum_price_total_repair.ToString();

                    Label lit_vat_report = (Label)e.Row.FindControl("lit_vat_report");
                    lit_vat_report.Text = String.Format("{0:N2}", Convert.ToDecimal(sum_vat.ToString())); //sum_price_total_repair.ToString();

                    Label lit_price_net_report = (Label)e.Row.FindControl("lit_price_net_report");

                    //sum_price_net_cal = (sum_price_net) - sum_discount;

                    lit_price_net_report.Text = String.Format("{0:N2}", Convert.ToDecimal(sum_price_net.ToString())); //sum_price_total_repair.ToString();

                    //litDebug.Text = sum_price_net.ToString();

                }


                break;

            case "GvDetailTopicReport":
                decimal sum = 0;
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lbl_u0_document_ma_idx_mareport = (Label)e.Row.FindControl("lbl_u0_document_ma_idx_mareport");
                    Label lbl_price_total_ma_report = (Label)e.Row.FindControl("lbl_price_total_mareport");
                    Label lbl_sum_price_total_decimal_value_report = (Label)e.Row.FindControl("lbl_sum_price_total_decimal_valuereport");

                    Label lbl_price_unit_ma_report = (Label)e.Row.FindControl("lbl_price_unit_mareport");
                    Label lbl_sum_price_unit_decimal_value_report = (Label)e.Row.FindControl("lbl_sum_price_unit_decimal_valuereport");

                    lbl_sum_price_unit_decimal_value_report.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_price_unit_ma_report.Text));
                    lbl_sum_price_total_decimal_value_report.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_price_total_ma_report.Text));

                    Label lblRowNumber = (Label)e.Row.FindControl("lblRowNumber");

                    //tot_actual_report += Convert.ToDecimal(lbl_sum_price_total_decimal_value_report.Text);


                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    cbk_u2_document_ma_detail[] _item_Totalprice = (cbk_u2_document_ma_detail[])ViewState["vs_ViewDetailTopicCarMA_report"];

                    var _linqTotalTopicMA = (from data in _item_Totalprice
                                             select new
                                             {
                                                 data.price_total,
                                                 data.sum_price_total_decimal


                                             }).ToList();

                    foreach (var item in _linqTotalTopicMA)
                    {

                        sum += Convert.ToDecimal(item.sum_price_total_decimal);
                    }
                    //litDebug.Text = sum.ToString();



                    //litDebug.Text = tot_actual_report.ToString();
                    Label lit_total_price_report = (Label)e.Row.FindControl("lit_total_price_report");
                    lit_total_price_report.Text = sum.ToString();
                }


                break;


        }
    }
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvDetail":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_topic_ma_status = (Label)e.Row.Cells[3].FindControl("lbl_topic_ma_status");
                    Label topic_ma_statusOnline = (Label)e.Row.Cells[3].FindControl("topic_ma_statusOnline");
                    Label topic_ma_statusOffline = (Label)e.Row.Cells[3].FindControl("topic_ma_statusOffline");

                    ViewState["vs_topic_status"] = lbl_topic_ma_status.Text;


                    if (ViewState["vs_topic_status"].ToString() == "1")
                    {
                        topic_ma_statusOnline.Visible = true;
                    }
                    else if (ViewState["vs_topic_status"].ToString() == "0")
                    {
                        topic_ma_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    btn_AddTopicMA.Visible = true;
                    FvInsert.Visible = false;

                }

                break;
            case "GvTopicDetailMa":

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    //cbk_searchreport_car_detail[] _item_Result = (cbk_searchreport_car_detail[])ViewState["Vs_ViewDetailReportCarTable"];

                    //var _linqTest = (from data in _item_Result
                    //                 select new
                    //                 {
                    //                     data.sum_expenses,
                    //                     data.sum_expenses_decimal


                    //                 }).ToList();


                    //if (_linqTest.Count() > 0)
                    //{

                    //    for (int z = 0; z < _linqTest.Count(); z++)
                    //    {
                    //        returnResult += Convert.ToInt32(_linqTest[z].sum_expenses.ToString());
                    //        tot_actual += Convert.ToDecimal(_linqTest[z].sum_expenses_decimal.ToString());


                    //    }
                    //}

                    //Label lbl = (Label)e.Row.FindControl("lit_total");
                    ////lbl.Text = returnResult.ToString();//ViewState["Total_Expenses"].ToString();//tot_actual.ToString();

                    //lbl.Text = String.Format("{0:N2}", tot_actual); // Convert.ToString(tot_actual);
                    //litDebug.Text = String.Format("{0:N2}", tot_actual);

                }

                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetail":
                GvDetail.EditIndex = e.NewEditIndex;
                Select_TopicMA(0);
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvDetail":

                var txt_topic_ma_idx_edit = (TextBox)GvDetail.Rows[e.RowIndex].FindControl("txt_topic_ma_idx_edit");
                var txt_topic_ma_name_edit = (TextBox)GvDetail.Rows[e.RowIndex].FindControl("txt_topic_ma_name_edit");
                //var txt_detail_ma_name_edit = (TextBox)GvDetail.Rows[e.RowIndex].FindControl("txt_detail_ma_name_edit");
                var ddltopic_ma_status_edit = (DropDownList)GvDetail.Rows[e.RowIndex].FindControl("ddltopic_ma_status_edit");


                GvDetail.EditIndex = -1;


                data_carbooking data_m0_topicma_edit = new data_carbooking();
                cbk_m0_topic_ma_detail m0_topicma_edit = new cbk_m0_topic_ma_detail();
                data_m0_topicma_edit.cbk_m0_topic_ma_list = new cbk_m0_topic_ma_detail[1];

                m0_topicma_edit.topic_ma_idx = int.Parse(txt_topic_ma_idx_edit.Text);
                m0_topicma_edit.topic_ma_name = txt_topic_ma_name_edit.Text;
                //m0_topicma_edit.detail_ma_name = txt_detail_ma_name_edit.Text;
                m0_topicma_edit.cemp_idx = _emp_idx;
                m0_topicma_edit.topic_ma_status = int.Parse(ddltopic_ma_status_edit.SelectedValue);

                data_m0_topicma_edit.cbk_m0_topic_ma_list[0] = m0_topicma_edit;
                data_m0_topicma_edit = callServicePostCarBooking(_urlSetCbkm0TopicMa, data_m0_topicma_edit);


                if (data_m0_topicma_edit.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                    Select_TopicMA(0);
                }
                else
                {
                    Select_TopicMA(0);
                }
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvDetail":
                GvDetail.EditIndex = -1;
                Select_TopicMA(0);
                //btn_addplace.Visible = true;
                //SETFOCUS_ONTOP.Focus();
                //Gv_select_unit.Visible = false;
                btn_AddTopicMA.Visible = true;
                FvInsert.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvDetail":
                GvDetail.PageIndex = e.NewPageIndex;
                GvDetail.DataBind();
                Select_TopicMA(0);

                setOntop.Focus();
                break;

            case "GvTopicDetailMa":
                GvTopicDetailMa.PageIndex = e.NewPageIndex;
                GvTopicDetailMa.DataBind();
                Select_TopicMA(1);

                setOntop.Focus();
                break;

        }
    }

    #endregion gridview

    #region Checkbox
    protected void onSelectedCheckChanged(object sender, EventArgs e)
    {
        var cbName = (CheckBox)sender;
        //var chkName = (CheckBoxList)sender;

        switch (cbName.ID)
        {
            case "chk_detailTopic_ma":

                //litDebug.Text = "5";
                //int count_ = 0;
                linkBtnTrigger(btnSaveDetailMA);
                foreach (GridViewRow row in GvTopicDetailMa.Rows)
                {
                    CheckBox chk_detailTopic_ma = (CheckBox)row.Cells[0].FindControl("chk_detailTopic_ma");
                    TextBox txt_Quantity = (TextBox)row.Cells[2].FindControl("txt_Quantity");
                    TextBox txt_price_uit = (TextBox)row.Cells[3].FindControl("txt_price_uit");
                    TextBox txt_price_total = (TextBox)row.Cells[4].FindControl("txt_price_total");



                    if (chk_detailTopic_ma.Checked)
                    {
                        //litDebug.Text = "1";

                        linkBtnTrigger(btnSaveDetailMA);

                        txt_Quantity.Visible = true;
                        txt_price_uit.Visible = true;
                        txt_price_total.Visible = true;

                        txt_Quantity.Enabled = true;
                        txt_price_uit.Enabled = true;

                        //txt_price_total.Enabled = true;
                        //break;
                    }
                    else
                    {
                        //litDebug.Text = "2";


                        txt_Quantity.Text = string.Empty;
                        txt_price_uit.Text = string.Empty;
                        txt_price_total.Text = string.Empty;

                        txt_Quantity.Visible = false;
                        txt_price_uit.Visible = false;
                        txt_price_total.Visible = false;


                        txt_Quantity.Enabled = false;
                        txt_price_uit.Enabled = false;
                        //txt_price_total.Enabled = false;
                    }
                    //count_++;
                }
                break;

        }
    }

    protected void TextBoxChanged(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;
        // fvActor1Node1
        //TextBox txt_material_search = (TextBox)fvActor1Node1.FindControl("txt_material_search");
        //TextBox txtSearchSampleCode = (TextBox)fvAnalyticalResults.FindControl("txtSearchSampleCode");
        //TextBox txt_material_search_edit = (TextBox)fvUserEditDocument.FindControl("txt_material_search_edit");


        switch (txtName.ID)
        {

            case "txt_Quantity":

                linkBtnTrigger(btnSaveDetailMA);

                foreach (GridViewRow row in GvTopicDetailMa.Rows)
                {
                    CheckBox chk_detailTopic_ma_txt = (CheckBox)row.Cells[0].FindControl("chk_detailTopic_ma");
                    TextBox txt_Quantity_txt = (TextBox)row.Cells[2].FindControl("txt_Quantity");
                    TextBox txt_price_uit_txt = (TextBox)row.Cells[3].FindControl("txt_price_uit");
                    TextBox txt_price_total_txt = (TextBox)row.Cells[4].FindControl("txt_price_total");
                    decimal total_price = 0;

                    if (chk_detailTopic_ma_txt.Checked)
                    {
                        if (txt_Quantity_txt.Text != "" && txt_price_uit_txt.Text != "")
                        {
                            linkBtnTrigger(btnSaveDetailMA);
                            //litDebug.Text = Convert.ToInt32(txt_Quantity_txt.Text).ToString();
                            total_price = Convert.ToDecimal(txt_Quantity_txt.Text) * Convert.ToDecimal(txt_price_uit_txt.Text);
                            //litDebug.Text = Convert.ToDecimal(total_price).ToString();
                            txt_price_total_txt.Text = Convert.ToDecimal(total_price).ToString();

                        }
                        else
                        {
                            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรอกราคา/หน่วย ---');", true);

                            break;
                        }
                    }


                }

                break;


            case "txt_price_uit":



                foreach (GridViewRow row in GvTopicDetailMa.Rows)
                {

                    CheckBox chk_detailTopic_ma_txt = (CheckBox)row.Cells[0].FindControl("chk_detailTopic_ma");
                    TextBox txt_Quantity_txt = (TextBox)row.Cells[2].FindControl("txt_Quantity");
                    TextBox txt_price_uit_txt = (TextBox)row.Cells[3].FindControl("txt_price_uit");
                    TextBox txt_price_total_txt = (TextBox)row.Cells[4].FindControl("txt_price_total");
                    decimal total_price = 0;
                    if (chk_detailTopic_ma_txt.Checked)
                    {
                        if (txt_Quantity_txt.Text != "" && txt_price_uit_txt.Text != "")
                        {
                            linkBtnTrigger(btnSaveDetailMA);
                            //litDebug.Text = Convert.ToInt32(txt_Quantity_txt.Text).ToString();
                            total_price = Convert.ToDecimal(txt_Quantity_txt.Text) * Convert.ToDecimal(txt_price_uit_txt.Text);
                            //litDebug.Text = Convert.ToDecimal(total_price).ToString();
                            txt_price_total_txt.Text = Convert.ToDecimal(total_price).ToString();

                        }
                        else
                        {
                            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรอกราคา/หน่วย --- !!!');", true);

                            break;
                        }
                    }


                }

                break;

        }

    }


    #endregion Checkbox

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0);

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            case "CmdSave":


                DropDownList ddlTypeCar = (DropDownList)FvInsertDetailCar.FindControl("ddlTypeCar");
                DropDownList ddlDetailTypeCar = (DropDownList)FvInsertDetailCar.FindControl("ddlDetailTypeCar");
                TextBox txt_car_register_insert = (TextBox)FvInsertDetailCar.FindControl("txt_car_register_insert");
                DropDownList ddlcar_province_insert = (DropDownList)FvInsertDetailCar.FindControl("ddlcar_province_insert");
                DropDownList ddladmin_idx_insert = (DropDownList)FvInsertDetailCar.FindControl("ddladmin_idx_insert");
                DropDownList ddlcostcenter_idx_insert = (DropDownList)FvInsertDetailCar.FindControl("ddlcostcenter_idx_insert");
                TextBox txt_asset_insert = (TextBox)FvInsertDetailCar.FindControl("txt_asset_insert");
                DropDownList ddltype_idx_insert = (DropDownList)FvInsertDetailCar.FindControl("ddltype_idx_insert");
                TextBox txt_car_look_insert = (TextBox)FvInsertDetailCar.FindControl("txt_car_look_insert");
                DropDownList ddlbrand_car_idx_insert = (DropDownList)FvInsertDetailCar.FindControl("ddlbrand_car_idx_insert");
                TextBox txt_carmodel_insert = (TextBox)FvInsertDetailCar.FindControl("txt_carmodel_insert");
                TextBox txt_car_genaration_insert = (TextBox)FvInsertDetailCar.FindControl("txt_car_genaration_insert");
                TextBox txt_car_color_insert = (TextBox)FvInsertDetailCar.FindControl("txt_car_color_insert");
                TextBox txt_car_number_insert = (TextBox)FvInsertDetailCar.FindControl("txt_car_number_insert");
                TextBox txt_car_number_located_insert = (TextBox)FvInsertDetailCar.FindControl("txt_car_number_located_insert");
                DropDownList ddl_engine_brand_idx_insert = (DropDownList)FvInsertDetailCar.FindControl("ddl_engine_brand_idx_insert");
                TextBox txt_engine_brand_insert = (TextBox)FvInsertDetailCar.FindControl("txt_engine_brand_insert");
                TextBox txt_engine_brand_located_insert = (TextBox)FvInsertDetailCar.FindControl("txt_engine_brand_located_insert");
                DropDownList ddlfuel_idx_insert = (DropDownList)FvInsertDetailCar.FindControl("ddlfuel_idx_insert");
                TextBox txt_gas_cylinder_insert = (TextBox)FvInsertDetailCar.FindControl("txt_gas_cylinder_insert");
                TextBox txt_count_pump_insert = (TextBox)FvInsertDetailCar.FindControl("txt_count_pump_insert");
                TextBox txt_cc_insert = (TextBox)FvInsertDetailCar.FindControl("txt_cc_insert");
                TextBox txt_horse_power_insert = (TextBox)FvInsertDetailCar.FindControl("txt_horse_power_insert");
                TextBox txt_car_weight_insert = (TextBox)FvInsertDetailCar.FindControl("txt_car_weight_insert");
                TextBox txt_car_load_insert = (TextBox)FvInsertDetailCar.FindControl("txt_car_load_insert");
                TextBox txt_total_weight_insert = (TextBox)FvInsertDetailCar.FindControl("txt_total_weight_insert");
                TextBox txt_count_seat_insert = (TextBox)FvInsertDetailCar.FindControl("txt_count_seat_insert");
                DropDownList ddlm0_car_status = (DropDownList)FvInsertDetailCar.FindControl("ddlm0_car_status");
                TextBox txt_tax_deadline_insert = (TextBox)FvInsertDetailCar.FindControl("txt_tax_deadline_insert");
                TextBox txt_price_tax_insert = (TextBox)FvInsertDetailCar.FindControl("txt_price_tax_insert");
                TextBox txt_date_insurance_term_insert = (TextBox)FvInsertDetailCar.FindControl("txt_date_insurance_term_insert");
                TextBox txt_date_car_insurance_deadline_insert = (TextBox)FvInsertDetailCar.FindControl("txt_date_car_insurance_deadline_insert");

                //insert Room Booking
                data_carbooking data_m0_car_detail = new data_carbooking();
                cbk_m0_car_detail m0_car_detail = new cbk_m0_car_detail();
                data_m0_car_detail.cbk_m0_car_list = new cbk_m0_car_detail[1];

                m0_car_detail.type_idx = int.Parse(ddltype_idx_insert.SelectedValue);
                m0_car_detail.detailtype_car_idx = int.Parse(ddlDetailTypeCar.SelectedValue);
                m0_car_detail.car_province_idx = int.Parse(ddlcar_province_insert.SelectedValue);
                m0_car_detail.car_register = txt_car_register_insert.Text;
                m0_car_detail.admin_idx = int.Parse(ddladmin_idx_insert.SelectedValue);
                m0_car_detail.costcenter_idx = int.Parse(ddlcostcenter_idx_insert.SelectedValue);
                m0_car_detail.asset_number = txt_asset_insert.Text;
                m0_car_detail.car_look = txt_car_look_insert.Text;
                m0_car_detail.brand_car_idx = int.Parse(ddlbrand_car_idx_insert.SelectedValue);
                m0_car_detail.type_car_idx = int.Parse(ddlTypeCar.SelectedValue);
                m0_car_detail.car_model = txt_carmodel_insert.Text;
                m0_car_detail.car_genaration = txt_car_genaration_insert.Text;
                m0_car_detail.car_color = txt_car_color_insert.Text;
                m0_car_detail.car_number = txt_car_number_insert.Text;
                m0_car_detail.car_number_located = txt_car_number_located_insert.Text;
                m0_car_detail.engine_brand_idx = int.Parse(ddl_engine_brand_idx_insert.SelectedValue);
                m0_car_detail.engine_number = txt_engine_brand_insert.Text;
                m0_car_detail.engine_number_located = txt_engine_brand_located_insert.Text;
                m0_car_detail.fuel_idx = int.Parse(ddlfuel_idx_insert.SelectedValue);
                m0_car_detail.gas_cylinder = txt_gas_cylinder_insert.Text;
                m0_car_detail.count_pump = int.Parse(txt_count_pump_insert.Text);
                m0_car_detail.cc = int.Parse(txt_cc_insert.Text);
                m0_car_detail.horse_power = int.Parse(txt_horse_power_insert.Text);
                m0_car_detail.car_weight = int.Parse(txt_car_weight_insert.Text);
                m0_car_detail.car_load = int.Parse(txt_car_load_insert.Text);
                m0_car_detail.total_weight = int.Parse(txt_total_weight_insert.Text);
                m0_car_detail.count_seat = int.Parse(txt_count_seat_insert.Text);
                m0_car_detail.cemp_idx = _emp_idx;
                m0_car_detail.m0_car_status = int.Parse(ddlm0_car_status.SelectedValue);

                m0_car_detail.tax_deadline = txt_tax_deadline_insert.Text;
                m0_car_detail.price_tax = txt_price_tax_insert.Text;
                m0_car_detail.date_insurance_term = txt_date_insurance_term_insert.Text;
                m0_car_detail.date_car_insurance_deadline = txt_date_car_insurance_deadline_insert.Text;

                data_m0_car_detail.cbk_m0_car_list[0] = m0_car_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_car_detail));// vs_StatusRoomBooking

                data_m0_car_detail = callServicePostCarBooking(_urlSetCbkm0CarDetail, data_m0_car_detail);

                if (data_m0_car_detail.return_code == 0)
                {
                    ViewState["vs_CreateCar_DetailFile"] = data_m0_car_detail.cbk_m0_car_list[0].m0_car_idx;
                    if (UploadFileCar.HasFile)
                    {
                        string getPathfile = ConfigurationManager.AppSettings["path_flie_cardetail"];
                        string m0_car_idx = ViewState["vs_CreateCar_DetailFile"].ToString();//ddlPlace_insert.SelectedItem + txt_roomname_en.Text + ViewState["vs_createRoom_DetailFile"].ToString();
                        string fileName_upload = m0_car_idx;
                        string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }
                        string extension = Path.GetExtension(UploadFileCar.FileName);

                        UploadFileCar.SaveAs(Server.MapPath(getPathfile + m0_car_idx) + "\\" + fileName_upload + extension);
                        //litDebug.Text = "5555";
                    }
                    else
                    {
                        //litDebug.Text = "3333";
                    }

                    setActiveTab("docDetailCar", 0, 0, 0, 0);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                    return;
                }

                break;


            case "cmdViewCarDetail":

                linkBtnTrigger(btnViewCarDetail);


                div_GvCarDetail.Visible = false;
                GvCarDetail.Visible = false;

                actionReadCarDetail(int.Parse(cmdArg));
                actionLogCarTax(int.Parse(cmdArg));

                linkBtnTrigger(btnEditDetailCar);

                setOntop.Focus();
                break;
            case "cmdBackToDetailIndexCar":
                setActiveTab("docDetailCar", 0, 0, 0, 0);

                break;

            case "CmdCancel":
                setActiveTab("docDetailCar", 0, 0, 0, 0);

                break;
            case "cmdEditDetailCar":
                //setActiveTab("docDetailCar", 0, 0, 0, 0);

                FvDetailCar.Visible = true;
                FvDetailCar.ChangeMode(FormViewMode.Edit);

                actionEditCarDetail(int.Parse(cmdArg));


                break;
            case "cmdDeleteCarDetail":

                data_carbooking data_m0_car_del = new data_carbooking();
                cbk_m0_car_detail m0_car_del = new cbk_m0_car_detail();
                data_m0_car_del.cbk_m0_car_list = new cbk_m0_car_detail[1];

                m0_car_del.m0_car_idx = int.Parse(cmdArg);
                m0_car_del.cemp_idx = _emp_idx;

                data_m0_car_del.cbk_m0_car_list[0] = m0_car_del;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_car_insert_tax));// vs_StatusRoomBooking ffff
                data_m0_car_del = callServicePostCarBooking(_urlSetCbkm0DetailCarDel, data_m0_car_del);

                setActiveTab("docDetailCar", 0, 0, 0, 0);

                break;

            case "cmdInsertTaxCar":

                ViewState["vs_m0_car_idx_Tax"] = int.Parse(cmdArg);

                FvDetailCar.Visible = false;

                FvInsertTax.Visible = true;
                FvInsertTax.ChangeMode(FormViewMode.Insert);

                break;

            case "CmdSaveTax":

                TextBox txt_tax_deadline_tax = (TextBox)FvInsertTax.FindControl("txt_tax_deadline_add");
                TextBox txt_price_tax_tax = (TextBox)FvInsertTax.FindControl("txt_price_tax_add");
                TextBox txt_date_insurance_term_tax = (TextBox)FvInsertTax.FindControl("txt_date_insurance_term_add");
                TextBox txt_date_car_insurance_deadline_tax = (TextBox)FvInsertTax.FindControl("txt_date_car_insurance_deadline_add");

                if (txt_tax_deadline_tax.Text != "" && txt_price_tax_tax.Text != "" && txt_date_insurance_term_tax.Text != "" && txt_date_car_insurance_deadline_tax.Text != "")
                {
                    data_carbooking data_m0_car_insert_tax = new data_carbooking();
                    cbk_m0_car_detail m0_car_insert_tax = new cbk_m0_car_detail();
                    data_m0_car_insert_tax.cbk_m0_car_list = new cbk_m0_car_detail[1];

                    m0_car_insert_tax.m0_car_idx = int.Parse(ViewState["vs_m0_car_idx_Tax"].ToString());
                    m0_car_insert_tax.tax_deadline = txt_tax_deadline_tax.Text;
                    m0_car_insert_tax.price_tax = txt_price_tax_tax.Text;
                    m0_car_insert_tax.date_insurance_term = txt_date_insurance_term_tax.Text;
                    m0_car_insert_tax.date_car_insurance_deadline = txt_date_car_insurance_deadline_tax.Text;
                    m0_car_insert_tax.cemp_idx = _emp_idx;

                    data_m0_car_insert_tax.cbk_m0_car_list[0] = m0_car_insert_tax;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_car_insert_tax));// vs_StatusRoomBooking ffff

                    data_m0_car_insert_tax = callServicePostCarBooking(_urlSetCbkm1CarDetailTax, data_m0_car_insert_tax);

                    if (data_m0_car_insert_tax.return_code == 0)
                    {
                        setActiveTab("docDetailCar", 0, 0, 0, 0);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลให้ครบถ้วนก่อนทำการบันทึก');", true);
                    return;
                }

                break;


            case "cmdSaveEdit":

                Label lbl_m0_car_idx_edit = (Label)FvDetailCar.FindControl("lbl_m0_car_idx_edit");
                DropDownList ddlTypeCar_edit = (DropDownList)FvDetailCar.FindControl("ddlTypeCar_edit");
                DropDownList ddlDetailTypeCar_edit = (DropDownList)FvDetailCar.FindControl("ddlDetailTypeCar_edit");
                TextBox txt_car_register = (TextBox)FvDetailCar.FindControl("txt_car_register");
                DropDownList ddlcar_province = (DropDownList)FvDetailCar.FindControl("ddlcar_province");
                DropDownList ddladmin_idx = (DropDownList)FvDetailCar.FindControl("ddladmin_idx");
                DropDownList ddlcostcenter_idx = (DropDownList)FvDetailCar.FindControl("ddlcostcenter_idx");
                TextBox txt_asset = (TextBox)FvDetailCar.FindControl("txt_asset");
                DropDownList ddltype_idx = (DropDownList)FvDetailCar.FindControl("ddltype_idx");
                TextBox txt_car_look = (TextBox)FvDetailCar.FindControl("txt_car_look");
                DropDownList ddlbrand_car_idx = (DropDownList)FvDetailCar.FindControl("ddlbrand_car_idx");
                TextBox txt_carmodel = (TextBox)FvDetailCar.FindControl("txt_carmodel");
                TextBox txt_car_genaration = (TextBox)FvDetailCar.FindControl("txt_car_genaration");
                TextBox txt_car_color = (TextBox)FvDetailCar.FindControl("txt_car_color");
                TextBox txt_car_number = (TextBox)FvDetailCar.FindControl("txt_car_number");
                TextBox txt_car_number_located = (TextBox)FvDetailCar.FindControl("txt_car_number_located");
                DropDownList ddl_engine_brand_idx_edit = (DropDownList)FvDetailCar.FindControl("ddl_engine_brand_idx_edit");
                TextBox txt_engine_brand_edit = (TextBox)FvDetailCar.FindControl("txt_engine_brand_edit");
                TextBox txt_engine_brand_located_edit = (TextBox)FvDetailCar.FindControl("txt_engine_brand_located_edit");
                DropDownList ddlfuel_idx_edit = (DropDownList)FvDetailCar.FindControl("ddlfuel_idx_edit");
                TextBox txt_gas_cylinder = (TextBox)FvDetailCar.FindControl("txt_gas_cylinder");
                TextBox txt_count_pump = (TextBox)FvDetailCar.FindControl("txt_count_pump");
                TextBox txt_cc = (TextBox)FvDetailCar.FindControl("txt_cc");
                TextBox txt_horse_power = (TextBox)FvDetailCar.FindControl("txt_horse_power");
                TextBox txt_car_weight = (TextBox)FvDetailCar.FindControl("txt_car_weight");
                TextBox txt_car_load = (TextBox)FvDetailCar.FindControl("txt_car_load");
                TextBox txt_total_weight = (TextBox)FvDetailCar.FindControl("txt_total_weight");
                TextBox txt_count_seat = (TextBox)FvDetailCar.FindControl("txt_count_seat");
                DropDownList ddlm0_car_status_edit = (DropDownList)FvDetailCar.FindControl("ddlm0_car_status_edit");
                TextBox txt_tax_deadline = (TextBox)FvDetailCar.FindControl("txt_tax_deadline");
                TextBox txt_price_tax = (TextBox)FvDetailCar.FindControl("txt_price_tax");
                TextBox txt_date_insurance_term = (TextBox)FvDetailCar.FindControl("txt_date_insurance_term");
                TextBox txt_date_car_insurance_deadline = (TextBox)FvDetailCar.FindControl("txt_date_car_insurance_deadline");
                TextBox txt_m1_car_idx_edit = (TextBox)FvDetailCar.FindControl("txt_m1_car_idx_edit");

                //insert Room Booking
                data_carbooking data_m0_car_edit = new data_carbooking();
                cbk_m0_car_detail m0_car_detail_edit = new cbk_m0_car_detail();
                data_m0_car_edit.cbk_m0_car_list = new cbk_m0_car_detail[1];

                m0_car_detail_edit.m0_car_idx = int.Parse(lbl_m0_car_idx_edit.Text);
                m0_car_detail_edit.m1_car_idx = int.Parse(txt_m1_car_idx_edit.Text);
                m0_car_detail_edit.type_idx = int.Parse(ddltype_idx.SelectedValue);
                m0_car_detail_edit.detailtype_car_idx = int.Parse(ddlDetailTypeCar_edit.SelectedValue);
                m0_car_detail_edit.car_province_idx = int.Parse(ddlcar_province.SelectedValue);
                m0_car_detail_edit.car_register = txt_car_register.Text;
                m0_car_detail_edit.admin_idx = int.Parse(ddladmin_idx.SelectedValue);
                m0_car_detail_edit.costcenter_idx = int.Parse(ddlcostcenter_idx.SelectedValue);
                m0_car_detail_edit.asset_number = txt_asset.Text;
                m0_car_detail_edit.car_look = txt_car_look.Text;
                m0_car_detail_edit.brand_car_idx = int.Parse(ddlbrand_car_idx.SelectedValue);
                m0_car_detail_edit.type_car_idx = int.Parse(ddlTypeCar_edit.SelectedValue);
                m0_car_detail_edit.car_model = txt_carmodel.Text;
                m0_car_detail_edit.car_genaration = txt_car_genaration.Text;
                m0_car_detail_edit.car_color = txt_car_color.Text;
                m0_car_detail_edit.car_number = txt_car_number.Text;
                m0_car_detail_edit.car_number_located = txt_car_number_located.Text;
                m0_car_detail_edit.engine_brand_idx = int.Parse(ddl_engine_brand_idx_edit.SelectedValue);
                m0_car_detail_edit.engine_number = txt_engine_brand_edit.Text;
                m0_car_detail_edit.engine_number_located = txt_engine_brand_located_edit.Text;
                m0_car_detail_edit.fuel_idx = int.Parse(ddlfuel_idx_edit.SelectedValue);
                m0_car_detail_edit.gas_cylinder = txt_gas_cylinder.Text;
                m0_car_detail_edit.count_pump = int.Parse(txt_count_pump.Text);
                m0_car_detail_edit.cc = int.Parse(txt_cc.Text);
                m0_car_detail_edit.horse_power = int.Parse(txt_horse_power.Text);
                m0_car_detail_edit.car_weight = int.Parse(txt_car_weight.Text);
                m0_car_detail_edit.car_load = int.Parse(txt_car_load.Text);
                m0_car_detail_edit.total_weight = int.Parse(txt_total_weight.Text);
                m0_car_detail_edit.count_seat = int.Parse(txt_count_seat.Text);
                m0_car_detail_edit.cemp_idx = _emp_idx;
                m0_car_detail_edit.m0_car_status = int.Parse(ddlm0_car_status_edit.SelectedValue);

                m0_car_detail_edit.tax_deadline = txt_tax_deadline.Text;
                m0_car_detail_edit.price_tax = txt_price_tax.Text;
                m0_car_detail_edit.date_insurance_term = txt_date_insurance_term.Text;
                m0_car_detail_edit.date_car_insurance_deadline = txt_date_car_insurance_deadline.Text;

                data_m0_car_edit.cbk_m0_car_list[0] = m0_car_detail_edit;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_car_edit));// vs_StatusRoomBooking ffff
                data_m0_car_edit = callServicePostCarBooking(_urlSetCbkm0CarDetail, data_m0_car_edit);

                if (data_m0_car_edit.return_code == 0)
                {
                    if (UploadFileCarEdit.HasFile)
                    {

                        string getPathfile_edit = ConfigurationManager.AppSettings["path_flie_cardetail"];
                        string directoryName_edit_old = lbl_m0_car_idx_edit.Text;
                        //lbl_place_name_edit_old.Text.Trim() + lbl_room_name_en_edit_old.Text + lblm0_room_idx_edit_old.Text;

                        string directoryName_edit_new = lbl_m0_car_idx_edit.Text;// ddlPlaceUpdate.SelectedItem + txt_roomname_en_Update.Text + lblm0_room_idx_edit_old.Text;
                        // string document_code = ViewState["_no_invoice"].ToString();

                        string filePath_old = Server.MapPath(getPathfile_edit + directoryName_edit_old);

                        if (!Directory.Exists(filePath_old))
                        {

                            string filePath_new = Server.MapPath(getPathfile_edit + directoryName_edit_new);

                            if (!Directory.Exists(filePath_new))
                            {
                                Directory.CreateDirectory(filePath_new);
                            }
                            string extension = Path.GetExtension(UploadFileCarEdit.FileName);

                            UploadFileCarEdit.SaveAs(Server.MapPath(getPathfile_edit + directoryName_edit_new) + "\\" + directoryName_edit_new + extension);

                        }
                        else
                        {
                            string path_old = string.Empty;
                            List<string> files = new List<string>();
                            DirectoryInfo dirListRoom = new DirectoryInfo(filePath_old);
                            foreach (FileInfo file in dirListRoom.GetFiles())
                            {
                                path_old = ResolveUrl(filePath_old + "\\" + file.Name);
                            }
                            File.Delete(path_old);

                            string extension = Path.GetExtension(UploadFileCarEdit.FileName);
                            UploadFileCarEdit.SaveAs(Server.MapPath(getPathfile_edit + directoryName_edit_old) + "\\" + directoryName_edit_old + extension);

                        }
                    }

                }

                setActiveTab("docDetailCar", 0, 0, 0, 0);


                break;
            case "cmdDetailCar":

                linkBtnTrigger(lbCreateDetail);
                setActiveTab("docCreateDetailCar", 0, 0, 0, int.Parse(cmdArg.ToString()));

                break;
            case "cmdCreateCarMA":

                linkBtnTrigger(lbCreateCar_MA);
                setActiveTab("docCreateDetailCar", 0, 0, 0, int.Parse(cmdArg.ToString()));
                break;
            case "cmdMasterTopicCar":
                setActiveTab("docMasterMA", 0, 0, 0, int.Parse(cmdArg.ToString()));
                break;

            case "cmdAddTopicMA":
                GvDetail.EditIndex = -1;
                Select_TopicMA(0);

                btn_AddTopicMA.Visible = false;
                setFormData(FvInsert, FormViewMode.Insert, null);
                FvInsert.Visible = true;
                break;
            case "CmdSaveTopicMA":
                Insert_TopicMA();
                Select_TopicMA(0);
                btn_AddTopicMA.Visible = true;
                FvInsert.Visible = false;

                setOntop.Focus();
                break;

            case "cmdCancel":
                btn_AddTopicMA.Visible = true;
                FvInsert.Visible = false;
                setOntop.Focus();
                break;

            case "cmdDelete":

                int topic_ma_idx_del = int.Parse(cmdArg);

                data_carbooking data_m0_topicma_del = new data_carbooking();
                cbk_m0_topic_ma_detail m0_topicma_del = new cbk_m0_topic_ma_detail();
                data_m0_topicma_del.cbk_m0_topic_ma_list = new cbk_m0_topic_ma_detail[1];

                m0_topicma_del.topic_ma_idx = topic_ma_idx_del;
                m0_topicma_del.cemp_idx = _emp_idx;

                data_m0_topicma_del.cbk_m0_topic_ma_list[0] = m0_topicma_del;

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_expenses_del));
                data_m0_topicma_del = callServicePostCarBooking(_urlSetCbkm0TopicMADel, data_m0_topicma_del);

                Select_TopicMA(0);
                //Gv_select_unit.Visible = false;
                btn_AddTopicMA.Visible = true;
                FvInsert.Visible = false;
                break;

            case "cmdSaveDetailMA":

                DropDownList ddlCarregister_MA_insert = (DropDownList)FvInsertCarMA.FindControl("ddlCarregister_MA");
                TextBox txt_miles_muturity_ma_insert = (TextBox)FvInsertCarMA.FindControl("txt_miles_muturity_ma");
                TextBox txt_miles_current_ma_insert = (TextBox)FvInsertCarMA.FindControl("txt_miles_current_ma");
                TextBox txt_company_repair_ma_insert = (TextBox)FvInsertCarMA.FindControl("txt_company_repair_ma");
                TextBox txt_comment_detailcreate_ma_insert = (TextBox)FvInsertCarMA.FindControl("txt_comment_detailcreate_ma");
                TextBox txt_prcode_ma_insert = (TextBox)FvInsertCarMA.FindControl("txt_prcode_ma");
                TextBox txt_pocode_ma_insert = (TextBox)FvInsertCarMA.FindControl("txt_pocode_ma");
                TextBox txt_create_repair_ma_insert = (TextBox)FvInsertCarMA.FindControl("txt_create_repair_ma");
                TextBox txt_create_repair_success_ma_insert = (TextBox)FvInsertCarMA.FindControl("txt_create_repair_success_ma");

                TextBox txt_count_total_ma_insert = (TextBox)FvInsertCarMA.FindControl("txt_count_total_ma");
                TextBox txt_discount_ma_insert = (TextBox)FvInsertCarMA.FindControl("txt_discount_ma");
                TextBox ttxt_price_tax_ma_insert = (TextBox)FvInsertCarMA.FindControl("txt_price_tax_ma");
                TextBox txt_net_price_ma_insert = (TextBox)FvInsertCarMA.FindControl("txt_net_price_ma");

                data_carbooking data_doc_ma = new data_carbooking();

                //u0_doc ma
                cbk_u0_document_ma_detail u0_doc_ma = new cbk_u0_document_ma_detail();
                data_doc_ma.cbk_u0_document_ma_list = new cbk_u0_document_ma_detail[1];

                u0_doc_ma.cemp_idx = _emp_idx;
                u0_doc_ma.m0_car_idx = int.Parse(ddlCarregister_MA_insert.SelectedValue);
                u0_doc_ma.miles_start = txt_miles_muturity_ma_insert.Text;
                u0_doc_ma.miles_current = txt_miles_current_ma_insert.Text;
                u0_doc_ma.company_detail = txt_company_repair_ma_insert.Text;
                u0_doc_ma.note_detail = txt_comment_detailcreate_ma_insert.Text;
                u0_doc_ma.pr_code = txt_prcode_ma_insert.Text;
                u0_doc_ma.po_code = txt_pocode_ma_insert.Text;
                u0_doc_ma.date_start_repair = txt_create_repair_ma_insert.Text;
                u0_doc_ma.date_end_repair = txt_create_repair_success_ma_insert.Text;

                u0_doc_ma.price_total_repair = txt_count_total_ma_insert.Text;
                u0_doc_ma.discount = txt_discount_ma_insert.Text;
                u0_doc_ma.vat = txt_price_tax_ma.Text;
                u0_doc_ma.price_net = txt_net_price_ma.Text;
                u0_doc_ma.m0_actor_idx = 4;
                u0_doc_ma.m0_node_idx = 6;
                u0_doc_ma.decision = 7;

                var _u2_document_ma = new cbk_u2_document_ma_detail[GvTopicDetailMa.Rows.Count];
                //u1_doc ma
                int count_ = 0;
                int sum_total_price = 0;


                foreach (GridViewRow gv_row_topic in GvTopicDetailMa.Rows)
                {

                    data_doc_ma.cbk_u2_document_ma_list = new cbk_u2_document_ma_detail[1];

                    Label lbl_topic_ma_idx = (Label)gv_row_topic.FindControl("lbl_topic_ma_idx");
                    CheckBox chk_detailTopic_ma = (CheckBox)gv_row_topic.FindControl("chk_detailTopic_ma");
                    TextBox txt_Quantity = (TextBox)gv_row_topic.FindControl("txt_Quantity");
                    TextBox txt_price_uit = (TextBox)gv_row_topic.FindControl("txt_price_uit");
                    TextBox txt_price_total = (TextBox)gv_row_topic.FindControl("txt_price_total");
                    Label lb_sum_counttotal = (Label)gv_row_topic.FindControl("lb_sum_counttotal");

                    if (chk_detailTopic_ma.Checked == true)
                    {

                        if (txt_Quantity.Text != "" && txt_price_uit.Text != "" && txt_price_total.Text != "")
                        {
                            _u2_document_ma[count_] = new cbk_u2_document_ma_detail();
                            _u2_document_ma[count_].topic_ma_idx = int.Parse(lbl_topic_ma_idx.Text);
                            _u2_document_ma[count_].quantity = int.Parse(txt_Quantity.Text);
                            _u2_document_ma[count_].price_unit = txt_price_uit.Text;
                            _u2_document_ma[count_].price_total = txt_price_total.Text;

                            sum_total_price += int.Parse(txt_price_total.Text);

                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณากรอกข้อมูลให้ครบถ้วน ---');", true);
                            break;
                        }

                    }

                    count_++;

                }

                //check total count price before save car ma
                decimal val_cal = 0;
                if (txt_count_total_ma_insert.Text != "")
                {
                    val_cal = Convert.ToDecimal(txt_count_total_ma_insert.Text) - Convert.ToDecimal(sum_total_price);
                }

                //litDebug.Text = val_cal.ToString();

                if (val_cal > 0)
                {

                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ราคารวมไม่เท่ากับข้อมูลที่กรอก ---');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('ราคารวมน้อยกว่าข้อมูลที่กรอก = {0} บาท');", val_cal.ToString()), true);
                    break;
                }
                else if (val_cal < 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('ราคารวมมากกว่าข้อมูลที่กรอก = {0} บาท');", val_cal.ToString()), true);
                    break;
                }
                else
                {
                    data_doc_ma.cbk_u0_document_ma_list[0] = u0_doc_ma;
                    data_doc_ma.cbk_u2_document_ma_list = _u2_document_ma;

                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_doc_ma));

                    //hasfile save file

                    if (UploadFileCarma != null)
                    {
                        //litDebug.Text = ddlCarregister_MA_insert.SelectedItem.ToString();
                        if (UploadFileCarma.HasFile)
                        {

                            string extension = Path.GetExtension(UploadFileCarma.FileName);

                            //litDebug.Text = extension.ToString();
                            if (extension.ToLower() == ".jpg" || extension.ToLower() == ".png" || extension.ToLower() == ".pdf")
                            {
                                //litDebug.Text = (getPathfile + fileName_upload) + "\\" + fileName_upload1 + extension;

                                data_doc_ma = callServicePostCarBooking(_urlSetCbkSaveDetailCarMa, data_doc_ma);

                                string getPathfile = ConfigurationManager.AppSettings["path_flie_car_ma"];

                                var u0_doc_maidx = data_doc_ma.cbk_u0_document_ma_list[0].u0_document_ma_idx;
                                string carma_file_name = ddlCarregister_MA_insert.SelectedItem.ToString() + "-" + u0_doc_maidx;//ViewState["vs_createRoom_DetailFile"].ToString();
                                string fileName_upload1 = ddlCarregister_MA_insert.SelectedItem.ToString();

                                string fileName_upload = carma_file_name;

                                string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                                //litDebug.Text = filePath_upload.ToString();

                                if (!Directory.Exists(filePath_upload))
                                {
                                    Directory.CreateDirectory(filePath_upload);
                                }

                                UploadFileCarma.SaveAs(Server.MapPath(getPathfile + fileName_upload) + "\\" + fileName_upload1 + extension);
                                //check_file_out = 0;

                            }
                            else
                            {
                                //litDebug.Text = "1111";
                                // _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");fffff
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะไฟล์ Excel (นามสกุลไฟล์ .jpg , .png หรือ .pdf) เท่านั้น');", true);
                                //check_file_out = 1;
                                break;
                            }
                            //UploadFile_UseCar.SaveAs(Server.MapPath(getPathfile + car_file_name) + "\\" + fileName_upload + extension);

                        }
                        else
                        {
                            data_doc_ma = callServicePostCarBooking(_urlSetCbkSaveDetailCarMa, data_doc_ma);
                            //litDebug.Text = "not have file";
                        }

                    }



                    setActiveTab("docDetailCarMA", 0, 0, 0, 0);
                }

                break;
            case "cmdViewDetailCarMA":

                int u0_document_ma_idx_view = int.Parse(cmdArg);

                setActiveTab("docDetailCarMA", u0_document_ma_idx_view, 0, 0, 1);

                break;
            case "cmdBackToDetailCarMa":

                setActiveTab("docDetailCarMA", 0, 0, 0, 0);

                break;

            case "cmdCancelDetailMA":

                setActiveTab("docDetailCarMA", 0, 0, 0, 0);

                break;
            case "cmdSearchReportCarDetail":

                data_carbooking data_report_macar = new data_carbooking();
                cbk_searchreport_macar_detail search_report_macar = new cbk_searchreport_macar_detail();
                data_report_macar.cbk_searchreport_macar_list = new cbk_searchreport_macar_detail[1];

                search_report_macar.condition = 0;
                search_report_macar.cemp_idx = _emp_idx;
                //search_report_macar.staidx = int.Parse(ddlStatusCarBooking.SelectedValue);
                search_report_macar.IFSearchbetween = int.Parse(ddlSearchDateReport.SelectedValue);
                search_report_macar.date_start = txtAddStartdateReport.Text;
                search_report_macar.date_end = txtAddEndDateReport.Text;
                search_report_macar.org_idx = int.Parse(ddlorgidx.SelectedValue);
                search_report_macar.rdept_idx = int.Parse(ddlrdeptidx.SelectedValue);
                search_report_macar.type_car_idx = int.Parse(ddlTypeCarMA_report.SelectedValue);
                search_report_macar.m0_car_idx = int.Parse(ddlCarregisterMa_report.SelectedValue);


                data_report_macar.cbk_searchreport_macar_list[0] = search_report_macar;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_macar));

                if (txtAddEndDateReport.Text != "")
                {

                    IFormatProvider culture_report = new CultureInfo("en-US", true);
                    //DateTime DateToday_Set = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture); //Date Time Today Incheck
                    DateTime DateTime_Start_Check = DateTime.ParseExact(txtAddStartdateReport.Text, "dd/MM/yyyy", culture_report);
                    DateTime DateTime_End_Check = DateTime.ParseExact(txtAddEndDateReport.Text, "dd/MM/yyyy", culture_report);

                    var result_day = DateTime_End_Check.Subtract(DateTime_Start_Check).TotalDays; // result time chrck > 30 min because cancel in room

                    //litDebug.Text = result_day.ToString();
                    if (result_day > 0)
                    {
                        data_report_macar = callServicePostCarBooking(_urlGetCbkSearchReportTableCarMA, data_report_macar);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_carbooking));

                        Update_PanelReportTable.Visible = true;
                        ViewState["Vs_DetailReportCarMATable"] = data_report_macar.cbk_searchreport_macar_list;
                        setGridData(GvDetailTableReport, ViewState["Vs_DetailReportCarMATable"]);

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาเช็ควันที่ค้นก่อนที่การค้นหา!!! ---');", true);
                        break;
                    }

                }
                else
                {
                    data_report_macar = callServicePostCarBooking(_urlGetCbkSearchReportTableCarMA, data_report_macar);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking));

                    Update_PanelReportTable.Visible = true;
                    ViewState["Vs_DetailReportCarMATable"] = data_report_macar.cbk_searchreport_macar_list;
                    setGridData(GvDetailTableReport, ViewState["Vs_DetailReportCarMATable"]);
                }
                break;
            case "cmdRefreshReport":

                setActiveTab("docReport", 0, 0, 0, 0);

                break;

        }

    }
    //endbtn

    #endregion event command

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {
        setActiveTab("docDetailCar", 0, 0, 0, 0);
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_carbooking callServicePostCarBooking(string _cmdUrl, data_carbooking _data_carbooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_carbooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_carbooking = (data_carbooking)_funcTool.convertJsonToObject(typeof(data_carbooking), _localJson);


        return _data_carbooking;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int u1idx, int staidx, int _chk_tab)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        //set tab create
        setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);
        setFormData(FvInsertDetailCar, FormViewMode.ReadOnly, null);
        FvDetailCar.Visible = false;

        setFormData(FvInsertCarMA, FormViewMode.ReadOnly, null);

        //set Detail Car Booking
        div_LogTax.Visible = false;
        Panel_LogTax.Visible = false;
        GvLogTax.Visible = false;
        setFormData(FvInsertTax, FormViewMode.ReadOnly, null);

        //tab master data ma
        setFormData(FvInsert, FormViewMode.ReadOnly, null);
        btn_AddTopicMA.Visible = false;

        //tab detail ma
        FvViewDetailCarMa.Visible = false;
        //setFormData(FvViewDetailCarMa, FormViewMode.ReadOnly, null);
        Div_DetailCarMa.Visible = false;
        divGVDetailCarMA_scroll_x.Visible = false;
        GVDetailCarMA.Visible = false;
        div_LogViewDetailCarMA.Visible = false;
        Update_BackToDetailCarMa.Visible = false;

        //tab report
        Update_PanelSearchReport.Visible = false;
        Update_PanelReportTable.Visible = false;
        Update_PnTableReportDetail.Visible = false;

        Update_PanelReportTable.Visible = false;
        ddlSearchDateReport.ClearSelection();

        ddlTypeCarMA_report.ClearSelection();

        txtAddEndDateReport.Enabled = false;
        txtAddStartdateReport.Text = string.Empty;
        txtAddEndDateReport.Text = string.Empty;

        ddlrdeptidx.Items.Clear();
        ddlrdeptidx.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));

        ddlCarregisterMa_report.Items.Clear();
        ddlCarregisterMa_report.Items.Insert(0, new ListItem("--- เลือกทะเบียนรถ ---", "0"));


        //tab index
        GvCarDetail.Visible = false;
        div_GvCarDetail.Visible = false;


        switch (activeTab)
        {

            case "docDetailCar":

                getDetailCar();
                setOntop.Focus();

                break;
            case "docDetailCarMA":
                switch (_chk_tab)
                {
                    case 0:
                        Div_DetailCarMa.Visible = true;
                        divGVDetailCarMA_scroll_x.Visible = true;
                        getDetailCarMA();

                        setOntop.Focus();
                        break;

                    case 1:
                        getViewDetailCarMA(uidx);
                        getTopicDetailCarMA(uidx);

                        div_LogViewDetailCarMA.Visible = true;
                        Update_BackToDetailCarMa.Visible = true;

                        getLogDetailCarMA(uidx);

                        setOntop.Focus();
                        break;
                }

                break;

            case "docCreateDetailCar":
                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                switch (_chk_tab)
                {
                    case 1:


                        setFormData(FvInsertDetailCar, FormViewMode.Insert, null);

                        //getTypeCar()
                        DropDownList ddlTypeCar = (DropDownList)FvInsertDetailCar.FindControl("ddlTypeCar");
                        DropDownList ddlcar_province_insert = (DropDownList)FvInsertDetailCar.FindControl("ddlcar_province_insert");
                        DropDownList ddladmin_idx_insert = (DropDownList)FvInsertDetailCar.FindControl("ddladmin_idx_insert");

                        DropDownList ddlcostcenter_idx_insert = (DropDownList)FvInsertDetailCar.FindControl("ddlcostcenter_idx_insert");
                        DropDownList ddltype_idx_insert = (DropDownList)FvInsertDetailCar.FindControl("ddltype_idx_insert");
                        DropDownList ddlbrand_car_idx_insert = (DropDownList)FvInsertDetailCar.FindControl("ddlbrand_car_idx_insert");
                        DropDownList ddl_engine_brand_idx_insert = (DropDownList)FvInsertDetailCar.FindControl("ddl_engine_brand_idx_insert");
                        DropDownList ddlfuel_idx_insert = (DropDownList)FvInsertDetailCar.FindControl("ddlfuel_idx_insert");

                        getTypeCar(ddlTypeCar, 0);
                        getDetailProvince(ddlcar_province_insert, 0);
                        getDetailAdmin(ddladmin_idx_insert, 0);

                        getCostcenter(ddlcostcenter_idx_insert, 0);
                        getType(ddltype_idx_insert, 0);
                        getBrandCar(ddlbrand_car_idx_insert, 0);
                        getEngineCar(ddl_engine_brand_idx_insert, 0);
                        getFuelCar(ddlfuel_idx_insert, 0);

                        linkBtnTrigger(btnSave);


                        break;
                    case 2:

                        setFormData(FvInsertCarMA, FormViewMode.Insert, null);
                        DropDownList ddlTypeCar_MA = (DropDownList)FvInsertCarMA.FindControl("ddlTypeCar_MA");

                        getTypeCar(ddlTypeCar_MA, 0);
                        linkBtnTrigger(btnSaveDetailMA);


                        break;
                }

                break;
            case "docReport":

                Update_PanelSearchReport.Visible = true;
                Update_PnTableReportDetail.Visible = true;
                getOrganizationList(ddlorgidx);
                getTypeCar(ddlTypeCarMA_report, 0);


                break;
            case "docMasterMA":

                switch (_chk_tab)
                {
                    case 1:
                        btn_AddTopicMA.Visible = true;
                        Select_TopicMA(0);
                        break;
                }

                break;


        }
    }

    protected void setActiveTab(string activeTab, int uidx, int u1idx, int staidx, int _chk_tab)
    {
        setActiveView(activeTab, uidx, u1idx, staidx, _chk_tab);
        switch (activeTab)
        {
            case "docDetailCar":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");

                liCreateDetail.Attributes.Add("class", "");
                liCreateCar_MA.Attributes.Add("class", "");

                lilblCreateTopicMA.Attributes.Add("class", "");

                break;
            case "docDetailCarMA":

                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                li4.Attributes.Add("class", "");

                liCreateDetail.Attributes.Add("class", "");
                liCreateCar_MA.Attributes.Add("class", "");

                lilblCreateTopicMA.Attributes.Add("class", "");

                break;
            case "docCreateDetailCar":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");

                switch(_chk_tab)
                {
                    case 1:
                        liCreateDetail.Attributes.Add("class", "active");
                        liCreateCar_MA.Attributes.Add("class", "");
                        break;
                    case 2:
                        liCreateDetail.Attributes.Add("class", "");
                        liCreateCar_MA.Attributes.Add("class", "active");
                        break;
                }

                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");

                lilblCreateTopicMA.Attributes.Add("class", "");
                break;
            case "docReport":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "active");

                liCreateDetail.Attributes.Add("class", "");
                liCreateCar_MA.Attributes.Add("class", "");

                lilblCreateTopicMA.Attributes.Add("class", "");
                break;
            case "docMasterMA":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");

                switch (_chk_tab)
                {
                    case 1:
                        lilblCreateTopicMA.Attributes.Add("class", "active");
                        //liCreateCar_MA.Attributes.Add("class", "");
                        break;

                }


                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                

                liCreateDetail.Attributes.Add("class", "");
                liCreateCar_MA.Attributes.Add("class", "");
                break;
        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {
            case "rp_place":

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    //var chk_coler = (Label)e.Item.FindControl("lbcheck_coler");
                    //var btnPlaceLab = (LinkButton)e.Item.FindControl("btnPlaceLab");

                    //for (int k = 0; k <= rp_place.Items.Count; k++)
                    //{
                    //    btnPlaceLab.CssClass = ConfigureColors(k);
                    //    //Console.WriteLine(i);
                    //}


                }

                break;
        }
    }

    #endregion reuse

}